##sample comment
#curdir=$(pwd)
#for f in $curdir/*/;
#    do 
 #       [ -d $f ] && cd "$f" && echo Entering into $f and building packages
 #       if [ ! -f build.sh ];then
 #           echo "build.sh doesn't exists in $f"
 #       else
 #           sh build.sh
 #       fi
 #   done;

    
    
#!/bin/bash   
source build.config

branch=default
bucket=builds-vedantu-qa
if [ "$CI_BRANCH" == "master" ] || [ "$CI_BRANCH" == "develop" ] 
	then 
		branch="$CI_BRANCH"
fi


export BUILD_PROFILE=${profile}
export BUILD_ALL=${buildAll}


cd utils
if [ "$profile" == "prod" ] 
	then
	echo "deleting swagger utils"
	rm -r src/main/java/com/vedantu/util/swagger
fi
mvn clean install 
cd ..


if [ "$skipCI" == "false" ] 
then
tag=$tag
#if [ "$CI_BRANCH" == "master" ] 
#	then 
#		tag="$(git describe --tags --exact-match "$CI_COMMIT_ID"~1)"
#fi
#echo $tag
if [ "$tag" == "" ] 
	then 
		tag=$CI_COMMIT_ID
fi
echo $tag
echo $profile

if [ "$profile" == "prod" ] || [ "$profile" == "qa" ] 
	then 
		export AWS_DEFAULT_REGION="ap-southeast-1"
		export AWS_ACCESS_KEY_ID="AKIAJNCJQKYFXLSY54LA"
		export AWS_SECRET_ACCESS_KEY="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"
		bucket=builds-vedantu-mumbai
else
	if [ "$buildAll" == "true" ] 
		then
			subsystemsToBuild="platform dinero listing lms notification-centre scheduling subscription user growth"
			
	fi		
fi

export BUILD_BUCKET=${bucket}
echo "build bucket is $BUILD_BUCKET"
export HOME="/home/rof/src/bitbucket.org/vedantu"
# export HOME="/Users/ajith/projects/git"
export REPO=vedantu-platform-workspace

IFS=' ' read -a subsystems <<< "${subsystemsToBuild}"

mkdir -p "${HOME}/build/all"
mkdir -p "${HOME}/build/tar"

for i in "${subsystems[@]}"
do
   echo "$i"
   
   cd "${HOME}/${REPO}/$i"
   var=$(pwd)
   echo "${var}"
   chmod +x buildprod.sh
   ./buildprod.sh
done


echo "copying appspec and deploymentScripts to build all folder"
cp "${HOME}/${REPO}/appspec.yml" "${HOME}/build/all/appspec.yml"
cp -R "${HOME}/${REPO}/deploymentScripts" "${HOME}/build/all/deploymentScripts"



#creating utility function for deploying or copying
deployorcopy ()
{
	if [ "$deployDirectly" == "true" ];
		then
		echo "copying the tar file directly to $profile server"
		ssh ubuntu@$profile-rtc.vedantu.com "
		  sudo rm -r /home/ubuntu/server-artifacts/warfiles
		  mkdir /home/ubuntu/server-artifacts/warfiles		  				  
		"
		scp "${BUILD_PROFILE}-${allTag}.tar.gz" ubuntu@$profile-rtc.vedantu.com:/home/ubuntu/server-artifacts/warfiles
		echo "copying done"
		ssh ubuntu@$profile-rtc.vedantu.com "
			cd /home/ubuntu/server-artifacts/warfiles
			echo "extracting ${BUILD_PROFILE}-${allTag}.tar.gz"
			sudo tar -xvf ${BUILD_PROFILE}-${allTag}.tar.gz
			sleepsecs=45
			for entry in *
			do			  				  
				if [[ "\$entry" == *".war"* ]]; then
				  echo "copying \$entry"
				  sudo cp -R \$entry /home/ubuntu/server-artifacts/tomcat/webapps/
				  echo "waiting for \$sleepsecs secs"
				  sleep \$sleepsecs
				fi
			done	
		"			
		echo "deployment done"	
	else
		echo "uploading  to s3"
		aws s3 cp . "s3://${bucket}/${REPO}/${branch}/combined" --recursive --exclude "*" --include "*.tar.gz"				
	fi	
}


if [ "$profile" == "prod" ] || [ "$profile" == "qa" ] 
	then 
	echo "not creating combined builds"
else
	if [ "$buildAll" == "true" ] 
		then
			tar -czvf "${BUILD_PROFILE}-${allTag}.tar.gz" -C "${HOME}/build/all" .
			mv "${BUILD_PROFILE}-${allTag}.tar.gz" "${HOME}/build/tar/${BUILD_PROFILE}-${allTag}.tar.gz"
			cd "${HOME}/build/tar"
			deployorcopy
	else
		echo "creating combined build for requested subsystems"
		combined=""
		for i in "${subsystems[@]}"
		do
			combined="$combined $i.war"
		done

		combined="${combined#"${combined%%[![:space:]]*}"}"		

		cd "${HOME}/build/all"		
	 	tar -czvf "${BUILD_PROFILE}-${allTag}.tar.gz" $combined appspec.yml deploymentScripts	 		
	 	deployorcopy
	fi
fi

fi

