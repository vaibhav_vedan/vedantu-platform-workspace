package com.vedantu.dashboard.fs.parser;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.dashboard.entities.CMDSQuestion;
import com.vedantu.dashboard.managers.AmazonS3Manager;
import com.vedantu.dashboard.pojo.SolutionFormat;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionRepositoryDocParser {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger LOGGER = logFactory.getLogger(QuestionRepositoryDocParser.class);



    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    public void populateAWSUrls(List<CMDSQuestion> questions) {
        // Generate AWS urls
        if (!CollectionUtils.isEmpty(questions)) {
            for (CMDSQuestion question : questions) {
                if (question.getQuestionBody() != null && !CollectionUtils.isEmpty(question.getQuestionBody().getUuidImages())) {
                    for (CMDSImageDetails imageDetails : question.getQuestionBody().getUuidImages()) {
                        String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                        imageDetails.setPublicUrl(publicUrl);
                        if (!StringUtils.isEmpty(question.getQuestionBody().getNewText())
                                && question.getQuestionBody().getNewText().contains(imageDetails.getFileName())) {
                            question.getQuestionBody().setNewText(question.getQuestionBody().getNewText()
                                    .replace(imageDetails.getFileName(), publicUrl));
                        }
                    }
                }

                if (question.getSolutionInfo() != null && !CollectionUtils.isEmpty((question.getSolutionInfo().getSolutions()))) {
                    for (SolutionFormat solutionFormat : question.getSolutionInfo().getSolutions()) {
                        if (!StringUtils.isEmpty(solutionFormat.getNewText())
                                && !CollectionUtils.isEmpty(solutionFormat.getUuidImages())) {
                            for (CMDSImageDetails imageDetails : solutionFormat.getUuidImages()) {
                                if (solutionFormat.getNewText().contains(imageDetails.getFileName())) {
                                    String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                                    solutionFormat.setNewText(solutionFormat.getNewText().replace(imageDetails.getFileName(),
                                            publicUrl));
                                }
                            }
                        }
                    }
                }

                if (question.getSolutionInfo() != null && question.getSolutionInfo().getOptionBody() != null) {
                    if (!CollectionUtils.isEmpty(question.getSolutionInfo().getOptionBody().newOptions)
                            && !CollectionUtils.isEmpty(question.getSolutionInfo().getOptionBody().uuidImages)) {
                        for (CMDSImageDetails imageDetails : question.getSolutionInfo().getOptionBody().uuidImages) {
                            for (int i = 0; i < question.getSolutionInfo().getOptionBody().newOptions.size(); i++) {
                                String optionText = question.getSolutionInfo().getOptionBody().newOptions.get(i);
                                if (StringUtils.isNotEmpty(optionText) && optionText.contains(imageDetails.getFileName())) {
                                    String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                                    question.getSolutionInfo().getOptionBody().newOptions.set(i, optionText.replace(imageDetails.getFileName(),
                                            publicUrl));
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}
