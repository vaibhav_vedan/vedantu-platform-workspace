package com.vedantu.dashboard.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewAttemptedTestRes {

    private String testTitle;
    private String subject;
    private boolean evaluated;
    private Long evaluatedTime;
    private Long attemptedTime;
    private String contentInfoId;
    private String attemptId;
    private Float marksAchieved;
    private Float totalMarks;
    private Integer correct;
    private Integer incorrect;
    private Integer unattempted;
    private Double testAverage;
}
