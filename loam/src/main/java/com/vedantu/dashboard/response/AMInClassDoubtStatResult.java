package com.vedantu.dashboard.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMInClassDoubtStatResult {
    private String _id;
    private long count;
}
