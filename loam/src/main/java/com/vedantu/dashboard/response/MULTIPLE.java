package com.vedantu.dashboard.response;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class MULTIPLE {
	private List<com.vedantu.dashboard.response.SINGLE> SINGLE;

	private MULTIPLE() {
	}

	@XmlElement
	public List<com.vedantu.dashboard.response.SINGLE> getSINGLE() {
		return SINGLE;
	}

	public void setSINGLE(List<com.vedantu.dashboard.response.SINGLE> sINGLE) {
		SINGLE = sINGLE;
	}
}
