/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.response;

/**
 *
 * @author parashar
 */
public class AMTestStats {
    private String groupName;
    private String testId;
    private String testName;
    private Float averageScore;
    private Integer numOfStudentsAttempted;
    private Integer numOfStudentsAbove65;
    private Integer numOfStudentsAbove75;
    private Float zScore;
    private Integer numOfStudentsAboveZScore;

    /**
     * @return the bundleId
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the bundleId to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the testId
     */
    public String getTestId() {
        return testId;
    }

    /**
     * @param testId the testId to set
     */
    public void setTestId(String testId) {
        this.testId = testId;
    }

    /**
     * @return the testName
     */
    public String getTestName() {
        return testName;
    }

    /**
     * @param testName the testName to set
     */
    public void setTestName(String testName) {
        this.testName = testName;
    }

    /**
     * @return the averageScore
     */
    public Float getAverageScore() {
        return averageScore;
    }

    /**
     * @param averageScore the averageScore to set
     */
    public void setAverageScore(Float averageScore) {
        this.averageScore = averageScore;
    }

    /**
     * @return the numOfStudentsAttempted
     */
    public Integer getNumOfStudentsAttempted() {
        return numOfStudentsAttempted;
    }

    /**
     * @param numOfStudentsAttempted the numOfStudentsAttempted to set
     */
    public void setNumOfStudentsAttempted(Integer numOfStudentsAttempted) {
        this.numOfStudentsAttempted = numOfStudentsAttempted;
    }

    /**
     * @return the numOfStudentsAbove65
     */
    public Integer getNumOfStudentsAbove65() {
        return numOfStudentsAbove65;
    }

    /**
     * @param numOfStudentsAbove65 the numOfStudentsAbove65 to set
     */
    public void setNumOfStudentsAbove65(Integer numOfStudentsAbove65) {
        this.numOfStudentsAbove65 = numOfStudentsAbove65;
    }

    /**
     * @return the numOfStudentsAbove75
     */
    public Integer getNumOfStudentsAbove75() {
        return numOfStudentsAbove75;
    }

    /**
     * @param numOfStudentsAbove75 the numOfStudentsAbove75 to set
     */
    public void setNumOfStudentsAbove75(Integer numOfStudentsAbove75) {
        this.numOfStudentsAbove75 = numOfStudentsAbove75;
    }

    /**
     * @return the zScore
     */
    public Float getzScore() {
        return zScore;
    }

    /**
     * @param zScore the zScore to set
     */
    public void setzScore(Float zScore) {
        this.zScore = zScore;
    }

    /**
     * @return the numOfStudentsAboveZScore
     */
    public Integer getNumOfStudentsAboveZScore() {
        return numOfStudentsAboveZScore;
    }

    /**
     * @param numOfStudentsAboveZScore the numOfStudentsAboveZScore to set
     */
    public void setNumOfStudentsAboveZScore(Integer numOfStudentsAboveZScore) {
        this.numOfStudentsAboveZScore = numOfStudentsAboveZScore;
    }
}
