package com.vedantu.dashboard.response;

import com.vedantu.User.User;
import com.vedantu.dashboard.pojo.SupermentorResponse.StudentSupermentorPojo;
import com.vedantu.dashboard.pojo.SupermentorResponse.TeacherSupermentorPojo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SuperMentorSearchResponse {

    private StudentSupermentorPojo studentUserData;
    private TeacherSupermentorPojo acadMentorUserData;

    public StudentSupermentorPojo getStudentUserData() {
        return studentUserData;
    }

    public void setStudentUserData(StudentSupermentorPojo studentUserData) {
        this.studentUserData = studentUserData;
    }

    public TeacherSupermentorPojo getAcadMentorUserData() {
        return acadMentorUserData;
    }

    public void setAcadMentorUserData(TeacherSupermentorPojo acadMentorUserData) {
        this.acadMentorUserData = acadMentorUserData;
    }

    @Override
    public String toString() {
        return "SuperMentorSearchResponse{" +
                "studentUserData=" + studentUserData +
                ", acadMentorUserData=" + acadMentorUserData +
                '}';
    }
}
