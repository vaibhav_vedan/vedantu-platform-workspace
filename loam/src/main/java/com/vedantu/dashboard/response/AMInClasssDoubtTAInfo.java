package com.vedantu.dashboard.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMInClasssDoubtTAInfo {

    private String name;
    private String profilePic;
    private String userId;
}
