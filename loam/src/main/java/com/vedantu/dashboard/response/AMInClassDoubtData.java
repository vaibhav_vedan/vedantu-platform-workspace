package com.vedantu.dashboard.response;

import com.vedantu.dashboard.pojo.helper.AMInClassConversation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor@AllArgsConstructor
public class AMInClassDoubtData {

    private String _id;
    private String sessionId;
    private String message;
    private String status;
    private List<AMInClassConversation> conversation;
    private String lastUpdated;
    private AMInClasssDoubtTAInfo taInfo;

    private Long boardId;
    private String sessionTitle;
    private String subjectName;
    private String teacherName;
}
