package com.vedantu.dashboard.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AMInClassDoubtStatRes {

    private String errorCode;
    private String errorMessage;
    private List<AMInClassDoubtStatResult> result;
}
