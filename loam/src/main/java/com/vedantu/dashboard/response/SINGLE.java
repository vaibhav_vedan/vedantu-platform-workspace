package com.vedantu.dashboard.response;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class SINGLE {
	private List<com.vedantu.dashboard.response.KEY> KEY;

	private SINGLE() {
	}

	@XmlElement
	public List<com.vedantu.dashboard.response.KEY> getKEY() {
		return KEY;
	}

	public void setKEY(List<com.vedantu.dashboard.response.KEY> kEY) {
		KEY = kEY;
	}

}
