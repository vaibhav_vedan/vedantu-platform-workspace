package com.vedantu.dashboard.response;

import lombok.Data;

@Data
public class RedisKeyValuePair {
    private String key;
    private String value;
}
