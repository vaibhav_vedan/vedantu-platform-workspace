package com.vedantu.dashboard.response;

import com.vedantu.dashboard.entities.BaseTopicTree;

import java.util.List;

public class GetBaseTreeNodesByParentRes {
    public List<BaseTopicTree> getList() {
        return list;
    }

    public void setList(List<BaseTopicTree> list) {
        this.list = list;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    private List<BaseTopicTree> list;
    private Integer count;
}
