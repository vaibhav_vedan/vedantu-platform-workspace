package com.vedantu.dashboard.response;

import com.vedantu.dashboard.pojo.AMTestStats;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMTestWiseStatsRes {
    private Map<String, AMTestStats> individualStats;
    private Map<String, AMTestStats> averageStats;
    private Map<String, Object> insights;
    private String currentNodeName;
    private String currentNodeId;
}
