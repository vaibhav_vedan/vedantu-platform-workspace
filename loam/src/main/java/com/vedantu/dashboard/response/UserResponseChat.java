package com.vedantu.dashboard.response;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseChat extends AbstractMongoStringIdEntityBean {

    private String firstName;

    private String lastName;
}
