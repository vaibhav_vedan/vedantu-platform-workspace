package com.vedantu.dashboard.response;

import java.util.ArrayList;

public class FiltersRes {
	String subject;
	ArrayList<String> topics;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public ArrayList<String> getTopics() {
		return topics;
	}
	public void setTopics(ArrayList<String> topics) {
		this.topics = topics;
	}
	
}
