
package com.vedantu.dashboard.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.enums.SubRole;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dashboard.managers.LOAMSchedulingManager;
import com.vedantu.dashboard.managers.LOAMSubscriptionManager;
import com.vedantu.dashboard.pojo.AMQuestionAttempt;
import com.vedantu.dashboard.managers.LOAMManager;
import com.vedantu.dashboard.pojo.DoubtPojo;
import com.vedantu.dashboard.pojo.PostClassDoubtChatMessagePojo;
import com.vedantu.dashboard.pojo.UserByIdReq;
import com.vedantu.dashboard.request.*;
import com.vedantu.dashboard.response.*;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.pojo.Node;

import com.vedantu.dashboard.entities.LOUsageStats;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/loamc")
public class LOAMController
{
    @Autowired
    private HttpSessionUtils sessionUtils;

	@Autowired
	private LOAMManager lOAMManager;

    @Autowired
    private LOAMSchedulingManager lOAMSchedulingManager;

    @Autowired
    private LOAMSubscriptionManager loamSubscriptionManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LOAMController.class);

    /*
    @Purpose : Getting QuestionAttempt Stats for Test by Node/Difficulty for individual and Average.
    */
    @RequestMapping(value = "/loam_getQuestionAttemptStats", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AMQuestionAttemptsStatsRes loam_getQuestionAttemptStats(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getQuestionAttemptStats");
     	return lOAMManager.loam_getQuestionAttemptStats(req.getStudentId(), req.getCurrentNodeName(), req.getCurrentNodeId(), req.getFromTime(), req.getThruTime());
    }

    /*
    @Purpose : Getting QuestionAttempt Stats for Test by Node/Difficulty for individual and Average.
    */
    @RequestMapping(value = "/loam_getViewAttemptedTests", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ViewAttemptedTestRes> loam_getViewAttemptedTests(@RequestBody ViewAttemptedTestReq req) throws VException
    {
        checkAuthorization();
            return lOAMManager.loam_getViewAttemptedTests(req.getStudentId(), req.getCurrentNodeName(), req.getFromTime(), req.getThruTime(), req.getStart(), req.getSize(), req.getTestType());
    }

    /*
    @Purpose : Getting QuestionAttempt Stats for Subjective Test by Node/Difficulty for individual and Average.
    */
    @RequestMapping(value = "/loam_getQuestionAttemptStats_Subjective", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AMQuestionAttemptsStatsRes loam_getQuestionAttemptStats_Subjective(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getQuestionAttemptStats");
        return lOAMManager.loam_getQuestionAttemptStats_Subjective(req.getStudentId(), req.getCurrentNodeName(), req.getCurrentNodeId(), req.getFromTime(), req.getThruTime());
    }

    /*
    @Purpose : Getting QuestionAttemptDetails Stats for Test by Label for individual.
    */
    @RequestMapping(value = "/loam_getQuestionAttemptDetails/byLabel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, List<AMQuestionAttempt> > loam_getQuestionAttemptDetailsByLabel(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getQuestionAttemptDetailsByLabel");
    	return lOAMManager.loam_getQuestionAttemptDetailsByLabel(req.getStudentId(), req.getFromTime(), req.getThruTime(), req.getCurrentNodeName());
    }

    /*
    @Purpose : Getting QuestionAttemptDetails Stats for Test by Difficulty for individual.
    */
    @RequestMapping(value = "/loam_getQuestionAttemptDetails/byDifficulty", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, List<AMQuestionAttempt> > loam_getQuestionAttemptDetailsByDifficulty(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getQuestionAttemptDetailsByDifficulty");
    	return lOAMManager.loam_getQuestionAttemptDetailsByDifficulty(req.getStudentId(), req.getFromTime(), req.getThruTime(), req.getCurrentNodeName(), req.getDifficulty(), req.getCurrentNodeId());
    }

    /*
    @Purpose : Updating One Time Test Stats Node Stats.
    */
    @RequestMapping(value = "/loam_oneTimeTestUpdateTestNodeStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Integer> loam_oneTimeTestUpdateTestNodeStats(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();
        return lOAMManager.loam_updateAMTestNodeStats(req.getFromTime(),req.getThruTime());
    }

    /*
    @Purpose : Updating One Time Test Stats Node Stats by Cron periodically.
    */
    @RequestMapping(value = "/loam_updateTestNodeStatsCron",  method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_updateTestNodeStats(@RequestHeader(value = "x-amz-sns-message-type")  String messageType,@RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_updateAMTestNodeStatsAsync();
        }
    }



    /*
    @Purpose : Clean Test Node Stats.
    */
    @RequestMapping(value = "/loam_cleanTestNodeStats", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_cleantestNodeStats() throws VException
    {
        checkAuthorization();

        lOAMManager.loam_cleanTestNodeStats();
    }

    /*
    @Purpose : Getting Test Wise Stats for Individual
    */
    @RequestMapping(value = "/loam_getTestWiseStats", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AMTestWiseStatsRes loam_getTestWiseStats(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getTestWiseStats");

        //logger.info("\n\n\n\n\n\n\n Logged user data \nSTUDENT ID : {}\nUSER_ID: {}\nUserRole: {}", req.getStudentId(), req.getCallingUserId(),req.getCallingUserRole());
        return lOAMManager.loam_getTestWiseStats(req);
    }

    /*
    @Purpose : Getting Assignment Wise Stats for Individual
    */
    @RequestMapping(value = "/loam_getAssignmentWiseStats", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AMAssignmentWiseStatsRes loam_getAssignmentWiseStats(@RequestBody AMQuestionAttemptStatsReq req) throws VException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getAssignmentWiseStats");
        return lOAMManager.loam_getAssignmentWiseStats(req);
    }

    /*
    @Purpose : Getting CurriculumInfo for Individual
    */
    @RequestMapping(value = "/loam_getCurriculumInfo/{userId}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Node> getLOAMCurriculumByBatchesAll(@PathVariable("userId") String userId) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException
    {
        //persist usage stats
        //lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "getLOAMCurriculumByBatchesAll");

        checkAuthorization();
        return loamSubscriptionManager.loam_getCurriculumByUser(userId);
    }

    /*
    @Purpose : Getting Doubt Conversation for Individual
    */
    @RequestMapping(value = "/loam_getDoubtConversationForStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PostClassDoubtChatMessagePojo> loam_getDoubtConversationForStudent(@RequestBody GetStudentDoubtConversationReq req) throws VException, BadRequestException
    {
        checkAuthorization();

        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getDoubtConversationForStudent");
        return lOAMManager.loam_getDoubtsConversationForStudent(req);
    }


    /*
    @Purpose : Getting Post-class Doubts Stat for Individual
    */
    @RequestMapping(value = "/loam_getDoubtsStatForStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConcurrentHashMap<String,Long> loam_getDoubtsStatForStudent(@RequestBody GetStudentDoubtsReq req) throws VException, BadRequestException
    {
        checkAuthorization();
        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getDoubtsStatForStudent");
        return lOAMManager.loam_getDoubtsStatForStudent(req);
    }

    /*
    @Purpose : Getting Post-Class Resolved Doubts for Individual
    */
    @RequestMapping(value = "/loam_getResolvedDoubtsForStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConcurrentHashMap<String,List<DoubtPojo>> loam_getResolvedDoubtsForStudent(@RequestBody  GetStudentDoubtsReq req) throws VException, BadRequestException
    {
        checkAuthorization();
        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getResolvedDoubtsForStudent");
        return lOAMManager.loam_getResolvedDoubtsForStudent(req);
    }

    /*
    @Purpose : Getting Post-class UnResolved Doubts for Individual
    */
    @RequestMapping(value = "/loam_getUnResolvedDoubtsForStudent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String,List<DoubtPojo>> loam_getUnResolvedDoubtsForStudent(@RequestBody  GetStudentDoubtsReq req) throws VException, BadRequestException
    {
        checkAuthorization();
        //persist usage stats
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), "loam_getUnResolvedDoubtsForStudent");
        return lOAMManager.loam_getUnResolvedgDoubtsForStudent(req);
    }

    /*
    @Purpose : Updating Dashboard Usage / Count of visitors of Dashboard
    */
    @RequestMapping(value = "/loam_updateDashboardUsage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Integer> loam_updateDashboardUsage(@RequestBody AMUsageStatusReq req) throws VException, BadRequestException
    {
        logger.info("Request : " + req);
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), LOUsageStats.Constants.VISITED);

        return lOAMManager.loam_getDashboardUsage(null,null);
    }

    /*
    @Purpose : Getting Dashboard Usage / Count of vistirs of Dashboard
    */
    @RequestMapping(value = "/loam_getDashboardUsage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Integer> loam_getDashboardUsage(@RequestBody  AMQuestionAttemptStatsReq req) throws VException, BadRequestException
    {
        logger.info("Request : " + req);
        lOAMManager.persistUsageStatusAsync(new Gson().toJson(req), LOUsageStats.Constants.VISITED);
        Long time = Instant.now().toEpochMilli();

        return lOAMManager.loam_getDashboardUsage(req.getFromTime(), req.getThruTime());
    }

    /*
    @Purpose : Getting In-class doubts stats/count
    */
    @RequestMapping(value = "/loam_getInClassDoubtStat/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AMInClassDoubtStatRes loam_getInClassDoubtStat(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime, @RequestParam(name = "boardId") Long boardId) throws VException
    {
        checkAuthorization();

        return lOAMSchedulingManager.loam_getInClassDoubtStat(studentId, fromTime, thruTime, boardId);
    }

    /*
    @Purpose : Getting In-class doubts question content for 2nd /3rd page
    */
    @RequestMapping(value = "/loam_getInClassDoubtData/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AMInClassDoubtDataRes loam_getInClassDoubtData(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime, @RequestParam(name = "boardId") Long boardId, @RequestParam(name = "start") Integer start, @RequestParam(name = "size") Integer size, @RequestParam(name = "chunk") Integer chunk, @RequestParam(name = "doubtStatus") String doubtStatus)  throws VException, UnsupportedEncodingException
    {
        checkAuthorization();

        return lOAMSchedulingManager.loam_getInClassDoubtData(studentId, fromTime, thruTime,boardId, start, size, chunk, doubtStatus);
    }

    /*
    @Purpose : Getting Insights for group of students of an Academic  Mentor, on Academic Mentor Page.
    */
    @RequestMapping(value = "/loam_getStudentsInsights", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public AMStudentInsightsRes loam_getStudentsInsights(@RequestBody AMStudentInsightsReq req) throws VException, ExecutionException, InterruptedException
    {
        checkAuthorization();
        if(req.getInsightList().contains("attendance") || req.getInsightList().contains("engagement"))
        {
            return lOAMSchedulingManager.loam_getStudentsInsights(req);
        }
        else
        {
            return lOAMManager.loam_getStudentsInsights(req);
        }
    }

    /*
    @Purpose : Getting students Attendance stats/count: Individual and ClassAverage.
    */
    @RequestMapping(value = "/loam_getInClassAttendanceStats/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<LinkedHashMap<String,Object>> loam_getInClassAttendanceStats(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime, @RequestParam(name = "boardId") Long boardId) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();

        return lOAMSchedulingManager.loam_getInClassAttendanceStats(studentId, fromTime, thruTime,boardId);
    }

    /*
    @Purpose : Getting students Engagement stats: Individual and ClassAverage.
    */
    @RequestMapping(value = "/loam_getInClassEngagement/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ConcurrentHashMap<String, Object> loam_getInClassEngagement(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime, @RequestParam(name = "boardId") Long boardId) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();

        return lOAMSchedulingManager.loam_getInClassEngagement(studentId, fromTime, thruTime, boardId);
    }


    /*
    @Purpose : Getting Student List for Acad Mentor
    1)Make the API call, such a way. so, that all the path variable, will be in the end of the string.
    2) Function naming conventions. getLoamStudents=> loam_getStudents
    3) getStudentsByAcadMentor => loam_getStudentsByAcadMentor
    */
    @RequestMapping(value = "/{acadMentorEmail}/loam_getStudents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getLoamStudents(@PathVariable("acadMentorEmail") String acadMentorEMail) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.getStudentsByAcadMentor(acadMentorEMail);
    }


    /*
    @Purpose : Getting ContentInfo Data from LMS to LOAM: Manually calling.
    */
    @RequestMapping(value = "/loam_getContentInfoDataFromLMS_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getDataFromLMS(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getContentInfoDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting ContentInfo Data from LMS to LOAM: Calling using Cron
    */

    @RequestMapping(value = "/loam_getContentInfoDataFromLMS_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getDataFromLMSCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());

            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getContentInfoDataFromLMSAsync();
        }
    }

    /*
    @Purpose : Getting CMDSTest Data from LMS to LOAM: Calling manually.
    */
    @RequestMapping(value = "/loam_getCMDSTestDataFromLMS_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getCMDSTestDataFromLMS(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getCMDSTestDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting CMDSTest Data from LMS to LOAM: Calling using cron.
    */
    @RequestMapping(value = "/loam_getCMDSTestDataFromLMS_cron",  method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getCMDSTestDataFromLMSCron(@RequestHeader(value = "x-amz-sns-message-type")  String messageType,@RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getCMDSTestDataFromLMSAsync();
        }
    }

    /*
    @Purpose : Getting CMDSQuestion Data from LMS to LOAM: Calling manually
    */
    @RequestMapping(value = "/loam_getCMDSQuestionDataFromLMS_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getCMDSQuestionDataFromLMS(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getCMDSQuestionDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting CMDSQuestion Data from LMS to LOAM: Calling using cron
    */
    @RequestMapping(value = "/loam_getCMDSQuestionDataFromLMS_cron",  method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getCMDSQuestionDataFromLMSCron(@RequestHeader(value = "x-amz-sns-message-type")  String messageType,@RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getCMDSQuestionDataFromLMSAsync();
        }
    }

    /*
    @Purpose : Getting QuestionAttempt Data from LMS to LOAM: Calling manually.
    */
    @RequestMapping(value = "/loam_getQuestionAttemptDataFromLMS_man", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getQuestionAttemptDataFromLMS(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getQuestionAttemptDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting QuestionAttempt Data from LMS to LOAM: Calling using cron.
    */
    @RequestMapping(value = "/loam_getQuestionAttemptDataFromLMS_cron",  method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getQuestionAttemptDataFromLMSCron(@RequestHeader(value = "x-amz-sns-message-type")  String messageType,@RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getQuestionAttemptDataFromLMSAsync();
        }
    }

    /*
    @Purpose : Getting Doubt Data from LMS to LOAM: Calling manually.
    */
    @RequestMapping(value = "/loam_getDoubtDataFromLMS_man", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getDoubtDataFromLMS(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getDoubtDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting Doubt Data from LMS to LOAM: Calling using cron.
    */
    @RequestMapping(value = "/loam_getDoubtDataFromLMS_cron",  method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getDoubtDataFromLMSCron(@RequestHeader(value = "x-amz-sns-message-type")  String messageType,@RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getDoubtDataFromLMSAsync();
        }
    }

    /*
    @Purpose : Getting DoubtChatMessage Data from LMS to LOAM: Calling manually.
    */
    @RequestMapping(value = "/loam_getDoubtChatMessageDataFromLMS_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getDoubtChatMessageDataFromLMS(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getDoubtChatMessageDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting DoubtChatMessage Data from LMS to LOAM: Calling using cron.
    */
    @RequestMapping(value = "/loam_getDoubtChatMessageDataFromLMS_cron",  method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getDoubtChatMessageDataFromLMSCron(@RequestHeader(value = "x-amz-sns-message-type")  String messageType,@RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getDoubtChatMessageDataFromLMSAsync();
        }
    }

    /*
    @Purpose : Getting OTFSession Data from Scheduling to LOAM: Calling manually.
    */
    @RequestMapping(value = "/loam_getOTFSessionDataFromScheduling_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getOTFSessionDataFromScheduling_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException {
        checkAuthorization();
        return lOAMSchedulingManager.loam_getOTFSessionDataFromScheduling(fromTime, thruTime);
    }

    /*
    @Purpose : Getting OTFSession Data from Scheduling to LOAM: Calling thrgh cron.
    */
    @RequestMapping(value = "/loam_getOTFSessionDataFromScheduling_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getOTFSessionDataFromScheduling_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException, UnsupportedEncodingException
    {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMSchedulingManager.loam_getOTFSessionDataFromSchedulingAsync();
        }
    }

    /*
   @Purpose : Getting GTTAttendeeDetails Data from Scheduling to LOAM: Calling manually.
   */
    @RequestMapping(value = "/loam_getGTTAttendeeDetailsDataFromScheduling_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getGTTAttendeeDetailsFromScheduling_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMSchedulingManager.loam_getGTTAttendeeDetailsFromScheduling(fromTime, thruTime);
    }

    /*
    @Purpose : Getting GTTAttendeeDetails Data from Scheduling to LOAM: Calling thrgh cron.
    */
    @RequestMapping(value = "/loam_getGTTAttendeeDetailsDataFromScheduling_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getGTTAttendeeDetailsDataFromScheduling_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException, UnsupportedEncodingException
    {
        logger.info(request);
        Gson gson = new Gson();

        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMSchedulingManager.loam_getGTTAttendeeDetailsDataFromSchedulingAsync();
        }
    }

    /*
   @Purpose : Getting OTMSessionEngagementData from Scheduling to LOAM: Calling manually.
   */
    @RequestMapping(value = "/loam_getOTMSessionEngagementDataFromScheduling_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getOTMSessionEngagementDataFromScheduling_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMSchedulingManager.loam_getOTMSessionEngagementDataFromScheduling(fromTime, thruTime);
    }

    /*
    @Purpose : Getting OTMSessionEngagementData from Scheduling to LOAM: Calling thrgh cron.
    */
    @RequestMapping(value = "/loam_getOTMSessionEngagementDataFromScheduling_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getOTMSessionEngagementDataFromScheduling_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException, UnsupportedEncodingException
    {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMSchedulingManager.loam_getOTMSessionEngagementDataFromSchedulingAsync();
        }
    }

    /*
    @Purpose : Update session attendance class average manually.
    */
    @RequestMapping(value = "/loam_updateSessionAttendanceAvg", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, Integer> loam_updateSessionAttendanceAvg(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMSchedulingManager.loam_updateSessionAttendanceAvg(fromTime, thruTime);
    }

    /*
    @Purpose : Update session attendance class average by cron periodically.
    */
    @RequestMapping(value = "/loam_updateSessionAttendanceAvgCron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_updateSessionAttendanceAvg(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMSchedulingManager.loam_updateSessionAttendanceAvgAsync();
        }

    }

    /*
    @Purpose : get all acadmentor for a given Super Mentor
    */
    @RequestMapping(value = "/loam_getAcadmentors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<User> loam_getAcadmentors() throws VException
    {
        checkAuthorizationSuperMentor();
        return lOAMManager.loam_getAcadmentors();
    }


    /*
    @Purpose : Update all Content Info data to slim version. Manually calling.
    */
    @RequestMapping(value = "/loam_updateContentInfoSlimVersion", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object loam_updateContentInfoSlimVersion(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_updateContentInfoSlimVersion(fromTime, thruTime);
    }

    /*
    @Purpose : Update all CMDS Test data to slim version. Manually calling.
    */
    @RequestMapping(value = "/loam_updateCMDSTestSlimVersion", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object loam_updateCMDSTestSlimVersion(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_updateCMDSTestSlimVersion(fromTime, thruTime);
    }

    /*
    @Purpose : Update all CMDS Question data to slim version. Manually calling.
    */
    @RequestMapping(value = "/loam_updateCMDSQuestionSlimVersion", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object loam_updateCMDSQuestionSlimVersion(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_updateCMDSQuestionSlimVersion(fromTime, thruTime);
    }

    /*
    @Purpose : Getting Batch Data from Subscription to LOAM: Manually calling.
    */
    @RequestMapping(value = "/loam_getBatchDataFromSubscription_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getBatchDataFromSubscription_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getBatchDataFromSubscription(fromTime, thruTime);
    }

    /*
    @Purpose : Getting Course Data from Subscription to LOAM: Manually calling.
    */
    @RequestMapping(value = "/loam_getCourseDataFromSubscription_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getCourseDataFromSubscription_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getCourseDataFromSubscription(fromTime, thruTime);
    }

    /*
    @Purpose : Getting Enrollment Data from Subscription to LOAM: Manually calling.
    */
    @RequestMapping(value = "/loam_getEnrollmentDataFromSubscription_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getEnrollmentDataFromSubscription_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getEnrollmentDataFromSubscription(fromTime, thruTime);
    }

    /*
    @Purpose : Getting Board Data from Platform LOAM: Manually calling.
    */
    @RequestMapping(value = "/loam_getBoardDataFromPlatform_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getBoardDataFromPlatform_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getBoardDataFromPlatform(fromTime, thruTime);
    }

    /*
    @Purpose : Getting AM Data from User LOAM: Manually calling.
    */
    @RequestMapping(value = "/loam_getAMDataFromUser_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getAMDataFromPlatform_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getAMDataFromUser(fromTime, thruTime);
    }

    @RequestMapping(value = "/loam_getUsersByIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<User> loam_getUsersByIds(@RequestBody UserByIdReq req, @RequestParam(value = "exposeContactInfo", required = false) boolean exposeContactInfo) throws VException
    {
        return lOAMManager.loam_getUsersFromUserIds(req.getUserIds(), "student");
    }

    /*
    @Purpose : Getting ContentInfo Data from LMS to LOAM: Calling using Cron
    */

    @RequestMapping(value = "/loam_getBatchDataFromSubscription_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getBatchDataFromSubscription_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());

            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getBatchDataFromSubscriptionAsync();
        }
    }

    /*
    @Purpose : Getting AM User Data from User to LOAM: Calling using Cron
    */

    @RequestMapping(value = "/loam_getAMDataFromUser_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getAMDataFromUser_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());

            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getAMDataFromUserAsync();
        }
    }

    @RequestMapping(value = "/loam_getCourseDataFromSubscription_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getCourseDataFromSubscription_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());

            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getCourseDataFromSubscriptionAsync();
        }
    }

    @RequestMapping(value = "/loam_getEnrollmentDataFromSubscription_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getEnrollmentDataFromSubscription_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());

            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getEnrollmentDataFromSubscriptionAsync();
        }
    }

    @RequestMapping(value = "/loam_getBoardDataFromPlatform_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getBoardDataFromPlatform_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws ForbiddenException
    {
        logger.info(request);

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());

            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getBoardDataFromPlatformAsync();
        }
    }

    /*
   @Purpose : Getting BaseTopicTree data from lms to LOAM: Calling manually.
   */
    @RequestMapping(value = "/loam_getBaseTopicTreeDataFromLMS_manual", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String loam_getBaseTopicTreeDataFromLMS_manual(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        checkAuthorization();
        return lOAMManager.loam_getBaseTopicTreeDataFromLMS(fromTime, thruTime);
    }

    /*
    @Purpose : Getting BaseTopicTree data from lms to LOAM: Calling thrgh cron.
    */
    @RequestMapping(value = "/loam_getBaseTopicTreeDataFromLMS_cron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void loam_getBaseTopicTreeDataFromLMS_cron(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException, UnsupportedEncodingException
    {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation"))
        {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        }
        else if (messageType.equals("Notification"))
        {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            lOAMManager.loam_getBaseTopicTreeDataFromLMSAsync();
        }
    }


    /*
   @Purpose : get all students for SuperMentor search
   */
    @RequestMapping(value = "/loam_getStudentsForSearch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SuperMentorSearchResponse> loam_getStudentsForSearch() throws VException
    {
        checkAuthorizationSuperMentor();
        return lOAMManager.loam_getStudentsForSearch();
    }
    //////////////////////////////////////////////////////////////////////////////////////
    //// Controller untility method //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * checkAuthorization
     *
     * @throws VException
     */
    private void checkAuthorization() throws VException
    {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);

        if(sessionUtils.getCurrentSessionData() != null)
        {
            Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();

            if (subRoles == null || !(subRoles.contains(SubRole.ACADMENTOR) || subRoles.contains(SubRole.SUPERMENTOR)))
            {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User other than Acedemic Mentor are forbidden to use this API");
            }
        }
        else
        {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Session is Expired. Please Login again.");
        }
    }

    /**
     * checkAuthorization
     *
     * @throws VException
     */
    private void checkAuthorizationSuperMentor() throws VException
    {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);

        if(sessionUtils.getCurrentSessionData() != null)
        {
            Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();

            if (subRoles == null || !(subRoles.contains(SubRole.SUPERMENTOR)))
            {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User other than Acedemic Mentor are forbidden to use this API");
            }
        }
        else
        {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Session is Expired. Please Login again.");
        }
    }
}
