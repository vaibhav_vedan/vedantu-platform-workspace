/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.enums;

/**
 *
 * @author parashar
 */
public enum DoubtState {
	DOUBT_POSTED, DOUBT_T1_ACCEPTED, DOUBT_T1_UNABLE_TO_SOLVE, DOUBT_T1_TIMEOUT, DOUBT_T2_POOL, DOUBT_T2_ACCEPTED, DOUBT_T1_SOLVED, DOUBT_T2_SOLVED, DOUBT_MARKED_SPAM,
    DOUBT_FEEDBACK_NO, DOUBT_T2_UNABLE_TO_SOLVE
}
