package com.vedantu.dashboard.enums;

import org.apache.commons.lang.StringUtils;

public enum CognitiveLevel {
    UNKNOWN, KNOWLEDGE, APPLICATION, ANALYSIS, HOTS , MEMORY,UNDERSTANDING;

    public static CognitiveLevel valueOfKey(String value) {
        CognitiveLevel cognitiveLevel = UNKNOWN;
        for (CognitiveLevel cl : values()) {
            if (StringUtils.equalsIgnoreCase(value, cl.name())) {
                cognitiveLevel = cl;
                break;
            }
        }
        return cognitiveLevel;
    }
}