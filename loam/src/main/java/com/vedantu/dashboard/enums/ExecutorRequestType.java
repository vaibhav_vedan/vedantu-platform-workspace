package com.vedantu.dashboard.enums;

public enum ExecutorRequestType {

    ALL_STUDENT_INSIGHT_ENGAGEMENT,
    ALL_STUDENT_INSIGHT_ATTENDANCE
}
