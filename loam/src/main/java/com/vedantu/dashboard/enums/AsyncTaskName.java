/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.enums;

import com.vedantu.async.IAsyncTaskName;

/**
 *
 * @author ajith
 */
public enum AsyncTaskName implements IAsyncTaskName {


    LOAM_UPDATE_TEST_NODE_STATS(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_RECORD_USAGE_STATS(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_CMDS_TEST_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_CMDS_QUESTION_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_QUESTION_ATTEMPT_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_DOUBT_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_DOUBT_CHAT_MESSAGE_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_CONTENT_INFO_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_UPDATE_SESSION_ATTENDANCE_AVERAGE(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_OTF_SESSION_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_GTTATTENDEE_DETAILS_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_OTMSESSION_ENGAGEMENT_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_UPDATE_CONTENT_INFO_SLIM_VERSION(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_UPDATE_CMDS_TEST_SLIM_VERSION(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_UPDATE_CMDS_QUESTION_SLIM_VERSION(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_UPDATE_DOUBT_SLIM_VERSION(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_UPDATE_DOUBT_CHAT_MESSAGE_SLIM_VERSION(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_BATCH_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_COURSE_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_ENROLLMENT_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_BOARD_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_AM_DATA(AsyncQueueName.DEFAULT_QUEUE),
    LOAM_GET_BASE_TOPIC_TREE_DATA(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_SNS(AsyncQueueName.DEFAULT_QUEUE);



    private AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
