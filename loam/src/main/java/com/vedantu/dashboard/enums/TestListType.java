package com.vedantu.dashboard.enums;

public enum TestListType {
    UPCOMING, ONGOING, NORMAL
}
