package com.vedantu.dashboard.enums;

public enum QCOption {
	/* Updated as per request GA-993
	 *
	 * QC Feedback - This will be a dropdown of the following fields.
	 * 
	 * 	    Correct Solution
	 * 	        Correct Solution
	 * 	        Credited to T1
	 * 	        Credited to T2
	 * 	    Incorrect solution
	 * 	        Incorrect Solution
	 * 	        Abusive Teacher
	 * 	        Chat Quality
	 * 	        Credited to T1
	 * 	        Credited to T2
	 * 	    Solution not applicable
	 * 	        General Enquiry
	 * 	        Solution by Email
	 * 	        Incorrect/Incomplete Question
	 * 	        Others
	 * 	        Abusive Student
	 * 	    Spam
	 * 	        Genuine Doubt
	 * 	        Genuine spam
	 * 
	 * Remarks are dropdown selections within every QC feedback.
	 */
	CORRECT, INCORRECT, NOT_APPLICABLE, SPAM
}
