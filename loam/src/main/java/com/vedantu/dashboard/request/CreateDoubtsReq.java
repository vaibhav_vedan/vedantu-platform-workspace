/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class CreateDoubtsReq extends AbstractFrontEndReq {
    
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String text;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String picUrl;
    private String picFileName;
    private Set<String> targetGrades;
    private Set<String> topics;
    
    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(StringUtils.isEmpty(text) && StringUtils.isEmpty(picUrl)){
            errors.add("Empty Doubt");
        }
        
        return errors;
    }    

    @Override
    public String toString() {
        return "CreateDoubtsReq{" + "text=" + text + ", picUrl=" + picUrl + ", picFileName=" + picFileName + ", targetGrades=" + targetGrades + ", topics=" + topics + '}';
    }


    /**
     * @return the picFileName
     */
    public String getPicFileName() {
        return picFileName;
    }

    /**
     * @param picFileName the picFileName to set
     */
    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

    /**
     * @return the targetGrades
     */
    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    /**
     * @param targetGrades the targetGrades to set
     */
    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    /**
     * @return the topics
     */
    public Set<String> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }
    
}
