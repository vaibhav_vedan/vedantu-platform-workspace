package com.vedantu.dashboard.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewAttemptedTestReq {
    private Long studentId;
    private Long fromTime;
    private Long thruTime;
    private String currentNodeName;
    private Integer start;
    private Integer size;
    private String testType;
}
