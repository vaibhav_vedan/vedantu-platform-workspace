/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.request;

import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.Set;

/**
 *
 * @author ajith
 */
public class AbstractAddCMDSEntityReq extends AbstractFrontEndReq {

    @Deprecated
    private Set<String> boardIds;//will be deprecated soon
    @Deprecated
    private Set<String> targetIds;//will be deprecated, use categories instead
    private String subject;// physics, chemistry etc
    private Set<String> topics;// newtons-laws-of-motion,thermodynamics
    public Set<String> subtopics;
    private Set<String> grades;
    private Set<String> targets;//cbse, icse,iit-jee etc
    private Set<String> tags;//free flowing tags
    private String name;
    public VedantuRecordState recordState = VedantuRecordState.ACTIVE;

    public Set<String> getBoardIds() {
        return boardIds;
    }

    public void setBoardIds(Set<String> boardIds) {
        this.boardIds = boardIds;
    }

    public Set<String> getTargetIds() {
        return targetIds;
    }

    public void setTargetIds(Set<String> targetIds) {
        this.targetIds = targetIds;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    public Set<String> getTargets() {
        return targets;
    }

    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getSubtopics() {
        return subtopics;
    }

    public void setSubtopics(Set<String> subtopics) {
        this.subtopics = subtopics;
    }

    public VedantuRecordState getRecordState() {
        return recordState;
    }

    public void setRecordState(VedantuRecordState recordState) {
        this.recordState = recordState;
    }
    
}
