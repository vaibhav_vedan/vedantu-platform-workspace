package com.vedantu.dashboard.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetStudentDoubtsReq extends AbstractFrontEndReq {
    private String studentId;
    private String currentNodeName;
    private Long fromTime;
    private Long thruTime;
    private int start;
    private int size;
}
