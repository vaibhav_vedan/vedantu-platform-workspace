package com.vedantu.dashboard.request;

import com.vedantu.dashboard.pojo.InsightSortBy;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMStudentInsightsReq extends AbstractFrontEndReq {

    private String amEmail;
    private List<String> insightList;
    private InsightSortBy sortBy;
    private List<Long> studentIdList;
}