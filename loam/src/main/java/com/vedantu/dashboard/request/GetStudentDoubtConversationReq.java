package com.vedantu.dashboard.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetStudentDoubtConversationReq extends AbstractFrontEndReq {
    private Long doubtId;
    private int start;
    private int size;
    private Long fromTime;
    private Long thruTime;
}
