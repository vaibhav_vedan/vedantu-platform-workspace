package com.vedantu.dashboard.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

@Data
public class LOAMStudentsReq extends AbstractFrontEndReq {

    private String acadMentorEmail;
    private String calledFrom;
}
