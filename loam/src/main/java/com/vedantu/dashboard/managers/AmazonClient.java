package com.vedantu.dashboard.managers;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


@Service
public class AmazonClient {

    public static final String PUBLIC_CONTENT_IDENTIFIER = "/public/";

    private static AmazonS3 s3Client;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonClient.class);

    @PostConstruct
    public void init() {
        try {
            s3Client = new AmazonS3Client();
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        } catch (Exception ex) {
            logger.info("Error occured initializing s3Client : " + ex.getMessage());
        }
    }

    public String getImageUrl(String keyName, String bucketName) {
        //key == fully qualified key name along with folder path
        java.util.Date expiration = new java.util.Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += 1000 * 60 * 60; // Add 1 hour.
        expiration.setTime(milliSeconds);
        if (keyName.contains(PUBLIC_CONTENT_IDENTIFIER)) {
            return s3Client.getUrl(bucketName, keyName).toString();
        }
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, keyName);
        return s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();
    }

}
