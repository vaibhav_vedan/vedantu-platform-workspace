package com.vedantu.dashboard.managers;

import com.vedantu.onetofew.enums.*;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OTFSessionManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFSessionManager.class);

    // Not used anywhere
    @Deprecated
    public static List<OTFSessionToolType> SUPPORTED_SESSION_TOOL_TYPES = Arrays.asList(OTFSessionToolType.GTT,
            OTFSessionToolType.GTT_PRO, OTFSessionToolType.GTT100, OTFSessionToolType.GTW,
            OTFSessionToolType.GTT_WEBINAR, OTFSessionToolType.VEDANTU_WAVE, OTFSessionToolType.VEDANTU_WAVE_BIG_WHITEBOARD, OTFSessionToolType.VEDANTU_WAVE_OTO);

}
