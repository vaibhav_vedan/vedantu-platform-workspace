package com.vedantu.dashboard.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AppContextManager;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dashboard.dao.LOAMDAO;
import com.vedantu.dashboard.entities.*;
import com.vedantu.dashboard.enums.AsyncTaskName;
import com.vedantu.dashboard.enums.ExecutorRequestType;
import com.vedantu.dashboard.pojo.*;
import com.vedantu.dashboard.request.AMStudentInsightsReq;
import com.vedantu.dashboard.request.LOAMStudentsReq;
import com.vedantu.dashboard.response.*;
import com.vedantu.dashboard.service.InsightRequestHandler;
import com.vedantu.dashboard.service.TaskExecutorService;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.*;
import org.apache.commons.collections.map.HashedMap;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
public class LOAMSchedulingManager
{
	/*
 	Reviewer comments: Put all the string at one place. Say in constants/EndPointsConstants.java.
 	In the next release.
	*/

	//Instance variable scope is fine or you need class scope?
    private final Long FROM_TIME = ConfigUtils.INSTANCE.getLongValue("FROM_TIME");

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private LOAMManager loamManager;

	@Autowired
	private LOAMDAO lOAMDAO;

	@Autowired
	private TaskExecutorService taskExecutorService;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LOAMSchedulingManager.class);
	private final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
	private final String SCHEDULING_ENDPOINT_LOAM = SCHEDULING_ENDPOINT  + "loam-ambassador-sched/";
	private final Long ONE_DAY_IN_MILLIS = 86400000L;


	public AMInClassDoubtStatRes loam_getInClassDoubtStat(Long studentId, Long fromTime, Long thruTime, Long boardId) throws VException
	{
		//chunk -1 signifies all the session
		Set<OTFSession> allSessions = loam_getSessionIdsForBoardId(studentId, fromTime, thruTime,boardId,-1);

		//Set can be used instead of List, as allSessions already is a set.
		List<String> sessionIds = allSessions.parallelStream()
				.map(session -> session.getId())
				.collect(Collectors.toList());

		AMInClassDoubtStatRes amInClassDoubtStat = new AMInClassDoubtStatRes();
		List<AMInClassDoubtStatResult> list = new ArrayList<>();
		amInClassDoubtStat.setResult(list);
		HashMap<String, Long> map = new HashMap<String, Long>();
		map.put("OPEN", 0l);
		map.put("RESOLVED", 0l);

		for(int i = 1 ; i < allSessions.size(); i++)
		{
			StringBuffer sb = new StringBuffer();
			List<String> sessionIdList = loam_getSessionIdsFromSessionList(sessionIds, i);

			if(sessionIdList.size() == 0)
			{
				break;
			}

			for (String sessionId:sessionIdList)
			{
				sb.append(sessionId + ",");
			}

			String sessionId = sb.toString();

			if (sessionId != null && sessionId.length() > 0 && sessionId.charAt(sessionId.length() - 1) == ',')
			{
				sessionId = sessionId.substring(0, sessionId.length() - 1);
			}

			String datadoubtEndPoint = ConfigUtils.INSTANCE.getStringValue("DATADOUBT_ENDPOINT1");
			String params = "userId=" + studentId + "&sessionIds=" + sessionId;
			ClientResponse resp = null;
			AMInClassDoubtStatRes response = null;

			resp = WebUtils.INSTANCE.doCall(datadoubtEndPoint + params,
						HttpMethod.GET, null, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String jsonString = resp.getEntity(String.class);
			response = new Gson().fromJson(jsonString, AMInClassDoubtStatRes.class);

			List<AMInClassDoubtStatResult> resultList = new ArrayList<>();
			if(null != response){
				resultList = response.getResult();
			}

			for(AMInClassDoubtStatResult result : resultList)
			{
				if(null != result.get_id() && result.get_id().equalsIgnoreCase("OPEN"))
				{
					map.put("OPEN", map.get("OPEN") + result.getCount());
				}
				else if(null != result.get_id() && result.get_id().equalsIgnoreCase("RESOLVED"))
				{
					map.put("RESOLVED", map.get("RESOLVED") + result.getCount());
				}
			}
		}

		List<AMInClassDoubtStatResult> responseList = new ArrayList<>();
		AMInClassDoubtStatResult amInClassDoubtStatResult1 = new AMInClassDoubtStatResult();
		amInClassDoubtStatResult1.set_id("RESOLVED");
		amInClassDoubtStatResult1.setCount(map.get("RESOLVED"));

		AMInClassDoubtStatResult amInClassDoubtStatResult2 = new AMInClassDoubtStatResult();
		amInClassDoubtStatResult2.set_id("UNRESOLVED");
		amInClassDoubtStatResult2.setCount(map.get("OPEN"));

		responseList.add(amInClassDoubtStatResult1);
		responseList.add(amInClassDoubtStatResult2);

		amInClassDoubtStat.getResult().addAll(responseList);

		return amInClassDoubtStat;
	}

	private List<String> loam_getSessionIdsFromSessionList(List<String> sessionList, int num)
	{
		List<String> sessionIdList = new ArrayList<>();

		int start = 60 * (num - 1);
		int end = start + 60;

		for(int i = start; i < end && i < sessionList.size(); i++)
		{
			sessionIdList.add(sessionList.get(i));
		}

		return sessionIdList;
	}

	public AMInClassDoubtDataRes loam_getInClassDoubtData(Long studentId, Long fromTime, Long thruTime, Long boardId, Integer start, Integer size, Integer sessionChunk, String doubtStatus) throws VException
	{
		Set<OTFSession> allSessions = loam_getSessionIdsForBoardId(studentId, fromTime, thruTime,boardId,sessionChunk);

		Map<String,OTFSession> sessionMap=allSessions.parallelStream().collect(Collectors.toMap(OTFSession::getId,session->session));
		AMInClassDoubtDataRes response = new AMInClassDoubtDataRes();
		response.setResult(new ArrayList<>());

		List<String> chunkSessionIds = loam_getSessionIdsFromSessionList(new ArrayList<>(sessionMap.keySet()), sessionChunk);

		StringBuffer sb = new StringBuffer();

		for (String sessionId: chunkSessionIds)
		{
			sb.append(sessionId + ",");
		}

		String sessionId = sb.toString();
		if (sessionId != null && sessionId.length() > 0 && sessionId.charAt(sessionId.length() - 1) == ',')
		{
			sessionId = sessionId.substring(0, sessionId.length() - 1);
		}

		String datadoubtEndPoint = ConfigUtils.INSTANCE.getStringValue("DATADOUBT_ENDPOINT2");
		String params = "userId=" + studentId + "&sessionIds=" + sessionId + "&start=" + start + "&size=" + size +"&doubtStatus=" + doubtStatus;
		ClientResponse resp = WebUtils.INSTANCE.doCall(datadoubtEndPoint + params,
				HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		response = new Gson().fromJson(jsonString, AMInClassDoubtDataRes.class);

		loam_setSessionTitleAndTeacherName(response.getResult(),sessionMap);

		setSubject(response.getResult());


		int remaining = sessionMap.size() - (sessionChunk * 60);

		response.setRemainingChunk(remaining==0?0:(int)Math.ceil((double)remaining/(double)60));

		return response;
	}

	private void setSubject(List<AMInClassDoubtData> resultList)
	{
		Set<Long> boardIds = new HashSet<>();

		for(AMInClassDoubtData amInClassDoubtData : resultList)
		{
			if(null != amInClassDoubtData.getBoardId())
			{
				boardIds.add(amInClassDoubtData.getBoardId());
			}
		}

		List<Long> boardIdList = new ArrayList<>(boardIds);
		FosUtils fosUtils = new FosUtils();
		Map<Long, Board> map = getBoardInfoMap(boardIdList);

		for(AMInClassDoubtData result : resultList)
		{
			if(map.containsKey(result.getBoardId()))
			{
				result.setSubjectName(map.get(result.getBoardId()).getName());
			}
		}
	}

	private Map<Long, Board> getBoardInfoMap(List<Long> boardIdList) {
		return lOAMDAO.getBoardInfoMap(boardIdList);
	}

	private void loam_setSessionTitleAndTeacherName(List<AMInClassDoubtData> result, Map<String,OTFSession> sessionMap)
	{
		Set<String> sessionIdSet = new HashSet<>();

		for(AMInClassDoubtData data : result)
		{
			String resultSessionId = data.getSessionId();
			OTFSession otfSession = sessionMap.get(resultSessionId);
			data.setSessionTitle(otfSession.getTitle());
			String teacherName = null != data.getTaInfo().getName() ? data.getTaInfo().getName() : "";
			data.setTeacherName(teacherName);
			data.setBoardId(otfSession.getBoardId());
		}
	}

	private String getResponseFromAPI(String url, HttpMethod method, String query) throws VException
	{
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, method, query, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		return resp.getEntity(String.class);
	}

    public AMStudentInsightsRes loam_getStudentsInsights(AMStudentInsightsReq req) throws VException, InterruptedException, ExecutionException
	{
		LOAMStudentsReq loamStudentsReq = new LOAMStudentsReq();
		loamStudentsReq.setAcadMentorEmail(req.getAmEmail());
		loamStudentsReq.setIpAddress(req.getIpAddress());
		req.setStudentIdList(loamManager.getStudentIdListFromSubscription(loamStudentsReq));

		/*String url = SCHEDULING_ENDPOINT + "loam-ambassador-sched/loam_getStudentsInsights";
		String respString = getResponseFromAPI(url, HttpMethod.POST, new Gson().toJson(req));
		return new Gson().fromJson(respString, AMStudentInsightsRes.class);*/
		AMStudentInsightsRes amStudentInsightsRes = new AMStudentInsightsRes();

		/*//gets the student who are mentored by acadMentorEMail
		String urlLOAM = LOAM_ENDPOINT  + "loamc/"+ req.getAmEmail() +"/"+ "loam_getStudents";

		String respString = getResponseFromAPI(urlLOAM, HttpMethod.GET);

		Type listType = new TypeToken<List<User>>() {}.getType();
		List<User> users = new Gson().fromJson(respString, listType);*/
		List<Long> users = req.getStudentIdList();
		List<String> insightList = req.getInsightList();
		Long currentTime = System.currentTimeMillis();

		long startTime = System.currentTimeMillis();
		if (insightList.contains("engagement"))
		{
			Map<Long, InsightResponseData> studentToInsightsEngagement = new HashMap<>();
			long threshold = ConfigUtils.INSTANCE.getLongValue("ENGAGEMENT_BATCH_THRESHOLD");
			List<Future<Map<Long, InsightResponseData>>> requestStatus = new ArrayList<>();
			List<Long> usersInBatch = new ArrayList<>();

			for (Long user : users)
			{
				usersInBatch.add(user);
				if (usersInBatch.size() >= threshold)
				{
					//gets all the session->boardId mapping from sessionAttendanceAvg
					requestStatus.add(addBatchRequestToThread(req, FROM_TIME, currentTime, usersInBatch, ExecutorRequestType.ALL_STUDENT_INSIGHT_ENGAGEMENT));
					usersInBatch = new ArrayList<>();
				}
			}

			if (usersInBatch.size() > 0)
			{
				requestStatus.add(addBatchRequestToThread(req, FROM_TIME, currentTime, usersInBatch, ExecutorRequestType.ALL_STUDENT_INSIGHT_ENGAGEMENT));
			}

			for (Future<Map<Long, InsightResponseData>> status : requestStatus)
			{
				if (null != status.get())
				{
					studentToInsightsEngagement.putAll(status.get());
				}
			}

			amStudentInsightsRes.setInsightResponseDataMap(studentToInsightsEngagement);
		}

		if(insightList.contains("attendance"))
		{
			Map<Long, InsightResponseData> studentToInsightsAttendance = new HashMap<>();
			long threshold = ConfigUtils.INSTANCE.getLongValue("ATTENDANCE_BATCH_THRESHOLD");
			List<Future<Map<Long, InsightResponseData>>> requestStatus = new ArrayList<>();
			List<Long> usersInBatch = new ArrayList<>();
			for (Long user : users) {
				usersInBatch.add(user);
				if (usersInBatch.size() >= threshold) {
					//gets all the session->boardId mapping from sessionAttendanceAvg
					requestStatus.add(addBatchRequestToThread(req, FROM_TIME, currentTime, usersInBatch, ExecutorRequestType.ALL_STUDENT_INSIGHT_ATTENDANCE));
					usersInBatch = new ArrayList<>();
				}
			}
			if (usersInBatch.size() > 0) {
				requestStatus.add(addBatchRequestToThread(req, FROM_TIME, currentTime, usersInBatch, ExecutorRequestType.ALL_STUDENT_INSIGHT_ATTENDANCE));
			}

			for (Future<Map<Long, InsightResponseData>> status : requestStatus) {
				if (null != status.get()) {
					studentToInsightsAttendance.putAll(status.get());
				}
			}
			amStudentInsightsRes.setInsightResponseDataMap(studentToInsightsAttendance);
		}
		return amStudentInsightsRes;
    }

	private Future<Map<Long, InsightResponseData>> addBatchRequestToThread(AMStudentInsightsReq req, Long FROM_TIME, Long currentTime, List<Long> usersInBatch, ExecutorRequestType allStudentInsightType) throws InterruptedException {
		InsightRequestHandler insightRequestHandler = (InsightRequestHandler) AppContextManager.getAppContext().getBean("insightRequestHandler");
		insightRequestHandler.setMessage(req, FROM_TIME, currentTime, usersInBatch, allStudentInsightType);
		return taskExecutorService.addTask(insightRequestHandler, allStudentInsightType);
	}

	public Map<Long,Map<String,Object>>  loam_getInClassAttendanceStatsInsights(List<Long> studentIds, Long fromTime, Long thruTime, Long boardId)
			throws VException
	{
		//Fetch all student attendance records from time range given
		List<GTTAttendeeDetails> rawGtt = lOAMDAO.loam_getAttendedGTTAttendeeDetailsInSessionRange(studentIds, fromTime, thruTime);

		//Collect all session ids from student attendance
		Set<String> sids = rawGtt.parallelStream().filter(gttAttendeeDetails -> gttAttendeeDetails.getSessionId() != null).map(gttAttendeeDetails -> gttAttendeeDetails.getSessionId()).collect(Collectors.toSet());

		//TODO : get all child boardIds
		List<Long> boardIds  = null;

		Set<String> fieldsToExclude = new HashSet<>();
		fieldsToExclude.add(SessionAttendanceAvg.Constants.STUDENT_INFO);
		List<SessionAttendanceAvg> sessionAttendeeAvgList= lOAMDAO.loam_getSessionAttendeeAvgBySessionIds(sids, fieldsToExclude, false);

		Map<String, SessionAttendanceAvg> sessionAttendeeAvgMap= sessionAttendeeAvgList.parallelStream().filter(sessionAttendanceAvg -> sessionAttendanceAvg.getSessionId() != null).collect(Collectors.toMap(SessionAttendanceAvg::getSessionId, sessionAttendanceAvg->sessionAttendanceAvg));

		//	Map<Long,ConcurrentHashMap<String, Double>> studentToWeekWiseSessionIndividual = new HashMap<>();
		Map<Long,Map<String,Object>> studentToInsightMap = new HashedMap();
		for(Long studentId:studentIds) {

			List<GTTAttendeeDetails> individualGttAttendeeDetails = rawGtt.parallelStream().filter(gttAttendeeDetails -> gttAttendeeDetails.getUserId().equalsIgnoreCase(String.valueOf(studentId))).collect(Collectors.toList());

			//
			CopyOnWriteArrayList<SessionAttendanceAvg> sessionAttendanceAvgStudent = new CopyOnWriteArrayList<>();
			long newFromTime = thruTime;
			long newThruTime = fromTime;
			List<String> sessionAttendeeAvgListForStudent = individualGttAttendeeDetails.stream().map(p -> p.getSessionId()).collect(Collectors.toList());
			for (String session : sessionAttendeeAvgListForStudent) {
				SessionAttendanceAvg sessionAttendanceAvg = sessionAttendeeAvgMap.get(session);
				if(sessionAttendanceAvg!=null) {
					sessionAttendanceAvgStudent.add(sessionAttendanceAvg);
					if (sessionAttendanceAvg.getStartTime() < newFromTime) {
						newFromTime = sessionAttendanceAvg.getStartTime();
					}
					if (sessionAttendanceAvg.getEndTime() > newThruTime) {
						newThruTime = sessionAttendanceAvg.getEndTime();
					}
				}
			}
			//

			ConcurrentHashMap<String, Double> weekWiseSessionIndividual = new ConcurrentHashMap<>();

			ConcurrentHashMap<String, List<String>> ss = new ConcurrentHashMap<>();
			for (GTTAttendeeDetails gttAttendeeDetails : individualGttAttendeeDetails) {

				if (!sessionAttendeeAvgMap.containsKey(gttAttendeeDetails.getSessionId())) {
					continue;
				}

				SessionAttendanceAvg sessionAttendanceAvg = sessionAttendeeAvgMap.get(gttAttendeeDetails.getSessionId());
				int weekCount = 1;
				for (long st = newFromTime, et = 0; st <= newThruTime; st = st + 604800000) {
					et = st + 604800000;
					if (sessionAttendanceAvg.getStartTime() != null && sessionAttendanceAvg.getEndTime() != null && sessionAttendanceAvg.getStartTime() >= st && sessionAttendanceAvg.getEndTime() <= et) {
						if (ArrayUtils.isNotEmpty(gttAttendeeDetails.getJoinTimes())) {
							if (weekWiseSessionIndividual.containsKey("Week " + weekCount)) {
								weekWiseSessionIndividual.put("Week " + weekCount, weekWiseSessionIndividual.get("Week " + weekCount) + 1);
								//to see log for sessions
								List<String> ses = ss.get("Week " + weekCount);
								ses.add(gttAttendeeDetails.getSessionId());
								ss.put("Week " + weekCount, ses);
							} else {
								weekWiseSessionIndividual.put("Week " + weekCount, 1.0);
								List<String> ses = new ArrayList<>();
								ses.add(gttAttendeeDetails.getSessionId());
								ss.put("Week " + weekCount, ses);
							}
						}
						break;
					}
					weekCount++;
				}
			}
			//	studentToWeekWiseSessionIndividual.put(studentId,weekWiseSessionIndividual);
			//For the average calulation
			HashMap<String, Object> weekWiseSession = new HashMap<>();
			LinkedHashMap<String, Object> weekWiseSessionInd1 = new LinkedHashMap<>();
			int weekCountAvg = 1;
			Double totalWeekIndividual = 0.0;

			for (long st = newFromTime, et = 0; st <= newThruTime; st = st + 604800000) {
				et = st + 604800000;
				double weekAverage = 0.0;
				int totalSessionInWeek = 0;

				List<SessionAttendanceAvg> weeklySessionAttendance = sessionAttendanceAvgStudent.parallelStream()
						.filter(loam_filterByDateRangePredicate(st, et)).collect(Collectors.toList());

				for (SessionAttendanceAvg sessionAttendanceAvg : weeklySessionAttendance) {

					weekAverage += sessionAttendanceAvg.getAverage();
					totalSessionInWeek += 1;
				}

				double weekInd = 0.0;

				if (totalSessionInWeek == 0) {
					weekAverage = 0;
				} else {
					weekAverage = (weekAverage / totalSessionInWeek) * 100;
					weekInd = weekWiseSessionIndividual.get("Week " + weekCountAvg) != null ? (weekWiseSessionIndividual.get("Week " + weekCountAvg) / totalSessionInWeek) * 100 : 0;
				}

				weekWiseSession.put("Week " + weekCountAvg, weekAverage);

				weekWiseSessionInd1.put("Week " + weekCountAvg, weekInd);
				//counting total attendance individual
				totalWeekIndividual = totalWeekIndividual + weekInd;
				weekCountAvg += 1;
			}

			Double totalWeekIndividualAverage = totalWeekIndividual / (weekCountAvg-1);
			Map<String,Object> averageMap = new HashMap<>();
			averageMap.put("insight_mark", totalWeekIndividualAverage);
			averageMap.put("insight_grade", loam_calculateInsightGrade(totalWeekIndividualAverage));
			studentToInsightMap.put(studentId,averageMap);

		}

		return studentToInsightMap;
	}

	public Map<Long,Map<String, Object>> loam_getInClassEngagementInsights(List<Long> studentIds, Long fromTime, Long thruTime, Long boardId)
			throws VException {
		CopyOnWriteArrayList<OTFSession> sessionList;
		List<Long> boardIds = null;
		ArrayList<GTTAttendeeDetails> details = lOAMDAO.loam_getAllStudentGTTAttendeeDetailsInRangeForSessions(studentIds, fromTime, thruTime);

		Set<String> sessionsAttendedByStudent = details.parallelStream().filter(p -> p.getSessionId() != null).map(p -> p.getSessionId()).collect(Collectors.toSet());

		//splitting db call in batch to reduce heap load
		List<SessionAttendanceAvg> sessionAttendeeAvgList = new ArrayList<>();
		long startTime = System.currentTimeMillis();
		sessionAttendeeAvgList = loam_getIndividualSessionAttendeeDetails(sessionsAttendedByStudent, studentIds);
		return loam_getInsightForStudents(studentIds, sessionAttendeeAvgList);
	}

	/**
	 * Calculating total engagement average for students
	 *
	 * @param
	 * @param studentIds
	 * @param sessionAttendeeAvgList
	 * @return
	 */
	private Map<Long,Map<String, Object>> loam_getInsightForStudents(List<Long> studentIds, List<SessionAttendanceAvg> sessionAttendeeAvgList)
	{
		Map<Long,Map<String, Object>> studentToInsightsMap = new HashMap<>();
		//get correct and total values for quiz and hotspot
		for(Long student:studentIds) {
			Map<String, Object> insightMap = new HashMap<>();
			Double totalPercentile = 0.0;
			int count = 0;
			Double percent = 0.0;
			for (SessionAttendanceAvg sessionAttendanceAvg : sessionAttendeeAvgList) {
				if (sessionAttendanceAvg != null && sessionAttendanceAvg.getStudentInfo() != null && sessionAttendanceAvg.getStudentInfo().getStudentEngagementMetadataMap() != null) {
					//get the student->Enaggement map.
					Map<String, StudentEngagementMetadata> studentToEngagementMap = sessionAttendanceAvg.getStudentInfo().getStudentEngagementMetadataMap();
					if (studentToEngagementMap.containsKey(String.valueOf(student))) {
						totalPercentile = totalPercentile + studentToEngagementMap.get(String.valueOf(student)).getStudentPercentile();
						count++;
					}
				}
			}
			//calculating percentage average
			if (totalPercentile > 0) {
				percent = totalPercentile / count;
			}
			insightMap.put("insight_mark", percent);
			insightMap.put("insight_grade", loam_calculateInsightGrade(percent));
			studentToInsightsMap.put(student,insightMap);
		}
		return studentToInsightsMap;
	}

	private List<SessionAttendanceAvg> loam_getIndividualSessionAttendeeDetails(Set<String> sessionsInBatch, List<Long> studentIds) {
		Set<String> fieldsToInclude = new HashSet<>();
		for (Long student : studentIds) {
			fieldsToInclude.add(SessionAttendanceAvg.Constants.STUDENT_INFO_METADATA + "." + student);
		}
		fieldsToInclude.add(SessionAttendanceAvg.Constants.STUDENT_INFO);
		fieldsToInclude.add(SessionAttendanceAvg.Constants.SESSION_ID);
		return lOAMDAO.loam_getSessionAttendeeAvgBySessionIds(sessionsInBatch,fieldsToInclude, true);
	}

	public List<LinkedHashMap<String, Object>> loam_getInClassAttendanceStats(Long studentId, Long fromTime, Long thruTime, Long boardId) throws VException {
		/*String url = SCHEDULING_ENDPOINT + "loam-ambassador-sched/loam_getInClassAttendanceStats/"+studentId+"?fromTime="+fromTime+"&thruTime="+thruTime+"&boardId="+boardId;
		String respString = getResponseFromAPI(url, HttpMethod.GET, null);
		Type listType = new TypeToken<List<LinkedHashMap<String,Object>>>() {}.getType();
		return new Gson().fromJson(respString, listType);*/
		List<LinkedHashMap<String,Object>> res=new ArrayList<>();

		//Fetch all student attendance records from time range given
		List<GTTAttendeeDetails> rawGtt = lOAMDAO.loam_getAttendedGTTAttendeeDetailsInSessionRange(studentId, fromTime, thruTime);


		//Collect all session ids from student attendance
		List<String> sids = rawGtt.parallelStream().filter(gttAttendeeDetails -> gttAttendeeDetails.getSessionId() != null).map(gttAttendeeDetails -> gttAttendeeDetails.getSessionId()).collect(Collectors.toList());

		//TODO : get all child boardIds

		List<Long> boardIds  = null;
		if(boardId != -1) {
			boardIds  = new ArrayList<>();
			boardIds.add(boardId);
		}


		Set<String> fieldsToExclude = new HashSet<>();
		fieldsToExclude.add(SessionAttendanceAvg.Constants.STUDENT_INFO);
		List<SessionAttendanceAvg> sessionAttendeeAvgList= lOAMDAO.loam_getSessionAttendeeAvgBySessionIdsAndBoardIds(sids, boardIds, fieldsToExclude, false);

		Map<String, SessionAttendanceAvg> sessionAttendeeAvgMap= sessionAttendeeAvgList.parallelStream().filter(sessionAttendanceAvg -> sessionAttendanceAvg.getSessionId() != null).collect(Collectors.toMap(SessionAttendanceAvg::getSessionId, sessionAttendanceAvg->sessionAttendanceAvg));

		//For getting total number of students suppose to attend the session and calculate individual attendance

		if(boardId != -1){
			List<OTFSession> sessionList = lOAMDAO.loam_getByBoardIdInSessions(boardId, sids);
			sids.clear();
			for(OTFSession session : sessionList){
				sids.add(session.getId());
			}
		}

		//Collect all session ids from student attendance for particular board id.
//		List<GTTAttendeeDetails> totalNumberOfStudentsGttAttendeeDetails = lOAMAmbassadorDAO.loam_getGTTAttendeeDetailsForSessions(sids);

		long newFromTime=thruTime;
		long newThruTime=fromTime;

		for(SessionAttendanceAvg sessionAttendanceAvg:sessionAttendeeAvgList)
		{
			if(sessionAttendanceAvg.getStartTime()<newFromTime){
				newFromTime=sessionAttendanceAvg.getStartTime();
			}
			if(sessionAttendanceAvg.getEndTime()>newThruTime){
				newThruTime=sessionAttendanceAvg.getEndTime();
			}
		}
		List<GTTAttendeeDetails> individualGttAttendeeDetails = rawGtt.parallelStream().filter(gttAttendeeDetails -> gttAttendeeDetails.getUserId().equalsIgnoreCase(studentId.toString())).collect(Collectors.toList());

		HashMap<String, Double> weekWiseSessionIndividual = new LinkedHashMap<>();

		ConcurrentHashMap<String,List<String>> ss=new ConcurrentHashMap<>();
		for(GTTAttendeeDetails gttAttendeeDetails:individualGttAttendeeDetails){

			if(!sessionAttendeeAvgMap.containsKey(gttAttendeeDetails.getSessionId())){
				continue;
			}

			SessionAttendanceAvg sessionAttendanceAvg = sessionAttendeeAvgMap.get(gttAttendeeDetails.getSessionId());
			int weekCount=1;
			for(long st=newFromTime,et=0;st<=newThruTime;st=st+604800000){
				et=st+604800000;
				if(sessionAttendanceAvg.getStartTime()!=null && sessionAttendanceAvg.getEndTime() !=null && sessionAttendanceAvg.getStartTime()>=st && sessionAttendanceAvg.getEndTime()<=et) {
					if(ArrayUtils.isNotEmpty(gttAttendeeDetails.getJoinTimes())){
						if(weekWiseSessionIndividual.containsKey("Week "+weekCount)){
							weekWiseSessionIndividual.put("Week "+weekCount, weekWiseSessionIndividual.get("Week "+weekCount)+1);
							//to see log for sessions
							List<String> ses=ss.get("Week "+weekCount);
							ses.add(gttAttendeeDetails.getSessionId());
							ss.put("Week "+weekCount,ses);
						}else{
							weekWiseSessionIndividual.put("Week "+weekCount,1.0);
							List<String> ses=new ArrayList<>();
							ses.add(gttAttendeeDetails.getSessionId());
							ss.put("Week "+weekCount,ses);
						}
					}
					break;
				}
				weekCount++;
			}
		}


		LinkedHashMap<String, Object> weekWiseSessionInd1 = new LinkedHashMap<>();

		//For the average calulation
		HashMap<String, Object> weekWiseSession = new HashMap<>();
		int weekCountAvg=1;
		Double totalWeekIndividual = 0.0;

		for(long st=newFromTime,et=0;st<=newThruTime;st=st+604800000){
			et=st+604800000;
			double weekAverage=0.0;
			int totalSessionInWeek=0;

			List<SessionAttendanceAvg> weeklySessionAttendance = sessionAttendeeAvgList.parallelStream()
					.filter(loam_filterByDateRangePredicate(st,et)).collect(Collectors.toList());

			for (SessionAttendanceAvg sessionAttendanceAvg: weeklySessionAttendance) {

				weekAverage += sessionAttendanceAvg.getAverage();
				totalSessionInWeek += 1;
			}

			double weekInd=0.0F;

			if(totalSessionInWeek==0){
				weekAverage=0;
			}else{
				weekAverage= (weekAverage/totalSessionInWeek)*100;
				weekInd = weekWiseSessionIndividual.get("Week "+weekCountAvg) != null? (weekWiseSessionIndividual.get("Week "+weekCountAvg)/totalSessionInWeek)*100:0;
			}

			weekWiseSession.put("Week "+weekCountAvg,weekAverage);


			weekWiseSessionInd1.put("Week "+weekCountAvg,weekInd);
			//counting total attendance individual
			totalWeekIndividual = totalWeekIndividual + weekInd;
			weekCountAvg += 1;
		}

		//counting total week count average
		Double totalWeekIndividualAverage = totalWeekIndividual / (weekCountAvg-1);
		LinkedHashMap<String,Object> averageMap = new LinkedHashMap<>();
		averageMap.put("insight_mark", totalWeekIndividualAverage);
		averageMap.put("insight_average", loam_calculateInsightGrade(totalWeekIndividualAverage));


		// Print previous map
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Individual 1 : " + weekWiseSessionInd1);
		// Reverse Linked HashMap
		LinkedHashMap<String, Object> weekWiseSessionInd2 = new LinkedHashMap<>();

		List<String> alKeys = new ArrayList<String>(weekWiseSessionInd1.keySet());
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Individual 1 Keys: " + alKeys);

		Collections.reverse(alKeys);
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Individual 1 Keys Reversed: " + alKeys);


		int i = 1;
		for(String strKey : alKeys){
			weekWiseSessionInd2.put(("Week "+ i),weekWiseSessionInd1.get(strKey));
			i++;
		}
		// Reverse Linked HashMap
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Individual 2 : " + weekWiseSessionInd2);

		// Reverse Linked HashMap
		LinkedHashMap<String, Object> weekWiseSessionAvg2 = new LinkedHashMap<>();
		List<String> alKeys1 = new ArrayList<String>(weekWiseSession.keySet());
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Individual 1 Keys: " + alKeys1);


		Collections.reverse(alKeys1);
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Individual 1 Keys Reversed: " + alKeys1);

		// Print previous map
		//logger.info("\n\n\n\n\n\n\n WeekWise Session Avg 1 : " + weekWiseSession);
		int total = i - 1;
		for(String strKey : alKeys1){
			int num = Integer.parseInt(strKey.split(" ")[1]);
			weekWiseSessionAvg2.put(("Week "+ (total - num + 1)),weekWiseSession.get(strKey));
		}
		// Reverse Linked HashMap

		//logger.info("\n\n\n\n\n\n\n WeekWise Session Avg 1 : " + weekWiseSessionAvg2);

		res.add(weekWiseSessionInd2);
		res.add(weekWiseSessionAvg2);
		res.add(averageMap);
		return res;
	}

	/**
	 * gets the grade for attendance
	 *
	 * @param totalWeekAverage
	 * @return
	 */
	public String loam_calculateInsightGrade(Double totalWeekAverage) {
		String[] firstGrade = ConfigUtils.INSTANCE.getStringValue("FIRST_GRADE").split("\\$");
		String[] secondGrade = ConfigUtils.INSTANCE.getStringValue("SECOND_GRADE").split("\\$");
		String[] thirdGrade = ConfigUtils.INSTANCE.getStringValue("THIRD_GRADE").split("\\$");
		String[] fourthGrade = ConfigUtils.INSTANCE.getStringValue("FOURTH_GRADE").split("\\$");
		if (totalWeekAverage >= Double.parseDouble(firstGrade[1])) {
			return firstGrade[0];
		} else if (totalWeekAverage >= Double.parseDouble(secondGrade[1].split("-")[0]) && totalWeekAverage <= Double.parseDouble(secondGrade[1].split("-")[1])) {
			return secondGrade[0];
		} else if (totalWeekAverage >= Double.parseDouble(thirdGrade[1].split("-")[0]) && totalWeekAverage <= Double.parseDouble(thirdGrade[1].split("-")[1])) {
			return thirdGrade[0];
		} else {
			return fourthGrade[0];
		}
	}

	public Predicate<SessionAttendanceAvg> loam_filterByDateRangePredicate(long startTime, long throughTime) {
		return sessionAttendanceAvg -> (sessionAttendanceAvg.getStartTime() == null || sessionAttendanceAvg.getEndTime() == null)|| (sessionAttendanceAvg.getStartTime() >= startTime && sessionAttendanceAvg.getEndTime() <= throughTime);
	}

	public ConcurrentHashMap<String, Object> loam_getInClassEngagement(Long studentId, Long fromTime, Long thruTime, Long boardId) throws VException {
		/*String url = SCHEDULING_ENDPOINT + "loam-ambassador-sched/loam_getInClassEngagement/"+studentId+"?fromTime="+fromTime+"&thruTime="+thruTime+"&boardId="+boardId;
		String respString = getResponseFromAPI(url, HttpMethod.GET, null);
		return new Gson().fromJson(respString, ConcurrentHashMap.class);*/
		CopyOnWriteArrayList<OTFSession> sessionList;

		List<Long> boardIds  = null;
		if (boardId == -1)
		{
			sessionList = lOAMDAO.loam_getAllSessions(fromTime, thruTime);
		}
		else
		{
			boardIds  = new ArrayList<>();
			boardIds.add(boardId);
			sessionList = lOAMDAO.loam_getByBoardId(boardId, fromTime, thruTime);
		}

		List<String> sessionIds = sessionList.parallelStream().filter(session -> session.getId() != null).map(session -> session.getId()).collect(Collectors.toList());

		CopyOnWriteArrayList<GTTAttendeeDetails> details = lOAMDAO.loam_getAllGTTAttendeeDetailsInRangeForSessions(studentId, sessionIds, fromTime, thruTime);

		List<String> sessionsAttendedByStudent = details.parallelStream().filter(p -> p.getSessionId() != null).map(p -> p.getSessionId()).collect(Collectors.toList());

		//splitting db call in batch to reduce heap load
		List<SessionAttendanceAvg> sessionAttendeeAvgList = new ArrayList<>();
		long threshold = ConfigUtils.INSTANCE.getLongValue("SESSION_THRESHOLD");
		if (sessionsAttendedByStudent.size() > threshold) {
			int count = 0;
			List<String> sessionsInBatch = new ArrayList<>();
			for (String studentSession : sessionsAttendedByStudent) {
				sessionsInBatch.add(studentSession);
				count++;
				if (count >= threshold) {
					//gets all the session->boardId mapping from sessionAttendanceAvg
					sessionAttendeeAvgList.addAll(loam_getSessionAttendeeDetails(sessionsInBatch, boardIds));
					sessionsInBatch = new ArrayList<>();
					count = 0;
				}
			}
			if (count > 0) {
				sessionAttendeeAvgList.addAll(loam_getSessionAttendeeDetails(sessionsInBatch, boardIds));
			}
		} else {
			sessionAttendeeAvgList.addAll(loam_getSessionAttendeeDetails(sessionsAttendedByStudent, boardIds));
		}

		//Arun Dhwaj: Tech-Debt: We need to take DS similar to LinkedHashMap, which support multi-threaded
		LinkedHashMap<String, AMSessionWiseDataRes> sessionIdGTTAttendeeDetailMap = new LinkedHashMap<>();

		ConcurrentHashMap<String, Object> insightSessionMap = new ConcurrentHashMap<>();

		CopyOnWriteArrayList<OTMSessionEngagementData> otmSessionEngagementDataList = lOAMDAO.loam_getOTMSessionEngagementDataList(sessionIds);

		Map<String, List<OTMSessionEngagementData>> otmSessionEngagementDataMap = new HashMap<>();

		for(OTMSessionEngagementData otmSessionEngagementData : otmSessionEngagementDataList){
			if(otmSessionEngagementDataMap.containsKey(otmSessionEngagementData.getSessionId())){
				List<OTMSessionEngagementData> otmSessionEngagementDataList1 = otmSessionEngagementDataMap.get(otmSessionEngagementData.getSessionId());
				otmSessionEngagementDataList1.add(otmSessionEngagementData);
				otmSessionEngagementDataMap.put(otmSessionEngagementData.getSessionId(), otmSessionEngagementDataList1);
			}
			else{
				List<OTMSessionEngagementData> otmSessionEngagementDataList1 = new ArrayList<>();
				otmSessionEngagementDataList1.add(otmSessionEngagementData);
				otmSessionEngagementDataMap.put(otmSessionEngagementData.getSessionId(), otmSessionEngagementDataList1);
			}
		}

		for (GTTAttendeeDetails each : details)
		{
			GTTAttendanceBasic avg = loam_calculateAvgAttemptStat(otmSessionEngagementDataMap.get(each.getSessionId()));
			GTTAttendanceBasic ind = loam_calculateAttemptStat(each);
			AMSessionWiseDataRes am = new AMSessionWiseDataRes(ind, avg, each.getCreationTime());
			sessionIdGTTAttendeeDetailMap.put(each.getSessionId(), am);
		}
		assignSessionName(sessionIdGTTAttendeeDetailMap);
		insightSessionMap.put("sessions", sessionIdGTTAttendeeDetailMap);
		insightSessionMap.put("insights", loam_calculateInsightsForEngagementPercentile(studentId,sessionAttendeeAvgList));

		return insightSessionMap;
	}

	private void assignSessionName(LinkedHashMap<String, AMSessionWiseDataRes> sessionIdGTTAttendeeDetailMap) {
		List<OTFSession> sessions = lOAMDAO.getByIds(new ArrayList<>(sessionIdGTTAttendeeDetailMap.keySet()));
		for(OTFSession session: sessions){
			sessionIdGTTAttendeeDetailMap.get(session.getId()).setSessionName(session.getTitle());
		}
	}

	private GTTAttendanceBasic loam_calculateAttemptStat(GTTAttendeeDetails gtt){
		AttemptStat quiz=new AttemptStat();
		AttemptStat hotspot=new AttemptStat();
		if(gtt.getQuizNumbers()!=null){
			quiz.setCorrect((double)gtt.getQuizNumbers().getNumberCorrect());
			quiz.setUnAttempted((double)(gtt.getQuizNumbers().getNumberAsked()-gtt.getQuizNumbers().getNumberAttempted()));
			quiz.setWrong((double)(gtt.getQuizNumbers().getNumberAttempted()-gtt.getQuizNumbers().getNumberCorrect()));
		}else {
			quiz=null;
		}
		if(gtt.getHotspotNumbers()!=null){
			hotspot.setCorrect((double)gtt.getHotspotNumbers().getNumberCorrect());
			hotspot.setUnAttempted((double)(gtt.getHotspotNumbers().getNumberAsked()-gtt.getHotspotNumbers().getNumberAttempted()));
			hotspot.setWrong((double)(gtt.getHotspotNumbers().getNumberAttempted()-gtt.getHotspotNumbers().getNumberCorrect()));
		}else{
			hotspot=null;
		}

		return  new GTTAttendanceBasic(quiz,hotspot);
	}

	private GTTAttendanceBasic loam_calculateAvgAttemptStat(List<OTMSessionEngagementData>  sessionEngagementData) {
		AttemptStat quiz = new AttemptStat();
		quiz.setWrong(0.0);
		quiz.setCorrect(0.0);
		quiz.setUnAttempted(0.0);
		AttemptStat hotspot = new AttemptStat();
		hotspot.setCorrect(0.0);
		hotspot.setUnAttempted(0.0);
		hotspot.setWrong(0.0);
		int quizStudentCount = 0;
		int hotspotStudentCount = 0;
		if(null == sessionEngagementData || sessionEngagementData.size() == 0){
			return new GTTAttendanceBasic(quiz, hotspot);
		}
		for(OTMSessionEngagementData data : sessionEngagementData){
			if(null != data.getContext() && data.getContext().toString().equalsIgnoreCase("QUIZ")){
				double correctQuiz = 0.0;
				if(null != data.getTotalCorrectResponses()){
					correctQuiz = quiz.getCorrect() + data.getTotalCorrectResponses();
					quiz.setCorrect(correctQuiz);
				}
				double wrongQuiz = 0.0;
				if(null != data.getTotalIncorrectResponses()){
					wrongQuiz = quiz.getWrong() + data.getTotalIncorrectResponses();
					quiz.setWrong(wrongQuiz);
				}
				double unAttemptedQuiz = 0.0;
				if(null != data.getTotalUnattempts()){
					unAttemptedQuiz = quiz.getUnAttempted() + data.getTotalUnattempts();
					quiz.setUnAttempted(unAttemptedQuiz);
				}
				if(null != data.getStudentCountAtStart()) {
					quizStudentCount = data.getStudentCountAtStart();
				}
			}
			else if(null != data.getContext() && data.getContext().toString().equalsIgnoreCase("HOTSPOT")){

				double correctHotspot = 0.0;
				if(null != data.getTotalCorrectResponses()) {
					correctHotspot = hotspot.getCorrect() + data.getTotalCorrectResponses();
					hotspot.setCorrect(correctHotspot);
				}
				double wrongHotspot = 0.0;
				if(null != data.getTotalIncorrectResponses()){
					wrongHotspot = hotspot.getWrong() + data.getTotalIncorrectResponses();
					hotspot.setWrong(wrongHotspot);
				}
				double unAttemptedHotspot = 0.0;
				if(null != data.getTotalUnattempts()){
					unAttemptedHotspot = hotspot.getUnAttempted() + data.getTotalUnattempts();
					hotspot.setUnAttempted(unAttemptedHotspot);
				}
				if(null != data.getStudentCountAtStart()){
					hotspotStudentCount = data.getStudentCountAtStart();
				}
			}
		}
		if(quizStudentCount > 0 ) {
			quiz.setCorrect(quiz.getCorrect()/quizStudentCount);
			quiz.setWrong(quiz.getWrong()/quizStudentCount);
			quiz.setUnAttempted(quiz.getUnAttempted()/quizStudentCount);
		}
		if(hotspotStudentCount  > 0){
			hotspot.setCorrect(hotspot.getCorrect()/hotspotStudentCount );
			hotspot.setWrong(hotspot.getWrong()/hotspotStudentCount );
			hotspot.setUnAttempted(hotspot.getUnAttempted()/hotspotStudentCount);
		}
		return new GTTAttendanceBasic(quiz, hotspot);
	}

	private Collection<? extends SessionAttendanceAvg> loam_getSessionAttendeeDetails(List<String> sessionsInBatch, List<Long> boardIds) {
		Set<String> fieldsToInclude = new HashSet<>();
		fieldsToInclude.add(SessionAttendanceAvg.Constants.STUDENT_INFO);
		fieldsToInclude.add(SessionAttendanceAvg.Constants.SESSION_ID);
		return lOAMDAO.loam_getSessionAttendeeAvgBySessionIdsAndBoardIds(sessionsInBatch, boardIds, fieldsToInclude, true);
	}

	/**
	 * Calculating total engagement average for student
	 *
	 * @param
	 * @param studentId
	 * @param sessionAttendeeAvgList
	 * @return
	 */
	private Map<String, Object> loam_calculateInsightsForEngagementPercentile(Long studentId, List<SessionAttendanceAvg> sessionAttendeeAvgList)
	{
		Double percentQuiz = 0.0;
		Double percentHotspot = 0.0;
		Map<String, Object> insightMap = new HashMap<>();
		Double totalPercentileQuiz = 0.0;
		int countQuiz = 0;
		Double totalPercentileHotspot = 0.0;
		int countHotspot = 0;
		//get correct and total values for quiz and hotspot
		for (SessionAttendanceAvg sessionAttendanceAvg : sessionAttendeeAvgList) {

			if (sessionAttendanceAvg != null && sessionAttendanceAvg.getStudentInfo() != null && sessionAttendanceAvg.getStudentInfo().getStudentEngagementMetadataMap() != null) {
				//get the student->Enaggement map.
				Map<String, StudentEngagementMetadata> studentToEngagementMap = sessionAttendanceAvg.getStudentInfo().getStudentEngagementMetadataMap();

				//sorting map for students on the basis of correct marks achieved in Quiz and hotspot.
				Map<String, StudentEngagementMetadata> sortedHotspotMap = loam_sortByValueDescendingHotspot(studentToEngagementMap);
				Map<String, StudentEngagementMetadata> sortedQuizMap = loam_sortByValueDescendingQuiz(studentToEngagementMap);

				Map<String, Double> studentToPercentileHotspotMap = new HashMap<>();
				Map<String, Double> studentToPercentileQuizMap = new HashMap<>();
				// calculating percentile for Hotspot
				Double rank = 1.0;
				int size = sortedHotspotMap.size();
				for (Map.Entry<String, StudentEngagementMetadata> entry : sortedHotspotMap.entrySet()) {
					Double percentile = (size - rank + 1) * 100 / size;
					studentToPercentileHotspotMap.put(entry.getKey(), percentile);
					rank += 1;
				}
				// calculating percentile for Quiz
				Double rankQuiz = 1.0;
				int sizeQuiz = sortedQuizMap.size();
				for (Map.Entry<String, StudentEngagementMetadata> entry : sortedQuizMap.entrySet()) {
					// calculating percentile
					Double percentile = (size - rankQuiz + 1) * 100 / sizeQuiz;
					studentToPercentileQuizMap.put(entry.getKey(), percentile);
					rankQuiz += 1;
				}
				//adding totalPercentile for both quiz and hotspot
				if (studentToPercentileQuizMap.containsKey(String.valueOf(studentId))) {
					totalPercentileQuiz = totalPercentileQuiz + studentToPercentileQuizMap.get(String.valueOf(studentId));
					countQuiz++;
				}
				if (studentToPercentileHotspotMap.containsKey(String.valueOf(studentId))) {
					totalPercentileHotspot = totalPercentileHotspot + studentToPercentileHotspotMap.get(String.valueOf(studentId));
					countHotspot++;
				}
			}
		}

		//calculating percentage
		if (totalPercentileQuiz > 0) {
			percentQuiz = totalPercentileQuiz / countQuiz;
		}
		if (totalPercentileHotspot > 0) {
			percentHotspot = totalPercentileHotspot / countHotspot;
		}

		insightMap.put("insight_quiz_mark", percentQuiz);
		insightMap.put("insight_quiz_grade", loam_calculateInsightGrade(percentQuiz));

		insightMap.put("insight_hotspot_mark", percentHotspot);
		insightMap.put("insight_hotspot_grade", loam_calculateInsightGrade(percentHotspot));
		return insightMap;
	}

	/**
	 * sorting the value for hotspot
	 * @param studentToEngagementMap
	 * @return
	 */
	public static Map<String, StudentEngagementMetadata> loam_sortByValueDescendingHotspot(final Map<String, StudentEngagementMetadata> studentToEngagementMap) {
		return studentToEngagementMap
				.entrySet()
				.stream()
				.sorted((e1, e2) -> e2.getValue().getHotspotNumbers().getNumberCorrect().compareTo(e1.getValue().getHotspotNumbers().getNumberCorrect()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}

	/**
	 * sorting the value for quiz
	 * @param studentToEngagementMap
	 * @return
	 */
	public static Map<String, StudentEngagementMetadata> loam_sortByValueDescendingQuiz(final Map<String, StudentEngagementMetadata> studentToEngagementMap) {
		return studentToEngagementMap
				.entrySet()
				.stream()
				.sorted((e1, e2) -> e2.getValue().getQuizNumbers().getNumberCorrect().compareTo(e1.getValue().getQuizNumbers().getNumberCorrect()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}

	public String loam_getOTFSessionDataFromScheduling(Long fromTime, Long thruTime) throws VException {
		String url = SCHEDULING_ENDPOINT_LOAM + "loam_getOTFSessionData";
		String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
		url = url.concat(params);
		String respString = getResponseFromAPI(url, HttpMethod.GET, null);
		Type listType = new TypeToken<ArrayList<OTFSession>>() {
		}.getType();
		List<OTFSession> otfSessions = new Gson().fromJson(respString, listType);

		// Update Doubt Chat Message.
		lOAMDAO.loam_updateOTFSession(otfSessions);
		return "OTFSession data updated";
	}

	public String loam_getGTTAttendeeDetailsFromScheduling(Long fromTime, Long thruTime) throws VException {
		Integer start = 0;
		Integer size = 1000;
		String gttUrl = SCHEDULING_ENDPOINT_LOAM + "loam_getGTTAttendeeDetails?fromTime=" + fromTime + "&thruTime=" + thruTime;
		while(true) {
			String timeParams = "&start=" + start + "&size=" + size;
			String url = gttUrl.concat(timeParams);
			logger.info("url: ,{}",url);
			String respString = getResponseFromAPI(url, HttpMethod.GET, null);
			Type listType = new TypeToken<ArrayList<GTTAttendeeDetails>>() {
			}.getType();
			List<GTTAttendeeDetails> gttAttendeeDetails = new Gson().fromJson(respString, listType);
			if(gttAttendeeDetails == null || gttAttendeeDetails.isEmpty()){
				break;
			}
			lOAMDAO.loam_updateGTTAttendeeDetails(gttAttendeeDetails);
			start = start + size;
		}
		return "GTTAttendeeDetails data updated";
	}

	public String loam_getOTMSessionEngagementDataFromScheduling(Long fromTime, Long thruTime) throws VException {
		String url = SCHEDULING_ENDPOINT_LOAM + "loam_getOTMSessionEngagementData";
		String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
		url = url.concat(params);
		String respString = getResponseFromAPI(url, HttpMethod.GET, null);
		Type listType = new TypeToken<ArrayList<OTMSessionEngagementData>>() {
		}.getType();
		List<OTMSessionEngagementData> otmSessionEngagementData = new Gson().fromJson(respString, listType);

		// Update Doubt Chat Message.
		lOAMDAO.loam_updateOTMSessionEngagementData(otmSessionEngagementData);
		return "OTMSessionEngagementData data updated";
	}

	public Map<String, Integer> loam_updateSessionAttendanceAvg(Long fromTime, Long thruTime)
			throws VException {

		//Getting all the sessions in given time range
		List<OTFSession> sessionList = lOAMDAO.loam_getAllEndedSessions(fromTime, thruTime);

		//converting the list of sessions into map of sessions
		Map<String, OTFSession> sessionDetailsMap = sessionList.parallelStream().filter(session -> session.getId() != null).collect(Collectors.toMap(OTFSession::getId, session -> session));

		//Collecting all session ids
		Set<String> sessionIds = sessionDetailsMap.keySet();

		//get all students belongs to the given session ids
		List<GTTAttendeeDetails> allStudentsAttendanceDetails = lOAMDAO.loam_getGTTAttendeeDetailsForSessions(new ArrayList(sessionIds));

		//Group GTTAttendeeDetails by session id
		Map<String, List<GTTAttendeeDetails>> studentsAttendanceGroupBySessionMap = allStudentsAttendanceDetails.parallelStream().
				filter(gttAttendeeDetails -> gttAttendeeDetails.getEnrollmentId() != null)
				.collect(Collectors.groupingBy(GTTAttendeeDetails::getSessionId));

		List<SessionAttendanceAvg> sessionAttendeeAvgList = new ArrayList<>();

		//iterate each group of GTTAttendeeDetails
		for (Map.Entry<String, List<GTTAttendeeDetails>> entry : studentsAttendanceGroupBySessionMap.entrySet()) {
			double totalAttendees = entry.getValue().size();

			String sessionId = entry.getKey();
			OTFSession session = sessionDetailsMap.get(sessionId);

			double average = session.getUniqueStudentAttendants() != null ? (session.getUniqueStudentAttendants() / totalAttendees) : 0;
			SessionAttendanceAvg sessionAttendanceAvg = new SessionAttendanceAvg();
			sessionAttendanceAvg.setSessionId(sessionId);
			sessionAttendanceAvg.setBoardId(session.getBoardId());
			sessionAttendanceAvg.setAverage(average);
			sessionAttendanceAvg.setStartTime(session.getStartTime());
			sessionAttendanceAvg.setEndTime(session.getEndTime());

			StudentInfo studentInfo = loam_generateEngagementMap(entry.getValue());
			sessionAttendanceAvg.setStudentInfo(studentInfo);

			sessionAttendeeAvgList.add(sessionAttendanceAvg);
		}

		List<String> idsToDelete = sessionAttendeeAvgList.parallelStream().filter(sessionAttendanceAvg -> sessionAttendanceAvg.getSessionId() != null).map(sessionAttendanceAvg -> sessionAttendanceAvg.getSessionId().trim()).collect(Collectors.toList());

		int deletedEntriesCount = lOAMDAO.loam_deleteSessionAttendeeAvgBySessionIds(idsToDelete);

		lOAMDAO.loam_saveSessionAttendeeAvgBySessionIdsAndBoardIds(sessionAttendeeAvgList);

		Map<String, Integer> response = new HashMap<>();
		response.put("Total rows updated", sessionAttendeeAvgList.size());

		return response;
	}

	private StudentInfo loam_generateEngagementMap(List<GTTAttendeeDetails> gttAttendeeDetailsList) {

		StudentInfo studentInfo = new StudentInfo();
		Map<String, StudentEngagementMetadata> studentEngagementMetadataMap = new HashMap<>();

		Map<String, Double> studentToPercentileMap = getPercentileForStudent(gttAttendeeDetailsList);

		for (GTTAttendeeDetails gttAttendeeDetails : gttAttendeeDetailsList) {

			if(StringUtils.isEmpty(gttAttendeeDetails.getUserId()))
				continue;

			HotspotNumbers hotspotNumbers;
			if (gttAttendeeDetails.getHotspotNumbers() == null) {
				hotspotNumbers = new HotspotNumbers(0, 0, 0);
			} else {
				hotspotNumbers = gttAttendeeDetails.getHotspotNumbers();
				if (hotspotNumbers.getNumberAsked() == null)
					hotspotNumbers.setNumberAsked(0);
				if (hotspotNumbers.getNumberAttempted() == null)
					hotspotNumbers.setNumberAttempted(0);
				if (hotspotNumbers.getNumberCorrect() == null)
					hotspotNumbers.setNumberCorrect(0);
			}

			QuizNumbers quizNumbers;
			if (gttAttendeeDetails.getQuizNumbers() == null) {
				quizNumbers = new QuizNumbers(0, 0, 0);
			} else {
				quizNumbers = gttAttendeeDetails.getQuizNumbers();
				if (quizNumbers.getNumberAsked() == null)
					quizNumbers.setNumberAsked(0);
				if (quizNumbers.getNumberAttempted() == null)
					quizNumbers.setNumberAttempted(0);
				if (quizNumbers.getNumberCorrect() == null)
					quizNumbers.setNumberCorrect(0);
			}
			StudentEngagementMetadata studentEngagementMetadata = new StudentEngagementMetadata();
			studentEngagementMetadata.setHotspotNumbers(hotspotNumbers);
			studentEngagementMetadata.setQuizNumbers(quizNumbers);
			studentEngagementMetadata.setStudentId(Long.parseLong(gttAttendeeDetails.getUserId()));
			if (studentToPercentileMap.containsKey(gttAttendeeDetails.getUserId())) {
				studentEngagementMetadata.setStudentPercentile(studentToPercentileMap.get(gttAttendeeDetails.getUserId()));
			}
			studentEngagementMetadataMap.put(gttAttendeeDetails.getUserId(), studentEngagementMetadata);
		}

		studentInfo.setStudentEngagementMetadataMap(studentEngagementMetadataMap);

		return studentInfo;
	}

	/**
	 * getPercentile For all the students present in a session
	 *
	 * @param gttAttendeeDetailsList
	 * @return
	 */
	private Map<String, Double> getPercentileForStudent(List<GTTAttendeeDetails> gttAttendeeDetailsList) {

		Map<String, Double> studentToPercentileMap = new HashMap<>();
		Map<String, Double> studentToAverageMap = new HashMap<>();
		for (GTTAttendeeDetails gttAttendeeDetails : gttAttendeeDetailsList) {
			HotspotNumbers hotspotNumbers;
			Integer hotspotCorrectNumber = 0;
			if (gttAttendeeDetails.getHotspotNumbers() != null) {
				hotspotNumbers = gttAttendeeDetails.getHotspotNumbers();
				if (hotspotNumbers.getNumberCorrect() != null) {
					hotspotCorrectNumber = hotspotNumbers.getNumberCorrect();
				}
			}
			QuizNumbers quizNumbers;
			Integer quizCorrectNumber = 0;
			if (gttAttendeeDetails.getQuizNumbers() != null) {
				quizNumbers = gttAttendeeDetails.getQuizNumbers();
				if (quizNumbers.getNumberCorrect() != null) {
					quizCorrectNumber = quizNumbers.getNumberCorrect();
				}
			}
			Double averageOfHotspotQuiz = Double.valueOf((hotspotCorrectNumber + quizCorrectNumber) / 2);
			studentToAverageMap.put(gttAttendeeDetails.getUserId(), averageOfHotspotQuiz);
		}
		//sorting map in descending order
		Map<String, Double> sortedStudentToAverageMap = studentToAverageMap
				.entrySet()
				.stream()
				.sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

		// calculating percentile..
		Double rank = 1.0;
		int size = sortedStudentToAverageMap.size();

		for (Map.Entry<String, Double> entry : sortedStudentToAverageMap.entrySet()) {
			Double percentile = (size - rank + 1) * 100 / size;
			studentToPercentileMap.put(entry.getKey(), percentile);
			rank += 1;
		}
		return studentToPercentileMap;
	}

	public void loam_updateSessionAttendanceAvgAsync() {
		Map<String, Object> payload = new HashMap<>();
		//time rage set to get past 25 hours records
		long fromTime = Instant.now().toEpochMilli() - 3600000;
		long thruTime = Instant.now().toEpochMilli();
		payload.put("fromTime", fromTime);
		payload.put("thruTime", thruTime);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_UPDATE_SESSION_ATTENDANCE_AVERAGE, payload);
		asyncTaskFactory.executeTask(params);
	}

	public void loam_getOTFSessionDataFromSchedulingAsync() {
		logger.info("Invoking  loam_getOTFSessionDataFromSchedulingAsync async");
		Map<String, Object> payload = new HashMap<>();
		long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
		long thruTime = Instant.now().toEpochMilli();
		payload.put("fromTime", fromTime);
		payload.put("thruTime", thruTime);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_OTF_SESSION_DATA, payload);
		asyncTaskFactory.loam_executeTaskWithoutDelay(params);
	}

	public void loam_getGTTAttendeeDetailsDataFromSchedulingAsync() {
		logger.info("Invoking  loam_getGTTAttendeeDetailsDataFromSchedulingAsync async");
		Map<String, Object> payload = new HashMap<>();
		long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
		long thruTime = Instant.now().toEpochMilli();
		payload.put("fromTime", fromTime);
		payload.put("thruTime", thruTime);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_GTTATTENDEE_DETAILS_DATA, payload);
		asyncTaskFactory.loam_executeTaskWithoutDelay(params);
	}

	public void loam_getOTMSessionEngagementDataFromSchedulingAsync() {
		logger.info("Invoking  loam_getOTMSessionEngagementDataFromSchedulingAsync async");
		Map<String, Object> payload = new HashMap<>();
		long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
		long thruTime = Instant.now().toEpochMilli();
		payload.put("fromTime", fromTime);
		payload.put("thruTime", thruTime);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_OTMSESSION_ENGAGEMENT_DATA, payload);
		asyncTaskFactory.loam_executeTaskWithoutDelay(params);
	}

	public Set<OTFSession> loam_getSessionIdsForBoardId(Long studentId, Long fromTime, Long thruTime, Long boardId,Integer sessionChunk) {

		Set<String> gttFilteredFields=new HashSet<>();
		gttFilteredFields.add(GTTAttendeeDetails.Constants.SESSION_ID);

		List<GTTAttendeeDetails> rawGtt = lOAMDAO.loam_getAttendedGTTAttendeeDetailsInSessionRange(studentId, fromTime, thruTime);

		Set<String> sids=new HashSet<>();

		for(GTTAttendeeDetails temp:rawGtt){
			sids.add(temp.getSessionId());
		}

		Set<String> sessionFilteredFields =new HashSet<>();
		sessionFilteredFields.add(OTFSession.Constants._ID);
		sessionFilteredFields.add(OTFSession.Constants.BOARD_ID);
		sessionFilteredFields.add(OTFSession.Constants.TITLE);
		sessionFilteredFields.add(OTFSession.Constants.START_TIME);


		List<OTFSession> sessionList;
		if(boardId==-1){
			sessionList= lOAMDAO.loam_getAllSessionsForDoubts(fromTime,thruTime,sessionFilteredFields,true);
		}else{
			sessionList = lOAMDAO.loam_getByBoardIdForDoubt(boardId,fromTime,thruTime,sessionFilteredFields,true);
		}
		Set<OTFSession> sessionSet;
		sessionSet=sessionList.parallelStream().filter(session -> sids.contains(session.getId())).collect(Collectors.toSet());
		return sessionSet;

	}
}
