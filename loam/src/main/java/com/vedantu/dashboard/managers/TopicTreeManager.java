package com.vedantu.dashboard.managers;

import com.vedantu.dashboard.dao.TopicTreeDAO;
import com.vedantu.dashboard.entities.BaseTopicTree;
import com.vedantu.dashboard.entities.CurriculumTopicTree;
import com.vedantu.dashboard.response.GetBaseTreeNodesByParentRes;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class TopicTreeManager {

    @Autowired
    public LogFactory logFactory;
    
    @Autowired
    public FosUtils fosUtils;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(TopicTreeManager.class);

    @Autowired
    TopicTreeDAO topicTreeDAO;


    public GetBaseTreeNodesByParentRes getBaseTreeNodesByParent(String parentId) {
        GetBaseTreeNodesByParentRes res = new GetBaseTreeNodesByParentRes();
        List<BaseTopicTree> baseTreeList = topicTreeDAO.getBasicTopicTreeNodesByParentId(parentId);
        res.setList(baseTreeList);
        res.setCount(baseTreeList.size());
        return res;
    }

    public List<BaseTopicTree> getBaseTreeNodesByLevel(int level) {
        return topicTreeDAO.getBaseTreeNodesByLevel(level);
    }
    
    public Set<BaseTopicTree> getBaseTreeNodesByNodeNames(Set<String> nodeNames){
        return topicTreeDAO.getBaseTreeNodesByNodeNames(nodeNames);
    }

    public List<CurriculumTopicTree> getCurriculumTreeNodesByLevel(int level) {
        return topicTreeDAO.getCurriculumTreeNodesByLevel(level);
    }

}
