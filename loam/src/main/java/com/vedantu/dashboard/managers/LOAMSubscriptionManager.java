package com.vedantu.dashboard.managers;

import com.vedantu.dashboard.dao.LOAMDAO;
import com.vedantu.dashboard.entities.*;
import com.vedantu.dashboard.pojo.subscription.BoardSubjectPair;
import com.vedantu.dashboard.pojo.subscription.UserBatchInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.security.HttpSessionUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class LOAMSubscriptionManager
{
    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LOAMDAO lOAMDAO;

    public List<Node> loam_getCurriculumByUser(String uId) throws VException
    {
        // Convert to long
        Long userId = Long.parseLong(uId);

        List<UserBatchInfo> userBatchInfoList = getUserBatchInfos(userId);


        List<String> entityIds = new ArrayList<>();

        for (UserBatchInfo userBatchInfo : userBatchInfoList)
        {
            entityIds.add(userBatchInfo.getBatchId());
        }

        List<Node> nodes = null;
        if (ArrayUtils.isNotEmpty(entityIds))
        {
            nodes = loam_getAllTreeNodesByBatch(entityIds);
        }

        if (ArrayUtils.isEmpty(nodes))
        {
            throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "curriculumId not found for User Id " + userId);
        }

        return loam_getNodesUpto(nodes, 2);
    }

    private List<UserBatchInfo> getUserBatchInfos(Long userId)
    {
        //Code Reviewer: Arun Dhwaj: In next release, worked for projection

        List<Enrollment> enrollmentList = lOAMDAO.getEnrollments(userId);

        if (ArrayUtils.isEmpty(enrollmentList)) {
            return new ArrayList<>();
        }

        Set<String> batchIds = new HashSet<>();

        for (Enrollment enrollment : enrollmentList)
        {
            batchIds.add(enrollment.getBatchId());
        }

        //Code Reviewer: Arun Dhwaj: In next release, worked for projection
        List<Batch> batchList = lOAMDAO.getBatchByIds(new ArrayList<>(batchIds));

        Set<String> courseIds = new HashSet<>();
        Set<Long> boardIds = new HashSet<>();

        Map<String, Batch> batchMap = new HashMap<>();

        for (Batch batch : batchList)
        {
            courseIds.add(batch.getCourseId());
            batchMap.put(batch.getId(), batch);

            for (BoardTeacherPair boardTeacherPair : batch.getBoardTeacherPairs())
            {
                boardIds.add(boardTeacherPair.getBoardId());
            }
        }

        //Code Reviewer: Arun Dhwaj: In next release, worked for projection
        List<Course> courseList = lOAMDAO.getCoursesBasicInfos(new ArrayList<>(courseIds));

        Map<Long, Board> boardMap = getBoardInfoMap(new ArrayList<>(boardIds));

        Map<String, Course> courseMap = new HashMap<>();

        for (Course course : courseList)
        {
            courseMap.put(course.getId(), course);
        }

        List<UserBatchInfo> userBatchInfoResList = new ArrayList<>();

        for (Map.Entry<String, Batch> entry : batchMap.entrySet())
        {
            UserBatchInfo userBatchInfoRes = new UserBatchInfo();
            userBatchInfoRes.setBatchId(entry.getKey());
            userBatchInfoRes.setCourseId(entry.getValue().getCourseId());

            if (courseMap.get( entry.getValue().getCourseId()) != null )
            {
                userBatchInfoRes.setCourseName(courseMap.get(entry.getValue().getCourseId()).getTitle());
            }
            else
            {
                continue;
            }

            if (entry.getValue().getBoardTeacherPairs() != null)
            {
                List<BoardSubjectPair> boardSubjectPairList = new ArrayList<>();

                for (BoardTeacherPair boardTeacherPair : entry.getValue().getBoardTeacherPairs())
                {
                    BoardSubjectPair boardSubjectPair = new BoardSubjectPair();
                    boardSubjectPair.setBoardId( boardTeacherPair.getBoardId() );

                    if (boardMap.get(boardTeacherPair.getBoardId()) != null)
                    {
                        boardSubjectPair.setSubject(boardMap.get(boardTeacherPair.getBoardId()).getName());
                        // continue;
                    }

                    boardSubjectPairList.add(boardSubjectPair);
                }

                userBatchInfoRes.setBoardPairs(boardSubjectPairList);
            }

            userBatchInfoResList.add(userBatchInfoRes);
        }

        return userBatchInfoResList;
    }

    private List<Node> getNodesUpto(List<Node> nodes, int uptoLevel)
    {
        List<Node> list = new ArrayList<>();

        for (Node node : nodes)
        {
            int level = 0;

            if (node != null)
            {
                node = getNodesUpto(node, level, uptoLevel);
                list.add(node);
            }
        }
        return nodes;
    }

    private List<Node> loam_getNodesUpto(List<Node> nodes, int uptoLevel)
    {
        List<Node> list = new ArrayList<>();

        for (Node node : nodes)
        {
            int level = 0;
            if (node != null)
            {
                node = getNodesUpto(node, level, uptoLevel);
                list.add(node);
            }
        }
        return nodes;
    }

    private Node getNodesUpto(Node node, int level, int uptoLevel)
    {
        while (level < uptoLevel)
        {
            level++;

            if (node != null)
            {
                for (Node child : node.getNodes())
                {
                    if (level < uptoLevel && child.getNodes() != null)
                    {
                        return getNodesUpto(child, level, uptoLevel);
                    }
                    else
                    {
                        child.setNodes(null);
                    }
                }
            }
        }

        return node;
    }


    private List<Node> loam_getAllTreeNodesByBatch(List<String> contextIds)
    {
        String[] exclude = {Curriculum.Constants.CREATED_BY, Curriculum.Constants.CREATION_TIME, Curriculum.Constants.ENTITY_STATE,
                Curriculum.Constants.LAST_UPDATED, Curriculum.Constants.LAST_UPDATED_BY, Curriculum.Constants.CONTENTS,
                Curriculum.Constants.SESSION_IDS};

        List<Curriculum> curriculum = lOAMDAO.getCurriculumByBatchIds(contextIds, null, exclude);

        Map<String, List<Curriculum>> map = curriculum.stream().collect(Collectors.groupingBy(Curriculum::getContextId));
        List<Node> nodes = new ArrayList<>();

        for (Map.Entry<String, List<Curriculum>> entry : map.entrySet())
        {
            Curriculum root = findRoot(entry.getValue());
            Node node = getAllNodes(entry.getValue(), root);
            nodes.add(node);
        }

        return nodes;
    }

    public Curriculum findRoot(List<Curriculum> curriculums)
    {
        String rootId;
        if (ArrayUtils.isNotEmpty(curriculums))
        {
            if (StringUtils.isNotEmpty(curriculums.get(0).getRootId()))
            {
                rootId = curriculums.get(0).getRootId();
            }
            else
            {
                return curriculums.get(0);
            }

            for (Curriculum curriculum : curriculums)
            {
                if (StringUtils.isNotEmpty(curriculum.getId()) && curriculum.getId().equals(rootId))
                {
                    return curriculum;
                }
            }
        }

        return null;
    }


    public Node getAllNodes(List<Curriculum> curriculums, Curriculum root)
    {
        Node node = null;

        if (ArrayUtils.isEmpty(curriculums))
        {
            return node;
        }

        Map<String, List<Node>> map = new HashMap<>();

        for (Curriculum iter : curriculums)
        {
            if (StringUtils.isNotEmpty(iter.getParentId()))
            {
                if (!map.containsKey(iter.getParentId()))
                {
                    map.put(iter.getParentId(), new ArrayList<Node>());
                }

                map.get(iter.getParentId()).add(mapper.map(iter, Node.class));
            }
        }

        node = mapper.map(root, Node.class);
        node.setNodes(structureAllNodes(map, root.getId()));

        return node;
    }

    public List<Node> structureAllNodes(Map<String, List<Node>> map, String parentId)
    {
        List<Node> nodes = map.get(parentId);

        if (ArrayUtils.isNotEmpty(nodes))
        {

            Collections.sort(nodes, new Comparator<Node>()
            {
                @Override
                public int compare(Node o1, Node o2) {
                    return o1.getChildOrder().compareTo(o2.getChildOrder());
                }
            });

            for (Node node : nodes)
            {
                node.setNodes(structureAllNodes(map, node.getId()));
            }
        }
        return nodes;
    }

    private Map<Long, Board> getBoardInfoMap(List<Long> boardIdList)
    {
        //Code Reviewer: Arun Dhwaj: In next release, worked for projection
        return lOAMDAO.getBoardInfoMap(boardIdList);
    }
}
