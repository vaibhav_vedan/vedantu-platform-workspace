package com.vedantu.dashboard.managers;

import com.vedantu.dashboard.entities.OTFSession;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.request.StudentPollDataReq;
import com.vedantu.dashboard.entities.GTTAttendeeDetails;
import com.vedantu.dashboard.dao.GTTAttendeeDetailsDAO;
import com.vedantu.dashboard.pojo.AggregatedGTTAttendeeCountForSession;
import com.vedantu.dashboard.pojo.GTTGetRegistrantDetailsRes;
import com.vedantu.dashboard.pojo.GTTRegisterAttendeeRes;
import com.vedantu.dashboard.pojo.ReplayWatchedDuration;
import com.vedantu.dashboard.request.UpdateReplayWatchedDurationReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class GTTAttendeeDetailsManager {

    private static final String EMAIL_SUFFIX_SLUG = "@user.com";

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GTTAttendeeDetailsManager.class);

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public static String extractUserId(String emailId) {
        String userId = "";
        if (!StringUtils.isEmpty(emailId) && emailId.contains(EMAIL_SUFFIX_SLUG)) {
            userId = emailId.replace(EMAIL_SUFFIX_SLUG, "");
        }

        return userId;
    }
}
