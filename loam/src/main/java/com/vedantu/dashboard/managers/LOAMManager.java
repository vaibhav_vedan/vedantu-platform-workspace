package com.vedantu.dashboard.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.dashboard.dao.CMDSTestDAO;
import com.vedantu.dashboard.entities.*;
import com.vedantu.dashboard.enums.AsyncTaskName;
import com.vedantu.dashboard.enums.Difficulty;
import com.vedantu.dashboard.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.dashboard.pojo.AMQuestionAttempt;
import com.vedantu.dashboard.pojo.AMTestStats;
import com.vedantu.dashboard.pojo.SupermentorResponse.StudentSupermentorPojo;
import com.vedantu.dashboard.pojo.SupermentorResponse.TeacherSupermentorPojo;
import com.vedantu.dashboard.pojo.TestContentInfoMetadata;
import com.vedantu.dashboard.pojo.TestMetadata;
import com.vedantu.dashboard.dao.LOAMDAO;
import com.vedantu.dashboard.entities.mongo.Doubt;
import com.vedantu.dashboard.entities.mongo.DoubtChatMessage;
import com.vedantu.dashboard.enums.DoubtState;
import com.vedantu.dashboard.pojo.DoubtPojo;
import com.vedantu.dashboard.pojo.PostClassDoubtChatMessagePojo;
import com.vedantu.dashboard.request.*;
import com.vedantu.dashboard.response.*;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.dashboard.entities.ContentInfo;
import com.vedantu.dashboard.entities.LOUsageStats;
import com.vedantu.dashboard.enums.InsightGrades;
import com.vedantu.dashboard.pojo.*;
import com.vedantu.lms.cmds.pojo.QuestionAttemptState;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/*Arun Dhwaj: 29th May, 2019: code review done as per scale point of view */
/*
 * Overall data structure adopted in this class is very poor from Concurrent Point of View.
 *
 * */

@SuppressWarnings("Duplicates")
@Service
public class LOAMManager
{

    @Autowired
    private LOAMDAO lOAMDAO;

    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @Autowired
    private TopicTreeManager topicTreeManager;

    @Autowired
    public LogFactory logFactory;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private CMDSTestDAO cmdsTestDAO;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(LOAMManager.class);

    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    private static final String USER_ENDPOINT_LOAM = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT_LOAM");

    private static final String LMS_ENDPOINT_LOAM = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT_LOAM");

    private static final String PLATFORM_ENDPOINT_LOAM = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT_LOAM");

    private final String SUBSCRIBTION_ENDPOINT_STUDENTS = ConfigUtils.INSTANCE.getStringValue("SUBSCRIBTION_ENDPOINT_STUDENTS");
    private final Long FROM_TIME = ConfigUtils.INSTANCE.getLongValue("FROM_TIME");

    private final Long ONE_DAY_IN_MILLIS = 86400000L;

    public AMAssignmentWiseStatsRes loam_getAssignmentWiseStats(AMQuestionAttemptStatsReq req)
    {
        if(req.getCurrentNodeName().trim().equalsIgnoreCase("Mathematics"))
        {
            req.setCurrentNodeName("Maths");
        }

        // linked has map for order.
        LinkedHashMap<String, AMTestStats> individualStats = new LinkedHashMap<>();
        LinkedHashMap<String, AMTestStats> averageStats = new LinkedHashMap<>();
        Map<String, Map<String,Double>> insightStats = new HashMap<>();
        Set<String> allQuestionIds= new HashSet<>();

        //fields to fetch
        Set<String> cmdTestRequiredFields = new HashSet<>();
        cmdTestRequiredFields.add(CMDSTest.Constants._ID);
        List<CMDSTest> cmdsTests= lOAMDAO.loam_getAllAssignments(req.getFromTime(),req.getThruTime(), cmdTestRequiredFields, true);

        List<String> tids= cmdsTests.parallelStream().filter(test -> test.getId() != null).map(test -> test.getId()).collect(Collectors.toList());

        long t1 = Instant.now().getEpochSecond();

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        //contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getAssignmentByStudentAndTest(req.getStudentId(),tids, req.getFromTime(),req.getThruTime(), contentInfoRequiredFields, true);

        long t2 = Instant.now().getEpochSecond();


        List<String> testIds =contentInfos.parallelStream().filter(loam_filterContentInfoTestIdNullCheck()).map(contentInfo -> ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId())
                .collect(Collectors.toList());
        /*List<String> testIds =contentInfos.parallelStream().filter(loam_filterContentInfoTestIdNullCheck()).map(contentInfo -> contentInfo.getTestId())
                .collect(Collectors.toList());*/



        if(req.getCurrentNodeName().equalsIgnoreCase("root")){

            // get child nodes at level 0.

            List<BaseTopicTree> childNodes = loam_getChildNodes("root", null);

            List<String> childNodeNames = childNodes.parallelStream().map(node -> node.getName()).collect(Collectors.toList());

            // get all test node stats for the testIds and childNodes.
            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, childNodeNames, testNodeStatsRequiredFields, true);

            // create a Hash map for testId and list of testnodestats.
            HashMap<String, List<TestNodeStats>> testIdTestNodeStatsMap = testNodeStatsList.parallelStream()
                    .filter(testNode -> testNode.getTestId() != null)
                    .collect(Collectors.groupingBy(TestNodeStats::getTestId, HashMap::new, Collectors.toList()));

            long t3 = Instant.now().getEpochSecond();

            //fields to fetch
            Set<String> contentInfoAllRequiredFields = new HashSet<>();
            contentInfoAllRequiredFields.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> allCInfo= lOAMDAO.loam_getAssignmentByTests(testIds,req.getFromTime(),req.getThruTime(), contentInfoAllRequiredFields, true);

            for (ContentInfo contentInfo : contentInfos)
            {
                String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();

                List<CMDSTestAttempt> testAttempts=((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();

                if(testAttempts!=null){
                    //testAttempts.sort((a,b)->(int)(a.getCreationTime()-b.getCreationTime()));
                    CMDSTestAttempt firstTestAttempt=testAttempts.get(0);
                    individualStats.put(testId, new AMTestStats((double)firstTestAttempt.getMarksAcheived(),(double)firstTestAttempt.getTotalMarks()));
                    averageStats.put( testId, loam_CalculateAverageTestWiseStatsFromTestNodeStats(testId, testIdTestNodeStatsMap));
                    insightStats.put(testId,loam_calculateInsightPercentile(testId,testIdTestNodeStatsMap.get(testId)));
                }
            }
        }else{

            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStats= lOAMDAO.loam_getTestNodeStats(testIds,req.getCurrentNodeName(), testNodeStatsRequiredFields, true);

            //  create a map of testId and testnodestats.
            HashMap<String, TestNodeStats> testNodeStatsHashMap = new HashMap<>();

            for(TestNodeStats testNodeStats1 : testNodeStats){
                String testId = testNodeStats1.getTestId();
                testNodeStatsHashMap.put(testId, testNodeStats1);
            }

            List<String> ntestIds= testNodeStats.parallelStream().filter(tn -> tn.getTestId() != null).map(tn -> tn.getTestId())
                    .collect(Collectors.toList());

            //fields to fetch
            Set<String> contentInfoAllRequiredFields = new HashSet<>();
            contentInfoAllRequiredFields.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> allCInfo= lOAMDAO.loam_getAssignmentByTests(ntestIds,req.getFromTime(),req.getThruTime(), contentInfoAllRequiredFields, true);

            for(ContentInfo c:allCInfo){
                String testId=((TestContentInfoMetadata) c.getMetadata()).getTestId();
                List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) c.getMetadata()).getTestAttempts();
                CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
                for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                    String questionId = qa.getQuestionId();
                    allQuestionIds.add(questionId);
                }
            }

            //fields to fetch
            Set<String> ncontentInfoRequiredFields = new HashSet<>();
            ncontentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> ncontentInfos = lOAMDAO.loam_getAssignmentByStudentAndTest(req.getStudentId(),ntestIds, req.getFromTime(),req.getThruTime(), ncontentInfoRequiredFields, true);

            List<CMDSQuestion> questions = lOAMDAO.loam_getByIds(new ArrayList(allQuestionIds), null);
            individualStats= loam_calculateTestWiseStatsBulk(questions, ncontentInfos,req.getCurrentNodeName(), false);
            for (ContentInfo contentInfo : ncontentInfos)
            {
                String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
                averageStats.put(testId,loam_CalculateAverageTestWiseStatsFromTestNodeStatsInNode(testId, testNodeStatsHashMap));
                insightStats.put(testId,loam_calculateInsightPercentile(testId,Arrays.asList(testNodeStatsHashMap.get(testId))));

            }
        }
        //sets the testName on the basis of testId
        calculateTestName(individualStats);
        calculateTestName(averageStats);
        //Map<String,Object> insightMap = loam_calculateTestInsight(individualStats);
        Map<String,Object> insightMap = loam_calculateTestInsight(req.getStudentId(),insightStats);
        return new AMAssignmentWiseStatsRes( individualStats, averageStats,insightMap, req.getCurrentNodeName(), req.getCurrentNodeId());
    }

    public AMAssignmentWiseStatsRes loam_getAssignmentWiseStatsInsights(AMQuestionAttemptStatsReq req)
    {
        if(req.getCurrentNodeName().trim().equalsIgnoreCase("Mathematics")){
            req.setCurrentNodeName("Maths");
        }
        // linked has map for order.
        LinkedHashMap<String, AMTestStats> individualStats = new LinkedHashMap<>();
        LinkedHashMap<String, AMTestStats> averageStats = new LinkedHashMap<>();
        Map<String, Map<String,Double>> insightStats = new HashMap<>();
        Set<String> allQuestionIds= new HashSet<>();

        //fields to fetch
        Set<String> cmdTestRequiredFields = new HashSet<>();
        cmdTestRequiredFields.add(CMDSTest.Constants._ID);
        List<CMDSTest> cmdsTests= lOAMDAO.loam_getAllAssignments(req.getFromTime(),req.getThruTime(), cmdTestRequiredFields, true);

        List<String> tids= cmdsTests.parallelStream().filter(test -> test.getId() != null).map(test -> test.getId()).collect(Collectors.toList());

        long t1 = Instant.now().getEpochSecond();

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getAssignmentByStudentAndTest(req.getStudentId(),tids, req.getFromTime(),req.getThruTime(), contentInfoRequiredFields, true);

        long t2 = Instant.now().getEpochSecond();

        List<String> testIds =contentInfos.parallelStream().filter(loam_filterContentInfoTestIdNullCheck()).map(contentInfo -> ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId())
                .collect(Collectors.toList());


        if(req.getCurrentNodeName().equalsIgnoreCase("root")){

            // get child nodes at level 0.

            List<BaseTopicTree> childNodes = loam_getChildNodes("root", null);

            List<String> childNodeNames = childNodes.parallelStream().map(node -> node.getName()).collect(Collectors.toList());

            // get all test node stats for the testIds and childNodes.
            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, childNodeNames, testNodeStatsRequiredFields, true);

            // create a Hash map for testId and list of testnodestats.
            HashMap<String, List<TestNodeStats>> testIdTestNodeStatsMap = testNodeStatsList.parallelStream()
                    .filter(testNode -> testNode.getTestId() != null)
                    .collect(Collectors.groupingBy(TestNodeStats::getTestId, HashMap::new, Collectors.toList()));

            long t3 = Instant.now().getEpochSecond();


            for (ContentInfo contentInfo : contentInfos)
            {
                String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();

                List<CMDSTestAttempt> testAttempts=((TestContentInfoMetadata)contentInfo.getMetadata()).getTestAttempts();

                if(testAttempts!=null){
                    insightStats.put(testId,loam_calculateInsightPercentile(testId,testIdTestNodeStatsMap.get(testId)));
                }
            }
        }else{

            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStats= lOAMDAO.loam_getTestNodeStats(testIds,req.getCurrentNodeName(), testNodeStatsRequiredFields, true);

            //  create a map of testId and testnodestats.
            HashMap<String, TestNodeStats> testNodeStatsHashMap = new HashMap<>();

            for(TestNodeStats testNodeStats1 : testNodeStats){
                String testId = testNodeStats1.getTestId();
                testNodeStatsHashMap.put(testId, testNodeStats1);
            }

            List<String> ntestIds= testNodeStats.parallelStream().filter(tn -> tn.getTestId() != null).map(tn -> tn.getTestId())
                    .collect(Collectors.toList());

            //fields to fetch
            Set<String> ncontentInfoRequiredFields = new HashSet<>();
            ncontentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> ncontentInfos = lOAMDAO.loam_getAssignmentByStudentAndTest(req.getStudentId(),ntestIds, req.getFromTime(),req.getThruTime(), ncontentInfoRequiredFields, true);

            for (ContentInfo contentInfo : ncontentInfos)
            {
                String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
                //averageStats.put(testId,loam_CalculateAverageTestWiseStatsFromTestNodeStatsInNode(testId, testNodeStatsHashMap));
                insightStats.put(testId,loam_calculateInsightPercentile(testId,Arrays.asList(testNodeStatsHashMap.get(testId))));

            }
        }
        //Map<String,Object> insightMap = loam_calculateTestInsight(individualStats);
        Map<String,Object> insightMap = loam_calculateTestInsight(req.getStudentId(),insightStats);
        return new AMAssignmentWiseStatsRes( individualStats, averageStats,insightMap, req.getCurrentNodeName(), req.getCurrentNodeId());
    }

    public Predicate<ContentInfo> loam_filterContentInfoTestIdNullCheck() {
       return contentInfo -> null != contentInfo.getMetadata() && null != ((TestContentInfoMetadata)  contentInfo.getMetadata()).getTestId();
    }

    /**
     * calculate insight for post-class assignment
     * @param individualStats
     * @return map
     */
    private Map<String, Object> loam_calculateTestInsight(Map<String, AMTestStats> individualStats) {
        double marks = 0;
        double total = 0;
        double percent = 0;
        Map<String, Object> insightMap = new HashMap<>();
        for(Map.Entry<String,AMTestStats>  entry:individualStats.entrySet()){
            marks += entry.getValue().getMarks();
            total += entry.getValue().getTotalMarks();
        }
        if(total>0){
            percent = (marks * 100)/total;
        }
        insightMap.put("insight_marks",percent);
        if(null != InsightGrades.getGrade(percent))
            insightMap.put("insight_grade",InsightGrades.getGrade(percent).getGrade());
        else
            insightMap.put("insight_grade", null);
        return insightMap;
    }


    public AMTestWiseStatsRes loam_getTestWiseStats(AMQuestionAttemptStatsReq req)
    {
        if(req.getCurrentNodeName().trim().equalsIgnoreCase("Mathematics")){
            req.setCurrentNodeName("Maths");
        }

        LinkedHashMap<String, AMTestStats> individualStats = new LinkedHashMap<>();
        LinkedHashMap<String, AMTestStats> averageStats = new LinkedHashMap<>();
        Map<String, Map<String,Double>> insightStats = new HashMap<>();
        Set<String> allQuestionIds= new HashSet<>();

        long t1 = Instant.now().toEpochMilli();

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
//        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_ZERO);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudentForTestWiseStats(req.getStudentId(), req.getFromTime(),req.getThruTime(), contentInfoRequiredFields, true);

        List<String> testIds =contentInfos.parallelStream().filter(loam_filterContentInfoTestIdNullCheck()).map(contentInfo -> ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId())
                .collect(Collectors.toList());



        if(req.getCurrentNodeName().equalsIgnoreCase("root")){

            // get child nodes at level 0.

            List<BaseTopicTree> childNodes = loam_getChildNodes("root", null);
            List<String> childNodeNames = new ArrayList<>();

            for(BaseTopicTree node : childNodes){
                childNodeNames.add(node.getName());
            }

            long t4 = Instant.now().toEpochMilli();
            logger.info("Logger info Time log ======: get root status started at :" +  t1);
            // get all test node stats for the testIds and childNodes.
            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, childNodeNames, testNodeStatsRequiredFields, true);

            long t2 = Instant.now().toEpochMilli();

            // create a Hash map for testId and list of testnodestats.
            HashMap<String, List<TestNodeStats>> testIdTestNodeStatsMap = new HashMap<>();

            for(TestNodeStats testNodeStats : testNodeStatsList){
                String testId = testNodeStats.getTestId();
                if(testIdTestNodeStatsMap.containsKey(testId)){
                    List<TestNodeStats> tempTestNodeStatsList = testIdTestNodeStatsMap.get(testId);
                    tempTestNodeStatsList.add(testNodeStats);
                    testIdTestNodeStatsMap.put(testId, tempTestNodeStatsList);
                }
                else{
                    List<TestNodeStats> tempTestNodeStatsList = new ArrayList<>();
                    tempTestNodeStatsList.add(testNodeStats);
                    testIdTestNodeStatsMap.put(testId, tempTestNodeStatsList);
                }
            }

            logger.info("Logger info Time log ======: contentinfo iteration started at :" + Instant.now() );

            for (ContentInfo contentInfo : contentInfos)
            {
                String testId=((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();

                List<CMDSTestAttempt> testAttempts=((TestContentInfoMetadata)contentInfo.getMetadata()).getTestAttempts();

                if(testAttempts!=null){
                    //testAttempts.sort((a,b)->(int)(a.getCreationTime()-b.getCreationTime()));
                    CMDSTestAttempt firstTestAttempt=testAttempts.get(0);
                    individualStats.put(testId, new AMTestStats((double)firstTestAttempt.getMarksAcheived(),(double)firstTestAttempt.getTotalMarks()));

                    averageStats.put( testId, loam_CalculateAverageTestWiseStatsFromTestNodeStats(testId, testIdTestNodeStatsMap));
                    insightStats.put(testId,loam_calculateInsightPercentile(testId,testIdTestNodeStatsMap.get(testId)));
                }

            }
            logger.info("Logger info Time log ======: contentinfo iteration completed at :" + Instant.now() );

            logger.info("Logger info Time log ======: get root status ended at :" + Instant.now() );
        }else{

            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStats= lOAMDAO.loam_getTestNodeStats(testIds,req.getCurrentNodeName(), testNodeStatsRequiredFields, true);

            long t2 = Instant.now().toEpochMilli();
            logger.info("Logger info Time log ======: loam_getTestWiseStats1 :" + (t2 - t1));

            //  create a map of testId and testnodestats.
            HashMap<String, TestNodeStats> testNodeStatsHashMap = new HashMap<>();

            for(TestNodeStats testNodeStats1 : testNodeStats){
                String testId = testNodeStats1.getTestId();
                testNodeStatsHashMap.put(testId, testNodeStats1);
            }

            List<String> ntestIds= testNodeStats.parallelStream().filter(tn -> tn.getTestId() != null).map(tn -> tn.getTestId())
                    .collect(Collectors.toList());

            //fields to fetch


            Set<String> contentInfoRequiredFieldsForAllInfos = new HashSet<>();
            contentInfoRequiredFieldsForAllInfos.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> allCInfo= lOAMDAO.loam_getTestByTestIds(ntestIds,req.getFromTime(),req.getThruTime(), contentInfoRequiredFieldsForAllInfos, true);

            long t3 = Instant.now().toEpochMilli();
            logger.info("Logger info Time log ======: loam_getTestWiseStats2:" + (t3 - t2));
            Map<String,List<ContentInfo>> cmap=new HashMap<>();
            for(ContentInfo c:allCInfo){
                String testId=((TestContentInfoMetadata) c.getMetadata()).getTestId();
                if(cmap.containsKey(testId)){
                    List<ContentInfo> li=cmap.get(testId);
                    li.add(c);
                    cmap.put(testId,li);
                }else{
                    List<ContentInfo> li=new ArrayList<>();
                    li.add(c);
                    cmap.put(testId,li);
                }

                List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) c.getMetadata()).getTestAttempts();
                CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
                for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                    String questionId = qa.getQuestionId();
                    allQuestionIds.add(questionId);
                }
            }

            List<CMDSQuestion> questions = lOAMDAO.loam_getByIds(new ArrayList(allQuestionIds), null);
            long t4 = Instant.now().toEpochMilli();
//            logger.info("Logger info Time log ======: loam_getTestWiseStats3:" + (t4 - t3));;

            //fields to fetch
            Set<String> ncontentInfoRequiredFieldsForAllInfos = new HashSet<>();
            ncontentInfoRequiredFieldsForAllInfos.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> ncontentInfos = lOAMDAO.loam_getTestByStudentAndTestIds(req.getStudentId(),ntestIds, req.getFromTime(),req.getThruTime(), ncontentInfoRequiredFieldsForAllInfos, true);

            long t5 = Instant.now().toEpochMilli();
            logger.info("Logger info Time log ======: loam_getTestWiseStats4:" + (t5 - t4));

            individualStats= loam_calculateTestWiseStatsBulk(questions, ncontentInfos,req.getCurrentNodeName(), false);
            for (ContentInfo contentInfo : ncontentInfos)
            {
                String testId=((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
                averageStats.put( testId, loam_CalculateAverageTestWiseStatsFromTestNodeStatsInNode(testId, testNodeStatsHashMap));
                insightStats.put(testId,loam_calculateInsightPercentile(testId,Arrays.asList(testNodeStatsHashMap.get(testId))));
            }
        }
        calculateTestName(individualStats);
        calculateTestName(averageStats);
        //Map<String,Object> insightMap = loam_calculateTestInsight(individualStats);
        Map<String,Object> insightMap = loam_calculateTestInsight(req.getStudentId(),insightStats);
        Instant completedAt = Instant.now();
        return new AMTestWiseStatsRes( individualStats, averageStats, insightMap, req.getCurrentNodeName(), req.getCurrentNodeId());
    }


    private void calculateTestName(LinkedHashMap<String, AMTestStats> stats) {
        List<CMDSTest> tests = cmdsTestDAO.getByIds(new ArrayList<>(stats.keySet()));
        for (CMDSTest test : tests) {
            stats.get(test.getId()).setTestName(test.getName());
          }
    }

    public AMTestWiseStatsRes loam_getTestWiseStatsInsights(AMQuestionAttemptStatsReq req)
    {

        if(req.getCurrentNodeName().trim().equalsIgnoreCase("Mathematics")){
            req.setCurrentNodeName("Maths");
        }

        LinkedHashMap<String, AMTestStats> individualStats = new LinkedHashMap<>();
        LinkedHashMap<String, AMTestStats> averageStats = new LinkedHashMap<>();
        Map<String, Map<String,Double>> insightStats = new HashMap<>();
        Set<String> allQuestionIds= new HashSet<>();

        long t1 = Instant.now().toEpochMilli();


        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudentForTestWiseStats(req.getStudentId(), req.getFromTime(),req.getThruTime(), contentInfoRequiredFields, true);


        List<String> testIds =contentInfos.parallelStream().filter(loam_filterContentInfoTestIdNullCheck()).map(contentInfo -> ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId())
                .collect(Collectors.toList());




        if(req.getCurrentNodeName().equalsIgnoreCase("root")){

            // get child nodes at level 0.

            List<BaseTopicTree> childNodes = loam_getChildNodes("root", null);
            List<String> childNodeNames = new ArrayList<>();

            for(BaseTopicTree node : childNodes){
                childNodeNames.add(node.getName());
            }

            long t4 = Instant.now().toEpochMilli();

            // get all test node stats for the testIds and childNodes.
            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, childNodeNames, testNodeStatsRequiredFields, true);

            long t2 = Instant.now().toEpochMilli();


            // create a Hash map for testId and list of testnodestats.
            HashMap<String, List<TestNodeStats>> testIdTestNodeStatsMap = new HashMap<>();

            for(TestNodeStats testNodeStats : testNodeStatsList){
                String testId = testNodeStats.getTestId();
                if(testIdTestNodeStatsMap.containsKey(testId)){
                    List<TestNodeStats> tempTestNodeStatsList = testIdTestNodeStatsMap.get(testId);
                    tempTestNodeStatsList.add(testNodeStats);
                    testIdTestNodeStatsMap.put(testId, tempTestNodeStatsList);
                }
                else{
                    List<TestNodeStats> tempTestNodeStatsList = new ArrayList<>();
                    tempTestNodeStatsList.add(testNodeStats);
                    testIdTestNodeStatsMap.put(testId, tempTestNodeStatsList);
                }
            }


            for (ContentInfo contentInfo : contentInfos)
            {
                String testId=((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();

                List<CMDSTestAttempt> testAttempts=((TestContentInfoMetadata)contentInfo.getMetadata()).getTestAttempts();

                if(testAttempts!=null){
                    insightStats.put(testId,loam_calculateInsightPercentile(testId,testIdTestNodeStatsMap.get(testId)));
                }

            }

        }else{

            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);
            List<TestNodeStats> testNodeStats= lOAMDAO.loam_getTestNodeStats(testIds,req.getCurrentNodeName(), testNodeStatsRequiredFields, true);

            long t2 = Instant.now().toEpochMilli();


            //  create a map of testId and testnodestats.
            HashMap<String, TestNodeStats> testNodeStatsHashMap = new HashMap<>();

            for(TestNodeStats testNodeStats1 : testNodeStats){
                String testId = testNodeStats1.getTestId();
                testNodeStatsHashMap.put(testId, testNodeStats1);
            }

            List<String> ntestIds= testNodeStats.parallelStream().filter(tn -> tn.getTestId() != null).map(tn -> tn.getTestId())
                    .collect(Collectors.toList());

            long t3 = Instant.now().toEpochMilli();

            Map<String,List<ContentInfo>> cmap=new HashMap<>();
//
            long t4 = Instant.now().toEpochMilli();

            //fields to fetch
            Set<String> ncontentInfoRequiredFieldsForAllInfos = new HashSet<>();
            ncontentInfoRequiredFieldsForAllInfos.add(ContentInfo.Constants.METADATA);
            List<ContentInfo> ncontentInfos = lOAMDAO.loam_getTestByStudentAndTestIds(req.getStudentId(),ntestIds, req.getFromTime(),req.getThruTime(), ncontentInfoRequiredFieldsForAllInfos, true);

            long t5 = Instant.now().toEpochMilli();


            //individualStats= loam_calculateTestWiseStatsBulk(questions, ncontentInfos,req.getCurrentNodeName(), false);
            for (ContentInfo contentInfo : ncontentInfos)
            {
                String testId=((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
                //averageStats.put( testId, loam_CalculateAverageTestWiseStatsFromTestNodeStatsInNode(testId, testNodeStatsHashMap));
                insightStats.put(testId,loam_calculateInsightPercentile(testId,Arrays.asList(testNodeStatsHashMap.get(testId))));
            }
        }
        //Map<String,Object> insightMap = loam_calculateTestInsight(individualStats);
        Map<String,Object> insightMap = loam_calculateTestInsight(req.getStudentId(),insightStats);

        Instant completedAt = Instant.now();
        return new AMTestWiseStatsRes( individualStats, averageStats, insightMap, req.getCurrentNodeName(), req.getCurrentNodeId());
    }

    /**
     * calculate test Insight for studentId
     *
     * @param studentId
     * @param insightStats
     * @return
     */
    private Map<String, Object> loam_calculateTestInsight(Long studentId, Map<String, Map<String, Double>> insightStats) {
        Map<String, Object> insightMap = new HashMap<>();
        Double averagePercentile = 0.0;
        Double totalPercentile = 0.0;
        Integer totalSize=0;

        for(Map.Entry<String,Map<String,Double>> entry: insightStats.entrySet()){
            if(entry.getValue()!=null && entry.getValue().get(String.valueOf(studentId))!=null) {
                totalPercentile += entry.getValue().get(String.valueOf(studentId));
                totalSize++;
            }
        }

        if (totalSize > 0) {
            averagePercentile = totalPercentile / totalSize;
        }
        String[] firstGrade = ConfigUtils.INSTANCE.getStringValue("FIRST_GRADE").split("\\$");
        String[] secondGrade = ConfigUtils.INSTANCE.getStringValue("SECOND_GRADE").split("\\$");
        String[] thirdGrade = ConfigUtils.INSTANCE.getStringValue("THIRD_GRADE").split("\\$");
        String[] fourthGrade = ConfigUtils.INSTANCE.getStringValue("FOURTH_GRADE").split("\\$");

        insightMap.put("insight_marks",averagePercentile);
        if(averagePercentile >= Float.parseFloat(firstGrade[1])){
            insightMap.put("insight_grade",firstGrade[0]);
        } else if (averagePercentile >= Float.parseFloat(secondGrade[1].split("-")[0]) && averagePercentile <= Float.parseFloat(secondGrade[1].split("-")[1])){
            insightMap.put("insight_grade",secondGrade[0]);
        } else if (averagePercentile >= Float.parseFloat(thirdGrade[1].split("-")[0]) && averagePercentile <= Float.parseFloat(thirdGrade[1].split("-")[1])){
            insightMap.put("insight_grade",thirdGrade[0]);
        } else{
            insightMap.put("insight_grade",fourthGrade[0]);
        }

        return insightMap;
    }

    private Map<String,Double> loam_calculateInsightPercentile(String testId, List<TestNodeStats> testNodeStatsList) {

        //StudentId->percentile
        Map<String,Double> totalPercentile = new HashMap<>();
        if(testNodeStatsList == null){
            return totalPercentile;
        }

        Map<String,Double> totalStudentMarksAchieved = new HashMap<>();
        for(TestNodeStats testNodeStats : testNodeStatsList){
            if (testNodeStats != null && testNodeStats.getTestNodeStudentData() != null) {
                Map<String, StudentMetaData> studentMetaDataMap = testNodeStats.getTestNodeStudentData().getStudentMetaDataMap();
                for (Map.Entry<String, StudentMetaData> entry : studentMetaDataMap.entrySet()) {
                    //Adding all marks i.e (of easy tough moderate unknown)
                    Double totalMarks = entry.getValue().getMarksAchievedEasy() + entry.getValue().getMarksAchievedModerate() + entry.getValue().getMarksAchievedTough() + entry.getValue().getMarksAchievedUnknown();
                    if (totalStudentMarksAchieved.containsKey(entry.getKey())) {
                        totalStudentMarksAchieved.put(entry.getKey(), totalStudentMarksAchieved.get(entry.getKey()) + totalMarks);
                    } else {
                        totalStudentMarksAchieved.put(entry.getKey(), totalMarks);
                    }
                }
            }
        }

        //sorting map for students.
        Map<String,Double> totalStudentMarkSorted = totalStudentMarksAchieved
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));

        Double rank=1.0;
        int size = totalStudentMarkSorted.size();
        // calculating percentile
        for (Map.Entry<String,Double> entry:totalStudentMarkSorted.entrySet()){
            Double percentile = (size - rank + 1) * 100 / size;
            totalPercentile.put(entry.getKey(),percentile);
            rank += 1;
        }

        return totalPercentile;
    }

    private AMTestStats loam_CalculateAverageTestWiseStatsFromTestNodeStatsInNode(String testId, HashMap<String, TestNodeStats> testNodeStatsHashMap) {
        AMTestStats amTestStats = new AMTestStats();

        double marksAcheived = 0.0;
        double totalMarks = 0.0;

        if(!testNodeStatsHashMap.containsKey(testId)){
            return amTestStats;
        }
        TestNodeStats testNodeStats = testNodeStatsHashMap.get(testId);

        if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getMarksAchieved()){
            marksAcheived += testNodeStats.getEasyQuestionStats().getMarksAchieved();
        }
        if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getMarksAchieved()){
            marksAcheived += testNodeStats.getModerateQuestionStats().getMarksAchieved();
        }
        if(null != testNodeStats.getToughQuestionStats() && null != testNodeStats.getToughQuestionStats().getMarksAchieved()){
            marksAcheived += testNodeStats.getToughQuestionStats().getMarksAchieved();
        }
        if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getMarksAchieved()){
            marksAcheived += testNodeStats.getUnknownQuestionStats().getMarksAchieved();
        }
        if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getTotalMarks()){
            totalMarks += testNodeStats.getEasyQuestionStats().getTotalMarks();
        }
        if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getTotalMarks()){
            totalMarks += testNodeStats.getModerateQuestionStats().getTotalMarks();
        }
        if(null != testNodeStats.getToughQuestionStats() && null != testNodeStats.getToughQuestionStats().getTotalMarks()){
            totalMarks += testNodeStats.getToughQuestionStats().getTotalMarks();
        }
        if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getTotalMarks()){
            totalMarks += testNodeStats.getUnknownQuestionStats().getTotalMarks();
        }

        long studentCount = testNodeStats.getStudentCount();

        if(studentCount != 0){
            marksAcheived = marksAcheived / studentCount;
            totalMarks = totalMarks / studentCount;
        }

        marksAcheived = loam_roundAvoid(marksAcheived, 1);
        totalMarks = loam_roundAvoid(totalMarks,1);

        amTestStats.setMarks(marksAcheived);
        amTestStats.setTotalMarks(totalMarks);

        return amTestStats;
    }

    private AMTestStats loam_CalculateAverageTestWiseStatsFromTestNodeStats(String testId, HashMap<String, List<TestNodeStats>> testIdTestNodeStatsMap) {
        AMTestStats amTestStats = new AMTestStats();

        double marksAcheived = 0.0;
        double totalMarks = 0.0;

        if(!testIdTestNodeStatsMap.containsKey(testId)){
            return amTestStats;
        }
        List<TestNodeStats> testNodeStatsList = testIdTestNodeStatsMap.get(testId);

        long studentCount = 1;

        for(TestNodeStats testNodeStats : testNodeStatsList){

            studentCount = testNodeStats.getStudentCount();
            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getMarksAchieved()){
                marksAcheived += testNodeStats.getEasyQuestionStats().getMarksAchieved();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getMarksAchieved()){
                marksAcheived += testNodeStats.getModerateQuestionStats().getMarksAchieved();
            }
            if(null != testNodeStats.getToughQuestionStats() && null != testNodeStats.getToughQuestionStats().getMarksAchieved()){
                marksAcheived += testNodeStats.getToughQuestionStats().getMarksAchieved();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getMarksAchieved()){
                marksAcheived += testNodeStats.getUnknownQuestionStats().getMarksAchieved();
            }
            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getTotalMarks()){
                totalMarks += testNodeStats.getEasyQuestionStats().getTotalMarks();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getTotalMarks()){
                totalMarks += testNodeStats.getModerateQuestionStats().getTotalMarks();
            }
            if(null != testNodeStats.getToughQuestionStats() && null != testNodeStats.getToughQuestionStats().getTotalMarks()){
                totalMarks += testNodeStats.getToughQuestionStats().getTotalMarks();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getTotalMarks()){
                totalMarks += testNodeStats.getUnknownQuestionStats().getTotalMarks();
            }
        }

        if(studentCount != 0){
            marksAcheived = marksAcheived / studentCount;
            totalMarks = totalMarks / studentCount;
        }

        marksAcheived = loam_roundAvoid(marksAcheived, 1);
        totalMarks = loam_roundAvoid(totalMarks,1);

        amTestStats.setMarks(marksAcheived);
        amTestStats.setTotalMarks(totalMarks);

        return amTestStats;
    }

    private LinkedHashMap<String, AMTestStats> loam_calculateTestWiseStatsBulk(List<CMDSQuestion> questions, List<ContentInfo> contentInfos,String nodeName, boolean isStudentmap)
    {

        LinkedHashMap<String, AMTestStats> studentMarks = new LinkedHashMap<>();
        for (ContentInfo contentInfo: contentInfos) {
            List<String> contentInfoQuestionIds = new ArrayList<>();
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
            long startTime = firstTestAttempt.getStartTime();
            Map<String, Float> marksGivenMap = new HashMap<>();
            for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                String questionId = qa.getQuestionId();
                contentInfoQuestionIds.add(questionId);
                marksGivenMap.put(qa.getQuestionId(),qa.getMarksGiven());
            }

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> contentInfoQuestionIds.contains(question.getId())).collect(Collectors.toList());

            double marks = 0.0;
            double totalMarks = 0.0;
            for (CMDSQuestion question : filteredQuestions) {
                if (loam_getNodesNamesFromQuestion(question).contains(nodeName)) {
                    marks += marksGivenMap.get(question.getId());
                    totalMarks += question.getMarks().getPositive();
                }
            }

            if(isStudentmap)
                studentMarks.put(contentInfo.getStudentId(), new AMTestStats(marks, totalMarks, startTime));
            else
                studentMarks.put(((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId(), new AMTestStats(marks, totalMarks, startTime));
        }

        return studentMarks;
    }

    public AMTestStats loam_calculateAverageTestWiseStats(List<CMDSQuestion> questions, List<ContentInfo> contentInfos, String nodeName, Long fromTime, Long thruTime) {


        Map<String, AMTestStats> studentMarks = new HashMap<>();

        if (nodeName.equalsIgnoreCase("root")) {
            for (ContentInfo contentInfo : contentInfos) {
                List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
                CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
                studentMarks.put(contentInfo.getStudentId(), new AMTestStats((double) firstTestAttempt.getMarksAcheived(), (double) firstTestAttempt.getTotalMarks()));
            }
        } else {
            studentMarks=loam_calculateTestWiseStatsBulk(questions, contentInfos, nodeName, true);
        }
        double marks = 0.0, totalMarks = 0.0;
        for (AMTestStats value : studentMarks.values()) {
            marks += value.getMarks();
            totalMarks += value.getTotalMarks();
        }
        marks = loam_roundAvoid(marks / studentMarks.size(), 1);
        totalMarks = loam_roundAvoid(totalMarks / studentMarks.size(), 1);


        return new AMTestStats(marks, totalMarks);
    }


    public AMQuestionAttemptsStatsRes loam_getQuestionAttemptStats(Long studentId, String currentNodeName,
                                                                   String currentNodeId, Long fromTime, Long thruTime) {

        if(currentNodeName.trim().equalsIgnoreCase("Mathematics")){
            currentNodeName = "Maths";
        }
        Map<String, AMQuestionStats> labelledStats = new HashMap<>();
        Map<String, AMQuestionStatsAvg> labelledStatsClassAvg = new HashMap<>();
        //map to nodeName -> (StudentId->StudentMetadata)
        Map<String,Map<String,Double>> labelledStatsInsights = new HashMap<>();
        //NodeName -> <Difficulty-><studentId,totaoPercentile>
        Map<String,Map<String,Map<String,Double>>> difficultyWiseStatsInsights = new HashMap<>();
        //calculating insights
        Map<String,Object> labelledInsights = new HashMap<>();
        Map<String,Object> difficultyWiseInsights = new HashMap<>();

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudent(studentId, fromTime, thruTime, contentInfoRequiredFields, true);

        List<BaseTopicTree> childNodes = loam_getChildNodes(currentNodeName, currentNodeId);

        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();
        List<String> testIds = new ArrayList<>();

        //Arun Dhwaj: 29th May, 2019: code review done as per scale point of view; Still its not optimised from concurrent point of view *//*
        Set<String> allQuestionIds=new HashSet<>();
        for (ContentInfo contentInfo : contentInfos) {
            String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
            testIds.add(testId);
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
            for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                allQuestionIds.add(qa.getQuestionId());
            }

        }

        //fields to fetch
        Set<String> questionRequiredFields = new HashSet<>();
        questionRequiredFields.add(CMDSQuestion.Constants._ID);
        questionRequiredFields.add(CMDSQuestion.Constants.MAIN_TAGS);
        questionRequiredFields.add(CMDSQuestion.Constants.TARGETS);
        questionRequiredFields.add(CMDSQuestion.Constants.GRADES);
        questionRequiredFields.add(CMDSQuestion.Constants.SOLUTION_INFO);
        questionRequiredFields.add(CMDSQuestion.Constants.DIFFICULTY);

        List<CMDSQuestion> questions = lOAMDAO.loam_getQuestionsByTest(new ArrayList(allQuestionIds), questionRequiredFields, true);

        for (ContentInfo contentInfo : contentInfos) {
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> questionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> questionIds.contains(question.getId())).collect(Collectors.toList());

            testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);
        }


        List<String> childNodeNames = new ArrayList<>();

        Set<String> nodNames= childNodes.parallelStream().map(childNode -> childNode.getName()).collect(Collectors.toSet());

        //fields to fetch
        Set<String> testNodeStatsRequiredFields = new HashSet<>();
        testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);

        List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, new ArrayList(nodNames), testNodeStatsRequiredFields, true);

        for (BaseTopicTree childNode : childNodes) {
            childNodeNames.add(childNode.getName());

//			labelledStats.put(childNode.getName(), calculateAMQuestionStats(
//					lOAMDAO.getLabelWiseQuestions(questionIds, childNode.getName()), studentId));
            labelledStats.put(childNode.getName(), loam_calculateAMQuestionStats(testQuestionMap, studentId, childNode.getName()));

            labelledStatsClassAvg.put(childNode.getName(), loam_calculateLabelledStatsClassAvgBulkNodes(childNode.getName(), testNodeStatsList));
            labelledStatsInsights.put(childNode.getName(), loam_calculateLabelledInsightPercentile(childNode.getName(), testNodeStatsList));
            difficultyWiseStatsInsights.put(childNode.getName(),loam_calculateDifficultyWiseInsightPercentile(childNode.getName(),testNodeStatsList));
        }

        Map<String, AMQuestionStats> difficultyWiseStats = new HashMap<>();
        difficultyWiseStats = loam_calculateAMDifficultyWiseStats(questions, contentInfos, currentNodeName, studentId, childNodeNames, currentNodeName);

        Map<String, AMQuestionStatsAvg> difficultyWiseStatsClassAvg = loam_calculateDifficultyWiseStatsClassAvg(contentInfos, currentNodeName, childNodes, testIds,testNodeStatsList);
        List<BaseTopicTreeBasic> childNodesBasic = loam_getNodesBasicFromNodes(childNodes);

        //calculating insights
        //labelledInsights.putAll(loam_calculateInsights(labelledStats));
        labelledInsights = loam_calculateTestInsight(studentId,labelledStatsInsights);
        //difficultyWiseInsights.putAll(loam_calculateDifficultyWiseInsights(difficultyWiseStats));
        difficultyWiseInsights = loam_calculateDifficultyWiseInsights(studentId,difficultyWiseStatsInsights);
        return new AMQuestionAttemptsStatsRes(labelledStats, labelledStatsClassAvg, labelledInsights, difficultyWiseStats, difficultyWiseStatsClassAvg, difficultyWiseInsights, childNodesBasic, currentNodeName, currentNodeId);
    }

    public AMQuestionAttemptsStatsRes loam_getQuestionAttemptStats_Subjective(Long studentId, String currentNodeName,
                                                                   String currentNodeId, Long fromTime, Long thruTime) {

        if(currentNodeName.trim().equalsIgnoreCase("Mathematics")){
            currentNodeName = "Maths";
        }
        Map<String, AMQuestionStats> labelledStats = new HashMap<>();
        Map<String, AMQuestionStatsAvg> labelledStatsClassAvg = new HashMap<>();
        //map to nodeName -> (StudentId->StudentMetadata)
        Map<String,Map<String,Double>> labelledStatsInsights = new HashMap<>();
        //NodeName -> <Difficulty-><studentId,totaoPercentile>
        Map<String,Map<String,Map<String,Double>>> difficultyWiseStatsInsights = new HashMap<>();
        //calculating insights
        Map<String,Object> labelledInsights = new HashMap<>();
        Map<String,Object> difficultyWiseInsights = new HashMap<>();

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudent_Subjective(studentId, fromTime, thruTime, contentInfoRequiredFields, true);

        List<BaseTopicTree> childNodes = loam_getChildNodes(currentNodeName, currentNodeId);

        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();
        List<QuestionAnalytics> resultEntriesList =new ArrayList<>();
        List<String> testIds = new ArrayList<>();

        //Arun Dhwaj: 29th May, 2019: code review done as per scale point of view; Still its not optimised from concurrent point of view *//*
        Set<String> allQuestionIds=new HashSet<>();
        for (ContentInfo contentInfo : contentInfos) {
            String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
            testIds.add(testId);
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
            for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                allQuestionIds.add(qa.getQuestionId());
            }

        }

        //fields to fetch
        Set<String> questionRequiredFields = new HashSet<>();
        questionRequiredFields.add(CMDSQuestion.Constants._ID);
        questionRequiredFields.add(CMDSQuestion.Constants.MAIN_TAGS);
        questionRequiredFields.add(CMDSQuestion.Constants.TARGETS);
        questionRequiredFields.add(CMDSQuestion.Constants.GRADES);
        questionRequiredFields.add(CMDSQuestion.Constants.SOLUTION_INFO);
        questionRequiredFields.add(CMDSQuestion.Constants.DIFFICULTY);

        List<CMDSQuestion> questions = lOAMDAO.loam_getQuestionsByTest(new ArrayList(allQuestionIds), questionRequiredFields, true);

        for (ContentInfo contentInfo : contentInfos) {
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> questionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> questionIds.contains(question.getId())).collect(Collectors.toList());

            testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);
            resultEntriesList.addAll(firstTestAttempt.getResultEntries());
        }


        List<String> childNodeNames = new ArrayList<>();

        Set<String> nodNames= childNodes.parallelStream().map(childNode -> childNode.getName()).collect(Collectors.toSet());

        //fields to fetch
        Set<String> testNodeStatsRequiredFields = new HashSet<>();
        testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);

        List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, new ArrayList(nodNames), testNodeStatsRequiredFields, true);

        for (BaseTopicTree childNode : childNodes) {
            childNodeNames.add(childNode.getName());
            labelledStats.put(childNode.getName(), loam_calculateAMQuestionStats_Subjective(testQuestionMap, resultEntriesList,studentId, childNode.getName()));
            labelledStatsClassAvg.put(childNode.getName(), loam_calculateLabelledStatsClassAvgBulkNodes_Subjective(childNode.getName(), testNodeStatsList));
            labelledStatsInsights.put(childNode.getName(), loam_calculateLabelledInsightPercentile(childNode.getName(), testNodeStatsList));
            difficultyWiseStatsInsights.put(childNode.getName(),loam_calculateDifficultyWiseInsightPercentile(childNode.getName(),testNodeStatsList));
        }

        Map<String, AMQuestionStats> difficultyWiseStats = new HashMap<>();
        difficultyWiseStats = loam_calculateAMDifficultyWiseStats_Subjective(questions, contentInfos, currentNodeName, studentId, childNodeNames, currentNodeName);
        Map<String, AMQuestionStatsAvg> difficultyWiseStatsClassAvg = loam_calculateDifficultyWiseStatsClassAvg_Subjective(contentInfos, currentNodeName, childNodes, testIds,testNodeStatsList);
        List<BaseTopicTreeBasic> childNodesBasic = loam_getNodesBasicFromNodes(childNodes);

        //calculating insights
        //labelledInsights.putAll(loam_calculateInsights(labelledStats));
        labelledInsights = loam_calculateTestInsight(studentId,labelledStatsInsights);
        //difficultyWiseInsights.putAll(loam_calculateDifficultyWiseInsights(difficultyWiseStats));
        difficultyWiseInsights = loam_calculateDifficultyWiseInsights(studentId,difficultyWiseStatsInsights);
        return new AMQuestionAttemptsStatsRes(labelledStats, labelledStatsClassAvg, labelledInsights, difficultyWiseStats, difficultyWiseStatsClassAvg, difficultyWiseInsights, childNodesBasic, currentNodeName, currentNodeId);
    }

    public AMQuestionAttemptsStatsRes loam_getQuestionAttemptStatsInsights(Long studentId, String currentNodeName,
                                                                   String currentNodeId, Long fromTime, Long thruTime) {

        if(currentNodeName.trim().equalsIgnoreCase("Mathematics")){
            currentNodeName = "Maths";
        }
        Map<String, AMQuestionStats> labelledStats = new HashMap<>();
        Map<String, AMQuestionStatsAvg> labelledStatsClassAvg = new HashMap<>();
        //map to nodeName -> (StudentId->StudentMetadata)
        Map<String,Map<String,Double>> labelledStatsInsights = new HashMap<>();
        //NodeName -> <Difficulty-><studentId,totaoPercentile>
        Map<String,Map<String,Map<String,Double>>> difficultyWiseStatsInsights = new HashMap<>();
        //calculating insights
        Map<String,Object> labelledInsights = new HashMap<>();
        Map<String,Object> difficultyWiseInsights = new HashMap<>();

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudent(studentId, fromTime, thruTime, contentInfoRequiredFields, true);

        List<BaseTopicTree> childNodes = loam_getChildNodes(currentNodeName, currentNodeId);



        //Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();
        List<String> testIds = new ArrayList<>();

        /*Arun Dhwaj: 29th May, 2019: code review done as per scale point of view; Still its not optimised from concurrent point of view */
        for (ContentInfo contentInfo : contentInfos) {
            String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
            testIds.add(testId);
        }


        List<String> childNodeNames = new ArrayList<>();

        Set<String> nodNames= childNodes.parallelStream().map(childNode -> childNode.getName()).collect(Collectors.toSet());

        //fields to fetch
        Set<String> testNodeStatsRequiredFields = new HashSet<>();
        testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.Node_Name);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.TEST_NODE_STUDENT_DATA);
        testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);

        List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStatsForNodeNames(testIds, new ArrayList(nodNames), testNodeStatsRequiredFields, true);

        for (BaseTopicTree childNode : childNodes) {
            labelledStatsInsights.put(childNode.getName(), loam_calculateLabelledInsightPercentile(childNode.getName(), testNodeStatsList));
            difficultyWiseStatsInsights.put(childNode.getName(),loam_calculateDifficultyWiseInsightPercentile(childNode.getName(),testNodeStatsList));
        }

        labelledInsights = loam_calculateTestInsight(studentId,labelledStatsInsights);
        difficultyWiseInsights = loam_calculateDifficultyWiseInsights(studentId,difficultyWiseStatsInsights);

        return new AMQuestionAttemptsStatsRes(labelledStats, labelledStatsClassAvg, labelledInsights, null, null, difficultyWiseInsights, null, currentNodeName, currentNodeId);
    }

    private Map<String,Map<String,Double>> loam_calculateDifficultyWiseInsightPercentile(String nodeName, List<TestNodeStats> testNodeStatsList) {

        //map of difficulty->(studentId->total)
        Map<String,Map<String,Double>> totalStudentMarksAchieved = new HashMap<>();
        if(testNodeStatsList == null){
            return totalStudentMarksAchieved;
        }
        //filtering for curNode
        List<TestNodeStats> filteredTestNodeStatsList = testNodeStatsList.parallelStream().filter(loam_getTestNodeStatsforNodeNameFilter(nodeName)).collect(Collectors.toList());

        //map student->total marks
        Map<String,Double> studentTotalEasyMarks = new HashMap<>();
        Map<String,Double> studentTotalModerateMarks = new HashMap<>();
        Map<String,Double> studentTotalToughMarks = new HashMap<>();
        Map<String,Double> studentTotalUnknownMarks = new HashMap<>();

        for(TestNodeStats testNodeStats : filteredTestNodeStatsList){
            if (testNodeStats != null && testNodeStats.getTestNodeStudentData() != null) {
                Map<String, StudentMetaData> studentMetaDataMap = testNodeStats.getTestNodeStudentData().getStudentMetaDataMap();
                for (Map.Entry<String, StudentMetaData> entry : studentMetaDataMap.entrySet()) {
                    //for easy marks
                    if (studentTotalEasyMarks.containsKey(entry.getKey())) {
                        studentTotalEasyMarks.put(entry.getKey(), studentTotalEasyMarks.get(entry.getKey()) + entry.getValue().getMarksAchievedEasy());
                    } else {
                        studentTotalEasyMarks.put(entry.getKey(), entry.getValue().getMarksAchievedEasy());
                    }
                    //for tough marks
                    if (studentTotalToughMarks.containsKey(entry.getKey())) {
                        studentTotalToughMarks.put(entry.getKey(), studentTotalToughMarks.get(entry.getKey()) + entry.getValue().getMarksAchievedTough());
                    } else {
                        studentTotalToughMarks.put(entry.getKey(), entry.getValue().getMarksAchievedTough());
                    }
                    //for moderate marks
                    if (studentTotalModerateMarks.containsKey(entry.getKey())) {
                        studentTotalModerateMarks.put(entry.getKey(), studentTotalModerateMarks.get(entry.getKey()) + entry.getValue().getMarksAchievedModerate());
                    } else {
                        studentTotalModerateMarks.put(entry.getKey(), entry.getValue().getMarksAchievedModerate());
                    }
                    //for unknown marks
                    if (studentTotalUnknownMarks.containsKey(entry.getKey())) {
                        studentTotalUnknownMarks.put(entry.getKey(), studentTotalUnknownMarks.get(entry.getKey()) + entry.getValue().getMarksAchievedUnknown());
                    } else {
                        studentTotalUnknownMarks.put(entry.getKey(), entry.getValue().getMarksAchievedUnknown());
                    }
                }
            }
        }
        //StudentId->percentile
        Map<String,Double> totalPercentileEasy = loam_sortMapByValueDescendingAndCalculatePercentile(studentTotalEasyMarks);
        Map<String,Double> totalPercentileModerate = loam_sortMapByValueDescendingAndCalculatePercentile(studentTotalModerateMarks);
        Map<String,Double> totalPercentileTough = loam_sortMapByValueDescendingAndCalculatePercentile(studentTotalToughMarks);
        Map<String,Double> totalPercentileUnknown = loam_sortMapByValueDescendingAndCalculatePercentile(studentTotalUnknownMarks);

        totalStudentMarksAchieved.put("easy",totalPercentileEasy);
        totalStudentMarksAchieved.put("medium",totalPercentileModerate);
        totalStudentMarksAchieved.put("tough",totalPercentileTough);
        totalStudentMarksAchieved.put("unknown",totalPercentileUnknown);
        return totalStudentMarksAchieved;
    }

    private Map<String, Double> loam_sortMapByValueDescendingAndCalculatePercentile(Map<String, Double> studentTotalMarks) {
        Map<String,Double> totalPercentile = new HashMap<>();
        Map<String, Double> studentTotalMarksSorted = studentTotalMarks
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));

        Double rank=1.0;
        int size = studentTotalMarksSorted.size();
        // calculating percentile
        for (Map.Entry<String,Double> entry:studentTotalMarksSorted.entrySet()){
            Double percentile = (size - rank + 1) * 100 / size;
            totalPercentile.put(entry.getKey(),percentile);
            rank += 1;
        }
        return totalPercentile;
    }

    private Map<String,Double> loam_calculateLabelledInsightPercentile(String nodeName, List<TestNodeStats> testNodeStatsList) {

        //StudentId->percentile
        Map<String,Double> totalPercentile = new HashMap<>();
        if(testNodeStatsList == null){
            return totalPercentile;
        }
        //filtering for curNode
        List<TestNodeStats> filteredTestNodeStatsList = testNodeStatsList.parallelStream().filter(loam_getTestNodeStatsforNodeNameFilter(nodeName)).collect(Collectors.toList());

        Map<String,Double> totalStudentMarksAchieved = new HashMap<>();
        for(TestNodeStats testNodeStats : filteredTestNodeStatsList){
            if (testNodeStats != null && testNodeStats.getTestNodeStudentData() != null) {
                Map<String, StudentMetaData> studentMetaDataMap = testNodeStats.getTestNodeStudentData().getStudentMetaDataMap();
                for (Map.Entry<String, StudentMetaData> entry : studentMetaDataMap.entrySet()) {
                    //Adding all marks i.e (of easy tough moderate unknown)
                    Double totalMarks = entry.getValue().getMarksAchievedEasy() + entry.getValue().getMarksAchievedModerate() + entry.getValue().getMarksAchievedTough() + entry.getValue().getMarksAchievedUnknown();
                    if (totalStudentMarksAchieved.containsKey(entry.getKey())) {
                        totalStudentMarksAchieved.put(entry.getKey(), totalStudentMarksAchieved.get(entry.getKey()) + totalMarks);
                    } else {
                        totalStudentMarksAchieved.put(entry.getKey(), totalMarks);
                    }
                }
            }
        }

        //sorting map for students.
        Map<String,Double> totalStudentMarkSorted = totalStudentMarksAchieved
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));

        Double rank=1.0;
        int size = totalStudentMarkSorted.size();
        // calculating percentile
        for (Map.Entry<String,Double> entry:totalStudentMarkSorted.entrySet()){
            Double percentile = (size - rank + 1) * 100 / size;
            totalPercentile.put(entry.getKey(),percentile);
            rank += 1;
        }
        return totalPercentile;
    }

    /**
     * Calculates Insights..
     *
     * @param labelledStats
     * @return
     */
    private Map<String,String> loam_calculateInsights(Map<String, AMQuestionStats> labelledStats) {
        Map<String,String> insightMap = new HashMap<>();
        long total = 0L;
        long correct = 0L;
        double percentage = 0.00;
        for (Map.Entry<String, AMQuestionStats> entry : labelledStats.entrySet()) {
            AMQuestionStats processedStats = entry.getValue();
            //calculating total stats
            total = total + processedStats.getNoCorrect() + processedStats.getNoWrong() + processedStats.getNoUnattempted();
            correct = correct + processedStats.getNoCorrect();
        }
        if (total > 0) {
            percentage = correct * 100 / total;
        }
        insightMap.put("insight_marks",String.valueOf(percentage));
        if(null != InsightGrades.getGrade(percentage))
            insightMap.put("insight_grade",InsightGrades.getGrade(percentage).getGrade());
        else
            insightMap.put("insight_grade",null);
        return insightMap;
    }

    /**
     * Calculates difficulty-wise Insights..
     *
     * @return
     */
    private Map<String,Object> loam_calculateDifficultyWiseInsights(Long studentId, Map<String, Map<String, Map<String, Double>>> difficultyStats) {

        Double total = 0.0;
        Double averagePercentile = 0.0;
        Map<String, Object> insightMap = new HashMap<>();
        Map<String, DifficultyInfo> totalDifficultyWiseMap = new HashMap<>();

        // node -> difficulty->studentMetadata
        for (Map.Entry<String, Map<String, Map<String, Double>>> entry : difficultyStats.entrySet()) {
            Map<String, Map<String, Double>> difficultyToTotalStudentPercentile = entry.getValue();
            if (difficultyToTotalStudentPercentile != null) {
                for (Map.Entry<String, Map<String, Double>> diffTotalEntry : difficultyToTotalStudentPercentile.entrySet()) {
                    if (diffTotalEntry != null && diffTotalEntry.getValue() != null && diffTotalEntry.getValue().containsKey(String.valueOf(studentId))) {
                        Double individualTotalDifficultyWise = diffTotalEntry.getValue().get(String.valueOf(studentId));
                        if (totalDifficultyWiseMap.containsKey(diffTotalEntry.getKey())) {
                            DifficultyInfo difficultyInfo = totalDifficultyWiseMap.get(diffTotalEntry.getKey());
                            difficultyInfo.setTotalMarks(difficultyInfo.getTotalMarks() + individualTotalDifficultyWise);
                            difficultyInfo.setCount(difficultyInfo.getCount() + 1);
                            totalDifficultyWiseMap.put(diffTotalEntry.getKey(), difficultyInfo);
                        } else {
                            totalDifficultyWiseMap.put(diffTotalEntry.getKey(), new DifficultyInfo(individualTotalDifficultyWise, 1));
                        }
                    }
                }
            }
        }

        if (totalDifficultyWiseMap.size() > 0) {
            for (Map.Entry<String, DifficultyInfo> entry : totalDifficultyWiseMap.entrySet()) {
                Map<String, Object> individualMap = new HashMap<>();
                Double percentage = entry.getValue().getTotalMarks() / entry.getValue().getCount();
                individualMap.put("insight_marks", percentage);
                individualMap.put("insight_grade", loam_getDifficultyGrade(percentage));
                insightMap.put(entry.getKey(), individualMap);
                total = total + percentage;
            }
        }
        if (total >= 0) {
            averagePercentile = total / totalDifficultyWiseMap.size();
        }
        insightMap.put("insight_marks", averagePercentile);
        insightMap.put("insight_grade", loam_getDifficultyGrade(averagePercentile));

        return insightMap;
    }

    private Object loam_getDifficultyGrade(double averagePercentile) {
        String[] firstGrade = ConfigUtils.INSTANCE.getStringValue("FIRST_GRADE").split("\\$");
        String[] secondGrade = ConfigUtils.INSTANCE.getStringValue("SECOND_GRADE").split("\\$");
        String[] thirdGrade = ConfigUtils.INSTANCE.getStringValue("THIRD_GRADE").split("\\$");
        String[] fourthGrade = ConfigUtils.INSTANCE.getStringValue("FOURTH_GRADE").split("\\$");

        if(averagePercentile >= Double.parseDouble(firstGrade[1])){
            return firstGrade[0];
        } else if (averagePercentile >= Double.parseDouble(secondGrade[1].split("-")[0]) && averagePercentile <= Double.parseDouble(secondGrade[1].split("-")[1])){
            return secondGrade[0];
        } else if (averagePercentile >= Double.parseDouble(thirdGrade[1].split("-")[0]) && averagePercentile <= Double.parseDouble(thirdGrade[1].split("-")[1])){
            return thirdGrade[0];
        } else {
            return fourthGrade[0];
        }
    }

    private AMQuestionStatsAvg calculateLabelledStatsClassAvg(String nodeName, List<ContentInfo> contentInfos) {
        List<String> testIds = new ArrayList<>();

        for (ContentInfo contentInfo : contentInfos) {
            String testId = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestId();
            testIds.add(testId);
        }

        List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStats(testIds, nodeName, new HashSet<>(), true);


        AMQuestionStatsAvg amQuestionStatsAvg = new AMQuestionStatsAvg();
        Double noCorrect = 0.0, noWrong = 0.0, noUnattempted = 0.0;

        for (TestNodeStats testNodeStats : testNodeStatsList) {
            Double curNodeNoCorrect = 0.0, curNodeNoWrong = 0.0, curNodeNoUnattempted = 0.0;

            curNodeNoCorrect += testNodeStats.getEasyQuestionStats().getCorrect();
            curNodeNoCorrect += testNodeStats.getModerateQuestionStats().getCorrect();
            curNodeNoCorrect += testNodeStats.getToughQuestionStats().getCorrect();
            curNodeNoCorrect += testNodeStats.getUnknownQuestionStats().getCorrect();

            curNodeNoWrong += testNodeStats.getEasyQuestionStats().getWrong();
            curNodeNoWrong += testNodeStats.getModerateQuestionStats().getWrong();
            curNodeNoWrong += testNodeStats.getToughQuestionStats().getWrong();
            curNodeNoWrong += testNodeStats.getUnknownQuestionStats().getWrong();

            curNodeNoUnattempted += testNodeStats.getEasyQuestionStats().getUnattempted();
            curNodeNoUnattempted += testNodeStats.getModerateQuestionStats().getUnattempted();
            curNodeNoUnattempted += testNodeStats.getToughQuestionStats().getUnattempted();
            curNodeNoUnattempted += testNodeStats.getUnknownQuestionStats().getUnattempted();


            noCorrect += curNodeNoCorrect / testNodeStats.getStudentCount();
            noWrong += curNodeNoWrong / testNodeStats.getStudentCount();
            noUnattempted += curNodeNoUnattempted / testNodeStats.getStudentCount();



        }


        amQuestionStatsAvg.setNoCorrect(loam_roundAvoid(noCorrect, 1));
        amQuestionStatsAvg.setNoWrong(loam_roundAvoid(noWrong, 1));
        amQuestionStatsAvg.setNoUnattempted(loam_roundAvoid(noUnattempted, 1));

        return amQuestionStatsAvg;
    }

    private AMQuestionStatsAvg loam_calculateLabelledStatsClassAvgBulkNodes(String nodeName, List<TestNodeStats> testNodeStatsListAllNodes) {

        List<TestNodeStats> filteredTestNodeStatsList = testNodeStatsListAllNodes.parallelStream().filter(loam_getTestNodeStatsforNodeNameFilter(nodeName)).collect(Collectors.toList());

        AMQuestionStatsAvg amQuestionStatsAvg = new AMQuestionStatsAvg();
        Double noCorrect = 0.0, noWrong = 0.0, noUnattempted = 0.0;

        for (TestNodeStats testNodeStats : filteredTestNodeStatsList) {
            Double curNodeNoCorrect = 0.0, curNodeNoWrong = 0.0, curNodeNoUnattempted = 0.0;

            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getEasyQuestionStats().getCorrect();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getModerateQuestionStats().getCorrect();
            }

            if(null != testNodeStats.getToughQuestionStats().getCorrect() && null != testNodeStats.getToughQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getToughQuestionStats().getCorrect();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getUnknownQuestionStats().getCorrect();
            }


            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getEasyQuestionStats().getWrong();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getModerateQuestionStats().getWrong();
            }

            if(null != testNodeStats.getToughQuestionStats().getCorrect() && null != testNodeStats.getToughQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getToughQuestionStats().getWrong();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getUnknownQuestionStats().getWrong();
            }

            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getEasyQuestionStats().getUnattempted();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getModerateQuestionStats().getUnattempted();
            }

            if(null != testNodeStats.getToughQuestionStats().getCorrect() && null != testNodeStats.getToughQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getToughQuestionStats().getUnattempted();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getUnknownQuestionStats().getUnattempted();
            }


            if(testNodeStats.getStudentCount() != 0){
                noCorrect += curNodeNoCorrect / testNodeStats.getStudentCount();
                noWrong += curNodeNoWrong / testNodeStats.getStudentCount();
                noUnattempted += curNodeNoUnattempted / testNodeStats.getStudentCount();
            }
            else{
                noCorrect += 0;
                noWrong += 0;
                noUnattempted += 0;
            }
        }

        amQuestionStatsAvg.setNoCorrect(loam_roundAvoid(noCorrect, 1));
        amQuestionStatsAvg.setNoWrong(loam_roundAvoid(noWrong, 1));
        amQuestionStatsAvg.setNoUnattempted(loam_roundAvoid(noUnattempted, 1));

        return amQuestionStatsAvg;
    }

    private AMQuestionStatsAvg loam_calculateLabelledStatsClassAvgBulkNodes_Subjective(String nodeName, List<TestNodeStats> testNodeStatsListAllNodes) {

        List<TestNodeStats> filteredTestNodeStatsList = testNodeStatsListAllNodes.parallelStream().filter(loam_getTestNodeStatsforNodeNameFilter(nodeName)).collect(Collectors.toList());

        AMQuestionStatsAvg amQuestionStatsAvg = new AMQuestionStatsAvg();
        float noCorrect = 0.0f, noWrong = 0.0f, noUnattempted = 0.0f;

        for (TestNodeStats testNodeStats : filteredTestNodeStatsList) {
            Double curNodeNoCorrect = 0.0, curNodeNoWrong = 0.0, curNodeNoUnattempted = 0.0;

            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getEasyQuestionStats().getMarksScored();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getModerateQuestionStats().getMarksScored();
            }

            if(null != testNodeStats.getToughQuestionStats().getCorrect() && null != testNodeStats.getToughQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getToughQuestionStats().getMarksScored();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getCorrect()){
                curNodeNoCorrect += testNodeStats.getUnknownQuestionStats().getMarksScored();
            }


            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getEasyQuestionStats().getMarksMissed();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getModerateQuestionStats().getMarksMissed();
            }

            if(null != testNodeStats.getToughQuestionStats().getCorrect() && null != testNodeStats.getToughQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getToughQuestionStats().getMarksMissed();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getWrong()){
                curNodeNoWrong += testNodeStats.getUnknownQuestionStats().getMarksMissed();
            }

            if(null != testNodeStats.getEasyQuestionStats() && null != testNodeStats.getEasyQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getEasyQuestionStats().getMarksUnattempted();
            }
            if(null != testNodeStats.getModerateQuestionStats() && null != testNodeStats.getModerateQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getModerateQuestionStats().getMarksUnattempted();
            }

            if(null != testNodeStats.getToughQuestionStats().getCorrect() && null != testNodeStats.getToughQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getToughQuestionStats().getMarksUnattempted();
            }
            if(null != testNodeStats.getUnknownQuestionStats() && null != testNodeStats.getUnknownQuestionStats().getUnattempted()){
                curNodeNoUnattempted += testNodeStats.getUnknownQuestionStats().getMarksUnattempted();
            }


            if(testNodeStats.getStudentCount() != 0){
                noCorrect += curNodeNoCorrect / testNodeStats.getStudentCount();
                noWrong += curNodeNoWrong / testNodeStats.getStudentCount();
                noUnattempted += curNodeNoUnattempted / testNodeStats.getStudentCount();
            }
            else{
                noCorrect += 0;
                noWrong += 0;
                noUnattempted += 0;
            }
        }

        amQuestionStatsAvg.setMarksScored(loam_roundMarks(noCorrect));
        amQuestionStatsAvg.setMarksMissed(loam_roundMarks(noWrong));
        amQuestionStatsAvg.setMarksUnattempted(loam_roundMarks(noUnattempted));

        return amQuestionStatsAvg;
    }

    private Predicate<TestNodeStats> loam_getTestNodeStatsforNodeNameFilter(String nodeName) {

        return testNodeStats->null != testNodeStats.getNodeName() && testNodeStats.getNodeName().equalsIgnoreCase(nodeName);
    }

    private Map<String, AMQuestionStatsAvg> loam_calculateDifficultyWiseStatsClassAvg(List<ContentInfo> contentInfos, String currentNodeName, List<BaseTopicTree> childNodes, List<String> testIds, List<TestNodeStats> testNodeStatsListAllNodes) {
        Map<String, AMQuestionStatsAvg> difficultyWiseStatsClassAvg = new HashMap<>();

        AMQuestionStatsAvg amQuestionStatsEasy = new AMQuestionStatsAvg();
        AMQuestionStatsAvg amQuestionStatsModerate = new AMQuestionStatsAvg();
        AMQuestionStatsAvg amQuestionStatsTough = new AMQuestionStatsAvg();
        AMQuestionStatsAvg amQuestionStatsUnknown = new AMQuestionStatsAvg();


        if (currentNodeName.equalsIgnoreCase("root")) {

            Double noCorrectEasy = 0.0, noWrongEasy = 0.0, noUnattemptedEasy = 0.0;
            Double noCorrectModerate = 0.0, noWrongModerate = 0.0, noUnattemptedModerate = 0.0;
            Double noCorrectHard = 0.0, noWrongHard = 0.0, noUnattemptedHard = 0.0;
            Double noCorrectUnknown = 0.0, noWrongUnknown = 0.0, noUnattemptedUnknown = 0.0;


            for (TestNodeStats testNodeStats : testNodeStatsListAllNodes) {
                noCorrectEasy += (double) Math.round((double) testNodeStats.getEasyQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongEasy += (double) Math.round((double) testNodeStats.getEasyQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedEasy += (double) Math.round((double) testNodeStats.getEasyQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                noCorrectModerate += (double) Math.round((double) testNodeStats.getModerateQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongModerate += (double) Math.round((double) testNodeStats.getModerateQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedModerate += (double) Math.round((double) testNodeStats.getModerateQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                noCorrectHard += (double) Math.round((double) testNodeStats.getToughQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongHard += (double) Math.round((double) testNodeStats.getToughQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedHard += (double) Math.round((double) testNodeStats.getToughQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;

                noCorrectUnknown += (double) Math.round((double) testNodeStats.getUnknownQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongUnknown += (double) Math.round((double) testNodeStats.getUnknownQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedUnknown += (double) Math.round((double) testNodeStats.getUnknownQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
            }

            amQuestionStatsEasy.setNoCorrect(loam_roundAvoid(noCorrectEasy, 1));
            amQuestionStatsEasy.setNoWrong(loam_roundAvoid(noWrongEasy, 1));
            amQuestionStatsEasy.setNoUnattempted(loam_roundAvoid(noUnattemptedEasy, 1));

            amQuestionStatsModerate.setNoCorrect(loam_roundAvoid(noCorrectModerate, 1));
            amQuestionStatsModerate.setNoWrong(loam_roundAvoid(noWrongModerate, 1));
            amQuestionStatsModerate.setNoUnattempted(loam_roundAvoid(noUnattemptedModerate, 1));

            amQuestionStatsTough.setNoCorrect(loam_roundAvoid(noCorrectHard, 1));
            amQuestionStatsTough.setNoWrong(loam_roundAvoid(noWrongHard, 1));
            amQuestionStatsTough.setNoUnattempted(loam_roundAvoid(noUnattemptedHard, 1));

            amQuestionStatsUnknown.setNoCorrect(loam_roundAvoid(noCorrectUnknown, 1));
            amQuestionStatsUnknown.setNoWrong(loam_roundAvoid(noWrongUnknown, 1));
            amQuestionStatsUnknown.setNoUnattempted(loam_roundAvoid(noUnattemptedUnknown, 1));

            difficultyWiseStatsClassAvg.put("easy", amQuestionStatsEasy);
            difficultyWiseStatsClassAvg.put("medium", amQuestionStatsModerate);
            difficultyWiseStatsClassAvg.put("hard", amQuestionStatsTough);
            difficultyWiseStatsClassAvg.put("unknown", amQuestionStatsUnknown);


        } else {

            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);

            List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStats(testIds, currentNodeName, testNodeStatsRequiredFields, true);


            Double noCorrectEasy = 0.0, noWrongEasy = 0.0, noUnattemptedEasy = 0.0;
            Double noCorrectModerate = 0.0, noWrongModerate = 0.0, noUnattemptedModerate = 0.0;
            Double noCorrectHard = 0.0, noWrongHard = 0.0, noUnattemptedHard = 0.0;
            Double noCorrectUnknown = 0.0, noWrongUnknown = 0.0, noUnattemptedUnknown = 0.0;
            for (TestNodeStats testNodeStats : testNodeStatsList) {
                noCorrectEasy += (double) Math.round((double) testNodeStats.getEasyQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongEasy += (double) Math.round((double) testNodeStats.getEasyQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedEasy += (double) Math.round((double) testNodeStats.getEasyQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                noCorrectModerate += (double) Math.round((double) testNodeStats.getModerateQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongModerate += (double) Math.round((double) testNodeStats.getModerateQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedModerate += (double) Math.round((double) testNodeStats.getModerateQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                noCorrectHard += (double) Math.round((double) testNodeStats.getToughQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongHard += (double) Math.round((double) testNodeStats.getToughQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedHard += (double) Math.round((double) testNodeStats.getToughQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;

                noCorrectUnknown += (double) Math.round((double) testNodeStats.getUnknownQuestionStats().getCorrect() / testNodeStats.getStudentCount() * 100) / 100;
                noWrongUnknown += (double) Math.round((double) testNodeStats.getUnknownQuestionStats().getWrong() / testNodeStats.getStudentCount() * 100) / 100;
                noUnattemptedUnknown += (double) Math.round((double) testNodeStats.getUnknownQuestionStats().getUnattempted() / testNodeStats.getStudentCount() * 100) / 100;

            }

            amQuestionStatsEasy.setNoCorrect(loam_roundAvoid(noCorrectEasy, 1));
            amQuestionStatsEasy.setNoWrong(loam_roundAvoid(noWrongEasy, 1));
            amQuestionStatsEasy.setNoUnattempted(loam_roundAvoid(noUnattemptedEasy, 1));

            amQuestionStatsModerate.setNoCorrect(loam_roundAvoid(noCorrectModerate, 1));
            amQuestionStatsModerate.setNoWrong(loam_roundAvoid(noWrongModerate, 1));
            amQuestionStatsModerate.setNoUnattempted(loam_roundAvoid(noUnattemptedModerate, 1));

            amQuestionStatsTough.setNoCorrect(loam_roundAvoid(noCorrectHard, 1));
            amQuestionStatsTough.setNoWrong(loam_roundAvoid(noWrongHard, 1));
            amQuestionStatsTough.setNoUnattempted(loam_roundAvoid(noUnattemptedHard, 1));

            amQuestionStatsUnknown.setNoCorrect(loam_roundAvoid(noCorrectUnknown, 1));
            amQuestionStatsUnknown.setNoWrong(loam_roundAvoid(noWrongUnknown, 1));
            amQuestionStatsUnknown.setNoUnattempted(loam_roundAvoid(noUnattemptedUnknown, 1));

            difficultyWiseStatsClassAvg.put("easy", amQuestionStatsEasy);
            difficultyWiseStatsClassAvg.put("medium", amQuestionStatsModerate);
            difficultyWiseStatsClassAvg.put("hard", amQuestionStatsTough);
            difficultyWiseStatsClassAvg.put("unknown", amQuestionStatsUnknown);

        }

        return difficultyWiseStatsClassAvg;
    }


    private Map<String, AMQuestionStatsAvg> loam_calculateDifficultyWiseStatsClassAvg_Subjective(List<ContentInfo> contentInfos, String currentNodeName, List<BaseTopicTree> childNodes, List<String> testIds, List<TestNodeStats> testNodeStatsListAllNodes) {
        Map<String, AMQuestionStatsAvg> difficultyWiseStatsClassAvg = new HashMap<>();

        AMQuestionStatsAvg amQuestionStatsEasy = new AMQuestionStatsAvg();
        AMQuestionStatsAvg amQuestionStatsModerate = new AMQuestionStatsAvg();
        AMQuestionStatsAvg amQuestionStatsTough = new AMQuestionStatsAvg();
        AMQuestionStatsAvg amQuestionStatsUnknown = new AMQuestionStatsAvg();


        if (currentNodeName.equalsIgnoreCase("root")) {

            Float marksScoredEasy = 0.0f, marksMissedEasy = 0.0f, marksUnattemptedEasy = 0.0f;
            Float marksScoredModerate = 0.0f, marksMissedModerate = 0.0f, marksUnattemptedModerate = 0.0f;
            Float marksScoredHard = 0.0f, marksMissedHard = 0.0f, marksUnattemptedHard = 0.0f;
            Float marksScoredUnknown = 0.0f, marksMissedUnknown = 0.0f, marksUnattemptedUnknown = 0.0f;


            for (TestNodeStats testNodeStats : testNodeStatsListAllNodes) {
                marksScoredEasy +=  Math.round((float) testNodeStats.getEasyQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedEasy +=  Math.round((float) testNodeStats.getEasyQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedEasy += (float) Math.round((float) testNodeStats.getEasyQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                marksScoredModerate += (float) Math.round((float) testNodeStats.getModerateQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedModerate += (float) Math.round((float) testNodeStats.getModerateQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedModerate += (float) Math.round((float) testNodeStats.getModerateQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                marksScoredHard += (float) Math.round((float) testNodeStats.getToughQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedHard += (float) Math.round((float) testNodeStats.getToughQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedHard += (float) Math.round((float) testNodeStats.getToughQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;

                marksScoredUnknown += (float) Math.round((float) testNodeStats.getUnknownQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedUnknown += (float) Math.round((float) testNodeStats.getUnknownQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedUnknown += (float) Math.round((float) testNodeStats.getUnknownQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
            }

            amQuestionStatsEasy.setMarksScored(loam_roundMarks(marksScoredEasy));
            amQuestionStatsEasy.setMarksMissed(loam_roundMarks(marksMissedEasy));
            amQuestionStatsEasy.setMarksUnattempted(loam_roundMarks(marksUnattemptedEasy));

            amQuestionStatsModerate.setMarksScored(loam_roundMarks(marksScoredModerate));
            amQuestionStatsModerate.setMarksMissed(loam_roundMarks(marksMissedModerate));
            amQuestionStatsModerate.setMarksUnattempted(loam_roundMarks(marksUnattemptedModerate));

            amQuestionStatsTough.setMarksScored(loam_roundMarks(marksScoredHard));
            amQuestionStatsTough.setMarksMissed(loam_roundMarks(marksMissedHard));
            amQuestionStatsTough.setMarksUnattempted(loam_roundMarks(marksUnattemptedHard));

            amQuestionStatsUnknown.setMarksScored(loam_roundMarks(marksScoredUnknown));
            amQuestionStatsUnknown.setMarksMissed(loam_roundMarks(marksMissedUnknown));
            amQuestionStatsUnknown.setMarksUnattempted(loam_roundMarks(marksUnattemptedUnknown));

            difficultyWiseStatsClassAvg.put("easy", amQuestionStatsEasy);
            difficultyWiseStatsClassAvg.put("medium", amQuestionStatsModerate);
            difficultyWiseStatsClassAvg.put("hard", amQuestionStatsTough);
            difficultyWiseStatsClassAvg.put("unknown", amQuestionStatsUnknown);


        } else {

            //fields to fetch
            Set<String> testNodeStatsRequiredFields = new HashSet<>();
            testNodeStatsRequiredFields.add(TestNodeStats.Constants._ID);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.EASY_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.MODERATE_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.TOUGH_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.UNKNOWN_QUESTION_STATS);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.STUDENT_COUNT);
            testNodeStatsRequiredFields.add(TestNodeStats.Constants.Test_ID);

            List<TestNodeStats> testNodeStatsList = lOAMDAO.loam_getTestNodeStats(testIds, currentNodeName, testNodeStatsRequiredFields, true);


            Float marksScoredEasy = 0.0f, marksMissedEasy = 0.0f, marksUnattemptedEasy = 0.0f;
            Float marksScoredModerate = 0.0f, marksMissedModerate = 0.0f, marksUnattemptedModerate = 0.0f;
            Float marksScoredHard = 0.0f, marksMissedHard = 0.0f, marksUnattemptedHard = 0.0f;
            Float marksScoredUnknown = 0.0f, marksMissedUnknown = 0.0f, marksUnattemptedUnknown = 0.0f;


            for (TestNodeStats testNodeStats : testNodeStatsList) {
                marksScoredEasy +=  Math.round((float) testNodeStats.getEasyQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedEasy +=  Math.round((float) testNodeStats.getEasyQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedEasy += (float) Math.round((float) testNodeStats.getEasyQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                marksScoredModerate += (float) Math.round((float) testNodeStats.getModerateQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedModerate += (float) Math.round((float) testNodeStats.getModerateQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedModerate += (float) Math.round((float) testNodeStats.getModerateQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
                marksScoredHard += (float) Math.round((float) testNodeStats.getToughQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedHard += (float) Math.round((float) testNodeStats.getToughQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedHard += (float) Math.round((float) testNodeStats.getToughQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;

                marksScoredUnknown += (float) Math.round((float) testNodeStats.getUnknownQuestionStats().getMarksScored() / testNodeStats.getStudentCount() * 100) / 100;
                marksMissedUnknown += (float) Math.round((float) testNodeStats.getUnknownQuestionStats().getMarksMissed() / testNodeStats.getStudentCount() * 100) / 100;
                marksUnattemptedUnknown += (float) Math.round((float) testNodeStats.getUnknownQuestionStats().getMarksUnattempted() / testNodeStats.getStudentCount() * 100) / 100;
            }

            amQuestionStatsEasy.setMarksScored(loam_roundMarks(marksScoredEasy));
            amQuestionStatsEasy.setMarksMissed(loam_roundMarks(marksMissedEasy));
            amQuestionStatsEasy.setMarksUnattempted(loam_roundMarks(marksUnattemptedEasy));

            amQuestionStatsModerate.setMarksScored(loam_roundMarks(marksScoredModerate));
            amQuestionStatsModerate.setMarksMissed(loam_roundMarks(marksMissedModerate));
            amQuestionStatsModerate.setMarksUnattempted(loam_roundMarks(marksUnattemptedModerate));

            amQuestionStatsTough.setMarksScored(loam_roundMarks(marksScoredHard));
            amQuestionStatsTough.setMarksMissed(loam_roundMarks(marksMissedHard));
            amQuestionStatsTough.setMarksUnattempted(loam_roundMarks(marksUnattemptedHard));

            amQuestionStatsUnknown.setMarksScored(loam_roundMarks(marksScoredUnknown));
            amQuestionStatsUnknown.setMarksMissed(loam_roundMarks(marksMissedUnknown));
            amQuestionStatsUnknown.setMarksUnattempted(loam_roundMarks(marksUnattemptedUnknown));

            difficultyWiseStatsClassAvg.put("easy", amQuestionStatsEasy);
            difficultyWiseStatsClassAvg.put("medium", amQuestionStatsModerate);
            difficultyWiseStatsClassAvg.put("hard", amQuestionStatsTough);
            difficultyWiseStatsClassAvg.put("unknown", amQuestionStatsUnknown);

        }

        return difficultyWiseStatsClassAvg;
    }
    private List<BaseTopicTreeBasic> loam_getNodesBasicFromNodes(List<BaseTopicTree> nodes) {
        List<BaseTopicTreeBasic> nodesBasicList = new ArrayList<>();
        for (BaseTopicTree node : nodes) {
            BaseTopicTreeBasic nodeBasic = new BaseTopicTreeBasic();
            nodeBasic.setId(node.getId());
            nodeBasic.setName(node.getName());
            nodesBasicList.add(nodeBasic);
        }
        return nodesBasicList;
    }

    private AMQuestionStats loam_calculateAMQuestionStats(Map<String, List<CMDSQuestion>> testQuestionMap, Long studentId, String nodeName) {
        Map<String, List<CMDSQuestion>> map=new HashMap<>();
        if (!nodeName.equalsIgnoreCase("root")) {
            for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet()) {
                if (entry.getValue().size() > 0){
                    List<CMDSQuestion> quests = entry.getValue().stream()
//                            .filter(Objects::nonNull)
                            .filter(p -> loam_getNodesNamesFromQuestion(p).contains(nodeName))
                            .collect(Collectors.toList());
                    map.put(entry.getKey(), quests);
                }
            }
        }else{
            map=testQuestionMap;
        }
     return loam_getAMQuestionStats(map,studentId);
    }


    private AMQuestionStats loam_getAMQuestionStats(Map<String, List<CMDSQuestion>> tmap,Long studentId){
        AMQuestionStats amQuestionStats = new AMQuestionStats();
        List<String> qids=new ArrayList<>();

        for (Map.Entry<String, List<CMDSQuestion>> entry : tmap.entrySet()) {
            for(CMDSQuestion q:entry.getValue()){
                qids.add(q.getId());
            }
        }

        List<QuestionAttempt> questionAttempts = lOAMDAO.loam_fetchQuestionAttempts(qids, studentId, new HashSet<>(), true);

        Map<String, List<QuestionAttempt>> map = questionAttempts.stream()
                .collect(Collectors.groupingBy(QuestionAttempt::getQuestionId));

        for (Map.Entry<String, List<CMDSQuestion>> entry : tmap.entrySet()) {
            for(CMDSQuestion question:entry.getValue()){

//                if(!map.containsKey(question.getId())) {
//                    continue;
//                }
                List<QuestionAttempt> qas=new ArrayList<>();
                if(map.containsKey(question.getId())){
                    qas= map.get(question.getId()).stream()
                            .filter(qa->qa.getContextId()!=null && qa.getContextId().equals(entry.getKey()))
                            .collect(Collectors.toList());
                }

                boolean attemptExists = false;
                String answerGiven = null;

                if (ArrayUtils.isNotEmpty(qas)) {

//                attemptExists = true;
//                answerGiven = qa.getAnswerGiven().get(0);
                    for (QuestionAttempt questionAttempt : qas) {
                        if (questionAttempt.getAnswerGiven()!=null && questionAttempt.getAnswerGiven().size()>0) {
                            attemptExists = true;
                            answerGiven = questionAttempt.getAnswerGiven().get(0);
                        }
                    }
                }

                if (attemptExists && answerGiven != null) {
                    if (answerGiven.equals(question.getSolutionInfo().getAnswer().get(0))) {
                        amQuestionStats.incrementNoCorrect();
                    } else {
                        amQuestionStats.incrementNoWrong();
                    }
                } else {
                    amQuestionStats.incrementNoUnattempted();
                }
            }
        }
        return  amQuestionStats;
    }

    private AMQuestionStats loam_calculateAMQuestionStats_Subjective(Map<String, List<CMDSQuestion>> testQuestionMap, List<QuestionAnalytics> resultEntriesList, Long studentId, String nodeName) {
        Map<String, List<CMDSQuestion>> map=new HashMap<>();
        if (!nodeName.equalsIgnoreCase("root")) {
            for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet()) {
                if (entry.getValue().size() > 0){
                    List<CMDSQuestion> quests = entry.getValue().stream()
//                            .filter(Objects::nonNull)
                            .filter(p -> loam_getNodesNamesFromQuestion(p).contains(nodeName))
                            .collect(Collectors.toList());
                    map.put(entry.getKey(), quests);
                }
            }
        }else{
            map=testQuestionMap;
        }
        return loam_getAMQuestionStats_Subjective(map,studentId, resultEntriesList);
    }


    private AMQuestionStats loam_getAMQuestionStats_Subjective(Map<String, List<CMDSQuestion>> tmap, Long studentId, List<QuestionAnalytics> resultEntriesList){
        AMQuestionStats amQuestionStats = new AMQuestionStats();

        // Create map of Question Id and Question Analytics
        Map<String, QuestionAnalytics> resultEntiresMap = new HashMap<>();

        for(QuestionAnalytics questionAnalytics : resultEntriesList){
            resultEntiresMap.put(questionAnalytics.getQuestionId(), questionAnalytics);
        }

        for (Map.Entry<String, List<CMDSQuestion>> entry : tmap.entrySet()) {
            for (CMDSQuestion question : entry.getValue()) {
                QuestionAnalytics questionAnalytics = resultEntiresMap.get(question.getId());
                if(questionAnalytics.getQuestionAttemptState().equals(QuestionAttemptState.NOT_ATTEMPTED)){
                    amQuestionStats.addUnatempted(questionAnalytics.getMaxMarks());
                }
                else {
                    amQuestionStats.addCorrect(questionAnalytics.getMarksGiven());
                    amQuestionStats.addMissed(questionAnalytics.getMaxMarks() - questionAnalytics.getMarksGiven());
                }
            }
        }

        return  amQuestionStats;
    }

    private List<BaseTopicTree> loam_getChildNodes(String currentNodeName, String currentNodeId) {
        if (currentNodeId == null || currentNodeName.equalsIgnoreCase("root")) {
            return topicTreeManager.getBaseTreeNodesByLevel(0);
        } else {
            return topicTreeManager.getBaseTreeNodesByParent(currentNodeId).getList();
        }
    }



    private Map<String, AMQuestionStats> loam_calculateAMDifficultyWiseStats(List<CMDSQuestion> questions, List<ContentInfo> contentInfos,
                                                                             String parentName, Long studentId, List<String> childNodeNames,String nodeName) {
        Map<String, AMQuestionStats> diffStats = new HashMap<>();

        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();

        for (ContentInfo contentInfo : contentInfos) {

            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> contentInfoquestionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> contentInfoquestionIds.contains(question.getId())).collect(Collectors.toList());

            testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);
        }

        Map<String,List<CMDSQuestion>> easyM=new HashMap<>();
        Map<String,List<CMDSQuestion>> hardM=new HashMap<>();
        Map<String,List<CMDSQuestion>> mediumM=new HashMap<>();
        Map<String,List<CMDSQuestion>> unknownM=new HashMap<>();

        for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet()) {
                List<CMDSQuestion> easyQ = new ArrayList<>();
                List<CMDSQuestion> mediumQ = new ArrayList<>();
                List<CMDSQuestion> hardQ = new ArrayList<>();
                List<CMDSQuestion> unknownQ = new ArrayList<>();

                for (String childnode : childNodeNames) {


                    for (CMDSQuestion question : entry.getValue()) {

                        if (loam_getNodesNamesFromQuestion(question).contains(childnode)) {
                            if (question.getDifficulty() == null) {
                                unknownQ.add(question);
                            } else {
                                if (question.getDifficulty().equals(Difficulty.UNKNOWN))
                                    unknownQ.add(question);
                                if (question.getDifficulty().equals(Difficulty.EASY))
                                    easyQ.add(question);
                                if (question.getDifficulty().equals(Difficulty.MODERATE))
                                    mediumQ.add(question);
                                if (question.getDifficulty().equals(Difficulty.TOUGH))
                                    hardQ.add(question);
                            }
                        }
                    }
                }
                unknownM.put(entry.getKey(), unknownQ);
                easyM.put(entry.getKey(), easyQ);
                mediumM.put(entry.getKey(), mediumQ);
                hardM.put(entry.getKey(), hardQ);

        }

        diffStats.put("easy", loam_calculateAMQuestionStats(easyM, studentId,nodeName));
        diffStats.put("medium", loam_calculateAMQuestionStats(mediumM, studentId,nodeName));
        diffStats.put("hard", loam_calculateAMQuestionStats(hardM, studentId,nodeName));
        diffStats.put("unknown", loam_calculateAMQuestionStats(unknownM, studentId,nodeName));
        return diffStats;
    }

    private Map<String, AMQuestionStats> loam_calculateAMDifficultyWiseStats_Subjective(List<CMDSQuestion> questions, List<ContentInfo> contentInfos,
                                                                             String parentName, Long studentId, List<String> childNodeNames,String nodeName) {
        Map<String, AMQuestionStats> diffStats = new HashMap<>();

        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();

        List<QuestionAnalytics> resultEntryList = new ArrayList<>();

        for (ContentInfo contentInfo : contentInfos) {

            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> contentInfoquestionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> contentInfoquestionIds.contains(question.getId())).collect(Collectors.toList());

            testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);

            resultEntryList.addAll(firstTestAttempt.getResultEntries());
        }

        Map<String,List<CMDSQuestion>> easyM=new HashMap<>();
        Map<String,List<CMDSQuestion>> hardM=new HashMap<>();
        Map<String,List<CMDSQuestion>> mediumM=new HashMap<>();
        Map<String,List<CMDSQuestion>> unknownM=new HashMap<>();

        for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet()) {
            List<CMDSQuestion> easyQ = new ArrayList<>();
            List<CMDSQuestion> mediumQ = new ArrayList<>();
            List<CMDSQuestion> hardQ = new ArrayList<>();
            List<CMDSQuestion> unknownQ = new ArrayList<>();

            for (String childnode : childNodeNames) {


                for (CMDSQuestion question : entry.getValue()) {

                    if (loam_getNodesNamesFromQuestion(question).contains(childnode)) {
                        if (question.getDifficulty() == null) {
                            unknownQ.add(question);
                        } else {
                            if (question.getDifficulty().equals(Difficulty.UNKNOWN))
                                unknownQ.add(question);
                            if (question.getDifficulty().equals(Difficulty.EASY))
                                easyQ.add(question);
                            if (question.getDifficulty().equals(Difficulty.MODERATE))
                                mediumQ.add(question);
                            if (question.getDifficulty().equals(Difficulty.TOUGH))
                                hardQ.add(question);
                        }
                    }
                }
            }
            unknownM.put(entry.getKey(), unknownQ);
            easyM.put(entry.getKey(), easyQ);
            mediumM.put(entry.getKey(), mediumQ);
            hardM.put(entry.getKey(), hardQ);

        }

        diffStats.put("easy", loam_getAMQuestionStats_Subjective(easyM, studentId, resultEntryList));
        diffStats.put("medium", loam_getAMQuestionStats_Subjective(mediumM, studentId, resultEntryList));
        diffStats.put("hard", loam_getAMQuestionStats_Subjective(hardM, studentId, resultEntryList));
        diffStats.put("unknown", loam_getAMQuestionStats_Subjective(unknownM, studentId, resultEntryList));
        return diffStats;
    }


    private Map<String, AMQuestionStats> loam_calculateAMDifficultyWiseStatsForNode_Subjective(List<CMDSQuestion> questions, List<ContentInfo> contentInfos,
                                                                                    String parentName, Long studentId, List<String> childNodeNames,String nodeName) {
        Map<String, AMQuestionStats> diffStats = new HashMap<>();

        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();

        for (ContentInfo contentInfo : contentInfos) {

            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();

            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> contentInfoquestionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> contentInfoquestionIds.contains(question.getId())).collect(Collectors.toList());

            testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);
        }

        Map<String,List<CMDSQuestion>> easyM=new HashMap<>();
        Map<String,List<CMDSQuestion>> hardM=new HashMap<>();
        Map<String,List<CMDSQuestion>> mediumM=new HashMap<>();
        Map<String,List<CMDSQuestion>> unknownM=new HashMap<>();

        for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet()) {
            List<CMDSQuestion> easyQ = new ArrayList<>();
            List<CMDSQuestion> mediumQ = new ArrayList<>();
            List<CMDSQuestion> hardQ = new ArrayList<>();
            List<CMDSQuestion> unknownQ = new ArrayList<>();


            for (CMDSQuestion question : entry.getValue()) {

                if (loam_getNodesNamesFromQuestion(question).contains(nodeName)) {
                    if (question.getDifficulty() == null) {
                        unknownQ.add(question);
                    } else {
                        if (question.getDifficulty().equals(Difficulty.UNKNOWN))
                            unknownQ.add(question);
                        if (question.getDifficulty().equals(Difficulty.EASY))
                            easyQ.add(question);
                        if (question.getDifficulty().equals(Difficulty.MODERATE))
                            mediumQ.add(question);
                        if (question.getDifficulty().equals(Difficulty.TOUGH))
                            hardQ.add(question);
                    }
                }
            }

            unknownM.put(entry.getKey(), unknownQ);
            easyM.put(entry.getKey(), easyQ);
            mediumM.put(entry.getKey(), mediumQ);
            hardM.put(entry.getKey(), hardQ);

        }

        diffStats.put("easy", loam_calculateAMQuestionStats(easyM, studentId,nodeName));
        diffStats.put("medium", loam_calculateAMQuestionStats(mediumM, studentId,nodeName));
        diffStats.put("hard", loam_calculateAMQuestionStats(hardM, studentId,nodeName));
        diffStats.put("unknown", loam_calculateAMQuestionStats(unknownM, studentId,nodeName));
        return diffStats;
    }

    private List<String> getTestQuestionIds(CMDSTest test)
    {
        List<TestMetadata> metadataList = test.getMetadata();
        List<String> questionIds = new ArrayList<>();

        for (TestMetadata testMetadata : metadataList)
        {
            List<CMDSTestQuestion> questions = testMetadata.getQuestions();
            for (CMDSTestQuestion qstn : questions) {
                questionIds.add(qstn.getQuestionId());
            }
        }

        return questionIds;
    }

    public Map<String, List<AMQuestionAttempt>> loam_getQuestionAttemptDetailsByLabel(Long studentId, Long fromTime,
                                                                                      Long thruTime, String parentName) {
        if(parentName.trim().equalsIgnoreCase("Mathematics")){
            parentName = "Maths";
        }

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudent(studentId, fromTime, thruTime, contentInfoRequiredFields, true);
        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();


        Set<String> allQuestionIds=new HashSet<>();
        for (ContentInfo contentInfo : contentInfos) {
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);
            for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                allQuestionIds.add(qa.getQuestionId());
            }

        }


        List<CMDSQuestion> questions = lOAMDAO.loam_getQuestionsByTest(new ArrayList(allQuestionIds), new HashSet<>(), false);


        for (ContentInfo contentInfo : contentInfos) {
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> questionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> questionIds.contains(question.getId())).collect(Collectors.toList());

            testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);

        }

        Map<String, List<CMDSQuestion>> map=new HashMap<>();

        List<String> qids=new ArrayList<>();
        for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet()) {

            List<CMDSQuestion> fquestions=new ArrayList<>();
            for (CMDSQuestion question : entry.getValue()) {
                if (loam_getNodesNamesFromQuestion(question).contains(parentName)) {
                    fquestions.add(question);
                    qids.add(question.getId());
                }
            }
            questionRepositoryDocParser.populateAWSUrls(fquestions);
            map.put(entry.getKey(), fquestions);
        }

        //fields to fetch
        Set<String> questionAttemptsRequiredFields = new HashSet<>();
        questionAttemptsRequiredFields.add(QuestionAttempt.Constants.CONTEXT_ID);
        questionAttemptsRequiredFields.add(QuestionAttempt.Constants.ANSWER_GIVEN);
        questionAttemptsRequiredFields.add(QuestionAttempt.Constants.QUESTION_ID);
        List<QuestionAttempt> questionAttempts = lOAMDAO.loam_fetchQuestionAttempts(qids, studentId, questionAttemptsRequiredFields, true);
        return loam_collectResultWiseQuestionsAndAttempts(map, questionAttempts);
    }

    public Map<String, List<AMQuestionAttempt>> loam_getQuestionAttemptDetailsByDifficulty(Long studentId, Long fromTime,
                                                                                           Long thruTime, String currentNodeName, String difficulty, String currentNodeId) {
        if(currentNodeName.trim().equalsIgnoreCase("Mathematics")){
            currentNodeName = "Maths";
        }
        if(difficulty.equalsIgnoreCase("HARD"))
            difficulty = "TOUGH";
        else if(difficulty.equalsIgnoreCase("MEDIUM"))
            difficulty = "MODERATE";
        else if(difficulty.equalsIgnoreCase("UNKNOWN"))
            difficulty = "UNKNOWN";

        //fields to fetch
        Set<String> contentInfoRequiredFields = new HashSet<>();
        contentInfoRequiredFields.add(ContentInfo.Constants.METADATA);
        List<ContentInfo> contentInfos = lOAMDAO.loam_getTestByStudent(studentId, fromTime, thruTime, contentInfoRequiredFields, true);


        Map<String,List<CMDSQuestion>> testQuestionMap=new HashMap<>();
        Set<String> allQuestionIds=new HashSet<>();

        for (ContentInfo contentInfo : contentInfos)
        {
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            for (QuestionAnalytics qa : firstTestAttempt.getResultEntries())
            {
                allQuestionIds.add(qa.getQuestionId());
            }

        }


        List<CMDSQuestion> questions = lOAMDAO.loam_getQuestionsByTest(new ArrayList(allQuestionIds), new HashSet<>(), false);


        for (ContentInfo contentInfo : contentInfos)
        {
            List<CMDSTestAttempt> testAttempts = ((TestContentInfoMetadata) contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt = testAttempts.get(0);

            List<String> questionIds =firstTestAttempt.getResultEntries().parallelStream().filter(qa -> qa.getQuestionId() != null).map(qa -> qa.getQuestionId())
                    .collect(Collectors.toList());

            List<CMDSQuestion> filteredQuestions = questions.parallelStream().filter(question-> questionIds.contains(question.getId())).collect(Collectors.toList());

            if(filteredQuestions !=null) {
                testQuestionMap.put(firstTestAttempt.getTestId(),filteredQuestions);
            }
        }


        Map<String, List<CMDSQuestion>> map=new HashMap<>();

        List<String> qids=new ArrayList<>();


        // Changes

        List<BaseTopicTree> childNodes = loam_getChildNodes(currentNodeName, currentNodeId);
        List<String> childNodeNames = new ArrayList<>();

        for (BaseTopicTree childNode : childNodes)
        {
            childNodeNames.add(childNode.getName());
        }


        if(currentNodeName.equalsIgnoreCase("root"))
        {

            for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet())
            {
                List<CMDSQuestion> fquestions = new ArrayList<>();
                for (String childnode : childNodeNames)
                {
                    for (CMDSQuestion question : entry.getValue())
                    {
                        //added null check
                        if (question != null && loam_getNodesNamesFromQuestion(question).contains(childnode))
                        {
                            if (question.getDifficulty() != null && difficulty.equalsIgnoreCase(question.getDifficulty().name()))
                            {
                                fquestions.add(question);
                                qids.add(question.getId());
                            }
                        }
                    }
                }
                questionRepositoryDocParser.populateAWSUrls(fquestions);
                map.put(entry.getKey(), fquestions);

            }

        }
        else
        {
            for (Map.Entry<String, List<CMDSQuestion>> entry : testQuestionMap.entrySet())
            {
                List<CMDSQuestion> fquestions=new ArrayList<>();

                for (CMDSQuestion question : entry.getValue())
                {
                    //added null check
                    if (question != null)
                    {
                        if (loam_getNodesNamesFromQuestion(question).contains(currentNodeName) && question.getDifficulty() == null && difficulty.equalsIgnoreCase(Difficulty.UNKNOWN.name()))
                        {
                            fquestions.add(question);
                            qids.add(question.getId());
                        }
                        else if (loam_getNodesNamesFromQuestion(question).contains(currentNodeName) && question.getDifficulty() != null && difficulty.equalsIgnoreCase(question.getDifficulty().name()))
                        {
                            fquestions.add(question);
                            qids.add(question.getId());
                        }
                    }
                }
                questionRepositoryDocParser.populateAWSUrls(fquestions);
                map.put(entry.getKey(), fquestions);
            }

        }

        //fields to fetch
        Set<String> questionAttemptsRequiredFields = new HashSet<>();
        questionAttemptsRequiredFields.add(QuestionAttempt.Constants.CONTEXT_ID);
        questionAttemptsRequiredFields.add(QuestionAttempt.Constants.ANSWER_GIVEN);
        questionAttemptsRequiredFields.add(QuestionAttempt.Constants.QUESTION_ID);
        List<QuestionAttempt> questionAttempts = lOAMDAO.loam_fetchQuestionAttempts(qids, studentId, questionAttemptsRequiredFields, true);
//

        return loam_collectResultWiseQuestionsAndAttempts(map, questionAttempts);
    }

    private Map<String, List<AMQuestionAttempt>> loam_collectResultWiseQuestionsAndAttempts(Map<String, List<CMDSQuestion>> tmap,
                                                                                            List<QuestionAttempt> questionAttempts) {
        Map<String, List<AMQuestionAttempt>> resultWiseQAttempt = new HashMap<>();
        List<AMQuestionAttempt> correctAttempts = new ArrayList<>();
        List<AMQuestionAttempt> wrongAttempts = new ArrayList<>();
        List<AMQuestionAttempt> unattempted = new ArrayList<>();

        Map<String, List<QuestionAttempt>> map = questionAttempts.stream()
                .collect(Collectors.groupingBy(QuestionAttempt::getQuestionId));

        for (Map.Entry<String, List<CMDSQuestion>> entry : tmap.entrySet())
        {
            for(CMDSQuestion question:entry.getValue())
            {

                List<QuestionAttempt> attempts=new ArrayList<>();

                if(map.containsKey(question.getId()))
                {
                    attempts= map.get(question.getId()).stream()
                            .filter(qa->qa.getContextId()!=null && qa.getContextId().equals(entry.getKey()))
                            .collect(Collectors.toList());
                }

                boolean attemptExists = false;
                String answerGiven = null;

                if (ArrayUtils.isNotEmpty(attempts))
                {
                    for (QuestionAttempt questionAttempt : attempts)
                    {
                        if (questionAttempt.getAnswerGiven().size()>0)
                        {
                            attemptExists = true;
                            answerGiven = questionAttempt.getAnswerGiven().get(0);
                        }
                    }
                }

                if (attemptExists && answerGiven != null)
                {
                    if (answerGiven.equals(question.getSolutionInfo().getAnswer().get(0)))
                    {
                        correctAttempts.add(new AMQuestionAttempt(question, answerGiven));
                    }
                    else
                    {
                        wrongAttempts.add(new AMQuestionAttempt(question, answerGiven));
                    }
                }
                else
                {
                    unattempted.add(new AMQuestionAttempt(question));
                }
            }
        }

        resultWiseQAttempt.put("correct", correctAttempts);
        resultWiseQAttempt.put("wrong", wrongAttempts);
        resultWiseQAttempt.put("unattempted", unattempted);
        return resultWiseQAttempt;
    }

    public Map<String, Integer> loam_updateAMTestNodeStats(long fromTime, long thruTime)
    {
        Map<String,List<ContentInfo>> cmap=new HashMap<>();

        int responseSize = 0;

        int start = 0;
        int size = 2000;


        while(true){
            List<ContentInfo> contentInfos= lOAMDAO.getAMAllContents(fromTime,thruTime, start, size);

            if(contentInfos == null || contentInfos.isEmpty()){
                break;
            }
            for (ContentInfo contentInfo:contentInfos)
            {
                TestContentInfoMetadata d = (TestContentInfoMetadata)contentInfo.getMetadata();
                CMDSTestAttempt t = d.getTestAttempts().get(0);
                if(t.getAttemptState().equals(CMDSAttemptState.EVALUATE_COMPLETE) && t.getLastUpdated() > fromTime && t.getLastUpdated() < thruTime) {
                    if (cmap.containsKey(t.getTestId())) {
                        List<ContentInfo> ci=cmap.get(t.getTestId());
                        ci.add(contentInfo);
                        cmap.put(t.getTestId(),ci);
                    } else {
                        List<ContentInfo> ci=new ArrayList<>();
                        ci.add(contentInfo);
                        cmap.put(t.getTestId(),ci);
                    }
                }
            }
            responseSize += loam_setPreTestNode(cmap);
            if(contentInfos.size() < size){
                break;
            }
            start = start + size;
        }

        Map<String, Integer> response = new HashMap<>();
        response.put("Size",responseSize);

        return response;
    }




    public int loam_cleanTestNodeStats(){
        Query query = new Query();
        return  lOAMDAO.deleteEntities(query, TestNodeStats.class);
    }

    private List<String> getQuestionIdsFromContentInfo(ContentInfo contentInfo){
        List<CMDSTestAttempt> testAttempts=((TestContentInfoMetadata)contentInfo.getMetadata()).getTestAttempts();
        CMDSTestAttempt firstTestAttempt=testAttempts.get(0);
        List<String> questionIds=new ArrayList<>();
        for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
            questionIds.add(qa.getQuestionId());

        }
        return questionIds;

    }


    private Integer loam_setPreTestNode(Map<String, List<ContentInfo>> cmap) {
        Map<String, Integer> response = new HashMap<>();
        Map<String,TestNodeStats> tmap=new HashMap<>();
        List<TestNodeStats> testNodeStatsList = new ArrayList<>();

        for (Map.Entry<String, List<ContentInfo>> eachTestList : cmap.entrySet()) {
            HashSet<String> nodeNames = new HashSet<String>();
            List<TestNodeStats> testNodeStatsList2 = new ArrayList<>();

            List<QuestionNodesCorrectenss> qnl=loam_getQuestionNodesCorrectenss(eachTestList.getValue(), nodeNames);

            List<String> nodeNameList = new ArrayList<>(nodeNames);

            testNodeStatsList2.addAll(lOAMDAO.loam_getTestNodeStatsforNodes(eachTestList.getKey(), nodeNameList));


            for(TestNodeStats testNodeStats : testNodeStatsList2){
                String key = testNodeStats.getTestId().concat(testNodeStats.getNodeName());
                testNodeStats.setStudentCount(testNodeStats.getStudentCount()+eachTestList.getValue().size());
                tmap.put(key, testNodeStats);
            }


            for(QuestionNodesCorrectenss qn:qnl){

                Set<BaseTopicTree> nodes = qn.getNodes();
                if(ArrayUtils.isNotEmpty(nodes)) {
                    for (BaseTopicTree node : nodes) {
                        TestNodeStats testNodeStats = tmap.get(eachTestList.getKey().concat(node.getName()));
                        if (testNodeStats == null) {
                            testNodeStats = new TestNodeStats();
                            testNodeStats.setNodename(node.getName());
                            testNodeStats.setTestId(eachTestList.getKey());
                            QuestionStats questionStats = new QuestionStats();
                            questionStats = loam_updateQuestionStatsBasedOnCorrectnessCode(questionStats, qn.getCorrectNess(), qn.getQuestion(), qn.getMarksGiven(), qn.getTotalMarks());
                            testNodeStats = loam_setDifficultyWiseQuestionStatsFromQuestion(questionStats, qn.getQuestion(), testNodeStats);
                            testNodeStats.addQuestionIdToQuestionIds(qn.getQuestionId());
                            //setting dtudents data in testNodeStats
                            Map<String, StudentMetaData> studentMetaDataMap = new HashMap<>();
                            StudentMetaData studentMetaData = getStudentMetadata(qn);
                            studentMetaDataMap.put(qn.getStudentId(),studentMetaData);
                            testNodeStats.setTestNodeStudentData(new TestNodeStudentData(studentMetaDataMap));
                            testNodeStats.setStudentCount((long)eachTestList.getValue().size());
                            tmap.put(eachTestList.getKey().concat(node.getName()),testNodeStats);

                        } else {
                            QuestionStats qStats = loam_getDifficultyWiseQuestionStatsFromQuestion(testNodeStats, qn.getQuestion());
                            if (qStats == null) {
                                QuestionStats questionStats = new QuestionStats();
                                questionStats = loam_updateQuestionStatsBasedOnCorrectnessCode(questionStats, qn.getCorrectNess(),
                                        qn.getQuestion(), qn.getMarksGiven(), qn.getTotalMarks());
                                testNodeStats = loam_setDifficultyWiseQuestionStatsFromQuestion(questionStats, qn.getQuestion(),
                                        testNodeStats);
                            } else {
                                qStats = loam_updateQuestionStatsBasedOnCorrectnessCode(qStats, qn.getCorrectNess(), qn.getQuestion(), qn.getMarksGiven(), qn.getTotalMarks());
                                testNodeStats = loam_setDifficultyWiseQuestionStatsFromQuestion(qStats, qn.getQuestion(), testNodeStats);
                            }
                            testNodeStats.addQuestionIdToQuestionIds(qn.getQuestionId());
                            //setting each student marks, which belongs to particular test and node
                            if (testNodeStats.getTestNodeStudentData() != null && testNodeStats.getTestNodeStudentData().getStudentMetaDataMap() != null) {
                                Map<String, StudentMetaData> studentMetaDataMap = testNodeStats.getTestNodeStudentData().getStudentMetaDataMap();
                                if (studentMetaDataMap.containsKey(qn.getStudentId())) {
                                    StudentMetaData studentMetaData = studentMetaDataMap.get(qn.getStudentId());
                                    setStudentMetadata(studentMetaData,qn);
                                    testNodeStats.getTestNodeStudentData().getStudentMetaDataMap().put(qn.getStudentId(), studentMetaData);
                                } else {
                                    StudentMetaData studentMetaData = new StudentMetaData();
                                    setStudentMetadata(studentMetaData,qn);
                                    testNodeStats.getTestNodeStudentData().getStudentMetaDataMap().put(qn.getStudentId(), studentMetaData);
                                }
                            } else {
                                Map<String,StudentMetaData> studentMetaDataMap = new HashMap<>();
                                StudentMetaData studentMetaData = new StudentMetaData();
                                setStudentMetadata(studentMetaData,qn);
                                studentMetaDataMap.put(qn.getStudentId(),studentMetaData);
                                testNodeStats.setTestNodeStudentData(new TestNodeStudentData(studentMetaDataMap));
                            }
                            tmap.put(eachTestList.getKey().concat(node.getName()),testNodeStats);
                        }
                    }
                }
            }
            testNodeStatsList.addAll(testNodeStatsList2);
        }
        loam_saveTestNodeStat(tmap, testNodeStatsList);
        return tmap.size();
    }

    private void setStudentMetadata(StudentMetaData studentMetaData, QuestionNodesCorrectenss qn) {
        CMDSQuestion question = qn.getQuestion();
        if(null == question.getDifficulty() || Difficulty.UNKNOWN.equals(question.getDifficulty())){
            studentMetaData.setMarksAchievedUnknown(studentMetaData.getMarksAchievedUnknown() + qn.getMarksGiven());
        }else if (question.getDifficulty().equals(Difficulty.EASY)) {
            studentMetaData.setMarksAchievedEasy(studentMetaData.getMarksAchievedEasy() + qn.getMarksGiven());
        } else if (question.getDifficulty().equals(Difficulty.MODERATE)) {
            studentMetaData.setMarksAchievedModerate(studentMetaData.getMarksAchievedModerate() + qn.getMarksGiven());
        } else if (question.getDifficulty().equals(Difficulty.TOUGH)) {
            studentMetaData.setMarksAchievedTough(studentMetaData.getMarksAchievedTough() + qn.getMarksGiven());
        }
    }

    /**
     * gets studentMetadata Object
     * @param qn
     * @return
     */
    private StudentMetaData getStudentMetadata(QuestionNodesCorrectenss qn) {
        StudentMetaData studentMetaData = new StudentMetaData();
        CMDSQuestion question = qn.getQuestion();
        if(null == question.getDifficulty() || Difficulty.UNKNOWN.equals(question.getDifficulty())){
            studentMetaData.setMarksAchievedUnknown(Double.valueOf(qn.getMarksGiven()));
        }else if (question.getDifficulty().equals(Difficulty.EASY)) {
            studentMetaData.setMarksAchievedEasy(Double.valueOf(qn.getMarksGiven()));
        } else if (question.getDifficulty().equals(Difficulty.MODERATE)) {
            studentMetaData.setMarksAchievedModerate(Double.valueOf(qn.getMarksGiven()));
        } else if (question.getDifficulty().equals(Difficulty.TOUGH)) {
            studentMetaData.setMarksAchievedTough(Double.valueOf(qn.getMarksGiven()));
        }
        return studentMetaData;
    }

    private void loam_saveTestNodeStat(Map<String,TestNodeStats> tmap , List<TestNodeStats> testNodeStatsList) {

        // delete test node stats

        List<String> ids = new ArrayList<>();
        for(TestNodeStats testNodeStats : testNodeStatsList){
            ids.add(testNodeStats.getId());
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").in(ids));
        logger.info("Deleted Entries : " );
        int num =  lOAMDAO.deleteEntities(query, TestNodeStats.class);
        logger.info("Deleted Entries : " + num);

        List<TestNodeStats> tl=new ArrayList<>();
        for(Map.Entry<String,TestNodeStats> eachTestNode : tmap.entrySet()) {
            tl.add(eachTestNode.getValue());
        }
        lOAMDAO.loam_insertMany(tl);

    }

    private List<QuestionNodesCorrectenss> loam_getQuestionNodesCorrectenss(List<ContentInfo> contentInfos, HashSet<String> nodeNames){
        List<QuestionNodesCorrectenss> questionNodesCorrectenssList=new ArrayList<>();
        int count =1;
        List<CMDSQuestion> questions = new ArrayList<>();
        //iterating contentInfo
        for (ContentInfo contentInfo : contentInfos) {
            //get the testAttempts from metadata
            List<CMDSTestAttempt> testAttempts=((TestContentInfoMetadata)contentInfo.getMetadata()).getTestAttempts();
            CMDSTestAttempt firstTestAttempt=testAttempts.get(0);
            List<String> questionIds=new ArrayList<>();
            List<QuestionNodesCorrectenss> qnl=new ArrayList<>();
            for (QuestionAnalytics qa : firstTestAttempt.getResultEntries()) {
                QuestionNodesCorrectenss qn=new QuestionNodesCorrectenss();
                if(count==1){
                    questionIds.add(qa.getQuestionId());
                }
                qn.setQuestionId(qa.getQuestionId());
                if(qa.getCorrectness().equals(EnumBasket.Correctness.CORRECT)){
                    qn.setCorrectNess(1);
                }else if(qa.getCorrectness().equals(EnumBasket.Correctness.INCORRECT)){
                    qn.setCorrectNess(0);
                }
                else if(qa.getCorrectness().equals(EnumBasket.Correctness.UNJUDGEABLE)){
                    if(qa.getQuestionAttemptState().equals(QuestionAttemptState.EVALUATED)){
                        qn.setCorrectNess(2);
                    }
                    else if(qa.getQuestionAttemptState().equals(QuestionAttemptState.NOT_ATTEMPTED)){
                        qn.setCorrectNess(-1);
                    }
                }
                else{
                    qn.setCorrectNess(-1);
                }
                qn.setStudentId(contentInfo.getStudentId());
                qn.setMarksGiven(qa.getMarksGiven());
                qn.setTotalMarks(qa.getMaxMarks());
                qnl.add(qn);
            }
            questionNodesCorrectenssList.addAll(qnl);
            if(count==1){
                questions.addAll(lOAMDAO.loam_getQuestionsByTest(questionIds, new HashSet<>(),true));
            }
            count++;
        }
        Map<String,CMDSQuestion> qidMap=new HashMap<>();
        Map<String,Set<BaseTopicTree>> qidNodeMap=new HashMap<>();
        for(CMDSQuestion q:questions){
            qidMap.put(q.getId(),q);
            Set<BaseTopicTree> nodes = loam_getNodesFromNodeNames(loam_getNodesNamesFromQuestion(q));
            qidNodeMap.put(q.getId(),nodes);
            if(nodes != null) {
                for (BaseTopicTree baseTopicTree : nodes) {
                    nodeNames.add(baseTopicTree.getName());
                }
            }
        }

        return  questionNodesCorrectenssList.stream()
                .map(qn -> new QuestionNodesCorrectenss(qn.getQuestionId(),qidMap.get(qn.getQuestionId()),qn.getCorrectNess(),qidNodeMap.get(qn.getQuestionId()),qn.getStudentId(),qn.getMarksGiven(), qn.getTotalMarks()))
                .collect(Collectors.toList());
    }


    private QuestionStats loam_updateQuestionStatsBasedOnCorrectnessCode(QuestionStats questionStats, int correctnessCode,
                                                                         CMDSQuestion question, Float marksGiven, Float totalMarks) {
        if (correctnessCode == 1) {
            questionStats.incrementCorrect();
            questionStats.addToMarksAchieved(question.getMarks().getPositive());
        } else if (correctnessCode == 0) {
            questionStats.incrementWrong();
            questionStats.subtractFromMarksAchieved(question.getMarks().getNegative());
        }else if (correctnessCode == 2){
            if(marksGiven == totalMarks){
                questionStats.addToMarksScored(marksGiven);
            }
            else{
                questionStats.addToMarksScored(marksGiven);
                questionStats.addToMarksMissed(totalMarks - marksGiven);
            }
        }
        else if (correctnessCode == -1) {
            questionStats.incrementUnattempted();
            questionStats.addToMarksUnattempted(totalMarks);
        }
        questionStats.addToTotalMarks(question.getMarks().getPositive());
        return questionStats;
    }

    private QuestionStats loam_getDifficultyWiseQuestionStatsFromQuestion(TestNodeStats testNodeStats,
                                                                          CMDSQuestion question) {
        if(question.getDifficulty()==null){
            return testNodeStats.getUnknownQuestionStats();
        }
        if (question.getDifficulty().equals(Difficulty.EASY)) {
            return testNodeStats.getEasyQuestionStats();
        } else if (question.getDifficulty().equals(Difficulty.MODERATE)) {
            return testNodeStats.getModerateQuestionStats();
        } else if (question.getDifficulty().equals(Difficulty.TOUGH)) {
            return testNodeStats.getToughQuestionStats();
        } else if (question.getDifficulty().equals(Difficulty.UNKNOWN)) {
            return testNodeStats.getUnknownQuestionStats();
        } else {
            return null;
        }
    }

    private TestNodeStats loam_setDifficultyWiseQuestionStatsFromQuestion(QuestionStats questionStats, CMDSQuestion question,
                                                                          TestNodeStats testNodeStats) {
        if(question.getDifficulty()==null || question.getDifficulty().equals(Difficulty.UNKNOWN)){
            testNodeStats.setUnknownQuestionStats(questionStats);
        }else
        if (question.getDifficulty().equals(Difficulty.EASY)) {
            testNodeStats.setEasyQuestionStats(questionStats);
        } else if (question.getDifficulty().equals(Difficulty.MODERATE)) {
            testNodeStats.setModerateQuestionStats(questionStats);
        } else if (question.getDifficulty().equals(Difficulty.TOUGH)) {
            testNodeStats.setToughQuestionStats(questionStats);
        }

        return testNodeStats;
    }

    private TestNodeStats loam_getTestNodeStats(String testId, String nodeName) {
        return lOAMDAO.loam_getTestNodeStats(testId, nodeName);
    }

    private int loam_getAttemptResultOfQuestion(CMDSQuestion question, List<QuestionAttempt> questionAttempts) {
        boolean attemptExists = false;
        String answerGiven = null;
        int correctnessCode = -1; // correct = 1, wrong = 0, unattempted = -1
        for (QuestionAttempt questionAttempt : questionAttempts) {
            if (question.getId().equals(questionAttempt.getQuestionId()) && questionAttempt.getAnswerGiven().size()>0) {
                attemptExists = true;
                answerGiven = questionAttempt.getAnswerGiven().get(0);
            }
        }
        if (attemptExists && answerGiven != null) {
            if (answerGiven.equals(question.getSolutionInfo().getAnswer().get(0))) {
                correctnessCode = 1;
            } else {
                correctnessCode = 0;
            }
        } // else is the case when attempt does not exist, in which case correctnessCode
        // remains -1 as initialized

        return correctnessCode;
    }

    private Set<String> loam_getNodesNamesFromQuestion(CMDSQuestion question) {
        Set<String> mainTags = question.getMainTags();
        Set<String> targets = question.getTargets();
        Set<String> grades = question.getGrades();
        if(ArrayUtils.isEmpty(mainTags)) {
            return new HashSet<>();
        }
        if(!ArrayUtils.isEmpty(targets)) {
            mainTags.removeAll(targets);
        }
        if(!ArrayUtils.isEmpty(grades)) {
            mainTags.removeAll(grades);
        }

		question.setMainTags(mainTags);
        return mainTags;
    }

    private Set<BaseTopicTree> loam_getNodesFromNodeNames(Set<String> nodeNames) {
        return topicTreeManager.getBaseTreeNodesByNodeNames(nodeNames);
    }
    private double loam_roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    private Float loam_roundMarks(float value) {
        DecimalFormat df = new DecimalFormat("#.#");
        return Float.parseFloat(df.format(value));
    }


    public void loam_updateAMTestNodeStatsAsync()
    {
        logger.info("Invoking UPDATE amtestnodestats async");
        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - 86400000;
        long thruTime = Instant.now().toEpochMilli();
        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_UPDATE_TEST_NODE_STATS, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_updateAMTestNodeStatsCron(long fromTime, long thruTime) {
        loam_updateAMTestNodeStats(fromTime,thruTime);
    }

    public List<ContentInfo> loam_testProjections(Long studentId, Long fromTime, Long thruTime) {
        List<ContentInfo> contentInfos= lOAMDAO.loam_testProjectionsgetAllAssignments(studentId, fromTime,thruTime);
        return contentInfos;
    }

    public List<PostClassDoubtChatMessagePojo> loam_getDoubtsConversationForStudent(GetStudentDoubtConversationReq req) throws VException {
        List<PostClassDoubtChatMessagePojo> responseList = new ArrayList<>();
        long doubtId = req.getDoubtId();
        int size = req.getSize();
        int start = req.getStart();
        List<DoubtChatMessage> doubtChatMessageList = lOAMDAO.loam_getChatMessages(doubtId, start, size);
        Set<String> userIdSet = new HashSet<>();

        for (DoubtChatMessage doubtChatMessage : doubtChatMessageList) {
            PostClassDoubtChatMessagePojo postClassDoubtChatMessagePojo = mapper.map(doubtChatMessage, PostClassDoubtChatMessagePojo.class);
            responseList.add(postClassDoubtChatMessagePojo);
            userIdSet.add(postClassDoubtChatMessagePojo.getFromId());
        }


        List<String> doubtSolverIdList = new ArrayList<>(userIdSet);
        List<DoubtSolver> userList = loam_getDoubtSolverList(doubtSolverIdList);

        HashMap<String, String> doubtSolverMap = new HashMap<>();

        for (DoubtSolver doubtSolver : userList) {
            String firstName = "";
            String lastName = "";
            if(null!= doubtSolver.getFirstName()){
                firstName = doubtSolver.getFirstName();
            }
            if(null!= doubtSolver.getLastName()){
                lastName = doubtSolver.getLastName();
            }
            String name = firstName.concat(lastName);
            doubtSolverMap.put(doubtSolver.getId(), name);
        }

        for (PostClassDoubtChatMessagePojo postClassDoubtChatMessagePojo : responseList) {
            postClassDoubtChatMessagePojo.setUserName(doubtSolverMap.get(postClassDoubtChatMessagePojo.getFromId()));
        }

        Collections.sort(responseList, new Comparator<PostClassDoubtChatMessagePojo>() {
            @Override
            public int compare(PostClassDoubtChatMessagePojo o1, PostClassDoubtChatMessagePojo o2) {
                return o1.getCreationTime().compareTo(o2.getCreationTime());
            }
        });
        return responseList;
    }

    public ConcurrentHashMap<String, List<DoubtPojo>> loam_getResolvedDoubtsForStudent(GetStudentDoubtsReq req) throws VException {
        List<Doubt> dbs = lOAMDAO.loam_getResolvedDoubtsForStudent(req.getStudentId(), req.getCurrentNodeName(), req.getFromTime(), req.getThruTime(), req.getStart(), req.getSize());
        ConcurrentHashMap<String, List<DoubtPojo>> map = new ConcurrentHashMap<>();
        List<DoubtPojo> resolved = new ArrayList<>();
        List<String> doubtSolverIdList = new ArrayList<>();
        for (Doubt db : dbs) {
            doubtSolverIdList.add(db.getDoubtSolverPojoList().get(0).getSolverId());
        }


        List<DoubtSolver> doubtSolverList = loam_getDoubtSolverList(doubtSolverIdList);


        HashMap<String, String> doubtSolverMap = new HashMap<>();

        for (DoubtSolver doubtSolver : doubtSolverList) {
            String firstName = "";
            String lastName = "";
            if(null!= doubtSolver.getFirstName()){
                firstName = doubtSolver.getFirstName();
            }
            if(null!= doubtSolver.getLastName()){
                lastName = doubtSolver.getLastName();
            }
            String name = firstName.concat(lastName);
            doubtSolverMap.put(doubtSolver.getId(), name);
        }

        for (Doubt db : dbs) {
                DoubtPojo doubtPojo = new DoubtPojo(db);
                if(null != db.getDoubtSolverPojoList() && null != db.getDoubtSolverPojoList().get(0) && doubtSolverMap.containsKey(db.getDoubtSolverPojoList().get(0).getSolverId())){
                    doubtPojo.setSolverName(doubtSolverMap.get(db.getDoubtSolverPojoList().get(0).getSolverId()));
                }
                resolved.add(doubtPojo);
            }
        map.put("Resolved",resolved);
        return  map;
    }

    private List<DoubtSolver> loam_getDoubtSolverList(List<String> doubtSolverIdList) {
        List<DoubtSolver> result = lOAMDAO.getDoubtSolverForSolverIdList(doubtSolverIdList);
        return result;
    }

    public ConcurrentHashMap<String, List<DoubtPojo>> loam_getUnResolvedgDoubtsForStudent(GetStudentDoubtsReq req) throws VException {
        List<Doubt> dbs = lOAMDAO.loam_getUnResolvedDoubtsForStudent(req.getStudentId(), req.getCurrentNodeName(), req.getFromTime(), req.getThruTime(), req.getStart(), req.getSize());
        ConcurrentHashMap<String, List<DoubtPojo>> map = new ConcurrentHashMap<>();
        List<DoubtPojo> unresolved = new ArrayList<>();


        for (Doubt db : dbs) {
            DoubtPojo doubtPojo = new DoubtPojo(db);
            unresolved.add(doubtPojo);
        }
        map.put("UnResolved",unresolved);
        return  map;
    }

    public ConcurrentHashMap<String, Long> loam_getDoubtsStatForStudent(GetStudentDoubtsReq req) throws VException {
        List<Doubt> dbs = lOAMDAO.loam_getDoubtsStatForStudent(req.getStudentId(), req.getCurrentNodeName(), req.getFromTime(), req.getThruTime());
        ConcurrentHashMap<String, Long> map = new ConcurrentHashMap<>();
        long resolved = 0;
        long unresolved = 0;
        for (Doubt db : dbs) {
            if (db.getDoubtState().equals(DoubtState.DOUBT_T1_SOLVED) || db.getDoubtState().equals(DoubtState.DOUBT_T2_SOLVED) ){
                resolved += 1;
            }else{
                unresolved += 1;
            }
        }
        map.put("Resolved",resolved);
        map.put("UnResolved",unresolved);
        return  map;
    }

    public void persistUsageStatusAsync(String data, String functionality) {
        try {

            AMUsageStatusReq req = new Gson().fromJson(data , AMUsageStatusReq.class);

            LOUsageStats usageStats = new LOUsageStats();
            usageStats.setCallingUserId(req.getCallingUserId());
            usageStats.setCallingUserRole(req.getCallingUserRole());
            usageStats.setStudentId(req.getStudentId());
            usageStats.setFunctionality(functionality);
            usageStats.setTimestamp(Instant.now().toEpochMilli());

            Map<String, Object> payload = new HashMap<>();
            payload.put("data",  new Gson().toJson(usageStats));

            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_RECORD_USAGE_STATS, payload);
            asyncTaskFactory.loam_executeTaskWithoutDelay(params);

        }catch (Exception e){
            logger.error("Error while persisting LOUsageStatus ", e);
        }
    }

    public void persistUsageStatus(String data ) {
        try {

            LOUsageStats usageStats =  new Gson().fromJson(data , LOUsageStats.class);
            lOAMDAO.loam_persistUsageStatus(usageStats);
            logger.info("persisting LOUsageStatus : {}", usageStats.toString());

        }catch (Exception e){
            logger.error("Error while persisting LOUsageStatus ", e);
        }
    }

    public Map<String, Integer> loam_getDashboardUsage(Long fromTime, Long thruTime) {
        
    	logger.info("===========loam_getDashboardUsage : {} : {}", fromTime, thruTime);
    	
    	List<LOUsageStats> loUsageStatsloUsageStats = lOAMDAO.loam_getDashboardUsage(fromTime,thruTime);

        logger.info("total stats : {}", loUsageStatsloUsageStats.size());

       /* LinkedHashMap<String, Double> usageAverage = new LinkedHashMap<>();
        while(fromTime < thruTime) {

            Long nextDay = fromTime + 86400000;
            List<LOUsageStats> filteredUsageStatusPerDay = loUsageStatsloUsageStats.stream().filter(filterLOUsageStatsBasedOnDate(fromTime, nextDay)).collect(Collectors.toList());

           // double dayAverage = filteredUsageStatusPerDay.size() /loUsageStatsloUsageStats.size();

            Date date = new Date(fromTime);
            DateFormat df = new SimpleDateFormat("dd:MM:yy");

            logger.info("day record size: {} ", filteredUsageStatusPerDay.size());
            logger.info("avg {}  on {}", dayAverage, df.format(date));

            usageAverage.put(df.format(date),filteredUsageStatusPerDay.size());

            fromTime = nextDay;
        }
        logger.info("final record: {} ", usageAverage);*/

        Map<String, Integer> payload = new HashMap<>();
        payload.put("count" ,loUsageStatsloUsageStats.size());
        return payload;
    }

    public AMStudentInsightsRes loam_getStudentsInsights(AMStudentInsightsReq req) throws VException {

        Map<Long, InsightResponseData> insightResponseDataMap = new HashMap<>();
        //gets the student who are mentored by acadMentorEMail
        List<User> users = loam_getStudentsByAcadMentorByEmail(req);

        for(User user: users){
            List<String> insightList = req.getInsightList();
            Map<String, InsightScore> map = new HashMap<>();
            Long currentTime = System.currentTimeMillis();
            if(insightList.contains("test")){
                AMTestWiseStatsRes amTestWiseStatsRes = loam_getTestWiseStatsInsights(new AMQuestionAttemptStatsReq(user.getId(),FROM_TIME,currentTime,null,null,"root"));
                map.put("test",new InsightScore( (Double) amTestWiseStatsRes.getInsights().get("insight_marks") , amTestWiseStatsRes.getInsights().get("insight_grade").toString()));
            }
            if(insightList.contains("testByNode") || insightList.contains("testByDifficulty")){
                AMQuestionAttemptsStatsRes amQuestionAttemptsStatsRes = loam_getQuestionAttemptStatsInsights(user.getId(), "root", null, FROM_TIME, currentTime);
                map.put("testByNode",new InsightScore((Double) amQuestionAttemptsStatsRes.getLabelledInsights().get("insight_marks"),amQuestionAttemptsStatsRes.getLabelledInsights().get("insight_grade").toString()));
                map.put("testByDifficulty",new InsightScore((Double) amQuestionAttemptsStatsRes.getDifficultyWiseInsights().get("insight_marks"),amQuestionAttemptsStatsRes.getDifficultyWiseInsights().get("insight_grade").toString()));
               }
            if(insightList.contains("assignment")){
                AMAssignmentWiseStatsRes amAssignmentWiseStatsRes = loam_getAssignmentWiseStatsInsights(new AMQuestionAttemptStatsReq(user.getId(),FROM_TIME,currentTime,null,null,"root"));
                map.put("assignment",new InsightScore((Double)amAssignmentWiseStatsRes.getInsights().get("insight_marks"),String.valueOf(amAssignmentWiseStatsRes.getInsights().get("insight_grade"))));
                }
            insightResponseDataMap.put(user.getId(),new InsightResponseData(user.getId(),map));
        }
        return new AMStudentInsightsRes(insightResponseDataMap);
    }

    public List<User> loam_getUsersFromUserIds(List<Long> userIds, String callerType) throws VException {
        String url = USER_ENDPOINT_LOAM + "loam_getUsersByIds" + "?exposeContactInfo=" + true;
        String respString = getResponseFromAPI(url, HttpMethod.POST, new Gson().toJson(new UserByIdReq(userIds, callerType)));
        return new ArrayList(Arrays.asList(new Gson().fromJson(respString, User[].class)));
    }

    private String getResponseFromAPI(String url, HttpMethod method, String query) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, method, query, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    public List<User> getStudentsByAcadMentor(String acadMentorEMail) throws VException
    {
        LOAMStudentsReq loamStudentsReq = new LOAMStudentsReq();
        loamStudentsReq.setAcadMentorEmail(acadMentorEMail);
        loamStudentsReq.setCalledFrom("LOAM");

        List<Long> userIds = getStudentIdListFromSubscription(loamStudentsReq);

        return loam_getUsersFromUserIds(userIds, "mentor");
    }

    public List<User> loam_getStudentsByAcadMentorByEmail(AMStudentInsightsReq amStudentInsightsReq) throws VException {
        LOAMStudentsReq loamStudentsReq = new LOAMStudentsReq();
        loamStudentsReq.setAcadMentorEmail(amStudentInsightsReq.getAmEmail());
        loamStudentsReq.setIpAddress(amStudentInsightsReq.getIpAddress());
        List<Long> userIds = getStudentIdListFromSubscription(loamStudentsReq);
        return loam_getUsersFromUserIds(userIds, "mentor");
    }

    public List<Long> getStudentIdListFromSubscription(LOAMStudentsReq loamStudentsReq) throws VException {
        String urlSubscription = SUBSCRIBTION_ENDPOINT_STUDENTS  + "loam_getStudents";
        String respString = getResponseFromAPI(urlSubscription, HttpMethod.POST, new Gson().toJson(loamStudentsReq));
        Type listType = new TypeToken<List<Long>>() {
        }.getType();
        List<Long> userIds = new Gson().fromJson(respString, listType);
        return userIds;
    }



    private void setQueryMaxTimeout(Query query) {
        if (query != null) {
            query.maxTimeMsec(86400000l);
        }
    }

    public String loam_getContentInfoDataFromLMS(Long fromTime, Long thruTime) throws VException
    {

        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getContentInfoData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<ContentInfo>>() {}.getType();

            List<ContentInfo> contentInfos = new Gson().fromJson(respString, listType);

            if(contentInfos == null || contentInfos.isEmpty()){
                break;
            }

            start = start + size;
            // Update new contentInfo
            lOAMDAO.loam_updateContentInfo(contentInfos);

            if(contentInfos.size() < size){
                break;
            }
        }
        return "Content Info updated";
    }

    public String loam_getCMDSTestDataFromLMS(Long fromTime, Long thruTime) throws VException {
        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getCMDSTestData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;


        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<CMDSTest>>() {}.getType();
            List<CMDSTest> cmdsTests = new Gson().fromJson(respString, listType);

            if(cmdsTests == null || cmdsTests.isEmpty()){
                break;
            }
            start = start + size;
            //Update New CMDS Test
            lOAMDAO.loam_updateCMDSTests(cmdsTests);

            if(cmdsTests.size() < size){
                break;
            }
        }

        return "CMDS Test updated";

    }

    public String loam_getCMDSQuestionDataFromLMS(Long fromTime, Long thruTime) throws VException {
        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getCMDSQuestionData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<CMDSQuestion>>() {}.getType();
            List<CMDSQuestion> cmdsQuestions = new Gson().fromJson(respString, listType);


            if(cmdsQuestions == null || cmdsQuestions.isEmpty()){
                break;
            }

            start = start + size;
            // Update new CMDSQuestion
            lOAMDAO.loam_updateCMDSQuestion(cmdsQuestions);

            if(cmdsQuestions.size() < size){
                break;
            }
        }



        return "CMDS Question updated";
    }

    public String loam_getQuestionAttemptDataFromLMS(Long fromTime, Long thruTime) throws VException {
        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getQuestionAttemptData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);

        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<QuestionAttempt>>() {}.getType();
            List<QuestionAttempt> questionAttempts = new Gson().fromJson(respString, listType);

            if(questionAttempts == null || questionAttempts.isEmpty()){
                break;
            }
            start = start + size;
            // Update Question Attempts.
            lOAMDAO.loam_updateQuestionAttempt(questionAttempts);

            if(questionAttempts.size() < size){
                break;
            }
        }


        return "Question Attempts updated";
    }

    public String loam_getDoubtDataFromLMS(Long fromTime, Long thruTime) throws VException
    {
        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getDoubtData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);

        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<Doubt>>() {}.getType();
            List<Doubt> doubts = new Gson().fromJson(respString, listType);

            if(doubts == null || doubts.isEmpty()){
                break;
            }
            start = start + size;

            // Update Doubt Data
            lOAMDAO.loam_updateDoubt(doubts);

            if(doubts.size() < size){
                break;
            }
        }

        return "Doubts updated";

    }

    public String loam_getDoubtChatMessageDataFromLMS(Long fromTime, Long thruTime) throws VException {
        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getDoubtChatMessageData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);

        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<DoubtChatMessage>>() {}.getType();
            List<DoubtChatMessage> doubtChatMessages = new Gson().fromJson(respString, listType);

            if(doubtChatMessages == null || doubtChatMessages.isEmpty()){
                break;
            }
            start = start + size;

            // Update Doubt Chat Message.
            lOAMDAO.loam_updateDoubtChatMessage(doubtChatMessages);

            if(doubtChatMessages.size() < size){
                break;
            }
        }

        return "Doubt Chat Message updated";

    }

    public void loam_getContentInfoDataFromLMSAsync()
    {
        logger.info("Invoking  loam_updateContentInfoDataFromLMSAsync async");
        Map<String, Object> payload = new HashMap<>();

        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_CONTENT_INFO_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getCMDSTestDataFromLMSAsync()
    {
        logger.info("Invoking  loam_updateContentInfoDataFromLMSAsync async");

        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_CMDS_TEST_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }


    public void loam_getCMDSQuestionDataFromLMSAsync()
    {
        logger.info("Invoking  loam_getCMDSQuestionDataFromLMSAsync async");

        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_CMDS_QUESTION_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getBaseTopicTreeDataFromLMSAsync()
    {
        logger.info("Invoking  loam_getBaseTopicTreeDataFromLMSAsync async");

        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_BASE_TOPIC_TREE_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getQuestionAttemptDataFromLMSAsync()
    {
        logger.info("Invoking  loam_getQuestionAttemptDataFromLMSAsync async");
        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();
        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_QUESTION_ATTEMPT_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }


    public void loam_getDoubtDataFromLMSAsync() {
        logger.info("Invoking  loam_getDoubtDataFromLMSAsync async");
        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();
        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_DOUBT_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getDoubtChatMessageDataFromLMSAsync() {
        logger.info("Invoking  loam_getDoubtChatMessageDataFromLMSAsync async");
        Map<String, Object> payload = new HashMap<>();
        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();
        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_DOUBT_CHAT_MESSAGE_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public List<User> loam_getAcadmentors() throws VException {
        String url = USER_ENDPOINT_LOAM + "loam_getAcadMentorUsers";
        String respString = getResponseFromAPI(url, HttpMethod.GET,null);
        return new ArrayList(Arrays.asList(new Gson().fromJson(respString, User[].class)));
    }

    public List<SuperMentorSearchResponse> loam_getStudentsForSearch() throws VException {
        String url = USER_ENDPOINT_LOAM + "loam_getAcadMentorUsers";
        String respString = getResponseFromAPI(url, HttpMethod.GET,null);
        List<User> acadMentorUserList = new ArrayList(Arrays.asList(new Gson().fromJson(respString, User[].class)));
        List<SuperMentorSearchResponse> response = new ArrayList<>();
        for(User acadmentorUser: acadMentorUserList){
            List<User> studentUserList = getStudentsByAcadMentor(acadmentorUser.getEmail());
            TeacherSupermentorPojo teacherSupermentorPojo = mapper.map(acadmentorUser, TeacherSupermentorPojo.class);
            for(User studentUser: studentUserList){
                StudentSupermentorPojo studentSupermentorPojo = mapper.map(studentUser, StudentSupermentorPojo.class);
                response.add(new SuperMentorSearchResponse(studentSupermentorPojo,teacherSupermentorPojo));
            }
        }
        return response;
    }

    public Object loam_updateContentInfoSlimVersion(Long fromTime, Long thruTime) {
        // Get data from content info old
        int start = 0;
        int size = 1000;

        Set<String> contentInfoRequiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(contentInfoRequiredFields);
        loam_addRequiredFieldsForContentInfo(contentInfoRequiredFields);

        while(true){
            List<ContentInfo_Old> contentInfos = lOAMDAO.getContentInfoSlimVersionData(fromTime, thruTime, start, size, contentInfoRequiredFields, true);

            if(contentInfos.isEmpty()){
                break;
            }

            // Update data in new table
            loam_updateContentInfoSlimVersionAsync(contentInfos);

            if(contentInfos.size() < size){
                break;
            }

            start = start + size;

        }
        return "Updated Content Info";
    }

    public Object loam_updateCMDSTestSlimVersion(Long fromTime, Long thruTime) {
        // Get data from content info old
        int start = 0;
        int size = 1000;

        Set<String> cmdsTestRequiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(cmdsTestRequiredFields);
        loam_addAbstractCMDSEntityFields(cmdsTestRequiredFields);
        loam_addRequiredFieldsForCMDSTest(cmdsTestRequiredFields);

        while(true){
            List<CMDSTest_Old> cmdsTests = lOAMDAO.getCMDSTestSlimVersionData(fromTime, thruTime, start, size, cmdsTestRequiredFields, true);

            if(cmdsTests.isEmpty()){
                break;
            }

            // Update data in new table
            loam_updateCMDSTestSlimVersionAsync(cmdsTests);

            if(cmdsTests.size() < size){
                break;
            }

            start = start + size;

        }
        return "Updated CMDS Test";
    }


    public Object loam_updateCMDSQuestionSlimVersion(Long fromTime, Long thruTime) {
        // Get data from content info old
        int start = 0;
        int size = 1000;

        Set<String> cmdsQuestionRequiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(cmdsQuestionRequiredFields);
        loam_addRequiredFieldsForCMDSQuestion(cmdsQuestionRequiredFields);

        while(true){
            List<CMDSQuestion_Old> cmdsQuestions = lOAMDAO.getCMDSQuestionSlimVersionData(fromTime, thruTime, start, size, cmdsQuestionRequiredFields, true);

            if(cmdsQuestions.isEmpty()){
                break;
            }

            // Update data in new table
            loam_updateCMDSQuestionSlimVersionAsync(cmdsQuestions);

            if(cmdsQuestions.size() < size){
                break;
            }

            start = start + size;

        }
        return "Updated CMDSQuestion";
    }


    private void loam_updateContentInfoSlimVersionAsync(List<ContentInfo_Old> contentInfos) {
        logger.info("Invoking  loam_updateContentInfoSlimVersionAsync async");
        Map<String, Object> payload = new HashMap<>();
        payload.put("contentInfos", contentInfos);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_UPDATE_CONTENT_INFO_SLIM_VERSION, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    private void loam_updateCMDSTestSlimVersionAsync(List<CMDSTest_Old> cmdsTests) {
        logger.info("Invoking  loam_updateCMDSTestSlimVersionAsync async");
        Map<String, Object> payload = new HashMap<>();
        payload.put("cmdsTests", cmdsTests);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_UPDATE_CMDS_TEST_SLIM_VERSION, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    private void loam_updateCMDSQuestionSlimVersionAsync(List<CMDSQuestion_Old> cmdsQuestions) {
        logger.info("Invoking  loam_updateCMDSQuestionSlimVersionAsync async");
        Map<String, Object> payload = new HashMap<>();
        payload.put("cmdsQuestions", cmdsQuestions);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_UPDATE_CMDS_QUESTION_SLIM_VERSION, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }


    public void insertContentInfoSlimVersion(List<ContentInfo> contentInfos) {
        lOAMDAO.loam_insertManyContentInfos(contentInfos);
    }

    public void insertCMDSTestSlimVersion(List<CMDSTest> cmdsTests) {
        lOAMDAO.loam_insertManyCMDSTest(cmdsTests);
    }

    public void insertCMDSQuestionSlimVersion(List<CMDSQuestion> cmdsQuestions) {
        lOAMDAO.loam_insertManyCMDSQuestion(cmdsQuestions);
    }

    public void insertDoubtSlimVersion(List<Doubt> doubts) {
        lOAMDAO.loam_insertManyDoubts(doubts);
    }

    public void insertDoubtChatMessageSlimVersion(List<DoubtChatMessage> doubtChatMessages) {
        lOAMDAO.loam_insertManyDoubtChatMessages(doubtChatMessages);
    }

    private void loam_addRequiredFieldsForContentInfo(Set<String> requiredFields) {
        requiredFields.add(ContentInfo.Constants.BOARD_ID);
        requiredFields.add(ContentInfo.Constants.BOARD_IDS);
        requiredFields.add(ContentInfo.Constants.EVALUATED_TIME);
        requiredFields.add(ContentInfo.Constants.CONTENT_INFO_TYPE);
        requiredFields.add(ContentInfo.Constants.CONTENT_STATE);
        requiredFields.add(ContentInfo.Constants.METADATA);
        requiredFields.add(ContentInfo.Constants.CONTENT_TYPE);
        requiredFields.add(ContentInfo.Constants.ATTEMPTED_TIME);
        requiredFields.add(ContentInfo.Constants.TEST_ID);
        requiredFields.add(ContentInfo.Constants.STUDENT_ID);
    }

    private void loam_addAbstractTargetTopicLongIdEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractTargetTopicLongIdEntity.Constants.MAIN_TAGS);
    }

    private void loam_addRequiredFieldsForCMDSTest(Set<String> requiredFields) {
        requiredFields.add(CMDSTest.Constants.TEST_STATE);
        requiredFields.add(CMDSTest.Constants.DURATION);
        requiredFields.add(CMDSTest.Constants.CONTENT_INFO_TYPE);
        requiredFields.add(CMDSTest.Constants.METADATA);
        requiredFields.add(CMDSTest.Constants.MAX_START_TIME);
        requiredFields.add(CMDSTest.Constants.MIN_START_TIME);
        requiredFields.add(CMDSTest.Constants.QUS_COUNT);
    }

    private void loam_addRequiredFieldsForCMDSQuestion(Set<String> requiredFields) {
        requiredFields.add(CMDSQuestion.Constants._ID);
        requiredFields.add(CMDSQuestion.Constants.MAIN_TAGS);
        requiredFields.add(CMDSQuestion.Constants.TARGETS);
        requiredFields.add(CMDSQuestion.Constants.GRADES);
        requiredFields.add(CMDSQuestion.Constants.SOLUTION_INFO);
        requiredFields.add(CMDSQuestion.Constants.DIFFICULTY);
        requiredFields.add(CMDSQuestion.Constants.MARKS);
        requiredFields.add(CMDSQuestion.Constants.QUESTION_BODY);
        requiredFields.add(CMDSQuestion.Constants.TYPE);
    }

    private void loam_addAbstractMongoEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractMongoEntity.Constants._ID);
        requiredFields.add(AbstractMongoEntity.Constants.CREATION_TIME);
        requiredFields.add(AbstractMongoEntity.Constants.CREATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED);
        requiredFields.add(AbstractMongoEntity.Constants.ID);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.ENTITY_STATE);
    }

    private void loam_addAbstractCMDSEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractCMDSEntity.Constants.NAME);
        requiredFields.add(AbstractCMDSEntity.Constants.RECORD_STATE);
        requiredFields.add(AbstractCMDSEntity.Constants.BOARD_IDS);
        requiredFields.add(AbstractCMDSEntity.Constants.GRADES);
        requiredFields.add(AbstractCMDSEntity.Constants.SUBJECT);
        requiredFields.add(AbstractCMDSEntity.Constants.TAGS);
        requiredFields.add(AbstractCMDSEntity.Constants.TARGET_IDS);
        requiredFields.add(AbstractCMDSEntity.Constants.TARGETS);
        requiredFields.add(AbstractCMDSEntity.Constants.GRADES_SLUGS);
        requiredFields.add(AbstractCMDSEntity.Constants.SUBJECT_SLUG);
        requiredFields.add(AbstractCMDSEntity.Constants.TAGS_SLUGS);
        requiredFields.add(AbstractCMDSEntity.Constants.TOPICS);
        requiredFields.add(AbstractCMDSEntity.Constants.TOPICS_SLUGS);
        requiredFields.add(AbstractCMDSEntity.Constants.TARGETS_SLUGS);

    }

    public void deleteTestNodeStats(String testId) {
        lOAMDAO.deleteTestNodeStats(testId);
    }

    //Next time, push this code into: subscriptionManager.
    public String loam_getBatchDataFromSubscription(Long fromTime, Long thruTime) throws VException
    {
        String lmsUrl = SUBSCRIBTION_ENDPOINT_STUDENTS + "loam_getBatchData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true)
        {
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<Batch>>() {}.getType();

            List<Batch> batches = new Gson().fromJson(respString, listType);

            if(batches == null || batches.isEmpty())
            {
                break;
            }

            start = start + size;
            // Update new contentInfo
            lOAMDAO.loam_updateBatch(batches);

            if(batches.size() < size)
            {
                break;
            }
        }
        return "batch updated";
    }

    public String loam_getAMDataFromUser(Long fromTime, Long thruTime) throws VException {
        String userUrl = USER_ENDPOINT_LOAM + "loam_getDoubtResolverData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        userUrl = userUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true)
        {
            String timeParams = "&start=" + start + "&size=" + size;
            String url = userUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<User>>() {}.getType();

            List<User> users = new Gson().fromJson(respString, listType);

            List<DoubtSolver> doubtSolverList = new ArrayList<>();
            for (User user : users) {
                DoubtSolver doubtSolver = mapper.map(user, DoubtSolver.class);
                doubtSolverList.add(doubtSolver);
            }

            if(users == null || users.isEmpty())
            {
                break;
            }

            start = start + size;
            // Update new contentInfo
            lOAMDAO.loam_updateAMData(doubtSolverList);

            if(users.size() < size)
            {
                break;
            }
        }
        return "User updated";
    }

    public String loam_getCourseDataFromSubscription(Long fromTime, Long thruTime) throws VException
    {
        String lmsUrl = SUBSCRIBTION_ENDPOINT_STUDENTS + "loam_getCourseData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<Course>>() {}.getType();

            List<Course> courses = new Gson().fromJson(respString, listType);

            if(courses == null || courses.isEmpty()){
                break;
            }

            start = start + size;
            // Update new contentInfo
            lOAMDAO.loam_updateCourse(courses);

            if(courses.size() < size){
                break;
            }
        }
        return "course updated";
    }

    public String loam_getEnrollmentDataFromSubscription(Long fromTime, Long thruTime) throws VException
    {
        String lmsUrl = SUBSCRIBTION_ENDPOINT_STUDENTS + "loam_getEnrollmentData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true)
        {
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<Enrollment>>() {}.getType();

            List<Enrollment> enrollments = new Gson().fromJson(respString, listType);

            if(enrollments == null || enrollments.isEmpty())
            {
                break;
            }

            start = start + size;
            // Update new contentInfo
            lOAMDAO.loam_updateEnrollment(enrollments);

            if(enrollments.size() < size)
            {
                break;
            }
        }
        return "Enrollment updated";
    }

    public String loam_getBoardDataFromPlatform(Long fromTime, Long thruTime) throws VException {
        String lmsUrl = PLATFORM_ENDPOINT_LOAM + "loam_getBoardData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<Board>>() {}.getType();

            List<Board> boards = new Gson().fromJson(respString, listType);

            if(boards == null || boards.isEmpty()){
                break;
            }

            start = start + size;
            // Update new contentInfo
            lOAMDAO.loam_updateBoard(boards);

            if(boards.size() < size){
                break;
            }
        }
        return "board updated";
    }

    public void loam_getBatchDataFromSubscriptionAsync()
    {
        logger.info("Invoking  loam_getBatchDataFromSubscriptionAsync async");
        Map<String, Object> payload = new HashMap<>();

        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_BATCH_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getCourseDataFromSubscriptionAsync()
    {
        logger.info("Invoking  loam_getCourseDataFromSubscriptionAsync async");
        Map<String, Object> payload = new HashMap<>();

        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_COURSE_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getBoardDataFromPlatformAsync() {
        logger.info("Invoking  loam_getEnrollmentDataFromSubscriptionAsync async");
        Map<String, Object> payload = new HashMap<>();

        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_BOARD_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public void loam_getEnrollmentDataFromSubscriptionAsync()
    {
        logger.info("Invoking  loam_getEnrollmentDataFromSubscriptionAsync async");
        Map<String, Object> payload = new HashMap<>();

        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_ENROLLMENT_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }


    public void loam_getAMDataFromUserAsync() {
        logger.info("Invoking  loam_getAMDataFromUserAsync async");
        Map<String, Object> payload = new HashMap<>();

        long fromTime = Instant.now().toEpochMilli() - ONE_DAY_IN_MILLIS;
        long thruTime = Instant.now().toEpochMilli();

        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);

        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_GET_AM_DATA, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }


    public void loam_updateAMTestNodeStatsAsync(Long fromTime, Long thruTime) {
        logger.info("Invoking UPDATE amtestnodestats async");
        Map<String, Object> payload = new HashMap<>();
        payload.put("fromTime", fromTime);
        payload.put("thruTime", thruTime);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LOAM_UPDATE_TEST_NODE_STATS, payload);
        asyncTaskFactory.loam_executeTaskWithoutDelay(params);
    }

    public List<ViewAttemptedTestRes> loam_getViewAttemptedTests(Long studentId, String currentNodeName, Long fromTime, Long thruTime, Integer start, Integer size, String testType) throws BadRequestException {

        if (studentId == null ) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : studentid - "
                    + studentId);
        }

        // return all the subjective tests taken by this student.

        // get all the question ids asked in all each test and get all the question

        // get main tags of all those questions and create a map of testIds and Set of main tags.

        // create a Set of testIds where Main tags contain the nodename.

        // Get the required tests and send response. Change the name of subject.

        List<ViewAttemptedTestRes> response = new ArrayList<>();


        Map<Long, Board> boardMap = new HashMap<>();
        List<String> testIds = new ArrayList<>();

        List<ContentInfo> contentInfoList = new ArrayList<>();
        Set<String> requiredContentInfoFields = new HashSet<>();
        loam_addProjectionContentInfoForViewAttemptedtest(requiredContentInfoFields);
        contentInfoList = lOAMDAO.getTestAttemptsList(studentId, fromTime, thruTime, start, size, testType);


        if(!currentNodeName.equalsIgnoreCase("root")){
            Map<String, List<String>> testQuestionIdMap = new HashMap<>();

            List<String> allQuestionIds = new ArrayList<>();
            for(ContentInfo contentInfo : contentInfoList){
                List<String> questionIds = new ArrayList<>();
                List<QuestionAnalytics> questionAnalyticsList = contentInfo.getMetadata().getTestAttempts().get(0).getResultEntries();
                for(QuestionAnalytics questionAnalytics : questionAnalyticsList){
                    questionIds.add(questionAnalytics.getQuestionId());
                }
                allQuestionIds.addAll(questionIds);
                testQuestionIdMap.put(contentInfo.getMetadata().getTestId(), questionIds);
            }

            // Get Main tags for all question for question ids.
            Set<String> requiredQuestionFields = new HashSet<>();
            loam_addProjectionCMDSQuestionForViewAttemptedtest(requiredQuestionFields);
            List<CMDSQuestion> cmdsQuestionList = lOAMDAO.loam_getQuestionsByTest(allQuestionIds, requiredQuestionFields, true);

            // Uncomment logs in Dev environment.
           /* logger.info("Questions List :" + cmdsQuestionList);*/
            // create a map of question Id and Questions
            Map<String, CMDSQuestion> questionMap = new HashMap<>();

            for(CMDSQuestion cmdsQuestion : cmdsQuestionList){
                questionMap.put(cmdsQuestion.getId(), cmdsQuestion);
            }

            // Iterate testQuestionIdMap
            for(Map.Entry<String, List<String>> entry : testQuestionIdMap.entrySet()){
                List<String> questionIds = entry.getValue();
                for(String questionId : questionIds){
                    CMDSQuestion question = questionMap.get(questionId);
                    if(null != question && loam_getNodesNamesFromQuestion(question).contains(currentNodeName)){
                        testIds.add(entry.getKey());
                    }
                }
            }

            List<CMDSTest> testList = lOAMDAO.loam_getTestListByTestIds(testIds);

            Map<String,TestNodeStats> testToTestNodeData = lOAMDAO.loam_getTestNodeStatsforNodes(testIds,currentNodeName);

            Map<String, CMDSTest> map = new HashMap<>();
            for(CMDSTest test : testList){
                map.put(test.getId(), test);
            }

            logger.info("gets testData for specific test-node of user");
            // Convert contentInfo list to response format.
            for(ContentInfo contentInfo : contentInfoList) {
                if(testIds.contains(contentInfo.getMetadata().getTestId())){
                    ViewAttemptedTestRes res = new ViewAttemptedTestRes();
                    res.setAttemptedTime(contentInfo.getMetadata().getTestAttempts().get(0).getStartTime());
                    res.setEvaluated(contentInfo.getContentState().equals(ContentState.EVALUATED) ? true : false);
                    res.setSubject(currentNodeName);
                    res.setEvaluatedTime(contentInfo.getMetadata().getTestAttempts().get(0).getEvaluatedTime());
                    if(ContentInfoType.OBJECTIVE.toString().equals(testType)){
                        if(contentInfo.getMetadata().getTestAttempts() != null) {
                            res.setMarksAchieved(contentInfo.getMetadata().getTestAttempts().get(0).getMarksAcheived());
                            res.setTotalMarks(contentInfo.getMetadata().getTestAttempts().get(0).getTotalMarks());
                            res.setCorrect(contentInfo.getMetadata().getTestAttempts().get(0).getCorrect());
                            res.setIncorrect(contentInfo.getMetadata().getTestAttempts().get(0).getIncorrect());
                            res.setUnattempted(contentInfo.getMetadata().getTestAttempts().get(0).getUnattempted());
                            res.setTestAverage(getTestAverage(testToTestNodeData.get(contentInfo.getMetadata().getTestId())));
                        }
                    }
                    String testId = contentInfo.getMetadata().getTestId();
                    if (map.containsKey(testId)) {
                        res.setTestTitle(map.get(testId).getName());
                    }
                    res.setContentInfoId(contentInfo.getId());
                    res.setAttemptId(contentInfo.getMetadata().getTestAttempts().get(0).getId());
                    response.add(res);
                }
            }
            return response;
        }
        else{
            Set<String> testIdSet = new HashSet<>();
            List<Long> boardIds = new ArrayList<>();

            for(ContentInfo contentInfo : contentInfoList){
                testIdSet.add(contentInfo.getMetadata().getTestId());
                boardIds.add(contentInfo.getBoardId());
            }
            boardMap = lOAMDAO.getBoardInfoMap(boardIds);

            List<String> testIdList = new ArrayList<>(testIdSet);
            List<CMDSTest> testList = lOAMDAO.loam_getTestListByTestIds(testIdList);

            Map<String, CMDSTest> map = new HashMap<>();
            Map<String,List<String>> testIdToNodeNames = new HashMap<>();
            for(CMDSTest test : testList){
                map.put(test.getId(), test);
                testIdToNodeNames.put(test.getId(),getNodeName(test));
            }
            logger.info("gets testData for every test-node of user");
            // Convert contentInfo list to response format.
            for(ContentInfo contentInfo : contentInfoList){
                if(currentNodeName.equalsIgnoreCase("root") || boardMap.get(contentInfo.getBoardId()).getName().equalsIgnoreCase(currentNodeName)){
                    ViewAttemptedTestRes res = new ViewAttemptedTestRes();
                    res.setAttemptedTime(contentInfo.getMetadata().getTestAttempts().get(0).getStartTime());
                    res.setEvaluated(contentInfo.getContentState().equals(ContentState.EVALUATED) ? true : false);
                    res.setSubject(boardMap.get(contentInfo.getBoardId()).getName());
                    res.setEvaluatedTime(contentInfo.getMetadata().getTestAttempts().get(0).getEvaluatedTime());
                    String testId = contentInfo.getMetadata().getTestId();
                    if(map.containsKey(testId)){
                        res.setTestTitle(map.get(testId).getName());
                    }
                    if(ContentInfoType.OBJECTIVE.toString().equals(testType)){
                        if(contentInfo.getMetadata().getTestAttempts() != null) {
                            res.setMarksAchieved(contentInfo.getMetadata().getTestAttempts().get(0).getMarksAcheived());
                            res.setTotalMarks(contentInfo.getMetadata().getTestAttempts().get(0).getTotalMarks());
                            res.setCorrect(contentInfo.getMetadata().getTestAttempts().get(0).getCorrect());
                            res.setIncorrect(contentInfo.getMetadata().getTestAttempts().get(0).getIncorrect());
                            res.setUnattempted(contentInfo.getMetadata().getTestAttempts().get(0).getUnattempted());
                            res.setTestAverage(calculateTestAverageOverall(testId,testIdToNodeNames.get(testId)));
                        }
                    }
                    res.setContentInfoId(contentInfo.getId());
                    res.setAttemptId(contentInfo.getMetadata().getTestAttempts().get(0).getId());
                    response.add(res);
                }
            }
            return response;

        }

    }

    private Double calculateTestAverageOverall(String testId, List<String> nodeNames) {
        List<TestNodeStats> testNodeStats = lOAMDAO.loam_getTestNodeStatsforNodes(testId, nodeNames);
        Double testAverage = 0.0;
        Double overallTestAverage = 0.0;
        if (testNodeStats.size() > 0) {
            for (TestNodeStats testData : testNodeStats) {
                testAverage = testAverage + getTestAverage(testData);
            }
            overallTestAverage = testAverage / testNodeStats.size();
        }
        return overallTestAverage;
    }

    private Double getTestAverage(TestNodeStats testNodeStats) {
        Double sum = 0.0;
        if (testNodeStats.getEasyQuestionStats().getMarksAchieved() != null) {
            sum = sum + testNodeStats.getEasyQuestionStats().getMarksAchieved();
        }
        if (testNodeStats.getModerateQuestionStats().getMarksAchieved() != null) {
            sum = sum + testNodeStats.getModerateQuestionStats().getMarksAchieved();
        }
        if (testNodeStats.getToughQuestionStats().getMarksAchieved() != null) {
            sum = sum + testNodeStats.getToughQuestionStats().getMarksAchieved();
        }
        if (testNodeStats.getUnknownQuestionStats().getMarksAchieved() != null) {
            sum = sum + testNodeStats.getUnknownQuestionStats().getMarksAchieved();
        }
        Double average = 0.0;
        if(testNodeStats.getStudentCount() !=null && testNodeStats.getStudentCount() !=0){
            average = sum / testNodeStats.getStudentCount();
        }
        return average;

    }

    /**
     * get node names from test Metadata
     * @param test
     * @return
     */
    private List<String> getNodeName(CMDSTest test) {
        List<String> nodeNames = new ArrayList<>();
        List<TestMetadata> metadataList = test.getMetadata();
        if(metadataList !=null){
            for(TestMetadata metadata: metadataList){
                nodeNames.add(metadata.getName());
            }
        }
        return nodeNames;
    }

    private void loam_addProjectionCMDSQuestionForViewAttemptedtest(Set<String> requiredQuestionFields) {
        requiredQuestionFields.add(CMDSQuestion.Constants.MAIN_TAGS);
    }

    private void loam_addProjectionContentInfoForViewAttemptedtest(Set<String> requiredContentInfoFields) {
        requiredContentInfoFields.add(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_RESULT_ENTRIES);
    }

    public String loam_getBaseTopicTreeDataFromLMS(Long fromTime, Long thruTime) throws VException {
        String lmsUrl = LMS_ENDPOINT_LOAM + "loam_getBaseTopicTreeData";
        String params = "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        lmsUrl = lmsUrl.concat(params);
        int start = 0;
        int size = 1000;

        while(true){
            String timeParams = "&start=" + start + "&size=" + size;
            String url = lmsUrl.concat(timeParams);
            String respString = getResponseFromAPI(url, HttpMethod.GET, null);
            Type listType = new TypeToken<ArrayList<BaseTopicTree>>() {}.getType();
            List<BaseTopicTree> baseTopicTree = new Gson().fromJson(respString, listType);


            if(baseTopicTree == null || baseTopicTree.isEmpty()){
                break;
            }

            start = start + size;
            // Update new CMDSQuestion
            lOAMDAO.loam_updateBaseTopicTree(baseTopicTree);

            if(baseTopicTree.size() < size){
                break;
            }
        }
        return "BaseTopicTree updated";
    }
}

