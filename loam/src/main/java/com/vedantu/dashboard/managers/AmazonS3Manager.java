package com.vedantu.dashboard.managers;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class AmazonS3Manager {

    @Autowired
    private AmazonClient amazonClient;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonS3Manager.class);


    public String getImageUrl(CMDSImageDetails imageDetails) {
        UploadTarget uploadTarget = imageDetails.getUploadTarget();
        if (uploadTarget == null) {
            uploadTarget = UploadTarget.QUESTIONSETS;//for backward compatibility
        }
        String bucket = uploadTarget.getBucketName();
        if (StringUtils.isEmpty(bucket)) {
            bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        }
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + File.separator + uploadTarget.getFolderPath() + File.separator + imageDetails.getFileName();
        return amazonClient.getImageUrl(fullKey, bucket);
    }


}
