package com.vedantu.dashboard.constants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class QuestionSetFileConstants
{
    public static final String PREFIX_TITLE = "title:";

    public static final String PREFIX_SOLUTION = "solution:";
    public static final String PREFIX_COLUMNA = "columna:";
    public static final String PREFIX_COLUMNB = "columnb:";
    public static final String PREFIX_ANSWER = "answer:";
    public static final String PREFIX_OPTIONS = "options:";
    public static final String PREFIX_QUESTION = "question:";
    public static final String PREFIX_HINTS = "hints:";

    public static final String PREFIX_TYPE = "type:";
    public static final String PREFIX_INDEXABLE = "indexable:";
    public static final String PREFIX_DIFFICULTY = "difficulty:";
    public static final String PREFIX_SUBJECT = "subject:";
    public static final String PREFIX_TOPICS = "topics:";
    public static final String PREFIX_SUBTOPICS = "subtopics:";
    public static final String PREFIX_TAGS = "tags:";
    public static final String PREFIX_GRADES = "grades:";
    public static final String PREFIX_TARGET_GRADE = "targetgrade:";
    public static final String PREFIX_DIFFICULTY_VALUE = "difficultyvalue:";
    public static final String PREFIX_COGNITIVE_LEVEL = "cognitivelevel:";
    public static final String PREFIX_SOURCE = "source:";
    public static final String PREFIX_TARGETS = "targets:";
    public static final String PREFIX_MARKS = "marks:";
    public static final String PREFIX_NEGATIVE_MARKS = "negativemarks:";

    //book related
    public static final String PREFIX_BOOK = "book:";
    public static final String PREFIX_EDITION = "edition:";
    public static final String PREFIX_CHAPTER = "chapter:";
    public static final String PREFIX_CHAPTER_NO = "chapterno:";
    public static final String PREFIX_EXERCISE = "exercise:";
    public static final String PREFIX_PAGE_NOS = "pagenos:";
    public static final String PREFIX_SLNOIN_BOOK = "slnoinbook:";
    public static final String PREFIX_QUESTION_NO_IN_BOOK = "questionno:";

    public List<String> allPrefixes = new ArrayList<>();
    public static QuestionSetFileConstants INSTANCE = new QuestionSetFileConstants();

    public QuestionSetFileConstants()
    {
        Field[] fields = QuestionSetFileConstants.class.getDeclaredFields();

        for (Field field : fields)
        {
            Object ob;
            try
            {
                ob = field.get(this);

                if (ob instanceof String)
                {
                    String val = (String) ob;
                    allPrefixes.add(val);
                }
            }
            catch (Exception ex)
            {
                //swallow
            }
        }
    }

}
