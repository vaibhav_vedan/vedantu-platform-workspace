package com.vedantu.dashboard.service;

import com.vedantu.dashboard.enums.ExecutorRequestType;
import com.vedantu.util.ConfigUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
public class TaskExecutorService implements InitializingBean {

    // Used to handle all students insights for Engagement
    private ExecutorService allStudentEngagementInsightExecutorService;
    // Used to handle all students insights for Engagement
    private ExecutorService allStudentAttendanceInsightExecutorService;

    public <T> Future<T> addTask(Callable<T> task, ExecutorRequestType requestType) {
        Future<T> requestStatus = null;
        switch (requestType) {
            case ALL_STUDENT_INSIGHT_ENGAGEMENT:
                requestStatus = allStudentEngagementInsightExecutorService.submit(task);
                break;
            case ALL_STUDENT_INSIGHT_ATTENDANCE:
                requestStatus = allStudentAttendanceInsightExecutorService.submit(task);
                break;
            default:
                break;
        }
        return requestStatus;
    }

    @PreDestroy
    public void closeExecutors() {
        allStudentEngagementInsightExecutorService.shutdown();
        allStudentAttendanceInsightExecutorService.shutdown();
    }

    public void afterPropertiesSet() throws Exception {
        allStudentEngagementInsightExecutorService = Executors.newFixedThreadPool(ConfigUtils.INSTANCE.getIntValue("ENGAGEMENT_BATCH_THREAD"));
        allStudentAttendanceInsightExecutorService = Executors.newFixedThreadPool(ConfigUtils.INSTANCE.getIntValue("ATTENDANCE_BATCH_THREAD"));
    }
}
