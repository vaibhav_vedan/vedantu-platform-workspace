package com.vedantu.dashboard.service;

import com.vedantu.dashboard.enums.ExecutorRequestType;
import com.vedantu.dashboard.managers.LOAMSchedulingManager;
import com.vedantu.dashboard.pojo.InsightResponseData;
import com.vedantu.dashboard.pojo.InsightScore;
import com.vedantu.dashboard.request.AMStudentInsightsReq;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

@Component("insightRequestHandler")
@Scope(value = "prototype")
public class InsightRequestHandler implements Callable<Map<Long, InsightResponseData>> {

    @Autowired
    private LOAMSchedulingManager loamSchedulingManager;
    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InsightRequestHandler.class);

    private AMStudentInsightsReq amStudentInsightsReq;
    private ExecutorRequestType allStudentInsightType;
    private Long fromTime;
    private Long currentTime;
    private List<Long> usersInBatch;

    @Override
    public Map<Long, InsightResponseData> call() throws Exception {

        Map<Long, InsightResponseData> studentToInsight = new HashMap<>();
        try {
            long startTime = System.currentTimeMillis();
            if (ExecutorRequestType.ALL_STUDENT_INSIGHT_ENGAGEMENT.equals(allStudentInsightType)) {
                studentToInsight = getStudentsInsightsEngagement();
            } else if (ExecutorRequestType.ALL_STUDENT_INSIGHT_ATTENDANCE.equals(allStudentInsightType)) {
                studentToInsight = getStudentsInsightsAttendance();
            }
            logger.info("Time taken by thread for {}, is {} is {}", allStudentInsightType, Thread.currentThread().getName(), System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            logger.error("Error occurred while processing Insights for: {}, is {}", allStudentInsightType, e);
        }
        return studentToInsight;
    }

    /**
     * calculates the studentInsight for Attendance
     *
     * @return
     */
    private Map<Long, InsightResponseData> getStudentsInsightsAttendance() throws VException {
        Map<Long, InsightResponseData> studentToInsight = new HashMap<>();
        if (usersInBatch != null) {
            logger.info("processing for this {}", this.usersInBatch);
            long startTime = System.currentTimeMillis();
            Map<Long, Map<String, Object>> studentToInsightsMap = loamSchedulingManager.loam_getInClassAttendanceStatsInsights(this.usersInBatch, fromTime, currentTime, -1L);
            logger.info("Time taken by attendanceMethod is {}, for user ids: {}", System.currentTimeMillis(), studentToInsightsMap);
            for (Map.Entry<Long, Map<String, Object>> entry : studentToInsightsMap.entrySet()) {
                Long userId = entry.getKey();
                Map<String, InsightScore> map = new HashMap<>();
                Map<String, Object> insightMap = entry.getValue();
                map.put("attendance", new InsightScore((Double) insightMap.get("insight_mark"), String.valueOf(insightMap.get("insight_grade"))));
                studentToInsight.put(userId, new InsightResponseData(userId, map));
            }
        }
        return studentToInsight;
    }

    /**
     * calculates the studentInsight for Engagement
     *
     * @return
     * @throws VException
     */
    private Map<Long, InsightResponseData> getStudentsInsightsEngagement() throws VException {

        Map<Long, InsightResponseData> studentToInsight = new HashMap<>();
        if (usersInBatch != null) {
            logger.info("processing for this {}", this.usersInBatch);
            long startTime = System.currentTimeMillis();
            Map<Long, Map<String, Object>> studentToInsightsMap = loamSchedulingManager.loam_getInClassEngagementInsights(this.usersInBatch, fromTime, currentTime, -1L);
            logger.info("Time taken by engagementMethod is {}, for user ids: {}, {}", System.currentTimeMillis(), studentToInsightsMap.size(), studentToInsightsMap);
            for (Map.Entry<Long, Map<String, Object>> entry : studentToInsightsMap.entrySet()) {
                Long userId = entry.getKey();
                Map<String, InsightScore> map = new HashMap<>();
                Map<String, Object> insightMap = entry.getValue();
                map.put("engagement", new InsightScore((Double) insightMap.get("insight_mark"), String.valueOf(insightMap.get("insight_grade"))));
                studentToInsight.put(userId, new InsightResponseData(userId, map));
            }
        }
        return studentToInsight;
    }

    public void setMessage(AMStudentInsightsReq req, Long FROM_TIME, Long currentTime, List<Long> usersInBatch, ExecutorRequestType allStudentInsightType) {
        this.amStudentInsightsReq = req;
        this.currentTime = currentTime;
        this.fromTime = FROM_TIME;
        this.usersInBatch = usersInBatch;
        this.allStudentInsightType = allStudentInsightType;
    }

    public List<Long> getMessage() {
        return this.usersInBatch;
    }

}
