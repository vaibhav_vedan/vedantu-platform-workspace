/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.utils;

import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.dashboard.entities.*;
import com.vedantu.dashboard.entities.mongo.Doubt;
import com.vedantu.dashboard.entities.mongo.DoubtChatMessage;
import com.vedantu.dashboard.enums.AsyncTaskName;
import com.vedantu.dashboard.managers.LOAMManager;
import com.vedantu.dashboard.managers.LOAMSchedulingManager;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author ajith
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor
{
    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);


    @Autowired
    private LOAMManager lOAMManager;

    @Autowired
    private LOAMSchedulingManager lOAMSchedulingManager;

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception
    {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName))
        {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }

        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();

        switch (taskName)
        {
            case LOAM_UPDATE_TEST_NODE_STATS:
                long fromTime = (Long) payload.get("fromTime");
                long thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_updateAMTestNodeStats(fromTime, thruTime);
                break;
            case LOAM_RECORD_USAGE_STATS:
                String data = payload.get("data") != null ? payload.get("data").toString() : "";
                lOAMManager.persistUsageStatus(data);
                break;
            case LOAM_GET_CONTENT_INFO_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getContentInfoDataFromLMS(fromTime, thruTime);
                break;
            case LOAM_GET_CMDS_TEST_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getCMDSTestDataFromLMS(fromTime, thruTime);
                break;
            case LOAM_GET_CMDS_QUESTION_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getCMDSQuestionDataFromLMS(fromTime, thruTime);
                break;
            case LOAM_GET_QUESTION_ATTEMPT_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getQuestionAttemptDataFromLMS(fromTime, thruTime);
                break;
            case LOAM_GET_DOUBT_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getDoubtDataFromLMS(fromTime, thruTime);
                break;
            case LOAM_GET_DOUBT_CHAT_MESSAGE_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getDoubtChatMessageDataFromLMS(fromTime, thruTime);
                break;
            case LOAM_UPDATE_SESSION_ATTENDANCE_AVERAGE:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMSchedulingManager.loam_updateSessionAttendanceAvg(fromTime, thruTime);
                break;
            case LOAM_GET_OTF_SESSION_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMSchedulingManager.loam_getOTFSessionDataFromScheduling(fromTime, thruTime);
                break;
            case LOAM_GET_GTTATTENDEE_DETAILS_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMSchedulingManager.loam_getGTTAttendeeDetailsFromScheduling(fromTime, thruTime);
                break;
            case LOAM_GET_OTMSESSION_ENGAGEMENT_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMSchedulingManager.loam_getOTMSessionEngagementDataFromScheduling(fromTime, thruTime);
                break;
            case LOAM_GET_BATCH_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getBatchDataFromSubscription(fromTime, thruTime);
                break;
            case LOAM_GET_COURSE_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getCourseDataFromSubscription(fromTime, thruTime);
                break;
            case LOAM_GET_ENROLLMENT_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getEnrollmentDataFromSubscription(fromTime, thruTime);
                break;
            case LOAM_GET_BOARD_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getBoardDataFromPlatform(fromTime, thruTime);
                break;
            case LOAM_GET_AM_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getAMDataFromUser(fromTime, thruTime);
                break;
            case LOAM_UPDATE_CONTENT_INFO_SLIM_VERSION:
                List<ContentInfo> contentInfos = (List<ContentInfo>) payload.get("contentInfos");
                lOAMManager.insertContentInfoSlimVersion(contentInfos);
                break;
            case LOAM_UPDATE_CMDS_TEST_SLIM_VERSION:
                List<CMDSTest> cmdsTests = (List<CMDSTest>) payload.get("cmdsTests");
                lOAMManager.insertCMDSTestSlimVersion(cmdsTests);
                break;
            case LOAM_UPDATE_CMDS_QUESTION_SLIM_VERSION:
                List<CMDSQuestion> cmdsQuestions = (List<CMDSQuestion>) payload.get("cmdsQuestions");
                lOAMManager.insertCMDSQuestionSlimVersion(cmdsQuestions);
                break;
            case LOAM_GET_BASE_TOPIC_TREE_DATA:
                fromTime = (Long) payload.get("fromTime");
                thruTime = (Long) payload.get("thruTime");
                lOAMManager.loam_getBaseTopicTreeDataFromLMS(fromTime, thruTime);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception)
    {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
