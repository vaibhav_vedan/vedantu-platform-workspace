package com.vedantu.dashboard.dao;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dashboard.entities.BaseTopicTree;
import com.vedantu.dashboard.entities.CurriculumTopicTree;
import com.vedantu.dashboard.enums.AsyncTaskName;
import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.dashboard.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.regex.Pattern;

@Service
public class TopicTreeDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TopicTreeDAO.class);

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    public TopicTreeDAO() {
        super();
    }

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    
    public List<BaseTopicTree> getBasicTopicTreeNodesByParentId(String parentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.PARENT_ID).is(parentId));
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }

    public List<BaseTopicTree> getBaseTreeNodesByLevel(Integer level) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.LEVEL).is(level));
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }
    
    public Set<BaseTopicTree> getBaseTreeNodesByNodeNames(Set<String> nodeNames){
        Query query = new Query();
        if(!ArrayUtils.isEmpty(nodeNames))
        	query.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).in(nodeNames));
        else
        	return null;
        Set<BaseTopicTree> nodes = new HashSet<>(runQuery(query, BaseTopicTree.class));
        return nodes;
    }
    
    public List<CurriculumTopicTree> getCurriculumTreeNodesByLevel(Integer level) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.LEVEL).is(level));
        List<CurriculumTopicTree> boards = runQuery(query, CurriculumTopicTree.class);
        return boards;
    }
}
