package com.vedantu.dashboard.dao;

import com.vedantu.dashboard.entities.CMDSTest;
import com.vedantu.dashboard.entities.CMDSTestListOrder;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class CMDSTestDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSTestDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public CMDSTestDAO() {
		super();
	}


	public void update(CMDSTestListOrder test) {
		logger.info("Request : " + test.toString());
		try {
			Assert.notNull(test);
			saveEntity(test);
			logger.info("Updated id : " + test.getId());
		} catch (Exception ex) {
			throw new RuntimeException("CMDSTestListOrderUpdateError : Error updating the CMDS Test List Order " + test.toString(), ex);
		}
	}

	public List<CMDSTest> getByIds(List<String> testIds) {

		Query testQuery = new Query();
		testQuery.addCriteria(Criteria.where(CMDSTest.Constants._ID).in(testIds));
		List<CMDSTest> results = runQuery(testQuery, CMDSTest.class);
		return results;
	}
        
	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

}
