package com.vedantu.dashboard.dao;


import com.google.gson.Gson;
import com.vedantu.User.User;
import com.vedantu.dashboard.entities.*;
import com.vedantu.dashboard.entities.mongo.Doubt;
import com.vedantu.dashboard.entities.mongo.DoubtChatMessage;
import com.vedantu.dashboard.enums.DoubtState;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.dashboard.entities.ContentInfo;
import com.vedantu.dashboard.entities.LOUsageStats;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.request.GetEnrollmentsReq;
import org.apache.logging.log4j.Logger;
import org.aspectj.weaver.ast.Test;
import org.elasticsearch.common.recycler.Recycler;
import org.jsoup.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
@Service
public class LOAMDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LOAMDAO.class);

	public LOAMDAO() {
		super();
	}

	@Override
	public MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}


	public List<QuestionAttempt> loam_fetchQuestionAttempts(List<String> questionIds, Long studentId, Set<String> includeKeySet, boolean isInclude ) {
		Query q = new Query();
		q.addCriteria(Criteria.where(QuestionAttempt.Constants.QUESTION_ID).in(questionIds));
		q.addCriteria(Criteria.where("userId").is(studentId.toString()));
		q.with(Sort.by(Sort.Direction.DESC, "creationTime"));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet, isInclude);

		return runQuery(q, QuestionAttempt.class);
	}

	public List<QuestionAttempt> loam_fetchAMQuestionAttemptsForTestNode(List<String> questionIds,String testId, String studentId) {
		Query q = new Query();
		q.addCriteria(Criteria.where(QuestionAttempt.Constants.QUESTION_ID).in(questionIds));
		q.addCriteria(Criteria.where(QuestionAttempt.Constants.CONTEXT_ID).is(testId));
		q.addCriteria(Criteria.where("userId").is(studentId));
		q.with(Sort.by(Sort.Direction.DESC, "creationTime"));
		setQueryMaxTimeout(q);
		return runQuery(q, QuestionAttempt.class);
	}

	private void setQueryMaxTimeout(Query query) {
		if (query != null) {
			query.maxTimeMsec(86400000l);
		}
	}
	public List<CMDSQuestion> loam_getQuestionsByTest(List<String> questionIds,Set<String> includeKeySet, boolean isInclude ) {
		Query q = new Query();
		q.addCriteria(Criteria.where(CMDSQuestion.Constants.ID).in(questionIds));
		setQueryMaxTimeout(q);

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet, isInclude);

		return runQuery(q, CMDSQuestion.class);
	}


	public void saveEntity(TestNodeStats testNodeStats) {
		super.saveEntity(testNodeStats);
	}


	public TestNodeStats loam_getTestNodeStats(String testId, String nodeName) {
		Query q = new Query();
		q.addCriteria(Criteria.where("testId").is(testId));
		q.addCriteria(Criteria.where("nodeName").is(nodeName));
		List<TestNodeStats> testNodeStats = runQuery(q, TestNodeStats.class);
		if(testNodeStats == null || testNodeStats.size() == 0) {
			return null;
		} else {
			return testNodeStats.get(0);
		}
	}

	public List<TestNodeStats> loam_getTestNodeStats(List<String> testIds, String nodeName, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		q.addCriteria(Criteria.where("testId").in(testIds));
		q.addCriteria(Criteria.where("nodeName").is(nodeName));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		List<TestNodeStats> testNodeStatsList = runQuery(q, TestNodeStats.class);
		return testNodeStatsList;
	}
	public List<TestNodeStats> loam_getTestNodeStatsForNodeNames(List<String> testIds, List<String> nodeNames,Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		q.addCriteria(Criteria.where("testId").in(testIds));
		q.addCriteria(Criteria.where("nodeName").in(nodeNames));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		List<TestNodeStats> testNodeStatsList = runQuery(q, TestNodeStats.class);
		return testNodeStatsList;
	}

	public List<CMDSTest> loam_getAllAssignments(final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		q.addCriteria(Criteria.where(CMDSTest.Constants.METADATA_DURATION).exists(false));
		q.addCriteria(Criteria.where(CMDSTest.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(fromTime), Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(thruTime));
		q.addCriteria(andCriteria);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, CMDSTest.class);
	}


	public List<ContentInfo> loam_getAssignmentByStudentAndTest(final Long studentId,List<String> testIds, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()),
				Criteria.where(ContentInfo.Constants.METADATA+"."+ContentInfo.Constants.TEST_ID).in(testIds),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).exists(false));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}
	public List<ContentInfo> loam_getAssignmentByTests(List<String> testIds, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.METADATA+"."+ContentInfo.Constants.TEST_ID).in(testIds),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).exists(false));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}

	public List<ContentInfo> loam_getTestByStudent(final Long studentId, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).gt(0));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}

	public List<ContentInfo> loam_getTestByStudent_Subjective(final Long studentId, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.SUBJECTIVE),
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}

	public List<ContentInfo> loam_getTestByStudentForTestWiseStats(final Long studentId, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).gt(0));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}

	public List<ContentInfo> loam_getTestByStudentAndTestIds(final Long studentId,List<String> testIds, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()),
				Criteria.where(ContentInfo.Constants.METADATA+"."+ContentInfo.Constants.TEST_ID).in(testIds),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).gt(0));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME));

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}
	public List<ContentInfo> loam_getTestByTestIds(List<String> testIds, final Long fromTime, final Long thruTime, Set<String> includeKeySet, boolean isInclude) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.METADATA+"."+ContentInfo.Constants.TEST_ID).in(testIds),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).gt(0));
		q.addCriteria(andCriteria1);

		//manage filter on fields required (Projection)
		loam_manageFieldsInclude(q,includeKeySet,isInclude);

		return runQuery(q, ContentInfo.class);
	}

	public List<CMDSTest> loam_getTestListByTestIds(List<String> testIds) {
		Query q = new Query();

		q.fields().include(CMDSTest.Constants.ID);
		q.fields().include("name");
		q.fields().include("metadata");
		q.addCriteria(Criteria.where(CMDSTest.Constants._ID).in(testIds));

		return runQuery(q, CMDSTest.class);
	}

	public List<TestNodeStats> loam_getTestNodeStatsforNodes(final String testId, List<String> nodeName){
		Query q = new Query();
		q.addCriteria(Criteria.where("testId").is(testId));
		q.addCriteria(Criteria.where("nodeName").in(nodeName));
		return runQuery(q, TestNodeStats.class);
	}

	public Map<String, TestNodeStats> loam_getTestNodeStatsforNodes(List<String> testIds, String nodeName){
		Query q = new Query();
		Map<String, TestNodeStats> testToTestNodeData = new HashMap<>();
		q.addCriteria(Criteria.where("testId").in(testIds));
		q.addCriteria(Criteria.where("nodeName").is(nodeName));
		List<TestNodeStats> testNodeStats = runQuery(q, TestNodeStats.class);
		for(TestNodeStats testData: testNodeStats){
			testToTestNodeData.put(testData.getTestId(),testData);
		}
		return testToTestNodeData;
	}

	public List<ContentInfo> loam_testProjectionsgetAllAssignments(Long studentId, Long fromTime, Long thruTime) {
		Query q = new Query();
		Criteria andCriteria1 = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(fromTime),
				Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(thruTime),
				Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST),
				Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED),
				Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE),
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()),
				Criteria.where(ContentInfo.Constants.METADATA_DURATION).gt(0));
		q.addCriteria(andCriteria1);
		q.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"));
		return runQuery(q, ContentInfo.class);
	}

	private void loam_manageFieldsInclude(Query q, Set<String> includeKeySet, boolean isInclude){
		if(includeKeySet !=null && !includeKeySet.isEmpty())
		{
			if(isInclude)
				includeKeySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
			else
				includeKeySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
		}
	}

	public List<Curriculum> getCurriculumByBatchIds(Collection<String> parentIds, String[] includeFields, String[] excludeFields) {
		List<Curriculum> curriculums = new ArrayList<>();
		int start = 0;
		int limit = 400;
		while (true) {
			try {
				Query query = new Query();
				query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(CurriculumEntityName.OTF_BATCH));
				query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(parentIds));
				query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

				if (includeFields != null && includeFields.length > 0) {
					Field fields = query.fields();
					for (String field : includeFields) {
						fields.include(field);
					}
				}
				if (excludeFields != null && excludeFields.length > 0) {
					Field fields = query.fields();
					for (String field : excludeFields) {
						fields.exclude(field);
					}
				}

				query.with(Sort.by(Sort.Direction.ASC, Curriculum.Constants.CREATION_TIME));
				setFetchParameters(query, start, limit);

				List<Curriculum> tempCurriculum = runQuery(query, Curriculum.class);
				start += limit;
				if (ArrayUtils.isEmpty(tempCurriculum)) {
					break;
				}
				curriculums.addAll(tempCurriculum);
			} catch (Exception e) {
				logger.error("Error in fetching Curriculum from DB " + e);
				break;
			}
		}
		return curriculums;

	}


	public List<DoubtChatMessage> loam_getChatMessages(Long doubtId, int start, int size){

		Query query = new Query();
		query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).is(doubtId));
		query.with(Sort.by(Sort.Direction.DESC, DoubtChatMessage.Constants.CREATION_TIME));
		setFetchParameters(query, start, size);
		return runQuery(query, DoubtChatMessage.class);
	}

	public List<Doubt> loam_getDoubtsStatForStudent(String studentId, String nodeName, Long fromTime, Long thruTime) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(studentId));
		if(!nodeName.equalsIgnoreCase("root")){
			query.addCriteria(Criteria.where(Doubt.Constants.MAIN_TAGS).is(nodeName));
		}
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(Doubt.Constants.CREATION_TIME).gte(fromTime), Criteria.where(Doubt.Constants.CREATION_TIME).lte(thruTime));
		query.addCriteria(andCriteria);
		query.fields().include("_id");
		query.fields().include(Doubt.Constants.DOUBT_STATE);
		query.with(Sort.by(Sort.Direction.ASC, Doubt.Constants.LAST_ACTIVITY_TIME));


		return runQuery(query, Doubt.class);
	}

	public List<Doubt> loam_getResolvedDoubtsForStudent(String studentId,String nodeName, Long fromTime, Long thruTime, int start, int size){

		Query query = new Query();
		query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(studentId));
		if(!nodeName.equalsIgnoreCase("root")){
			query.addCriteria(Criteria.where(Doubt.Constants.MAIN_TAGS).is(nodeName));
		}
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(Doubt.Constants.CREATION_TIME).gte(fromTime),
				Criteria.where(Doubt.Constants.CREATION_TIME).lte(thruTime));
		query.addCriteria(andCriteria);
		query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_T1_SOLVED, DoubtState.DOUBT_T2_SOLVED)));
		query.with(Sort.by(Sort.Direction.ASC, Doubt.Constants.LAST_ACTIVITY_TIME));

		setFetchParameters(query, start, size);
		return runQuery(query, Doubt.class);
	}

	public List<Doubt> loam_getUnResolvedDoubtsForStudent(String studentId,String nodeName, Long fromTime, Long thruTime, int start, int size){

		Query query = new Query();
		query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(studentId));
		if(!nodeName.equalsIgnoreCase("root")){
			query.addCriteria(Criteria.where(Doubt.Constants.MAIN_TAGS).is(nodeName));
		}

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(Doubt.Constants.CREATION_TIME).gte(fromTime),
				Criteria.where(Doubt.Constants.CREATION_TIME).lte(thruTime));
		query.addCriteria(andCriteria);
		query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_POSTED,
				DoubtState.DOUBT_T2_POOL, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED,
				DoubtState.DOUBT_FEEDBACK_NO, DoubtState.DOUBT_MARKED_SPAM, DoubtState.DOUBT_T1_TIMEOUT, DoubtState.DOUBT_T1_UNABLE_TO_SOLVE, DoubtState.DOUBT_T2_UNABLE_TO_SOLVE)));
		query.with(Sort.by(Sort.Direction.ASC, Doubt.Constants.LAST_ACTIVITY_TIME));

		setFetchParameters(query, start, size);
		return runQuery(query, Doubt.class);
	}

	public void loam_persistUsageStatus(LOUsageStats usageStats) {

		super.saveEntity(usageStats);

	}

	public List<LOUsageStats> loam_getDashboardUsage(Long fromTime, Long thruTime) {

		Query query = new Query();

		if(fromTime !=null  && thruTime !=null)
		{
			Criteria andCriteria = new Criteria().andOperator(Criteria.where(LOUsageStats.Constants.TIMESTAMP).gte(fromTime),
					Criteria.where(LOUsageStats.Constants.TIMESTAMP).lte(thruTime));
			query.addCriteria(andCriteria);
		}

		query.addCriteria(Criteria.where(LOUsageStats.Constants.FUNCTIONALITY).is(LOUsageStats.Constants.VISITED));
		query.with(Sort.by(Sort.Direction.ASC, LOUsageStats.Constants.TIMESTAMP));


		return runQuery(query, LOUsageStats.class);

	}

	public List<CMDSQuestion> loam_getByIds(List<String> questionIds, VedantuRecordState recordState) {

		Query questionQuery = new Query();
		questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.ID).in(questionIds));
		if (recordState != null) {
			questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).is(recordState));
		}
		List<CMDSQuestion> results = runQuery(questionQuery, CMDSQuestion.class);
		return results;

	}

	public void loam_insertMany(List<TestNodeStats> testNodeStats) {
		super.insertAllEntities(testNodeStats,"TestNodeStats");
	}

	public void loam_insertManyContentInfos(List<ContentInfo> contentInfos) {
		getMongoOperations().insert(contentInfos, "ContentInfo");
	}

	public void loam_insertManyCMDSTest(List<CMDSTest> cmdsTests) {
		getMongoOperations().insert(cmdsTests,"CMDSTest");
	}

	public void loam_insertManyCMDSQuestion(List<CMDSQuestion> cmdsQuestions) {
		getMongoOperations().insert(cmdsQuestions,"CMDSQuestion");
	}

	public void loam_insertManyBaseTopicTree(List<BaseTopicTree> baseTopicTrees) {
		getMongoOperations().insert(baseTopicTrees,"BaseTopicTree");
	}

	public void loam_insertManyQuestionAttempts(List<QuestionAttempt> questionAttempts) {
		getMongoOperations().insert(questionAttempts,"QuestionAttempt");
	}

	public void loam_insertManyDoubts(List<Doubt> doubts) {
		getMongoOperations().insert(doubts,"Doubt");
	}

	public void loam_insertManyDoubtChatMessages(List<DoubtChatMessage> doubtChatMessages) {
		getMongoOperations().insert(doubtChatMessages,"DoubtChatMessage");
	}

	public void loam_insertManyOTFSessions(List<OTFSession> otfSessions) {
		getMongoOperations().insert(otfSessions,"OTFSession");
	}

	public void loam_insertManyGTTAttendeeDetails(List<GTTAttendeeDetails> gttAttendeeDetails) {
		getMongoOperations().insert(gttAttendeeDetails,"GTTAttendeeDetails");
	}

	public void loam_insertManyOTMSessionEngagementData(List<OTMSessionEngagementData> otmSessionEngagementData) {
		getMongoOperations().insert(otmSessionEngagementData,"OTMSessionEngagementData");
	}

	public void loam_updateContentInfo(List<ContentInfo> contentInfos) {
		List<String> ids = new ArrayList<>();
		for(ContentInfo contentInfo : contentInfos)
		{
			ids.add(contentInfo.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, ContentInfo.class);

		loam_insertManyContentInfos(contentInfos);
	}

	public void loam_updateCMDSTests(List<CMDSTest> cmdsTests) {
		List<String> ids = new ArrayList<>();
		for(CMDSTest cmdsTest : cmdsTests){
			ids.add(cmdsTest.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, CMDSTest.class);

		loam_insertManyCMDSTest(cmdsTests);
	}

	public void loam_updateCMDSQuestion(List<CMDSQuestion> cmdsQuestions) {
		List<String> ids = new ArrayList<>();
		for(CMDSQuestion cmdsQuestion : cmdsQuestions){
			ids.add(cmdsQuestion.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, CMDSQuestion.class);

		loam_insertManyCMDSQuestion(cmdsQuestions);
	}

	public void loam_updateBaseTopicTree(List<BaseTopicTree> baseTopicTrees) {
		List<String> ids = new ArrayList<>();
		for(BaseTopicTree baseTopicTree : baseTopicTrees){
			ids.add(baseTopicTree.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, BaseTopicTree.class);

		loam_insertManyBaseTopicTree(baseTopicTrees);
	}

	public void loam_updateQuestionAttempt(List<QuestionAttempt> questionAttempts) {
		List<String> ids = new ArrayList<>();
		for(QuestionAttempt questionAttempt : questionAttempts){
			ids.add(questionAttempt.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, QuestionAttempt.class);

		loam_insertManyQuestionAttempts(questionAttempts);
	}

	public void loam_updateDoubt(List<Doubt> doubts) {
		List<Long> ids = new ArrayList<>();
		for(Doubt doubt : doubts){
			ids.add(doubt.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, Doubt.class);

		loam_insertManyDoubts(doubts);
	}

	public void loam_updateDoubtChatMessage(List<DoubtChatMessage> doubtChatMessages) {
		List<String> ids = new ArrayList<>();
		for(DoubtChatMessage doubtChatMessage : doubtChatMessages){
			ids.add(doubtChatMessage.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, DoubtChatMessage.class);

		loam_insertManyDoubtChatMessages(doubtChatMessages);

	}

	public void loam_updateOTFSession(List<OTFSession> OTFSessions) {
		List<String> ids = new ArrayList<>();
		for(OTFSession otfSession : OTFSessions){
			ids.add(otfSession.getId());
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, OTFSession.class);

		loam_insertManyOTFSessions(OTFSessions);
	}

	public void loam_updateGTTAttendeeDetails(List<GTTAttendeeDetails> gttAttendeeDetails) {
		List<String> ids = new ArrayList<>();
		for(GTTAttendeeDetails gttAttendeeDetail : gttAttendeeDetails){
			ids.add(gttAttendeeDetail.getId());
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, GTTAttendeeDetails.class);

		loam_insertManyGTTAttendeeDetails(gttAttendeeDetails);
	}

	public void loam_updateOTMSessionEngagementData(List<OTMSessionEngagementData> otmSessionEngagementDatas) {
		List<String> ids = new ArrayList<>();
		for(OTMSessionEngagementData otmSessionEngagementData : otmSessionEngagementDatas){
			ids.add(otmSessionEngagementData.getId());
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, OTMSessionEngagementData.class);

		loam_insertManyOTMSessionEngagementData(otmSessionEngagementDatas);
	}

	public CopyOnWriteArrayList<OTFSession> loam_getAllEndedSessions(Long fromTime, Long thruTime)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		Query query = new Query();
		query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(fromTime).lte(thruTime));

		List<OTFSession> results = runQuery(query, OTFSession.class);

		if (!CollectionUtils.isEmpty(results))
		{
			oTFSessions.addAll(results);
		}

		return oTFSessions;
	}

	public List<GTTAttendeeDetails> loam_getGTTAttendeeDetailsForSessions(List<String> sessionIds)
	{
		Query q = new Query();
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		return runQuery(q, GTTAttendeeDetails.class);
	}

	public int loam_deleteSessionAttendeeAvgBySessionIds(List<String> idsToDelete){
		Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendanceAvg.Constants.SESSION_ID).in(idsToDelete));
		return  deleteEntities(query, SessionAttendanceAvg.class);
	}

	public void loam_saveSessionAttendeeAvgBySessionIdsAndBoardIds(List<SessionAttendanceAvg> sessionAttendanceAvgs){
		super.insertAllEntities(sessionAttendanceAvgs, SessionAttendanceAvg.class.getSimpleName());
	}

	public List<GTTAttendeeDetails>  loam_getAttendedGTTAttendeeDetailsInSessionRange(final Long studentId, final Long fromTime, final Long thruTime)
	{
		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
		q.addCriteria(andCriteria);
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		return runQuery(q, GTTAttendeeDetails.class);
	}
	public CopyOnWriteArrayList<OTFSession> loam_getAllSessionsForDoubts(Long fromTime, Long thruTime, Set<String> includeKeySet, boolean isInclude)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		Query query = new Query();
		query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
		query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));

		loam_manageFieldsInclude(query,includeKeySet,isInclude);

		List<OTFSession> results = runQuery(query, OTFSession.class);

		if (!CollectionUtils.isEmpty(results))
		{
			oTFSessions.addAll(results);
		}

		return oTFSessions;
	}

	public CopyOnWriteArrayList<OTFSession> loam_getByBoardIdForDoubt(Long boardId, Long fromTime, Long thruTime, Set<String> includeKeySet, boolean isInclude)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		if (!StringUtils.isEmpty(boardId))
		{
			Query query = new Query();
			query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(boardId));

			//For no fromTime and thruTime send -1,-1 as fromTime and thruTime
			if (fromTime > -1 && thruTime > -1)
			{
				query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
			}
			query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
			loam_manageFieldsInclude(query,includeKeySet,isInclude);
			List<OTFSession> results = runQuery(query, OTFSession.class);

			if (!CollectionUtils.isEmpty(results))
			{
				oTFSessions.addAll(results);
			}
		}

		return oTFSessions;
	}

	public List<SessionAttendanceAvg> loam_getSessionAttendeeAvgBySessionIdsAndBoardIds(List<String> ids, List<Long> boardIds, Set<String> includeKeySet, boolean isInclude)
	{
		List<SessionAttendanceAvg> sessionAttendanceAvgList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(ids))
		{
			Query query = new Query();
			query.addCriteria(Criteria.where(SessionAttendanceAvg.Constants.SESSION_ID).in(ids));
			if(boardIds != null)
				query.addCriteria(Criteria.where(SessionAttendanceAvg.Constants.BOARD_ID).in(boardIds));

			//manage filter on fields required (Projection)
			loam_manageFieldsInclude(query,includeKeySet,isInclude);

			List<SessionAttendanceAvg> results = runQuery(query, SessionAttendanceAvg.class);
			if (!CollectionUtils.isEmpty(results))
			{
				sessionAttendanceAvgList.addAll(results);
			}
		}

		return sessionAttendanceAvgList;
	}

	public List<OTFSession> loam_getByBoardIdInSessions(Long id,List<String> sids)
	{
		List<OTFSession> oTFSessions = new ArrayList<>();

		if (!StringUtils.isEmpty(id))
		{
			Query query = new Query();
			query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(id));
			query.addCriteria(Criteria.where(OTFSession.Constants._ID).in(sids));
			query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
			List<OTFSession> results = runQuery(query, OTFSession.class);

			if (!CollectionUtils.isEmpty(results))
			{
				oTFSessions.addAll(results);
			}
		}

		return oTFSessions;
	}

	//Purpose: Getting all the sessions of all the students in the given date range, for overall.
	//As it required for proper Insightnes of a given student w.r.t to others students.
	public CopyOnWriteArrayList<OTFSession> loam_getAllSessions(Long fromTime, Long thruTime)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		Query query = new Query();
		query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));


		//Arun Dhwaj: Code review-13thJul'19: As we know, we finally we responding the end-client a map. So, why sorting required here?
		// Which un-necessary increase the response time by o(nlogn) in general. So, commenting out.

		//query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));

		List<OTFSession> results = runQuery(query, OTFSession.class);

		if (!CollectionUtils.isEmpty(results))
		{
			oTFSessions.addAll(results);
		}

		return oTFSessions;
	}

	//Purpose: Getting all the sessions of all the students in the given date range, for given NodeName/boardId.
	//As it required for proper Insightnes of a given student w.r.t to others students.
	public CopyOnWriteArrayList<OTFSession> loam_getByBoardId(Long boardId, Long fromTime, Long thruTime)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		if (!StringUtils.isEmpty(boardId))
		{
			Query query = new Query();
			query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(boardId));

			//For no fromTime and thruTime send -1,-1 as fromTime and thruTime
			if (fromTime > -1 && thruTime > -1)
			{
				query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
			}

			//Arun Dhwaj: Code review-13thJul'19: As we know, we finally we responding the end-client a map. So, why sorting required here?
			// Which un-necessary increase the response time by o(nlogn) in general. So, commenting out.

			//query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
			List<OTFSession> results = runQuery(query, OTFSession.class);

			if (!CollectionUtils.isEmpty(results))
			{
				oTFSessions.addAll(results);
			}
		}

		return oTFSessions;
	}

	public CopyOnWriteArrayList<GTTAttendeeDetails> loam_getAllGTTAttendeeDetailsInRangeForSessions(final Long studentId, List<String> sessionIds, final Long fromTime, final Long thruTime)
	{
		CopyOnWriteArrayList<GTTAttendeeDetails> gTTAttendeeDetailsList = new CopyOnWriteArrayList<>();

		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));

		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));

		q.addCriteria(andCriteria);

		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

		//Arun Dhwaj: Code review-13thJul'19: As we know, we finally we responding the end-client a map. So, why sorting required here?
		// Which un-necessary increase the response time by o(nlogn) in general. So, commenting out.
		q.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));


		List<GTTAttendeeDetails> results = runQuery(q, GTTAttendeeDetails.class);

		if (!CollectionUtils.isEmpty(results))
		{
			gTTAttendeeDetailsList.addAll(results);
		}

		return gTTAttendeeDetailsList;
	}

	//Arun Dhwaj: Munti-threaded enviornment
	public CopyOnWriteArrayList<OTMSessionEngagementData> loam_getOTMSessionEngagementDataList(List<String> sessionIds)
	{
		CopyOnWriteArrayList<OTMSessionEngagementData> oTMSessionEngagementDataList = new CopyOnWriteArrayList<>();

		Query q = new Query();
		q.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.SESSIONID).in(sessionIds));

    	/*q.fields().include("totalUnattempts");
		q.fields().include("totalCorrectResponses");
		q.fields().include("totalIncorrectResponses");
		q.fields().include("studentCountAtStart");*/

		List<OTMSessionEngagementData> results = runQuery(q, OTMSessionEngagementData.class);

		if (!CollectionUtils.isEmpty(results))
		{
			oTMSessionEngagementDataList.addAll(results);
		}

		return oTMSessionEngagementDataList;

	}

	public List<OTFSession> getByIds(List<String> ids) {
		List<OTFSession> oTFSessions = new ArrayList<>();
		if (!CollectionUtils.isEmpty(ids)) {
			Query query = new Query();
			query.addCriteria(Criteria.where(OTFSession.Constants.ID).in(ids));
			query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
			List<OTFSession> results = runQuery(query, OTFSession.class);
			if (!CollectionUtils.isEmpty(results)) {
				oTFSessions.addAll(results);
			}
		}

		return oTFSessions;
	}

	public List<SessionAttendanceAvg> loam_getSessionAttendeeAvgBySessionIds(Set<String> ids, Set<String> includeKeySet, boolean isInclude)
	{
		List<SessionAttendanceAvg> sessionAttendanceAvgList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(ids))
		{
			Query query = new Query();
			query.addCriteria(Criteria.where(SessionAttendanceAvg.Constants.SESSION_ID).in(ids));
//			query.addCriteria(Criteria.where(SessionAttendanceAvg.Constants.STUDENT_ID).in(studentIds));
			//manage filter on fields required (Projection)
			loam_manageFieldsInclude(query,includeKeySet,isInclude);

			List<SessionAttendanceAvg> results = runQuery(query, SessionAttendanceAvg.class);

			if (!CollectionUtils.isEmpty(results))
			{
				sessionAttendanceAvgList.addAll(results);
			}
		}
		return sessionAttendanceAvgList;
	}

	public List<GTTAttendeeDetails>  loam_getAttendedGTTAttendeeDetailsInSessionRange(final List<Long> studentIds, final Long fromTime, final Long thruTime)
	{
		List<String> students = studentIds.stream().map(s->String.valueOf(s)).collect(Collectors.toList());
		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
		q.addCriteria(andCriteria);
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(students));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		return runQuery(q, GTTAttendeeDetails.class);
	}

	public ArrayList<GTTAttendeeDetails> loam_getAllStudentGTTAttendeeDetailsInRangeForSessions(final List<Long> studentId, final Long fromTime, final Long thruTime)
	{
		ArrayList<GTTAttendeeDetails> gTTAttendeeDetailsList = new ArrayList<>();
		List<String> students = studentId.stream().map(s->String.valueOf(s)).collect(Collectors.toList());
		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));

		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(students));
//		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));

		q.addCriteria(andCriteria);

		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

		//Arun Dhwaj: Code review-13thJul'19: As we know, we finally we responding the end-client a map. So, why sorting required here?
		// Which un-necessary increase the response time by o(nlogn) in general. So, commenting out.
		q.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));


		List<GTTAttendeeDetails> results = runQuery(q, GTTAttendeeDetails.class);

		if (!CollectionUtils.isEmpty(results))
		{
			gTTAttendeeDetailsList.addAll(results);
		}

		return gTTAttendeeDetailsList;
	}

    public List<ContentInfo_Old> getContentInfoSlimVersionData(Long fromTime, Long thruTime, int start, int size, Set<String> includeKeySet, boolean isInclude) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo_Old.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(ContentInfo_Old.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);

		query.with(Sort.by(Sort.Direction.DESC, ContentInfo_Old.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		loam_manageFieldsInclude(query,includeKeySet,isInclude);

		return runQuery(query, ContentInfo_Old.class);
    }

	public void setFetchParameters(Query query, Integer start, Integer limit) {
		int fetchStart = 0;
		if (start != null && start >= 0) {
			fetchStart = start;
		}
		int fetchSize = DEFAULT_FETCH_SIZE;
		if (limit != null && limit >= 0) {
			fetchSize = limit;
		}
		query.skip(fetchStart);
		query.limit(fetchSize);
	}

	public List<CMDSTest_Old> getCMDSTestSlimVersionData(Long fromTime, Long thruTime, int start, int size,  Set<String> includeKeySet, boolean isInclude) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(CMDSTest_Old.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(CMDSTest_Old.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);

		query.with(Sort.by(Sort.Direction.DESC, CMDSTest_Old.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		loam_manageFieldsInclude(query,includeKeySet,isInclude);

		return runQuery(query, CMDSTest_Old.class);
	}

	public List<CMDSQuestion_Old> getCMDSQuestionSlimVersionData(Long fromTime, Long thruTime, int start, int size,  Set<String> includeKeySet, boolean isInclude) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(CMDSQuestion_Old.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(CMDSQuestion_Old.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);

		query.with(Sort.by(Sort.Direction.DESC, CMDSQuestion_Old.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		loam_manageFieldsInclude(query,includeKeySet,isInclude);

		return runQuery(query, CMDSQuestion_Old.class);
	}

	public void deleteTestNodeStats(String testId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(TestNodeStats.Constants.Test_ID).in(testId));
		logger.info("Deleted Entries : " );
		int num =  deleteEntities(query, TestNodeStats.class);
		logger.info("Deleted Entries : " + num);
	}

	public List<Enrollment> getEnrollments(Long userId) {
		Query query = new Query();

		query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
		query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.CREATION_TIME));
		return runQuery(query, Enrollment.class);
	}

	public List<Batch> getBatchByIds(List<String> batchIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
		return runQuery(query, Batch.class);
	}

	public List<Course> getCoursesBasicInfos(List<String> ids) {
		return getCoursesBasicInfos(ids, null);
	}

	public List<Course> getCoursesBasicInfos(List<String> ids, List<String> includeFields) {

		Query query = new Query();
		if (ArrayUtils.isNotEmpty(ids)) {
			query.addCriteria(Criteria.where(Course.Constants.ID).in(ids));
		}
		if (ArrayUtils.isNotEmpty(includeFields)) {
			includeFields.forEach((fieldName) -> {
				query.fields().include(fieldName);
			});
		}
		logger.info("query " + query.toString());
		List<Course> courses = runQuery(query, Course.class);
		return courses;
	}

	public void loam_updateBatch(List<Batch> batches) {
		List<String> ids = new ArrayList<>();
		for(Batch batch : batches){
			ids.add(batch.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, Batch.class);

		loam_insertManyBatch(batches);
	}

	private void loam_insertManyBatch(List<Batch> batches) {
		getMongoOperations().insert(batches,"Batch");
	}

	public void loam_updateCourse(List<Course> courses) {
		List<String> ids = new ArrayList<>();
		for(Course batch : courses){
			ids.add(batch.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, Course.class);

		loam_insertManyCourse(courses);
	}

	private void loam_insertManyCourse(List<Course> courses) {
		getMongoOperations().insert(courses,"Course");
	}

	public void loam_updateEnrollment(List<Enrollment> enrollments) {
		List<String> ids = new ArrayList<>();
		for(Enrollment batch : enrollments){
			ids.add(batch.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, Enrollment.class);

		loam_insertManyEnrollment(enrollments);
	}

	private void loam_insertManyEnrollment(List<Enrollment> enrollments) {
		getMongoOperations().insert(enrollments,"Enrollment");
	}

	public Map<Long, Board> getBoardInfoMap(List<Long> boardIdList) {
		Map<Long, Board> boardsMap = new HashMap<>();
		Query query = new Query();
		query.addCriteria(Criteria.where(Board.Constants.ID).in(boardIdList));
		List<Board> boards = runQuery(query, Board.class);
		for (Board board : boards) {
			boardsMap.put(board.getId(), board);
		}
		return boardsMap;
	}

	public void loam_updateBoard(List<Board> boards) {
		List<Long> ids = new ArrayList<>();
		for(Board board : boards){
			ids.add(board.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(ids));
		deleteEntities(query, Board.class);

		loam_insertManyBoards(boards);
	}

	private void loam_insertManyBoards(List<Board> boards) {
		getMongoOperations().insert(boards,"Board");
	}

	public void loam_updateAMData(List<DoubtSolver> users) {
		List<String> ids = new ArrayList<>();
		for(DoubtSolver user : users){
			ids.add(user.getId());
		}

		Query query = new Query();
		query.addCriteria(Criteria.where(DoubtSolver.Constants._ID).in(ids));
		deleteEntities(query, DoubtSolver.class);

		loam_insertManyDoubtSolvers(users);
	}

	private void loam_insertManyDoubtSolvers(List<DoubtSolver> doubtSolvers) {
		getMongoOperations().insert(doubtSolvers, "DoubtSolver");
	}

	public List<DoubtSolver> getDoubtSolverForSolverIdList(List<String> doubtSolverIdList) {
		Query query = new Query();
		query.addCriteria(Criteria.where(DoubtSolver.Constants._ID).in(doubtSolverIdList));
		return runQuery(query, DoubtSolver.class);
	}

	public List<ContentInfo> getTestAttemptsList(Long studentId, Long fromTime, Long thruTime, Integer start, Integer size, String testType)
	{
		Query query = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_ENDED_TIME).gte(fromTime),
                Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_ENDED_TIME).lte(thruTime));
		query.addCriteria(andCriteria);
		query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
		if(ContentInfoType.SUBJECTIVE.toString().equals(testType)) {
			query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.SUBJECTIVE));
		} else if(ContentInfoType.OBJECTIVE.toString().equals(testType)){
			query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE));
		}
		setFetchParameters(query, start, size);

		return runQuery(query, ContentInfo.class);
	}

	public List<ContentInfo> getSubjectiveTestAttemptListForNodeName(Long studentId, String currentNodeName, Long fromTime, Long thruTime, Integer start, Integer size)
	{
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_ENDED_TIME).gte(fromTime),
                Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_ENDED_TIME).gte(thruTime));
		query.addCriteria(andCriteria);
		query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
		query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.SUBJECTIVE));
		query.addCriteria(Criteria.where(ContentInfo.Constants.SUBJECT).is(currentNodeName));

		setFetchParameters(query, start, size);

		return runQuery(query, ContentInfo.class);
	}

		public List<ContentInfo> getAMAllContents(Long startTime, Long endTime, Integer start, Integer size) {
			logger.info("getAllContents startTime : " + startTime + " endTime : " + endTime);
			Query query = new Query();
			long t1 = Instant.now().toEpochMilli();
			logger.info("getAllContents:  query start time:" + t1);

			Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).gte(startTime), Criteria.where(ContentInfo.Constants.METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME).lte(endTime),
					Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));

			query.addCriteria(andCriteria);

			setFetchParameters(query, start, size);
			setQueryMaxTimeout(query);

			logger.info("Query Info" + query);

			List<ContentInfo> contentInfos = runQuery(query, ContentInfo.class);
			long t2 = Instant.now().toEpochMilli();
			logger.info("getAllContents: query time:" + (t2 - t1));
			logger.info("getAllContents: " + contentInfos == null ? 0 : contentInfos.size());
			logger.info("getAllContents: " + new Gson().toJson(contentInfos));
			return contentInfos;
		}

}
