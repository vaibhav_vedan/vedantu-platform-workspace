package com.vedantu.dashboard.entities;

import com.vedantu.User.Role;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LOUsageStats")
public class LOUsageStats  extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long timestamp;

    @Indexed(background = true)
    private Long callingUserId;

    @Indexed(background = true)
    private Long studentId;
    private Role callingUserRole;
    private String functionality;

    public LOUsageStats(){
        super();
    }

    public LOUsageStats(Long timestamp, Long callingUserId, Long studentId, Role callingUserRole) {
        this.timestamp = timestamp;
        this.callingUserId = callingUserId;
        this.studentId = studentId;
        this.callingUserRole = callingUserRole;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getCallingUserId() {
        return callingUserId;
    }

    public void setCallingUserId(Long callingUserId) {
        this.callingUserId = callingUserId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Role getCallingUserRole() {
        return callingUserRole;
    }

    public void setCallingUserRole(Role callingUserRole) {
        this.callingUserRole = callingUserRole;
    }

    public String getFunctionality() {
        return functionality;
    }

    public void setFunctionality(String functionality) {
        this.functionality = functionality;
    }

    public static class Constants{
        public static final String TIMESTAMP = "timestamp";
        public static final String FUNCTIONALITY = "functionality";
        public static final String VISITED = "VISITED";
    }

    @Override
    public String toString() {
        return "LOUsageStats{" +
                "timestamp=" + timestamp +
                ", callingUserId=" + callingUserId +
                ", studentId=" + studentId +
                ", callingUserRole=" + callingUserRole +
                ", functionality='" + functionality + '\'' +
                '}';
    }
}
