package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.pojo.TestNodeStudentData;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Document(collection = "TestNodeStats")
@CompoundIndexes({
    @CompoundIndex(name = "testId_1_nodeName_1", def = "{'testId': 1, 'nodeName': 1}", background = true, unique = true)
})
public class TestNodeStats extends AbstractMongoStringIdEntity {
	
	private String testId;
	private String nodeName;
	private Set<String> questionIds;
	private QuestionStats easyQuestionStats;
	private QuestionStats moderateQuestionStats;
	private QuestionStats toughQuestionStats;
	private QuestionStats unknownQuestionStats;
	private Long studentCount = 0l;
	private TestNodeStudentData testNodeStudentData;

	public TestNodeStats() {
		super();
		easyQuestionStats = new QuestionStats();
		moderateQuestionStats = new QuestionStats();
		toughQuestionStats = new QuestionStats();
		unknownQuestionStats = new QuestionStats();
	}

	public QuestionStats getUnknownQuestionStats() {
		return unknownQuestionStats;
	}

	public void setUnknownQuestionStats(QuestionStats unknownQuestionStats) {
		this.unknownQuestionStats = unknownQuestionStats;
	}

	public String getTestId() {
		return testId;
	}
	public void setTestId(String testId) {
		this.testId = testId;
	}
	public String getNodename() {
		return nodeName;
	}
	public void setNodename(String nodename) {
		this.nodeName = nodename;
	}
	public QuestionStats getEasyQuestionStats() {
		return easyQuestionStats;
	}
	public void setEasyQuestionStats(QuestionStats easyQuestionStats) {
		this.easyQuestionStats = easyQuestionStats;
	}
	public QuestionStats getModerateQuestionStats() {
		return moderateQuestionStats;
	}
	public void setModerateQuestionStats(QuestionStats moderateQuestionStats) {
		this.moderateQuestionStats = moderateQuestionStats;
	}
	public QuestionStats getToughQuestionStats() {
		return toughQuestionStats;
	}
	public void setToughQuestionStats(QuestionStats toughQuestionStats) {
		this.toughQuestionStats = toughQuestionStats;
	}
	public Long getStudentCount() {
		return studentCount;
	}
	public void setStudentCount(Long studentCount) {
		this.studentCount = studentCount;
	}
	public void incrementStudentCount() {
		this.studentCount++;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public Set<String> getQuestionIds() {
		return questionIds;
	}
	public void setQuestionIds(Set<String> questionIds) {
		this.questionIds = questionIds;
	}
	public void addQuestionIdToQuestionIds(String questionId) {
		if(this.questionIds == null) {
			this.questionIds = new HashSet<>();
			this.questionIds.add(questionId);
		} else if(!questionIds.contains(questionId)) {
			this.questionIds.add(questionId);
		}
	}
	public static class Constants {

		public static final String _ID = "_id";
		public static final String Test_ID = "testId";
		public static final String Node_Name = "nodeName";
		public static final String EASY_QUESTION_STATS = "easyQuestionStats";
		public static final String MODERATE_QUESTION_STATS = "moderateQuestionStats";
		public static final String TOUGH_QUESTION_STATS = "toughQuestionStats";
		public static final String UNKNOWN_QUESTION_STATS = "unknownQuestionStats";
		public static final String STUDENT_COUNT = "studentCount";
		public static final String TEST_NODE_STUDENT_DATA = "testNodeStudentData";

	}

	public TestNodeStudentData getTestNodeStudentData() {
		return testNodeStudentData;
	}

	public void setTestNodeStudentData(TestNodeStudentData testNodeStudentData) {
		this.testNodeStudentData = testNodeStudentData;
	}
}
