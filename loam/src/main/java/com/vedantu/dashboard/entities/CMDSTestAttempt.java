package com.vedantu.dashboard.entities;

import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.dashboard.pojo.ContentInfoMetadata;
import com.vedantu.dashboard.utils.CMDSCommonUtils;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.TestEndType;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptImage;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import com.vedantu.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class CMDSTestAttempt extends ContentInfoMetadata {
	private String testId;
	private String contentInfoId;
	// private String studentId;
	private String teacherId;
	private Long evaluatedViewTime;
	private Long evaluatedTime;
	private Long reportViewed;
	private CMDSAttemptState attemptState;
	private float totalMarks;
	private Float marksAcheived = 0f;
	private Long timeTakenByStudent;
	private String attemptRemarks;

	private List<CMDSTestAttemptImage> imageDetails;

	private Long duration;
	private Long startTime;
	private Long endTime;
	private Long startedAt;
	private Long endedAt;
	private TestEndType testEndType;

	private List<QuestionAnalytics> resultEntries;
	private List<CategoryAnalytics> categoryAnalyticsList;
	private Double percentile;
	private Integer rank;
	private Integer totalStudents;
	private Integer batchAvg;
	private Integer attempted = 0;
	private Integer unattempted = 0;
	private Integer correct = 0;
	private Integer incorrect = 0;
	private float attemptMarksPercent;
	private float testPercentile;


	// Reevaluation parameters
	private Integer reevaluationNumber = 0;
	private Long reevaluationTime;

	public Integer getReevaluationNumber() {
		return reevaluationNumber;
	}

	public void setReevaluationNumber(Integer reevaluationNumber) {
		this.reevaluationNumber = reevaluationNumber;
	}

	public Long getReevaluationTime() {
		return reevaluationTime;
	}

	public void setReevaluationTime(Long reevaluationTime) {
		this.reevaluationTime = reevaluationTime;
	}



	// Marketing related temporary field
	private String marketingRedirectUrl;

	public String getMarketingRedirectUrl() {
		return marketingRedirectUrl;
	}

	public void setMarketingRedirectUrl(String marketingRedirectUrl) {
		this.marketingRedirectUrl = marketingRedirectUrl;
	}

	public CMDSTestAttempt() {
		super();
	}

	public CMDSTestAttempt(CMDSTestAttempt cmdsTestAttempt){
		super();
		this.testId = cmdsTestAttempt.getTestId();
		this.contentInfoId = cmdsTestAttempt.getContentInfoId();
		this.teacherId = cmdsTestAttempt.getTeacherId();
		this.evaluatedViewTime = cmdsTestAttempt.getEvaluatedViewTime();
		this.evaluatedTime = cmdsTestAttempt.getEvaluatedTime();
		this.reportViewed = cmdsTestAttempt.getReportViewed();
		this.attemptState = cmdsTestAttempt.getAttemptState();
		this.totalMarks = cmdsTestAttempt.getTotalMarks();
		this.marksAcheived = cmdsTestAttempt.getMarksAcheived();
		this.timeTakenByStudent = cmdsTestAttempt.getTimeTakenByStudent();
		this.attemptRemarks = cmdsTestAttempt.getAttemptRemarks();
		this.resultEntries = cmdsTestAttempt.getResultEntries();
		this.imageDetails = cmdsTestAttempt.getImageDetails();
		this.percentile = cmdsTestAttempt.getPercentile();
		this.rank = cmdsTestAttempt.getRank();
		this.totalStudents = cmdsTestAttempt.getTotalStudents();
		this.batchAvg = cmdsTestAttempt.getBatchAvg();
		this.attempted = cmdsTestAttempt.getAttempted();
		this.unattempted = cmdsTestAttempt.getUnattempted();
		this.correct = cmdsTestAttempt.getCorrect();
		this.incorrect = cmdsTestAttempt.getIncorrect();
		this.attemptMarksPercent = cmdsTestAttempt.getAttemptMarksPercent();
		this.testPercentile = cmdsTestAttempt.getTestPercentile();
		this.reevaluationNumber = cmdsTestAttempt.getReevaluationNumber();
		this.reevaluationTime = cmdsTestAttempt.getReevaluationTime();
	 	this.duration = cmdsTestAttempt.getDuration();
		this.startTime = cmdsTestAttempt.getStartTime();
		this.endTime = cmdsTestAttempt.getEndTime();
		this.startedAt = cmdsTestAttempt.getStartedAt();
		this.endedAt = cmdsTestAttempt.getEndedAt();
		this.testEndType = cmdsTestAttempt.getTestEndType();
		this.categoryAnalyticsList = cmdsTestAttempt.getCategoryAnalyticsList();
	}


	public CMDSTestAttempt(String userId) {
		super();
	}

	public CMDSTestAttempt(String testId, String contentInfoId, String studentId, String teacherId,
						   Long evaluatedViewTime, Long evaluatedTime, Long reportViewed, CMDSAttemptState attemptState,
						   float totalMarks, Float marksAcheived, Long timeTakenByStudent, String attemptRemarks,
						   List<QuestionAnalytics> resultEntries, List<CMDSTestAttemptImage> imageDetails) {
		super();
		this.testId = testId;
		this.contentInfoId = contentInfoId;
		// this.studentId = studentId;
		this.teacherId = teacherId;
		this.evaluatedViewTime = evaluatedViewTime;
		this.evaluatedTime = evaluatedTime;
		this.reportViewed = reportViewed;
		this.attemptState = attemptState;
		this.totalMarks = totalMarks;
		this.marksAcheived = marksAcheived;
		this.timeTakenByStudent = timeTakenByStudent;
		this.attemptRemarks = attemptRemarks;
		this.resultEntries = resultEntries;
		this.imageDetails = imageDetails;
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public String getContentInfoId() {
		return contentInfoId;
	}

	public void setContentInfoId(String contentInfoId) {
		this.contentInfoId = contentInfoId;
	}

	public Long getEvaluatedViewTime() {
		return evaluatedViewTime;
	}

	public void setEvaluatedViewTime(Long evaluatedViewTime) {
		this.evaluatedViewTime = evaluatedViewTime;
	}

	public Long getEvaluatedTime() {
		return evaluatedTime;
	}

	public void setEvaluatedTime(Long evaluatedTime) {
		this.evaluatedTime = evaluatedTime;
	}

	public Long getReportViewed() {
		return reportViewed;
	}

	public void setReportViewed(Long reportViewed) {
		this.reportViewed = reportViewed;
	}

	public CMDSAttemptState getAttemptState() {
		return attemptState;
	}

	public void setAttemptState(CMDSAttemptState attemptState) {
		this.attemptState = attemptState;
	}

	public float getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(float totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Float getMarksAcheived() {
		return marksAcheived;
	}

	public void setMarksAcheived(Float marksAcheived) {
		this.marksAcheived = marksAcheived;
	}

	public Long getTimeTakenByStudent() {
		return timeTakenByStudent;
	}

	public void setTimeTakenByStudent(Long timeTakenByStudent) {
		this.timeTakenByStudent = timeTakenByStudent;
	}

	public List<QuestionAnalytics> getResultEntries() {
		return resultEntries;
	}

	public void setResultEntries(List<QuestionAnalytics> resultEntries) {
		this.resultEntries = resultEntries;
	}

	public List<CMDSTestAttemptImage> getImageDetails() {
		return imageDetails;
	}

	public void setImageDetails(List<CMDSTestAttemptImage> imageDetails) {
		this.imageDetails = imageDetails;
	}

//	public String getStudentId() {
//		return studentId;
//	}
//
//	public void setStudentId(String studentId) {
//		this.studentId = studentId;
//	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getAttemptRemarks() {
		return attemptRemarks;
	}

	public void setAttemptRemarks(String attemptRemarks) {
		this.attemptRemarks = attemptRemarks;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Long startedAt) {
		this.startedAt = startedAt;
	}

	public Long getEndedAt() {
		return endedAt;
	}

	public void setEndedAt(Long endedAt) {
		this.endedAt = endedAt;
	}

	public TestEndType getTestEndType() {
		return testEndType;
	}

	public void setTestEndType(TestEndType testEndType) {
		this.testEndType = testEndType;
	}

	public List<CategoryAnalytics> getCategoryAnalyticsList() {
		return categoryAnalyticsList;
	}

	public void setCategoryAnalyticsList(List<CategoryAnalytics> categoryAnalyticsList) {
		this.categoryAnalyticsList = categoryAnalyticsList;
	}

	public Double getPercentile() {
		return percentile;
	}

	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Integer getAttempted() {
		return attempted;
	}

	public void setAttempted(Integer attempted) {
		this.attempted = attempted;
	}

	public Integer getUnattempted() {
		return unattempted;
	}

	public void setUnattempted(Integer unattempted) {
		this.unattempted = unattempted;
	}

	public Integer getCorrect() {
		return correct;
	}

	public void setCorrect(Integer correct) {
		this.correct = correct;
	}

	public Integer getIncorrect() {
		return incorrect;
	}

	public void setIncorrect(Integer incorrect) {
		this.incorrect = incorrect;
	}

	public Integer getTotalStudents() {
		return totalStudents;
	}

	public void setTotalStudents(Integer totalStudents) {
		this.totalStudents = totalStudents;
	}

	public Integer getBatchAvg() {
		return batchAvg;
	}

	public void setBatchAvg(Integer batchAvg) {
		this.batchAvg = batchAvg;
	}

	public float getAttemptMarksPercent() {
		return attemptMarksPercent;
	}

	public void setAttemptMarksPercent(float attemptMarksPercent) {
		this.attemptMarksPercent = attemptMarksPercent;
	}

	public float getTestPercentile() {
		return testPercentile;
	}

	public void setTestPercentile(float testPercentile) {
		this.testPercentile = testPercentile;
	}

	public void removeImageUrls() {
		if (!CollectionUtils.isEmpty(this.imageDetails)) {
			for (CMDSTestAttemptImage image : imageDetails) {
				image.setEvaluatedPublicUrl(null);
				image.setPublicUrl(null);
			}
		}
	}

	@Override
	public String toString() {
		return "CMDSTestAttempt [testId=" + testId + ", contentInfoId=" + contentInfoId + ", teacherId=" + teacherId
				+ ", evaluatedViewTime=" + evaluatedViewTime + ", evaluatedTime=" + evaluatedTime + ", reportViewed="
				+ reportViewed + ", attemptState=" + attemptState + ", totalMarks=" + totalMarks + ", marksAcheived="
				+ marksAcheived + ", timeTakenByStudent=" + timeTakenByStudent + ", attemptRemarks=" + attemptRemarks
				+ ", resultEntries=" + resultEntries + ", imageDetails=" + imageDetails + ", categoryAnalyticsList="
				+ categoryAnalyticsList + "]";
	}
}
