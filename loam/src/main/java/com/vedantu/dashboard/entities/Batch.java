package com.vedantu.dashboard.entities;


import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import java.util.*;

public class Batch extends AbstractMongoStringIdEntity {

    /**
     * @return the modularBatch
     */
    public boolean isModularBatch() {
        return modularBatch;
    }

    /**
     * @param modularBatch the modularBatch to set
     */
    public void setModularBatch(boolean modularBatch) {
        this.modularBatch = modularBatch;
    }
    private String courseId;
    private Long startTime;
    private Long endTime;
    private int maxEnrollment = 5;
    private int minEnrollment = 1;
    private Set<String> teacherIds;
    private String currency = "INR";
    private int purchasePrice;
    private int cutPrice = -1;
    private String planString;

    private VisibilityState visibilityState;
    private EntityStatus batchState;
    private String demoVideoUrl;
    private boolean isHalfScheduled;
    //	private List<Agenda> agenda;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.GTM; // If
    private Integer registrationFee;
    private List<BoardTeacherPair> boardTeacherPairs;
    private Long duration;
    private String groupName;
    private String groupSlug;
    private String preferredSeat;
    private List<SessionPlanPojo> sessionPlan;
    private List<CourseTerm> searchTerms;
    private String whatsappJoinUrl;
    private boolean contentBatch = false;
    private boolean modularBatch = false;
    private String acadMentorId;
    private boolean recordedVideo=false;
    private Long lastPurchaseDate;
    private String orgId;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Batch() {
        super();
    }

    public Batch(String id, String courseId, Long startTime, Long endTime, int maxEnrollment, int minEnrollment,
                 Set<String> teacherIds, String currency, int purchasePrice, int cutPrice, 
                 String planString, VisibilityState visibilityState, EntityStatus batchState, String demoVideoUrl,
                 boolean isHalfScheduled, List<Agenda> agenda,
                 OTFSessionToolType sessionToolType,
                 Integer registrationFee, List<BoardTeacherPair> boardTeacherPairs, String orgId) {
        super();
        super.setId(id);
        this.courseId = courseId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.maxEnrollment = maxEnrollment;
        this.minEnrollment = minEnrollment;
        this.teacherIds = teacherIds;
        this.currency = currency;
        this.purchasePrice = purchasePrice;
        this.cutPrice = cutPrice;
        this.planString = planString;
        this.visibilityState = visibilityState;
        this.batchState = batchState;
        this.demoVideoUrl = demoVideoUrl;
        this.isHalfScheduled = isHalfScheduled;
        this.sessionToolType = sessionToolType == null ? OTFSessionToolType.GTM : sessionToolType;
        this.registrationFee = registrationFee;
        this.orgId = orgId;
        if (agenda != null) {
//			this.agenda = agenda;
        }
        this.boardTeacherPairs=boardTeacherPairs;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public int getMaxEnrollment() {
        if (maxEnrollment <= 0) {
            return 5;
        }

        return maxEnrollment;
    }

    public void setMaxEnrollment(int maxEnrollment) {
        this.maxEnrollment = maxEnrollment;
    }

    public int getMinEnrollment() {
        if (minEnrollment <= 0) {
            return 1;
        }

        return minEnrollment;
    }

    public void setMinEnrollment(int minEnrollment) {
        this.minEnrollment = minEnrollment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(int purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public int getCutPrice() {
        return cutPrice;
    }

    public void setCutPrice(int cutPrice) {
        this.cutPrice = cutPrice;
    }

    public Set<String> getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(Set<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    public String getPlanString() {
        return planString;
    }

    public void setPlanString(String planString) {
        this.planString = planString;
    }

    public EntityStatus getBatchState() {
        return batchState;
    }

    public void setBatchState(EntityStatus batchState) {
        this.batchState = batchState;
    }

    public String getDemoVideoUrl() {
        return demoVideoUrl;
    }

    public void setDemoVideoUrl(String demoVideoUrl) {
        this.demoVideoUrl = demoVideoUrl;
    }

    public VisibilityState getVisibilityState() {
        return visibilityState;
    }

    public void setVisibilityState(VisibilityState visibilityState) {
        this.visibilityState = visibilityState;
    }

//	public List<Agenda> getAgenda() {
//		return agenda;
//	}
//
//	public void setAgenda(List<Agenda> agenda) {
//		this.agenda = agenda;
//	}

    public boolean isHalfScheduled() {
        return isHalfScheduled;
    }

    public void setHalfScheduled(boolean isHalfScheduled) {
        this.isHalfScheduled = isHalfScheduled;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public Integer getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(Integer registrationFee) {
        this.registrationFee = registrationFee;
    }


    public List<BoardTeacherPair> getBoardTeacherPairs() {
        return boardTeacherPairs;
    }

    public void setBoardTeacherPairs(List<BoardTeacherPair> boardTeacherPairs) {
        this.boardTeacherPairs = boardTeacherPairs;
    }

//	public void setAgendaPojos(List<AgendaPojo> agendaPojos) {
//		List<Agenda> agendas = new ArrayList<>();
//		if (agendaPojos == null) {
//			this.agenda = null;
//		} else {
//			for (AgendaPojo agendaPojo : agendaPojos) {
//				// Handle null sessions
//				if (AgendaType.SESSION.equals(agendaPojo.getType()) && ((agendaPojo.getSessionPOJO() == null)
//						|| StringUtils.isEmpty(agendaPojo.getSessionPOJO().getId()))) {
//					continue;
//				}
//
//				Agenda agenda = new Agenda(agendaPojo);
//				agendas.add(agenda);
//			}
//			this.agenda = agendas;
//		}
//	}

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupSlug() {
        return groupSlug;
    }

    public void setGroupSlug(String groupSlug) {
        this.groupSlug = groupSlug;
    }

    public String getPreferredSeat() {
        return preferredSeat;
    }

    public void setPreferredSeat(String preferredSeat) {
        this.preferredSeat = preferredSeat;
    }

    public List<SessionPlanPojo> getSessionPlan() {
        return sessionPlan;
    }

    public void setSessionPlan(List<SessionPlanPojo> sessionPlan) {
        this.sessionPlan = sessionPlan;
    }

    public String getWhatsappJoinUrl() {
        return whatsappJoinUrl;
    }

    public void setWhatsappJoinUrl(String whatsappJoinUrl) {
        this.whatsappJoinUrl = whatsappJoinUrl;
    }

    public boolean isRecordedVideo() {
        return recordedVideo;
    }

    public void setRecordedVideo(boolean recordedVideo) {
        this.recordedVideo = recordedVideo;
    }

    public BatchBasicInfo toBatchBasicInfo(Batch batch, Course course,
                                           Map<String, UserBasicInfo> userMap, Map<Long, String> boardSubjectMap) {
        BatchBasicInfo batchBasicInfo = new BatchBasicInfo();
        if (batch != null) {
            batchBasicInfo.setBatchId(batch.getId());
            batchBasicInfo.setVisibilityState(batch.getVisibilityState());
            batchBasicInfo.setPlanString(batch.getPlanString());
            batchBasicInfo.setPurchasePrice(batch.getPurchasePrice());
            batchBasicInfo.setCutPrice(batch.getCutPrice());
            batchBasicInfo.setStartTime(batch.getStartTime());
            batchBasicInfo.setEndTime(batch.getEndTime());
            batchBasicInfo.setMaxEnrollment(batch.getMaxEnrollment());
            batchBasicInfo.setMinEnrollment(batch.getMinEnrollment());
            batchBasicInfo.setHalfScheduled(batch.isHalfScheduled());
            batchBasicInfo.setCreationTime(batch.getCreationTime());
            batchBasicInfo.setLastUpdated(batch.getLastUpdated());
            batchBasicInfo.setBatchState(batch.getBatchState());
            batchBasicInfo.setTeacherIds(batch.getTeacherIds());
            batchBasicInfo.setSearchTerms(batch.getSearchTerms());
            batchBasicInfo.setContentBatch(batch.isContentBatch());
            batchBasicInfo.setModularBatch(batch.isModularBatch());
            batchBasicInfo.setAcadMentorId(batch.getAcadMentorId());
            batchBasicInfo.setCourseId(batch.getCourseId());
            batchBasicInfo.setRecordedVideo(batch.isRecordedVideo());
            if (userMap != null) {
                List<UserBasicInfo> _teachers = new ArrayList<>();
                Set<String> userIds = batch.getTeacherIds();
                if (userIds != null && !userIds.isEmpty()) {
                    for (String userId : userIds) {
                        if (userMap.containsKey(userId)) {
                            _teachers.add(userMap.get(userId));
                        }
                    }
                }

                batchBasicInfo.setTeachers(_teachers);


                //fetching board infos
//                                Set<Long> boardIds = new HashSet<>();
//                                List<BoardTeacherPair> boardTeacherPairs=course.getBoardTeacherPairs();
//                                if(ArrayUtils.isNotEmpty(boardTeacherPairs)){
//                                    for(BoardTeacherPair pair:boardTeacherPairs){
//                                        boardIds.add(pair.getBoardId());
//                                    }
//                                }

            }
            batchBasicInfo.setSessionToolType(batch.getSessionToolType());
            if(course != null){
                batchBasicInfo.setCourseInfo(course.toCourseBasicInfo(course, userMap, null));
            }
            batchBasicInfo.setRegistrationFee(batch.getRegistrationFee());

            batchBasicInfo.setDuration(batch.getDuration());
            batchBasicInfo.setGroupName(batch.getGroupName());
            batchBasicInfo.setGroupSlug(batch.getGroupSlug());
            batchBasicInfo.setSessionPlan(batch.getSessionPlan());
            if(ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())){
                for(BoardTeacherPair pair:batch.getBoardTeacherPairs()){
                    if(boardSubjectMap!=null&&boardSubjectMap.containsKey(pair.getBoardId())){
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if(ArrayUtils.isNotEmpty(pair.getTeachers())){
                        for(BoardTeacher teacher: pair.getTeachers()){
                            if (teacher.getTeacherId() != null && userMap != null
                                    && userMap.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userMap.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            batchBasicInfo.setBoardTeacherPairs(batch.getBoardTeacherPairs());
            batchBasicInfo.setOrgId(batch.getOrgId());
        }
        return batchBasicInfo;
    }

    public BatchInfo toBatchInfo(Batch batch, Map<String, UserBasicInfo> userMap,
                                 Map<Long, String> boardSubjectMap) {
        // TODO use reflection to map properties, this is too error prone
        BatchInfo batchInfo = new BatchInfo();
        if (batch != null) {
            batchInfo.setBatchId(batch.getId());
            batchInfo.setVisibilityState(batch.getVisibilityState());
            batchInfo.setPlanString(batch.getPlanString());
            batchInfo.setPurchasePrice(batch.getPurchasePrice());
            batchInfo.setCutPrice(batch.getCutPrice());
            batchInfo.setStartTime(batch.getStartTime());
            batchInfo.setEndTime(batch.getEndTime());
            batchInfo.setMaxEnrollment(batch.getMaxEnrollment());
            batchInfo.setMinEnrollment(batch.getMinEnrollment());
            batchInfo.setHalfScheduled(batch.isHalfScheduled());
            batchInfo.setCreationTime(batch.getCreationTime());
            batchInfo.setLastUpdated(batch.getLastUpdated());
            batchInfo.setContentBatch(batch.isContentBatch());
            batchInfo.setModularBatch(batch.isModularBatch());
            batchInfo.setAcadMentorId(batch.getAcadMentorId());
            batchInfo.setWhatsappJoinUrl(batch.getWhatsappJoinUrl());
            if (userMap != null) {
                List<UserBasicInfo> _teachers = new ArrayList<>();
                Set<String> userIds = batch.getTeacherIds();
                if (userIds != null && !userIds.isEmpty()) {
                    for (String userId : userIds) {
                        if (userMap.containsKey(userId)) {
                            _teachers.add(userMap.get(userId));
                        }
                    }
                }

             
                batchInfo.setTeachers(_teachers);
            }
            batchInfo.setSessionToolType(batch.getSessionToolType());
            batchInfo.setCurrency(batch.getCurrency());
            batchInfo.setBatchState(batch.getBatchState());
            batchInfo.setDemoVideoUrl(batch.getDemoVideoUrl());
            batchInfo.setRegistrationFee(batch.getRegistrationFee());
            batchInfo.setDuration(batch.getDuration());
            batchInfo.setGroupName(batch.getGroupName());
            batchInfo.setGroupSlug(batch.getGroupSlug());
            batchInfo.setSessionPlan(batch.getSessionPlan());
            batchInfo.setSearchTerms(batch.getSearchTerms());
            if(ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())){
                for(BoardTeacherPair pair:batch.getBoardTeacherPairs()){
                    if(boardSubjectMap!=null&&boardSubjectMap.containsKey(pair.getBoardId())){
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if(ArrayUtils.isNotEmpty(pair.getTeachers())){
                        for(BoardTeacher teacher: pair.getTeachers()){
                            if (teacher.getTeacherId() != null && userMap != null
                                    && userMap.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userMap.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            batchInfo.setBoardTeacherPairs(batch.getBoardTeacherPairs());
            batchInfo.setRecordedVideo(batch.isRecordedVideo());
        }
        return batchInfo;
    }

    public static Batch toBatch(BatchReq req) {
        Batch batch = new Batch(req.getId(), req.getCourseId(), req.getStartTime(),
                req.getEndTime(), req.getMaxEnrollment(), req.getMinEnrollment(),
                req.getTeacherIds(), req.getCurrency(),
                req.getPurchasePrice(), req.getCutPrice(),
                req.getPlanString(), req.getVisibilityState(), req.getBatchState(), req.getDemoVideoUrl(),
                req.isHalfScheduled(), null, req.getSessionToolType(),req.getRegistrationFee(),
                req.getBoardTeacherPairs(),req.getOrgId());
//                batch.setAgendaPojos(req.getAgenda());
        Set<String> teacherIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(req.getBoardTeacherPairs())) {
            for (BoardTeacherPair pair : req.getBoardTeacherPairs()) {
                if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                    for (BoardTeacher teacher : pair.getTeachers()) {
                        if (teacher.getTeacherId() != null) {
                            teacherIds.add(teacher.getTeacherId().toString());
                        }
                    }
                }
            }
        }
        batch.setModularBatch(req.isModularBatch());
        batch.setContentBatch(req.isContentBatch());
        batch.setAcadMentorId(req.getAcadMentorId());
        batch.setDuration(req.getDuration());
        batch.setGroupName(req.getGroupName());
        batch.setPreferredSeat(req.getPreferredSeat());
        batch.setTeacherIds(teacherIds);
        batch.setSessionPlan(req.getSessionPlan());
        batch.setSearchTerms(req.getSearchTerms());
        batch.setWhatsappJoinUrl(req.getWhatsappJoinUrl());
        batch.setRecordedVideo(req.isRecordedVideo());
        return batch;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String TEACHER_IDS = "teacherIds";
        public static final String ENROLLED_STUDENTS = "enrolledStudents";
        public static final String PURCHASE_PRICE = "purchasePrice";
        public static final String COURSE_ID = "courseId";
        public static final String VISIBILITY_STATE = "visibilityState";
        public static final String START_TIME = "startTime";
        public static final String BATCH_STATE = "batchState";
        public static final String END_TIME = "endTime";
        public static final String CUT_PRICE = "cutPrice";
        public static final String AGENDA = "agenda";
        public static final String PLAN_STRING = "planString";
        public static final String GROUP_NAME = "groupName";
        public static final String SEARCH_TERMS = "searchTerms";
        public static final String CONTENT_BATCH = "contentBatch";
        public static final String MODULAR_BATCH = "modularBatch";
        public static final String AM_ID = "acadMentorId";
        public static final String SESSION_PLAN = "sessionPlan";
        public static final String BOARD_TEACHER_PAIRS="boardTeacherPairs";

    }

    public List<CourseTerm> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<CourseTerm> searchTerms) {
        this.searchTerms = searchTerms;
    }

    /**
     * @return the contentBatch
     */
    public boolean isContentBatch() {
        return contentBatch;
    }

    /**
     * @param contentBatch the contentBatch to set
     */
    public void setContentBatch(boolean contentBatch) {
        this.contentBatch = contentBatch;
    }

    /**
     * @return the acadMentorId
     */
    public String getAcadMentorId() {
        return acadMentorId;
    }

    /**
     * @param acadMentorId the acadMentorId to set
     */
    public void setAcadMentorId(String acadMentorId) {
        this.acadMentorId = acadMentorId;
    }

    public Long getLastPurchaseDate() {
        return lastPurchaseDate;
    }

    public void setLastPurchaseDate(Long lastPurchaseDate) {
        this.lastPurchaseDate = lastPurchaseDate;
    }
}
