package com.vedantu.dashboard.entities;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dashboard.pojo.TestContentInfoMetadata;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "ContentInfo")
@CompoundIndexes({
    @CompoundIndex(name = "testAttempt_evaluatedTime", def = "{'metadata.testAttempts.0.evaluatedTime': 1, 'contentType' : 1, 'contentState' : 1, 'contentInfoType' : 1}}", background = true)
})
public class ContentInfo extends AbstractMongoStringIdEntity
{

    @Indexed(background = true)
    private String studentId;

    private ContentType contentType = ContentType.TEST;
    private ContentInfoType contentInfoType = ContentInfoType.SUBJECTIVE;
    private ContentState contentState = ContentState.SHARED;

    private String subject;

    private TestContentInfoMetadata metadata;

    private Long boardId;
    private Set<Long> boardIds = new HashSet<>();

    //TODO: Following fields are added for backward compatibility of moodle and youscore, Do not use it in future
    private String testId;

    private Long attemptedTime;
    private Long evaulatedTime;

    private String contentTitle;


    public ContentInfo()
    {
        super();
    }


    public TestContentInfoMetadata getMetadata()
    {
        return metadata;
    }

    public void setMetadata(TestContentInfoMetadata metadata)
    {
        this.metadata = metadata;
    }

    public String getStudentId()
    {
        return studentId;
    }

    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }

    public ContentType getContentType()
    {
        return contentType;
    }

    public void setContentType(ContentType contentType)
    {
        this.contentType = contentType;
    }

    public ContentInfoType getContentInfoType()
    {
        return contentInfoType;
    }

    public void setContentInfoType(ContentInfoType contentInfoType)
    {
        this.contentInfoType = contentInfoType;
    }



    public ContentState getContentState()
    {
        return contentState;
    }

    public void setContentState(ContentState contentState)
    {
        this.contentState = contentState;
    }

    public String getTestId()
    {
        return testId;
    }

    public void setTestId(String testId)
    {
        this.testId = testId;
    }

    public Long getBoardId()
    {
        return boardId;
    }

    public void setBoardId(Long boardId)
    {
        this.boardId = boardId;
    }

    public Long getAttemptedTime()
    {
        return attemptedTime;
    }

    public void setAttemptedTime(Long attemptedTime)
    {
        this.attemptedTime = attemptedTime;
    }

    public Long getEvaulatedTime()
    {
        return evaulatedTime;
    }

    public void setEvaulatedTime(Long evaulatedTime)
    {
        this.evaulatedTime = evaulatedTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContentTitle()
    {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle)
    {
        this.contentTitle = contentTitle;
    }

    @Override
    public String toString()
    {
        return "ContentInfo{"
                + "studentId='" + studentId + '\''
                + ", contentType=" + contentType
                + ", contentState=" + contentState
                + ", metadata=" + metadata
                + '}';
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants
    {

        public static final String STUDENT_ID = "studentId";
        public static final String SUBJECT = "subject";
        public static final String CONTENT_TYPE = "contentType";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String CONTENT_STATE = "contentState";
        public static final String TEST_ID = "testId";
        public static final String METADATA = "metadata";
        public static final String BOARD_ID = "boardId";
        public static final String BOARD_IDS = "boardIds";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME = "metadata.testAttempts.0.evaluatedTime";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_ENDED_TIME = "metadata.testAttempts.0.endedAt";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_RESULT_ENTRIES = "metadata.testAttempts.0.resultEntries";
        public static final String METADATA_DURATION = "metadata.duration";
        public static final String EVALUATED_TIME = "evaulatedTime";
        public static final String ATTEMPTED_TIME = "attemptedTime";
    }

    /**
     * @return the boardIds
     */
    public Set<Long> getBoardIds()
    {
        return boardIds;
    }

    /**
     * @param boardIds the boardIds to set
     */
    public void setBoardIds(Set<Long> boardIds)
    {
        this.boardIds = boardIds;
    }

}
