/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.entities.mongo;

import com.vedantu.dashboard.pojo.DoubtSolverPojo;
import com.vedantu.dashboard.pojo.DoubtStateChangePojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
@Document(collection = "DoubtStats")
public class DoubtStats extends AbstractTargetTopicEntity {
    
    private String grade;
    private String board;
    private Long boardId;
    private String target;
    @Indexed(background = true, unique = true)
    private Long doubtId;
    @Indexed(background = true)
    private String studentId;
    private Set<String> membersList = new HashSet<>();
    private List<DoubtStateChangePojo> doubtStateChanges;
    private List<DoubtSolverPojo> doubtSolverPojoList = new ArrayList<>();
    private Long lastActivityTime;
    private boolean closed = false;
    private boolean userFeedback = false;
    private String picUrl;
    private String picFileName;
    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @param grade the grade to set
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * @return the board
     */
    public String getBoard() {
        return board;
    }

    /**
     * @param board the board to set
     */
    public void setBoard(String board) {
        this.board = board;
    }

    /**
     * @return the boardId
     */
    public Long getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the membersList
     */
    public Set<String> getMembersList() {
        return membersList;
    }

    /**
     * @param membersList the membersList to set
     */
    public void setMembersList(Set<String> membersList) {
        this.membersList = membersList;
    }

    /**
     * @return the doubtStateChanges
     */
    public List<DoubtStateChangePojo> getDoubtStateChanges() {
        return doubtStateChanges;
    }

    /**
     * @param doubtStateChanges the doubtStateChanges to set
     */
    public void setDoubtStateChanges(List<DoubtStateChangePojo> doubtStateChanges) {
        this.doubtStateChanges = doubtStateChanges;
    }

    /**
     * @return the lastActivityTime
     */
    public Long getLastActivityTime() {
        return lastActivityTime;
    }

    /**
     * @param lastActivityTime the lastActivityTime to set
     */
    public void setLastActivityTime(Long lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    /**
     * @return the doubtSolverPojoList
     */
    public List<DoubtSolverPojo> getDoubtSolverPojoList() {
        return doubtSolverPojoList;
    }

    /**
     * @param doubtSolverPojoList the doubtSolverPojoList to set
     */
    public void setDoubtSolverPojoList(List<DoubtSolverPojo> doubtSolverPojoList) {
        this.doubtSolverPojoList = doubtSolverPojoList;
    }

    /**
     * @return the closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed the closed to set
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the userFeedback
     */
    public boolean isUserFeedback() {
        return userFeedback;
    }

    /**
     * @param userFeedback the userFeedback to set
     */
    public void setUserFeedback(boolean userFeedback) {
        this.userFeedback = userFeedback;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
            public static final String DOUBT_ID = "doubtId";
            public static final String MEMBERS_LIST = "membersList";
            public static final String LAST_ACTIVITY_TIME = "lastActivityTime";
    }    

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the picFileName
     */
    public String getPicFileName() {
        return picFileName;
    }

    /**
     * @param picFileName the picFileName to set
     */
    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }
    
    
}
