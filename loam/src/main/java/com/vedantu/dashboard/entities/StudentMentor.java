package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.pojo.SupermentorResponse.StudentInfoPojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "StudentMentor")
public class StudentMentor extends AbstractMongoStringIdEntity{
    private Long acadMentorId;
    private Long studentId;

    // Student Data
    private String studentFullName;
    private String studentProfilePicUrl;
    private StudentInfoPojo studentInfo = new StudentInfoPojo();

    // Teacher Data
    private String teacherEmail;
    private String teacherFullName;
    private String teacherProfilePicUrl;

    public Long getAcadMentorId() {
        return acadMentorId;
    }

    public void setAcadMentorId(Long acadMentorId) {
        this.acadMentorId = acadMentorId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getStudentFullName() {
        return studentFullName;
    }

    public void setStudentFullName(String studentFullName) {
        this.studentFullName = studentFullName;
    }

    public String getStudentProfilePicUrl() {
        return studentProfilePicUrl;
    }

    public void setStudentProfilePicUrl(String studentProfilePicUrl) {
        this.studentProfilePicUrl = studentProfilePicUrl;
    }

    public StudentInfoPojo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfoPojo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherFullName() {
        return teacherFullName;
    }

    public void setTeacherFullName(String teacherFullName) {
        this.teacherFullName = teacherFullName;
    }

    public String getTeacherProfilePicUrl() {
        return teacherProfilePicUrl;
    }

    public void setTeacherProfilePicUrl(String teacherProfilePicUrl) {
        this.teacherProfilePicUrl = teacherProfilePicUrl;
    }


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String ACADMENTORID = "acadMentorId";
        public static final String STUDENTID = "studentId";
        public static final String STUDENT_FULL_NAME= "studentFullName";
    }
}
