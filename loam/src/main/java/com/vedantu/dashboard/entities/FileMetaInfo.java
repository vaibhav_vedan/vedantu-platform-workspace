package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.enums.CMDSEntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Document(collection = "FileMetaInfo")
public class FileMetaInfo extends AbstractCMDSEntity {

	public Map<String, String> tags;
	@Indexed(background = true)
	public String fileId;
	public String name;
	public CMDSEntityType type;
	public String id;
	public long size;

	public FileMetaInfo() {

		super();
		this.fileId = null;
		tags = new HashMap<String, String>();
		size = -1;

	}

	public FileMetaInfo(String fileId) {

		super();
		this.fileId = fileId;
		tags = new HashMap<String, String>();
		size = -1;

	}

	public void add(String key, String value) {

		tags.put(key, value);
	}

	public void add(Map<String, String> tags) {

		if (tags != null) {
			tags.putAll(tags);
		}
	}

	public String getFileId() {

		return fileId;
	}

	public void setFileId(String fileId) {

		this.fileId = fileId;
	}

	public Map<String, String> getFileTags() {
		return tags;
	}

	public void setTags(Map<String, String> tag) {

		this.tags = tag;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public long getSize() {

		return size;
	}

	public void setSize(long size) {

		this.size = size;
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String FILE_ID = "fileId";
	}
}
