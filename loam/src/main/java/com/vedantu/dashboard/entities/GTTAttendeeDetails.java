package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.managers.GTTAttendeeDetailsManager;
import com.vedantu.dashboard.pojo.GTTGetRegistrantDetailsRes;
import com.vedantu.dashboard.pojo.GTTRegisterAttendeeRes;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.dashboard.pojo.HotspotNumbers;
import com.vedantu.dashboard.pojo.QuizNumbers;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "GTTAttendeeDetails")
public class GTTAttendeeDetails extends AbstractMongoStringIdEntity
{
    private String userId;
    private String sessionId;
    private List<Long> joinTimes = new ArrayList<>();
    private AttendeeContext attendeeType;
    private String enrollmentId;
    private Long sessionStartTime;
    private Long sessionEndTime;

    private QuizNumbers quizNumbers;
    private HotspotNumbers hotspotNumbers;

    public GTTAttendeeDetails()
    {
        super();
    }

    public GTTAttendeeDetails(String sessionId, String userId, String enrollmentId)
    {
        super();
        this.userId = userId;
        this.sessionId = sessionId;
        this.enrollmentId = enrollmentId;
    }

    public GTTAttendeeDetails(GTTGetRegistrantDetailsRes registrantDetailsRes, String organizerAccessToken,
                              String organizerKey, OTFSession oTFSession)
    {
        super();
        this.userId = GTTAttendeeDetailsManager.extractUserId(registrantDetailsRes.getEmail());
        this.sessionId = oTFSession.getId();
    }

    public GTTAttendeeDetails(GTTRegisterAttendeeRes gttRegisterAttendeeRes, String organizerAccessToken,
                              String organizerKey, OTFSession oTFSession, String userId)
    {
        super();
        this.userId = userId;
        this.sessionId = oTFSession.getId();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<Long> getJoinTimes() {
        return joinTimes;
    }

    public void setJoinTimes(List<Long> joinTimes) {
        this.joinTimes = joinTimes;
    }

    public AttendeeContext getAttendeeType() {
        return attendeeType;
    }

    public void setAttendeeType(AttendeeContext attendeeType) {
        this.attendeeType = attendeeType;
    }

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public void addJoinTime() {
        if (this.joinTimes == null) {
            this.joinTimes = new ArrayList<>();
        }

        this.joinTimes.add(System.currentTimeMillis());
    }

    /**
     * @return the sessionStartTime
     */
    public Long getSessionStartTime() {
        return sessionStartTime;
    }

    /**
     * @param sessionStartTime the sessionStartTime to set
     */
    public void setSessionStartTime(Long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    /**
     * @return the sessionEndTime
     */
    public Long getSessionEndTime() {
        return sessionEndTime;
    }

    /**
     * @param sessionEndTime the sessionEndTime to set
     */
    public void setSessionEndTime(Long sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public QuizNumbers getQuizNumbers() {
        return quizNumbers;
    }

    public void setQuizNumbers(QuizNumbers quizNumbers) {
        this.quizNumbers = quizNumbers;
    }

    public HotspotNumbers getHotspotNumbers() {
        return hotspotNumbers;
    }

    public void setHotspotNumbers(HotspotNumbers hotspotNumbers) {
        this.hotspotNumbers = hotspotNumbers;
    }


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String SESSION_ID = "sessionId";
        public static final String JOIN_TIMES = "joinTimes";
        public static final String ENROLLMENT_ID = "enrollmentId";
        public static final String ATTENDEE_TYPE = "attendeeType";
        public static final String OTF_SESSION_END_TIME = "sessionEndTime";
        public static final String OTF_SESSION_START_TIME = "sessionStartTime";
    }

}
