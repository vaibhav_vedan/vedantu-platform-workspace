package com.vedantu.dashboard.entities;

import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.OTFWebinarSession;
import com.vedantu.onetofew.request.OTFWebinarSessionReq;
import com.vedantu.dashboard.enums.OTMServiceProvider;
import com.vedantu.dashboard.managers.OTFSessionManager;
import com.vedantu.dashboard.pojo.GTMCreateSessionRes;
import com.vedantu.dashboard.pojo.OTMSessionConfig;
import com.vedantu.scheduling.pojo.Pollsdata;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.dashboard.request.AddWebinarSessionReq;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.DBUtils;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "OTFSession")
@CompoundIndexes({@CompoundIndex(name = "endTime", def = "{'endTime': 1}", background = true)})
public class OTFSession extends AbstractMongoStringIdEntity {

    private Long boardId;// subject
    private Long startTime;
    private String title;

    @Indexed(background = true)
    private Long endTime;
    private Integer uniqueStudentAttendants;

    public OTFSession() {
        super();
    }

    public OTFSession(OTFWebinarSession webinarSession, OTFWebinarSessionReq req) {
        super();
        this.title = webinarSession.getTitle();
        this.startTime = webinarSession.getStartTime();
        this.endTime = webinarSession.getEndTime();
    }

    public OTFSession(AddWebinarSessionReq webinarSession) {
        super();
        this.title = webinarSession.getTitle();
        this.startTime = webinarSession.getStartTime();
        this.endTime = webinarSession.getEndTime();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public List<String> validate() {
        List<String> errors = new ArrayList<>();

        if (StringUtils.isEmpty(startTime)) {
            errors.add(Constants.START_TIME);
        }

        if (StringUtils.isEmpty(endTime)) {
            errors.add(Constants.END_TIME);
        }
        return errors;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String BOARD_ID = "boardId";
        public static final String TITLE = "title";
    }

    public Integer getUniqueStudentAttendants() {
        return uniqueStudentAttendants;
    }

    public void setUniqueStudentAttendants(Integer uniqueStudentAttendants) {
        this.uniqueStudentAttendants = uniqueStudentAttendants;
    }
}
