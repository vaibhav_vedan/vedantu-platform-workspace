/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.enums.InteractionType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "OTMSessionEngagementData")
public class OTMSessionEngagementData extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String sessionId;
    private InteractionType context;
    private Integer studentCountAtStart;//when the quiz/poll/hotspot started, what was the count
    private Integer totalUnattempts;
    private Integer totalAttempts;
    private Integer totalCorrectResponses;//quiz,hotspot only
    private Integer totalIncorrectResponses;//quiz,hotspot only
    private String correctAnswer;
    private Integer averageResponseTime;//quiz,hotspot,poll

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public InteractionType getContext() {
        return context;
    }

    public void setContext(InteractionType context) {
        this.context = context;
    }

    public Integer getStudentCountAtStart() {
        return studentCountAtStart;
    }

    public void setStudentCountAtStart(Integer studentCountAtStart) {
        this.studentCountAtStart = studentCountAtStart;
    }

    public Integer getTotalUnattempts() {
        return totalUnattempts;
    }

    public void setTotalUnattempts(Integer totalUnattempts) {
        this.totalUnattempts = totalUnattempts;
    }

    public Integer getTotalAttempts() {
        return totalAttempts;
    }

    public void setTotalAttempts(Integer totalAttempts) {
        this.totalAttempts = totalAttempts;
    }

    public Integer getTotalCorrectResponses() {
        return totalCorrectResponses;
    }

    public void setTotalCorrectResponses(Integer totalCorrectResponses) {
        this.totalCorrectResponses = totalCorrectResponses;
    }

    public Integer getTotalIncorrectResponses() {
        return totalIncorrectResponses;
    }

    public void setTotalIncorrectResponses(Integer totalIncorrectResponses) {
        this.totalIncorrectResponses = totalIncorrectResponses;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Integer getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(Integer averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String SESSIONID = "sessionId";
        public static final String CONTEXT = "context";
        public static final String TOTAL_INCORRECT_RESPONSE = "totalIncorrectResponses";
        public static final String TOTAL_CORRECT_RESPONSE = "totalCorrectResponses";
        public static final String TOTAL_UNATTEMPTS = "totalUnattempts";
        public static final String STUDENT_COUNT_AT_START = "studentCountAtStart";
    }
}