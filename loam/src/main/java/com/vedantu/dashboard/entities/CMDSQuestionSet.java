package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.enums.Difficulty;
import com.vedantu.lms.cmds.enums.QuestionType;

import java.util.List;

public class CMDSQuestionSet extends AbstractCMDSEntity {

	public List<String> questionIds;

	public String type;

	public int numberOfQuestionsComplete;
	public QuestionType questionSetType;
	public String fileName;
	public String originalRefName;
	public Difficulty difficulty;
	public int versionCount = 0;

	public CMDSQuestionSet() {
		super();
	}

	public CMDSQuestionSet(List<String> questionIds, String type, int numberOfQuestionsComplete,
			QuestionType questionSetType, String fileName, String originalRefName, Difficulty difficulty) {
		super();
		this.questionIds = questionIds;
		this.type = type;
		this.numberOfQuestionsComplete = numberOfQuestionsComplete;
		this.questionSetType = questionSetType;
		this.fileName = fileName;
		this.originalRefName = originalRefName;
		this.difficulty = difficulty;
	}

	public void updateTags() {
		// TODO : Implement this
	}

	public int getNumberOfQuestionsComplete() {
		return numberOfQuestionsComplete;
	}

	public void setNumberOfQuestionsComplete(int numberOfQuestionsComplete) {
		this.numberOfQuestionsComplete = numberOfQuestionsComplete;
	}

	public List<String> getQuestionIds() {
		return questionIds;
	}

	public void setQuestionIds(List<String> questionIds) {
		this.questionIds = questionIds;
	}


	public int calculateNextVersion() {
		return versionCount + 1;
	}

	public void incrementVersion() {
		this.versionCount++;
	}

	public static class Constants extends AbstractCMDSEntity.Constants {
		public static final String QUESTION_IDS = "questionIds";
		public static final String DIFFICULTY = "difficulty";
		public static final String STATUS = "status";
	}
}
