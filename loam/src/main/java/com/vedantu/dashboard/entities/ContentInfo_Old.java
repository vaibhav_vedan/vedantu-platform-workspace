package com.vedantu.dashboard.entities;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dashboard.pojo.TestContentInfoMetadata;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "ContentInfo_Old")
@CompoundIndexes({
        @CompoundIndex(name = "contentState_expiryTime", def = "{'contentState' : 1, 'expiryTime': 1}", background = true),
        @CompoundIndex(name = "contentState_evaulatedTime", def = "{'contentState' : 1, 'evaulatedTime': 1}", background = true),
        @CompoundIndex(name = "metadata.testId_1_lastUpdated_-1", def = "{'metadata.testId' : 1, 'lastUpdated' : -1}", background = true),
        @CompoundIndex(name = "metadata_testAttempts_id_1", def = "{'metadata.testAttempts._id' : 1}", background = true),
        @CompoundIndex(name = "metadata.testAttempts.endTime_1", def = "{'metadata.testAttempts.endTime': 1}", background = true),
        @CompoundIndex(name = "testAttempt_evaluatedTime", def = "{'metadata.testAttempts.0.evaluatedTime': 1, 'contentType' : 1, 'contentState' : 1, 'contentInfoType' : 1}}", background = true)
})
public class ContentInfo_Old extends AbstractMongoStringIdEntity
{
    private UserType userType;

    @Indexed(background = true)
    private String studentId;

    private String studentFullName;
    private String teacherId;
    private String teacherFullName;

    @Indexed(background = true)
    private String contentTitle;

    private String courseName;
    private String courseId;
    private String subject;
    private Set<String> subjects = new HashSet<>();

    private Long expiryTime;

    private ContentType contentType = ContentType.TEST;
    private ContentInfoType contentInfoType = ContentInfoType.SUBJECTIVE;
    private EngagementType engagementType = EngagementType.OTO;
    private ContentState contentState = ContentState.SHARED;
    private LMSType lmsType;

    private String contentLink;
    private String studentActionLink;
    private String teacherActionLink;

    private EntityType contextType;

    @Indexed(background = true)
    private String contextId;

    private List<String> tags;

    private TestContentInfoMetadata metadata;
    private AccessLevel accessLevel = AccessLevel.PRIVATE;

    private Long boardId;
    private Set<Long> boardIds = new HashSet<>();

    //TODO: Following fields are added for backward compatibility of moodle and youscore, Do not use it in future
    private String testId;
    private Long duration;
    private Long attemptedTime;
    private Long evaulatedTime;
    private Float totalMarks;
    private Float marksAcheived;
    private Integer noOfQuestions;
    private String attemptId; // Latest attempt id incase of multiple active
    // attempts

    public ContentInfo_Old()
    {
        super();
    }

    public ContentInfo_Old(UserBasicInfo student, UserBasicInfo teacher, String examCode)
    {
        // Youscore
        super();
        this.studentId = String.valueOf(student.getUserId());
        this.studentFullName = student.getFullName();
        this.teacherId = String.valueOf(teacher.getUserId());
        this.teacherFullName = teacher.getFullName();
        //this.testId = examCode;

        this.contentType = ContentType.TEST;
        //this.contentInfoType = ContentInfoType.OBJECTIVE;
        this.engagementType = EngagementType.OTO;
        this.contentState = ContentState.SHARED;
        this.lmsType = LMSType.YOUSCORE;
    }

    public AccessLevel getAccessLevel()
    {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel)
    {
        this.accessLevel = accessLevel;
    }

    public TestContentInfoMetadata getMetadata()
    {
        return metadata;
    }

    public void setMetadata(TestContentInfoMetadata metadata)
    {
        this.metadata = metadata;
    }

    public LMSType getLmsType()
    {
        return lmsType;
    }

    public void setLmsType(LMSType lmsType)
    {
        this.lmsType = lmsType;
    }

    public String getStudentFullName()
    {
        return studentFullName;
    }

    public void setStudentFullName(String studentFullName)
    {
        this.studentFullName = studentFullName;
    }

    public String getStudentId()
    {
        return studentId;
    }

    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }

    public String getTeacherId()
    {
        return teacherId;
    }

    public void setTeacherId(String teacherId)
    {
        this.teacherId = teacherId;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getCourseId()
    {
        return courseId;
    }

    public void setCourseId(String courseId)
    {
        this.courseId = courseId;
    }

    public Long getDuration()
    {
        return duration;
    }

    public void setDuration(Long duration)
    {
        this.duration = duration;
    }

    public Long getExpiryTime()
    {
        return expiryTime;
    }

    public void setExpiryTime(Long expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    public ContentType getContentType()
    {
        return contentType;
    }

    public void setContentType(ContentType contentType)
    {
        this.contentType = contentType;
    }

    public ContentInfoType getContentInfoType()
    {
        return contentInfoType;
    }

    public void setContentInfoType(ContentInfoType contentInfoType)
    {
        this.contentInfoType = contentInfoType;
    }

    public EngagementType getEngagementType()
    {
        return engagementType;
    }

    public void setEngagementType(EngagementType engagementType)
    {
        this.engagementType = engagementType;
    }

    public ContentState getContentState()
    {
        return contentState;
    }

    public void setContentState(ContentState contentState)
    {
        this.contentState = contentState;
    }

    public String getContentLink()
    {
        return contentLink;
    }

    public void setContentLink(String contentLink)
    {
        this.contentLink = contentLink;
    }

    public String getStudentActionLink()
    {
        return studentActionLink;
    }

    public void setStudentActionLink(String studentActionLink)
    {
        this.studentActionLink = studentActionLink;
    }

    public String getTeacherActionLink()
    {
        return teacherActionLink;
    }

    public void setTeacherActionLink(String teacherActionLink)
    {
        this.teacherActionLink = teacherActionLink;
    }

    public Float getTotalMarks()
    {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks)
    {
        this.totalMarks = totalMarks;
    }

    public Float getMarksAcheived()
    {
        return marksAcheived;
    }

    public void setMarksAcheived(Float marksAcheived)
    {
        this.marksAcheived = marksAcheived;
    }

    public String getTeacherFullName()
    {
        return teacherFullName;
    }

    public void setTeacherFullName(String teacherFullName)
    {
        this.teacherFullName = teacherFullName;
    }

    public String getContentTitle()
    {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle)
    {
        this.contentTitle = contentTitle;
    }

    public String getCourseName()
    {
        return courseName;
    }

    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public String getTestId()
    {
        return testId;
    }

    public void setTestId(String testId)
    {
        this.testId = testId;
    }

    public UserType getUserType()
    {
        return userType;
    }

    public void setUserType(UserType userType)
    {
        this.userType = userType;
    }

    public List<String> getTags()
    {
        return tags;
    }

    public EntityType getContextType()
    {
        return contextType;
    }

    public void setContextType(EntityType contextType)
    {
        this.contextType = contextType;
    }

    public String getContextId()
    {
        return contextId;
    }

    public void setContextId(String contextId)
    {
        this.contextId = contextId;
    }

    public Long getBoardId()
    {
        return boardId;
    }

    public void setBoardId(Long boardId)
    {
        this.boardId = boardId;
    }

    public Long getAttemptedTime()
    {
        return attemptedTime;
    }

    public void setAttemptedTime(Long attemptedTime)
    {
        this.attemptedTime = attemptedTime;
    }

    public Long getEvaulatedTime()
    {
        return evaulatedTime;
    }

    public void setEvaulatedTime(Long evaulatedTime)
    {
        this.evaulatedTime = evaulatedTime;
    }

    public Integer getNoOfQuestions()
    {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions)
    {
        this.noOfQuestions = noOfQuestions;
    }

    public void setTags(List<String> tags)
    {
        this.tags = tags;
    }

    public String getAttemptId()
    {
        return attemptId;
    }

    public void setAttemptId(String attemptId)
    {
        this.attemptId = attemptId;
    }


    public void setTags()
    {
        List<String> tags = new ArrayList<String>();
        if (!StringUtils.isEmpty(this.teacherFullName))
        {
            tags.add(this.teacherFullName.toUpperCase());
        }

        if (!StringUtils.isEmpty(this.contentTitle))
        {
            tags.add(this.contentTitle.toUpperCase());
        }

        if (!StringUtils.isEmpty(this.courseName))
        {
            tags.add(this.courseName.toUpperCase());

            if (this.courseName.indexOf("OTF") != -1)
            {
                tags.add("1-TO-FEW");
            }

            if (this.courseName.indexOf("OTO") != -1) {
                tags.add("1-TO-1");
            }
        }
//		if (this.contentInfoType != null) {
//			tags.add(String.valueOf(this.contentInfoType).toUpperCase());
//		}

        if (this.contentState != null)
        {
            tags.add(String.valueOf(this.contentState).toUpperCase());
        }

        if (this.contentType != null)
        {
            tags.add(String.valueOf(this.contentType).toUpperCase());
        }

        this.tags = tags;
    }

    @Override
    public String toString()
    {
        return "ContentInfo{"
                + "studentId='" + studentId + '\''
                + ", studentFullName='" + studentFullName + '\''
                + ", teacherId='" + teacherId + '\''
                + ", teacherFullName='" + teacherFullName + '\''
                + ", contentTitle='" + contentTitle + '\''
                + ", courseName='" + courseName + '\''
                + ", subject='" + subject + '\''
                + ", courseId='" + courseId + '\''
                + ", expiryTime=" + expiryTime
                + ", contentType=" + contentType
                + ", engagementType=" + engagementType
                + ", contentState=" + contentState
                + ", lmsType=" + lmsType
                + ", contentLink='" + contentLink + '\''
                + ", studentActionLink='" + studentActionLink + '\''
                + ", teacherActionLink='" + teacherActionLink + '\''
                + ", tags=" + tags
                + ", metadata=" + metadata
                + '}';
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants
    {

        public static final String STUDENT_ID = "studentId";
        public static final String TEACHER_ID = "teacherId";
        public static final String SUBJECT = "subject";
        public static final String COURSE_ID = "courseId";
        public static final String CONTENT_TYPE = "contentType";
        public static final String STUDENT_FULL_NAME = "studentFullName";
        public static final String TEACHER_FULL_NAME = "teacherFullName";
        public static final String CONTENT_TITLE = "contentTitle";
        public static final String CONTENT_LINK = "contentLink";
        public static final String COURSE_NAME = "courseName";
        public static final String EXPIRY_TIME = "expiryTime";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String ENGAGEMENT_TYPE = "engagementType";
        public static final String LMS_TYPE = "lmsType";
        public static final String CONTENT_STATE = "contentState";
        public static final String TEST_ID = "testId";
        public static final String EVAULATED_TIME = "evaulatedTime";
        public static final String ATTEMPTED_TIME = "attemptedTime";
        public static final String TAGS = "tags";
        public static final String CONTEXT_TYPE = "contextType";
        public static final String CONTEXT_ID = "contextId";
        public static final String METADATA = "metadata";
        public static final String BOARD_ID = "boardId";
        public static final String BOARD_IDS = "boardIds";
        public static final String RANK = "rank";
        public static final String PERCENTAGE = "percentage";
        public static final String PERCENTILE = "percentile";
        public static final String METADATA_TESTID = "metadata.testId";
        public static final String METADATA_TESTATTEMPTS = "metadata.testAttempts";
        public static final String METADATA_TESTATTEMPTS_RESULT_ENTRIES = "metadata.testAttempts.resultEntries";
        public static final String METADATA_TESTATTEMPTS_RESULT_ENTRIES_QUESTION_ID = "metadata.testAttempts.resultEntries.questionId";
        public static final String METADATA_TESTATTEMPTS_ATTEMPTSTATE="metadata.testAttempts.attemptState";
        public static final String METADATA_TESTATTEMPTS_ENDTIME="metadata.testAttempts.endTime";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME = "metadata.testAttempts.0.evaluatedTime";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_ZERO = "metadata.testAttempts.0";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_RESULT_ENTRIES = "metadata.testAttempts.0.resultEntries";
        public static final String METADATA_DURATION = "metadata.duration";
        public static final long ONE_DAY_IN_MILLIS = 86400000;
    }

    /**
     * @return the boardIds
     */
    public Set<Long> getBoardIds()
    {
        return boardIds;
    }

    /**
     * @param boardIds the boardIds to set
     */
    public void setBoardIds(Set<Long> boardIds)
    {
        this.boardIds = boardIds;
    }

    /**
     * @return the subjects
     */
    public Set<String> getSubjects()
    {
        return subjects;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setSubjects(Set<String> subjects)
    {
        this.subjects = subjects;
    }
}
