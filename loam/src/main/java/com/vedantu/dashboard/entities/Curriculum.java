package com.vedantu.dashboard.entities;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.lms.cmds.pojo.CurriculumStatusChangeTime;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

@Document(collection = "Curriculum")
@CompoundIndexes({
		@CompoundIndex(name = "statusChangeTime.changeTime_1",def = "{'statusChangeTime.changeTime':1}",background = true)

})
public class Curriculum extends AbstractMongoStringIdEntity {

	@Indexed(background = true)
	private String parentId;
	private Long startTime;
	private Long endTime;
	private String title;
	private String note;
	private CurriculumStatus status = CurriculumStatus.PENDING; /*
																 * PENDING[DEFAULT],
																 * INPROGRESS,
																 * DONE
																 */
	private SessionType sessionType; /* OTO, OTF */
	private List<ContentInfo> contents; /*
										 * Content need to have sharing state
										 * once shared
										 */
	//@Indexed
	private Set<String> sessionIds; 
	private CurriculumEntityName contextType;
	
	@Indexed(background = true)
	private String contextId;
	private String rootId;
	private Long boardId;
	private Integer childOrder;
	private List<CurriculumStatusChangeTime> statusChangeTime;
	private String parentCourseId;
	private Float expectedHours;
	@Indexed(background = true)
	private String courseNodeId;

	public Curriculum() {
		super();
	}

	public Curriculum(Node node) {
		super();
		this.startTime = node.getStartTime();
		this.endTime = node.getEndTime();
		this.title = node.getTitle();
		this.note = node.getNote();
		if (node.getStatus() != null) {
			this.status = node.getStatus();
		} else {
			this.status = CurriculumStatus.PENDING;
		}

		this.sessionType = node.getSessionType();
		this.contents = node.getContents();
		// this.sessions = node.getSessions();
		this.sessionIds =  node.getSessionIds();

		if (node.getParentId() != null) {
			this.parentId = node.getParentId();
		}
		this.boardId = node.getBoardId();
		this.childOrder = node.getChildOrder();
		this.expectedHours = node.getExpectedHours();

	}

	public Curriculum(Curriculum curriculum) {
		super();
		this.startTime = curriculum.getStartTime();
		this.endTime = curriculum.getEndTime();
		this.title = curriculum.getTitle();
		this.note = curriculum.getNote();
		if (curriculum.getStatus() != null) {
			this.status = curriculum.getStatus();
		} else {
			this.status = CurriculumStatus.PENDING;
		}

		this.sessionType = curriculum.getSessionType();
		this.contents = curriculum.getContents();
		// this.sessions = curriculum.getSessions();
		// this.edited = curriculum.getEdited();

		if (curriculum.getParentId() != null)
			this.parentId = curriculum.getParentId();
		// this.nodes = curriculum.getNodes();

	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public CurriculumStatus getStatus() {
		return status;
	}

	public void setStatus(CurriculumStatus status) {
		this.status = status;
	}

	public SessionType getSessionType() {
		return sessionType;
	}

	public void setSessionType(SessionType sessionType) {
		this.sessionType = sessionType;
	}

	public List<ContentInfo> getContents() {
		return contents;
	}

	public void setContents(List<ContentInfo> contents) {
		this.contents = contents;
	}

	public String getRootId() {
		return rootId;
	}

	public void setRootId(String rootId) {
		this.rootId = rootId;
	}

	public Set<String> getSessionIds() {
		return sessionIds;
	}

	public void setSessionIds(Set<String> sessionIds) {
		this.sessionIds = sessionIds;
	}

	public CurriculumEntityName getContextType() {
		return contextType;
	}

	public void setContextType(CurriculumEntityName contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public Integer getChildOrder() {
		return childOrder;
	}

	public void setChildOrder(Integer childOrder) {
		this.childOrder = childOrder;
	}

	public List<CurriculumStatusChangeTime> getStatusChangeTime() {
		return statusChangeTime;
	}

	public void setStatusChangeTime(List<CurriculumStatusChangeTime> statusChangeTime) {
		this.statusChangeTime = statusChangeTime;
	}

	public String getParentCourseId() {
		return parentCourseId;
	}

	public void setParentCourseId(String parentCourseId) {
		this.parentCourseId = parentCourseId;
	}

	public Float getExpectedHours() {
		return expectedHours;
	}

	public void setExpectedHours(Float expectedHours) {
		this.expectedHours = expectedHours;
	}

	public String getCourseNodeId() {
		return courseNodeId;
	}

	public void setCourseNodeId(String courseNodeId) {
		this.courseNodeId = courseNodeId;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String PARENT_ID = "parentId";
		public static final String CONTEXT_ID = "contextId";
		public static final String CONTEXT_TYPE = "contextType";
		public static final String ROOT_ID = "rootId";
		public static final String CHILD_ORDER = "childOrder";
		public static final String COURSE_NODE_ID = "courseNodeId";
		public static final String CONTENTS = "contents";
		public static final String CONTENT_INFOID = "contentInfoId";//part of contents
		public static final String TITLE = "title";
		public static final String NOTE = "note";
		public static final String EXPECTED_HOURS = "expectedHours";
		public static final String BOARD_ID = "boardId";
		public static final String SESSION_IDS = "sessionIds";
		public static final String STATUS = "status";
		public static final String CONTENT_TYPE = "contents.contentType";
		public static final String STATUS_CHANGE_TIME="statusChangeTime.changeTime";

	}

}
