package com.vedantu.dashboard.entities;


import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.pojo.BatchBasicInfo;

import com.vedantu.onetofew.pojo.CourseBasicInfo;

import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchChangeTime;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;
import java.util.Map;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Enrollment")
public class Enrollment extends AbstractMongoStringIdEntity {

    private String userId;
    private String batchId;
    private String courseId;
    private String sessionId;
    private Role role;
    private EntityStatus status;
    private EntityType type;
    private EnrollmentState state;
    private Long endTime;
    private String endReason;
    private Long endedBy;
    private List<EnrollmentStateChangeTime> stateChangeTime;
    private List<StatusChangeTime> statusChangeTime;
    private com.vedantu.session.pojo.EntityType entityType;
    private Integer hourlyRate = 0;

    @Indexed(background = true)
    private String entityId;
    private String entityTitle;
    private List<BatchChangeTime> batchChangeTime;
    private EnrollmentPurchaseContext purchaseContextType;
    private String purchaseContextId;
    private String purchaseEnrollmentId;
    private EnrollmentType purchaseContextEnrollmentType;

    public Enrollment() {
        super();
    }

    public Enrollment(String userId, String batchId, String courseId, String sessionId, Role role, EntityStatus status,
                      EntityType type) {
        super();
        this.userId = userId;
        this.batchId = batchId;
        this.courseId = courseId;
        this.sessionId = sessionId;
        this.role = role;
        this.status = status;
        this.type = type;
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public EntityStatus getStatus() {
        return status;
    }

    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public EnrollmentState getState() {
        return state;
    }

    public void setState(EnrollmentState state) {
        this.state = state;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public List<EnrollmentStateChangeTime> getStateChangeTime() {
        if (stateChangeTime == null) {
            return new ArrayList<>();
        }
        return stateChangeTime;
    }

    public void setStateChangeTime(List<EnrollmentStateChangeTime> stateChangeTime) {
        this.stateChangeTime = stateChangeTime;
    }

    public List<StatusChangeTime> getStatusChangeTime() {
        if (statusChangeTime == null) {
            return new ArrayList<>();
        }
        return statusChangeTime;
    }

    public void setStatusChangeTime(List<StatusChangeTime> statusChangeTime) {
        this.statusChangeTime = statusChangeTime;
    }

    public com.vedantu.session.pojo.EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(com.vedantu.session.pojo.EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public List<BatchChangeTime> getBatchChangeTime() {
        if (batchChangeTime == null) {
            return new ArrayList<>();
        }
        return batchChangeTime;
    }

    public void setBatchChangeTime(List<BatchChangeTime> batchChangeTime) {
        this.batchChangeTime = batchChangeTime;
    }

    public EnrollmentPurchaseContext getPurchaseContextType() {
        return purchaseContextType;
    }

    public void setPurchaseContextType(EnrollmentPurchaseContext purchaseContextType) {
        this.purchaseContextType = purchaseContextType;
    }

    public String getPurchaseContextId() {
        return purchaseContextId;
    }

    public void setPurchaseContextId(String purchaseContextId) {
        this.purchaseContextId = purchaseContextId;
    }

    public EnrollmentType getPurchaseContextEnrollmentType() {
        return purchaseContextEnrollmentType;
    }

    public void setPurchaseContextEnrollmentType(EnrollmentType purchaseContextEnrollmentType) {
        this.purchaseContextEnrollmentType = purchaseContextEnrollmentType;
    }


    public String getPurchaseEnrollmentId() {
        return purchaseEnrollmentId;
    }

    public void setPurchaseEnrollmentId(String purchaseEnrollmentId) {
        this.purchaseEnrollmentId = purchaseEnrollmentId;
    }

    public EnrollmentPojo toEnrollmentInfo(Enrollment enrollment, Batch batch,
                                           Course course, UserBasicInfo userBasicInfo, Map<String, UserBasicInfo> usersMap) {
        EnrollmentPojo enrollmentInfo = new EnrollmentPojo();
        enrollmentInfo.setId(enrollment.getId());
        enrollmentInfo.setEnrollmentId(enrollment.getId());
        enrollmentInfo.setUser(userBasicInfo);

        if (batch != null) {
            int count = 0;
            BatchBasicInfo basicInfo = batch.toBatchBasicInfo(batch, course, usersMap, null);
            enrollmentInfo.setBatch(basicInfo);
        }
        if (course != null) {
            enrollmentInfo.setCourse(course.toCourseBasicInfo(course, usersMap, null));
        }
        enrollmentInfo.setRole(enrollment.getRole());
        enrollmentInfo.setStatus(enrollment.getStatus());
        enrollmentInfo.setCreationTime(enrollment.getCreationTime());
        enrollmentInfo.setLastUpdated(enrollment.getLastUpdated());
        enrollmentInfo.setBatchId(enrollment.getBatchId());
        enrollmentInfo.setCourseId(enrollment.getCourseId());
        enrollmentInfo.setUserId(enrollment.getUserId());
        enrollmentInfo.setState(enrollment.getState());

        enrollmentInfo.setEndedBy(enrollment.getEndedBy());
        enrollmentInfo.setEndTime(enrollment.getEndTime());
        enrollmentInfo.setEndReason(enrollment.getEndReason());
        enrollmentInfo.setStatusChangeTime(enrollment.getStatusChangeTime());
        enrollmentInfo.setEntityType(enrollment.getEntityType());
        enrollmentInfo.setEntityId(enrollment.getEntityId());
        enrollmentInfo.setEntityTitle(enrollment.getEntityTitle());
        enrollmentInfo.setBatchChangeTime(enrollment.getBatchChangeTime());
        if(enrollment.getPurchaseContextId() != null ) {
            enrollmentInfo.setPurchaseContextId(enrollment.getPurchaseContextId());
        }
        if(enrollment.getPurchaseContextType() != null ) {
            enrollmentInfo.setPurchaseContextType(enrollment.getPurchaseContextType());
        }
        if(enrollment.getPurchaseContextEnrollmentType()!=null){
            enrollmentInfo.setPurchaseContextEnrollmentType(enrollment.getPurchaseContextEnrollmentType());
        }
        if(enrollment.getPurchaseEnrollmentId() != null ) {
            enrollmentInfo.setPurchaseEnrollmentId(enrollment.getPurchaseEnrollmentId());
        }
        return enrollmentInfo;
    }

    public EnrollmentPojo toEnrollmentInfoByBasicInfo(Enrollment enrollment, BatchBasicInfo batch,
                                                      CourseBasicInfo course, UserBasicInfo userBasicInfo, Map<String, UserBasicInfo> usersMap) {
        EnrollmentPojo enrollmentInfo = new EnrollmentPojo();
        enrollmentInfo.setId(enrollment.getId());
        enrollmentInfo.setEnrollmentId(enrollment.getId());
        enrollmentInfo.setUser(userBasicInfo);

        if (batch != null) {
//        	int count = 0;
//        	BatchBasicInfo basicInfo = batch;
//        	if(ArrayUtils.isNotEmpty(batch.getEnrolledStudents()) && batch.getEnrolledStudents().contains(enrollment.getUserId())){
//        		basicInfo.setEnrolledStudents(Arrays.asList(userBasicInfo));
//        	}else{
//        		basicInfo.setEnrolledStudents(new ArrayList<>());
//        	}
            enrollmentInfo.setBatch(batch);
        }
        if (course != null) {
            enrollmentInfo.setCourse(course);
        }
        enrollmentInfo.setRole(enrollment.getRole());
        enrollmentInfo.setStatus(enrollment.getStatus());
        enrollmentInfo.setCreationTime(enrollment.getCreationTime());
        enrollmentInfo.setLastUpdated(enrollment.getLastUpdated());
        enrollmentInfo.setBatchId(enrollment.getBatchId());
        enrollmentInfo.setCourseId(enrollment.getCourseId());
        enrollmentInfo.setUserId(enrollment.getUserId());
        enrollmentInfo.setState(enrollment.getState());

        enrollmentInfo.setEndedBy(enrollment.getEndedBy());
        enrollmentInfo.setEndTime(enrollment.getEndTime());
        enrollmentInfo.setEndReason(enrollment.getEndReason());
        enrollmentInfo.setStatusChangeTime(enrollment.getStatusChangeTime());
        enrollmentInfo.setEntityType(enrollment.getEntityType());
        enrollmentInfo.setEntityId(enrollment.getEntityId());
        enrollmentInfo.setEntityTitle(enrollment.getEntityTitle());
        enrollmentInfo.setBatchChangeTime(enrollment.getBatchChangeTime());
        if(enrollment.getPurchaseContextId() != null ) {
            enrollmentInfo.setPurchaseContextId(enrollment.getPurchaseContextId());
        }
        if(enrollment.getPurchaseContextType() != null ) {
            enrollmentInfo.setPurchaseContextType(enrollment.getPurchaseContextType());
        }
        if(enrollment.getPurchaseContextEnrollmentType()!=null){
            enrollmentInfo.setPurchaseContextEnrollmentType(enrollment.getPurchaseContextEnrollmentType());
        }
        if(enrollment.getPurchaseEnrollmentId() != null ) {
            enrollmentInfo.setPurchaseEnrollmentId(enrollment.getPurchaseEnrollmentId());
        }
        return enrollmentInfo;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String STATUS = "status";
        public static final String COURSE_ID = "courseId";
        public static final String BATCH_ID = "batchId";
        public static final String SESSION_ID = "sessionId";
        public static final String ROLE = "role";
        public static final String TYPE = "type";
        public static final String STATE = "state";
        public static final String STATE_CHANGE_TIME = "stateChangeTime";
        public static final String END_TIME = "endTime";
        public static final String STATE_CHANGE_TIME_LONG = "stateChangeTime.changeTime";
        public static final String STATE_CHANGE_TIME_NEW_STATE = "stateChangeTime.newState";
        public static final String ENTITY_ID = "entityId";
        public static final String ENTITY_TYPE = "entityType";
        public static final String PURCHASE_CONTEXT_ID = "purchaseContextId";
        public static final String PURCHASE_CONTEXT = "purchaseContextType";
        public static final String PURCHASE_ENROLLMENT_ID = "purchaseEnrollmentId";
        public static final String PURCHASE_CONTEXT_ENROLLMENT_TYPE = "purchaseContextEnrollmentType";
    }

    /**
     * @return the hourlyRate
     */
    public Integer getHourlyRate() {
        return hourlyRate;
    }

    /**
     * @param hourlyRate the hourlyRate to set
     */
    public void setHourlyRate(Integer hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

}
