package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.pojo.StudentInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SessionAttendanceAvg")
public class SessionAttendanceAvg extends AbstractMongoStringIdEntity {

	@Indexed(background = true)
	private String sessionId;
	private double average;
	private Long boardId;
	private Long startTime;
	private StudentInfo studentInfo;
	private Long endTime;

	public SessionAttendanceAvg() {

		super();
	}

	public String getSessionId() {

		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public double getAverage() {

		return average;
	}

	public void setAverage(double average) {

		this.average = average;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public StudentInfo getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(StudentInfo studentInfo) {
		this.studentInfo = studentInfo;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public static class Constants {

		public static final String _ID = "_id";
		public static final String SESSION_ID = "sessionId";
		public static final String AVERAGE = "average";
		public static final String BOARD_ID = "boardId";
		public static final String START_TIME = "startTime";
		public static final String STUDENT_INFO = "studentInfo";
		public static final String STUDENT_INFO_METADATA = "studentInfo.studentEngagementMetadataMap";
		public static final String STUDENT_ID = "studentId";

	}

}
