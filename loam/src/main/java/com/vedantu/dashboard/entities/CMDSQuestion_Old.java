package com.vedantu.dashboard.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.vedantu.dashboard.enums.CognitiveLevel;
import com.vedantu.dashboard.enums.Difficulty;
import com.vedantu.dashboard.enums.Indexable;
import com.vedantu.dashboard.pojo.ParseQuestionMetadata;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.dashboard.pojo.SolutionInfo;
import com.vedantu.dashboard.pojo.challenges.HintFormat;
import com.vedantu.dashboard.pojo.metadata.MCQsolutionInfo;
import com.vedantu.dashboard.utils.CMDSCommonUtils;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import com.vedantu.lms.cmds.pojo.AnswerChangeRecord;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.ArrayUtils;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Document(collection = "CMDSQuestion_Old")
public class CMDSQuestion_Old extends AbstractCMDSEntity implements ILatexProcessor {

    @JsonDeserialize(as = MCQsolutionInfo.class)
    private MCQsolutionInfo solutionInfo;
    private RichTextFormat questionBody;
    private QuestionType type;
    private String questionSetName;
    private String questionSetId;
    private Marks marks;



    private List<HintFormat> refHints;

    @Indexed(background = true)
    private Indexable indexable;

    private Difficulty difficulty;


    // New Question Tagging related
    private Integer difficultyValue;
    private CognitiveLevel cognitiveLevel;
    private List<String> source;
    @Indexed(background = true)
    private Set<String> mainTags;


    //book related
    private String book;
    private String edition;
    private String chapter;
    @Indexed(background = true)
    private String chapterNo;
    private String exercise;
    private Set<String> pageNos;
    private int slNoInBook;//used for ordering only
    private String questionNo;

    private String changedFromId;

    private List<AnswerChangeRecord> answerChangeRecords = new ArrayList<>();

    //social Layer
    private long upVotes;
    private long downVotes;
    private long attempts;

    //slug data for queries
    @Indexed(background = true)
    private String bookSlug;
    @Indexed(background = true)
    private String editionSlug;
    @Indexed(background = true)
    private String chapterSlug;
    @Indexed(background = true)
    private String exerciseSlug;

    private Set<String> targetGrade;

    @Indexed(background = true)
    private Set<String> pageNosSlugs;

    @Indexed(background = true)
    private Integer used = 0;

    public CMDSQuestion_Old() {

    }

    public CMDSQuestion_Old(RichTextFormat q, QuestionType type, MCQsolutionInfo solutionInfo, String userId,
                        String questionSetName, String assignedTo, VedantuRecordState recordState, ParseQuestionMetadata metadata) {

        super(userId);
        this.questionBody = q;
        this.type = type;
        this.solutionInfo = solutionInfo;
        this.questionSetName = questionSetName;
        this.recordState = recordState;
        this.questionSetId = "";
        if (metadata != null) {
            this.difficulty = metadata.difficulty;
            this.indexable = metadata.indexable;
            super.setSubject(metadata.getSubject());
            super.setTopics(metadata.getTopics());
            super.setSubtopics(metadata.getSubTopics());
            super.setTags(metadata.getTags());
            super.setGrades(metadata.getGrades());
            super.setTargets(metadata.getTargets());
            this.marks = metadata.getMarks();
            this.book = metadata.getBook();
            this.edition = metadata.getEdition();
            this.chapter = metadata.getChapter();
            this.chapterNo = metadata.getChapterNo();
            this.exercise = metadata.getExercise();
            this.pageNos = metadata.getPageNos();
            this.slNoInBook = metadata.getSlNoInBook();

            this.questionNo = metadata.getQuestionNo();

            this.targetGrade = metadata.getTargetgrade();
            this.source = metadata.getSource();
            this.difficultyValue = metadata.getDifficultyValue();
            this.cognitiveLevel = metadata.getCognitiveLevel();

            //adding slug data
            this.bookSlug = CMDSCommonUtils.makeSlug(metadata.getBook());
            this.editionSlug = CMDSCommonUtils.makeSlug(metadata.getEdition());
            this.chapterSlug = CMDSCommonUtils.makeSlug(metadata.getChapter());
            this.exerciseSlug = CMDSCommonUtils.makeSlug(metadata.getExercise());
            this.pageNosSlugs = CMDSCommonUtils.makeSlugs(metadata.getPageNos());
        }

        this.refHints = new ArrayList<>();
    }

    public void addHint(String hint) {

    }

    // //
    @Override
    public void addHook() {

        if (solutionInfo != null) {
            solutionInfo.addHook();
        }
        if (questionBody != null) {
            questionBody.addHook();
        }
        if (ArrayUtils.isNotEmpty(refHints)) {
            for (HintFormat hintFormat : refHints) {
                hintFormat.addHook();
            }
        }
    }

    public SolutionInfo getSolutionInfo() {
        return solutionInfo;
    }

    public void setSolutionInfo(MCQsolutionInfo solutionInfo) {
        this.solutionInfo = solutionInfo;
    }

    public RichTextFormat getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(RichTextFormat questionBody) {
        this.questionBody = questionBody;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public String getQuestionSetName() {
        return questionSetName;
    }

    public void setQuestionSetName(String questionSetName) {
        this.questionSetName = questionSetName;
    }

    public String getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(String questionSetId) {
        this.questionSetId = questionSetId;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getDifficultyValue() {
        return difficultyValue;
    }

    public void setDifficultyValue(Integer difficultyValue) {
        this.difficultyValue = difficultyValue;
    }

    public CognitiveLevel getCognitiveLevel() {
        return cognitiveLevel;
    }

    public void setCognitiveLevel(CognitiveLevel cognitiveLevel) {
        this.cognitiveLevel = cognitiveLevel;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public void updateTags() {
        // TODO : Implement this
    }

    public long getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(long upVotes) {
        this.upVotes = upVotes;
    }

    public long getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(long downVotes) {
        this.downVotes = downVotes;
    }

    public long getAttempts() {
        return attempts;
    }

    public void setAttempts(long attempts) {
        this.attempts = attempts;
    }

    public List<HintFormat> getRefHints() {
        return refHints;
    }

    public void setRefHints(List<HintFormat> refHints) {
        this.refHints = refHints;
    }

    public void setBook(String book) {
        this.book = book;
        this.bookSlug = CMDSCommonUtils.makeSlug(book);
    }

    public String getBook() {
        return book;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
        this.chapterSlug = CMDSCommonUtils.makeSlug(chapter);
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
        this.exerciseSlug = CMDSCommonUtils.makeSlug(exercise);
    }

    public Set<String> getPageNos() {
        return pageNos;
    }

    public void setPageNos(Set<String> pageNos) {
        this.pageNos = pageNos;
        this.pageNosSlugs = CMDSCommonUtils.makeSlugs(pageNos);
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
        this.editionSlug = CMDSCommonUtils.makeSlug(edition);
    }

    public String getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(String chapterNo) {
        this.chapterNo = chapterNo;
    }

    public String getBookSlug() {
        return bookSlug;
    }

    public void setBookSlug(String bookSlug) {
        this.bookSlug = bookSlug;
    }

    public String getEditionSlug() {
        return editionSlug;
    }

    public void setEditionSlug(String editionSlug) {
        this.editionSlug = editionSlug;
    }

    public String getChapterSlug() {
        return chapterSlug;
    }

    public void setChapterSlug(String chapterSlug) {
        this.chapterSlug = chapterSlug;
    }

    public String getExerciseSlug() {
        return exerciseSlug;
    }

    public void setExerciseSlug(String exerciseSlug) {
        this.exerciseSlug = exerciseSlug;
    }

    public Set<String> getPageNosSlugs() {
        return pageNosSlugs;
    }

    public void setPageNosSlugs(Set<String> pageNosSlugs) {
        this.pageNosSlugs = pageNosSlugs;
    }

    public int getSlNoInBook() {
        return slNoInBook;
    }

    public void setSlNoInBook(int slNoInBook) {
        this.slNoInBook = slNoInBook;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public Set<String> getTargetGrade() {
        return targetGrade;
    }

    public void setTargetGrade(Set<String> targetGrade) {
        this.targetGrade = targetGrade;
    }

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Indexable getIndexable() {
        return indexable;
    }

    public void setIndexable(Indexable indexable) {
        this.indexable = indexable;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String QUESTION_BODY = "questionBody";
        public static final String TYPE = "type";
        public static final String DIFFICULTY = "difficulty";
        public static final String SOLUTION_INFO = "solutionInfo";
        public static final String ATTEMPTS = "attempts";
        public static final String UP_VOTES = "upVotes";
        public static final String DOWN_VOTES = "downVotes";
        public static final String BOOK = "book";
        public static final String EDITION = "edition";
        public static final String CHAPTER = "chapter";
        public static final String CHAPTER_NO = "chapterNo";
        public static final String EXERCISE = "exercise";
        public static final String PAGENOS = "pageNos";
        public static final String BOOK_SLUG = "bookSlug";
        public static final String EDITION_SLUG = "editionSlug";
        public static final String CHAPTER_SLUG = "chapterSlug";
        public static final String EXERCISE_SLUG = "exerciseSlug";
        public static final String PAGENOS_SLUGS = "pageNosSlugs";
        public static final String SLNO_IN_BOOK = "slNoInBook";
        public static final String QUESTION_NO = "questionNo";
        public static final String MAIN_TAGS = "mainTags";
        public static final String SOURCE = "source";
        public static final String DIFFICULTY_VALUE = "difficultyValue";
        public static final String COGNITIVE_LEVEL = "cognitiveLevel";
        public static final String RECORD_STATE = "recordState";
        public static final String MARKS = "marks";
        public static final String USED = "used";
        public static final String INDEXABLE = "indexable";
    }

    /**
     * @return the answerChangeRecords
     */
    public List<AnswerChangeRecord> getAnswerChangeRecords() {
        return answerChangeRecords;
    }

    /**
     * @param answerChangeRecords the answerChangeRecords to set
     */
    public void setAnswerChangeRecords(List<AnswerChangeRecord> answerChangeRecords) {
        this.answerChangeRecords = answerChangeRecords;
    }

    /**
     * @return the changedFromId
     */
    public String getChangedFromId() {
        return changedFromId;
    }

    /**
     * @param changedFromId the changedFromId to set
     */
    public void setChangedFromId(String changedFromId) {
        this.changedFromId = changedFromId;
    }

}
