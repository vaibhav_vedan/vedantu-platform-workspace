package com.vedantu.dashboard.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.vedantu.dashboard.enums.CognitiveLevel;
import com.vedantu.dashboard.enums.Difficulty;
import com.vedantu.dashboard.enums.Indexable;
import com.vedantu.dashboard.pojo.ParseQuestionMetadata;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.dashboard.pojo.SolutionInfo;
import com.vedantu.dashboard.pojo.challenges.HintFormat;
import com.vedantu.dashboard.pojo.metadata.MCQsolutionInfo;
import com.vedantu.dashboard.utils.CMDSCommonUtils;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import com.vedantu.lms.cmds.pojo.AnswerChangeRecord;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.ArrayUtils;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Document(collection = "CMDSQuestion")
public class CMDSQuestion extends AbstractCMDSEntity implements ILatexProcessor {

    @JsonDeserialize(as = MCQsolutionInfo.class)
    private MCQsolutionInfo solutionInfo;
    private RichTextFormat questionBody;
    private QuestionType type;
    private Marks marks;

    private Difficulty difficulty;


    @Indexed(background = true)
    private Set<String> mainTags;



    public CMDSQuestion() {

    }

    public CMDSQuestion(RichTextFormat q, QuestionType type, MCQsolutionInfo solutionInfo, String userId,
            String questionSetName, String assignedTo, VedantuRecordState recordState, ParseQuestionMetadata metadata) {

        this.questionBody = q;
        this.type = type;
        this.solutionInfo = solutionInfo;
        this.recordState = recordState;
        if (metadata != null) {
            this.difficulty = metadata.difficulty;
            super.setSubject(metadata.getSubject());
            super.setTopics(metadata.getTopics());
            super.setSubtopics(metadata.getSubTopics());
            super.setTags(metadata.getTags());
            super.setGrades(metadata.getGrades());
            super.setTargets(metadata.getTargets());
            this.marks = metadata.getMarks();
        }
    }

    public void addHint(String hint) {

    }

    // //
    @Override
    public void addHook() {

        if (solutionInfo != null) {
            solutionInfo.addHook();
        }
        if (questionBody != null) {
            questionBody.addHook();
        }
    }

    public SolutionInfo getSolutionInfo() {
        return solutionInfo;
    }

    public void setSolutionInfo(MCQsolutionInfo solutionInfo) {
        this.solutionInfo = solutionInfo;
    }

    public RichTextFormat getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(RichTextFormat questionBody) {
        this.questionBody = questionBody;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }


    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }


    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }


    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String QUESTION_BODY = "questionBody";
        public static final String TYPE = "type";
        public static final String DIFFICULTY = "difficulty";
        public static final String SOLUTION_INFO = "solutionInfo";
        public static final String MAIN_TAGS = "mainTags";
        public static final String RECORD_STATE = "recordState";
        public static final String MARKS = "marks";

    }

}
