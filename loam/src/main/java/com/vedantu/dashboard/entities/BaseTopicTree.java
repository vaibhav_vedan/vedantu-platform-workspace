package com.vedantu.dashboard.entities;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ravneet
 */
@Document(collection = "BaseTopicTree")
public class BaseTopicTree extends AbstractMongoStringIdEntity {

    public static final String DEFAULT_PARENT_ID = "0";

    @Indexed(unique=true, background=true)
    private String name;
    private Integer level;
    private String slug;
    private String parentId;
    private VisibilityState state;

    private Set<String> children;
    private Set<String> parents;

    private Long userId;

    public BaseTopicTree() {
        super();
    }

    public BaseTopicTree(String name, String slug, String parentId,
                 VisibilityState state, Long userId) {
        super();
        this.name = name;
        this.slug = slug;
        this.parentId = parentId == null ? DEFAULT_PARENT_ID : parentId;
        this.state = state == null ? VisibilityState.VISIBLE : state;
        this.userId = userId;
        this.parents = new HashSet<String>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public VisibilityState getState() {
        return state;
    }

    public void setState(VisibilityState state) {
        this.state = state;
    }

    public Set<String> getChildren() {
        return children;
    }

    public void setChildren(Set<String> children) {
        this.children = children;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Set<String> getParents() {
        return parents;
    }

    public void setParents(Set<String> parents) {
        this.parents = parents;
    }

    @Override
    public String toString() {
        return String
                .format("{id=%s, name=%s, slug=%s, parentId=%s, state=%s, userId=%s, creationTime=%s, lastUpdated=%s}",
                        getId(), name, slug, parentId, state, userId, getCreationTime(),
                        getLastUpdated());
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String SLUG = "slug";
        public static final String NAME = "name";
        public static final String PARENT_ID = "parentId";
        public static final String LEVEL = "level";
        public static final String PARENTS = "parents";
        public static final String CHILDREN = "children";
    }

    public String _getComparableValue() {
        return slug;
    }

}
