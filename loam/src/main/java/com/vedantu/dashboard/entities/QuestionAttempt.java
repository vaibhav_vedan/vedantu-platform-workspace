package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.enums.QuestionAttemptContext;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by somil on 23/03/17.
 */
@Document(collection = "QuestionAttempt")
public class QuestionAttempt extends AbstractMongoStringIdEntity {

    private String userId;
    private Integer duration;
    //private Long startTime;
    //private Long endTime;

    private String questionId;

    private List<String> answerGiven;
    private QuestionAttemptContext contextType;
    private String contextId;
    @Indexed(background = true)
    private String attemptId;//testattemptId|challengeTakenId
    private Boolean correct;

    public QuestionAttempt() {
        super();
    }

    public QuestionAttempt(String userId, Integer duration, 
            QuestionAttemptContext contextType,
            String contextId, String attemptId, 
            String questionId, List<String> answerGiven) {
        this.userId = userId;
        this.duration = duration;
        this.contextType=contextType;
        this.contextId = contextId;
        this.attemptId = attemptId;
        this.questionId = questionId;
        this.answerGiven = answerGiven;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public QuestionAttemptContext getContextType() {
        return contextType;
    }

    public void setContextType(QuestionAttemptContext contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getAttemptId() {
        return attemptId;
    }

    public void setAttemptId(String attemptId) {
        this.attemptId = attemptId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getAnswerGiven() {
        return answerGiven;
    }

    public void setAnswerGiven(List<String> answerGiven) {
        this.answerGiven = answerGiven;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String QUESTION_ID = "questionId";
        public static final String ATTEMPT_ID = "attemptId";
        public static final String CONTEXT_ID = "contextId";
        public static final String ANSWER_GIVEN = "answerGiven";
    }
    
}
