package com.vedantu.dashboard.entities;


import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DoubtSolver")
public class DoubtSolver extends AbstractMongoStringIdEntity {

    private String firstName;
    private String lastName;
    private String fullName;

    public DoubtSolver(){super();}

    public DoubtSolver(String firstName, String lastName, String fullName){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
