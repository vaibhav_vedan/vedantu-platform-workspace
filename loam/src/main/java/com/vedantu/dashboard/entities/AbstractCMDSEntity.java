package com.vedantu.dashboard.entities;


import com.vedantu.dashboard.utils.CMDSCommonUtils;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import java.util.HashSet;
import java.util.Set;

public class AbstractCMDSEntity extends AbstractMongoStringIdEntity {

    @Deprecated
    public Set<String> boardIds;//will be deprecated soon
    @Deprecated
    public Set<String> targetIds;//will be deprecated, use categories instead
    private String subject;
    public Set<String> topics;
    public Set<String> subtopics;
    public Set<String> grades;
    public Set<String> targets;
    public Set<String> tags;//free flowing tags

    public VedantuRecordState recordState = VedantuRecordState.ACTIVE;
    public String name;
    private AccessLevel accessLevel;

    //slugs
    private String subjectSlug;// physics, chemistry etc
    public Set<String> topicsSlugs;// newtons-laws-of-motion,thermodynamics
    public Set<String> subtopicsSlugs;// newtons-laws-of-motion,thermodynamics
    public Set<String> gradesSlugs;
    public Set<String> targetsSlugs;//cbse, icse,iit-jee etc
    public Set<String> tagsSlugs;//free flowing tags

    public AbstractCMDSEntity() {
        this(null);
    }

    public AbstractCMDSEntity(String userId) {
        super(System.currentTimeMillis(), userId, System.currentTimeMillis(), userId);
        this.boardIds = new HashSet<>();
        this.targetIds = new HashSet<>();
        this.grades = new HashSet<>();
        this.tags = new HashSet<>();
        this.topics = new HashSet<>();
        this.subtopics = new HashSet<>();
        this.targets = new HashSet<>();
        this.topicsSlugs = new HashSet<>();
        this.subtopicsSlugs = new HashSet<>();
        this.gradesSlugs = new HashSet<>();
        this.targetsSlugs = new HashSet<>();
        this.tagsSlugs = new HashSet<>();
    }

    public void addGrades(Set<String> grades) {

        if (CollectionUtils.isEmpty(grades)) {
            return;
        }
        if (this.grades == null) {
            this.grades = new HashSet<>();
        }

        this.grades.addAll(grades);
        this.gradesSlugs.addAll(CMDSCommonUtils.makeSlugs(grades));
    }

    public void addBoardIds(Set<String> boardIds) {

        if (CollectionUtils.isEmpty(boardIds)) {
            return;
        }
        if (this.boardIds == null) {
            this.boardIds = new HashSet<>();
        }
        this.boardIds.addAll(boardIds);
    }

    public void addTargets(Set<String> targets) {

        if (CollectionUtils.isEmpty(targets)) {
            return;
        }
        if (this.targets == null) {
            this.targets = new HashSet<>();
        }
        this.targets.addAll(targets);
        this.targetsSlugs.addAll(CMDSCommonUtils.makeSlugs(targets));
    }

    public void addTags(Set<String> tags) {

        if (tags == null) {
            return;
        }
        if (this.tags == null) {
            this.tags = new HashSet<>();
        }
        this.tags.addAll(tags);
        this.tagsSlugs.addAll(CMDSCommonUtils.makeSlugs(tags));
    }

    public Set<String> getBoardIds() {
        return boardIds;
    }

    public void setBoardIds(Set<String> boardIds) {
        this.boardIds = boardIds;
    }

    public Set<String> getTargetIds() {
        return targetIds;
    }

    public void setTargetIds(Set<String> targetIds) {
        this.targetIds = targetIds;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
        this.tagsSlugs = CMDSCommonUtils.makeSlugs(tags);
    }

    public VedantuRecordState getRecordState() {
        return recordState;
    }

    public void setRecordState(VedantuRecordState recordState) {
        this.recordState = recordState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
        this.gradesSlugs = CMDSCommonUtils.makeSlugs(grades);
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
        this.topicsSlugs = CMDSCommonUtils.makeSlugs(topics);
    }

    public Set<String> getTargets() {
        return targets;
    }

    public void setTargets(Set<String> targets) {
        this.targets = targets;
        this.targetsSlugs = CMDSCommonUtils.makeSlugs(targets);
    }

    public Set<String> getSubtopics() {
        return subtopics;
    }

    public void setSubtopics(Set<String> subtopics) {
        this.subtopics = subtopics;
        this.subtopicsSlugs = CMDSCommonUtils.makeSlugs(subtopics);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
        this.subjectSlug = CMDSCommonUtils.makeSlug(subject);
    }

    public String getSubjectSlug() {
        return subjectSlug;
    }

    public void setSubjectSlug(String subjectSlug) {
        this.subjectSlug = subjectSlug;
    }

    public Set<String> getTopicsSlugs() {
        return topicsSlugs;
    }

    public void setTopicsSlugs(Set<String> topicsSlugs) {
        this.topicsSlugs = topicsSlugs;
    }

    public Set<String> getSubtopicsSlugs() {
        return subtopicsSlugs;
    }

    public void setSubtopicsSlugs(Set<String> subtopicsSlugs) {
        this.subtopicsSlugs = subtopicsSlugs;
    }

    public Set<String> getGradesSlugs() {
        return gradesSlugs;
    }

    public void setGradesSlugs(Set<String> gradesSlugs) {
        this.gradesSlugs = gradesSlugs;
    }

    public Set<String> getTargetsSlugs() {
        return targetsSlugs;
    }

    public void setTargetsSlugs(Set<String> targetsSlugs) {
        this.targetsSlugs = targetsSlugs;
    }

    public Set<String> getTagsSlugs() {
        return tagsSlugs;
    }

    public void setTagsSlugs(Set<String> tagsSlugs) {
        this.tagsSlugs = tagsSlugs;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String NAME = "name";
        public static final String RECORD_STATE = "recordState";
        public static final String BOARD_IDS = "boardIds";
        public static final String TARGET_IDS = "targetIds";
        public static final String GRADES = "grades";
        public static final String TAGS = "tags";
        public static final String TARGETS = "targets";
        public static final String SUBJECT = "subject";
        public static final String TOPICS = "topics";
        public static final String GRADES_SLUGS = "gradesSlugs";
        public static final String TAGS_SLUGS = "tagsSlugs";
        public static final String TARGETS_SLUGS = "targetsSlugs";
        public static final String SUBJECT_SLUG = "subjectSlug";
        public static final String TOPICS_SLUGS = "topicsSlugs";
    }
}
