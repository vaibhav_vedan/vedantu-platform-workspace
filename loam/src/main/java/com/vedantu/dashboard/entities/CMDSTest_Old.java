package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.enums.TestMode;
import com.vedantu.dashboard.pojo.CMDSTestState;
import com.vedantu.dashboard.pojo.CustomSectionEvaluationRule;
import com.vedantu.dashboard.pojo.TestMetadata;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.CollectionUtils;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

import static com.vedantu.platform.enums.SocialContextType.CMDSTEST;

@Document(collection = "CMDSTest_Old")
public class CMDSTest_Old extends AbstractCMDSEntity {

    private CMDSTestState testState;
    //private List<CMDSTestQuestion> questions;
    private int versionNo = 1;
    private String questionSetId;
    private SocialContextType socialContextType = CMDSTEST;
    public String description;
    public int qusCount;
    public Long duration;
    public Float totalMarks = 0f;
    public ContentInfoType contentInfoType;
    @Indexed(background = true)
    private boolean visibleInApp = false;
    public List<TestMetadata> metadata;

    public EnumBasket.TestType type;
    public EnumBasket.TestTagType testTag;

    public TestMode mode;
    public String code;                                             // unique code
    // for test
    // inside an
    // organization
    // (if
    // applicable)

    public long attempts;
    public boolean published;

    public List<String> childrenIds;                                      // (if this is
    // testGroup
    // then members
    // of
    // this group)
    public String parentId;                                         // if this is
    // part of a
    // testGroup
    // (testGroupId)

    //    @ElementCollection
//    public List<TestQuestionSet> sets;                                             // if this test
    // have
    // different
    // sets of
    // question
    public TestResultVisibility resultVisibility;
    public String resultVisibilityMessage;
    private Long doNotShowResultsTill;
    private boolean hardStop=false;
    public Long minStartTime;
    @Indexed(background = true)
    public Long maxStartTime;
    public boolean reattemptAllowed = false;
    public boolean globalRankRequired = false;
    private SignUpRestrictionLevel signupHook;
    private List<String> instructions;

    private List<CustomSectionEvaluationRule> customSectionEvaluationRules;

    // Marketing related dummy fields for now
    private String marketingRedirectUrl;

    // Only for subjective tests
    private Long expiryDate;// for public test
    private Long expiryDays;// for private test

    public String getMarketingRedirectUrl() {
        return marketingRedirectUrl;
    }

    public void setMarketingRedirectUrl(String marketingRedirectUrl) {
        this.marketingRedirectUrl = marketingRedirectUrl;
    }

    public SignUpRestrictionLevel getSignupHook() {
        return signupHook;
    }

    public void setSignupHook(SignUpRestrictionLevel signupHook) {
        this.signupHook = signupHook;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public CMDSTestState getTestState() {
        return testState;
    }

    public void setTestState(CMDSTestState testState) {
        this.testState = testState;
    }

    public String getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(String questionSetId) {
        this.questionSetId = questionSetId;
    }

    public int getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(int versionNo) {
        this.versionNo = versionNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQusCount() {
        return qusCount;
    }

    public void setQusCount(int qusCount) {
        this.qusCount = qusCount;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public List<TestMetadata> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<TestMetadata> metadata) {
        this.metadata = metadata;
    }

    public EnumBasket.TestType getType() {
        return type;
    }

    public void setType(EnumBasket.TestType type) {
        this.type = type;
    }

    public TestMode getMode() {
        return mode;
    }

    public void setMode(TestMode mode) {
        this.mode = mode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getAttempts() {
        return attempts;
    }

    public void setAttempts(long attempts) {
        this.attempts = attempts;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public List<String> getChildrenIds() {
        return childrenIds;
    }

    public void setChildrenIds(List<String> childrenIds) {
        this.childrenIds = childrenIds;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public TestResultVisibility getResultVisibility() {
        return resultVisibility;
    }

    public void setResultVisibility(TestResultVisibility resultVisibility) {
        this.resultVisibility = resultVisibility;
    }

    public String getResultVisibilityMessage() {
        return resultVisibilityMessage;
    }

    public void setResultVisibilityMessage(String resultVisibilityMessage) {
        this.resultVisibilityMessage = resultVisibilityMessage;
    }

    public Long getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Long minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Long getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public boolean isReattemptAllowed() {
        return reattemptAllowed;
    }

    public void setReattemptAllowed(boolean reattemptAllowed) {
        this.reattemptAllowed = reattemptAllowed;
    }

    public boolean isGlobalRankRequired() {
        return globalRankRequired;
    }

    public void setGlobalRankRequired(boolean globalRankRequired) {
        this.globalRankRequired = globalRankRequired;
    }

    public ContentInfoType getContentInfoType() {
        return contentInfoType;
    }

    public void setContentInfoType(ContentInfoType contentInfoType) {
        this.contentInfoType = contentInfoType;
    }

    public List<String> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<String> instructions) {
        this.instructions = instructions;
    }

    public boolean isHardStop() {
        return hardStop;
    }

    public void setHardStop(boolean hardStop) {
        this.hardStop = hardStop;
    }

    public Long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getExpiryDays() {
        return expiryDays;
    }

    public void setExpiryDays(Long expiryDays) {
        this.expiryDays = expiryDays;
    }

    public void updateTags() {
        // TODO : implement this
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<String>();
        //if (this.duration == null) {
        //	errors.add("duration");
        //}
        if (this.testState == null) {
            errors.add("state");
        }
//		if (CollectionUtils.isEmpty(this.metadata)) {
//			errors.add("metadata");
//		} else {
//			for(TestMetadata testMetadata: this.metadata) {
//				for (CMDSTestQuestion question : testMetadata.getQuestions()) {
//					if (StringUtils.isEmpty(question.getQuestionId())) {
//						errors.add("questionId");
//					} else if (question.getMarks() == null) {
//						errors.add("question marks misssing :" + question.getQuestionId());
//					}
//				}
//			}
//		}

        return errors;
    }

    public float calculateTotalMarks() {
        float totalMarks = 0;
        if (!CollectionUtils.isEmpty(this.metadata)) {
            for (TestMetadata testMetadata : this.metadata) {
                for (CMDSTestQuestion cmdsTestQuestion : testMetadata.getQuestions()) {
                    totalMarks += cmdsTestQuestion.getMarks().getPositive();
                }
            }
        }

        return totalMarks;
    }

    public EnumBasket.TestTagType getTestTag() {
        return testTag;
    }

    public void setTestTag(EnumBasket.TestTagType testTag) {
        this.testTag = testTag;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String _ID = "_id";
        public static final String TEST_STATE = "testState";
        public static final String VERSION_NUMBER = "versionNo";
        public static final String QUESTION_SET_ID = "questionSetId";
        public static final String CODE = "code";
        public static final String TEST_TAG = "testTag";
        public static final String MAX_START_TIME = "maxStartTime";
        public static final String MIN_START_TIME = "minStartTime";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String QUESTION_ID = "metadata.questions.questionId";
        public static final String VISIBLE_IN_APP = "visibleInApp";
        public static final String METADATA_DURATION  = "metadata.duration";
        public static final String METADATA  = "metadata";
        public static final String DURATION  = "duration";
        public static final String QUS_COUNT  = "qusCount";
    }

    /**
     * @return the customSectionEvaluationRules
     */
    public List<CustomSectionEvaluationRule> getCustomSectionEvaluationRules() {
        return customSectionEvaluationRules;
    }

    /**
     * @param customSectionEvaluationRules the customSectionEvaluationRules to
     * set
     */
    public void setCustomSectionEvaluationRules(List<CustomSectionEvaluationRule> customSectionEvaluationRules) {
        this.customSectionEvaluationRules = customSectionEvaluationRules;
    }

    public boolean isVisibleInApp() {
        return visibleInApp;
    }

    public void setVisibleInApp(boolean visibleInApp) {
        this.visibleInApp = visibleInApp;
    }

    public Long getDoNotShowResultsTill() {
        return doNotShowResultsTill;
    }

    public void setDoNotShowResultsTill(Long doNotShowResultsTill) {
        this.doNotShowResultsTill = doNotShowResultsTill;
    }

}
