package com.vedantu.dashboard.entities;

import com.vedantu.dashboard.enums.TestMode;
import com.vedantu.dashboard.pojo.CMDSTestState;
import com.vedantu.dashboard.pojo.CustomSectionEvaluationRule;
import com.vedantu.dashboard.pojo.TestMetadata;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.CollectionUtils;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

import static com.vedantu.platform.enums.SocialContextType.CMDSTEST;

@Document(collection = "CMDSTest")
@CompoundIndexes({
        @CompoundIndex(name = "duration_entitystate_creationtime", def = "{'metadata.duration': 1, 'entityState' : 1, 'creationTime' : -1}}", background = true)
})
public class CMDSTest extends AbstractCMDSEntity {

    private CMDSTestState testState;
    //private List<CMDSTestQuestion> questions;
    private int versionNo = 1;

    public String description;
    public int qusCount;
    public Long duration;
    public Float totalMarks = 0f;
    public ContentInfoType contentInfoType;

    public List<TestMetadata> metadata;

    public TestMode mode;


    public long attempts;
    public boolean published;


    public Long minStartTime;
    @Indexed(background = true)
    public Long maxStartTime;


    public CMDSTest() {
        super();
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public CMDSTestState getTestState() {
        return testState;
    }

    public void setTestState(CMDSTestState testState) {
        this.testState = testState;
    }

    public int getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(int versionNo) {
        this.versionNo = versionNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQusCount() {
        return qusCount;
    }

    public void setQusCount(int qusCount) {
        this.qusCount = qusCount;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public List<TestMetadata> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<TestMetadata> metadata) {
        this.metadata = metadata;
    }

    public TestMode getMode() {
        return mode;
    }

    public void setMode(TestMode mode) {
        this.mode = mode;
    }

    public long getAttempts() {
        return attempts;
    }

    public void setAttempts(long attempts) {
        this.attempts = attempts;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }


    public Long getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Long minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Long getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public ContentInfoType getContentInfoType() {
        return contentInfoType;
    }

    public void setContentInfoType(ContentInfoType contentInfoType) {
        this.contentInfoType = contentInfoType;
    }


    public static class Constants extends AbstractCMDSEntity.Constants {
        public static final String _ID = "_id";
        public static final String TEST_STATE = "testState";
        public static final String MAX_START_TIME = "maxStartTime";
        public static final String MIN_START_TIME = "minStartTime";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String METADATA_DURATION  = "metadata.duration";
        public static final String METADATA  = "metadata";
        public static final String DURATION  = "duration";
        public static final String QUS_COUNT  = "qusCount";
    }

}
