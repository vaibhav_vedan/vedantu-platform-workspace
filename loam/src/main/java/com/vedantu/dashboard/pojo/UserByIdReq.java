package com.vedantu.dashboard.pojo;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserByIdReq {

    private List<Long> userIds;
    private String callerType;
}
