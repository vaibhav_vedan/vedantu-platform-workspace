/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo.helper;

/**
 *
 * @author ajith
 */
public class DoubtData {

    private Long serverTime;
    private String status;
    private Long taId;
    private String doubtMessage;

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTaId() {
        return taId;
    }

    public void setTaId(Long taId) {
        this.taId = taId;
    }

    public String getDoubtMessage() {
        return doubtMessage;
    }

    public void setDoubtMessage(String doubtMessage) {
        this.doubtMessage = doubtMessage;
    }

    @Override
    public String toString() {
        return "DoubtData{" + "serverTime=" + serverTime + ", status=" + status + ", taId=" + taId + ", doubtMessage=" + doubtMessage + '}';
    }

}
