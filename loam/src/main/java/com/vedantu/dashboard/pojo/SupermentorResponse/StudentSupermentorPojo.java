package com.vedantu.dashboard.pojo.SupermentorResponse;

import com.vedantu.User.StudentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentSupermentorPojo {
    private String fullName;
    private Long id;
    private String profilePicUrl;
    private StudentInfoPojo studentInfo = new StudentInfoPojo();
}
