package com.vedantu.dashboard.pojo;

import com.sun.mail.imap.protocol.FLAGS;

public class AMQuestionStatsAvg {
	
	private Double noCorrect = 0.0;

	private Double noWrong = 0.0;
	private Double noUnattempted = 0.0;

	private Float marksScored = 0.0f;
	private Float marksMissed = 0.0f;
	private Float marksUnattempted = 0.0f;

	public AMQuestionStatsAvg(Double noCorrect, Double noWrong, Double noUnattempted,
					Float marksScored, Float marksMissed, Float marksUnattempted) {
		this.noCorrect = noCorrect;
		this.noWrong = noWrong;
		this.noUnattempted = noUnattempted;
		this.marksScored = marksScored;
		this.marksMissed = marksMissed;
		this.marksUnattempted = marksUnattempted;
	}

	
	public AMQuestionStatsAvg() {
		super();
	}
	
	public Double getNoCorrect() {
		return noCorrect;
	}
	public void setNoCorrect(Double noCorrect) {
		this.noCorrect = noCorrect;
	}
	public Double getNoWrong() {
		return noWrong;
	}
	public void setNoWrong(Double noWrong) {
		this.noWrong = noWrong;
	}
	public Double getNoUnattempted() {
		return noUnattempted;
	}
	public void setNoUnattempted(Double noUnattempted) {
		this.noUnattempted = noUnattempted;
	}

	public void addUnatempted(float maxMarks) {
		this.marksUnattempted += maxMarks;
	}

	public void addMissed(float marksMissed) {
		this.marksMissed += marksMissed;
	}

	public void addCorrect(float marksScored) {
		this.marksScored += marksScored;
	}

	public Float getMarksScored() {
		return marksScored;
	}

	public void setMarksScored(Float marksScored) {
		this.marksScored = marksScored;
	}

	public Float getMarksMissed() {
		return marksMissed;
	}

	public void setMarksMissed(Float marksMissed) {
		this.marksMissed = marksMissed;
	}

	public Float getMarksUnattempted() {
		return marksUnattempted;
	}

	public void setMarksUnattempted(Float marksUnattempted) {
		this.marksUnattempted = marksUnattempted;
	}
}
