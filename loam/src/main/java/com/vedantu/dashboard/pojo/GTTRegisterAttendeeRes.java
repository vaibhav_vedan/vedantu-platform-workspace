package com.vedantu.dashboard.pojo;

public class GTTRegisterAttendeeRes {
	/*
	 * {"joinUrl":
	 * "https://global.gototraining.com/join/training/893408553371535874/107393998"
	 * ,"confirmationUrl":
	 * "https://attendee.gototraining.com/registration/confirmation.tmpl?registrant=8437663606892038401&training=893408553371535874"
	 * ,"registrantKey":"8437663606892038401"}
	 */
	private String joinUrl;
	private String confirmationUrl;
	private String registrantKey;

	public GTTRegisterAttendeeRes() {
		super();
	}

	public String getJoinUrl() {
		return joinUrl;
	}

	public void setJoinUrl(String joinUrl) {
		this.joinUrl = joinUrl;
	}

	public String getConfirmationUrl() {
		return confirmationUrl;
	}

	public void setConfirmationUrl(String confirmationUrl) {
		this.confirmationUrl = confirmationUrl;
	}

	public String getRegistrantKey() {
		return registrantKey;
	}

	public void setRegistrantKey(String registrantKey) {
		this.registrantKey = registrantKey;
	}

	@Override
	public String toString() {
		return "GTTRegisterAttendeeRes [joinUrl=" + joinUrl + ", confirmationUrl=" + confirmationUrl
				+ ", registrantKey=" + registrantKey + ", toString()=" + super.toString() + "]";
	}
}
