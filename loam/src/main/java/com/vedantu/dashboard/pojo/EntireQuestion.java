package com.vedantu.dashboard.pojo;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.dashboard.constants.QuestionSetFileConstants;
import com.vedantu.dashboard.entities.CMDSQuestion;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.dashboard.utils.CMDSCommonUtils;
import com.vedantu.dashboard.pojo.challenges.HintFormat;
import com.vedantu.dashboard.pojo.metadata.*;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EntireQuestion {

    public static final String REGEX_EXCEPT_ALPHANUMERIC_COMMA = "[^a-zA-Z0-9,]";
    public static final String REGEX_NUMERIC_ANSWER = "[^a-zA-Z0-9^.\\-;]";
    public static final String REGEX_ANSWER_STRING_MATCH = "([a-z]|,|[1-2])+";
    private static final String regex = "^([\\s]*)([(]?)([a-zA-Z0-9]{1,3})([)\\.])"; // /
    // TODO
    // check
    // what
    // it
    // does?
    // seems
    // like
    // it
    // will
    // have
    // only
    // 3
    // values
    public static final String REGEX_HTML_STRIP = "<[^>]*>";
    private static final Pattern p = Pattern.compile(regex);
    public String answer = StringUtils.EMPTY;
    public List<RichTextFormat> cola = new ArrayList<>();
    public List<RichTextFormat> colb = new ArrayList<>();
    public Map<String, Set<String>> gridAnswer = new HashMap<>();
    public QuestionParts state;

    public OptionFormat formattedOptions = new OptionFormat();
    public RichTextFormat formattedQuestion = new RichTextFormat();
    public List<SolutionFormat> formattedSolutions = new ArrayList<>();
    // public List<String> qulutions = new ArrayList<>();
    public ParseQuestionMetadata metadata = null;
    public List<HintFormat> formattedHints = new ArrayList<>();

    public EntireQuestion() {
        metadata = new ParseQuestionMetadata();
    }

    public QuestionParts accumulateQuestionInfo(boolean isStart, String runText, String htmlText, List<CMDSImageDetails> uuids,
            boolean inHTML, boolean imgInBase64, boolean newPara, int leftMargin) {

        System.out.println(">>> state is " + state);
        System.out.println(">>> newPara is " + newPara);

        if (StringUtils.isEmpty(htmlText.trim()) && !(QuestionParts.SOLUTION.equals(state)
                || QuestionParts.QUESTION.equals(state))) {
            return null;
        }
        boolean changeOfState = false;
        if (isStart) {
            String checkStart = runText.trim().toLowerCase().replaceAll(REGEX_HTML_STRIP, "").trim();
            if (CMDSCommonUtils.doesStartsWith(htmlText, QuestionSetFileConstants.PREFIX_QUESTION)) {
                state = QuestionParts.QUESTION;
                changeOfState = true;
            } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_OPTIONS)) {
                state = QuestionParts.OPTIONS;
                changeOfState = true;
            } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_ANSWER)) {
                state = QuestionParts.ANSWER;
                changeOfState = true;
            } else if (CMDSCommonUtils.doesStartsWith(htmlText, QuestionSetFileConstants.PREFIX_SOLUTION)) {
                state = QuestionParts.SOLUTION;
                changeOfState = true;
            } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_HINTS)) {
                state = QuestionParts.HINT;
                changeOfState = true;
            } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_COLUMNA)) {
                state = QuestionParts.COLUMNA;
                changeOfState = true;
            } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_COLUMNB)) {
                state = QuestionParts.COLUMNB;
                changeOfState = true;
            }
        }
        System.out.println(">>> changeOfState is " + changeOfState);
        if (null != state) {
            if (!changeOfState && newPara && (state == QuestionParts.QUESTION || state == QuestionParts.SOLUTION)) {
                System.out.println("adding br tag");
//                htmlText = StringUtils.isNotEmpty(htmlText.trim()) ? "<br>" + htmlText : htmlText;
                htmlText = CMDSCommonUtils.handleStartingSpacesAndTabs(htmlText);
                if (leftMargin > 0) {
                    htmlText = "<div style='display:inline-block;margin-left:"
                            + leftMargin + "px' class='qr-indent'>" + htmlText + "</div>";
                }
                htmlText = "<br>" + htmlText;
            }
            state.accumulate(this, isStart, htmlText, uuids, inHTML, imgInBase64);
        }
        return state;
    }

    public void addParentUlTags(Map<String, Integer> listCounterMap) {
        if (null != state) {
            state.addParentUlTags(this, listCounterMap);
        }
    }

    public static List<RichTextFormat> formatOptions(List<RichTextFormat> options, List<String> optionOrder) {

        List<RichTextFormat> convertedOption = new ArrayList<>();
        int n = 0;
        if (CollectionUtils.isNotEmpty(options)) {
            for (RichTextFormat option : options) {
                Matcher m = p.matcher(option.getNewText());
                if (m.find()) {
                    n++;
                    String opn = m.group();
                    optionOrder.add(opn);
                    // option = option.replaceFirst(regex, "(" + n + ") ");
                    option.setNewText(option.getNewText().replaceFirst(regex, ""));
                    convertedOption.add(option);
                } else if (n != 0) {
                    int lastIndex = convertedOption.size() - 1;
                    RichTextFormat lastOption = convertedOption.get(lastIndex);
                    lastOption.setNewText(lastOption.getNewText() + option.getNewText());
                    lastOption.addAllUuidImages(option.getUuidImages());
//                    String lastOption = convertedOption.get(lastIndex) + option;
//                    convertedOption.set(lastIndex, lastOption);
                }
            }
        }
        return convertedOption;
    }

    public CMDSQuestion toQuestion(EntireQuestion entireQuestion, String userId,
                                   String questionSetName) throws BadRequestException {

        MCQsolutionInfo solutionInfo = null;
        // Default is not Para now

//        if (metadata.type == null) {
//            metadata.type = QuestionType.PARA;
//        }
        // change the options to 1,2,3...etc

        List<String> optionOrder = new ArrayList<>();
        int n = 0;
        List<String> convertedOption = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(entireQuestion.formattedOptions.newOptions)) {
            for (String option : entireQuestion.formattedOptions.newOptions) {
                Matcher m = p.matcher(option);
                if (m.find()) {
                    n++;
                    String opn = m.group();
                    optionOrder.add(opn);
                    // option = option.replaceFirst(regex, "(" + n + ") ");
                    option = option.replaceFirst(regex, "");
                    convertedOption.add(option);
                } else if (n != 0) {
                    int lastIndex = convertedOption.size() - 1;
                    String lastOption = convertedOption.get(lastIndex) + option;
                    convertedOption.set(lastIndex, lastOption);
                }
            }
            // if (!correctOptionFormat) {
            // throw new Exception(
            // "invalid answer, options format (bullets formating not allowed) :
            // original: "
            // + entireQuestion.of.originalOptions
            // + " , converted: "
            // + entireQuestion.of.newOptions);
            // }
        }
        // Checking for duplicate options and throw error if they are duplicate
        Set<String> convertedOptionSet = new HashSet<>();
        convertedOptionSet.addAll(convertedOption);
        if (convertedOptionSet.size() < convertedOption.size()) {
            throw new BadRequestException(ErrorCode.OPTION_DUPLICATION_OCCURED,
                    "Invalid options.Please review the options carefully. " + StringUtils.join(convertedOption, " , "));

        }
        // Fix ends
        entireQuestion.formattedOptions.optionOrder = optionOrder;
        entireQuestion.formattedOptions.originalOptions = entireQuestion.formattedOptions.newOptions;
        entireQuestion.formattedOptions.newOptions = convertedOption;
         if (metadata.type == QuestionType.MCQ) {
            if (StringUtils.isEmpty(entireQuestion.answer)) {
                throw new BadRequestException(ErrorCode.INVALID_QUESTION_FORMAT, "");
            }

            entireQuestion.answer = entireQuestion.answer.trim().replaceAll(REGEX_EXCEPT_ALPHANUMERIC_COMMA, "");

            String ans = entireQuestion.answer;
            String[] allAns = ans.split(",");

            if (allAns.length == 0) {
                throw new BadRequestException(ErrorCode.INVALID_ANSWER_FORMAT, "answer not provided in question");
            }

            List<String> mcqANS = new ArrayList<>();
            for (int i = 0; i < allAns.length; i++) {
                int ansi = 0;

                for (String ansOptn : entireQuestion.formattedOptions.optionOrder) {
                    ansi++;
                    if (StringUtils.equalsIgnoreCase(stripAnsFormat(ansOptn), stripAnsFormat(allAns[i]))) {
                        mcqANS.add("" + ansi + "");
                        break;
                    }
                }
            }

            solutionInfo = new MCQsolutionInfo(entireQuestion.formattedOptions, mcqANS);
        }
        if (solutionInfo != null) {
            solutionInfo.setSolutions(CollectionUtils.isEmpty(entireQuestion.formattedSolutions)
                    ? new ArrayList<SolutionFormat>()
                    : entireQuestion.formattedSolutions);
        }

        CMDSQuestion qrQuestion = new CMDSQuestion(entireQuestion.formattedQuestion, metadata.type, solutionInfo,
                userId, questionSetName, "", VedantuRecordState.TEMPORARY, metadata);

        return qrQuestion;
    }

    public static class InvalidMetadataException extends Exception {

        private static final long serialVersionUID = -6395800213997388287L;

        public InvalidMetadataException(String message, Throwable t) {

            super(message, t);
        }

        public InvalidMetadataException(String message) {

            super(message);
        }

    }

    public static void main(String[] a) {
        /*
        String regex = "\\{\\\\rmf\\}";
        String regex1 = "^([\\s]*)([(]?)([a-zA-Z0-9]{1,3})([)\\.]*)";
        System.out.println("<br>shanakr</b>".trim().replaceAll("<[^>]*>", "") + regex + ", " + regex1);
        List<String> z = new ArrayList<>();
        z.add("hello");
        z.add("testing");
        System.out.println(z);
        String p = z.get(z.size() - 1);
        p += "minus";
        z.set(z.size() - 1, p);
        System.out.println(z);
         */

        String s2 = " \t \t this \t is a string ";
        s2 = "  ";
        String s2trimmed = s2.trim();
        while (s2.contains("\t") && s2.indexOf("\t") < s2.indexOf(s2trimmed)) {
            System.err.println(">> replaincg");
            s2 = s2.replaceFirst("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        }
        while (s2.contains(" ") && s2.indexOf(" ") < s2.indexOf(s2trimmed)) {
            System.err.println(">> replaincg space");
            s2 = s2.replaceFirst(" ", "&nbsp;");
        }
        System.err.println(">>>are" + s2);
    }

    public static String stripAnsFormat(String ans) {

        return ans.trim().replace("(", "").replace(")", "").replace(".", "");
    }

    public void resetState() {
        state = null;
    }

    @Override
    public String toString() {
        return "EntireQuestion{" + "answer=" + answer + ", cola=" + cola + ", colb=" + colb + ", gridAnswer=" + gridAnswer + ", state=" + state + ", formattedOptions=" + formattedOptions + ", formattedQuestion=" + formattedQuestion + ", formattedSolutions=" + formattedSolutions + ", metadata=" + metadata + ", formattedHints=" + formattedHints + '}';
    }

}
