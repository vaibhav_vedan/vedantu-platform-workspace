package com.vedantu.dashboard.pojo.helper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AMInClassConversation {
    private String _id;
    private String message;
    private String from;
    private String to;
    private String serverTime;
    private AMInClassDoubtChatFromUserInfo fromUserInfo;
    private String contentType;
    private String scribbleUrl;
}
