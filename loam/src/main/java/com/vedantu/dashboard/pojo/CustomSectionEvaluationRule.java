/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo;

import java.util.Set;

/**
 *
 * @author parashar
 */
public class CustomSectionEvaluationRule {
    
    private int bestOf;
    private Set<String> testMetadataKey;

    /**
     * @return the bestOf
     */
    public int getBestOf() {
        return bestOf;
    }

    /**
     * @param bestOf the bestOf to set
     */
    public void setBestOf(int bestOf) {
        this.bestOf = bestOf;
    }

    /**
     * @return the testMetadataKey
     */
    public Set<String> getTestMetadataKey() {
        return testMetadataKey;
    }

    /**
     * @param testMetadataKey the testMetadataKey to set
     */
    public void setTestMetadataKey(Set<String> testMetadataKey) {
        this.testMetadataKey = testMetadataKey;
    }
    
}
