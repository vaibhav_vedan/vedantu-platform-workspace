package com.vedantu.dashboard.pojo.subscription;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class AcadMentorStudents extends AbstractMongoStringIdEntity {
    private Long acadMentorId;
    private Long studentId;

    public Long getAcadMentorId() {
        return acadMentorId;
    }

    public void setAcadMentorId(Long acadMentorId) {
        this.acadMentorId = acadMentorId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }
}
