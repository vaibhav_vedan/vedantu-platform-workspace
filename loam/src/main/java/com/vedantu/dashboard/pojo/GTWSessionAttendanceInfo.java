package com.vedantu.dashboard.pojo;

import java.util.List;


public class GTWSessionAttendanceInfo {

	private String firstName;
	private String lastName;
	private String sessionKey;
	private Long attendanceTimeInSeconds;
	private String registrantKey;
	private String email;
	private List<GTTSessionAttendeeSlot> attendance;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public Long getAttendanceTimeInSeconds() {
		return attendanceTimeInSeconds;
	}

	public void setAttendanceTimeInSeconds(Long attendanceTimeInSeconds) {
		this.attendanceTimeInSeconds = attendanceTimeInSeconds;
	}

	public String getRegistrantKey() {
		return registrantKey;
	}

	public void setRegistrantKey(String registrantKey) {
		this.registrantKey = registrantKey;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<GTTSessionAttendeeSlot> getAttendance() {
		return attendance;
	}

	public void setAttendance(List<GTTSessionAttendeeSlot> attendance) {
		this.attendance = attendance;
	}
}
