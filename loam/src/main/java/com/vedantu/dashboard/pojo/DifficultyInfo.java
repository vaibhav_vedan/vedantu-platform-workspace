package com.vedantu.dashboard.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DifficultyInfo {
    private Double totalMarks = 0.0;
    private Integer count = 0;
}
