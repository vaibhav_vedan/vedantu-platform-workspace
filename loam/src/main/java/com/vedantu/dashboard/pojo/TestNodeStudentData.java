package com.vedantu.dashboard.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestNodeStudentData {

    private Map<String, StudentMetaData> studentMetaDataMap;
}
