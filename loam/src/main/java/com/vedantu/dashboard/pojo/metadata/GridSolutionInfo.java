package com.vedantu.dashboard.pojo.metadata;

import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.dashboard.pojo.EntireQuestion;
import com.vedantu.dashboard.pojo.OptionFormat;
import com.vedantu.dashboard.pojo.SolutionInfo;

import java.util.*;
import java.util.Map.Entry;

public class GridSolutionInfo extends SolutionInfo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public List<RichTextFormat> cola;
    public List<RichTextFormat> colb;
    public Map<String, List<String>> gridAnswer;

    public List<String> optionOrdera;
    public List<String> optionOrderb;
    public List<RichTextFormat> originalCola;
    public List<RichTextFormat> originalColb;
    public Map<String, Set<String>> originalGridAnswer;

    public GridSolutionInfo() {
        this(new HashMap<String, Set<String>>(), new OptionFormat(), new ArrayList<RichTextFormat>(), new ArrayList<RichTextFormat>());
    }

    public GridSolutionInfo(Map<String, Set<String>> ans, OptionFormat op, List<RichTextFormat> ca, List<RichTextFormat> cb) {
        super(op);
        optionOrdera = new ArrayList<>();
        optionOrderb = new ArrayList<>();
        this.cola = EntireQuestion.formatOptions(ca, optionOrdera);
        this.colb = EntireQuestion.formatOptions(cb, optionOrderb);
        originalCola = ca;
        originalColb = cb;
        originalGridAnswer = ans;
        this.gridAnswer = new LinkedHashMap<>();
        for (Entry<String, Set<String>> entry : ans.entrySet()) {
            Set<String> matches = entry.getValue();
            List<String> nOptn = new ArrayList<>();
            for (String m : matches) {
                if (optionOrderb.contains(m)) {
                    nOptn.add(Integer.toString((optionOrderb.indexOf(m)) + 1));
                }
            }
            gridAnswer.put(Integer.toString((optionOrdera.indexOf(entry.getKey().trim()) + 1)), nOptn);
        }

    }

    @Override
    public void addHook() {
        super.addHook();
        if (cola != null && !cola.isEmpty()) {
            for (RichTextFormat option : cola) {
                option.addHook();
            }
        }
        if (colb != null && !colb.isEmpty()) {
            for (RichTextFormat option : colb) {
                option.addHook();
            }
        }
    }

    @Override
    public List<String> getAnswer() {
        //TODO: Implement this
        return null;
    }

    @Override
    public void setAnswer(List<String> answer) {
        //TODO 
    }

}
