package com.vedantu.dashboard.pojo.challenges;

import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.lms.cmds.interfaces.ILatexProcessor;

public class HintFormat extends RichTextFormat implements ILatexProcessor{

    private Integer deduction;

    public HintFormat() {
    }

    public Integer getDeduction() {
        return deduction;
    }

    public void setDeduction(Integer deduction) {
        this.deduction = deduction;
    }
}
