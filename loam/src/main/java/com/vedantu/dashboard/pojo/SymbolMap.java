package com.vedantu.dashboard.pojo;

import com.vedantu.util.ConfigUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SymbolMap {

	private static SymbolMap INSTANCE = null;

	public Map<String, String> symbol;

	public SymbolMap() throws IOException {
		symbol = new HashMap<String, String>();
		setSymbols();
	}

	public static SymbolMap getSymbolMap() {
		if (INSTANCE == null) {
			SymbolMap symbolMap = null;
			try {
				symbolMap = new SymbolMap();
				INSTANCE = symbolMap;
				return symbolMap;
			} catch (Exception ex) {
			}
			return symbolMap;
		} else {
			return INSTANCE;
		}
	}

	private void setSymbols() throws IOException {
		// TODO : config params play.absolute.path and symbol.file
		Resource resource = new ClassPathResource("/" + ConfigUtils.INSTANCE.getStringValue("symbol.file"));
		InputStream fileInput = resource.getInputStream();
		DataInputStream din = new DataInputStream(fileInput);
		BufferedReader br = new BufferedReader(new InputStreamReader(din));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				String[] map = StringUtils.split(line);
				// for(String s:map){
				// LOGGER.info("map: "+s);
				// }
				if (map.length >= 2) {
					String key = map[0];
					String value = map[1];
					key = StringEscapeUtils.unescapeJava("\\uF0" + map[1].trim());
					value = StringEscapeUtils.unescapeJava("\\u" + map[0].trim());
					symbol.put(key, value);
				}
			}

			Iterator<String> it = symbol.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next().toString();
				String value = symbol.get(key);
			}
		} catch (IOException e) {
		} finally {
			try {
				fileInput.close();
				din.close();
				br.close();
			} catch (IOException e) {
			}

		}
	}

}
