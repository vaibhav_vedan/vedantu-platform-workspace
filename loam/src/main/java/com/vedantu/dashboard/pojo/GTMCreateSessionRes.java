package com.vedantu.dashboard.pojo;

import com.vedantu.util.StringUtils;

public class GTMCreateSessionRes {
	/*
	 * [ { "conferenceCallInfo":"VoIP", "meetingid":175022333,
	 * "uniqueMeetingId":175022333,
	 * "joinURL":"https://www.gotomeeting.com/join/175022333",
	 * "maxParticipants":26}]
	 */

	private String joinURL;
	private String meetingid;
	private String conferenceCallInfo;

	public GTMCreateSessionRes() {
		super();
	}

	public String getJoinURL() {
		return joinURL;
	}

	public void setJoinURL(String joinURL) {
		this.joinURL = joinURL;
	}

	public String getMeetingid() {
		return meetingid;
	}

	public void setMeetingid(String meetingid) {
		this.meetingid = meetingid;
	}

	public String getConferenceCallInfo() {
		return conferenceCallInfo;
	}

	public void setConferenceCallInfo(String conferenceCallInfo) {
		this.conferenceCallInfo = conferenceCallInfo;
	}

	public void validate() throws Exception {
		if (StringUtils.isEmpty(this.joinURL) || StringUtils.isEmpty(this.meetingid)) {
			throw new Exception("Invalid response from GTM. Link did not get generated. response : " + this.toString());
		}
	}

	@Override
	public String toString() {
		return "GTMCreateSessionRes [joinURL=" + joinURL + ", meetingid=" + meetingid + ", conferenceCallInfo="
				+ conferenceCallInfo + ", toString()=" + super.toString() + "]";
	}
}
