package com.vedantu.dashboard.pojo;

import com.vedantu.dashboard.entities.BaseTopicTree;
import com.vedantu.dashboard.entities.CMDSQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class QuestionNodesCorrectenss {
    private String questionId;
    private CMDSQuestion question;
    private int correctNess;
    private Set<BaseTopicTree> nodes;
    private String studentId;
    private Float marksGiven;
    private Float totalMarks;
}
