package com.vedantu.dashboard.pojo;

public class AMQuestionStats {

	private Long noCorrect = 0l;
	private Long noWrong = 0l;
	private Long noUnattempted = 0l;

	private Float marksScored = 0.0f;
	private Float marksMissed = 0.0f;
	private Float marksUnattempted = 0.0f;

	public AMQuestionStats() {
		super();
	}

	public AMQuestionStats(Long noCorrect, Long noWrong, Long noUnattempted, Float marksScored, Float marksMissed, Float marksUnattempted) {
		super();
		this.noCorrect = noCorrect;
		this.noWrong = noWrong;
		this.noUnattempted = noUnattempted;
		this.marksScored = marksScored;
		this.marksMissed = marksMissed;
		this.marksUnattempted = marksUnattempted;
	}

	public Long getNoCorrect() {
		return noCorrect;
	}

	public void setNoCorrect(Long noCorrect) {
		this.noCorrect = noCorrect;
	}

	public Long getNoWrong() {
		return noWrong;
	}

	public void setNoWrong(Long noWrong) {
		this.noWrong = noWrong;
	}

	public Long getNoUnattempted() {
		return noUnattempted;
	}

	public void setNoUnattempted(Long noUnattempted) {
		this.noUnattempted = noUnattempted;
	}

	public void incrementNoCorrect() {
		this.noCorrect++;
	}

	public void incrementNoWrong() {
		this.noWrong++;
	}

	public void incrementNoUnattempted() {
		this.noUnattempted++;
	}
	
	@Override
	public String toString() {
		return "AMQuestionStats[noCorrect: "+ noCorrect + ", " + "noWrong: " + noWrong + "noUnattempted: " + noUnattempted + "]";
	}

	public void addUnatempted(float maxMarks) {
		this.marksUnattempted += maxMarks;
	}

	public void addMissed(float marksMissed) {
		this.marksMissed += marksMissed;
	}

	public void addCorrect(float marksScored) {
		this.marksScored += marksScored;
	}

	public Float getMarksScored() {
		return marksScored;
	}

	public void setMarksScored(Float marksScored) {
		this.marksScored = marksScored;
	}

	public Float getMarksMissed() {
		return marksMissed;
	}

	public void setMarksMissed(Float marksMissed) {
		this.marksMissed = marksMissed;
	}

	public Float getMarksUnattempted() {
		return marksUnattempted;
	}

	public void setMarksUnattempted(Float marksUnattempted) {
		this.marksUnattempted = marksUnattempted;
	}
}
