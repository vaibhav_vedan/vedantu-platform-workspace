package com.vedantu.dashboard.pojo;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import com.vedantu.lms.cmds.utils.LatexProcessor;
import com.vedantu.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OptionFormat implements ILatexProcessor {

    public Set<CMDSImageDetails> uuidImages;
    public List<String> originalOptions;
    public List<String> newOptions;
    public List<String> optionOrder;

    public OptionFormat() {

        this.uuidImages = new HashSet<>();
        this.originalOptions = new ArrayList<>();
        this.newOptions = new ArrayList<>();
        this.optionOrder = new ArrayList<>();
    }

    public Set<CMDSImageDetails> getUuidImages() {
        return uuidImages;
    }

    public void setUuidImages(Set<CMDSImageDetails> uuidImages) {
        this.uuidImages = uuidImages;
    }

    public List<String> getOriginalOptions() {
        return originalOptions;
    }

    public void setOriginalOptions(List<String> originalOptions) {
        this.originalOptions = originalOptions;
    }

    public List<String> getNewOptions() {
        return newOptions;
    }

    public void setNewOptions(List<String> newOptions) {
        this.newOptions = newOptions;
    }

    public List<String> getOptionOrder() {
        return optionOrder;
    }

    public void setOptionOrder(List<String> optionOrder) {
        this.optionOrder = optionOrder;
    }

    @Override
    public void addHook() {

        if (CollectionUtils.isNotEmpty(newOptions)) {
            List<String> finalOption = new ArrayList<>();
            for (String option : this.newOptions) {
                finalOption.add(LatexProcessor.addHookToLatex(option));
            }

            this.newOptions = finalOption;
        }

    }

    @Override
    public String toString() {
        return "OptionFormat{" + "uuidImages=" + uuidImages + ", originalOptions=" + originalOptions + ", newOptions=" + newOptions + ", optionOrder=" + optionOrder + '}';
    }

}
