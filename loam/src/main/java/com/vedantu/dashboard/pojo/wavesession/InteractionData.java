/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo.wavesession;

import com.vedantu.dashboard.enums.InteractionType;

/**
 *
 * @author ajith
 */
public class InteractionData {

    private InteractionType interactionType;
    private boolean isCorrect;
    private String selected;
    private String correctAnswer;
    private Integer timeTaken;//millis
    private String slideId;
    private Long submitTime;

    public InteractionType getInteractionType() {
        return interactionType;
    }

    public void setInteractionType(InteractionType interactionType) {
        this.interactionType = interactionType;
    }

    public boolean isIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Integer getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Integer timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getSlideId() {
        return slideId;
    }

    public void setSlideId(String slideId) {
        this.slideId = slideId;
    }

    public Long getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Long submitTime) {
        this.submitTime = submitTime;
    }

    @Override
    public String toString() {
        return "InteractionData{" + "interactionType=" + interactionType + ", isCorrect=" + isCorrect + ", selected=" + selected + ", correctAnswer=" + correctAnswer + ", timeTaken=" + timeTaken + ", slideId=" + slideId + ", submitTime=" + submitTime + '}';
    }

}
