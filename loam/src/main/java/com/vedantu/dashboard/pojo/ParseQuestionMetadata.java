package com.vedantu.dashboard.pojo;

import com.vedantu.dashboard.constants.QuestionSetFileConstants;
import com.vedantu.dashboard.enums.CognitiveLevel;
import com.vedantu.dashboard.enums.Difficulty;
import com.vedantu.dashboard.enums.Indexable;
import com.vedantu.dashboard.enums.MetadataParts;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ParseQuestionMetadata {

    private static final String SEPERATOR = ",";
    public String title;
    private MetadataParts state;
    public QuestionType type;
    public Difficulty difficulty;

    public Indexable indexable;

    public Integer difficultyValue;
    public CognitiveLevel cognitiveLevel;
    public List<String> source;

    public String subject;
    public Set<String> topics = new HashSet<>();
    public Set<String> subTopics = new HashSet<>();
    public Set<String> tags = new HashSet<>();
    public Set<String> grades = new HashSet<>();
    public Set<String> targets = new HashSet<>();
    public Set<String> targetgrade = new HashSet<>();
    public Marks marks = new Marks();

    //book related
    private String book;
    private String edition;
    private String chapter;
    private String chapterNo;
    private String exercise;
    private Set<String> pageNos;
    private int slNoInBook;
    private String questionNo;

    public ParseQuestionMetadata(String title, String subject, List<String> topics, List<String> subTopics, List<String> tags, List<String> grades, List<String> targetgrade, List<String> targets, String book, String edition, String chapter, String chapterNo, String exercise, List<String> pageNos) {
        this.title = title;
        this.subject = subject;
        if(ArrayUtils.isNotEmpty(topics)){
            this.topics = new HashSet(topics);
        }
        if(ArrayUtils.isNotEmpty(subTopics)){
            this.subTopics = new HashSet(subTopics);
        }
        if(ArrayUtils.isNotEmpty(tags)){
            this.tags = new HashSet(tags);
        }
        if(ArrayUtils.isNotEmpty(grades)){
            this.grades = new HashSet(grades);
        }
        if(ArrayUtils.isNotEmpty(targetgrade)){
            this.targetgrade = new HashSet(targetgrade);
        }
        if(ArrayUtils.isNotEmpty(targets)){
            this.targets = new HashSet(targets);
        }
        this.book = book;
        this.edition = edition;
        this.chapter = chapter;
        this.chapterNo = chapterNo;
        this.exercise = exercise;
        if(ArrayUtils.isNotEmpty(pageNos)){
            this.targets = new HashSet(pageNos);
        }
    }

    public ParseQuestionMetadata() {
    }

    
    
    public void overrideMetadata(ParseQuestionMetadata metadata) {
        if(metadata == null){
            return;
        }
        if(StringUtils.isNotEmpty(metadata.getBook())){
            this.book = metadata.getBook();
        }
        if(StringUtils.isNotEmpty(metadata.getEdition())){
            this.edition = metadata.getEdition();
        }
        if(StringUtils.isNotEmpty(metadata.getChapter())){
            this.chapter = metadata.getChapter();
        }
        if(StringUtils.isNotEmpty(metadata.getChapterNo())){
            this.chapterNo = metadata.getChapterNo();
        }
        if(StringUtils.isNotEmpty(metadata.getExercise())){
            this.exercise = metadata.getExercise();
        }
        if(StringUtils.isNotEmpty(metadata.getTitle())){
            this.title = metadata.getTitle();
        }
        if(StringUtils.isNotEmpty(metadata.getSubject())){
            this.subject = metadata.getSubject();
        }
        if(ArrayUtils.isNotEmpty(metadata.getPageNos())){
            this.pageNos = metadata.getPageNos();
        }
        if(ArrayUtils.isNotEmpty(metadata.getTopics())){
            this.topics = metadata.getTopics();
        }
        if(ArrayUtils.isNotEmpty(metadata.getSubTopics())){
            this.subTopics = metadata.getSubTopics();
        }
        if(ArrayUtils.isNotEmpty(metadata.getTags())){
            this.tags = metadata.getTags();
        }
        if(ArrayUtils.isNotEmpty(metadata.getGrades())){
            this.grades = metadata.getGrades();
        }
        if(ArrayUtils.isNotEmpty(metadata.getTargetgrade())) {
            this.targetgrade = metadata.getTargetgrade();
        }
        if(ArrayUtils.isNotEmpty(metadata.getSource())) {
            this.source = metadata.getSource();
        }
        if(metadata.getCognitiveLevel() != null) {
            this.cognitiveLevel = metadata.getCognitiveLevel();
        }
        if(metadata.getDifficultyValue() != null) {
            this.difficultyValue = metadata.getDifficultyValue();
        }
        if(ArrayUtils.isNotEmpty(metadata.getTargets())){
            this.targets = metadata.getTargets();
        }
    }
    public MetadataParts accumulateMetadataInfo(String runText, boolean override) throws Exception {

        if (com.vedantu.util.StringUtils.isEmpty(runText)) {
            return null;
        }
        String checkStart = runText.replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").trim().toLowerCase();

        if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_TITLE)) {
            state = MetadataParts.TITLE;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_TYPE)) {
            state = MetadataParts.TYPE;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_DIFFICULTY)) {
            state = MetadataParts.DIFFICULTY;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_INDEXABLE)) {
            state = MetadataParts.INDEXABLE;
        }
        else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_SUBJECT)) {
            state = MetadataParts.SUBJECT;
        }
        else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_TOPICS)) {
            state = MetadataParts.TOPICS;
        }
//        else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_SUBTOPICS)) {
//            state = MetadataParts.SUBTOPICS;
//        }
        else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_TAGS)) {
            state = MetadataParts.TAGS;
        }
//        else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_GRADES)) {
//            state = MetadataParts.GRADES;
//        }
        else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_TARGET_GRADE)) {
            state = MetadataParts.TARGETGRADE;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_SOURCE)) {
            state = MetadataParts.SOURCE;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_DIFFICULTY_VALUE)) {
            state = MetadataParts.DIFFICULTY_VALUE;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_COGNITIVE_LEVEL)) {
            state = MetadataParts.COGNITIVE_LEVEL;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_TARGETS)) {
            state = MetadataParts.TARGETS;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_MARKS)) {
            state = MetadataParts.MARKS;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_NEGATIVE_MARKS)) {
            state = MetadataParts.NEGATIVE_MARKS;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_BOOK)) {
            state = MetadataParts.BOOK;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_EDITION)) {
            state = MetadataParts.EDITION;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_CHAPTER)) {
            state = MetadataParts.CHAPTER;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_CHAPTER_NO)) {
            state = MetadataParts.CHAPTER_NO;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_EXERCISE)) {
            state = MetadataParts.EXERCISE;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_PAGE_NOS)) {
            state = MetadataParts.PAGE_NOS;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_SLNOIN_BOOK)) {
            state = MetadataParts.SLNOIN_BOOK;
        } else if (checkStart.startsWith(QuestionSetFileConstants.PREFIX_QUESTION_NO_IN_BOOK)) {
            state = MetadataParts.QUESTION_NO_IN_BOOK;
        } else {
            state = null;
        }

        if (state != null) {
            state.accumulate(this, runText.trim(), override);
        }
        return state;
    }

    public void addTags(String tagsText) {

        if (this.tags == null) {
            this.tags = new HashSet<>();
        }
        this.tags.addAll(Arrays.asList(StringUtils.split(tagsText, SEPERATOR)));
    }

    public void addDifficulty(String levelText) {

        String[] levelArray = StringUtils.split(levelText, SEPERATOR);
        if (levelArray != null) {

            Difficulty _difficulty = Difficulty.valueOfKey(levelArray[0]);
            this.difficulty = _difficulty;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public MetadataParts getState() {
        return state;
    }

    public void setState(MetadataParts state) {
        this.state = state;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public Set<String> getPageNos() {
        return pageNos;
    }

    public void setPageNos(Set<String> pageNos) {
        this.pageNos = pageNos;
    }

    public Set<String> getSubTopics() {
        return subTopics;
    }

    public void setSubTopics(Set<String> subTopics) {
        this.subTopics = subTopics;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    public Set<String> getTargetgrade() {
        return targetgrade;
    }

    public void setTargetgrade(Set<String> targetgrade) {
        this.targetgrade = targetgrade;
    }

    public Set<String> getTargets() {
        return targets;
    }

    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }

    public String getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(String chapterNo) {
        this.chapterNo = chapterNo;
    }

    public int getSlNoInBook() {
        return slNoInBook;
    }

    public void setSlNoInBook(int slNoInBook) {
        this.slNoInBook = slNoInBook;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public Integer getDifficultyValue() {
        return difficultyValue;
    }

    public void setDifficultyValue(Integer difficultyValue) {
        this.difficultyValue = difficultyValue;
    }

    public CognitiveLevel getCognitiveLevel() {
        return cognitiveLevel;
    }

    public void setCognitiveLevel(CognitiveLevel cognitiveLevel) {
        this.cognitiveLevel = cognitiveLevel;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public Indexable getIndexable() {
        return indexable;
    }

    public void setIndexable(Indexable indexable) {
        this.indexable = indexable;
    }

    @Override
    public String toString() {
        return "ParseQuestionMetadata{" + "title=" + title + ", state=" + state + ", type=" + type + ", difficulty=" + difficulty + ", subject=" + subject + ", topics=" + topics + ", subTopics=" + subTopics + ", tags=" + tags + ", grades=" + grades + ", targetgrade=" + targetgrade + ", difficultyValue=" + difficultyValue + ", source=" + source + ", cognitiveLevel=" + cognitiveLevel + ", targets=" + targets + ", marks=" + marks + ", book=" + book + ", edition=" + edition + ", chapter=" + chapter + ", chapterNo=" + chapterNo + ", exercise=" + exercise + ", pageNos=" + pageNos + ", slNoInBook=" + slNoInBook + ", questionNo=" + questionNo + '}';
    }
}
