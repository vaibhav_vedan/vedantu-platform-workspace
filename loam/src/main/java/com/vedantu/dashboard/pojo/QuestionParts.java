package com.vedantu.dashboard.pojo;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.dashboard.constants.QuestionSetFileConstants;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.dashboard.utils.CMDSCommonUtils;
import com.vedantu.dashboard.pojo.challenges.HintFormat;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public enum QuestionParts {

    QUESTION {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            if (uuids.size() > 0) {
                q.formattedQuestion.getUuidImages().addAll(uuids);
            }
            v = QuestionStatesFiniteAutomata.getHTMLOutputFromLatex(v, q, "text", inHTML, imgInBase64);
            if (inHTML) {
            }
            String _new = isStart
                    && CMDSCommonUtils.doesStartsWith(v.trim().replaceAll(EntireQuestion.REGEX_HTML_STRIP, ""), QuestionSetFileConstants.PREFIX_QUESTION)
                    ? v.substring(v.indexOf(":") + 1)
                    : StringUtils.isEmpty(q.formattedQuestion.getNewText().trim()) ? v.replaceFirst("<br>", "") : v;
            q.formattedQuestion.setNewText(q.formattedQuestion.getNewText() + _new);
        }

        @Override
        public void addParentUlTags(EntireQuestion question, Map<String, Integer> listCounterMap) {
            String text = question.formattedQuestion.getNewText();
            question.formattedQuestion.setNewText(CMDSCommonUtils._addParentUlTags(text, listCounterMap));
        }

    },
    SOLUTION {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            if (q.formattedSolutions == null) {
                q.formattedSolutions = new ArrayList<>();
            }
            boolean newSolution = isStart
                    && CMDSCommonUtils.doesStartsWith(v.trim().replaceAll(EntireQuestion.REGEX_HTML_STRIP, ""), QuestionSetFileConstants.PREFIX_SOLUTION);
            SolutionFormat sf = q.formattedSolutions.isEmpty() || newSolution ? new SolutionFormat()
                    : q.formattedSolutions.get(q.formattedSolutions.size() - 1);
            if (uuids.size() > 0) {
                sf.getUuidImages().addAll(uuids);
            }
            v = QuestionStatesFiniteAutomata.getHTMLOutputFromLatex(v, q, "text", inHTML, imgInBase64);
//            if (inHTML) {
//                if (sf.getStartIndexOfLatexOrImage() == null || sf.getStartIndexOfLatexOrImage().isEmpty()) {
//                    sf.setStartIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getStartingIndexesOfRequiredSubstring(v));
//                    sf.setEndIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getEndIndexesOfRequiredSubstring(v));
//                }
//                if (isStart) {
//                    sf.setStartIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getStartingIndexesOfRequiredSubstring(v));
//                    sf.setEndIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getEndIndexesOfRequiredSubstring(v));
//                }
//            }
            String _new = newSolution ? v.substring(v.indexOf(":") + 1)
                    : StringUtils.isEmpty(sf.getNewText().trim()) ? v.replaceFirst("<br>", "") : v;
            System.out.println("adding new text to solution " + _new);
            sf.setNewText(sf.getNewText() + _new);
            if (newSolution) {
                q.formattedSolutions.add(sf);
            }
        }

        @Override
        public void addParentUlTags(EntireQuestion question, Map<String, Integer> listCounterMap) {
            if (ArrayUtils.isNotEmpty(question.formattedSolutions)) {
                for (SolutionFormat solution : question.formattedSolutions) {
                    solution.setNewText(CMDSCommonUtils._addParentUlTags(solution.getNewText(), listCounterMap));
                }
            }
        }
    },
    OPTIONS {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            if (uuids.size() > 0) {
                q.formattedOptions.uuidImages.addAll(uuids);
            }
            // if (!v.trim().toLowerCase().startsWith("options:")) {
            if (v.trim().length() <= 0) {
                return;
            }
            v = QuestionStatesFiniteAutomata.getHTMLOutputFromLatex(v, q, "options", inHTML, imgInBase64);
//            if (inHTML) {
//                if (q.formattedOptions.startIndexOfLatexOrImage == null
//                        || q.formattedOptions.startIndexOfLatexOrImage.isEmpty()) {
//
//                    q.formattedOptions.startIndexOfLatexOrImage = QuestionStatesFiniteAutomata
//                            .getStartingIndexesOfRequiredSubstring(v);
//                    q.formattedOptions.endIndexOfLatexOrImage = QuestionStatesFiniteAutomata
//                            .getEndIndexesOfRequiredSubstring(v);
//                }
//            }
            String sOptn = v.trim().replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").toLowerCase().startsWith("options:")
                    ? StringUtils.substringAfter(v, ":")
                    : v;
            if (StringUtils.isNotEmpty(sOptn)) {
                sOptn = sOptn.replace("\n", "");
                if (StringUtils.isNotEmpty(sOptn)) {
                    if (isStart) {
                        q.formattedOptions.newOptions.add(sOptn);
                    } else {
                        int i = q.formattedOptions.newOptions.size() - 1;
                        if (i >= 0) {
                            String o = q.formattedOptions.newOptions.get(i) + sOptn;
                            q.formattedOptions.newOptions.set(i, o);
                        } else if (i == -1 && !isStart) {
                            q.formattedOptions.newOptions.add(sOptn);
                        }
                    }
                }
            }
        }
        // }
    },
    ANSWER {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            v = v.trim().toLowerCase().replaceAll(EntireQuestion.REGEX_HTML_STRIP, "");
            if (v.startsWith("answer:")) {
                v = v.substring(v.indexOf(":") + 1).trim();
            }
            if (v.length() <= 0) {
                return;
            }

            if ((q.metadata.type != null
                    && q.metadata.type.toString().toLowerCase().equals("grid"))
                    || (!q.cola.isEmpty() && !q.colb.isEmpty())) {
                String s = v.trim();
                if (s.length() <= 0) {
                    return;
                }
                v = v.substring(v.indexOf(":") + 1);
                if (v.length() <= 0) {
                    return;
                }
                String[] ans = v.trim().split("\\s");
                String key = ans[0];
                Set<String> corrAns = new HashSet<>();
                for (int i = 1; i < ans.length; i++) {
                    corrAns.add(ans[i].replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").trim());
                }
                q.gridAnswer.put(key.replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").trim(), corrAns);
            } else {
                v = QuestionStatesFiniteAutomata.getHTMLOutputFromLatex(v, q, "options", inHTML, imgInBase64);
                QuestionType qType = q.metadata.type;
                if (qType == QuestionType.MCQ) {
                    q.answer = q.answer + v + ",";
                } else {
                    q.answer = q.answer + v;
                }
            }
        }
    },
    COLUMNA {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            if (v.replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").toLowerCase().startsWith("columna:")) {
                v = v.substring(v.indexOf(":") + 1);
            }
            if (v.length() <= 0) {
                return;
            }
            RichTextFormat richTextFormat = new RichTextFormat();
            if (ArrayUtils.isNotEmpty(uuids)) {
                richTextFormat.setUuidImages(new HashSet<>(uuids));
            }
            //TODO latex eqn indexes
            richTextFormat.setNewText(v);
            q.cola.add(richTextFormat);
        }
    },
    COLUMNB {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            if (v.replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").toLowerCase().startsWith("columnb:")) {
                v = v.substring(v.indexOf(":") + 1);
            }
            if (v.length() <= 0) {
                return;
            }
            RichTextFormat richTextFormat = new RichTextFormat();
            if (ArrayUtils.isNotEmpty(uuids)) {
                richTextFormat.setUuidImages(new HashSet<>(uuids));
            }
            //TODO latex eqn indexes
            richTextFormat.setNewText(v);
            q.colb.add(richTextFormat);
        }
    },
    HINT {
        @Override
        public void accumulate(EntireQuestion q, boolean isStart, String v, List<CMDSImageDetails> uuids,
                               boolean inHTML, boolean imgInBase64) {
            if (q.formattedHints == null) {
                q.formattedHints = new ArrayList<>();
            }
            boolean newHint = isStart
                    && v.trim().replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").toLowerCase().startsWith("hints:");
            HintFormat hf = q.formattedHints.isEmpty() || newHint ? new HintFormat()
                    : q.formattedHints.get(q.formattedHints.size() - 1);
            if (uuids.size() > 0) {
                hf.getUuidImages().addAll(uuids);
            }
            v = QuestionStatesFiniteAutomata.getHTMLOutputFromLatex(v, q, "text", inHTML, imgInBase64);
//            if (inHTML) {
//                if (hf.getStartIndexOfLatexOrImage() == null || hf.getStartIndexOfLatexOrImage().isEmpty()) {
//                    hf.setStartIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getStartingIndexesOfRequiredSubstring(v));
//                    hf.setEndIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getEndIndexesOfRequiredSubstring(v));
//                }
//                if (isStart) {
//                    hf.setStartIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getStartingIndexesOfRequiredSubstring(v));
//                    hf.setEndIndexOfLatexOrImage(QuestionStatesFiniteAutomata.getEndIndexesOfRequiredSubstring(v));
//                }
//            }
            String _new = newHint ? v.substring(v.indexOf(":") + 1)
                    : StringUtils.isEmpty(hf.getNewText().trim()) ? v.replaceFirst("<br>", "") : v;
            hf.setNewText(hf.getNewText() + _new);
            if (newHint) {
                q.formattedHints.add(hf);
            }
        }
    };

    public void accumulate(EntireQuestion questionType, boolean isStart, String v, List<CMDSImageDetails> uuids,
                           boolean inHTML, boolean imgInBase64) {
    }

    public void addParentUlTags(EntireQuestion question, Map<String, Integer> listCounterMap) {

    }

}
