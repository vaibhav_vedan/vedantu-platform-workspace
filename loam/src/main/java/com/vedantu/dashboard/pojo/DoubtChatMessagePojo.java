/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo;

import com.vedantu.User.Role;
import com.vedantu.dashboard.entities.mongo.DoubtChatMessage;

/**
 *
 * @author parashar
 */
public class DoubtChatMessagePojo {

    private String fromId;
    private Role fromRole;
    private boolean read;
    private String text;
    private String picUrl;
    private Long creationTime;
    private Long readTime;
    private String messageId;
    private Long doubtId;

    public DoubtChatMessagePojo() {
    }

    public DoubtChatMessagePojo(DoubtChatMessage doubtChatMessage) {
        this.fromId = doubtChatMessage.getFromId();
        this.text = doubtChatMessage.getText();
        this.read = doubtChatMessage.isRead();
        this.picUrl = doubtChatMessage.getPicUrl();
        this.creationTime = doubtChatMessage.getCreationTime();
        this.readTime = doubtChatMessage.getCreationTime();
        this.readTime = doubtChatMessage.getReadTime();
        this.doubtId = doubtChatMessage.getDoubtId();
    }

    public DoubtChatMessagePojo(String fromId, Role fromRole, boolean read, String text, String picUrl, Long creationTime, Long readTime) {
        this.fromId = fromId;
        this.fromRole = fromRole;
        this.read = read;
        this.text = text;
        this.picUrl = picUrl;
        this.creationTime = creationTime;
        this.readTime = readTime;
    }

    /**
     * @return the fromId
     */
    public String getFromId() {
        return fromId;
    }

    /**
     * @param fromId the fromId to set
     */
    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    /**
     * @return the fromRole
     */
    public Role getFromRole() {
        return fromRole;
    }

    /**
     * @param fromRole the fromRole to set
     */
    public void setFromRole(Role fromRole) {
        this.fromRole = fromRole;
    }

    /**
     * @return the read
     */
    public boolean isRead() {
        return read;
    }

    /**
     * @param read the read to set
     */
    public void setRead(boolean read) {
        this.read = read;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the creationTime
     */
    public Long getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * @return the readTime
     */
    public Long getReadTime() {
        return readTime;
    }

    /**
     * @param readTime the readTime to set
     */
    public void setReadTime(Long readTime) {
        this.readTime = readTime;
    }

    /**
     * @return the messageId
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    @Override
    public String toString() {
        return "DoubtChatMessagePojo{" + "fromId=" + fromId + ", fromRole=" + fromRole + ", read=" + read + ", text=" + text + ", picUrl=" + picUrl + ", creationTime=" + creationTime + ", readTime=" + readTime + ", messageId=" + messageId + ", doubtId=" + doubtId + '}';
    }

}
