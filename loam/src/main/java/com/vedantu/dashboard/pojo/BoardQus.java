package com.vedantu.dashboard.pojo;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 20/03/17.
 */
public class BoardQus implements Serializable {

    public String brdId;
    public String name;
    public int qusCount;
    public List<String> qids;

    public BoardQus() {
        this(StringUtils.EMPTY, StringUtils.EMPTY, 0);
    }

    public BoardQus(String brdId, String name, int qusCount) {
        this.brdId = brdId;
        this.name = name;
        this.qusCount = qusCount;
        this.qids = new ArrayList<String>();
    }


    @Override
    public int hashCode() {
        return ((brdId == null) ? 0 : brdId.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BoardQus other = (BoardQus) obj;
        return StringUtils.equals(brdId, other.brdId);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BoardQus [brdId:").append(brdId).append(", name:")
                .append(name).append(", qusCount:").append(qusCount)
                .append(", qids:").append(qids).append("]");
        return builder.toString();
    }

}
