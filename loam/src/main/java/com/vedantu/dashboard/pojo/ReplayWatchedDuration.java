/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo;

/**
 *
 * @author jeet
 */
public class ReplayWatchedDuration {
    private long startTime;
    private int duration;

    public ReplayWatchedDuration() {
    }

    public ReplayWatchedDuration(long startTime, int duration) {
        this.startTime = startTime;
        this.duration = duration;
    }
    
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
}
