package com.vedantu.dashboard.pojo.subscription;

import java.util.List;

public class UserBatchInfo
{
    private String courseName;
    private String courseId;
    private String batchId;
    private List<BoardSubjectPair> boardPairs;

    public String getCourseName()
    {
        return courseName;
    }

    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public String getCourseId()
    {
        return courseId;
    }

    public void setCourseId(String courseId)
    {
        this.courseId = courseId;
    }

    public String getBatchId()
    {
        return batchId;
    }

    public void setBatchId(String batchId)
    {
        this.batchId = batchId;
    }

    public List<BoardSubjectPair> getBoardPairs()
    {
        return boardPairs;
    }

    public void setBoardPairs(List<BoardSubjectPair> boardPairs)
    {
        this.boardPairs = boardPairs;
    }
}
