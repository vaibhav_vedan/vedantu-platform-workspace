package com.vedantu.dashboard.pojo;

import com.vedantu.dashboard.enums.OrderType;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 20/03/17.
 */
public class TestMetadata {

    public String brdId; // board _id
    public String name; // borad name
    public String slug;
    public int qusCount;

    public List<TestDetails> details;
    public Float totalMarks;

    public List<BoardQus> children;

    public List<CMDSTestQuestion> questions;

    public OrderType qOrderType;

    public TestMetadata() {
        this.details=new ArrayList<>();
        this.children=new ArrayList<>();
        this.questions=new ArrayList<>();
    }

    public String getBrdId() {
        return brdId;
    }

    public void setBrdId(String brdId) {
        this.brdId = brdId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQusCount() {
        return qusCount;
    }

    public void setQusCount(int qusCount) {
        this.qusCount = qusCount;
    }

    public List<TestDetails> getDetails() {
        return details;
    }

    public void setDetails(List<TestDetails> details) {
        this.details = details;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public List<BoardQus> getChildren() {
        return children;
    }

    public void setChildren(List<BoardQus> children) {
        this.children = children;
    }

    public List<CMDSTestQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<CMDSTestQuestion> questions) {
        this.questions = questions;
    }

    public OrderType getqOrderType() {
        return qOrderType;
    }

    public void setqOrderType(OrderType qOrderType) {
        this.qOrderType = qOrderType;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public static String convertToSlug(String name) {
        String slug = null;
        if(StringUtils.isNotEmpty(name)) {
            slug = name.trim().toLowerCase().replace(' ', '_');
        }
        return slug;
    }

    @Override
    public String toString() {
        return "TestMetadata{" +
                "brdId='" + brdId + '\'' +
                ", name='" + name + '\'' +
                ", qusCount=" + qusCount +
                ", details=" + details +
                ", totalMarks=" + totalMarks +
                ", children=" + children +
                ", questions=" + questions +
                ", qOrderType=" + qOrderType +
                '}';
    }


}
