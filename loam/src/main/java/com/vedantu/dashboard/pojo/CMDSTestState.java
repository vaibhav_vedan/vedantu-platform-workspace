package com.vedantu.dashboard.pojo;

public enum CMDSTestState {
	DRAFT, PRIVATE, PUBLIC
}
