package com.vedantu.dashboard.pojo.helper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AMInClassDoubtChatFromUserInfo {
    private String name;
    private String profilePic;
    private String userId;
}
