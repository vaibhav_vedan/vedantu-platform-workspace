package com.vedantu.dashboard.pojo.SupermentorResponse;

import com.vedantu.User.StudentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentInfoPojo {
    private String board;
    private String grade;
    private List<String> examTargets = new ArrayList<>();
}
