package com.vedantu.dashboard.pojo;

import com.vedantu.dashboard.entities.CMDSTest;
import com.vedantu.dashboard.entities.CMDSTestAttempt;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;

import java.util.List;

/**
 * Created by somil on 22/03/17.
 */
public class TestContentInfoMetadata extends ContentInfoMetadata {
    String testId;
    Long duration;
    Float totalMarks;
    Integer noOfQuestions;
    
    private Integer rank;
    private Float percentage;
    private Float percentile;

    List<CMDSTestAttempt> testAttempts;
    public Long minStartTime;
    public Long maxStartTime;
    boolean hardStop=false;
    public boolean reattemptAllowed = false;
    public TestResultVisibility displayResultOnEnd = TestResultVisibility.VISIBLE;
    public String displayMessageOnEnd;
    private SignUpRestrictionLevel signupHook;

    // Only for subjective tests
    private Long expiryDate;// for public test
    private Long expiryDays;// for private test

    public TestContentInfoMetadata() {
        super();
    }


    public TestContentInfoMetadata(CMDSTest cmdsTest) {
        super();
        testId = cmdsTest.getId();
        minStartTime = cmdsTest.getMinStartTime();
        maxStartTime = cmdsTest.getMaxStartTime();
        duration = cmdsTest.getDuration();
        totalMarks = cmdsTest.getTotalMarks();
        noOfQuestions = cmdsTest.getQusCount();
    }

    public SignUpRestrictionLevel getSignupHook() {
        return signupHook;
    }

    public void setSignupHook(SignUpRestrictionLevel signupHook) {
        this.signupHook = signupHook;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public List<CMDSTestAttempt> getTestAttempts() {
        return testAttempts;
    }

    public void setTestAttempts(List<CMDSTestAttempt> testAttempts) {
        this.testAttempts = testAttempts;
    }

    public Long getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Long minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Long getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public boolean isReattemptAllowed() {
        return reattemptAllowed;
    }

    public void setReattemptAllowed(boolean reattemptAllowed) {
        this.reattemptAllowed = reattemptAllowed;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public Integer getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public TestResultVisibility getDisplayResultOnEnd() {
        return displayResultOnEnd;
    }

    public void setDisplayResultOnEnd(TestResultVisibility displayResultOnEnd) {
        this.displayResultOnEnd = displayResultOnEnd;
    }

    public String getDisplayMessageOnEnd() {
        return displayMessageOnEnd;
    }

    public void setDisplayMessageOnEnd(String displayMessageOnEnd) {
        this.displayMessageOnEnd = displayMessageOnEnd;
    }


	public Integer getRank() {
		return rank;
	}


	public void setRank(Integer rank) {
		this.rank = rank;
	}


	public Float getPercentage() {
		return percentage;
	}


	public void setPercentage(Float percentage) {
		this.percentage = percentage;
	}


	public Float getPercentile() {
		return percentile;
	}


	public void setPercentile(Float percentile) {
		this.percentile = percentile;
	}

    public boolean isHardStop() {
        return hardStop;
    }

    public void setHardStop(boolean hardStop) {
        this.hardStop = hardStop;
    }

    public Long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getExpiryDays() {
        return expiryDays;
    }

    public void setExpiryDays(Long expiryDays) {
        this.expiryDays = expiryDays;
    }
}
