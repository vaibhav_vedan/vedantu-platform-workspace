package com.vedantu.dashboard.pojo;

public class BaseTopicTreeBasic {
	
	private String name;
	private String id;
	
	public BaseTopicTreeBasic() {
		super();
	}
	public BaseTopicTreeBasic(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
