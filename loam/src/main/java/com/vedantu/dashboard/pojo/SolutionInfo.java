package com.vedantu.dashboard.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import org.springframework.data.annotation.Transient;

public abstract class SolutionInfo implements Serializable, ILatexProcessor {

    @Transient
    public static final String ANSWER = "answer";

    private static final long serialVersionUID = 1L;
    private OptionFormat optionBody;
    private List<SolutionFormat> solutions;

    public SolutionInfo() {

        this(new OptionFormat());

    }

    public SolutionInfo(OptionFormat optionBody, List<SolutionFormat> solutions) {
        this.optionBody = optionBody;
        this.solutions = solutions;
    }

    public OptionFormat getOptionBody() {
        return optionBody;
    }

    public void setOptionBody(OptionFormat optionBody) {
        this.optionBody = optionBody;
    }

    public List<SolutionFormat> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<SolutionFormat> solutions) {
        this.solutions = solutions;
    }

    public SolutionInfo(OptionFormat op) {

        this.optionBody = op;
        this.solutions = new ArrayList<>();
    }

    public abstract List<String> getAnswer();
    public abstract void setAnswer(List<String> answer);

    @Override
    public void addHook() {

        if (optionBody != null) {
            optionBody.addHook();
        }

        if (solutions != null && !solutions.isEmpty()) {
            for (SolutionFormat solutionBody : solutions) {
                solutionBody.addHook();
            }
        }
    }

    @Override
    public String toString() {
        return "SolutionInfo{" + "optionBody=" + optionBody + ", solutions=" + solutions + '}';
    }

}
