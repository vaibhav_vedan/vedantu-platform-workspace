package com.vedantu.dashboard.pojo;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.bson.types.ObjectId;

/**
 * Created by somil on 22/03/17.
 */
public class ContentInfoMetadata extends AbstractMongoStringIdEntity
{
    public ContentInfoMetadata()
    {
        setId(ObjectId.get().toString());
    }
}
