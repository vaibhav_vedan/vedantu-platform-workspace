/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo;

import com.vedantu.dashboard.entities.mongo.Doubt;
import com.vedantu.dashboard.enums.DoubtState;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

/**
 *
 * @author parashar
 */
public class DoubtPojo extends AbstractTargetTopicEntity {

    private Long doubtId;
    private String text;
    private String picUrl;
    private DoubtState doubtState;



    private String solverName;
    private boolean ongoingDoubt = false;

    public DoubtPojo() {
    }

    public DoubtPojo(Doubt doubt) {
        this.setMainTags(doubt.getMainTags());
        this.setTargetGrades(doubt.getTargetGrades());
        this.settGrades(doubt.gettGrades());
        this.settTopics(doubt.gettTopics());
        this.settTargets(doubt.gettTargets());
        this.settSubjects(doubt.gettSubjects());
        this.setDoubtId(doubt.getId());
        this.setDoubtState(doubt.getDoubtState());
        this.setText(doubt.getText());
        this.setPicUrl(doubt.getPicUrl());
        this.setLastUpdated(doubt.getLastUpdated());
        this.setCreationTime(doubt.getCreationTime());
        if(doubt.getDoubtStateChanges().size() > 2) {
            this.ongoingDoubt = true;
        }
    }

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the doubtState
     */
    public DoubtState getDoubtState() {
        return doubtState;
    }

    /**
     * @param doubtState the doubtState to set
     */
    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    public boolean isOngoingDoubt() {
        return ongoingDoubt;
    }

    public void setOngoingDoubt(boolean ongoingDoubt) {
        this.ongoingDoubt = ongoingDoubt;
    }


    public String getSolverName() {
        return solverName;
    }

    public void setSolverName(String solverName) {
        this.solverName = solverName;
    }
}
