package com.vedantu.dashboard.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StudentInfo {

    Map<String, StudentEngagementMetadata> studentEngagementMetadataMap;
}
