package com.vedantu.dashboard.pojo;

import com.vedantu.User.Role;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostClassDoubtChatMessagePojo extends AbstractMongoStringIdEntityBean{
    private String fromId;
    private Role fromRole;
    private boolean read;
    private String text;
    private String picUrl;
    private Long creationTime;
    private Long readTime;
    private String messageId;
    private Long doubtId;
    private String userName;
    private String answeredBy;
}
