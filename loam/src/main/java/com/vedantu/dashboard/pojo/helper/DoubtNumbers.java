package com.vedantu.dashboard.pojo.helper;

public class DoubtNumbers {

    private Integer numberAsked;
    private Integer numberResolved;

    public DoubtNumbers() {
    }

    public DoubtNumbers(Integer numberAsked, Integer numberResolved) {
        this.numberAsked = numberAsked;
        this.numberResolved = numberResolved;
    }

    
    public Integer getNumberAsked() {
        return numberAsked;
    }

    public void setNumberAsked(Integer numberAsked) {
        this.numberAsked = numberAsked;
    }

    public Integer getNumberResolved() {
        return numberResolved;
    }

    public void setNumberResolved(Integer numberResolved) {
        this.numberResolved = numberResolved;
    }

    @Override
    public String toString() {
        return "DoubtNumbers{" + "numberAsked=" + numberAsked + ", numberResolved=" + numberResolved + '}';
    }

}
