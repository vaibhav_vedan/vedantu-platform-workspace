/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dashboard.pojo.helper;

/**
 *
 * @author ajith
 */
public class OTMSessionConfig {

    private boolean chatEnabled = true;
    private String chatMode = "PUBLIC";
    private String handRaiseMode = "IMMEDIATE";

    public boolean isChatEnabled() {
        return chatEnabled;
    }

    public void setChatEnabled(boolean chatEnabled) {
        this.chatEnabled = chatEnabled;
    }

    public String getChatMode() {
        return chatMode;
    }

    public void setChatMode(String chatMode) {
        this.chatMode = chatMode;
    }

    public String getHandRaiseMode() {
        return handRaiseMode;
    }

    public void setHandRaiseMode(String handRaiseMode) {
        this.handRaiseMode = handRaiseMode;
    }

    @Override
    public String toString() {
        return "OTMSessionConfig{" + "chatEnabled=" + chatEnabled + ", chatMode=" + chatMode + ", handRaiseMode=" + handRaiseMode + '}';
    }

}
