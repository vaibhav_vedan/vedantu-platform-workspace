package com.vedantu.dashboard.pojo.subscription;

public class BoardSubjectPair
{
    private String subject;
    private Long boardId;

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public Long getBoardId()
    {
        return boardId;
    }

    public void setBoardId(Long boardId)
    {
        this.boardId = boardId;
    }
}
