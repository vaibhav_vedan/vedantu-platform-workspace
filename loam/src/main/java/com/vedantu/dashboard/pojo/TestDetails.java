package com.vedantu.dashboard.pojo;

import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.Marks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 20/03/17.
 */
public class TestDetails {

    private QuestionType type;
    private int qusCount;

    private Marks marks;

    private List<String> qids;

    public TestDetails() {
        this.qids=new ArrayList<>();
    }

    public TestDetails(QuestionType type,Marks marks) {
        this.type = type;
        this.marks=marks;
        this.qids=new ArrayList<>();
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public int getQusCount() {
        return qusCount;
    }

    public void setQusCount(int qusCount) {
        this.qusCount = qusCount;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public List<String> getQids() {
        return qids;
    }

    public void setQids(List<String> qids) {
        this.qids = qids;
    }

    public void addQid(String qid){
        if(this.qids==null){
            this.qids=new ArrayList<>();
        }
        this.qids.add(qid);
    }

    @Override
    public String toString() {
        return "TestDetails{" + "type=" + type + ", qusCount=" + qusCount + ", marksGiven=" + marks + ", qids=" + qids + '}';
    }

}
