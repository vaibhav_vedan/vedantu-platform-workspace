package com.vedantu.dashboard.pojo.SupermentorResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherSupermentorPojo {
    private String teacherEmail;
    private String teacherFullName;
    private String teacherProfilePicUrl;
}
