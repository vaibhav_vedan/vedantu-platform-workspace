package com.vedantu.dashboard.pojo;

public class AMTestStats {

    private Double marks = 0.0;
    private Double totalMarks = 0.0;
    private Long startTime = 0l;
    private String testName;

    public AMTestStats() {
        super();
    }

    public AMTestStats(Double marks, Double totalMarks) {
        this.marks = marks;
        this.totalMarks = totalMarks;
    }

    public AMTestStats(Double marks, Double totalMarks, Long startTime) {
        this.marks = marks;
        this.totalMarks = totalMarks;
        this.startTime = startTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

    public Double getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Double totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}

