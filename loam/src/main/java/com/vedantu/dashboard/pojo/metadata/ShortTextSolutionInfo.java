package com.vedantu.dashboard.pojo.metadata;

import com.vedantu.dashboard.pojo.OptionFormat;
import com.vedantu.dashboard.pojo.SolutionInfo;
import com.vedantu.lms.cmds.utils.LatexProcessor;
import com.vedantu.util.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author manishkumarsingh
 */
public class ShortTextSolutionInfo extends SolutionInfo {
    public String answer;
    
    public ShortTextSolutionInfo() {
	this(new OptionFormat(), new String());
    }
    
    public ShortTextSolutionInfo(OptionFormat op, String answer) {
        super(op);
        this.answer = answer;
    }
    
    @Override
    public void addHook() {
        super.addHook();
        if (StringUtils.isNotEmpty(answer)) {
            answer = LatexProcessor.addHookToLatex(answer);
        }
    }

    @Override
    public List<String> getAnswer() {
        return Arrays.asList(answer);
    }
        
    @Override
    public void setAnswer(List<String> answer) {
        if(ArrayUtils.isNotEmpty(answer)){
            this.answer=answer.get(0);
        }else{
            this.answer=null;
        }
    }
}
