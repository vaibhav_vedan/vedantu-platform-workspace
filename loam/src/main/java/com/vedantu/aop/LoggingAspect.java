/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.aop;

import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.aop.AbstractAOPLayer;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.security.JwtAuthenticatedProfile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.UUID;

@Component
@Aspect
public class LoggingAspect extends AbstractAOPLayer {

    @Autowired
    private LogFactory logfactory;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(LoggingAspect.class);

    @Before("allCtrlMethods()")
    public void beforeExecutionForCtrlMethods(JoinPoint jp) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        if (servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest request = servletRequestAttributes.getRequest();
        //LogstashLayout.addToMDC(request);
        handleCorrelationId(request);
        handleCallingUserId(request);
    }

    //tracking dao methods calls
    @Pointcut("within(com.vedantu.dashboard.dao..*)||within(com.vedantu.moodle.dao..*)||within(com.vedantu.doubts.dao..*)")
    public void allDAOMethods() {
    }

    private void handleCallingUserId(HttpServletRequest request) {
        try {
            String callingUserId = request.getHeader("X-Caller-Id");
            if (StringUtils.isEmpty(callingUserId)) {
                HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
                if (sessionData != null && sessionData.getUserId() != null) {
                    callingUserId = sessionData.getUserId().toString();
                }
            }
            if (StringUtils.isNotEmpty(callingUserId)) {
                ThreadContext.put("callingUserId", callingUserId);
                //request.setAttribute("callingUserId", callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Error in handleCallingUserId", ex);
        }
    }

    private void handleCorrelationId(HttpServletRequest request) {
        try {
            String correlationId = request.getHeader("X-Correlation-Id");
            if (StringUtils.isEmpty(correlationId)) {
                correlationId = generateId();
            }
            ThreadContext.put("correlation-id", correlationId);
            //request.setAttribute("correlation-id", correlationId);
        } catch (Exception ex) {
            logger.error("Error in handleCorrelationId", ex);
        }
    }

    private String generateId() {
        return UUID.randomUUID().toString();
    }

}
