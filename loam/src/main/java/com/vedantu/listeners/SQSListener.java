/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.listeners;

import com.google.gson.Gson;
import com.vedantu.dashboard.managers.LOAMManager;
import com.vedantu.dashboard.pojo.PostChangeAnswerSQSPojo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;

/**
 *
 * @author ajith
 */

@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private LOAMManager lOAMManager;
    
    private static final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);
    private String env = ConfigUtils.INSTANCE.getStringValue("environment");

    
    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message "+ textMessage.getText());
            String queueName = ((Queue)textMessage.getJMSDestination()).getQueueName();

            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(queueName, sqsMessageType, textMessage.getText());
            message.acknowledge();

        } catch (Exception e) {
            logger.error("Error processing message ",e);
        }
    }

    
    public void handleMessage(String queueName, SQSMessageType sqsMessageType, String text) throws Exception{

        if(SQSQueue.POST_CHANGE_QUESTION_QUEUE.getQueueName(env).equals(queueName)){
            handlePostChangeQuestion(sqsMessageType, text);
        } else {
            throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, queueName + " can't process this queue here");
        }
    }

    private void handlePostChangeQuestion(SQSMessageType sqsMessageType, String text) {
        PostChangeAnswerSQSPojo postChangeAnswerSQSPojo = new Gson().fromJson(text, PostChangeAnswerSQSPojo.class);
        switch(sqsMessageType){

            case UPDATE_TEST_NODE_STATS_LOAM:
                lOAMManager.deleteTestNodeStats(postChangeAnswerSQSPojo.getTestId());
                break;
        }
    }
}
