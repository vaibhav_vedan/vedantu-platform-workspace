package com.vedantu.review.pojo;

import java.util.List;

public class Feedback {

	private String id;
	private String receiverId;
	private String senderId;
	private String sessionId;
	private int rating;
	private String sessionMessage;
	private String partnerMessage;
	private List<String> reason;
	// Sent to student only if the feedback given by teacher
	private String optionalMessage;
	private String sessionCoverage;
	private String nextSessionPlan;
	private String homeWork;
	private List<String> studentPerformance;

	private Long creationTime;
	private Long lastUpdated;

	public Feedback() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSessionMessage() {
		return sessionMessage;
	}

	public void setSessionMessage(String sessionMessage) {

		this.sessionMessage = sessionMessage;
	}

	public String getPartnerMessage() {
		return partnerMessage;
	}

	public void setPartnerMessage(String partnerMessage) {

		this.partnerMessage = partnerMessage;
	}

	public String getReceiverId() {

		return receiverId;
	}

	public void setReceiverId(String receiverId) {

		this.receiverId = receiverId;
	}

	public String getSessionId() {

		return sessionId;
	}

	public void setSessionId(String sessionId) {

		this.sessionId = sessionId;
	}

	public String getSenderId() {

		return senderId;
	}

	public void setSenderId(String senderId) {

		this.senderId = senderId;
	}

	public int getRating() {

		return rating;
	}

	public void setRating(int rating) {

		this.rating = rating;
	}

	public List<String> getReason() {
		return reason;
	}

	public void setReason(List<String> reason) {
		this.reason = reason;
	}

	public String getOptionalMessage() {
		return optionalMessage;
	}

	public void setOptionalMessage(String optionalMessage) {
		this.optionalMessage = optionalMessage;
	}

	public String getSessionCoverage() {
		return sessionCoverage;
	}

	public void setSessionCoverage(String sessionCoverage) {
		this.sessionCoverage = sessionCoverage;
	}

	public String getNextSessionPlan() {
		return nextSessionPlan;
	}

	public void setNextSessionPlan(String nextSessionPlan) {
		this.nextSessionPlan = nextSessionPlan;
	}

	public String getHomeWork() {
		return homeWork;
	}

	public void setHomeWork(String homeWork) {
		this.homeWork = homeWork;
	}

	public List<String> getStudentPerformance() {
		return studentPerformance;
	}

	public void setStudentPerformance(List<String> studentPerformance) {
		this.studentPerformance = studentPerformance;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return "Feedback [receiverId=" + receiverId + ", senderId=" + senderId
				+ ", sessionId=" + sessionId + ", rating=" + rating
				+ ", sessionMessage=" + sessionMessage + ", partnerMessage="
				+ partnerMessage + ", reason=" + reason + ", optionalMessage="
				+ optionalMessage + ", sessionCoverage=" + sessionCoverage
				+ ", nextSessionPlan=" + nextSessionPlan + ", homeWork="
				+ homeWork + ", studentPerformance=" + studentPerformance + "]";
	}

	public static class Constants {

		public static final String FEEDBACK_ID = "feedbackId";
		public static final String SESSION_ID = "sessionId";
		public static final String RECEIVER_ID = "receiverId";
		public static final String SENDER_ID = "senderId";
		public static final String RATING = "rating";
		public static final String END_TIME = "endTime";
		public static final String START_TIME = "startTime";
		public static final String REASON = "reason";
		public static final String OPTIONAL_MESSAGE = "optionalMessage";
	}
}
