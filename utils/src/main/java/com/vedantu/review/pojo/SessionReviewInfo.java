/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.review.pojo;

/**
 *
 * @author ajith
 */
public class SessionReviewInfo {

    private String grade;
    private String target;
    private String subject;

    public SessionReviewInfo() {
        super();
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "SessionReviewInfo{" + "grade=" + grade + ", target=" + target + ", subject=" + subject + '}';
    }
}
