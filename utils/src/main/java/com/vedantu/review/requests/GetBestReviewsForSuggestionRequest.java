package com.vedantu.review.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetBestReviewsForSuggestionRequest extends AbstractFrontEndReq {

	public Long userId;
	public Long lengthPreferred;
	public Integer noOfReviews;
	public Boolean isPositive;

	public GetBestReviewsForSuggestionRequest() {
		super();
	}

	public GetBestReviewsForSuggestionRequest(Long userId,
			Long lengthPreferred, Integer noOfReviews, Boolean isPositive) {
		super();
		this.userId = userId;
		this.lengthPreferred = lengthPreferred;
		this.noOfReviews = noOfReviews;
		this.isPositive = isPositive;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getLengthPreferred() {
		return lengthPreferred;
	}

	public void setLengthPreferred(Long lengthPreferred) {
		this.lengthPreferred = lengthPreferred;
	}

	public Integer getNoOfReviews() {
		return noOfReviews;
	}

	public void setNoOfReviews(Integer noOfReviews) {
		this.noOfReviews = noOfReviews;
	}

	public Boolean getIsPositive() {
		return isPositive;
	}

	public void setIsPositive(Boolean isPositive) {
		this.isPositive = isPositive;
	}
}
