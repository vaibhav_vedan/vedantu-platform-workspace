package com.vedantu.review.requests;

import java.util.List;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class EditReviewRequest extends AbstractFrontEndReq {

	private String id;
	private VisibilityState state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return String.format("{id=%s, state=%s}", id, state);
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (id == null || id.isEmpty()) {
			errors.add("ID");
		}
		if (state == null) {
			errors.add("STATE");
		}
		return errors;
	}

}
