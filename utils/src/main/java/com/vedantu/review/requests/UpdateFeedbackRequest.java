package com.vedantu.review.requests;

import java.util.List;

import com.vedantu.review.pojo.Feedback;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidReasonStringList;
import javax.validation.constraints.Size;

public class UpdateFeedbackRequest extends AbstractFrontEndReq {

    private String feedbackId;
    private Integer rating;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String partnerMessage;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String sessionMessage;

    @ValidReasonStringList(message = ReqLimitsErMsgs.REASON_STRING_LIST_MAX)
    private List<String> reason;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String optionalMessage;

    public String getFeedbackId() {

        return feedbackId;
    }

    public void setFeedbackId(String feedbackId) {

        this.feedbackId = feedbackId;
    }

    public Integer getRating() {

        return rating;
    }

    public void setRating(Integer rating) {

        this.rating = rating;
    }

    public String getPartnerMessage() {

        return partnerMessage;
    }

    public void setPartnerMessage(String partnerMessage) {

        this.partnerMessage = partnerMessage;
    }

    public String getSessionMessage() {

        return sessionMessage;
    }

    public void setSessionMessage(String sessionMessage) {

        this.sessionMessage = sessionMessage;
    }

    public List<String> getReason() {
        return reason;
    }

    public void setReason(List<String> reason) {
        this.reason = reason;
    }

    public String getOptionalMessage() {
        return optionalMessage;
    }

    public void setOptionalMessage(String optionalMessage) {
        this.optionalMessage = optionalMessage;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == feedbackId) {
            errors.add(Feedback.Constants.FEEDBACK_ID);
        }

        return errors;
    }
}
