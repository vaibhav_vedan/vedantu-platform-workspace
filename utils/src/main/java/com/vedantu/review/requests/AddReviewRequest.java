package com.vedantu.review.requests;

import java.util.List;

import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidReasonStringList;
import javax.validation.constraints.Size;

public class AddReviewRequest extends AbstractFrontEndReq {

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String review;
    private Float rating;
    private Long entityId;
    private EntityType entityType;
    private ContextType contextType;
    private Long contextId;
    @ValidReasonStringList(message = ReqLimitsErMsgs.REASON_STRING_LIST_MAX)
    private List<String> reason;
    private boolean poorFeedback;

    public AddReviewRequest() {
        super();
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public ContextType getContextType() {
        return contextType;
    }

    public void setContextType(ContextType contextType) {
        this.contextType = contextType;
    }

    public Long getContextId() {
        return contextId;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public List<String> getReason() {
        return reason;
    }

    public void setReason(List<String> reason) {
        this.reason = reason;
    }

    public boolean isPoorFeedback() {
        return poorFeedback;
    }

    public void setPoorFeedback(boolean poorFeedback) {
        this.poorFeedback = poorFeedback;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (rating == null || rating < 1) {
            errors.add("rating");
        }

        if (entityId == null || entityId < 1) {
            errors.add("entityId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "AddReviewReq{" + "review=" + review + ", rating=" + rating + ", entityId=" + entityId + ", entityType=" + entityType + ", contextType=" + contextType + ", contextId=" + contextId + ", reason=" + reason + ", poorFeedback=" + poorFeedback + '}';
    }

}
