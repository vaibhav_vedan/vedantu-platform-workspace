package com.vedantu.review.requests;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetReviewsRequest extends AbstractFrontEndListReq {

	private Long userId;
	private String entityId;
	private EntityType entityType;
	private String contextId;
	private ContextType contextType;
	private VisibilityState state;
	private Long excludeUserId;
	private Boolean skipReviewCheck;
	private Boolean sortByPriority;
	
	public GetReviewsRequest() {
		super();
	}

	public GetReviewsRequest(int start, int limit) {
		super(start, limit);
		// TODO Auto-generated constructor stub
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public ContextType getContextType() {
		return contextType;
	}

	public void setContextType(ContextType contextType) {
		this.contextType = contextType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getExcludeUserId() {
		return excludeUserId;
	}

	public void setExcludeUserId(Long excludeUserId) {
		this.excludeUserId = excludeUserId;
	}

	public Boolean getSkipReviewCheck() {
		return skipReviewCheck;
	}

	public void setSkipReviewCheck(Boolean skipReviewCheck) {
		this.skipReviewCheck = skipReviewCheck;
	}

	public Boolean getSortByPriority() {
		return sortByPriority;
	}

	public void setSortByPriority(Boolean sortByPriority) {
		this.sortByPriority = sortByPriority;
	}
}
