package com.vedantu.review.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetFeedbackRequest extends AbstractFrontEndReq {

    private String receiverId;
    private String senderId;
    private String sessionId;
    private Long startTime;
    private Long endTime;
    private Long from;
    private Long till;
    private Boolean orderDesc = Boolean.FALSE;

    public String getReceiverId() {

        return receiverId;
    }

    public void setReceiverId(String receiverId) {

        this.receiverId = receiverId;
    }

    public Long getStartTime() {

        return startTime;
    }

    public void setStartTime(Long startTime) {

        this.startTime = startTime;
    }

    public Long getEndTime() {

        return endTime;
    }

    public void setEndTime(Long endTime) {

        this.endTime = endTime;
    }

    public Long getFrom() {

        return from;
    }

    public void setFrom(Long from) {

        this.from = from;
    }

    public Long getTill() {

        return till;
    }

    public void setTill(Long till) {

        this.till = till;
    }

    public Boolean getOrderDesc() {

        return orderDesc;
    }

    public void setOrderDesc(Boolean orderDesc) {

        this.orderDesc = orderDesc;
    }

    public String getSenderId() {

        return senderId;
    }

    public void setSenderId(String senderId) {

        this.senderId = senderId;
    }

    public String getSessionId() {

        return sessionId;
    }

    public void setSessionId(String sessionId) {

        this.sessionId = sessionId;
    }
}
