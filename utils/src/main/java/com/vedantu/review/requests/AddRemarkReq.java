package com.vedantu.review.requests;


import java.util.List;

import com.vedantu.review.pojo.RemarkContext;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddRemarkReq extends AbstractFrontEndReq{
	
	private Long toUser;
	private RemarkContext contextType;
	private String contextId;
	private String remark;
	private String nextSessionRemark;
	private Long boardId;
	public Long getToUser() {
		return toUser;
	}
	public void setToUser(Long toUser) {
		this.toUser = toUser;
	}
	public RemarkContext getContextType() {
		return contextType;
	}
	public void setContextType(RemarkContext contextType) {
		this.contextType = contextType;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNextSessionRemark() {
		return nextSessionRemark;
	}
	public void setNextSessionRemark(String nextSessionRemark) {
		this.nextSessionRemark = nextSessionRemark;
	}
	
	public Long getBoardId() {
		return boardId;
	}
	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(contextType.toString())) {
            errors.add("contextType");
        }
        if (StringUtils.isEmpty(contextId)) {
            errors.add("contextId");
        }
        if (StringUtils.isEmpty(remark)) {
            errors.add("remark");
        }
        return errors;
	}

}
