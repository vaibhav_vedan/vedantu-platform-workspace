package com.vedantu.review.requests;

import java.util.Calendar;
import java.util.List;

import com.vedantu.review.pojo.Feedback;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidReasonStringList;
import javax.validation.constraints.Size;

public class GiveFeedbackRequest extends AbstractFrontEndReq {

    private String senderId;
    private String receiverId;
    private String sessionId;
    private Integer rating;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String sessionMessage;
    
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String partnerMessage;
    
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String optionalMessage;
    
    @ValidReasonStringList(message = ReqLimitsErMsgs.REASON_STRING_LIST_MAX)
    private List<String> reason;
    
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String sessionCoverage;
    
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String nextSessionPlan;
    
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String homeWork;
    
    @ValidReasonStringList(message = ReqLimitsErMsgs.REASON_STRING_LIST_MAX)
    private List<String> studentPerformance;
    
    private Boolean noEmailAlert;

    public String getSenderId() {

        return senderId;
    }

    public void setSenderId(String senderId) {

        this.senderId = senderId;
    }

    public String getReceiverId() {

        return receiverId;
    }

    public void setReceiverId(String receiverId) {

        this.receiverId = receiverId;
    }

    public String getSessionId() {

        return sessionId;
    }

    public void setSessionId(String sessionId) {

        this.sessionId = sessionId;
    }

    public Integer getRating() {

        return rating;
    }

    public void setRating(Integer rating) {

        this.rating = rating;
    }

    public String getSessionMessage() {

        return sessionMessage;
    }

    public void setSessionMessage(String sessionMessage) {

        this.sessionMessage = sessionMessage;
    }

    public String getPartnerMessage() {

        return partnerMessage;
    }

    public void setPartnerMessage(String partnerMessage) {

        this.partnerMessage = partnerMessage;
    }

    public String getOptionalMessage() {
        return optionalMessage;
    }

    public void setOptionalMessage(String optionalMessage) {
        this.optionalMessage = optionalMessage;
    }

    public List<String> getReason() {
        return reason;
    }

    public void setReason(List<String> reason) {
        this.reason = reason;
    }

    public String getSessionCoverage() {
        return sessionCoverage;
    }

    public void setSessionCoverage(String sessionCoverage) {
        this.sessionCoverage = sessionCoverage;
    }

    public String getNextSessionPlan() {
        return nextSessionPlan;
    }

    public void setNextSessionPlan(String nextSessionPlan) {
        this.nextSessionPlan = nextSessionPlan;
    }

    public String getHomeWork() {
        return homeWork;
    }

    public void setHomeWork(String homeWork) {
        this.homeWork = homeWork;
    }

    public List<String> getStudentPerformance() {
        return studentPerformance;
    }

    public void setStudentPerformance(List<String> studentPerformance) {
        this.studentPerformance = studentPerformance;
    }

    public Boolean getNoEmailAlert() {
        return noEmailAlert;
    }

    public void setNoEmailAlert(Boolean noEmailAlert) {
        this.noEmailAlert = noEmailAlert;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == senderId) {
            errors.add(Feedback.Constants.SENDER_ID);
        }

        if (null == receiverId) {
            errors.add(Feedback.Constants.RECEIVER_ID);
        }

        if (null == sessionId) {
            errors.add(Feedback.Constants.SESSION_ID);
        }

        if (null == rating) {
            errors.add(Feedback.Constants.RATING);
        }

        return errors;
    }

    public Feedback toFeedback() {

        Feedback feedback = new Feedback();
        String _sessionMessage = getSessionMessage();
        String _partnerMessage = getPartnerMessage();
        if (_sessionMessage == null) {
            _sessionMessage = "";
        }
        if (_partnerMessage == null) {
            _partnerMessage = "";
        }
        feedback.setSessionMessage(_sessionMessage);
        feedback.setPartnerMessage(_partnerMessage);
        feedback.setRating(rating);
        feedback.setReceiverId(getReceiverId());
        feedback.setSessionId(getSessionId());
        feedback.setSenderId(getSenderId());
        feedback.setCreationTime(Calendar.getInstance().getTimeInMillis());
        feedback.setReason(reason);
        feedback.setOptionalMessage(optionalMessage);
        feedback.setSessionCoverage(sessionCoverage);
        feedback.setNextSessionPlan(nextSessionPlan);
        feedback.setNextSessionPlan(nextSessionPlan);
        feedback.setStudentPerformance(studentPerformance);
        feedback.setHomeWork(homeWork);
        return feedback;
    }
}
