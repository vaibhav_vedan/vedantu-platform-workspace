package com.vedantu.review.requests;

import java.util.List;

import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetReviewRequest extends AbstractFrontEndReq {

    private String id;
    private Long userId;
    private String entityId;
    private EntityType entityType;
    private String contextId;
    private ContextType contextType;

    public GetReviewRequest() {
        super();
    }

    public GetReviewRequest(String id, Long userId, String entityId,
            EntityType entityType, String contextId, ContextType contextType) {
        super();
        this.id = id;
        this.userId = userId;
        this.entityId = entityId;
        this.entityType = entityType;
        this.contextId = contextId;
        this.contextType = contextType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public ContextType getContextType() {
        return contextType;
    }

    public void setContextType(ContextType contextType) {
        this.contextType = contextType;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (id == null || id.isEmpty()) {
            if (entityId == null || entityId.isEmpty()) {
                errors.add("entityId");
            }
            if (contextId == null || contextId.isEmpty()) {
                errors.add("contextId");
            }
            if (entityType == null) {
                errors.add("entityType");
            }
            if (contextType == null) {
                errors.add("contextType");
            }
        }

        return errors;
    }

    @Override
    public String toString() {
        return super.toString() + ", GetReviewRequest{" + "id=" + id + ", userId=" + userId + ", entityId=" + entityId + ", entityType=" + entityType + ", contextId=" + contextId + ", contextType=" + contextType + '}';
    }
}
