package com.vedantu.review.requests;

import com.vedantu.util.StringUtils;
import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddPrioritiesRequest extends AbstractFrontEndReq {

	private List<AddPriorityReq> addPriorityReqs;

	public List<AddPriorityReq> getAddPriorityReqs() {
		return addPriorityReqs;
	}

	public void setAddPriorityReqs(List<AddPriorityReq> addPriorityReqs) {
		this.addPriorityReqs = addPriorityReqs;
	}

	@Override
	public String toString() {
		return super.toString() + " AddPrioritiesReq{" + "addPriorityReqs="
				+ addPriorityReqs + '}';
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (addPriorityReqs.isEmpty()) {
			errors.add("req empty");
		} else {
			for (AddPriorityReq addPriorityReq : addPriorityReqs) {
				if (StringUtils.isEmpty(addPriorityReq.reviewId)
						|| addPriorityReq.priority < 0) {
					errors.add("reviewId not valid or priority < 0");
				}
			}
		}
		return errors;
	}

	public class AddPriorityReq {

		private String reviewId;
		private Integer priority;

		public String getReviewId() {
			return reviewId;
		}

		public void setReviewId(String reviewId) {
			this.reviewId = reviewId;
		}

		public Integer getPriority() {
			return priority;
		}

		public void setPriority(Integer priority) {
			this.priority = priority;
		}

		public AddPriorityReq() {
			super();
		}

		@Override
		public String toString() {
			return "AddPriorityReq{" + "reviewId=" + reviewId + ", priority="
					+ priority + '}';
		}

	}
}
