package com.vedantu.review.response;

import java.util.List;

import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.review.pojo.Review;
import com.vedantu.review.pojo.SessionReviewInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.EntityType;
import com.vedantu.util.fos.response.AbstractRes;

public class ReviewInfo extends AbstractRes {

    private String id;
    private String review;
    private Integer rating;
    private String entityId;
    private EntityType entityType;
    private UserBasicInfo user;
    private Long lastUpdated;
    private VisibilityState state;
    private List<String> reason;
    private Integer priority;
    private SessionReviewInfo reviewExtraInfo;

    public ReviewInfo(Review review, User user) {
        super();
        fillDetails(review);
        this.user = new ReviewUserInfo(user, false);
    }

    private void fillDetails(Review review) {
        if (review.getId() != null) {
            this.id = review.getId();
        }
        this.review = review.getReview();
        this.rating = review.getRating();
        this.entityId = review.getEntityId();
        this.entityType = review.getEntityType();
        this.lastUpdated = review.getLastUpdated();
        this.state = review.getState();
        this.reason = review.getReason();
        this.priority = review.getPriority();
        this.reviewExtraInfo = review.getReviewExtraInfo();
    }

    public ReviewInfo() {

    }

    public ReviewInfo(Review review) {
        super();
        fillDetails(review);
    }

    public String getId() {
        return id;
    }

    public String getReview() {
        return review;
    }

    public Integer getRating() {
        return rating;
    }

    public String getEntityId() {
        return entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public VisibilityState getState() {
        return state;
    }

    public List<String> getReason() {
        return reason;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public SessionReviewInfo getReviewExtraInfo() {
        return reviewExtraInfo;
    }

    public void setReviewExtraInfo(SessionReviewInfo reviewExtraInfo) {
        this.reviewExtraInfo = reviewExtraInfo;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "ReviewInfo{" + "id=" + id + ", review=" + review + ", rating=" + rating + ", entityId=" + entityId + ", entityType=" + entityType + ", user=" + user + ", lastUpdated=" + lastUpdated + ", state=" + state + ", reason=" + reason + ", priority=" + priority + ", reviewExtraInfo=" + reviewExtraInfo + '}';
    }

    //there is no point in adding city field to UserBasicInfo
    private class ReviewUserInfo extends UserBasicInfo {

        private String city;

        public ReviewUserInfo(User user, boolean exposeEmail) {
            super(user, exposeEmail);
            if (user != null && user.getLocationInfo() != null) {
                this.city = user.getLocationInfo().getCity();
            }
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

    }
}
