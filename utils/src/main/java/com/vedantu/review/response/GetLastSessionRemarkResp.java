package com.vedantu.review.response;

public class GetLastSessionRemarkResp {

	private RemarkResp lastSessionRemark;
	private RemarkResp currentSessionRemark;

	public RemarkResp getLastSessionRemark() {
		return lastSessionRemark;
	}

	public void setLastSessionRemark(RemarkResp lastSessionRemark) {
		this.lastSessionRemark = lastSessionRemark;
	}

	public RemarkResp getCurrentSessionRemark() {
		return currentSessionRemark;
	}

	public void setCurrentSessionRemark(RemarkResp currentSessionRemark) {
		this.currentSessionRemark = currentSessionRemark;
	}
}
