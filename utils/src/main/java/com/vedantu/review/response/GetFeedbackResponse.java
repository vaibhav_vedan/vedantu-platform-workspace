package com.vedantu.review.response;

import java.util.List;

public class GetFeedbackResponse {

	private List<FeedbackInfo> data;

	public List<FeedbackInfo> getData() {
		return data;
	}

	public void setData(List<FeedbackInfo> data) {
		this.data = data;
	}
}
