package com.vedantu.review.response;

import com.vedantu.review.pojo.Feedback;
import com.vedantu.util.fos.response.AbstractRes;

public class UpdateFeedbackResponse extends AbstractRes {

	private String feedbackId;
	private Integer rating;
	private String sessionMessage;
	private String partnerMessage;

	public UpdateFeedbackResponse() {

	}

	public UpdateFeedbackResponse(Feedback feedback) {

		this.feedbackId = feedback.getId();
		this.rating = feedback.getRating();
		this.partnerMessage = feedback.getPartnerMessage();
		this.sessionMessage = feedback.getSessionMessage();
	}

	public String getFeedbackId() {

		return feedbackId;
	}

	public void setFeedbackId(String feedbackId) {

		this.feedbackId = feedbackId;
	}

	public Integer getRating() {

		return rating;
	}

	public void setRating(Integer rating) {

		this.rating = rating;
	}

	public String getSessionMessage() {

		return sessionMessage;
	}

	public void setSessionMessage(String sessionMessage) {

		this.sessionMessage = sessionMessage;
	}

	public String getPartnerMessage() {

		return partnerMessage;
	}

	public void setPartnerMessage(String partnerMessage) {

		this.partnerMessage = partnerMessage;
	}
}
