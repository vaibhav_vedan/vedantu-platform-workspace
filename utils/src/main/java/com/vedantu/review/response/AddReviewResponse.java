package com.vedantu.review.response;

import java.util.List;


public class AddReviewResponse{

	private String id;
	private Float avgRating;
	private Long totalRatingCount;
	private Integer rating;
	private String review;
	private List<String> reason;

	public AddReviewResponse(String id, Float avgRating, Long totalRatingCount,
			Integer rating, String review, List<String> reason) {
		super();
		this.id = id;
		this.avgRating = avgRating;
		this.rating = rating;
		this.review = review;
		this.totalRatingCount = totalRatingCount;
		this.reason = reason;
	}

	public String getId() {
		return id;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	public Long getTotalRatingCount() {
		return totalRatingCount;
	}

	public Integer getRating() {
		return rating;
	}

	public String getReview() {
		return review;
	}

	public List<String> getReason() {
		return reason;
	}

	@Override
	public String toString() {
		return "AddReviewRes [id=" + id + ", avgRating=" + avgRating
				+ ", totalRatingCount=" + totalRatingCount + ", rating="
				+ rating + ", review=" + review + ", reason=" + reason + "]";
	}
}
