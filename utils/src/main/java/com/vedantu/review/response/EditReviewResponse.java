package com.vedantu.review.response;

import com.vedantu.session.pojo.VisibilityState;

public class EditReviewResponse {

		private VisibilityState state;

		public EditReviewResponse(VisibilityState state) {
			super();
			this.state = state;
		}

		public VisibilityState getState() {
			return state;
		}

		public void setState(VisibilityState state) {
			this.state = state;
		}

		@Override
		public String toString() {
			return String.format("{state=%s}", state);
		}
}
