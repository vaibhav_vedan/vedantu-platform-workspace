package com.vedantu.review.response;

import com.vedantu.review.pojo.RemarkContext;

public class RemarkResp {

	private Long reviewer;
	private Long toUser;
	private RemarkContext contextType;
	private String contextId;
	private String remark;
	private String nextSessionRemark;
	private Long creationTime;
	private Long lastUpdated;
	
	public Long getReviewer() {
		return reviewer;
	}
	public void setReviewer(Long reviewer) {
		this.reviewer = reviewer;
	}
	public Long getToUser() {
		return toUser;
	}
	public void setToUser(Long toUser) {
		this.toUser = toUser;
	}
	public RemarkContext getContextType() {
		return contextType;
	}
	public void setContextType(RemarkContext contextType) {
		this.contextType = contextType;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNextSessionRemark() {
		return nextSessionRemark;
	}
	public void setNextSessionRemark(String nextSessionRemark) {
		this.nextSessionRemark = nextSessionRemark;
	}
	public Long getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}
	public Long getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Override
	public String toString() {
		return "RemarkResp [reviewer=" + reviewer + ", toUser=" + toUser + ", contextType=" + contextType
				+ ", contextId=" + contextId + ", remark=" + remark + ", nextSessionRemark=" + nextSessionRemark
				+ ", creationTime=" + creationTime + ", lastUpdated=" + lastUpdated + "]";
	}
}
