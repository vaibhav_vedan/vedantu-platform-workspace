package com.vedantu.review.response;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.review.pojo.Feedback;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.ListUtils;

public class FeedbackInfo {

    private String feedbackId;
    private SessionInfo sessionInfo;
    private UserBasicInfo receiver;
    private UserBasicInfo sender;
    private String sessionMessage;
    private String partnerMessage;
    private int rating;
    private List<String> reason;
    private String optionalMessage;
    private String sessionCoverage;
    private String nextSessionPlan;
    private String homeWork;
    private List<String> studentPerformance;

    public FeedbackInfo() {
    }

    public FeedbackInfo(Feedback feedback) {

        this.feedbackId = feedback.getId();
        this.rating = feedback.getRating();
        this.partnerMessage = feedback.getPartnerMessage();
        this.sessionMessage = feedback.getSessionMessage();
        this.reason = feedback.getReason();
        this.optionalMessage = feedback.getOptionalMessage();
        this.sessionCoverage = feedback.getSessionCoverage();
        this.homeWork = feedback.getHomeWork();
        this.studentPerformance = feedback.getStudentPerformance();
        this.nextSessionPlan = feedback.getNextSessionPlan();
    }

    public String getFeedbackId() {

        return feedbackId;
    }

    public void setFeedbackId(String feedbackId) {

        this.feedbackId = feedbackId;
    }

    public SessionInfo getSessionInfo() {

        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {

        this.sessionInfo = sessionInfo;
    }

    public UserBasicInfo getReceiver() {

        return receiver;
    }

    public void setReceiver(UserBasicInfo receiver) {

        this.receiver = receiver;
    }

    public UserBasicInfo getSender() {

        return sender;
    }

    public void setSender(UserBasicInfo sender) {

        this.sender = sender;
    }

    public String getSessionMessage() {

        return sessionMessage;
    }

    public void setSessionMessage(String sessionMessage) {

        this.sessionMessage = sessionMessage;
    }

    public String getPartnerMessage() {

        return partnerMessage;
    }

    public void setPartnerMessage(String partnerMessage) {

        this.partnerMessage = partnerMessage;
    }

    public int getRating() {

        return rating;
    }

    public void setRating(int rating) {

        this.rating = rating;
    }

    public List<String> getReason() {
        return reason;
    }

    public String getReasonString() {
        if (reason != null && !reason.isEmpty()) {
            return ListUtils.join(reason, ",");
        } else {
            return null;
        }
    }

    public void setReason(List<String> list) {
        this.reason = list;
    }

    public String getOptionalMessage() {
        return optionalMessage;
    }

    public void setOptionalMessage(String optionalMessage) {
        this.optionalMessage = optionalMessage;
    }

    public String getSessionCoverage() {
        return sessionCoverage;
    }

    public void setSessionCoverage(String sessionCoverage) {
        this.sessionCoverage = sessionCoverage;
    }

    public String getNextSessionPlan() {
        return nextSessionPlan;
    }

    public void setNextSessionPlan(String nextSessionPlan) {
        this.nextSessionPlan = nextSessionPlan;
    }

    public String getHomeWork() {
        return homeWork;
    }

    public void setHomeWork(String homeWork) {
        this.homeWork = homeWork;
    }

    public List<String> getStudentPerformance() {
        return studentPerformance;
    }

    public String getStudentPerformanceString() {
        if (studentPerformance != null && !studentPerformance.isEmpty()) {
            return ListUtils.join(studentPerformance, ",");
        } else {
            return null;
        }
    }

    public void setStudentPerformance(List<String> studentPerformance) {
        this.studentPerformance = studentPerformance;
    }

    @Override
    public String toString() {
        return "FeedbackInfo{" + "feedbackId=" + feedbackId + ", sessionInfo=" + sessionInfo + ", receiver=" + receiver + ", sender=" + sender + ", sessionMessage=" + sessionMessage + ", partnerMessage=" + partnerMessage + ", rating=" + rating + ", reason=" + reason + ", optionalMessage=" + optionalMessage + ", sessionCoverage=" + sessionCoverage + ", nextSessionPlan=" + nextSessionPlan + ", homeWork=" + homeWork + ", studentPerformance=" + studentPerformance + '}';
    }

}
