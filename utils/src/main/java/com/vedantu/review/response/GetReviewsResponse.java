package com.vedantu.review.response;

import com.vedantu.util.fos.response.AbstractListRes;

public class GetReviewsResponse extends AbstractListRes<ReviewInfo> {

	// review by requesting user
	private ReviewInfo userReview;

	public ReviewInfo getUserReview() {
		return userReview;
	}

	public void setUserReview(ReviewInfo userReview) {
		this.userReview = userReview;
	}

	@Override
	public String toString() {
		return String.format("{userReview=%s, list=%s}", userReview, getList());
	}

}
