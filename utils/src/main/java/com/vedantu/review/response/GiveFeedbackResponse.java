package com.vedantu.review.response;

import java.util.List;

import com.vedantu.review.pojo.Feedback;
import com.vedantu.util.fos.response.AbstractRes;

public class GiveFeedbackResponse extends AbstractRes{

	private String feedbackId;
	private String reciverId;
	private String sessionId;
	private String senderId;
	private int rating;
	private String sessionMessage;
	private String partnerMessage;
	private String optionalMessage;
	private List<String> reason;
	private String sessionCoverage;
	private String nextSessionPlan;
	private String homeWork;
	private List<String> studentPerformance;

	public GiveFeedbackResponse(Feedback feedback) {

		if (null == feedback) {
			return;
		}
		this.feedbackId = feedback.getId();
		this.senderId = feedback.getSenderId();
		this.sessionId = feedback.getSessionId();
		this.reciverId = feedback.getReceiverId();
		this.rating = feedback.getRating();
		this.sessionMessage = feedback.getSessionMessage();
		this.partnerMessage = feedback.getPartnerMessage();
		this.reason = feedback.getReason();
		this.sessionCoverage = feedback.getSessionCoverage();
		this.nextSessionPlan = feedback.getNextSessionPlan();
		this.homeWork = feedback.getHomeWork();
		this.studentPerformance = feedback.getStudentPerformance();
	}

	public String getFeedbackId() {

		return feedbackId;
	}

	public void setFeedbackId(String feedbackId) {

		this.feedbackId = feedbackId;
	}

	public String getReciverId() {

		return reciverId;
	}

	public void setReciverId(String reciverId) {

		this.reciverId = reciverId;
	}

	public String getSessionId() {

		return sessionId;
	}

	public void setSessionId(String sessionId) {

		this.sessionId = sessionId;
	}

	public String getSenderId() {

		return senderId;
	}

	public void setSenderId(String senderId) {

		this.senderId = senderId;
	}

	public int getRating() {

		return rating;
	}

	public void setRating(int rating) {

		this.rating = rating;
	}

	public String getSessionCoverage() {
		return sessionCoverage;
	}

	public void setSessionCoverage(String sessionCoverage) {
		this.sessionCoverage = sessionCoverage;
	}

	public String getNextSessionPlan() {
		return nextSessionPlan;
	}

	public void setNextSessionPlan(String nextSessionPlan) {
		this.nextSessionPlan = nextSessionPlan;
	}

	public String getHomeWork() {
		return homeWork;
	}

	public void setHomeWork(String homeWork) {
		this.homeWork = homeWork;
	}

	public List<String> getStudentPerformance() {
		return studentPerformance;
	}

	public void setStudentPerformance(List<String> studentPerformance) {
		this.studentPerformance = studentPerformance;
	}

	public String getSessionMessage() {
		return sessionMessage;
	}

	public void setSessionMessage(String sessionMessage) {
		this.sessionMessage = sessionMessage;
	}

	public String getPartnerMessage() {
		return partnerMessage;
	}

	public void setPartnerMessage(String partnerMessage) {
		this.partnerMessage = partnerMessage;
	}

	public String getOptionalMessage() {
		return optionalMessage;
	}

	public void setOptionalMessage(String optionalMessage) {
		this.optionalMessage = optionalMessage;
	}

	public List<String> getReason() {
		return reason;
	}

	public void setReason(List<String> reason) {
		this.reason = reason;
	}
}
