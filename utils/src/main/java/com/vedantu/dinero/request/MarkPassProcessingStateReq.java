package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 * Created by somil on 05/06/17.
 */
public class MarkPassProcessingStateReq extends AbstractFrontEndReq {
    private String id;
    private PassProcessingState passProcessingState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PassProcessingState getPassProcessingState() {
        return passProcessingState;
    }

    public void setPassProcessingState(PassProcessingState passProcessingState) {
        this.passProcessingState = passProcessingState;
    }



    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(id)) {
            errors.add("id");
        }
        if (passProcessingState==null) {
            errors.add("passProcessingState");
        }
        return errors;
    }
}
