/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.elasticsearch.common.lang3.StringUtils;

/**
 *
 * @author ajith
 */
public class ChangeInstalmentDueDateReq extends AbstractFrontEndReq {

    private String instalmentId;
    private Long dueTime;

    public String getInstalmentId() {
        return instalmentId;
    }

    public void setInstalmentId(String instalmentId) {
        this.instalmentId = instalmentId;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(instalmentId)) {
            errors.add("instalmentId");
        }
        if (dueTime == null || dueTime < System.currentTimeMillis()) {
            errors.add("dueTime");
        }
        return errors;
    }

}
