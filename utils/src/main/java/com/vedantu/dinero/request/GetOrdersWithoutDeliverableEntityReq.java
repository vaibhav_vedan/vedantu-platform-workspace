package com.vedantu.dinero.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetOrdersWithoutDeliverableEntityReq extends AbstractFrontEndListReq{

	private EntityType entityType;
	private Long userId;
	private String entityId;
	
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
}
