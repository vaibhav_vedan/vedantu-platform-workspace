package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 * Created by somil on 02/06/17.
 */
public class AddCouponRedeemEntryReq extends AbstractFrontEndReq {
    private String passCode;
    private Long userId;
    private String entityId;
    private EntityType entityType;
    private RedeemedCouponState state;
    private Long passExpirationTime;
    private PassProcessingState passProcessingState;
    private List<ReferenceTag> referenceTags;

    public List<ReferenceTag> getReferenceTags() {
        return referenceTags;
    }

    public void setReferenceTags(List<ReferenceTag> referenceTags) {
        this.referenceTags = referenceTags;
    }

    public PassProcessingState getPassProcessingState() {
        return passProcessingState;
    }

    public void setPassProcessingState(PassProcessingState passProcessingState) {
        this.passProcessingState = passProcessingState;
    }

    public Long getPassExpirationTime() {
        return passExpirationTime;
    }

    public void setPassExpirationTime(Long passExpirationTime) {
        this.passExpirationTime = passExpirationTime;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public RedeemedCouponState getState() {
        return state;
    }

    public void setState(RedeemedCouponState state) {
        this.state = state;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (state==null) {
            errors.add("state");
        }
        if (entityType==null) {
            errors.add("entityType");
        }
        return errors;
    }
}
