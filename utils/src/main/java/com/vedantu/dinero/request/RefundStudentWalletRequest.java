package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class RefundStudentWalletRequest extends AbstractFrontEndReq {

    private Long studentId;
    private int refundAmount;//based on hourlyRate paid to teacher
    private Long subscriptionId;
    private Float remainingPercentage;

    public RefundStudentWalletRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

    public RefundStudentWalletRequest(Long studentId, int refundAmount, Long subscriptionId,
            Float remainingPercentage) {
        super();
        this.studentId = studentId;
        this.refundAmount = refundAmount;
        this.subscriptionId = subscriptionId;
        this.remainingPercentage = remainingPercentage;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public int getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(int refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Float getRemainingPercentage() {
        return remainingPercentage;
    }

    public void setRemainingPercentage(Float remainingPercentage) {
        this.remainingPercentage = remainingPercentage;
    }

    @Override
    public String toString() {
        return "RefundStudentWalletRequest{" + "studentId=" + studentId + ", refundAmount=" + refundAmount + ", subscriptionId=" + subscriptionId + ", remainingPercentage=" + remainingPercentage + '}';
    }

}
