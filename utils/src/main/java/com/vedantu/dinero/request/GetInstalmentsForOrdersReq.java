/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentStatus;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetInstalmentsForOrdersReq {

    private List<String> orderIds;
    private PaymentStatus paymentStatus;

    public GetInstalmentsForOrdersReq(List<String> orderIds, PaymentStatus paymentStatus) {
        this.orderIds = orderIds;
        this.paymentStatus = paymentStatus;
    }

    public GetInstalmentsForOrdersReq() {
    }

    public List<String> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Override
    public String toString() {
        return "GetInstalmentsForOrdersReq{" + "orderIds=" + orderIds + ", paymentStatus=" + paymentStatus + '}';
    }
}
