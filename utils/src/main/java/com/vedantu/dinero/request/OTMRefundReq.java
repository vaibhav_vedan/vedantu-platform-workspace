/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

/**
 *
 * @author parashar
 */
public class OTMRefundReq extends OTMConsumptionReq{
    
    private Long userId;
    private Integer amtFromDiscount = 0;
    private boolean customerGrievence = false;
    private Integer amtCGP = 0;
    private Integer amtCGNP = 0;
    private Integer amtCGFromDiscount = 0;
    private String reasonNote;
    private boolean registration = false;
    

    


    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the customerGrievence
     */
    public boolean isCustomerGrievence() {
        return customerGrievence;
    }

    /**
     * @param customerGrievence the customerGrievence to set
     */
    public void setCustomerGrievence(boolean customerGrievence) {
        this.customerGrievence = customerGrievence;
    }

    /**
     * @return the CGAmtP
     */
    public Integer getAmtCGP() {
        return amtCGP;
    }

    /**
     * @param amtCGP the CGAmtP to set
     */
    public void setAmtCGP(Integer amtCGP) {
        this.amtCGP = amtCGP;
    }

    /**
     * @return the CGAmtNP
     */
    public Integer getAmtCGNP() {
        return amtCGNP;
    }

    /**
     * @param amtCGNP the CGAmtNP to set
     */
    public void setAmtCGNP(Integer amtCGNP) {
        this.amtCGNP = amtCGNP;
    }



    /**
     * @return the reasonNote
     */
    public String getReasonNote() {
        return reasonNote;
    }

    /**
     * @param reasonNote the reasonNote to set
     */
    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    /**
     * @return the amtCGFromDiscount
     */
    public Integer getAmtCGFromDiscount() {
        return amtCGFromDiscount;
    }

    /**
     * @param amtCGFromDiscount the amtCGFromDiscount to set
     */
    public void setAmtCGFromDiscount(Integer amtCGFromDiscount) {
        this.amtCGFromDiscount = amtCGFromDiscount;
    }

    /**
     * @return the amtFromDiscount
     */
    public Integer getAmtFromDiscount() {
        return amtFromDiscount;
    }

    /**
     * @param amtFromDiscount the amtFromDiscount to set
     */
    public void setAmtFromDiscount(Integer amtFromDiscount) {
        this.amtFromDiscount = amtFromDiscount;
    }

    /**
     * @return the registration
     */
    public boolean isRegistration() {
        return registration;
    }

    /**
     * @param registration the registration to set
     */
    public void setRegistration(boolean registration) {
        this.registration = registration;
    }
    
    
    
    
}
