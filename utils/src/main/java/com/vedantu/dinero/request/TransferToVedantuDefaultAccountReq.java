package com.vedantu.dinero.request;

public class TransferToVedantuDefaultAccountReq extends
		RechargeFreebiesAccountReq {

	private String reasonNote;
	private int promotionalAmount;
	private int nonPromotionalAmount;

	public int getPromotionalAmount() {
		return promotionalAmount;
	}

	public void setPromotionalAmount(int promotionalAmount) {
		this.promotionalAmount = promotionalAmount;
	}

	public int getNonPromotionalAmount() {
		return nonPromotionalAmount;
	}

	public void setNonPromotionalAmount(int nonPromotionalAmount) {
		this.nonPromotionalAmount = nonPromotionalAmount;
	}

	public TransferToVedantuDefaultAccountReq(Long userId, Integer amount, String reasonNote, int promotionalAmount,
			int nonPromotionalAmount) {
		super(userId, amount);
		this.reasonNote = reasonNote;
		this.promotionalAmount = promotionalAmount;
		this.nonPromotionalAmount = nonPromotionalAmount;
	}

	public String getReasonNote() {
		return reasonNote;
	}

	public void setReasonNote(String reasonNote) {
		this.reasonNote = reasonNote;
	}

	public TransferToVedantuDefaultAccountReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransferToVedantuDefaultAccountReq(Long userId, Integer amount) {
		super(userId, amount);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "TransferToVedantuDefaultAccountReq [reasonNote=" + reasonNote + ", promotionalAmount="
				+ promotionalAmount + ", nonPromotionalAmount=" + nonPromotionalAmount + "]";
	}

	

}
