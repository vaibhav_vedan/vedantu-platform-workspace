package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;

public class LockBalanceReq {

    private Long studentId;
    private Long teacherId;
    private String entityId;
    private EntityType entityType;
    private Long totalHours;
    private SessionSchedule sessionSchedule;
    private String teacherDiscountCouponId;
    private Long teacherDiscountAmount;
    private String vedantuDiscountCouponId;
    private Long vedantuDiscountAmount;
    private PaymentType paymentType;
    private PurchaseFlowType purchaseFlowType;
    private String purchaseFlowId;

    public LockBalanceReq() {
        super();
    }

    public LockBalanceReq(Long studentId, Long teacherId, String entityId, EntityType entityType, Long totalHours,
            String teacherDiscountCouponId, Long teacherDiscountAmount, String vedantuDiscountCouponId,
            Long vedantuDiscountAmount, SessionSchedule sessionSchedule,
            PaymentType paymentType,PurchaseFlowType purchaseFlowType,String purchaseFlowId) {
        super();
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.entityId = entityId;
        this.entityType = entityType;
        this.totalHours = totalHours;
        this.teacherDiscountCouponId = teacherDiscountCouponId;
        this.teacherDiscountAmount = teacherDiscountAmount;
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
        this.vedantuDiscountAmount = vedantuDiscountAmount;
        this.sessionSchedule = sessionSchedule;
        this.paymentType = paymentType;
        this.purchaseFlowId=purchaseFlowId;
        this.purchaseFlowType=purchaseFlowType;
        collectErrors();
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public String getTeacherDiscountCouponId() {
        return teacherDiscountCouponId;
    }

    public void setTeacherDiscountCouponId(String teacherDiscountCouponId) {
        this.teacherDiscountCouponId = teacherDiscountCouponId;
    }

    public Long getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    public Long getVedantuDiscountAmount() {
        return vedantuDiscountAmount;
    }

    public void setVedantuDiscountAmount(Long vedantuDiscountAmount) {
        this.vedantuDiscountAmount = vedantuDiscountAmount;
    }

    public SessionSchedule getSessionSchedule() {
        return sessionSchedule;
    }

    public void setSessionSchedule(SessionSchedule sessionSchedule) {
        this.sessionSchedule = sessionSchedule;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PurchaseFlowType getPurchaseFlowType() {
        return purchaseFlowType;
    }

    public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
        this.purchaseFlowType = purchaseFlowType;
    }

    public String getPurchaseFlowId() {
        return purchaseFlowId;
    }

    public void setPurchaseFlowId(String purchaseFlowId) {
        this.purchaseFlowId = purchaseFlowId;
    }
    
    //TODO put this at caller end preferably in constructor
    public List<String> collectErrors() {
        List<String> errors = new ArrayList<String>();
        if (this.studentId == null || this.studentId <= 0l) {
            errors.add("studentId");
        }

        if (this.totalHours == null || this.totalHours <= 0l) {
            errors.add("totalHours");
        }

        if (this.entityType == null) {
            errors.add("entityType");
        }

        if ((this.teacherId == null || this.teacherId <= 0l)
                || (StringUtils.isEmpty(this.entityId) || this.entityType == null)) {
            errors.add("entityId");
            errors.add("entityType");
            errors.add("teacherId");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "LockBalanceReq{" + "studentId=" + studentId + ", teacherId=" + teacherId + ", entityId=" + entityId + ", entityType=" + entityType + ", totalHours=" + totalHours + ", sessionSchedule=" + sessionSchedule + ", teacherDiscountCouponId=" + teacherDiscountCouponId + ", teacherDiscountAmount=" + teacherDiscountAmount + ", vedantuDiscountCouponId=" + vedantuDiscountCouponId + ", vedantuDiscountAmount=" + vedantuDiscountAmount + ", paymentType=" + paymentType + ", purchaseFlowType=" + purchaseFlowType + ", purchaseFlowId=" + purchaseFlowId + '}';
    }
}
