package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.BillingReasonType;
import java.util.List;

public class TransferFromFreebiesAccountReq extends RechargeFreebiesAccountReq {

    private TransactionRefType transactionRefType;
    private BillingReasonType billingReasonType;
    private Boolean noAlert = false;
    private Boolean noSMS = false;
    private Boolean allowNegativeBalance;
    private String reasonRefNo;
    private String reasonNote;
    private List<String> tags;

    public TransferFromFreebiesAccountReq() {
        super();
    }

    public TransferFromFreebiesAccountReq(Long userId, Integer amount) {
        super(userId, amount);
    }

    public TransferFromFreebiesAccountReq(Long userId, Integer amount,
            TransactionRefType transactionRefType, BillingReasonType billingReasonType, Boolean noAlert, Boolean sendNoSMS,
            Boolean allowNegativeBalance, String reasonRefNo, String reasonNote) {
        super(userId, amount);
        this.transactionRefType = transactionRefType;
        this.billingReasonType = billingReasonType;
        this.noAlert = noAlert;
        this.noSMS = sendNoSMS;
        this.allowNegativeBalance = allowNegativeBalance;
        this.reasonRefNo = reasonRefNo;
        this.reasonNote = reasonNote;
    }

    public String getReasonRefNo() {
        return reasonRefNo;
    }

    public void setReasonRefNo(String reasonRefNo) {
        this.reasonRefNo = reasonRefNo;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public TransactionRefType getTransactionRefType() {
        return transactionRefType;
    }

    public void setTransactionRefType(TransactionRefType transactionRefType) {
        this.transactionRefType = transactionRefType;
    }

    public BillingReasonType getBillingReasonType() {
        return billingReasonType;
    }

    public void setBillingReasonType(BillingReasonType billingReasonType) {
        this.billingReasonType = billingReasonType;
    }

    public Boolean getAllowNegativeBalance() {
        return allowNegativeBalance;
    }

    public void setAllowNegativeBalance(Boolean allowNegativeBalance) {
        this.allowNegativeBalance = allowNegativeBalance;
    }

    public Boolean getNoAlert() {
        return noAlert;
    }

    public void setNoAlert(Boolean noAlert) {
        this.noAlert = noAlert;
    }

    public Boolean getNoSMS() {
        return noSMS;
    }

    public void setNoSMS(Boolean noSMS) {
        this.noSMS = noSMS;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    
}
