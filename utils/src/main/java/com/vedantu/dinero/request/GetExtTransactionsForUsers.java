package com.vedantu.dinero.request;

import java.util.List;

/**
 * Created by somil on 04/05/17.
 */
public class GetExtTransactionsForUsers {
    List<Long> userIds;

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }
}
