package com.vedantu.dinero.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddPaymentInvoiceReq extends AbstractFrontEndReq {
	private Long studentId;
	private Long teacherId;

	private Long totalAmount; // In paisa. Including all the taxes
	private Long actualAmount;
	private Long invoiceTime; // Default will be set to Current time if not provided
	private String invoiceTitle;
	private String invoiceDescription;
	private PaymentInvoiceType paymentInvoiceType;

	private DeliverableEntityType deliverableEntityType;
	private String deliverableEntityId; // This has to be unique
	private String orderId;
	private List<String> tags;

	public AddPaymentInvoiceReq() {
		super();
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getInvoiceTime() {
		return invoiceTime;
	}

	public void setInvoiceTime(Long invoiceTime) {
		this.invoiceTime = invoiceTime;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public String getInvoiceDescription() {
		return invoiceDescription;
	}

	public void setInvoiceDescription(String invoiceDescription) {
		this.invoiceDescription = invoiceDescription;
	}

	public PaymentInvoiceType getPaymentInvoiceType() {
		return paymentInvoiceType;
	}

	public void setPaymentInvoiceType(PaymentInvoiceType paymentInvoiceType) {
		this.paymentInvoiceType = paymentInvoiceType;
	}

	public DeliverableEntityType getDeliverableEntityType() {
		return deliverableEntityType;
	}

	public void setDeliverableEntityType(DeliverableEntityType deliverableEntityType) {
		this.deliverableEntityType = deliverableEntityType;
	}

	public String getDeliverableEntityId() {
		return deliverableEntityId;
	}

	public void setDeliverableEntityId(String deliverableEntityId) {
		this.deliverableEntityId = deliverableEntityId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(Long actualAmount) {
		this.actualAmount = actualAmount;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		if (this.paymentInvoiceType == null) {
			errors.add("paymentInvoiceType");
		}
		if (this.actualAmount == null && this.totalAmount == null) {
			errors.add("actualAmount or totalAmount should be provided");
		}
		if (this.studentId == null && this.teacherId == null) {
			errors.add("fromUserId and toUserId missing");
		}

		return errors;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "AddPaymentInvoiceReq [studentId=" + studentId + ", teacherId=" + teacherId + ", totalAmount="
				+ totalAmount + ", actualAmount=" + actualAmount + ", invoiceTime=" + invoiceTime + ", invoiceTitle="
				+ invoiceTitle + ", invoiceDescription=" + invoiceDescription + ", paymentInvoiceType="
				+ paymentInvoiceType + ", deliverableEntityType=" + deliverableEntityType + ", deliverableEntityId="
				+ deliverableEntityId + ", orderId=" + orderId + ", tags=" + tags + ", toString()=" + super.toString()
				+ "]";
	}
}
