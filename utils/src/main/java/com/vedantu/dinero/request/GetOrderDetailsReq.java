/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetOrderDetailsReq extends AbstractFrontEndReq {

    private Long userId;
    private String entityId;
    private EntityType entityType;
    private List<PaymentStatus> paymentStatuses;
    
    
    public GetOrderDetailsReq() {
    }

    public GetOrderDetailsReq(Long userId, String entityId, EntityType entityType,
            List<PaymentStatus> paymentStatuses) {
        this.userId = userId;
        this.entityId = entityId;
        this.entityType = entityType;
        this.paymentStatuses=paymentStatuses;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public List<PaymentStatus> getPaymentStatuses() {
        return paymentStatuses;
    }

    public void setPaymentStatuses(List<PaymentStatus> paymentStatuses) {
        this.paymentStatuses = paymentStatuses;
    }
    
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (entityId == null) {
            errors.add("entityId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }   
        return errors;
    }

}
