/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.aspectj.weaver.Iterators.array;

/**
 *
 * @author ajith
 */
public class UpdateDeliverableDataReq extends AbstractFrontEndReq {

    private String orderId;
    private List<String> deliverableIds;//make sure the size of this and the below entry is same else it will throw error
    private List<String> purchasingEntityIds;
    private Map<String,String> entityDeliverableMap;
    private Long userId;
    private String entityId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<String> getDeliverableIds() {
        return deliverableIds;
    }

    public void setDeliverableIds(List<String> deliverableIds) {
        this.deliverableIds = deliverableIds;
    }

    public void addDeliverableId(String deliverableId) {
        if (StringUtils.isEmpty(deliverableId)) {
            return;
        }
        if (this.deliverableIds == null) {
            this.deliverableIds = new ArrayList<>();
        }
        this.deliverableIds.add(deliverableId);
    }

    public List<String> getPurchasingEntityIds() {
        return purchasingEntityIds;
    }

    public void setPurchasingEntityIds(List<String> purchasingEntityIds) {
        this.purchasingEntityIds = purchasingEntityIds;
    }

    public void addPurchasingEntityId(String purchasingEntityId) {
        if (StringUtils.isEmpty(purchasingEntityId)) {
            return;
        }
        if (this.purchasingEntityIds == null) {
            this.purchasingEntityIds = new ArrayList<>();
        }
        this.purchasingEntityIds.add(purchasingEntityId);
    }

    public Map<String, String> getEntityDeliverableMap() {
        return entityDeliverableMap;
    }

    public void setEntityDeliverableMap(Map<String, String> entityDeliverableMap) {
        this.entityDeliverableMap = entityDeliverableMap;
    }
    
    public void addEntityDeliverableMap(String purchasingEntityId,String deliverableId) {
        if (StringUtils.isEmpty(purchasingEntityId) || StringUtils.isEmpty(deliverableId)) {
            return;
        }
        if (this.entityDeliverableMap == null) {
            this.entityDeliverableMap = new HashMap<>();
        }
        this.entityDeliverableMap.put(purchasingEntityId,deliverableId);
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
    
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
//        if (ArrayUtils.isEmpty(purchasingEntityIds)) {
//            errors.add("purchasingEntityIds");
//        }
//        if (ArrayUtils.isEmpty(deliverableIds)) {
//            errors.add("deliverableIds");
//        }
//        if (ArrayUtils.isNotEmpty(purchasingEntityIds)
//                && ArrayUtils.isNotEmpty(deliverableIds)
//                && deliverableIds.size() != purchasingEntityIds.size()) {
//            errors.add("size of ids not equal");
//        }
        
        if(entityDeliverableMap == null || entityDeliverableMap.isEmpty()){
            errors.add("entityDeliverableMap");
        }
        return errors;
    }

}
