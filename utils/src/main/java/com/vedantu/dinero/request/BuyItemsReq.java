package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.exception.ErrorCode;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BuyItemsReq extends AbstractFrontEndUserReq {

    private List<OrderItem> items;
    private Boolean useAccountBalance;
    private String redirectUrl;
    private Long contextId;
    private PaymentType paymentType = PaymentType.BULK;
    private PurchaseFlowType purchaseFlowType = PurchaseFlowType.DIRECT;
    private String purchaseFlowId;
    private PaymentGatewayName preferredGateway;
    private PaymentOption paymentOption;

    // UTM Parameters added
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private String agentEmployeId;
    private String reEnrollmentOrderId;
    private String campaign;
    private String subscriptionPlanId ;

    public BuyItemsReq(List<OrderItem> items, Boolean useAccountBalance,
            Long contextId, String purchaseFlowId,
            PurchaseFlowType purchaseFlowType, PaymentType paymentType) {
        this.items = items;
        this.useAccountBalance = useAccountBalance;
        this.contextId = contextId;
        this.purchaseFlowId = purchaseFlowId;
        this.purchaseFlowType = purchaseFlowType;
        this.paymentType = paymentType;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (items == null || items.isEmpty()) {
            errors.add("OrderedItems is empty");
        } else if (items.size() > 1) {
            errors.add(ErrorCode.MULTIPLE_ORDERED_ITEMS_NOT_ALLOWED.name());
        }

        if (!items.isEmpty()) {
            OrderItem item = items.get(0);
            if (PaymentType.INSTALMENT.equals(paymentType)) {
                if ((EntityType.PLAN.equals(item.getEntityType())
                        && SessionModel.TRIAL.equals(item.getModel()))
                        || EntityType.INSTALEARN.equals(item.getEntityType())
                        || EntityType.BUNDLE_TRIAL.equals(item.getEntityType())
                        || ((EntityType.PLAN.equals(item.getEntityType()))
                        && !SubModel.DEFAULT.equals(item.getSubModel()))) {
                    errors.add("instalment not allowed for IL and trial session");
                }
                if ((item.getSchedule() == null
                        || ArrayUtils.isEmpty(item.getSchedule().getSessionSlots()))
                        && EntityType.PLAN.equals(item.getEntityType())) {
                    errors.add("No session slots found for instalment for plans");
                }
            }

            if(null == item.getEntityId()){
                errors.add("Entity Id cannot be null");
            }
            if(null == item.getEntityType()){
                errors.add("Entity Type cannot be null");
            }
        }
        return errors;
    }



}
