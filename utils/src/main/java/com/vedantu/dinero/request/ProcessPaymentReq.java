/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class ProcessPaymentReq extends AbstractFrontEndReq {

    private String orderId;
    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private Long userId;
    private List<BaseInstalmentInfo> baseInstalmentInfos;
    private String deliverableEntityId;
    private List<PurchasingEntity> purchasingEntites;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseEntityId() {
        return purchaseEntityId;
    }

    public void setPurchaseEntityId(String purchaseEntityId) {
        this.purchaseEntityId = purchaseEntityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<BaseInstalmentInfo> getBaseInstalmentInfos() {
        return baseInstalmentInfos;
    }

    public void setBaseInstalmentInfos(List<BaseInstalmentInfo> baseInstalmentInfos) {
        this.baseInstalmentInfos = baseInstalmentInfos;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public List<PurchasingEntity> getPurchasingEntites() {
        return purchasingEntites;
    }

    public void setPurchasingEntites(List<PurchasingEntity> purchasingEntites) {
        this.purchasingEntites = purchasingEntites;
    }
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (orderId == null) {
            errors.add("orderId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }

}
