/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class UpdateDeliverableEntityIdInOrderReq extends AbstractFrontEndReq {

    private String orderId;
    private String deliverableEntityId;
    private DeliverableEntityType deliverableEntityType;
    private String courseId;

    public UpdateDeliverableEntityIdInOrderReq() {
    }

    public UpdateDeliverableEntityIdInOrderReq(String orderId, String deliverableEntityId, DeliverableEntityType deliverableEntityType) {
        this.orderId = orderId;
        this.deliverableEntityId = deliverableEntityId;
        this.deliverableEntityType = deliverableEntityType;
    }
    
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public DeliverableEntityType getDeliverableEntityType() {
        return deliverableEntityType;
    }

    public void setDeliverableEntityType(DeliverableEntityType deliverableEntityType) {
        this.deliverableEntityType = deliverableEntityType;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
    
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(orderId)) {
            errors.add("orderId");
        }
        if (StringUtils.isEmpty(deliverableEntityId)) {
            errors.add("deliverableEntityId");
        }
        return errors;
    }

}
