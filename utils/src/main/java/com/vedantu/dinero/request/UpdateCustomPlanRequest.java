package com.vedantu.dinero.request;

import com.vedantu.User.AbstractEntityRes;

import java.util.List;


public class UpdateCustomPlanRequest extends AbstractEntityRes {
	private String planId;
	private Long teacherId;
	private Long totalHours;
	private Long price;
	private boolean active;

	public UpdateCustomPlanRequest() {
		super();
	}
	
	public UpdateCustomPlanRequest(String planId, Long teacherId, Long totalHours, Long price,
			boolean active) {
		super();
		this.planId = planId;
		this.teacherId = teacherId;
		this.totalHours = totalHours;
		this.price = price;
		this.active = active;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}


	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (null == teacherId) {
			errors.add("teacherId");
		}
		if (null == price || price < 0l) {
			errors.add("price");
		}
		if (null == totalHours || totalHours < 0l) {
			errors.add("totalHours");
		}
		return errors;
	}

	@Override
	public String toString() {
		return "UpdateCustomPlanRequest [teacherId=" + teacherId + ", totalHours=" + totalHours 
				+ ", price=" + price + ", active=" + active + "]";
	}
}
