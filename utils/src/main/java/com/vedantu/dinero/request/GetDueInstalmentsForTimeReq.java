package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetDueInstalmentsForTimeReq extends AbstractFrontEndListReq {

	private InstalmentPurchaseEntity contextType;
	private Long startTime;
	private Long endTime;
	private CoursePlanState state;

	public InstalmentPurchaseEntity getContextType() {
		return contextType;
	}

	public void setContextType(InstalmentPurchaseEntity contextType) {
		this.contextType = contextType;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CoursePlanState getState() {
		return state;
	}

	public void setState(CoursePlanState state) {
		this.state = state;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if (contextType == null) {
			errors.add("contextType");
		}
		if (startTime == null) {
			errors.add("startTime");
		}
		if (endTime == null) {
			errors.add("endTime");
		}

		return errors;
	}
}
