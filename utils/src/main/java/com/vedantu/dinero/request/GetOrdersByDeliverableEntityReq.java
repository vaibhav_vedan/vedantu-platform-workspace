package com.vedantu.dinero.request;


import java.util.List;

/**
 * Created by somil on 28/04/17.
 */
public class GetOrdersByDeliverableEntityReq {
    List<String> deliverableorEntityIds;

    public List<String> getDeliverableorEntityIds() {
        return deliverableorEntityIds;
    }

    public void setDeliverableorEntityIds(List<String> deliverableorEntityIds) {
        this.deliverableorEntityIds = deliverableorEntityIds;
    }
}
