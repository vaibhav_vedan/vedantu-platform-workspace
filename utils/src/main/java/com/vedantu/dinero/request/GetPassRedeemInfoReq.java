package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.List;

/**
 * Created by somil on 05/06/17.
 */
public class GetPassRedeemInfoReq extends AbstractFrontEndListReq {
    private Long passExpirationTime;
    private PassProcessingState passProcessingState;

    public Long getPassExpirationTime() {
        return passExpirationTime;
    }

    public void setPassExpirationTime(Long passExpirationTime) {
        this.passExpirationTime = passExpirationTime;
    }

    public PassProcessingState getPassProcessingState() {
        return passProcessingState;
    }

    public void setPassProcessingState(PassProcessingState passProcessingState) {
        this.passProcessingState = passProcessingState;
    }


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (passExpirationTime==null) {
            errors.add("passExpirationTime");
        }
        if (passProcessingState==null) {
            errors.add("passProcessingState");
        }
        return errors;
    }
}
