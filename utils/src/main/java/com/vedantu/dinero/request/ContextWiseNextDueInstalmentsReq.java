/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class ContextWiseNextDueInstalmentsReq extends AbstractFrontEndReq {

    private Long userId;//only for OTF
    private List<String> bundleIds;
    private List<String> batchIds;
    private List<String> deliverableIds;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<String> getBundleIds() {
        return bundleIds;
    }

    public void setBundleIds(List<String> bundleIds) {
        this.bundleIds = bundleIds;
    }

    public List<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(List<String> batchIds) {
        this.batchIds = batchIds;
    }

    public List<String> getDeliverableIds() {
        return deliverableIds;
    }

    public void setDeliverableIds(List<String> deliverableIds) {
        this.deliverableIds = deliverableIds;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (ArrayUtils.isEmpty(bundleIds) && ArrayUtils.isEmpty(batchIds)) {
            errors.add("bundleIds or batchIds");
        }
        return errors;
    }

}
