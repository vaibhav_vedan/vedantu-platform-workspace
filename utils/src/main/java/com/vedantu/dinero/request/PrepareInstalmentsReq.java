/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author ajith
 */
public class PrepareInstalmentsReq extends AbstractFrontEndReq {

    private SessionSchedule sessionSchedule;
    private Integer hourlyRate;
    private InstalmentPurchaseEntity instalmentPurchaseEntity;
    private String instalmentPurchaseEntityId;
    private Integer discountPerHour;
    private Boolean onlyFirstInstalment;
    private Integer totalDiscount;
    private Long userId;

    public PrepareInstalmentsReq() {
        super();
    }

    public PrepareInstalmentsReq(SessionSchedule sessionSchedule,
            Integer hourlyRate, InstalmentPurchaseEntity instalmentPurchaseEntity,
            String instalmentPurchaseEntityId, Integer discountPerHour) {
        super();
        this.sessionSchedule = sessionSchedule;
        this.hourlyRate = hourlyRate;
        this.instalmentPurchaseEntity = instalmentPurchaseEntity;
        this.instalmentPurchaseEntityId = instalmentPurchaseEntityId;
        this.discountPerHour = discountPerHour;
    }

    public SessionSchedule getSessionSchedule() {
        return sessionSchedule;
    }

    public void setSessionSchedule(SessionSchedule sessionSchedule) {
        this.sessionSchedule = sessionSchedule;
    }

    public Integer getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Integer hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public InstalmentPurchaseEntity getInstalmentPurchaseEntity() {
        return instalmentPurchaseEntity;
    }

    public void setInstalmentPurchaseEntity(InstalmentPurchaseEntity instalmentPurchaseEntity) {
        this.instalmentPurchaseEntity = instalmentPurchaseEntity;
    }

    public String getInstalmentPurchaseEntityId() {
        return instalmentPurchaseEntityId;
    }

    public void setInstalmentPurchaseEntityId(String instalmentPurchaseEntityId) {
        this.instalmentPurchaseEntityId = instalmentPurchaseEntityId;
    }

    public Integer getDiscountPerHour() {
        return discountPerHour;
    }

    public void setDiscountPerHour(Integer discountPerHour) {
        this.discountPerHour = discountPerHour;
    }

    public Boolean getOnlyFirstInstalment() {
        return onlyFirstInstalment;
    }

    public void setOnlyFirstInstalment(Boolean onlyFirstInstalment) {
        this.onlyFirstInstalment = onlyFirstInstalment;
    }
    
    public Integer getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Integer totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }    

}
