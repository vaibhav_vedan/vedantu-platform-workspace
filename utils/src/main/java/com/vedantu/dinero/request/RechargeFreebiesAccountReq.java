package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class RechargeFreebiesAccountReq extends AbstractFrontEndReq {

	private Long userId;
	private Integer amount;

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public Integer getAmount() {

		return amount;
	}

	public void setAmount(Integer amount) {

		this.amount = amount;
	}

	public RechargeFreebiesAccountReq(Long userId, Integer amount) {
		super();
		this.userId = userId;
		this.amount = amount;
	
        }
	public RechargeFreebiesAccountReq() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
