/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.enums.SortType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author somil
 */
public class SalesTransactionReq extends AbstractFrontEndReq {

    public Long userId;
    public Long tillTime;
    public Long fromTime;
    public int start;
    public int limit;
    public String reason;
    private SortType orderSortType;
    private PaymentStatus paymentStatus;
    private PaymentType paymentType;
    private EntityType entityType;

    // Added as a part of tools enhancement
    private PaymentThrough paymentThrough;
    private TeamType teamType;
    private String agentEmailId;
    private String teamLeadEmailId;
    private Centre centre;
    private EnrollmentType enrollmentType;
    private FintechRisk fintechRisk;
    private RequestRefund requestRefund;
    private long netCollection;
    private String notes;

    // Filtering in case of order details change
    private Long creationTimeLowerLimit;
    private Long creationTimeUpperLimit;
    private Boolean conditionalFiltering=Boolean.FALSE;

    private Integer minAmounForFilter;
    private Integer maxAmounForFilter;
    private String agentCode;
    private List<PaymentStatus> paymentStatuses;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public SortType getOrderSortType() {
        return orderSortType;
    }

    public void setOrderSortType(SortType orderSortType) {
        this.orderSortType = orderSortType;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public PaymentThrough getPaymentThrough() {
        return paymentThrough;
    }

    public void setPaymentThrough(PaymentThrough paymentThrough) {
        this.paymentThrough = paymentThrough;
    }

    public TeamType getTeamType() {
        return teamType;
    }

    public void setTeamType(TeamType teamType) {
        this.teamType = teamType;
    }

    public String getAgentEmailId() {
        return agentEmailId;
    }

    public void setAgentEmailId(String agentEmailId) {
        this.agentEmailId = agentEmailId;
    }

    public String getTeamLeadEmailId() {
        return teamLeadEmailId;
    }

    public void setTeamLeadEmailId(String teamLeadEmailId) {
        this.teamLeadEmailId = teamLeadEmailId;
    }

    public Centre getCentre() {
        return centre;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    public EnrollmentType getEnrollmentType() {
        return enrollmentType;
    }

    public void setEnrollmentType(EnrollmentType enrollmentType) {
        this.enrollmentType = enrollmentType;
    }

    public FintechRisk getFintechRisk() {
        return fintechRisk;
    }

    public void setFintechRisk(FintechRisk fintechRisk) {
        this.fintechRisk = fintechRisk;
    }

    public RequestRefund getRequestRefund() { return  requestRefund;}

    public void setRequestRefund(RequestRefund requestRefund) {this.requestRefund = requestRefund; }

    public long getNetCollection() {
        return netCollection;
    }

    public void setNetCollection(long netCollection) {
        this.netCollection = netCollection;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getCreationTimeLowerLimit() {
        return creationTimeLowerLimit;
    }

    public void setCreationTimeLowerLimit(Long creationTimeLowerLimit) {
        this.creationTimeLowerLimit = creationTimeLowerLimit;
    }

    public Long getCreationTimeUpperLimit() {
        return creationTimeUpperLimit;
    }

    public void setCreationTimeUpperLimit(Long creationTimeUpperLimit) {
        this.creationTimeUpperLimit = creationTimeUpperLimit;
    }

    public Boolean getConditionalFiltering() {
        return conditionalFiltering;
    }

    public void setConditionalFiltering(Boolean conditionalFiltering) {
        this.conditionalFiltering = conditionalFiltering;
    }

    public Integer getMinAmounForFilter() {
        return minAmounForFilter;
    }

    public void setMinAmounForFilter(Integer minAmounForFilter) {
        this.minAmounForFilter = minAmounForFilter;
    }

    public Integer getMaxAmounForFilter() {
        return maxAmounForFilter;
    }

    public void setMaxAmounForFilter(Integer maxAmounForFilter) {
        this.maxAmounForFilter = maxAmounForFilter;
    }

    public String getAgentCode() { return agentCode; }

    public void setAgentCode(String agentCode) { this.agentCode = agentCode; }

    public List<PaymentStatus> getPaymentStatuses() { return paymentStatuses; }

    public void setPaymentStatuses(List<PaymentStatus> paymentStatuses) { this.paymentStatuses = paymentStatuses; }
}
