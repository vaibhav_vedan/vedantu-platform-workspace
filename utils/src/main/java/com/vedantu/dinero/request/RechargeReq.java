package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.vedantu.dinero.enums.PaymentGatewayName.JUSPAY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RechargeReq extends AbstractFrontEndReq {

    private Long userId;
    // amount in paisa
    private Integer amount;
    private String redirectUrl;
    private TransactionRefType refType;
    private String refId;
    private PaymentOption paymentOption;

    public RechargeReq(Long userId, Integer amount, String redirectUrl) {
        super();
        this.userId = userId;
        this.amount = amount;
        this.redirectUrl = redirectUrl;
    }

    public RechargeReq(Long callingUserId, String ipAddress, Long userId, int amount, String redirectUrl, TransactionRefType refType,
                       String refId, PaymentOption paymentOption) {
        super.setCallingUserId(callingUserId);
        super.setIpAddress(ipAddress);
        this.userId = userId;
        this.amount = amount;
        this.redirectUrl = redirectUrl;
        this.refType = refType;
        this.refId = refId;
        this.paymentOption = paymentOption;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (paymentOption != null && paymentOption.getPaymentInstrument() != null && JUSPAY.equals(paymentOption.getOptedGateway())) {
            switch (paymentOption.getPaymentInstrument().getOptedPaymentMethodType()) {
                case CARD: {
                    PaymentOption.Card instrument = (PaymentOption.Card) paymentOption.getPaymentInstrument();
                    if (instrument != null) {
                        errors.addAll(instrument.getErrors());
                    } else {
                        errors.add("Card details empty.");
                    }
                    break;
                }
                case NETBANKING: {
                    PaymentOption.NetBanking instrument = (PaymentOption.NetBanking) paymentOption.getPaymentInstrument();
                    if (instrument != null) {
                        errors.addAll(instrument.getErrors());
                    } else {
                        errors.add("Netbanking details empty.");
                    }
                    break;
                }
                case WALLET: {
                    PaymentOption.Wallet instrument = (PaymentOption.Wallet) paymentOption.getPaymentInstrument();
                    if (instrument != null) {
                        errors.addAll(instrument.getErrors());
                    } else {
                        errors.add("Wallet details empty.");
                    }
                    break;
                }
                case WALLET_DIRECT_DEBIT: {
                    PaymentOption.Wallet instrument = (PaymentOption.Wallet) paymentOption.getPaymentInstrument();
                    if (instrument != null) {
                        errors.addAll(instrument.getErrors());
                        if (StringUtils.isEmpty(instrument.getDirectDebitToken())) {
                            errors.add("Wallet Debit token is empty");
                        }
                    } else {
                        errors.add("Wallet details empty.");
                    }
                    break;
                }
                case ATM_SEAMLESS_PIN:
                case ATM_REDIRECTION:
                case UPI_COLLECT:
                case UPI_PAY:
                    errors.add("Payment type not configured");
            }
        }
        return errors;
    }
}
