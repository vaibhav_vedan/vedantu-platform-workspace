package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.TransactionRefSubType;
import com.vedantu.dinero.enums.LoanType;

public class SubmitCashChequeReq extends RechargeFreebiesAccountReq {

    private String reasonRefNo;
    private String reasonNote;
    private String reasonNoteType;
    private TransactionRefSubType reasonRefSubType;
    private LoanType loanType;


    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public String getReasonRefNo() {
        return reasonRefNo;
    }

    public void setReasonRefNo(String reasonRefNo) {
        this.reasonRefNo = reasonRefNo;
    }

    public String getReasonNoteType() {
        return reasonNoteType;
    }

    public void setReasonNoteType(String reasonNoteType) {
        this.reasonNoteType = reasonNoteType;
    }

    public TransactionRefSubType getReasonRefSubType() {
        return reasonRefSubType;
    }

    public void setReasonRefSubType(TransactionRefSubType reasonRefSubType) {
        this.reasonRefSubType = reasonRefSubType;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }
}
