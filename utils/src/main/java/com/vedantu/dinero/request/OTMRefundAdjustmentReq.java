/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

/**
 *
 * @author parashar
 */
public class OTMRefundAdjustmentReq {
    
    private String enrollmentId;
    private boolean trial;
    private Integer amount;
    private Integer promotionalAmount;
    private Integer nonPromotionalAmount;
    private Long userId;
    private String batchId;

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the isTrial
     */
    public boolean isTrial() {
        return trial;
    }

    /**
     * @param trial the isTrial to set
     */
    public void setTrial(boolean trial) {
        this.trial = trial;
    }

    /**
     * @return the amount
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * @return the promotionalAmount
     */
    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    /**
     * @param promotionalAmount the promotionalAmount to set
     */
    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    /**
     * @return the nonPromotionalAmount
     */
    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    /**
     * @param nonPromotionalAmount the nonPromotionalAmount to set
     */
    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
    
    
    
}
