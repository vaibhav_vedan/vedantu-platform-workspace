package com.vedantu.dinero.request;

import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class OTMConsumptionReq extends AbstractFrontEndReq {

    private Integer amount;
    private Integer promotionalAmt;
    private Integer nonPromotionalAmt;
    private EnrollmentTransactionContextType enrollmentTransactionContextType;
    private String enrollmentId;
    private Integer negativeMarginAmount;
    private boolean negativeMargin = false;
    private boolean negativeMarginAdjustment = false;

    
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPromotionalAmt() {
        return promotionalAmt;
    }

    public void setPromotionalAmt(Integer promotionalAmt) {
        this.promotionalAmt = promotionalAmt;
    }

    public Integer getNonPromotionalAmt() {
        return nonPromotionalAmt;
    }

    public void setNonPromotionalAmt(Integer nonPromotionalAmt) {
        this.nonPromotionalAmt = nonPromotionalAmt;
    }

    /**
     * @return the enrollmentTransactionContextType
     */
    public EnrollmentTransactionContextType getEnrollmentTransactionContextType() {
        return enrollmentTransactionContextType;
    }

    /**
     * @param enrollmentTransactionContextType the enrollmentTransactionContextType to set
     */
    public void setEnrollmentTransactionContextType(EnrollmentTransactionContextType enrollmentTransactionContextType) {
        this.enrollmentTransactionContextType = enrollmentTransactionContextType;
    }


    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the negativeMargin
     */
    public boolean isNegativeMargin() {
        return negativeMargin;
    }

    /**
     * @param negativeMargin the negativeMargin to set
     */
    public void setNegativeMargin(boolean negativeMargin) {
        this.negativeMargin = negativeMargin;
    }

    /**
     * @return the negativeMarginAmount
     */
    public Integer getNegativeMarginAmount() {
        return negativeMarginAmount;
    }

    /**
     * @param negativeMarginAmount the negativeMarginAmount to set
     */
    public void setNegativeMarginAmount(Integer negativeMarginAmount) {
        this.negativeMarginAmount = negativeMarginAmount;
    }

    /**
     * @return the negativeMarginAdjustment
     */
    public boolean isNegativeMarginAdjustment() {
        return negativeMarginAdjustment;
    }

    /**
     * @param negativeMarginAdjustment the negativeMarginAdjustment to set
     */
    public void setNegativeMarginAdjustment(boolean negativeMarginAdjustment) {
        this.negativeMarginAdjustment = negativeMarginAdjustment;
    }
    
}
