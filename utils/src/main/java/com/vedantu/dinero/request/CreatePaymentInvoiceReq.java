/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentInvoiceContextType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class CreatePaymentInvoiceReq extends AbstractReq{
    private String ipAddress;
    private Long userId;
    private PaymentInvoiceContextType contextType;
    private PaymentGatewayName gatewayName;
    private String contextId;
    private Integer amount;
    private Long invoiceTime;

    public CreatePaymentInvoiceReq() {
    }

    public CreatePaymentInvoiceReq(String ipAddress, Long userId, PaymentInvoiceContextType contextType, PaymentGatewayName gatewayName, String contextId, Integer amount, Long invoiceTime) {
        this.ipAddress = ipAddress;
        this.userId = userId;
        this.contextType = contextType;
        this.gatewayName = gatewayName;
        this.contextId = contextId;
        this.amount = amount;
        this.invoiceTime = invoiceTime;
    }
    
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public PaymentInvoiceContextType getContextType() {
        return contextType;
    }

    public void setContextType(PaymentInvoiceContextType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    
    public PaymentGatewayName getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(PaymentGatewayName gatewayName) {
        this.gatewayName = gatewayName;
    }

    public Long getInvoiceTime() {
        return invoiceTime;
    }

    public void setInvoiceTime(Long invoiceTime) {
        this.invoiceTime = invoiceTime;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (!PaymentInvoiceContextType.CASH_CHEQUE.equals(contextType) && gatewayName == null) {
            errors.add("gatewayName");
        }
        if (amount == null) {
            errors.add("amount");
        }
        return errors;
    }
}
