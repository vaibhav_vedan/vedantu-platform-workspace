package com.vedantu.dinero.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetPaymentInvoiceReq extends AbstractFrontEndListReq {
	private Long userId;

	public GetPaymentInvoiceReq() {
		super();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
    protected List<String> collectVerificationErrors() {
		List<String> errros = new ArrayList<>();
		if (userId == null || userId <= 0l) {
			errros.add("userId or callingUserId missing");
		}
        return errros;
    }

	@Override
	public String toString() {
		return "GetPaymentInvoiceReq [userId=" + userId + ", toString()=" + super.toString() + "]";
	}
}
