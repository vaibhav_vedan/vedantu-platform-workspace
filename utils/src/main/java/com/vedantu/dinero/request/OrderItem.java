package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderItem {

    private String entityId;
    private EntityType entityType;
    private Integer count;
    private Long hours;
    private SessionModel model;
    private SubModel subModel;
    private String target;
    private Integer grade;
    private Long boardId;
    private SessionSchedule schedule;
    private Long teacherId;
    private Long offeringId;
    private Boolean renew;
    private String teacherDiscountCouponId;
    private String vedantuDiscountCouponId;
    private String note;
    private SessionSource sessionSource;
    private RefundPolicy refundStatus;
    private String trialId;

    public OrderItem(String entityId, EntityType entityType, Integer count, Long hours, SessionModel model,
            SubModel subModel, String target, Integer grade, Long boardId, SessionSchedule schedule, Long teacherId,
            Long offeringId, Boolean renew, String teacherDiscountCouponId, String vedantuDiscountCouponId,
            String note) {
        super();
        this.entityId = entityId;
        this.entityType = entityType;
        this.count = count;
        this.hours = hours;
        this.model = model;
        this.subModel = subModel;
        this.target = target;
        this.grade = grade;
        this.boardId = boardId;
        this.schedule = schedule;
        this.teacherId = teacherId;
        this.offeringId = offeringId;
        this.renew = renew;
        this.teacherDiscountCouponId = teacherDiscountCouponId;
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
        this.note = note;
    }

}
