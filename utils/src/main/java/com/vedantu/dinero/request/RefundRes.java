/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
public class RefundRes {

    private int refundAmount;//final refund
    private int promotionalAmount;
    private int nonPromotionalAmount;
    private int security;
    private int amountGivenasDiscount;
    private List<PaymentType> paymentTypes;
    private List<String> deliverableEntityIds;
    private List<String> orderIds;

    public RefundRes() {
        this.paymentTypes = new ArrayList<>();
        this.deliverableEntityIds = new ArrayList<>();
        this.orderIds = new ArrayList<>();
    }

    public RefundRes(int refundAmount, int promotionalAmount, int nonPromotionalAmount,
            int security, int amountGivenasDiscount, List<PaymentType> paymentType,
            List<String> deliverableEntityId, List<String> orderId) {
        this.refundAmount = refundAmount;
        this.promotionalAmount = promotionalAmount;
        this.nonPromotionalAmount = nonPromotionalAmount;
        this.security = security;
        this.amountGivenasDiscount = amountGivenasDiscount;
        this.paymentTypes = paymentType;
        this.deliverableEntityIds = deliverableEntityId;
        this.orderIds = orderId;
    }

    public int getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(int refundAmount) {
        this.refundAmount = refundAmount;
    }

    public int getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(int promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public int getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(int nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public int getSecurity() {
        return security;
    }

    public void setSecurity(int security) {
        this.security = security;
    }

    public int getAmountGivenasDiscount() {
        return amountGivenasDiscount;
    }

    public void setAmountGivenasDiscount(int amountGivenasDiscount) {
        this.amountGivenasDiscount = amountGivenasDiscount;
    }

    public List<PaymentType> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<PaymentType> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public List<String> getDeliverableEntityIds() {
        return deliverableEntityIds;
    }

    public void setDeliverableEntityIds(List<String> deliverableEntityIds) {
        this.deliverableEntityIds = deliverableEntityIds;
    }

    public List<String> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    @Override
    public String toString() {
        return "RefundRes{" + "refundAmount=" + refundAmount + ", promotionalAmount=" + promotionalAmount + ", nonPromotionalAmount=" + nonPromotionalAmount + ", security=" + security + ", amountGivenasDiscount=" + amountGivenasDiscount + ", paymentTypes=" + paymentTypes + ", deliverableEntityIds=" + deliverableEntityIds + ", orderId=" + orderIds + '}';
    }
}
