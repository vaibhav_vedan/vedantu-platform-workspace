/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.response.InstalmentInfo;
import java.util.List;

/**
 *
 * @author somil
 */
public class SendInstalmentPaidEmailReq {

    private String subscriptionId;
    private String subscriptionTitle;
    private Long teacherId;
    private Long studentId;
    private List<InstalmentInfo> instalments;
    private String subscriptionLink; //only for OTF

    public SendInstalmentPaidEmailReq() {
        super();
    }

    public SendInstalmentPaidEmailReq(String subscriptionId, String subscriptionTitle,
            Long teacherId, Long studentId, List<InstalmentInfo> instalments) {
        this.subscriptionId = subscriptionId;
        this.subscriptionTitle = subscriptionTitle;
        this.teacherId = teacherId;
        this.studentId = studentId;
        this.instalments = instalments;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public List<InstalmentInfo> getInstalments() {
        return instalments;
    }

    public void setInstalments(List<InstalmentInfo> instalments) {
        this.instalments = instalments;
    }

    public String getSubscriptionLink() {
        return subscriptionLink;
    }

    public void setSubscriptionLink(String subscriptionLink) {
        this.subscriptionLink = subscriptionLink;
    }

    @Override
    public String toString() {
        return "SendInstalmentPaidEmailReq{" + "subscriptionId=" + subscriptionId + ", subscriptionTitle=" + subscriptionTitle + ", teacherId=" + teacherId + ", studentId=" + studentId + ", instalments=" + instalments + '}';
    }
}
