package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.enums.SortType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

public class GetOrdersReq extends AbstractFrontEndListReq {

    private Long userId;
    private PaymentStatus paymentStatus;
    private List<PaymentStatus> paymentStatuses;
    private PaymentType paymentType;
    private EntityType entityType;
    private String entityId;
    private Long startTime;
    private Long endTime;
    private SortType orderSortType;

    // Added as a part of tools enhancement
    private PaymentThrough paymentThrough;
    private TeamType teamType;
    private String agentEmailId;
    private String teamLeadEmailId;
    private Centre centre;
    private EnrollmentType enrollmentType;
    private FintechRisk fintechRisk;
    private RequestRefund requestRefund;
    private long netCollection;
    private String notes;

    // Filtering in case of order details change
    private Long creationTimeLowerLimit;
    private Long creationTimeUpperLimit;
    private Boolean conditionalFiltering=Boolean.FALSE;

    private Integer minAmounForFilter;
    private Integer maxAmounForFilter;
    private String agentCode;

    public GetOrdersReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    public GetOrdersReq(Long userId, PaymentStatus paymentStatus, PaymentType paymentType) {
        this.userId = userId;
        this.paymentStatus = paymentStatus;
        this.paymentType = paymentType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public List<PaymentStatus> getPaymentStatuses() {
        return paymentStatuses;
    }

    public void setPaymentStatuses(List<PaymentStatus> paymentStatuses) {
        this.paymentStatuses = paymentStatuses;
    }
    

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public SortType getOrderSortType() {
        return orderSortType;
    }

    public void setOrderSortType(SortType orderSortType) {
        this.orderSortType = orderSortType;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public PaymentThrough getPaymentThrough() {
        return paymentThrough;
    }

    public void setPaymentThrough(PaymentThrough paymentThrough) {
        this.paymentThrough = paymentThrough;
    }

    public TeamType getTeamType() {
        return teamType;
    }

    public void setTeamType(TeamType teamType) {
        this.teamType = teamType;
    }

    public String getAgentEmailId() {
        return agentEmailId;
    }

    public void setAgentEmailId(String agentEmailId) {
        this.agentEmailId = agentEmailId;
    }

    public String getTeamLeadEmailId() {
        return teamLeadEmailId;
    }

    public void setTeamLeadEmailId(String teamLeadEmailId) {
        this.teamLeadEmailId = teamLeadEmailId;
    }

    public Centre getCentre() {
        return centre;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    public EnrollmentType getEnrollmentType() {
        return enrollmentType;
    }

    public void setEnrollmentType(EnrollmentType enrollmentType) {
        this.enrollmentType = enrollmentType;
    }

    public FintechRisk getFintechRisk() {
        return fintechRisk;
    }

    public void setFintechRisk(FintechRisk fintechRisk) {
        this.fintechRisk = fintechRisk;
    }

    public RequestRefund getRequestRefund() { return  requestRefund; }

    public void  setRequestRefund(RequestRefund requestRefund) {this.requestRefund = requestRefund; }

    public long getNetCollection() {
        return netCollection;
    }

    public void setNetCollection(long netCollection) {
        this.netCollection = netCollection;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getCreationTimeLowerLimit() {
        return creationTimeLowerLimit;
    }

    public void setCreationTimeLowerLimit(Long creationTimeLowerLimit) {
        this.creationTimeLowerLimit = creationTimeLowerLimit;
    }

    public Long getCreationTimeUpperLimit() {
        return creationTimeUpperLimit;
    }

    public void setCreationTimeUpperLimit(Long creationTimeUpperLimit) {
        this.creationTimeUpperLimit = creationTimeUpperLimit;
    }

    public Boolean getConditionalFiltering() {
        return conditionalFiltering;
    }

    public void setConditionalFiltering(Boolean conditionalFiltering) {
        this.conditionalFiltering = conditionalFiltering;
    }

    public Integer getMinAmounForFilter() {
        return minAmounForFilter;
    }

    public void setMinAmounForFilter(Integer minAmounForFilter) {
        this.minAmounForFilter = minAmounForFilter;
    }

    public Integer getMaxAmounForFilter() {
        return maxAmounForFilter;
    }

    public void setMaxAmounForFilter(Integer maxAmounForFilter) {
        this.maxAmounForFilter = maxAmounForFilter;
    }

    public String getAgentCode() { return agentCode; }

    public void setAgentCode(String agentCode) { this.agentCode = agentCode; }
}
