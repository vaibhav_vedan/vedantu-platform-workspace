package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddAccountPancardReq extends AbstractFrontEndReq {
	private Long userId;
	private String panCard;

	public AddAccountPancardReq() {
		super();
	}

	public AddAccountPancardReq(Long userId, String panCard) {
		super();
		this.userId = userId;
		this.panCard = panCard;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	@Override
	public String toString() {
		return "AddAccountPancardReq [userId=" + userId + ", panCard=" + panCard + ", toString()=" + super.toString()
				+ "]";
	}
}
