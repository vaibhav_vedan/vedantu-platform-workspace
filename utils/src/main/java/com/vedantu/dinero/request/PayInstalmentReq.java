/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import lombok.Data;

import java.util.List;

/**
 *
 * @author ajith
 */
@Data
public class PayInstalmentReq extends AbstractFrontEndUserReq {

    private Boolean useAccountBalance = Boolean.TRUE;
    private String instalmentId;
    private String redirectUrl;
    private Boolean newFlow;
    private PaymentOption paymentOption;

    public PayInstalmentReq() {
        super();
    }

    public PayInstalmentReq(Boolean useAccountBalance, String instalmentId) {
        super();
        this.useAccountBalance = useAccountBalance;
        this.instalmentId = instalmentId;
    }

    public Boolean getUseAccountBalance() {
        return useAccountBalance;
    }

    public void setUseAccountBalance(Boolean useAccountBalance) {
        this.useAccountBalance = useAccountBalance;
    }

    public String getInstalmentId() {
        return instalmentId;
    }

    public void setInstalmentId(String instalmentId) {
        this.instalmentId = instalmentId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Boolean getNewFlow() {
        return newFlow;
    }

    public void setNewFlow(Boolean newFlow) {
        this.newFlow = newFlow;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(instalmentId)) {
            errors.add("instalmentId");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "PayInstalmentReq{" + "useAccountBalance=" + useAccountBalance + ", instalmentId=" + instalmentId + ", redirectUrl=" + redirectUrl + ", newFlow=" + newFlow + '}';
    }

}
