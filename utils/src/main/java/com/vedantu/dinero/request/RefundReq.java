/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.datanucleus.util.StringUtils;

/**
 *
 * @author ajith
 */
public class RefundReq extends AbstractFrontEndReq {

    private Long userId;
    private EntityType entityType;
    private String entityId;
    private Float remainingPercentage;//will help in calculating refund amt for partial refund    
    private RefundPolicy refundPolicy;
    private Boolean refundAllOrders = false;//if multiple orders found what is the strategy
    private Boolean getRefundInfoOnly = false;
    //sometimes hard coded values are refunded
    private Integer promotionalAmount = 0;
    private Integer nonPromotionalAmount = 0;
    private Integer discountLeft = 0;
    private String deliverableEntityId;
    private String reason;
    private String orderId;
    private Long actualRefundDate;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Float getRemainingPercentage() {
        return remainingPercentage;
    }

    public void setRemainingPercentage(Float remainingPercentage) {
        this.remainingPercentage = remainingPercentage;
    }

    public RefundPolicy getRefundPolicy() {
        return refundPolicy;
    }

    public void setRefundPolicy(RefundPolicy refundPolicy) {
        this.refundPolicy = refundPolicy;
    }

    public Boolean getRefundAllOrders() {
        return refundAllOrders;
    }

    public void setRefundAllOrders(Boolean refundAllOrders) {
        this.refundAllOrders = refundAllOrders;
    }

    public Boolean getGetRefundInfoOnly() {
        return getRefundInfoOnly;
    }

    public void setGetRefundInfoOnly(Boolean getRefundInfoOnly) {
        this.getRefundInfoOnly = getRefundInfoOnly;
    }

    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public Integer getDiscountLeft() {
        return discountLeft;
    }

    public void setDiscountLeft(Integer discountLeft) {
        this.discountLeft = discountLeft;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getActualRefundDate() {
        return actualRefundDate;
    }
    public void setActualRefundDate(Long actualRefundDate) {
        this.actualRefundDate = actualRefundDate;
    }


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (refundPolicy == null) {
            errors.add("refundPolicy");
        }

        if (super.getCallingUserId() == null) {
            errors.add("callingUserId");
        }

//        if (RefundPolicy.PARTIAL.equals(refundPolicy) && remainingPercentage == null) {
//            errors.add("remainingPercentage");
//        }
        return errors;
    }

}
