package com.vedantu.dinero.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class ApplyCouponReq extends AbstractFrontEndReq {

    private String code;

    public ApplyCouponReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ApplyCouponReq(String code) {
        super();
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(code)) {
            errors.add("code");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "ApplyCouponReq [code=" + code + "]";
    }

}
