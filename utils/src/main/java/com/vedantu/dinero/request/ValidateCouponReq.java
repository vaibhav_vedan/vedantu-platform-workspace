package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

import lombok.Data;
import org.springframework.util.StringUtils;
@Data
public class ValidateCouponReq extends AbstractFrontEndReq {

    private String code;
    private EntityType entityType;//purchasingentityType eg.PLAN,OTF
    private String entityId;
    private Integer totalAmount;
    private PaymentType forPaymentType = PaymentType.BULK;
    private String subscriptionPlanId;


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(code)) {
            errors.add("code");
        }
        if (totalAmount == null || totalAmount < 0) {
            errors.add("totalAmount");
        }
        return errors;
    }

    public PaymentType getForPaymentType() {
        return forPaymentType;
    }

    public void setForPaymentType(PaymentType forPaymentType) {
        this.forPaymentType = forPaymentType;
    }

}
