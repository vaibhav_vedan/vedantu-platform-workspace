package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class BuyItemsReqNew extends AbstractFrontEndReq {

    private Long userId;
    private String entityId;
    private EntityType entityType;
    private String teacherDiscountCouponId;
    private String vedantuDiscountCouponId;
    private Boolean useAccountBalance = Boolean.TRUE;
    private String redirectUrl;
    // TODO: remove contextId when only teacher plans comes
    // this will be teacherId when buying a course plan and selecting a teacher
    private Long contextId;

    private PaymentType paymentType = PaymentType.BULK;
    private PurchaseFlowType purchaseFlowType = PurchaseFlowType.DIRECT;
    private String purchaseFlowId;
    private Integer amountToPay;//will be filled by platform before sending to dinero
    private boolean throwErrorIfAlreadyPaid;//will be filled by platform before sending to dinero
    private List<BaseInstalmentInfo> baseInstalmentInfos;
    private List<PurchasingEntity> purchasingEntities;
    
    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseContextId; // Bundle id from where it is coming
    private String subscriptionPlanId ;
    private Integer adjustmentAmount;
    // UTM Parameters added
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getTeacherDiscountCouponId() {
        return teacherDiscountCouponId;
    }

    public void setTeacherDiscountCouponId(String teacherDiscountCouponId) {
        this.teacherDiscountCouponId = teacherDiscountCouponId;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    public Boolean getUseAccountBalance() {
        return useAccountBalance;
    }

    public void setUseAccountBalance(Boolean useAccountBalance) {
        this.useAccountBalance = useAccountBalance;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Long getContextId() {
        return contextId;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PurchaseFlowType getPurchaseFlowType() {
        return purchaseFlowType;
    }

    public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
        this.purchaseFlowType = purchaseFlowType;
    }

    public String getPurchaseFlowId() {
        return purchaseFlowId;
    }

    public void setPurchaseFlowId(String purchaseFlowId) {
        this.purchaseFlowId = purchaseFlowId;
    }

    public Integer getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(Integer amountToPay) {
        this.amountToPay = amountToPay;
    }

    public boolean getThrowErrorIfAlreadyPaid() {
        return throwErrorIfAlreadyPaid;
    }

    public void setThrowErrorIfAlreadyPaid(boolean throwErrorIfAlreadyPaid) {
        this.throwErrorIfAlreadyPaid = throwErrorIfAlreadyPaid;
    }

    public List<BaseInstalmentInfo> getBaseInstalmentInfos() {
        return baseInstalmentInfos;
    }

    public void setBaseInstalmentInfos(List<BaseInstalmentInfo> baseInstalmentInfos) {
        this.baseInstalmentInfos = baseInstalmentInfos;
    }

    public List<PurchasingEntity> getPurchasingEntities() {
        return purchasingEntities;
    }

    public void setPurchasingEntities(List<PurchasingEntity> purchasingEntities) {
        this.purchasingEntities = purchasingEntities;
    }

    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseContextId() {
        return purchaseContextId;
    }

    public void setPurchaseContextId(String purchaseContextId) {
        this.purchaseContextId = purchaseContextId;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSubscriptionPlanId() {
        return subscriptionPlanId;
    }

    public void setSubscriptionPlanId(String subscriptionPlanId) {
        this.subscriptionPlanId = subscriptionPlanId;
    }

    public Integer getAdjustmentAmount() {
        return adjustmentAmount;
    }

    public void setAdjustmentAmount(Integer adjustmentAmount) {
        this.adjustmentAmount = adjustmentAmount;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (entityId == null) {
            errors.add("entityId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }
        return errors;
    }

}
