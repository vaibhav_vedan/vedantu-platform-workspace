/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.EntityType;
import java.util.ArrayList;

/**
 *
 * @author ajith
 */
public class RegisteredCourse {

    private EntityType entityType;
    private String title;
    private Integer registrationFee;
    private Long boardId;
    private String description;
    private String subject;
    private String id;
    private Long registrationTime;

    public RegisteredCourse() {
    }

    public RegisteredCourse(StructuredCourseInfo courseInfo) {
        this.title = courseInfo.getTitle();
        this.registrationFee = courseInfo.getRegistrationFee();
        this.entityType = EntityType.OTO_COURSE_PLAN;
        this.boardId = courseInfo.getBoardId();
        this.description = courseInfo.getDescription();
        this.subject = courseInfo.getSubject();
        this.id = courseInfo.getId();
    }

    public RegisteredCourse(CourseInfo courseInfo) {
        this.title = courseInfo.getTitle();
        this.registrationFee = courseInfo.getRegistrationFee();
        this.entityType = EntityType.OTF_COURSE;
        this.description = courseInfo.getDescription();
//        if (ArrayUtils.isNotEmpty(courseInfo.getSubjects())) {
//            this.subject = (new ArrayList<>(courseInfo.getSubjects())).get(0);
//        }
        this.id = courseInfo.getId();
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(Integer registrationFee) {
        this.registrationFee = registrationFee;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(Long registrationTime) {
        this.registrationTime = registrationTime;
    }

}
