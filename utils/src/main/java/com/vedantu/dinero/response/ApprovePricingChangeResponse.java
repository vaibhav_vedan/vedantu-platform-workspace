/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

/**
 *
 * @author somil
 */
public class ApprovePricingChangeResponse {
    Long teacherId;
    Integer teacherMinPrice;
    Integer teacherOneHourRate;

    public ApprovePricingChangeResponse(Long teacherId, Integer teacherMinPrice, Integer teacherOneHourRate) {
        this.teacherId = teacherId;
        this.teacherMinPrice = teacherMinPrice;
        this.teacherOneHourRate = teacherOneHourRate;
    }
    
    

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getTeacherMinPrice() {
        return teacherMinPrice;
    }

    public void setTeacherMinPrice(Integer teacherMinPrice) {
        this.teacherMinPrice = teacherMinPrice;
    }

    public Integer getTeacherOneHourRate() {
        return teacherOneHourRate;
    }

    public void setTeacherOneHourRate(Integer teacherOneHourRate) {
        this.teacherOneHourRate = teacherOneHourRate;
    }
    
    
}
