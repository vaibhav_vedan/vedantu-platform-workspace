/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author ajith
 */
public class ContextWiseNextDueInstalmentsRes extends AbstractRes{
    private String contextId;
    private InstalmentInfo lastPaid;
    private InstalmentInfo nextDue;

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public InstalmentInfo getLastPaid() {
        return lastPaid;
    }

    public void setLastPaid(InstalmentInfo lastPaid) {
        this.lastPaid = lastPaid;
    }

    public InstalmentInfo getNextDue() {
        return nextDue;
    }

    public void setNextDue(InstalmentInfo nextDue) {
        this.nextDue = nextDue;
    }
    
}
