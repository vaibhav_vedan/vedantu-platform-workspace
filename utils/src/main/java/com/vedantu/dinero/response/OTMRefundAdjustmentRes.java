/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

/**
 *
 * @author parashar
 */
public class OTMRefundAdjustmentRes {
    
    private Integer leftOverAmount;
    private Integer extraRefundAmount;

    /**
     * @return the leftOverAmount
     */
    public Integer getLeftOverAmount() {
        return leftOverAmount;
    }

    /**
     * @param leftOverAmount the leftOverAmount to set
     */
    public void setLeftOverAmount(Integer leftOverAmount) {
        this.leftOverAmount = leftOverAmount;
    }

    /**
     * @return the extraRefundAmount
     */
    public Integer getExtraRefundAmount() {
        return extraRefundAmount;
    }

    /**
     * @param extraRefundAmount the extraRefundAmount to set
     */
    public void setExtraRefundAmount(Integer extraRefundAmount) {
        this.extraRefundAmount = extraRefundAmount;
    }
    
}
