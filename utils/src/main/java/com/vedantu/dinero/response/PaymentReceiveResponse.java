package com.vedantu.dinero.response;

import com.vedantu.dinero.pojo.ExtTransactionPojo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class PaymentReceiveResponse extends AbstractFrontEndReq {

    private OrderInfo orderInfo;
    private InstalmentInfo instalmentInfo;
    private ExtTransactionPojo transaction;

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public ExtTransactionPojo getTransaction() {
        return transaction;
    }

    public void setTransaction(ExtTransactionPojo transaction) {
        this.transaction = transaction;
    }

    public InstalmentInfo getInstalmentInfo() {
        return instalmentInfo;
    }

    public void setInstalmentInfo(InstalmentInfo instalmentInfo) {
        this.instalmentInfo = instalmentInfo;
    }

}
