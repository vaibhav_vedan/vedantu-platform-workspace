package com.vedantu.dinero.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentMessageStrip
{
    private String messageStrip;
    private String messageStripUrl;
}
