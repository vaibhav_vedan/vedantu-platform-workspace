/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.util.fos.response.AbstractRes;
import lombok.Data;

/**
 *
 * @author DPadhya
 */
@Data
public class UserDueInstalmentsResp extends AbstractRes{
    private String contextId;
    private String contextName;
    private Integer amount;
    private Long dueDate;


}
