/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

/**
 *
 * @author ajith
 */
public class InstalmentDues {

    private String orderId;
    private Long dueTime;

    public InstalmentDues() {
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    @Override
    public String toString() {
        return "InstalmentDues{" + "orderId=" + orderId + ", dueTime=" + dueTime + '}';
    }

}
