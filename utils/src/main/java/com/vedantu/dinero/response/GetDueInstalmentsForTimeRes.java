package com.vedantu.dinero.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;

public class GetDueInstalmentsForTimeRes {
	
	private String contextId;
	private InstalmentPurchaseEntity contextType;
	private String status;
	private String state;
	private Integer totalAmount;
	private Long dueTime;
	private Long studentId;
	private UserBasicInfo student;
	
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public InstalmentPurchaseEntity getContextType() {
		return contextType;
	}
	public void setContextType(InstalmentPurchaseEntity contextType) {
		this.contextType = contextType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Long getDueTime() {
		return dueTime;
	}
	public void setDueTime(Long dueTime) {
		this.dueTime = dueTime;
	}
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public UserBasicInfo getStudent() {
		return student;
	}
	public void setStudent(UserBasicInfo student) {
		this.student = student;
	}

}
