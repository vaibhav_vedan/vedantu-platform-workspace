/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

/**
 *
 * @author ajith This is almost a clone of Instalment.java. Block the important
 * params and send to user
 */
public class InstalmentInfo extends AbstractMongoStringIdEntityBean {

    private String orderId;
    private String deliverableEntityId;
    private Long dueTime;//in millis    
    private PaymentStatus paymentStatus;
    private Integer convenienceCharge;//in paisa
    private Integer securityCharge;//in paisa
    private Integer hoursCost;//in paisa
    private Integer totalAmount;//in paisa
    private Integer totalPromotionalAmount; //in paisa
    private Integer totalNonPromotionalAmount;//in paisa
    private SessionSchedule sessionSchedule;
    private String triggeredBy;
    private Long paidTime;//in millis    
    private Long hours;//in millis

    private String dueDate;
    private String paidDate;
    private String paymentStatusStr;
    private String contextId;
    private InstalmentPurchaseEntity contextType;
    private Long userId;//only in pojo
    private Integer teacherDiscountAmount;
    private Integer vedantuDiscountAmount;

    public InstalmentInfo() {
    }

    public InstalmentInfo(String orderId, Long dueTime,
            PaymentStatus paymentStatus, Integer convenienceCharge,
            Integer securityCharge, Integer hoursCost, Integer totalAmtToBePaid,
            Integer totalNonPromotionalAmount, Integer totalPromotionalAmount,
            SessionSchedule sessionSchedule, Long hours, String callingUserId,
            InstalmentPurchaseEntity contextType, String contextId) {
        super();
        this.orderId = orderId;
        this.dueTime = dueTime;
        this.paymentStatus = paymentStatus;
        this.convenienceCharge = convenienceCharge;
        this.securityCharge = securityCharge;
        this.hoursCost = hoursCost;
        this.totalAmount = totalAmtToBePaid;
        this.totalNonPromotionalAmount = totalNonPromotionalAmount;
        this.totalPromotionalAmount = totalPromotionalAmount;
        this.sessionSchedule = sessionSchedule;
        this.hours = hours;
        this.contextId = contextId;
        this.contextType = contextType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getConvenienceCharge() {
        return convenienceCharge;
    }

    public void setConvenienceCharge(Integer convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public Integer getSecurityCharge() {
        return securityCharge;
    }

    public void setSecurityCharge(Integer securityCharge) {
        this.securityCharge = securityCharge;
    }

    public Integer getHoursCost() {
        return hoursCost;
    }

    public void setHoursCost(Integer hoursCost) {
        this.hoursCost = hoursCost;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalPromotionalAmount() {
        return totalPromotionalAmount;
    }

    public void setTotalPromotionalAmount(Integer totalPromotionalAmount) {
        this.totalPromotionalAmount = totalPromotionalAmount;
    }

    public Integer getTotalNonPromotionalAmount() {
        return totalNonPromotionalAmount;
    }

    public void setTotalNonPromotionalAmount(Integer totalNonPromotionalAmount) {
        this.totalNonPromotionalAmount = totalNonPromotionalAmount;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public SessionSchedule getSessionSchedule() {
        return sessionSchedule;
    }

    public void setSessionSchedule(SessionSchedule sessionSchedule) {
        this.sessionSchedule = sessionSchedule;
    }

    public String getTriggeredBy() {
        return triggeredBy;
    }

    public void setTriggeredBy(String triggeredBy) {
        this.triggeredBy = triggeredBy;
    }

    public Long getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(Long paidTime) {
        this.paidTime = paidTime;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getPaymentStatusStr() {
        return paymentStatusStr;
    }

    public void setPaymentStatusStr(String paymentStatusStr) {
        this.paymentStatusStr = paymentStatusStr;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public InstalmentPurchaseEntity getContextType() {
        return contextType;
    }

    public void setContextType(InstalmentPurchaseEntity contextType) {
        this.contextType = contextType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Integer teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public Integer getVedantuDiscountAmount() {
        return vedantuDiscountAmount;
    }

    public void setVedantuDiscountAmount(Integer vedantuDiscountAmount) {
        this.vedantuDiscountAmount = vedantuDiscountAmount;
    }

}
