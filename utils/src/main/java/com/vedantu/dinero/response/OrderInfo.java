package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class OrderInfo extends RechargeUrlInfo {

    private String id;
    private Long userId;
    private List<OrderItemInfo> items = new ArrayList<>();
    private Integer amount;
    private Integer promotionalAmount;
    private Integer nonpromotionalAmount;
    private Integer amountPaid;//paid till now
    private PaymentStatus paymentStatus;
    private PaymentType paymentType;
    private Long creationTime;
    private Boolean needRecharge;
    private Long contextId;
    private Integer security;
    private Integer convenienceCharge;
    private List<PurchasingEntity> purchasingEntities;
    private List<OrderedItem> orderedItems = new ArrayList<>();
    private String purchaseContextId; // Bundle id from where it is coming
    private String campaign;


    public OrderInfo() {
        super();
    }

    public OrderInfo(String id, Long userId, Integer amount, Integer amountPaid, PaymentStatus paymentStatus,
            PaymentType paymentType, Long creationTime, Long contextId, Integer security,
            Integer convenienceCharge) {
        super();
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.paymentStatus = paymentStatus;
        this.creationTime = creationTime;
        this.contextId = contextId;
        this.security = security;
        this.convenienceCharge = convenienceCharge;
        this.paymentType = paymentType;
        this.amountPaid = amountPaid;
    }

}
