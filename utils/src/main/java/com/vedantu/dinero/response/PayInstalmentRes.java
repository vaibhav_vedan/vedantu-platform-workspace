/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.pojo.RechargeUrlInfo;

/**
 *
 * @author ajith
 */
public class PayInstalmentRes extends RechargeUrlInfo {

    private Integer amount;
    private Boolean needRecharge;
    private PaymentStatus paymentStatus;
    private InstalmentInfo instalmentInfo;

    public PayInstalmentRes() {
    }

    public PayInstalmentRes(Integer amount, Boolean needRecharge) {
        this.amount = amount;
        this.needRecharge = needRecharge;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Boolean getNeedRecharge() {
        return needRecharge;
    }

    public void setNeedRecharge(Boolean needRecharge) {
        this.needRecharge = needRecharge;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public InstalmentInfo getInstalmentInfo() {
        return instalmentInfo;
    }

    public void setInstalmentInfo(InstalmentInfo instalmentInfo) {
        this.instalmentInfo = instalmentInfo;
    }

    @Override
    public String toString() {
        return super.toString() +" PayInstalmentRes{" + "amount=" + amount + ", needRecharge=" + needRecharge + ", paymentStatus=" + paymentStatus + ", instalmentInfo=" + instalmentInfo + '}';
    }
    
}
