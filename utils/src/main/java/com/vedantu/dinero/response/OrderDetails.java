/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author ajith
 */
public class OrderDetails extends AbstractRes {

    private Boolean paid;
    private Integer amountPaid;
    private String orderId;
    private PaymentStatus paymentStatus;

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Integer getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Integer amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Override
    public String toString() {
        return "OrderDetails{" + "paid=" + paid + ", amountPaid=" + amountPaid + ", orderId=" + orderId + ", paymentStatus=" + paymentStatus + '}';
    }
}
