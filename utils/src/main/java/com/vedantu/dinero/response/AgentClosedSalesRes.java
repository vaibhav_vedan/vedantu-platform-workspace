package com.vedantu.dinero.response;

import java.util.List;

import com.vedantu.User.UserBasicInfo;

public class AgentClosedSalesRes {

	private String agentName;
	private List<Long> studentId;
	private List<UserBasicInfo> student;
	private Integer count = 0;
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public List<Long> getStudentId() {
		return studentId;
	}
	public void setStudentId(List<Long> studentId) {
		this.studentId = studentId;
	}
	public List<UserBasicInfo> getStudent() {
		return student;
	}
	public void setStudent(List<UserBasicInfo> student) {
		this.student = student;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
