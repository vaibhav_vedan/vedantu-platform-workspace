/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class DashboardUserInstalmentHistoryRes extends AbstractRes{
    private Long _id;
    private Long firstInstalmentPaidTime;
    private Integer firstInstalmentPaidAmount;
    private Long lastInstalmentTime;
    private Long firstExTransactionTime;

    public DashboardUserInstalmentHistoryRes() {
    }
    
    public Long getFirstInstalmentPaidTime() {
        return firstInstalmentPaidTime;
    }

    public void setFirstInstalmentPaidTime(Long firstInstalmentPaidTime) {
        this.firstInstalmentPaidTime = firstInstalmentPaidTime;
    }

    public Integer getFirstInstalmentPaidAmount() {
        return firstInstalmentPaidAmount;
    }

    public void setFirstInstalmentPaidAmount(Integer firstInstalmentPaidAmount) {
        this.firstInstalmentPaidAmount = firstInstalmentPaidAmount;
    }

    public Long getLastInstalmentTime() {
        return lastInstalmentTime;
    }

    public void setLastInstalmentTime(Long lastInstalmentTime) {
        this.lastInstalmentTime = lastInstalmentTime;
    }

    public Long getFirstExTransactionTime() {
        return firstExTransactionTime;
    }

    public void setFirstExTransactionTime(Long firstExTransactionTime) {
        this.firstExTransactionTime = firstExTransactionTime;
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long _id) {
        this._id = _id;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String FIRST_INSTALMENT_PAID_TIME = "firstInstalmentPaidTime";
        public static final String FIRST_EXT_TRANSACTION_TIME = "firstExTransactionTime";
        public static final String LAST_INSTALMENT_TIME = "lastInstalmentTime";
        public static final String FIRST_INSTALMENT_PAID_AMOUNT = "firstInstalmentPaidAmount";
    }
    
}
