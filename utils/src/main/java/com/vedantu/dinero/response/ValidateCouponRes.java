package com.vedantu.dinero.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 * Created by somil on 02/06/17.
 */
public class ValidateCouponRes extends AbstractRes {

    private Boolean applicable;
    private Integer redeemValue;
    private Integer discount;
    private Integer maxDiscount;
    private String couponId;
    private Long targetUserId;
    private Long passDurationInMillis;


    public ValidateCouponRes() {
        super();
    }

    public ValidateCouponRes(Boolean applicable,
                             Integer redeemValue, Integer discount, Integer maxDiscount,
                             String couponId, Long targetUserId) {
        this.applicable = applicable;

        this.redeemValue = redeemValue;
        this.discount = discount;
        this.maxDiscount = maxDiscount;
        this.couponId = couponId;

        this.targetUserId = targetUserId;

    }

    public Long getPassDurationInMillis() {
        return passDurationInMillis;
    }

    public void setPassDurationInMillis(Long passDurationInMillis) {
        this.passDurationInMillis = passDurationInMillis;
    }



    public Integer getRedeemValue() {
        return redeemValue;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public Boolean getApplicable() {
        return applicable;
    }

    public void setApplicable(Boolean applicable) {
        this.applicable = applicable;
    }



    public void setRedeemValue(Integer redeemValue) {
        this.redeemValue = redeemValue;
    }

    @Override
    public String toString() {
        return "ValidateCouponRes{" + "applicable=" + applicable + ",  redeemValue=" + redeemValue + ", discount=" + discount + ", maxDiscount=" + maxDiscount + ", couponId=" + couponId + ", targetUserId=" + targetUserId + " + }";
    }
}
