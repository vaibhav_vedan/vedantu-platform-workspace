/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class CounselorDashboardInstalmentInfoRes extends AbstractRes{
    private String contextId; // 
    private InstalmentPurchaseEntity contextType; //
    private String contextTitle; //
    private Long contextStartTime; //
    private String status; //
    private String state; //
    private Integer totalAmount; //
    private Long dueTime; //
    private UserBasicInfo student; //
    private UserBasicInfo teacher; //
    private Long firstInstalmentPaidTime; //
    private Integer firstInstalmentPaidAmount; //
    private Long lastInstalmentTime; //
    private Long firstExTransactionTime; //
    private Long lastSessionEndTime; //
    private Integer totalNoOfSessions; //

    public CounselorDashboardInstalmentInfoRes() {
    }

    public Long getFirstInstalmentPaidTime() {
        return firstInstalmentPaidTime;
    }

    public void setFirstInstalmentPaidTime(Long firstInstalmentPaidTime) {
        this.firstInstalmentPaidTime = firstInstalmentPaidTime;
    }

    public Integer getFirstInstalmentPaidAmount() {
        return firstInstalmentPaidAmount;
    }

    public void setFirstInstalmentPaidAmount(Integer firstInstalmentPaidAmount) {
        this.firstInstalmentPaidAmount = firstInstalmentPaidAmount;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public InstalmentPurchaseEntity getContextType() {
        return contextType;
    }

    public void setContextType(InstalmentPurchaseEntity contextType) {
        this.contextType = contextType;
    }

    public String getContextTitle() {
        return contextTitle;
    }

    public void setContextTitle(String contextTitle) {
        this.contextTitle = contextTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    public UserBasicInfo getStudent() {
        return student;
    }

    public void setStudent(UserBasicInfo student) {
        this.student = student;
    }

    public UserBasicInfo getTeacher() {
        return teacher;
    }

    public void setTeacher(UserBasicInfo teacher) {
        this.teacher = teacher;
    }

    public Long getLastInstalmentTime() {
        return lastInstalmentTime;
    }

    public void setLastInstalmentTime(Long lastInstalmentTime) {
        this.lastInstalmentTime = lastInstalmentTime;
    }

    public Long getFirstExTransactionTime() {
        return firstExTransactionTime;
    }

    public void setFirstExTransactionTime(Long firstExTransactionTime) {
        this.firstExTransactionTime = firstExTransactionTime;
    }

    public Long getContextStartTime() {
        return contextStartTime;
    }

    public void setContextStartTime(Long contextStartTime) {
        this.contextStartTime = contextStartTime;
    }

    public Long getLastSessionEndTime() {
        return lastSessionEndTime;
    }

    public void setLastSessionEndTime(Long lastSessionEndTime) {
        this.lastSessionEndTime = lastSessionEndTime;
    }

    public Integer getTotalNoOfSessions() {
        return totalNoOfSessions;
    }

    public void setTotalNoOfSessions(Integer totalNoOfSessions) {
        this.totalNoOfSessions = totalNoOfSessions;
    }
     
}
