package com.vedantu.dinero.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.pojo.Orders;

public class GetOrderUserInfoRes extends Orders{
	
	private UserBasicInfo user;

	public UserBasicInfo getUser() {
		return user;
	}

	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

}
