/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class GetUserDashboardAccountInfoRes extends AbstractRes{
    private Long totalAmountPaid;
    private Long firstTransactionTime;
    private Long lastTransactionTime;
    private Integer balance;
    private Integer lockedBalance;
    private Integer promotionalBalance;
    private Integer nonPromotionalBalance;

    public Long getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(Long totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public Long getFirstTransactionTime() {
        return firstTransactionTime;
    }

    public void setFirstTransactionTime(Long firstTransactionTime) {
        this.firstTransactionTime = firstTransactionTime;
    }

    public Long getLastTransactionTime() {
        return lastTransactionTime;
    }

    public void setLastTransactionTime(Long lastTransactionTime) {
        this.lastTransactionTime = lastTransactionTime;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getLockedBalance() {
        return lockedBalance;
    }

    public void setLockedBalance(Integer lockedBalance) {
        this.lockedBalance = lockedBalance;
    }

    public Integer getPromotionalBalance() {
        return promotionalBalance;
    }

    public void setPromotionalBalance(Integer promotionalBalance) {
        this.promotionalBalance = promotionalBalance;
    }

    public Integer getNonPromotionalBalance() {
        return nonPromotionalBalance;
    }

    public void setNonPromotionalBalance(Integer nonPromotionalBalance) {
        this.nonPromotionalBalance = nonPromotionalBalance;
    }
    
    public static class Constants {

        public static final String TOTAL_AMOUNT_PAID = "totalAmountPaid";
        public static final String FIRST_TRANSACTION_TIME = "firstTransactionTime";
        public static final String LAST_TRANSACTION_TIME = "lastTransactionTime";
        
    }
    
}