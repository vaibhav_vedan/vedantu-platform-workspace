package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import java.util.List;

/**
 * Created by somil on 05/06/17.
 */
public class RedeemCouponInfo extends AbstractMongoStringIdEntityBean {

    private String code;
    private Long userId;
    private Integer redeemValue;

    // Redeem on entity
    private String entityId; //changing it to string as the OrderedItem has entityId as string
    //and since it is being used at many places, wouldn't want to change that
    private EntityType entityType;

    private RedeemedCouponState state = RedeemedCouponState.UNUSED;

    private PurchaseFlowType purchaseFlowType;//useful in cases of locking the coupon

    private String purchaseFlowId;//each flow has to be unique, user should not add multiple coupons to same flowId

    private Long passExpirationTime;
    private PassProcessingState passProcessingState;
    private List<ReferenceTag> referenceTags;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getRedeemValue() {
        return redeemValue;
    }

    public void setRedeemValue(Integer redeemValue) {
        this.redeemValue = redeemValue;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public RedeemedCouponState getState() {
        return state;
    }

    public void setState(RedeemedCouponState state) {
        this.state = state;
    }

    public PurchaseFlowType getPurchaseFlowType() {
        return purchaseFlowType;
    }

    public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
        this.purchaseFlowType = purchaseFlowType;
    }

    public String getPurchaseFlowId() {
        return purchaseFlowId;
    }

    public void setPurchaseFlowId(String purchaseFlowId) {
        this.purchaseFlowId = purchaseFlowId;
    }

    public Long getPassExpirationTime() {
        return passExpirationTime;
    }

    public void setPassExpirationTime(Long passExpirationTime) {
        this.passExpirationTime = passExpirationTime;
    }

    public PassProcessingState getPassProcessingState() {
        return passProcessingState;
    }

    public void setPassProcessingState(PassProcessingState passProcessingState) {
        this.passProcessingState = passProcessingState;
    }

    public List<ReferenceTag> getReferenceTags() {
        return referenceTags;
    }

    public void setReferenceTags(List<ReferenceTag> referenceTags) {
        this.referenceTags = referenceTags;
    }
}
