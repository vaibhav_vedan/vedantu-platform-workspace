package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.SubscriptionBasicInfo;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.interfaces.IItemDetailInfo;

public class OrderItemInfo implements IItemDetailInfo {

	private String name;
	private String entityId;
	private EntityType entityType;
	private Integer count;
	private Integer rate;
	private Integer cost;
	private Integer duration;
	private PaymentType paymentType;
	private SessionModel model;
	private SubModel subModel;
	private String target;
	private Integer grade;
	private String note;

	private SubscriptionBasicInfo extraInfo;

	public OrderItemInfo(OrderedItem item) {
		super();
		this.entityId = item.getEntityId();
		this.entityType = item.getEntityType();
		this.cost = item.getCost();
		this.count = item.getCount();
		this.rate = item.getRate();
		this.model = item.getModel();
		this.subModel = item.getSubModel();
		this.target = item.getTarget();
		this.grade = item.getGrade();
		this.note = item.getNote();
	}

	public SessionModel getModel() {
		return model;
	}

	public OrderItemInfo(String name, String entityId, EntityType entityType, Integer count, Integer rate, Integer cost,
			Integer duration, PaymentType paymentType, SessionModel model, SubModel subModel, String target,
			Integer grade, String note, SubscriptionBasicInfo extraInfo) {
		super();
		this.name = name;
		this.entityId = entityId;
		this.entityType = entityType;
		this.count = count;
		this.rate = rate;
		this.cost = cost;
		this.duration = duration;
		this.paymentType = paymentType;
		this.model = model;
		this.subModel = subModel;
		this.target = target;
		this.grade = grade;
		this.note = note;
		this.extraInfo = extraInfo;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public SubscriptionBasicInfo getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(SubscriptionBasicInfo extraInfo) {
		this.extraInfo = extraInfo;
	}

	
	@Override
	public String toString() {
		return "OrderItemInfo [name=" + name + ", entityId=" + entityId + ", entityType=" + entityType + ", count="
				+ count + ", rate=" + rate + ", cost=" + cost + ", duration=" + duration + ", paymentType="
				+ paymentType + ", model=" + model + ", subModel=" + subModel + ", target=" + target + ", grade="
				+ grade + ", note=" + note + ", extraInfo=" + extraInfo + "]";
	}

	public OrderItemInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

}
