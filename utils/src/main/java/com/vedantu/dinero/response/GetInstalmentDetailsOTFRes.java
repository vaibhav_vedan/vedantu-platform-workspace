package com.vedantu.dinero.response;

import java.util.List;

/**
 * Created by somil on 29/03/17.
 */
public class GetInstalmentDetailsOTFRes {
    private List<InstalmentInfo> instalments;
    private Integer registrationAmount;
    private Long creationTime;

    public List<InstalmentInfo> getInstalments() {
        return instalments;
    }

    public void setInstalments(List<InstalmentInfo> instalments) {
        this.instalments = instalments;
    }

    public Integer getRegistrationAmount() {
        return registrationAmount;
    }

    public void setRegistrationAmount(Integer registrationAmount) {
        this.registrationAmount = registrationAmount;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }
}
