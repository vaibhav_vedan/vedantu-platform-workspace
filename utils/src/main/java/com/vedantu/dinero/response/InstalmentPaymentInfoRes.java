package com.vedantu.dinero.response;

/**
 * Created by somil on 23/05/17.
 */
public class InstalmentPaymentInfoRes {
    private Integer noOfPaid;
    private Long dueDate;
    private Integer amount;
    private Long lastInstalmentDate;

    public Long getLastInstalmentDate() {
        return lastInstalmentDate;
    }

    public void setLastInstalmentDate(Long lastInstalmentDate) {
        this.lastInstalmentDate = lastInstalmentDate;
    }

    public Integer getNoOfPaid() {
        return noOfPaid;
    }

    public void setNoOfPaid(Integer noOfPaid) {
        this.noOfPaid = noOfPaid;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    
    @Override
    public String toString() {
        return "InstalmentPaymentInfoRes{" +
                "noOfPaid=" + noOfPaid +
                ", dueDate=" + dueDate +
                ", amount=" + amount +
                ", lastInstalmentDate=" + lastInstalmentDate +
                '}';
    }
}
