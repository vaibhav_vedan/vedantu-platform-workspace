package com.vedantu.dinero.response;

import com.vedantu.dinero.pojo.LockBalanceInfo;
import com.vedantu.exception.ErrorCode;

public class LockBalanceRes extends LockBalanceInfo {
	private ErrorCode errorCode;
	private Long amountToBeLocked;
	private Long accountBalance;
	private Long teacherDiscountAmount;

	public Long getTeacherDiscountAmount() {
		return teacherDiscountAmount;
	}

	public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
		this.teacherDiscountAmount = teacherDiscountAmount;
	}

	public LockBalanceRes() {
		super();
	}

	public LockBalanceRes(Long studentId) {
		super(studentId);
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Long getAmountToBeLocked() {
		return amountToBeLocked;
	}

	public void setAmountToBeLocked(Long amountToBeLocked) {
		this.amountToBeLocked = amountToBeLocked;
	}

	public Long getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Long accountBalance) {
		this.accountBalance = accountBalance;
	}


	@Override
	public String toString() {
		return "LockBalanceRes [errorCode=" + errorCode + ", amountToBeLocked=" + amountToBeLocked + ", accountBalance="
				+ accountBalance + ", teacherDiscountAmount=" + teacherDiscountAmount + "]";
	}
}
