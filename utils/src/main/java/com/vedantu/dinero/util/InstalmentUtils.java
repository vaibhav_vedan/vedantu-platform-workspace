/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.InstalmentMap;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.scheduling.SchedulingUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class InstalmentUtils {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalmentUtils.class);

    public ConflictDetails fillResolvableConflictDetails(SessionSchedule schedule) {
        logger.info("ENTRY " + schedule);
        //this is the schedule data present in subscriptionrequest which contains
        //the sessionschedule with stale sessionslots and latest slotBitSet[] and conflictBitSet[]
        ConflictDetails conflictDetails = null;
        Long conflictsTill = null;
        Long resolutionEndTime = null;
        if (schedule.getSlotBitSet() == null || schedule.getConflictSlotBitSet() == null) {
            return null;
        }
        List<SessionSlot> conflictSessions = SchedulingUtils.createSessionSlots(schedule.getConflictSlotBitSet(),
                schedule.getStartTime());
        try {
            List<InstalmentInfo> instalments = prepareInstalmentSchedules(schedule);
            logger.info("Instalments " + instalments);
            if (ArrayUtils.isNotEmpty(instalments)) {
                SessionSchedule _schedule = instalments.get(0).getSessionSchedule();
                conflictsTill = _schedule.getEndDate();
                resolutionEndTime = conflictsTill;
                if (instalments.size() > 1) {
                    resolutionEndTime = instalments.get(1).getDueTime();
                }
            }
        } catch (Exception e) {
            logger.info("EXCEPTION " + e.getMessage());
        }
        if (conflictsTill != null) {
            logger.info("Returning the conflict sessions until " + conflictsTill);
            Collections.sort(conflictSessions, new Comparator<SessionSlot>() {
                @Override
                public int compare(SessionSlot o1, SessionSlot o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
            if (ArrayUtils.isNotEmpty(conflictSessions)) {
                int count = 0;
                for (SessionSlot _slot : conflictSessions) {
                    if (_slot.getStartTime() > conflictsTill) {
                        break;
                    }
                    count++;
                }
                logger.info("sublisting form 0 to " + count + " (exclusive)");
                conflictDetails = new ConflictDetails(conflictSessions.subList(0, count), schedule.getStartDate(),
                        resolutionEndTime);
            }
        }
        logger.info("EXIT " + conflictDetails);
        return conflictDetails;
    }

    public List<InstalmentInfo> prepareInstalmentSchedules(SessionSchedule sessionSchedule)
            throws BadRequestException, InternalServerErrorException, NotFoundException {
        //this sessionSchedule contains sessionslots made out of the latest slotBitSet[]
        logger.info("ENTRY: " + sessionSchedule);
        List<InstalmentInfo> instalments = new ArrayList<>();
        if (sessionSchedule != null) {
            Map<Long, InstalmentMap> map = getInstalmentMap(sessionSchedule, null, null, null);
            logger.info("Instalmentmap: " + map);

            SortedSet<Long> keys = new TreeSet<>(map.keySet());
            for (Long key : keys) {
                InstalmentMap instMap = map.get(key);
                Long dueTime = key;
                SessionSchedule _scheduleEntry = instMap.sessionSchedule;
                _scheduleEntry.setStartTime(CommonCalendarUtils.getDayStartTime(_scheduleEntry.getStartDate()));//setting to day start time for easy reference
                _scheduleEntry.setEndTime(_scheduleEntry.getEndDate());
                InstalmentInfo instalment = new InstalmentInfo(null, dueTime, PaymentStatus.NOT_PAID,
                        0, 0, instMap.hoursCost, 0,
                        0, 0, _scheduleEntry, instMap.hours, null, null, null);
                instalments.add(instalment);
            }
        }
        logger.info("EXIT: " + instalments);
        return instalments;
    }

    public Map<Long, InstalmentMap> getInstalmentMap(SessionSchedule sessionSchedule, Integer hourlyRate,
            Integer vedantuDiscountPerHour, Integer teacherDiscountPerHour) {
        if (vedantuDiscountPerHour == null) {
            vedantuDiscountPerHour = 0;
        }
        if (teacherDiscountPerHour == null) {
            teacherDiscountPerHour = 0;
        }
        List<SessionSlot> sessionSlots;
        Map<Long, InstalmentMap> map = new HashMap<>();//map of due time vs total cost for that month
        Long sessionScheduleStartTime = null;
        if (sessionSchedule.getSlotBitSet() != null && sessionSchedule.getSlotBitSet().length > 0) {
            logger.info("Using slotbitset to create session slots");
            sessionSlots = SchedulingUtils.createSessionSlots(sessionSchedule.getSlotBitSet(),
                    sessionSchedule.getStartTime());
            if (sessionSchedule.getSessionSlots() != null && sessionSchedule.getSessionSlots().size() > 0) {
                sessionScheduleStartTime = sessionSchedule.getSessionSlots().get(0).getStartTime();
            }
        } else {
            logger.info("Using sessionSchedule.getSessionSlots()");
            sessionSlots = sessionSchedule.getSessionSlots();
            //making sure slots are in order
            Collections.sort(sessionSlots, (SessionSlot o1, SessionSlot o2) -> o1.getStartTime().compareTo(o2.getStartTime()));
            sessionSlots = addSessionsForAllWeeks(sessionSlots,
                    sessionSchedule.getNoOfWeeks());
        }

        if (sessionSlots != null && sessionSlots.size() > 0) {
            if (hourlyRate == null) {
                hourlyRate = 0;
            }
            //deducting discount
            hourlyRate -= (vedantuDiscountPerHour + teacherDiscountPerHour);

            Long firstSlotStartTime = sessionSlots.get(0).getStartTime();
            logger.info("firstSlotStartTime " + firstSlotStartTime);
            Long thisInstStartTime = firstSlotStartTime;
            if (sessionScheduleStartTime != null) {
                thisInstStartTime = sessionScheduleStartTime;
            }
            logger.info("thisInstStartTime " + thisInstStartTime);
            Long nextInstStartTime = getNextInstStartTime(thisInstStartTime);
            logger.info("nextInstStartTime " + nextInstStartTime);

            //for cases where the resolve conflicts led to preponing the first session before the initial 
            //first session time, this case instalmentduration will be >32 days
            if (thisInstStartTime > firstSlotStartTime) {
                thisInstStartTime = firstSlotStartTime;
            }
            logger.info("final thisInstStartTime " + thisInstStartTime);

            for (int i = 0, s = sessionSlots.size(); i < s; i++) {
                SessionSlot slot = sessionSlots.get(i);
                if (slot.getStartTime() < nextInstStartTime) {
                    if (map.get(thisInstStartTime) == null) {
                        SessionSchedule _schedule = new SessionSchedule();
                        _schedule.setSessionSlots(new ArrayList<>());
                        _schedule.setNoOfWeeks(1l);
                        map.put(thisInstStartTime, new InstalmentMap(0, _schedule, 0l, 0, 0));
                    }
                    float hrs = (float) (slot.getEndTime() - slot.getStartTime()) / DateTimeUtils.MILLIS_PER_HOUR;
                    int cost = (int) (hourlyRate * hrs);
                    int vedantuDiscountAmount = (int) (vedantuDiscountPerHour * hrs);
                    int teacherDiscountAmount = (int) (teacherDiscountPerHour * hrs);
                    InstalmentMap instMap = map.get(thisInstStartTime);
                    instMap.hoursCost += cost;
                    instMap.sessionSchedule.getSessionSlots().add(slot);
                    instMap.hours += (slot.getEndTime() - slot.getStartTime());
                    instMap.vedantuDiscountAmount += vedantuDiscountAmount;
                    instMap.teacherDiscountAmount += teacherDiscountAmount;
                    if (i < (s - 1)) {
                        SessionSlot nextSlot = sessionSlots.get(i + 1);
                        if (nextSlot.getStartTime() >= nextInstStartTime) {
                            thisInstStartTime = nextInstStartTime;
                            nextInstStartTime = getNextInstStartTime(thisInstStartTime);
                        }
                    }
                } else {
                    logger.warn("startTime is greater than the next start time " + sessionSlots);
                }
            }
        }
        return map;
    }

    public static class ConflictDetails {

        public List<SessionSlot> conflictSessions;
        public Long resolutionStartTime;
        public Long resolutionEndTime;

        public ConflictDetails(List<SessionSlot> conflictSessions, Long resolutionStartTime, Long resolutionEndTime) {
            this.conflictSessions = conflictSessions;
            this.resolutionStartTime = resolutionStartTime;
            this.resolutionEndTime = resolutionEndTime;
        }
    }

    public List<SessionSlot> addSessionsForAllWeeks(List<SessionSlot> sessionSlots, Long noOfWeeks) {
        if (noOfWeeks == null || noOfWeeks < 2) {
            return sessionSlots;
        } else if (ArrayUtils.isEmpty(sessionSlots)) {
            return new ArrayList<>();
        }
        List<SessionSlot> newslots = new ArrayList<>();
        for (Long i = 0l; i < noOfWeeks; ++i) {
            for (SessionSlot s : sessionSlots) {
                SessionSlot newSlot = new SessionSlot(s);
                newSlot.setStartTime(s.getStartTime() + i * DateTimeUtils.MILLIS_PER_WEEK);
                newSlot.setEndTime(s.getEndTime() + i * DateTimeUtils.MILLIS_PER_WEEK);
                newslots.add(newSlot);
            }
        }
        return newslots;
    }

    public Long getNextInstStartTime(Long prevInstStartTime) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(prevInstStartTime));
        int prevInstDate = calendar.get(Calendar.DAY_OF_MONTH);
        int nextInstDate = prevInstDate;
        int nextInstMonth = calendar.get(Calendar.MONTH) + 1;//next month
        int nextInstYear = calendar.get(Calendar.YEAR);
        if (nextInstMonth > 11) {
            nextInstMonth = 0;
            nextInstYear++;
        }
        if (nextInstDate > 28) {
            nextInstDate = 1;
            nextInstMonth++;
            if (nextInstMonth > 11) {
                nextInstMonth = 0;
                nextInstYear++;
            }
        }
        calendar.clear();
        calendar.set(Calendar.DATE, nextInstDate);
        calendar.set(Calendar.MONTH, nextInstMonth);
        calendar.set(Calendar.YEAR, nextInstYear);
        return calendar.getTimeInMillis();
    }

    public static void main(String[] args) {
        InstalmentUtils instalmentUtils = new InstalmentUtils();
        SessionSchedule sessionSchedule = new SessionSchedule();
        sessionSchedule.setStartTime(1484265600000l);
        List<SessionSlot> slots = new ArrayList<>();
        sessionSchedule.setSessionSlots(slots);
//        sessionSchedule.setNoOfWeeks(40l);
        long[] slotBitSet = {0l, 0l, 0l, 0l, 1006632960l, 4323455642275676160l, 0l, 1006632960l, 4323455642275676160l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 245760l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 1055531162664960l};
//        slotBitSet = null;
        long[] conflictBitSet = {};
        sessionSchedule.setSlotBitSet(slotBitSet);
//        sessionSchedule.setConflictSlotBitSet(conflictBitSet);
        Map<Long, InstalmentMap> map = instalmentUtils.getInstalmentMap(sessionSchedule, 100, null, null);
        SortedSet<Long> keys = new TreeSet<>(map.keySet());
        Long dueTime = keys.first();
        System.out.println("duw time " + dueTime);
        InstalmentMap instmap = map.get(dueTime);
        System.out.println("com.vedantu.dinero.util.InstalmentUtils.main() " + (instmap.hours / 3600000));
        SessionSchedule schedule = instmap.sessionSchedule;
        schedule.setStartTime(CommonCalendarUtils.getDayStartTime(schedule.getStartDate()));//setting to day start time for easy reference
        schedule.setEndTime(schedule.getEndDate());
        List<SessionSlot> slts = schedule.getSessionSlots();
        for (SessionSlot slot : slts) {
            System.out.println(">> " + new Date(slot.getStartTime()) + " to " + new Date(slot.getEndTime()));
            System.out.println(">> " + ((slot.endTime - slot.startTime) / 3600000));
        }
    }

    public static List<BaseInstalmentInfo> getBaseInstalmentsFromRequest(String requestBody) {
        Gson gson = new Gson();
        List<BaseInstalmentInfo> baseInstalments = null;
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(requestBody);
        if (jsonElement != null) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            JsonElement instalmentDetailsElement = jsonObject.get("instalmentDetails");
            if (instalmentDetailsElement != null) {
                String instalmentDetailsString = gson.toJson(instalmentDetailsElement);
                if (com.vedantu.util.StringUtils.isNotEmpty(instalmentDetailsString)) {
                    Type listType = new TypeToken<List<BaseInstalmentInfo>>() {
                    }.getType();
                    baseInstalments = gson.fromJson(instalmentDetailsString, listType);
                }
            }
        }
        return baseInstalments;
    }
}
