package com.vedantu.dinero.pojo;

import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;

public class PaymentInvoiceUserInfo extends UserBasicInfo {

    private LocationInfo locationInfo;
    private String panCard;

    public PaymentInvoiceUserInfo() {
        super();
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public String getPanCard() {
        return panCard;
    }

    public void setPanCard(String panCard) {
        this.panCard = panCard;
    }

    public void updateAdditionalDetails(User user, String panCard) {
        this.locationInfo = user.getLocationInfo();
        if (Role.TEACHER.equals(user.getRole())) {
            this.panCard = panCard;
        }
    }

    @Override
    public String toString() {
        return "PaymentInvoiceUserInfo{" + "locationInfo="
                + locationInfo + ", panCard=" + panCard + '}' + super.toString();
    }
}
