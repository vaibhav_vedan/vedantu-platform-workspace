/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

import com.vedantu.session.pojo.EntityType;

/**
 *
 * @author ajith
 */
public class PurchasingEntity {

    private EntityType entityType;
    private String entityId;
    private String deliverableId;
    private Integer price; //everytime it has to be set

    public PurchasingEntity() {
    }

    public PurchasingEntity(EntityType entityType, String entityId) {
        this.entityType = entityType;
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getDeliverableId() {
        return deliverableId;
    }

    public void setDeliverableId(String deliverableId) {
        this.deliverableId = deliverableId;
    }

    public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
    public String toString() {
        return "PurchasingEntity{" + "entityType=" + entityType + ", entityId=" + entityId + ", deliverableId=" + deliverableId + '}';
    }

}
