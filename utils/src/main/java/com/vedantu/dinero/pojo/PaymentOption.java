package com.vedantu.dinero.pojo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.payment.CardPaymentMethod;
import com.vedantu.dinero.enums.payment.NetbankingPaymentMethod;
import com.vedantu.dinero.enums.payment.PaymentMethodType;
import com.vedantu.dinero.enums.payment.WalletPaymentMethod;
import com.vedantu.util.security.DevicesAllowed;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class PaymentOption {
    private PaymentGatewayName optedGateway;
    private DevicesAllowed device;
    private PaymentInstrument paymentInstrument;
    private String emiId;


    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.EXISTING_PROPERTY,
            property = "optedPaymentMethodType", visible = true)
    @JsonSubTypes({
            @JsonSubTypes.Type(value = Card.class, name = "CARD"),
            @JsonSubTypes.Type(value = NetBanking.class, name = "NETBANKING"),
            @JsonSubTypes.Type(value = Wallet.class, name = "WALLET"),
            @JsonSubTypes.Type(value = Wallet.class, name = "WALLET_DIRECT_DEBIT"),
    })
    public interface PaymentInstrument {
        List<String> getErrors();
        PaymentMethodType getOptedPaymentMethodType();
    }

    @Data
    public static class Card implements PaymentInstrument {

        private Long cardNumber;
        private Long cardExpiryMonth;
        private Long cardExpiryYear;
        private String nameOnCard;
        private Long cardSecurityCode;
        private boolean saveToLocker;
        private String cardToken;
        private final String paymentMethodType = "CARD";
        private CardPaymentMethod paymentMethod;
        private PaymentMethodType optedPaymentMethodType;

        @Override
        public List<String> getErrors() {
            List<String> errors = new ArrayList<>();
            if (cardSecurityCode == null) {
                errors.add("Card security code(CVV) is empty.");
            }

            if (StringUtils.isEmpty(cardToken) && (cardNumber == null || cardExpiryMonth == null || cardExpiryYear == null || StringUtils.isEmpty(nameOnCard))) {
                errors.add("Card details invalid. Please check all inputs");
            }
            return errors;
        }
    }

    @Data
    public static class NetBanking implements PaymentInstrument {
        private final String paymentMethodType = "NB";
        private NetbankingPaymentMethod paymentMethod;
        private PaymentMethodType optedPaymentMethodType;

        @Override
        public List<String> getErrors() {
            if (paymentMethod == null) {
                return Collections.singletonList("Netbanking Code is invalid.");
            }
            return new ArrayList<>();
        }
    }

    @Data
    public static class Wallet implements PaymentInstrument {
        private final String paymentMethodType = "WALLET";
        private WalletPaymentMethod paymentMethod;
        private String directDebitToken;
        private PaymentMethodType optedPaymentMethodType;

        @Override
        public List<String> getErrors() {
            if (paymentMethod == null) {
                return Collections.singletonList("Wallet Code is invalid");
            }
            return new ArrayList<>();
        }
    }
}
