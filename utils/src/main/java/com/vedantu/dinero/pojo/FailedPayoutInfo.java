package com.vedantu.dinero.pojo;

import com.vedantu.session.pojo.EntityType;

/**
 * Created by somil on 11/07/17.
 */
public class FailedPayoutInfo {

    private Long sessionId;
    private Boolean payoutFound;
    private Integer billingDuration;
    private EntityType contextType;


    public FailedPayoutInfo() {
        super();
    }


    public FailedPayoutInfo(Long sessionId, Boolean payoutFound, Integer billingDuration) {
        this.sessionId = sessionId;
        this.payoutFound = payoutFound;
        this.billingDuration = billingDuration;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Boolean getPayoutFound() {
        return payoutFound;
    }

    public void setPayoutFound(Boolean payoutFound) {
        this.payoutFound = payoutFound;
    }

    public Integer getBillingDuration() {
        return billingDuration;
    }

    public void setBillingDuration(Integer billingDuration) {
        this.billingDuration = billingDuration;
    }

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    @Override
    public String toString() {
        return "FailedPayoutInfo{" +
                "sessionId=" + sessionId +
                ", payoutFound=" + payoutFound +
                ", billingDuration=" + billingDuration +
                ", contextType=" + contextType +
                '}';
    }
}
