package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import org.springframework.data.mongodb.core.index.Indexed;

public class OrderedItem {

    private String entityId;
    private EntityType entityType;
    private Integer count;
    private Integer rate;
    private Integer cost;
    private Integer promotionalCost;
    private Integer nonPromotionalCost;
    private Long hours;
    private SessionModel model;
    private SubModel subModel;
    private String target;
    private Integer grade;
    private Long boardId;
    private String schedule;
    private Long teacherId;
    private Long offeringId;
    private Boolean renew;
    private String teacherDiscountCouponId;
    private Integer teacherDiscountAmount;
    private String vedantuDiscountCouponId;
    private Integer vedantuDiscountAmount;
    private Integer vedantuDiscountAmountClaimed;
    private Integer teacherDiscountAmountClaimed;
    private String note;
    private DeliverableEntityType deliverableEntityType;
    @Indexed(background = true)
    private String deliverableEntityId;
    private SessionSource sessionSource;
    private RefundPolicy refundStatus;

    public OrderedItem() {
        super();
    }

    public OrderedItem(String entityId, EntityType entityType, Integer count,
            Integer rate, Integer cost, Integer promotionalCost, Integer nonPromotionalCost, Long hours, SessionModel model,
            String target, Integer grade, Long boardId,
            String schedule, Long teacherId, Long offeringId,
            Boolean renew, String note, String teacherDiscountCouponId,
            Integer teacherDiscountAmount, String vedantuDiscountCouponId,
            Integer vedantuDiscountAmount, SubModel subModel, SessionSource sessionSource) {
        super();
        this.entityId = entityId;
        this.entityType = entityType;
        this.count = count;
        this.rate = rate;
        this.cost = cost;
        this.promotionalCost = promotionalCost;
        this.nonPromotionalCost = nonPromotionalCost;
        this.hours = hours;
        this.model = model;
        this.target = target;
        this.grade = grade;
        this.boardId = boardId;
        this.schedule = schedule;
        this.teacherId = teacherId;
        this.offeringId = offeringId;
        this.renew = renew;
        this.note = note;
        this.teacherDiscountCouponId = teacherDiscountCouponId;
        this.teacherDiscountAmount = teacherDiscountAmount;
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
        this.vedantuDiscountAmount = vedantuDiscountAmount;
        this.subModel = subModel;
        this.sessionSource = sessionSource;
    }

    public SubModel getSubModel() {
        return subModel;
    }

    public void setSubModel(SubModel subModel) {
        this.subModel = subModel;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public Boolean getRenew() {
        return renew;
    }

    public void setRenew(Boolean renew) {
        this.renew = renew;
    }

    public String getTeacherDiscountCouponId() {
        return teacherDiscountCouponId;
    }

    public void setTeacherDiscountCouponId(String teacherDiscountCouponId) {
        this.teacherDiscountCouponId = teacherDiscountCouponId;
    }

    public Integer getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Integer teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    public Integer getVedantuDiscountAmount() {
        return vedantuDiscountAmount;
    }

    public void setVedantuDiscountAmount(Integer vedantuDiscountAmount) {
        this.vedantuDiscountAmount = vedantuDiscountAmount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getPromotionalCost() {
        return promotionalCost;
    }

    public void setPromotionalCost(Integer promotionalCost) {
        this.promotionalCost = promotionalCost;
    }

    public Integer getNonPromotionalCost() {
        return nonPromotionalCost;
    }

    public void setNonPromotionalCost(Integer nonPromotionalCost) {
        this.nonPromotionalCost = nonPromotionalCost;
    }

    public DeliverableEntityType getDeliverableEntityType() {
        return deliverableEntityType;
    }

    public void setDeliverableEntityType(DeliverableEntityType deliverableEntityType) {
        this.deliverableEntityType = deliverableEntityType;
    }

    public SessionSource getSessionSource() {
        return sessionSource;
    }

    public void setSessionSource(SessionSource sessionSource) {
        this.sessionSource = sessionSource;
    }

    public RefundPolicy getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(RefundPolicy refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Integer getVedantuDiscountAmountClaimed() {
        return vedantuDiscountAmountClaimed;
    }

    public void setVedantuDiscountAmountClaimed(Integer vedantuDiscountAmountClaimed) {
        this.vedantuDiscountAmountClaimed = vedantuDiscountAmountClaimed;
    }

    public Integer getTeacherDiscountAmountClaimed() {
        return teacherDiscountAmountClaimed;
    }

    public void setTeacherDiscountAmountClaimed(Integer teacherDiscountAmountClaimed) {
        this.teacherDiscountAmountClaimed = teacherDiscountAmountClaimed;
    }

    @Override
    public String toString() {
        return "OrderedItem{" + "entityId=" + entityId + ", entityType=" + entityType + ", count=" + count + ", rate=" + rate + ", cost=" + cost + ", promotionalCost=" + promotionalCost + ", nonPromotionalCost=" + nonPromotionalCost + ", hours=" + hours + ", model=" + model + ", subModel=" + subModel + ", target=" + target + ", grade=" + grade + ", boardId=" + boardId + ", schedule=" + schedule + ", teacherId=" + teacherId + ", offeringId=" + offeringId + ", renew=" + renew + ", teacherDiscountCouponId=" + teacherDiscountCouponId + ", teacherDiscountAmount=" + teacherDiscountAmount + ", vedantuDiscountCouponId=" + vedantuDiscountCouponId + ", vedantuDiscountAmount=" + vedantuDiscountAmount + ", vedantuDiscountAmountClaimed=" + vedantuDiscountAmountClaimed + ", note=" + note + ", deliverableEntityType=" + deliverableEntityType + ", deliverableEntityId=" + deliverableEntityId + ", sessionSource=" + sessionSource + ", refundStatus=" + refundStatus + '}';
    }

}
