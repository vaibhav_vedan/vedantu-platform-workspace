package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.TransactionRefType;

public class SalesTransactionInfo {
	public String transactionId;
	public int amount;// amount in paisa
	// default to INR
	public String currencyCode;
	public String debitFromAccountNumber;
	public String debitFromAccountName;
	public String debitFromAccountEmail;
	public String creditToAccountNumber;
	public String creditToAccountName;
	public String creditToAccountEmail;
	public String reasonType;
	public String reasonRefNo;
	public TransactionRefType reasonRefType;
	public String reasonNote;
	public String triggredBy;
	public Long creationTime;

	public SalesTransactionInfo() {
		super();
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDebitFromAccountNumber() {
		return debitFromAccountNumber;
	}

	public void setDebitFromAccountNumber(String debitFromAccountNumber) {
		this.debitFromAccountNumber = debitFromAccountNumber;
	}

	public String getDebitFromAccountName() {
		return debitFromAccountName;
	}

	public void setDebitFromAccountName(String debitFromAccountName) {
		this.debitFromAccountName = debitFromAccountName;
	}

	public String getDebitFromAccountEmail() {
		return debitFromAccountEmail;
	}

	public void setDebitFromAccountEmail(String debitFromAccountEmail) {
		this.debitFromAccountEmail = debitFromAccountEmail;
	}

	public String getCreditToAccountNumber() {
		return creditToAccountNumber;
	}

	public void setCreditToAccountNumber(String creditToAccountNumber) {
		this.creditToAccountNumber = creditToAccountNumber;
	}

	public String getCreditToAccountName() {
		return creditToAccountName;
	}

	public void setCreditToAccountName(String creditToAccountName) {
		this.creditToAccountName = creditToAccountName;
	}

	public String getCreditToAccountEmail() {
		return creditToAccountEmail;
	}

	public void setCreditToAccountEmail(String creditToAccountEmail) {
		this.creditToAccountEmail = creditToAccountEmail;
	}

	public String getReasonType() {
		return reasonType;
	}

	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}

	public String getReasonRefNo() {
		return reasonRefNo;
	}

	public void setReasonRefNo(String reasonRefNo) {
		this.reasonRefNo = reasonRefNo;
	}

	public TransactionRefType getReasonRefType() {
		return reasonRefType;
	}

	public void setReasonRefType(TransactionRefType reasonRefType) {
		this.reasonRefType = reasonRefType;
	}

	public String getReasonNote() {
		return reasonNote;
	}

	public void setReasonNote(String reasonNote) {
		this.reasonNote = reasonNote;
	}

	public String getTriggredBy() {
		return triggredBy;
	}

	public void setTriggredBy(String triggredBy) {
		this.triggredBy = triggredBy;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "SalesTranasactionInfo [transactionId=" + transactionId + ", amount=" + amount + ", currencyCode="
				+ currencyCode + ", debitFromAccountNumber=" + debitFromAccountNumber + ", debitFromAccountName="
				+ debitFromAccountName + ", debitFromAccountEmail=" + debitFromAccountEmail + ", creditToAccountNumber="
				+ creditToAccountNumber + ", creditToAccountName=" + creditToAccountName + ", creditToAccountEmail="
				+ creditToAccountEmail + ", reasonType=" + reasonType + ", reasonRefNo=" + reasonRefNo
				+ ", reasonRefType=" + reasonRefType + ", reasonNote=" + reasonNote + ", triggredBy=" + triggredBy
				+ ", creationTime=" + creationTime + "]";
	}
}
