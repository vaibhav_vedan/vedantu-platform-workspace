package com.vedantu.dinero.pojo;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.dinero.enums.*;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Orders extends AbstractMongoStringIdEntityBean{

    private Long userId;

    private List<OrderedItem> items = new ArrayList<>();

    private String ipAddress;

    private Integer amount;//items cost +security+convenience-discount

    //value will be zero for instalments initially as we cannot predit 
    //how much promotional and np amout will be paid before hand
    //will update these fields at the end of the payment
    private Integer promotionalAmount;

    private Integer nonPromotionalAmount;

    private Integer amountPaid;//paid till now

    private Integer promotionalAmountPaid;//paid till now

    private Integer nonPromotionalAmountPaid;//paid till now

    private PaymentStatus paymentStatus;

    private PaymentType paymentType = PaymentType.BULK;

    private String transactionId;

    // in case of offering purchase it will be teacherId
    private Long contextId;

    private Integer security;

    private Integer convenienceCharge;

    private PurchaseFlowType purchaseFlowType = PurchaseFlowType.DIRECT;

    private String purchaseFlowId;

    private List<ReferenceTag> referenceTags = new ArrayList<>();
    
    private List<PurchasingEntity> purchasingEntities;

    // Added as a part of tools enhancement
    private PaymentThrough paymentThrough;
    private TeamType teamType;
    private String agentCode;
    private String agentEmailId;
    private String reportingTeamLeadId;
    private String teamLeadEmailId;
    private Centre centre;
    private EnrollmentType enrollmentType;
    private FintechRisk fintechRisk;
    private RequestRefund requestRefund;
    private long netCollection;
    private String notes;

    private String paymentSource;
    private String subscriptionUpdateId;
    private String subscriptionPlanId ;
}
