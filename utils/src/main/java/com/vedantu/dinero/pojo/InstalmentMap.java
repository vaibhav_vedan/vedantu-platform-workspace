/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

import com.vedantu.session.pojo.SessionSchedule;

/**
 *
 * @author ajith
 */
public class InstalmentMap {

    public Integer hoursCost;
    public SessionSchedule sessionSchedule;
    public Long hours;
    public int vedantuDiscountAmount;
    public int teacherDiscountAmount;

    public InstalmentMap(Integer totalCost, SessionSchedule sessionSchedule, Long hours,
            int vedantuDiscountAmount, int teacherDiscountAmount) {
        this.hoursCost = totalCost;
        this.sessionSchedule = sessionSchedule;
        this.hours = hours;
        this.vedantuDiscountAmount = vedantuDiscountAmount;
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    @Override
    public String toString() {
        return "InstalmentMap{" + "hoursCost=" + hoursCost + ", sessionSchedule=" + sessionSchedule + ", hours=" + hours + '}';
    }

}
