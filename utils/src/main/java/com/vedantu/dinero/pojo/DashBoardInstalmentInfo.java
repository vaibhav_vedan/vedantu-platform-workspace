package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;

public class DashBoardInstalmentInfo {
	private String id;
	private Long dueTime;
	private PaymentStatus paymentStatus;
	private Long paidTime;
	private Integer totalAmount;
        private Integer totalPromotionalAmount;
	private String userId;
	private String orderId;
	private PaymentType paymentType;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getDueTime() {
		return dueTime;
	}
	public void setDueTime(Long dueTime) {
		this.dueTime = dueTime;
	}
	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Long getPaidTime() {
		return paidTime;
	}
	public void setPaidTime(Long paidTime) {
		this.paidTime = paidTime;
	}
	public Integer getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public PaymentType getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

        public Integer getTotalPromotionalAmount() {
            return totalPromotionalAmount;
        }

        public void setTotalPromotionalAmount(Integer totalPromotionalAmount) {
            this.totalPromotionalAmount = totalPromotionalAmount;
        }

    @Override
    public String toString() {
        return "OTFBasicInstalmentInfo{" + "id=" + id + ", dueTime=" + dueTime + ", paymentStatus=" + paymentStatus + ", paidTime=" + paidTime + ", totalAmount=" + totalAmount + ", userId=" + userId + ", orderId=" + orderId + ", paymentType=" + paymentType + '}';
    }

}
