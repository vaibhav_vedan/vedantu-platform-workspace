/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;


/**
 *
 * @author somil
 */
public class BaseInstalmentInfo {
    
    private Long dueTime;
    private Integer amount;
    private Integer minimumAmount;

    public BaseInstalmentInfo() {
    }

    public BaseInstalmentInfo(Long dueTime, Integer amount) {
        this.dueTime = dueTime;
        this.amount = amount;
    }

    
    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Integer minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    @Override
    public String toString() {
        return "BaseInstalmentInfo{" + "dueTime=" + dueTime + ", amount=" + amount + ", minimumAmount=" + minimumAmount + '}';
    }

}
