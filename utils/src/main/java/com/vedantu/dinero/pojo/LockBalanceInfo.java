package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import java.util.ArrayList;
import java.util.List;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.BasicResponse;

public class LockBalanceInfo extends BasicResponse {

    private Long userId;
    private EntityType referenceType;
    private String referenceId;
    private Integer promotionalCost;
    private Integer nonPromotionalCost;
    private PaymentType paymentType;
    private PurchaseFlowType purchaseFlowType;
    private String purchaseFlowId;

    // TODO change cost to Amount
    public LockBalanceInfo() {
        super();
    }

    public LockBalanceInfo(Long userId) {
        super();
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getPromotionalCost() {
        return promotionalCost;
    }

    public void setPromotionalCost(Integer promotionalCost) {
        this.promotionalCost = promotionalCost;
    }

    public Integer getNonPromotionalCost() {
        return nonPromotionalCost;
    }

    public void setNonPromotionalCost(Integer nonPromotionalCost) {
        this.nonPromotionalCost = nonPromotionalCost;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public EntityType getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(EntityType referenceType) {
        this.referenceType = referenceType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PurchaseFlowType getPurchaseFlowType() {
        return purchaseFlowType;
    }

    public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
        this.purchaseFlowType = purchaseFlowType;
    }

    public String getPurchaseFlowId() {
        return purchaseFlowId;
    }

    public void setPurchaseFlowId(String purchaseFlowId) {
        this.purchaseFlowId = purchaseFlowId;
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<String>();
        if (this.userId == null || this.userId <= 0l) {
            errors.add("userId");
        }

        if ((this.promotionalCost == null || this.promotionalCost < 0l)
                && (this.nonPromotionalCost == null || this.nonPromotionalCost < 0l)) {
            errors.add("promotionalCost");
            errors.add("nonPromotionalCost");
        }

        return errors;
    }

    @Override
    public String toString() {
        return "LockBalanceInfo [userId=" + userId + ", referenceType=" + referenceType + ", referenceId=" + referenceId
                + ", promotionalCost=" + promotionalCost + ", nonPromotionalCost=" + nonPromotionalCost + "]";
    }
}
