package com.vedantu.dinero.pojo;

import com.vedantu.exception.ErrorCode;

public class LockBalanceRes extends LockBalanceInfo {

    private ErrorCode errorCode;
    private Long amountToBeLocked;
    private Long accountBalance;
    private Long teacherDiscountAmount;
    private String teacherDiscountCouponId;
    private String vedantuDiscountCouponId;    
    private Integer amountToBePaid;//introducing this as amountToBeLocked-accountBalance will fail in case of instalments

    public LockBalanceRes() {
        super();
    }

    public Long getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public Long getAmountToBeLocked() {
        return amountToBeLocked;
    }

    public void setAmountToBeLocked(Long amountToBeLocked) {
        this.amountToBeLocked = amountToBeLocked;
    }

    public Long getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Long accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Integer getAmountToBePaid() {
        return amountToBePaid;
    }

    public void setAmountToBePaid(Integer amountToBePaid) {
        this.amountToBePaid = amountToBePaid;
    }

    public String getTeacherDiscountCouponId() {
        return teacherDiscountCouponId;
    }

    public void setTeacherDiscountCouponId(String teacherDiscountCouponId) {
        this.teacherDiscountCouponId = teacherDiscountCouponId;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    @Override
    public String toString() {
        return "LockBalanceRes{" + "errorCode=" + errorCode + ", amountToBeLocked=" + amountToBeLocked + ", accountBalance=" + accountBalance + ", teacherDiscountAmount=" + teacherDiscountAmount + ", teacherDiscountCouponId=" + teacherDiscountCouponId + ", vedantuDiscountCouponId=" + vedantuDiscountCouponId + ", amountToBePaid=" + amountToBePaid + '}';
    }

}
