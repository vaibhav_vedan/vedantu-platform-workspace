package com.vedantu.dinero.pojo;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InvoiceStateCode;
import com.vedantu.dinero.enums.PaymentInvoiceContextType;
import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.List;

public class PaymentInvoiceBasicInfo extends AbstractMongoStringIdEntityBean{
	private String invoiceNo;
	private Long fromUserId;
	private Role fromUserRole;
	private Long toUserId;
	private Role toUserRole;

	private Long totalAmount; // In paisa. Including all the taxes
	private Long actualAmount;//includes promotional component in case of vedantu teacher invoices
        private Long promotionalAmount;
        private Long taxableAmount;//totalAmount-taxes=taxableamount, this will be backcalculated
        
	private Long invoiceTime;
	private String invoiceTitle;
	private String invoiceDescription;
        
        private PaymentInvoiceTypeDetails paymentInvoiceTypeDetails;
	private DeliverableEntityType deliverableEntityType;
	private String deliverableEntityId; // This has to be unique
	private String orderId;
	private List<String> tags;
        private PaymentInvoiceContextType contextType;
        private String contextId;
        private String ipAddress;
        private String supplyState;
        private String address;
        private String country;
        private InvoiceStateCode invoiceStateCode;
        private UserBasicInfo toUser;

	public PaymentInvoiceBasicInfo() {
		super();
	}

	public Long getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}

	public Role getFromUserRole() {
		return fromUserRole;
	}

	public void setFromUserRole(Role fromUserRole) {
		this.fromUserRole = fromUserRole;
	}

	public Role getToUserRole() {
		return toUserRole;
	}

	public void setToUserRole(Role toUserRole) {
		this.toUserRole = toUserRole;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getInvoiceTime() {
		return invoiceTime;
	}

	public void setInvoiceTime(Long invoiceTime) {
		this.invoiceTime = invoiceTime;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public String getInvoiceDescription() {
		return invoiceDescription;
	}

	public void setInvoiceDescription(String invoiceDescription) {
		this.invoiceDescription = invoiceDescription;
	}

        public Long getActualAmount() {
            return actualAmount;
        }

        public void setActualAmount(Long actualAmount) {
            this.actualAmount = actualAmount;
        }

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
        public Long getPromotionalAmount() {
            return promotionalAmount;
        }

        public void setPromotionalAmount(Long promotionalAmount) {
            this.promotionalAmount = promotionalAmount;
        }

        public Long getTaxableAmount() {
            return taxableAmount;
        }

        public void setTaxableAmount(Long taxableAmount) {
            this.taxableAmount = taxableAmount;
        }

        public PaymentInvoiceTypeDetails getPaymentInvoiceTypeDetails() {
            return paymentInvoiceTypeDetails;
        }

        public void setPaymentInvoiceTypeDetails(PaymentInvoiceTypeDetails paymentInvoiceTypeDetails) {
            this.paymentInvoiceTypeDetails = paymentInvoiceTypeDetails;
        }

        public DeliverableEntityType getDeliverableEntityType() {
            return deliverableEntityType;
        }

        public void setDeliverableEntityType(DeliverableEntityType deliverableEntityType) {
            this.deliverableEntityType = deliverableEntityType;
        }

        public String getDeliverableEntityId() {
            return deliverableEntityId;
        }

        public void setDeliverableEntityId(String deliverableEntityId) {
            this.deliverableEntityId = deliverableEntityId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public PaymentInvoiceContextType getContextType() {
            return contextType;
        }

        public void setContextType(PaymentInvoiceContextType contextType) {
            this.contextType = contextType;
        }

        public String getContextId() {
            return contextId;
        }

        public void setContextId(String contextId) {
            this.contextId = contextId;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public void setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
        }

        public String getSupplyState() {
            return supplyState;
        }

        public void setSupplyState(String supplyState) {
            this.supplyState = supplyState;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public InvoiceStateCode getInvoiceStateCode() {
            return invoiceStateCode;
        }

        public void setInvoiceStateCode(InvoiceStateCode invoiceStateCode) {
            this.invoiceStateCode = invoiceStateCode;
        }

        public UserBasicInfo getToUser() {
            return toUser;
        }

        public void setToUser(UserBasicInfo toUser) {
            this.toUser = toUser;
        }
}
