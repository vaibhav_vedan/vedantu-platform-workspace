package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.TaxType;

public class TaxPercentage {

    private TaxType taxType;
    private float percentage;
    private Long amount;

    public TaxPercentage() {
        super();
    }

    public TaxPercentage(TaxType taxType, float percentage) {
        this.taxType = taxType;
        this.percentage = percentage;
    }
    
    public TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxType taxType) {
        this.taxType = taxType;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public void updateAmount(long totalAmount) {
        this.amount = (long) ((totalAmount * this.percentage) / 100);
    }

    @Override
    public String toString() {
        return "TaxPercentage [taxType=" + taxType + ", percentage=" + percentage + ", amount=" + amount
                + ", toString()=" + super.toString() + "]";
    }
}
