package com.vedantu.dinero.pojo;

import java.util.Map;

import com.vedantu.dinero.enums.PaymentGatewayName;

public class RechargeUrlInfo {

    private PaymentGatewayName gatewayName;
    private String rechargeUrl;
    private String forwardHttpMethod;
    private Map<String, Object> postParams;

    public RechargeUrlInfo() {
        super();
    }

    public RechargeUrlInfo(PaymentGatewayName gatewayName, String rechargeUrl, String forwardHttpMethod, Map<String, Object> postParams) {
        super();
        this.gatewayName = gatewayName;
        this.rechargeUrl = rechargeUrl;
        this.forwardHttpMethod = forwardHttpMethod;
        this.postParams = postParams;
    }

    public PaymentGatewayName getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(PaymentGatewayName gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getRechargeUrl() {
        return rechargeUrl;
    }

    public String getForwardHttpMethod() {
        return forwardHttpMethod;
    }

    public void setRechargeUrl(String rechargeUrl) {
        this.rechargeUrl = rechargeUrl;
    }

    public void setForwardHttpMethod(String forwardHttpMethod) {
        this.forwardHttpMethod = forwardHttpMethod;
    }

    public Map<String, Object> getPostParams() {
        return postParams;
    }

    public void setPostParams(Map<String, Object> postParams) {
        this.postParams = postParams;
    }

    @Override
    public String toString() {
        return "RechargeUrlInfo{" + "gatewayName=" + gatewayName + ", rechargeUrl=" + rechargeUrl + ", forwardHttpMethod=" + forwardHttpMethod + ", postParams=" + postParams + '}';
    }

}
