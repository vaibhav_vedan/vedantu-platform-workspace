package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.BaseSubscriptionPurchaseEntity;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.subscription.response.BaseSubscriptionDuration;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author darshit
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseSubscriptionPlanReq {
    private String id;
    private Integer validMonths;
    private BaseSubscriptionPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private Integer price;
    private Integer cutPrice;
    private EntityState entityState;
    private BaseSubscriptionDuration planDuration;


}
