package com.vedantu.dinero.pojo;

/**
 * Created by somil on 04/05/17.
 */
public class FirstExtTransaction {
    Long userId;
    Long creationTime;
    String creditToAccount;

    public String getCreditToAccount() {
        return creditToAccount;
    }

    public void setCreditToAccount(String creditToAccount) {
        this.creditToAccount = creditToAccount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }
}
