package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author ajith
 */
public class BaseInstalment extends AbstractMongoStringIdEntityBean {

    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private List<BaseInstalmentInfo> info;
    Long userId;
    //Integer dueTime;
    //Integer amount;

    public BaseInstalment() {
        super();
    }

    public BaseInstalment(InstalmentPurchaseEntity purchaseEntityType, String purchaseEntityId, List<BaseInstalmentInfo> info) {
        this.purchaseEntityType = purchaseEntityType;
        this.purchaseEntityId = purchaseEntityId;
        this.info = info;
    }

    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseEntityId() {
        return purchaseEntityId;
    }

    public void setPurchaseEntityId(String purchaseEntityId) {
        this.purchaseEntityId = purchaseEntityId;
    }

//    public Integer getDueTime() {
//        return dueTime;
//    }
//
//    public void setDueTime(Integer dueTime) {
//        this.dueTime = dueTime;
//    }
//
//    public Integer getAmount() {
//        return amount;
//    }
//
//    public void setAmount(Integer amount) {
//        this.amount = amount;
//    }
    public List<BaseInstalmentInfo> getInfo() {
        return info;
    }

    public void setInfo(List<BaseInstalmentInfo> info) {
        this.info = info;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BaseInstalmentInfo getFirstDueBaseInstalmentInfo() {
        if (info == null) {
            return null;
        }
        Collections.sort(info, new Comparator<BaseInstalmentInfo>() {
            @Override
            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });
        if (!info.isEmpty()) {
            return info.get(0);
        } else {
            return null;
        }
    }
}
