package com.vedantu.dinero.pojo;

public class TeacherSlabPlan {

	String id;
	int min;
	int max;
	Long price;
	Long teacherId;

	public TeacherSlabPlan(String id, int min, int max, Long price, Long teacherId) {
		super();
		this.id = id;
		this.min = min;
		this.max = max;
		this.price = price;
		this.teacherId = teacherId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "TeacherSlabPlan [id=" + id + ", min=" + min + ", max=" + max + ", price=" + price + ", teacherId="
				+ teacherId + "]";
	}

	public TeacherSlabPlan(String id, int min, int max, Long price) {
		super();
		this.id = id;
		this.min = min;
		this.max = max;
		this.price = price;
	}

	public TeacherSlabPlan() {
		super();
		// TODO Auto-generated constructor stub
	}

}
