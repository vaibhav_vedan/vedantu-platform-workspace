package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.lms.cmds.enums.AccessLevel;
import lombok.Data;

import java.util.List;

@Data
public class Coupon {
    private String code;
}
