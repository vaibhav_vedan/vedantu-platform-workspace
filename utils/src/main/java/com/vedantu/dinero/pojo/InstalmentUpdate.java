/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

/**
 *
 * @author ajith
 */
public class InstalmentUpdate {
    private Long oldDueTime;
    private Long changeTime;
    private Long changedBy;

    public Long getOldDueTime() {
        return oldDueTime;
    }

    public void setOldDueTime(Long oldDueTime) {
        this.oldDueTime = oldDueTime;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public Long getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Long changedBy) {
        this.changedBy = changedBy;
    }

    @Override
    public String toString() {
        return "InstalmentUpdates{" + "oldDueTime=" + oldDueTime + ", changeTime=" + changeTime + ", changedBy=" + changedBy + '}';
    }
    
}
