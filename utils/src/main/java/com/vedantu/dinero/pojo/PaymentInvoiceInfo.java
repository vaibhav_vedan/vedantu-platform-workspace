package com.vedantu.dinero.pojo;

public class PaymentInvoiceInfo extends PaymentInvoiceBasicInfo {

	// Additional Info
	private PaymentInvoiceUserInfo fromUserInfo;
	private PaymentInvoiceUserInfo toUserInfo;

	public PaymentInvoiceInfo() {
		super();
	}
        
	public PaymentInvoiceUserInfo getFromUserInfo() {
		return fromUserInfo;
	}

	public void setFromUserInfo(PaymentInvoiceUserInfo fromUserInfo) {
		this.fromUserInfo = fromUserInfo;
	}

	public PaymentInvoiceUserInfo getToUserInfo() {
		return toUserInfo;
	}

	public void setToUserInfo(PaymentInvoiceUserInfo toUserInfo) {
		this.toUserInfo = toUserInfo;
	}
        
}
