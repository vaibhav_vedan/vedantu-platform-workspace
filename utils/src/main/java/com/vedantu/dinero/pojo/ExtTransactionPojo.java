package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.enums.TransactionType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class ExtTransactionPojo extends AbstractMongoStringIdEntityBean {

    private Long userId;
    private String ipAddress;
    private TransactionType type;
    private TransactionStatus status;

    // amount in paisa
    private Integer amount;
    // (currencyCode==INR)
    private String currencyCode;
    private String transactionTime;
    private String paymentChannelTransactionId;
    private String paymentMethod;
    private String paymentInstrument;
    private String bankRefNo;
    // vedantuOrderId --> this is id of Order entry created which is
    // in PENDING state when some one directly buys product and do payment from
    // Payment Gateway
    private String vedantuOrderId;
    private PaymentType paymentType;
    private String redirectUrl;
    private String checksum;
    private PaymentGatewayName gatewayName;
    private TransactionRefType refType;
    private String refId;

    public ExtTransactionPojo() {

        super();
    }

    public ExtTransactionPojo(Long userId, String ipAddress, TransactionType type,
            TransactionStatus status, Integer amount, String currencyCode,
            String redirectUrl, PaymentGatewayName gatewayName) {

        this();
        this.userId = userId;
        this.ipAddress = ipAddress;
        this.type = type;
        this.status = status;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.redirectUrl = redirectUrl;
        this.gatewayName = gatewayName;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public String getIpAddress() {

        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {

        this.ipAddress = ipAddress;
    }

    public TransactionType getType() {

        return type;
    }

    public void setType(TransactionType type) {

        this.type = type;
    }

    public TransactionStatus getStatus() {

        return status;
    }

    public void setStatus(TransactionStatus status) {

        this.status = status;
    }

    public Integer getAmount() {

        return amount;
    }

    public void setAmount(Integer amount) {

        this.amount = amount;
    }

    public String getCurrencyCode() {

        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {

        this.currencyCode = currencyCode;
    }

    public String getTransactionTime() {

        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {

        this.transactionTime = transactionTime;
    }

    public String getPaymentChannelTransactionId() {

        return paymentChannelTransactionId;
    }

    public void setPaymentChannelTransactionId(
            String paymentChannelTransactionId) {

        this.paymentChannelTransactionId = paymentChannelTransactionId;
    }

    public String getPaymentMethod() {

        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {

        this.paymentMethod = paymentMethod;
    }

    public String getPaymentInstrument() {

        return paymentInstrument;
    }

    public void setPaymentInstrument(String paymentInstrument) {

        this.paymentInstrument = paymentInstrument;
    }

    public String getBankRefNo() {

        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {

        this.bankRefNo = bankRefNo;
    }

    public String getVedantuOrderId() {
        return vedantuOrderId;
    }

    public void setVedantuOrderId(String vedantuOrderId) {
        this.vedantuOrderId = vedantuOrderId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public PaymentGatewayName getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(PaymentGatewayName gatewayName) {
        this.gatewayName = gatewayName;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public TransactionRefType getRefType() {
        return refType;
    }

    public void setRefType(TransactionRefType refType) {
        this.refType = refType;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public static class Constants extends AbstractMongoEntityBean.Constants {

        public static final String DEFAULT_CURRENCY_CODE = "INR";
        public static final String USER_ID = "userId";
    }

}
