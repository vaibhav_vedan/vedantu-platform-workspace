package com.vedantu.dinero.pojo;

import java.util.List;

import com.vedantu.dinero.enums.PaymentInvoiceType;

public class PaymentInvoiceTypeDetails {
	private String id;
	private PaymentInvoiceType type;
	private List<TaxPercentage> taxPercentages;

	public PaymentInvoiceTypeDetails() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PaymentInvoiceType getType() {
		return type;
	}

	public void setType(PaymentInvoiceType type) {
		this.type = type;
	}

	public List<TaxPercentage> getTaxPercentages() {
		return taxPercentages;
	}

	public void setTaxPercentages(List<TaxPercentage> taxPercentages) {
		this.taxPercentages = taxPercentages;
	}

	@Override
	public String toString() {
		return "PaymentInvoiceTypeDetails [id=" + id + ", type=" + type + ", taxPercentages=" + taxPercentages
				+ ", toString()=" + super.toString() + "]";
	}
}
