package com.vedantu.dinero.enums;

public enum PaymentMethod {
    CARD,
    NETBANKING,
    WALLET,
    EMI
}
