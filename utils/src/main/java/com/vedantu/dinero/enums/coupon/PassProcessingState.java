package com.vedantu.dinero.enums.coupon;

/**
 * Created by somil on 05/06/17.
 */
public enum PassProcessingState {
    VALID, EXPIRED
}
