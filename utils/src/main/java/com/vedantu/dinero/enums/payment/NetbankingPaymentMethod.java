package com.vedantu.dinero.enums.payment;

import javax.validation.constraints.NotBlank;
import java.util.*;

public enum NetbankingPaymentMethod {
    NB_AXIS("Axis Bank"), // PAYU
    NB_BOI("Bank of India"), // PAYU
    NB_BOM("Bank of Maharashtra"), // PAYU
    NB_CBI("Central Bank Of India"), // PAYU
    NB_CORP("Corporation Bank"), // PAYU
    NB_DCB("Development Credit Bank"), // PAYU
    NB_FED("Federal Bank"), // PAYU
    NB_HDFC("HDFC Bank"), // PAYU
    NB_ICICI("ICICI Netbanking"), // PAYU
    NB_IDBI("Industrial Development Bank of India"), // PAYU
    NB_INDB("Indian Bank"), // PAYU
    NB_INDUS("IndusInd Bank"), // PAYU
    NB_IOB("Indian Overseas Bank"), // PAYU
    NB_JNK("Jammu and Kashmir Bank"), // PAYU
    NB_KARN("Karnataka Bank"), // PAYU
    NB_KVB("Karur Vysya"), // PAYU
//    NB_SBBJ("State Bank of Bikaner and Jaipur"), //
//    NB_SBH("State Bank of Hyderabad"),
    NB_SBI("State Bank of India"), // PAYU
//    NB_SBM("State Bank of Mysore"),
//    NB_SBT("State Bank of Travancore"),
    NB_SOIB("South Indian Bank"), // PAYU
    NB_UBI("Union Bank of India"), // PAYU
    NB_UNIB("United Bank Of India"), // PAYU
    NB_VJYB("Vijaya Bank"), // PAYU
    NB_YESB("Yes Bank"), // PAYU
    NB_CUB("CityUnion"), // PAYU
    NB_CANR("Canara Bank"), // PAYU
//    NB_SBP("State Bank of Patiala"),
    NB_CITI("Citi Bank NetBanking"), // PAYU
    NB_DEUT("Deutsche Bank"), // PAYU
    NB_KOTAK("Kotak Bank"), // PAYU
    NB_DLS("Dhanalaxmi Bank"), // PAYU
//    NB_ING("ING Vysya Bank"),
    NB_ANDHRA("Andhra Bank"), // PAYU
//    NB_PNBCORP("Punjab National Bank CORPORATE"), // PAYU
    NB_PNB("Punjab National Bank"), // PAYU
//    NB_BOB("Bank of Baroda"),
    NB_CSB("Catholic Syrian Bank"), // PAYU
    NB_OBC("Oriental Bank Of Commerce"), // PAYU
//    NB_SCB("Standard Chartered Bank"),
    NB_TMB("Tamilnad Mercantile Bank"),
    NB_SARASB("Saraswat Bank"), // PAYU
    NB_SYNB("Syndicate Bank"), // PAYU
    NB_UCOB("UCO Bank"), // PAYU
//    NB_BOBCORP("Bank of Baroda Corporate"),
//    NB_ALLB("Allahabad Bank"),
//    NB_BBKM("Bank of Bahrain and Kuwait"),
    NB_JSB("Janata Sahakari Bank"),// PAYU
    NB_LVBCORP("Lakshmi Vilas Bank Corporate"), // PAYU
    NB_LVB("Lakshmi Vilas Bank Retail"), // PAYU
//    NB_NKGSB("North Kanara GSB"),
//    NB_PMCB("Punjab and Maharashtra Coop Bank"), // PAYU
    NB_PNJSB("Punjab and Sind Bank"), // PAYU
//    NB_RATN("Ratnakar Bank"),
//    NB_RBS("Royal Bank of Scotland"),
    NB_SVCB("Shamrao Vithal Coop Bank"), // PAYU
//    NB_TNSC("Tamil Nadu State Apex Coop Bank"),
    NB_DENA("DENA Bank"), // PAYU
    NB_COSMOS("COSMOS Bank"), // PAYU
//    NB_DBS("DBS Bank Ltd"),
//    NB_DCBB("DCB BANK Business"),
//    NB_SVC("SVC Cooperative Bank"),
//    NB_BHARAT("Bharat Bank"),
    NB_KVBCORP("Karur Vysya Corporate Banking"),
    NB_UBICORP("Union Bank Corporate Banking"),
    NB_IDFC("IDFC Bank"),
    NB_NAIB("The Nainital Bank"),

    ;

    private String bankDisplayName;

    NetbankingPaymentMethod(@NotBlank String bankDisplayName) {
        this.bankDisplayName = bankDisplayName;
    }

    public static List<Map<String, String>> getNetBankingList() {
        List<Map<String, String>> netbankingList = new ArrayList<>();
        for (NetbankingPaymentMethod value : values()) {
            Map<String, String> netbanking = new HashMap<>();
            netbanking.put("netbanking_code", value.name());
            netbanking.put("bank_display_name", value.bankDisplayName);
            netbankingList.add(netbanking);
        }
        netbankingList.sort(Comparator.comparing(e -> e.get("bank_display_name")));
        return netbankingList;
    }
}
