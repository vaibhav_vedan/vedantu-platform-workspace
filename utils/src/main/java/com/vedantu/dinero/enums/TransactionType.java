package com.vedantu.dinero.enums;

public enum TransactionType {
	CREDIT, DEBIT;
}
