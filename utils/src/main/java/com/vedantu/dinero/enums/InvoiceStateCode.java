/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.enums;

/**
 *
 * @author jeet
 */
public enum InvoiceStateCode {
    KARNATAKA(29, "Karnataka"),
    KERALA(32, "Kerala"),
    STATEOFPUNJAB(3, "Punjab"),
    TELANGANA(36, "Telangana"),
    GUJARAT(24, "Gujarat"),
    NATIONALCAPITALTERRITORYOFDELHI(7, "Delhi"),
    MAHARASHTRA(27, "Maharashtra"),
    WESTBENGAL(19, "West Bengal"),
    ANDHRAPRADESH(28, "Andhra Pradesh"),
    TAMILNADU(33, "Tamil Nadu"),
    TN(33, "Tamil Nadu"),
    CHANDIGARH(4, "Chandigarh"),
    BIHAR(10, "Bihar"),
    MADHYAPRADESH(23, "Madhya Pradesh"),
    RAJASTHAN(8, "Rajasthan"),
    ANDAMANANDNICOBARISLANDS(35, "Andaman and Nicobar Islands"),
    ANDHRAPRADESHNEW(37, "Andhra Pradesh (New)"),
    ARUNACHALPRADESH(12, "Arunachal Pradesh"),
    ASSAM(18, "Assam"),
    CHATTISGARH(22, "Chattisgarh"),
    CHHATTISGARH(22, "Chattisgarh"),
    DADRAANDNAGARHAVELI(26, "Dadra and Nagar Haveli"),
    DAMANANDDIU(25, "Daman and Diu"),
    DELHI(7, "Delhi"),
    GOA(30, "Goa"),
    HARYANA(6, "Haryana"),
    HIMACHALPRADESH(2, "Himachal Pradesh"),
    JAMMUANDKASHMIR(1, "Jammu and Kashmir"),
    JAMMUKASHMIR(1, "Jammu and Kashmir"),
    JHARKHAND(20, "Jharkhand"),
    LAKSHADWEEPISLANDS(31, "Lakshadweep Islands"),
    MANIPUR(14, "Manipur"),
    MEGHALAYA(17, "Meghalaya"),
    MIZORAM(15, "Mizoram"),
    NAGALAND(13, "Nagaland"),
    ODISHA(21, "Odisha"),
    ORISSA(21, "Odisha"),
    PONDICHERRY(34, "Pondicherry"),
    PUDUCHERRY(34, "Pondicherry"),
    PUNJAB(3, "Punjab"),
    SIKKIM(11, "Sikkim"),
    TRIPURA(16, "Tripura"),
    UTTARPRADESH(9, "Uttar Pradesh"),
    UP(9, "Uttar Pradesh"),
    UTTARAKHAND(5, "Uttarakhand"),
    INTERNATIONAL(0, "International");
    
    private int numVal;
    private String strVal;

    InvoiceStateCode(int numVal, String strVal) {
        this.numVal = numVal;
        this.strVal = strVal;
    }

    public int getNumVal() {
        return numVal;
    }
    
    public String getStrVal() {
        return strVal;
    }
}
