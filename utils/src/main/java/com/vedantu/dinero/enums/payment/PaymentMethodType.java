package com.vedantu.dinero.enums.payment;

public enum PaymentMethodType {
    CARD,
    NETBANKING,
    WALLET,
    WALLET_DIRECT_DEBIT,
    ATM_SEAMLESS_PIN,
    ATM_REDIRECTION,
    UPI_COLLECT,
    UPI_PAY
}
