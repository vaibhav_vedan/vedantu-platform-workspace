package com.vedantu.dinero.enums;

public enum LoanType {
    FULL, PARTIAL, NO_LOAN
}
