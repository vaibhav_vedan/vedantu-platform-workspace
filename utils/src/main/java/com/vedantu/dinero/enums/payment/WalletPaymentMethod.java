package com.vedantu.dinero.enums.payment;

public enum WalletPaymentMethod {
    MOBIKWIK,
    PAYTM,
    FREECHARGE,
    OLAMONEY,
    PAYUMONEY,
    AIRTEL_MONEY,
    OXIGEN,
    PAYZAPP,
    JANACASH,
    JIOMONEY,
    PHONEPE,
    AMAZONPAY,
    PAYPAL;


    public boolean isJuspayDirectDebitSupported() {
        return MOBIKWIK.equals(this) || PAYTM.equals(this) ||
                FREECHARGE.equals(this) || OLAMONEY.equals(this);
    }

}
