/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.enums.coupon;

/**
 *
 * @author ajith
 */
public enum RedeemedCouponState {
    PROCESSED, LOCKED, UNUSED //unused implies not used, if a coupon has no state by default it is unused state
}
