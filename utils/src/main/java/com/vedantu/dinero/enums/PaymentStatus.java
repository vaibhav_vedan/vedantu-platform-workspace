package com.vedantu.dinero.enums;

public enum PaymentStatus {
    PAID("Paid"), NOT_PAID("Not Paid"),PARTIALLY_PAID("Incomplete Payment"), CANCELLED("Cancelled"), FAILED("Failed"), REVERTED("Reverted"),
    PAYMENT_SUSPENDED("PAYMENT_SUSPENDED"),FORFEITED("Forfeited");
    private String displayName;

    private PaymentStatus(String name) {
        this.displayName = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
