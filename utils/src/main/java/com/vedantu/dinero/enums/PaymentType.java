package com.vedantu.dinero.enums;

public enum PaymentType {

	BULK, INSTALMENT;
}
