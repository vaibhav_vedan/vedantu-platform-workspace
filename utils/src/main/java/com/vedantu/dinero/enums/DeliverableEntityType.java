package com.vedantu.dinero.enums;

public enum DeliverableEntityType {
	SUBSCRIPTION_DETAILS,OTF_BATCH_ENROLLMENT,COURSE_PLAN, BUNDLE_ENROLMENT
}
