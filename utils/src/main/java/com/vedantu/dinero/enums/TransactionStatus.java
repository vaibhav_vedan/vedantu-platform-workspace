package com.vedantu.dinero.enums;

public enum TransactionStatus {
	SUCCESS, FAILED, PENDING, CANCELLED;
}
