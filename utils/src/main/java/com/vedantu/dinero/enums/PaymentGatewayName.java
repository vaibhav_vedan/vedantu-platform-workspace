package com.vedantu.dinero.enums;

import com.vedantu.exception.ForbiddenException;

public enum PaymentGatewayName {

	CCAVENUE, ZAAKPAY, PAYU, RAZORPAY, AXIS, AXIS_JUSPAY, JUSPAY,PAYTM_MINI;
	
	public static PaymentGatewayName getPaymentGatewayName(String gatewayName)
			throws ForbiddenException {
		PaymentGatewayName paymentGateway = null;
		try {
			paymentGateway = PaymentGatewayName.valueOf(gatewayName.trim().toUpperCase());
		} catch (Throwable e) {

		}
		return paymentGateway;
	}
}
