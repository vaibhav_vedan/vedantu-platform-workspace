package com.vedantu.dinero.enums.payment;

public enum CardPaymentMethod {
    VISA,
    MASTERCARD,
    MAESTRO,
    AMEX,
    RUPAY,
}
