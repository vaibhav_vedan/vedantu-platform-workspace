package com.vedantu.dinero.enums;

public enum RequestRefund {
    NA,WITHIN_15_DAYS,PAST_15_DAYS
}
