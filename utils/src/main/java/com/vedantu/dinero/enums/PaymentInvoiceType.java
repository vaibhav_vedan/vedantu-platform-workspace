package com.vedantu.dinero.enums;

import com.vedantu.User.Role;

public enum PaymentInvoiceType {
	// Null is vedantu and vedantu is Null

	VEDANTU_TEACHER(null, Role.TEACHER), VEDANTU_STUDENT(null, Role.STUDENT), TEACHER_STUDENT(Role.TEACHER, Role.STUDENT);

	private Role fromUserRole;
	private Role toUserRole;

	PaymentInvoiceType(Role fromUserRole, Role toUserRole) {
		this.fromUserRole = fromUserRole;
		this.toUserRole = toUserRole;
	}

	public Role getFromUserRole() {
		return fromUserRole;
	}

	public void setFromUserRole(Role fromUserRole) {
		this.fromUserRole = fromUserRole;
	}

	public Role getToUserRole() {
		return toUserRole;
	}

	public void setToUserRole(Role toUserRole) {
		this.toUserRole = toUserRole;
	}
}
