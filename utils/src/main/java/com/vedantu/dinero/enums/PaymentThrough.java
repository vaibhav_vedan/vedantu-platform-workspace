package com.vedantu.dinero.enums;

public enum PaymentThrough {
    VBULK, VEMI, ZEST_MONEY, SNAPMINT, CAPITAL_FLOAT, LIQUI_LOANS, BAJAJ_FINSERV, LIQUI_LOANS_FLDG, DP_NACH, DP_NACH_DBS, NO_COST_EMI_CC, PAYTM_OFFLINE, QATAR_MASTERMIND, BFL, MONEY_TAP, TCPL, SHOPSE, NONE
}
