package com.vedantu.dinero.enums;

public enum TaxType {
	GST, SERVICE_TAX, SBC, KKC, CGST, SGST, IGST;
}
