package com.vedantu.User;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.fos.interfaces.IEntity;

import java.util.ArrayList;
import java.util.List;

public class AbstractEntityRes implements IEntity {

    private Long id;
    private Long creationTime;
    private Long lastUpdated;

    public AbstractEntityRes() {

        super();
        this.creationTime = System.currentTimeMillis();
    }

    public AbstractEntityRes(Long id, Long creationTime, Long lastUpdated) {
        this.id = id;
        this.creationTime = creationTime;
        this.lastUpdated = lastUpdated;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getCreationTime() {

        return creationTime;
    }

    public void setCreationTime(Long creationTime) {

        this.creationTime = creationTime;
    }

    public Long getLastUpdated() {

        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {

        this.lastUpdated = lastUpdated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractEntityRes other = (AbstractEntityRes) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public String _getCacheKey() {
        return getClass().getName() + "_" + getId();
    }

    public static String _getCacheKey(Class<?> clazz, Long id) {
        return clazz.getName() + "_" + id;
    }

    public void verify() throws BadRequestException {

        List<String> verificationErrors = collectVerificationErrors();
        if (!verificationErrors.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, verificationErrors.toString());
        }
    }

    protected List<String> collectVerificationErrors() {

        return new ArrayList<>();
    }

    public static class Constants {

        public static final String ID = "id";
        public static final String STATE = "state";
        public static final String CODE = "code";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String SCOPE = "scope";
        public static final String TYPE = "type";
        public static final String DURATION = "duration";
        public static final String USER_ID = "userId";
        public static final String CREATION_TIME = "creationTime";
        public static final String LAST_UPDATED = "lastUpdated";
        public static final String UTM_SOURCE = "utm_source";
        public static final String UTM_MEDIUM = "utm_medium";
        public static final String UTM_CAMPAIGN = "utm_campaign";
        public static final String UTM_CONTENT = "utm_content";
        public static final String UTM_TERM = "utm_term";
        public static final String CHANNEL = "channel";

        public static final String OFFERING_ID = "offeringId";
        public static final String ENTITY_ID = "entityId";
        public static final String ENTITY_TYPE = "entityType";
    }

    @Override
    public void preStore() {
    }

    @Override
    public void postStore() {
    }

}
