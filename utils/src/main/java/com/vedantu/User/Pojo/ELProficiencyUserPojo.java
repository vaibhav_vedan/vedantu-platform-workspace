package com.vedantu.User.Pojo;

import java.util.List;

import com.vedantu.User.enums.TeacherCategory.ProficiencyType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ELProficiencyUserPojo {

    private ProficiencyType proficiencyType;
    private List<String> teacherIds;
}
