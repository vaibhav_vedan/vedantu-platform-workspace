package com.vedantu.User.Pojo;

import com.vedantu.User.enums.TeacherCategory.ProficiencyType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherProficiency {
	private String _id;
	private ProficiencyType proficiencyType;
}
