package com.vedantu.User.Pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.subscription.enums.EarlyLearningCourseType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ELTeachersProficiencies {

	private Map<ProficiencyType, List<String>> proficiencyToTeacherMap = new HashMap<>();
	private Map<String, ProficiencyType> redisKeyToProficiencyMap = new HashMap<>();

	public void addKeyToProficiency(String key, ProficiencyType proficiencyType) {
		redisKeyToProficiencyMap.put(key, proficiencyType);
	}

	public void setTeachersForProficiecy(String key, List<String> teachers) {
		proficiencyToTeacherMap.put(redisKeyToProficiencyMap.get(key), teachers);
	}

	public void setTeachersForProficiecy(ProficiencyType proficiencyType, List<String> teachers) {
		proficiencyToTeacherMap.put(proficiencyType, teachers);
	}

	public List<String> getTeachersForProficiency(ProficiencyType proficiencyType) {
		return Optional.ofNullable(proficiencyToTeacherMap.get(proficiencyType)).orElseGet(ArrayList::new);
	}

	public void setTeachersForProficiency(ELProficiencyUserPojo proficiency) {
		setTeachersForProficiecy(proficiency.getProficiencyType(), proficiency.getTeacherIds());
	}

	public List<String> getElTeachersByGrade(int grade) {
		List<String> teacherIds = new ArrayList<>();
		TeacherCategory.getGradeType(grade).getProficiencies().forEach(proficiencyType -> {
			teacherIds.addAll(
					Optional.ofNullable(proficiencyToTeacherMap.get(proficiencyType)).orElseGet(ArrayList::new));
		});
		return teacherIds;
	}

	public boolean teachersAvailable() {
		return proficiencyToTeacherMap.entrySet().stream().filter(nonNullAndEmpty).count() > 0;
	}

	public ProficiencyType getTeacherProficiencyType(String presenter) {

		if (getTeachersForProficiency(ProficiencyType.L).contains(presenter)) {
			return ProficiencyType.L;
		} else if (getTeachersForProficiency(ProficiencyType.LH).contains(presenter)) {
			return ProficiencyType.LH;
		} else if (getTeachersForProficiency(ProficiencyType.HL).contains(presenter)) {
			return ProficiencyType.HL;
		} else {
			return ProficiencyType.H;
		}
	}

	private transient final Predicate<Map.Entry<ProficiencyType, List<String>>> nonNullAndEmpty = e -> e.getValue() != null
			&& !e.getValue().isEmpty();

	public static String getELRedisKey(SessionLabel earlyLearningCourseType,
									   ProficiencyType proficiencyType) {
		return "EL_TEACHERS_" + earlyLearningCourseType.name() + "_" + proficiencyType.getDefinition()
				+ "-Proficiency_Type";
	}
}
