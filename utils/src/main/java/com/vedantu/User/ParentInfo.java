package com.vedantu.User;
import com.vedantu.User.enums.ParentType;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

public class ParentInfo {
    private ParentType type;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String firstName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String lastName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String fullName;
    private String email;
    private boolean emailVerified;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String contactNumber;
    private String phoneCode;
    private boolean contactNumberVerified;
    private String skypeId;
    private boolean primaryContactPerson;
   
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ParentType getType() {
        return type;
    }

    public void setType(ParentType type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public boolean isContactNumberVerified() {
        return contactNumberVerified;
    }

    public void setContactNumberVerified(boolean contactNumberVerified) {
        this.contactNumberVerified = contactNumberVerified;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public boolean isPrimaryContactPerson() {
        return primaryContactPerson;
    }

    public void setPrimaryContactPerson(boolean primaryContactPerson) {
        this.primaryContactPerson = primaryContactPerson;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "ParentInfo{" + "type=" + type + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", emailVerified=" + emailVerified + ", contactNumber=" + contactNumber + ", phoneCode=" + phoneCode + ", contactNumberVerified=" + contactNumberVerified + ", skypeId=" + skypeId + ", primaryContactPerson=" + primaryContactPerson + '}';
    }
 
}
