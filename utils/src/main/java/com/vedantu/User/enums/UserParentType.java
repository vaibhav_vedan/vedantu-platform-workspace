package com.vedantu.User.enums;

public enum UserParentType {
	
	FATHER, MOTHER, GUARDIAN;
}
