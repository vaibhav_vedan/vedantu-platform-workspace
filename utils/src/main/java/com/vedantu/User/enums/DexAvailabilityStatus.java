package com.vedantu.User.enums;

public enum DexAvailabilityStatus {
    AVAILABLE, NOT_AVAILABLE
}
