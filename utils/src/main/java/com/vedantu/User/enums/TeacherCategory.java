package com.vedantu.User.enums;

import lombok.NoArgsConstructor;


import java.util.Arrays;
import java.util.List;

@NoArgsConstructor
public enum TeacherCategory {

	PRIMARY_GRADE(Arrays.asList(ProficiencyType.L, ProficiencyType.LH, ProficiencyType.HL)),
	SECONDARY_GRADE(Arrays.asList(ProficiencyType.H, ProficiencyType.HL, ProficiencyType.LH));

	private List<ProficiencyType> proficiencies;

	TeacherCategory(List<ProficiencyType> proficiencies) {
		this.proficiencies = proficiencies;
	}

	public List<ProficiencyType> getProficiencies(){
		return this.proficiencies;
	}

	public static TeacherCategory getGradeType(int grade) {
		if (grade > 6) {
			return TeacherCategory.SECONDARY_GRADE;
		}
		return TeacherCategory.PRIMARY_GRADE;
	}

	//TODO Constants

	public static enum ProficiencyType {
		L(Arrays.asList("1", "2", "3", "4", "5", "6"),"PRIMARY_GRADE"),  // Being expert only in handling primary grade students
		LH(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8"),"PRIMARY_AND_SECONDARY_GRADE"), // Being expert in handling primary grade students vs secondary grade
		HL(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8"),"SECONDARY_AND_PRIMARY_GRADE"), // Being expert in handling secondary grade students vs primary grade
		H(Arrays.asList("7", "8"),"SECONDARY_GRADE"); // Being expert only in handling secondary grade students
	
		ProficiencyType(List<String> grades, String definition) {
			this.proficiencyGrades = grades;
			this.definition = definition;
		}

		ProficiencyType ( ) {

		}
		private List<String> proficiencyGrades;
		private String definition;

		public List<String> getProficiencyGrades() {
			return proficiencyGrades;
		}
		
		public String getDefinition() {
			return definition;
		}

	}

}
