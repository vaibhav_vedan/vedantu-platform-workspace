package com.vedantu.User.enums;

public enum ParentType {
	FATHER, MOTHER, GUARDIAN, OTHERS
}