package com.vedantu.User.enums;

public enum VoltStatus {
    REGISTERED, PENDING, REJECTED
}