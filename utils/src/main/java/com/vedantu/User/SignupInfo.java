/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User;
import java.util.List;
/**
 *
 * @author Anirban
 */
public class SignupInfo extends AbstractInfo{
    
    private String signUpUrl;
    private FeatureSource signupFeature;

    public SignupInfo() {
        super();
    }
    
    public String getSignUpUrl() {
        return signUpUrl;
    }

    public void setSignUpUrl(String signUpUrl) {
        this.signUpUrl = signUpUrl;
    }

    public FeatureSource getSignupFeature() {
        return signupFeature;
    }

    public void setSignupFeature(FeatureSource signupFeature) {
        this.signupFeature = signupFeature;
    }
    
    @Override
    public List<String> collectVerificationErrors() {
        return null;
    }
}
