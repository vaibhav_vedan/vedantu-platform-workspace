package com.vedantu.User;

import com.vedantu.User.enums.SubRole;
import java.util.List;

import com.vedantu.board.pojo.Board;
import com.vedantu.session.pojo.CumilativeRating;
import com.vedantu.session.pojo.EntityRatingInfo;
import com.vedantu.util.StringUtils;
import java.util.Set;

public class UserInfo extends UserBasicInfo {

    private String tempContactNumber;
    private String tempPhoneCode;
    private Boolean profileEnabled;
    private List<String> languagePrefs;
    private AbstractInfo info;
    private LocationInfo locationInfo;
    private SocialInfo socialInfo;
    private Boolean isEmailVerified;
    private Boolean isContactNumberVerified;
    private UTMParams utm;
    private EntityRatingInfo ratingInfo;
    private Boolean parentRegistration;
    private Board primaryBoard;
    private Board secondaryBoard;
    private SignupInfo signupInfo;
    private String referralCode;
    private TeacherInfo teacherInfo;
    private StudentInfo studentInfo;
    private boolean telegramLinked = false;


    public UserInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void setTempContactNumber(String number) {
        this.tempContactNumber = number;
    }

    public String getTempContactNumber() {
        return tempContactNumber;
    }

    public void setTempPhoneCode(String number) {
        this.tempPhoneCode = number;
    }

    public String getTempPhoneCode() {
        return tempPhoneCode;
    }

    public void setInfo(AbstractInfo info) {
        this.info = info;
    }

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public Board getPrimaryBoard() {
        return primaryBoard;
    }

    public void setPrimaryBoard(Board primaryBoard) {
        this.primaryBoard = primaryBoard;
    }

    public Board getSecondaryBoard() {
        return secondaryBoard;
    }

    public void setSecondaryBoard(Board secondaryBoard) {
        this.secondaryBoard = secondaryBoard;
    }

    public Boolean getParentRegistration() {
        return parentRegistration;
    }

    public UserInfo(User user, CumilativeRating rating, boolean exposeEmail) {
        super(user, exposeEmail);
        if (null == user) {
            return;
        }
        this.profileEnabled = user.getProfileEnabled();

        this.languagePrefs = user.getLanguagePrefs();
        this.locationInfo = user.getLocationInfo();
        this.socialInfo = user.getSocialInfo();
        this.telegramLinked = user.isTelegramLinked();

        if (Role.TEACHER.equals(super.getRole())) {
            this.info = user.getTeacherInfo();
        } else if (super.getRole() == Role.STUDENT) {
            this.info = user.getStudentInfo();
        }
        this.isEmailVerified = user.getIsEmailVerified();
        this.isContactNumberVerified = user.getIsContactNumberVerified();
        this.utm = new UTMParams();
        this.utm.setUtm_campaign(user.getUtm_campaign());
        this.utm.setUtm_source(user.getUtm_source());
        this.utm.setUtm_medium(user.getUtm_medium());
        this.utm.setUtm_content(user.getUtm_content());
        this.utm.setUtm_term(user.getUtm_term());
        this.utm.setChannel(user.getChannel());
        SignupInfo _signUpInfo = new SignupInfo();
        _signUpInfo.setSignUpUrl(user.getSignUpURL());
        _signUpInfo.setSignupFeature(user.getSignUpFeature());
        this.signupInfo = _signUpInfo;
        if (rating != null) {
            this.ratingInfo = new EntityRatingInfo(rating);
        }
        this.parentRegistration = user.getParentRegistration();
        this.referralCode = user.getReferralCode();
    }

    public AbstractInfo getInfo() {

        return info;
    }

    public LocationInfo getLocationInfo() {

        return locationInfo;
    }

    public List<String> getLanguagePrefs() {

        return languagePrefs;
    }

    public void setSocialInfo(SocialInfo socialInfo) {

        this.socialInfo = socialInfo;
    }

    public SocialInfo getSocialInfo() {

        return socialInfo;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public Boolean getIsContactNumberVerified() {
        return isContactNumberVerified;
    }

    public UTMParams getUtm() {
        return utm;
    }

    public EntityRatingInfo getRatingInfo() {
        return ratingInfo;
    }

    public void setRatingInfo(EntityRatingInfo ratingInfo) {
        this.ratingInfo = ratingInfo;
    }

    public Boolean getProfileEnabled() {
        return profileEnabled;
    }

    public SignupInfo getSignupInfo() {
        return signupInfo;
    }

    public void setSignupInfo(SignupInfo signupInfo) {
        this.signupInfo = signupInfo;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String _getFullName() {
        return (super.getFirstName() + " " + StringUtils.defaultIfEmpty(super.getLastName())).trim();
    }

    /**
     * @return the telegramLinked
     */
    public boolean isTelegramLinked() {
        return telegramLinked;
    }

    /**
     * @param telegramLinked the telegramLinked to set
     */
    public void setTelegramLinked(boolean telegramLinked) {
        this.telegramLinked = telegramLinked;
    }

    public Set<SubRole> getSubRoles() {
        Set<SubRole> subRoles = null;
        if (teacherInfo != null) {
            subRoles = teacherInfo.getSubRole();
        }
        return subRoles;
    }


}
