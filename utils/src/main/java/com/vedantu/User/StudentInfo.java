package com.vedantu.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.Size;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.pojo.DeviceType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentInfo extends AbstractInfo {

    private static final long serialVersionUID = 1L;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String grade;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String board;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String school;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String target;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private List<String> examTargets = new ArrayList<>();
    
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String stream;

    private List<ParentInfo> parentInfos;
    private Long lastUpdated;
    private Boolean updateNeeded = false;
    
    private String introduceText;
    private String interests;
    private String pastPerformance;
    private String schoolActivities;
    private String strengths;
    private String previousGradeScore;
    private List<String> preferredTimeForLiveClasses;
    private DeviceType deviceType;
    public String getParentContactNo() {

        if (ArrayUtils.isNotEmpty(parentInfos)) {
            for (ParentInfo parentInfo : parentInfos) {
                if (parentInfo.isPrimaryContactPerson()) {
                    return parentInfo.getContactNumber();
                }
            }
            return parentInfos.get(0).getContactNumber();
        }

        return "";
    }

    public Set<String> getParentContactNumbers() {
        Set<String> result = new HashSet<>();
        if (ArrayUtils.isNotEmpty(parentInfos)) {
            for (ParentInfo parentInfo : parentInfos) {
                result.add(parentInfo.getContactNumber());
            }
        }
        return result;
    }

    public Set<String> getParentContactNumbers(String contactNumberToRemove) {
        Set<String> result = new HashSet<>();
        if (StringUtils.isNotEmpty(contactNumberToRemove)) {
            result.remove(contactNumberToRemove);
        }
        return result;
    }

    public Set<String> getParentEmails() {
        Set<String> result = new HashSet<>();
        if (ArrayUtils.isNotEmpty(parentInfos)) {
            for (ParentInfo parentInfo : parentInfos) {
                result.add(parentInfo.getEmail());
            }
        }
        return result;
    }

    public Set<String> getParentEmails(String emailToRemove) {
        Set<String> result = getParentEmails();
        if (StringUtils.isNotEmpty(emailToRemove)) {
            result.remove(emailToRemove);
        }
        return result;
    }

    public String getParentFirstName() {
        if (ArrayUtils.isNotEmpty(parentInfos)) {
            for (ParentInfo parentInfo : parentInfos) {
                if (parentInfo.isPrimaryContactPerson()) {
                    return parentInfo.getFirstName();
                }
            }
            return parentInfos.get(0).getFirstName();
        }
        return "";
    }

    public String getParentLastName() {
        if (ArrayUtils.isNotEmpty(parentInfos)) {
            for (ParentInfo parentInfo : parentInfos) {
                if (parentInfo.isPrimaryContactPerson()) {
                    return parentInfo.getLastName();
                }
            }
            return parentInfos.get(0).getFirstName();
        }
        return "";
    }

    public Boolean isEmpty() {
        return (StringUtils.isEmpty(grade)
                && StringUtils.isEmpty(board)
                && StringUtils.isEmpty(school)
                && examTargets.isEmpty()
                && ArrayUtils.isEmpty(parentInfos));
    }

    @Override
    public List<String> collectVerificationErrors() {

        List<String> errors = new ArrayList<>();
        return errors;
    }

    public List<ParentInfo> getParentInfos() {
        return parentInfos;
    }

    public static class Constants {

        public static final String GRADE = "grade";
        public static final String BOARD = "board";
        public static final String SCHOOL = "school";
        public static final String DEVICE_TYPE = "deviceType";
        public static final String TARGET = "target";
        public static final String STREAM = "stream";

        public static final String PARENT_CONTACT_NO = "parentContactNo";
    }
}
