package com.vedantu.User;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.User.enums.ViewerAccess;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.interfaces.IEntity;
import com.vedantu.util.pojo.DeviceType;

public class UserBasicInfo implements IEntity {

    private String rollNumber;
    private Long userId;
    private String email;
    private String contactNumber;
    private String phoneCode;
    private String firstName;
    private String lastName;
    private String fullName;
    private Role role;
    private String profilePicUrl;
    private String grade;
    private String primaryCallingNumberForTeacher;
    private Integer extensionNumber;
    private Gender gender;
    private TeacherPoolType teacherPoolType;
    private Long creationTime;
    private String school;
    private List<ViewerAccess> viewerAccesses;
    private String target;
    private String orgId;
    private DeviceType deviceType;
    private String board;
    private String stream;
    private String teacherSliderPicUrl;

    public UserBasicInfo() {

    }

    public UserBasicInfo(User user, boolean exposeEmail) {

        if (null == user) {
            return;
        }
        this.userId = user.getId();
        if (exposeEmail) {
            this.contactNumber = user.getContactNumber();
            this.phoneCode = user.getPhoneCode();
            this.email = user.getEmail();
            if (Role.TEACHER.equals(user.getRole())) {
                this.primaryCallingNumberForTeacher = UserUtils.INSTANCE
                        .getPrimaryCallingNumberForTeacher(user.getTeacherInfo().getPrimaryCallingNumberCode());
                this.extensionNumber = user.getTeacherInfo().getExtensionNumber();
            }
        }
        this.creationTime = user.getCreationTime();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.fullName = user.getFullName();
        this.creationTime = user.getCreationTime();
        this.role = user.getRole();
        this.profilePicUrl = StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
                : UserUtils.INSTANCE.toProfilePicUrl(user.getProfilePicPath());
        this.grade = (user.getStudentInfo() != null ? user.getStudentInfo().getGrade() : "");
        this.target = (user.getStudentInfo() != null ? user.getStudentInfo().getTarget() : "");
        this.school = (user.getStudentInfo() != null ? user.getStudentInfo().getSchool() : "");
        this.gender = user.getGender();
        if (Role.TEACHER.equals(user.getRole()) && user.getTeacherInfo().getDexInfo() != null) {
            this.teacherPoolType = user.getTeacherInfo().getDexInfo().getTeacherPoolType();
            this.viewerAccesses = user.getTeacherInfo().getDexInfo().getViewerAccesses();
        }
        this.rollNumber = user.getRollNumber();
        this.orgId = user.getOrgId();
        this.deviceType = (user.getStudentInfo() != null ? user.getStudentInfo().getDeviceType() : null);
        this.board = (user.getStudentInfo() != null ? user.getStudentInfo().getBoard() : "");
        this.stream = (user.getStudentInfo() != null ? user.getStudentInfo().getStream() : "");
        this.teacherSliderPicUrl = (user.getTeacherInfo() != null ? user.getTeacherInfo().getSliderPicUrl(): null);
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getPrimaryCallingNumberForTeacher() {
        return primaryCallingNumberForTeacher;
    }

    public void setPrimaryCallingNumberForTeacher(String primaryCallingNumberForTeacher) {
        this.primaryCallingNumberForTeacher = primaryCallingNumberForTeacher;
    }

    public Integer getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(Integer extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getSchool() {
        return school;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public void updateUserDetails(UserBasicInfo userBasicInfo) {
        if (this.userId == null
                || (userBasicInfo.getUserId() != null && userBasicInfo.getUserId().equals(this.userId))) {
            this.email = userBasicInfo.getEmail();
            this.contactNumber = userBasicInfo.getContactNumber();
            this.phoneCode = userBasicInfo.getPhoneCode();
            this.firstName = userBasicInfo.getFirstName();
            this.lastName = userBasicInfo.getLastName();
            this.fullName = userBasicInfo.getFullName();
            this.role = userBasicInfo.getRole();
            this.profilePicUrl = userBasicInfo.getProfilePicUrl();
            this.grade = userBasicInfo.getGrade();
            this.primaryCallingNumberForTeacher = userBasicInfo.getPrimaryCallingNumberForTeacher();
            this.extensionNumber = userBasicInfo.getExtensionNumber();
            this.gender = userBasicInfo.getGender();
            this.orgId = userBasicInfo.getOrgId();
            this.board = userBasicInfo.getBoard();
            this.target = userBasicInfo.getTarget();
            this.stream = userBasicInfo.getStream();
        }
    }

    public void purgeEmail() {
        this.contactNumber = null;
        this.phoneCode = null;
        this.email = null;
        if (Role.TEACHER.equals(this.role)) {
            this.primaryCallingNumberForTeacher = null;
            this.extensionNumber = null;
        }
    }

    @Override
    public void preStore() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postStore() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the teacherPoolType
     */
    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    /**
     * @param teacherPoolType the teacherPoolType to set
     */
    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public DeviceType getDeviceType() { return deviceType; }

    public void setDeviceType(DeviceType deviceType) { this.deviceType = deviceType; }

	public List<ViewerAccess> getViewerAccesses() {
		return viewerAccesses;
	}

	public void setViewerAccesses(List<ViewerAccess> viewerAccesses) {
		this.viewerAccesses = viewerAccesses;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

    public String getTeacherSliderPicUrl() {
        return teacherSliderPicUrl;
    }

    public void setTeacherSliderPicUrl(String teacherSliderPicUrl) {
        this.teacherSliderPicUrl = teacherSliderPicUrl;
    }
}
