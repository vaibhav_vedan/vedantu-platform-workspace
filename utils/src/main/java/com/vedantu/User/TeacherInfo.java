package com.vedantu.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.data.mongodb.core.index.Indexed;

import com.vedantu.User.enums.SubRole;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.User.enums.TeacherCategoryType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidStringList;

public class TeacherInfo extends AbstractInfo {

    //TODO: Ask front end to make changes to handle changes from Text to String
    // payoutRate in paisa
    private Integer payoutRate;
    private TeacherCurrentStatus currentStatus = TeacherCurrentStatus.ACTIVE;
    // chargerate per session per hour in paisa
    private Integer chargeRate;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String experience;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String latestEducation;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String latestJob;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String subjectExpertise;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String journey;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String philosophy;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String interests;

    private Long primarySubject;
    private Long secondarySubject;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String biggestStrength;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String professionalCategory;

    @ValidStringList(message = ReqLimitsErMsgs.STRING_LIST_MAX)
    private List<String> grades;

    @ValidStringList(message = ReqLimitsErMsgs.STRING_LIST_MAX)
    private List<String> categories;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String awards;
    private Integer primaryCallingNumberCode;

    @Indexed(background = true) //(unique=true)this won't be created because for students it will be empty and it will exception
    private Integer extensionNumber;

    private Long responseTime;
    private Boolean active;
    private Long sessionHours = 0l;
    private Long sessions = 0l;
    private Long noOfSubscriptions = 0l;
    private Float retainabilityIndex = 100f;
    private Float punctualityIndex = 100f;
    private Float regularityIndex = 100f;
    private Float contentSharingIndex = 0f;
    private Long responseTimeIndex = 0l;
    private Long usersChattedWith = 0l;

    private DexInfo dexInfo;
    private Set<SubRole> subRole;
    private Set<TeacherCategoryType> teacherCategoryTypes;
    private List<EarlyLearningCourseType> earlyLearningCourses;
    private ProficiencyType proficiencyType;
    private String sliderPicUrl;

    //Teacher Image URL centralised for web
    private String thumbnailUrlWeb;
    //Teacher Image URL centralised for app
    private String thumbnailUrlApp;


    //Cropped Image of Teacher's Face

    private String teacherSmallImage;

    //Transparent image of Teacher- Full Body

    private String transparentImage;

    //Teacher's salient points

    @ValidStringList(message = ReqLimitsErMsgs.REASON_STRING_LIST_MAX)
    private List<@Size(max = ReqLimits.NAME_TYPE_MAX) String> salientPoints;

    @Valid
    private List<TeacherContentLink> contentLinks = new ArrayList<>();
    

    public TeacherInfo() {

        super();
    }

    public Integer getPayoutRate() {
        return payoutRate;
    }

    public void setPayoutRate(Integer payoutRate) {
        this.payoutRate = payoutRate;
    }

    public Integer getChargeRate() {
        return chargeRate == null ? 0 : chargeRate;
    }

    public void setChargeRate(Integer chargeRate) {
        this.chargeRate = chargeRate;
    }

    public String getExperience() {

        return experience;
    }

    public void setExperience(String experience) {

        this.experience = experience;
    }

    public String getSubjectExpertise() {

        return subjectExpertise;
    }

    public void setSubjectExpertise(String subjectExpertise) {

        this.subjectExpertise = subjectExpertise;
    }

    public String getJourney() {

        return journey;
    }

    public void setJourney(String journey) {

        this.journey = journey;
    }

    public String getPhilosophy() {

        return philosophy;
    }

    public void setPhilosophy(String philosophy) {

        this.philosophy = philosophy;
    }

    public String getInterests() {

        return interests;
    }

    public void setInterests(String interests) {

        this.interests = interests;
    }

    public Long getPrimarySubject() {
        return primarySubject;
    }

    public void setPrimarySubject(Long primarySubject) {
        this.primarySubject = primarySubject;
    }

    public Long getSecondarySubject() {
        return secondarySubject;
    }

    public void setSecondarySubject(Long secondarySubject) {
        this.secondarySubject = secondarySubject;
    }

    public String getBiggestStrength() {
        return biggestStrength;
    }

    public void setBiggestStrength(String biggestStrength) {
        this.biggestStrength = biggestStrength;
    }

    public String getProfessionalCategory() {
        return professionalCategory;
    }

    public void setProfessionalCategory(String professionalCategory) {
        this.professionalCategory = professionalCategory;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public Integer getPrimaryCallingNumberCode() {
        return primaryCallingNumberCode;
    }

    public void setPrimaryCallingNumberCode(Integer primaryCallingNumberCode) {
        this.primaryCallingNumberCode = primaryCallingNumberCode;
    }

    public Integer getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(Integer extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {

        this.awards = awards;
    }

    // public Qualification getEducation() {
    // return education;
    // }
    //
    // public void setEducation(Qualification education) {
    // this.education = education;
    // }
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getLatestJob() {
        return latestJob;
    }

    public String getLatestEducation() {
        return this.latestEducation;
    }

    public void setLatestEducation(String latestEducation) {
        this.latestEducation = latestEducation;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public void setLatestJob(String latestJob) {

        this.latestJob = latestJob;
    }

    public Long getSessionHours() {
        if (sessionHours == null || sessionHours < 0) {
            return Long.valueOf(0);
        } else {
            return sessionHours;
        }
    }

    public void setSessionHours(Long sessionHours) {
        this.sessionHours = sessionHours;
    }

    public Long getSessions() {
        if (sessions == null || sessions < 0) {
            return Long.valueOf(0);
        } else {
            return sessions;
        }
    }

    public void setSessions(Long sessions) {
        this.sessions = sessions;
    }

    public Long getNoOfSubscriptions() {
        return noOfSubscriptions;
    }

    public void setNoOfSubscriptions(Long noOfSubscriptions) {
        this.noOfSubscriptions = noOfSubscriptions;
    }

    public Float getRetainabilityIndex() {
        return retainabilityIndex;
    }

    public void setRetainabilityIndex(Float retainabilityIndex) {
        this.retainabilityIndex = retainabilityIndex;
    }

    public Float getPunctualityIndex() {
        return punctualityIndex;
    }

    public void setPunctualityIndex(Float punctualityIndex) {
        this.punctualityIndex = punctualityIndex;
    }

    public Float getRegularityIndex() {
        return regularityIndex;
    }

    public void setRegularityIndex(Float regularityIndex) {
        this.regularityIndex = regularityIndex;
    }

    public Float getContentSharingIndex() {
        return contentSharingIndex;
    }

    public void setContentSharingIndex(Float contentSharingIndex) {
        this.contentSharingIndex = contentSharingIndex;
    }

    public Long getResponseTimeIndex() {
        return responseTimeIndex;
    }

    public void setResponseTimeIndex(Long responseTimeIndex) {
        this.responseTimeIndex = responseTimeIndex;
    }

    public Long getUsersChattedWith() {
        return usersChattedWith;
    }

    public void setUsersChattedWith(Long usersChattedWith) {
        this.usersChattedWith = usersChattedWith;
    }

    public TeacherCurrentStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(TeacherCurrentStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    public List<TeacherContentLink> getContentLinks() {
        return contentLinks;
    }

    public void setContentLinks(List<TeacherContentLink> contentLinks) {
        this.contentLinks = contentLinks;
    }

    public DexInfo getDexInfo() {
        return dexInfo;
    }

    public void setDexInfo(DexInfo dexInfo) {
        this.dexInfo = dexInfo;
    }

    public Set<SubRole> getSubRole() {
        return subRole;
    }

    public void setSubRole(Set<SubRole> subRole) {
        this.subRole = subRole;
    }

    public Set<TeacherCategoryType> getTeacherCategoryTypes() {
        return teacherCategoryTypes;
    }

    public void setTeacherCategoryTypes(Set<TeacherCategoryType> teacherCategoryTypes) {
        this.teacherCategoryTypes = teacherCategoryTypes;
    }

    public List<EarlyLearningCourseType> getEarlyLearningCourses() {
        return earlyLearningCourses;
    }

    public void setEarlyLearningCourses(List<EarlyLearningCourseType> earlyLearningCourses) {
        this.earlyLearningCourses = earlyLearningCourses;
    }

    public ProficiencyType getProficiencyType() {
        return proficiencyType;
    }

    public void setProficiencyType(ProficiencyType proficiencyType) {
        this.proficiencyType = proficiencyType;
    }

    public String getSliderPicUrl() { return sliderPicUrl; }

    public void setSliderPicUrl(String sliderPicUrl) { this.sliderPicUrl = sliderPicUrl; }

    @Override
	public String toString() {
		return "TeacherInfo [payoutRate=" + payoutRate + ", currentStatus=" + currentStatus + ", chargeRate="
				+ chargeRate + ", experience=" + experience + ", latestEducation=" + latestEducation + ", latestJob="
				+ latestJob + ", subjectExpertise=" + subjectExpertise + ", journey=" + journey + ", philosophy="
				+ philosophy + ", interests=" + interests + ", primarySubject=" + primarySubject + ", secondarySubject="
				+ secondarySubject + ", biggestStrength=" + biggestStrength + ", professionalCategory="
				+ professionalCategory + ", grades=" + grades + ", categories=" + categories + ", awards=" + awards
				+ ", primaryCallingNumberCode=" + primaryCallingNumberCode + ", extensionNumber=" + extensionNumber
				+ ", responseTime=" + responseTime + ", active=" + active + ", sessionHours=" + sessionHours
				+ ", sessions=" + sessions + ", noOfSubscriptions=" + noOfSubscriptions + ", retainabilityIndex="
				+ retainabilityIndex + ", punctualityIndex=" + punctualityIndex + ", regularityIndex=" + regularityIndex
				+ ", contentSharingIndex=" + contentSharingIndex + ", responseTimeIndex=" + responseTimeIndex
				+ ", usersChattedWith=" + usersChattedWith + ", dexInfo=" + dexInfo + ", subRole=" + subRole
				+ ", teacherCategoryTypes=" + teacherCategoryTypes + ", earlyLearningCourses=" + earlyLearningCourses
				+ ", proficiencyType=" + proficiencyType + ", thumbnailUrlWeb=" + thumbnailUrlWeb + ", thumbnailUrlApp="
				+ thumbnailUrlApp + ", teacherSmallImage=" + teacherSmallImage + ", transparentImage="
				+ transparentImage + ", salientPoints=" + salientPoints + ", contentLinks=" + contentLinks + "]";
	}

    @Override
    public List<String> collectVerificationErrors() {

        List<String> errors = new ArrayList<>();

        if (primarySubject == null) {
            errors.add(Constants.PRIMARY_SUBJECT);
        }
        if (StringUtils.isEmpty(biggestStrength)) {
            errors.add(Constants.BIGGEST_STRENGTH);
        }
        if (grades == null || grades.isEmpty()) {
            if(proficiencyType != null) {
                grades = proficiencyType.getProficiencyGrades();
            }else {
                errors.add(Constants.GRADES);
            }
        }
        if (categories == null || categories.isEmpty()) {
            errors.add(Constants.CATEGORIES);
        }
        if (responseTime == null || responseTime == 0) {
            errors.add(Constants.RESPONSE_TIME);
        }

        return errors;
    }

    public String getThumbnailUrlWeb() {
		return thumbnailUrlWeb;
	}

	public void setThumbnailUrlWeb(String thumbnailUrlWeb) {
		this.thumbnailUrlWeb = thumbnailUrlWeb;
	}

	public String getThumbnailUrlApp() {
		return thumbnailUrlApp;
	}

	public void setThumbnailUrlApp(String thumbnailUrlApp) {
		this.thumbnailUrlApp = thumbnailUrlApp;
	}

	public String getTeacherSmallImage() {
		return teacherSmallImage;
	}

	public void setTeacherSmallImage(String teacherSmallImage) {
		this.teacherSmallImage = teacherSmallImage;
	}

	public String getTransparentImage() {
		return transparentImage;
	}

	public void setTransparentImage(String transparentImage) {
		this.transparentImage = transparentImage;
	}

	public List<String> getSalientPoints() {
		return salientPoints;
	}

	public void setSalientPoints(List<String> salientPoints) {
		this.salientPoints = salientPoints;
	}

	public static class Constants {

        public static final String LATEST_EDUCATION = "latestEducation";
        public static final String LATEST_JOB = "latestJob";
        public static final String PAYOUT_RATE = "payoutRate";
        public static final String SUBJECT_EXPERTISE = "subjectExpertise";
        public static final String JOURNEY = "journey";
        public static final String PHILOSOPHY = "philosophy";
        public static final String PRIMARY_NUMBER_CODE = "primaryCallingNumberCode";
        public static final String EXTENSION_NUMBER = "extensionNumber";
        public static final String PRIMARY_SUBJECT = "primarySubject";
        public static final String GRADES = "grades";
        public static final String CATEGORIES = "categories";
        public static final String BIGGEST_STRENGTH = "biggestStrength";
        public static final String RESPONSE_TIME = "responseTime";
        public static final String DEX_INFO = "dexInfo";
        public static final String SUB_ROLE = "subRole";
        public static final String DEX_INFO_TEACHER_POOL_TYPE = "dexInfo.teacherPoolType";
        public static final String DEX_AVAILABILITY_STATUS = "dexInfo.avaiblabilityStatus";
        public static final String DEX_VIEWER_ACCESSES = "dexInfo.viewerAccesses";
        public static final String TEACHER_CATEGORY_TYPES = "teacherCategoryTypes";
        public static final String EARLY_LEARNING_TYPES = "earlyLearningCourses";
        public static final String PROFICIENCY_TYPE = "proficiencyType";
        public static final String TEACHER_IMAGE_URL_WEB = "thumbnailUrlWeb";
        public static final String TEACHER_IMAGE_URL_APP = "thumbnailUrlApp";
        public static final String TEACHER_IDS = "teacherIds";
        public static final String ID = "_id";
        public static final String SLIDER_PIC_URL = "sliderPicUrl";
	}


}
