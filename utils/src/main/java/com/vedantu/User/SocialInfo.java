package com.vedantu.User;

import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

public class SocialInfo {

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String linkedIn;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String facebook;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String twitter;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String profileLine;

    public String getLinkedIn() {

        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {

        this.linkedIn = linkedIn;
    }

    public String getFacebook() {

        return facebook;
    }

    public void setFacebook(String facebook) {

        this.facebook = facebook;
    }

    public String getTwitter() {

        return twitter;
    }

    public void setTwitter(String twitter) {

        this.twitter = twitter;
    }

    public String getProfileLine() {

        return profileLine;
    }

    public void setProfileLine(String profileLine) {

        this.profileLine = profileLine;
    }

    @Override
    public String toString() {

        return "{linkedIn:" + linkedIn + ", facebook:" + facebook
                + ", twitter:" + twitter + ", profileLine:" + profileLine + "}";
    }

    public List<String> collectVerificationErrors() {

        return new ArrayList<>();
    }
}
