package com.vedantu.User;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.LocationAddedBy;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

public class LocationInfo {

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String country;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String state;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String city;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String streetAddress;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String streetAddressLine2;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String pincode;//or zip code

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String isp;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String queryStatusCode;

    private LocationAddedBy addedBy;

    public LocationInfo() {
        super();
    }

    public LocationInfo(String country, String state, String city) {

        super();

        this.country = country;
        this.state = state;
        this.city = city;
    }

    public String getQueryStatusCode() {
        return queryStatusCode;
    }

    public void setQueryStatusCode(String queryStatusCode) {
        this.queryStatusCode = queryStatusCode;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }

    public String getState() {

        return state;
    }

    public void setState(String state) {

        this.state = state;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {

        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getStreetAddressLine2() {
        return streetAddressLine2;
    }

    public void setStreetAddressLine2(String streetAddressLine2) {
        this.streetAddressLine2 = streetAddressLine2;
    }

    public String getPincode() {

        return pincode;
    }

    public void setPincode(String pincode) {

        this.pincode = pincode;
    }

    public LocationAddedBy getAddedBy() {
    	return addedBy;
    }
    
    public void setAddedBy(LocationAddedBy system) {
    	this.addedBy = system;
    }
    
    public List<String> collectVerificationErrors() {

        List<String> errors = new ArrayList<>();

        if (StringUtils.isEmpty(country)) {
            errors.add(Constants.COUNTRY);
        }

        if (StringUtils.isEmpty(state)) {
            errors.add(Constants.STATE);
        }

        if (StringUtils.isEmpty(city)) {
            errors.add(Constants.CITY);
        }

        return errors;
    }

    public static class Constants {

        public static final String COUNTRY = "country";
        public static final String STATE = "state";
        public static final String CITY = "city";
    }

    @Override
    public String toString() {
        return "LocationInfo{" + "country=" + country + ", state=" + state + ", city=" + city + ", streetAddress=" + streetAddress + ", streetAddressLine2=" + streetAddressLine2 + ", pincode=" + pincode + '}';
    }

}
