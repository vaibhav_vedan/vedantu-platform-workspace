/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User;

/**
 *
 * @author somil
 */
public enum VerificationTokenType {
	EMAIL_VERIFICATION,
	SMS_VERIFICATION,
	EMAIL_VERIFICATION_SIGNUP, //this will be used when a user signup as public(not added by admin)
	RESET_PASSWORD,
        ISL_REG_FOR_REGISTERED_USERS;
}
