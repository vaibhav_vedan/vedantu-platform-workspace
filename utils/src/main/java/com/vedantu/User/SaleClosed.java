/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User;

import com.vedantu.User.enums.EntityModel;
import com.vedantu.session.pojo.EntityType;

/**
 *
 * @author pranavm
 */
public class SaleClosed {

    private Long closedTime;
    private EntityModel model;
    private Integer promotionalAmount;
    private Integer nonPromotionalAmount;
    private EntityType entityType;
    private String entityId;
    private Integer vedantuDiscount;
    private String ownerEmail;
    private String ownerName;
    private String lastWebinarAttended;
    private String orderId;

    public SaleClosed() {

    }

    public SaleClosed(Long closedTime, EntityModel model, Integer promotionalAmount,
            Integer nonPromotionalAmount, EntityType entityType, String entityId, String orderId) {
        this.closedTime = closedTime;
        this.model = model;
        this.promotionalAmount = promotionalAmount;
        this.nonPromotionalAmount = nonPromotionalAmount;
        this.entityType = entityType;
        this.entityId = entityId;
        this.orderId = orderId;
    }

    public Long getClosedTime() {
        return closedTime;
    }

    public void setClosedTime(Long closedTime) {
        this.closedTime = closedTime;
    }

    public EntityModel getModel() {
        return model;
    }

    public void setModel(EntityModel model) {
        this.model = model;
    }

    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Integer getVedantuDiscount() {
        return vedantuDiscount;
    }

    public void setVedantuDiscount(Integer vedantuDiscount) {
        this.vedantuDiscount = vedantuDiscount;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getLastWebinarAttended() {
        return lastWebinarAttended;
    }

    public void setLastWebinarAttended(String lastWebinarAttended) {
        this.lastWebinarAttended = lastWebinarAttended;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "SaleClosed{" + "closedTime=" + closedTime + ", model=" + model + ", promotionalAmount=" + promotionalAmount + ", nonPromotionalAmount=" + nonPromotionalAmount + ", entityType=" + entityType + ", entityId=" + entityId + ", vedantuDiscount=" + vedantuDiscount + ", ownerEmail=" + ownerEmail + ", ownerName=" + ownerName + ", lastWebinarAttended=" + lastWebinarAttended + ", orderId=" + orderId + '}';
    }

}
