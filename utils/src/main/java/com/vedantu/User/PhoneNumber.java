package com.vedantu.User;

public class PhoneNumber implements IPhoneNumber {

    private String number;
    private String phoneCode;
    private Boolean isVerified;
    private Boolean isDND;
    private Boolean isWhitelisted;
    private Boolean isActiveForCall;

    public PhoneNumber() {
        super();
    }

    public PhoneNumber(String number, Boolean isVerified, Boolean isDND,
            Boolean isWhitelisted, Boolean isActiveForCall) {
        super();
        this.number = number;
        this.isVerified = isVerified;
        this.isDND = isDND;
        this.isWhitelisted = isWhitelisted;
        this.isActiveForCall = isActiveForCall;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public Boolean getIsDND() {
        return isDND;
    }

    public void setIsDND(Boolean isDND) {
        this.isDND = isDND;
    }

    public Boolean getIsWhitelisted() {
        return isWhitelisted;
    }

    public void setIsWhitelisted(Boolean isWhitelisted) {
        this.isWhitelisted = isWhitelisted;
    }

    public Boolean getIsActiveForCall() {
        return isActiveForCall;
    }

    public void setIsActiveForCall(Boolean isActiveForCall) {
        this.isActiveForCall = isActiveForCall;
    }

    @Override
    public String _getNumber() {
        return number;
    }

    @Override
    public String _getPhoneCode() {
        return phoneCode;
    }

    @Override
    public Boolean _getIsVerified() {
        return isVerified;
    }

    @Override
    public Boolean _getIsDND() {
        return isDND;
    }

    @Override
    public Boolean _getIsWhitelisted() {
        return isWhitelisted;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" + "number=" + number + ", phoneCode=" + phoneCode + ", isVerified=" + isVerified + ", isDND=" + isDND + ", isWhitelisted=" + isWhitelisted + ", isActiveForCall=" + isActiveForCall + '}';
    }

}
