/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 *
 * @author somil
 */
public class AbstractInfoAdapter implements JsonDeserializer<AbstractInfo>{

    @Override
    public AbstractInfo deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement grade = jsonObject.get("grade");
        String className = StudentInfo.class.getCanonicalName();
        if(grade==null) {
            className = TeacherInfo.class.getCanonicalName();
        }
 
        try {
            return context.deserialize(json, Class.forName(className));
        } catch (ClassNotFoundException cnfe) {
            throw new JsonParseException("Unknown element type: " + className, cnfe);
        }
    }
    
}
