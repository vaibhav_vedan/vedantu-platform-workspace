package com.vedantu.User;

public interface IPhoneNumber {

	public String _getNumber();
        
        public String _getPhoneCode();

	public Boolean _getIsVerified();

	public Boolean _getIsDND();

	public Boolean _getIsWhitelisted();
}
