package com.vedantu.User;

import com.vedantu.User.enums.SubRole;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class User implements IPhoneNumber {

    private Long id;
    private Long creationTime;
    private Long lastUpdated;
    private String rollNumber;
    private Boolean profileEnabled = Boolean.TRUE;
    private String userId;
    private String email;
    private String contactNumber;
    private String tempContactNumber;
    private String phoneCode;
    private String tempPhoneCode;
    private String firstName;
    private String lastName;
    private String fullName;
    private Gender gender;
    private Role role;
    private Boolean isEmailVerified = Boolean.FALSE;
    private Long profilePicId;
    private String profilePicUrl; // this will be used in case of social signup
    private String profilePicPath;
    private List<String> languagePrefs;
    private StudentInfo studentInfo = new StudentInfo();
    private TeacherInfo teacherInfo;
    private LocationInfo locationInfo;
    private SocialInfo socialInfo;
    private String tncVersion;
    private String socialSource;
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private Long appId;
    private Boolean isContactNumberVerified;
    private Boolean isContactNumberDND;
    private Boolean isContactNumberWhitelisted;
    private String referralCode;
    private String referrerCode;
    // userId of the referrer user
    private Long referrerId;
    // bonus in paisa
    private Integer referrerBonus;
    private Boolean referrerSuccess;
    private List<PhoneNumber> phones = new ArrayList<>();
    private boolean isNewUser;
    private Boolean parentRegistration;
    private String signUpURL;
    private FeatureSource signUpFeature;
    private String signUpFeatureRefId;
    private String ipAddress;
    private Long otfSubscriptionCount = 0l;
    private Long subscriptionRequestCount = 0l;
    private String deviceId;
    private Boolean passwordAutogenerated;
    private SaleClosed saleClosed;
    private String referrer;
    private boolean telegramLinked = false;
    private boolean userInProcessOfOnboarding;
    private String orgId;
    private Boolean isEmailConflict;

    public User(String email, String contactNumber, String tempContactNumber, String firstName, String lastName, String password, Gender gender,
            Role role, LocationInfo locationInfo) {
        super();
        this.email = email;
        this.contactNumber = contactNumber;
        this.tempContactNumber = tempContactNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.role = role;
        this.locationInfo = locationInfo;
    }

    public String getTncVersion() {
        return tncVersion;
    }

    public void setTncVersion(String tncVersion) {
        this.tncVersion = tncVersion;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String _getFullName() {

        return (firstName + " " + StringUtils.defaultIfEmpty(lastName)).trim();
    }

    @Override
    public String _getNumber() {
        return contactNumber;
    }

    @Override
    public String _getPhoneCode() {
        return phoneCode;
    }

    @Override
    public Boolean _getIsVerified() {
        return isContactNumberVerified;
    }

    @Override
    public Boolean _getIsDND() {
        return isContactNumberDND;
    }

    @Override
    public Boolean _getIsWhitelisted() {
        return isContactNumberWhitelisted;
    }


    public IPhoneNumber _getActivePhoneNumberForCall() {

        try {
            for (PhoneNumber phone : getPhones()) {
                if (phone.getIsActiveForCall() != null && phone.getIsActiveForCall()) {
                    return phone;
                }
            }
        } catch (Throwable e) {
            Logger.getLogger("User").throwing("User", "_getActivePhoneNumberForCall", e);
            e.printStackTrace();
        }

        return this;
    }

    public static class Constants extends AbstractMongoEntityBean.Constants {

        public static final String EMAIL = "email";
        public static final String NAME = "name";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String FULL_NAME = "fullName";
        public static final String PASSWORD = "password";
        public static final String OLD_PASSWORD = "oldPassword";
        public static final String ROLE = "role";
        public static final String CONTACT_NUMBER = "contactNumber";
        public static final String PHONE_CODE = "phoneCode";
        public static final String IS_CONTACT_NUMBER_DND = "isContactNumberDND";
        public static final String IS_CONTACT_NUMBER_WHITELISTED = "isContactNumberWhitelisted";
        public static final String GENDER = "gender";
        public static final String IS_EMAIL_VERIFIED = "isEmailVerified";
        public static final String PROFILE_PIC_ID = "profilePicId";
        public static final String TNC_VERSION = "tncVersion";
        public static final String REFERRER_ID = "referrerId";
        public static final String SIGNUP_FEATURE = "signUpFeature";
        public static final String SIGNUP_URL = "signUpURL";
        public static final String REFERRAL_CODE = "referralCode";
        public static final String STUDENTINFO_GRADE = "studentInfo.grade";
        public static final String STUDENTINFO_BOARD = "studentInfo.board";
        public static final String STUDENTINFO_EXAM_TARGET = "studentInfo.examTargets";
        public static final String STUDENTINFO_PARENT_INFOS = "studentInfo.parentInfos";
        public static final String LANGUAGE_PREFS = "languagePrefs";
        public static final String LOCATION_INFO = "locationInfo";
        public static final String SOCIAL_INFO = "socialInfo";
        public static final String PROFILE_PIC_URL = "profilePicUrl";
        public static final String PROFILE_PIC_PATH = "profilePicPath";
        public static final String TEACHER_INFO = "teacherInfo";
    }

    public Set<SubRole> getSubRoles() {
        Set<SubRole> subRoles = null;
        if (teacherInfo != null) {
            subRoles = teacherInfo.getSubRole();
        }
        return subRoles;
    }

}
