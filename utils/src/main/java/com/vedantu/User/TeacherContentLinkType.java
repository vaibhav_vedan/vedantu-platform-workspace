package com.vedantu.User;

/**
 * Created by somil on 21/02/17.
 */
public enum TeacherContentLinkType {
    SLIDES, VIDEO, AUDIO, IMAGE
}
