package com.vedantu.User;

import java.util.Arrays;
import java.util.List;

import com.vedantu.User.enums.DexAvailabilityStatus;
import com.vedantu.User.enums.DexType;
import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.User.enums.ViewerAccess;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

public class DexInfo extends AbstractTargetTopicEntity {

    private TeacherPoolType teacherPoolType;

    private DexAvailabilityStatus avaiblabilityStatus = DexAvailabilityStatus.NOT_AVAILABLE;

    private DexType dexType;

    private String alternateEmail;
    private List<ViewerAccess> viewerAccesses;
    private String accountName;
    private String accountNumber;
    private String bankName;
    private String branchName;
    private String ifsc;
    private String panNumber;
    private String canceledChequeUrl;

    public DexAvailabilityStatus getAvaiblabilityStatus() {
        return avaiblabilityStatus;
    }

    public void setAvaiblabilityStatus(DexAvailabilityStatus avaiblabilityStatus) {
        this.avaiblabilityStatus = avaiblabilityStatus;
    }

    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getCanceledChequeUrl() {
        return canceledChequeUrl;
    }

    public void setCanceledChequeUrl(String canceledChequeUrl) {
        this.canceledChequeUrl = canceledChequeUrl;
    }

    public DexType getDexType() {
        return dexType;
    }

    public void setDexType(DexType dexType) {
        this.dexType = dexType;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public void setAlternateEmail(String alternateEmail) {
        this.alternateEmail = alternateEmail;
    }

	public List<ViewerAccess> getViewerAccesses() {
		return viewerAccesses;
	}

	public void setViewerAccesses(List<ViewerAccess> viewerAccesses) {
		this.viewerAccesses = viewerAccesses;
	}

}
