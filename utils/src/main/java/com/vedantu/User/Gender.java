package com.vedantu.User;

public enum Gender {
	MALE {
		@Override
		public String getPronoun() {

			return "his";
		}
	},
	FEMALE {
		@Override
		public String getPronoun() {

			return "her";
		}
	},
	OTHER {
		@Override
		public String getPronoun() {

			return "their";
		}
	},
	UNKNOWN {
		@Override
		public String getPronoun() {

			return null;
		}
	};
	
	public static Gender valueOfKey(String key){
		Gender gender = UNKNOWN;
		try{
			gender = Gender.valueOf(key.trim().toUpperCase());
		}catch(Throwable t){}
		return gender;
		
	}

	public abstract String getPronoun();
}
