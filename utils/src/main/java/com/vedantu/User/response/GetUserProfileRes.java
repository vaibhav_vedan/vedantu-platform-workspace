/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.User;
import com.vedantu.User.UserInfo;
import com.vedantu.session.pojo.CumilativeRating;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetUserProfileRes extends UserInfo {

    private List<PhoneNumberRes> phones;

    public GetUserProfileRes() {
    }

    public GetUserProfileRes(User user, CumilativeRating rating, boolean exposeEmail) {

        super(user, rating, exposeEmail);
    }

    public void setPhones(List<PhoneNumberRes> phones) {
        this.phones = phones;
    }

    public List<PhoneNumberRes> getPhones() {
        return phones;
    }

}
