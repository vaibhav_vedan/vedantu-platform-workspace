/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class SetProfilePicRes extends AbstractRes {

	private String profilePicUrl;

	public SetProfilePicRes(){
		super();
	}

	public SetProfilePicRes(String profilePicUrl) {
		super();
		this.profilePicUrl = profilePicUrl;
	}

	public String getProfilePicUrl() {

		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl){
		this.profilePicUrl = profilePicUrl;
	}

	@Override
	public String toString() {

		return "SetProfilePicRes{" +
				"profilePicUrl='" + profilePicUrl + '\'' +
				"} " + super.toString();
	}
}
