/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetActiveTutorsRes extends AbstractRes {

    private List<TeacherBasicInfo> users;
    private Boolean hasNext = Boolean.FALSE;

    public List<TeacherBasicInfo> getUsers() {
        return users;
    }

    public void setUsers(List<TeacherBasicInfo> users) {
        this.users = users;
    }

    public void addUser(TeacherBasicInfo user) {
        if (users == null) {
            users = new ArrayList<TeacherBasicInfo>();
        }
        users.add(user);
    }

    public Boolean getHasNext() {
        return hasNext;
    }

    public void setHasNext(Boolean hasNext) {
        this.hasNext = hasNext;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{users=").append(users).append("}");
        return builder.toString();
    }
}

