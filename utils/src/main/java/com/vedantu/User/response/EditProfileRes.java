/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.TeacherInfo;
import com.vedantu.User.User;
import com.vedantu.User.UserInfo;

/**
 *
 * @author somil
 */
public class EditProfileRes extends UserInfo {
    
        TeacherInfo prevTeacherInfo;
      
	public EditProfileRes(User user) {
		super(user, null, false);
	}

    public TeacherInfo getPrevTeacherInfo() {
        return prevTeacherInfo;
    }

    public void setPrevTeacherInfo(TeacherInfo prevTeacherInfo) {
        this.prevTeacherInfo = prevTeacherInfo;
    }

   
       
    
               
}
