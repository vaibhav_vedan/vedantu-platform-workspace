/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.UserInfo;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class EditContactNumberRes extends AbstractRes {

	private String contactNumber;
        private String phoneCode;
	private boolean isDND;
        private boolean isVerified;
        UserInfo userInfo;
        String mobileTokenCode;
        String utm_campaign;

	public EditContactNumberRes(String contactNumber, String phoneCode, boolean isDND, boolean isVerified) {
		super();
		this.contactNumber = contactNumber;
                this.phoneCode = phoneCode;
		this.isDND = isDND;
                this.isVerified = isVerified;
	}

    public EditContactNumberRes() {
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getMobileTokenCode() {
        return mobileTokenCode;
    }

    public void setMobileTokenCode(String mobileTokenCode) {
        this.mobileTokenCode = mobileTokenCode;
    }
    
    
        
        

    public boolean isIsDND() {
        return isDND;
    }

    public void setIsDND(boolean isDND) {
        this.isDND = isDND;
    }

    public boolean isIsVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }
        
        

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public boolean isDND() {
		return isDND;
	}

	public void setDND(boolean isDND) {
		this.isDND = isDND;
	}

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }
    
    
        
        

	@Override
	public String toString() {
		return String.format("{contactNumber=%s, isDND=%s}", contactNumber,
				isDND);
	}

}

