/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class ConnectSessionCallRes extends AbstractRes {

	private String callSid;

	public ConnectSessionCallRes() {
		super();
	}

	public ConnectSessionCallRes(String callSid) {
		super();
		this.callSid = callSid;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	@Override
	public String toString() {
		return String.format("{callSid=%s}", callSid);
	}

}
