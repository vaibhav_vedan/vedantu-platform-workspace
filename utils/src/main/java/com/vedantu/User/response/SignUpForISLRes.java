/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author ajith
 */
public class SignUpForISLRes extends AbstractRes {

    private SignUpForISLResResult result;
    private Long userId;
    private UserDetailsInfo userDetailsInfo;

    public SignUpForISLRes() {
    }

    public SignUpForISLResResult getResult() {
        return result;
    }

    public void setResult(SignUpForISLResResult result) {
        this.result = result;
    }

    public UserDetailsInfo getUserDetailsInfo() {
        return userDetailsInfo;
    }

    public void setUserDetailsInfo(UserDetailsInfo userDetailsInfo) {
        this.userDetailsInfo = userDetailsInfo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    public enum SignUpForISLResResult {
        EMAILS_NOT_MATCHED, PASSWORDS_NOT_MATCHED, PASSWORDS_MATCHED, PASSWORD_RESET_DONE
    }
}
