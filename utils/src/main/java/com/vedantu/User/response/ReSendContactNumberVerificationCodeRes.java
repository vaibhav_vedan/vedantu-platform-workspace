/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.UserInfo;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class ReSendContactNumberVerificationCodeRes extends AbstractRes {

	private boolean sended;
        private UserInfo userInfo;
        private String mobileTokenCode;
        String utm_campaign;

	public boolean isSended() {
		return sended;
	}

	public void setSended(boolean sended) {
		this.sended = sended;
	}

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getMobileTokenCode() {
        return mobileTokenCode;
    }

    public void setMobileTokenCode(String mobileTokenCode) {
        this.mobileTokenCode = mobileTokenCode;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }
    
    
    
        
        

	@Override
	public String toString() {
		return String.format("{sended=%s}", sended);
	}

}
