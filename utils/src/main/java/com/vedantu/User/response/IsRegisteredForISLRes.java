/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.enums.VoltStatus;
import com.vedantu.util.fos.response.AbstractRes;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author ajith
 */
@AllArgsConstructor
public class IsRegisteredForISLRes extends AbstractRes {

    private boolean isRegistered;
    private Long userId;
    private VoltStatus voltStatus;

    public IsRegisteredForISLRes() {
    }

    public IsRegisteredForISLRes(boolean isRegistered, Long userId) {
        this.isRegistered = isRegistered;
        this.userId = userId;
    }

    public boolean isIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public VoltStatus getVoltStatus() {
        return voltStatus;
    }

    public void setVoltStatus(VoltStatus voltStatus) {
        this.voltStatus = voltStatus;
    }
}
