/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.UserInfo;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetUsersRes extends AbstractRes {

    private List<UserInfo> users=new ArrayList<>();
    
    public GetUsersRes() {
    }    

    public List<UserInfo> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfo> users) {
        this.users = users;
    }

    public void addUser(UserInfo user) {
        if (this.users == null) {
            this.users = new ArrayList<>();
        }
        this.users.add(user);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{users=").append(users).append("}");
        return builder.toString();
    }

}
