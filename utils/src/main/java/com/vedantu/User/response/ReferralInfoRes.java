/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.UserInfo;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class ReferralInfoRes extends AbstractRes {
    
    private UserInfo userInfo;
    private UserInfo referrerUserInfo;

    public ReferralInfoRes(UserInfo userInfo, UserInfo referrerUserInfo) {
        super();
        this.userInfo = userInfo;
        this.referrerUserInfo = referrerUserInfo;
    }
    
    public ReferralInfoRes() {
        super();
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserInfo getReferrerUserInfo() {
        return referrerUserInfo;
    }

    public void setReferrerUserInfo(UserInfo referrerUserInfo) {
        this.referrerUserInfo = referrerUserInfo;
    }

    @Override
    public String toString() {
        return "ReferralInfoRes{" + "userInfo=" + userInfo + ", referrerUserInfo=" + referrerUserInfo + '}';
    }
    
    
    
    
}
