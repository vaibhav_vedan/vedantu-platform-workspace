/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class StatusCounterResponse extends AbstractRes {

	private Long memberCount;
	private Long sessionDurationCount;

	public StatusCounterResponse() {

	}

	public StatusCounterResponse(Long memberCount, Long sessionDurationCount) {
		super();
		this.memberCount = memberCount;
		this.sessionDurationCount = sessionDurationCount;
	}

	public Long getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(Long memberCount) {
		this.memberCount = memberCount;
	}

	public Long getSessionDurationCount() {
		return sessionDurationCount;
	}

	public void setSessionDurationCount(Long sessionDurationCount) {
		this.sessionDurationCount = sessionDurationCount;
	}

	@Override
	public String toString() {
		return String.format("{memberCount=%s, sessionDurationCount=%s}", memberCount, sessionDurationCount);
	}

}
