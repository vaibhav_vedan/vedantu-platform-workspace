/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.User.User;

/**
 *
 * @author somil
 */
public class ProcessVerifyContactNumberResponse extends AbstractRes{
	private User user;
    private HttpSessionData userDetails;
    private String number;
    private String phoneCode;
    private Boolean allowFreebies = false;
    private Boolean processReferralBonus = false;
    private Boolean emailVerifed = false;
    private boolean verified;
    private boolean whitelisted;
    private boolean dnd;
    private String contactNumber;
    private Boolean emailOTPPasswordVerified;

    public Boolean getEmailOTPPasswordVerified() {
        return emailOTPPasswordVerified;
    }

    public void setEmailOTPPasswordVerified(Boolean emailOTPPasswordVerified) {
        this.emailOTPPasswordVerified = emailOTPPasswordVerified;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Boolean getEmailVerifed() {
        return emailVerifed;
    }

    public void setEmailVerifed(Boolean emailVerifed) {
        this.emailVerifed = emailVerifed;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }
    
    

    public Boolean getAllowFreebies() {
        return allowFreebies;
    }

    public void setAllowFreebies(Boolean allowFreebies) {
        this.allowFreebies = allowFreebies;
    }

    public Boolean getProcessReferralBonus() {
        return processReferralBonus;
    }

    public void setProcessReferralBonus(Boolean processReferralBonus) {
        this.processReferralBonus = processReferralBonus;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isWhitelisted() {
        return whitelisted;
    }

    public void setWhitelisted(boolean whitelisted) {
        this.whitelisted = whitelisted;
    }

    public boolean isDnd() {
        return dnd;
    }

    public void setDnd(boolean dnd) {
        this.dnd = dnd;
    }

	public HttpSessionData getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(HttpSessionData userDetails) {
		this.userDetails = userDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
