/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.User;

/**
 *
 * @author somil
 */
public class ProcessVerificationResponse {

    private VerificationStatus status;
    private Boolean sendWelcomeMail = false;
    private Boolean processReferralBonus = false;
    //TODO: Discuss changing this to userBasicInfo
    private User user;
    private Long userId;

    public ProcessVerificationResponse() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getProcessReferralBonus() {
        return processReferralBonus;
    }

    public void setProcessReferralBonus(Boolean processReferralBonus) {
        this.processReferralBonus = processReferralBonus;
    }

    public Boolean getSendWelcomeMail() {
        return sendWelcomeMail;
    }

    public void setSendWelcomeMail(Boolean sendWelcomeMail) {
        this.sendWelcomeMail = sendWelcomeMail;
    }

    public VerificationStatus getStatus() {
        return status;
    }

    public void setStatus(VerificationStatus status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ProcessVerificationResponse{" + "status=" + status + ", sendWelcomeMail=" + sendWelcomeMail + ", processReferralBonus=" + processReferralBonus + ", user=" + user + ", userId=" + userId + '}';
    }

}
