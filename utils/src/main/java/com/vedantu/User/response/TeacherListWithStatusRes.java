package com.vedantu.User.response;

import com.vedantu.User.TeacherCurrentStatus;

public class TeacherListWithStatusRes {

	private Long teacherId;
	private TeacherCurrentStatus currentStatus;
	private Long primarySubject;
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public TeacherCurrentStatus getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(TeacherCurrentStatus currentStatus) {
		this.currentStatus = currentStatus;
	}
	public Long getPrimarySubject() {
		return primarySubject;
	}
	public void setPrimarySubject(Long primarySubject) {
		this.primarySubject = primarySubject;
	}
}
