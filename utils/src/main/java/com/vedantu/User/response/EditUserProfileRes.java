/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.TeacherInfo;
import com.vedantu.User.User;
import com.vedantu.User.UserInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EditUserProfileRes extends UserInfo {
	public EditUserProfileRes(User user) {
		super(user, null, false);
	}
}
