/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;

public class UserSystemIntroRes extends AbstractRes {

    private String id;
    private Long userId;
    private String type;

    
    private String status;
    private String data;
    private String identifier;

    public UserSystemIntroRes(String id, Long userId, String type, String status, String data, String identifier) {
        super();
        this.id = id;
        this.userId = userId;
        this.type = type;
        this.status = status;
        this.data = data;
        this.identifier = identifier;
    }
    
    public UserSystemIntroRes(){
        super();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        return String
                .format("{id=%s, userId=%s, type=%s, status=%s, data=%s, identifier=%s}",
                        id, userId, type, status, data, identifier);
    }

}
