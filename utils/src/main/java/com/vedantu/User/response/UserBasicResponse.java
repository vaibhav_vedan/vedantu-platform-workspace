package com.vedantu.User.response;

import com.vedantu.User.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserBasicResponse
{
    private boolean success;
    private List<User> users;
    private String errorMessage;
}
