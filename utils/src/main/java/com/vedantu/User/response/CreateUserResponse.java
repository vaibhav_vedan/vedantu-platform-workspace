/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.User;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class CreateUserResponse extends AbstractRes {

    User user;
    String mobileTokenCode;
    String emailTokenCode;
    Boolean userDetailCreated = Boolean.FALSE;

    public CreateUserResponse(User user, String mobileTokenCode, String emailTokenCode) {
        this.user = user;
        this.mobileTokenCode = mobileTokenCode;
        this.emailTokenCode = emailTokenCode;
    }
    
    public CreateUserResponse() {
        super();
    }
    
    

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMobileTokenCode() {
        return mobileTokenCode;
    }

    public void setMobileTokenCode(String mobileTokenCode) {
        this.mobileTokenCode = mobileTokenCode;
    }

    public String getEmailTokenCode() {
        return emailTokenCode;
    }

    public void setEmailTokenCode(String emailTokenCode) {
        this.emailTokenCode = emailTokenCode;
    }

	public Boolean getUserDetailCreated() {
		return userDetailCreated;
	}

	public void setUserDetailCreated(Boolean userDetailCreated) {
		this.userDetailCreated = userDetailCreated;
	}

}
