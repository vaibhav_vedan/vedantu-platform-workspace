/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.PhoneNumber;
import com.vedantu.User.UserInfo;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class PhoneNumberRes extends AbstractRes {

	private String number;
        private String phoneCode;
	private Boolean isDND;
	private Boolean isWhitelisted;
	private Boolean isVerified;
	private Boolean isActiveForCall;
        private UserInfo userInfo;
        private String mobileTokenCode;
        private String utm_campaign;

	public PhoneNumberRes(PhoneNumber phone) {
		super();
		this.number = phone.getNumber();
                this.phoneCode = phone.getPhoneCode();
		this.isDND = phone.getIsDND();
		this.isWhitelisted = phone.getIsWhitelisted();
		this.isVerified = phone.getIsVerified();
		this.isActiveForCall = phone.getIsActiveForCall();
	}

	public PhoneNumberRes(String number, String phoneCode, Boolean isDND, Boolean isWhitelisted,
			Boolean isVerified, Boolean isActiveForCall) {
		super();
		this.number = number;
                this.phoneCode = phoneCode;
		this.isDND = isDND;
		this.isWhitelisted = isWhitelisted;
		this.isVerified = isVerified;
		this.isActiveForCall = isActiveForCall;
	}

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getMobileTokenCode() {
        return mobileTokenCode;
    }

    public void setMobileTokenCode(String mobileTokenCode) {
        this.mobileTokenCode = mobileTokenCode;
    }
        
        
        

	public String getNumber() {
		return number;
	}

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }
        
        

	public Boolean getIsDND() {
		return isDND;
	}

	public Boolean getIsWhitelisted() {
		return isWhitelisted;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public Boolean getIsActiveForCall() {
		return isActiveForCall;
	}

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }
        
        

	@Override
	public String toString() {
		return String
				.format("{number=%s, isDND=%s, isWhitelisted=%s, isVerified=%s, isActiveForCall=%s}",
						number, isDND, isWhitelisted, isVerified,
						isActiveForCall);
	}

}
