/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.util.fos.response.AbstractRes;
import com.vedantu.util.security.HttpSessionData;

/**
 *
 * @author somil
 */
public class VerifyContactNumberRes extends AbstractRes {

	private boolean verified;
	private boolean whitelisted;
	private boolean dnd;
	private String number;
	private HttpSessionData user;

	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getNumber() {
		return number;
	}
	
	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public boolean isWhitelisted() {
		return whitelisted;
	}

	public void setWhitelisted(boolean whitelisted) {
		this.whitelisted = whitelisted;
	}

	public boolean isDnd() {
		return dnd;
	}

	public void setDnd(boolean dnd) {
		this.dnd = dnd;
	}

	@Override
	public String toString() {
		return String.format("{verified=%s, whitelisted=%s, dnd=%s}", verified,
				whitelisted, dnd);
	}

	public HttpSessionData getUser() {
		return user;
	}

	public void setUser(HttpSessionData user) {
		this.user = user;
	}

}

