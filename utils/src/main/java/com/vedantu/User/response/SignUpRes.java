/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class SignUpRes extends AbstractRes {

    private Long userId;
    private String email;
    private String firstName;
    private String lastName;
    private Role role;
    private Boolean appliedCoupon;
    private Long creationTime;
    private String profilePicUrl;
    private String contactNumber;
    private String phoneCode;
    private Boolean isSignUpURLSet;
    
    public SignUpRes() {
    }

    public SignUpRes(User user) {

        if(null == user) {
            return;
        }
        this.userId = user.getId();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.role = user.getRole();
        this.user = user;
        this.creationTime = user.getCreationTime();
        this.profilePicUrl = StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
				: UserUtils.INSTANCE.toProfilePicUrl(user.getProfilePicPath());
        this.contactNumber = user.getContactNumber();
        this.phoneCode = user.getPhoneCode();
    }

    public Long getUserId() {

        return userId;
    }

    public String getEmail() {

        return email;
    }

    public String getFirstName() {

        return firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public Role getRole() {

        return role;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public Boolean getAppliedCoupon() {
        return appliedCoupon;
    }

    public void setAppliedCoupon(Boolean appliedCoupon) {
        this.appliedCoupon = appliedCoupon;
    }

    // this will be used then the user is created by the system
    private transient User user;

    public User __getUser() {
        return user;
    }

    
    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

	public Boolean getIsSignUpURLSet() {
		return isSignUpURLSet;
	}

	public void setIsSignUpURLSet(Boolean isSignUpURLSet) {
		this.isSignUpURLSet = isSignUpURLSet;
	}

}
