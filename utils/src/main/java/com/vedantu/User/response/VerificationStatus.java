/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

/**
 *
 * @author somil
 */
public enum VerificationStatus {
        VERIFIED,
        INVALID_TOKEN,
        PASSWORD_RESET_SUCCESS,
        SHOW_PASSWORD_RESET
    }
