/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.response;

import com.vedantu.User.enums.LoginTokenContext;

/**
 *
 * @author jeet
 */
public class CreateUserAuthenticationTokenRes {
    
    private LoginTokenContext contextType;
    private String loginToken;
    private String contextId;
    private Long expirationTime;
    private Long userId;
    
    public CreateUserAuthenticationTokenRes() {
    }

    public CreateUserAuthenticationTokenRes(LoginTokenContext contextType, String contextId, Long expirationTime, Long userId) {
        this.contextType = contextType;
        this.contextId = contextId;
        this.expirationTime = expirationTime;
        this.userId = userId;
    }

    public LoginTokenContext getContextType() {
        return contextType;
    }

    public void setContextType(LoginTokenContext contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Long expirationTime) {
        this.expirationTime = expirationTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    @Override
    public String toString() {
        return "CreateUserAuthenticationTokenRes{" + "contextType=" + contextType + ", loginToken=" + loginToken + ", contextId=" + contextId + ", expirationTime=" + expirationTime + ", userId=" + userId + '}';
    }
    
}
