package com.vedantu.User.response;

import com.vedantu.User.Gender;
import com.vedantu.User.ParentInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.UserParentType;
import com.vedantu.User.enums.VoltStatus;
import software.amazon.ion.Decimal;

import java.util.List;
import java.util.Map;

public class UserDetailsInfo {

    private Long userId;
    private String studentName;
    private String email;
    private String studentEmail;
    private String studentPhoneNo;
    private String studentPhoneCode;
    private String school;
    private Integer grade;
    private String board;
    private String country;
    private String parentFirstName;
    private String parentLastName;
    private String parentName;
    private String parentEmail;
    private String parentPhoneNo;
    private String parentPhoneCode;
    private UserParentType parentType;
    private String address;
    private String city;
    private String event;
    private String category;
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private UserBasicInfo student;
    private Long serverTime;
    private String agentName;
    private String agentEmail;
    private List<String> examPreps;
    private String exam;
    private String studentFirstName;
    private String studentLastName;
    private String registerUrl;
    private String socialSource;
    private Gender gender;

    private String voltId;
    private VoltStatus voltStatus;
    private List<String> achievements;
    private List<String> achievementDocments;
    private List<Map<String, String>> idproofs;
    private String state;
    private Long approvedTime;
    private String approvalReason;
    // for volt registered users parents info
    private List<ParentInfo> parentInfo;
    private Long creationTime;
    private String id;
    private Boolean testAttempted;
    private String testId;

    //revise India
    private String dob;
    private String admitCardNumber;
    private String schoolNo;
    private String centreNo;
    private String rollNumber;

    //revise jee
    private String jeeApplicationNumber;
    private Float jeeMainPercentile;

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAdmitCardNumber() {
        return admitCardNumber;
    }

    public void setAdmitCardNumber(String admitCardNumber) {
        this.admitCardNumber = admitCardNumber;
    }

    public String getSchoolNo() {
        return schoolNo;
    }

    public void setSchoolNo(String schoolNo) {
        this.schoolNo = schoolNo;
    }

    public String getCentreNo() {
        return centreNo;
    }

    public void setCentreNo(String centreNo) {
        this.centreNo = centreNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentPhoneNo() {
        return studentPhoneNo;
    }

    public void setStudentPhoneNo(String studentPhoneNo) {
        this.studentPhoneNo = studentPhoneNo;
    }

    public String getStudentPhoneCode() {
        return studentPhoneCode;
    }

    public void setStudentPhoneCode(String studentPhoneCode) {
        this.studentPhoneCode = studentPhoneCode;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getParentFirstName() {
        return parentFirstName;
    }

    public void setParentFirstName(String parentFirstName) {
        this.parentFirstName = parentFirstName;
    }

    public String getParentLastName() {
        return parentLastName;
    }

    public void setParentLastName(String parentLastName) {
        this.parentLastName = parentLastName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentPhoneNo() {
        return parentPhoneNo;
    }

    public void setParentPhoneNo(String parentPhoneNo) {
        this.parentPhoneNo = parentPhoneNo;
    }

    public String getParentPhoneCode() {
        return parentPhoneCode;
    }

    public void setParentPhoneCode(String parentPhoneCode) {
        this.parentPhoneCode = parentPhoneCode;
    }

    public UserParentType getParentType() {
        return parentType;
    }

    public void setParentType(UserParentType parentType) {
        this.parentType = parentType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public UserBasicInfo getStudent() {
        return student;
    }

    public void setStudent(UserBasicInfo student) {
        this.student = student;
    }

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public List<String> getExamPreps() {
        return examPreps;
    }

    public void setExamPreps(List<String> examPreps) {
        this.examPreps = examPreps;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public String getSocialSource() {
        return socialSource;
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public String getVoltId() {
        return voltId;
    }

    public void setVoltId(String voltId) {
        this.voltId = voltId;
    }

    public VoltStatus getVoltStatus() {
        return voltStatus;
    }

    public void setVoltStatus(VoltStatus voltStatus) {
        this.voltStatus = voltStatus;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public List<String> getAchievementDocments() {
        return achievementDocments;
    }

    public void setAchievementDocments(List<String> achievementDocments) {
        this.achievementDocments = achievementDocments;
    }

    public List<Map<String, String>> getIdproofs() {
        return idproofs;
    }

    public void setIdproofs(List<Map<String, String>> idproofs) {
        this.idproofs = idproofs;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(Long approvedTime) {
        this.approvedTime = approvedTime;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    public List<ParentInfo> getParentInfo() {
        return parentInfo;
    }

    public void setParentInfo(List<ParentInfo> parentInfo) {
        this.parentInfo = parentInfo;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getTestAttempted() {
        return testAttempted;
    }

    public void setTestAttempted(Boolean testAttempted) {
        this.testAttempted = testAttempted;
    }


    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Float getJeeMainPercentile() {
        return jeeMainPercentile;
    }

    public void setJeeMainPercentile(Float jeeMainPercentile) {
        this.jeeMainPercentile = jeeMainPercentile;
    }

    public String getJeeApplicationNumber() {
        return jeeApplicationNumber;
    }

    public void setJeeApplicationNumber(String jeeApplicationNumber) {
        this.jeeApplicationNumber = jeeApplicationNumber;
    }

    @Override
    public String toString() {
        return "UserDetailsInfo{" + "userId=" + userId + ", studentName=" + studentName + ", studentEmail=" + studentEmail + ", studentPhoneNo=" + studentPhoneNo + ", studentPhoneCode=" + studentPhoneCode + ", school=" + school + ", grade=" + grade + ", board=" + board + ", country=" + country + ", parentFirstName=" + parentFirstName + ", parentLastName=" + parentLastName + ", parentName=" + parentName + ", parentEmail=" + parentEmail + ", parentPhoneNo=" + parentPhoneNo + ", parentPhoneCode=" + parentPhoneCode + ", parentType=" + parentType + ", address=" + address + ", city=" + city + ", event=" + event + ", category=" + category + ", utm_source=" + utm_source + ", utm_medium=" + utm_medium + ", utm_campaign=" + utm_campaign + ", utm_term=" + utm_term + ", utm_content=" + utm_content + ", channel=" + channel + ", student=" + student + ", serverTime=" + serverTime + '}';
    }
    

}
