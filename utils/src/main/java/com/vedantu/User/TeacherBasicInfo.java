/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User;

import java.util.List;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class TeacherBasicInfo extends AbstractRes {
	private Long userId;
	private String email;
	private String contactNumber;
	private String firstName;
	private String lastName;
	private String fullName;
	private Role role;
	private String profilePicUrl;
	private List<String> grades;
    private Boolean active;
    private String latestEducation;
    private String thumbnailUrlWeb;
    private String thumbnailUrlApp;
    private Long primarySubject;
    private String teacherSmallImage;
    private String transparentImage;
    private List<String> salientPoints;


	public TeacherBasicInfo() {

	}

	public TeacherBasicInfo(User user, boolean exposeEmail) {
            if (null == user) {
                    return;
            }
            this.userId = user.getId();
            if (exposeEmail) {
                    this.contactNumber = user.getContactNumber();
                    this.email = user.getEmail();
            }
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.fullName = user.getFullName();
            this.role = user.getRole();
            this.profilePicUrl = StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
				: UserUtils.INSTANCE.toProfilePicUrl(user.getProfilePicPath());
            if(user.getTeacherInfo()!=null){
                TeacherInfo info = user.getTeacherInfo();
                this.grades = info.getGrades();
                this.active = info.getActive()==null?true:info.getActive();
                this.latestEducation = info.getLatestEducation();
                this.thumbnailUrlWeb = info.getThumbnailUrlWeb();
                this.thumbnailUrlApp = info.getThumbnailUrlApp();
                this.primarySubject = info.getPrimarySubject();
                this.teacherSmallImage = info.getTeacherSmallImage();
                this.transparentImage = info.getTransparentImage();
                this.salientPoints = info.getSalientPoints();
            }
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

        public List<String> getGrades() {
            return grades;
        }

        public void setGrades(List<String> grades) {
            this.grades = grades;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public String getLatestEducation() {
            return latestEducation;
        }

        public void setLatestEducation(String latestEducation) {
            this.latestEducation = latestEducation;
        }



	public String getThumbnailUrlWeb() {
			return thumbnailUrlWeb;
		}

		public void setThumbnailUrlWeb(String thumbnailUrlWeb) {
			this.thumbnailUrlWeb = thumbnailUrlWeb;
		}

		public String getThumbnailUrlApp() {
			return thumbnailUrlApp;
		}

		public void setThumbnailUrlApp(String thumbnailUrlApp) {
			this.thumbnailUrlApp = thumbnailUrlApp;
		}

		public String getTeacherSmallImage() {
			return teacherSmallImage;
		}

		public void setTeacherSmallImage(String teacherSmallImage) {
			this.teacherSmallImage = teacherSmallImage;
		}

		public String getTransparentImage() {
			return transparentImage;
		}

		public void setTransparentImage(String transparentImage) {
			this.transparentImage = transparentImage;
		}


		public Long getPrimarySubject() {
			return primarySubject;
		}

		public void setPrimarySubject(Long primarySubject) {
			this.primarySubject = primarySubject;
		}

	public List<String> getSalientPoints() {
			return salientPoints;
		}

		public void setSalientPoints(List<String> salientPoints) {
			this.salientPoints = salientPoints;
		}

	@Override
	public String toString() {
		return "TeacherBasicInfo [userId=" + userId + ", email=" + email + ", contactNumber=" + contactNumber
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName + ", role=" + role
				+ ", profilePicUrl=" + profilePicUrl + ", grades=" + grades + ", active=" + active
				+ ", latestEducation=" + latestEducation + ", thumbnailUrlWeb=" + thumbnailUrlWeb + ", thumbnailUrlApp="
				+ thumbnailUrlApp + ", primarySubject=" + primarySubject + ", teacherSmallImage=" + teacherSmallImage
				+ ", transparentImage=" + transparentImage + ", salientPoints=" + salientPoints + "]";
	}

}

