/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

/**
 *
 * @author Darshit
 */
public class PaytmUserProfileResp {

	private String email;
	private PaytmPhoneInfo phoneInfo;
	private PaytmProfileInfo profileInfo;


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public PaytmPhoneInfo getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(PaytmPhoneInfo phoneInfo) {
		this.phoneInfo = phoneInfo;
	}

	public PaytmProfileInfo getProfileInfo() {
		return profileInfo;
	}

	public void setProfileInfo(PaytmProfileInfo profileInfo) {
		this.profileInfo = profileInfo;
	}
}

