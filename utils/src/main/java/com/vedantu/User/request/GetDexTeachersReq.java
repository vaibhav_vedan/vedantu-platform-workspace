/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class GetDexTeachersReq extends AbstractFrontEndListReq {
    
    private TeacherPoolType teacherPoolType;
    private Set<Long> userIds;

    /**
     * @return the teacherPoolType
     */
    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    /**
     * @param teacherPoolType the teacherPoolType to set
     */
    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

    /**
     * @return the userIds
     */
    public Set<Long> getUserIds() {
        return userIds;
    }

    /**
     * @param userIds the userIds to set
     */
    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
    
}
