/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class SignInReq extends AbstractFrontEndReq {
	public static final String AUTH_TOKEN = "authToken";
	private String email;
	private String password;
	private String phone;
	private String phoneCode;
	private boolean oauth; // weather this is a oauth request or not

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public boolean isOauth() {
		return oauth;
	}

	public void setOauth(boolean oauth) {
		this.oauth = oauth;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if (null == email && null == phone) {
			errors.add(User.Constants.EMAIL);
			errors.add(User.Constants.CONTACT_NUMBER);
		}
		if (null == password) {
			errors.add(User.Constants.PASSWORD);
		}
		return errors;
	}

	@Override
	public String toString() {
		return "SignInReq{" +
				"email='" + email + '\'' +
				", oauth=" + oauth +
				"} " + super.toString();
	}
}

