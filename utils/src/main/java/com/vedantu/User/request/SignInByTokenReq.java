/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class SignInByTokenReq extends AbstractFrontEndReq {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (null == token) {
            errors.add("token");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "SignInByTokenReq{" + "token=" + token + '}';
    }
    
}
