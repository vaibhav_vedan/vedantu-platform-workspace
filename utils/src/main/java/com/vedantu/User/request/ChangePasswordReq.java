/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author somil
 */
public class ChangePasswordReq extends AbstractFrontEndUserReq {

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String password;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String oldPassword;
    private String otp;
    private boolean reloginRequired;

    public ChangePasswordReq() {

        super();
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getOldPassword() {

        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {

        this.oldPassword = oldPassword;
    }

    public boolean isReloginRequired() {
        return reloginRequired;
    }

    public void setReloginRequired(boolean reloginRequired) {
        this.reloginRequired = reloginRequired;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == getUserId()) {
            errors.add(User.Constants.USER_ID);
        }
        if (StringUtils.isEmpty(password)) {
            errors.add(User.Constants.PASSWORD);
        }
        if (StringUtils.isEmpty(oldPassword) && StringUtils.isEmpty(otp)) {
            errors.add(User.Constants.OLD_PASSWORD);
        }
        return errors;
    }

}
