/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.google.gson.JsonObject;
import com.vedantu.User.Gender;
import com.vedantu.util.StringUtils;
import java.util.logging.Logger;

/**
 *
 * @author somil
 */
public enum SocialSource {
    UNKNOWN {
        @Override
        public SocialUserInfo getSocialUserInfo(JsonObject json,
                String accessToken) {
            return null;
        }
    },
    VEDANTU {
        @Override
        public SocialUserInfo getSocialUserInfo(JsonObject json,
                String accessToken) {
            return null;
        }
    },
    FACEBOOK {
        @Override
        public SocialUserInfo getSocialUserInfo(JsonObject json,
                String accessToken) {

            Logger.getLogger("SocialSource").info(
                    "facebook json response  : " + json);
            SocialUserInfo socialUserInfo = new SocialUserInfo();
            if (json == null || json.get("email") == null) {
                return socialUserInfo;
            }
            socialUserInfo.email = json.get("email").getAsString();
            if (StringUtils.isEmpty(socialUserInfo.email)) {
                return socialUserInfo;
            }
            socialUserInfo.firstName = json.get("first_name") != null ? json
                    .get("first_name").getAsString() : "";
            socialUserInfo.lastName = json.get("last_name") != null ? json.get(
                    "last_name").getAsString() : "";
            socialUserInfo.gender = json.get("gender") == null ? Gender.UNKNOWN
                    : Gender.valueOfKey(json.get("gender").getAsString());
            String uid = json.get("id").getAsString();
            socialUserInfo.uid = uid;
            socialUserInfo.picUrl = String.format(
                    "https://graph.facebook.com/%s/picture?type=large", uid);

            return socialUserInfo;
        }
    },
    GOOGLE {
        @Override
        public SocialUserInfo getSocialUserInfo(JsonObject json,
                String accessToken) {

            Logger.getLogger("SocialSource").info(
                    "google json response  : " + json);
            SocialUserInfo socialUserInfo = new SocialUserInfo();
            if (json == null) {
                return socialUserInfo;
            }
            if (json.get("email") != null && StringUtils.isNotEmpty(json.get("email").getAsString())) {
                socialUserInfo.email = json.get("email").getAsString();
            } else {
                return socialUserInfo;
            }

            if (json.get("given_name") != null && StringUtils.isNotEmpty(json.get("given_name").getAsString())) {
                socialUserInfo.firstName = json.get("given_name").getAsString();
            }
            if (json.get("family_name") != null && StringUtils.isNotEmpty(json.get("family_name").getAsString())) {
                socialUserInfo.lastName = json.get("family_name").getAsString();
            }

            if (json.get("sub") != null && StringUtils.isNotEmpty(json.get("sub").getAsString())) {
                socialUserInfo.uid = json.get("sub").getAsString();
            }

            if (json.get("picture") != null && StringUtils.isNotEmpty(json.get("picture").getAsString())) {
                socialUserInfo.picUrl = json.get("picture").getAsString();
                socialUserInfo.picUrl = !socialUserInfo.picUrl.contains("?") ? socialUserInfo.picUrl + "?sz=200"
                        : socialUserInfo.picUrl.substring(0,
                                socialUserInfo.picUrl.indexOf("?")) + "?sz=200";
            }

            if (json.get("gender") != null && StringUtils.isNotEmpty(json.get("gender").getAsString())) {
                socialUserInfo.gender = Gender.valueOfKey(json.get("gender").getAsString());
            }

            return socialUserInfo;
        }
    };

    public static SocialSource valueOfKey(String key) {
        SocialSource socialSource = UNKNOWN;
        try {
            socialSource = valueOf(key.trim().toUpperCase());
        } catch (Throwable e) {
        }

        return socialSource;
    }

    public abstract SocialUserInfo getSocialUserInfo(JsonObject json,
            String accessToken);
}
