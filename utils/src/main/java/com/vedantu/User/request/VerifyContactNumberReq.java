/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

import javax.validation.Valid;

/**
 *
 * @author somil
 */
public class VerifyContactNumberReq extends AbstractFrontEndReq {

	private String verificationCode;
	private String number;
    private String phoneCode;
    private boolean otpOnly;     

	public String getVerificationCode() {
		return verificationCode.toUpperCase();
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

        public boolean isOtpOnly() {
            return otpOnly;
        }

        public void setOtpOnly(boolean otpOnly) {
            this.otpOnly = otpOnly;
        }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }   

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(verificationCode)) {
			errors.add("verificationCode");
		}
		return errors;
	}

	@Override
	public String toString() {
		return String.format(
				"{verificationCode=%s, number=%s, otpOnly=%s, super.toString():%s}",
				verificationCode, number,otpOnly, super.toString());
	}

}
