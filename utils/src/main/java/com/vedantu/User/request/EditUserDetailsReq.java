package com.vedantu.User.request;

import java.util.List;
import java.util.Map;

import com.vedantu.User.Gender;
import com.vedantu.User.ParentInfo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import software.amazon.ion.Decimal;

import javax.validation.constraints.Size;

public class EditUserDetailsReq extends AbstractFrontEndReq {

    private Long userId;
    private String event;

    private String email;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String studentName;
    private Integer grade;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String schoolName;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String parentFirstName;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String parentLastName;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String phoneCode;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String parentPhoneNo;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String city;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String state;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String board;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String country;

    private List<String> examPreps;
    
    private String exam;

    private List<ParentInfo> parentInfo;

    private Gender gender;

    private List<String> achievements;
    private List<String> achievementDocments;
    private List<Map<String, String>> idproofs;

    private Float jeeMainPercentile;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getParentPhoneNo() {
        return parentPhoneNo;
    }

    public void setParentPhoneNo(String parentPhoneNo) {
        this.parentPhoneNo = parentPhoneNo;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getParentFirstName() {
        return parentFirstName;
    }

    public void setParentFirstName(String parentFirstName) {
        this.parentFirstName = parentFirstName;
    }

    public String getParentLastName() {
        return parentLastName;
    }

    public void setParentLastName(String parentLastName) {
        this.parentLastName = parentLastName;
    }

    public List<String> getExamPreps() {
        return examPreps;
    }

    public void setExamPreps(List<String> examPreps) {
        this.examPreps = examPreps;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }


    public List<ParentInfo> getParentInfo() {
        return parentInfo;
    }

    public void setParentInfo(List<ParentInfo> parentInfo) {
        this.parentInfo = parentInfo;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public List<String> getAchievementDocments() {
        return achievementDocments;
    }

    public void setAchievementDocments(List<String> achievementDocments) {
        this.achievementDocments = achievementDocments;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Map<String, String>> getIdproofs() {
        return idproofs;
    }

    public void setIdproofs(List<Map<String, String>> idproofs) {
        this.idproofs = idproofs;
    }

    public Float getJeeMainPercentile() {
        return jeeMainPercentile;
    }

    public void setJeeMainPercentile(Float jeeMainPercentile) {
        this.jeeMainPercentile = jeeMainPercentile;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(event)) {
            errors.add("event");
        }
        return errors;
    }

}
