/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.User.enums.TeacherPoolType;
import java.util.List;

/**
 *
 * @author somil
 */
public class EditProfileReq extends UserProfileInfoReq {

	private Long userId;
	private Boolean parentRegistration;
	private TeacherPoolType teacherPoolType;
	
	public Boolean getParentRegistration() {
	    return parentRegistration;
	}
	
	public void setParentRegistration(Boolean parentRegistration) {
	    this.parentRegistration = parentRegistration;
	}
        
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
        
	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (userId==null) {
			errors.add(User.Constants.USER_ID);
		}
		return errors;
	}

    /**
     * @return the teacherPoolType
     */
    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    /**
     * @param teacherPoolType the teacherPoolType to set
     */
    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

}
