package com.vedantu.User.request;

import com.vedantu.User.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserContactList
{
    @NotNull
    private Set<String> contactNumbers;

    private String groupId;

    private Role role;
}
