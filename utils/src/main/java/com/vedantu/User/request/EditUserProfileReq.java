package com.vedantu.User.request;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.CustomValidator;
//import org.apache.commons.lang.StringUtils;
import com.vedantu.User.Gender;
import com.vedantu.User.ParentInfo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.pojo.DeviceType;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

@Data
public class EditUserProfileReq extends AbstractFrontEndReq {

    private Long userId;
    private String email;
    private String parentEmail;
    private String contactNumber;
    private String phoneCode;
    private DeviceType deviceType;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId is empty");
        }

        if (StringUtils.isNotEmpty(email)  && !CommonUtils.isValidEmailId(email)) {
            errors.add("Email Id not valid");
        }

        if (StringUtils.isNotEmpty(parentEmail)  && !CommonUtils.isValidEmailId(parentEmail)) {
            errors.add("Parent Email Id not valid");
        }

        if (StringUtils.isNotEmpty(contactNumber) && !CustomValidator.validPhoneNumber(contactNumber))
        {
            errors.add("Phone number not valid");
        }

        return errors;
    }

}
