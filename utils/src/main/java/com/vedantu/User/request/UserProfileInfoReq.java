/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.*;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidStringList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 *
 * @author somil
 */
public class UserProfileInfoReq extends AbstractFrontEndReq {

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String firstName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String lastName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String contactNumber;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String phoneCode;
    private Role role;
    private Gender gender;
    @ValidStringList(message = ReqLimitsErMsgs.STRING_LIST_MAX)
    private List<String> languagePrefs;
    @Valid
    private StudentInfo studentInfo = new StudentInfo();
    @Valid
    private TeacherInfo teacherInfo = new TeacherInfo();
    @Valid
    private LocationInfo locationInfo = new LocationInfo();
    @Valid
    private SocialInfo socialInfo = new SocialInfo();

    public UserProfileInfoReq() {
        super();
    }

    public UserProfileInfoReq(String firstName, String lastName,
            String contactNo, Role role, Gender gender,
            List<String> languagePrefs, StudentInfo studentInfo,
            TeacherInfo teacherInfo, LocationInfo locationInfo,
            SocialInfo socialInfo) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNumber = contactNo;
        this.role = role;
        this.gender = gender;
        this.languagePrefs = languagePrefs;
        this.studentInfo = studentInfo;
        this.teacherInfo = teacherInfo;
        this.locationInfo = locationInfo;
        this.socialInfo = socialInfo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<String> getLanguagePrefs() {
        return languagePrefs;
    }

    public void setLanguagePrefs(List<String> languagePrefs) {
        this.languagePrefs = languagePrefs;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public SocialInfo getSocialInfo() {
        return socialInfo;
    }

    public void setSocialInfo(SocialInfo socialInfo) {
        this.socialInfo = socialInfo;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        /*if (StringUtils.isEmpty(getFirstName())) {
            errors.add(User.Constants.FIRST_NAME);
        }

        if (null == getGender()) {
            errors.add(User.Constants.GENDER);
        }*/

        //&& !Validator.validPhoneNumber(getContactNumber())
        if (StringUtils.isEmpty(getContactNumber()) && !Role.STUDENT.equals(getRole())) {
            errors.add("Invalid " + User.Constants.CONTACT_NUMBER);
        }

        if (getRole() == null) {
            errors.add(User.Constants.ROLE);
        } else if (getRole() == Role.STUDENT && getStudentInfo() != null) {
            errors.addAll(getStudentInfo().collectVerificationErrors());
        } else if (Role.TEACHER.equals(getRole()) && getTeacherInfo() != null) {
            errors.addAll(getTeacherInfo().collectVerificationErrors());
        }

        if (null != getSocialInfo()) {
            errors.addAll(getSocialInfo().collectVerificationErrors());
        }

        return errors;
    }

}
