/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

public class AddUserSystemIntroReq extends GetUserSystemIntroReq {

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

//	public UserSystemIntroData toUserSystemIntroData() {
//		UserSystemIntroData userSystemIntroData = new UserSystemIntroData(
//				getUserId(), getType(), getStatus(), data == null ? null
//						: new Text(data), getIdentifier());
//		return userSystemIntroData;
//	}
}
