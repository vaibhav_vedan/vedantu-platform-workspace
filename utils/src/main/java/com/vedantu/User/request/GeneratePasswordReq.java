/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GeneratePasswordReq extends AbstractFrontEndReq {

    private String email;
    private String adminSecurity;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdminSecurity() {
        return adminSecurity;
    }

    public void setAdminSecurity(String adminSecurity) {
        this.adminSecurity = adminSecurity;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == email) {
            errors.add(User.Constants.EMAIL);
        }
        return errors;
    }
}
