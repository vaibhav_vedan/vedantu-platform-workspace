package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class ManualResetPasswordReq extends AbstractFrontEndReq {

    private Long userId;
    private String secretKey;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        return errors;
    }
}
