/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class StudentTeacherExotelCallConnectReq extends AbstractFrontEndReq {

    private Long teacherId;
    private Long studentId;


    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (studentId == null || studentId < 1) {
            errors.add("studentId");
        }
        if (teacherId == null || teacherId < 1) {
            errors.add("teacherId");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "StudentTeacherExotelCallConnectReq{" + "teacherId=" + teacherId + ", studentId=" + studentId + '}';
    }

}
