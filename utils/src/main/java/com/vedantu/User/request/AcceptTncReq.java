/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class AcceptTncReq extends AbstractFrontEndUserReq {

	private String tncVersion;

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> verificationErrors = super.collectVerificationErrors();
		if (getCallingUserId() == null) {
			verificationErrors.add("callingUserId");
		}

		if (StringUtils.isEmpty(tncVersion)) {
			verificationErrors.add(User.Constants.TNC_VERSION);
		}

		return verificationErrors;
	}


	public String getTncVersion() {
		return tncVersion;
	}

	public void setTncVersion(String tncVersion) {
		this.tncVersion = tncVersion;
	}

}
