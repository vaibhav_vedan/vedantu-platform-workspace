/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author somil
 */
public class LeadSquaredRequest extends AbstractReq {

    private LeadSquaredAction action;
    private LeadSquaredDataType dataType;
    private User user;
    private Map<String, String> params = new HashMap<>();
    private Long callingUserId;

    public LeadSquaredRequest(LeadSquaredAction action, LeadSquaredDataType dataType,
            User user, Map<String, String> params, Long callingUserId) {
        this.action = action;
        this.dataType = dataType;
        this.user = user;
        if (params != null) {
            this.params = params;
        }
        this.callingUserId = callingUserId;
    }

    public LeadSquaredRequest() {
    }

    public LeadSquaredAction getAction() {
        return action;
    }

    public void setAction(LeadSquaredAction action) {
        this.action = action;
    }

    public LeadSquaredDataType getDataType() {
        return dataType;
    }

    public void setDataType(LeadSquaredDataType dataType) {
        this.dataType = dataType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Long getCallingUserId() {
        return callingUserId;
    }

    public void setCallingUserId(Long callingUserId) {
        this.callingUserId = callingUserId;
    }

    public static class Constants {

        public static final String PARENT_NAME = "parentName";
        public static final String PARENT_EMAIL = "parentEmail";
        public static final String PARENT_PHONE = "parentPhone";
        public static final String CHILD_NAME = "childName";
        public static final String STUDENT_ID = "studentId";
        public static final String GRADE = "grade";
        public static final String BOARD = "board";
        public static final String DEVICE_TYPE = "deviceType";
        public static final String CUSTOMER_STATUS = "customerStatus";
        public static final String DEMO_SESSION_TIME = "demoSessionTime";
        public static final String DEMO_TEACHER = "demoteacher";
        public static final String SESSION_ID = "sessionId";
        public static final String BOOKING_ID = "bookingId";
        public static final String CALLING_USER_ID = "callingUserId";
        public static final String ALTERNATE_PHONE_NUMBER = "alternatePhoneNumber";
        public static final String COURSE_ID = "courseId";
        public static final String COURSE_NAME = "courseName";
        public static final String AIO_ID = "aioId";
        public static final String BATCH_ID = "batchId";
        public static final String SESSION_SUCCESSFUL = "sessionSUCCESSFUL";
        public static final String SESSION_FAILURE_REASON = "sessionFailureReason";
        public static final String PARENT_INTEREST = "parentInterest";
        public static final String NOTES = "notes";
        public static final String EARLY_LEARNING_COURSE = "earlyLearningCourse";
        public static final String OTHER_COMMENT = "otherComment";
    }
}
