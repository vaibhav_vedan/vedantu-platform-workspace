/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author ajith
 */
public class SetBlockStatusForUserReq extends AbstractFrontEndUserReq {

    private Boolean profileEnabled;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String reason;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String reasonType;

    public SetBlockStatusForUserReq() {
        super();
    }

    public Boolean isProfileEnabled() {
        return profileEnabled;
    }

    public void setProfileEnabled(Boolean profileEnabled) {
        this.profileEnabled = profileEnabled;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (getUserId() == null) {
            errors.add("userId");
        }
        if (profileEnabled == null) {
            errors.add("profileEnabled");
        }
        return errors;
    }

}
