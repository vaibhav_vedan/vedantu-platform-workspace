/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetUserPhoneNumbersReq extends AbstractFrontEndReq {
        private Long userId;

        public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
        
        @Override
	public String toString() {
		return "GetUserPhoneNumbersReq {userId:" + userId + "}";
	}

        @Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (userId==null) {
			errors.add(User.Constants.USER_ID);
		}
		return errors;
	}
}
