/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetUserProfileReq extends AbstractFrontEndUserReq {

        @Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if (null == getUserId()) {
			errors.add(User.Constants.USER_ID);
		}
		return errors;
	}

}
