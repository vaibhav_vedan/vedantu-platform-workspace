/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author somil
 */
public class ExtensionNumberReq extends AbstractFrontEndReq {

	private String CallSid;
	private String From;
	private String To;
	private String Direction;
	private long DialCallDuration;
	private String StartTime;
	private String EndTime;
	private String CallType;
	private String digits;
	private String RecordingUrl;
	private String CustomField;

	public ExtensionNumberReq() {
		super();
	}



//	@Override
//	protected void populate(HttpServletRequest req) {
//		this.CallSid = getStringFieldValue("CallSid", req);
//		this.From = getStringFieldValue("From", req);
//		this.To = getStringFieldValue("To", req);
//		this.Direction = getStringFieldValue("Direction", req);
//		this.StartTime = getStringFieldValue("StartTime", req);
//		this.EndTime = getStringFieldValue("EndTime", req);
//		this.CallType = getStringFieldValue("CallType", req);
//		this.RecordingUrl = getStringFieldValue("RecordingUrl", req);
//		this.digits = Integer.parseInt(getStringFieldValue("digits", req)
//				.replace("\"", "").trim());
//		this.DialCallDuration = getLongFieldValue("DialCallDuration", req);
//		this.CustomField = getStringFieldValue("CustomField", req);
//	}

	public String getCallSid() {
		return CallSid;
	}

	public void setCallSid(String callSid) {
		CallSid = callSid;
	}

	public String getFrom() {
		return From;
	}

	public void setFrom(String from) {
		From = from;
	}

	public String getTo() {
		return To;
	}

	public void setTo(String to) {
		To = to;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		Direction = direction;
	}

	public long getDialCallDuration() {
		return DialCallDuration;
	}

	public void setDialCallDuration(long dialCallDuration) {
		DialCallDuration = dialCallDuration;
	}

	public String getStartTime() {
		return StartTime;
	}

	public void setStartTime(String startTime) {
		StartTime = startTime;
	}

	public String getEndTime() {
		return EndTime;
	}

	public void setEndTime(String endTime) {
		EndTime = endTime;
	}

	public String getCallType() {
		return CallType;
	}

	public void setCallType(String callType) {
		CallType = callType;
	}

	public String getDigits() {
		return digits;
	}

	public void setDigits(String digits) {
		this.digits = digits;
	}

	public String getRecordingUrl() {
		return RecordingUrl;
	}

	public void setRecordingUrl(String recordingUrl) {
		RecordingUrl = recordingUrl;
	}

	public String getCustomField() {
		return CustomField;
	}

	public void setCustomField(String customField) {
		CustomField = customField;
	}

	@Override
	public String toString() {
		return "ExtensionNumberReq [CallSid=" + CallSid + ", From=" + From
				+ ", To=" + To + ", Direction=" + Direction
				+ ", DialCallDuration=" + DialCallDuration + ", StartTime="
				+ StartTime + ", EndTime=" + EndTime + ", CallType=" + CallType
				+ ", digits=" + digits + ", RecordingUrl=" + RecordingUrl
				+ ", CustomField=" + CustomField + "]";
	}

}

