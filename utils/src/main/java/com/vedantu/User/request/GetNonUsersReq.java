package com.vedantu.User.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author mnpk
 */

public class GetNonUsersReq extends AbstractFrontEndListReq {
    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
