/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.Gender;

/**
 *
 * @author somil
 */
public class SocialUserInfo {
	
	public String email;
	public String firstName;
	public String lastName;
	public Gender gender;
	public String linkUrl;
	public String uid;
	public String picUrl;
	
	public String userId;
	
	public String getUserId() {
        return userId;
    }

    public void setUserId(String id) {
        this.userId = id;
    }
}
