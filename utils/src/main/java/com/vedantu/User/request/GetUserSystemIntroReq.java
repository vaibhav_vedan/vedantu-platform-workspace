package com.vedantu.User.request;


import java.util.List;


import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

public class GetUserSystemIntroReq extends AbstractFrontEndUserReq {
        @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
	private String type;
        @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
	private String status;
        @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
	private String identifier;
	private boolean allData;
        
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdentifier() {
		return identifier.toUpperCase();
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public boolean isAllData() {
		return allData;
	}

	public void setAllData(boolean allData) {
		this.allData = allData;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (getUserId()==null || getUserId() < 1) {
			errors.add("userId");
		}
		if (StringUtils.isEmpty(status)) {
			errors.add("status");
		}
		if (StringUtils.isEmpty(type)) {
			errors.add("type");
		}
		if (StringUtils.isEmpty(identifier)) {
			errors.add("identifier");
		}
		return errors;
	}


}

