package com.vedantu.User.request;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.vedantu.User.FeatureSource;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.UTMParams;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

public class ExistingUserISLRegReq extends AbstractFrontEndReq {

    private Long userId;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String studentFirstName;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String studentLastName;

    @Valid
    private StudentInfo studentInfo;

    @Valid
    private LocationInfo locationInfo;

    @Valid
    private UTMParams utm;
    private String event;
    private FeatureSource featureSource;

    private FeatureSource signUpFeature;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String agentName;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String agentEmail;
    private List<String> examPreps;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String exam;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String contactNumber;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String phoneCode;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String registerUrl;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String socialSource;

    private String voltId;
    private List<String> achievements;
    private List<String> achievementDocments;
    private List<Map<String, String>> idproofs;
    private String dob;
    private String jeeApplicationNumber;


    public FeatureSource getSignUpFeature() {
        return signUpFeature;
    }

    public void setSignUpFeature(FeatureSource signUpFeature) {
        this.signUpFeature = signUpFeature;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public UTMParams getUtm() {
        return utm;
    }

    public void setUtm(UTMParams utm) {
        this.utm = utm;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public FeatureSource getFeatureSource() {
        return featureSource;
    }

    public void setFeatureSource(FeatureSource featureSource) {
        this.featureSource = featureSource;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public List<String> getExamPreps() {
        return examPreps;
    }

    public void setExamPreps(List<String> examPreps) {
        this.examPreps = examPreps;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public String getSocialSource() {
        return socialSource;
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public String getVoltId() {
        return voltId;
    }

    public void setVoltId(String voltId) {
        this.voltId = voltId;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public List<String> getAchievementDocments() {
        return achievementDocments;
    }

    public void setAchievementDocments(List<String> achievementDocments) {
        this.achievementDocments = achievementDocments;
    }

    public List<Map<String, String>> getIdproofs() {
        return idproofs;
    }

    public void setIdproofs(List<Map<String, String>> idproofs) {
        this.idproofs = idproofs;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getJeeApplicationNumber() {
        return jeeApplicationNumber;
    }

    public void setJeeApplicationNumber(String jeeApplicationNumber) {
        this.jeeApplicationNumber = jeeApplicationNumber;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (studentInfo == null) {
            errors.add("studentInfo");
        }
        if (StringUtils.isEmpty(studentFirstName)) {
            errors.add("studentFirstName");
        }
        if (StringUtils.isEmpty(event)) {
            errors.add("event");
        }
        if (StringUtils.isEmpty(phoneCode)) {
            errors.add("phoneCode");
        }
        if (StringUtils.isEmpty(contactNumber)) {
            errors.add("contactNumber");
        }
        return errors;
    }
}
