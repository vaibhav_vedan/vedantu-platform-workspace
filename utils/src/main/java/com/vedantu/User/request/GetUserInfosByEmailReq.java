package com.vedantu.User.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;


public class GetUserInfosByEmailReq extends AbstractFrontEndReq{
	
	private List<String> emailIds;
	private List<Long> userIds;

	public List<String> getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(List<String> emailIds) {
		this.emailIds = emailIds;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

}
