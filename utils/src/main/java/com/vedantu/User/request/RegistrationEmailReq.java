package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractReq;

public class RegistrationEmailReq extends AbstractReq {

    User user;
    String key = "C1X?KF#jW9xl+f{(";


    public RegistrationEmailReq(User user){
        this.user = user;
    }

    public RegistrationEmailReq(){

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
