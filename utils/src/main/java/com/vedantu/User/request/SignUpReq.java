/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.*;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 *
 * @author somil
 */
public class SignUpReq extends UserProfileInfoReq {

    @NotNull(message = ReqLimitsErMsgs.RQD)
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String email;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String password;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String socialSource;
    // this will be used when the socialSource is either facebook or google
    private String authToken;

    private String verificationToken;

    // this will be added as user profile pic
    private String picUrl;

    @Valid
    private UTMParams utm = new UTMParams();

//	private String geoCountry;is already part of AbstractFrontEndReq.java so not declaring it again
    // optional, user can use a coupon while signing up
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String couponCode;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String referrerCode;

    private Long referrerId;

    private Integer referrerBonus;

    private Boolean parentRegistration;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String tncVersion;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String signUpURL;

    private FeatureSource signUpFeature;

    private String signUpFeatureRefId;

    private Boolean redirectAfterSignup;

//        private String ipAddress; is already part of AbstractFrontEndReq.java so not declaring it again
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String deviceId;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String agentName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String agentEmail;

    private String examName;

    private String referrer;
    private Boolean optIn;

    private String voltId;

    private List<String> achievements;
    private List<String> achievementDocments;
    private List<Map<String, String>> idproofs;

    private String orgId;

    public SignUpReq() {

        super();
    }

    public SignUpReq(String email, String firstName, String lastName, String password, String contactNo, Role role,
            Gender gender, List<String> languagePrefs, StudentInfo studentInfo, TeacherInfo teacherInfo,
            LocationInfo locationInfo, SocialInfo socialInfo) {

        super(firstName, lastName, contactNo, role, gender, languagePrefs, studentInfo, teacherInfo, locationInfo,
                socialInfo);
        this.email = email;
        this.password = password;
    }

    public String getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public Boolean getOptIn() {
        return optIn;
    }

    public void setOptIn(Boolean optIn) {
        this.optIn = optIn;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = StringUtils.toLowerCase(email).trim();
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }



    public String getSocialSource() {
        return socialSource;
    }

    public SocialSource __getSocialSource() {
        return SocialSource.valueOfKey(socialSource);
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public UTMParams getUtm() {
        return utm;
    }

    public void setUtm(UTMParams utm) {
        this.utm = utm;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public Integer getReferrerBonus() {
        return referrerBonus;
    }

    public void setReferrerBonus(Integer referrerBonus) {
        this.referrerBonus = referrerBonus;
    }

    public Long getReferrerId() {
        return referrerId;
    }

    public void setReferrerId(Long referrerId) {
        this.referrerId = referrerId;
    }

    public Boolean getParentRegistration() {
        return parentRegistration;
    }

    public void setParentRegistration(Boolean parentRegistration) {
        this.parentRegistration = parentRegistration;
    }

    public String getTncVersion() {
        return tncVersion;
    }

    public void setTncVersion(String tncVersion) {
        this.tncVersion = tncVersion;
    }

    public String getSignUpURL() {
        return signUpURL;
    }

    public void setSignUpURL(String signUpURL) {
        this.signUpURL = signUpURL;
    }

    public FeatureSource getSignUpFeature() {
        return signUpFeature;
    }

    public void setSignUpFeature(FeatureSource signUpFeature) {
        this.signUpFeature = signUpFeature;
    }

    public Boolean getRedirectAfterSignup() {
        return redirectAfterSignup;
    }

    public void setRedirectAfterSignup(Boolean redirectAfterSignup) {
        this.redirectAfterSignup = redirectAfterSignup;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getSignUpFeatureRefId() {
        return signUpFeatureRefId;
    }

    public void setSignUpFeatureRefId(String signUpFeatureRefId) {
        this.signUpFeatureRefId = signUpFeatureRefId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getVoltId() {
        return voltId;
    }

    public void setVoltId(String voltId) {
        this.voltId = voltId;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public List<String> getAchievementDocments() {
        return achievementDocments;
    }

    public void setAchievementDocments(List<String> achievementDocments) {
        this.achievementDocments = achievementDocments;
    }

    public List<Map<String, String>> getIdproofs() {
        return idproofs;
    }

    public void setIdproofs(List<Map<String, String>> idproofs) {
        this.idproofs = idproofs;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (!Role.TEACHER.equals(super.getRole()) && !CustomValidator.validEmail(email)) {
            errors.add("Invalid " + User.Constants.EMAIL);
        }
        if (CustomValidator.isVedantuEmail(email) && ConfigUtils.INSTANCE.getStringValue("environment").equals("PROD") && Role.STUDENT.equals(super.getRole())) {
            errors.add("User trying to signup with a @vedantu.com domain mail id" + User.Constants.EMAIL);
        }

        if (super.getFirstName() != null && "dsgdfdsfsdf".equalsIgnoreCase(super.getFirstName())) {
            errors.add("Invalid name");
        }

        SocialSource _socialSourceEnum = __getSocialSource();

        if (Role.ADMIN.equals(super.getRole()) || Role.STUDENT_CARE.equals(super.getRole())) {
            if (StringUtils.isEmpty(password)) {
                errors.add("Invalid " + User.Constants.PASSWORD);
            }
        }

        /*
		 * TNC VERSION IS NOT COMPULSORY as USER REGISTER VIA TOOLS if
		 * (StringUtils.isEmpty(tncVersion)) { errors.add("Invalid " +
		 * User.Constants.TNC_VERSION); }
         */
        // if (null == locationInfo) {
        // errors.add("Location Info");
        // } else {
        // errors.addAll(locationInfo.collectVerificationErrors());
        // }
        return errors;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

}
