/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.enums.LoginTokenContext;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class CreateUserAuthenticationTokenReq extends AbstractFrontEndReq {
    
    private LoginTokenContext contextType;
    private String contextId;
    private Long expireAfterMinutes;
    private Long userId;
    private String testLink;

    public CreateUserAuthenticationTokenReq() {
        super();
    }

    public CreateUserAuthenticationTokenReq(LoginTokenContext contextType, String contextId, Long expireAfterMinutes, Long userId) {
        super();
        this.contextType = contextType;
        this.contextId = contextId;
        this.expireAfterMinutes = expireAfterMinutes;
        this.userId = userId;
    }

    public LoginTokenContext getContextType() {
        return contextType;
    }

    public void setContextType(LoginTokenContext contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getExpireAfterMinutes() {
        return expireAfterMinutes;
    }

    public void setExpireAfterMinutes(Long expireAfterMinutes) {
        this.expireAfterMinutes = expireAfterMinutes;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTestLink() {
        return testLink;
    }

    public void setTestLink(String testLink) {
        this.testLink = testLink;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (null == contextType) {
            errors.add("contextType");
        }
        if(LoginTokenContext.DEMO_SESSION.equals(contextType)){
            if (null == contextId) {
                errors.add("contextId");
            }
        }
        else if (LoginTokenContext.JRP_TEST_LINK.equals(contextType)){
            if (null == testLink) {
                errors.add("testLink");
            }
        }
        if (null == userId) {
            errors.add("userId");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "CreateUserAuthenticationTokenReq{" + "contextType=" + contextType + ", contextId=" + contextId + ", expireAfterMinutes=" + expireAfterMinutes + ", userId=" + userId +", testLink=" + testLink + '}';
    }
    
}
