package com.vedantu.User.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DemoTriggerCallToAgentReq {

    String fromNumber;
    String toNumber;
    String contextId;
    Long callingUserId;
    String prospectActivityId;

}
