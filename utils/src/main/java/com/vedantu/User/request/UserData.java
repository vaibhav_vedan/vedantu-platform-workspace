/**
 * 
 */
package com.vedantu.User.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserData {

	private String userId;
	
	private String grade;
	
	private String board;
	
	private String stream;
	
	private String target;
	
}
