/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.Role;
import com.vedantu.User.enums.SubRole;
import com.vedantu.User.enums.TeacherCategoryType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author somil
 */
public class GetUsersReq extends AbstractFrontEndListReq {
		

	private Role role;
	private String query;
	// registration time
	private Long fromTime;
	private Long tillTime;
	private SubRole subRole;
	private TeacherCategoryType teacherCategoryType;

	public GetUsersReq() {
	}
        
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Long getFromTime() {
		return fromTime;
	}

	public void setFromTime(Long fromTime) {
		this.fromTime = fromTime;
	}

	public Long getTillTime() {
		return tillTime;
	}

	public void setTillTime(Long tillTime) {
		this.tillTime = tillTime;
	}

	public SubRole getSubRole() {
		return subRole;
	}

	public void setSubRole(SubRole subRole) {
		this.subRole = subRole;
	}

	public TeacherCategoryType getTeacherCategoryType() {
		return teacherCategoryType;
	}

	public void setTeacherCategoryType(TeacherCategoryType teacherCategoryType) {
		this.teacherCategoryType = teacherCategoryType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{role=").append(role).append(",query=").append(query)
				.append(" start=").append(getStart()).append(", limit=")
				.append(getSize()).append(", callingUserId=")
				.append(getCallingUserId()).append("}");
		return builder.toString();
	}

}

