/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User.request;

import com.vedantu.User.User;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author Darshit
 */
public class PaytmSignInReq extends AbstractFrontEndReq {

	private String authCode;
	private String grade;
	private String target;

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (StringUtils.isEmpty(authCode)) {
			errors.add("no accesss");
		}
		if (StringUtils.isEmpty(grade)) {
			errors.add("no accesss");
		}
		if (StringUtils.isEmpty(target)) {
			errors.add("no accesss");
		}
		return errors;
	}


}

