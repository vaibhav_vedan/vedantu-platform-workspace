package com.vedantu.User;

import com.vedantu.util.StringUtils;

public enum Role {
    ADMIN, STUDENT_CARE, TEACHER {
        @Override
        public String getProfileUrlPropertyKey() {
            return "url.profile.teacher";
        }
    },
    STUDENT {
        @Override
        public String getProfileUrlPropertyKey() {
            return "url.profile.student";
        }
    },
    PARENT,
    SEO_MANAGER,
    SEO_EDITOR,
    ADMIN_VOLT
    ;

    public static Role valueOfKey(String roleStr) {
        Role role = null;

        if (!StringUtils.isEmpty(roleStr)) {
            try {
                role = valueOf(roleStr.toUpperCase());
            } finally {
                // nothing
            }
        }

        return role;
    }

    public String getProfileUrlPropertyKey() {
        return StringUtils.EMPTY;
    }
}
