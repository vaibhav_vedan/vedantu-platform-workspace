/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.User;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class UserUtils {

    public String getPrimaryCallingNumberForTeacher(Integer primaryCallingNumberCode) {
        return ConfigUtils.INSTANCE.getStringValue("teacher.call.extension." + primaryCallingNumberCode);
    }

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserUtils.class);

    

    private static final String BASE_URL = "https://storage.googleapis.com";
    private static String bucket;

    public static UserUtils INSTANCE = new UserUtils(); //TODO either use a bean or create singleton

    public UserUtils() {
        bucket = ConfigUtils.INSTANCE.getStringValue("gcs.bucket.name");
    }

    /*
    public String toProfilePicUrl(String path) {
        String profilePicBucketName = ConfigUtils.INSTANCE.getStringValue("aws.profilePicBucketName");
        String urlString = null;

        AmazonS3 s3client = new AmazonS3Client();

        try {
            logger.info("Generating pre-signed URL.");
            java.util.Date expiration = new java.util.Date();
            long milliSeconds = expiration.getTime();
            milliSeconds += 1000 * 60 * 60; // Add 1 hour.
            expiration.setTime(milliSeconds);

            GeneratePresignedUrlRequest generatePresignedUrlRequest
                    = new GeneratePresignedUrlRequest(profilePicBucketName, path);
            generatePresignedUrlRequest.setMethod(HttpMethod.GET);
            generatePresignedUrlRequest.setExpiration(expiration);

            URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
            urlString = url.toString();

            logger.info("Pre-Signed URL = " + url.toString());
        } catch (AmazonServiceException exception) {
            logger.info("Caught an AmazonServiceException, "
                    + "which means your request made it "
                    + "to Amazon S3, but was rejected with an error response "
                    + "for some reason.");
            logger.info("Error Message: " + exception.getMessage());
            logger.info("HTTP  Code: " + exception.getStatusCode());
            logger.info("AWS Error Code:" + exception.getErrorCode());
            logger.info("Error Type:    " + exception.getErrorType());
            logger.info("Request ID:    " + exception.getRequestId());
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException, "
                    + "which means the client encountered "
                    + "an internal error while trying to communicate"
                    + " with S3, "
                    + "such as not being able to access the network.");
            logger.info("Error Message: " + ace.getMessage());
        }
        return urlString;

    }
     */
    public String toProfilePicUrl(String path) {
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        return new StringBuilder(BASE_URL).append("/").append(bucket)
                .append("/").append(path).toString();
    }
}
