package com.vedantu.User;

import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.List;
import javax.validation.constraints.Size;

public class UTMParams extends AbstractInfo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_source;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_medium;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_campaign;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_term;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_content;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String channel;

    public UTMParams() {
        super();
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{utm_source=").append(utm_source).append(", utm_medium=").append(utm_medium)
                .append(", utm_campaign=").append(utm_campaign).append(", utm_term=").append(utm_term)
                .append(", utm_content=").append(utm_content).append(", channel=").append(channel).append("}");
        return builder.toString();
    }

    @Override
    public List<String> collectVerificationErrors() {
        return null;
    }
}
