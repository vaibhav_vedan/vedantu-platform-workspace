package com.vedantu.User;

import com.vedantu.util.enums.SessionSource;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum FeatureSource {
    
	READY_TO_TEACH, PROPOSAL, ONE_TO_FEW, CHALLENGE, REQUEST_CALLBACK, SINGLE, 
        BECOME_TEACHER_CAMPAIGN, TRIAL, FMAT, VTP, PUBLICPROFILE, SUBSCRIPTION,
        CHAT,TOOLS,TRACK_COURSE,ISL,ISL_REGISTRATION_VIA_TOOLS, SEO_MANAGER_DOWNLOAD,
	VOLT_2020_JAN,
        VSAT_2018_JUN, SEM_PAGES, SEO_PAGES, JRP_SEP_2018, JRP_SEP_2019, DOUBT_APP, VSAT_2018_DEC, VSAT_2019_JAN,
        VSAT_2019_FEB, VSAT_2019_MAR, VSAT_2019_APR, VSAT_2019_APR_2, VSAT_2019_MAY, VSAT_2019_MAY_2,
	VSAT_2019_JUN, VSAT_2019_JUN_2, VSAT_2019_JUL, VSAT_2019_JUL_2, VSAT_2019_AUG, VSAT_2019_AUG_2,
	VSAT_2019_SEP, VSAT_2019_SEP_2,VSAT_2019_OCT,VSAT_2019_OCT_2,VSAT_2019_NOV,VSAT_2019_NOV_2,VSAT_2019_DEC,VSAT_2019_DEC_2,VSAT_2019_DEC_3,VSAT_2019_DEC_4,
	VSAT_2020_JAN, VSAT_2020_JAN_2, VSAT_2020_JAN_3, VSAT_2020_JAN_4, VSAT_2020_FEB, VSAT_2020_FEB_2, VSAT_2020_FEB_3, VSAT_2020_FEB_4,
	REVISEINDIA_2020_FEB,
	VSAT_2020_MAR,VSAT_2020_MAR_2,VSAT_2020_MAR_3,VSAT_2020_MAR_4,VSAT_2020_MAR_5,ORG_USER_SIGNUP,REVISE_JEE_2020_MARCH,
	VSAT_2020_APR,
	VSAT,
	VSAT_2020_APR_2,
	JEENEET_LAND,
	INSTA_SOLVER,
	MASTERTALK_DEEPIKA_P,
	MASTERTALK_VIDYA_B,
	TARGET_JEE_NEET,
	MASTER_TALK,
	PAYTM_MINI
	;

	public static FeatureSource identifyFeatureSource(SessionSource sessionSource) {
		if (sessionSource != null) {
			return identifyFeatureSource(sessionSource.name());
		} else {
			return null;
		}
	}

	public static FeatureSource identifyFeatureSource(String sessionSourceString) {
		// Expected input is DEVICE_MODEL_SOURCE. ex: MOBILE_TRIAL_PUBLICPROFILE

		// Note: This might fail if there is any overlapping of the names. like
		// CHAT and PROFILE_CHAT
		FeatureSource[] values = FeatureSource.values();

		// ValueList will be immutable if we use Arrays as list directly
		List<FeatureSource> valueList = new ArrayList<FeatureSource>();
		valueList.addAll(Arrays.asList(values));

		// Skip trail. TRIAL should not be a FeatureSource. Cannot remove it now
		// as there are entries in DB using it.
		valueList.remove(FeatureSource.TRIAL);
		if (!StringUtils.isEmpty(sessionSourceString)) {
			for (FeatureSource value : valueList) {
				if (sessionSourceString.contains(value.name())) {
					return value;
				}
			}
		}

		return null;
	}
}
