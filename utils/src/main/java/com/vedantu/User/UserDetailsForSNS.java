package com.vedantu.User;


public class UserDetailsForSNS {

    private String email;
    private String firstName;
    private String lastName;
    private String fullName;
    private String password;
    private Role role;
    private String category;
    private Long id;
    private String profilePicUrl;
    private Boolean passwordAutogenerated;
    private String contactNumber;
    private String phoneCode;
    private String grade;
    private Boolean isContactNumberVerified;
    private String referralCode;
    private String tncVersion;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public Boolean getPasswordAutogenerated() {
        return passwordAutogenerated;
    }

    public void setPasswordAutogenerated(Boolean passwordAutogenerated) {
        this.passwordAutogenerated = passwordAutogenerated;
    }

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Boolean getIsContactNumberVerified() {
		return isContactNumberVerified;
	}

	public void setIsContactNumberVerified(Boolean isContactNumberVerified) {
		this.isContactNumberVerified = isContactNumberVerified;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getTncVersion() {
		return tncVersion;
	}

	public void setTncVersion(String tncVersion) {
		this.tncVersion = tncVersion;
	}
    
    

}
