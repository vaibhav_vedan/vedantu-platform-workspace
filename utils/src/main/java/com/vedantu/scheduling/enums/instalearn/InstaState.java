package com.vedantu.scheduling.enums.instalearn;

public enum InstaState {
	INIT, INPROGRESS, ACCEPTED, REJECTED, EMAIL_VERIFICATION_PENDING, PAYMENT_PENDING, SESSION_BOOK_STATUS, MISSED;
	public static InstaState getInstaState(String taskname) {

		InstaState instaState = null;
		try {
			instaState = InstaState.valueOf(taskname.trim().toUpperCase());
		} catch (Throwable e) {
		}
		return instaState;
	}
}
