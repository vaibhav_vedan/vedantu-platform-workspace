package com.vedantu.scheduling.pojo.session;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.session.pojo.OTFCourseBasicInfo;
import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class BatchWiseSessionInfo {
    private Set<String> batchIds;
    private Long boardId;
    private String presenter;
    private Long startTime;
    private Long endTime;
    private String subject;
    private String title;
    private String description;

}
