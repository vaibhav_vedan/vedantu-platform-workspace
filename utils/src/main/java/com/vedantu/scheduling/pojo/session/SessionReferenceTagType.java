package com.vedantu.scheduling.pojo.session;

public enum SessionReferenceTagType {
	PROPOSAL, OFFERING
}
