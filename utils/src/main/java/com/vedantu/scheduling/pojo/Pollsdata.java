/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

import com.vedantu.util.fos.response.AbstractRes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class Pollsdata extends AbstractRes{
    int totalPolls;
    int totalAnswers;
    int avgAnswers;
    List<PollQuestion> pollQuestions = new ArrayList<>();

    public int getTotalPolls() {
        return totalPolls;
    }

    public void setTotalPolls(int totalPolls) {
        this.totalPolls = totalPolls;
    }

    public int getTotalAnswers() {
        return totalAnswers;
    }

    public void setTotalAnswers(int totalAnswers) {
        this.totalAnswers = totalAnswers;
    }

    public int getAvgAnswers() {
        return avgAnswers;
    }

    public void setAvgAnswers(int avgAnswers) {
        this.avgAnswers = avgAnswers;
    }

    public List<PollQuestion> getPollQuestions() {
        return pollQuestions;
    }

    public void setPollQuestions(List<PollQuestion> pollQuestions) {
        this.pollQuestions = pollQuestions;
    }
            
}
