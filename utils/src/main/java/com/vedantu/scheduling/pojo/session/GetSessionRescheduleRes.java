package com.vedantu.scheduling.pojo.session;

import java.util.ArrayList;
import java.util.List;

public class GetSessionRescheduleRes {
	private SessionInfo sessionInfo;
	private List<SessionRescheduleInfo> rescheduleInfos = new ArrayList<>();

	public GetSessionRescheduleRes() {
		super();
	}

	public GetSessionRescheduleRes(SessionInfo sessionInfo) {
		super();
		this.sessionInfo = sessionInfo;
	}

	public SessionInfo getSessionInfo() {
		return sessionInfo;
	}

	public void setSessionInfo(SessionInfo sessionInfo) {
		this.sessionInfo = sessionInfo;
	}

	public List<SessionRescheduleInfo> getRescheduleInfos() {
		return rescheduleInfos;
	}

	public void setRescheduleInfos(List<SessionRescheduleInfo> rescheduleInfos) {
		this.rescheduleInfos = rescheduleInfos;
	}

	public void addRescheduleData(SessionRescheduleInfo sessionRescheduleInfo) {
		this.rescheduleInfos.add(sessionRescheduleInfo);
	}

	@Override
	public String toString() {
		return "GetSessionRescheduleRes [sessionInfo=" + sessionInfo + ", rescheduleInfos=" + rescheduleInfos
				+ ", toString()=" + super.toString() + "]";
	}
}
