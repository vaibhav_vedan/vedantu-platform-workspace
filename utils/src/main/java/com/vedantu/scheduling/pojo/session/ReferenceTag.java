package com.vedantu.scheduling.pojo.session;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.StringUtils;

public class ReferenceTag {
	private ReferenceType referenceType;
	private String referenceId;

	public ReferenceTag() {
		super();
	}

	public ReferenceTag(ReferenceType referenceType, String referenceId) {
		super();
		this.referenceType = referenceType;
		this.referenceId = referenceId;
	}

	public ReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(ReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public void validate() throws VException {
		if (referenceType==null) {
			throw new BadRequestException(ErrorCode.INVALID_REFERENCE_TAG, "Empty reference tag");
		}
	}

	@Override
	public String toString() {
		return "ReferenceTag [referenceType=" + referenceType + ", referenceId=" + referenceId + "]";
	}
}
