package com.vedantu.scheduling.pojo.session;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.User.FeatureSource;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoLongIdEntityBean;
import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SessionModel;

public class Session extends AbstractMongoLongIdEntityBean{
    
	private String subject;
	private String topic;
	private String title;
	private String description;
	private Long startTime;
	private Long endTime;
	private SessionState state = SessionState.SCHEDULED;
	private Long scheduledBy;
	private Long startedAt;
	private Long startedBy;
	private Long endedAt;
	private Long endedBy;
	private Long activatedAt; // Introduced to identify any discrepancies
	private SessionPayoutType type;
	private Long subscriptionId;
	private List<Long> studentIds;
	private Long teacherId;
	private String remark;
	private FeatureSource sessionSource;
	private SessionModel model;
	private RequestSource deviceSource;
	private List<RescheduleData> rescheduleData = new ArrayList<>();
	private List<ReferenceTag> sessionTags = new ArrayList<>();
	private LiveSessionPlatformType liveSessionPlatformType=LiveSessionPlatformType.DEFAULT;
	private GTMGTTSessionDetails liveSessionPlatformDetails;
	private EntityType contextType;
	private String contextId;
        private String vimeoId;
        private String wizIQclassId;
        private String replayUrl;

	public Session() {
		super();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public SessionState getState() {
		return state;
	}

	public void setState(SessionState state) {
		this.state = state;
	}

	public Long getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(Long scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public Long getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Long startedAt) {
		this.startedAt = startedAt;
	}

	public Long getStartedBy() {
		return startedBy;
	}

	public void setStartedBy(Long startedBy) {
		this.startedBy = startedBy;
	}

	public Long getEndedAt() {
		return endedAt;
	}

	public void setEndedAt(Long endedAt) {
		this.endedAt = endedAt;
	}

	public Long getEndedBy() {
		return endedBy;
	}

	public void setEndedBy(Long endedBy) {
		this.endedBy = endedBy;
	}

	public Long getActivatedAt() {
		return activatedAt;
	}

	public void setActivatedAt(Long activatedAt) {
		this.activatedAt = activatedAt;
	}

	public SessionPayoutType getType() {
		return type;
	}

	public void setType(SessionPayoutType type) {
		this.type = type;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public FeatureSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(FeatureSource sessionSource) {
		this.sessionSource = sessionSource;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public RequestSource getDeviceSource() {
		return deviceSource;
	}

	public void setDeviceSource(RequestSource deviceSource) {
		this.deviceSource = deviceSource;
	}

	public List<RescheduleData> getRescheduleData() {
		return rescheduleData;
	}

	public void setRescheduleData(List<RescheduleData> rescheduleData) {
		this.rescheduleData = rescheduleData;
	}

	public List<ReferenceTag> getSessionTags() {
		return sessionTags;
	}

	public void setSessionTags(List<ReferenceTag> sessionTags) {
		this.sessionTags = sessionTags;
	}

        public LiveSessionPlatformType getLiveSessionPlatformType() {
            return liveSessionPlatformType;
        }

        public void setLiveSessionPlatformType(LiveSessionPlatformType liveSessionPlatformType) {
            this.liveSessionPlatformType = liveSessionPlatformType;
        }

        public GTMGTTSessionDetails getLiveSessionPlatformDetails() {
            return liveSessionPlatformDetails;
        }

        public void setLiveSessionPlatformDetails(GTMGTTSessionDetails liveSessionPlatformDetails) {
            this.liveSessionPlatformDetails = liveSessionPlatformDetails;
        }

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

        public String getVimeoId() {
            return vimeoId;
        }

        public void setVimeoId(String vimeoId) {
            this.vimeoId = vimeoId;
        }

        public String getWizIQclassId() {
            return wizIQclassId;
        }

        public void setWizIQclassId(String wizIQclassId) {
            this.wizIQclassId = wizIQclassId;
        }

        public String getReplayUrl() {
            return replayUrl;
        }

        public void setReplayUrl(String replayUrl) {
            this.replayUrl = replayUrl;
        }
        
}
