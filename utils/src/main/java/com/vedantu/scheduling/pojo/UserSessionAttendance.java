/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

import java.util.Set;

/**
 *
 * @author parashar
 */
public class UserSessionAttendance {
    
    private String sessionTitle;
    private Long startTime;
    private Set<String> contextIds;
    private boolean attended = false;

    /**
     * @return the sessionTitle
     */
    public String getSessionTitle() {
        return sessionTitle;
    }

    /**
     * @param sessionTitle the sessionTitle to set
     */
    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the batchIds
     */
    public Set<String> getContextIds() {
        return contextIds;
    }

    /**
     * @param contextIds the batchIds to set
     */
    public void setContextIds(Set<String> contextIds) {
        this.contextIds = contextIds;
    }

    /**
     * @return the attended
     */
    public boolean isAttended() {
        return attended;
    }

    /**
     * @param attended the attended to set
     */
    public void setAttended(boolean attended) {
        this.attended = attended;
    }

    @Override
    public String toString() {
        return "UserSessionAttendance{" + "sessionTitle=" + sessionTitle + ", startTime=" + startTime + ", contextIds=" + contextIds + ", attended=" + attended + '}';
    }
    
}
