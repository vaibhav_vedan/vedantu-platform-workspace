package com.vedantu.scheduling.pojo.session;

/**
 * Created by somil on 08/06/17.
 */
public enum ReferenceType {
    OFFERING, PROPOSAL, OTF_COURSE_ID, OTO_STRUCTURED_COURSE_ID, BUNDLE_ENROLMENT
}
