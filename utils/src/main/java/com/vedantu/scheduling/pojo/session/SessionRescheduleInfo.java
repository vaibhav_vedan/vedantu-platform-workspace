package com.vedantu.scheduling.pojo.session;

import com.vedantu.User.UserBasicInfo;

public class SessionRescheduleInfo extends RescheduleData {
	private UserBasicInfo rescheduledByInfo;

	public SessionRescheduleInfo() {
		super();
	}

	public UserBasicInfo getRescheduledByInfo() {
		return rescheduledByInfo;
	}

	public void setRescheduledByInfo(UserBasicInfo rescheduledByInfo) {
		this.rescheduledByInfo = rescheduledByInfo;
	}

	@Override
	public String toString() {
		return "SessionRescheduleInfo [rescheduledByInfo=" + rescheduledByInfo + ", toString()=" + super.toString()
				+ "]";
	}
}
