package com.vedantu.scheduling.pojo;

import com.vedantu.session.pojo.SessionState;

public class TotalSessionDuration {
	private SessionState state;
	private long duration;
	private long count;

	public SessionState getState() {
		return state;
	}

	public void setState(SessionState state) {
		this.state = state;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "TotalSessionDuration{" +
				"state=" + state +
				", duration=" + duration +
				", count=" + count +
				'}';
	}
}
