package com.vedantu.scheduling.pojo;

public class OTFSessionEnrollmentPojo {
    private String sessionId;
    public String batchId;
    public Long startTime;
    public Long endTime;

    public OTFSessionEnrollmentPojo(String sessionId,String batchId, Long startTime, Long endTime) {
        this.sessionId = sessionId;
        this.batchId = batchId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public OTFSessionEnrollmentPojo() {
        super();
    }
    
    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "OTFSessionEnrollmentPojo{" + "batchId=" + batchId + ", startTime=" + startTime + ", endTime=" + endTime + '}';
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
