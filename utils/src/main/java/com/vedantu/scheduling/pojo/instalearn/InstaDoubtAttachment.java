package com.vedantu.scheduling.pojo.instalearn;

import com.vedantu.scheduling.enums.instalearn.InstaDoubtAttachmentType;


public class InstaDoubtAttachment {
	
	private String name;
	private InstaDoubtAttachmentType type;
	private String url;
	
	
	public InstaDoubtAttachmentType getType() {
		return type;
	}
	
	public void setType(InstaDoubtAttachmentType type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getURL() {
		return url;
	}
	
	public void setURL(String url) {
		this.url = url;
	}
	

}
