/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo.session;

import java.io.Serializable;

/**
 *
 * @author ajith
 */
public class GTMGTTSessionDetails{

    private String teacherLink;
    private String studentLink;

    public GTMGTTSessionDetails() {
    }

    public String getTeacherLink() {
        return teacherLink;
    }

    public void setTeacherLink(String teacherLink) {
        this.teacherLink = teacherLink;
    }

    public String getStudentLink() {
        return studentLink;
    }

    public void setStudentLink(String studentLink) {
        this.studentLink = studentLink;
    }

    @Override
    public String toString() {
        return "GTMGTTSessionDetails{" + "teacherLink=" + teacherLink + ", studentLink=" + studentLink + '}';
    }

}
