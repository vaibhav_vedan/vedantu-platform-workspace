package com.vedantu.scheduling.pojo.session;

public enum CanvasStreamingType {
    AGORA, VEDANTU
}
