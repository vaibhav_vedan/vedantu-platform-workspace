package com.vedantu.scheduling.pojo.calendar;

public class CalendarBlockSlot {
	private String referenceId;
	private CalendarBlockReferenceType referenceType;
	private String studentId;
	private String teacherId;
	private Long startTime;
	private Long endTime;

	public CalendarBlockSlot() {
		super();
	}

	public CalendarBlockSlot(String referenceId, CalendarBlockReferenceType referenceType, String studentId,
			String teacherId, Long startTime, Long endTime) {
		super();
		this.referenceId = referenceId;
		this.referenceType = referenceType;
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public CalendarBlockReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(CalendarBlockReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "CalendarBlockSlot [referenceId=" + referenceId + ", referenceType=" + referenceType + ", studentId="
				+ studentId + ", teacherId=" + teacherId + ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}
}
