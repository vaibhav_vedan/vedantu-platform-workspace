package com.vedantu.scheduling.pojo.session;

import com.google.common.collect.Sets;

public enum OTMSessionType {
    /**
     * Trial Sessions
     */
    TRIAL,

    /**
     * Regular Sessions
     */
    REGULAR,

    /**
     * Test Sessions
     */
    TEST,

    /**
     * Parent Teachers Meeting
     */
    PTM,

    /**
     * Demo Sessions
     */
    DEMO,

    /**
     * Sales Demo Sessions
     */
    SALES_DEMO,

    /**
     * Early Learning Sessions
     */
    OTO_NURSERY,

    /**
     * regular session that indicates it was extended beyond planned sessions for a batch
     */
    EXTRA_REGULAR,

    /**
     * Internal townhall Meetings
     */

    TOWNHALL,

    /**
     * legacy code
     */
    @Deprecated EXTRA_TRIAL,
    /**
     * legacy code
     */
    @Deprecated NON_REGULAR,

    /**
     * wrong type moved to {@link com.vedantu.onetofew.enums.OTFSessionFlag#ABRUPT_ENDING}
     */
    @Deprecated ABRUPT_ENDING,

    /**
     * wrong type moved to {@link com.vedantu.onetofew.enums.EntityTag#SIMULATED_LIVE}
     */
    @Deprecated SIM_LIVE,

    ;


    public boolean isUnsupported() {
        return Sets.newHashSet(EXTRA_TRIAL, NON_REGULAR, ABRUPT_ENDING, SIM_LIVE).contains(this);
    }

}
