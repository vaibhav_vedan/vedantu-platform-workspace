package com.vedantu.scheduling.pojo.calendar;

public class GetLastMarkedDayResponse {
	private String userId;
	private Long lastMarkedDay;

	public GetLastMarkedDayResponse(String userId, Long lastMarkedDay) {
		super();
		this.userId = userId;
		this.lastMarkedDay = lastMarkedDay;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getLastMarkedDay() {
		return lastMarkedDay;
	}

	public void setLastMarkedDay(Long lastMarkedDay) {
		this.lastMarkedDay = lastMarkedDay;
	}

	@Override
	public String toString() {
		return "GetLastMarkedDayResponse [userId=" + userId + ", lastMarkedDay=" + lastMarkedDay + ", toString()="
				+ super.toString() + "]";
	}
}
