package com.vedantu.scheduling.pojo.calendar;

public enum CalendarReferenceType {
	OTO_SESSION, OTF_SESSION, WEBINAR_SESSION, OTF_BATCH, PROPOSAL
}
