package com.vedantu.scheduling.pojo.calendar;

public class RetryCalendarEventReq {
	private String eventId;
	private String userId;
	private Long creationEndTime;
	private Long creationStartTime;

	public RetryCalendarEventReq() {
		super();
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getCreationEndTime() {
		return creationEndTime;
	}

	public void setCreationEndTime(Long creationEndTime) {
		this.creationEndTime = creationEndTime;
	}

	public Long getCreationStartTime() {
		return creationStartTime;
	}

	public void setCreationStartTime(Long creationStartTime) {
		this.creationStartTime = creationStartTime;
	}
}
