/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.session.pojo.EntityType;

import java.util.List;

/**
 *
 * @author parashar
 */
public class CommonSessionInfo {
    
    private EntityType entityType;
    private String entityId;
    private Long startTime;
    private Long endTime;
    private String displayState;
    private String teacherName;
    private String teacherId;
    private String sessionTitle;
    private String vimeoId;
    private List<String> replayUrls;
    private boolean attended = false;
    private OTFSessionToolType oTFSessionToolType;
    private Boolean isPastSession = false;
    private String courseId;
    private String courseName;
    private String batchId;

    public CommonSessionInfo(){
        
    }

    public CommonSessionInfo(EntityType entityType, String entityId, Long startTime, Long endTime, String teacherName, String teacherId, String sessionTitle) {
        this.entityType = entityType;
        this.entityId = entityId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.teacherName = teacherName;
        this.teacherId = teacherId;
        this.sessionTitle = sessionTitle;
    }
    
    /**
     * @return the entityType
     */
    public EntityType getEntityType() {
        return entityType;
    }

    /**
     * @param entityType the entityType to set
     */
    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName the teacherName to set
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * @return the teacherId
     */
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the sessionTitle
     */
    public String getSessionTitle() {
        return sessionTitle;
    }

    /**
     * @param sessionTitle the sessionTitle to set
     */
    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    /**
     * @return the vimeoId
     */
    public String getVimeoId() {
        return vimeoId;
    }

    /**
     * @param vimeoId the vimeoId to set
     */
    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    /**
     * @return the replayUrls
     */
    public List<String> getReplayUrls() {
        return replayUrls;
    }

    /**
     * @param replayUrls the replayUrls to set
     */
    public void setReplayUrls(List<String> replayUrls) {
        this.replayUrls = replayUrls;
    }

    @Override
    public String toString() {
        return "CommonSessionInfo{" + "entityType=" + entityType + ", entityId=" + entityId + ", startTime=" + startTime + ", endTime=" + endTime + ", teacherName=" + teacherName + ", teacherId=" + teacherId + ", sessionTitle=" + sessionTitle + ", vimeoId=" + vimeoId + ", replayUrls=" + replayUrls + '}';
    }

    /**
     * @return the attended
     */
    public boolean isAttended() {
        return attended;
    }

    /**
     * @param attended the attended to set
     */
    public void setAttended(boolean attended) {
        this.attended = attended;
    }

    /**
     * @return the displayState
     */
    public String getDisplayState() {
        return displayState;
    }

    /**
     * @param displayState the displayState to set
     */
    public void setDisplayState(String displayState) {
        this.displayState = displayState;
    }

    /**
     * @return the oTFSessionToolType
     */
    public OTFSessionToolType getoTFSessionToolType() {
        return oTFSessionToolType;
    }

    /**
     * @param oTFSessionToolType the oTFSessionToolType to set
     */
    public void setoTFSessionToolType(OTFSessionToolType oTFSessionToolType) {
        this.oTFSessionToolType = oTFSessionToolType;
    }

    public Boolean getIsPastSession() {
        return isPastSession;
    }

    public void setIsPastSession(Boolean pastSession) {
        isPastSession = pastSession;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
}
