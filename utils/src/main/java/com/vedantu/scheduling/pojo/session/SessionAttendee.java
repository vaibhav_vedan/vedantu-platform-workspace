package com.vedantu.scheduling.pojo.session;

import com.vedantu.User.Role;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.session.pojo.SessionAttendee.SessionUserState;

public class SessionAttendee {
	private String id;
	private Long creationTime;
	private String createdBy;
	private Long lastUpdated;
	private String callingUserId; // For tracking last updated by
	private EntityState entityState;

	private Long sessionId;
	private Long userId;
	private Long startTime;
	private Long endTime;
	private Role role;
	private Integer billingPeriod;
	private int chargedAmount;
	private int paidAmount;
	private SessionUserState userState = SessionUserState.NOT_JOINED;
	private Long joinTime;

	public SessionAttendee() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCallingUserId() {
		return callingUserId;
	}

	public void setCallingUserId(String callingUserId) {
		this.callingUserId = callingUserId;
	}

	public EntityState getEntityState() {
		return entityState;
	}

	public void setEntityState(EntityState entityState) {
		this.entityState = entityState;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Integer getBillingPeriod() {
		return billingPeriod;
	}

	public void setBillingPeriod(Integer billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	public int getChargedAmount() {
		return chargedAmount;
	}

	public void setChargedAmount(int chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	public int getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(int paidAmount) {
		this.paidAmount = paidAmount;
	}

	public SessionUserState getUserState() {
		return userState;
	}

	public void setUserState(SessionUserState userState) {
		this.userState = userState;
	}

	public Long getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(Long joinTime) {
		this.joinTime = joinTime;
	}

	@Override
	public String toString() {
		return "SessionAttendee [id=" + id + ", creationTime=" + creationTime + ", createdBy=" + createdBy
				+ ", lastUpdated=" + lastUpdated + ", callingUserId=" + callingUserId + ", entityState=" + entityState
				+ ", sessionId=" + sessionId + ", userId=" + userId + ", startTime=" + startTime + ", endTime="
				+ endTime + ", role=" + role + ", billingPeriod=" + billingPeriod + ", chargedAmount=" + chargedAmount
				+ ", paidAmount=" + paidAmount + ", userState=" + userState + ", joinTime=" + joinTime + "]";
	}
}
