package com.vedantu.scheduling.pojo.calendar;

public class RemoveCalendarEntryReq {

	// @PathVariable(value = "id") String calendarEntryId,
	// @RequestParam("startTime") Long startTime, @RequestParam("endTime") Long
	// endTime,
	// @RequestParam("slotState") SlotState slotState

	private String calendarEntryId;
	private Long startTime;
	private Long endTime;
	private CalendarEntrySlotState slotState;
	private CalendarReferenceType referenceType;
	private String referenceId;

	public RemoveCalendarEntryReq() {
		super();
	}

	public RemoveCalendarEntryReq(String calendarEntryId, Long startTime, Long endTime,
			CalendarEntrySlotState slotState, CalendarReferenceType referenceType, String referenceId) {
		super();
		this.calendarEntryId = calendarEntryId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.slotState = slotState;
		this.referenceType = referenceType;
		this.referenceId = referenceId;
	}

	public String getCalendarEntryId() {
		return calendarEntryId;
	}

	public void setCalendarEntryId(String calendarEntryId) {
		this.calendarEntryId = calendarEntryId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	public CalendarReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(CalendarReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "RemoveCalendarEntryReq [calendarEntryId=" + calendarEntryId + ", startTime=" + startTime + ", endTime="
				+ endTime + ", slotState=" + slotState + ", referenceType=" + referenceType + ", referenceId="
				+ referenceId + ", toString()=" + super.toString() + "]";
	}
}
