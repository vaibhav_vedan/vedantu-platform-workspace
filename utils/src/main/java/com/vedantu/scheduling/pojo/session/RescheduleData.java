package com.vedantu.scheduling.pojo.session;

import com.vedantu.User.Role;

public class RescheduleData {

	private static final long serialVersionUID = 1L;
	private Long rescheduledBy;
	private Role rescheduledByRole;// putting it at the behest of Simriti as it
									// eases the data analysis
	private String reason;
	private Long previousStartTime;
	private Long updateTime;

	public RescheduleData() {
	}
	
	public RescheduleData(Long rescheduledBy, Role rescheduledByRole, String reason, Long lastStartTime) {
		this.rescheduledBy = rescheduledBy;
		this.rescheduledByRole = rescheduledByRole;
		this.reason = reason;
		this.previousStartTime = lastStartTime;
		this.updateTime = System.currentTimeMillis();
	}

	public Long getRescheduledBy() {
		return rescheduledBy;
	}

	public void setRescheduledBy(Long rescheduledBy) {
		this.rescheduledBy = rescheduledBy;
	}

	public Role getRescheduledByRole() {
		return rescheduledByRole;
	}

	public void setRescheduledByRole(Role rescheduledByRole) {
		this.rescheduledByRole = rescheduledByRole;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getPreviousStartTime() {
		return previousStartTime;
	}

	public void setPreviousStartTime(Long previousStartTime) {
		this.previousStartTime = previousStartTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "RescheduleData [rescheduledBy=" + rescheduledBy + ", rescheduledByRole=" + rescheduledByRole
				+ ", reason=" + reason + ", previousStartTime=" + previousStartTime + ", updateTime=" + updateTime
				+ ", toString()=" + super.toString() + "]";
	}
}
