package com.vedantu.scheduling.pojo.calendar;

public class AddCalendarEntryReq {
	private String userId;
	private Long startTime;
	private Long endTime;
	private CalendarEntrySlotState state;
	private CalendarReferenceType referenceType;
	private String referenceId;

	public AddCalendarEntryReq() {
		super();
	}

	public AddCalendarEntryReq(String userId, Long startTime, Long endTime, CalendarEntrySlotState state, CalendarReferenceType referenceType, String referenceId) {
		super();
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.state = state;
		this.referenceType = referenceType;
		this.referenceId = referenceId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getState() {
		return state;
	}

	public void setState(CalendarEntrySlotState state) {
		this.state = state;
	}

	public CalendarReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(CalendarReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "AddCalendarEntryReq [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime + ", state="
				+ state + ", referenceType=" + referenceType + ", referenceId=" + referenceId + ", toString()="
				+ super.toString() + "]";
	}
}