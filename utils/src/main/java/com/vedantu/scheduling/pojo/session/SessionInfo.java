package com.vedantu.scheduling.pojo.session;

import java.util.List;

import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.UserSessionInfo;

public class SessionInfo extends Session {

	private String sessionId; // Added so that in email session id is not displayed in exponential form
	private SessionState displayState;
	private Long sessionExpireTime;
	private List<UserSessionInfo> attendees;
	private long currentSystemTime = System.currentTimeMillis();
	private Long activeDuration;
	private Boolean endCoursePlan = Boolean.FALSE;

	public SessionInfo() {
		super();
	}

	public SessionState getDisplayState() {
		return displayState;
	}

	public void setDisplayState(SessionState displayState) {
		this.displayState = displayState;
	}

	public List<UserSessionInfo> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<UserSessionInfo> attendees) {
		this.attendees = attendees;
	}

	public Long getSessionExpireTime() {
		return sessionExpireTime;
	}

	public void setSessionExpireTime(Long sessionExpireTime) {
		this.sessionExpireTime = sessionExpireTime;
	}

	public long getCurrentSystemTime() {
		return currentSystemTime;
	}

	public void setCurrentSystemTime(long currentSystemTime) {
		this.currentSystemTime = currentSystemTime;
	}

	public Long getActiveDuration() {
		return activeDuration;
	}

	public void setActiveDuration(Long activeDuration) {
		this.activeDuration = activeDuration;
	}

	public Boolean getEndCoursePlan() {
		return endCoursePlan;
	}

	public void setEndCoursePlan(Boolean endCoursePlan) {
		this.endCoursePlan = endCoursePlan;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "SessionInfo [displayState=" + displayState + ", sessionExpireTime=" + sessionExpireTime + ", attendees="
				+ attendees + ", currentSystemTime=" + currentSystemTime + ", activeDuration=" + activeDuration
				+ ", toString()=" + super.toString() + "]";
	}
}
