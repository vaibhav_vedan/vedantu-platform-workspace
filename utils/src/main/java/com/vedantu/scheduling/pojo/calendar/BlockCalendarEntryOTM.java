package com.vedantu.scheduling.pojo.calendar;

import java.util.List;

public class BlockCalendarEntryOTM {
    private String userId;
    private String batchId;
    private List<String> userIds;

    public BlockCalendarEntryOTM(String userId, String batchId) {
        this.userId = userId;
        this.batchId = batchId;
    }

    public BlockCalendarEntryOTM(String batchId, List<String> userIds) {
        this.batchId = batchId;
        this.userIds = userIds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
