package com.vedantu.scheduling.pojo;

public class AMInclassAggregateResp {
	private long totalSessionsInRange;
	private long noOfSessionsAtteneded;
	private long quizAsked;
	private long quizAttempted;
	private long quizCorrect;
	private long quizIncorrect;
	private long hotspotAsked;
	private long hotspotAttempted;
	private long hotspotCorrect;
	private long hotspotIncorrect;
	private long doubtsAsked;
	private long doubtsResolved;
	
	public AMInclassAggregateResp() {
		super();
	}

	public AMInclassAggregateResp(long totalSessionsInRange, long noOfSessionsAtteneded, long quizAsked,
			long quizAttempted, long quizCorrect, long quizIncorrect, long hotspotAsked, long hotspotAttempted,
			long hotspotCorrect, long hotspotIncorrect, long doubtsAsked, long doubtsResolved) {
		super();
		this.totalSessionsInRange = totalSessionsInRange;
		this.noOfSessionsAtteneded = noOfSessionsAtteneded;
		this.quizAsked = quizAsked;
		this.quizAttempted = quizAttempted;
		this.quizCorrect = quizCorrect;
		this.quizIncorrect = quizIncorrect;
		this.hotspotAsked = hotspotAsked;
		this.hotspotAttempted = hotspotAttempted;
		this.hotspotCorrect = hotspotCorrect;
		this.hotspotIncorrect = hotspotIncorrect;
		this.doubtsAsked = doubtsAsked;
		this.doubtsResolved = doubtsResolved;
	}

	public long getTotalSessionsInRange() {
		return totalSessionsInRange;
	}

	public void setTotalSessionsInRange(long totalSessionsInRange) {
		this.totalSessionsInRange = totalSessionsInRange;
	}

	public long getNoOfSessionsAtteneded() {
		return noOfSessionsAtteneded;
	}

	public void setNoOfSessionsAtteneded(long noOfSessionsAtteneded) {
		this.noOfSessionsAtteneded = noOfSessionsAtteneded;
	}

	public long getQuizAttempted() {
		return quizAttempted;
	}

	public void setQuizAttempted(long quizAttempted) {
		this.quizAttempted = quizAttempted;
	}

	public long getQuizCorrect() {
		return quizCorrect;
	}

	public void setQuizCorrect(long quizCorrect) {
		this.quizCorrect = quizCorrect;
	}

	public long getQuizIncorrect() {
		return quizIncorrect;
	}

	public void setQuizIncorrect(long quizIncorrect) {
		this.quizIncorrect = quizIncorrect;
	}

	public long getHotspotAttempted() {
		return hotspotAttempted;
	}

	public void setHotspotAttempted(long hotspotAttempted) {
		this.hotspotAttempted = hotspotAttempted;
	}

	public long getHotspotCorrect() {
		return hotspotCorrect;
	}

	public void setHotspotCorrect(long hotspotCorrect) {
		this.hotspotCorrect = hotspotCorrect;
	}

	public long getHotspotIncorrect() {
		return hotspotIncorrect;
	}

	public void setHotspotIncorrect(long hotspotIncorrect) {
		this.hotspotIncorrect = hotspotIncorrect;
	}

	public long getDoubtsAsked() {
		return doubtsAsked;
	}

	public void setDoubtsAsked(long doubtsAsked) {
		this.doubtsAsked = doubtsAsked;
	}

	public long getDoubtsResolved() {
		return doubtsResolved;
	}

	public void setDoubtsResolved(long doubtsResolved) {
		this.doubtsResolved = doubtsResolved;
	}
	
	public long getQuizAsked() {
		return quizAsked;
	}

	public void setQuizAsked(long quizAsked) {
		this.quizAsked = quizAsked;
	}

	public long getHotspotAsked() {
		return hotspotAsked;
	}

	public void setHotspotAsked(long hotspotAsked) {
		this.hotspotAsked = hotspotAsked;
	}

	@Override
	public String toString() {
		return "AMInclassAggregateResp [noOfSessionsAtteneded: "+noOfSessionsAtteneded + " quizAttempted:" + quizAttempted + " quizCorrect:" + quizCorrect + " quizIncorrect: "+ quizIncorrect + " hotspotAttempted: " + hotspotAttempted
				+" hotspotCorrect:" +hotspotCorrect +" hotspotIncorrect:"+ hotspotIncorrect + " doubtsAsked:" + doubtsAsked + " doubtsResolved: "+ doubtsResolved + "]";
	}
}
