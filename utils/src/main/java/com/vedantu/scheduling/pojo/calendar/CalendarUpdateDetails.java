package com.vedantu.scheduling.pojo.calendar;

public class CalendarUpdateDetails {
	private long updateTime;
	private long startTime;
	private long endTime;
	private boolean remove;
	private CalendarReferenceType referenceType;
	private String referenceId;
	private CalendarEntrySlotState slotState; // For now only storing booked

	public CalendarUpdateDetails() {
		super();
	}

	public CalendarUpdateDetails(CalendarEntrySlotState slotState, long startTime, long endTime, boolean remove,
			CalendarReferenceType referenceType, String referenceId) {
		super();
		this.updateTime = System.currentTimeMillis();
		this.startTime = startTime;
		this.endTime = endTime;
		this.remove = remove;
		this.referenceType = referenceType;
		this.referenceId = referenceId;
		this.slotState = slotState;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public boolean isRemove() {
		return remove;
	}

	public void setRemove(boolean remove) {
		this.remove = remove;
	}

	public CalendarReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(CalendarReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	@Override
	public String toString() {
		return "CalendarUpdateDetails [updateTime=" + updateTime + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", remove=" + remove + ", referenceType=" + referenceType + ", referenceId=" + referenceId
				+ ", slotState=" + slotState + ", toString()=" + super.toString() + "]";
	}
}
