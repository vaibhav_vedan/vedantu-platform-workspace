package com.vedantu.scheduling.pojo.session;

import java.util.Set;

public class TeacherSessionDashboard {
	
	private Long teacherId;
	private Set<Long> studentIds;
	private Integer booked = 0;
	private Integer ended = 0;
	private Integer cancelled = 0;
	private Integer cancelledByTeacher = 0;
	private Integer cancelledByStudent=0;
	private Integer cancelledByAdmin = 0;
	private Integer rescheduled = 0;
	private Integer expired = 0;// For OTF -> implies no one joined
	private Integer lateJoined = 0;
	private Integer forfeited = 0;//for OTF -> implies only teacher joined
	private Set<String> attendees;
	private Integer teacherNoShow=0;
	
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public Set<Long> getStudentIds() {
		return studentIds;
	}
	public void setStudentIds(Set<Long> studentIds) {
		this.studentIds = studentIds;
	}
	public Integer getBooked() {
		return booked;
	}
	public void setBooked(Integer booked) {
		this.booked = booked;
	}
	public Integer getEnded() {
		return ended;
	}
	public void setEnded(Integer ended) {
		this.ended = ended;
	}
	public Integer getCancelled() {
		return cancelled;
	}
	public void setCancelled(Integer cancelled) {
		this.cancelled = cancelled;
	}
	public Integer getRescheduled() {
		return rescheduled;
	}
	public void setRescheduled(Integer rescheduled) {
		this.rescheduled = rescheduled;
	}
	public Integer getExpired() {
		return expired;
	}
	public void setExpired(Integer expired) {
		this.expired = expired;
	}
	public Integer getLateJoined() {
		return lateJoined;
	}
	public void setLateJoined(Integer lateJoined) {
		this.lateJoined = lateJoined;
	}
	public Integer getForfeited() {
		return forfeited;
	}
	public void setForfeited(Integer forfeited) {
		this.forfeited = forfeited;
	}
	public Set<String> getAttendees() {
		return attendees;
	}
	public void setAttendees(Set<String> attendees) {
		this.attendees = attendees;
	}
	public Integer getTeacherNoShow() {
		return teacherNoShow;
	}
	public void setTeacherNoShow(Integer teacherNoShow) {
		this.teacherNoShow = teacherNoShow;
	}
	public Integer getCancelledByTeacher() {
		return cancelledByTeacher;
	}
	public void setCancelledByTeacher(Integer cancelledByTeacher) {
		this.cancelledByTeacher = cancelledByTeacher;
	}
	public Integer getCancelledByStudent() {
		return cancelledByStudent;
	}
	public void setCancelledByStudent(Integer cancelledByStudent) {
		this.cancelledByStudent = cancelledByStudent;
	}
	public Integer getCancelledByAdmin() {
		return cancelledByAdmin;
	}
	public void setCancelledByAdmin(Integer cancelledByAdmin) {
		this.cancelledByAdmin = cancelledByAdmin;
	}
}
