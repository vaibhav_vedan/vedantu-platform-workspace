package com.vedantu.scheduling.pojo.instalearn;

import com.vedantu.User.AbstractEntityRes;
import java.util.List;

import com.vedantu.scheduling.enums.instalearn.InstaState;

public class InstaPayload {

    private Long boardId;
    private Integer grade;
    private String target;
    private Integer chargeRate;
    private Long duration;
    private InstaState state;
    private String doubtText;
    private String note;
    private List<InstaDoubtAttachment> doubtAttachments;

    public InstaPayload() {
        super();
    }

    public InstaPayload(Long boardId, String subjectName, Integer grade, String target, Integer chargeRate,
            Long duration, InstaState state, Long requestId, Integer dbId, String doubtText, String note,
            List<InstaDoubtAttachment> doubtAttachments) {

        super();
        this.boardId = boardId;
        this.grade = grade;
        this.target = target;
        this.chargeRate = chargeRate;
        this.duration = duration;
        this.state = state;
        this.doubtText = doubtText;
        this.note = note;
        this.doubtAttachments = doubtAttachments;
    }

    public List<InstaDoubtAttachment> getDoubtAttachments() {
        return doubtAttachments;
    }

    public void setDoubtAttachments(List<InstaDoubtAttachment> doubtAttachments) {
        this.doubtAttachments = doubtAttachments;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getChargeRate() {
        return chargeRate;
    }

    public void setChargeRate(Integer chargeRate) {
        this.chargeRate = chargeRate;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public InstaState getState() {
        return state;
    }

    public void setState(InstaState state) {
        this.state = state;
    }

    public String getDoubtText() {
        return doubtText;
    }

    public void setDoubtText(String doubtText) {
        this.doubtText = doubtText;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public static class Constants extends AbstractEntityRes.Constants {

        public static final String BOARD_ID = "boardId";
        public static final String GRADE = "grade";
        public static final String TARGET = "target";
        public static final String CHARGERATE = "chargeRate";
        public static final String DURATION = "duration";
        public static final String STATE = "state";
        public static final String ID = "Id";
        public static final String DOUBT_TEXT = "doubtText";
        public static final String NOTE = "note";
        public static final String ATTACHMENTS = "doubtAttachments";
    }

    @Override
    public String toString() {
        return "InstaPayload{" + "boardId=" + boardId + ", grade=" + grade + ", target=" + target + ", chargeRate=" + chargeRate + ", duration=" + duration + ", state=" + state + ", doubtText=" + doubtText + ", note=" + note + ", doubtAttachments=" + doubtAttachments + '}';
    }

}
