package com.vedantu.scheduling.response.instalearn;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.scheduling.pojo.instalearn.InstaPayload;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class SubmitInstaRequestRes extends AbstractMongoStringIdEntityBean{

    private Long to;
    private Long from;
    private UserBasicInfo toUser;
    private UserBasicInfo fromUser;
    private String subjectName;
    private boolean currentRequest;
    private InstaPayload data;

    public SubmitInstaRequestRes() {
        super();
    }


    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public InstaPayload getData() {
        return data;
    }

    public void setData(InstaPayload data) {
        this.data = data;
    }

    public boolean getCurrentRequest() {
        return currentRequest;
    }

    public void setCurrentRequest(boolean currentRequest) {
        this.currentRequest = currentRequest;
    }

    public UserBasicInfo getToUser() {
        return toUser;
    }

    public void setToUser(UserBasicInfo toUser) {
        this.toUser = toUser;
    }

    public UserBasicInfo getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserBasicInfo fromUser) {
        this.fromUser = fromUser;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public String toString() {
        return "InstaProposalResponse{" + "to=" + to + ", from=" + from + ", toUser=" + toUser + ", fromUser=" + fromUser + ", subjectName=" + subjectName + ", currentRequest=" + currentRequest + ", data=" + data + '}';
    }

}
