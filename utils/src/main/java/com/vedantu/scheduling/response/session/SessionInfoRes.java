package com.vedantu.scheduling.response.session;

import java.util.List;

import com.vedantu.scheduling.pojo.session.SessionInfo;

public class SessionInfoRes {
	private List<SessionInfo> sessionsInfoRes;
	private long currentSystemTime = System.currentTimeMillis();

	public SessionInfoRes() {
		super();
	}

	public SessionInfoRes(List<SessionInfo> sessionsInfoRes) {
		super();
		this.sessionsInfoRes = sessionsInfoRes;
		this.currentSystemTime = System.currentTimeMillis();
	}

	public List<SessionInfo> getSessionsInfoRes() {
		return sessionsInfoRes;
	}

	public void setSessionsInfoRes(List<SessionInfo> sessionsInfoRes) {
		this.sessionsInfoRes = sessionsInfoRes;
	}

	public long getCurrentSystemTime() {
		return currentSystemTime;
	}

	public void setCurrentSystemTime(long currentSystemTime) {
		this.currentSystemTime = currentSystemTime;
	}

	@Override
	public String toString() {
		return "SessionInfoRes [sessionsInfoRes=" + sessionsInfoRes + ", currentSystemTime=" + currentSystemTime
				+ ", toString()=" + super.toString() + "]";
	}
}
