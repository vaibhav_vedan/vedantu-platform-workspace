package com.vedantu.scheduling.response.instalearn;

public class TeacherSubscriptionResponse {

    private Long userId;
    private boolean subscribed;

    public TeacherSubscriptionResponse() {

    }

    public TeacherSubscriptionResponse(Long userId, boolean subscribed) {
        this.userId = userId;
        this.subscribed = subscribed;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public static class Constants {

        public static final String ID = "id";
        public static final String SUBSCRIBED = "subscribed";
    }

    @Override
    public String toString() {
        return "TeacherSubscriptionResponse{" + "userId=" + userId + ", subscribed=" + subscribed + '}';
    }

}
