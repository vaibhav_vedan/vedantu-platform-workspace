/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

/**
 *
 * @author ajith
 */
public class GetScheduledAndAllSessionCountRes {

    private Long scheduledSessionCount;
    private Long allSessionsCount;
    private Long trialSessionCount;
    private Long subscriptionRequestCount;
    private Long otfSubscriptionCount;

    public GetScheduledAndAllSessionCountRes() {
        super();
    }

    public GetScheduledAndAllSessionCountRes(Long scheduledSessionCount,
            Long allSessionsCount, Long trialSessionCount,
            Long subscriptionRequestCount, Long otfSubscriptionCount) {
        super();
        this.scheduledSessionCount = scheduledSessionCount;
        this.allSessionsCount = allSessionsCount;
        this.trialSessionCount = trialSessionCount;
        this.subscriptionRequestCount = subscriptionRequestCount;
        this.otfSubscriptionCount = otfSubscriptionCount;
    }

    public Long getScheduledSessionCount() {
        return scheduledSessionCount;
    }

    public void setScheduledSessionCount(Long scheduledSessionCount) {
        this.scheduledSessionCount = scheduledSessionCount;
    }

    public Long getAllSessionsCount() {
        return allSessionsCount;
    }

    public void setAllSessionsCount(Long allSessionsCount) {
        this.allSessionsCount = allSessionsCount;
    }

    public Long getTrialSessionCount() {
        return trialSessionCount;
    }

    public void setTrialSessionCount(Long trialSessionCount) {
        this.trialSessionCount = trialSessionCount;
    }

    public Long getSubscriptionRequestCount() {
        return subscriptionRequestCount;
    }

    public void setSubscriptionRequestCount(Long subscriptionRequestCount) {
        this.subscriptionRequestCount = subscriptionRequestCount;
    }

    public Long getOtfSubscriptionCount() {
        return otfSubscriptionCount;
    }

    public void setOtfSubscriptionCount(Long otfSubscriptionCount) {
        this.otfSubscriptionCount = otfSubscriptionCount;
    }

    @Override
    public String toString() {
        return "GetScheduledAndAllSessionCountRes [scheduledSessionCount="
                + scheduledSessionCount + ", allSessionsCount="
                + allSessionsCount + ", trialSessionCount=" + trialSessionCount
                + ", subscriptionRequestCount=" + subscriptionRequestCount
                + ", otfSubscriptionCount=" + otfSubscriptionCount + "]";
    }

}
