/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class GetUserDashboardSessionInfoRes extends AbstractRes{
    private Long firstOTOSessionTime;
    private Long lastOTOSessionTime;
    private Long firstOTFSessionTime;
    private Long lastOTFSessionTime;

    public Long getFirstOTOSessionTime() {
        return firstOTOSessionTime;
    }

    public void setFirstOTOSessionTime(Long firstOTOSessionTime) {
        this.firstOTOSessionTime = firstOTOSessionTime;
    }

    public Long getLastOTOSessionTime() {
        return lastOTOSessionTime;
    }

    public void setLastOTOSessionTime(Long lastOTOSessionTime) {
        this.lastOTOSessionTime = lastOTOSessionTime;
    }

    public Long getFirstOTFSessionTime() {
        return firstOTFSessionTime;
    }

    public void setFirstOTFSessionTime(Long firstOTFSessionTime) {
        this.firstOTFSessionTime = firstOTFSessionTime;
    }

    public Long getLastOTFSessionTime() {
        return lastOTFSessionTime;
    }

    public void setLastOTFSessionTime(Long lastOTFSessionTime) {
        this.lastOTFSessionTime = lastOTFSessionTime;
    }
    
}
