package com.vedantu.scheduling.response.session;

import com.vedantu.session.pojo.EntityType;

public class ContextSessionDurationResponse {
	private String contextId;
	private EntityType contextType;

	private int upcomingDuration = 0;
	private int endedSessionDuration = 0;
	private int activeSessionDuration = 0;

	public ContextSessionDurationResponse() {
		super();
	}

	public ContextSessionDurationResponse(String contextId, EntityType contextType) {
		super();
		this.contextId = contextId;
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public void setUpcomingDuration(int upcomingDuration) {
		this.upcomingDuration = upcomingDuration;
	}

	public int getUpcomingDuration() {
		return upcomingDuration;
	}

	public int getEndedSessionDuration() {
		return endedSessionDuration;
	}

	public void setEndedSessionDuration(int endedSessionDuration) {
		this.endedSessionDuration = endedSessionDuration;
	}

	public int getActiveSessionDuration() {
		return activeSessionDuration;
	}

	public void setActiveSessionDuration(int activeSessionDuration) {
		this.activeSessionDuration = activeSessionDuration;
	}

	public void incrementActiveSessionDuration(long value) {
		this.activeSessionDuration += Long.valueOf(value).intValue();
	}

	public void incrementUpcomingDuration(long value) {
		this.upcomingDuration += Long.valueOf(value).intValue();
	}

	@Override
	public String toString() {
		return "ContextSessionDurationResponse [upcomingDuration=" + upcomingDuration + ", endedSessionDuration="
				+ endedSessionDuration + ", activeSessionDuration=" + activeSessionDuration + ", toString()="
				+ super.toString() + "]";
	}
}
