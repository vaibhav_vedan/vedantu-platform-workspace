package com.vedantu.scheduling.response.proposal;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.offering.pojo.OfferingInfo;
import com.vedantu.session.pojo.ProposalRepeatOrder;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.session.pojo.proposal.ProposalSlot;
import com.vedantu.util.enums.ProposalPaymentMode;
import com.vedantu.util.enums.ProposalStatus;

public class ProposalRes {

	private Long id;
	private UserBasicInfo fromUser;
	private UserBasicInfo toUser;
        private Long fromUserId;
	private Long toUserId;        
	private Long startTime;
	private Long endTime;
	private Board board;
	private String topicName;
	private Board parentBoard;
        private Long boardId;
	private String title;
	private ProposalRepeatOrder repeatOrder;
	private Integer repeatFrequency;
	private OfferingInfo offering;
	private Long subscriptionId;
	private ProposalStatus proposalStatus;
	private SessionPayoutType paymentType;
	private List<ProposalSlot> proposalSlotList;
	private boolean conflictOccured;
	private String reason;
	private String modifiedFieldData;
	private Long lastUpdated;
	private Long creationTime;
	private ProposalPaymentMode paymentMode;
	private boolean isCustomProposal;
	private Long createdBy;

	public ProposalRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

        public Long getFromUserId() {
            return fromUserId;
        }

        public void setFromUserId(Long fromUserId) {
            this.fromUserId = fromUserId;
        }

        public Long getToUserId() {
            return toUserId;
        }

        public void setToUserId(Long toUserId) {
            this.toUserId = toUserId;
        }

        public OfferingInfo getOffering() {
            return offering;
        }

        public void setOffering(OfferingInfo offering) {
            this.offering = offering;
        }

        public boolean isIsCustomProposal() {
            return isCustomProposal;
        }

        public void setIsCustomProposal(boolean isCustomProposal) {
            this.isCustomProposal = isCustomProposal;
        }
        
        

	public UserBasicInfo getFromUser() {
		return fromUser;
	}

	public void setFromUser(UserBasicInfo fromUser) {
		this.fromUser = fromUser;
	}

	public UserBasicInfo getToUser() {
		return toUser;
	}

	public void setToUser(UserBasicInfo toUser) {
		this.toUser = toUser;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

        public Long getBoardId() {
            return boardId;
        }

        public void setBoardId(Long boardId) {
            this.boardId = boardId;
        }

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public Board getParentBoard() {
		return parentBoard;
	}

	public void setParentBoard(Board parentBoard) {
		this.parentBoard = parentBoard;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ProposalRepeatOrder getRepeatOrder() {
		return repeatOrder;
	}

	public void setRepeatOrder(ProposalRepeatOrder repeatOrder) {
		this.repeatOrder = repeatOrder;
	}

	public Integer getRepeatFrequency() {
		return repeatFrequency;
	}

	public void setRepeatFrequency(Integer repeatFrequency) {
		this.repeatFrequency = repeatFrequency;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public ProposalStatus getProposalStatus() {
		return proposalStatus;
	}

	public void setProposalStatus(ProposalStatus proposalStatus) {
		this.proposalStatus = proposalStatus;
	}

	public SessionPayoutType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(SessionPayoutType paymentType) {
		this.paymentType = paymentType;
	}

	public List<ProposalSlot> getProposalSlotList() {
		return proposalSlotList;
	}

	public void setProposalSlotList(List<ProposalSlot> proposalSlotList) {
		this.proposalSlotList = proposalSlotList;
	}

	public boolean isConflictOccured() {
		return conflictOccured;
	}

	public void setConflictOccured(boolean conflictOccured) {
		this.conflictOccured = conflictOccured;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getModifiedFieldData() {
		return modifiedFieldData;
	}

	public void setModifiedFieldData(String modifiedFieldData) {
		this.modifiedFieldData = modifiedFieldData;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public ProposalPaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(ProposalPaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public boolean isCustomProposal() {
		return isCustomProposal;
	}

	public void setCustomProposal(boolean isCustomProposal) {
		this.isCustomProposal = isCustomProposal;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

        @Override
        public String toString() {
            return "ProposalRes{" + "id=" + id + ", fromUser=" + fromUser + ", toUser=" + toUser + ", fromUserId=" + fromUserId + ", toUserId=" + toUserId + ", startTime=" + startTime + ", endTime=" + endTime + ", board=" + board + ", topicName=" + topicName + ", parentBoard=" + parentBoard + ", boardId=" + boardId + ", title=" + title + ", repeatOrder=" + repeatOrder + ", repeatFrequency=" + repeatFrequency + ", offering=" + offering + ", subscriptionId=" + subscriptionId + ", proposalStatus=" + proposalStatus + ", paymentType=" + paymentType + ", proposalSlotList=" + proposalSlotList + ", conflictOccured=" + conflictOccured + ", reason=" + reason + ", modifiedFieldData=" + modifiedFieldData + ", lastUpdated=" + lastUpdated + ", creationTime=" + creationTime + ", paymentMode=" + paymentMode + ", isCustomProposal=" + isCustomProposal + ", createdBy=" + createdBy + '}';
        }
}
