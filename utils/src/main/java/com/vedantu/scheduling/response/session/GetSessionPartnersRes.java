/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetSessionPartnersRes extends AbstractRes {

	private List<UserBasicInfo> partners;

	public GetSessionPartnersRes(List<UserBasicInfo> partners) {
		super();
		this.partners = partners;
	}

	public List<UserBasicInfo> getPartners() {
		return partners;
	}

	@Override
	public String toString() {
		return " {partners=" + partners + "}";
	}

}
