/**
 * 
 */
package com.vedantu.scheduling.response.session;

import java.util.Set;

import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OTFSessionResponse {

    private String id;
    private Set<String> batchIds;
    //private Long boardId;
    private String title;
    private String subject;
    //private String description;
    private String presenter;
    //private Set<Long> taIds;
    private Long startTime;
    private Long endTime;
    private String sessionURL;
    private String presenterURL;
    private String replayVideoURL;
    //private List<String> replayUrls;
    //private boolean isRescheduled;
    //private com.vedantu.onetofew.enums.SessionState state;
    private OTMSessionType otmSessionType;
    //private String rescheduledFromId;
    //private List<String> attendees;
    //private List<OTFSessionAttendeeInfo> attendeeInfos;
    //private List<OTFCourseBasicInfo> courseInfos;
    //private List<RescheduleData> rescheduleData;
    //private String organizerAccessToken;
    //private UserBasicInfo presenterInfo;
    //private com.vedantu.onetofew.enums.EntityType type;
    //private String meetingId;
    //private String presenterUrl;
    //private String remarks;
    //private Boolean teacherJoined;
    private Long creationTime;
    //private String createdBy;
    private Long lastUpdated;
    //private String callingUserId;
    private OTFSessionToolType sessionToolType;
    private OTFSessionContextType sessionContextType;
    private String sessionContextId;
    //private Set<String> flags = new HashSet<>();
    //private RemarkResp previousSessionRemark;
    //private String previousPresenter;
    //private String vimeoId;
    //private String agoraVimeoId;
    //private List<ContentInfo> preSessionContents;
    //private List<ContentInfo> postSessionContents;
    //private String ownerId;
    //private String ownerComment;
   // private String adminJoined;
    //private String techSupport;
    //private String finalIssueStatus;
    //private List<String> curricullumTopics;
    //private List<String> baseTreeTopics;
    //private  String primaryBatchId;
    //private String parentSessionId;
    //private List<String> groupNames;
}
