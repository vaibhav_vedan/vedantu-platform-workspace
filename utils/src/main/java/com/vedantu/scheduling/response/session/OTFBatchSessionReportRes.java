/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

/**
 *
 * @author jeet
 */
public class OTFBatchSessionReportRes {
    private String batchId;
    private Long firstSessionTime;
    private Long lastSessionTime;
    private Long upcomingSessionTime;
    private Long totalSessionCount;
    private Long completedSessionCount;
    private Integer sessionAttendedByUser;

    public Long getFirstSessionTime() {
        return firstSessionTime;
    }

    public void setFirstSessionTime(Long firstSessionTime) {
        this.firstSessionTime = firstSessionTime;
    }

    public Long getLastSessionTime() {
        return lastSessionTime;
    }

    public void setLastSessionTime(Long lastSessionTime) {
        this.lastSessionTime = lastSessionTime;
    }

    public Long getUpcomingSessionTime() {
        return upcomingSessionTime;
    }

    public void setUpcomingSessionTime(Long upcomingSessionTime) {
        this.upcomingSessionTime = upcomingSessionTime;
    }

    public Long getTotalSessionCount() {
        return totalSessionCount;
    }

    public void setTotalSessionCount(Long totalSessionCount) {
        this.totalSessionCount = totalSessionCount;
    }

    public Long getCompletedSessionCount() {
        return completedSessionCount;
    }

    public void setCompletedSessionCount(Long completedSessionCount) {
        this.completedSessionCount = completedSessionCount;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Integer getSessionAttendedByUser() {
        return sessionAttendedByUser;
    }

    public void setSessionAttendedByUser(Integer sessionAttendedByUser) {
        this.sessionAttendedByUser = sessionAttendedByUser;
    }
    
}
