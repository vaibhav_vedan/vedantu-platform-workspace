package com.vedantu.scheduling.response.session;

public class GetSessionBillDetailsRes {
	private Integer duration;
	private Integer billAmount;

	public GetSessionBillDetailsRes() {
		super();
	}

	public GetSessionBillDetailsRes(Integer duration, Integer billAmount) {
		super();
		this.duration = duration;
		this.billAmount = billAmount;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Integer billAmount) {
		this.billAmount = billAmount;
	}

	@Override
	public String toString() {
		return "GetSessionBillDetailsRes [duration=" + duration + ", billAmount=" + billAmount + ", toString()="
				+ super.toString() + "]";
	}
}
