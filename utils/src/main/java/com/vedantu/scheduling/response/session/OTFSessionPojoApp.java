/**
 * 
 */
package com.vedantu.scheduling.response.session;

import java.util.List;

import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OTFSessionPojoApp {
	
	private String sessionId;
	
	private String title;
	
	private Long startTime;
	
	private Long endTime;
	
	private TeacherBasicInfo teacherBasicInfo;
	
	private String subject;
	
	private Boolean isReminderSet;
	
	private String sessionUrl;
	
	private String courseTitle;
	
	private List<String> batchIds;
	
	private String bundleId;
	
	@Builder.Default
    private Boolean isEnrolled = false;
    
    private EnrollmentState enrollmentState;
    
    private EntityStatus enrollmentStatus;

}
