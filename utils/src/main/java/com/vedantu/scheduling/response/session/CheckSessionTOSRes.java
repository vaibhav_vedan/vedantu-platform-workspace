/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

/**
 *
 * @author ajith
 */
public class CheckSessionTOSRes {

    private boolean isTOS = false;

    public CheckSessionTOSRes() {
        this.isTOS = false;
    }

    public boolean isIsTOS() {
        return isTOS;
    }

    public void setIsTOS(boolean isTOS) {
        this.isTOS = isTOS;
    }

    @Override
    public String toString() {
        return "CheckSessionTOSServlet{" + "isTOS=" + isTOS + '}';
    }

}
