/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class CounselorDashboardSessionsInfoRes extends AbstractRes{
    private String _id;
    private Long lastSessionEndTime;
    private Integer totalNoOfSessions;
    private Long firstSessionStartTime;

    public CounselorDashboardSessionsInfoRes() {
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public Long getLastSessionEndTime() {
        return lastSessionEndTime;
    }

    public void setLastSessionEndTime(Long lastSessionEndTime) {
        this.lastSessionEndTime = lastSessionEndTime;
    }

    public Integer getTotalNoOfSessions() {
        return totalNoOfSessions;
    }

    public void setTotalNoOfSessions(Integer totalNoOfSessions) {
        this.totalNoOfSessions = totalNoOfSessions;
    }

    public Long getFirstSessionStartTime() {
        return firstSessionStartTime;
    }

    public void setFirstSessionStartTime(Long firstSessionStartTime) {
        this.firstSessionStartTime = firstSessionStartTime;
    }
    
     public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String LAST_SESSION_END_TIME = "lastSessionEndTime";
        public static final String TOTAL_NO_OF_SESSIONS = "totalNoOfSessions";
        public static final String FIRST_SESSION_START_TIME = "firstSessionStartTime";
    }
}
