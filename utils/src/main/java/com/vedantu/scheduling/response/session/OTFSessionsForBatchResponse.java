/**
 * 
 */
package com.vedantu.scheduling.response.session;

import java.util.List;

/**
 * @author subarna
 *
 */
public class OTFSessionsForBatchResponse {
	
	private String batchId;
	
	private List<OTFSessionPojoApp> sessions;
	
	private String subject;
	
	private String teacherName;
	
	

}
