/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

import com.vedantu.scheduling.pojo.session.SessionInfo;

/**
 *
 * @author jeet
 */
public class CreateDemoSessionRes {
    private SessionInfo sessionInfo;
    private String studentJoinLink;
    public CreateDemoSessionRes(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public String getStudentJoinLink() {
        return studentJoinLink;
    }

    public void setStudentJoinLink(String studentJoinLink) {
        this.studentJoinLink = studentJoinLink;
    }

    @Override
    public String toString() {
        return "CreateDemoSessionRes{" + "sessionInfo=" + sessionInfo + ", studentJoinLink=" + studentJoinLink + '}';
    }
}
