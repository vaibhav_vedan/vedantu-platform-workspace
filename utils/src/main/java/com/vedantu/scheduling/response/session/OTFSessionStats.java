package com.vedantu.scheduling.response.session;

public class OTFSessionStats {

    private Integer sessionsPlanned;
    private Integer sessionsScheduled;
    private Integer sessionsDone;
    private Integer sessionsCancelled;
    private Integer sessionsRescheduled;
    private Integer extraSessions;

    public Integer getSessionsPlanned() {
        return sessionsPlanned;
    }

    public void setSessionsPlanned(Integer sessionsPlanned) {
        this.sessionsPlanned = sessionsPlanned;
    }

    public Integer getSessionsScheduled() {
        return sessionsScheduled;
    }

    public void setSessionsScheduled(Integer sessionsScheduled) {
        this.sessionsScheduled = sessionsScheduled;
    }

    public Integer getSessionsDone() {
        return sessionsDone;
    }

    public void setSessionsDone(Integer sessionsDone) {
        this.sessionsDone = sessionsDone;
    }

    public Integer getSessionsCancelled() {
        return sessionsCancelled;
    }

    public void setSessionsCancelled(Integer sessionsCancelled) {
        this.sessionsCancelled = sessionsCancelled;
    }

    public Integer getSessionsRescheduled() {
        return sessionsRescheduled;
    }

    public void setSessionsRescheduled(Integer sessionsRescheduled) {
        this.sessionsRescheduled = sessionsRescheduled;
    }

    public Integer getExtraSessions() {
        return extraSessions;
    }

    public void setExtraSessions(Integer extraSessions) {
        this.extraSessions = extraSessions;
    }

    public void incSessionsScheduled(Integer inc){
        this.sessionsScheduled+=inc;
    }

    public void incSessionsCancelled(Integer inc){
        this.sessionsCancelled+=inc;
    }

    public void incSessionsRescheduled(Integer inc){
        this.sessionsRescheduled+=inc;
    }

    public void incSessionsDone(Integer inc){
        this.sessionsDone+=inc;
    }

    public void incSessionsPlanned(Integer inc){
        this.sessionsPlanned+=inc;
    }

    public void incExtraSessions(Integer inc){
        this.extraSessions+=inc;
    }



}
