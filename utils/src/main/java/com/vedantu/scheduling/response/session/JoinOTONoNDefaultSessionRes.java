/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response.session;

/**
 *
 * @author ajith
 */
public class JoinOTONoNDefaultSessionRes {

    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "JoinOTONoNDefaultSessionRes{" + "link=" + link + '}';
    }

}
