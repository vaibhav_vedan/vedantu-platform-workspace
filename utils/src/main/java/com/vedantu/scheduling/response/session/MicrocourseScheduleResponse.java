/**
 * 
 */
package com.vedantu.scheduling.response.session;

import java.util.List;

import com.vedantu.scheduling.response.session.OTFSessionPojoApp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MicrocourseScheduleResponse {
	
	private String bundleId;
	
	private String batchId;
	
	private List<OTFSessionPojoApp> sessionPojoApp;
	
	private String teacherName;
	
	private String subject;
	

}
