/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.proposal;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetProposalReq extends AbstractFrontEndReq {

    private Long id;

    public GetProposalReq() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (id == null) {
            errors.add("id");
        }

        return errors;
    }

    @Override
    public String toString() {
        return super.toString() + " GetProposalReq{" + "id=" + id + '}';
    }

}
