package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.vedantu.User.Role;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetUserUpcomingSessionReq extends AbstractFrontEndListReq {

    private List<SessionState> sessionStates;
    private String query;
    private Long startTime;
    private Long endTime;
    private boolean populateAttendees = true;

    public GetUserUpcomingSessionReq() {
        super();
    }

    public GetUserUpcomingSessionReq(Integer start, Integer size) {
        super(start, size);
    }

    public GetUserUpcomingSessionReq(Integer start, Integer size, Role callingUserRole, Long callingUserId) {
        super(callingUserId, callingUserRole, start, size);
    }

    public List<SessionState> getSessionStates() {
        return sessionStates;
    }

    public void setSessionStates(List<SessionState> sessionStates) {
        this.sessionStates = sessionStates;
    }

    public boolean isPopulateAttendees() {
        return populateAttendees;
    }

    public void setPopulateAttendees(boolean populateAttendees) {
        this.populateAttendees = populateAttendees;
    }

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        if (getCallingUserId() == null || getCallingUserId() <= 0l) {
            errors.add("callingUserId");
        }
        if (!Role.TEACHER.equals(getCallingUserRole()) && !Role.STUDENT.equals(getCallingUserRole())) {
            errors.add("callingUserRole");
        }
        Predicate<Long> function = (e) -> e != null && e > 0;
        if (function.test(startTime) && !function.test(endTime)) {
            errors.add("endTime is mandatory when start time is specified");
        }

        if (!function.test(startTime) && function.test(endTime)) {
            errors.add("startTime is mandatory when end time is specified");
        }

        if (function.test(startTime) && function.test(endTime) && startTime > endTime) {
            errors.add("startTime is greater than endTime");
        }
        return errors;
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
}
