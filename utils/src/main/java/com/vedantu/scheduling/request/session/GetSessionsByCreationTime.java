/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.session;

import com.vedantu.util.enums.SessionModel;

public class GetSessionsByCreationTime {

    Long studentId;
    Long teacherId;
    Long fromtime;
    Long toTime;
    SessionModel sessionModel;

    public GetSessionsByCreationTime() {
    }

    public GetSessionsByCreationTime(Long studentId, Long teacherId, Long fromtime, Long toTime,
            SessionModel sessionModel) {
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.fromtime = fromtime;
        this.toTime = toTime;
        this.sessionModel = sessionModel;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getFromtime() {
        return fromtime;
    }

    public void setFromtime(Long fromtime) {
        this.fromtime = fromtime;
    }

    public Long getToTime() {
        return toTime;
    }

    public void setToTime(Long toTime) {
        this.toTime = toTime;
    }

    public SessionModel getSessionModel() {
        return sessionModel;
    }

    public void setSessionModel(SessionModel sessionModel) {
        this.sessionModel = sessionModel;
    }

    @Override
    public String toString() {
        return "GetSessionsByCreationTime{" + "studentId=" + studentId + ", teacherId=" + teacherId + ", fromtime="
                + fromtime + ", toTime=" + toTime + ", sessionModel=" + sessionModel + '}';
    }

}
