package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.User.FeatureSource;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SessionScheduleReq extends AbstractFrontEndReq {

	private String subject;
	private String topic;
	private String title;
	private String description;
	private Long startTime;
	private Long endTime;
	private SessionPayoutType type;
	private Long subscriptionId;
	private List<Long> studentIds;
	private Long teacherId;
	private FeatureSource sessionSource;
	private SessionModel sessionModel;
	private RequestSource deviceSource;
	private Long offeringId;
	private Long proposalId;
	private Boolean noCalendarCheck;

	public SessionScheduleReq() {
		super();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public SessionPayoutType getType() {
		return type;
	}

	public void setType(SessionPayoutType type) {
		this.type = type;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public FeatureSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(FeatureSource sessionSource) {
		this.sessionSource = sessionSource;
	}

	public SessionModel getSessionModel() {
		return sessionModel;
	}

	public void setSessionModel(SessionModel sessionModel) {
		this.sessionModel = sessionModel;
	}

	public RequestSource getDeviceSource() {
		return deviceSource;
	}

	public void setDeviceSource(RequestSource deviceSource) {
		this.deviceSource = deviceSource;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Long getProposalId() {
		return proposalId;
	}

	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}

	public Boolean getNoCalendarCheck() {
		return noCalendarCheck;
	}

	public void setNoCalendarCheck(Boolean noCalendarCheck) {
		this.noCalendarCheck = noCalendarCheck;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(this.title)) {
			errors.add("title");
		}
		if (StringUtils.isEmpty(this.startTime)) {
			errors.add("startTime");
		}
		if (CollectionUtils.isEmpty(studentIds)) {
			errors.add("studentIds");
		}
		if (teacherId == null || teacherId <= 0l) {
			errors.add("teacherId");
		}

		// Validate duration
		if (this.endTime != null && this.endTime >= 0l && calculateMaxDuration() < (this.endTime - this.startTime)) {
			errors.add("maxDuration");
		}

		return errors;
	}

	private long calculateMaxDuration() {
		return ConfigUtils.INSTANCE.getLongValue("max.session.duration") * DateTimeUtils.MILLIS_PER_MINUTE;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!errors.isEmpty()) {
			throw new BadRequestException(ErrorCode.INVALID_SESSION_FIELDS, Arrays.toString(errors.toArray()));
		}
	}
}
