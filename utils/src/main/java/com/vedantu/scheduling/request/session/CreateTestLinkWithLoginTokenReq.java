package com.vedantu.scheduling.request.session;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class CreateTestLinkWithLoginTokenReq extends AbstractFrontEndReq {
    private Long studentId;
    private String testLink;
    private String origin ;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getTestLink() {
        return testLink;
    }

    public void setTestLink(String testLink) {
        this.testLink = testLink;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (null == studentId) {
            errors.add("studentId");
        }
        if (null == testLink) {
            errors.add("testLink");
        }
        if(StringUtils.isEmpty(origin)){
            errors.add("origin");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "CreateUserAuthenticationTokenReq{" + "studentId=" + studentId + ", testLink=" + testLink + ", origin=" + origin + '}';
    }
}
