package com.vedantu.scheduling.request.session;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProcessSessionPayoutReq {

    private Long sessionId;
    private Long billingDuration;
    private int teacherPayout;
    private int studentCharge;
    private Boolean isUpdateRequest;

    public ProcessSessionPayoutReq() {
        super();
    }

    public ProcessSessionPayoutReq(Long sessionId, Long billingDuration, int teacherPayout, int studentCharge, Boolean isUpdateRequest) {
        this.sessionId = sessionId;
        this.billingDuration = billingDuration;
        this.teacherPayout = teacherPayout;
        this.studentCharge = studentCharge;
        this.isUpdateRequest = isUpdateRequest;
    }
    
    

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getBillingDuration() {
        return billingDuration;
    }

    public void setBillingDuration(Long billingDuration) {
        this.billingDuration = billingDuration;
    }

    public int getTeacherPayout() {
        return teacherPayout;
    }

    public void setTeacherPayout(int teacherPayout) {
        this.teacherPayout = teacherPayout;
    }

    public int getStudentCharge() {
        return studentCharge;
    }

    public void setStudentCharge(int studentCharge) {
        this.studentCharge = studentCharge;
    }

    public Boolean getIsUpdateRequest() {
        return isUpdateRequest;
    }

    public void setIsUpdateRequest(Boolean isUpdateRequest) {
        this.isUpdateRequest = isUpdateRequest;
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(this.sessionId)) {
            errors.add("sessionId");
        }
        if (this.billingDuration == null) {
            errors.add("billingDuration");
        }
        return errors;
    }

    public void validate() throws BadRequestException {
        List<String> errors = collectErrors();
        if (!errors.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Arrays.toString(errors.toArray()));
        }
    }

    @Override
    public String toString() {
        return "ProcessSessionPayoutReq [sessionId=" + sessionId + ", billingDuration=" + billingDuration
                + ", teacherPayout=" + teacherPayout + ", studentCharge=" + studentCharge + ", isUpdateRequest="
                + isUpdateRequest + ", toString()=" + super.toString() + "]";
    }
}
