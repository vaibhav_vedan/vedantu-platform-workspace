package com.vedantu.scheduling.request.session;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetOTFSessionByIdsReq extends AbstractFrontEndListReq {

    private List<String> sessionIds;
    private List<String> includeSet;
}
