/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.session;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class SetVimeoVideoIdRequest extends AbstractFrontEndReq {

    private String vimeoId;

    public SetVimeoVideoIdRequest() {
        super();
    }

    public SetVimeoVideoIdRequest(String vimeoId) {
        super();
        this.vimeoId = vimeoId;
    }

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    @Override
    public String toString() {
        return "SetVimeoVideoIdRequest{" + "vimeoId=" + vimeoId + '}';
    }
    
    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == vimeoId) {
            errors.add("userId");
        }

        return errors;
    }
}
