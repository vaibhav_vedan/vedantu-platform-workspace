package com.vedantu.scheduling.request.proposal;

import java.util.List;

public class UpdateProposalReq extends AddProposalReq {

    private Long id;

    public UpdateProposalReq() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (id == null) {
            errors.add("id");
        }
        return errors;
    }

    @Override
    public String toString() {
        return super.toString() + " UpdateProposalServletReq{" + "id=" + id + '}';
    }
}
