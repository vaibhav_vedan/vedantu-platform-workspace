package com.vedantu.scheduling.request.session;

public class MarkSessionActiveReq {
	private Long sessionId;

	public MarkSessionActiveReq() {
		super();
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "MarkSessionActiveReq [sessionId=" + sessionId + "]";
	}
}
