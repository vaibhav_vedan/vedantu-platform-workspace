package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.User.Role;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SubscriptioncoursePlanCancelSessionsReq extends AbstractFrontEndReq {
	private Long subscriptionId;
        private String contextId;
        private EntityType contextType;
	private String remark;

	public SubscriptioncoursePlanCancelSessionsReq() {
		super();
	}

	public SubscriptioncoursePlanCancelSessionsReq(Long subscriptionId, String remark, Long callingUserId, Role callingUserRole) {
		super(callingUserId,callingUserRole);
		this.subscriptionId = subscriptionId;
		this.remark = remark;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

        public String getContextId() {
            return contextId;
        }

        public void setContextId(String contextId) {
            this.contextId = contextId;
        }

        public EntityType getContextType() {
            return contextType;
        }

        public void setContextType(EntityType contextType) {
            this.contextType = contextType;
        }

        @Override
	public List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		if (this.subscriptionId == null && this.contextId==null) {
			errors.add("subscriptionId/contextId");
		}
		if (StringUtils.isEmpty(this.remark)) {
			errors.add("remark");
		}
		if (getCallingUserId()==null) {
			errors.add("callingUserId");
		}

		return errors;
	}

}
