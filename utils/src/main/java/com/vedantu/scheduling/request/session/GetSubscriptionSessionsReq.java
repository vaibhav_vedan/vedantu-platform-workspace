package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.User.Role;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetSubscriptionSessionsReq extends AbstractFrontEndListReq {

    public Long subscriptionId;

    public GetSubscriptionSessionsReq() {
        super();
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public GetSubscriptionSessionsReq(Long subscriptionId, Long callingUserId, Role callingUserRole, Integer start,
            Integer size) {
        super(callingUserId, callingUserRole, start, size);
        this.subscriptionId = subscriptionId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        if (this.subscriptionId == null) {
            errors.add("subscriptionId");
        }
        return errors;
    }

}
