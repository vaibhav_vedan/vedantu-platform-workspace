/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.session;

/**
 *
 * @author jeet
 */
public class GetSessionBillingDurationRes {
    
    private Long sessionId;
    private Long billingDuration;

    public GetSessionBillingDurationRes(Long sessionId, Long billingDuration) {
        this.sessionId = sessionId;
        this.billingDuration = billingDuration;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getBillingDuration() {
        return billingDuration;
    }

    public void setBillingDuration(Long billingDuration) {
        this.billingDuration = billingDuration;
    }

    @Override
    public String toString() {
        return "GetSessionBillingDurationRes{" + "sessionId=" + sessionId + ", billingDuration=" + billingDuration + '}';
    }
    
}
