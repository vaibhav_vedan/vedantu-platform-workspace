/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.proposal;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CancelSubscriptionProposalsReq extends AbstractFrontEndReq {

    private Long subscriptionId;

    public CancelSubscriptionProposalsReq() {
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (subscriptionId == null) {
            errors.add("subscriptionId");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "CancelSubscriptionProposalReq{" + "subscriptionId=" + subscriptionId + '}';
    }

}
