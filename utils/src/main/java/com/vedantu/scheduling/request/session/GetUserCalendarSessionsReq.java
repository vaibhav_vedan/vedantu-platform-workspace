package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetUserCalendarSessionsReq extends AbstractFrontEndListReq {
	private Long afterStartTime;
	private Long beforeStartTime;

	public GetUserCalendarSessionsReq() {
		super();
	}

	public Long getAfterStartTime() {
		return afterStartTime;
	}

	public void setAfterStartTime(Long afterStartTime) {
		this.afterStartTime = afterStartTime;
	}

	public Long getBeforeStartTime() {
		return beforeStartTime;
	}

	public void setBeforeStartTime(Long beforeStartTime) {
		this.beforeStartTime = beforeStartTime;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		if (this.afterStartTime == null) {
			errors.add("afterStartTime");
		}
		if (this.beforeStartTime == null) {
			errors.add("beforeStartTime");
		}

		return errors;
	}

	@Override
	public void verify() throws BadRequestException {
		List<String> verificationErrors = collectVerificationErrors();
		if (CollectionUtils.isNotEmpty(verificationErrors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, verificationErrors.toString());
		}
	}

	@Override
	public String toString() {
		return "GetUserCalendarSessionsReq [afterStartTime=" + afterStartTime + ", beforeStartTime=" + beforeStartTime
				+ ", toString()=" + super.toString() + "]";
	}
}
