/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.instalearn;

import com.vedantu.scheduling.enums.instalearn.InstaState;
import java.util.List;

/**
 *
 * @author ajith
 */
public class UpdateInstaRequestReq extends AddInstaRequestReq {

    private String id;

    public UpdateInstaRequestReq() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(id==null){
            errors.add("id");
        }
        if (super.getData() != null && InstaState.REJECTED.equals(super.getData().getState()) && super.getData().getNote() == null) {
            errors.add("note");
        }
        return errors;
    }

    @Override
    public String toString() {
        return super.toString() + " UpdateInstaRequestReq{" + "id=" + id + '}';
    }

}
