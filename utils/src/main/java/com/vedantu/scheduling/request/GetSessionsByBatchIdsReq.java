package com.vedantu.scheduling.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetSessionsByBatchIdsReq extends AbstractFrontEndListReq {

    private List<String> batchIds = new ArrayList<>();
    private Long startTime;
    private Long endTime;
}
