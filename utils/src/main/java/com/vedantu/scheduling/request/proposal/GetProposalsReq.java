package com.vedantu.scheduling.request.proposal;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.enums.ProposalStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetProposalsReq extends AbstractFrontEndListReq {
	private Long userId;
	private Long subscriptionId;
	private List<ProposalStatus> statusList = new ArrayList<>();
	private Long registeredFromTime;
	private Long registeredTillTime;

	public GetProposalsReq() {
		super();
	}

	public GetProposalsReq(Integer start, Integer size) {
		super(start, size);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public List<ProposalStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<ProposalStatus> statusList) {
		this.statusList = statusList;
	}

	public void addStatus(ProposalStatus status) {
		if (this.statusList == null) {
			this.statusList = new ArrayList<>();
		}
		statusList.add(status);
	}

	public Long getRegisteredFromTime() {
		return registeredFromTime;
	}

	public void setRegisteredFromTime(Long registeredFromTime) {
		this.registeredFromTime = registeredFromTime;
	}

	public Long getRegisteredTillTime() {
		return registeredTillTime;
	}

	public void setRegisteredTillTime(Long registeredTillTime) {
		this.registeredTillTime = registeredTillTime;
	}
}
