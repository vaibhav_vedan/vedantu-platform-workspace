package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractReq;

public class GetSessionDurationByContextIdReq extends AbstractReq {
	private List<String> contextIds = new ArrayList<>();
	private EntityType entityType;

	public GetSessionDurationByContextIdReq() {
		super();
	}

	public GetSessionDurationByContextIdReq(EntityType entityType) {
		super();
		this.entityType = entityType;
	}

	public List<String> getContextIds() {
		return contextIds;
	}

	public void setContextIds(List<String> contextIds) {
		this.contextIds = contextIds;
	}

	public void addContextId(String contextId) {
		contextIds.add(contextId);
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	protected List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		if (CollectionUtils.isEmpty(this.contextIds)) {
			errors.add("contextIds");
		}
		if (entityType == null) {
			errors.add("entityType");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "GetSessionDurationByContextIdListReq [contextIds=" + contextIds + ", entityType=" + entityType
				+ ", toString()=" + super.toString() + "]";
	}
}
