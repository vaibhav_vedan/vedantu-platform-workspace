package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

public class AdminSessionUpdateReq {
	private Long sessionId;
        @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
	private String note;

	public AdminSessionUpdateReq() {
		super();
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (this.sessionId == null) {
			errors.add("sessionId");
		}
		return errors;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Missing fields:" + Arrays.toString(errors.toArray()));
		}
	}

	@Override
	public String toString() {
		return "AdminRescheduleReq [sessionId=" + sessionId + ", note=" + note + ", toString()=" + super.toString()
				+ "]";
	}
}
