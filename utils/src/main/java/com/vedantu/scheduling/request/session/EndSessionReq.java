package com.vedantu.scheduling.request.session;

import com.vedantu.User.Role;
import com.vedantu.session.pojo.SessionEndType;
import com.vedantu.subscription.request.CancelSubscriptionSessionRequest;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

public class EndSessionReq extends AbstractFrontEndReq {

    private Long sessionId;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String remark;
    private Boolean endCoursePlan = Boolean.FALSE;
    private SessionEndType endType;
    private Long endedBy;

    public EndSessionReq() {
        super();
    }

    public EndSessionReq(Long sessionId, String remark, Long callingUserId, Role callingUserRole) {
        super(callingUserId, callingUserRole);
        this.sessionId = sessionId;
        this.remark = remark;
    }

    public EndSessionReq(CancelSubscriptionSessionRequest cancelSubscriptionSessionRequest) {
        super(cancelSubscriptionSessionRequest.getCallingUserId(), cancelSubscriptionSessionRequest.getCallingUserRole());
        this.sessionId = cancelSubscriptionSessionRequest.getSessionId();
        this.remark = cancelSubscriptionSessionRequest.getReason();
    }

    public EndSessionReq(Long sessionId, String remark) {
        this(sessionId, remark, null, null);
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getEndCoursePlan() {
        return endCoursePlan;
    }

    public void setEndCoursePlan(Boolean endCoursePlan) {
        this.endCoursePlan = endCoursePlan;
    }

    public SessionEndType getEndType() {
        return endType;
    }

    public void setEndType(SessionEndType endType) {
        this.endType = endType;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }
    
}
