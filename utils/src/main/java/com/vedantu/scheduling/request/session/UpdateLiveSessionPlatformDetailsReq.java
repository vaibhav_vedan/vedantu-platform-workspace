/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.session;

import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class UpdateLiveSessionPlatformDetailsReq extends AbstractFrontEndReq {

    
    private Long sessionId;
    private LiveSessionPlatformType liveSessionPlatformType;
    private String teacherLink;
    private String studentLink;

    public UpdateLiveSessionPlatformDetailsReq() {
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public LiveSessionPlatformType getLiveSessionPlatformType() {
        return liveSessionPlatformType;
    }

    public void setLiveSessionPlatformType(LiveSessionPlatformType liveSessionPlatformType) {
        this.liveSessionPlatformType = liveSessionPlatformType;
    }

    public String getTeacherLink() {
        return teacherLink;
    }

    public void setTeacherLink(String teacherLink) {
        this.teacherLink = teacherLink;
    }

    public String getStudentLink() {
        return studentLink;
    }

    public void setStudentLink(String studentLink) {
        this.studentLink = studentLink;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (sessionId == null) {
            errors.add("sessionId");
        }        
        
        if (liveSessionPlatformType == null) {
            errors.add("liveSessionPlatformType");
        }
        if (null == teacherLink) {
            errors.add("teacherLink");
        }
        if (null == studentLink) {
            errors.add("studentLink");
        }
        //TODO check meeting id of teacher and student matching
        return errors;
    }

}
