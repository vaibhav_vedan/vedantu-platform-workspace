/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.instalearn;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class SetTeacherSubscriptionReq extends AbstractFrontEndReq {
    
    private Long userId;
    private boolean subscribed;
    
    public SetTeacherSubscriptionReq() {
        super();
    }
    
    public Long getUserId() {
        return userId;
    }
    
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    public boolean getSubscribed() {
        return subscribed;
    }
    
    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }
    
    @Override
    public String toString() {
        return super.toString() + " SetTeacherSubscriptionReq{" + "userId=" + userId + ", subscribed=" + subscribed + '}';
    }
    
}
