package com.vedantu.scheduling.request.session;

import com.vedantu.User.Role;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class GetUserPastSessionReq extends AbstractFrontEndListReq {

    private Long userId;
    private String query;
    private List<SessionState> sessionStates;
    private Long startTime;
    private Long endTime;

    public GetUserPastSessionReq() {
        super();
    }

    public GetUserPastSessionReq(Integer start, Integer size) {
        super(start, size);
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getQueryString() {
        // TODO : Implement this
        return "";
    }
    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        Predicate<Long> function = (e) -> e != null && e > 0;
        if (function.test(startTime) && !function.test(endTime)) {
            errors.add("endTime is mandatory when start time is specified");
        }

        if (!function.test(startTime) && function.test(endTime)) {
            errors.add("startTime is mandatory when end time is specified");
        }

        if (function.test(startTime) && function.test(endTime) && startTime > endTime) {
            errors.add("startTime is greater than endTime");
        }
        return errors;
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return the sessionStates
     */
    public List<SessionState> getSessionStates() {
        return sessionStates;
    }

    /**
     * @param sessionStates the sessionStates to set
     */
    public void setSessionStates(List<SessionState> sessionStates) {
        this.sessionStates = sessionStates;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
}
