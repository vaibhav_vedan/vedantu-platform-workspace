package com.vedantu.scheduling.request.session;

import com.vedantu.User.Role;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetUserLatestActiveSessionReq extends AbstractFrontEndListReq {

	public GetUserLatestActiveSessionReq() {
		super();
	}

	public GetUserLatestActiveSessionReq(Integer start, Integer size, Role callingUserRole, Long callingUserId) {
		super(callingUserId, callingUserRole, start, size);
	}

	public GetUserLatestActiveSessionReq(Integer start, Integer size) {
		super(start, size);
	}

}
