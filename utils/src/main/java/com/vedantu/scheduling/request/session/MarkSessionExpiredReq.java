package com.vedantu.scheduling.request.session;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class MarkSessionExpiredReq extends AbstractFrontEndReq{

    private Long sessionId;

    public MarkSessionExpiredReq() {
        super();
    }

    public MarkSessionExpiredReq(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }
}
