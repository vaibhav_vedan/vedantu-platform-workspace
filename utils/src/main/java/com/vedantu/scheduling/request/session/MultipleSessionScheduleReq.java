package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.vedantu.util.ArrayUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.User.FeatureSource;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class MultipleSessionScheduleReq extends AbstractFrontEndReq {

	private String subject;
	private String topic;
	private String title;
	private String description;
	private List<SessionSlot> slots;
	private SessionPayoutType type;
	private Long subscriptionId;
	private List<Long> studentIds;
	private Long teacherId;
	private FeatureSource sessionSource;
	private SessionModel sessionModel;
	private RequestSource deviceSource;
	private Long offeringId;
	private Long proposalId;
	private Boolean noCalendarCheck;
	private EntityType contextType;
	private String contextId;        
        
        
	public MultipleSessionScheduleReq() {
		super();
	}

	public MultipleSessionScheduleReq(String subject, String topic, String title, String description,
			List<SessionSlot> slots, SessionPayoutType type, Long subscriptionId, List<Long> studentIds, Long teacherId,
			FeatureSource sessionSource, SessionModel sessionModel, RequestSource deviceSource, Long callingUserId,
			Long offeringId, Long proposalId) {
		super(callingUserId, null);
		this.subject = subject;
		this.topic = topic;
		this.title = title;
		this.description = description;
		this.slots = slots;
		this.type = type;
		this.subscriptionId = subscriptionId;
		this.studentIds = studentIds;
		this.teacherId = teacherId;
		this.sessionSource = sessionSource;
		this.sessionModel = sessionModel;
		this.deviceSource = deviceSource;
		this.offeringId = offeringId;
		this.proposalId = proposalId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<SessionSlot> getSlots() {
		return slots;
	}

	public void setSlots(List<SessionSlot> slots) {
		this.slots = slots;
	}

	public SessionPayoutType getType() {
		return type;
	}

	public void setType(SessionPayoutType type) {
		this.type = type;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public FeatureSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(FeatureSource sessionSource) {
		this.sessionSource = sessionSource;
	}

	public SessionModel getSessionModel() {
		return sessionModel;
	}

	public void setSessionModel(SessionModel sessionModel) {
		this.sessionModel = sessionModel;
	}

	public RequestSource getDeviceSource() {
		return deviceSource;
	}

	public void setDeviceSource(RequestSource deviceSource) {
		this.deviceSource = deviceSource;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Long getProposalId() {
		return proposalId;
	}

	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}

	public Boolean getNoCalendarCheck() {
		return noCalendarCheck;
	}

	public void setNoCalendarCheck(Boolean noCalendarCheck) {
		this.noCalendarCheck = noCalendarCheck;
	}

	public static List<Long> createStudentList(Long studentId) {
		List<Long> students = new ArrayList<>();
		students.add(studentId);
		return students;
	}

        public EntityType getContextType() {
            return contextType;
        }

        public void setContextType(EntityType contextType) {
            this.contextType = contextType;
        }

        public String getContextId() {
            return contextId;
        }

        public void setContextId(String contextId) {
            this.contextId = contextId;
        }

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (CollectionUtils.isEmpty(this.studentIds)) {
			errors.add("studentIds");
		}
		if (teacherId == null || teacherId <= 0l) {
			errors.add("teacherId");
		}
		if (CollectionUtils.isEmpty(this.slots)) {
			errors.add("slots");
		} else {
			long maxSessionDuration = calculateMaxDuration();
			for (SessionSlot slot : this.slots) {
				if (slot.getStartTime() == null || slot.getStartTime() <= 0l) {
					errors.add("slot.startTime:" + slot.toString());
				}
				if (StringUtils.isEmpty(this.title) && StringUtils.isEmpty(slot.getTitle())) {
					errors.add("slot.title:" + slot.toString());
				}

				// Validate duration
				if (slot.getEndTime() != null && slot.getEndTime() >= 0l
						&& maxSessionDuration < (slot.getEndTime() - slot.getStartTime())) {
					errors.add("slot.maxDuration:" + slot.toString());
				}
			}
		}
		if (ArrayUtils.isNotEmpty(slots)) {
			Collections.sort(slots, new Comparator<SessionSlot>() {
				@Override
				public int compare(SessionSlot o1, SessionSlot o2) {
					return o1.getStartTime().compareTo(o2.getStartTime());
				}
			});
			int size = slots.size();
			Long currentMillis = System.currentTimeMillis();
			Long maxSessionDuration = ConfigUtils.INSTANCE.getLongValue("max.session.duration") * DateTimeUtils.MILLIS_PER_MINUTE;
			for (int i = 0; i < size; i++) {
				SessionSlot slot = slots.get(i);
				if (i < size - 1) {
					SessionSlot nextSlot = slots.get(i + 1);
					if (slot.getEndTime() > nextSlot.getStartTime()) {
						errors.add("overlapping slots ");
						break;
					}
				}
				if (slot.getStartTime() < currentMillis) {
					errors.add("slot in the past " + slot);
					break;
				}
				if (slot.getStartTime() > slot.getEndTime()) {
					errors.add("slot starttime > endTime" + slot);
					break;
				}
				if (slot.getEndTime() - slot.getStartTime() > maxSessionDuration) {
					errors.add("session time greater than max session duration "
							+ maxSessionDuration + " for slot " + slot);
					break;
				}

			}
		}

		return errors;
	}

	private long calculateMaxDuration() {
		return ConfigUtils.INSTANCE.getLongValue("max.session.duration") * DateTimeUtils.MILLIS_PER_MINUTE;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!errors.isEmpty()) {
			throw new BadRequestException(ErrorCode.INVALID_SESSION_FIELDS, Arrays.toString(errors.toArray()));
		}
	}
}
