/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.instalearn;

import com.vedantu.scheduling.enums.instalearn.InstaState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetInstaRequestsReq extends AbstractFrontEndListReq {

    private Long userId;
    private List<InstaState> states;

    public GetInstaRequestsReq() {
        super();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<InstaState> getStates() {
        return states;
    }

    public void setStates(List<InstaState> states) {
        this.states = states;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();         
        return errors;
    }

    @Override
    public String toString() {
        return super.toString() + " GetInstaRequestsReq{" + "userId=" + userId + ", states=" + states + '}';
    }

}
