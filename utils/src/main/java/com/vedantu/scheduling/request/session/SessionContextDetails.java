package com.vedantu.scheduling.request.session;

import com.vedantu.session.pojo.EntityType;

public class SessionContextDetails {
	private EntityType contextType;
	private String contextId;

	public SessionContextDetails() {
		super();
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	@Override
	public String toString() {
		return "GetSessionCountsByContextIdReq [contextType=" + contextType + ", contextId=" + contextId
				+ ", toString()=" + super.toString() + "]";
	}
}
