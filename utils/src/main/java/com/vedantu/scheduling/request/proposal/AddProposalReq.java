package com.vedantu.scheduling.request.proposal;

import java.util.List;

import com.vedantu.session.pojo.ProposalRepeatOrder;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.session.pojo.proposal.ProposalSlot;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.enums.ProposalPaymentMode;
import com.vedantu.util.enums.ProposalStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.Iterator;

public class AddProposalReq extends AbstractFrontEndReq {

    private String title;
    private Long toUserId;
    private Long fromUserId;
    private Long startTime;
    private Long endTime;
    private Long boardId;
    private String topicName;
    private ProposalRepeatOrder repeatOrder;
    private Integer repeatFrequency;
    private String reason;
    private Long offeringId;
    private Long subscriptionId;
    private SessionPayoutType paymentType;
    private ProposalPaymentMode paymentMode;
    private ProposalStatus proposalStatus;
    private List<ProposalSlot> proposalSlotList;
    private boolean isCustomProposal;

    public AddProposalReq() {
        super();
    }

    public AddProposalReq(String title, Long toUserId, Long fromUserId, Long startTime, Long endTime, Long boardId, String topicName,
            ProposalRepeatOrder repeatOrder, Integer repeatFrequency, String reason, Long offeringId,
            Long subscriptionId, SessionPayoutType paymentType, ProposalPaymentMode paymentMode,
            ProposalStatus proposalStatus, List<ProposalSlot> proposalSlotList, boolean isCustomProposal,
            Boolean noConflictCheck) {
        super();
        this.title = title;
        this.toUserId = toUserId;
        this.fromUserId = fromUserId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.boardId = boardId;
        this.topicName = topicName;
        this.repeatOrder = repeatOrder;
        this.repeatFrequency = repeatFrequency;
        this.reason = reason;
        this.offeringId = offeringId;
        this.subscriptionId = subscriptionId;
        this.paymentType = paymentType;
        this.paymentMode = paymentMode;
        this.proposalStatus = proposalStatus;
        this.proposalSlotList = proposalSlotList;
        this.isCustomProposal = isCustomProposal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isIsCustomProposal() {
        return isCustomProposal;
    }

    public void setIsCustomProposal(boolean isCustomProposal) {
        this.isCustomProposal = isCustomProposal;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public ProposalRepeatOrder getRepeatOrder() {
        return repeatOrder;
    }

    public void setRepeatOrder(ProposalRepeatOrder repeatOrder) {
        this.repeatOrder = repeatOrder;
    }

    public Integer getRepeatFrequency() {
        return repeatFrequency;
    }

    public void setRepeatFrequency(Integer repeatFrequency) {
        this.repeatFrequency = repeatFrequency;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public SessionPayoutType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(SessionPayoutType paymentType) {
        this.paymentType = paymentType;
    }

    public ProposalPaymentMode getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(ProposalPaymentMode paymentMode) {
        this.paymentMode = paymentMode;
    }

    public ProposalStatus getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(ProposalStatus proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public List<ProposalSlot> getProposalSlotList() {
        return proposalSlotList;
    }

    public void setProposalSlotList(List<ProposalSlot> proposalSlotList) {
        this.proposalSlotList = proposalSlotList;
    }

    public boolean isCustomProposal() {
        return isCustomProposal;
    }

    public void setCustomProposal(boolean isCustomProposal) {
        this.isCustomProposal = isCustomProposal;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ProposalStatus.UNSENT.equals(proposalStatus)
                || ProposalStatus.PENDING.equals(proposalStatus)) {
            if (fromUserId == null) {
                errors.add("fromUserId");
            }
            if (toUserId == null) {
                errors.add("toUserId");
            }
        }
        if (proposalSlotList != null && !proposalSlotList.isEmpty()) {
            Iterator<ProposalSlot> proposalSlotIterator = proposalSlotList.iterator();
            Long proposalEndTime = Long.valueOf(0);
            while (proposalSlotIterator.hasNext()) {
                ProposalSlot proposalSlot = proposalSlotIterator.next();
                Long scheduledDuration = proposalSlot.getEndTime() - proposalSlot.getStartTime();
                if (scheduledDuration > ConfigUtils.INSTANCE.getLongValue("session.maximum.duration.millis")) {
                    errors.add("Proposal slot exceeds max session duration");
                    break;
                }
                if (proposalSlot.getStartTime() < proposalEndTime) {
                    errors.add("Proposal slots are overlapping");
                    break;
                }
                proposalEndTime = proposalSlot.getEndTime();
            }
        }

        return errors;
    }

    @Override
    public String toString() {
        return "AddProposalServletReq{" + "title=" + title + ", toUserId=" + toUserId + ", fromUserId=" + fromUserId + ", startTime=" + startTime + ", endTime=" + endTime + ", boardId=" + boardId + ", topicName=" + topicName + ", repeatOrder=" + repeatOrder + ", repeatFrequency=" + repeatFrequency + ", reason=" + reason + ", offeringId=" + offeringId + ", subscriptionId=" + subscriptionId + ", paymentType=" + paymentType + ", paymentMode=" + paymentMode + ", proposalStatus=" + proposalStatus + ", proposalSlotList=" + proposalSlotList + ", isCustomProposal=" + isCustomProposal + '}';
    }
}
