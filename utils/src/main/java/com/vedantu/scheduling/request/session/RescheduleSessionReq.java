package com.vedantu.scheduling.request.session;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class RescheduleSessionReq extends AbstractFrontEndReq {
	private Long sessionId;
	private Long startTime;

	public RescheduleSessionReq() {
		super();
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
}
