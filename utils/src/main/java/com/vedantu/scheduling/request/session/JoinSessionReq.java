package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndUserReq;

public class JoinSessionReq extends AbstractFrontEndUserReq {

    private Long sessionId;

    public JoinSessionReq() {
        super();
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        if (sessionId == null) {
            errors.add("sessionId");
        }
        if (getUserId() == null) {
            errors.add("userId");
        }

        return errors;
    }

}
