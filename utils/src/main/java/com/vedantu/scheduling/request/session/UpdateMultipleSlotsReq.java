package com.vedantu.scheduling.request.session;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.session.pojo.SessionSlot;

public class UpdateMultipleSlotsReq {
	private List<Long> userIds;
	private List<SessionSlot> sessionSlots;
	private CalendarEntrySlotState slotState;
	private Boolean remove;
	private CalendarReferenceType referenceType;
	private String referenceId;

	public UpdateMultipleSlotsReq() {
		super();
	}

	public UpdateMultipleSlotsReq(Long userId, List<SessionSlot> sessionSlots, CalendarEntrySlotState slotState,
			Boolean remove, CalendarReferenceType referenceType, String referenceId) {
		this(new ArrayList<>(), sessionSlots, slotState, remove, referenceType, referenceId);
		this.userIds.add(userId);
	}

	public UpdateMultipleSlotsReq(List<Long> userIds, List<SessionSlot> sessionSlots, CalendarEntrySlotState slotState,
			Boolean remove, CalendarReferenceType referenceType, String referenceId) {
		super();
		this.userIds = userIds;
		this.sessionSlots = sessionSlots;
		this.slotState = slotState;
		this.remove = remove;
		this.referenceType = referenceType;
		this.referenceId = referenceId;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	public List<SessionSlot> getSessionSlots() {
		return sessionSlots;
	}

	public void setSessionSlots(List<SessionSlot> sessionSlots) {
		this.sessionSlots = sessionSlots;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	public Boolean getRemove() {
		return remove;
	}

	public void setRemove(Boolean remove) {
		this.remove = remove;
	}

	public CalendarReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(CalendarReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (!CollectionUtils.isEmpty(this.sessionSlots)) {
			errors.add("sessionSlots");
		}
		if (!CollectionUtils.isEmpty(this.userIds)) {
			errors.add("userIds");
		}
		if (this.slotState == null) {
			errors.add("slotState");
		}
		return errors;
	}

	@Override
	public String toString() {
		return "UpdateMultipleSlotsReq [userIds=" + userIds + ", sessionSlots=" + sessionSlots + ", slotState="
				+ slotState + ", remove=" + remove + "]";
	}
}
