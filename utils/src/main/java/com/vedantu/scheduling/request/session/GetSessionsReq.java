package com.vedantu.scheduling.request.session;

import com.vedantu.session.pojo.EntityType;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import com.vedantu.session.pojo.SessionSortType;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.Size;

public class GetSessionsReq extends AbstractFrontEndListReq {

    private Long userId;
    private Long studentId;
    private Long teacherId;
    private Long subscriptionId;
    private List<SessionState> sessionStates;
    private Long afterStartTime; // Value is Inclusive
    private Long beforeStartTime; // Value is Inclusive
    private Long afterEndTime; // Value is Inclusive
    private Long beforeEndTime; // Value is Inclusive
    private boolean includeAttendees;
    private SessionSortType sortType = SessionSortType.START_TIME_ASC;
    private SessionModel model;
    private EntityType contextType;
    private String contextId;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String query;
    private String batchId;

    private Set<String> batchIds;
    private Long startTime;
    private Long endTime;

    public GetSessionsReq() {
        super();
    }

    public GetSessionsReq(Integer start, Integer size) {
        super(start, size);
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public List<SessionState> getSessionStates() {
        return sessionStates;
    }

    public void setSessionStates(List<SessionState> sessionStates) {
        this.sessionStates = sessionStates;
    }

    public Long getAfterStartTime() {
        return afterStartTime;
    }

    public void setAfterStartTime(Long afterStartTime) {
        this.afterStartTime = afterStartTime;
    }

    public Long getBeforeStartTime() {
        return beforeStartTime;
    }

    public void setBeforeStartTime(Long beforeStartTime) {
        this.beforeStartTime = beforeStartTime;
    }

    public Long getAfterEndTime() {
        return afterEndTime;
    }

    public void setAfterEndTime(Long afterEndTime) {
        this.afterEndTime = afterEndTime;
    }

    public Long getBeforeEndTime() {
        return beforeEndTime;
    }

    public void setBeforeEndTime(Long beforeEndTime) {
        this.beforeEndTime = beforeEndTime;
    }

    public boolean isIncludeAttendees() {
        return includeAttendees;
    }

    public void setIncludeAttendees(boolean includeAttendees) {
        this.includeAttendees = includeAttendees;
    }

    public SessionSortType getSortType() {
        return sortType;
    }

    public void setSortType(SessionSortType sortType) {
        this.sortType = sortType;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }
    
    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        Predicate<Long> function = (e) -> e != null && e > 0;
        if (function.test(startTime) && !function.test(endTime)) {
            errors.add("endTime is mandatory when start time is specified");
        }

        if (!function.test(startTime) && function.test(endTime)) {
            errors.add("startTime is mandatory when end time is specified");
        }

        if (function.test(startTime) && function.test(endTime) && startTime > endTime) {
            errors.add("startTime is greater than endTime");
        }

        if (StringUtils.isNotBlank(batchId) && ArrayUtils.isNotEmpty(batchIds)) {
            errors.add("both batchid and array of batchids specified. specify either onr");
        }
        return errors;
    }
}
