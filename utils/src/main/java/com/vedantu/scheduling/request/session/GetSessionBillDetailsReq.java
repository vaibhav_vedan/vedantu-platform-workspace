package com.vedantu.scheduling.request.session;

import com.vedantu.util.fos.request.AbstractFrontEndUserReq;

public class GetSessionBillDetailsReq extends AbstractFrontEndUserReq {

	private Long sessionId;

	public GetSessionBillDetailsReq() {
		super();
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

}
