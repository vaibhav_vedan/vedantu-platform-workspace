/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.session;

/**
 *
 * @author jeet
 */
public class CreateDemoSessionReq extends SessionScheduleReq{
    private boolean createStudentJoinLink;

    public boolean isCreateStudentJoinLink() {
        return createStudentJoinLink;
    }

    public void setCreateStudentJoinLink(boolean createStudentJoinLink) {
        this.createStudentJoinLink = createStudentJoinLink;
    }

}
