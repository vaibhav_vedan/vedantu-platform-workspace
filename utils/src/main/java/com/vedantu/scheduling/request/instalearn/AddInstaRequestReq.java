/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.instalearn;

import com.vedantu.scheduling.enums.instalearn.InstaState;
import com.vedantu.scheduling.pojo.instalearn.InstaPayload;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AddInstaRequestReq extends AbstractFrontEndReq {

    private Long to;
    private Long from;
    private InstaPayload data;

    public AddInstaRequestReq() {
        super();
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public InstaPayload getData() {
        return data;
    }

    public void setData(InstaPayload data) {
        this.data = data;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (to == null && data != null && !InstaState.INIT.equals(data.getState())) {
            errors.add("to");
        }
        if (from == null && data != null && !InstaState.INIT.equals(data.getState())) {
            errors.add("from");
        }
        if (data == null) {
            errors.add("data");
        }
        return errors;
    }

    @Override
    public String toString() {
        return super.toString() + " AddInstaRequestReq{" + "to=" + to + ", from=" + from + ", data=" + data + '}';
    }

}
