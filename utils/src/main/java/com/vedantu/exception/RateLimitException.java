package com.vedantu.exception;

import org.apache.logging.log4j.Level;

public class RateLimitException extends VException {
	
	private static final long serialVersionUID = -6113100492430682133L;

	public RateLimitException(ErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}
	
	public RateLimitException(ErrorCode errorCode, String errorMessage, Level level) {
		super(errorCode, errorMessage, level);
	}
}