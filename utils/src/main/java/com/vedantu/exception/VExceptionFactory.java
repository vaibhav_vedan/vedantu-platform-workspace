package com.vedantu.exception;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class VExceptionFactory {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VExceptionFactory.class);

    private final Gson gson = new Gson();

    public static final VExceptionFactory INSTANCE = new VExceptionFactory();

    public VException throwableException(ClientResponse resp, boolean logAsError) {
        VException e = new VException(ErrorCode.SERVICE_ERROR, ErrorCode.SERVICE_ERROR.name());
        String errorString = resp.getEntity(String.class);
        if (logAsError) {
            logger.error("throwableException:  error received " + errorString);
        } else {
            logger.info("throwableException:  error received " + errorString);
        }

        try {
            JSONObject j = new JSONObject(errorString);
            if (j.has(Constants.ERROR_CODE) && StringUtils.isNotEmpty(j.getString(Constants.ERROR_CODE))) {
                String errorCode = j.getString(Constants.ERROR_CODE);
                String errorMessage = (j.has(Constants.ERROR_MESSAGE) ? j.getString(Constants.ERROR_MESSAGE)
                        : errorCode);
                e = new VException(ErrorCode.valueOfKey(errorCode), errorMessage);
            }
        } catch (Exception ex) {
            // swallow
        }
        return parseStatusCodeAndThrowException(resp, e);
    }

    private VException parseStatusCodeAndThrowException(ClientResponse resp, VException e) {
        int statusCode = resp.getStatus();
        logger.info("http status code is "+ statusCode);
        switch (statusCode) {
            case 400:
                e = new BadRequestException(e.getErrorCode(), e.getErrorMessage());
                break;
            case 401:
            	e = new UnauthorizedException(e.getErrorCode(), e.getErrorMessage());
            case 403:
                e = new ForbiddenException(e.getErrorCode(), e.getErrorMessage());
                break;
            case 404:
                e = new NotFoundException(e.getErrorCode(), e.getErrorMessage());
                break;
            case 409:
                e = new ConflictException(e.getErrorCode(), e.getErrorMessage());
                break;
            case 500:
            default:
                e = new InternalServerErrorException(e.getErrorCode(), e.getErrorMessage());
                break;
        }
        return e;
    }

    public void parseAndThrowException(ClientResponse resp) throws VException {
        parseAndThrowException(resp, false);
    }

    public void parseAndThrowException(ClientResponse resp, boolean logAsError) throws VException {
        if (resp.getStatus() != HttpStatus.OK.value() && resp.getStatus() != HttpStatus.CREATED.value()) {
            throw VExceptionFactory.INSTANCE.throwableException(resp, logAsError);
        }
    }

    public VException throwableFosException(ClientResponse resp, boolean logAsError) {
        VException e = new VException(ErrorCode.SERVICE_ERROR, ErrorCode.SERVICE_ERROR.name());
        String errorString = resp.getEntity(String.class);
        if (logAsError) {
            logger.error("throwableException:  error "+ resp.getStatus()+ " received " + errorString);
        } else {
            logger.info("throwableException:  error "+ resp.getStatus()+ " received " + errorString);
        }

        try {
            JSONObject j = new JSONObject(errorString);
            if (j.has(Constants.ERROR_CODE) && StringUtils.isNotEmpty(j.getString(Constants.ERROR_CODE))) {
                String errorCode = j.getString(Constants.ERROR_CODE);
                String errorMessage = (j.has(Constants.MESSAGE) ? j.getString(Constants.MESSAGE) : errorCode);
                e = new VException(ErrorCode.valueOfKey(errorCode), errorMessage);
            }
        } catch (Exception ex) {
            // swallow
        }
        return parseStatusCodeAndThrowException(resp, e);
    }

    public void parseAndThrowFosException(ClientResponse fosResp) throws VException {
        parseAndThrowException(fosResp, false);
    }

    public void parseAndThrowFosException(ClientResponse fosResp, boolean logAsError) throws VException {
        if (fosResp.getStatus() != HttpStatus.OK.value()) {
            throw throwableFosException(fosResp, logAsError);
        }
    }

    public String getErrorJSONString(String errorCode, String message) {

        JsonObject jsonObject = new JsonObject();
        if (StringUtils.isEmpty(errorCode)) {
            errorCode = ErrorCode.SERVICE_ERROR.name();
        }
        jsonObject.addProperty(Constants.ERROR_CODE, errorCode);
        jsonObject.addProperty(Constants.ERROR_MESSAGE, message);

        return gson.toJson(jsonObject);
    }

    public static ErrorCode parseErrorCode(String errorCodeString) {
        try {
            return ErrorCode.valueOf(errorCodeString);
        } catch (Exception ex) {
            return null;
        }
    }

    protected class Constants {

        protected static final String ERROR_CODE = "errorCode";
        protected static final String ERROR_MESSAGE = "errorMessage";
        protected static final String MESSAGE = "message";
    }

}
