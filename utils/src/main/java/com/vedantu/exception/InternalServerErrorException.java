/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.exception;

import org.apache.logging.log4j.Level;

/**
 *
 * @author ajith
 */
public class InternalServerErrorException extends VException {

	private static final long serialVersionUID = -3769869452955402036L;

	private static final Level defaultLevel = Level.ERROR;

	public InternalServerErrorException(ErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage, defaultLevel);
	}
	public InternalServerErrorException(ErrorCode errorCode, String errorMessage, Level level) {
		super(errorCode, errorMessage, level);
	}

}
