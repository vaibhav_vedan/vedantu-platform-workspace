package com.vedantu.exception;

import org.apache.logging.log4j.Level;

public class BroadcastAcceptedException extends VException {

	private static final long serialVersionUID = -4334654605072507146L;

	public BroadcastAcceptedException(ErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}


	public BroadcastAcceptedException(ErrorCode errorCode, String errorMessage, Level level) {
		super(errorCode, errorMessage, level);
	}

}
