package com.vedantu.exception;

import org.apache.logging.log4j.Level;

public class DuplicateEntryException extends VException {

    private static final long serialVersionUID = 50358750618791304L;

    public DuplicateEntryException(ErrorCode errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    public DuplicateEntryException(ErrorCode errorCode, String errorMessage, Level level) {
        super(errorCode, errorMessage, level);
    }

}
