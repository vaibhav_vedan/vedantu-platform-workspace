package com.vedantu.exception;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.data.mongodb.core.query.Query;

public class MongoQueryTimeoutException extends RuntimeException {

    private static final long serialVersionUID = -3567100492430682133L;
    private Logger logger = LogFactory.getLogger(MongoQueryTimeoutException.class);

    public MongoQueryTimeoutException(long timeout, Query query) {
        logger.error("Mongo operation timed out. Operation exceeded max limit={}, query={}", timeout, query);
    }

    public MongoQueryTimeoutException(long timeout, Throwable e, Query query) {
        super(e);
        logger.error("Mongo Operation exceeded max limit={}, error={}, query={}", timeout, e, query);
    }
}
