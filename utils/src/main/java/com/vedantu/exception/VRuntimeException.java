/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.exception;

import org.apache.logging.log4j.Level;

/**
 * @author mano
 */
public class VRuntimeException extends RuntimeException  {

    private static final long serialVersionUID = 1L;
    private ErrorCode errorCode;
    private String errorMessage;
    // This field is only used for logging uncaught exceptions at different log
    // levels
    // This default value can be different in a subclass.
    private transient Level level = Level.INFO;

    public VRuntimeException(ErrorCode errorCode, String errorMessage, Level level) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.level = level;
    }

    public VRuntimeException(ErrorCode errorCode, String errorMessage) {
        this(errorCode, errorMessage, Level.INFO);
    }

    public VRuntimeException(VException e) {
        super(e.getErrorMessage());
        this.errorCode = e.getErrorCode();
        this.errorMessage = e.getErrorMessage();
        this.level = e.getLevel();
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void String(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "VRuntimeException{" + "errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", level=" + level + '}';
    }
}
