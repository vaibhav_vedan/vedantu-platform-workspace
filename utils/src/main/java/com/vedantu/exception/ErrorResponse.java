package com.vedantu.exception;

import org.apache.commons.httpclient.HttpStatus;

public class ErrorResponse {

	public String message;
	public HttpStatus  httpStatus;
	
	public ErrorResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
	
}
