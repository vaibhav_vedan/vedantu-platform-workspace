package com.vedantu.exception;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.vedantu.util.LogFactory;
import com.vedantu.util.redis.RateLimitingRedisDao;
import java.util.List;

@EnableWebMvc
@ControllerAdvice
public class GlobalExceptionHandler {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GlobalExceptionHandler.class);
    
    @Autowired
    private RateLimitingRedisDao rateLimitRedisDAO;
    

    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT) // 409
    @ExceptionHandler(ConflictException.class)
    public String handleConflict(ConflictException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 400
    @ExceptionHandler(BadRequestException.class)
    public String handleBadRequest(BadRequestException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED) // 401
    @ExceptionHandler(UnauthorizedException.class)
    public String handleUnauthorizedException(UnauthorizedException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN) // 403
    @ExceptionHandler(ForbiddenException.class)
    public String handleForbiddenAccess(ForbiddenException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND) // 404
    @ExceptionHandler(NotFoundException.class)
    public String handleNotFound(NotFoundException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
    @ExceptionHandler(InternalServerErrorException.class)
    public String handleConflict(InternalServerErrorException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {IllegalArgumentException.class})
    @ResponseBody
    public String handleIAEException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        logger.info("ERROR ", ex);// Not using .error as it will flood sentry
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), ex.getMessage());
    }

    @ExceptionHandler(value = BroadcastAcceptedException.class)
    public ResponseEntity<String> handleBroadcastException(BroadcastAcceptedException ex, HttpServletRequest request) {
        logger.log(ex.getLevel(), "ERROR ", ex);
        String response = VExceptionFactory.INSTANCE.getErrorJSONString(ex.getErrorCode().name(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.ALREADY_REPORTED);
    }

    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    @ExceptionHandler(value = SQLException.class)
    @ResponseBody
    public String handleSQLException(Exception ex, HttpServletRequest request) {
        logger.error("ERROR ", ex);// Not using .error as it will flood sentry
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), ex.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public String handleNullPointerException(Exception ex, HttpServletRequest request) {
        logger.error("ERROR ", ex);// Not using .error as it will flood sentry
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), ex.getMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
    @ExceptionHandler(VException.class)
    public String handleVException(VException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) // 500
    @ExceptionHandler(VRuntimeException.class)
    public String handleVRuntimeException(VRuntimeException e) {
        logger.log(e.getLevel(), "ERROR ", e);
        return VExceptionFactory.INSTANCE.getErrorJSONString(e.getErrorCode().name(), e.getErrorMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public String handleException(Exception ex, HttpServletRequest request) {
        logger.info("ERROR ", ex);// Not using .error as it will flood sentry
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), ex.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public String handleRuntimeException(RuntimeException ex, HttpServletRequest request) {
        logger.info("ERROR ", ex);// Not using .error as it will flood sentry
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), ex.getMessage());
    }

    /*
    references: https://g00glen00b.be/validating-the-input-of-your-rest-api-with-spring/
    https://stackoverflow.com/questions/39504205/spring-validate-list-of-strings-for-non-empty-elements
    https://spring.io/guides/gs/validating-form-input/
    https://www.owasp.org/index.php/Bean_Validation_Cheat_Sheet
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handle(MethodArgumentNotValidException ex, HttpServletRequest request) {
//        String message = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage)
//                .collect(Collectors.toList()).toString();
        StringBuilder customMessage = new StringBuilder();
        List<FieldError> errors = ex.getBindingResult().getFieldErrors();
        for (FieldError fieldError : errors) {
            customMessage.append(fieldError.getField());
            customMessage.append(" : ");
            customMessage.append(fieldError.getDefaultMessage());
            customMessage.append(", ");
        }
        logger.info("BAD_REQUEST_ERROR " + customMessage.toString());// Not using .error as it will flood sentry
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.BAD_REQUEST_ERROR.name(), customMessage.toString());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseBody
    public String handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, HttpServletRequest request) {
        logger.info("ERROR", ex);// Not using .error as it will flood sentry
        String urlAccessed = request.getRequestURI();
        logger.info("req uri "+urlAccessed);
        
        try{
            if("/platform/user/login".equalsIgnoreCase(urlAccessed)){
                String ipAddress = request.getHeader("X-FORWARDED-FOR");
                if (ipAddress == null) {
                    ipAddress = request.getRemoteAddr();
                }                
                String key = "RL:" + ipAddress + ":" + "/platform/user/login";
                rateLimitRedisDAO.setex("BL:" + key, "", 864000);
                logger.warn("USER BLOCKED: " + key + "Requests Blocked from: " + ipAddress);
            }
        }catch(Exception e){
            logger.warn("error in blocking ip "+e.getMessage());
        }
        
        return VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), ex.getMessage());
    }


}
