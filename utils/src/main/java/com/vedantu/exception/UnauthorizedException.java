package com.vedantu.exception;

import org.apache.logging.log4j.Level;

public class UnauthorizedException extends VException {

	private static final long serialVersionUID = 1L;

	public UnauthorizedException(ErrorCode errorCode, java.lang.String errorMessage) {
		super(errorCode, errorMessage);
	}

	public UnauthorizedException(ErrorCode errorCode, String errorMessage, Level level) {
		super(errorCode, errorMessage, level);
	}
}
