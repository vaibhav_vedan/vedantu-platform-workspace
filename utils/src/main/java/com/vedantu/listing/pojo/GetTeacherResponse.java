package com.vedantu.listing.pojo;

import java.util.List;

public class GetTeacherResponse {

	long hits;
	List<EsTeacherData> teachers;
	
	public long getHits() {
		return hits;
	}
	public void setHits(long hits) {
		this.hits = hits;
	}
	public List<EsTeacherData> getTeachers() {
		return teachers;
	}
	public void setTeachers(List<EsTeacherData> teachers) {
		this.teachers = teachers;
	}
	
}
