/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums;

/**
 *
 * @author ajith
 */
public enum SocialContextType {
    CMDSQUESTION, CHALLENGE, COMMENT, CLAN, BATCH, CMDSTEST, CMDSVIDEO, STUDY_ENTRY_ITEM, DOUBT, CMDSPLAYLIST, ONBOARDING
}
