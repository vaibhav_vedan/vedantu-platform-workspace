package com.vedantu.platform.seo.dto;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.RichTextFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class CMDSQuestionDto extends AbstractSeoEntityDto {
    private String subject;
    private Set<String> grades;
    private Set<String> targets;
    private Set<String> targetGrade;
    private String name;
    private RichTextFormat questionBody;
    private Boolean makePageLive;
    private String video;
    public Set<String> topics;
    private Difficulty difficulty;
}
