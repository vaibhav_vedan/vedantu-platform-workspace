package com.vedantu.cmds.enums;

public enum OnboardingVideoType {
    INTRODUCTION(0,"Introduction about the course"),
    WAVE_DEMO(1,"Introduction to Wave"),
    QUIZ_AND_LEADERBOARD(2,"Quizzes and Leaderboard"),
    HOTSPOTS(3,"Hotspots"),
    DOUBTS(4,"In-class Doubts");

    private int index;
    private String title;

    OnboardingVideoType(int index,String title) {
        this.index=index;
        this.title=title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
