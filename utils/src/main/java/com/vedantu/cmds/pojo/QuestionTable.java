/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
public class QuestionTable {

    private List<QuestionTableRow> rows;

    public List<QuestionTableRow> getRows() {
        return rows;
    }

    public void setRows(List<QuestionTableRow> rows) {
        this.rows = rows;
    }

    public void addRow(QuestionTableRow row) {
        if (row == null) {
            return;
        }
        if (this.rows == null) {
            this.rows = new ArrayList<>();
        }
        this.rows.add(row);
    }

    @Override
    public String toString() {
        return "QuestionTable{" + "rows=" + rows + '}';
    }

}
