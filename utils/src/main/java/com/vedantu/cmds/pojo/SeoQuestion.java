package com.vedantu.cmds.pojo;

import com.vedantu.cmds.enums.Difficulty;

import java.util.List;

public class SeoQuestion {

	private String id;
	private List<SolutionFormat> solutions;
    private RichTextFormat questionBody;
    private String topic;
    private Difficulty difficulty;
	
    public SeoQuestion() {
    	super();
    }
    
    public SeoQuestion(RichTextFormat questionBody, List<SolutionFormat> solutions) {
    	this.questionBody = questionBody;
    	this.solutions = solutions;
    }


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<SolutionFormat> getSolutions() {
		return solutions;
	}
	
    public void setSolutions(List<SolutionFormat> list) {
		this.solutions = list;
	}
	
    public RichTextFormat getQuestionBody() {
		return questionBody;
	}
	
    public void setQuestionBody(RichTextFormat questionBody) {
		this.questionBody = questionBody;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	@Override
	public String toString() {
		return "SeoQuestion [questionBody=" + questionBody + ", solutions=" + solutions + ", toString()="
				+ super.toString() + "]";
	}
}