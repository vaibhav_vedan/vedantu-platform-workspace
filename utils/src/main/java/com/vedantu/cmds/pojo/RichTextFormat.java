package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.lms.cmds.utils.LatexProcessor;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RichTextFormat implements ILatexProcessor {

    @Valid
    private Set<CMDSImageDetails> uuidImages;
    
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String newText;

    @Valid
    private List<VideoContent> videos;

    public RichTextFormat() {

        this.uuidImages = new HashSet<>();
        this.newText = new String();
        this.videos = new ArrayList<>();
    }

    public Set<CMDSImageDetails> getUuidImages() {
        return uuidImages;
    }

    public void setUuidImages(Set<CMDSImageDetails> uuidImages) {
        this.uuidImages = uuidImages;
    }

    public void addAllUuidImages(Set<CMDSImageDetails> uuidImages) {
        if (ArrayUtils.isNotEmpty(uuidImages)) {
            if (this.uuidImages == null) {
                this.uuidImages = new HashSet<>();
            }
            this.uuidImages.addAll(uuidImages);
        }
    }

    public String getNewText() {
        return newText;
    }

    public void setNewText(String newText) {
        this.newText = newText;
    }

    public List<VideoContent> getVideos() {
        return videos;
    }

    public void setVideos(List<VideoContent> videos) {
        this.videos = videos;
    }

    @Override
    public void addHook() {

        if (StringUtils.isNotEmpty(newText)) {
            newText = LatexProcessor.addHookToLatex(newText);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
