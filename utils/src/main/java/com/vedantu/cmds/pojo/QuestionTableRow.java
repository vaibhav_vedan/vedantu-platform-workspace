/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
public class QuestionTableRow {

    private List<QuestionTableCell> cells;

    public List<QuestionTableCell> getCells() {
        return cells;
    }

    public void setCells(List<QuestionTableCell> cells) {
        this.cells = cells;
    }

    public void addCell(QuestionTableCell cell) {
        if (cell == null) {
            return;
        }
        if (this.cells == null) {
            this.cells = new ArrayList<>();
        }
        this.cells.add(cell);
    }

    @Override
    public String toString() {
        return "QuestionTableRow{" + "cells=" + cells + '}';
    }

}
