package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.enums.StorageType;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class CMDSImageDetails {

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String fileName;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String publicUrl;

    private Long publicUrlExpiryTime;
    private Boolean inLine;
    private StorageType storageType = StorageType.AMAZON_S3;
    private Float aspectRatio;
    private UploadTarget uploadTarget;

    public CMDSImageDetails(String fileName, String publicUrl, Integer height, Integer width, Boolean inLine,
            StorageType storageType, Float aspectRatio, UploadTarget uploadTarget) {
        super();
        this.fileName = fileName;
        this.publicUrl = publicUrl;
        this.inLine = inLine;
        this.storageType = storageType;
        this.aspectRatio = aspectRatio;
        this.uploadTarget = uploadTarget;
    }

}
