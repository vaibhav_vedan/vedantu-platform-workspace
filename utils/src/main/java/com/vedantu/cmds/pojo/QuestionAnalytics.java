package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.pojo.AnswerChangeRecord;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.lms.cmds.pojo.QuestionAttemptState;
import com.vedantu.lms.cmds.pojo.QuestionChangeRecord;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by somil on 22/03/17.
 */
@Data
@NoArgsConstructor
public class QuestionAnalytics {
    String questionId;
    private String newQuestionId;
    Long duration = 0L;
    List<String> answerGiven;
    List<String> correctAnswer;
    Float marksGiven = 0f;
    Float averageAttemptDuration;
    private Long evaluatedTime;
    private String questionRemarks;
    EnumBasket.Correctness correctness = EnumBasket.Correctness.UNKNOWN;
    private QuestionAttemptState questionAttemptState = QuestionAttemptState.NOT_EVALUATED;
    private float maxMarks;
    private int questionIndex;
    private List<AnswerChangeRecord> answerChangeRecords = new ArrayList<>();
    private List<QuestionChangeRecord> questionChangeRecords = new ArrayList<>();
    private boolean noMarks = false;

    public QuestionAnalytics(CMDSTestQuestion cmdsTestQuestion) {
        this(cmdsTestQuestion.getQuestionId(), cmdsTestQuestion.getQuestionIndex(), QuestionAttemptState.NOT_EVALUATED,
                cmdsTestQuestion.getMarks().getPositive(), null, null, null);
    }

    private QuestionAnalytics(String questionId, int questionIndex, QuestionAttemptState questionAttemptState,
                              Float maxMarks, Float marksGiven, Long evaluatedTime, String questionRemarks) {
        super();
        this.questionId = questionId;
        this.questionIndex = questionIndex;
        this.questionAttemptState = questionAttemptState;
        this.maxMarks = maxMarks;
        this.marksGiven = marksGiven;
        this.evaluatedTime = evaluatedTime;
        this.questionRemarks = questionRemarks;
    }

    public void mutateMaxMarks(Map<String, Marks> questionIdMarks) {
        if(questionIdMarks.containsKey(this.questionId) && Objects.nonNull(questionIdMarks.get(this.questionId))){
            this.maxMarks=questionIdMarks.get(questionId).getPositive();
        }
    }
}
