/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.board.requests;

import com.vedantu.board.pojo.Board;
import com.vedantu.util.StringUtils;
import java.util.List;

/**
 *
 * @author somil
 */
public class UpdateBoardReq extends CreateBoardReq {

	private Long id;

        /*
	public UpdateBoardReq(HttpServletRequest req) {
		super(req);
	}
        */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

        /*
	@Override
	protected void populate(HttpServletRequest req) {
		super.populate(req);
		id = getLongFieldValue(Board.Constants.ID, req);
	}
        */

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (id == null && id == 0) {
			errors.add(Board.Constants.ID);
		}
		return errors;
	}

	@Override
	public Board toBoard() {
		Board board = super.toBoard();
		board.setId(id);
		return board;
	}

}
