/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.board.requests;

import com.vedantu.board.pojo.Board;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class CreateBoardReq extends AbstractFrontEndReq {

	private String name;
	private String slug;
	private Long parentId;
	private VisibilityState state;

	// tags
	private List<String> categories;
	private List<String> grades;
	private List<String> exams;

	/*
        public CreateBoardReq(HttpServletRequest req) {
		super(req);
	}

        
	@Override
	protected void populate(HttpServletRequest req) {
		name = getStringFieldValue(Constants.NAME, req);
		slug = getStringFieldValue(Constants.SLUG, req);
		parentId = getLongFieldValue(Constants.PARENT_ID, req);
		String state = getStringFieldValue(Constants.STATE, req);
		this.state = StringUtils.isNotEmpty(state) ? VisibilityState
				.valueOf(state) : VisibilityState.VISIBLE;
				
		categories = getListFieldValue(Constants.CATEGORIES, req);
		grades = getListFieldValue(Constants.GRADES, req);
		exams = getListFieldValue(Constants.EXAMS, req);

	}
        */

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getGrades() {
		return grades;
	}

	public void setGrades(List<String> grades) {
		this.grades = grades;
	}

	public List<String> getExams() {
		return exams;
	}

	public void setExams(List<String> exams) {
		this.exams = exams;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (getCallingUserId() == null || getCallingUserId() == 0) {
			errors.add(Board.Constants.USER_ID);
		}
		if (StringUtils.isEmpty(name)) {
			errors.add(Board.Constants.NAME);
		}
		if (StringUtils.isEmpty(slug)) {
			errors.add(Board.Constants.SLUG);
		}
		return errors;
	}

	public Board toBoard() {
		Board board = new Board(name.trim(), slug.trim(), parentId, state,
				categories, grades, exams, getCallingUserId());
		return board;
	}

	@Override
	public String toString() {
		return String
				.format("{name=%s, slug=%s, parentId=%s, state=%s, categories=%s, grades=%s, exams=%s}",
						name, slug, parentId, state, categories, grades, exams);
	}
}

