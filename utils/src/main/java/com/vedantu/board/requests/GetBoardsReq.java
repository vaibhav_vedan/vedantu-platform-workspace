/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.board.requests;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author somil
 */
public class GetBoardsReq extends AbstractFrontEndReq {

	private String category;
	private String grade;
	private String exam;
	private Long parentId;
	private VisibilityState state;
        private Boolean addChilds;

    public GetBoardsReq(String category, String grade, String exam, Long parentId, VisibilityState state, Boolean addChilds) {
        this.category = category;
        this.grade = grade;
        this.exam = exam;
        this.parentId = parentId;
        this.state = state;
        this.addChilds = addChilds;
    }
        
        
        
	
	public GetBoardsReq() {
		super();
	}

	//public GetBoardsReq(HttpServletRequest req) {
	//	super(req);
	//}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getExam() {
		return exam;
	}

	public void setExam(String exam) {
		this.exam = exam;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

        public Boolean getAddChilds() {
            return addChilds;
        }

        public void setAddChilds(Boolean addChilds) {
            this.addChilds = addChilds;
        }
        
	@Override
	public String toString() {
		return String.format(
				"{category=%s, grade=%s, exam=%s, parentId=%s, state=%s, addChilds=%s}",
				category, grade, exam, parentId, state, addChilds);
	}

}

