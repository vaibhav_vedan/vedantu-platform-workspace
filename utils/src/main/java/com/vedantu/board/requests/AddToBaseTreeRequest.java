package com.vedantu.board.requests;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class AddToBaseTreeRequest extends AbstractFrontEndReq {

    private String name;
    private String parentId;
    private String parentName;
    private VisibilityState state;

    private boolean computeChildren = false;

    private Integer baseTreeNodeId;

    private String baseTreeNodeName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public VisibilityState getState() {
        return state;
    }

    public void setState(VisibilityState state) {
        this.state = state;
    }

    public String getBaseTreeNodeName() {
        return baseTreeNodeName;
    }

    public void setBaseTreeNodeName(String baseTreeNodeName) {
        this.baseTreeNodeName = baseTreeNodeName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public boolean isComputeChildren() {
        return computeChildren;
    }

    public void setComputeChildren(boolean computeChildren) {
        this.computeChildren = computeChildren;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
//        if (getCallingUserId() == null || getCallingUserId() == 0) {
//            errors.add("USER_ID");
//        }
        if (StringUtils.isEmpty(name)) {
            errors.add("NAME");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "AddToBaseTreeRequest{" + "name=" + name + ", parentId=" + parentId + ", parentName=" + parentName + ", state=" + state + ", computeChildren=" + computeChildren + ", baseTreeNodeId=" + baseTreeNodeId + ", baseTreeNodeName=" + baseTreeNodeName + '}';
    }

}
