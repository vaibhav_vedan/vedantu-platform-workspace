package com.vedantu.board.pojo;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoLongIdEntityBean;

public class Board extends AbstractMongoLongIdEntityBean {
	public static final long DEFAULT_PARENT_ID = 0l;
	private String name;
	private String slug;
	private Long parentId;
	private VisibilityState state;
	private Set<String> categories;
	private Set<String> grades;
	private Set<String> exams;
	private Long userId;

	public Board() {
		super();
	}

	public Board(String name, String slug, Long parentId,
			VisibilityState state, Collection<String> categories,
			Collection<String> grades, Collection<String> exams, Long userId) {
		super();
		this.name = name;
		this.slug = slug;
		this.parentId = parentId == null ? DEFAULT_PARENT_ID : parentId;
		this.state = state == null ? VisibilityState.VISIBLE : state;
		this.categories = categories == null ? null : new HashSet<String>(
				categories);
		this.grades = grades == null ? null : new HashSet<String>(grades);
		this.exams = exams == null ? null : new HashSet<String>(exams);
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public Set<String> getCategories() {
		return categories;
	}

	public void setCategories(Set<String> categories) {
		this.categories = categories;
	}

	public Set<String> getGrades() {
		return grades;
	}

	public void setGrades(Set<String> grades) {
		this.grades = grades;
	}

	public Set<String> getExams() {
		return exams;
	}

	public void setExams(Set<String> exams) {
		this.exams = exams;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
        
	@Override
	public String toString() {
		return String
				.format("{id=%s, name=%s, slug=%s, parentId=%s, state=%s, categories=%s, grades=%s, exams=%s, userId=%s, creationTime=%s, lastUpdated=%s}",
						getId(), name, slug, parentId, state, categories,
						grades, exams, userId, getCreationTime(),
						getLastUpdated());
	}

	public static class Constants extends AbstractMongoLongIdEntityBean.Constants {
		public static final String SLUG = "slug";
		public static final String PARENT_ID = "parentId";
		public static final String CATEGORIES = "categories";
		public static final String GRADES = "grades";
		public static final String EXAMS = "exams";
		public static final String STATE = "state";
                public static final String NAME = "name";

	}
}
