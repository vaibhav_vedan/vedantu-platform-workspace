package com.vedantu.board.pojo;

import java.util.List;

public class BoardTreeInfo extends BoardInfoRes {

	private List<BoardTreeInfo> children;

	public BoardTreeInfo(Board board) {
		super(board);
	}

	public List<BoardTreeInfo> getChildren() {
		return children;
	}

	public void setChildren(List<BoardTreeInfo> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return String.format("{children=%s, toString()=%s}", children,
				super.toString());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ this.getId().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		BoardInfoRes other = (BoardInfoRes) obj;
		if (this.getId() == null) {
			if (other.getId() == null){
				return false;
			}else{
				if(this.getId() == other.getId()){
					return true;
				}
			}
			
		}
		return true;
	}
	
}
