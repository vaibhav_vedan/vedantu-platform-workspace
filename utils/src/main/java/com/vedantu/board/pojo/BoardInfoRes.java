package com.vedantu.board.pojo;

import java.util.Set;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoLongIdEntityBean;
import com.vedantu.util.enums.IValueComparable;

public class BoardInfoRes extends AbstractMongoLongIdEntityBean implements IValueComparable{

	private String name;
	private String slug;
	private Long parentId;
	private VisibilityState state;
	// tags
	private Set<String> categories;
	private Set<String> grades;
	private Set<String> exams;

        
	public BoardInfoRes(Board board) {
		super(board.getId(), board.getCreationTime(), board.getCreatedBy(), board.getLastUpdated(), board.getLastUpdatedBy());
		super.setId(board.getId());
		this.name = board.getName();
		this.slug = board.getSlug();
		this.parentId = board.getParentId();
		this.state = board.getState();
		this.categories = board.getCategories();
		this.grades = board.getGrades();
		this.exams = board.getExams();
	}

	public String getName() {
		return name;
	}

	public String getSlug() {
		return slug;
	}

	public Long getParentId() {
		return parentId;
	}

	public VisibilityState getState() {
		return state;
	}

	public Set<String> getCategories() {
		return categories;
	}

	public Set<String> getGrades() {
		return grades;
	}

	public Set<String> getExams() {
		return exams;
	}

	@Override
	public String _getComparableValue() {
		return slug;
	}
}
