/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.response;

import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class AddRemoveBatchIdsToSessionRes extends AbstractRes {

    private Set<String> addedBatchIds;
    private Set<String> removedBatchIds;
    private Integer currentEnrollCountForSession;
    private OTFSessionPojoUtils updatedSession;

    public Set<String> getAddedBatchIds() {
        return addedBatchIds;
    }

    public void setAddedBatchIds(Set<String> addedBatchIds) {
        this.addedBatchIds = addedBatchIds;
    }

    public Set<String> getRemovedBatchIds() {
        return removedBatchIds;
    }

    public void setRemovedBatchIds(Set<String> removedBatchIds) {
        this.removedBatchIds = removedBatchIds;
    }

    public Integer getCurrentEnrollCountForSession() {
        return currentEnrollCountForSession;
    }

    public void setCurrentEnrollCountForSession(Integer currentEnrollCountForSession) {
        this.currentEnrollCountForSession = currentEnrollCountForSession;
    }

    public OTFSessionPojoUtils getUpdatedSession() {
        return updatedSession;
    }

    public void setUpdatedSession(OTFSessionPojoUtils updatedSession) {
        this.updatedSession = updatedSession;
    }

}
