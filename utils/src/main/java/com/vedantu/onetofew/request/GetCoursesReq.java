package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetCoursesReq extends AbstractFrontEndListReq{
	private List<String> subjects;
	private List<String> grades;
	private List<String> targets;
	private String title;
	private List<String> teacherIds;
	private VisibilityState visibilityState;
	private Boolean featured;
	private String parentCourseId;
	private Boolean sendNoTeachers;
	private List<String> courseIds;
	private Integer startPriceFrom;
	private Integer startPriceTill;

	public GetCoursesReq() {
		super();
	}
        
	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getGrades() {
		return grades;
	}

	public void setGrades(List<String> grades) {
		this.grades = grades;
	}

	public List<String> getTargets() {
		return targets;
	}

	public void setTargets(List<String> targets) {
		this.targets = targets;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(List<String> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public VisibilityState getVisibilityState() {
		return visibilityState;
	}

	public void setVisibilityState(VisibilityState visibilityState) {
		this.visibilityState = visibilityState;
	}

	public Boolean getFeatured() {
		return featured;
	}

	public void setFeatured(Boolean featured) {
		this.featured = featured;
	}

	public String getParentCourseId() {
		return parentCourseId;
	}

	public void setParentCourseId(String parentCourseId) {
		this.parentCourseId = parentCourseId;
	}

	public Boolean getSendNoTeachers() {
		return sendNoTeachers;
	}

	public void setSendNoTeachers(Boolean sendNoTeachers) {
		this.sendNoTeachers = sendNoTeachers;
	}

	public List<String> getCourseIds() {
		return courseIds;
	}

	public void setCourseIds(List<String> courseIds) {
		this.courseIds = courseIds;
	}

	public Integer getStartPriceFrom() {
		return startPriceFrom;
	}

	public void setStartPriceFrom(Integer startPriceFrom) {
		this.startPriceFrom = startPriceFrom;
	}

	public Integer getStartPriceTill() {
		return startPriceTill;
	}

	public void setStartPriceTill(Integer startPriceTill) {
		this.startPriceTill = startPriceTill;
	}

}
