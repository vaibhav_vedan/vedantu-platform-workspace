package com.vedantu.onetofew.request;

import com.vedantu.User.Role;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import nl.basjes.shaded.org.springframework.util.StringUtils;

public class OTFJoinSessionReq extends AbstractFrontEndReq {

    private String sessionId;
    private String userId;
    private Role role;
    private Boolean admin;

    public OTFJoinSessionReq() {
        super();
    }

    public OTFJoinSessionReq(String sessionId, String userId, Role role, Boolean admin) {
        super();
        this.sessionId = sessionId;
        this.userId = userId;
        this.role = role;
        this.admin = admin;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }
        return errors;
    }

}
