/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.scheduling.pojo.PollQuestionAnswer;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class StudentPollDataReq extends AbstractReq{
    private String userId;
    private List<PollQuestionAnswer> pollAnswered;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<PollQuestionAnswer> getPollAnswered() {
        return pollAnswered;
    }

    public void setPollAnswered(List<PollQuestionAnswer> pollAnswered) {
        this.pollAnswered = pollAnswered;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(userId)) {
                errors.add("userId");
        }
        return errors;
    }
}
