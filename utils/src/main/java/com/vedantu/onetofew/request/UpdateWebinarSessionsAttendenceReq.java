package com.vedantu.onetofew.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UpdateWebinarSessionsAttendenceReq extends AbstractFrontEndReq {
	private String sessionId;
	private String bundleId;
	private String studentId;
	private EntityStatus entityStatus = EntityStatus.ACTIVE;
	private Boolean attendeeConfirmed;

	public UpdateWebinarSessionsAttendenceReq() {
		super();
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public EntityStatus getEntityStatus() {
		return entityStatus;
	}

	public void setEntityStatus(EntityStatus entityStatus) {
		this.entityStatus = entityStatus;
	}

	public Boolean getAttendeeConfirmed() {
		return attendeeConfirmed;
	}

	public void setAttendeeConfirmed(Boolean attendeeConfirmed) {
		this.attendeeConfirmed = attendeeConfirmed;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(bundleId) && StringUtils.isEmpty(sessionId)) {
			errors.add("bundleId or sessionId should be provided");
		}
		if (StringUtils.isEmpty(studentId)) {
			errors.add("studentId");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "UpdateWebinarSessionsAttendenceReq [sessionId=" + sessionId + ", bundleId=" + bundleId + ", studentId="
				+ studentId + ", entityStatus=" + entityStatus + ", attendeeConfirmed=" + attendeeConfirmed
				+ ", toString()=" + super.toString() + "]";
	}
}
