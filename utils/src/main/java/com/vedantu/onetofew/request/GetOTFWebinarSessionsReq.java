package com.vedantu.onetofew.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.onetofew.enums.SessionState;

public class GetOTFWebinarSessionsReq {
	private List<String> bundleIds;
	private String studentId;
	private String teacherId;
	private Long afterStartTime;
	private Long beforeStartTime;
	private Long afterEndTime;
	private Long beforeEndTime;
	private List<SessionState> sessionStates;
	private Integer start;
	private Integer limit;

	public GetOTFWebinarSessionsReq() {
		super();
	}

	public List<String> getBundleIds() {
		return bundleIds;
	}

	public void setBundleIds(List<String> bundleIds) {
		this.bundleIds = bundleIds;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Long getAfterStartTime() {
		return afterStartTime;
	}

	public void setAfterStartTime(Long afterStartTime) {
		this.afterStartTime = afterStartTime;
	}

	public Long getBeforeStartTime() {
		return beforeStartTime;
	}

	public void setBeforeStartTime(Long beforeStartTime) {
		this.beforeStartTime = beforeStartTime;
	}

	public Long getAfterEndTime() {
		return afterEndTime;
	}

	public void setAfterEndTime(Long afterEndTime) {
		this.afterEndTime = afterEndTime;
	}

	public Long getBeforeEndTime() {
		return beforeEndTime;
	}

	public void setBeforeEndTime(Long beforeEndTime) {
		this.beforeEndTime = beforeEndTime;
	}

	public List<SessionState> getSessionStates() {
		return sessionStates;
	}

	public void setSessionStates(List<SessionState> sessionStates) {
		this.sessionStates = sessionStates;
	}

	public void addSessionStates(SessionState sessionState) {
		if (this.sessionStates == null) {
			this.sessionStates = new ArrayList<>();
		}
		if (!this.sessionStates.contains(sessionState)) {
			this.sessionStates.add(sessionState);
		}
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "GetOTFWebinarSessionsReq [bundleIds=" + bundleIds + ", studentId=" + studentId + ", teacherId="
				+ teacherId + ", afterStartTime=" + afterStartTime + ", beforeStartTime=" + beforeStartTime
				+ ", afterEndTime=" + afterEndTime + ", beforeEndTime=" + beforeEndTime + ", sessionStates="
				+ sessionStates + ", start=" + start + ", limit=" + limit + ", toString()=" + super.toString() + "]";
	}
}
