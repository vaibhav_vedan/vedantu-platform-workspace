package com.vedantu.onetofew.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CancelOTFWebinarSessionsReq extends AbstractFrontEndReq {
	private String bundleId;
	private List<String> sessionIds;

	public CancelOTFWebinarSessionsReq() {
		super();
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public List<String> getSessionIds() {
		return sessionIds;
	}

	public void setSessionIds(List<String> sessionIds) {
		this.sessionIds = sessionIds;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(bundleId) && CollectionUtils.isEmpty(this.sessionIds)) {
			errors.add("bundleId or sessionIds should be provided");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "CancelOTFWebinarSessionsReq [bundleId=" + bundleId + ", sessionIds=" + sessionIds + ", toString()="
				+ super.toString() + "]";
	}
}
