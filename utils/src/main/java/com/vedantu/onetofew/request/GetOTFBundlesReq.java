package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetOTFBundlesReq extends AbstractFrontEndListReq{

	private List<String> otfBundleIds;
    private List<String> tags;
	private List<String> entityId;

	public List<String> getOtfBundleIds() {
		return otfBundleIds;
	}

	public void setOtfBundleIds(List<String> otfBundleIds) {
		this.otfBundleIds = otfBundleIds;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getEntityId() {
		return entityId;
	}

	public void setEntityId(List<String> entityId) {
		this.entityId = entityId;
	}
}
