package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddUpdateSessionContentReq extends AbstractFrontEndReq {

	private String sessionId;
	private List<ContentInfo> preSessionContents;
	private List<ContentInfo> postSessionContents;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<ContentInfo> getPreSessionContents() {
		return preSessionContents;
	}

	public void setPreSessionContents(List<ContentInfo> preSessionContents) {
		this.preSessionContents = preSessionContents;
	}

	public List<ContentInfo> getPostSessionContents() {
		return postSessionContents;
	}

	public void setPostSessionContents(List<ContentInfo> postSessionContents) {
		this.postSessionContents = postSessionContents;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (sessionId == null) {
            errors.add("sessionId");
        }
        return errors;
	}
}
