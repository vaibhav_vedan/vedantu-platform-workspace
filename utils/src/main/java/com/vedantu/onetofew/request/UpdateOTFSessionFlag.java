package com.vedantu.onetofew.request;

import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.onetofew.enums.OTFSessionFlag;
import org.springframework.util.CollectionUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;

public class UpdateOTFSessionFlag {
	private List<String> sessionIds = new ArrayList<>();
	private List<OTFSessionFlag> flags = new ArrayList<>();

	public UpdateOTFSessionFlag() {
		super();
	}

	public List<String> getSessionIds() {
		return sessionIds;
	}

	public void setSessionIds(List<String> sessionIds) {
		this.sessionIds = sessionIds;
	}

	public List<OTFSessionFlag> getFlags() {
		return flags;
	}

	public void setFlags(List<OTFSessionFlag> flags) {
		this.flags = flags;
	}

	public void addFlag(OTFSessionFlag flag) {
		if (this.flags == null) {
			this.flags = new ArrayList<>(); 
		}
		if (flag != null) {
			this.flags.add(flag);
		}
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (CollectionUtils.isEmpty(this.sessionIds)) {
			errors.add("sessionIds");
		}
		if (CollectionUtils.isEmpty(this.flags)) {
			errors.add("flags");
		}
		Set<OTFSessionFlag> unsupportedFlags = Optional.ofNullable(this.getFlags()).orElseGet(ArrayList::new)
				.stream()
				.filter(OTFSessionFlag::isUnsupported)
				.collect(Collectors.toSet());
		if (!unsupportedFlags.isEmpty()) {
			errors.add("deprecated flags " + unsupportedFlags);
		}

		return errors;
	}

	@Override
	public String toString() {
		return "UpdateOTFSessionFlag [sessionIds=" + sessionIds + ", flags=" + flags + ", toString()="
				+ super.toString() + "]";
	}
}
