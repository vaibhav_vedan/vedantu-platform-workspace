/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class MarkBatchInactiveReq extends AbstractFrontEndReq {

    private String batchId;

    public MarkBatchInactiveReq() {
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();
        if(StringUtils.isEmpty(batchId)){
            errors.add("batchId");
        }
        return errors;
    }
    
    @Override
    public String toString() {
        return "MarkBatchInactiveReq{" + "batchId=" + batchId + '}';
    }

}
