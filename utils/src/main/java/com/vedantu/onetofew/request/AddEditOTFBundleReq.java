package com.vedantu.onetofew.request;

import java.util.List;
import java.util.Map;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;

public class AddEditOTFBundleReq extends AbstractFrontEndReq {

	private List<OTMBundleEntityInfo> entities;
	private String title;
	private String target;
	private Integer grade;
	private List<Long> boardIds;
	private String description;
	private Map<String, String> keyValues;
	private Integer price;
	private Integer cutPrice;
	private List<String> tags;
	private boolean featured;
	// lowest is highest
	private Integer priority;
	private String id;
	private String referralCouponCode;
	private Integer registrationFee;
	private List<OTFSlot> allowedSlots;
	private List<BaseInstalmentInfo> instalmentDetails;
        private List<Object> teacherInfos;
        private List<Object> scheduleInfos;
        private Map<String, Object> bundleInfo;
        private List<Object> courseInclusionInfos;  
        private List<Object> testimonials;
        private Long startTime;
        private String groupName;
        private VisibilityState visibilityState;
        private List<CourseTerm> searchTerms;
        private Integer registrationCutPrice;
        private boolean earlyLearning;

	public List<OTMBundleEntityInfo> getEntities() {
		return entities;
	}

	public void setEntities(List<OTMBundleEntityInfo> entities) {
		this.entities = entities;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public List<Long> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(List<Long> boardIds) {
		this.boardIds = boardIds;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, String> getKeyValues() {
		return keyValues;
	}

	public void setKeyValues(Map<String, String> keyValues) {
		this.keyValues = keyValues;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getCutPrice() {
		return cutPrice;
	}

	public void setCutPrice(Integer cutPrice) {
		this.cutPrice = cutPrice;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public boolean isFeatured() {
		return featured;
	}

	public void setFeatured(boolean featured) {
		this.featured = featured;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReferralCouponCode() {
		return referralCouponCode;
	}

	public void setReferralCouponCode(String referralCouponCode) {
		this.referralCouponCode = referralCouponCode;
	}

	public Integer getRegistrationFee() {
		return registrationFee;
	}

	public void setRegistrationFee(Integer registrationFee) {
		this.registrationFee = registrationFee;
	}

	public List<OTFSlot> getAllowedSlots() {
		return allowedSlots;
	}

	public void setAllowedSlots(List<OTFSlot> allowedSlots) {
		this.allowedSlots = allowedSlots;
	}

        public List<BaseInstalmentInfo> getInstalmentDetails() {
            if (instalmentDetails == null) {
                return new ArrayList<>();
            }
            return instalmentDetails;
        }

	public void setInstalmentDetails(List<BaseInstalmentInfo> instalmentDetails) {
		this.instalmentDetails = instalmentDetails;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (ArrayUtils.isEmpty(entities)) {
			errors.add("entities");
		}
		if (title == null) {
			errors.add("title");
		}
		if (registrationFee != null && registrationFee < 0) {
			errors.add("registrationFee");
		}
		if ((price == null || price < 0)) {
			errors.add("invalid prices");
		}
                
                if (startTime == null || startTime < 0){
                    errors.add("Invalid startTime");
                }
                
                if (StringUtils.isEmpty(groupName)){
                    errors.add("Invalid groupName");
                }
                
                if (ArrayUtils.isEmpty(boardIds)){
                    errors.add("Invalid boardIds");
                }
                
                if(visibilityState == null){
                    errors.add("Invalid visibility state");
                }
                
                                        if(ArrayUtils.isEmpty(searchTerms)){
                                            errors.add("searchTerms");
                                        }
		int sum = 0;
		if (ArrayUtils.isNotEmpty(entities)) {
			for (OTMBundleEntityInfo pricingPojo : entities) {
				if (pricingPojo.getPrice() == null || pricingPojo.getPrice() < 0) {
					errors.add("invalid entry in pricing");
				} else {
					sum += pricingPojo.getPrice();
				}
			}
		}
		if (sum != price) {
			errors.add("price and entity price sum don't match");
		}
		return errors;
	}

        /**
         * @return the teacherInfos
         */
        public List<Object> getTeacherInfos() {
            return teacherInfos;
        }

        /**
         * @param teacherInfos the teacherInfos to set
         */
        public void setTeacherInfos(List<Object> teacherInfos) {
            this.teacherInfos = teacherInfos;
        }

        /**
         * @return the scheduleInfo
         */
        public List<Object> getScheduleInfos() {
            return scheduleInfos;
        }

        /**
         * @param scheduleInfos the scheduleInfo to set
         */
        public void setScheduleInfos(List<Object> scheduleInfos) {
            this.scheduleInfos = scheduleInfos;
        }

        /**
         * @return the bundleInfo
         */
        public Map<String, Object> getBundleInfo() {
            return bundleInfo;
        }

        /**
         * @param bundleInfo the bundleInfo to set
         */
        public void setBundleInfo(Map<String, Object> bundleInfo) {
            this.bundleInfo = bundleInfo;
        }

        /**
         * @return the courseInclusionInfos
         */
        public List<Object> getCourseInclusionInfos() {
            return courseInclusionInfos;
        }

        /**
         * @param courseInclusionInfos the courseInclusionInfos to set
         */
        public void setCourseInclusionInfos(List<Object> courseInclusionInfos) {
            this.courseInclusionInfos = courseInclusionInfos;
        }

    /**
     * @return the testimonials
     */
    public List<Object> getTestimonials() {
        return testimonials;
    }

    /**
     * @param testimonials the testimonials to set
     */
    public void setTestimonials(List<Object> testimonials) {
        this.testimonials = testimonials;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the visibilityState
     */
    public VisibilityState getVisibilityState() {
        return visibilityState;
    }

    /**
     * @param visibilityState the visibilityState to set
     */
    public void setVisibilityState(VisibilityState visibilityState) {
        this.visibilityState = visibilityState;
    }
      
                    public List<CourseTerm> getSearchTerms() {
                        return searchTerms;
                    }

                    public void setSearchTerms(List<CourseTerm> searchTerms) {
                        this.searchTerms = searchTerms;
                    }                    

    /**
     * @return the registrationCutPrice
     */
    public Integer getRegistrationCutPrice() {
        return registrationCutPrice;
    }

    /**
     * @param registrationCutPrice the registrationCutPrice to set
     */
    public void setRegistrationCutPrice(Integer registrationCutPrice) {
        this.registrationCutPrice = registrationCutPrice;
    }

	public boolean isEarlyLearning() {
		return earlyLearning;
	}

	public void setEarlyLearning(boolean earlyLearning) {
		this.earlyLearning = earlyLearning;
	}
}
