package com.vedantu.onetofew.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetUserMessageReq extends AbstractFrontEndListReq {
	private String userId;
	private String emailId;
	private String mobileNumber;
	private Long createdAfter;

	public GetUserMessageReq() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Long getCreatedAfter() {
		return createdAfter;
	}

	public void setCreatedAfter(Long createdAfter) {
		this.createdAfter = createdAfter;
	}
}
