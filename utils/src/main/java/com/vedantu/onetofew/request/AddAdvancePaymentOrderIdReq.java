/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.elasticsearch.common.lang3.StringUtils;

/**
 *
 * @author ajith
 */
public class AddAdvancePaymentOrderIdReq extends AbstractFrontEndReq {

    private String orderId;
    private String entityId;
    private Long userId;
    private EntityType entityType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(orderId)) {
            errors.add("orderId");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }
        return errors;
    }

}
