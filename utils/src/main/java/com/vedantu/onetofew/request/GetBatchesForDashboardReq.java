package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetBatchesForDashboardReq extends AbstractFrontEndListReq{
	
	private List<String> fields;
	private EntityStatus batchStatus;
	private List<String> batchIds;

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public EntityStatus getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(EntityStatus batchStatus) {
		this.batchStatus = batchStatus;
	}

	public List<String> getBatchIds() {
		return batchIds;
	}

	public void setBatchIds(List<String> batchIds) {
		this.batchIds = batchIds;
	}

}
