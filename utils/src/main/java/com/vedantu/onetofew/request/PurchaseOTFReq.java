/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class PurchaseOTFReq extends AbstractFrontEndReq{
    
    private String entityId;
    private Long userId;
    private EntityType entityType;
    private String redirectUrl;
    private String vedantuDiscountCouponId;
    private PaymentType paymentType;
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;

    // Added to take care from which bundle it is coming, and 
    // what is the enrollment type involved in the same
    private EnrollmentPurchaseContext purchaseContextType;
    private String purchaseContextId; // Bundle id from where it is coming
    private EnrollmentType purchaseContextEnrollmentType;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }
    
    public EnrollmentPurchaseContext getPurchaseContextType() {
		return purchaseContextType;
	}

	public void setPurchaseContextType(EnrollmentPurchaseContext purchaseContextType) {
		this.purchaseContextType = purchaseContextType;
	}

	public String getPurchaseContextId() {
		return purchaseContextId;
	}

	public void setPurchaseContextId(String purchaseContextId) {
		this.purchaseContextId = purchaseContextId;
	}

	public EnrollmentType getPurchaseContextEnrollmentType() {
		return purchaseContextEnrollmentType;
	}

	public void setPurchaseContextEnrollmentType(EnrollmentType purchaseContextEnrollmentType) {
		this.purchaseContextEnrollmentType = purchaseContextEnrollmentType;
	}

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }
    
}
