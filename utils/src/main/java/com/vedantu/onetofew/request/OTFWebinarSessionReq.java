package com.vedantu.onetofew.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.pojo.OTFWebinarSession;
import com.vedantu.subscription.request.AddEditBundleSessionReq;
import com.vedantu.util.CollectionUtils;

public class OTFWebinarSessionReq {
	private String bundleId;
	private List<OTFWebinarSession> sessions;
	private Set<String> enrolledStudents;

	public OTFWebinarSessionReq() {
		super();
	}

	public OTFWebinarSessionReq(AddEditBundleSessionReq req) {
		super();
		this.bundleId = req.getBundleId();
		OTFWebinarSession session = req.getOtfWebinarSession();
		session.setReferenceId(req.getWebinarBundleContentInfo().getReferenceId());
		this.addSession(session);
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public List<OTFWebinarSession> getSessions() {
		return sessions;
	}

	public void setSessions(List<OTFWebinarSession> sessions) {
		this.sessions = sessions;
	}

	public Set<String> getEnrolledStudents() {
		return enrolledStudents;
	}

	public void setEnrolledStudents(Set<String> enrolledStudents) {
		this.enrolledStudents = enrolledStudents;
	}

	public void addSession(OTFWebinarSession session) {
		if (this.sessions == null) {
			this.sessions = new ArrayList<>();
		}

		this.sessions.add(session);
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(bundleId)) {
			errors.add("bundleId");
		}
		if (CollectionUtils.isEmpty(sessions)) {
			errors.add("sessions");
		} else {
			for (OTFWebinarSession session : sessions) {
				if (session.getStartTime() <= 0l || session.getStartTime() < System.currentTimeMillis()) {
					errors.add("session start time invalid:" + session.getStartTime());
				}
				if (session.getEndTime() <= 0l) {
					errors.add("session end time invalid:" + session.getStartTime());
				}
				if (session.getSessionToolType() == null) {
					errors.add("invalid sessionToolType:" + session.getStartTime());
				}
			}
		}

		return errors;
	}

	@Override
	public String toString() {
		return "OTFWebinarSessionReq [bundleId=" + bundleId + ", sessions=" + sessions + ", toString()="
				+ super.toString() + "]";
	}
}
