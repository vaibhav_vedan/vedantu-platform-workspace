package com.vedantu.onetofew.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetOTFDashboardReq extends AbstractFrontEndListReq{

	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
