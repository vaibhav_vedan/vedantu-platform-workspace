package com.vedantu.onetofew.request;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetDemoSessionReq extends AbstractFrontEndListReq {
	private String courseId;
	private String batchId;
	private String teacherId;
	private String sessionId;
	private EntityStatus status;
	private String studentId;

	public GetDemoSessionReq() {
		super();
	}

        public GetDemoSessionReq(String courseId, String batchId, String teacherId, 
                String sessionId, EntityStatus status, String studentId) {
            this.courseId = courseId;
            this.batchId = batchId;
            this.teacherId = teacherId;
            this.sessionId = sessionId;
            this.status = status;
            this.studentId = studentId;
        }

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
}
