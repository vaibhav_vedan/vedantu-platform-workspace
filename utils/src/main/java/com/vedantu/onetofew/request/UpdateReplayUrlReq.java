package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UpdateReplayUrlReq extends AbstractFrontEndReq{

	private String sessionId;
	private String meetingId;
	private List<String> downloadUrl;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}

	public List<String> getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(List<String> downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(sessionId) && StringUtils.isEmpty(meetingId)) {
            errors.add("sessionId/meetingId");
        }
        if (ArrayUtils.isEmpty(downloadUrl)) {
            errors.add("downloadUrl");
        }
        return errors;
	}
}
