package com.vedantu.onetofew.request;

import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.List;

public class GetOTFSessionsBatchReq extends AbstractFrontEndListReq {

    private Long teacherId;
    private Long studentId;
    private Long boardId;
    private String batchId;
    private Long fromTime;
    private Long tillTime;
    private List<SessionState> states;
    private List<String> meetingIds;
    private Boolean sessionURLExists;
    private SortOrder sortOrder;

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public List<SessionState> getStates() {
        return states;
    }

    public void setStates(List<SessionState> states) {
        this.states = states;
    }

    public List<String> getMeetingIds() {
        return meetingIds;
    }

    public void setMeetingIds(List<String> meetingIds) {
        this.meetingIds = meetingIds;
    }

    public Boolean getSessionURLExists() {
        return sessionURLExists;
    }

    public void setSessionURLExists(Boolean sessionURLExists) {
        this.sessionURLExists = sessionURLExists;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

}
