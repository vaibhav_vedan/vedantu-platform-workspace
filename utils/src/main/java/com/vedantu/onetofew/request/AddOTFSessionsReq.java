package com.vedantu.onetofew.request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.scheduling.CommonCalendarUtils;

import java.util.Set;

public class AddOTFSessionsReq extends AbstractFrontEndReq {

    private Set<String> batchIds;
    @Deprecated private OTFSessionToolType sessionToolType;
    private List<OTFSessionPojoUtils> sessionPojo;

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public List<OTFSessionPojoUtils> getSessionPojo() {
        return sessionPojo;
    }

    public void setSessionPojo(List<OTFSessionPojoUtils> sessionPojo) {
        this.sessionPojo = sessionPojo;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
//        if (sessionToolType == null) {
//            errors.add("sessionToolType");
//        }
//        if (sessionToolType != null && sessionToolType.isUnsupported()) {
//            errors.add("deprecated sessionToolType: " + sessionToolType);
//        }
        if (ArrayUtils.isEmpty(sessionPojo)) {
            errors.add("sessionPojo");
        } else {
            sessionPojo.stream()
                    .filter(e -> e.getSessionToolType() == null)
                    .findAny()
                    .ifPresent(
                            e -> errors.add("session tool type null")
                    );
        }

        // construct a list of session slots for validation from the request object
        List<SessionSlot> slots = new ArrayList<>();
        sessionPojo.forEach(s -> {
            if (s.getEndTime() - s.getStartTime() > 5 * CommonCalendarUtils.MILLIS_PER_HOUR) {
                errors.add("Duration of each session should not exceed 5 hours");
            }
            SessionSlot slot = new SessionSlot(s.getStartTime(), s.getEndTime());
            slots.add(slot);
        });

        // validation check for session slots overlap
        if (ArrayUtils.isNotEmpty(slots)) {
            Collections.sort(slots, (s1, s2) -> s1.getStartTime().compareTo(s2.getStartTime()));
            int size = slots.size();
            Long currentMillis = System.currentTimeMillis();
            for (int i = 0; i < size; i++) {
                SessionSlot slot = slots.get(i);
                if (slot.getStartTime() == null || slot.getStartTime() <= 0l) {
                    errors.add("slot.startTime:" + slot.toString());
                    break;
                }

                if (slot.getEndTime() == null || slot.getEndTime() <= 0l) {
                    errors.add("slot.endTime:" + slot.toString());
                    break;
                }

                if (i < size - 1) {
                    SessionSlot nextSlot = slots.get(i + 1);
                    if (slot.getEndTime() > nextSlot.getStartTime()) {
                        errors.add("overlapping slots ");
                        break;
                    }
                }
                if (slot.getStartTime() < currentMillis) {
                    errors.add("slot in the past " + slot);
                    break;
                }
                if (slot.getStartTime() > slot.getEndTime()) {
                    errors.add("slot starttime > endTime" + slot);
                    break;
                }
            }
        }

        return errors;
    }

}
