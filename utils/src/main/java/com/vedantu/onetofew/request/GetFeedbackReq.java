package com.vedantu.onetofew.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetFeedbackReq extends AbstractFrontEndListReq {
	private String receiverid;
	private String senderId;
	private String sessionId;

	public GetFeedbackReq() {
		super();
	}
        
	public String getReceiverid() {
		return receiverid;
	}

	public void setReceiverid(String receiverid) {
		this.receiverid = receiverid;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
