/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CreateTrialEnrollmentReq extends AbstractFrontEndReq {

    private Long userId;
    private String courseId;
    private String batchId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (courseId == null) {
            errors.add("courseId");
        }
        if (batchId == null) {
            errors.add("batchId");
        }

        return errors;
    }

}
