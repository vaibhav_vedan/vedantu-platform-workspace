/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class MarkRegistrationStatusReq extends AbstractFrontEndReq {

    private String regId;
    private EntityType entityType;
    private String entityId;
    private String newstatus;
    private Long userId;
    private List<String> deliverableEntityIds;
    private List<String> oldStatuses;

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(String newstatus) {
        this.newstatus = newstatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<String> getDeliverableEntityIds() {
        return deliverableEntityIds;
    }

    public void setDeliverableEntityIds(List<String> deliverableEntityIds) {
        this.deliverableEntityIds = deliverableEntityIds;
    }

    public List<String> getOldStatuses() {
        return oldStatuses;
    }

    public void setOldStatuses(List<String> oldStatuses) {
        this.oldStatuses = oldStatuses;
    }

    
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (regId == null && entityId == null) {
            errors.add("regId && entityId is null");
        }
        if (userId == null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(newstatus)) {
            errors.add("newstatus");
        }
        if(ArrayUtils.isEmpty(oldStatuses)){
            errors.add("oldStatuses");
        }
        return errors;
    }

}
