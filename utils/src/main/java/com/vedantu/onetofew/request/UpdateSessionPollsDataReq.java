/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.scheduling.pojo.Pollsdata;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class UpdateSessionPollsDataReq extends AbstractReq{
    private String sessionId;
    private Pollsdata pollsdata;
    private List<StudentPollDataReq> studentPollDatas;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Pollsdata getPollsdata() {
        return pollsdata;
    }

    public void setPollsdata(Pollsdata pollsdata) {
        this.pollsdata = pollsdata;
    }

    public List<StudentPollDataReq> getStudentPollDatas() {
        return studentPollDatas;
    }

    public void setStudentPollDatas(List<StudentPollDataReq> studentPollDatas) {
        this.studentPollDatas = studentPollDatas;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(sessionId)) {
                errors.add("sessionId");
        }
        return errors;
    }
            
}
