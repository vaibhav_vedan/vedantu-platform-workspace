/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class AddRemoveBatchIdsToSessionReq extends AbstractFrontEndReq {

    private Set<String> batchIds;
    private String sessionId;
    private String secret;
    private com.vedantu.onetofew.enums.EntityType type;
    private Set<String> toBeDeletedBatchIds;

    public Set<String> getToBeDeletedBatchIds() {
        return toBeDeletedBatchIds;
    }

    public void setToBeDeletedBatchIds(Set<String> toBeDeletedBatchIds) {
        this.toBeDeletedBatchIds = toBeDeletedBatchIds;
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(batchIds)) {
            errors.add("batchIds");
        }
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }
        return errors;
    }

}
