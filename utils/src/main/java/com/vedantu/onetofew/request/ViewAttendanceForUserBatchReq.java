package com.vedantu.onetofew.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class ViewAttendanceForUserBatchReq extends AbstractFrontEndListReq{

	private String userId;
	private String batchId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
}
