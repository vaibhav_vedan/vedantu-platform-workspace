package com.vedantu.onetofew.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetOTFSessionsForRecordingDeletionReq extends AbstractFrontEndListReq {
	private Long startTime;
	private Long endTime;

	public GetOTFSessionsForRecordingDeletionReq() {
		super();
	}

	public GetOTFSessionsForRecordingDeletionReq(Long startTime, Long endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	@Override
	public List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		if (this.startTime == null || this.startTime <= 0l) {
			errors.add("startTime");
		}
		if (this.endTime == null || this.endTime <= 0l) {
			errors.add("endTime");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "GetOTFSessionsForRecordingDeletionReq [startTime=" + startTime + ", endTime=" + endTime
				+ ", toString()=" + super.toString() + "]";
	}
}
