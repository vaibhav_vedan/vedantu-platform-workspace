package com.vedantu.onetofew.request;

import java.util.Set;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetBatchesReq extends AbstractFrontEndListReq {

	private String courseId;
	private Set<String> teacherIds;
	private Set<String> studentIds;
	private Integer purchasePrice;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	private VisibilityState visibilityState;
	private String courseTitle;
	private Boolean showInactive;
	private Boolean returnEnrollments;
	private String orgId;

	public GetBatchesReq() {
		super();
	}
        
	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public Set<String> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(Set<String> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public Set<String> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(Set<String> studentIds) {
		this.studentIds = studentIds;
	}

	public Integer getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Integer purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public VisibilityState getVisibilityState() {
		return visibilityState;
	}

	public void setVisibilityState(VisibilityState visibilityState) {
		this.visibilityState = visibilityState;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public Boolean getShowInactive() {
		return showInactive;
	}

	public void setShowInactive(Boolean showInactive) {
		this.showInactive = showInactive;
	}

	public Boolean getReturnEnrollments() {
		return returnEnrollments;
	}

	public void setReturnEnrollments(Boolean returnEnrollments) {
		this.returnEnrollments = returnEnrollments;
	}
}
