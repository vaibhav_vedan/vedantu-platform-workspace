package com.vedantu.onetofew.request;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class PurchaseOTFBundleReq extends AbstractFrontEndReq{
	
    private String otfBundleId;
    private Long userId;
    private String redirectUrl;
    private String vedantuDiscountCouponId;
    private List<PurchasingEntity> purchasingEntities;
    private PaymentType paymentType;
    private List<OTFSlot> preferredSlots;
    
    // Added to take care from which bundle it is coming, and 
    // what is the enrollment type involved in the same
    private EnrollmentPurchaseContext purchaseContextType;
    private String purchaseContextId; // Bundle id from where it is coming
    private EnrollmentType purchaseContextEnrollmentType;
	public String getOtfBundleId() {
		return otfBundleId;
	}

	public void setOtfBundleId(String otfBundleId) {
		this.otfBundleId = otfBundleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getVedantuDiscountCouponId() {
		return vedantuDiscountCouponId;
	}

	public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
		this.vedantuDiscountCouponId = vedantuDiscountCouponId;
	}

	public List<PurchasingEntity> getPurchasingEntities() {
		return purchasingEntities;
	}

	public void setPurchasingEntities(List<PurchasingEntity> purchasingEntities) {
		this.purchasingEntities = purchasingEntities;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}
	
	public List<OTFSlot> getPreferredSlots() {
		return preferredSlots;
	}

	public void setPreferredSlots(List<OTFSlot> preferredSlots) {
		this.preferredSlots = preferredSlots;
	}

	
	public EnrollmentPurchaseContext getPurchaseContextType() {
		return purchaseContextType;
	}

	public void setPurchaseContextType(EnrollmentPurchaseContext purchaseContextType) {
		this.purchaseContextType = purchaseContextType;
	}

	public String getPurchaseContextId() {
		return purchaseContextId;
	}

	public void setPurchaseContextId(String purchaseContextId) {
		this.purchaseContextId = purchaseContextId;
	}

	public EnrollmentType getPurchaseContextEnrollmentType() {
		return purchaseContextEnrollmentType;
	}

	public void setPurchaseContextEnrollmentType(EnrollmentType purchaseContextEnrollmentType) {
		this.purchaseContextEnrollmentType = purchaseContextEnrollmentType;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(otfBundleId)) {
            errors.add("otfBundleId");
        }
        if (userId == null) {
            errors.add("userId");
        }

        return errors;
    }
}
