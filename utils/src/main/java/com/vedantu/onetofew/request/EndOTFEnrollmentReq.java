package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class EndOTFEnrollmentReq extends AbstractFrontEndReq{
	
	private String id;
    private String reason;
    private Boolean getRefundInfoOnly = false;
    private Integer promotionalAmount = 0;
    private Integer nonPromotionalAmount = 0;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Boolean getGetRefundInfoOnly() {
		return getRefundInfoOnly;
	}
	public void setGetRefundInfoOnly(Boolean getRefundInfoOnly) {
		this.getRefundInfoOnly = getRefundInfoOnly;
	}
	public Integer getPromotionalAmount() {
		return promotionalAmount;
	}
	public void setPromotionalAmount(Integer promotionalAmount) {
		this.promotionalAmount = promotionalAmount;
	}
	public Integer getNonPromotionalAmount() {
		return nonPromotionalAmount;
	}
	public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
		this.nonPromotionalAmount = nonPromotionalAmount;
	}
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(id)) {
            errors.add("enrollmentId");
        }
 
        return errors;
    }

}
