package com.vedantu.onetofew.request;

import com.vedantu.User.Role;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;

public class OTFEnrollmentReq extends AbstractMongoEntityBean {

    private String email;
    private String userId;
    private String batchId;
    private String courseId;
    private String sessionId;
    private Role role;
    private String status;
    private String type;
    private EnrollmentState state;
    private Long endTime;
    private String endReason;
    private Long endedBy;
    private String id;
    private com.vedantu.session.pojo.EntityType entityType;
    private String entityId;
    private String entityTitle;
    private EnrollmentPurchaseContext purchaseContextType;
    private String purchaseContextId;

    public OTFEnrollmentReq() {
        super();
    }

    public OTFEnrollmentReq(String userId, String batchId,
            String courseId, String sessionId, Role role, String status,
            String type, EnrollmentState state) {
        this.userId = userId;
        this.batchId = batchId;
        this.courseId = courseId;
        this.sessionId = sessionId;
        this.role = role;
        this.status = status;
        this.type = type;
        this.state = state;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EnrollmentState getState() {
        return state;
    }

    public void setState(EnrollmentState state) {
        this.state = state;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public com.vedantu.session.pojo.EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(com.vedantu.session.pojo.EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public EnrollmentPurchaseContext getPurchaseContextType() {
        return purchaseContextType;
    }

    public void setPurchaseContextType(EnrollmentPurchaseContext purchaseContextType) {
        this.purchaseContextType = purchaseContextType;
    }

    public String getPurchaseContextId() {
        return purchaseContextId;
    }

    public void setPurchaseContextId(String purchaseContextId) {
        this.purchaseContextId = purchaseContextId;
    }

}
