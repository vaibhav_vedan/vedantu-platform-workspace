package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.onetofew.pojo.BatchInfo;

public class GetBatchRes {

	private Integer count;
	private Integer totalCount;
	private List<BatchInfo> list;
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public List<BatchInfo> getList() {
		return list;
	}
	public void setList(List<BatchInfo> list) {
		this.list = list;
	}
}
