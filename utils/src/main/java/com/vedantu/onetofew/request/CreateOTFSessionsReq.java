package com.vedantu.onetofew.request;

import java.util.List;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.AgendaPojo;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.commons.lang.StringUtils;

public class CreateOTFSessionsReq extends AbstractFrontEndReq {
    
    private String batchId;
    private OTFSessionToolType sessionToolType;
    private List<AgendaPojo> agendaPojoList;
    private BatchBasicInfo batchInfo;
    
    public String getBatchId() {
        return batchId;
    }
    
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
    
    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }
    
    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }
    
    public List<AgendaPojo> getAgendaPojoList() {
        return agendaPojoList;
    }
    
    public void setAgendaPojoList(List<AgendaPojo> agendaPojoList) {
        this.agendaPojoList = agendaPojoList;
    }
    
    public BatchBasicInfo getBatchInfo() {
        return batchInfo;
    }
    
    public void setBatchInfo(BatchBasicInfo batchInfo) {
        this.batchInfo = batchInfo;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (sessionToolType == null) {
            errors.add("sessionToolType");
        }
        if (this.sessionToolType != null && this.sessionToolType.isUnsupported()) {
            errors.add("using deprecated session tool type");
        }
        if (StringUtils.isEmpty(batchId)) {
            errors.add("batchId");
        }
        if (ArrayUtils.isEmpty(agendaPojoList)) {
            errors.add("agendaPojoList");
        }
        return errors;
    }
}
