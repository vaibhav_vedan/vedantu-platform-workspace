package com.vedantu.onetofew.request;

import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.onetofew.enums.OTMSessionInProgressState;
import com.vedantu.onetofew.pojo.BatchChangeTime;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class GetOTFSessionsListReq extends AbstractFrontEndListReq {

    private Long teacherId;
    private Long studentId;
    private Long boardId;
    private String batchId;
    private Long fromTime;
    private Long tillTime;
    private SessionLabel earlyLearningCourseType;
    private List<SessionState> states;
    private List <OTMSessionType> otmSessionType;
    private List <OTFSessionToolType> sessionToolType;
    private List <OTMSessionInProgressState> progressState;
    private List<String> meetingIds;
    private Boolean sessionURLExists;
    private SortOrder sortOrder;
    private List<BatchChangeTime> batchChangeTime;
    private List<String> sessionIds;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (earlyLearningCourseType != null &&earlyLearningCourseType != SessionLabel.SUPER_CODER && earlyLearningCourseType != SessionLabel.SUPER_READER) {
            errors.add(earlyLearningCourseType + " earlyLearningCourseType not supported");
        }

        Set<OTMSessionType> unsupportedTypes = Optional.ofNullable(otmSessionType).orElseGet(ArrayList::new)
                .stream().filter(OTMSessionType::isUnsupported)
                .collect(Collectors.toSet());
        if (!unsupportedTypes.isEmpty()) {
            errors.add("deprecated otm session types " + unsupportedTypes);
        }
        Set<OTFSessionToolType> unsupportedToolTypes = Optional.ofNullable(sessionToolType).orElseGet(ArrayList::new)
                .stream().filter(OTFSessionToolType::isUnsupported)
                .collect(Collectors.toSet());
        if (!unsupportedToolTypes.isEmpty()) {
            errors.add("deprecated session tool types " + unsupportedToolTypes);
        }
        return errors;
    }
}
