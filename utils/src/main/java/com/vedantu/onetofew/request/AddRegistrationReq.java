/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.request;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AddRegistrationReq extends AbstractFrontEndReq {

    private Long userId;
    private EntityType entityType;
    private String entityId;
    private String orderId;
    private Integer bulkPriceToPay;//this is the (bulk)price when reg was done,will be used for bulk payment
    private List<OTMBundleEntityInfo> entities;
    private List<BaseInstalmentInfo> instalmentDetails;//will be used to create instalments

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getBulkPriceToPay() {
        return bulkPriceToPay;
    }

    public void setBulkPriceToPay(Integer bulkPriceToPay) {
        this.bulkPriceToPay = bulkPriceToPay;
    }

    public List<OTMBundleEntityInfo> getEntities() {
        return entities;
    }

    public void setEntities(List<OTMBundleEntityInfo> entities) {
        this.entities = entities;
    }

    public List<BaseInstalmentInfo> getInstalmentDetails() {
        return instalmentDetails;
    }

    public void setInstalmentDetails(List<BaseInstalmentInfo> instalmentDetails) {
        this.instalmentDetails = instalmentDetails;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }
        if (StringUtils.isEmpty(orderId)) {
            errors.add("orderId");
        }
        if (ArrayUtils.isEmpty(entities) && EntityType.OTM_BUNDLE_REGISTRATION.equals(entityType)) {
            errors.add("entities");
        }
        return errors;
    }

}
