package com.vedantu.onetofew.request;

import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.*;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
public class GetOTFSessionsReq extends AbstractFrontEndListReq {
	private Long teacherId;
	private Long studentId; // // Will be considered only if batch id is empty
	private Set<String> batchIds;
	private String courseId; // Will be considered only if batch id is empty
	private Long startTime; // Inclusive on end time
	private Long endTime; // Inclusive and cannot be greater than current time on end time
	private Long afterStartTime; // Inclusive on end time
	private Long beforeEndTime; // Inclusive and cannot be greater than current time on end time
	private OTFSessionContextType sessionContextType;
	private String sessionContextId;
	private List<SessionState> state;
	private Set<OTFSessionFlag> flags;
	private Set<SessionLabel> labels;
	private Set<EntityTag> entityTags;
	private Boolean teacherJoined;
	private String query;
	private String sessionId;

	public void validate() throws BadRequestException {
		List<String> errors = collectVerificationErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid parameters - " + Arrays.toString(errors.toArray()));
		}
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		if (this.startTime != null && this.endTime != null && this.endTime < this.startTime) {
			errors.add("endTime < startTime");
		}
		if (this.sessionId != null && !ObjectId.isValid(this.sessionId)) {
			errors.add("invalid sessionId");
		}

		Set<OTFSessionFlag> unsupportedFlags = Optional.ofNullable(this.getFlags()).orElseGet(HashSet::new)
				.stream()
				.filter(OTFSessionFlag::isUnsupported)
				.collect(Collectors.toSet());
		if (!unsupportedFlags.isEmpty()) {
			errors.add("deprecated flags " + unsupportedFlags);
		}

		return errors;
	}
}
