package com.vedantu.onetofew.enums;

public enum OTFSessionContextType {
	/**
	 * Used for GTT Webinar types not used anymore.
	 * Used here for backward compatibility
	 */
	@Deprecated
	WEBINAR
}
