package com.vedantu.onetofew.enums;

public enum Language {

    NEUTRAL,
    ASSAMESE,
    BENGALI,
    ENGLISH,
    GUJARAT,
    HINDI,
    KANNADA,
    MALAYALAM,
    MARATHI,
    TAMIL,
    TELUGU;
    public static final Language DEFAULT = ENGLISH;
}