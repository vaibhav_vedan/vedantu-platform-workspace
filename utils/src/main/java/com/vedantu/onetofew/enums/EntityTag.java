package com.vedantu.onetofew.enums;

/**
 * Mutually Inclusive Set of Tags. Used for adding extra features to In-Class. Not Used for Querying(We have to use {@link SessionLabel} for Querying)
 * @author mano
 */
public enum EntityTag {
    /**
     * Any one can join using link
     */
    WEBINAR,
    /**
     *
     */
    BIG_WHITE_BOARD,
    /**
     * Pre Recorded Video
     */
    SIMULATED_LIVE
}
