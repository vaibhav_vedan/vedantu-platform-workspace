package com.vedantu.onetofew.enums;

import com.google.common.collect.Sets;

public enum OTFSessionFlag {
    /**
     *
     */
    ABRUPT_ENDING,
    /**
     *
     */
    AGORA_CLOUD_RECORDING_STARTED,
    /**
     *
     */
    AGORA_RECORDING_STARTED,
    /**
     *
     */
    WHITEBOARD_DATA_MIGRATED,
    /**
     *
     */
    EC2RECORDING_STARTED,
    /**
     *
     */
    HLS_RENDITIONS_CREATED,
    /**
     *
     */
    OTF_FEEDBACK_HANDLED,
    /**
     *
     */
    NODE_TO_SCHEDULING,
    /**
     *
     */
    POST_SESSION_DATA_PROCESSED,
    /**
     *
     */
    SESSION_NOTES_PDF_CREATED,
    /**
     *
     */
    TEACHER_CREATED,
    /**
     *
     */
    EC2_RECORDING_STARTED,
    /**
     * use {@link com.vedantu.scheduling.pojo.session.OTMSessionType#SALES_DEMO}
     */
    @Deprecated
    SALES_DEMO,
    /**
     * use {@link SessionLabel}
     */
    @Deprecated
    EARLY_LEARNING,
    /**
     * use {@link SessionLabel#SUPER_CODER}
     */
    @Deprecated
    SUPER_CODER,
    /**
     * use {@link SessionLabel#SUPER_READER}
     */
    @Deprecated
    SUPER_READER,
    /**
     *
     */
    @Deprecated
    SALES_DEMO_NEW,

    ;

    public boolean isUnsupported() {
        return Sets.newHashSet(SALES_DEMO, SALES_DEMO_NEW, EARLY_LEARNING, SUPER_READER, SUPER_CODER).contains(this);
    }

}
