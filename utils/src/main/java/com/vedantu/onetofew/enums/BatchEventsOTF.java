package com.vedantu.onetofew.enums;

public enum BatchEventsOTF {
	
	BATCH_ACTIVE, BATCH_INACTIVE, USER_ENROLLED, USER_ACTIVE, USER_INACTIVE, USER_ENDED

}
