package com.vedantu.onetofew.enums;


/**
 * Used to identify type of session(Subtype of OTMSessionType. May be mutually inclusive). Used for querying.
 * @author mano
 */
public enum SessionLabel {
    /**
     * Webinar Session. Can be clubbed with any {@link EntityType}
     */
    WEBINAR,

    /**
     * Coding Session For Juniors. Can be clubbed only with {@link EntityType#ONE_TO_ONE}
     */
    SUPER_CODER,

    /**
     * Reading Session For Juniors. Can be clubbed only with {@link EntityType#ONE_TO_ONE}
     */
    SUPER_READER
}
