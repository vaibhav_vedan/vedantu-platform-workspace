package com.vedantu.onetofew.enums;

import com.vedantu.util.enums.SNSTopicIFace;

public enum SNSTopicOTF implements SNSTopicIFace {

    SESSION_EVENTS_OTF,
	BATCH_EVENTS_OTF,
	OTM_SNAPSHOT_SESSION_ENDED,
	CRON_CHIME_3HOURLY,
	CRON_CHIME_DAILY,
	ORDER_UPDATED,
    AUTO_ENROLL_BATCH,
	;
}
