package com.vedantu.onetofew.enums;


/**
 * This enums specify the basic type of the session. Used for wave team to setup resources accordingly.
 */
public enum EntityType {
	/**
	 * ~100 users can will be allowed
	 */
	STANDARD,
	/**
	 * ~teacher changes
	 */
	TWO_TEACHER_MODEL,
	/**
	 * ~50 users can will be allowed
	 */
	OTF_SESSION,
	/**
	 * ~10K users can will be allowed
	 */
	BIG_SESSION,

	/**
	 * both way communication session
	 */
	ONE_TO_ONE,

	/**
	 * internal purpose - like training, townhall, team meetings
	 */

	INTERNAL,

	/**
	 * wrong data, moved to {@link com.vedantu.scheduling.pojo.session.OTMSessionType#DEMO}
	 */
	@Deprecated DEMO;

	// Internal


	public boolean isUnsupported() {
		return this == DEMO;
	}

}
