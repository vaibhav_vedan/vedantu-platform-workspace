package com.vedantu.onetofew.enums;

public enum SessionState {
    SCHEDULED, CANCELLED, FORFEITED, DELETED
}
