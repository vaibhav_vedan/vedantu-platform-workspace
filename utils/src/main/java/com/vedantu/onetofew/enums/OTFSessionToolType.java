package com.vedantu.onetofew.enums;

import com.google.common.collect.Sets;

public enum OTFSessionToolType {
	/**
	 * Go To Meeting Platform
	 */
	@Deprecated
	GTM,
	/**
	 * Go To Training Platform
	 */
	@Deprecated
	GTT,
	/**
	 * Go To Training Pro Platform
	 */
	@Deprecated
	GTT_PRO,
	/**
	 * Go To Training Platform
	 */
	@Deprecated
	GTT100,
	/**
	 * Go To Webinar Platform
	 */
	@Deprecated
	GTW,
	/**
	 * Go To Training Platform
	 */
	@Deprecated
	GTT_WEBINAR,

	/**
	 * In house WAVE Platform
	 */
	VEDANTU_WAVE,

	/**
	 * In house WAVE Platform. This is not a actual platform, so deprecated and moved to {@link EntityTag#BIG_WHITE_BOARD}
	 */
	@Deprecated
	VEDANTU_WAVE_BIG_WHITEBOARD,
	/**
	 * In house WAVE Platform. This is not a actual platform, so deprecated. Have to use {@link EntityType#ONE_TO_ONE}
	 */
	@Deprecated
	VEDANTU_WAVE_OTO;



	public boolean isUnsupported() {
		return Sets.newHashSet(GTT, GTT100, GTT_PRO, GTT_WEBINAR, GTM, GTW, VEDANTU_WAVE_BIG_WHITEBOARD, VEDANTU_WAVE_OTO).contains(this);
	}
}
