package com.vedantu.onetofew.enums;

public enum SubscriptionPackageType {
    TRIAL, REGULAR;
    public static final SubscriptionPackageType DEFAULT = REGULAR;
}
