package com.vedantu.onetofew.pojo;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class ModifyStudentAccountManagerOfStudentsReq extends AbstractFrontEndReq {

    private String studentAccountManagerEmail;
    private List<Long> studentIds;

    public ModifyStudentAccountManagerOfStudentsReq() {
        super();
    }

    public String getStudentAccountManagerEmail() {
        return studentAccountManagerEmail;
    }

    public void setStudentAccountManagerEmail(String studentAccountManagerEmail) {
        this.studentAccountManagerEmail = studentAccountManagerEmail;
    }

    public List<Long> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<Long> studentIds) {
        this.studentIds = studentIds;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentAccountManagerEmail)){
            errors.add("Empty Email of Student Account Manager");
        }
        if(ArrayUtils.isEmpty(studentIds)){
            errors.add("No student Ids to assign");
        }
        return errors;
    }
}
