package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BundleEntityInfo extends AbstractMongoStringIdEntity {
    private Integer grade;
    private PackageType packageType;
    private String entityId;
    private EnrollmentType enrollmentType;
    private BatchDetails batchDetails;
    private List<String> subjects;
    private Set<String> targets;
    private String courseId;
    private  Boolean recordedVideo;
    private  Long startTime;
    private  Long endTime;
    private String courseTitle;
    @Getter(AccessLevel.NONE)
    private List<CourseTerm> searchTerms;
    @Getter(AccessLevel.NONE)
    private List<String> mainTags;
    private Integer courseCount;
    private Language language = Language.DEFAULT;
    private List<SubscriptionPackageType> subscriptionPackageTypes;
    private String bundleId;
    private boolean isSubscription = false ;

    public List<CourseTerm> getSearchTerms() {
        if(ArrayUtils.isEmpty(searchTerms)){
            searchTerms = new ArrayList<>();
        }
        return searchTerms;
    }

    public List<String> getMainTags() {
        if(ArrayUtils.isEmpty(mainTags)){
            mainTags = new ArrayList<>();
        }
        return mainTags;
    }

}
