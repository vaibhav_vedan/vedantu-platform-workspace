package com.vedantu.onetofew.pojo;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import java.util.List;


public class CourseInfo extends CourseBasicInfo {
	private List<ContentInfo> contents;

	// Stored in bits, each with 30 min duration.
	private List<BatchTiming> batchTimings;
	private List<CurriculumPojo> curriculum;
	private String targetAudience;

	private List<SessionBluePrintEntry> sessionBluePrint;
	private List<FAQPojo> faq;

	// grades, subjects, targets, course title will be added and removed by
	// default
	private String demoVideoUrl;
	private Long exitDate;
	private int startPrice;
	private List<PreRequisites> preRequisites;
	private String parentCourseId;
        
        private List<BaseInstalmentInfo> instalmentDetails;
        

	public CourseInfo() {
		super();
	}


	public List<ContentInfo> getContents() {
		return contents;
	}

	public void setContents(List<ContentInfo> contents) {
		this.contents = contents;
	}

	public List<BatchTiming> getBatchTimings() {
		return batchTimings;
	}

	public void setBatchTimings(List<BatchTiming> batchTimings) {
		this.batchTimings = batchTimings;
	}

	public List<CurriculumPojo> getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(List<CurriculumPojo> curriculum) {
		this.curriculum = curriculum;
	}

	public String getTargetAudience() {
		return targetAudience;
	}

	public void setTargetAudience(String targetAudience) {
		this.targetAudience = targetAudience;
	}

	public List<SessionBluePrintEntry> getSessionBluePrint() {
		return sessionBluePrint;
	}

	public void setSessionBluePrint(List<SessionBluePrintEntry> sessionBluePrint) {
		this.sessionBluePrint = sessionBluePrint;
	}

	public List<FAQPojo> getFaq() {
		return faq;
	}

	public void setFaq(List<FAQPojo> faq) {
		this.faq = faq;
	}

	public String getDemoVideoUrl() {
		return demoVideoUrl;
	}

	public void setDemoVideoUrl(String demoVideoUrl) {
		this.demoVideoUrl = demoVideoUrl;
	}

	public Long getExitDate() {
		return exitDate;
	}

	public void setExitDate(Long exitDate) {
		this.exitDate = exitDate;
	}

	public int getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(int startPrice) {
		this.startPrice = startPrice;
	}

	public List<PreRequisites> getPreRequisites() {
		return preRequisites;
	}

	public void setPreRequisites(List<PreRequisites> preRequisites) {
		this.preRequisites = preRequisites;
	}

	public String getParentCourseId() {
		return parentCourseId;
	}

	public void setParentCourseId(String parentCourseId) {
		this.parentCourseId = parentCourseId;
	}

    public List<BaseInstalmentInfo> getInstalmentDetails() {
        return instalmentDetails;
    }

    public void setInstalmentDetails(List<BaseInstalmentInfo> instalmentDetails) {
        this.instalmentDetails = instalmentDetails;
    }

    @Override
    public String toString() {
        return "CourseInfo{" + "contents=" + contents + ", batchTimings=" + batchTimings + ", curriculum=" + curriculum + ", targetAudience=" + targetAudience + ", sessionBluePrint=" + sessionBluePrint + ", faq=" + faq + ", demoVideoUrl=" + demoVideoUrl + ", exitDate=" + exitDate + ", startPrice=" + startPrice + ", preRequisites=" + preRequisites + ", parentCourseId=" + parentCourseId + ", instalmentDetails=" + instalmentDetails + '}';
    }
    
    
    
        

}
