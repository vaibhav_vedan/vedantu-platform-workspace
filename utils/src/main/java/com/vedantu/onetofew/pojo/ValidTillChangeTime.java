package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EnrollmentState;
import lombok.Data;

@Data
public class ValidTillChangeTime {

	private Long changeTime;
	private Long changedBy;
	private Long newValidTill;
	private Long previousValidTill;
}
