package com.vedantu.onetofew.pojo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVEnrollmentReq extends AbstractFrontEndReq{
    private List<CSVEnrollmentPojo> csvEnrollmentPojoList;

    public CSVEnrollmentReq(){
        super();
    }

    public CSVEnrollmentReq(List<CSVEnrollmentPojo> csvEnrollmentPojoList) {
        this.csvEnrollmentPojoList = csvEnrollmentPojoList;
    }

    public List<String> collectErrors() throws BadRequestException{
        List<String> errors = new ArrayList<>();
        if (csvEnrollmentPojoList.isEmpty()) {
            errors.add("CSVEntries");
        }else{
            for(CSVEnrollmentPojo csvEnrollmentPojo: csvEnrollmentPojoList){
                csvEnrollmentPojo.validate();
            }
        }

        return errors;
    }

    public void validate() throws BadRequestException {
        List<String> errors = collectErrors();
        if (!CollectionUtils.isEmpty(errors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Missing params:" + Arrays.toString(errors.toArray()));
        }
    }

    public List<CSVEnrollmentPojo> getCsvEnrollmentPojoList() {
        return csvEnrollmentPojoList;
    }

    public void setCsvEnrollmentPojoList(List<CSVEnrollmentPojo> csvEnrollmentPojoList) {
        this.csvEnrollmentPojoList = csvEnrollmentPojoList;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        return super.collectVerificationErrors(); //To change body of generated methods, choose Tools | Templates.
    }
    
}

