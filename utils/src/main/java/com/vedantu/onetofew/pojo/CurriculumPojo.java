package com.vedantu.onetofew.pojo;

import java.util.List;

public class CurriculumPojo {
	private String topic;
	private List<String> subTopics;

	public CurriculumPojo() {
		super();
	}

	public CurriculumPojo(String topic, List<String> subTopics) {
		super();
		this.topic = topic;
		this.subTopics = subTopics;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public List<String> getSubTopics() {
		return subTopics;
	}

	public void setSubTopics(List<String> subTopics) {
		this.subTopics = subTopics;
	}
}
