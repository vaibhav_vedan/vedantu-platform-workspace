package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.enums.bundle.BundleState;
import lombok.Data;

@Data
public class BundleStateChangeTime {

	private Long changeTime;
	private Long changedBy;
	private BundleState newStatus;

	public BundleStateChangeTime() {
	}

	public BundleStateChangeTime(Long changeTime, Long changedBy, BundleState newStatus) {
		this.changeTime = changeTime;
		this.changedBy = changedBy;
		this.newStatus = newStatus;
	}
}
