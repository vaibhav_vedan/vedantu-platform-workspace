package com.vedantu.onetofew.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.*;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Data
public class BatchBasicInfo extends AbstractMongoStringIdEntityBean {

    @Setter(AccessLevel.NONE)
    private String batchId;
    private VisibilityState visibilityState;
    private String planString;
    private List<UserBasicInfo> teachers;
    private int purchasePrice;
    private int cutPrice;
    private Long startTime;
    private Long endTime;
    private int maxEnrollment;
    private int minEnrollment;
    private boolean subscribed;
    private CourseBasicInfo courseInfo;
    private boolean isHalfScheduled;
    private OTFSessionToolType sessionToolType;
    private Integer registrationFee;
    private String curriculumId;
    private List<BoardTeacherPair> boardTeacherPairs;
    private Boolean curriculumMap;// if linked to course curriculum 
    private Boolean curriculumExists;
    private int enrollmentCount;
    private Long duration;
    private String groupName;
    private String groupSlug;
    private EntityStatus batchState;
    private String preferredSeat;
    private List<SessionPlanPojo> sessionPlan;
    private Set<String> teacherIds;
    private List<CourseTerm> searchTerms;
    private String whatsappJoinUrl;
    private boolean contentBatch = false;
    private boolean modularBatch = false;
    private String acadMentorId;
    private String courseId;
    private boolean recordedVideo;
    private Long lastPurchaseDate;
    private Boolean earlyLearning;
    private String orgId;
    private Language language = Language.DEFAULT;
    private List<SubscriptionPackageType> packageTypes;
    private List<EarlyLearningCourseType> earlyLearningCourses;
    private Boolean hasSections;

    public void setBatchId(String batchId) {
        this.batchId = batchId;
        this.setId(batchId);
    }

    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String _ID = "_id";
        public static final String TITLE = "title";
        public static final String START_TIME = "startTime";
        public static final String COURSE_ID ="courseId" ;
        public static final String GROUP_NAME ="groupName" ;
        public static final String GRADES = "grades";
        public static final String SEARCH_TERMS = "searchTerms";
    }

}
