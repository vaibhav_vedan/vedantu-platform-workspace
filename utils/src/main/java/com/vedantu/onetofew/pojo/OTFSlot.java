package com.vedantu.onetofew.pojo;

import java.util.List;

import com.vedantu.onetofew.enums.SlotDayType;

public class OTFSlot {

	private List<String> slotTimes;
	private SlotDayType dayType;
	private String startDay;

	public List<String> getSlotTimes() {
		return slotTimes;
	}

	public void setSlotTimes(List<String> slotTimes) {
		this.slotTimes = slotTimes;
	}

	public SlotDayType getDayType() {
		return dayType;
	}

	public void setDayType(SlotDayType dayType) {
		this.dayType = dayType;
	}

	public String getStartDay() {
		return startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

}
