/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import java.util.Set;

/**
 *
 * @author parashar
 */
public class GTTAttendeeSessionInfo extends GTTAttendeeDetailsInfo {
    
    private Set<String> batchIds;
    private Long startTime;
    private String title;

    /**
     * @return the batchIds
     */
    public Set<String> getBatchIds() {
        return batchIds;
    }

    /**
     * @param batchIds the batchIds to set
     */
    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
}
