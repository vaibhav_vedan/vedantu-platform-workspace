package com.vedantu.onetofew.pojo;

import java.util.Set;

public class SessionAttendanceIdsPojo {
	
	private Set<String> attended;
	private Set<String> absentees;
	public Set<String> getAttended() {
		return attended;
	}
	public void setAttended(Set<String> attended) {
		this.attended = attended;
	}
	public Set<String> getAbsentees() {
		return absentees;
	}
	public void setAbsentees(Set<String> absentees) {
		this.absentees = absentees;
	}

	@Override
	public String toString() {
		return "SessionAttendanceIdsPojo{" +
				"attended=" + attended +
				", absentees=" + absentees +
				'}';
	}
}
