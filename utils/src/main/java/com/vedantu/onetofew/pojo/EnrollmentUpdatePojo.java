/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class EnrollmentUpdatePojo extends AbstractFrontEndReq {

    private EnrollmentPojo previousEnrollment;
    private EnrollmentPojo newEnrollment;

    public EnrollmentPojo getPreviousEnrollment() {
        return previousEnrollment;
    }

    public void setPreviousEnrollment(EnrollmentPojo previousEnrollment) {
        this.previousEnrollment = previousEnrollment;
    }

    public EnrollmentPojo getNewEnrollment() {
        return newEnrollment;
    }

    public void setNewEnrollment(EnrollmentPojo newEnrollment) {
        this.newEnrollment = newEnrollment;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (newEnrollment == null) {
            errors.add("newEnrollment");
        }
        return errors;
    }

}
