package com.vedantu.onetofew.pojo;

public class StudentContentPojo {
	private String subject;
	private Integer shared=0;//total
	private Integer attempted=0;//attempted+evaluated
	private Integer evaluated=0;//evaluated
	private Long lastAttempted;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Integer getShared() {
		return shared;
	}
	public void setShared(Integer shared) {
		this.shared = shared;
	}
	public Integer getAttempted() {
		return attempted;
	}
	public void setAttempted(Integer attempted) {
		this.attempted = attempted;
	}
	public Integer getEvaluated() {
		return evaluated;
	}
	public void setEvaluated(Integer evaluated) {
		this.evaluated = evaluated;
	}
	public Long getLastAttempted() {
		return lastAttempted;
	}
	public void setLastAttempted(Long lastAttempted) {
		this.lastAttempted = lastAttempted;
	}

    @Override
    public String toString() {
        return "StudentContentPojo{" + "subject=" + subject + ", shared=" + shared + ", attempted=" + attempted + ", evaluated=" + evaluated + ", lastAttempted=" + lastAttempted + '}';
    }

}
