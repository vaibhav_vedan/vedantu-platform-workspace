package com.vedantu.onetofew.pojo;

public class ContentInfo {
	private String title;
	private String teacherId;
	private String description;
	private String contentType;
	private String url;
	private String id;

	public ContentInfo() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "OTFContentInfo [title=" + title + ", teacherId=" + teacherId + ", description=" + description
				+ ", contentType=" + contentType + ", url=" + url + ", toString()=" + super.toString() + "]";
	}
}
