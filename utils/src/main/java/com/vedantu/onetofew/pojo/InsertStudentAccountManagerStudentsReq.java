package com.vedantu.onetofew.pojo;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;
import java.util.Set;

public class InsertStudentAccountManagerStudentsReq extends AbstractFrontEndReq {

    private String studentAccountManagerEmail;
    private Set<String> studentEmails;

    public InsertStudentAccountManagerStudentsReq() {
        super();
    }

    public String getStudentAccountManagerEmail() {
        return studentAccountManagerEmail;
    }

    public void setStudentAccountManagerEmail(String studentAccountManagerEmail) {
        this.studentAccountManagerEmail = studentAccountManagerEmail;
    }

    public Set<String> getStudentEmails() {
        return studentEmails;
    }

    public void setStudentEmails(Set<String> studentEmails) {
        this.studentEmails = studentEmails;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(studentAccountManagerEmail)) {
            errors.add("Empty Email of student Account Manager ");
        }

        if (ArrayUtils.isEmpty(studentEmails)) {
            errors.add("No student emails to assign");
        }

        return errors;

    }
}

