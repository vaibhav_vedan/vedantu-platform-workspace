/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.util.fos.response.AbstractListRes;
import java.util.List;


/**
 *
 * @author somil
 */
public class GetCourseRes extends AbstractListRes<CourseInfo> {
	private int totalCount;

	public GetCourseRes() {
		super();
	}

	public GetCourseRes(List data, int totalCount) {
		super();
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
