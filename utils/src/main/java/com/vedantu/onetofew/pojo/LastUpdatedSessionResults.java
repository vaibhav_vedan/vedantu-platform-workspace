package com.vedantu.onetofew.pojo;

import java.util.HashSet;
import java.util.Set;

public class LastUpdatedSessionResults extends LastUpdatedUsers {
	private Set<String> batchIds = new HashSet<>();

	public LastUpdatedSessionResults() {
		super();
	}

	public Set<String> getBatchIds() {
		return batchIds;
	}

	public void setBatchIds(Set<String> batchIds) {
		this.batchIds = batchIds;
	}

	@Override
	public String toString() {
		return "LastUpdateSessionUserResults [batchIds=" + batchIds + ", toString()=" + super.toString() + "]";
	}
}
