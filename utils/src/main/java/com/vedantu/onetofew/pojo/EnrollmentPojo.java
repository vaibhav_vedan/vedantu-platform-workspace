/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author ajith
 */
public class EnrollmentPojo {

    private String id;
    private String enrollmentId;
    private UserBasicInfo user;
    private CourseBasicInfo course;
    private BatchBasicInfo batch;
    private Role role;
    private EntityStatus status;
    private Long creationTime;
    private Long lastUpdated;
    private String batchId;
    private String courseId;
    private String userId;
    private EnrollmentState state;
    private com.vedantu.onetofew.enums.EntityType type;
    private boolean createdNew = true;

    private Long endTime;
    private String endReason;
    private Long endedBy;
    private Long changeTime;
    private List<StatusChangeTime> statusChangeTime;
    private String entityId;
    private PaymentType paymentType;
    private String orderId;
    private com.vedantu.session.pojo.EntityType entityType;
    private String entityTitle;
    private List<BatchChangeTime> batchChangeTime;
    private List<SubjectFileCountPojo> subjectFileCountPojos;
    private Integer refundAmt=0;
    private Integer consumptionAmt=0;
    private Integer paidAmt=0;
    private EnrollmentPurchaseContext purchaseContextType;
    private String purchaseContextId;
    private String purchaseEnrollmentId;
    private EnrollmentType purchaseContextEnrollmentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public void setUser(UserBasicInfo user) {
        this.user = user;
    }

    public CourseBasicInfo getCourse() {
        return course;
    }

    public void setCourse(CourseBasicInfo course) {
        this.course = course;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public EntityStatus getStatus() {
        return status;
    }

    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EnrollmentState getState() {
        return state;
    }

    public void setState(EnrollmentState state) {
        this.state = state;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public BatchBasicInfo getBatch() {
        return batch;
    }

    public void setBatch(BatchBasicInfo batch) {
        this.batch = batch;
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public boolean isCreatedNew() {
        return createdNew;
    }

    public void setCreatedNew(boolean createdNew) {
        this.createdNew = createdNew;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public List<StatusChangeTime> getStatusChangeTime() {
        return statusChangeTime;
    }

    public void setStatusChangeTime(List<StatusChangeTime> statusChangeTime) {
        this.statusChangeTime = statusChangeTime;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public com.vedantu.session.pojo.EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(com.vedantu.session.pojo.EntityType entityType) {
		this.entityType = entityType;
	}

	public String getEntityTitle() {
		return entityTitle;
	}

	public void setEntityTitle(String entityTitle) {
		this.entityTitle = entityTitle;
	}

    public Integer getRefundAmt() {
        return refundAmt;
    }

    public void setRefundAmt(Integer refundAmt) {
        this.refundAmt = refundAmt;
    }

    public Integer getConsumptionAmt() {
        return consumptionAmt;
    }

    public void setConsumptionAmt(Integer consumptionAmt) {
        this.consumptionAmt = consumptionAmt;
    }

    public Integer getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(Integer paidAmt) {
        this.paidAmt = paidAmt;
    }

    public EnrollmentPurchaseContext getPurchaseContextType() {
        return purchaseContextType;
    }

    public void setPurchaseContextType(EnrollmentPurchaseContext purchaseContextType) {
        this.purchaseContextType = purchaseContextType;
    }

    public String getPurchaseEnrollmentId() {
        return purchaseEnrollmentId;
    }

    public void setPurchaseEnrollmentId(String purchaseEnrollmentId) {
        this.purchaseEnrollmentId = purchaseEnrollmentId;
    }

    public String getPurchaseContextId() {
        return purchaseContextId;
    }

    public void setPurchaseContextId(String purchaseContextId) {
        this.purchaseContextId = purchaseContextId;
    }

    @Override
    public String toString() {
        return "EnrollmentPojo{" + "id=" + id + ", enrollmentId=" + enrollmentId + ", user=" + user + ", course=" + course + ", batch=" + batch + ", role=" + role + ", status=" + status + ", creationTime=" + creationTime + ", lastUpdated=" + lastUpdated + ", batchId=" + batchId + ", courseId=" + courseId + ", userId=" + userId + ", state=" + state + ", type=" + type + ", createdNew=" + createdNew + ", endTime=" + endTime + ", endReason=" + endReason + ", endedBy=" + endedBy + ", changeTime=" + changeTime + ", statusChangeTime=" + statusChangeTime +  ", refundAmt=" + refundAmt + ", consumptionAmt=" + consumptionAmt + ", paidAmt=" + paidAmt +'}';
    }

    /**
     * @return the batchChangeTime
     */
    public List<BatchChangeTime> getBatchChangeTime() {
        return batchChangeTime;
    }

    /**
     * @param batchChangeTime the batchChangeTime to set
     */
    public void setBatchChangeTime(List<BatchChangeTime> batchChangeTime) {
        this.batchChangeTime = batchChangeTime;
    }


    /**
     * @return the subjectFileCountPojos
     */
    public List<SubjectFileCountPojo> getSubjectFileCountPojos() {
        return subjectFileCountPojos;
    }

    /**
     * @param subjectFileCountPojos the subjectFileCountPojos to set
     */
    public void setSubjectFileCountPojos(List<SubjectFileCountPojo> subjectFileCountPojos) {
        this.subjectFileCountPojos = subjectFileCountPojos;
    }


    public EnrollmentType getPurchaseContextEnrollmentType() {
        return purchaseContextEnrollmentType;
    }

    public void setPurchaseContextEnrollmentType(EnrollmentType purchaseContextEnrollmentType) {
        this.purchaseContextEnrollmentType = purchaseContextEnrollmentType;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String STATUS = "status";
        public static final String COURSE_ID = "courseId";
        public static final String BATCH_ID = "batchId";
        public static final String SESSION_ID = "sessionId";
        public static final String ROLE = "role";
        public static final String TYPE = "type";
        public static final String STATE = "state";
        public static final String STATE_CHANGE_TIME = "stateChangeTime";
        public static final String END_TIME = "endTime";
        public static final String STATE_CHANGE_TIME_LONG = "stateChangeTime.changeTime";
        public static final String STATE_CHANGE_TIME_NEW_STATE = "stateChangeTime.newState";
        public static final String ENTITY_ID = "entityId";
        public static final String ENTITY_TYPE = "entityType";
        public static final String PURCHASE_CONTEXT_ID = "purchaseContextId";
        public static final String PURCHASE_CONTEXT = "purchaseContextType";
        public static final String PURCHASE_ENROLLMENT_ID = "purchaseEnrollmentId";
        public static final String PURCHASE_CONTEXT_ENROLLMENT_TYPE = "purchaseContextEnrollmentType";
        public static final String ENTITY_TITLE="entityTitle";
    }
}
