package com.vedantu.onetofew.pojo;

import java.util.Set;

public class BatchEnrolmentInfo {

    private String batchId;
    private Set<String> enrolledStudentIds;


    public BatchEnrolmentInfo() {
        super();
    }

    public BatchEnrolmentInfo(String batchId, Set<String> enrolledStudentIds) {
        this.batchId = batchId;
        this.enrolledStudentIds = enrolledStudentIds;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Set<String> getEnrolledStudentIds() {
        return enrolledStudentIds;
    }

    public void setEnrolledStudentIds(Set<String> enrolledStudentIds) {
        this.enrolledStudentIds = enrolledStudentIds;
    }
}
