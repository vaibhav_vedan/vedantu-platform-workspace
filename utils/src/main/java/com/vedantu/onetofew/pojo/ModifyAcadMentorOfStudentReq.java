package com.vedantu.onetofew.pojo;

import java.util.List;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ModifyAcadMentorOfStudentReq extends AbstractFrontEndReq{
	
	private String acadMentorEmail;
	private List<Long> studentIds;
	
	public ModifyAcadMentorOfStudentReq() {
		super();
	}
	
	public String getAcadMentorEmail() {
		return acadMentorEmail;
	}
	public void setAcadMentorEmail(String acadMentorEmail) {
		this.acadMentorEmail = acadMentorEmail;
	}
	public List<Long> getStudentIds() {
		return studentIds;
	}
	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(StringUtils.isEmpty(acadMentorEmail)){
            errors.add("Empty Email of Acad Mentor");
        }
        
        if(ArrayUtils.isEmpty(studentIds)){
            errors.add("No student Ids to assign");
        }
        
        return errors;        
    }

}
