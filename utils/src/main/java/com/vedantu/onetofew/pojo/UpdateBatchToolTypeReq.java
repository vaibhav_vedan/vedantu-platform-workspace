package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;

public class UpdateBatchToolTypeReq {
	private String batchId;
	private OTFSessionToolType sessionToolType;

	public UpdateBatchToolTypeReq() {
		super();
	}

	public UpdateBatchToolTypeReq(String batchId, OTFSessionToolType sessionToolType) {
		super();
		this.batchId = batchId;
		this.sessionToolType = sessionToolType;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public OTFSessionToolType getSessionToolType() {
		return sessionToolType;
	}

	public void setSessionToolType(OTFSessionToolType sessionToolType) {
		this.sessionToolType = sessionToolType;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(this.batchId)) {
			errors.add("batchId");
		}
		if (this.sessionToolType == null) {
			errors.add("sessionToolType");
		}

		return errors;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Missing params:" + Arrays.toString(errors.toArray()));
		}
	}

	@Override
	public String toString() {
		return "UpdateBatchToolTypeReq [batchId=" + batchId + ", sessionToolType=" + sessionToolType + ", toString()="
				+ super.toString() + "]";
	}
}
