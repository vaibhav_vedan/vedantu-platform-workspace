/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.OTFSessionToolType;

import com.vedantu.onetofew.enums.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.section.SectionPojo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.onetofew.enums.CourseTerm;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@NoArgsConstructor
public class BatchReq extends AbstractFrontEndReq {
    private String courseId;
    private Long startTime;
    private Long endTime;
    @Getter(AccessLevel.NONE)
    private int maxEnrollment;
    @Getter(AccessLevel.NONE)
    private int minEnrollment = 1;
    private String id;
    private Set<String> teacherIds;
    private String currency = "INR";
    private int purchasePrice;
    private int cutPrice;
    private boolean isHalfScheduled;

    private String planString;
    private VisibilityState visibilityState;
    private EntityStatus batchState;
    private String demoVideoUrl;
    private List<BaseInstalmentInfo> instalmentDetails;

    private List<AgendaPojo> agenda;
    private OTFSessionToolType sessionToolType;
    private Integer registrationFee;
    private List<BoardTeacherPair> boardTeacherPairs;
    private Long duration;
    private String groupName;
    private String preferredSeat;
    private List<SessionPlanPojo> sessionPlan;
    private List<CourseTerm> searchTerms;
    private String whatsappJoinUrl;
    private boolean contentBatch = false;
    private boolean modularBatch = false;
    private String acadMentorId;
    private boolean recordedVideo = false;
    private Long lastPurchaseDate;
    @Getter(AccessLevel.NONE)
    private Language language = Language.DEFAULT;
    @Getter(AccessLevel.NONE)
    private List<SubscriptionPackageType> packageTypes;
    private Boolean earlyLearning = Boolean.FALSE;
    private String orgId;
    private List<EarlyLearningCourseType> earlyLearningCourses;
    private List<SectionPojo> sections;
    private Boolean hasSections = Boolean.FALSE;

    public int getMaxEnrollment() {
        if (maxEnrollment <= 0) {
            return 5;
        }

        return maxEnrollment;
    }

    public int getMinEnrollment() {
        if (minEnrollment <= 0) {
            return 1;
        }

		return minEnrollment;
	}
    public Language getLanguage() {
        if(Objects.isNull( language  )) {
            return Language.DEFAULT;
        }
        return language;
    }
    public List<SubscriptionPackageType> getPackageType() {
        if(Objects.isNull(packageTypes)) {
            List<SubscriptionPackageType> subscriptionPackageTypes1 =  new ArrayList<>();
            subscriptionPackageTypes1.add( SubscriptionPackageType.DEFAULT );
            return subscriptionPackageTypes1;
        }
        return packageTypes;
    }

}
