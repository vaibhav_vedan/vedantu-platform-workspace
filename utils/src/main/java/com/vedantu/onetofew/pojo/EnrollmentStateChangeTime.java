package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EnrollmentState;
import lombok.Data;

@Data
public class EnrollmentStateChangeTime {

	private Long changeTime;
	private Long changedBy;
	private EnrollmentState newState;
	private EnrollmentState previousState;
}
