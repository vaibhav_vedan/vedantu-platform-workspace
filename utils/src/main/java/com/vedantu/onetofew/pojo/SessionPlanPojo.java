package com.vedantu.onetofew.pojo;

import java.util.List;

public class SessionPlanPojo {

	private String day;
	private List<String> timings;

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public List<String> getTimings() {
		return timings;
	}

	public void setTimings(List<String> timings) {
		this.timings = timings;
	}
}
