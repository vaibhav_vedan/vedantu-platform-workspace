package com.vedantu.onetofew.pojo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVEnrollmentPojo {

    private String email;
    private List<String> batchIds;
    private String bundleId;

    public CSVEnrollmentPojo() {
        super();
    }

    public CSVEnrollmentPojo(String email, List<String> batchIds, String bundleId) {
        this.email = email;
        this.batchIds = batchIds;
        this.bundleId = bundleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(List<String> batchIds) {
        this.batchIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            for (String batchId : batchIds) {
                if (StringUtils.isNotEmpty(batchId)) {
                    this.batchIds.add(batchId.trim());
                }
            }
        }
    }

    public String getbundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<>();
        if (this.batchIds == null || this.batchIds.isEmpty()) {
            errors.add("batchIds");
        }
        if (this.bundleId == null) {
            errors.add("bundleId");
        }
        if (this.email == null) {
            errors.add("email");
        }

        return errors;
    }

    public void validate() throws BadRequestException {
        List<String> errors = collectErrors();
        if (!CollectionUtils.isEmpty(errors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Missing params:" + Arrays.toString(errors.toArray()));
        }
    }

    @Override
    public String toString() {
        return "CSVEnrollmentPojo{" + "email=" + email + ", batchIds=" + batchIds + ", bundleId=" + bundleId + '}';
    }

}
