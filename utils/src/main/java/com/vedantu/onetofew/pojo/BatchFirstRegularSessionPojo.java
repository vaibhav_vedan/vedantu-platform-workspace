/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

/**
 *
 * @author pranavm
 */
public class BatchFirstRegularSessionPojo {

    private String batchId;
    private Long endTime;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "BatchFirstRegularSessionPojo{" + "batchId=" + batchId + ", endTime=" + endTime + '}';
    }
    
    

}
