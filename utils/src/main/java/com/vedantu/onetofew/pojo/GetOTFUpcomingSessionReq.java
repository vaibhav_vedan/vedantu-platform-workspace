/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

/**
 *
 * @author shyam
 */
public class GetOTFUpcomingSessionReq {

	private Long afterEndTime;
	private String teacherId;
	private String studentId;
        private String query;
	private Integer start;
	private Integer limit;

	public GetOTFUpcomingSessionReq() {
	}

	public GetOTFUpcomingSessionReq(Long afterEndTime, String teacherId, String studentId, Integer start,
			Integer limit) {
		this.afterEndTime = afterEndTime;
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.start = start;
		this.limit = limit;
	}

	public Long getAfterEndTime() {
		return afterEndTime;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public String getStudentId() {
		return studentId;
	}

	public Integer getStart() {
		return start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setAfterEndTime(Long afterEndTime) {
		this.afterEndTime = afterEndTime;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "GetOTFUpcomingSessionReq{" + "afterEndTime=" + afterEndTime + ", teacherId=" + teacherId
				+ ", studentId=" + studentId + ", start=" + start + ", limit=" + limit + '}';
	}

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }
}
