package com.vedantu.onetofew.pojo;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.TimeInterval;

public class OTFSessionAttendeeInfo extends UserBasicInfo {
	private List<Long> joinTimes = new ArrayList<>();
	private long timeInSession = 0l;
	private List<TimeInterval> activeIntervals = new ArrayList<>();
	private Long studentSessionJoinTime;

	public OTFSessionAttendeeInfo(String userId) {
		super();
		this.setUserId(Long.parseLong(userId));
	}

	public OTFSessionAttendeeInfo(){

	}
        
	public OTFSessionAttendeeInfo(long userId) {
		super();
		this.setUserId(userId);
	}

	public List<Long> getJoinTimes() {
		return joinTimes;
	}

	public void setJoinTimes(List<Long> joinTimes) {
		this.joinTimes = joinTimes;
	}

	public long getTimeInSession() {
		return timeInSession;
	}

	public void setTimeInSession(long timeInSession) {
		this.timeInSession = timeInSession;
	}

	public List<TimeInterval> getActiveIntervals() {
		return activeIntervals;
	}

	public void setActiveIntervals(List<TimeInterval> activeIntervals) {
		this.activeIntervals = activeIntervals;
	}

	public Long getStudentSessionJoinTime() {
		return studentSessionJoinTime;
	}

	public void setStudentSessionJoinTime(Long studentSessionJoinTime) {
		this.studentSessionJoinTime = studentSessionJoinTime;
	}

	@Override
	public String toString() {
		return "OTFSessionAttendeeInfo [joinTimes=" + joinTimes + ", timeInSession=" + timeInSession
				+ ", activeIntervals=" + activeIntervals + ", toString()=" + super.toString() + "]";
	}
}
