package com.vedantu.onetofew.pojo;


import com.vedantu.onetofew.enums.AgendaType;

public class Agenda {
	private AgendaType type;
	private String sessionId;
	private ContentPojo contentPOJO;

	public Agenda() {
		super();
	}

	public Agenda(AgendaType type, String sessionId, ContentPojo contentPOJO) {
		super();
		this.type = type;
		this.sessionId = sessionId;
		this.contentPOJO = contentPOJO;
	}

	public Agenda(AgendaPojo agendaPojo) {
		super();
		this.type = agendaPojo.getType();
		switch (this.type) {
		case SESSION:
			if (agendaPojo.getSessionPOJO() != null) {
				this.sessionId = agendaPojo.getSessionPOJO().getId();
			}
			break;
		case CONTENT:
			this.contentPOJO = agendaPojo.getContentPOJO();
			break;
		default:
			break;
		}
	}

	public AgendaType getType() {
		return type;
	}

	public void setType(AgendaType type) {
		this.type = type;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ContentPojo getContentPOJO() {
		return contentPOJO;
	}

	public void setContentPOJO(ContentPojo contentPOJO) {
		this.contentPOJO = contentPOJO;
	}

	@Override
	public String toString() {
		return "Agenda [type=" + type + ", sessionId=" + sessionId + ", contentPOJO=" + contentPOJO + "]";
	}
}
