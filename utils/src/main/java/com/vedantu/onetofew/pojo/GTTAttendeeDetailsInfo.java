/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.User.LocationInfo;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.scheduling.pojo.HotspotNumbers;
import com.vedantu.scheduling.pojo.PollQuestionAnswer;
import com.vedantu.scheduling.pojo.QuizNumbers;
import com.vedantu.util.TimeInterval;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import com.vedantu.util.pojo.DeviceDetails;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GTTAttendeeDetailsInfo extends AbstractMongoStringIdEntityBean {

    private String userId;
    private String organizerAccessToken;
    private String organizerKey;
    private String traningId;
    private String sessionId;
    private String registrantKey;
    private String joinUrl;
    private List<Long> joinTimes = new ArrayList<>();

    private long timeInSession = 0L;
    private List<TimeInterval> activeIntervals = new ArrayList<>();
    private AttendeeContext attendeeType;
    private String enrollmentId;
    private List<PollQuestionAnswer> pollAnswered;
//    private List<ReplayWatchedDuration> replayWatchedDurations = new ArrayList<>();
    private long totalReplayWatchedDuration;
    private long avgReplayWatchedDuration;
    private boolean noshowEmailSent = false;
    private LocationInfo locationInfo;
    private DeviceDetails deviceDetails;
    private Long sessionStartTime;
    private Long sessionEndTime;

    //from post session data insights, info will be for Student, Teacher, TA , some of the fields won't apply to all
    private Long studentSessionJoinTime;
    private Long studentLastDisconnectTime;
    private Integer totalDisconnections;
    private Integer studentChatCount;
    private Integer thumbsUpCount;
    private Integer thumbsDownCount;
    private Float averageResponseTime;
    private List<Long> connectTimes;//connectiontimes
    private List<Long> disconnectTimes;//diconnectiontimes
//    private List<InteractionData> interactionDatas;
    private QuizNumbers quizNumbers;
    private HotspotNumbers hotspotNumbers;
//    private List<DoubtData> doubtDatas;
//    private DoubtNumbers doubtNumbers;
//    private PollNumbers pollNumbers;
    private Integer lbRank;
    private Float lbPercentile;
    private Integer lbPoints;
    private Integer studentOnlineDuration;
    private boolean isdeletedWithDeenrolled;
    private String idx;

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public String getOrganizerKey() {
        return organizerKey;
    }

    public void setOrganizerKey(String organizerKey) {
        this.organizerKey = organizerKey;
    }

    public String getTraningId() {
        return traningId;
    }

    public void setTraningId(String traningId) {
        this.traningId = traningId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getRegistrantKey() {
        return registrantKey;
    }

    public void setRegistrantKey(String registrantKey) {
        this.registrantKey = registrantKey;
    }

    public String getJoinUrl() {
        return joinUrl;
    }

    public void setJoinUrl(String joinUrl) {
        this.joinUrl = joinUrl;
    }

    public List<Long> getJoinTimes() {
        return joinTimes;
    }

    public void setJoinTimes(List<Long> joinTimes) {
        this.joinTimes = joinTimes;
    }

    public long getTimeInSession() {
        return timeInSession;
    }

    public void setTimeInSession(long timeInSession) {
        this.timeInSession = timeInSession;
    }

    public List<TimeInterval> getActiveIntervals() {
        return activeIntervals;
    }

    public void setActiveIntervals(List<TimeInterval> activeIntervals) {
        this.activeIntervals = activeIntervals;
    }

    public AttendeeContext getAttendeeType() {
        return attendeeType;
    }

    public void setAttendeeType(AttendeeContext attendeeType) {
        this.attendeeType = attendeeType;
    }

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public List<PollQuestionAnswer> getPollAnswered() {
        return pollAnswered;
    }

    public void setPollAnswered(List<PollQuestionAnswer> pollAnswered) {
        this.pollAnswered = pollAnswered;
    }

    public long getTotalReplayWatchedDuration() {
        return totalReplayWatchedDuration;
    }

    public void setTotalReplayWatchedDuration(long totalReplayWatchedDuration) {
        this.totalReplayWatchedDuration = totalReplayWatchedDuration;
    }

    public long getAvgReplayWatchedDuration() {
        return avgReplayWatchedDuration;
    }

    public void setAvgReplayWatchedDuration(long avgReplayWatchedDuration) {
        this.avgReplayWatchedDuration = avgReplayWatchedDuration;
    }

    public boolean isNoshowEmailSent() {
        return noshowEmailSent;
    }

    public void setNoshowEmailSent(boolean noshowEmailSent) {
        this.noshowEmailSent = noshowEmailSent;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public DeviceDetails getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(DeviceDetails deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    public Long getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(Long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public Long getSessionEndTime() {
        return sessionEndTime;
    }

    public void setSessionEndTime(Long sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public Long getStudentSessionJoinTime() {
        return studentSessionJoinTime;
    }

    public void setStudentSessionJoinTime(Long studentSessionJoinTime) {
        this.studentSessionJoinTime = studentSessionJoinTime;
    }

    public Long getStudentLastDisconnectTime() {
        return studentLastDisconnectTime;
    }

    public void setStudentLastDisconnectTime(Long studentLastDisconnectTime) {
        this.studentLastDisconnectTime = studentLastDisconnectTime;
    }

    public Integer getTotalDisconnections() {
        return totalDisconnections;
    }

    public void setTotalDisconnections(Integer totalDisconnections) {
        this.totalDisconnections = totalDisconnections;
    }

    public Integer getStudentChatCount() {
        return studentChatCount;
    }

    public void setStudentChatCount(Integer studentChatCount) {
        this.studentChatCount = studentChatCount;
    }

    public Integer getThumbsUpCount() {
        return thumbsUpCount;
    }

    public void setThumbsUpCount(Integer thumbsUpCount) {
        this.thumbsUpCount = thumbsUpCount;
    }

    public Integer getThumbsDownCount() {
        return thumbsDownCount;
    }

    public void setThumbsDownCount(Integer thumbsDownCount) {
        this.thumbsDownCount = thumbsDownCount;
    }

    public Float getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(Float averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public List<Long> getConnectTimes() {
        return connectTimes;
    }

    public void setConnectTimes(List<Long> connectTimes) {
        this.connectTimes = connectTimes;
    }

    public List<Long> getDisconnectTimes() {
        return disconnectTimes;
    }

    public void setDisconnectTimes(List<Long> disconnectTimes) {
        this.disconnectTimes = disconnectTimes;
    }

    public Integer getLbRank() {
        return lbRank;
    }

    public void setLbRank(Integer lbRank) {
        this.lbRank = lbRank;
    }

    public Float getLbPercentile() {
        return lbPercentile;
    }

    public void setLbPercentile(Float lbPercentile) {
        this.lbPercentile = lbPercentile;
    }

    public Integer getLbPoints() {
        return lbPoints;
    }

    public void setLbPoints(Integer lbPoints) {
        this.lbPoints = lbPoints;
    }

    public Integer getStudentOnlineDuration() {
        return studentOnlineDuration;
    }

    public void setStudentOnlineDuration(Integer studentOnlineDuration) {
        this.studentOnlineDuration = studentOnlineDuration;
    }

    public boolean isIsdeletedWithDeenrolled() {
        return isdeletedWithDeenrolled;
    }

    public void setIsdeletedWithDeenrolled(boolean isdeletedWithDeenrolled) {
        this.isdeletedWithDeenrolled = isdeletedWithDeenrolled;
    }

    public QuizNumbers getQuizNumbers() {
        return quizNumbers;
    }

    public void setQuizNumbers(QuizNumbers quizNumbers) {
        this.quizNumbers = quizNumbers;
    }

    public HotspotNumbers getHotspotNumbers() {
        return hotspotNumbers;
    }

    public void setHotspotNumbers(HotspotNumbers hotspotNumbers) {
        this.hotspotNumbers = hotspotNumbers;
    }
}
