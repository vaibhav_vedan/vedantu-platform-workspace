package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.AgendaType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;

public class AgendaPojo {
	private AgendaType type;
	private ContentPojo contentPOJO;
	private OTFSessionPojoUtils sessionPOJO;

	public AgendaPojo() {
		super();
	}
        
	public AgendaPojo(AgendaType type, OTFSessionPojoUtils sessionPOJO, ContentPojo contentPOJO) {
		super();
		this.type = type;
		this.sessionPOJO = sessionPOJO;
		this.contentPOJO = contentPOJO;
	}        

	public AgendaType getType() {
		return type;
	}

	public void setType(AgendaType type) {
		this.type = type;
	}

	public ContentPojo getContentPOJO() {
		return contentPOJO;
	}

	public void setContentPOJO(ContentPojo contentPOJO) {
		this.contentPOJO = contentPOJO;
	}

	public OTFSessionPojoUtils getSessionPOJO() {
		return sessionPOJO;
	}

	public void setSessionPOJO(OTFSessionPojoUtils sessionPOJO) {
		this.sessionPOJO = sessionPOJO;
	}

	@Override
	public String toString() {
		return "OTFAgenda [type=" + type + ", contentPOJO=" + contentPOJO + ", sessionPOJO=" + sessionPOJO
				+ ", toString()=" + super.toString() + "]";
	}
}
