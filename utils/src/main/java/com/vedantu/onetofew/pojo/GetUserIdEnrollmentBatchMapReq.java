/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;

/**
 *
 * @author ajith
 */
public class GetUserIdEnrollmentBatchMapReq extends AbstractFrontEndReq {

    private ArrayList<String> userIds;
    private ArrayList<String> batchIds;

    public ArrayList<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(ArrayList<String> userIds) {
        this.userIds = userIds;
    }

    public ArrayList<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(ArrayList<String> batchIds) {
        this.batchIds = batchIds;
    }

}
