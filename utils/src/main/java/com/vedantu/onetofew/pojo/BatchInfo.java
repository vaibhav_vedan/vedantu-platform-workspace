package com.vedantu.onetofew.pojo;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.subscription.pojo.BundleInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class BatchInfo extends BatchBasicInfo {
	private String currency;
	private String demoVideoUrl;
	private List<AgendaPojo> agenda;
	private Boolean demoSession;
	private Boolean demoSubscribed;
	private List<EnrollmentPojo> enrollments;
	private List<BaseInstalmentInfo> instalmentDetails;
	private BundleInfo bundleInfo;

    public List<EnrollmentPojo> getEnrollments() {
        return enrollments;
    }
        
        
        
}
