package com.vedantu.onetofew.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * @author mano
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebinarSessionInfo {
    private String webinarId;
    private String sessionId;
    private String joinUrl;
    private String joinUrlShort;
    private Set<String> subjects;
}
