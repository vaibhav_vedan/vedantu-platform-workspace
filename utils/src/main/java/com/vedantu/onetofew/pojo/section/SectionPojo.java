package com.vedantu.onetofew.pojo.section;

import com.vedantu.onetofew.enums.EnrollmentState;
import lombok.Data;
import java.util.List;
@Data
public class SectionPojo{
    String sectionId;
    private long size;
    private String sectionName;
    private EnrollmentState sectionType;
    private List<String> emailIds;
}
