package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
public class BatchDashboardInfo {

	private String batchId;
	private String courseId;
	private String courseTitle;
	private EntityStatus batchState;
	private String batchStartDate;
	private Long startTime;
	private Long endTime;
	private Set<String> teacherIds;
	private Set<String> subjects;
	private Set<String> targets;
	private Set<String> grades;
	private List<SessionPlanPojo> sessionPlan;
	private BundleEntityInfo aioPackage;
	private List<CurriculumPojo> curriculum;
	private List<BoardTeacherPair> boardTeacherPairs;

}
