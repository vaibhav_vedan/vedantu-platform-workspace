package com.vedantu.onetofew.pojo;

import java.util.List;
import java.util.Set;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class InsertAcadMentorStudentsReq extends AbstractFrontEndReq {

    private String acadMentorEmail;
    private Set<String> studentEmails;

    public InsertAcadMentorStudentsReq() {
        super();
    }

    public String getAcadMentorEmail() {
        return acadMentorEmail;
    }

    public void setAcadMentorEmail(String acadMentorEmail) {
        this.acadMentorEmail = acadMentorEmail;
    }

    public Set<String> getStudentEmails() {
        return studentEmails;
    }

    public void setStudentEmails(Set<String> studentEmails) {
        this.studentEmails = studentEmails;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(acadMentorEmail)) {
            errors.add("Empty Email of Acad Mentor");
        }

        if (ArrayUtils.isEmpty(studentEmails)) {
            errors.add("No student emails to assign");
        }

        return errors;

    }
}
