package com.vedantu.onetofew.pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.util.CollectionUtils;

public class OTFSessionSanityCheckResponse {
	private Set<String> tokenMissingsessionIds = new HashSet<>();
	private List<String> conflictCheckErrors = new ArrayList<>();

	public OTFSessionSanityCheckResponse() {
		super();
	}

	public Set<String> getTokenMissingsessionIds() {
		return tokenMissingsessionIds;
	}

	public void setConflictCheckErrors(List<String> conflictCheckErrors) {
		this.conflictCheckErrors = conflictCheckErrors;
	}

	public List<String> getConflictCheckErrors() {
		return conflictCheckErrors;
	}

	public void addTokenMissingsessionIds(Collection<String> input) {
		if (!CollectionUtils.isEmpty(input)) {
			this.tokenMissingsessionIds.addAll(input);
		}
	}

	public void addConflictCheckErrors(Collection<String> input) {
		if (!CollectionUtils.isEmpty(input)) {
			this.conflictCheckErrors.addAll(input);
		}
	}

	public void addNewEntry(OTFSessionSanityCheckResponse response) {
		if (response != null) {
			addTokenMissingsessionIds(response.getTokenMissingsessionIds());
			addConflictCheckErrors(response.getConflictCheckErrors());
		}
	}

	public boolean containsErrors() {
		return !CollectionUtils.isEmpty(this.tokenMissingsessionIds) ||!CollectionUtils.isEmpty(this.conflictCheckErrors);
	}

	@Override
	public String toString() {
		return "OTFSessionSanityCheckResponse [sessionIds=" + Arrays.toString(tokenMissingsessionIds.toArray())
				+ ", conflictCheckErrors=" + Arrays.toString(conflictCheckErrors.toArray()) + ", toString()="
				+ super.toString() + "]";
	}
}
