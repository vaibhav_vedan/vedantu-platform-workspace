/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

/**
 *
 * @author pranavm
 */
public class BatchChangeTime {
    
    	private Long changeTime;
	private Long changedBy;
        private String reason;
        private String previousBatchId;
        private String newBatchId;

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public Long getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Long changedBy) {
        this.changedBy = changedBy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPreviousBatchId() {
        return previousBatchId;
    }

    public void setPreviousBatchId(String previousBatchId) {
        this.previousBatchId = previousBatchId;
    }

    public String getNewBatchId() {
        return newBatchId;
    }

    public void setNewBatchId(String newBatchId) {
        this.newBatchId = newBatchId;
    }

    @Override
    public String toString() {
        return "BatchChangeTime(" + previousBatchId + ",changedBy" + changedBy + ",newBatchId " + newBatchId + ")";
    }
 }
