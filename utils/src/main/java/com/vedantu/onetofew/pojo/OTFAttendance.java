package com.vedantu.onetofew.pojo;

import com.vedantu.User.UserBasicInfo;

public class OTFAttendance {

    private String subject;
    private Long teacherId;
    private Integer attended = 0;
    private Integer total = 0;
    private Integer lastFive = 0;
    private Long lastAttended;
    private UserBasicInfo teacher;
    private Long studentId;
    private Long boardId;

    public OTFAttendance() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getAttended() {
        return attended;
    }

    public void setAttended(Integer attended) {
        this.attended = attended;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getLastFive() {
        return lastFive;
    }

    public void setLastFive(Integer lastFive) {
        this.lastFive = lastFive;
    }

    public Long getLastAttended() {
        return lastAttended;
    }

    public void setLastAttended(Long lastAttended) {
        this.lastAttended = lastAttended;
    }

    public UserBasicInfo getTeacher() {
        return teacher;
    }

    public void setTeacher(UserBasicInfo teacher) {
        this.teacher = teacher;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;

    }

    @Override
    public String toString() {
        return "OTFAttendance{" + "subject=" + subject + ", teacherId=" + teacherId + ", attended=" + attended + ", total=" + total + ", lastFive=" + lastFive + ", lastAttended=" + lastAttended + ", teacher=" + teacher + ", studentId=" + studentId + ", boardId=" + boardId + '}';
    }

}
