package com.vedantu.onetofew.pojo;

import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.util.fos.response.AbstractRes;

public class OTFUpdateSessionRes extends AbstractRes{

    private OTFSessionPojoUtils preUpdateSession;
    private OTFSessionPojoUtils postUpdateSession;

    public OTFUpdateSessionRes() {
        super();
    }

    public OTFSessionPojoUtils getPreUpdateSession() {
        return preUpdateSession;
    }

    public void setPreUpdateSession(OTFSessionPojoUtils preUpdateSession) {
        this.preUpdateSession = preUpdateSession;
    }

    public OTFSessionPojoUtils getPostUpdateSession() {
        return postUpdateSession;
    }

    public void setPostUpdateSession(OTFSessionPojoUtils postUpdateSession) {
        this.postUpdateSession = postUpdateSession;
    }

}
