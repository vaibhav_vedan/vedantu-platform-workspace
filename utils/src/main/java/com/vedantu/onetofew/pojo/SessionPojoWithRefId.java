package com.vedantu.onetofew.pojo;

import com.vedantu.session.pojo.OTFSessionPojoUtils;

/**
 * Created by somil on 13/05/17.
 */
public class SessionPojoWithRefId extends OTFSessionPojoUtils {
    private String referenceId;

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
