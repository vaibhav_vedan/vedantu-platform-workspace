package com.vedantu.onetofew.pojo;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class StudentSlotPreferencePojo extends AbstractFrontEndReq {

    private String userId;
    private String entityId;
    private EntityType entityType;
    private List<OTFSlot> preferredSlots;
    private UserBasicInfo userInfo;
    private Boolean postPayment;
    private String id;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public List<OTFSlot> getPreferredSlots() {
        return preferredSlots;
    }

    public void setPreferredSlots(List<OTFSlot> preferredSlots) {
        this.preferredSlots = preferredSlots;
    }

    public UserBasicInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserBasicInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Boolean getPostPayment() {
        return postPayment;
    }

    public void setPostPayment(Boolean postPayment) {
        this.postPayment = postPayment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
