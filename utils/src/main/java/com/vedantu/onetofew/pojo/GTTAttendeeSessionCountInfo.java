package com.vedantu.onetofew.pojo;

import com.vedantu.util.fos.response.AbstractRes;

public class GTTAttendeeSessionCountInfo extends AbstractRes {
    private Long _id ;
    private Long userId ;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public  Long getId() {
        return _id;
    }

    public void setId( Long _Id) {
        this._id = _Id;
    }

}
