package com.vedantu.onetofew.pojo;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;


public class BatchTiming {
	private Long startTime;
	private Long endTime;

	public BatchTiming() {
		super();
	}

	public BatchTiming(Long startTime, Long endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
        
	public static class Constants extends AbstractMongoStringIdEntityBean.Constants {
		public static final String START_TIME = "startTime";
		public static final String END_TIME = "endTime";		
	}        

}
