/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import java.util.Objects;

/**
 *
 * @author ajith
 */
public class BoardTeacher {

    private Long teacherId;
    private UserBasicInfo teacherInfo;//will be filled when response is given to front end, not stored in db
    private EntityState state = EntityState.ACTIVE;
    private BoardTeacherRole role = BoardTeacherRole.TEACHER;

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

    public UserBasicInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(UserBasicInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public BoardTeacherRole getRole() {
        return role;
    }

    public void setRole(BoardTeacherRole role) {
        this.role = role;
    }

    public enum BoardTeacherRole {
        TEACHER, OPS, MENTOR
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoardTeacher other = (BoardTeacher) obj;
        if (!Objects.equals(this.teacherId, other.teacherId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BoardTeacher{" + "teacherId=" + teacherId + ", teacherInfo=" + teacherInfo + ", state=" + state + ", role=" + role + '}';
    }

}
