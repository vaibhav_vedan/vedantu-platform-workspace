package com.vedantu.onetofew.pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class OTFWebinarSession extends AbstractFrontEndReq {
	private String id;
	private String title;
	private String description;
	private String presenter;
	private long startTime;
	private long endTime;
	private OTFSessionToolType sessionToolType;
	private String referenceId;

	public OTFWebinarSession() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPresenter() {
		return presenter;
	}

	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public OTFSessionToolType getSessionToolType() {
		return sessionToolType;
	}

	public void setSessionToolType(OTFSessionToolType sessionToolType) {
		this.sessionToolType = sessionToolType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (this.startTime <= 0l || this.startTime < System.currentTimeMillis()) {
			errors.add("startTime");
		}
		if (this.endTime <= 0l || this.endTime < System.currentTimeMillis()) {
			errors.add("endTime");
		}
		if (sessionToolType != null && sessionToolType.isUnsupported()) {
			errors.add("deprecated sessionToolType " + sessionToolType);
		}

		return errors;
	}
	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "OTFWebinarSession [id=" + id + ", title=" + title + ", description=" + description + ", presenter="
				+ presenter + ", startTime=" + startTime + ", endTime=" + endTime + ", sessionToolType="
				+ sessionToolType + ", toString()=" + super.toString() + "]";
	}
}
