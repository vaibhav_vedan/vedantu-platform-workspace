package com.vedantu.onetofew.pojo.section;

import lombok.Data;

@Data
public class SectionChangeTime {
    private Long changeTime;
    private Long changedBy;
    private String previousSectionId;
    private String newSectionId;
}
