/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.subscription.enums.EnrollmentType;

import java.util.List;

/**
 *
 * @author Darshit
 */
public class ActiveUserIdsByBatchIdsResp {

    private String batchId;
    private String userId;
    private String id;
    private List<BatchChangeTime> batchChangeTime;
    private String sectionId;

    public ActiveUserIdsByBatchIdsResp(String batchId, String userId, String id, List<BatchChangeTime> batchChangeTime, String sectionId) {
        this.batchId = batchId;
        this.userId = userId;
        this.id = id;
        this.batchChangeTime = batchChangeTime;
        this.sectionId = sectionId;
    }

    public ActiveUserIdsByBatchIdsResp(String batchId, String userId, String id, List<BatchChangeTime> batchChangeTimes) {
        this.batchId = batchId;
        this.userId = userId;
        this.id = id;
        this.batchChangeTime = batchChangeTimes;
    }

    public ActiveUserIdsByBatchIdsResp(String batchId, String userId, String id) {
        this.batchId = batchId;
        this.userId = userId;
        this.id = id;
    }

    public ActiveUserIdsByBatchIdsResp(String batchId, String userId) {
        this.batchId = batchId;
        this.userId = userId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<BatchChangeTime> getBatchChangeTime() {
        return batchChangeTime;
    }

    public void setBatchChangeTime(List<BatchChangeTime> batchChangeTimes) {
        this.batchChangeTime = batchChangeTimes;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

}
