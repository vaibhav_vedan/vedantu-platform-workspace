package com.vedantu.onetofew.pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.response.AbstractRes;

public class HandleOTFRecordingLambda extends AbstractRes{

    private String environment;
    private String sessionId;
    private List<RecordingEntry> recordings;
    private List<String> sessionkeys;
    private Credentials credentials;
    private Boolean deleteRecordings = Boolean.TRUE;

    public HandleOTFRecordingLambda() {
        super();
    }

    public HandleOTFRecordingLambda(String environment, String sessionId) {
        super();
        this.environment = environment;
        this.sessionId = sessionId;
        this.recordings = new ArrayList<>();
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<RecordingEntry> getRecordings() {
        return recordings;
    }

    public void setRecordings(List<RecordingEntry> recordings) {
        this.recordings = recordings;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public void addRecordingEntry(String recordingId, String downloadUrl) {
        this.recordings.add(new RecordingEntry(recordingId, downloadUrl));
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public static class Credentials {

        private String email;
        private String password;

        public Credentials() {
        }

        public Credentials(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "Credentials{" + "email=" + email + ", password=" + password + '}';
        }

    }

    public static class RecordingEntry {

        private String recordingId;
        private String downloadUrl;

        public RecordingEntry() {
            super();
        }

        public RecordingEntry(String recordingId, String downloadUrl) {
            super();
            this.recordingId = recordingId;
            this.downloadUrl = downloadUrl;
        }

        public String getRecordingId() {
            return recordingId;
        }

        public void setRecordingId(String recordingId) {
            this.recordingId = recordingId;
        }

        public String getDownloadUrl() {
            return downloadUrl;
        }

        public void setDownloadUrl(String downloadUrl) {
            this.downloadUrl = downloadUrl;
        }

        @Override
        public String toString() {
            return "RecordingEntry [recordingId=" + recordingId + ", downloadUrl=" + downloadUrl + ", toString()="
                    + super.toString() + "]";
        }
    }

    public Boolean getDeleteRecordings() {
		return deleteRecordings;
	}

	public void setDeleteRecordings(Boolean deleteRecordings) {
		this.deleteRecordings = deleteRecordings;
	}

	public List<String> collectErrors() {
        List<String> errors = new ArrayList<>();
        if (this.sessionId == null) {
            errors.add("sessionId");
        }

        if (CollectionUtils.isEmpty(recordings)) {
            errors.add("recordings");
        } else {
            for (RecordingEntry recordingEntry : recordings) {
                if (!StringUtils.isEmpty(recordingEntry.getRecordingId())
                        && !StringUtils.isEmpty(recordingEntry.getDownloadUrl())) {
                    errors.add("recording-id:" + recordingEntry.getRecordingId() + ",downloadUrl:"
                            + recordingEntry.getDownloadUrl());
                }
            }
        }

        return errors;
    }

    public void validate() throws BadRequestException {
        List<String> errors = collectErrors();
        if (!CollectionUtils.isEmpty(errors)) {
            throw new BadRequestException(ErrorCode.OTF_INVALID_HANDLE_RECORD_REQ, Arrays.toString(errors.toArray()));
        }
    }

    public List<String> getSessionkeys() {
        return sessionkeys;
    }

    public void setSessionkeys(List<String> sessionkeys) {
        this.sessionkeys = sessionkeys;
    }
}
