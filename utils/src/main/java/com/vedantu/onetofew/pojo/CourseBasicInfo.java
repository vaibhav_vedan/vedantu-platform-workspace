package com.vedantu.onetofew.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import java.util.List;
import java.util.Set;


public class CourseBasicInfo extends AbstractMongoStringIdEntityBean {
    private String title;
    private int liveBatches;
    private int upcomingBatches;
    private Set<String> targets;
    private Set<String> normalizeTargets;
    private Set<String> normalizeSubjects;
    private Set<String> grades;
    private VisibilityState visibilityState;
    private int priority;
    private List<UserBasicInfo> teachers;
    private List<String> tags;
    private Long launchDate;
    private boolean subscribed;
    private boolean featured;
    private String description;
    private Long batchStartTime;
    private Long batchEndTime;
    private Long studentsEnrolled;
    private Long totalSeats;
    private Integer registrationFee;
    List<BoardTeacherPair> boardTeacherPairs;
    private Long duration;
    private List<BoardTeacherPair> featuredTeachers;
    private List<CourseTerm> searchTerms;
    private List<CurriculumPojo> curriculumPojos;
    private Boolean earlyLearning;
    private String orgId;
    private List<EarlyLearningCourseType> earlyLearningCourses;



    public CourseBasicInfo() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLiveBatches() {
        return liveBatches;
    }

    public void setLiveBatches(int liveBatches) {
        this.liveBatches = liveBatches;
    }

    public int getUpcomingBatches() {
        return upcomingBatches;
    }

    public void setUpcomingBatches(int upcomingBatches) {
        this.upcomingBatches = upcomingBatches;
    }

    public VisibilityState getVisibilityState() {
        return visibilityState;
    }

    public void setVisibilityState(VisibilityState visibilityState) {
        this.visibilityState = visibilityState;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Set<String> getTargets() {
        return targets;
    }

    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    public List<UserBasicInfo> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<UserBasicInfo> teachers) {
        this.teachers = teachers;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Long getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(Long launchDate) {
        this.launchDate = launchDate;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getBatchStartTime() {
        return batchStartTime;
    }

    public void setBatchStartTime(Long batchStartTime) {
        this.batchStartTime = batchStartTime;
    }

    public Long getBatchEndTime() {
        return batchEndTime;
    }

    public void setBatchEndTime(Long batchEndTime) {
        this.batchEndTime = batchEndTime;
    }

    public Long getStudentsEnrolled() {
        return studentsEnrolled;
    }

    public void setStudentsEnrolled(Long studentsEnrolled) {
        this.studentsEnrolled = studentsEnrolled;
    }

    public Long getTotalSeats() {
        return totalSeats;
    }

    public Set<String> getNormalizeSubjects() {
        return normalizeSubjects;
    }

    public void setNormalizeSubjects(Set<String> normalizeSubjects) {
        this.normalizeSubjects = normalizeSubjects;
    }

    public Set<String> getNormalizeTargets() {
        return normalizeTargets;
    }

    public void setNormalizeTargets(Set<String> normalizeTargets) {
        this.normalizeTargets = normalizeTargets;
    }

    public void setTotalSeats(Long totalSeats) {
        this.totalSeats = totalSeats;
    }

    public Integer getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(Integer registrationFee) {
        this.registrationFee = registrationFee;
    }

    public List<BoardTeacherPair> getBoardTeacherPairs() {
        return boardTeacherPairs;
    }

    public void setBoardTeacherPairs(List<BoardTeacherPair> boardTeacherPairs) {
        this.boardTeacherPairs = boardTeacherPairs;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public List<BoardTeacherPair> getFeaturedTeachers() {
        return featuredTeachers;
    }

    public void setFeaturedTeachers(List<BoardTeacherPair> featuredTeachers) {
        this.featuredTeachers = featuredTeachers;
    }

    public List<CourseTerm> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<CourseTerm> searchTerms) {
        this.searchTerms = searchTerms;
    }

    public List<CurriculumPojo> getCurriculumPojos() {
        return curriculumPojos;
    }

    public void setCurriculumPojos(List<CurriculumPojo> curriculumPojos) {
        this.curriculumPojos = curriculumPojos;
    }

	public Boolean getEarlyLearning() {
		return earlyLearning;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


	public void setEarlyLearning(Boolean earlyLearning) {
		this.earlyLearning = earlyLearning;
	}

    public List<EarlyLearningCourseType> getEarlyLearningCourses() {
        return earlyLearningCourses;
    }

    public void setEarlyLearningCourses(List<EarlyLearningCourseType> earlyLearningCourses) {
        this.earlyLearningCourses = earlyLearningCourses;
    }
}
