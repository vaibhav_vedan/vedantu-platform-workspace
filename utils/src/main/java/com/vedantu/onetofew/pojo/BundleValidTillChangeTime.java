package com.vedantu.onetofew.pojo;

import com.vedantu.subscription.enums.bundle.BundleState;
import lombok.Data;

@Data
public class BundleValidTillChangeTime {

	private Long changeTime;
	private Long changedBy;
	private Long validTill;

	public BundleValidTillChangeTime() {
	}

	public BundleValidTillChangeTime(Long changeTime, Long changedBy, Long validTill) {
		this.changeTime = changeTime;
		this.changedBy = changedBy;
		this.validTill = validTill;
	}
}
