/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import java.util.Objects;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class BoardTeacherPair {

    private Long boardId;
    private String subject;//will be filled when response is given to front end, not stored in db
    private Set<BoardTeacher> teachers;

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Set<BoardTeacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<BoardTeacher> teachers) {
        this.teachers = teachers;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "BoardTeacherPair{" + "boardId=" + boardId + ", subject=" + subject + ", teachers=" + teachers + '}';
    }

}
