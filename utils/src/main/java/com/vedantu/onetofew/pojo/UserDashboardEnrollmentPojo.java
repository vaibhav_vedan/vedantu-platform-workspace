/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.onetofew.pojo;

import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.scheduling.response.session.OTFBatchSessionReportRes;
import java.util.List;

/**
 *
 * @author jeet
 */
public class UserDashboardEnrollmentPojo{
    private EnrollmentPojo enrollmentPojo;
    private OTFBatchSessionReportRes oTFBatchSessionReportRes;
    private List<ContentInfoResp> testInfos;
    
    public UserDashboardEnrollmentPojo(EnrollmentPojo pojo){
        this.enrollmentPojo = pojo;
    }

    public OTFBatchSessionReportRes getoTFBatchSessionReportRes() {
        return oTFBatchSessionReportRes;
    }

    public void setoTFBatchSessionReportRes(OTFBatchSessionReportRes oTFBatchSessionReportRes) {
        this.oTFBatchSessionReportRes = oTFBatchSessionReportRes;
    }

    public EnrollmentPojo getEnrollmentPojo() {
        return enrollmentPojo;
    }

    public void setEnrollmentPojo(EnrollmentPojo enrollmentPojo) {
        this.enrollmentPojo = enrollmentPojo;
    }

    public List<ContentInfoResp> getTestInfos() {
        return testInfos;
    }

    public void setTestInfos(List<ContentInfoResp> testInfos) {
        this.testInfos = testInfos;
    }
   
}
