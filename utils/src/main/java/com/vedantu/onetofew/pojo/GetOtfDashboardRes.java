package com.vedantu.onetofew.pojo;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class GetOtfDashboardRes extends AbstractMongoStringIdEntityBean {

	private Long userId;
	private String batchId;
	private String batchStartDate;
	private String courseId;
	private String courseTitle;
	private Long startTime;
	private EnrollmentState state;
	private EntityStatus status;
	private String enrollmentId;
	private String registrationOrderId;
	private String registrationRefundStatus;
	private Long registrationCreationTime;
	private Integer registrationAmount;
	private List<DashBoardInstalmentInfo> otfBasicInstalmentInfos;
	private List<OTFAttendance> otfAttendances;
	private List<StudentContentPojo> contents;
	private UserBasicInfo user;
	private List<UserBasicInfo> teachers;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getBatchStartDate() {
		return batchStartDate;
	}

	public void setBatchStartDate(String batchStartDate) {
		this.batchStartDate = batchStartDate;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public EnrollmentState getState() {
		return state;
	}

	public void setState(EnrollmentState state) {
		this.state = state;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public String getRegistrationOrderId() {
		return registrationOrderId;
	}

	public void setRegistrationOrderId(String registrationOrderId) {
		this.registrationOrderId = registrationOrderId;
	}

	public String getRegistrationRefundStatus() {
		return registrationRefundStatus;
	}

	public void setRegistrationRefundStatus(String registrationRefundStatus) {
		this.registrationRefundStatus = registrationRefundStatus;
	}

	public Long getRegistrationCreationTime() {
		return registrationCreationTime;
	}

	public void setRegistrationCreationTime(Long registrationCreationTime) {
		this.registrationCreationTime = registrationCreationTime;
	}

	public Integer getRegistrationAmount() {
		return registrationAmount;
	}

	public void setRegistrationAmount(Integer registrationAmount) {
		this.registrationAmount = registrationAmount;
	}

	public List<DashBoardInstalmentInfo> getOtfBasicInstalmentInfos() {
		return otfBasicInstalmentInfos;
	}

	public void setOtfBasicInstalmentInfos(List<DashBoardInstalmentInfo> otfBasicInstalmentInfos) {
		this.otfBasicInstalmentInfos = otfBasicInstalmentInfos;
	}

	public List<OTFAttendance> getOtfAttendances() {
		return otfAttendances;
	}

	public void setOtfAttendances(List<OTFAttendance> otfAttendances) {
		this.otfAttendances = otfAttendances;
	}

	public List<StudentContentPojo> getContents() {
		return contents;
	}

	public void setContents(List<StudentContentPojo> contents) {
		this.contents = contents;
	}

	public UserBasicInfo getUser() {
		return user;
	}

	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	public List<UserBasicInfo> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<UserBasicInfo> teachers) {
		this.teachers = teachers;
	}

}
