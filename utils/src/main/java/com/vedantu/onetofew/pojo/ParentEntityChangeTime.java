package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import lombok.Data;

@Data
public class ParentEntityChangeTime {
	private Long changeTime;

	private String oldBundleEnrollmentId;
	private String newBundleEnrollmentId;

}
