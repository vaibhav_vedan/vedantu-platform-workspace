package com.vedantu.onetofew.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;

public class GetOTFInstalmentReminderRes {
	private String agentName;
	private Long studentId;
	private UserBasicInfo student;
	private String state;
	private String status;
	private Long instalmentDueTime;
	private String enrollmentId;
	private String batchId;
         private DashboardUserInstalmentHistoryRes extraInfos;
         
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public UserBasicInfo getStudent() {
		return student;
	}
	public void setStudent(UserBasicInfo student) {
		this.student = student;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getInstalmentDueTime() {
		return instalmentDueTime;
	}
	public void setInstalmentDueTime(Long instalmentDueTime) {
		this.instalmentDueTime = instalmentDueTime;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

        public DashboardUserInstalmentHistoryRes getExtraInfos() {
            return extraInfos;
        }

        public void setExtraInfos(DashboardUserInstalmentHistoryRes extraInfos) {
            this.extraInfos = extraInfos;
        }

}
