package com.vedantu.onetofew.pojo;

import com.vedantu.session.pojo.EntityType;

public class OTMBundleEntityInfo {

    private EntityType entityType;
    private String entityId;
    private Integer price;
    private Integer duration; //will be populated when creating Registration entity, not to be populated by user

    public OTMBundleEntityInfo() {
    }

    public OTMBundleEntityInfo(EntityType entityType, String entityId) {
        this.entityType = entityType;
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "OTMBundleEntityInfo{" + "entityType=" + entityType + ", entityId=" + entityId + ", price=" + price + ", hrs=" + duration + '}';
    }
    


}
