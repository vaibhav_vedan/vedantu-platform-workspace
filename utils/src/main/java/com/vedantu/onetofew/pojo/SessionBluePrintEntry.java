package com.vedantu.onetofew.pojo;

public class SessionBluePrintEntry {
	private String title;
	private String description;
	private String teacherEmail;
	private String teacherId;
	private int srNo;

	public SessionBluePrintEntry() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTeacherEmail() {
		return teacherEmail;
	}

	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	@Override
	public String toString() {
		return "SessionBluePrintEntry [title=" + title + ", description=" + description + ", teacherEmail="
				+ teacherEmail + ", teacherId=" + teacherId + ", srNo=" + srNo + ", toString()=" + super.toString()
				+ "]";
	}
}
