package com.vedantu.onetofew.pojo;

import java.util.List;

public class ContentPojo {
	public List<ContentInfo> contentList;
	public String description;
	public Long startTime;
	public String title;

	public ContentPojo() {
		super();
	}

	public ContentPojo(List<ContentInfo> contentList, String description, Long startTime, String title) {
		super();
		this.contentList = contentList;
		this.description = description;
		this.startTime = startTime;
		this.title = title;
	}

	public List<ContentInfo> getContentList() {
		return contentList;
	}

	public void setContentList(List<ContentInfo> contentList) {
		this.contentList = contentList;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "OTFContentPojo [contentList=" + contentList + ", description=" + description + ", startTime="
				+ startTime + ", title=" + title + "]";
	}
}
