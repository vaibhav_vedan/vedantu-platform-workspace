package com.vedantu.onetofew.pojo;

import java.util.List;

public class GetBatchesRes {
	
	private List<BatchBasicInfo> batches;
	
	public List<BatchBasicInfo> getBatches() {
		return batches;
	}

	public void setBatches(List<BatchBasicInfo> batches) {
		this.batches = batches;
	}
}
