package com.vedantu.onetofew.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.*;
import com.vedantu.scheduling.pojo.Pollsdata;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class OTFSessionInfo extends AbstractMongoStringIdEntityBean {

    private Set<String> batchIds;
    private String title;
    private Long boardId;//subject
    private String description;
    private String presenter;
    private Long startTime;
    private Long teacherJoinTime;
    private Long duration;
    private String vimeoId;
    private Integer attendance = 0;
    private Integer totalAttendance = 0;
    private List<String> attended;
    private List<String> absentees;
    private List<UserBasicInfo> attendedUsers;
    private List<UserBasicInfo> absentUsers;
    private SessionState state;
    private Pollsdata pollsdata;
    private UserBasicInfo teacher;
    private Set<Long> taIds;
    private Long startedAt;
    private Long endedAt;
    private OTMSessionInProgressState progressState;
    private OTFSessionContextType sessionContextType;
    private EntityType type;
    private OTMSessionType otmSessionType;
    private Set<OTFSessionFlag> flags = new HashSet<>();
    private Set<SessionLabel> labels = new HashSet<>();
    private Set<EntityTag> entityTags = new HashSet<>();

    public boolean containsFlag(OTFSessionFlag flag) {
        return flag != null && this.flags != null && this.flags.contains(flag);
    }

    public boolean isEarlyLearningSession() {
        return getLabels() != null && (getLabels().contains(SessionLabel.SUPER_CODER) || getLabels().contains(SessionLabel.SUPER_READER));
    }

    public boolean isWebinarSession() {
        return getLabels() != null && (getLabels().contains(SessionLabel.WEBINAR) || getSessionContextType() == OTFSessionContextType.WEBINAR);
    }

    public boolean isSalesDemoSession() {
        return this.containsFlag(OTFSessionFlag.SALES_DEMO) || this.otmSessionType == OTMSessionType.SALES_DEMO;
    }
}
