package com.vedantu.onetofew.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import lombok.Data;

@Data
public class StatusChangeTime {

	private Long changeTime;
	private Long changedBy;
	private EntityStatus previousStatus;
	private EntityStatus newStatus;
	private String transactionId;
}
