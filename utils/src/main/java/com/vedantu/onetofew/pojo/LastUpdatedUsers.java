package com.vedantu.onetofew.pojo;

import java.util.HashSet;
import java.util.Set;

import com.vedantu.util.CollectionUtils;

public class LastUpdatedUsers {
	private Set<String> teacherIds = new HashSet<>();
	private Set<String> studentIds = new HashSet<>();
	private long startTime;
	private long endTime;
	private Set<String> batchIds = new HashSet<>();

	public LastUpdatedUsers() {
		super();
	}

	public LastUpdatedUsers(long startTime, long endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Set<String> getTeacherIds() {
		return teacherIds;
	}

	public int getTeacherIdsSize() {
		return teacherIds == null ? 0 : teacherIds.size() ;
	}

	public void setTeacherIds(Set<String> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public Set<String> getStudentIds() {
		return studentIds;
	}

	public int getStudentIdsSize() {
		return studentIds == null ? 0 : studentIds.size() ;
	}

	public void setStudentIds(Set<String> studentIds) {
		this.studentIds = studentIds;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public void addStudentIds(Set<String> studentIds) {
		if (!CollectionUtils.isEmpty(studentIds)) {
			this.studentIds.addAll(studentIds);
		}
	}

	public void addTeacherIds(Set<String> teacherIds) {
		if (!CollectionUtils.isEmpty(teacherIds)) {
			this.teacherIds.addAll(teacherIds);
		}
	}

	public void addEntry(LastUpdatedUsers lastUpdatedUsers) {
		if (lastUpdatedUsers == null) {
			return;
		}

		if (!CollectionUtils.isEmpty(lastUpdatedUsers.getTeacherIds())) {
			this.teacherIds.addAll(lastUpdatedUsers.getTeacherIds());
		}
		if (!CollectionUtils.isEmpty(lastUpdatedUsers.getStudentIds())) {
			this.studentIds.addAll(lastUpdatedUsers.getStudentIds());
		}
	}

	public Set<String> getBatchIds() {
		return batchIds;
	}

	public void setBatchIds(Set<String> batchIds) {
		this.batchIds = batchIds;
	}

	@Override
	public String toString() {
		return "LastUpdatedUsers [teacherIds=" + teacherIds + ", studentIds=" + studentIds + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", toString()=" + super.toString() + "]";
	}
}
