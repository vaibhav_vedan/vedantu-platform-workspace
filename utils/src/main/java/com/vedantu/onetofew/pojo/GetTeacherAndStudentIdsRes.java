package com.vedantu.onetofew.pojo;

import java.util.List;

public class GetTeacherAndStudentIdsRes {

	private List<Long> teacherIds;
	private List<Long> studentIds;

	public List<Long> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(List<Long> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}
}
