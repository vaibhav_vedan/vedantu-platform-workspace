package com.vedantu.onetofew.pojo;

import java.util.Set;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AcadMentorStudentsResp {
	private String acadMentorId;
	private Set<String> studentIds;
	private String acadMentorName;
	private String acadMentorEmail;
	
	public AcadMentorStudentsResp() {}
	
	public String getAcadMentorId() {
		return acadMentorId;
	}
	public void setAcadMentorId(String acadMentorId) {
		this.acadMentorId = acadMentorId;
	}
	public Set<String> getStudentIds() {
		return studentIds;
	}
	public void setStudentIds(Set<String> studentIds) {
		this.studentIds = studentIds;
	}

	public String getAcadMentorName() {
		return acadMentorName;
	}

	public void setAcadMentorName(String acadMentorName) {
		this.acadMentorName = acadMentorName;
	}

	public String getAcadMentorEmail() {
		return acadMentorEmail;
	}

	public void setAcadMentorEmail(String acadMentorEmail) {
		this.acadMentorEmail = acadMentorEmail;
	}
}
