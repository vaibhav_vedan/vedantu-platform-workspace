package com.vedantu.onetofew.pojo;

import java.util.List;

public class CSVEnrollmentRes {


    private String email;
    private List<String> batchIds;
    private String bundleId;
    private boolean success;
    private String failReason;

    public CSVEnrollmentRes(){
        super();
    }

    public CSVEnrollmentRes(CSVEnrollmentPojo csvEnrollmentPojo, boolean success, String failReason) {
        this.email = csvEnrollmentPojo.getEmail();
        this.batchIds = csvEnrollmentPojo.getBatchIds();
        this.bundleId = csvEnrollmentPojo.getbundleId();
        this.success = success;
        this.failReason = failReason;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(List<String> batchIds) {
        this.batchIds = batchIds;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }
}
