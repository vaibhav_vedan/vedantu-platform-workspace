/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.async;

import com.vedantu.util.LogFactory;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class AsyncTaskFactory
{

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(AsyncTaskFactory.class);


    private void executeTaskWithoutDelay(AsyncTaskParams params)
    {
        try
        {
            logger.info("ENTRY: "+params);
            IAsyncTaskName taskName = params.getAsyncTaskName();
            IAsyncQueueName queue = taskName.getQueue();

            //This is implemented in DefaultQueueExecutor.java
            queue.execute(params);
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
        }
    }


    public void loam_executeTaskWithoutDelay(AsyncTaskParams params)
    {
        try
        {
            logger.info("ENTRY: "+params);
            IAsyncTaskName taskName = params.getAsyncTaskName();
            IAsyncQueueName queue = taskName.getQueue();
            queue.execute(params);
        }
        catch (Exception ex)
        {
            logger.error(ex);
        }
    }

    public void executeTask(AsyncTaskParams params)
    {
        Long delayMillis = params.getDelayMillis();

        if (delayMillis != null && delayMillis > 0L)
        {
            ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
            ConcurrentTaskScheduler scheduler = new ConcurrentTaskScheduler(localExecutor);
            Date date = new Date(System.currentTimeMillis() + delayMillis);//delay

            scheduler.schedule(new Runnable() {
                @Override
                public void run()
                {
                    try
                    {
                        logger.info("executing async method "+params);
                        executeTaskWithoutDelay(params);
                    }
                    catch (Exception ex)
                    {
                        logger.info(ex.getMessage(), ex);
                    }
                }
            }, date);
        }
        else
        {
            executeTaskWithoutDelay(params);
        }
    }
}
