/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.async;

/**
 *
 * @author somil
 */
public interface IAsyncQueueExecutor {
    public void execute(AsyncTaskParams parmas) throws Exception;
    public void recover(Exception exception);
}
