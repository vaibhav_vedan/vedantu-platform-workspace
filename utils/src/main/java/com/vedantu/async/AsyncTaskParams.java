/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.async;

import java.util.Map;

/**
 *
 * @author somil
 */
public class AsyncTaskParams
{
    IAsyncTaskName asyncTaskName;
    Map<String, Object> payload;
    Long delayMillis = null;
    String videoId;


    public AsyncTaskParams(IAsyncTaskName asyncTaskName, Map<String, Object> payload)
    {
        this.delayMillis = null;
        this.asyncTaskName = asyncTaskName;
        this.payload = payload;
    }
    
    public AsyncTaskParams(IAsyncTaskName asyncTaskName, Map<String, Object> payload, Long delayMillis)
    {
        this.asyncTaskName = asyncTaskName;
        this.payload = payload;
        this.delayMillis = delayMillis;
    }

    public IAsyncTaskName getAsyncTaskName() {
        return asyncTaskName;
    }

    public void setAsyncTaskName(IAsyncTaskName asyncTaskName) {
        this.asyncTaskName = asyncTaskName;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }

    public Long getDelayMillis() {
        return delayMillis;
    }

    public void setDelayMillis(Long delayMillis) {
        this.delayMillis = delayMillis;
    }

    public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	@Override
    public String toString() {
        return "AsyncTaskParams{" + "asyncTaskName=" + asyncTaskName + ", payload=" + payload + ", delayMillis=" + delayMillis + '}';
    }
    
}
