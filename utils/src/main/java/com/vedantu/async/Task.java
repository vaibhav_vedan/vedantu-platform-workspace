package com.vedantu.async;

import org.springframework.scheduling.annotation.Async;

public interface Task {
	
	@Async
	void execute();

}
