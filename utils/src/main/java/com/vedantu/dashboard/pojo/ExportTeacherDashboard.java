package com.vedantu.dashboard.pojo;

public class ExportTeacherDashboard {
	
	private Long teacherId;
	private String teacherName;
	private String emailId;
	private String phoneCode;
	private String contactNumber;
	private String entityType;
	private Long primarySubject;
	private String currentState;
	private Integer sessionsBooked = 0;
	private Integer cancelledByTeacher = 0;
	private Integer cancelledByStudent = 0;
	private Integer totalCancelled = 0;
	private Integer rescheduled = 0;
	private Integer sessionsDone = 0;
	private Integer sessionsLateJoined = 0;
	private Integer activeStudents = 0;
	private Integer churn = 0;
	private Integer contentShared = 0;
	private Integer contentAttempted = 0;
	private Integer contentEvaluated = 0;
	private Integer planShared = 0;
	private Integer planAcceptedAndPaid = 0;
	private Integer planEnded = 0;
	
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public Long getPrimarySubject() {
		return primarySubject;
	}
	public void setPrimarySubject(Long primarySubject) {
		this.primarySubject = primarySubject;
	}
	public String getCurrentState() {
		return currentState;
	}
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	public Integer getSessionsBooked() {
		return sessionsBooked;
	}
	public void setSessionsBooked(Integer sessionsBooked) {
		this.sessionsBooked = sessionsBooked;
	}
	public Integer getCancelledByTeacher() {
		return cancelledByTeacher;
	}
	public void setCancelledByTeacher(Integer cancelledByTeacher) {
		this.cancelledByTeacher = cancelledByTeacher;
	}
	public Integer getCancelledByStudent() {
		return cancelledByStudent;
	}
	public void setCancelledByStudent(Integer cancelledByStudent) {
		this.cancelledByStudent = cancelledByStudent;
	}
	public Integer getTotalCancelled() {
		return totalCancelled;
	}
	public void setTotalCancelled(Integer totalCancelled) {
		this.totalCancelled = totalCancelled;
	}
	public Integer getRescheduled() {
		return rescheduled;
	}
	public void setRescheduled(Integer rescheduled) {
		this.rescheduled = rescheduled;
	}
	public Integer getSessionsDone() {
		return sessionsDone;
	}
	public void setSessionsDone(Integer sessionsDone) {
		this.sessionsDone = sessionsDone;
	}
	public Integer getSessionsLateJoined() {
		return sessionsLateJoined;
	}
	public void setSessionsLateJoined(Integer sessionsLateJoined) {
		this.sessionsLateJoined = sessionsLateJoined;
	}
	public Integer getActiveStudents() {
		return activeStudents;
	}
	public void setActiveStudents(Integer activeStudents) {
		this.activeStudents = activeStudents;
	}
	public Integer getChurn() {
		return churn;
	}
	public void setChurn(Integer churn) {
		this.churn = churn;
	}
	public Integer getContentShared() {
		return contentShared;
	}
	public void setContentShared(Integer contentShared) {
		this.contentShared = contentShared;
	}
	public Integer getContentAttempted() {
		return contentAttempted;
	}
	public void setContentAttempted(Integer contentAttempted) {
		this.contentAttempted = contentAttempted;
	}
	public Integer getContentEvaluated() {
		return contentEvaluated;
	}
	public void setContentEvaluated(Integer contentEvaluated) {
		this.contentEvaluated = contentEvaluated;
	}
	public Integer getPlanShared() {
		return planShared;
	}
	public void setPlanShared(Integer planShared) {
		this.planShared = planShared;
	}
	public Integer getPlanAcceptedAndPaid() {
		return planAcceptedAndPaid;
	}
	public void setPlanAcceptedAndPaid(Integer planAcceptedAndPaid) {
		this.planAcceptedAndPaid = planAcceptedAndPaid;
	}
	public Integer getPlanEnded() {
		return planEnded;
	}
	public void setPlanEnded(Integer planEnded) {
		this.planEnded = planEnded;
	}

}
