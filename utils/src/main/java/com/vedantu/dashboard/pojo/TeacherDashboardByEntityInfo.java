package com.vedantu.dashboard.pojo;

import java.util.Set;

import com.vedantu.session.pojo.EntityType;

public class TeacherDashboardByEntityInfo {

	private EntityType entityType;
	private Set<Long> studentIds;//will be used to find active students
	private Integer booked = 0;
	private Integer sessionEnded = 0;
	private Integer cancelledByTeacher = 0;
	private Integer cancelled = 0;
	private Integer rescheduled = 0;
	private Integer expired;// For OTF -> implies no one joined
	private Integer lateJoined = 0;
	private Integer forfeited;//for OTF -> implies only teacher joined
	private Set<String> attendees;//will be used to find active students
	private Integer teacherNoShow;
	private Integer cancelledByStudent=0;
	private Integer cancelledByAdmin = 0;
	private Integer contentShared = 0;
	private Integer contentAttempted = 0;
	private Integer contentEvaluated = 0;
	private Integer newCourse = 0;
	private Integer enrolled = 0;
	private Integer courseEnded = 0;
	private Set<Long> courseEndedStudents;
	
	public TeacherDashboardByEntityInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TeacherDashboardByEntityInfo(EntityType entityType) {
		super();
		this.entityType = entityType;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public Set<Long> getStudentIds() {
		return studentIds;
	}
	public void setStudentIds(Set<Long> studentIds) {
		this.studentIds = studentIds;
	}
	public Integer getBooked() {
		return booked;
	}
	public void setBooked(Integer booked) {
		this.booked = booked;
	}
	public Integer getSessionEnded() {
		return sessionEnded;
	}
	public void setSessionEnded(Integer sessionEnded) {
		this.sessionEnded = sessionEnded;
	}
	public Integer getCancelledByTeacher() {
		return cancelledByTeacher;
	}
	public void setCancelledByTeacher(Integer cancelledByTeacher) {
		this.cancelledByTeacher = cancelledByTeacher;
	}
	public Integer getCancelled() {
		return cancelled;
	}
	public void setCancelled(Integer cancelled) {
		this.cancelled = cancelled;
	}
	public Integer getRescheduled() {
		return rescheduled;
	}
	public void setRescheduled(Integer rescheduled) {
		this.rescheduled = rescheduled;
	}
	public Integer getExpired() {
		return expired;
	}
	public void setExpired(Integer expired) {
		this.expired = expired;
	}
	public Integer getLateJoined() {
		return lateJoined;
	}
	public void setLateJoined(Integer lateJoined) {
		this.lateJoined = lateJoined;
	}
	public Integer getForfeited() {
		return forfeited;
	}
	public void setForfeited(Integer forfeited) {
		this.forfeited = forfeited;
	}
	public Set<String> getAttendees() {
		return attendees;
	}
	public void setAttendees(Set<String> attendees) {
		this.attendees = attendees;
	}
	public Integer getTeacherNoShow() {
		return teacherNoShow;
	}
	public void setTeacherNoShow(Integer teacherNoShow) {
		this.teacherNoShow = teacherNoShow;
	}
	public Integer getCancelledByStudent() {
		return cancelledByStudent;
	}
	public void setCancelledByStudent(Integer cancelledByStudent) {
		this.cancelledByStudent = cancelledByStudent;
	}
	public Integer getCancelledByAdmin() {
		return cancelledByAdmin;
	}
	public void setCancelledByAdmin(Integer cancelledByAdmin) {
		this.cancelledByAdmin = cancelledByAdmin;
	}
	public Integer getContentShared() {
		return contentShared;
	}
	public void setContentShared(Integer contentShared) {
		this.contentShared = contentShared;
	}
	public Integer getContentAttempted() {
		return contentAttempted;
	}
	public void setContentAttempted(Integer contentAttempted) {
		this.contentAttempted = contentAttempted;
	}
	public Integer getContentEvaluated() {
		return contentEvaluated;
	}
	public void setContentEvaluated(Integer contentEvaluated) {
		this.contentEvaluated = contentEvaluated;
	}
	public Integer getNewCourse() {
		return newCourse;
	}
	public void setNewCourse(Integer newCourse) {
		this.newCourse = newCourse;
	}
	public Integer getEnrolled() {
		return enrolled;
	}
	public void setEnrolled(Integer enrolled) {
		this.enrolled = enrolled;
	}
	public Integer getCourseEnded() {
		return courseEnded;
	}
	public void setCourseEnded(Integer courseEnded) {
		this.courseEnded = courseEnded;
	}
	public Set<Long> getCourseEndedStudents() {
		return courseEndedStudents;
	}
	public void setCourseEndedStudents(Set<Long> courseEndedStudents) {
		this.courseEndedStudents = courseEndedStudents;
	}

}
