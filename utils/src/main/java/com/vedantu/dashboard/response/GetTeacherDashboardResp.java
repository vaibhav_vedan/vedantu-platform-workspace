package com.vedantu.dashboard.response;

import com.vedantu.User.TeacherCurrentStatus;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dashboard.pojo.TeacherDashboardByEntityInfo;

public class GetTeacherDashboardResp {

	private Long teacherId;
	private TeacherCurrentStatus teacherStatus;
	private Long startTime;
	private Long primarySubject;
	private TeacherDashboardByEntityInfo coursePlan;
	private TeacherDashboardByEntityInfo otf;
	private UserBasicInfo teacher;
	private Integer coursePlanActiveStudents=0;
	private Integer otfActiveStudents=0;
	private Integer coursePlanChurn=0;
	private Integer otfChurn=0;
	
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public TeacherCurrentStatus getTeacherStatus() {
		return teacherStatus;
	}
	public void setTeacherStatus(TeacherCurrentStatus teacherStatus) {
		this.teacherStatus = teacherStatus;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getPrimarySubject() {
		return primarySubject;
	}
	public void setPrimarySubject(Long primarySubject) {
		this.primarySubject = primarySubject;
	}
	public TeacherDashboardByEntityInfo getCoursePlan() {
		return coursePlan;
	}
	public void setCoursePlan(TeacherDashboardByEntityInfo coursePlan) {
		this.coursePlan = coursePlan;
	}
	public TeacherDashboardByEntityInfo getOtf() {
		return otf;
	}
	public void setOtf(TeacherDashboardByEntityInfo otf) {
		this.otf = otf;
	}
	public UserBasicInfo getTeacher() {
		return teacher;
	}
	public void setTeacher(UserBasicInfo teacher) {
		this.teacher = teacher;
	}
	public Integer getCoursePlanActiveStudents() {
		return coursePlanActiveStudents;
	}
	public void setCoursePlanActiveStudents(Integer coursePlanActiveStudents) {
		this.coursePlanActiveStudents = coursePlanActiveStudents;
	}
	public Integer getOtfActiveStudents() {
		return otfActiveStudents;
	}
	public void setOtfActiveStudents(Integer otfActiveStudents) {
		this.otfActiveStudents = otfActiveStudents;
	}
	public Integer getCoursePlanChurn() {
		return coursePlanChurn;
	}
	public void setCoursePlanChurn(Integer coursePlanChurn) {
		this.coursePlanChurn = coursePlanChurn;
	}
	public Integer getOtfChurn() {
		return otfChurn;
	}
	public void setOtfChurn(Integer otfChurn) {
		this.otfChurn = otfChurn;
	}

}
