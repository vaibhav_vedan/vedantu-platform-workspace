package com.vedantu.dashboard.response;

import java.util.List;

import com.vedantu.User.TeacherCurrentStatus;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetTeacherDashboardReq extends AbstractFrontEndListReq{
	
	private List<Long> teacherIds;
	private TeacherCurrentStatus teacherStatus;
	private EntityType entityType;
	private Long startTime;
	
	public List<Long> getTeacherIds() {
		return teacherIds;
	}
	public void setTeacherIds(List<Long> teacherIds) {
		this.teacherIds = teacherIds;
	}
	public TeacherCurrentStatus getTeacherStatus() {
		return teacherStatus;
	}
	public void setTeacherStatus(TeacherCurrentStatus teacherStatus) {
		this.teacherStatus = teacherStatus;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}	

}
