/**
 * 
 */
package com.vedantu.app.pojos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OTFSessionPojoForApp {
	
	private String title;
	
	private Long startTime;
	
	private Long endTime;
	
	private String teacherName;
	
	private String joinSessionUrl;
	
	private String replaySessionUrl;
	
	private String subject;

}
