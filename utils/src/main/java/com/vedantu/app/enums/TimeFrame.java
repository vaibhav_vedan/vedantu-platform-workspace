/**
 * 
 */
package com.vedantu.app.enums;

/**
 * @author subarna
 *
 */
public enum TimeFrame {

	LIVE, UPCOMING, PAST
}
