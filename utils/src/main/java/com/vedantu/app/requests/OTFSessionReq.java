/**
 * 
 */
package com.vedantu.app.requests;

import java.util.List;

import com.vedantu.User.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OTFSessionReq {
	
	private String userId;
	
	private Role userRole;
	
	private List<String> batchIds;
	
	private Long startTime;
	
	private Long endTime;
	
	private Integer start;
	
	private Integer size;
	
	private Integer year;
	
	private Integer month;

}
