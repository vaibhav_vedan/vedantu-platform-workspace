/**
 * 
 */
package com.vedantu.app.responses;

import java.util.List;

import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.SessionPlanPojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BatchDetailsResponse {
	
	private String pageTitle;
	
	private String courseDuration;
	
	private String courseDescription;
	
	private Long batchStartTime;
	
	private Long batchEndTime;
	
	private List<String> batchIds;
	
	@Builder.Default
	private Boolean isEnrolled = false;
	
	private EntityStatus enrollmentStatus;
	
	private EnrollmentState state;
	
	private Long enrollmentEndTime;
	
	private List<TeacherBasicInfo> teacherBasicInfo;
	
	private List<SessionPlanPojo> sessionPlan;
	
	private String bundleImageUrl;
	
	private String bundleId;
	
	private String premiumSubscriptionId;
	
	private Long bundleValidTill;


}
