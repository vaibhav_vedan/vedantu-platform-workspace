/**
 * 
 */
package com.vedantu.app.responses;

import java.util.List;

import com.vedantu.app.pojos.OTFSessionPojoForApp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WeeklySessionDetails {
	
	private String sectionTitle;
	
	private Long startTime;
	
	private Long endTime;
	
	private List<OTFSessionPojoForApp> sessions;

}
