package com.vedantu.notification.enums;

public enum EmailPriorityType {
    HIGH, LOW, NONE
}
