/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.enums;

/**
 *
 * @author somil
 */
public enum NotificationSubType {
	LMS_TEST_SHARED,
	LMS_NOTES_SHARED,
	LMS_ASSIGNMENT_SHARED,
	LMS_TEST_REMINDER,
	LMS_ASSIGNMENT_REMINDER,
	LMS_TEST_REPORT,
	LMS_ASSIGNMENT_REPORT,
	LMS_TEST_ATTEMPTED,
	LMS_ASSIGNMENT_ATTEMPTED,
	LMS_TEST_EVAULATION_REMINDER,
	LMS_ASSIGNMENT_EVAULATION_REMINDER,
	WEBINAR;
	
}
