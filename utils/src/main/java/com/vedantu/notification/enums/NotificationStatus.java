package com.vedantu.notification.enums;

public enum NotificationStatus {
    EXPIRED, ACTIVE, SCHEDULED, SENT, DISCARDED, FAILED
}
