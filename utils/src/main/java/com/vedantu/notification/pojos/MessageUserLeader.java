/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojos;

import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;

/**
 *
 * @author jeet
 */
public class MessageUserLeader {
    private UserBasicInfo teacher;
    private Long wishesCount;
    private Long teacherId;

    public MessageUserLeader() {
    }

    public MessageUserLeader(UserBasicInfo teacher, Long wishesCount) {
        this.teacher = teacher;
        this.wishesCount = wishesCount;
    }
    public MessageUserLeader(User teacher, Long wishesCount) {
        this.teacher = new UserBasicInfo(teacher, false);
        this.wishesCount = wishesCount;
    }
    
    public MessageUserLeader(Long teacherId, Long wishesCount) {
        this.teacherId = teacherId;
        this.wishesCount = wishesCount;
    }

    public UserBasicInfo getTeacher() {
        return teacher;
    }

    public void setTeacher(UserBasicInfo teacher) {
        this.teacher = teacher;
    }

    public Long getWishesCount() {
        return wishesCount;
    }

    public void setWishesCount(Long wishesCount) {
        this.wishesCount = wishesCount;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "MessageUserLeader{" + "teacher=" + teacher + ", wishesCount=" + wishesCount + ", teacherId=" + teacherId + '}';
    }
    
}

