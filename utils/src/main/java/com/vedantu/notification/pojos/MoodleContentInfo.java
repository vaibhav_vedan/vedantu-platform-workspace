
package com.vedantu.notification.pojos;

import com.vedantu.lms.cmds.enums.UserType;
import com.vedantu.lms.cmds.pojo.TestContentInfoMetadataPojo;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.session.pojo.EntityType;

import java.util.List;

public class MoodleContentInfo implements INotificationInfo {


	private UserType userType;
	private EntityType contextType;
	private String contextId;
	private TestContentInfoMetadataPojo metadata;


	//TODO: Following fields are added for backward compatibility of moodle and youscore, Do not use it in future
	private String testId;
	private Integer noOfQuestions;
	private String attemptId; // Latest attempt id incase of multiple active


	private String id; // module data id
	private String studentId; // id
	private String studentName;
	private String studentFullName; // first last
	private String teacherId;
	private String teacherName;
	private String teacherFullName;
	private String contentTitle; // name from assign
	private String courseName; // course full???
	private String subject; // shirt name in course
	private String courseId; // course data id
	private String coursePath;
	private Long duration; // quiz time limit
	private Long expiryTime; // due data assign time close in quiz
	private Long attemptedTime;
	private Long evaulatedTime;

	private String contentType; // assign data or quiz
								// data
	private String contentInfoType;// neglect
	private EngagementType engagementType = EngagementType.OTO;// short name

	private String contentState;// def shared

	private String contentLink; // http://vedantu.skoolbox.in/mod/quiz/view.php?id=78(test
								// id)
	private String studentActionLink;
	private String teacherActionLink;
	private String referenceLink;// chuckk it

	private Float totalMarks = new Float(1); // grade assign --- raw maximum
	private Float marksAcheived;// grade assign_grade -- raw grade
	private List<String> tags; // comma delimeted values
	private Long creationTime;

	private String lmsType;

	public MoodleContentInfo() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentFullName() {
		return studentFullName;
	}

	public void setStudentFullName(String studentFullName) {
		this.studentFullName = studentFullName;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherFullName() {
		return teacherFullName;
	}

	public void setTeacherFullName(String teacherFullName) {
		this.teacherFullName = teacherFullName;
	}

	public String getContentTitle() {
		return contentTitle;
	}

	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCoursePath() {
		return coursePath;
	}

	public void setCoursePath(String coursePath) {
		this.coursePath = coursePath;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentInfoType() {
		return contentInfoType;
	}

	public void setContentInfoType(String contentInfoType) {
		this.contentInfoType = contentInfoType;
	}

	public EngagementType getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}

	public String getContentState() {
		return contentState;
	}

	public void setContentState(String contentState) {
		this.contentState = contentState;
	}

	public String getContentLink() {
		return contentLink;
	}

	public void setContentLink(String contentLink) {
		this.contentLink = contentLink;
	}

	public String getStudentActionLink() {
		return studentActionLink;
	}

	public void setStudentActionLink(String studentActionLink) {
		this.studentActionLink = studentActionLink;
	}

	public String getTeacherActionLink() {
		return teacherActionLink;
	}

	public void setTeacherActionLink(String teacherActionLink) {
		this.teacherActionLink = teacherActionLink;
	}

	public String getReferenceLink() {
		return referenceLink;
	}

	public void setReferenceLink(String referenceLink) {
		this.referenceLink = referenceLink;
	}

	public Float getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(Float totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Float getMarksAcheived() {
		return marksAcheived;
	}

	public void setMarksAcheived(Float marksAcheived) {
		this.marksAcheived = marksAcheived;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public Long getAttemptedTime() {
		return attemptedTime;
	}

	public void setAttemptedTime(Long attemptedTime) {
		this.attemptedTime = attemptedTime;
	}

	public Long getEvaulatedTime() {
		return evaulatedTime;
	}

	public void setEvaulatedTime(Long evaulatedTime) {
		this.evaulatedTime = evaulatedTime;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public void setAdditionalFields() {
		this.studentName = this.studentFullName;
		this.teacherName = this.teacherFullName;
	}

	public String getLmsType() {
		return lmsType;
	}

	public void setLmsType(String lmsType) {
		this.lmsType = lmsType;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public TestContentInfoMetadataPojo getMetadata() {
		return metadata;
	}

	public void setMetadata(TestContentInfoMetadataPojo metadata) {
		this.metadata = metadata;
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public Integer getNoOfQuestions() {
		return noOfQuestions;
	}

	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public String getAttemptId() {
		return attemptId;
	}

	public void setAttemptId(String attemptId) {
		this.attemptId = attemptId;
	}

	@Override
	public String toString() {
		return "MoodleContentInfo{" +
				"userType=" + userType +
				", contextType=" + contextType +
				", contextId='" + contextId + '\'' +
				", metadata=" + metadata +
				", testId='" + testId + '\'' +
				", noOfQuestions=" + noOfQuestions +
				", attemptId='" + attemptId + '\'' +
				", id='" + id + '\'' +
				", studentId='" + studentId + '\'' +
				", studentName='" + studentName + '\'' +
				", studentFullName='" + studentFullName + '\'' +
				", teacherId='" + teacherId + '\'' +
				", teacherName='" + teacherName + '\'' +
				", teacherFullName='" + teacherFullName + '\'' +
				", contentTitle='" + contentTitle + '\'' +
				", courseName='" + courseName + '\'' +
				", subject='" + subject + '\'' +
				", courseId='" + courseId + '\'' +
				", coursePath='" + coursePath + '\'' +
				", duration=" + duration +
				", expiryTime=" + expiryTime +
				", attemptedTime=" + attemptedTime +
				", evaulatedTime=" + evaulatedTime +
				", contentType='" + contentType + '\'' +
				", contentInfoType='" + contentInfoType + '\'' +
				", engagementType=" + engagementType +
				", contentState='" + contentState + '\'' +
				", contentLink='" + contentLink + '\'' +
				", studentActionLink='" + studentActionLink + '\'' +
				", teacherActionLink='" + teacherActionLink + '\'' +
				", referenceLink='" + referenceLink + '\'' +
				", totalMarks=" + totalMarks +
				", marksAcheived=" + marksAcheived +
				", tags=" + tags +
				", creationTime=" + creationTime +
				", lmsType='" + lmsType + '\'' +
				'}';
	}
}
