/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojos;

/**
 *
 * @author somil
 */
public class MoodleNotificationData implements INotificationInfo {

	private MoodleContentInfo contentInfo;
	private String state;

	public MoodleNotificationData() {
		super();
	}

	public MoodleNotificationData(MoodleContentInfo contentInfo, String state) {
		super();
		this.contentInfo = contentInfo;
		this.state = state;
	}

	public MoodleContentInfo getContentInfo() {
		return contentInfo;
	}

	public void setContentInfo(MoodleContentInfo contentInfo) {
		this.contentInfo = contentInfo;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "MoodleNotificationData [contentInfo=" + contentInfo + ", state=" + state + "]";
	}
	
	

}
