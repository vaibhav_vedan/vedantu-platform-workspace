/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojos;

import com.vedantu.User.AbstractEntityRes;
import java.util.List;

/**
 *
 * @author jeet
 */
public class MessageUserRes extends AbstractEntityRes {

    private Long userId;

    private Long toUserId;

    private String contextType;

    private String message;

    private List<String> replies;

    private boolean sendSMS;

    public MessageUserRes() {
        super();
    }

    public MessageUserRes(Long userId, Long toUserId, String contextType, String message) {
        super();
        this.userId = userId;
        this.toUserId = toUserId;
        this.contextType = contextType;
        this.message = message;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageString() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContextType() {
        return contextType;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public List<String> getReplies() {
        return replies;
    }

    public void setReplies(List<String> replies) {
        this.replies = replies;
    }

    public boolean isSendSMS() {
        return sendSMS;
    }

    public void setSendSMS(boolean sendSMS) {
        this.sendSMS = sendSMS;
    }

    @Override
    public String toString() {
        return "MessageUserRes{" + "userId=" + userId + ", toUserId=" + toUserId + ", contextType=" + contextType + ", message=" + message + ", replies=" + replies + ", sendSMS=" + sendSMS + '}';
    }

}
