/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojos;

import com.vedantu.notification.enums.CommunicationType;

/**
 *
 * @author somil
 */
public class InstalmentNotificationReq implements INotificationInfo {

    private CommunicationType type;
    private String subscriptionLink;
    private String subscriptionId;//courseplanid or subscriptionid
    private String subscriptionTitle;
    private String dueDate;
    private String teacherName;
    private String amount;
    private String security;
    private String studentName;

    public InstalmentNotificationReq() {
    }

    public InstalmentNotificationReq(CommunicationType type,String subscriptionId, String subscriptionLink, 
            String subscriptionTitle, String dueDate, String teacherName, 
            String amount, String security,String studentName) {
        this.type = type;
        this.subscriptionId=subscriptionId;
        this.subscriptionLink = subscriptionLink;
        this.subscriptionTitle = subscriptionTitle;
        this.dueDate = dueDate;
        this.teacherName = teacherName;
        this.amount = amount;
        this.security = security;
        this.studentName=studentName;
    }

    public CommunicationType getType() {
        return type;
    }

    public void setType(CommunicationType type) {
        this.type = type;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }
    
    public String getSubscriptionLink() {
        return subscriptionLink;
    }

    public void setSubscriptionLink(String subscriptionLink) {
        this.subscriptionLink = subscriptionLink;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    @Override
    public String toString() {
        return "InstalmentNotificationReq{" + "type=" + type + ", subscriptionLink=" + subscriptionLink + ", subscriptionId=" + subscriptionId + ", subscriptionTitle=" + subscriptionTitle + ", dueDate=" + dueDate + ", teacherName=" + teacherName + ", amount=" + amount + ", security=" + security + ", studentName=" + studentName + '}';
    }

}
