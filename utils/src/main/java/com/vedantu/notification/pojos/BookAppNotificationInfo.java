package com.vedantu.notification.pojos;

import com.vedantu.cmds.pojo.CMDSImageDetails;

public class BookAppNotificationInfo {

	private Long startTime;
	private String grade;
	private String topic;
	private String title;
	private String redirectLink;
	private String imageLink;
	private String description;
	private String teacherName;
	private Long expiryTime;
	private CMDSImageDetails imageDetails;

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRedirectLink() {
		return redirectLink;
	}

	public void setRedirectLink(String redirectLink) {
		this.redirectLink = redirectLink;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public CMDSImageDetails getImageDetails() {
		return imageDetails;
	}

	public void setImageDetails(CMDSImageDetails imageDetails) {
		this.imageDetails = imageDetails;
	}
}
