/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.notification.enums.NotificationType;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class MarkSeenReq extends AbstractFrontEndUserReq {

    //TODO: changed from Long to String, check consequences
    private String notificationId;
    private Long entityId;
    private NotificationType notificationType;

    public String getNotificationId() {

        return notificationId;
    }

    public void setNotificationId(String notificationId) {

        this.notificationId = notificationId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == getUserId()) {
            errors.add(AbstractFrontEndUserReq.Constants.USER_ID);
        }
        return errors;
    }
}
