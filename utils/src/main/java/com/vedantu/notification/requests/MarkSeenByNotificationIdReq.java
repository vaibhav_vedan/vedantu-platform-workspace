package com.vedantu.notification.requests;

import java.util.List;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class MarkSeenByNotificationIdReq extends AbstractFrontEndReq {

	private String notificationId;
	private String deviceId;
	private String regToken;

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	
	public String getRegToken() {
		return regToken;
	}

	public void setRegToken(String regToken) {
		this.regToken = regToken;
	}

	@Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(notificationId)) {
            errors.add("notificationId");
        }
        if (StringUtils.isEmpty(deviceId)) {
            errors.add("deviceId");
        }
        if (StringUtils.isEmpty(regToken)) {
            errors.add("regToken");
        }
        return errors;
    }
}
