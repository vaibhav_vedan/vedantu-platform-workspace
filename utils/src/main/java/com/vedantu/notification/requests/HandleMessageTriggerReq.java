package com.vedantu.notification.requests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.vedantu.User.User;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author jeet
 */
public class HandleMessageTriggerReq extends AbstractFrontEndReq {

    private User fromuser;
    private User toUser;
    private MessageUserRes messageUser;

    public HandleMessageTriggerReq() {
        super();
    }

    public User getFromuser() {
        return fromuser;
    }

    public void setFromuser(User fromuser) {
        this.fromuser = fromuser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public MessageUserRes getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(MessageUserRes messageUser) {
        this.messageUser = messageUser;
    }

}
