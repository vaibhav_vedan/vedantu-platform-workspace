/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import java.util.Map;

/**
 *
 * @author somil
 */
public class TextSMSRequest extends AbstractFrontEndReq {

    private String to;
    private String phoneCode;
    private String body;
    private CommunicationType type;
    private Map<String, Object> scopeParams;
    private Role role;
    private SMSPriorityType priorityType = SMSPriorityType.NONE;
    private String sqsMessageId;
//    private Integer preferredSMSType = 0;

    private final long createdAt = System.currentTimeMillis();
    private long expectedDeliveryMillis;
    private long maxDelayMillis = DateTimeUtils.MILLIS_PER_MINUTE * 15;

    public TextSMSRequest(String to, String phoneCode, Map<String, Object> scopeParams,
            CommunicationType type, Role role) {
        this.to = to;
        this.phoneCode = phoneCode;
        this.scopeParams = scopeParams;
        this.type = type;
        this.role = role;
    }

//    public TextSMSRequest(String to, String phoneCode, Map<String, Object> scopeParams,
//                          CommunicationType type, Role role, Integer preferredSMSType) {
//        this.to = to;
//        this.phoneCode = phoneCode;
//        this.scopeParams = scopeParams;
//        this.type = type;
//        this.role = role;
//        this.preferredSMSType = preferredSMSType;
//    }

    public TextSMSRequest() {
        super();
    }

    public TextSMSRequest(String to, String phoneCode, String body, CommunicationType type) {
        super();
        this.to = to;
        this.phoneCode = phoneCode;
        this.body = body;
        this.type = type;
    }

//    public Integer getPreferredSMSType() {
//        return preferredSMSType;
//    }
//
//    public void setPreferredSMSType(Integer preferredSMSType) {
//        this.preferredSMSType = preferredSMSType;
//    }

    public TextSMSRequest(TextSMSRequest textSMSRequest){
        this.body=textSMSRequest.getBody();
        this.type=textSMSRequest.getType();
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getExpectedDeliveryMillis() {
        return expectedDeliveryMillis;
    }

    public void setExpectedDeliveryMillis(long expectedDeliveryMillis) {
        this.expectedDeliveryMillis = expectedDeliveryMillis;
    }

    public long getMaxDelayMillis() {
        return maxDelayMillis;
    }

    public void setMaxDelayMillis(long maxDelayMillis) {
        this.maxDelayMillis = maxDelayMillis;
    }

    public String getSqsMessageId() {
        return sqsMessageId;
    }

    public void setSqsMessageId(String sqsMessageId) {
        this.sqsMessageId = sqsMessageId;
    }

    public Map<String, Object> getScopeParams() {
        return scopeParams;
    }

    public void setScopeParams(Map<String, Object> scopeParams) {
        this.scopeParams = scopeParams;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public CommunicationType getType() {
        return type;
    }

    public void setType(CommunicationType type) {
        this.type = type;
    }

    public SMSPriorityType getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(SMSPriorityType priorityType) {
        this.priorityType = priorityType;
    }

    public static class Constants extends AbstractFrontEndReq.Constants {

        public static final String TO = "to";
        public static final String BODY = "body";

    }
    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == to) {
            errors.add(TextSMSRequest.Constants.TO);
        }

        //if (null == body) {
        //	errors.add(TextSMSRequest.Constants.BODY);
        //}
        return errors;
    }
}
