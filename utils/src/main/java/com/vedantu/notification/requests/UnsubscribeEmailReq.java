package com.vedantu.notification.requests;

import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 * Created by somil on 20/07/17.
 */
public class UnsubscribeEmailReq extends AbstractFrontEndReq {
    private String email;
    private EntityState state;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }


    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(email)) {
            errors.add("email");
        }
        if(state == null) {
            errors.add("state");
        }

        //if (null == body) {
        //	errors.add(TextSMSRequest.Constants.BODY);
        //}
        return errors;
    }


    @Override
    public String toString() {
        return "UnsubscribeEmailReq{" +
                "email='" + email + '\'' +
                ", state=" + state +
                "} " + super.toString();
    }
}
