/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author ajith
 */
public class GetUnsubscribedListReq extends AbstractFrontEndListReq {

    private String email;
    private EntityState state;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

}
