package com.vedantu.notification.requests;

import com.vedantu.User.User;

public class VSATRegistrationEmailReq {

    User user;

    String key;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
