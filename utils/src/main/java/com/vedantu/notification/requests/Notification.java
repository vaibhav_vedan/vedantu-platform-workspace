/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.notification.enums.NotificationSubType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

/**
 *
 * @author somil
 */
public class Notification extends AbstractMongoStringIdEntityBean {

    private Long userId;

    private NotificationType type;

    private String header;

    private String body;

    private String footer;

    private String description;

    private Boolean isSeen;

    private Long seenAt;

    private String entityType;

    private Long entityId;

    //Was here before, should be replaced with String
    private String info;

    private Boolean hidden;

    private NotificationSubType subType;

    public Notification() {
        super();
    }

    public NotificationSubType getSubType() {
        return subType;
    }

    public void setSubType(NotificationSubType subType) {
        this.subType = subType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public NotificationType getType() {

        return type;
    }

    public void setType(NotificationType type) {

        this.type = type;
    }

    public String getHeader() {

        return header;
    }

    public void setHeader(String header) {

        this.header = header;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public String getFooter() {

        return footer;
    }

    public void setFooter(String footer) {

        this.footer = footer;
    }

    public Boolean getIsSeen() {

        return isSeen;
    }

    public void setIsSeen(Boolean isSeen) {

        this.isSeen = isSeen;
    }

    public Long getSeenAt() {

        return seenAt;
    }

    public void setSeenAt(Long seenAt) {

        this.seenAt = seenAt;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
}
