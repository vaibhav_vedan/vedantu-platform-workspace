package com.vedantu.notification.requests;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class RegisterUserDeviceRequest extends AbstractFrontEndReq {

    private String regId;
    private String deviceId;
    private Long userId;

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static class Constants extends AbstractMongoEntityBean.Constants {

        public static final String REGID = "regId";
        public static final String DEVICEID = "deviceId";
        public static final String USERID = "userId";
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == regId) {
            errors.add(RegisterUserDeviceRequest.Constants.REGID);
        }

        if (null == deviceId) {
            errors.add(RegisterUserDeviceRequest.Constants.DEVICEID);
        }

        if (null == userId) {
            errors.add(RegisterUserDeviceRequest.Constants.USERID);
        }

        return errors;
    }

}
