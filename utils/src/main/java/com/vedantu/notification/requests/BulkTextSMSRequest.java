/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.User.AbstractEntityRes;
import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author somil
 */
/*
 * Incoming request object for BulkTextSMS API
 */
public class BulkTextSMSRequest extends AbstractFrontEndReq {

    //TODO: Add phone code support here
    private ArrayList<String> to;
    private String body;
    private CommunicationType type;

    private Map<String, Object> scopeParams;
    private Role role;
    private SMSPriorityType priorityType = SMSPriorityType.NONE;
    private String manualNotificationId;


    private final long createdAt = System.currentTimeMillis();
    private long expectedDeliveryMillis;
    private long maxDelayMillis = DateTimeUtils.MILLIS_PER_MINUTE * 15;


    public BulkTextSMSRequest(ArrayList<String> to, Map<String, Object> scopeParams,
            CommunicationType type, Role role) {
        this.to = to;
        this.scopeParams = scopeParams;
        this.type = type;
        this.role = role;
    }

    public BulkTextSMSRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

    public BulkTextSMSRequest(ArrayList<String> to, String body) {
        super();
        this.to = to;
        this.body = body;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getExpectedDeliveryMillis() {
        return expectedDeliveryMillis;
    }

    public void setExpectedDeliveryMillis(long expectedDeliveryMillis) {
        this.expectedDeliveryMillis = expectedDeliveryMillis;
    }

    public long getMaxDelayMillis() {
        return maxDelayMillis;
    }

    public void setMaxDelayMillis(long maxDelayMillis) {
        this.maxDelayMillis = maxDelayMillis;
    }

    public Map<String, Object> getScopeParams() {
        return scopeParams;
    }

    public void setScopeParams(Map<String, Object> scopeParams) {
        this.scopeParams = scopeParams;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public ArrayList<String> getTo() {
        return to;
    }

    public void setTo(ArrayList<String> to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public CommunicationType getType() {
        return type;
    }

    public void setType(CommunicationType type) {
        this.type = type;
    }

    public SMSPriorityType getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(SMSPriorityType priorityType) {
        this.priorityType = priorityType;
    }

    public String getManualNotificationId() {
        return manualNotificationId;
    }

    public void setManualNotificationId(String manualNotificationId) {
        this.manualNotificationId = manualNotificationId;
    }

    @Override
    public String toString() {
        return "BulkTextSMSRequest{" +
                "to=" + to +
                ", body='" + body + '\'' +
                ", type=" + type +
                ", scopeParams=" + scopeParams +
                ", role=" + role +
                '}';
    }

    public static class Constants extends AbstractEntityRes.Constants {

        public static final String TO = "to";
        public static final String BODY = "body";
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == to) {
            errors.add(BulkTextSMSRequest.Constants.TO);
        }

        //if (null == body) {
        //	errors.add(BulkTextSMSRequest.Constants.BODY);
        //}
        if (to.size() > 100) {
            throw new IllegalArgumentException("Can only send a Bulk Message to a maximum of 100 numbers. Please reduce the size of the input");
        }

        return errors;
    }

}
