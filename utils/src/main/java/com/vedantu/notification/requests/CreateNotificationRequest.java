/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.User.AbstractEntityRes;
import com.vedantu.User.Role;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author somil
 */
public class CreateNotificationRequest extends AbstractFrontEndReq {
    Long userId;
    NotificationType notificationType;
    String notificationObj;//this a json str
    Role role;

    public CreateNotificationRequest(Long userId, NotificationType notificationType, String notificationObj, Role role) {
        this.userId = userId;
        this.notificationType = notificationType;
        this.notificationObj = notificationObj;
        this.role = role;
    }

    public CreateNotificationRequest() {
    }


    
    
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationObj() {
        return notificationObj;
    }

    public void setNotificationObj(String notificationObj) {
        this.notificationObj = notificationObj;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    
    @Override
	public String toString() {
		return "SendNotificationRequest [userId=" + userId + ", type=" + notificationType + "]";
	}

	public static class Constants extends AbstractEntityRes.Constants {
		public static final String USER_ID = "userId";
                public static final String NOTIFICATION_TYPE = "notificationType";

	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == userId) {
			errors.add(Constants.USER_ID);
		}

		if (null == notificationType) {
			errors.add(Constants.NOTIFICATION_TYPE);
		}

		return errors;
	}
}
