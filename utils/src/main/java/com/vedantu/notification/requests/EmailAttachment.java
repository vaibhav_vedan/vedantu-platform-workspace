/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;


/**
 *
 * @author somil
 */
public class EmailAttachment {

    private String fileName;
    private Object attachmentData;
    private String application;

    public EmailAttachment() {
    }

    public EmailAttachment(String fileName, Object attachmentData, String application) {
        this.fileName = fileName;
        this.attachmentData = attachmentData;
        this.application = application;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Object getAttachmentData() {
        return attachmentData;
    }

    public void setAttachmentData(Object attachmentData) {
        this.attachmentData = attachmentData;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return "EmailAttachment  fileName:" + fileName + ", attachmentData:"
                + attachmentData + ", application:" + application;
    }
}
