package com.vedantu.notification.requests;

import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.enums.CommunicationKind;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManualNotificationRequest extends AbstractFrontEndReq {

    private String id;
    private CommunicationKind communicationKind;
    private String subject;
    private String body;
    private long scheduledTime;
    private long expiryTime;

    private List<String> userIds;
    private List<String> batchIds;
    private List<String> otmBundleIds;
    private List<String> aioPackageIds;
    private List<String> emailIds;

    private List<String> allUserIds;

    private NotificationStatus status;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (communicationKind == null) {
            errors.add("You need to specify the kind of communication");
        }

        if (CommunicationKind.EMAIL.equals(communicationKind) && subject == null) {
            errors.add("Subject can't be null for email");
        }

        if (body == null) {
            errors.add("Message body couldn't be null");
        }

        if (userIds == null && batchIds == null && otmBundleIds == null && aioPackageIds == null && emailIds == null) {
            errors.add("You can't send message to no one. Please specify some valid list...");
        }

        return errors;
    }
}
