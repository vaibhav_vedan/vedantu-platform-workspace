/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetNotificationsReq extends AbstractFrontEndListReq {

	private Long userId;

	public GetNotificationsReq() {
		super();
	}

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if (null == userId) {
			errors.add("userId");
		}
		return errors;
	}
}

