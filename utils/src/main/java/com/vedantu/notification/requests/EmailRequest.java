/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailPriorityType;
import com.vedantu.notification.enums.EmailService;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import javax.mail.internet.InternetAddress;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author somil
 */
public class EmailRequest extends AbstractFrontEndReq {

    private List<InternetAddress> to;
    private InternetAddress from;
    private List<InternetAddress> bcc;
    private List<InternetAddress> cc;
    private List<InternetAddress> replyTo;
    private String subject;
    private String body;
    private EmailAttachment attachment;
    private String receiverName;
    private CommunicationType type;
    private Map<String, Object> subjectScopes;
    private Map<String, Object> bodyScopes;
    private Role role;
    private Boolean includeHeaderFooter = Boolean.TRUE;
    private boolean clickTrackersEnabled;
    private Set<String> tags;
    private EmailService emailService = EmailService.AMAZON_SES;
    private List<EmailAttachment> extraAttachments;
    private String manualNotificationId;
    private String key;
    private String bucket;
    private String sqsMessageId;
    private final long createdAt = System.currentTimeMillis();
    private long expectedDeliveryMillis;
    private long maxDelayMillis = DateTimeUtils.MILLIS_PER_MINUTE * 15;
    private EmailPriorityType priorityType;
    private Boolean includeUnsubscribeLink = Boolean.FALSE;
    private boolean isEarlyLearning = Boolean.FALSE;

    public EmailRequest() {
        super();
    }

    public EmailRequest(List<InternetAddress> to, Map<String, Object> subjectScopes, Map<String, Object> bodyScopes,
                        CommunicationType type, Role role) {
        this.to = to;
        this.subjectScopes = subjectScopes;
        this.bodyScopes = bodyScopes;
        this.type = type;
        this.role = role;
    }

    // Only use it in case of manual notification
    public EmailRequest(EmailRequest emailRequest) {
        this.body = emailRequest.getBody();
        this.subject = emailRequest.getSubject();
        this.type = emailRequest.getType();
        this.clickTrackersEnabled = emailRequest.isClickTrackersEnabled();
        this.manualNotificationId = emailRequest.getManualNotificationId();
        this.bodyScopes = emailRequest.getBodyScopes();
    }

    public Boolean isEarlyLearning() {return isEarlyLearning; }

    public void setIsEarlyLearning(boolean isEarlyLearning) {this.isEarlyLearning = isEarlyLearning; }

    public Boolean getIncludeUnsubscribeLink() {
        return includeUnsubscribeLink;
    }

    public void setIncludeUnsubscribeLink(Boolean includeUnsubscribeLink) {
        this.includeUnsubscribeLink = includeUnsubscribeLink;
    }

    public EmailPriorityType getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(EmailPriorityType priorityType) {
        this.priorityType = priorityType;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getExpectedDeliveryMillis() {
        return expectedDeliveryMillis;
    }

    public void setExpectedDeliveryMillis(long expectedDeliveryMillis) {
        this.expectedDeliveryMillis = expectedDeliveryMillis;
    }

    public long getMaxDelayMillis() {
        return maxDelayMillis;
    }

    public void setMaxDelayMillis(long maxDelayMillis) {
        this.maxDelayMillis = maxDelayMillis;
    }

    public String getSqsMessageId() {
        return sqsMessageId;
    }

    public void setSqsMessageId(String sqsMessageId) {
        this.sqsMessageId = sqsMessageId;
    }

    public CommunicationType getType() {
        return type;
    }


    public void setType(CommunicationType type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getReceiverName() {

        return receiverName;
    }

    public void setReceiverName(String receiverName) {

        this.receiverName = receiverName;
    }

    public List<InternetAddress> getTo() {

        return to;
    }

    public void setTo(List<InternetAddress> to) {

        this.to = to;
    }

    public List<InternetAddress> getBcc() {

        return bcc;
    }

    public void setBcc(List<InternetAddress> bcc) {

        this.bcc = bcc;
    }

    public List<InternetAddress> getCc() {

        return cc;
    }

    public void setCc(List<InternetAddress> cc) {

        this.cc = cc;
    }

    public String getSubject() {

        return subject;
    }

    public void setSubject(String subject) {

        this.subject = subject;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public List<InternetAddress> getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(List<InternetAddress> replyTo) {
        this.replyTo = replyTo;
    }

    public EmailAttachment getAttachment() {
        return attachment;
    }

    public void setAttachment(EmailAttachment attachment) {
        this.attachment = attachment;
    }

    public Map<String, Object> getSubjectScopes() {
        return subjectScopes;
    }

    public void setSubjectScopes(Map<String, Object> subjectScopes) {
        this.subjectScopes = subjectScopes;
    }

    public Map<String, Object> getBodyScopes() {
        return bodyScopes;
    }

    public void setBodyScopes(Map<String, Object> bodyScopes) {
        this.bodyScopes = bodyScopes;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getIncludeHeaderFooter() {
        return includeHeaderFooter;
    }

    public void setIncludeHeaderFooter(Boolean includeHeaderFooter) {
        this.includeHeaderFooter = includeHeaderFooter;
    }

    public boolean isClickTrackersEnabled() {
        return clickTrackersEnabled;
    }

    public void setClickTrackersEnabled(boolean clickTrackersEnabled) {
        this.clickTrackersEnabled = clickTrackersEnabled;
    }

    public Set<String> getTags() {
        if (tags == null) {
            return new HashSet<>();
        }
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public String getManualNotificationId() {
        return manualNotificationId;
    }

    public void setManualNotificationId(String manualNotificationId) {
        this.manualNotificationId = manualNotificationId;
    }

    public static class Constants {

        public static final String TO = "to";
        public static final String BODY = "body";
        public static final String SUBJECT = "subject";
        public static final String TYPE = "type";
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        int ccSize = 0, bccSize = 0;

        if (null == to || to.size() == 0) {
            errors.add(EmailRequest.Constants.TO);
        }

        if (cc != null) {
            ccSize = cc.size();
        }
        if (bcc != null) {
            bccSize = bcc.size();
        }

        if (to.size() + ccSize + bccSize > 50 && !CommunicationType.MANUAL_NOTIFICATION.equals(type)) {
            throw new IllegalArgumentException("Can only send a Bulk Email to a maximum of 50 emails (to + cc + bcc). Please reduce the size of the input");
        }

        //if (null == body) {
        //	errors.add(EmailRequest.Constants.BODY);
        //}
        //if (null == subject) {
        //	errors.add(EmailRequest.Constants.SUBJECT);
        //}
        if (null == type) {
            errors.add(EmailRequest.Constants.TYPE);
        }

        return errors;
    }


    public List<EmailAttachment> getExtraAttachments() {
        return extraAttachments;
    }

    public void setExtraAttachments(List<EmailAttachment> extraAttachments) {
        this.extraAttachments = extraAttachments;
    }

    public InternetAddress getFrom() {
        return from;
    }

    public void setFrom(InternetAddress from) {
        this.from = from;
    }
}
