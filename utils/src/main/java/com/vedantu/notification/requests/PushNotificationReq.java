/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.notification.enums.NotificationSubType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojos.BookAppNotificationInfo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class PushNotificationReq extends AbstractFrontEndReq {

    private NotificationType type;
    private String header;
    private String body;
    private String footer;
    private String description;
    private String info;
    private NotificationSubType subType;
    private String channel;
    private String sendTo;
    private String entityType;
    private String app;
    private BookAppNotificationInfo appInfo;

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public NotificationSubType getSubType() {
        return subType;
    }

    public void setSubType(NotificationSubType subType) {
        this.subType = subType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public BookAppNotificationInfo getAppInfo() {
		return appInfo;
	}

	public void setAppInfo(BookAppNotificationInfo appInfo) {
		this.appInfo = appInfo;
	}

	@Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == type) {
            errors.add("type");
        }

        if (StringUtils.isEmpty(header)) {
            errors.add("header");
        }

        if (StringUtils.isEmpty(body)) {
            errors.add("body");
        }
        return errors;
    }
}
