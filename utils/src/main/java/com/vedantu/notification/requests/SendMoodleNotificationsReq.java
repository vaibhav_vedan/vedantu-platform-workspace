/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.requests;

import com.vedantu.notification.pojos.MoodleContentInfo;

/**
 *
 * @author somil
 */
public class SendMoodleNotificationsReq {
	String eventName;
	MoodleContentInfo contentInfo;

	public SendMoodleNotificationsReq() {
		super();
	}

	public SendMoodleNotificationsReq(String eventName, MoodleContentInfo contentInfo) {
		super();
		this.eventName = eventName;
		this.contentInfo = contentInfo;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public MoodleContentInfo getContentInfo() {
		return contentInfo;
	}

	public void setContentInfo(MoodleContentInfo contentInfo) {
		this.contentInfo = contentInfo;
	}

	@Override
	public String toString() {
		return "SendMoodleNotificationsReq{" + "eventName=" + eventName + ", contentInfo=" + contentInfo + '}';
	}

}
