/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.responses;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class MarkSeenRes extends AbstractRes{

	private String notificationId;
	private Boolean isMarked;

	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
        
        public Boolean getIsMarked() {
            return isMarked;
        }

        public void setIsMarked(Boolean isMarked) {
            this.isMarked = isMarked;
        }
        
	@Override
	public String toString() {
		return "MarkSeenRes{" +
				"notificationId='" + notificationId + '\'' +
				", isMarked=" + isMarked +
				"} " + super.toString();
	}
}
