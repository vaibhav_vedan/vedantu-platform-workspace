/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.responses;

import com.vedantu.notification.requests.Notification;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetNotificationsRes extends AbstractRes {

	private List<Notification> notifications;


	public GetNotificationsRes(List<Notification> notifications) {

		super();
		this.notifications = notifications;
	}

	public List<Notification> getNotifications() {

		return notifications;
	}

	@Override
	public String toString() {

		return "GetNotificationsRes{" +
				"notifications=" + notifications +
				"} " + super.toString();
	}
}
