/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.responses;

import com.vedantu.notification.pojos.UnsubscribedEmailInfo;
import com.vedantu.util.fos.response.AbstractListRes;

/**
 *
 * @author ajith
 */
public class GetUnsubscribedListRes extends AbstractListRes<UnsubscribedEmailInfo>{
    
}
