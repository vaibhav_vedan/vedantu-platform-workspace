/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.responses;

/**
 *
 * @author somil
 */
public class BasicRes {
	private Boolean success = true;

	public BasicRes() {
		super();
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "BasicRes [success=" + success + "]";
	}
}
