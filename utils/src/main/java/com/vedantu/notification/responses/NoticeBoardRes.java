package com.vedantu.notification.responses;

import com.vedantu.notification.enums.NotificationStatus;

public class NoticeBoardRes {
    private String studentId;
    private String message;
    private long expiryDate;
    private long scheduledTime;
    private NotificationStatus notificationStatus;
    private String manualNotificationId;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getManualNotificationId() {
        return manualNotificationId;
    }

    public void setManualNotificationId(String manualNotificationId) {
        this.manualNotificationId = manualNotificationId;
    }

    public long getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(long scheduledTime) {
        this.scheduledTime = scheduledTime;
    }
}
