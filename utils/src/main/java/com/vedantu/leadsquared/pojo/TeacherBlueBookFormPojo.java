package com.vedantu.leadsquared.pojo;

import java.util.List;

public class TeacherBlueBookFormPojo {

//	{
//		  "slug": "submit\/51382834937464\/",
//		  "temp_upload": {
//		    "q12_resume": [
//		      "speech recog.pdf"
//		    ]
//		  },
//		  "q1_fullName": {
//		    "first ": "firsr",
//		    "last": "R"
//		  },
//		  "q4_email": "email100@vedantu.com",
//		  "q15_phone": "324343443243242324432432",
//		  "q52_highestQualification52": "B.Tech",
//		  "q16_specializationAlong": "CS",
//		  "q51_yearsOf": "Less Four years",
//		  "q18_primarySubject": "Other",
//		  "q54_otherPrimary": "Primary",
//		  "q19_secondarySubject19": "Other",
//		  "q55_otherSecondary": "Secondary",
//		  "q20_pleaseMark": [
//		    "4 & 5",
//		    "6 to 8",
//		    "Other"
//		  ],
//		  "q56_otherGrade": "Grade",
//		  "q53_boardsYou": [
//		    "CBSE",
//		    "Other "
//		  ],
//		  "q57_pleaseSpecify": "Boards",
//		  "q28_hoursYou": [
//		    [
//		      "2"
//		    ],
//		    [
//		      "3"
//		    ]
//		  ],
//		  "q36_availableTimings36": [
//		    "3-4 pm",
//		    "5-6 pm",
//		    "7-8 pm",
//		    "8-9 pm"
//		  ],
//		  "q22_currentOccupation": "Other",
//		  "q58_pleaseSpecify58": "Occupation",
//		  "q48_dateOf": {
//		    "month": "09",
//		    "day": "04",
//		    "year": "1996"
//		  },
//		  "q41_typeOf": "Other",
//		  "q59_otherType": "Type _Of_Internet_connection",
//		  "q42_downloadSpeed": "Other",
//		  "q61_pleaseEnter": "16 mbps",
//		  "q62_uploadSpeed": "Other",
//		  "q63_pleaseEnter63": "10 mbps",
//		  "q46_yourAssociation": "Other",
//		  "q60_pleaseSpecify60": "Association",
//		  "q43_yourLocation": "Bangalore",
//		  "q24_howDid": "Work",
//		  "q29_channel": "",
//		  "q30_utm_campai gn": "",
//		  "q31_utm_source": "",
//		  "q32_utm_medium": "",
//		  "q33_utm_term": "",
//		  "q34_utm_content": "",
//		  "event_id": "1504770394236_51382834937464_lhdmQsW",
//		  "screen Shot50": "https:\/\/www.jotform.me\/uploads\/acad_vedantu\/51382834937464\/3805799858787629163\/Kmeans.png",
//		  "resume": [
//		    "https:\/\/www.jotform.me\ /uploads\/acad_vedantu\/51382834937464\/3805799858787629163\/speech%20recog.pdf"
//		  ]
//		}
	private String q4_email;
	private String q15_phone;
	private String q52_highestQualification52;
	private String q16_specializationAlong;
	private String q51_yearsOf;
	private String q18_primarySubject;
	private String q19_secondarySubject19;
	private String q22_currentOccupation;
	private FullNamePojo q1_fullName;
	private List<String> q20_pleaseMark;
	private List<String> q53_boardsYou;
	private List<List<String>> q28_hoursYou;
	private List<String> q36_availableTimings36;
	
	private DateJson q48_dateOf;
	
	private String q41_typeOf ;
	private String q62_uploadSpeed ;
	private String q46_yourAssociation ;
	private String q43_yourLocation ;
	private String q24_howDid ;
	private String q29_channel ;
	private String q30_utm_campaign ;
	private String q31_utm_source ;
	private String q32_utm_medium ;
	private String q33_utm_term ;
	private String q34_utm_content ;
	private String event_id ;
	private String screenShot50;
	private List<String> resume;
	private String q54_otherPrimary;
	private String q55_otherSecondary;
	private String q56_otherGrade;
	private String q57_pleaseSpecify;//Boards
	private String q58_pleaseSpecify58;//occupation
	private String q59_otherType;//typeOfInternet
	private String q63_pleaseEnter63;//upload Speed other
	private String q60_pleaseSpecify60;//Association
	private String q61_pleaseEnter;//download_speed other
	private String q42_downloadSpeed;//download_speed
	
	
	public String getQ4_email() {
		return q4_email;
	}

	public void setQ4_email(String q4_email) {
		this.q4_email = q4_email;
	}

	public String getQ15_phone() {
		return q15_phone;
	}

	public void setQ15_phone(String q15_phone) {
		this.q15_phone = q15_phone;
	}

	public String getQ52_highestQualification52() {
		return q52_highestQualification52;
	}

	public void setQ52_highestQualification52(String q52_highestQualification52) {
		this.q52_highestQualification52 = q52_highestQualification52;
	}

	public String getQ16_specializationAlong() {
		return q16_specializationAlong;
	}

	public void setQ16_specializationAlong(String q16_specializationAlong) {
		this.q16_specializationAlong = q16_specializationAlong;
	}

	public String getQ51_yearsOf() {
		return q51_yearsOf;
	}

	public void setQ51_yearsOf(String q51_yearsOf) {
		this.q51_yearsOf = q51_yearsOf;
	}

	public String getQ18_primarySubject() {
		return q18_primarySubject;
	}

	public void setQ18_primarySubject(String q18_primarySubject) {
		this.q18_primarySubject = q18_primarySubject;
	}

	public String getQ19_secondarySubject19() {
		return q19_secondarySubject19;
	}

	public void setQ19_secondarySubject19(String q19_secondarySubject19) {
		this.q19_secondarySubject19 = q19_secondarySubject19;
	}

	public String getQ22_currentOccupation() {
		return q22_currentOccupation;
	}

	public void setQ22_currentOccupation(String q22_currentOccupation) {
		this.q22_currentOccupation = q22_currentOccupation;
	}

	public FullNamePojo getQ1_fullName() {
		return q1_fullName;
	}

	public void setQ1_fullName(FullNamePojo q1_fullName) {
		this.q1_fullName = q1_fullName;
	}

	public List<String> getQ20_pleaseMark() {
		return q20_pleaseMark;
	}

	public void setQ20_pleaseMark(List<String> q20_pleaseMark) {
		this.q20_pleaseMark = q20_pleaseMark;
	}

	public List<String> getQ53_boardsYou() {
		return q53_boardsYou;
	}

	public void setQ53_boardsYou(List<String> q53_boardsYou) {
		this.q53_boardsYou = q53_boardsYou;
	}

	public List<List<String>> getQ28_hoursYou() {
		return q28_hoursYou;
	}

	public void setQ28_hoursYou(List<List<String>> q28_hoursYou) {
		this.q28_hoursYou = q28_hoursYou;
	}

	public List<String> getQ36_availableTimings36() {
		return q36_availableTimings36;
	}

	public void setQ36_availableTimings36(List<String> q36_availableTimings36) {
		this.q36_availableTimings36 = q36_availableTimings36;
	}

	public DateJson getQ48_dateOf() {
		return q48_dateOf;
	}

	public void setQ48_dateOf(DateJson q48_dateOf) {
		this.q48_dateOf = q48_dateOf;
	}

	public String getQ41_typeOf() {
		return q41_typeOf;
	}

	public void setQ41_typeOf(String q41_typeOf) {
		this.q41_typeOf = q41_typeOf;
	}

	public String getQ46_yourAssociation() {
		return q46_yourAssociation;
	}

	public void setQ46_yourAssociation(String q46_yourAssociation) {
		this.q46_yourAssociation = q46_yourAssociation;
	}

	public String getQ43_yourLocation() {
		return q43_yourLocation;
	}

	public void setQ43_yourLocation(String q43_yourLocation) {
		this.q43_yourLocation = q43_yourLocation;
	}

	public String getQ24_howDid() {
		return q24_howDid;
	}

	public void setQ24_howDid(String q24_howDid) {
		this.q24_howDid = q24_howDid;
	}

	public String getQ29_channel() {
		return q29_channel;
	}

	public void setQ29_channel(String q29_channel) {
		this.q29_channel = q29_channel;
	}

	public String getQ30_utm_campaign() {
		return q30_utm_campaign;
	}

	public void setQ30_utm_campaign(String q30_utm_campaign) {
		this.q30_utm_campaign = q30_utm_campaign;
	}

	public String getQ31_utm_source() {
		return q31_utm_source;
	}

	public void setQ31_utm_source(String q31_utm_source) {
		this.q31_utm_source = q31_utm_source;
	}

	public String getQ32_utm_medium() {
		return q32_utm_medium;
	}

	public void setQ32_utm_medium(String q32_utm_medium) {
		this.q32_utm_medium = q32_utm_medium;
	}

	public String getQ33_utm_term() {
		return q33_utm_term;
	}

	public void setQ33_utm_term(String q33_utm_term) {
		this.q33_utm_term = q33_utm_term;
	}

	public String getQ34_utm_content() {
		return q34_utm_content;
	}

	public void setQ34_utm_content(String q34_utm_content) {
		this.q34_utm_content = q34_utm_content;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getScreenShot50() {
		return screenShot50;
	}

	public void setScreenShot50(String screenShot50) {
		this.screenShot50 = screenShot50;
	}

	public List<String> getResume() {
		return resume;
	}

	public void setResume(List<String> resume) {
		this.resume = resume;
	}

	public String getQ56_otherGrade() {
		return q56_otherGrade;
	}

	public void setQ56_otherGrade(String q56_otherGrade) {
		this.q56_otherGrade = q56_otherGrade;
	}

	public String getQ58_pleaseSpecify58() {
		return q58_pleaseSpecify58;
	}

	public void setQ58_pleaseSpecify58(String q58_pleaseSpecify58) {
		this.q58_pleaseSpecify58 = q58_pleaseSpecify58;
	}

	public String getQ63_pleaseEnter63() {
		return q63_pleaseEnter63;
	}

	public void setQ63_pleaseEnter63(String q63_pleaseEnter63) {
		this.q63_pleaseEnter63 = q63_pleaseEnter63;
	}

	public String getQ62_uploadSpeed() {
		return q62_uploadSpeed;
	}

	public void setQ62_uploadSpeed(String q62_uploadSpeed) {
		this.q62_uploadSpeed = q62_uploadSpeed;
	}

	public String getQ54_otherPrimary() {
		return q54_otherPrimary;
	}

	public void setQ54_otherPrimary(String q54_otherPrimary) {
		this.q54_otherPrimary = q54_otherPrimary;
	}

	public String getQ55_otherSecondary() {
		return q55_otherSecondary;
	}

	public void setQ55_otherSecondary(String q55_otherSecondary) {
		this.q55_otherSecondary = q55_otherSecondary;
	}

	public String getQ57_pleaseSpecify() {
		return q57_pleaseSpecify;
	}

	public void setQ57_pleaseSpecify(String q57_pleaseSpecify) {
		this.q57_pleaseSpecify = q57_pleaseSpecify;
	}

	public String getQ59_otherType() {
		return q59_otherType;
	}

	public void setQ59_otherType(String q59_otherType) {
		this.q59_otherType = q59_otherType;
	}

	public String getQ60_pleaseSpecify60() {
		return q60_pleaseSpecify60;
	}

	public void setQ60_pleaseSpecify60(String q60_pleaseSpecify60) {
		this.q60_pleaseSpecify60 = q60_pleaseSpecify60;
	}

	public String getQ61_pleaseEnter() {
		return q61_pleaseEnter;
	}

	public void setQ61_pleaseEnter(String q61_pleaseEnter) {
		this.q61_pleaseEnter = q61_pleaseEnter;
	}

	public String getQ42_downloadSpeed() {
		return q42_downloadSpeed;
	}

	public void setQ42_downloadSpeed(String q42_downloadSpeed) {
		this.q42_downloadSpeed = q42_downloadSpeed;
	}

}
