/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.leadsquared.pojo;

import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pranavm
 */
public class ExternalLeadSquaredRequest extends AbstractFrontEndReq{

    private LeadSquaredAction action;
    private LeadSquaredDataType dataType;
    private Map<String, String> params = new HashMap<>();

    public ExternalLeadSquaredRequest(LeadSquaredAction action, LeadSquaredDataType dataType, Map<String, String> params) {
        super();
        this.action = action;
        this.dataType = dataType;
        this.params = params;
    }
    
    public LeadSquaredAction getAction() {
        return action;
    }

    public void setAction(LeadSquaredAction action) {
        this.action = action;
    }

    public LeadSquaredDataType getDataType() {
        return dataType;
    }

    public void setDataType(LeadSquaredDataType dataType) {
        this.dataType = dataType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "ExternalLeadSquaredRequest{" + "action=" + action + ", dataType=" + dataType + ", params=" + params + '}';
    }

}
