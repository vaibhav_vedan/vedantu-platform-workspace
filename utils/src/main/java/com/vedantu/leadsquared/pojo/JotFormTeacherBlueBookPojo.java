package com.vedantu.leadsquared.pojo;

public class JotFormTeacherBlueBookPojo {

//	ip: 45.112.138.78
//	rawRequest: {"slug":"submit\/72404024170947\/","q1_fullName":{"first":"firstname","last":"las"},"q4_email":"firstname@firstname.com","q15_phone":"1111111111111111","q52_highestQualification52":"B.Arch","q16_specializationAlong":"firstname","q51_yearsOf":"Less Two years","q18_primarySubject":"Chemistry","q19_secondarySubject19":"Sanskrit","q20_pleaseMark":["6 to 8","11 & 12 (Regular Curriculum)"],"q53_boardsYou":["CBSE","ICSE"],"q28_hoursYou":[["2"],["2"]],"q36_availableTimings36":["9-10 am","3-4 pm"],"q22_currentOccupation":"Freelancer","q48_dateOf":{"month":"08","day":"15","year":"2000"},"q41_typeOf":"Wifi","q42_speedOf":"4 mbps","q46_yourAssociation":"Additonal source of Income","q43_yourLocation":"firstname","q24_howDid":"firstname","q29_channel":"","q30_utm_campaign":"","q31_utm_source":"","q32_utm_medium":"","q33_utm_term":"","q34_utm_content":"","event_id":"1504073740093_72404024170947_tKKeWEy"}
//	formID: 72404024170947
//	webhookURL: https://requestb.in/1gmhnce1
//	username: acad_vedantu
//	formTitle: Clone of Teacher's Blue Book
//	submissionID: 3798830098782690376
//	type: WEB
//	username: acad_vedantu
	private String ip;
	private String formId;
	private String webhookURL;
	private String userName;
	private String formTitle;
	private String submissionId;
	private String type;
	private TeacherBlueBookFormPojo rawRequest;

	@Override
	public String toString() {
		return "JotFormTeacherBlueBookPojo [ip=" + ip + ", formId=" + formId + ", webhookURL=" + webhookURL
				+ ", userName=" + userName + ", formTitle=" + formTitle + ", submissionId=" + submissionId + ", type="
				+ type + ", rawRequest=" + rawRequest + "]";
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getWebhookURL() {
		return webhookURL;
	}

	public void setWebhookURL(String webhookURL) {
		this.webhookURL = webhookURL;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFormTitle() {
		return formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public TeacherBlueBookFormPojo getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(TeacherBlueBookFormPojo rawRequest) {
		this.rawRequest = rawRequest;
	}
	
}
