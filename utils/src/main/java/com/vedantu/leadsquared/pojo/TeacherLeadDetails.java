package com.vedantu.leadsquared.pojo;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.User.User;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;

public class TeacherLeadDetails {

	// As the fields are directly mapped to the ones in Lead squared fields, it
	// does not follow the camel case format
	public String mx_Weekend_Hours;//c++
	public String mx_Weekday_Hours;//c++
	public String mx_Available_Timings;// MultiSelect//c-o
	public String mx_Current_Occupation;//c -o++
	public String mx_Date_of_Birth;
	public String mx_Type_of_Internet_Connection;//c-o++
	public String mx_Upload_Speed_of_Internet;// Q
	public String mx_Download_Speed;//c-o
	public String mx_Association;//c-o++
	public String mx_How_did_you_come_to_know_about_Vedantu;//c
	public String mx_Boards;// MultiSelect//c-o++
	public String mx_Student_Grades;// MultiSelect-o++
	public String mx_Secondary_Subject;//c-o++
	public String mx_Primary_Subject;//c-o++
	public String mx_Years_of_experience_in_teaching;//c
	public String mx_Specialization_College;//c++
	public String mx_Highest_Qualification;//c
	public String mx_Gender;

	public String FirstName;//c
	public String LastName;//c
	public String EmailAddress;//c
	public String Phone;//
	public String Mobile;//c

	public String mx_Street1;
	public String mx_Street2;
	public String mx_City;
	public String mx_State;
	public String mx_Country;
	public String mx_ScreenshotURL;
	public String mx_ResumeURL;
	
	public String mx_Upload_Speed;
	
	public String SourceMedium;

	public TeacherLeadDetails(){
		super();
	}
	
	public TeacherLeadDetails(TeacherBlueBookFormPojo blueBook) {
		super();
		this.FirstName = blueBook.getQ1_fullName().getFirst();
		this.LastName = blueBook.getQ1_fullName().getLast();
		this.EmailAddress = blueBook.getQ4_email();
		this.Mobile = blueBook.getQ15_phone();
		
		if(StringUtils.isNotEmpty(blueBook.getQ16_specializationAlong())){
			this.mx_Specialization_College = blueBook.getQ16_specializationAlong();
		}
		
		if(StringUtils.isNotEmpty(blueBook.getQ18_primarySubject())){
			if(blueBook.getQ18_primarySubject().equalsIgnoreCase("Other") 
					&& StringUtils.isNotEmpty(blueBook.getQ54_otherPrimary())){
				this.mx_Primary_Subject = blueBook.getQ54_otherPrimary();
			}else{
				this.mx_Primary_Subject = blueBook.getQ18_primarySubject();
			}			
		}
		if(StringUtils.isNotEmpty(blueBook.getQ19_secondarySubject19())){
			if(blueBook.getQ19_secondarySubject19().equalsIgnoreCase("Other")
					&& StringUtils.isNotEmpty(blueBook.getQ55_otherSecondary())){
				this.mx_Secondary_Subject = blueBook.getQ55_otherSecondary();//
			}else{
				this.mx_Secondary_Subject = blueBook.getQ19_secondarySubject19();
			}
		}
		if(StringUtils.isNotEmpty(blueBook.getQ22_currentOccupation())){
			if(blueBook.getQ22_currentOccupation().equalsIgnoreCase("Other")
					&& StringUtils.isNotEmpty(blueBook.getQ58_pleaseSpecify58())){
				this.mx_Current_Occupation = blueBook.getQ58_pleaseSpecify58();//
			}else{
				this.mx_Current_Occupation = blueBook.getQ22_currentOccupation();
			}
		}
		if(StringUtils.isNotEmpty(blueBook.getQ24_howDid())){
			this.mx_How_did_you_come_to_know_about_Vedantu = blueBook.getQ24_howDid();
		}
		if(StringUtils.isNotEmpty(blueBook.getQ41_typeOf())){
			if(blueBook.getQ41_typeOf().equalsIgnoreCase("Other")
					&& StringUtils.isNotEmpty(blueBook.getQ59_otherType())){
				this.mx_Type_of_Internet_Connection = blueBook.getQ59_otherType();//
			}else{
				this.mx_Type_of_Internet_Connection = blueBook.getQ41_typeOf();
			}
		
		}
		// Make sure
		if(StringUtils.isNotEmpty(blueBook.getQ42_downloadSpeed())){
			if(blueBook.getQ42_downloadSpeed().equalsIgnoreCase("Other")
					&& StringUtils.isNotEmpty(blueBook.getQ61_pleaseEnter())){
				this.mx_Download_Speed = blueBook.getQ61_pleaseEnter();//
			}else{
				this.mx_Download_Speed = blueBook.getQ42_downloadSpeed();
			}
		}
		
		if(StringUtils.isNotEmpty(blueBook.getQ62_uploadSpeed())){
			if(blueBook.getQ62_uploadSpeed().equalsIgnoreCase("Other")
					&& StringUtils.isNotEmpty(blueBook.getQ63_pleaseEnter63())){
				this.mx_Upload_Speed = blueBook.getQ63_pleaseEnter63();
			}else{
				this.mx_Upload_Speed = blueBook.getQ62_uploadSpeed();
			}
		}
		if(StringUtils.isNotEmpty(blueBook.getQ43_yourLocation())){
			this.mx_City = blueBook.getQ43_yourLocation();
		}
		if(StringUtils.isNotEmpty(blueBook.getQ46_yourAssociation())){
			if(blueBook.getQ46_yourAssociation().equalsIgnoreCase("Other")
					&& StringUtils.isNotEmpty(blueBook.getQ60_pleaseSpecify60())){
				this.mx_Association = blueBook.getQ60_pleaseSpecify60();
			}else{
				this.mx_Association = blueBook.getQ46_yourAssociation();
			}
		}
		if(StringUtils.isNotEmpty(blueBook.getQ51_yearsOf())){
			this.mx_Years_of_experience_in_teaching = blueBook.getQ51_yearsOf();
		}
		if(StringUtils.isNotEmpty(blueBook.getQ52_highestQualification52())){
			this.mx_Highest_Qualification = blueBook.getQ52_highestQualification52();
		}
		if(ArrayUtils.isNotEmpty(blueBook.getQ53_boardsYou())){
			List<String> boards = new ArrayList<>();
			for(String board : blueBook.getQ53_boardsYou()){
				if(StringUtils.isNotEmpty(board)){
					if("Other".equalsIgnoreCase(board) 
							&& StringUtils.isNotEmpty(blueBook.getQ57_pleaseSpecify())){
						//	Put data based on the question for other
							boards.add(blueBook.getQ57_pleaseSpecify());
					}else{
						boards.add(board);
					}					
				}
			}
			if(ArrayUtils.isNotEmpty(boards)){
				this.mx_Boards = join(boards, ";");
			}
		}
		if(ArrayUtils.isNotEmpty(blueBook.getQ28_hoursYou())){
			List<String> weekdays = blueBook.getQ28_hoursYou().get(0);
			if(ArrayUtils.isNotEmpty(weekdays) && StringUtils.isNotEmpty(weekdays.get(0))){
				this.mx_Weekday_Hours = weekdays.get(0);
			}
			
			List<String> weekends = blueBook.getQ28_hoursYou().get(1);
			if(ArrayUtils.isNotEmpty(weekends) && StringUtils.isNotEmpty(weekends.get(0))){
				this.mx_Weekend_Hours = weekends.get(0);
			}
		}
		if(ArrayUtils.isNotEmpty(blueBook.getQ36_availableTimings36())){
			List<String> times = new ArrayList<>();
			for(String time : blueBook.getQ36_availableTimings36()){
				if(StringUtils.isNotEmpty(time)){
					times.add(time);										
				}
			}
			if(ArrayUtils.isNotEmpty(times)){
				this.mx_Available_Timings = join(times, ";");
			}
		}
		if(ArrayUtils.isNotEmpty(blueBook.getQ20_pleaseMark())){
			List<String> grades = new ArrayList<>();
			for(String grade : blueBook.getQ20_pleaseMark()){
				if(StringUtils.isNotEmpty(grade)){
					if("Other".equalsIgnoreCase(grade) && StringUtils.isNotEmpty(blueBook.getQ56_otherGrade())){
						grades.add(blueBook.getQ56_otherGrade());
					}else{
						grades.add(grade);
					}					
				}
			}
			if(ArrayUtils.isNotEmpty(grades)){
				this.mx_Student_Grades = join(grades,";");
			}
		}
		
		if(blueBook.getQ48_dateOf() != null){
			DateJson dob = blueBook.getQ48_dateOf();
			if(StringUtils.isNotEmpty(dob.getDay()) && StringUtils.isNotEmpty(dob.getMonth())
					&&	StringUtils.isNotEmpty(dob.getYear())){
				String date = dob.getYear()+"-"+dob.getMonth() + "-" + dob.getDay();
				this.mx_Date_of_Birth = date;
			}
		}
		if(StringUtils.isNotEmpty(blueBook.getScreenShot50())){
			this.mx_ScreenshotURL = blueBook.getScreenShot50();
		}
		if(ArrayUtils.isNotEmpty(blueBook.getResume()) 
				&& StringUtils.isNotEmpty(blueBook.getResume().get(0))){
			this.mx_ResumeURL = blueBook.getResume().get(0);
		}
		this.SourceMedium = "JOTFORM";
	}

	public String getMx_Available_Timings() {
		return mx_Available_Timings;
	}

	public void setMx_Available_Timings(String mx_Available_Timings) {
		this.mx_Available_Timings = mx_Available_Timings;
	}

	public String getMx_Current_Occupation() {
		return mx_Current_Occupation;
	}

	public void setMx_Current_Occupation(String mx_Current_Occupation) {
		this.mx_Current_Occupation = mx_Current_Occupation;
	}

	public String getMx_Date_of_Birth() {
		return mx_Date_of_Birth;
	}

	public void setMx_Date_of_Birth(String mx_Date_of_Birth) {
		this.mx_Date_of_Birth = mx_Date_of_Birth;
	}

	public String getMx_Type_of_Internet_Connection() {
		return mx_Type_of_Internet_Connection;
	}

	public void setMx_Type_of_Internet_Connection(String mx_Type_of_Internet_Connection) {
		this.mx_Type_of_Internet_Connection = mx_Type_of_Internet_Connection;
	}

	public String getMx_Upload_Speed_of_Internet() {
		return mx_Upload_Speed_of_Internet;
	}

	public void setMx_Upload_Speed_of_Internet(String mx_Upload_Speed_of_Internet) {
		this.mx_Upload_Speed_of_Internet = mx_Upload_Speed_of_Internet;
	}

	public String getMx_Download_Speed_of_Internet() {
		return mx_Download_Speed;
	}

	public void setMx_Download_Speed_of_Internet(String mx_Download_Speed_of_Internet) {
		this.mx_Download_Speed = mx_Download_Speed_of_Internet;
	}

	public String getMx_Your_association_with_Vedantu_is_for() {
		return mx_Association;
	}

	public void setMx_Your_association_with_Vedantu_is_for(String mx_Your_association_with_Vedantu_is_for) {
		this.mx_Association = mx_Your_association_with_Vedantu_is_for;
	}

	public String getMx_How_did_you_come_to_know_about_Vedantu() {
		return mx_How_did_you_come_to_know_about_Vedantu;
	}

	public void setMx_How_did_you_come_to_know_about_Vedantu(String mx_How_did_you_come_to_know_about_Vedantu) {
		this.mx_How_did_you_come_to_know_about_Vedantu = mx_How_did_you_come_to_know_about_Vedantu;
	}

	public String getMx_Secondary_Subject_of_teaching() {
		return mx_Secondary_Subject;
	}

	public void setMx_Secondary_Subject_of_teaching(String mx_Secondary_Subject_of_teaching) {
		this.mx_Secondary_Subject = mx_Secondary_Subject_of_teaching;
	}

	public String getMx_Primary_Subject_of_teaching() {
		return mx_Primary_Subject;
	}

	public void setMx_Primary_Subject_of_teaching(String mx_Primary_Subject_of_teaching) {
		this.mx_Primary_Subject = mx_Primary_Subject_of_teaching;
	}

	public String getMx_Years_of_experience_in_teaching() {
		return mx_Years_of_experience_in_teaching;
	}

	public void setMx_Years_of_experience_in_teaching(String mx_Years_of_experience_in_teaching) {
		this.mx_Years_of_experience_in_teaching = mx_Years_of_experience_in_teaching;
	}

	public String getMx_Specialization_along_with_your_college_details() {
		return mx_Specialization_College;
	}

	public void setMx_Specialization_along_with_your_college_details(
			String mx_Specialization_along_with_your_college_details) {
		this.mx_Specialization_College = mx_Specialization_along_with_your_college_details;
	}

	public String getMx_Highest_Qualification() {
		return mx_Highest_Qualification;
	}

	public void setMx_Highest_Qualification(String mx_Highest_Qualification) {
		this.mx_Highest_Qualification = mx_Highest_Qualification;
	}

	public String getMx_Gender() {
		return mx_Gender;
	}

	public void setMx_Gender(String mx_Gender) {
		this.mx_Gender = mx_Gender;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getMx_Street1() {
		return mx_Street1;
	}

	public void setMx_Street1(String mx_Street1) {
		this.mx_Street1 = mx_Street1;
	}

	public String getMx_Street2() {
		return mx_Street2;
	}

	public void setMx_Street2(String mx_Street2) {
		this.mx_Street2 = mx_Street2;
	}

	public String getMx_City() {
		return mx_City;
	}

	public void setMx_City(String mx_City) {
		this.mx_City = mx_City;
	}

	public String getMx_State() {
		return mx_State;
	}

	public void setMx_State(String mx_State) {
		this.mx_State = mx_State;
	}

	public String getMx_Country() {
		return mx_Country;
	}

	public void setMx_Country(String mx_Country) {
		this.mx_Country = mx_Country;
	}

	public String getMx_Weekend_Hours() {
		return mx_Weekend_Hours;
	}

	public void setMx_Weekend_Hours(String mx_Weekend_Hours) {
		this.mx_Weekend_Hours = mx_Weekend_Hours;
	}

	public String getMx_Weekday_Hours() {
		return mx_Weekday_Hours;
	}

	public void setMx_Weekday_Hours(String mx_Weekday_Hours) {
		this.mx_Weekday_Hours = mx_Weekday_Hours;
	}

	public String getMx_Association() {
		return mx_Association;
	}

	public void setMx_Association(String mx_Association) {
		this.mx_Association = mx_Association;
	}

	public String getMx_Boards() {
		return mx_Boards;
	}

	public void setMx_Boards(String mx_Boards) {
		this.mx_Boards = mx_Boards;
	}

	public String getMx_Student_Grades() {
		return mx_Student_Grades;
	}

	public void setMx_Student_Grades(String mx_Student_Grades) {
		this.mx_Student_Grades = mx_Student_Grades;
	}

	public String getMx_Specialization_College() {
		return mx_Specialization_College;
	}

	public void setMx_Specialization_College(String mx_Specialization_College) {
		this.mx_Specialization_College = mx_Specialization_College;
	}

	public String getMx_ScreenshotURL() {
		return mx_ScreenshotURL;
	}

	public void setMx_ScreenshotURL(String mx_ScreenshotURL) {
		this.mx_ScreenshotURL = mx_ScreenshotURL;
	}

	public String getMx_resumeURL() {
		return mx_ResumeURL;
	}

	public void setMx_resumeURL(String mx_resumeURL) {
		this.mx_ResumeURL = mx_resumeURL;
	}

	public String getMx_Download_Speed() {
		return mx_Download_Speed;
	}

	public void setMx_Download_Speed(String mx_Download_Speed) {
		this.mx_Download_Speed = mx_Download_Speed;
	}

	public String getMx_Secondary_Subject() {
		return mx_Secondary_Subject;
	}

	public void setMx_Secondary_Subject(String mx_Secondary_Subject) {
		this.mx_Secondary_Subject = mx_Secondary_Subject;
	}

	public String getMx_Primary_Subject() {
		return mx_Primary_Subject;
	}

	public void setMx_Primary_Subject(String mx_Primary_Subject) {
		this.mx_Primary_Subject = mx_Primary_Subject;
	}

	public String getMx_ResumeURL() {
		return mx_ResumeURL;
	}

	public void setMx_ResumeURL(String mx_ResumeURL) {
		this.mx_ResumeURL = mx_ResumeURL;
	}

	public String getMx_Upload_Speed() {
		return mx_Upload_Speed;
	}

	public void setMx_Upload_Speed(String mx_Upload_Speed) {
		this.mx_Upload_Speed = mx_Upload_Speed;
	}

	public String getSourceMedium() {
		return SourceMedium;
	}

	public void setSourceMedium(String sourceMedium) {
		SourceMedium = sourceMedium;
	}
	
	public String join(List<String> inputs, String separator) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < inputs.size(); i++) {

			builder.append(inputs.get(i));
			if (inputs.size() - 1 != i) {
				builder.append(separator);
			}

		}
		return builder.toString();
	}

	@Override
	public String toString() {
		return "TeacherLeadDetails [mx_Weekend_Hours=" + mx_Weekend_Hours + ", mx_Weekday_Hours=" + mx_Weekday_Hours
				+ ", mx_Available_Timings=" + mx_Available_Timings + ", mx_Current_Occupation=" + mx_Current_Occupation
				+ ", mx_Date_of_Birth=" + mx_Date_of_Birth + ", mx_Type_of_Internet_Connection="
				+ mx_Type_of_Internet_Connection + ", mx_Upload_Speed_of_Internet=" + mx_Upload_Speed_of_Internet
				+ ", mx_Download_Speed=" + mx_Download_Speed + ", mx_Association=" + mx_Association
				+ ", mx_How_did_you_come_to_know_about_Vedantu=" + mx_How_did_you_come_to_know_about_Vedantu
				+ ", mx_Boards=" + mx_Boards + ", mx_Student_Grades=" + mx_Student_Grades + ", mx_Secondary_Subject="
				+ mx_Secondary_Subject + ", mx_Primary_Subject=" + mx_Primary_Subject
				+ ", mx_Years_of_experience_in_teaching=" + mx_Years_of_experience_in_teaching
				+ ", mx_Specialization_College=" + mx_Specialization_College + ", mx_Highest_Qualification="
				+ mx_Highest_Qualification + ", mx_Gender=" + mx_Gender + ", FirstName=" + FirstName + ", LastName="
				+ LastName + ", EmailAddress=" + EmailAddress + ", Phone=" + Phone + ", Mobile=" + Mobile
				+ ", mx_Street1=" + mx_Street1 + ", mx_Street2=" + mx_Street2 + ", mx_City=" + mx_City + ", mx_State="
				+ mx_State + ", mx_Country=" + mx_Country + ", mx_ScreenshotURL=" + mx_ScreenshotURL + ", mx_ResumeURL="
				+ mx_ResumeURL + ", mx_Upload_Speed=" + mx_Upload_Speed + ", SourceMedium=" + SourceMedium + "]";
	}
}
