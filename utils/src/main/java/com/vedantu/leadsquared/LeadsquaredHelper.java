package com.vedantu.leadsquared;

public class LeadsquaredHelper {

    public static String BULK_LEAD_PUSH_PROCESS_COUNT  = "BULK_LEAD_PUSH_PROCESS_COUNT";
    public static String BULK_LS_API_PROCESSING_COUNT  = "BULK_LS_API_PROCESSING_COUNT";
    public static String BULK_LS_API_PROCESSING_TIME_TRACKER  = "BULK_LS_API_PROCESSING_TIME_TRACKER";

    private static Integer LEADSQUARED_API_BULK_TOTAL_USABLE_THRESHOLD = 6;
    private static Integer BULK_LEAD_UPSERT_DEFAULT_QUOTA = 4;

    public static Integer getQuotaForBulkAPI(boolean isBulkApiProcessing){

        if(isBulkApiProcessing)
            return  LEADSQUARED_API_BULK_TOTAL_USABLE_THRESHOLD - BULK_LEAD_UPSERT_DEFAULT_QUOTA;

        return LEADSQUARED_API_BULK_TOTAL_USABLE_THRESHOLD;
    }

    public static Integer getQuotaForBulkLeadUpsertAPI(){

        return LEADSQUARED_API_BULK_TOTAL_USABLE_THRESHOLD;
    }
}
