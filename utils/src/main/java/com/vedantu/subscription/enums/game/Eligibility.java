package com.vedantu.subscription.enums.game;

public enum Eligibility {
    ELIGIBLE, INELIGIBLE
}
