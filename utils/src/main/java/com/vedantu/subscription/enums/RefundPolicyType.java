package com.vedantu.subscription.enums;

public enum RefundPolicyType {
    PRO_RATA, CLIFF;
}
