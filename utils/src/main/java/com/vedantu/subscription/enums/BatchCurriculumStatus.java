package com.vedantu.subscription.enums;

public enum BatchCurriculumStatus {

    ON_TRACK, SLOW, FAST, NOT_ON_TRACK;

}
