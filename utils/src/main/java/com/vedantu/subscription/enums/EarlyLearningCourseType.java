package com.vedantu.subscription.enums;

@Deprecated
public enum EarlyLearningCourseType {
    SUPER_CODER, SUPER_READER
}
