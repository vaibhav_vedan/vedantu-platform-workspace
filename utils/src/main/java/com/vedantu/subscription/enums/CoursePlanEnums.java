/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.enums;

/**
 *
 * @author ajith
 */
public class CoursePlanEnums {

    public enum CoursePlanState {
        DRAFT(0), PUBLISHED(1), TRIAL_PAYMENT_DONE(2),
        TRIAL_SESSIONS_DONE(3), ENROLLED(4), ENDED(5);

        public int order;

        CoursePlanState(int priority) {
            this.order = priority;
        }
    }

    public enum CoursePlanEndType {
        TRIAL_REFUNDED, NON_PAYMENT_INSTALMENT_DUES
    }
}
