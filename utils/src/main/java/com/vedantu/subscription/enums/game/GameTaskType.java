package com.vedantu.subscription.enums.game;

/**
 * @author mano
 */

public enum GameTaskType {

    TASK, REWARD
}
