package com.vedantu.subscription.enums;

public enum EnrollmentType {
    AUTO_ENROLL, LOCKED, UPSELL, CLICK_TO_ENROLL, NORMAL_ENROLMENT
}
