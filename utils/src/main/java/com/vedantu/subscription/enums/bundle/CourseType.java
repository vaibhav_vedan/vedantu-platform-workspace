package com.vedantu.subscription.enums.bundle;

public enum CourseType {
    LIVE, RECORDED
}
