package com.vedantu.subscription.enums.bundle;

/**
 * Created by somil on 08/05/17.
 */
public enum BundleState {
    DRAFT, ACTIVE, INACTIVE, ENDED
}
