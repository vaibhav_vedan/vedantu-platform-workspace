package com.vedantu.subscription.enums.game;

public enum GameSubtaskName {

    SINGLE_CLASS, TWO_CONSECUTIVE_CLASS, TEST, DOWNLOAD, REPLAY
}
