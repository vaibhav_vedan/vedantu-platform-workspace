package com.vedantu.subscription.enums.game;

/**
 * @author mano
 */

public enum GameEntity {
    GTT_SESSION, TEST, DOWNLOAD, USER, BUNDLE
}
