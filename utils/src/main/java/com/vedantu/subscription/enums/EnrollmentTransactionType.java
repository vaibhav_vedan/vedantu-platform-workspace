package com.vedantu.subscription.enums;

public enum EnrollmentTransactionType {

    CREDIT, DEBIT;

}
