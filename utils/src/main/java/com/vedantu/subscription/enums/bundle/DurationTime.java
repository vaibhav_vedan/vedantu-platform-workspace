package com.vedantu.subscription.enums.bundle;

public enum DurationTime {
	NONE("NA"),
    THREE_MONTHS("3 Months"),
    SIX_MONTHS("6 Months"),
    NINE_MONTHS("9 Months"),
    ONE_YEAR("12 Months"),
    TWO_YEARS("24 Months"),
    THREE_YEARS("36 Months"),
    FOUR_YEARS("48 Months"),
    FIVE_YEARS("62 Months"),
    SIX_YEARS("74 Months"),
    SEVEN_YEARS("86 Months"),
    EIGHT_YEARS("98 Months"),
    NINE_YEARS("110 Months"),
    TEN_YEARS("122 Months");
    
    private String durationTimeInApp;
	
    private DurationTime(String durationTimeInApp) {
    	this.durationTimeInApp = durationTimeInApp;
    	
    }

	public String getDurationTimeInApp() {
		return durationTimeInApp;
	}
}
