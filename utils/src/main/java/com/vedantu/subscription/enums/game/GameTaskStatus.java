package com.vedantu.subscription.enums.game;

public enum GameTaskStatus {

    INCOMPLETE, COMPLETE, DISABLED, LOCKED, UNLOCKED, CLAIMED, SHIPPED
}
