package com.vedantu.subscription.enums;

public enum BundleContentType {
	TEST, VIDEO, WEBINAR, PACKAGES
}
