package com.vedantu.subscription.enums;

/**
 * Created by somil on 09/05/17.
 */
public enum ContentPriceType {
    PUBLIC, FREE, PAID
}
