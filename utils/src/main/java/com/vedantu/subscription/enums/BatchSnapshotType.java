package com.vedantu.subscription.enums;

public enum BatchSnapshotType {
    MONTHLY, INITIAL;
}
