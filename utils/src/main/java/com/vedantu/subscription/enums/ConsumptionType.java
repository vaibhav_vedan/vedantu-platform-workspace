package com.vedantu.subscription.enums;

public enum ConsumptionType {

    ACTIVE, INACTIVE, ENDED;

}
