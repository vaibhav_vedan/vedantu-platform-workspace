/**
 * 
 */
package com.vedantu.subscription.enums;

/**
 * @author subarna
 *
 */
public enum CourseCategory {

	LONG_TRACK("Long Track Full Syllabus Batches"),
	FAST_TRACK("Fast Track Full Syllabus Batches"),
	REVISION_COURSE("Full syllabus Revision Batches"),
	EXAM_PREP("Full syllabus Exam Preparation Batches"),
	MID_SEM_PREP("Mid Semester Preparation Batches"),
	CRASH_COURSE("Crash Course Batches");

	private String title; 
	  

    public String getTitle() 
    { 
        return this.title; 
    } 
  

    private CourseCategory(String title) 
    { 
        this.title = title; 
    } 
}
