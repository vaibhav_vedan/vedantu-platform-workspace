package com.vedantu.subscription.enums;

public enum ReminderTimeType {

	TRIAL, INSTALMENT
}
