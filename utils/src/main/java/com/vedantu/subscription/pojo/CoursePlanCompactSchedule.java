/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

import com.vedantu.util.enums.Day;

/**
 *
 * @author ajith
 */
public class CoursePlanCompactSchedule {

    private Day day;
    private Long startTime;
    private Long endTime;

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "CoursePlanCompactSchedule{" + "days=" + day + ", startTime=" + startTime + ", endTime=" + endTime + '}';
    }

}
