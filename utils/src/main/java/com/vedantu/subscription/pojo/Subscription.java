/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

import com.vedantu.User.AbstractEntityRes;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.dbentitybeans.mysql.AbstractSqlEntityBean;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.subscription.SubscriptionState;

/**
 *
 * @author somil
 */
public class Subscription extends AbstractSqlEntityBean {
    private Long teacherId;
	private Long studentId;
	private String planId;
	private SessionModel model;
	private ScheduleType schedule;
	private Long offeringId;
	private Long totalHours;
	private Long consumedHours;
	private Long lockedHours;
	private Long remainingHours;
	private String subject;
	private String target;
	private Integer grade;
	private Long hourlyRate;
	private String title;
	private Long startDate;
	private Long boardId;
	private Long noOfWeeks;
	private Long endTime; // CancelSubscriptionTime
	private String endReason; // Subscription End Reason
	private Long endedBy; // UserId which ended Subscription
	private String note; // note for trial
	private SubModel subModel;
	private SubscriptionState state = SubscriptionState.INACTIVE;

	public Subscription() {
		super();
	}

	public Subscription(Long teacherId, Long studentId, String planId, SessionModel model, ScheduleType schedule,
			Long offeringId, Long totalHours, Long consumedHours, Long lockedHours, Long remainingHours, String subject,
			String target, Integer grade, Long hourlyRate, String title, Long startDate, Long boardId, Long noOfWeeks,
			Long endTime, String endReason, Long endedBy, String note, SubModel subModel, SubscriptionState state) {
		super();
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.planId = planId;
		this.model = model;
		this.schedule = schedule;
		this.offeringId = offeringId;
		this.totalHours = totalHours;
		this.consumedHours = consumedHours;
		this.lockedHours = lockedHours;
		this.remainingHours = remainingHours;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.hourlyRate = hourlyRate;
		this.title = title;
		this.startDate = startDate;
		this.boardId = boardId;
		this.noOfWeeks = noOfWeeks;
		this.endTime = endTime;
		this.endReason = endReason;
		this.endedBy = endedBy;
		this.note = note;
		this.state = state;
		if (subModel == null) {
			this.subModel = SubModel.DEFAULT;
		} else {
			this.subModel = subModel;
		}

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public ScheduleType getSchedule() {
		return schedule;
	}

	public void setSchedule(ScheduleType schedule) {
		this.schedule = schedule;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public Long getConsumedHours() {
		return consumedHours;
	}

	public void setConsumedHours(Long consumedHours) {
		this.consumedHours = consumedHours;
	}

	public Long getLockedHours() {
		return lockedHours;
	}

	public void setLockedHours(Long lockedHours) {
		this.lockedHours = lockedHours;
	}

	public Long getRemainingHours() {
		return remainingHours;
	}

	public void setRemainingHours(Long remainingHours) {
		this.remainingHours = remainingHours;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public Long getNoOfWeeks() {
		return noOfWeeks;
	}

	public void setNoOfWeeks(Long noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getEndReason() {
		return endReason;
	}

	public void setEndReason(String endReason) {
		this.endReason = endReason;
	}

	public Long getEndedBy() {
		return endedBy;
	}

	public void setEndedBy(Long endedBy) {
		this.endedBy = endedBy;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	public SubscriptionState getState() {
		return state;
	}

	public void setState(SubscriptionState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Subscription [teacherId=" + teacherId + ", studentId=" + studentId + ", planId=" + planId + ", model="
				+ model + ", schedule=" + schedule + ", offeringId=" + offeringId + ", totalHours=" + totalHours
				+ ", consumedHours=" + consumedHours + ", lockedHours=" + lockedHours + ", remainingHours="
				+ remainingHours + ", subject=" + subject + ", target=" + target + ", grade=" + grade + ", hourlyRate="
				+ hourlyRate + ", title=" + title + ", startDate=" + startDate + ", boardId=" + boardId + ", noOfWeeks="
				+ noOfWeeks + ", endTime=" + endTime + ", endReason=" + endReason + ", endedBy=" + endedBy + ", note="
				+ note + ", subModel=" + subModel + ", state=" + state + "]";
	}
}
