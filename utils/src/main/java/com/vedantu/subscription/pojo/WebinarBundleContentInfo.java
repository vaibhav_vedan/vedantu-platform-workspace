package com.vedantu.subscription.pojo;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.SessionPojoWithRefId;

/**
 * Created by somil on 13/05/17.
 */
public class WebinarBundleContentInfo extends BaseBundleContentInfo {
    private String thumbnailUrl;

    private String presenter;
    private long startTime;
    private long endTime;
    private OTFSessionToolType sessionToolType;
    private String referenceId;

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }


    public String getPresenter() {
        return presenter;
    }

    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public void updateSessionResponse(SessionPojoWithRefId sessionPojoWithRefId) {
		this.startTime = sessionPojoWithRefId.getStartTime();
		this.endTime = sessionPojoWithRefId.getEndTime();
		this.presenter = sessionPojoWithRefId.getPresenter();
		this.sessionToolType = sessionPojoWithRefId.getSessionToolType();
		this.setContentId(sessionPojoWithRefId.getId());

		this.setTitle(sessionPojoWithRefId.getTitle());
		this.setDescription(sessionPojoWithRefId.getDescription());
	}

	@Override
	public String toString() {
		return "WebinarBundleContentInfo [thumbnailUrl=" + thumbnailUrl + ", presenter=" + presenter + ", startTime="
				+ startTime + ", endTime=" + endTime + ", sessionToolType=" + sessionToolType + ", referenceId="
				+ referenceId + ", toString()=" + super.toString() + "]";
	}
}
