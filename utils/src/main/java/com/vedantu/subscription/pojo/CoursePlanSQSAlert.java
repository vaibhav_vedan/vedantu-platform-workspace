/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

/**
 *
 * @author parashar
 */
public class CoursePlanSQSAlert {
    private String userId;
    private String studentEmailId;
    private String teacherEmailId;
    private String studentContactNumber;
    private String teacherContactNumber;
    private Long sessionEndTime;

    /**
     * @return the studentEmailId
     */
    public String getStudentEmailId() {
        return studentEmailId;
    }

    /**
     * @param studentEmailId the studentEmailId to set
     */
    public void setStudentEmailId(String studentEmailId) {
        this.studentEmailId = studentEmailId;
    }

    /**
     * @return the teacherEmailId
     */
    public String getTeacherEmailId() {
        return teacherEmailId;
    }

    /**
     * @param teacherEmailId the teacherEmailId to set
     */
    public void setTeacherEmailId(String teacherEmailId) {
        this.teacherEmailId = teacherEmailId;
    }

    /**
     * @return the studentContactNumber
     */
    public String getStudentContactNumber() {
        return studentContactNumber;
    }

    /**
     * @param studentContactNumber the studentContactNumber to set
     */
    public void setStudentContactNumber(String studentContactNumber) {
        this.studentContactNumber = studentContactNumber;
    }

    /**
     * @return the teacherContactNumber
     */
    public String getTeacherContactNumber() {
        return teacherContactNumber;
    }

    /**
     * @param teacherContactNumber the teacherContactNumber to set
     */
    public void setTeacherContactNumber(String teacherContactNumber) {
        this.teacherContactNumber = teacherContactNumber;
    }

    /**
     * @return the sessionEndTime
     */
    public Long getSessionEndTime() {
        return sessionEndTime;
    }

    /**
     * @param sessionEndTime the sessionEndTime to set
     */
    public void setSessionEndTime(Long sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    
    
}
