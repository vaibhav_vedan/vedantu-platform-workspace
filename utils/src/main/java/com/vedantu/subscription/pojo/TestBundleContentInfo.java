package com.vedantu.subscription.pojo;

/**
 * Created by somil on 13/05/17.
 */
public class TestBundleContentInfo extends BaseBundleContentInfo {
    private Integer duration;
    private Long teacherId;
    private String thumbnailUrl;
    private Boolean isActive;



    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}
