package com.vedantu.subscription.pojo;

import java.util.List;
import java.util.Map;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.BundleEntityInfo;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.subscription.response.BaseSubscriptionPlanResp;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by somil on 09/05/17.
 */
@Data
@NoArgsConstructor
public class BundleInfo extends AbstractMongoStringIdEntityBean {

    private List<Track> webinarCategories;
    private VideoContentData videos;
    private TestContentData tests;
    private String title;
    private List<String> subject;
    private List<String> target;
    private List<Integer> grade;
    private Long boardId;
    private BundleState state = BundleState.DRAFT;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<String> tags;
    private List<String> teacherIds;
    private Integer noOfWebinars;
    private Integer noOfVideos;
    private Integer noOfTests;
    private List<UserBasicInfo> userBasicInfos;
    private List<String> courses;
    private List<BaseInstalmentInfo> instalmentDetails;
    private Map<String, Object> metadata;
    private int trailAmount;
    private int trailAllowedDays;
    private Boolean trailIncluded ;
    private Long validTill ;
    private int validDays ;
    private Boolean tabletIncluded ;
    private Boolean amIncluded ;
    private Boolean unLimitedDoubtsIncluded ;
    private List<String> suggestionPackages;
    @Deprecated
    private List<AioPackage> packages;
    private Boolean isTrail = false;
    private List<CourseTerm> searchTerms;
    private Boolean isRankBooster;
    private BundleDetails bundleDetails;
    private Boolean isInstalmentVisible;
    @Getter(AccessLevel.NONE)
    private Boolean isSubscription = Boolean.FALSE ;
    private DurationTime durationtime;
    private Long lastPurchaseDate;
    private Integer minimumPurchasePrice;
    private Long windowRight;
    private List<TrialData> trialData;
    private boolean earlyLearning;
    private String orgId;
    private List<BundleEntityInfo> bundleEntityInfos;
    private List<BaseSubscriptionPlanResp>  baseSubscriptionPlan;
    private List<String> displaySubject;
    private Boolean isVassist;
    private boolean packagesExists;//added for caching data

    public Boolean getIsSubscription() {
        return isSubscription != null && isSubscription;
    }

}
