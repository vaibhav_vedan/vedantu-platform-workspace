/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

/**
 *
 * @author pranavm
 */
public class BatchIdCourseTitlePojo {

    private String batchId;
    private String courseId;
    private String courseTitle;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    @Override
    public String toString() {
        return "BatchIdCourseTitlePojo{" + "batchId=" + batchId + ", courseId=" + courseId + ", courseTitle=" + courseTitle + '}';
    }

}
