package com.vedantu.subscription.pojo;

import com.vedantu.scheduling.pojo.session.OTMSessionType;
import java.util.List;

public class SessionSnapshot {

    private String sessionId;
    private Long startTime;
    private Long endTime;
    private List<String> batchIds;
    private OTMSessionType oTMSessionType;

    public SessionSnapshot() {
    }

    public SessionSnapshot(String sessionId, Long startTime, Long endTime, List<String> batchIds, OTMSessionType oTMSessionType) {
        this.sessionId = sessionId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.batchIds = batchIds;
        this.oTMSessionType = oTMSessionType;
    }

    @Override
    public String toString() {
        return "SessionSnapshot{" + "sessionId=" + sessionId + ", startTime=" + startTime + ", endTime=" + endTime + ", batchIds=" + batchIds + ", oTMSessionType=" + oTMSessionType + '}';
    }

    public boolean isTrialSnapShot() {
        return this.oTMSessionType == OTMSessionType.TRIAL || this.oTMSessionType == OTMSessionType.EXTRA_TRIAL;
    }

    
    
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the batchIds
     */
    public List<String> getBatchIds() {
        return batchIds;
    }

    /**
     * @param batchIds the batchIds to set
     */
    public void setBatchIds(List<String> batchIds) {
        this.batchIds = batchIds;
    }

    /**
     * @return the oTMSessionType
     */
    public OTMSessionType getoTMSessionType() {
        return oTMSessionType;
    }

    /**
     * @param oTMSessionType the oTMSessionType to set
     */
    public void setoTMSessionType(OTMSessionType oTMSessionType) {
        this.oTMSessionType = oTMSessionType;
    }
}
