package com.vedantu.subscription.pojo;

import java.util.Set;

public class TeacherCourseStateCount {
	
	private Long teacherId;
	private Set<Long> endedStudentIds;
	private Integer published = 0;
	private Integer enrolled = 0;
	private Integer ended = 0;
	
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public Set<Long> getEndedStudentIds() {
		return endedStudentIds;
	}
	public void setEndedStudentIds(Set<Long> endedStudentIds) {
		this.endedStudentIds = endedStudentIds;
	}
	public Integer getPublished() {
		return published;
	}
	public void setPublished(Integer published) {
		this.published = published;
	}
	public Integer getEnrolled() {
		return enrolled;
	}
	public void setEnrolled(Integer enrolled) {
		this.enrolled = enrolled;
	}
	public Integer getEnded() {
		return ended;
	}
	public void setEnded(Integer ended) {
		this.ended = ended;
	}
}
