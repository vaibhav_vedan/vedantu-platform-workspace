package com.vedantu.subscription.pojo;

/**
 * Created by somil on 13/04/17.
 */
public class ExportCoursePlan {
    private String email;//
    private String grade;//
    private String id;//
    private String coursePlanTitle;//
    private String coursePlanSendDate;//
    private String registrationDate;
    private int registrationAmount;//
    private String firstSessionTime;//
    private int trialSessionsScheduled;
    private int trialSessionsCancelled;
    private int trialSessionsExpired;
    private int trialSessionsForfeited;
    private int trialSessionsEnded;
    private int trialSessionsTotal;
    private int firstInstalmentAmount; //
    private String firstInstalmentPaymentDate; //
    private int bulkAmount; //
    private String bulkPaymentDate; //
    private String teacherName;//
    private String teacherEmail;//
    private String state; //
    private String entityType;
    private String enrolmentStatus;
    private String parentCourseId;
    private String instalmentPaidDate;
    private String bulkPaidDate;
    private String endType;
    private String extTransactionDate;
    private Integer noOfPaidInstalments;
    private String instalmentDueDate;
    private Integer instalmentAmount;
    private String batchId;
    private String studentName;
    private String phoneCode;
    private String contactNumber;
    private String endTime;
    private String lastInstalmentDate;
    private String createdBy;
    private String creationTime;


    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLastInstalmentDate() {
        return lastInstalmentDate;
    }

    public void setLastInstalmentDate(String lastInstalmentDate) {
        this.lastInstalmentDate = lastInstalmentDate;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Integer getNoOfPaidInstalments() {
        return noOfPaidInstalments;
    }

    public void setNoOfPaidInstalments(Integer noOfPaidInstalments) {
        this.noOfPaidInstalments = noOfPaidInstalments;
    }

    public String getInstalmentDueDate() {
        return instalmentDueDate;
    }

    public void setInstalmentDueDate(String instalmentDueDate) {
        this.instalmentDueDate = instalmentDueDate;
    }

    public Integer getInstalmentAmount() {
        return instalmentAmount;
    }

    public void setInstalmentAmount(Integer instalmentAmount) {
        this.instalmentAmount = instalmentAmount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getCoursePlanTitle() {
        return coursePlanTitle;
    }

    public void setCoursePlanTitle(String coursePlanTitle) {
        this.coursePlanTitle = coursePlanTitle;
    }

    public String getCoursePlanSendDate() {
        return coursePlanSendDate;
    }

    public void setCoursePlanSendDate(String coursePlanSendDate) {
        this.coursePlanSendDate = coursePlanSendDate;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getRegistrationAmount() {
        return registrationAmount;
    }

    public void setRegistrationAmount(int registrationAmount) {
        this.registrationAmount = registrationAmount;
    }

    public String getFirstSessionTime() {
        return firstSessionTime;
    }

    public void setFirstSessionTime(String firstSessionTime) {
        this.firstSessionTime = firstSessionTime;
    }

    public int getTrialSessionsScheduled() {
        return trialSessionsScheduled;
    }

    public void setTrialSessionsScheduled(int trialSessionsScheduled) {
        this.trialSessionsScheduled = trialSessionsScheduled;
    }

    public int getTrialSessionsCancelled() {
        return trialSessionsCancelled;
    }

    public void setTrialSessionsCancelled(int trialSessionsCancelled) {
        this.trialSessionsCancelled = trialSessionsCancelled;
    }

    public int getTrialSessionsExpired() {
        return trialSessionsExpired;
    }

    public void setTrialSessionsExpired(int trialSessionsExpired) {
        this.trialSessionsExpired = trialSessionsExpired;
    }

    public int getTrialSessionsEnded() {
        return trialSessionsEnded;
    }

    public void setTrialSessionsEnded(int trialSessionsEnded) {
        this.trialSessionsEnded = trialSessionsEnded;
    }

    public int getFirstInstalmentAmount() {
        return firstInstalmentAmount;
    }

    public void setFirstInstalmentAmount(int firstInstalmentAmount) {
        this.firstInstalmentAmount = firstInstalmentAmount;
    }

    public String getFirstInstalmentPaymentDate() {
        return firstInstalmentPaymentDate;
    }

    public void setFirstInstalmentPaymentDate(String firstInstalmentPaymentDate) {
        this.firstInstalmentPaymentDate = firstInstalmentPaymentDate;
    }

    public int getBulkAmount() {
        return bulkAmount;
    }

    public void setBulkAmount(int bulkAmount) {
        this.bulkAmount = bulkAmount;
    }

    public String getBulkPaymentDate() {
        return bulkPaymentDate;
    }

    public void setBulkPaymentDate(String bulkPaymentDate) {
        this.bulkPaymentDate = bulkPaymentDate;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEnrolmentStatus() {
        return enrolmentStatus;
    }

    public void setEnrolmentStatus(String enrolmentStatus) {
        this.enrolmentStatus = enrolmentStatus;
    }

    public String getParentCourseId() {
        return parentCourseId;
    }

    public void setParentCourseId(String parentCourseId) {
        this.parentCourseId = parentCourseId;
    }

    public int getTrialSessionsTotal() {
        return trialSessionsTotal;
    }

    public void setTrialSessionsTotal(int trialSessionsTotal) {
        this.trialSessionsTotal = trialSessionsTotal;
    }

    public String getInstalmentPaidDate() {
        return instalmentPaidDate;
    }

    public void setInstalmentPaidDate(String instalmentPaidDate) {
        this.instalmentPaidDate = instalmentPaidDate;
    }

    public String getBulkPaidDate() {
        return bulkPaidDate;
    }

    public void setBulkPaidDate(String bulkPaidDate) {
        this.bulkPaidDate = bulkPaidDate;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getEndType() {
        return endType;
    }

    public void setEndType(String endType) {
        this.endType = endType;
    }

    public String getExtTransactionDate() {
        return extTransactionDate;
    }

    public void setExtTransactionDate(String extTransactionDate) {
        this.extTransactionDate = extTransactionDate;
    }

    public int getTrialSessionsForfeited() {
        return trialSessionsForfeited;
    }

    public void setTrialSessionsForfeited(int trialSessionsForfeited) {
        this.trialSessionsForfeited = trialSessionsForfeited;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return "ExportCoursePlan{" +
                "email='" + email + '\'' +
                ", grade='" + grade + '\'' +
                ", id='" + id + '\'' +
                ", coursePlanTitle='" + coursePlanTitle + '\'' +
                ", coursePlanSendDate='" + coursePlanSendDate + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", registrationAmount=" + registrationAmount +
                ", firstSessionTime='" + firstSessionTime + '\'' +
                ", trialSessionsScheduled=" + trialSessionsScheduled +
                ", trialSessionsCancelled=" + trialSessionsCancelled +
                ", trialSessionsExpired=" + trialSessionsExpired +
                ", trialSessionsEnded=" + trialSessionsEnded +
                ", instalmentAmount=" + instalmentAmount +
                ", firstInstalmentAmount=" + firstInstalmentAmount +
                ", firstInstalmentPaymentDate='" + firstInstalmentPaymentDate + '\'' +
                ", bulkAmount=" + bulkAmount +
                ", bulkPaymentDate='" + bulkPaymentDate + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", state='" + state + '\'' +
                ", entityType='" + entityType + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", creationTime='" + creationTime + '\'' +
                '}';
    }
}
