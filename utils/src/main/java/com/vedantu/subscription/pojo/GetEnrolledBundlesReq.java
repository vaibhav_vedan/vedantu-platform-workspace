package com.vedantu.subscription.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetEnrolledBundlesReq
{
    private String userId;
    private List<String> bundleIds;
}
