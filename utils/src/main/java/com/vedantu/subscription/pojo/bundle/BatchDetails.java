package com.vedantu.subscription.pojo.bundle;

import com.vedantu.subscription.enums.bundle.CourseType;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.*;

public class BatchDetails {

    private LinkedHashSet<Classes> courseSyllabus;
    private long coursePeriodMillis;
    @Indexed(background = true)
    private CourseType courseType;
    @Indexed(background = true)
    private long courseStartTime;
    // USED FOR MICRO_COURSES
    private Map<String, Object> teacherInfo;

    public LinkedHashSet<Classes> getCourseSyllabus() {
        return courseSyllabus == null ? new LinkedHashSet<>() : courseSyllabus;
    }

    public void setCourseSyllabus(LinkedHashSet<Classes> courseSyllabus) {
        this.courseSyllabus = courseSyllabus;
    }

    public long getCoursePeriodMillis() {
        return coursePeriodMillis;
    }

    public void setCoursePeriodMillis(long coursePeriodMillis) {
        this.coursePeriodMillis = coursePeriodMillis;
    }

    public Map<String, Object> getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(Map<String, Object> teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    public long getCourseStartTime() {
        return courseStartTime;
    }

    public void setCourseStartTime(long courseStartTime) {
        this.courseStartTime = courseStartTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BatchDetails.class.getSimpleName() + "[", "]")
                .add("courseSyllabus=" + courseSyllabus)
                .add("coursePeriodMillis=" + coursePeriodMillis)
                .add("courseType=" + courseType)
                .add("courseStartTime=" + courseStartTime)
                .add("teacherInfo=" + teacherInfo)
                .toString();
    }
}
