/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

import java.util.List;

/**
 *
 * @author ajith
 */
public class OTOCurriculumPojo {

    private String name;
    private List<OTOCurriculumPojo> children;
    private List<ContentInfo> contents;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OTOCurriculumPojo> getChildren() {
        return children;
    }

    public void setChildren(List<OTOCurriculumPojo> children) {
        this.children = children;
    }

    public List<ContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<ContentInfo> contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "OTOCurriculumPojo{" + "name=" + name + ", children=" + children + ", contents=" + contents + '}';
    }

}
