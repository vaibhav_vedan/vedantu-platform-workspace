package com.vedantu.subscription.pojo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RewardDistributionInfo {

    @EqualsAndHashCode.Include
    private String gameId;

    private int count;
    private int distributionRatio;
}
