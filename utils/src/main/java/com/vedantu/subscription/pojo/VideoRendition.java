package com.vedantu.subscription.pojo;

/**
 * Created by somil on 13/05/17.
 */
public class VideoRendition {
    private Integer width;
    private Integer height;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
