package com.vedantu.subscription.pojo;

import com.vedantu.subscription.enums.ContentPriceType;
import com.vedantu.lms.cmds.enums.ContentType;

import java.util.List;
import java.util.Map;

/**
 * Created by somil on 09/05/17.
 */
public class BaseBundleContentInfo {
    private String title;
    //private Long teacherId;
    private String description;
    private ContentType contentType;
    private String contentId;
    private String url;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private ContentPriceType type;
    private Map<String, String> keyValues;
    private List<String> tags;
    private List<String> categoryTags;
    private boolean featured;
    private boolean locked;
    private Integer priority;

    public BaseBundleContentInfo() {
		super();
	}

	public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<String> getCategoryTags() {
        return categoryTags;
    }

    public void setCategoryTags(List<String> categoryTags) {
        this.categoryTags = categoryTags;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public ContentPriceType getType() {
        return type;
    }

    public void setType(ContentPriceType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

	@Override
	public String toString() {
		return "BaseBundleContentInfo [title=" + title + ", description=" + description + ", contentType=" + contentType
				+ ", contentId=" + contentId + ", url=" + url + ", subject=" + subject + ", target=" + target
				+ ", grade=" + grade + ", boardId=" + boardId + ", type=" + type + ", keyValues=" + keyValues
				+ ", tags=" + tags + ", categoryTags=" + categoryTags + ", featured=" + featured + ", locked=" + locked
				+ ", priority=" + priority + ", toString()=" + super.toString() + "]";
	}
}
