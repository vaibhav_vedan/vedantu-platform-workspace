package com.vedantu.subscription.pojo;

import com.vedantu.subscription.enums.CoursePlanEnums;

public class CourseStateChangeTime {

	private Long changeTime;
	private CoursePlanEnums.CoursePlanState newState;
	private Long changedBy;
	private CoursePlanEnums.CoursePlanState previousState;

	public Long getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Long changeTime) {
		this.changeTime = changeTime;
	}

	public CoursePlanEnums.CoursePlanState getNewState() {
		return newState;
	}

	public void setNewState(CoursePlanEnums.CoursePlanState newState) {
		this.newState = newState;
	}

	public Long getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(Long changedBy) {
		this.changedBy = changedBy;
	}

	public CoursePlanEnums.CoursePlanState getPreviousState() {
		return previousState;
	}

	public void setPreviousState(CoursePlanEnums.CoursePlanState previousState) {
		this.previousState = previousState;
	}

}
