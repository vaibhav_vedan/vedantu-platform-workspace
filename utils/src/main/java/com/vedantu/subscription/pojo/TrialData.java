package com.vedantu.subscription.pojo;

import com.vedantu.session.pojo.EntityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TrialData {
    private String id;
    private Long cutPrice;
    private Long trialAmount;
    private Long trialAllowedDays;
    private int gracePeriod=7;
    private EntityType contextType;
    private String contextId;
}
