/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

/**
 *
 * @author ajith
 */
public class BundlePricingPojo {

    private int itemCount;
    private Integer cutPrice;
    private Integer price;

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public Integer getCutPrice() {
        return cutPrice;
    }

    public void setCutPrice(Integer cutPrice) {
        this.cutPrice = cutPrice;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BundlePricingPojo{" + "itemCount=" + itemCount + ", cutPrice=" + cutPrice + ", price=" + price + '}';
    }

}
