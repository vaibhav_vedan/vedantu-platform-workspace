package com.vedantu.subscription.pojo;

public class SubscriptionMetric {

	private String status;
	private String date;
	private Long subscriptionId;
	private String model;
	private String purchasedHours;
	private Double amount;
	private Long studentId;
	private Long teacherId;

	public SubscriptionMetric() {
		super();
	}

	public SubscriptionMetric(String status, String date, Long subscriptionId, String model, String purchasedHours,
			Double amount, Long studentId, Long teacherId) {
		super();
		this.status = status;
		this.date = date;
		this.subscriptionId = subscriptionId;
		this.model = model;
		this.purchasedHours = purchasedHours;
		this.amount = amount;
		this.studentId = studentId;
		this.teacherId = teacherId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getPurchasedHours() {
		return purchasedHours;
	}

	public void setPurchasedHours(String purchasedHours) {
		this.purchasedHours = purchasedHours;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Override
	public String toString() {
		return "SubscriptionMetric [status=" + status + ", date=" + date + ", subscriptionId=" + subscriptionId
				+ ", model=" + model + ", purchasedHours=" + purchasedHours + ", amount=" + amount + ", studentId="
				+ studentId + ", teacherId=" + teacherId + "]";
	}
}
