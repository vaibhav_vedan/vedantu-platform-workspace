package com.vedantu.subscription.pojo.game;

import lombok.Data;

/**
 * @author mano
 */
@Data
public class GameRewardPojo {
    private String name;
    private int distributionRatio;
}
