package com.vedantu.subscription.pojo.bundle;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.util.ArrayUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AioPackage {
    private Integer grade;
    private PackageType packageType;
    private String entityId;
    private EnrollmentType enrollmentType;
    private BatchDetails batchDetails;
    private List<String> subjects;
    private Set<String> targets;
    private String courseId;
    private  Boolean recordedVideo;
    private  Long startTime;
    private  Long endTime;
    private String courseTitle;
    private List<CourseTerm> searchTerms;
    private List<String> mainTags;
    private Integer courseCount;
    private Language language = Language.DEFAULT;
    private List<SubscriptionPackageType> subscriptionPackageTypes;



    public AioPackage(Integer grade, PackageType packageType, String entityId, EnrollmentType enrollmentType) {
        this.grade = grade;
        this.packageType = packageType;
        this.entityId = entityId;
        this.enrollmentType = enrollmentType;
    }

    public AioPackage() {

    }

    public List<CourseTerm> getSearchTerms() {
        if(ArrayUtils.isEmpty(searchTerms)){
            searchTerms = new ArrayList<>();
        }
        return searchTerms;
    }

    public void setSearchTerms(List<CourseTerm> searchTerms) {
        this.searchTerms = searchTerms;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EnrollmentType getEnrollmentType() {
        return enrollmentType;
    }

    public void setEnrollmentType(EnrollmentType enrollmentType) {
        this.enrollmentType = enrollmentType;
    }

    public BatchDetails getBatchDetails() {
        return batchDetails;
    }

    public void setBatchDetails(BatchDetails batchDetails) {
        this.batchDetails = batchDetails;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public Boolean getRecordedVideo() {
        return recordedVideo;
    }

    public void setRecordedVideo(Boolean includeRecorded) {
        this.recordedVideo = includeRecorded;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public List<String> getMainTags() {
        if(ArrayUtils.isEmpty(mainTags)){
            mainTags = new ArrayList<>();
        }

        return mainTags;

    }

    public void setMainTags(List<String> mainTags) {
        this.mainTags = mainTags;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public Integer getCourseCount() {
        return courseCount;
    }

    public void setCourseCount(Integer courseCount) {
        this.courseCount = courseCount;
    }

    public Set<String> getTargets() {
        return targets;
    }

    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Language getLanguage() {
        return language;
    }

    public void setSubscriptionPackageTypes(List<SubscriptionPackageType> subscriptionPackageTypes) {
        this.subscriptionPackageTypes = subscriptionPackageTypes;
    }

    public List<SubscriptionPackageType> getSubscriptionPackageTypes() {
        return subscriptionPackageTypes;
    }

    @Override
    public String toString() {
        return "AioPackage{" +
                "grade=" + grade +
                ", packageType=" + packageType +
                ", entityId='" + entityId + '\'' +
                ", enrollmentType=" + enrollmentType +
                ", batchDetails=" + batchDetails +
                ", subjects=" + subjects +
                ", targets=" + targets +
                ", courseId='" + courseId + '\'' +
                ", recordedVideo=" + recordedVideo +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", courseTitle='" + courseTitle + '\'' +
                ", searchTerms=" + searchTerms +
                ", mainTags=" + mainTags +
                ", courseCount=" + courseCount +
                ", language=" + language +
                ", subscriptionPackageTypes=" + subscriptionPackageTypes +
                '}';
    }
}
