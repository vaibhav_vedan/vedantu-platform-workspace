package com.vedantu.subscription.pojo;

import com.vedantu.onetofew.enums.EntityStatus;

import java.util.List;

/**
 * Created by somil on 09/05/17.
 */
public class TestContentData {

    private List<TestContentData> children;
    private List<TestBundleContentInfo> contents;
    private String title;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;
    private Long validFrom;
    private Long validTill;


    private String bundleId;
    private EntityStatus testContentEntityStatus;

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public EntityStatus getTestContentEntityStatus() {
        return testContentEntityStatus;
    }

    public void setTestContentEntityStatus(EntityStatus testContentEntityStatus) {
        this.testContentEntityStatus = testContentEntityStatus;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    //lowest is highest
    private Integer priority;

    public Integer getPriority() {
        return priority;
    }

    public List<TestContentData> getChildren() {
        return children;
    }

    public void setChildren(List<TestContentData> children) {
        this.children = children;
    }

    public List<TestBundleContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<TestBundleContentInfo> contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    public Long getValidTill() {
        return validTill;
    }

    @Override
    public String toString() {
        return "TestContentData{" + "children=" + children + ", contents=" + contents + ", title=" + title + ", subject=" + subject + ", target=" + target + ", grade=" + grade + ", boardId=" + boardId + ", description=" + description + ", priority=" + priority + '}';
    }

}
