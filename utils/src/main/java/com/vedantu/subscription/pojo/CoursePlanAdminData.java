package com.vedantu.subscription.pojo;

/**
 * Created by somil on 07/04/17.
 */
public class CoursePlanAdminData {
    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
