package com.vedantu.subscription.pojo.game;

import com.vedantu.subscription.enums.game.GameTaskStatus;
import lombok.Data;

@Data
public class GameTaskHistory {

    private GameTaskStatus prevStatus;
    private GameTaskStatus currStatus;
    private String awb;
    private Long updatedTime;
}
