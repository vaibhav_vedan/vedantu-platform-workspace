package com.vedantu.subscription.pojo;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import java.util.List;
import java.util.Map;

/**
 * Created by somil on 13/05/17.
 */
public class BundlePackageDetailsInfo extends AbstractMongoStringIdEntityBean {
    private List<BundleInfo> entities;
    private String title;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private List<String> tags;
    private boolean featured;
    //lowest is highest
    private Integer priority;
    private List<String> categoryTags;
    private List<CourseTerm> searchTerms;
    private VisibilityState visibilityState = VisibilityState.VISIBLE;


    public List<BundleInfo> getEntities() {
        return entities;
    }

    public void setEntities(List<BundleInfo> entities) {
        this.entities = entities;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<String> getCategoryTags() {
        return categoryTags;
    }

    public void setCategoryTags(List<String> categoryTags) {
        this.categoryTags = categoryTags;
    }

	public Integer getCutPrice() {
		return cutPrice;
	}

	public void setCutPrice(Integer cutPrice) {
		this.cutPrice = cutPrice;
	}

    public List<CourseTerm> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<CourseTerm> searchTerms) {
        this.searchTerms = searchTerms;
    }

    /**
     * @return the visibilityState
     */
    public VisibilityState getVisibilityState() {
        return visibilityState;
    }

    /**
     * @param visibilityState the visibilityState to set
     */
    public void setVisibilityState(VisibilityState visibilityState) {
        this.visibilityState = visibilityState;
    }
        
}
