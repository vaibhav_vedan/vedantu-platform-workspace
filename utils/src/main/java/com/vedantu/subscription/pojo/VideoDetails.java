package com.vedantu.subscription.pojo;

import java.util.List;

/**
 * Created by somil on 13/05/17.
 */
public class VideoDetails {
    private String id;
    private String title;
    private String thumbnail_url;
    private String description;
    private Float duration;
    private String embed_code;
    private List<String> categories;
    private List<VideoRendition> legacy_renditions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public String getEmbed_code() {
        return embed_code;
    }

    public void setEmbed_code(String embed_code) {
        this.embed_code = embed_code;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }


    public List<VideoRendition> getLegacy_renditions() {
        return legacy_renditions;
    }

    public void setLegacy_renditions(List<VideoRendition> legacy_renditions) {
        this.legacy_renditions = legacy_renditions;
    }
}
