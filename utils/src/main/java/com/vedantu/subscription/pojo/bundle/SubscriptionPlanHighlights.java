package com.vedantu.subscription.pojo.bundle;

import com.vedantu.subscription.enums.SubscriptionPlanType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;

import java.util.List;


/**
 * Created by Dpadhya
 */

@Data
public class SubscriptionPlanHighlights extends AbstractMongoStringIdEntity {

    private String name;
    private List<String> highLights;
    private Integer rank;

}
