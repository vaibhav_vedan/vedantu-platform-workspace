package com.vedantu.subscription.pojo.bundle;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;
import java.util.Set;

@Data
public class BundleEntityInfo {
    private String entityId;
    private EnrollmentType enrollmentType;
    private String courseTitle;
}
