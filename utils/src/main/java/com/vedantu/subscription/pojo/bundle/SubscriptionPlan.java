package com.vedantu.subscription.pojo.bundle;

import com.amazonaws.services.budgets.model.SubscriptionType;
import com.vedantu.subscription.enums.SubscriptionPlanType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by Dpadhya
 */

@Data
public class SubscriptionPlan extends AbstractMongoStringIdEntity {

    private String bundleId;
    private SubscriptionPlanType planType;
    private Boolean isTrial;
    private Integer price;
    private Integer cutPrice;


}
