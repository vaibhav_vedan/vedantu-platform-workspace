package com.vedantu.subscription.pojo.game;

import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.*;

/**
 * @author mano
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GameSessionActivity extends AbstractMongoStringIdEntityBean {

    public enum TaskType {DOWNLOAD, REPLAY, SESSION_ATTENDANCE}

    private String sessionId;
    private Long userId;
    private TaskType taskType;
    private Long timeInSession;
    private Long sessionEndTime;
    private Long sessionStartTime;
    private long lastReplayWatchedDuration;
    private long lastReplayWatchedTime;
    private long sessionNotesDownloadTime;
    private String enrollmentId;
    private boolean attended;
    private OTMSessionType sessionType;

}
