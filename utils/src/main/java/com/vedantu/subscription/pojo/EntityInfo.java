package com.vedantu.subscription.pojo;

import com.vedantu.session.pojo.EntityType;

/**
 * Created by somil on 10/05/17.
 */
public class EntityInfo {
    private EntityType entityType;
    private String entityId;

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

	public static class Constants {
		public static final String ENTITY_TYPE = "entityType";
		public static final String ENTITY_ID = "entityId";
	}

	@Override
	public String toString() {
		return "EntityInfo [entityType=" + entityType + ", entityId=" + entityId + "]";
	}
}
