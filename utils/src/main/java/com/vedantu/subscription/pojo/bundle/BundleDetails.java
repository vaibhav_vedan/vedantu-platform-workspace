package com.vedantu.subscription.pojo.bundle;

import com.vedantu.subscription.enums.bundle.CourseType;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.StringJoiner;

/**
 * @author mano
 */
public class BundleDetails {
    private String bundleImageUrl;
    private Long position;
    private LinkedHashSet<String> displayTags;
    private int displayTagsSize = 0;
    private Map<String, Integer> displayTagsRanks = new HashMap<>();
    private String promotionTag;
    @Indexed(background = true)
    private CourseType courseType;
    @Indexed(background = true)
    private long courseStartTime;

    public LinkedHashSet<String> getDisplayTags() {
        this.displayTags = displayTags == null ? new LinkedHashSet<>() : displayTags;
        this.displayTagsSize = displayTags.size();
        return displayTags;
    }

    public void setDisplayTags(LinkedHashSet<String> displayTags) {
        this.displayTags = displayTags;
        this.displayTagsSize = this.displayTags != null ? this.displayTags.size() : 0;
    }

    public int getDisplayTagsSize() {
        this.displayTagsSize = this.displayTags != null ? this.displayTags.size() : 0;
        return this.displayTagsSize;
    }

    private void setDisplayTagsSize(int displayTagsSize) {
        this.displayTagsSize = displayTagsSize;
    }

    public String getBundleImageUrl() {
        return bundleImageUrl;
    }

    public void setBundleImageUrl(String bundleImageUrl) {
        this.bundleImageUrl = bundleImageUrl;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public String getPromotionTag() {
        return promotionTag;
    }

    public void setPromotionTag(String promotionTag) {
        this.promotionTag = promotionTag;
    }

    public Map<String, Integer> getDisplayTagsRanks() {
        return displayTagsRanks;
    }

    public void setDisplayTagsRanks(Map<String, Integer> displayTagsRanks) {
        this.displayTagsRanks = displayTagsRanks;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    public long getCourseStartTime() {
        return courseStartTime;
    }

    public void setCourseStartTime(long courseStartTime) {
        this.courseStartTime = courseStartTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BundleDetails.class.getSimpleName() + "[", "]")
                .add("bundleImageUrl='" + bundleImageUrl + "'")
                .add("position=" + position)
                .add("displayTags=" + displayTags)
                .add("displayTagsSize=" + displayTagsSize)
                .add("displayTagsRanks=" + displayTagsRanks)
                .add("promotionTag='" + promotionTag + "'")
                .add("courseType='" + courseType + "'")
                .add("courseStartTime='" + courseStartTime + "'")
                .toString();
    }
}
