package com.vedantu.subscription.pojo.section;

import lombok.Data;

@Data
public class BatchSectionPojo {
    String batchId;
    Boolean sectionExists;
}
