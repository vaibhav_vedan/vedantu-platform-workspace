package com.vedantu.subscription.pojo;


import com.vedantu.onetofew.pojo.SessionPlanPojo;

import java.util.List;

public class BatchEnrollmentEmail {
    public String courseName;
    public List<SessionPlanPojo> batchSlots;
    public BatchEnrollmentEmail(String courseName, List<SessionPlanPojo> batchSlots) {
        this.courseName = courseName;
        this.batchSlots = batchSlots;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public List<SessionPlanPojo> getBatchSlots() {
        return batchSlots;
    }

    public void setBatchSlots(List<SessionPlanPojo> batchSlots) {
        this.batchSlots = batchSlots;
    }
}
