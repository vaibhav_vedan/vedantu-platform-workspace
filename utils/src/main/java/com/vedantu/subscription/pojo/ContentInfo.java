/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

/**
 *
 * @author ajith
 */
import com.vedantu.lms.cmds.enums.ContentType;

public class ContentInfo {

    private String title;
    private Long teacherId;
    private String description;
    private ContentType contentType;
    private String url;
    private Boolean shared;
    private String testId;

    public ContentInfo() {
        super();
    }

    public ContentInfo(String title, Long teacherId, String description, ContentType contentType, String url) {
        super();
        this.title = title;
        this.teacherId = teacherId;
        this.description = description;
        this.contentType = contentType;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getShared() {
        return shared;
    }

    public void setShared(Boolean shared) {
        this.shared = shared;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    @Override
    public String toString() {
        return "ContentInfo{" + "title=" + title + ", teacherId=" + teacherId + ", description=" + description + ", contentType=" + contentType + ", url=" + url + ", testId=" + testId +'}';
    }

}
