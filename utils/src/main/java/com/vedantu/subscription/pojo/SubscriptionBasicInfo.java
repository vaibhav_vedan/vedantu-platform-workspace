package com.vedantu.subscription.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.interfaces.IItemDetailInfo;

public class SubscriptionBasicInfo implements IItemDetailInfo {

    public UserBasicInfo teacher;
    public Long hours;
    public String subscriptionName;
    public SessionModel model;

    public SubscriptionBasicInfo() {
        super();
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public UserBasicInfo getTeacher() {
        return teacher;
    }

    public void setTeacher(UserBasicInfo teacher) {
        this.teacher = teacher;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    @Override
    public String toString() {
        return "SubscriptionBasicInfo{" + "teacher=" + teacher + ", hours=" + hours + ", subscriptionName=" + subscriptionName + ", model=" + model + '}';
    }

}
