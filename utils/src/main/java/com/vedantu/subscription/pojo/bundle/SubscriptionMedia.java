package com.vedantu.subscription.pojo.bundle;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;


/**
 * Created by Dpadhya
 */

@Data
public class SubscriptionMedia  {

    private String url;
    private String mediaType;


}
