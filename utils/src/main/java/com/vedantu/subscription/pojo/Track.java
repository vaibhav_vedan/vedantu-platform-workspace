package com.vedantu.subscription.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by somil on 09/05/17.
 */
public class Track {

    private String trackId;
    private String title;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;
    private Map<String, String> keyValues;
    private Long startTime;
    private List<String> tags;
    private List<String> categoryTags;

    private List<WebinarBundleContentInfo> webinars;
    private boolean featured;
    private boolean locked;

    //lowest is highest
    private Integer priority;

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public List<String> getCategoryTags() {
        return categoryTags;
    }

    public void setCategoryTags(List<String> categoryTags) {
        this.categoryTags = categoryTags;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<WebinarBundleContentInfo> getWebinars() {
        return webinars;
    }

    public void setWebinars(List<WebinarBundleContentInfo> webinars) {
        this.webinars = webinars;
    }

    public void addWebinar(WebinarBundleContentInfo webinar) {
        if (this.webinars == null) {
            this.webinars = new ArrayList<>();
        }

        this.webinars.add(webinar);
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    @Override
    public String toString() {
        return "Track [trackId=" + trackId + ", title=" + title + ", subject=" + subject + ", target=" + target
                + ", grade=" + grade + ", boardId=" + boardId + ", description=" + description + ", keyValues="
                + keyValues + ", startTime=" + startTime + ", tags=" + tags + ", categoryTags=" + categoryTags
                + ", webinars=" + webinars + ", featured=" + featured + ", locked=" + locked + ", priority=" + priority
                + ", toString()=" + super.toString() + "]";
    }
}
