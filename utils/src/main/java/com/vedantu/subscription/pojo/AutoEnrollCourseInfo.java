package com.vedantu.subscription.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.BundleEntityInfo;
import com.vedantu.onetofew.pojo.CurriculumPojo;
import com.vedantu.onetofew.pojo.SessionPlanPojo;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class AutoEnrollCourseInfo {
    private String courseId;
    private String batchId;
    private String courseTitle;
    private Long startTime;
    private Long endTime;
    private Set<String> teacherIds;
    private List<SessionPlanPojo> sessionPlan;
    private List<CurriculumPojo> curriculum;
    private Set<String> subject;
}
