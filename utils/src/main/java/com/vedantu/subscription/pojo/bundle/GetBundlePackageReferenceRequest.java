package com.vedantu.subscription.pojo.bundle;

import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class GetBundlePackageReferenceRequest extends AbstractFrontEndListReq {
    private String bundleId;
    @Override
    protected List<String> collectVerificationErrors() {
        final List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(bundleId)){
            errors.add("[bundleId] cann't be null");
        }
        return errors;
    }
}

