package com.vedantu.subscription.pojo;

import com.vedantu.lms.cmds.enums.VideoType;

/**
 * Created by somil on 13/05/17.
 */
public class VideoBundleContentInfo extends BaseBundleContentInfo {
    private Integer duration;
    private Integer width;
    private Integer height;
    private Long teacherId;
    private String thumbnailUrl;
    private String embedCode;
    private VideoType videoType;

    //Following fields are not not part of entity
    private boolean viewed;
    private Integer viewCount;
    private boolean isActive;


    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

    public boolean getViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public String getEmbedCode() {
        return embedCode;
    }

    public void setEmbedCode(String embedCode) {
        this.embedCode = embedCode;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "VideoBundleContentInfo{" +
                "duration=" + duration +
                ", width=" + width +
                ", height=" + height +
                ", teacherId=" + teacherId +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", embedCode='" + embedCode + '\'' +
                ", videoType=" + videoType +
                ", viewed=" + viewed +
                ", viewCount=" + viewCount +
                "} " + super.toString();
    }
}
