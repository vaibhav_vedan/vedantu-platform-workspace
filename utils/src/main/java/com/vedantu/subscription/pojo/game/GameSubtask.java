package com.vedantu.subscription.pojo.game;

import com.vedantu.subscription.enums.game.GameSubtaskName;
import com.vedantu.subscription.enums.game.GameTaskStatus;
import lombok.Data;

import java.util.List;

/**
 * @author anirudha
 */
@Data
public class GameSubtask {

    private GameSubtaskName name;
    private GameTaskStatus status;
    private Long completedTime;
    private List<GameMetaData> meta;

    public GameSubtask() {
    }

    public GameSubtask(GameSubtaskName name, GameTaskStatus status, Long completedTime) {
        this.name = name;
        this.status = status;
        this.completedTime = completedTime;
    }
}
