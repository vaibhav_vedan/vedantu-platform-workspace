package com.vedantu.subscription.pojo.game;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.subscription.enums.game.GameTaskType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.subscription.enums.game.GameSubtaskName;
import com.vedantu.subscription.enums.game.GameTaskStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Optional;

/**
 * @author anirudha
 */
@Data
@NoArgsConstructor
public class GameTask {

    private String name;
    private GameTaskType type;
    private GameTaskStatus status;
    private List<GameSubtask> subtaskList;
    private GameRewardDetails gameRewardDetails;
    private List<GameTaskHistory> gameTaskHistoryList;
    private Long completedTime;

    public GameTask(String name, GameTaskStatus status, List<GameSubtask> subtaskList, GameRewardDetails gameRewardDetails, List<GameTaskHistory> gameTaskHistoryList, Long completedTime) {
        this.name = name;
        this.status = status;
        this.subtaskList = subtaskList;
        this.gameRewardDetails = gameRewardDetails;
        this.gameTaskHistoryList = gameTaskHistoryList;
        this.completedTime = completedTime;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String NAME = "name";
        public static final String STATUS = "status";
        public static final String SUBTASKLIST = "subtaskList";
        public static final String GAME_REWARD_DETAILS = "gameRewardDetails";
        public static final String GAME_HISTORY_DETAILS = "gameTaskHistoryList";
        public static final String COMPLETED_TIME = "completedTime";


    }

    public GameSubtask getGameSubTaskForName(GameSubtaskName name) {
        Optional<GameSubtask> optionalGameSubtask = subtaskList.stream()
                .filter(e -> e.getName() == name)
                .findAny();

        if (optionalGameSubtask.isPresent()) {
            return subtaskList.stream()
                    .filter(e -> e.getName() == name && e.getStatus() != GameTaskStatus.COMPLETE)
                    .findAny()
                    .orElse(null);
        }
        throw new VRuntimeException(ErrorCode.SERVICE_ERROR, String.format("Sub Task - %s not found", name));
    }

}
