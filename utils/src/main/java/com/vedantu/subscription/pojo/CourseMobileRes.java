/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.pojo;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class CourseMobileRes extends AbstractTargetTopicEntity {

    private String title;
    private Long startsFrom;//unix timestamp
    private Integer duration;//milliseconds
    private Integer price;//paisa
    private EntityType type;
    private Long registeredOn;//unix timestamp
    private Set<String> subjects;
    private String webLink;
    private String webinarUrl;
    private String takenBy;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartsFrom() {
        return startsFrom;
    }

    public void setStartsFrom(Long startsFrom) {
        this.startsFrom = startsFrom;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public Long getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(Long registeredOn) {
        this.registeredOn = registeredOn;
    }

    /**
     * @return the subjects
     */
    public Set<String> getSubjects() {
        return subjects;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    /**
     * @return the webinarUrl
     */
    public String getWebinarUrl() {
        return webinarUrl;
    }

    /**
     * @param webinarUrl the webinarUrl to set
     */
    public void setWebinarUrl(String webinarUrl) {
        this.webinarUrl = webinarUrl;
    }

    public String getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(String takenBy) {
        this.takenBy = takenBy;
    }

}
