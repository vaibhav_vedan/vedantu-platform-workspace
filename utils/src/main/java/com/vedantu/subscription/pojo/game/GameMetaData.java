package com.vedantu.subscription.pojo.game;

import com.vedantu.subscription.enums.game.GameEntity;
import lombok.Data;

/**
 * @author mano
 */
@Data
public class GameMetaData {
    private GameEntity entity;
    private String contextId;
    private Long startTime;
}
