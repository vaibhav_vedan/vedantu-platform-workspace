/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetRegisteredCoursesReq extends AbstractFrontEndListReq {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }

}
