package com.vedantu.subscription.request.section;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class TeacherInSectionReq extends AbstractFrontEndReq {
    private String userId;
    private List<String> batchIds;
    private final int LIMIT = 100;
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(ArrayUtils.isEmpty(batchIds))
            errors.add("batchIds is empty");

        if(batchIds.size() > LIMIT)
            errors.add("batchIds size is greater than " + LIMIT);

        if(StringUtils.isEmpty(userId))
            errors.add("userId is empty/null");
        return errors;
    }
}
