package com.vedantu.subscription.request;

import com.vedantu.subscription.pojo.bundle.SubscriptionMedia;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlan;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlanHighlights;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by Dpadhya
 */
@Data
public class addSubscriptionProgramReq extends AbstractFrontEndReq {

    private List<SubscriptionPlan> subscriptionPlans;
    private String name;
    private Integer grade;
    private Integer year;
    private String target;
    private List<SubscriptionMedia> subscriptionMedias;
    private List<String> highLights;
    private List<String> functionality;
    private String offer;
    private String id;
    private Boolean isHidden;
    private List<SubscriptionPlanHighlights> subscriptionPlanHighlights;

}
