/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CoursePlanReqFormReq extends AbstractFrontEndReq {

    private List<CoursePlanCompactSchedule> compactSchedule;//only for display, no business logic
    private String requirement;
    private Long studentId;
    private Long teacherId;
    private Integer hours;

    public List<CoursePlanCompactSchedule> getCompactSchedule() {
        return compactSchedule;
    }

    public void setCompactSchedule(List<CoursePlanCompactSchedule> compactSchedule) {
        this.compactSchedule = compactSchedule;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }
    
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (studentId == null) {
            errors.add("studentId");
        }        
        return errors;
    }

}
