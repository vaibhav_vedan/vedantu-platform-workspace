package com.vedantu.subscription.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetBundlePackagesReq extends AbstractFrontEndListReq {

    private List<String> bundleIds;
    private List<String> tags;
    private List<String> entityId;

    public GetBundlePackagesReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    public List<String> getBundleIds() {
        return bundleIds;
    }

    public void setBundleIds(List<String> bundleIds) {
        this.bundleIds = bundleIds;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

	public List<String> getEntityId() {
		return entityId;
	}

	public void setEntityId(List<String> entityId) {
		this.entityId = entityId;
	}

}
