package com.vedantu.subscription.request;

import java.util.List;

public class GTTAttendeeRequest {
    List<String> userIds;
    Long fromTime;
    Long thruTime;

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getThruTime() {
        return thruTime;
    }

    public void setThruTime(Long thruTime) {
        this.thruTime = thruTime;
    }
}
