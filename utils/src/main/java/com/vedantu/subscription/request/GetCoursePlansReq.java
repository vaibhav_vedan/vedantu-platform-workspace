/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetCoursePlansReq extends AbstractFrontEndListReq {

    private Long studentId;
    private Long teacherId;
    private List<CoursePlanEnums.CoursePlanState> states;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private Long fromRegularSessionStartDate;
    private Long tillRegularSessionStartDate;
    private Long fromStartDate;
    private Long tillStartDate;
    private Boolean onlyBasicDetails = true;
    private Long lastUpdated;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public List<CoursePlanEnums.CoursePlanState> getStates() {
        return states;
    }

    public void setStates(List<CoursePlanEnums.CoursePlanState> states) {
        this.states = states;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Boolean getOnlyBasicDetails() {
        return onlyBasicDetails;
    }

    public void setOnlyBasicDetails(Boolean onlyBasicDetails) {
        this.onlyBasicDetails = onlyBasicDetails;
    }

    public Long getFromRegularSessionStartDate() {
        return fromRegularSessionStartDate;
    }

    public void setFromRegularSessionStartDate(Long fromRegularSessionStartDate) {
        this.fromRegularSessionStartDate = fromRegularSessionStartDate;
    }

    public Long getTillRegularSessionStartDate() {
        return tillRegularSessionStartDate;
    }

    public void setTillRegularSessionStartDate(Long tillRegularSessionStartDate) {
        this.tillRegularSessionStartDate = tillRegularSessionStartDate;
    }

    public Long getFromStartDate() {
        return fromStartDate;
    }

    public void setFromStartDate(Long fromStartDate) {
        this.fromStartDate = fromStartDate;
    }

    public Long getTillStartDate() {
        return tillStartDate;
    }

    public void setTillStartDate(Long tillStartDate) {
        this.tillStartDate = tillStartDate;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
