package com.vedantu.subscription.request;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by somil on 28/03/17.
 */
public class CoursePlanSessionRequest extends AbstractFrontEndReq {

    private Long studentId;
    private Long teacherId;
    private EntityType entityType;
    private String entityId;
    private SessionModel model;
    private List<SessionSlot> slots;
    private Long sessionId;
    private Long scheduledBy;
    private Long boardId;
    private String reason;
    private SessionSource sessionSource;

    public CoursePlanSessionRequest() {
        super();
    }

    public CoursePlanSessionRequest(Long studentId, Long teacherId, EntityType entityType, String entityId, SessionModel model,
            List<SessionSlot> slots, Long sessionId, Long scheduledBy, Long boardId, String reason) {
        super();
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.entityType = entityType;
        this.entityId = entityId;
        this.model = model;
        this.slots = slots;
        this.sessionId = sessionId;
        this.scheduledBy = scheduledBy;
        this.boardId = boardId;
        this.reason = reason;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public List<SessionSlot> getSlots() {
        return slots;
    }

    public void setSlots(List<SessionSlot> slots) {
        this.slots = slots;
    }

    public Long getScheduledBy() {
        return scheduledBy;
    }

    public void setScheduledBy(Long scheduledBy) {
        this.scheduledBy = scheduledBy;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public SessionSource getSessionSource() {
        return sessionSource;
    }

    public void setSessionSource(SessionSource sessionSource) {
        this.sessionSource = sessionSource;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Long getHours() {
        Long hours = 0l;
        if (slots == null) {
            return 0l;
        }
        for (SessionSlot s : slots) {
            if (s == null || s.endTime == null || s.startTime == null) {
                continue;
            }
            hours += s.endTime - s.startTime;
        }
        return hours;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();

        if (ArrayUtils.isNotEmpty(slots)) {
            Collections.sort(slots, new Comparator<SessionSlot>() {
                @Override
                public int compare(SessionSlot o1, SessionSlot o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
            int size = slots.size();
            Long currentMillis = System.currentTimeMillis();
            Long maxSessionDuration = ConfigUtils.INSTANCE.getLongValue("max.session.duration") * DateTimeUtils.MILLIS_PER_MINUTE;
            for (int i = 0; i < size; i++) {
                SessionSlot slot = slots.get(i);
                if (i < size - 1) {
                    SessionSlot nextSlot = slots.get(i + 1);
                    if (slot.getEndTime() > nextSlot.getStartTime()) {
                        errors.add("overlapping slots ");
                        break;
                    }
                }
                if (slot.getStartTime() < currentMillis) {
                    errors.add("slot in the past " + slot);
                    break;
                }
                if (slot.getStartTime() > slot.getEndTime()) {
                    errors.add("slot starttime > endTime" + slot);
                    break;
                }
                if (slot.getEndTime() - slot.getStartTime() > maxSessionDuration) {
                    errors.add("session time greater than max session duration "
                            + maxSessionDuration + " for slot " + slot);
                    break;
                }

            }
        }

        return errors;
    }

}
