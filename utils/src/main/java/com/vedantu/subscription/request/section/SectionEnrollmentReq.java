package com.vedantu.subscription.request.section;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SectionEnrollmentReq {
    List<String> enrollmentIds;
    String userId;
}
