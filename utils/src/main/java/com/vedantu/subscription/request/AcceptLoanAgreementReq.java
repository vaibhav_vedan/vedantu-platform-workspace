package com.vedantu.subscription.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.enums.CommunicationKind;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author manishkumarsingh
 */
public class AcceptLoanAgreementReq extends AbstractFrontEndListReq {

    private Long userId;
    private String entityId;

    public AcceptLoanAgreementReq() {
        super();
    }

    public AcceptLoanAgreementReq(Long userId, String entityId) {
        this.userId = userId;
        this.entityId = entityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    @Override
    public String toString() {
        return "AcceptLoanAgreementReq{" + "userId=" + userId + ", entityId=" + entityId + '}';
    }
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (userId == null) {
            errors.add("userId is missing.");
        }
        
        if (entityId == null) {
            errors.add("Aio ID is missing.");
        }

        return errors;
    }

}
