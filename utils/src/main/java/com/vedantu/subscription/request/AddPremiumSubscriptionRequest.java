/**
 * 
 */
package com.vedantu.subscription.request;

import java.util.List;

import com.vedantu.subscription.enums.CourseCategory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@EqualsAndHashCode(callSuper = true)
public class AddPremiumSubscriptionRequest extends AbstractFrontEndReq{
	
	private String id;
	
	private String title;
	
	private String grade;
	
	private String board;
	
	private String target;
	
	private String stream;
	
	private String medium;
	
	private CourseCategory courseCategory;
	
	private Long validFrom;
	
	private Long validTill;
	
	private Integer	noOfDaysOfFreeAccess;
	
	private String bundleId;

	 @Override
	    protected List<String> collectVerificationErrors() {
	        List<String> errors=super.collectVerificationErrors();
	        if(StringUtils.isEmpty(title)){
	            errors.add("Title cannot be null or empty");
	        }

	        if(StringUtils.isEmpty(grade)){
	            errors.add("Grade cannot be null or empty");
	        }

//	        if(StringUtils.isEmpty(board)){
//	            errors.add("Board cannot be null or empty");
//	        }


	        if(StringUtils.isEmpty(courseCategory.name())){
	            errors.add("Course Category cannot be null or empty");
	        }
	        
	        if(null == validFrom) {
	        	errors.add("Valid From cannot be null or empty");
	        }
	        
	        if(null == validTill) {
	        	errors.add("Valid Till cannot be null or empty");
	        }
	        
	        if(null == noOfDaysOfFreeAccess) {
	        	errors.add("No Of Days Of Free Access cannot be null or empty");
	        }
	        
	        if(StringUtils.isEmpty(bundleId)) {
	        	errors.add("BundleId cannot be null or empty");
	        }
	        
	        return errors;
	    }

}
