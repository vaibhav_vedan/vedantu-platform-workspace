package com.vedantu.subscription.request;


import com.vedantu.subscription.enums.bundle.CourseType;


import java.util.List;
import java.util.Map;

/**
 * Created by Dpadhya
 */
public class AIOGroupResp  {

	private String id;
	private String title;
	private CourseType courseType;
	private long courseStartTime;
	private Map<String, Object> teacherInfo;
	private String bundleImageUrl;
	private List<String> Subjects;
	private Integer price;
	private Integer cutPrice;

	public AIOGroupResp(String id){
		this.id = id;
	}


	public AIOGroupResp(String id, String title, CourseType courseType, long courseStartTime, Map<String, Object> teacherInfo, String bundleImageUrl, List<String> subjects, Integer price, Integer cutPrice) {
		this.id = id;
		this.title = title;
		this.courseType = courseType;
		this.courseStartTime = courseStartTime;
		this.teacherInfo = teacherInfo;
		this.bundleImageUrl = bundleImageUrl;
		Subjects = subjects;
		this.price = price;
		this.cutPrice = cutPrice;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CourseType getCourseType() {
		return courseType;
	}

	public void setCourseType(CourseType courseType) {
		this.courseType = courseType;
	}

	public long getCourseStartTime() {
		return courseStartTime;
	}

	public void setCourseStartTime(long courseStartTime) {
		this.courseStartTime = courseStartTime;
	}

	public Map<String, Object> getTeacherInfo() {
		return teacherInfo;
	}

	public void setTeacherInfo(Map<String, Object> teacherInfo) {
		this.teacherInfo = teacherInfo;
	}

	public String getBundleImageUrl() {
		return bundleImageUrl;
	}

	public void setBundleImageUrl(String bundleImageUrl) {
		this.bundleImageUrl = bundleImageUrl;
	}

	public List<String> getSubjects() {
		return Subjects;
	}

	public void setSubjects(List<String> subjects) {
		Subjects = subjects;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getCutPrice() {
		return cutPrice;
	}

	public void setCutPrice(Integer cutPrice) {
		this.cutPrice = cutPrice;
	}
}
