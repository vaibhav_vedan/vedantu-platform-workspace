/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class EndCoursePlanReq extends AbstractFrontEndReq {

    private String coursePlanId;
    private String reason;
    private Boolean getRefundInfoOnly = false;
    private Integer promotionalAmount = 0;
    private Integer nonPromotionalAmount = 0;
    private CoursePlanEnums.CoursePlanEndType endType;

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Boolean getGetRefundInfoOnly() {
        return getRefundInfoOnly;
    }

    public void setGetRefundInfoOnly(Boolean getRefundInfoOnly) {
        this.getRefundInfoOnly = getRefundInfoOnly;
    }

    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public CoursePlanEnums.CoursePlanEndType getEndType() {
        return endType;
    }

    public void setEndType(CoursePlanEnums.CoursePlanEndType endType) {
        this.endType = endType;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (coursePlanId == null) {
            errors.add("coursePlanId");
        }
        if (reason == null) {
            errors.add("reason");
        }
        return errors;
    }
}
