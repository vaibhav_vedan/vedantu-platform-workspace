/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.EnrollmentTransactionContextType;

/**
 *
 * @author parashar
 */
public class RegPaymentOTFCourseReq {
    
    private String batchId;
    private String orderId;
    private String enrollmentId;
    private String courseId;
    private EnrollmentTransactionContextType enrollmentTransactionContextType;

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the courseId
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the enrollmentTransactionContextType
     */
    public EnrollmentTransactionContextType getEnrollmentTransactionContextType() {
        return enrollmentTransactionContextType;
    }

    /**
     * @param enrollmentTransactionContextType the enrollmentTransactionContextType to set
     */
    public void setEnrollmentTransactionContextType(EnrollmentTransactionContextType enrollmentTransactionContextType) {
        this.enrollmentTransactionContextType = enrollmentTransactionContextType;
    }
    
    
    
    
    
}
