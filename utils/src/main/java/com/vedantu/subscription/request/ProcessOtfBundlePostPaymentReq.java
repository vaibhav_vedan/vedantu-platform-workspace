/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.onetofew.pojo.OTFBundleInfo;

/**
 *
 * @author pranavm
 */
public class ProcessOtfBundlePostPaymentReq {
    private String orderId;
    private Long userId;
    private OTFBundleInfo otfBundleInfo;
    private String otfBundleId;

    

    @Override
    public String toString() {
        return "ProcessOtfBundlePostPaymentReq{" + "orderId=" + orderId + ", userId=" + userId + ", otfBundleInfo=" + otfBundleInfo + '}';
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public OTFBundleInfo getOtfBundleInfo() {
        return otfBundleInfo;
    }

    public void setOtfBundleInfo(OTFBundleInfo otfBundleInfo) {
        this.otfBundleInfo = otfBundleInfo;
    }
    
    public String getOtfBundleId() {
        return otfBundleId;
    }

    public void setOtfBundleId(String otfBundleId) {
        this.otfBundleId = otfBundleId;
    }
}
