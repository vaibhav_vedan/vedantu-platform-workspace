package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class BundleEntityFilterRequest extends AbstractFrontEndListReq {
    private String bundleId;
    private Integer grade;
    private PackageType packageType;
    private String entityId;
    private EnrollmentType enrollmentType;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(bundleId)){
            errors.add("[bundleId] cann't be null");
        }
        if(Objects.isNull(getStart()) || Objects.isNull(getSize())){
            errors.add("Please define values of [start] and [size] properly");
        }
        return errors;
    }
}
