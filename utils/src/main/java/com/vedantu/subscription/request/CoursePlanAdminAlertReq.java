/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CoursePlanAdminAlertReq {

    private List<CoursePlanInfo> firstPaymentApproachingPlans;
    private CoursePlanBasicInfo trialDoneCoursePlan;
    private List<SessionInfo> trailDoneSessions;

    public List<CoursePlanInfo> getFirstPaymentApproachingPlans() {
        return firstPaymentApproachingPlans;
    }

    public void setFirstPaymentApproachingPlans(List<CoursePlanInfo> firstPaymentApproachingPlans) {
        this.firstPaymentApproachingPlans = firstPaymentApproachingPlans;
    }

    public CoursePlanBasicInfo getTrialDoneCoursePlan() {
        return trialDoneCoursePlan;
    }

    public void setTrialDoneCoursePlan(CoursePlanBasicInfo trialDoneCoursePlan) {
        this.trialDoneCoursePlan = trialDoneCoursePlan;
    }

    public List<SessionInfo> getTrailDoneSessions() {
        return trailDoneSessions;
    }

    public void setTrailDoneSessions(List<SessionInfo> trailDoneSessions) {
        this.trailDoneSessions = trailDoneSessions;
    }

    @Override
    public String toString() {
        return "CoursePlanAdminAlertReq{" + "firstPaymentApproachingPlans=" + firstPaymentApproachingPlans + ", trialDoneCoursePlan=" + trialDoneCoursePlan + ", trailDoneSessions=" + trailDoneSessions + '}';
    }

}
