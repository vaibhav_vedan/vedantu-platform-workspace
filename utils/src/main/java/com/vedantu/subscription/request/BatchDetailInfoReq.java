package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatchDetailInfoReq extends AbstractFrontEndReq {
    private List<String> batchIds;

    private List<String> batchDataIncludeFields;
    private List<String> batchDataExcludeFields;

    private Boolean courseInfoRequired=Boolean.TRUE;
    private List<String> courseDataIncludeFields;
    private List<String> courseDataExcludeFields;

    public BatchDetailInfoReq(List<String> batchIds){
        this.batchIds=batchIds;
    }
}
