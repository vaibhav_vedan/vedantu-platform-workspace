package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by somil on 24/05/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetBundleTestDetailsReq extends AbstractFrontEndReq {
    private String bundleId;
    private Long userId;
    private List<String> testIds;
}
