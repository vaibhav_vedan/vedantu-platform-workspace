package com.vedantu.subscription.request;

import com.vedantu.subscription.pojo.bundle.SubscriptionMedia;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlan;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;


/**
 * Created by Dpadhya
 */
@Data
public class updateSubscription extends AbstractFrontEndReq {


    private String newSubscriptionPlanId;
    private String existingEnrollmentId;
    private Long userId;
    private String redirectUrl;
    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private String vedantuDiscountCouponId;


}
