package com.vedantu.subscription.request;

import java.util.List;

import com.vedantu.subscription.enums.ModeOfLearning;
import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidStringList;
import javax.validation.constraints.Size;

public class AddARMFormReq extends AbstractFrontEndReq {

    private String id;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String lookingFor;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String target;

    private Integer grade;

    @ValidStringList(message = ReqLimitsErMsgs.STRING_LIST_MAX)
    private List<String> subject;

    private List<Long> boardId;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String schoolBooks;

    private Long startDate;

    private ModeOfLearning modeOfLearning;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String comment;

    private List<CoursePlanCompactSchedule> compactSchedule;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String school;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String city;

    private Long studentId;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String chapterSequence;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String marksScored;

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String doubts;

    private Boolean registrationDone;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String description;

    private Long adminId;
    
    private Integer hours;
    
    private String adminComment;
    
    private boolean isClosed = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(String lookingFor) {
        this.lookingFor = lookingFor;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    public List<Long> getBoardId() {
        return boardId;
    }

    public void setBoardId(List<Long> boardId) {
        this.boardId = boardId;
    }

    public String getSchoolBooks() {
        return schoolBooks;
    }

    public void setSchoolBooks(String schoolBooks) {
        this.schoolBooks = schoolBooks;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public ModeOfLearning getModeOfLearning() {
        return modeOfLearning;
    }

    public void setModeOfLearning(ModeOfLearning modeOfLearning) {
        this.modeOfLearning = modeOfLearning;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<CoursePlanCompactSchedule> getCompactSchedule() {
        return compactSchedule;
    }

    public void setCompactSchedule(List<CoursePlanCompactSchedule> compactSchedule) {
        this.compactSchedule = compactSchedule;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getChapterSequence() {
        return chapterSequence;
    }

    public void setChapterSequence(String chapterSequence) {
        this.chapterSequence = chapterSequence;
    }

    public String getMarksScored() {
        return marksScored;
    }

    public void setMarksScored(String marksScored) {
        this.marksScored = marksScored;
    }

    public String getDoubts() {
        return doubts;
    }

    public void setDoubts(String doubts) {
        this.doubts = doubts;
    }

    public Boolean getRegistrationDone() {
        return registrationDone;
    }

    public void setRegistrationDone(Boolean registrationDone) {
        this.registrationDone = registrationDone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }
    
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (studentId == null) {
            errors.add("studentId");
        }
        if (target == null) {
            errors.add("target");
        }
        if (subject == null) {
            errors.add("subject");
        }
        if (boardId == null) {
            errors.add("boardId");
        }
        if (grade == null) {
            errors.add("grade");
        }
        if (lookingFor == null) {
            errors.add("lookingFor");
        }
        if (schoolBooks == null) {
            errors.add("schoolBooks");
        }
        if (startDate == null) {
            errors.add("startDate");
        }
        if (compactSchedule == null) {
            errors.add("compactSchedule");
        }
        return errors;
    }

    /**
     * @return the armAdminComment
     */
    public String getAdminComment() {
        return adminComment;
    }

    /**
     * @param adminComment the armAdminComment to set
     */
    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }

    /**
     * @return the isClosed
     */
    public boolean isIsClosed() {
        return isClosed;
    }

    /**
     * @param isClosed the isClosed to set
     */
    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

}
