/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class RenewSubscriptionForInstalmentReq extends AbstractFrontEndReq {

    private Long subscriptionDetailsId;
    private Long millisToAdd;
    private Long millisToLock;
    private String instalmentId;

    public RenewSubscriptionForInstalmentReq() {
        super();
    }

    public Long getSubscriptionDetailsId() {
        return subscriptionDetailsId;
    }

    public void setSubscriptionDetailsId(Long subscriptionDetailsId) {
        this.subscriptionDetailsId = subscriptionDetailsId;
    }

    public Long getMillisToAdd() {
        return millisToAdd;
    }

    public void setMillisToAdd(Long millisToAdd) {
        this.millisToAdd = millisToAdd;
    }

    public String getInstalmentId() {
        return instalmentId;
    }

    public void setInstalmentId(String instalmentId) {
        this.instalmentId = instalmentId;
    }

    public Long getMillisToLock() {
        return millisToLock;
    }

    public void setMillisToLock(Long millisToLock) {
        this.millisToLock = millisToLock;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (subscriptionDetailsId == null) {
            errors.add("subscriptionDetailsId");
        }

        if (instalmentId == null) {
            errors.add("instalmentId");
        }
        return errors;
    }
}
