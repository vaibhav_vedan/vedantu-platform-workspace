package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by somil on 11/05/17.
 */
@Data
@NoArgsConstructor
public class BundleCreateEnrolmentReq extends AbstractFrontEndReq {
    private String userId;
    private String entityId;
    private EnrollmentState state;
    private EntityStatus status;
    private String trialId;
    private Integer validMonths;
    private Boolean isSubscription;
    private boolean isSubscriptionRenew;
    private String orderId;

}
