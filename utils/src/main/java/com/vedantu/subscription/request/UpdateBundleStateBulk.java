package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
@Data
public class UpdateBundleStateBulk extends AbstractFrontEndReq {

    private List<String> bundleIds;
    BundleState newState;
    private Long  validTill;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (newState ==null  && validTill == null) {
            errors.add("Please set State or validTill ");
        }

        if(ArrayUtils.isEmpty(bundleIds)){
            errors.add("Please set bundle ids ");
        }

        return errors;
    }

}
