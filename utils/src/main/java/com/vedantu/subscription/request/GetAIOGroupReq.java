package com.vedantu.subscription.request;


import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.List;

/**
 * Created by Dpadhya
 */
public class GetAIOGroupReq extends AbstractFrontEndListReq {

	private Integer grade;
	private List<String> groupName;
	private List<String> groupIds;

	public List<String> getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(List<String> groupIds) {
		this.groupIds = groupIds;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public List<String> getGroupName() {
		return groupName;
	}

	public void setGroupName(List<String> groupName) {
		this.groupName = groupName;
	}
}
