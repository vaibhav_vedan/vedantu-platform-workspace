package com.vedantu.subscription.request;

import java.util.List;

import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SessionRequest extends AbstractFrontEndReq {

	private Long studentId;
	private Long teacherId;
	private Long subscriptionId;
	private SessionModel model;
	private List<SessionSlot> slots;
	private Long sessionId;
	private Long scheduledBy;
	private Long boardId;
	private String reason;
	private Long proposalId;
	private SessionSource sessionSource;

	public SessionRequest() {
		super();
	}

	public SessionRequest(Long studentId, Long teacherId, Long subscriptionId, SessionModel model,
			List<SessionSlot> slots, Long sessionId, Long scheduledBy, Long boardId, String reason) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.subscriptionId = subscriptionId;
		this.model = model;
		this.slots = slots;
		this.sessionId = sessionId;
		this.scheduledBy = scheduledBy;
		this.boardId = boardId;
		this.reason = reason;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public List<SessionSlot> getSlots() {
		return slots;
	}

	public void setSlots(List<SessionSlot> slots) {
		this.slots = slots;
	}

	public Long getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(Long scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public SessionSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(SessionSource sessionSource) {
		this.sessionSource = sessionSource;
	}

	public Long getProposalId() {
		return proposalId;
	}

	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}

	public Long getHours() {
		Long hours = 0l;
		if (slots == null) {
			return 0l;
		}
		for (SessionSlot s : slots) {
			hours += s.endTime - s.startTime;
		}
		return hours;
	}

}
