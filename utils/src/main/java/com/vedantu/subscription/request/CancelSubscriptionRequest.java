package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CancelSubscriptionRequest extends AbstractFrontEndReq {

    private Boolean refundInfoCall;
    private Long subscriptionId;
    private String refundReason;

    public CancelSubscriptionRequest() {
        super();
    }

    public CancelSubscriptionRequest(Boolean refundInfoCall, Long subscriptionId, String refundReason,
            Long sessionUserId) {
        super();
        this.refundInfoCall = refundInfoCall;
        this.subscriptionId = subscriptionId;
        this.refundReason = refundReason;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public Boolean getRefundInfoCall() {
        return refundInfoCall;
    }

    public void setRefundInfoCall(Boolean refundInfoCall) {
        this.refundInfoCall = refundInfoCall;
    }

    @Override
    public String toString() {
        return super.toString() + " CancelSubscriptionRequest{" + "refundInfoCall=" + refundInfoCall + ", subscriptionId=" + subscriptionId + ", refundReason=" + refundReason + '}';
    }
}
