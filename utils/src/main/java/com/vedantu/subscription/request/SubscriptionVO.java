package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.SubModel;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SubscriptionVO extends AbstractFrontEndReq {

    private Long teacherId;
    private Long studentId;
    private String planId;
    private Long offeringId;
    private SessionModel model;
    private ScheduleType scheduleType;
    private Long totalHours;
    private String teacherCouponId;
    private Long teacherDiscountAmount;
    private Long teacherHourlyRate;
    private String subject;
    private Long boardId;
    private String target;
    private Integer grade;
    private SessionSchedule schedule;
    private Long subscriptionId;
    private String note;
    private SubModel subModel = SubModel.DEFAULT;
    private String transactionId;
    private Boolean gradeAndTargetReq = false;// used in IL, //TODO remove this
    // and get info from IL request
    private SessionSource sessionSource;

    public SubscriptionVO() {
        super();
    }

    public SubscriptionVO(Long teacherId, Long studentId, String planId,
            Long offeringId, SessionModel model, ScheduleType scheduleType,
            Long totalHours, String teacherCouponId,
            Long teacherDiscountAmount, Long teacherHourlyRate, Long boardId,
            String target, Integer grade, SessionSchedule schedule,
            Long subscriptionId, String note, Boolean renew, SubModel subModel,
            String transactionId, SessionSource sessionSource) {
        super();
        this.teacherId = teacherId;
        this.studentId = studentId;
        this.planId = planId;
        this.offeringId = offeringId;
        this.model = model;
        this.scheduleType = scheduleType;
        this.totalHours = totalHours;
        this.teacherCouponId = teacherCouponId;
        this.teacherDiscountAmount = teacherDiscountAmount;
        this.teacherHourlyRate = teacherHourlyRate;
        this.boardId = boardId;
        this.target = target;
        this.grade = grade;
        this.schedule = schedule;
        this.subscriptionId = subscriptionId;
        this.note = note;
        this.subModel = subModel;
        this.transactionId = transactionId;
        this.sessionSource = sessionSource;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public String getTeacherCouponId() {
        return teacherCouponId;
    }

    public void setTeacherCouponId(String teacherCouponId) {
        this.teacherCouponId = teacherCouponId;
    }

    public Long getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public Long getTeacherHourlyRate() {
        return teacherHourlyRate;
    }

    public void setTeacherHourlyRate(Long teacherHourlyRate) {
        this.teacherHourlyRate = teacherHourlyRate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public SessionSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(SessionSchedule schedule) {
        this.schedule = schedule;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public SubModel getSubModel() {
        return subModel;
    }

    public void setSubModel(SubModel subModel) {
        this.subModel = subModel;
    }

    public Boolean getGradeAndTargetReq() {
        return gradeAndTargetReq;
    }

    public void setGradeAndTargetReq(Boolean gradeAndTargetReq) {
        this.gradeAndTargetReq = gradeAndTargetReq;
    }

    public SessionSource getSessionSource() {
        return sessionSource;
    }

    public void setSessionSource(SessionSource sessionSource) {
        this.sessionSource = sessionSource;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

}
