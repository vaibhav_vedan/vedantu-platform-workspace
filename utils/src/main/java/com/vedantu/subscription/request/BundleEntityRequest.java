package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class BundleEntityRequest extends AbstractFrontEndReq {
    private String id;
    private Integer grade;
    private PackageType packageType;
    private String entityId;
    private EnrollmentType enrollmentType;
    private String bundleId;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(bundleId)){
            errors.add("bundleId cann't be empty, please save the bundle if it is a new one then edit the packages");
        }
        if(Objects.isNull(grade) || Objects.isNull(packageType) || StringUtils.isEmpty(entityId) || Objects.isNull(enrollmentType)){
            errors.add("[grade] or [packageType] or [entityId] or [enrollmentType] cann't be null");
        }
        if(PackageType.OTM_BUNDLE.equals(packageType) && (EnrollmentType.CLICK_TO_ENROLL.equals(enrollmentType) || EnrollmentType.UPSELL.equals(enrollmentType))){
            errors.add("For a OTM_BUNDLE the enrollment type can't be click to enroll or upsell, it can only be auto enroll or locked");
        }
        return errors;
    }
}
