package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 * Created by somil on 28/03/17.
 */
public class TransferCancelSessionHoursReq extends AbstractFrontEndReq {
    private String coursePlanId;
    private Long sessionId;
    private Integer hours;

    public TransferCancelSessionHoursReq() {
        super();
    }

    public TransferCancelSessionHoursReq(String coursePlanId, Long sessionId, Integer hours) {
        this.coursePlanId = coursePlanId;
        this.sessionId = sessionId;
        this.hours = hours;
    }

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

}
