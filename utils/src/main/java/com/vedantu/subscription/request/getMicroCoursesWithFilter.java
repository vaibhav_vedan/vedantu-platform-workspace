package com.vedantu.subscription.request;


import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by Dpadhya
 */
@Data
public class getMicroCoursesWithFilter extends AbstractFrontEndListReq {

	private Integer grade;
	private CourseType courseType;
	private List<String> subject;
	private List<String> target;
	private Long fromTime;
	private Long toTime;
	private List<String> groupIds;
	private List<String> bundleIds;
	private Boolean sortAsc;
	private Boolean searchWithOnlyQuery;
	private String query;

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if( !StringUtils.isEmpty(getQuery())){
			if(getQuery().length() < 4){
				errors.add("add atleast 4 charecter for search");
			}
		}


		return errors;
	}



}
