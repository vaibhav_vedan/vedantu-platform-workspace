package com.vedantu.subscription.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.pojo.OTFWebinarSession;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.subscription.pojo.WebinarBundleContentInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddEditBundleSessionReq extends AbstractFrontEndReq {

    private String bundleId;
    private String trackId;
    private OTFWebinarSession otfWebinarSession;
    private WebinarBundleContentInfo webinarBundleContentInfo;

    public AddEditBundleSessionReq() {
        super();
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public OTFWebinarSession getOtfWebinarSession() {
        return otfWebinarSession;
    }

    public void setOtfWebinarSession(OTFWebinarSession otfWebinarSession) {
        this.otfWebinarSession = otfWebinarSession;
    }

    public WebinarBundleContentInfo getWebinarBundleContentInfo() {
        return webinarBundleContentInfo;
    }

    public void setWebinarBundleContentInfo(WebinarBundleContentInfo webinarBundleContentInfo) {
        this.webinarBundleContentInfo = webinarBundleContentInfo;
    }

    public void validate() throws BadRequestException {
        List<String> errors = collectErrors();
        if (!CollectionUtils.isEmpty(errors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Invalid params:" + Arrays.toString(errors.toArray()));
        }
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(this.bundleId)) {
            errors.add("bundleId");
        }
        if (StringUtils.isEmpty(this.trackId)) {
            errors.add("trackId");
        }
        if (StringUtils.isEmpty(this.webinarBundleContentInfo)) {
            errors.add("webinarBundleContentInfo");
        } else {
            if (!ContentType.WEBINAR.equals(this.webinarBundleContentInfo.getContentType())) {
                errors.add("webinarBundleContentInfo.contentType not WEBINAR");
            }
        }
        if (this.otfWebinarSession == null) {
            errors.add("otfWebinarSession");
        } else {
            if (!CollectionUtils.isEmpty(this.otfWebinarSession.collectErrors())) {
                errors.addAll(this.otfWebinarSession.collectErrors());
            }
        }
        return errors;
    }

    @Override
    public String toString() {
        return "AddEditBundleSessionReq [bundleId=" + bundleId + ", trackId=" + trackId + ", toString()="
                + super.toString() + "]";
    }
}
