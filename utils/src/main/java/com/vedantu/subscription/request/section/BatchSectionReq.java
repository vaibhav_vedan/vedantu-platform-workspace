package com.vedantu.subscription.request.section;

import lombok.Data;

import java.util.List;

@Data
public class BatchSectionReq {
    List<String> batchIds;
}
