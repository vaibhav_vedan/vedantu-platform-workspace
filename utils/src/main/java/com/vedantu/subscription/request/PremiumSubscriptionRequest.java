/**
 * 
 */
package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.CourseCategory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class PremiumSubscriptionRequest {

	private String id;
	
	private String title;
	
	private String grade;
	
	private String board;
	
	private String target;
	
	private String stream;
	
	private String medium;
	
	private CourseCategory courseCategory;
	
	private Long validFrom;
	
	private Long validTill;
	
	private Integer	noOfDaysOfFreeAccess;
	
	private String bundleId;
	
}
