/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class MarkTrialPaymentDoneReq extends AbstractFrontEndReq {

    private String coursePlanId;
    private String parentCourseId;
    private Long userId;

    public String getParentCourseId() {
        return parentCourseId;
    }

    public void setParentCourseId(String parentCourseId) {
        this.parentCourseId = parentCourseId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (coursePlanId == null) {
            errors.add("coursePlanId");
        }
        if (parentCourseId == null) {
            errors.add("parentCourseId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }
}
