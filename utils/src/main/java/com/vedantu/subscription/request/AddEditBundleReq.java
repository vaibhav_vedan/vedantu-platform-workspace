package com.vedantu.subscription.request;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.BaseSubscriptionPlanReq;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.TrialData;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import lombok.Data;

/**
 * Created by somil on 09/05/17.
 */

@Data
public class AddEditBundleReq extends AbstractFrontEndReq {

    private List<Track> webinarCategories;
    private VideoContentData videos;
    private TestContentData tests;
    private String title;
    private List<String> subject;
    private List<String> target;
    private List<Integer> grade;
    private Long boardId;
    private BundleState state = BundleState.DRAFT;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<String> tags;
    private String id;
    private List<String> teacherIds;
    private String demoVideoUrl;
    private boolean featured;
    private Integer priority; //lowest is highest
    private List<String> courses;
    private List<BaseInstalmentInfo> instalmentDetails;
    private Map<String, Object> metadata;
    private int trailAmount;
    private int trailAllowedDays;
    private Boolean trailIncluded;
    private Long validTill ;
    private int validDays ;
    private Boolean tabletIncluded ;
    private Boolean amIncluded ;
    private Boolean unLimitedDoubtsIncluded ;
    private List<String> suggestionPackages;
    @Deprecated
    private List<AioPackage> packages;
    private Boolean isTrail;
    private List<CourseTerm> searchTerms;
    private Boolean isRankBooster;
    private BundleDetails bundleDetails;
    private Boolean isInstalmentVisible;
    private DurationTime durationtime;
    private Long lastPurchaseDate;
    private Integer minimumPurchasePrice;
    private Boolean isSubscription = Boolean.FALSE;
    private Long windowRight;
    private List<TrialData> trialData;
    private List<String> trialIdsForRemoval;
    private List<BaseSubscriptionPlanReq> baseSubscriptionPlan;
    private boolean earlyLearning;
    private List<EarlyLearningCourseType> earlyLearningCourses;
    private String orgId;
    private List<String> displaySubject;
    private Boolean isVassist;
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (validTill ==null && validDays ==0) {
            errors.add("Please set valid till date or valid days ");
        }

        if(Boolean.TRUE.equals(isSubscription)) {
            if((windowRight == null)) {
                errors.add("Batch Selection start/end date can't be null for subscription");
            }
            if(ArrayUtils.isEmpty( grade ) || ArrayUtils.isEmpty( subject ) || ArrayUtils.isEmpty( target )) {
                errors.add("Grade/subject/target can be empty for subscription");
            }
        }

        if(ArrayUtils.isNotEmpty(trialData)){
            if(Boolean.FALSE.equals(isSubscription)){
                errors.add("Trial only allowed for subscription");
            }
            int distinctTrialDays = trialData.stream().map(TrialData::getTrialAllowedDays).collect(Collectors.toSet()).size();
            if(distinctTrialDays!=trialData.size()){
                errors.add("You cann't give same no of trial days with different amount.");
            }
            Collections.sort(trialData, Comparator.comparing(TrialData::getTrialAllowedDays));
            for (int i = 1; i < trialData.size(); i++) {
                if(trialData.get(i).getTrialAmount()<=trialData.get(i-1).getTrialAmount()){
                    errors.add("It is not recommended to have a trial with more days with less than or equal amount compared " +
                            "to a trail with less no of days.");
                    break;
                }
            }
        }

        return errors;
    }

}
