package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetSubscriptionsReq extends AbstractFrontEndListReq {

	private Long studentId;
	private Long teacherId;
	private SessionModel model;
	private Boolean active;
	private Long beforeTime;
	private Long afterTime;
	private SubModel subModel;
	private Long lastUpdatedTime;

	public GetSubscriptionsReq() {
		super();
	}

	public GetSubscriptionsReq(Integer start, Integer size, Long studentId, Long teacherId, SessionModel model,
			Boolean active, Long beforeTime, Long afterTime, SubModel subModel, Long lastUpdatedTime) {
		super(start, size);
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.model = model;
		this.active = active;
		this.beforeTime = beforeTime;
		this.afterTime = afterTime;
		this.subModel = subModel;
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getBeforeTime() {
		return beforeTime;
	}

	public void setBeforeTime(Long beforeTime) {
		this.beforeTime = beforeTime;
	}

	public Long getAfterTime() {
		return afterTime;
	}

	public void setAfterTime(Long afterTime) {
		this.afterTime = afterTime;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	public Long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
}
