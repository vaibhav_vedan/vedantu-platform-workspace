package com.vedantu.subscription.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ResetInstallmentStateReq extends AbstractFrontEndReq {
	private Long studentId;
	private String batchId;

	public ResetInstallmentStateReq() {
		super();
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Missing params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(this.studentId)) {
			errors.add("studentId");
		}
		if (StringUtils.isEmpty(this.batchId)) {
			errors.add("batchId");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "ResetInstallmentStateReq [studentId=" + studentId + ", batchId=" + batchId + ", toString()="
				+ super.toString() + "]";
	}
}
