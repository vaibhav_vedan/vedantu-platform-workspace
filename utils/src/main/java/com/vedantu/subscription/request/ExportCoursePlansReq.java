package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 * Created by somil on 13/04/17.
 */
public class ExportCoursePlansReq extends AbstractFrontEndListReq {

    private Long tillTime;
    private Long fromTime;
    private Long startDate;
    private Long studentId;
    private Long adminId;
    private Boolean isClosed;


    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    /**
     * @return the startDate
     */
    public Long getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the studentId
     */
    public Long getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the adminId
     */
    public Long getAdminId() {
        return adminId;
    }

    /**
     * @param adminId the adminId to set
     */
    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    /**
     * @return the isClosed
     */
    public Boolean isIsClosed() {
        return isClosed;
    }

    /**
     * @param isClosed the isClosed to set
     */
    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

}
