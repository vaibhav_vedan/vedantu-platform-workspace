package com.vedantu.subscription.request;

import com.vedantu.User.Role;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.subscription.SubscriptionRequestAction;
import com.vedantu.util.subscription.SubscriptionRequestState;

public class GetSubscriptionRequestReq extends AbstractFrontEndListReq {

	private Long studentId;
	private Long teacherId;
	private SubscriptionRequestState state;
	private SubscriptionRequestAction action;
	private Role createdByRole;
	private SessionModel model;
	private SubModel subModel;
	private Long lastUpdatedTime;

	public GetSubscriptionRequestReq() {
		super();
	}

	public GetSubscriptionRequestReq(Integer start, Integer size, Long studentId, Long teacherId, SubscriptionRequestState state,
			SubscriptionRequestAction action, Role createdByRole, SessionModel model, SubModel subModel,
			Long lastUpdatedTime) {
		super(start, size);
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.state = state;
		this.action = action;
		this.createdByRole = createdByRole;
		this.model = model;
		this.subModel = subModel;
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SubscriptionRequestState getState() {
		return state;
	}

	public void setState(SubscriptionRequestState state) {
		this.state = state;
	}

	public SubscriptionRequestAction getAction() {
		return action;
	}

	public void setAction(SubscriptionRequestAction action) {
		this.action = action;
	}

	public Role getCreatedByRole() {
		return createdByRole;
	}

	public void setCreatedByRole(Role createdByRole) {
		this.createdByRole = createdByRole;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	public Long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
}
