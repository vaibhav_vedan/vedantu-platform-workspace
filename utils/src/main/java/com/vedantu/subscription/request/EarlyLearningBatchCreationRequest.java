package com.vedantu.subscription.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class EarlyLearningBatchCreationRequest extends AbstractFrontEndReq {

    private String courseId;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(courseId)) {
            errors.add("course id is null");
        }
        return errors;
    }
}
