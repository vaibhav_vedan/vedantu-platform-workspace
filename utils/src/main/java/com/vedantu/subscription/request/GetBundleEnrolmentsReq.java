package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by somil on 19/05/17.
 */
@Data
@NoArgsConstructor
public class GetBundleEnrolmentsReq extends AbstractFrontEndListReq {
    private EntityStatus status;
    private String bundleId;
    private String userId;
    private List<String> userIdList;
    private List<EntityStatus> statusList;
    private Long createdBefore;
    private Long LastUpdated;
}
