package com.vedantu.subscription.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 * Created by somil on 02/06/17.
 */
public class EnrolViaPassReq extends AbstractFrontEndReq {
    private Long userId;
    private String entityId;
    private String passCode;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(passCode)) {
            errors.add("passCode");
        }
        if (userId==null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        return errors;
    }
}
