/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class EditInstalmentAmtAndSchedule extends AbstractFrontEndReq {

    private String coursePlanId;
//    private List<BaseInstalmentInfo> futureInstalmentsInfo;
    private SessionSchedule fromNextInstalmentSchedule;
    private Long nextDueTime;

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public SessionSchedule getFromNextInstalmentSchedule() {
        return fromNextInstalmentSchedule;
    }

    public void setFromNextInstalmentSchedule(SessionSchedule fromNextInstalmentSchedule) {
        this.fromNextInstalmentSchedule = fromNextInstalmentSchedule;
    }

    public Long getNextDueTime() {
        return nextDueTime;
    }

    public void setNextDueTime(Long nextDueTime) {
        this.nextDueTime = nextDueTime;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (coursePlanId == null) {
            errors.add("coursePlanId");
        }
//        if (ArrayUtils.isNotEmpty(futureInstalmentsInfo)) {
//            for (BaseInstalmentInfo instalmentInfo : futureInstalmentsInfo) {
//                if (instalmentInfo.getDueTime() == null
//                        || instalmentInfo.getDueTime() < System.currentTimeMillis()
//                        || instalmentInfo.getDueTime() <= 0) {
//                    errors.add("instalmentsInfo--dueTime");
//                }
//                if (instalmentInfo.getAmount() == null || instalmentInfo.getAmount() < 0) {
//                    errors.add("instalmentsInfo--amount");
//                    break;
//                }
//            }
//        }
        if (fromNextInstalmentSchedule != null && ArrayUtils.isNotEmpty(fromNextInstalmentSchedule.getSessionSlots())) {
            errors.addAll(fromNextInstalmentSchedule.collectVerificationErrors(fromNextInstalmentSchedule.getSessionSlots()));
        }
        return errors;
    }
}
