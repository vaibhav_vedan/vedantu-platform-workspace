package com.vedantu.subscription.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.subscription.pojo.WebinarBundleContentInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddEditBundleWebinarReq extends AbstractFrontEndReq {
	private String bundleId;
	private String trackId;
	private List<WebinarBundleContentInfo> webinarBundleContentInfos;

	public AddEditBundleWebinarReq() {
		super();
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public String getTrackId() {
		return trackId;
	}

	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}

	public List<WebinarBundleContentInfo> getWebinarBundleContentInfos() {
		return webinarBundleContentInfos;
	}

	public void setWebinarBundleContentInfos(List<WebinarBundleContentInfo> webinarBundleContentInfos) {
		this.webinarBundleContentInfos = webinarBundleContentInfos;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Invalid params:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (StringUtils.isEmpty(this.bundleId)) {
			errors.add("bundleId");
		}
		if (StringUtils.isEmpty(this.trackId)) {
			errors.add("trackId");
		}

		if (CollectionUtils.isEmpty(this.webinarBundleContentInfos)) {
			errors.add("webinarBundleContentInfos");
		} else {
			for (WebinarBundleContentInfo webinarBundleContentInfo : webinarBundleContentInfos) {
				if (webinarBundleContentInfo.getStartTime() <= System.currentTimeMillis()) {
					errors.add("webinarBundleContentInfo.startTime - referenceId:" + webinarBundleContentInfo.getReferenceId());
				}
			}
		}

		return errors;
	}

	@Override
	public String toString() {
		return "AddEditBundleWebinarReq [bundleId=" + bundleId + ", trackId=" + trackId + ", toString()="
				+ super.toString() + "]";
	}
}