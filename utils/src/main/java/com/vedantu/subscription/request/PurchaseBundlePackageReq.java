/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class PurchaseBundlePackageReq extends AbstractFrontEndReq {

    private String bundlePackageId;
    private Long userId;
    private String redirectUrl;
    private String vedantuDiscountCouponId;
    private List<PurchasingEntity> purchasingEntities;

    public String getBundlePackageId() {
        return bundlePackageId;
    }

    public void setBundlePackageId(String bundlePackageId) {
        this.bundlePackageId = bundlePackageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    public List<PurchasingEntity> getPurchasingEntities() {
        return purchasingEntities;
    }

    public void setPurchasingEntities(List<PurchasingEntity> purchasingEntities) {
        this.purchasingEntities = purchasingEntities;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (bundlePackageId == null) {
            errors.add("bundlePackageId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }
}
