package com.vedantu.subscription.request;


import com.vedantu.subscription.enums.bundle.CourseType;

import java.util.List;
import java.util.Map;

/**
 * Created by Dpadhya
 */
public class LandingPageCollectionResp {
	String id;
String heading;
	List<AIOGroupResp> AIOGroupResps;

	public LandingPageCollectionResp(String id,String heading, List<AIOGroupResp> AIOGroupResps) {

		this.id = id;
		this.heading = heading;
		this.AIOGroupResps = AIOGroupResps;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public List<AIOGroupResp> getAIOGroupResps() {
		return AIOGroupResps;
	}

	public void setAIOGroupResps(List<AIOGroupResp> AIOGroupResps) {
		this.AIOGroupResps = AIOGroupResps;
	}
}
