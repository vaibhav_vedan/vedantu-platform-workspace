/**
 * 
 */
package com.vedantu.subscription.request;

import java.util.List;

import com.vedantu.User.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserPremiumSubscriptionEnrollmentReq {

	private String userId;
	
	private Role userRole;
	
	private String grade;
	
	private String board;
	
	private String target;
	
	private String stream;
	
	private String medium;
	
    private Integer start;
    
    private Integer size;
    
    private Long validFrom;
    
    private Long validTill;
    
    private boolean includeCourseId;
    
    private List<String> batchIds;
    
    private List<String> bundleIds;
}
