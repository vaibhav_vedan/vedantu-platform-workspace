/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class MarkCoursePlanStateReq extends AbstractFrontEndReq {

    private String coursePlanId;
    private CoursePlanEnums.CoursePlanState state;
    private CoursePlanEnums.CoursePlanEndType endType;//only for ended state
    private String reason;//only for endedstate
    
    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public CoursePlanEnums.CoursePlanState getState() {
        return state;
    }

    public void setState(CoursePlanEnums.CoursePlanState state) {
        this.state = state;
    }

    public CoursePlanEnums.CoursePlanEndType getEndType() {
        return endType;
    }

    public void setEndType(CoursePlanEnums.CoursePlanEndType endType) {
        this.endType = endType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(coursePlanId==null){
            errors.add("coursePlanId");
        }
        if(state==null){
            errors.add("state");
        }
        if(!CoursePlanEnums.CoursePlanState.TRIAL_SESSIONS_DONE.equals(state)){
            if(super.getCallingUserId()==null){
                errors.add("callingUserId");
            }            
        }
        return errors;
    }

}
