package com.vedantu.subscription.request;

import java.util.List;

import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.subscription.enums.ReminderTimeType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetCoursePlanReminderListReq extends AbstractFrontEndListReq {

	private Long startTime;
	private Long endTime;
	private List<CoursePlanState> state;
	private List<String> coursePlanIds;
	private ReminderTimeType timeType;
	private List<SessionState> sessionStates;

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public List<CoursePlanState> getState() {
		return state;
	}

	public void setState(List<CoursePlanState> state) {
		this.state = state;
	}
	
	public List<String> getCoursePlanIds() {
		return coursePlanIds;
	}

	public void setCoursePlanIds(List<String> coursePlanIds) {
		this.coursePlanIds = coursePlanIds;
	}

	public ReminderTimeType getTimeType() {
		return timeType;
	}

	public void setTimeType(ReminderTimeType timeType) {
		this.timeType = timeType;
	}


	public List<SessionState> getSessionStates() {
		return sessionStates;
	}

	public void setSessionStates(List<SessionState> sessionStates) {
		this.sessionStates = sessionStates;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        	if (startTime == null) {
                errors.add("startTime");
            }
            if (endTime == null) {
                errors.add("endTime");
            }        
       
            if(timeType == null){
            	errors.add("timeType");
            }
        return errors;
	}
}
