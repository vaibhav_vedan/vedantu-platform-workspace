/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author pranavm
 */
public class ProcessOtfPostPaymentReq extends AbstractFrontEndReq {

    private String orderId;
    private Long userId;
    private String entityId;
    private String purchaseContextId; // Bundle id from where it is coming

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getPurchaseContextId() {
        return purchaseContextId;
    }

    public void setPurchaseContextId(String purchaseContextId) {
        this.purchaseContextId = purchaseContextId;
    }
}
