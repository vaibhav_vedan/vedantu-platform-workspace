package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 * Created by somil on 05/06/17.
 */
public class MarkEnrolmentStateReq extends AbstractFrontEndReq {
    private List<String> enrolmentIds;
    private EntityStatus entityStatus;

    public List<String> getEnrolmentIds() {
        return enrolmentIds;
    }

    public void setEnrolmentIds(List<String> enrolmentIds) {
        this.enrolmentIds = enrolmentIds;
    }

    public EntityStatus getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(EntityStatus entityStatus) {
        this.entityStatus = entityStatus;
    }


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (entityStatus==null) {
            errors.add("entityStatus");
        }
        if (CollectionUtils.isEmpty(enrolmentIds)) {
            errors.add("enrolmentIds");
        }
        return errors;
    }
}
