package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.subscription.enums.BundleContentType;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.ContentPriceType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.List;

/**
 * Created by somil on 12/05/17.
 */
public class GetBundlesReq extends AbstractFrontEndListReq {
	private List<BundleState> states;
	private List<ContentPriceType> contentPriceTypes;
	private List<BundleContentType> returnContentTypes;
	private Boolean featured;
	private boolean checkAccessControl = false;
	private List<String> bundleIds;
	private String bundleName;
	private String bundleEnrollmentId;
	private EnrollmentState state;
	private boolean removeByReturnContentTypes;
	private boolean includeSubscriptionPlan;

	public boolean isCheckAccessControl() {
		return checkAccessControl;
	}

	public void setCheckAccessControl(boolean checkAccessControl) {
		this.checkAccessControl = checkAccessControl;
	}

	public GetBundlesReq() {
		super();
	}

	public List<BundleState> getStates() {
		return states;
	}

	public void setStates(List<BundleState> states) {
		this.states = states;
	}

	public Boolean getFeatured() {
		return featured;
	}

	public void setFeatured(Boolean featured) {
		this.featured = featured;
	}

	public List<BundleContentType> getReturnContentTypes() {
		return returnContentTypes;
	}

	public void setReturnContentTypes(List<BundleContentType> returnContentTypes) {
		this.returnContentTypes = returnContentTypes;
	}

	public List<ContentPriceType> getContentPriceTypes() {
		return contentPriceTypes;
	}

	public void setContentPriceTypes(List<ContentPriceType> contentPriceTypes) {
		this.contentPriceTypes = contentPriceTypes;
	}

	public List<String> getBundleIds() {
		return bundleIds;
	}

	public void setBundleIds(List<String> bundleIds) {
		this.bundleIds = bundleIds;
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public String getBundleEnrollmentId() {
		return bundleEnrollmentId;
	}

	public void setBundleEnrollmentId(String bundleEnrollmentId) {
		this.bundleEnrollmentId = bundleEnrollmentId;
	}

	public EnrollmentState getState() {
		return state;
	}

	public void setState(EnrollmentState state) {
		this.state = state;
	}

	public boolean getRemoveByReturnContentTypes() {
		return removeByReturnContentTypes;
	}

	public void setRemoveByReturnContentTypes(boolean removeByReturnContentTypes) {
		this.removeByReturnContentTypes = removeByReturnContentTypes;
	}

	public boolean isIncludeSubscriptionPlan() {
		return includeSubscriptionPlan;
	}

	public void setIncludeSubscriptionPlan(boolean includeSubscriptionPlan) {
		this.includeSubscriptionPlan = includeSubscriptionPlan;
	}

	@Override
	public String toString() {
		return "GetBundlesReq [states=" + states + ", contentPriceTypes=" + contentPriceTypes + ", returnContentTypes="
				+ returnContentTypes + ", featured=" + featured + ", toString()=" + super.toString() + "]";
	}
}
