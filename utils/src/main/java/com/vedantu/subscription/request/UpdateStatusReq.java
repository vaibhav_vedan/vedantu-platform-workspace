package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 * Created by somil on 12/05/17.
 */
public class UpdateStatusReq extends AbstractFrontEndListReq {

	private String bundleId;
	private String userId;
	private EntityStatus status;
        private String bundleEnrollmentId;
        private String orderId;
	private Boolean updateOrder = true;

	public String getBundleEnrollmentId() {
            return bundleEnrollmentId;
        }

        public void setBundleEnrollmentId(String bundleEnrollmentId) {
            this.bundleEnrollmentId = bundleEnrollmentId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public Boolean getUpdateOrder() {
		return updateOrder;
	}

	public void setUpdateOrder(Boolean updateOrder) {
		this.updateOrder = updateOrder;
	}
}
