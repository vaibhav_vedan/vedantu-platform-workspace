package com.vedantu.subscription.request;

import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import lombok.Data;

import java.util.Map;

@Data
public class BundleDetailsUpdateReq {
    private Map<String, BundleDetails> bundleDetails;
    private Map<String, Map<String, BatchDetails>> bacthDetails;
    private boolean emptyDisplayTags = false;
}
