package com.vedantu.subscription.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;
import java.util.Set;

public class GetCourseIdByUsingUserIdBatchIdsReq extends AbstractFrontEndReq {

    private Set<String> batchIds;
    private  String userId;

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(ArrayUtils.isEmpty(batchIds)){
            errors.add("batchIds can't be empty");
        }
        if (StringUtils.isEmpty(userId)) {
            errors.add("userId can't be empty");
        }
        return errors;
    }
}
