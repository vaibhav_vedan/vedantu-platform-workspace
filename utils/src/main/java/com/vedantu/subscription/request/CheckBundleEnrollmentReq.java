package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CheckBundleEnrollmentReq extends AbstractFrontEndReq {
    private String userId;
    private String bundleId;
    private EnrollmentState state;
    private EnrollmentState newState;
}
