package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class EditMultipleBundleReq extends AbstractFrontEndReq {
    private List<String> bundleIds;

    public List<String> getBundleIds() {
        return bundleIds;
    }

    public void setBundleIds(List<String> bundleIds) {
        this.bundleIds = bundleIds;
    }
}
