/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AddCoursePlanHoursReq extends AbstractFrontEndReq {

    private Integer hoursToAdd;
    private String coursePlanId;
    private String transactionRefNo;
    private Boolean instalmentPayment;

    public Integer getHoursToAdd() {
        return hoursToAdd;
    }

    public void setHoursToAdd(Integer hoursToAdd) {
        this.hoursToAdd = hoursToAdd;
    }

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }

    public Boolean getInstalmentPayment() {
        return instalmentPayment;
    }

    public void setInstalmentPayment(Boolean instalmentPayment) {
        this.instalmentPayment = instalmentPayment;
    }
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (coursePlanId == null) {
            errors.add("coursePlanId");
        }
        if (hoursToAdd == null || hoursToAdd < 0) {
            errors.add("hoursToAdd");
        }
        if (transactionRefNo == null) {
            errors.add("transactionRefNo");
        }
        return errors;
    }

}
