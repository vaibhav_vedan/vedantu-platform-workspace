/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.request;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.FAQPojo;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.subscription.pojo.CoursePlanAdminData;
import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.subscription.pojo.OTOCurriculumPojo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class AddEditCoursePlanReq extends AbstractFrontEndReq {

    private String id;
    private String title;
    private Long teacherId;
    private Long studentId;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private Integer totalCourseHours;
    private List<CoursePlanCompactSchedule> compactSchedule;//only for display, no business logic
    private Integer noOfWeeks;
    private CoursePlanState state = CoursePlanState.PUBLISHED;
    private String description;
    private List<OTOCurriculumPojo> curriculum;
    private Set<Long> suggestedTeacherIds;
    private List<FAQPojo> faq;
    private String parentCourseId;
    private List<String> tags;
    private Map<String, String> keyValues;
    private String demoVideoUrl;
    //money related
    private Integer price;//includes registration fee, user has to pay price-registration to get ENROLLED
    private SessionSchedule trialSessionSchedule;
    private SessionSchedule regularSessionSchedule;
    private List<BaseInstalmentInfo> instalmentsInfo;
    private ScheduleType scheduleType = ScheduleType.FS;
    private Integer teacherPayoutRate;
    private Integer trialRegistrationFee;
    private List<CoursePlanAdminData> adminData;
    private String armFormId;
    private Integer intendedHours;//millis

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Integer getTotalCourseHours() {
        return totalCourseHours;
    }

    public void setTotalCourseHours(Integer totalCourseHours) {
        this.totalCourseHours = totalCourseHours;
    }

    public List<CoursePlanCompactSchedule> getCompactSchedule() {
        return compactSchedule;
    }

    public void setCompactSchedule(List<CoursePlanCompactSchedule> compactSchedule) {
        this.compactSchedule = compactSchedule;
    }

    public Integer getNoOfWeeks() {
        return noOfWeeks;
    }

    public void setNoOfWeeks(Integer noOfWeeks) {
        this.noOfWeeks = noOfWeeks;
    }

    public CoursePlanState getState() {
        return state;
    }

    public void setState(CoursePlanState state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OTOCurriculumPojo> getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(List<OTOCurriculumPojo> curriculum) {
        this.curriculum = curriculum;
    }

    public Set<Long> getSuggestedTeacherIds() {
        return suggestedTeacherIds;
    }

    public void setSuggestedTeacherIds(Set<Long> suggestedTeacherIds) {
        this.suggestedTeacherIds = suggestedTeacherIds;
    }

    public List<FAQPojo> getFaq() {
        return faq;
    }

    public void setFaq(List<FAQPojo> faq) {
        this.faq = faq;
    }

    public String getParentCourseId() {
        return parentCourseId;
    }

    public void setParentCourseId(String parentCourseId) {
        this.parentCourseId = parentCourseId;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public String getDemoVideoUrl() {
        return demoVideoUrl;
    }

    public void setDemoVideoUrl(String demoVideoUrl) {
        this.demoVideoUrl = demoVideoUrl;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public SessionSchedule getTrialSessionSchedule() {
        return trialSessionSchedule;
    }

    public void setTrialSessionSchedule(SessionSchedule trialSessionSchedule) {
        this.trialSessionSchedule = trialSessionSchedule;
    }

    public SessionSchedule getRegularSessionSchedule() {
        return regularSessionSchedule;
    }

    public void setRegularSessionSchedule(SessionSchedule regularSessionSchedule) {
        this.regularSessionSchedule = regularSessionSchedule;
    }

    public List<BaseInstalmentInfo> getInstalmentsInfo() {
        return instalmentsInfo;
    }

    public void setInstalmentsInfo(List<BaseInstalmentInfo> instalmentsInfo) {
        this.instalmentsInfo = instalmentsInfo;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public Integer getTeacherPayoutRate() {
        return teacherPayoutRate;
    }

    public void setTeacherPayoutRate(Integer teacherPayoutRate) {
        this.teacherPayoutRate = teacherPayoutRate;
    }

    public Integer getTrialRegistrationFee() {
        return trialRegistrationFee;
    }

    public void setTrialRegistrationFee(Integer trialRegistrationFee) {
        this.trialRegistrationFee = trialRegistrationFee;
    }

    public List<CoursePlanAdminData> getAdminData() {
        return adminData;
    }

    public void setAdminData(List<CoursePlanAdminData> adminData) {
        this.adminData = adminData;
    }

    public String getArmFormId() {
        return armFormId;
    }

    public void setArmFormId(String armFormId) {
        this.armFormId = armFormId;
    }

    public Integer getIntendedHours() {
        return intendedHours;
    }

    public void setIntendedHours(Integer intendedHours) {
        this.intendedHours = intendedHours;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (title == null) {
            errors.add("title");
        }
        if (teacherId == null) {
            errors.add("teacherId");
        }
        if (studentId == null) {
            errors.add("studentId");
        }
        if (subject == null) {
            errors.add("subject");
        }
        if (boardId == null) {
            errors.add("boardId");
        }
        if (grade == null) {
            errors.add("grade");
        }
        if (price == null || price < 0) {
            errors.add("price");
        }
        if (teacherPayoutRate == null || teacherPayoutRate < 0) {
            errors.add("teacherPayoutRate");
        }

        if (trialRegistrationFee != null && trialRegistrationFee < 0) {
            errors.add("negative trialRegistrationFee");
        }

        if (ArrayUtils.isNotEmpty(instalmentsInfo)) {

            Collections.sort(instalmentsInfo, new Comparator<BaseInstalmentInfo>() {
                @Override
                public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                    if (o1.getDueTime() == null) {
                        return (o2.getDueTime() == null) ? 0 : 1;
                    }
                    if (o2.getDueTime() == null) {
                        return -1;
                    }
                    return o1.getDueTime().compareTo(o2.getDueTime());
                }
            });
            if (instalmentsInfo.get(0).getDueTime() == null) {
                errors.add("first instalment dueTime is null");
            } else if (regularSessionSchedule != null
                    && ArrayUtils.isNotEmpty(regularSessionSchedule.getSessionSlots())) {
                Long startTimeOfFirstSession = regularSessionSchedule.getStartDate();
                Long firstDueTime = instalmentsInfo.get(0).getDueTime();
                if (firstDueTime > startTimeOfFirstSession) {
                    errors.add("first session in regular schedule is before first instalment due time");
                }
            }

            for (BaseInstalmentInfo instalmentInfo : instalmentsInfo) {
                if (instalmentInfo.getDueTime() == null
                        || instalmentInfo.getDueTime() < System.currentTimeMillis()
                        || instalmentInfo.getDueTime() <= 0) {
                    errors.add("dueTime of one of the instalments is not set or is in the past");
                }
                if (instalmentInfo.getAmount() == null || instalmentInfo.getAmount() < 0) {
                    errors.add("amount of one of the instalments is not set or is less than 0");
                    break;
                }
            }
        }
        
        List<SessionSlot> allSlots = new ArrayList<>();
        if (regularSessionSchedule != null
                && ArrayUtils.isNotEmpty(regularSessionSchedule.getSessionSlots())) {
            allSlots.addAll(regularSessionSchedule.getSessionSlots());
        }
        if (trialSessionSchedule != null
                && ArrayUtils.isNotEmpty(trialSessionSchedule.getSessionSlots())) {
            allSlots.addAll(trialSessionSchedule.getSessionSlots());
        }

        errors.addAll(regularSessionSchedule.collectVerificationErrors(allSlots));

        return errors;
    }
        
}
