package com.vedantu.subscription.request;

import com.vedantu.lms.cmds.enums.VideoType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 * Created by somil on 24/05/17.
 */
public class AddVideoAnalyticsDataReq extends AbstractFrontEndReq {
    private Long userId;
    private EntityType contextType;
    private String contextId;
    private String videoId;
    private VideoType videoType;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

    @Override
    public String toString() {
        return "AddVideoAnalyticsDataReq{" +
                "userId=" + userId +
                ", contextType=" + contextType +
                ", contextId='" + contextId + '\'' +
                ", videoId='" + videoId + '\'' +
                ", videoType=" + videoType +
                '}';
    }
}
