package com.vedantu.subscription.request;

import com.vedantu.session.pojo.SessionSchedule;

public class BlockSlotScheduleReq {
	private String studentId;
	private String teacherId;
	private String referenceId;
	private SessionSchedule schedule;

	public BlockSlotScheduleReq() {
		super();
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public SessionSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(SessionSchedule schedule) {
		this.schedule = schedule;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "BlockSlotScheduleReq [studentId=" + studentId + ", teacherId=" + teacherId + ", referenceId="
				+ referenceId + ", schedule=" + schedule + "]";
	}
}
