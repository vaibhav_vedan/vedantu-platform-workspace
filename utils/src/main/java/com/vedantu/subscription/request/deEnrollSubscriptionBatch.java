package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class deEnrollSubscriptionBatch extends AbstractFrontEndReq {
    private String userId;
    private String batchId;
    private String bundleId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }
}
