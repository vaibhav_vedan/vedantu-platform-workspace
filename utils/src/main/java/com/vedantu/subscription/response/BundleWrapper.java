package com.vedantu.subscription.response;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.pojo.AdminTag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BundleWrapper
{
    private List<Track> webinarCategories;
    private VideoContentData videos;
    private TestContentData tests;
    private String title;
    private List<String> subject;
    private List<String> target;
    private List<Integer> grade;
    private Long boardId;
    private BundleState state = BundleState.DRAFT;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<String> tags;
    private List<String> teacherIds;
    private boolean featured;
    // lowest is highest
    private Integer priority;

    private String demoVideoUrl;
    private Integer noOfWebinars;
    private Integer noOfVideos;
    private Integer noOfTests;
    private List<String> courses;
    private Map<String, Object> metadata;

    private int trailAmount;
    private int trailAllowedDays;
    private Boolean trailIncluded;
    private Long validTill;
    private int validDays;
    private Boolean tabletIncluded;
    private Boolean amIncluded;
    private Boolean unLimitedDoubtsIncluded;
    private List<String> suggestionPackages;
    private List<AioPackage> packages;
    private int packagesSize;
    private Boolean isTrail;
    private List<CourseTerm> searchTerms;
    private Boolean isRankBooster;
    private BundleDetails bundleDetails;
    private Boolean isInstalmentVisible = Boolean.FALSE;
    private Boolean isSubscription = Boolean.FALSE ;
    private DurationTime durationtime;
    private Long lastPurchaseDate;
    private Integer minimumPurchasePrice;

    private String id;

    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String lastUpdatedBy; // For tracking last updated by
    private EntityState entityState = EntityState.ACTIVE;
    private Set<AdminTag> adminTags;

    private String messageStrip;
    private String messageStripUrl;

}
