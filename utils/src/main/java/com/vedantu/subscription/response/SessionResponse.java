package com.vedantu.subscription.response;

import com.vedantu.scheduling.pojo.session.SessionInfo;
import java.util.ArrayList;
import java.util.List;

public class SessionResponse {

    private List<SessionInfo> sessionInfo;
    private SubscriptionResponse subscriptionResponse;

    public SessionResponse() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SessionResponse(List<SessionInfo> sessionInfo, SubscriptionResponse subscriptionResponse) {
        super();
        this.sessionInfo = sessionInfo;
        this.subscriptionResponse = subscriptionResponse;
    }

    public SessionResponse(List<SessionInfo> sessionInfo, CancelSubscriptionResponse cancelSubscriptionResponse) {
        super();
        this.sessionInfo = sessionInfo;

        //TODO: hack for passing submodel
        SubscriptionResponse subscriptionResponseTemp = new SubscriptionResponse();
        subscriptionResponseTemp.setSubModel(cancelSubscriptionResponse.getSubModel());
        subscriptionResponseTemp.setModel(cancelSubscriptionResponse.getModel());
        this.subscriptionResponse = subscriptionResponseTemp;
    }

    public SessionResponse(SessionInfo sessionInfo, SubscriptionResponse sr) {
        super();
        this.sessionInfo = new ArrayList<>();
        this.sessionInfo.add(sessionInfo);
        this.subscriptionResponse = sr;
    }

    public List<SessionInfo> getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(List<SessionInfo> sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public SubscriptionResponse getSubscriptionResponse() {
        return subscriptionResponse;
    }

    public void setSubscriptionResponse(SubscriptionResponse subscriptionResponse) {
        this.subscriptionResponse = subscriptionResponse;
    }

    @Override
    public String toString() {
        return "SessionResponse [sessionInfo=" + sessionInfo + ", subscriptionResponse=" + subscriptionResponse + "]";
    }

}
