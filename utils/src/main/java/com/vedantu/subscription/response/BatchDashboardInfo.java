package com.vedantu.subscription.response;

import com.vedantu.subscription.enums.BatchCurriculumStatus;

public class BatchDashboardInfo {

    private String batchId;
    private String courseId;
    private String courseTitle;
    private Integer numStudentsEnrolled;
    private Integer numStudentsPaidBulk;
    private Integer numStudentsPaidInstallment;
    private Integer numStudentsActive;
    private Integer numSessionsPlanned;
    private Integer numSessionsScheduled;
    private Integer numSessionsDone;
    private Integer numSessionsCancelled;
    private Integer numSessionsRescheduled;
    private Integer numExtraSessions;
    private BatchCurriculumStatus curriculumProgress;
    private Integer numTestShared;
    private Integer numTestAttempted;
    private Integer numAssignShared;
    private Integer numAssignAttempted;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public Integer getNumStudentsEnrolled() {
        return numStudentsEnrolled;
    }

    public void setNumStudentsEnrolled(Integer numStudentsEnrolled) {
        this.numStudentsEnrolled = numStudentsEnrolled;
    }

    public Integer getNumStudentsPaidBulk() {
        return numStudentsPaidBulk;
    }

    public void setNumStudentsPaidBulk(Integer numStudentsPaidBulk) {
        this.numStudentsPaidBulk = numStudentsPaidBulk;
    }

    public Integer getNumStudentsPaidInstallment() {
        return numStudentsPaidInstallment;
    }

    public void setNumStudentsPaidInstallment(Integer numStudentsPaidInstallment) {
        this.numStudentsPaidInstallment = numStudentsPaidInstallment;
    }

    public Integer getNumStudentsActive() {
        return numStudentsActive;
    }

    public void setNumStudentsActive(Integer numStudentsActive) {
        this.numStudentsActive = numStudentsActive;
    }

    public Integer getNumSessionsPlanned() {
        return numSessionsPlanned;
    }

    public void setNumSessionsPlanned(Integer numSessionsPlanned) {
        this.numSessionsPlanned = numSessionsPlanned;
    }

    public Integer getNumSessionsScheduled() {
        return numSessionsScheduled;
    }

    public void setNumSessionsScheduled(Integer numSessionsScheduled) {
        this.numSessionsScheduled = numSessionsScheduled;
    }

    public Integer getNumSessionsDone() {
        return numSessionsDone;
    }

    public void setNumSessionsDone(Integer numSessionsDone) {
        this.numSessionsDone = numSessionsDone;
    }

    public Integer getNumSessionsCancelled() {
        return numSessionsCancelled;
    }

    public void setNumSessionsCancelled(Integer numSessionsCancelled) {
        this.numSessionsCancelled = numSessionsCancelled;
    }

    public Integer getNumSessionsRescheduled() {
        return numSessionsRescheduled;
    }

    public void setNumSessionsRescheduled(Integer numSessionsRescheduled) {
        this.numSessionsRescheduled = numSessionsRescheduled;
    }

    public Integer getNumExtraSessions() {
        return numExtraSessions;
    }

    public void setNumExtraSessions(Integer numExtraSessions) {
        this.numExtraSessions = numExtraSessions;
    }

    public BatchCurriculumStatus getCurriculumProgress() {
        return curriculumProgress;
    }

    public void setCurriculumProgress(BatchCurriculumStatus curriculumProgress) {
        this.curriculumProgress = curriculumProgress;
    }

    public Integer getNumTestShared() {
        return numTestShared;
    }

    public void setNumTestShared(Integer numTestShared) {
        this.numTestShared = numTestShared;
    }

    public Integer getNumTestAttempted() {
        return numTestAttempted;
    }

    public void setNumTestAttempted(Integer numTestAttempted) {
        this.numTestAttempted = numTestAttempted;
    }

    public Integer getNumAssignShared() {
        return numAssignShared;
    }

    public void setNumAssignShared(Integer numAssignShared) {
        this.numAssignShared = numAssignShared;
    }

    public Integer getNumAssignAttempted() {
        return numAssignAttempted;
    }

    public void setNumAssignAttempted(Integer numAssignAttempted) {
        this.numAssignAttempted = numAssignAttempted;
    }
}
