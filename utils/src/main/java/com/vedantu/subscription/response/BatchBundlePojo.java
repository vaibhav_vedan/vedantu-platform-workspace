/**
 * 
 */
package com.vedantu.subscription.response;

import java.util.List;
import java.util.Map;

import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.enums.CourseCategory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BatchBundlePojo {
	
	private String bundleId;
	private String premiumBundleTitle;
	private List<String> batchIds;
	private CourseCategory courseCategory;
	private Map<String, EnrollmentState> batchEnrollmentStateMap;
	private Map<String, EntityStatus> batchEnrollmentStatusMap;
	private Integer noOfDaysOfFreeAccess;


}
