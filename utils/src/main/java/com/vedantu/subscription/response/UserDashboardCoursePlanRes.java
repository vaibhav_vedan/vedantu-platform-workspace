/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.response;

import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author jeet
 */
public class UserDashboardCoursePlanRes extends AbstractRes{
    private CoursePlanInfo coursePlanInfo;
    private List<DashBoardInstalmentInfo> dashBoardInstalmentInfos;
    private CoursePlanDashboardSessionInfo coursePlanDashboardSessionInfo;
    private Orders orderInfo;
    
    public UserDashboardCoursePlanRes(CoursePlanInfo info){
        this.coursePlanInfo = info;
    }
    
    public CoursePlanInfo getCoursePlanInfo() {
        return coursePlanInfo;
    }

    public void setCoursePlanInfo(CoursePlanInfo coursePlanInfo) {
        this.coursePlanInfo = coursePlanInfo;
    }

    public List<DashBoardInstalmentInfo> getDashBoardInstalmentInfos() {
        return dashBoardInstalmentInfos;
    }

    public void setDashBoardInstalmentInfos(List<DashBoardInstalmentInfo> dashBoardInstalmentInfos) {
        this.dashBoardInstalmentInfos = dashBoardInstalmentInfos;
    }

    public CoursePlanDashboardSessionInfo getCoursePlanDashboardSessionInfo() {
        return coursePlanDashboardSessionInfo;
    }

    public void setCoursePlanDashboardSessionInfo(CoursePlanDashboardSessionInfo coursePlanDashboardSessionInfo) {
        this.coursePlanDashboardSessionInfo = coursePlanDashboardSessionInfo;
    }

    public Orders getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(Orders orderInfo) {
        this.orderInfo = orderInfo;
    }
    
}
