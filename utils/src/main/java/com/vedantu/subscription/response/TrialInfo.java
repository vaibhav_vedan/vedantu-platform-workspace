package com.vedantu.subscription.response;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class TrialInfo {
    private Long cutPrice;
    private Long trialAmount;
    private Long trialAllowedDays;
    private int gracePeriod=7;
    private EntityType contextType;
    private String contextId;
    private String id;
}
