package com.vedantu.subscription.response;

import com.vedantu.subscription.enums.game.GameTaskStatus;
import lombok.Data;

@Data
public class ClaimedUsersResponse {

    private Long userId;
    private Long claimedTime;
    private String rewardName;
    private String rewardItem;
    private String awb;
    private GameTaskStatus status;
}