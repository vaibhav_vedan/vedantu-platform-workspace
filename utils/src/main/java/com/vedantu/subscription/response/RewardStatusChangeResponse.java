package com.vedantu.subscription.response;

import com.vedantu.subscription.enums.game.GameTaskStatus;
import lombok.Data;

@Data
public class RewardStatusChangeResponse {

    private Long userId;
    private String rewardName;
    private GameTaskStatus prevStatus;
    private GameTaskStatus currStatus;
    private String item;
}
