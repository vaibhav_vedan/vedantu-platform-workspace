/**
 * 
 */
package com.vedantu.subscription.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PremiumSubscriptionResp {
	
	@Builder.Default
	private Boolean isPremiumSubscriber = false;
	
	private Integer noOfDaysOfFreeAccess;
	
	private Long enrollmentEndTime;
	
	private String premiumBundleTitle;

}
