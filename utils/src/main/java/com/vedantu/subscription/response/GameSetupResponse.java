package com.vedantu.subscription.response;

import com.vedantu.subscription.enums.game.Eligibility;
import com.vedantu.subscription.pojo.game.GameTask;
import lombok.Data;

import java.util.List;

@Data
public class GameSetupResponse {

    private Eligibility eligibility;
    private List<GameTask> gameTaskList;
}
