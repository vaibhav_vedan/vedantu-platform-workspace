package com.vedantu.subscription.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;

public class GetCoursePlanReminderListRes {

	private Long agentId;
	private UserBasicInfo agent;
	private String agentName;
	private Long studentId;
        private Long teacherId;
	private UserBasicInfo student;
        private UserBasicInfo teacher;
        
	private Long trialTime;
	private SessionState sessionState;
//	private SessionInfo sessionInfo;
	private Long instalmentDueTime;
	private CoursePlanState state;
        private Long coursePlanStartDate;
        private String coursePlanTitle;
	private String coursePlanId;
	private String coursePlanLink;
        private DashboardUserInstalmentHistoryRes extraInfos;

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public UserBasicInfo getAgent() {
		return agent;
	}

	public void setAgent(UserBasicInfo agent) {
		this.agent = agent;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public UserBasicInfo getStudent() {
		return student;
	}

	public void setStudent(UserBasicInfo student) {
		this.student = student;
	}

	public Long getTrialTime() {
		return trialTime;
	}

	public void setTrialTime(Long trialTime) {
		this.trialTime = trialTime;
	}

	public Long getInstalmentDueTime() {
		return instalmentDueTime;
	}

	public void setInstalmentDueTime(Long instalmentDueTime) {
		this.instalmentDueTime = instalmentDueTime;
	}

	public CoursePlanState getState() {
		return state;
	}

	public void setState(CoursePlanState state) {
		this.state = state;
	}

//	public CoursePlanBasicInfo getCoursePlan() {
//		return coursePlan;
//	}
//
//	public void setCoursePlan(CoursePlanBasicInfo coursePlan) {
//		this.coursePlan = coursePlan;
//	}

	public String getCoursePlanId() {
		return coursePlanId;
	}

	public void setCoursePlanId(String coursePlanId) {
		this.coursePlanId = coursePlanId;
	}

	public String getCoursePlanLink() {
		return coursePlanLink;
	}

	public void setCoursePlanLink(String coursePlanLink) {
		this.coursePlanLink = coursePlanLink;
	}

	public SessionState getSessionState() {
		return sessionState;
	}

	public void setSessionState(SessionState sessionState) {
		this.sessionState = sessionState;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

//	public SessionInfo getSessionInfo() {
//		return sessionInfo;
//	}
//
//	public void setSessionInfo(SessionInfo sessionInfo) {
//		this.sessionInfo = sessionInfo;
//	}

        public Long getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(Long teacherId) {
            this.teacherId = teacherId;
        }

        public UserBasicInfo getTeacher() {
            return teacher;
        }

        public void setTeacher(UserBasicInfo teacher) {
            this.teacher = teacher;
        }

        public Long getCoursePlanStartDate() {
            return coursePlanStartDate;
        }

        public void setCoursePlanStartDate(Long coursePlanStartDate) {
            this.coursePlanStartDate = coursePlanStartDate;
        }

        public String getCoursePlanTitle() {
            return coursePlanTitle;
        }

        public void setCoursePlanTitle(String coursePlanTitle) {
            this.coursePlanTitle = coursePlanTitle;
        }

        public DashboardUserInstalmentHistoryRes getExtraInfos() {
            return extraInfos;
        }

        public void setExtraInfos(DashboardUserInstalmentHistoryRes extraInfos) {
            this.extraInfos = extraInfos;
        }
}
