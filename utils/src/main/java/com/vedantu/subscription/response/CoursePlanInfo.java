/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.response;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.FAQPojo;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.pojo.OTOCurriculumPojo;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class CoursePlanInfo extends CoursePlanBasicInfo {

    private List<OTOCurriculumPojo> curriculum;
    private Set<Long> suggestedTeacherIds;
    private List<FAQPojo> faq;
    private List<String> tags;
    private Map<String, String> keyValues;
    private String demoVideoUrl;
    private SessionSchedule trialSessionSchedule;
    private SessionSchedule regularSessionSchedule;
    private List<BaseInstalmentInfo> instalmentsInfo;

    private Long endTime; // CancelSubscriptionTime
    private String endReason; // Subscription End Reason
    private Long endedBy; // UserId which ended Subscription
    private CoursePlanEnums.CoursePlanEndType endType;
    private List<SessionInfo> sessionList;
    private Integer intendedHours;//millis

    public List<OTOCurriculumPojo> getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(List<OTOCurriculumPojo> curriculum) {
        this.curriculum = curriculum;
    }

    public Set<Long> getSuggestedTeacherIds() {
        return suggestedTeacherIds;
    }

    public void setSuggestedTeacherIds(Set<Long> suggestedTeacherIds) {
        this.suggestedTeacherIds = suggestedTeacherIds;
    }

    public List<FAQPojo> getFaq() {
        return faq;
    }

    public void setFaq(List<FAQPojo> faq) {
        this.faq = faq;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public String getDemoVideoUrl() {
        return demoVideoUrl;
    }

    public void setDemoVideoUrl(String demoVideoUrl) {
        this.demoVideoUrl = demoVideoUrl;
    }

    public SessionSchedule getTrialSessionSchedule() {
        return trialSessionSchedule;
    }

    public void setTrialSessionSchedule(SessionSchedule trialSessionSchedule) {
        this.trialSessionSchedule = trialSessionSchedule;
    }

    public SessionSchedule getRegularSessionSchedule() {
        return regularSessionSchedule;
    }

    public void setRegularSessionSchedule(SessionSchedule regularSessionSchedule) {
        this.regularSessionSchedule = regularSessionSchedule;
    }

    public List<BaseInstalmentInfo> getInstalmentsInfo() {
        return instalmentsInfo;
    }

    public void setInstalmentsInfo(List<BaseInstalmentInfo> instalmentsInfo) {
        this.instalmentsInfo = instalmentsInfo;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public CoursePlanEnums.CoursePlanEndType getEndType() {
        return endType;
    }

    public void setEndType(CoursePlanEnums.CoursePlanEndType endType) {
        this.endType = endType;
    }

    public List<SessionInfo> getSessionList() {
        return sessionList;
    }

    public void setSessionList(List<SessionInfo> sessionList) {
        this.sessionList = sessionList;
    }

    public Integer getIntendedHours() {
        return intendedHours;
    }

    public void setIntendedHours(Integer intendedHours) {
        this.intendedHours = intendedHours;
    }

}
