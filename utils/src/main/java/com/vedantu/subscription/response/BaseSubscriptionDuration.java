package com.vedantu.subscription.response;

public enum BaseSubscriptionDuration {
    MONTHLY("MONTHLY"), FULLCOURSE("FULLCOURSE");
    private final String name;

    BaseSubscriptionDuration(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
