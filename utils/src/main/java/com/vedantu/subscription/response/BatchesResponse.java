/**
 * 
 */
package com.vedantu.subscription.response;

import java.util.List;

import com.vedantu.subscription.enums.CourseCategory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BatchesResponse {

	private String sectionTitle;
	
	private CourseCategory courseCategory;
	
	private List<BundleOfBatchResponse> bundleOfBatchesResponses;
}
