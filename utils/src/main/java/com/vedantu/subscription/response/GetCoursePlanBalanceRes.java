/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author ajith
 */
public class GetCoursePlanBalanceRes extends AbstractRes {

    private float totalBillingDurationInHrs;//hrs
    private int hourlyRate;//in paisa
    private int amountPaid;
    private int npPaid;
    private int pPaid;
    private int amountLeft;
    private int npLeft;
    private int pLeft;
    private float discountRatio;
    private int discountAllowed;
    private int discountClaimed;
    private int discountLeft;

    public float getTotalBillingDurationInHrs() {
        return totalBillingDurationInHrs;
    }

    public void setTotalBillingDurationInHrs(float totalBillingDurationInHrs) {
        this.totalBillingDurationInHrs = totalBillingDurationInHrs;
    }

    public int getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(int hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(int amountPaid) {
        this.amountPaid = amountPaid;
    }

    public int getNpPaid() {
        return npPaid;
    }

    public void setNpPaid(int npPaid) {
        this.npPaid = npPaid;
    }

    public int getpPaid() {
        return pPaid;
    }

    public void setpPaid(int pPaid) {
        this.pPaid = pPaid;
    }

    public int getAmountLeft() {
        return amountLeft;
    }

    public void setAmountLeft(int amountLeft) {
        this.amountLeft = amountLeft;
    }

    public int getNpLeft() {
        return npLeft;
    }

    public void setNpLeft(int npLeft) {
        this.npLeft = npLeft;
    }

    public int getpLeft() {
        return pLeft;
    }

    public void setpLeft(int pLeft) {
        this.pLeft = pLeft;
    }

    public float getDiscountRatio() {
        return discountRatio;
    }

    public void setDiscountRatio(float discountRatio) {
        this.discountRatio = discountRatio;
    }

    public int getDiscountAllowed() {
        return discountAllowed;
    }

    public void setDiscountAllowed(int discountAllowed) {
        this.discountAllowed = discountAllowed;
    }

    public int getDiscountClaimed() {
        return discountClaimed;
    }

    public void setDiscountClaimed(int discountClaimed) {
        this.discountClaimed = discountClaimed;
    }

    public int getDiscountLeft() {
        return discountLeft;
    }

    public void setDiscountLeft(int discountLeft) {
        this.discountLeft = discountLeft;
    }

}
