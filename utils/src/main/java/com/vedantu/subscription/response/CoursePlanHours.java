package com.vedantu.subscription.response;

public class CoursePlanHours {

	private String coursePlanId;
	private Integer totalHours;// millis
	private Integer consumedHours;// millis
	private Integer lockedHours;// millis
	private Integer remainingHours;// millis

	public CoursePlanHours() {
		super();
	}

	public String getCoursePlanId() {
		return coursePlanId;
	}

	public void setCoursePlanId(String coursePlanId) {
		this.coursePlanId = coursePlanId;
	}

	public Integer getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public Integer getConsumedHours() {
		return consumedHours;
	}

	public void setConsumedHours(Integer consumedHours) {
		this.consumedHours = consumedHours;
	}

	public Integer getLockedHours() {
		return lockedHours;
	}

	public void setLockedHours(Integer lockedHours) {
		this.lockedHours = lockedHours;
	}

	public Integer getRemainingHours() {
		return remainingHours;
	}

	public void setRemainingHours(Integer remainingHours) {
		this.remainingHours = remainingHours;
	}

	@Override
	public String toString() {
		return "CoursePlanHours [coursePlanId=" + coursePlanId + ", totalHours=" + totalHours + ", consumedHours="
				+ consumedHours + ", lockedHours=" + lockedHours + ", remainingHours=" + remainingHours
				+ ", toString()=" + super.toString() + "]";
	}
}
