package com.vedantu.subscription.response;

import java.util.List;

public class CurriculumProgressInfo {

    private List<String> topicToBeCovered; //to be covered till now
    private List<String> topicDone;//topics done till now

    public List<String> getTopicToBeCovered() {
        return topicToBeCovered;
    }

    public void setTopicToBeCovered(List<String> topicToBeCovered) {
        this.topicToBeCovered = topicToBeCovered;
    }

    public List<String> getTopicDone() {
        return topicDone;
    }

    public void setTopicDone(List<String> topicDone) {
        this.topicDone = topicDone;
    }
}
