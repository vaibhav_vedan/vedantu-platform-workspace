/**
 * 
 */
package com.vedantu.subscription.response;

import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TrialExpirationResp {
	
	private String userId;
	
	@Builder.Default
	private Boolean isPaidUser = false;
	
	@Builder.Default
	private Boolean isTrialUser = false;
	
	private String bundleId;
	
	private EntityStatus status;
	
	private EnrollmentState state;
	
	private String trialId;
	
	private Long validTill;
	

}
