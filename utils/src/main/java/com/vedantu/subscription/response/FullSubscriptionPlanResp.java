package com.vedantu.subscription.response;

import com.vedantu.subscription.enums.SubscriptionPlanType;
import com.vedantu.subscription.pojo.bundle.SubscriptionMedia;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlan;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by Dpadhya
 */
@Data
public class FullSubscriptionPlanResp extends AbstractFrontEndReq {
    private Map<String,List<BaseSubscriptionPlanResp>> subscriptionPlanMap;
    private String offerStrip;
    private String subscriptionOfferStrip;
    private Map<String,List<TrialInfo>> trialInfoMap;
    private String enrolledTrial;

}
