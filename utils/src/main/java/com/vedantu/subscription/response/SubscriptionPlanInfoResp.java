package com.vedantu.subscription.response;

import com.vedantu.dinero.enums.BaseSubscriptionPurchaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author darshit
 */
@Data
@AllArgsConstructor
public class SubscriptionPlanInfoResp {

    private Integer validMonths;

    private String orderId;
    private Long creationTime;

}
