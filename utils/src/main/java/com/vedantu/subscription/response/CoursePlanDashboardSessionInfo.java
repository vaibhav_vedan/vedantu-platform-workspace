/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class CoursePlanDashboardSessionInfo  extends AbstractRes{
    private Long firstSessionTime; //
    private Long lastSessionTime; //
    private Long upcomingSessionTime; //
    private Long studentNotJoined;
    private Long teacherNotJoined;
    private Long expiredSessionCount; //
    private Long forfittedSessionCount; //
    private Long canceledSessionCount; //
    private Long totalSessionCount; //
    private Long completedSessionCount; //
    private Long upcomingSessionCount; //
    private String coursePlanId; //

    public Long getFirstSessionTime() {
        return firstSessionTime;
    }

    public void setFirstSessionTime(Long firstSessionTime) {
        this.firstSessionTime = firstSessionTime;
    }

    public Long getLastSessionTime() {
        return lastSessionTime;
    }

    public void setLastSessionTime(Long lastSessionTime) {
        this.lastSessionTime = lastSessionTime;
    }

    public Long getUpcomingSessionTime() {
        return upcomingSessionTime;
    }

    public void setUpcomingSessionTime(Long upcomingSessionTime) {
        this.upcomingSessionTime = upcomingSessionTime;
    }

    public Long getStudentNotJoined() {
        return studentNotJoined;
    }

    public void setStudentNotJoined(Long studentNotJoined) {
        this.studentNotJoined = studentNotJoined;
    }

    public Long getTeacherNotJoined() {
        return teacherNotJoined;
    }

    public void setTeacherNotJoined(Long teacherNotJoined) {
        this.teacherNotJoined = teacherNotJoined;
    }

    public Long getExpiredSessionCount() {
        return expiredSessionCount;
    }

    public void setExpiredSessionCount(Long expiredSessionCount) {
        this.expiredSessionCount = expiredSessionCount;
    }

    public Long getCanceledSessionCount() {
        return canceledSessionCount;
    }

    public void setCanceledSessionCount(Long canceledSessionCount) {
        this.canceledSessionCount = canceledSessionCount;
    }

    public Long getTotalSessionCount() {
        return totalSessionCount;
    }

    public void setTotalSessionCount(Long totalSessionCount) {
        this.totalSessionCount = totalSessionCount;
    }

    public Long getCompletedSessionCount() {
        return completedSessionCount;
    }

    public void setCompletedSessionCount(Long completedSessionCount) {
        this.completedSessionCount = completedSessionCount;
    }

    public Long getUpcomingSessionCount() {
        return upcomingSessionCount;
    }

    public void setUpcomingSessionCount(Long upcomingSessionCount) {
        this.upcomingSessionCount = upcomingSessionCount;
    }

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public Long getForfittedSessionCount() {
        return forfittedSessionCount;
    }

    public void setForfittedSessionCount(Long forfittedSessionCount) {
        this.forfittedSessionCount = forfittedSessionCount;
    }
    
}
