package com.vedantu.subscription.response;

import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.pojo.Pollsdata;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.pojo.BatchIdCourseTitlePojo;

import java.util.List;

public class OTMSessionDashboardInfo implements Comparable<OTMSessionDashboardInfo>{

    private List<String> batchIds;

    private List<BatchIdCourseTitlePojo> courseTitles;
    private String sessionId;
    private String sessionTitle;
    private Long boardId;
    private String teacherId;
    private Long sessionStartTime;
    private String subject;
    private String teacher;
    private String topicToBeCovered;
    private String topicDone;
    private boolean rescheduled;
    private OTMSessionType otmSessionType;
    private SessionState sessionStatus;
    private Float studentsAttendance;
    private Integer studentPresent;
    private Integer studentTotal;
    private Long teacherJoinTime;
    private List<String> testsShared;
    private Integer testsAttempted;
    private Integer testsEvaluated;
    private List<String> assignmentShared;
    private Integer assignmentAttempted;
    private Integer assignmentEvaluated;
    private String vimeoReplayId;
    private Pollsdata pollsdata;
    private Long startTime;
    private Long endTime;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getTestsAttempted() {
        return testsAttempted;
    }

    public void setTestsAttempted(Integer testsAttempted) {
        this.testsAttempted = testsAttempted;
    }

    public List<String> getAssignmentShared() {
        return assignmentShared;
    }

    public void setAssignmentShared(List<String> assignmentShared) {
        this.assignmentShared = assignmentShared;
    }

    public Integer getAssignmentAttempted() {
        return assignmentAttempted;
    }

    public void setAssignmentAttempted(Integer assignmentAttempted) {
        this.assignmentAttempted = assignmentAttempted;
    }

    public Integer getAssignmentEvaluated() {
        return assignmentEvaluated;
    }

    public void setAssignmentEvaluated(Integer assignmentEvaluated) {
        this.assignmentEvaluated = assignmentEvaluated;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getSessionTitle() {
        return sessionTitle;
    }

    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    public List<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(List<String> batchId) {
        this.batchIds = batchId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(Long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getTopicToBeCovered() {
        return topicToBeCovered;
    }

    public void setTopicToBeCovered(String topicToBeCovered) {
        this.topicToBeCovered = topicToBeCovered;
    }

    public String getTopicDone() {
        return topicDone;
    }

    public void setTopicDone(String topicDone) {
        this.topicDone = topicDone;
    }

    public boolean isRescheduled() {
        return rescheduled;
    }

    public void setRescheduled(boolean rescheduled) {
        this.rescheduled = rescheduled;
    }

    public OTMSessionType getOtmSessionType() {
        return otmSessionType;
    }

    public void setOtmSessionType(OTMSessionType otmSessionType) {
        this.otmSessionType = otmSessionType;
    }

    public SessionState getSessionStatus() {
        return sessionStatus;
    }

    public void setSessionStatus(SessionState sessionStatus) {
        this.sessionStatus = sessionStatus;
    }

    public Float getStudentsAttendance() {
        return studentsAttendance;
    }

    public void setStudentsAttendance(Float studentsAttendance) {
        this.studentsAttendance = studentsAttendance;
    }

    public Long getTeacherJoinTime() {
        return teacherJoinTime;
    }

    public void setTeacherJoinTime(Long teacherJoinTime) {
        this.teacherJoinTime = teacherJoinTime;
    }

    public List<String> getTestsShared() {
        return testsShared;
    }

    public void setTestsShared(List<String> testsShared) {
        this.testsShared = testsShared;
    }

    public Integer getTestsEvaluated() {
        return testsEvaluated;
    }

    public void setTestsEvaluated(Integer testsEvaluated) {
        this.testsEvaluated = testsEvaluated;
    }

    public String getVimeoReplayId() {
        return vimeoReplayId;
    }

    public void setVimeoReplayId(String vimeoReplayId) {
        this.vimeoReplayId = vimeoReplayId;
    }

    public List<BatchIdCourseTitlePojo> getCourseTitles() {
        return courseTitles;
    }

    public void setCourseTitles(List<BatchIdCourseTitlePojo> courseTitles) {
        this.courseTitles = courseTitles;
    }

    public Pollsdata getPollsdata() {
        return pollsdata;
    }

    public void setPollsdata(Pollsdata pollsdata) {
        this.pollsdata = pollsdata;
    }
    

    @Override
    public String toString() {
        return "OTMSessionDashboardInfo{" + "batchId=" + batchIds+ ", courseId="  + ", courseTitles=" + courseTitles + ", sessionId=" + sessionId + ", sessionTitle=" + sessionTitle + ", boardId=" + boardId + ", teacherId=" + teacherId + ", sessionStartTime=" + sessionStartTime + ", subject=" + subject + ", teacher=" + teacher + ", topicToBeCovered=" + topicToBeCovered + ", topicDone=" + topicDone + ", rescheduled=" + rescheduled + ", otmSessionType=" + otmSessionType + ", sessionStatus=" + sessionStatus + ", studentsAttendance=" + studentsAttendance + ", teacherJoinTime=" + teacherJoinTime + ", testsShared=" + testsShared + ", testsAttempted=" + testsAttempted + ", testsEvaluated=" + testsEvaluated + ", assignmentShared=" + assignmentShared + ", assignmentAttempted=" + assignmentAttempted + ", assignmentEvaluated=" + assignmentEvaluated + ", vimeoReplayId=" + vimeoReplayId + '}';
    }
    
    @Override
    public int compareTo(OTMSessionDashboardInfo otmSessionDashboardInfo){
        return (this.getSessionStartTime() < otmSessionDashboardInfo.getSessionStartTime() ? -1 :
                (this.getSessionStartTime().equals(otmSessionDashboardInfo.getSessionStartTime())? 0: 1));

    }

    /**
     * @return the studentTotal
     */
    public Integer getStudentTotal() {
        return studentTotal;
    }

    /**
     * @param studentTotal the studentTotal to set
     */
    public void setStudentTotal(Integer studentTotal) {
        this.studentTotal = studentTotal;
    }

    /**
     * @return the studentPresent
     */
    public Integer getStudentPresent() {
        return studentPresent;
    }

    /**
     * @param studentPresent the studentPresent to set
     */
    public void setStudentPresent(Integer studentPresent) {
        this.studentPresent = studentPresent;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
    
}
