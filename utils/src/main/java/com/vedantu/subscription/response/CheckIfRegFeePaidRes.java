/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.response;

/**
 *
 * @author ajith
 */
public class CheckIfRegFeePaidRes {

    private boolean regFeePaid = true;
    private String orderId;

    public CheckIfRegFeePaidRes() {
    }

    public CheckIfRegFeePaidRes(String orderId, boolean regFeePaid) {
        this.orderId = orderId;
        this.regFeePaid = regFeePaid;
    }

    public boolean isRegFeePaid() {
        return regFeePaid;
    }

    public void setRegFeePaid(boolean regFeePaid) {
        this.regFeePaid = regFeePaid;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}
