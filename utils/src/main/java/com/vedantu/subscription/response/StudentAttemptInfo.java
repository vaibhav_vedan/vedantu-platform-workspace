package com.vedantu.subscription.response;

import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;

public class StudentAttemptInfo extends StudentEnrolledInfo {

    private Float marks;
    private Float totalMarks;
    private ContentState contentState;
    private boolean objective;
    private ContentType contentType;
    private String contentId;
    private String contentTitle;
    private boolean attempted;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public ContentState getContentState() {
        return contentState;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }


    public ContentState getContentInfoType() {
        return contentState;
    }

    public void setContentState(ContentState contentState) {
        this.contentState = contentState;
    }

    public Float getMarks() {
        return marks;
    }

    public void setMarks(Float marks) {
        this.marks = marks;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public boolean isObjective() {
        return objective;
    }

    public void setObjective(boolean objective) {
        this.objective = objective;
    }

    public boolean isAttempted() {
        return attempted;
    }

    public void setAttempted(boolean attempted) {
        this.attempted = attempted;
    }
    
}
