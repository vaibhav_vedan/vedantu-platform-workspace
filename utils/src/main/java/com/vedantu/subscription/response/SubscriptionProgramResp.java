package com.vedantu.subscription.response;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.subscription.pojo.bundle.SubscriptionMedia;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlan;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlanHighlights;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import lombok.Data;


/**
 * Created by Dpadhya
 */
@Data
public class SubscriptionProgramResp extends AbstractFrontEndReq {
    private List<SubscriptionPlan> subscriptionPlans;
    private String name;
    private Integer grade;
    private Integer year;
    private String target;
    private List<SubscriptionMedia> subscriptionMedias;
    private List<String> highLights;
    private List<String> functionality;
    private String offer;
    private String id;
    private List<TrialInfo> trialList;
    private Set<Integer> displayGrades;
    private Set<String> displaySubjects;
    private Set<String> displayTargets;
    private List<SubscriptionPlanHighlights> subscriptionPlanHighlights;
    private Map<String ,List<String>> courseCurriculumMap;
    private Boolean isHidden;
}
