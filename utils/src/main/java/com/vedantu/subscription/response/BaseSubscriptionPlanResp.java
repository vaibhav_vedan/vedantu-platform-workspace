package com.vedantu.subscription.response;

import com.vedantu.dinero.enums.BaseSubscriptionPurchaseEntity;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author darshit
 */
@Data
public class BaseSubscriptionPlanResp {
    private String id;
    private Integer validMonths;
    private BaseSubscriptionPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private Integer price;
    private Integer cutPrice;
    private BaseSubscriptionDuration planDuration;
    private long aioValidTill;

}
