package com.vedantu.subscription.response;

public class StudentAttendanceInfo extends StudentEnrolledInfo {

    private boolean present;
    private Long timeInSession;

    public boolean isPresent() {
        return present;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public Long getTimeInSession() {
        return timeInSession;
    }

    public void setTimeInSession(Long timeInSession) {
        this.timeInSession = timeInSession;
    }
}
