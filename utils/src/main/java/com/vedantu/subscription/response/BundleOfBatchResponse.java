/**
 * 
 */
package com.vedantu.subscription.response;

import java.util.List;

import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.SessionPlanPojo;
import com.vedantu.subscription.enums.CourseCategory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BundleOfBatchResponse {
	
	private CourseCategory courseCategory;
	
	private String daysOfClass;
	
	private String timings;
	
	private Long batchStartTime;
	
	private Long batchEndTime;
	@Builder.Default
	private Boolean isEnrolled = false;
	
	private List<SessionPlanPojo> sessionPlan;
	
	//private EntityStatus batchState;
	
	private String enrolledUsersCount;
	
	private List<TeacherBasicInfo> teacherBasicInfo;
	
	private EntityStatus enrollmentStatus; 
	
	private String bundleId;
	
	private String premiumBundleTitle; 
	
	private String courseDuration;
	
	private String subjectExpertise;
	
	private Long bundleValidFrom;
	
	private Long bundleValidTill;
	
	
}
