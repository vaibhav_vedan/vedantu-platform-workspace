package com.vedantu.subscription.response;

public class StudentEvaluationInfo {

    private boolean evaluated;

    public boolean isEvaluated() {
        return evaluated;
    }

    public void setEvaluated(boolean evaluated) {
        this.evaluated = evaluated;
    }
}
