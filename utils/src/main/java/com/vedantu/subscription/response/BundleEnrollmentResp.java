package com.vedantu.subscription.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * created by Darshit
 */
@Data
public class BundleEnrollmentResp extends AbstractMongoStringIdEntityBean {

    private List<Track> webinarCategories;
    private VideoContentData videos;
    private TestContentData tests;
    private String title;
    private List<String> subject;
    private List<String> target;
    private Integer grade;
    private Long boardId;
    private BundleState state = BundleState.DRAFT;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<String> tags;
    private List<String> teacherIds;
    private Integer noOfWebinars;
    private Integer noOfVideos;
    private Integer noOfTests;
    private List<UserBasicInfo> userBasicInfos;
    private List<String> courses;
    private List<BaseInstalmentInfo> instalmentDetails;
    private Map<String, Object> metadata;
    private int regAmount;
    private int regAllowedDays ;
    private Long validTill ;
    private int validDays ;
    private Boolean tabletIncluded ;
    private Boolean amIncluded ;
    private Boolean unLimitedDoubtsIncluded ;
    private List<String> suggestionPackages;
    private List<AioPackage> packages;
    private EntityStatus enrollmentStatus;
    private EnrollmentState enrollmentState;
    private String enrollmentId;
    private Boolean isSubscription = Boolean.FALSE ;
    private boolean entityExists;
}
