package com.vedantu.subscription.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

/**
 * Created by somil on 19/05/17.
 */
public class BundleEnrolmentInfo extends AbstractMongoStringIdEntityBean {
    private String bundleId;
    private String userId;
    private EntityStatus status;
    private EnrollmentState state;
    private UserBasicInfo userBasicInfo;
    private Long passExpirationTime;
    private Long endedBy;
    private String endReason;

    public Long getPassExpirationTime() {
        return passExpirationTime;
    }

    public void setPassExpirationTime(Long passExpirationTime) {
        this.passExpirationTime = passExpirationTime;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EntityStatus getStatus() {
        return status;
    }

    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    public EnrollmentState getState() {
        return state;
    }

    public void setState(EnrollmentState state) {
        this.state = state;
    }

    public UserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }
}
