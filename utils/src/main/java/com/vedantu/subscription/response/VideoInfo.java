package com.vedantu.subscription.response;

import com.vedantu.lms.cmds.enums.VideoType;

/**
 * Created by somil on 25/05/17.
 */
public class VideoInfo {
    private VideoType videoType;
    private String videoId;
    private Integer viewCount;

    public VideoInfo() {
    }

    public VideoInfo(VideoType videoType, String videoId) {
        this.videoType = videoType;
        this.videoId = videoId;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
