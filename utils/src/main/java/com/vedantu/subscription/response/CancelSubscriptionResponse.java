package com.vedantu.subscription.response;

import com.vedantu.dinero.request.RefundRes;
import java.util.List;

import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.enums.SessionModel;

public class CancelSubscriptionResponse extends RefundRes {

    private Long totalHours;
    private Long remainingHours;
    private Long lockedHours;
    private Long hourlyRate;
    private String refundReason;
    private List<Long> subscriptionDetailsIds;
    private Long studentId;
    private Long teacherId;
    private Long endedBy;
    private String subscriptionName;
    private List<SessionInfo> sessionList;
    private SubModel subModel;
    private SessionModel model;

    public CancelSubscriptionResponse() {
        super();
    }


    public CancelSubscriptionResponse(int refundAmount, Long totalHours, Long remainingHours, Long lockedHours,
            Long hourlyRate, String refundReason, List<Long> subscriptionDetailsIds, Long studentId, Long teacherId,
            Long endedBy, String subscriptionName, List<SessionInfo> sessionList, SubModel subModel,SessionModel model) {
        super();
        super.setRefundAmount(refundAmount);
        this.totalHours = totalHours;
        this.remainingHours = remainingHours;
        this.lockedHours = lockedHours;
        this.hourlyRate = hourlyRate;
        this.refundReason = refundReason;
        this.subscriptionDetailsIds = subscriptionDetailsIds;
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.endedBy = endedBy;
        this.subscriptionName = subscriptionName;
        this.sessionList = sessionList;
        this.subModel = subModel;
        this.model=model;
    }

    public Long getRemainingHours() {
        return remainingHours;
    }

    public void setRemainingHours(Long remainingHours) {
        this.remainingHours = remainingHours;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public Long getLockedHours() {
        return lockedHours;
    }

    public void setLockedHours(Long lockedHours) {
        this.lockedHours = lockedHours;
    }

    public Long getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public List<Long> getSubscriptionDetailsIds() {
        return subscriptionDetailsIds;
    }

    public void setSubscriptionDetailsIds(List<Long> subscriptionDetailsIds) {
        this.subscriptionDetailsIds = subscriptionDetailsIds;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public List<SessionInfo> getSessionList() {
        return sessionList;
    }

    public void setSessionList(List<SessionInfo> sessionList) {
        this.sessionList = sessionList;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    public SubModel getSubModel() {
        return subModel;
    }

    public void setSubModel(SubModel subModel) {
        this.subModel = subModel;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public void fillCancelSubscriptionResponse(RefundRes refundRes) {
        if (refundRes != null) {
            super.setNonPromotionalAmount(refundRes.getNonPromotionalAmount());
            super.setPromotionalAmount(refundRes.getPromotionalAmount());
            super.setRefundAmount(refundRes.getRefundAmount());
            super.setSecurity(refundRes.getSecurity());
            super.setAmountGivenasDiscount(refundRes.getAmountGivenasDiscount());
        }
    }

    @Override
    public String toString() {
        return "CancelSubscriptionResponse{" + "totalHours=" + totalHours + ", remainingHours=" + remainingHours + ", lockedHours=" + lockedHours + ", hourlyRate=" + hourlyRate + ", refundReason=" + refundReason + ", subscriptionDetailsIds=" + subscriptionDetailsIds + ", studentId=" + studentId + ", teacherId=" + teacherId + ", endedBy=" + endedBy + ", subscriptionName=" + subscriptionName + ", sessionList=" + sessionList + ", subModel=" + subModel + ", model=" + model + '}';
    }
}
