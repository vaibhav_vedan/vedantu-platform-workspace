/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.pojo.CoursePlanAdminData;
import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import com.vedantu.util.enums.ScheduleType;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CoursePlanBasicInfo extends AbstractMongoStringIdEntityBean {

    private String title;
    private Long teacherId;
    private Long studentId;
    private UserBasicInfo teacher;
    private UserBasicInfo student;
    private Integer totalHours;//millis
    private Integer consumedHours;//millis
    private Integer lockedHours;//millis
    private Integer remainingHours;//millis
    private Integer totalCourseHours;
    private List<CoursePlanCompactSchedule> compactSchedule;//only for display, no business logic
    private Integer noOfWeeks;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private CoursePlanEnums.CoursePlanState state = CoursePlanEnums.CoursePlanState.PUBLISHED;

    private String description;
    private String parentCourseId;
    private Boolean registeredForParentCourse;
    //money related
    private Integer price;//includes registration fee, user has to pay price-registration to get ENROLLED
    private ScheduleType scheduleType = ScheduleType.FS;
    private Integer teacherPayoutRate;
    private Integer trialRegistrationFee;
    private Long startDate;//date of the first session trail/regular
    private Long regularSessionStartDate;
    private String armFormId;
    private List<CoursePlanAdminData> adminData;
    private Long sessionEndDate;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserBasicInfo getTeacher() {
        return teacher;
    }

    public void setTeacher(UserBasicInfo teacher) {
        this.teacher = teacher;
    }

    public UserBasicInfo getStudent() {
        return student;
    }

    public void setStudent(UserBasicInfo student) {
        this.student = student;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Integer getTotalCourseHours() {
        return totalCourseHours;
    }

    public void setTotalCourseHours(Integer totalCourseHours) {
        this.totalCourseHours = totalCourseHours;
    }

    public List<CoursePlanCompactSchedule> getCompactSchedule() {
        return compactSchedule;
    }

    public void setCompactSchedule(List<CoursePlanCompactSchedule> compactSchedule) {
        this.compactSchedule = compactSchedule;
    }

    public Integer getNoOfWeeks() {
        return noOfWeeks;
    }

    public void setNoOfWeeks(Integer noOfWeeks) {
        this.noOfWeeks = noOfWeeks;
    }

    public Integer getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }

    public Integer getConsumedHours() {
        return consumedHours;
    }

    public void setConsumedHours(Integer consumedHours) {
        this.consumedHours = consumedHours;
    }

    public Integer getLockedHours() {
        return lockedHours;
    }

    public void setLockedHours(Integer lockedHours) {
        this.lockedHours = lockedHours;
    }

    public Integer getRemainingHours() {
        return remainingHours;
    }

    public void setRemainingHours(Integer remainingHours) {
        this.remainingHours = remainingHours;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public CoursePlanEnums.CoursePlanState getState() {
        return state;
    }

    public void setState(CoursePlanEnums.CoursePlanState state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentCourseId() {
        return parentCourseId;
    }

    public void setParentCourseId(String parentCourseId) {
        this.parentCourseId = parentCourseId;
    }

    public Boolean getRegisteredForParentCourse() {
        return registeredForParentCourse;
    }

    public void setRegisteredForParentCourse(Boolean registeredForParentCourse) {
        this.registeredForParentCourse = registeredForParentCourse;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public Integer getTeacherPayoutRate() {
        return teacherPayoutRate;
    }

    public void setTeacherPayoutRate(Integer teacherPayoutRate) {
        this.teacherPayoutRate = teacherPayoutRate;
    }

    public Integer getTrialRegistrationFee() {
        return trialRegistrationFee;
    }

    public void setTrialRegistrationFee(Integer trialRegistrationFee) {
        this.trialRegistrationFee = trialRegistrationFee;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getRegularSessionStartDate() {
        return regularSessionStartDate;
    }

    public void setRegularSessionStartDate(Long regularSessionStartDate) {
        this.regularSessionStartDate = regularSessionStartDate;
    }
    
    public List<CoursePlanAdminData> getAdminData() {
        return adminData;
    }

    public void setAdminData(List<CoursePlanAdminData> adminData) {
        this.adminData = adminData;
    }    

	public String getArmFormId() {
		return armFormId;
	}

	public void setArmFormId(String armFormId) {
		this.armFormId = armFormId;
	}

    public Long getSessionEndDate() {
        return sessionEndDate;
    }

    public void setSessionEndDate(Long sessionEndDate) {
        this.sessionEndDate = sessionEndDate;
    }
        
        

}
