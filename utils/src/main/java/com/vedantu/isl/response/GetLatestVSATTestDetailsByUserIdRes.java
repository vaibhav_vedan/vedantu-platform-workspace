package com.vedantu.isl.response;

import com.vedantu.isl.pojo.AnalyticsInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetLatestVSATTestDetailsByUserIdRes {

    private Integer rank;
    private AnalyticsInfo info;
    private Integer topScorerValue = 0;
    private Long attemptedOn;
}
