package com.vedantu.isl.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestAttemptAnalytics {

    private String testId;
    private String testAttemptId;
    private Long studentId;
    private Double percentile;
    private Integer rank;
    private AnalyticsInfo info;
    private String name;
    private Long testStartTime;
    private Boolean groupAnalyticsComputed;
    private String eventCategory;
    private String key;
}
