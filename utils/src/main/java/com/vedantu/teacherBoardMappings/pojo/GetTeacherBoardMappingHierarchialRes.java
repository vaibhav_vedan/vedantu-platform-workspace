package com.vedantu.teacherBoardMappings.pojo;

import java.util.Set;

import com.vedantu.board.pojo.BoardTreeInfo;

public class GetTeacherBoardMappingHierarchialRes {
	
		public Set<String> grades;
		public Set<String> categories;
		public Set<BoardTreeInfo> boards;
		
		public GetTeacherBoardMappingHierarchialRes() {
			super();
		}

		public GetTeacherBoardMappingHierarchialRes(Set<String> grades,
				Set<String> categories, Set<BoardTreeInfo> boards) {
			super();
			this.grades = grades;
			this.categories = categories;
			this.boards = boards;
		}

		public Set<String> getGrades() {
			return grades;
		}

		public void setGrades(Set<String> grades) {
			this.grades = grades;
		}

		public Set<String> getCategories() {
			return categories;
		}

		public void setCategories(Set<String> categories) {
			this.categories = categories;
		}

		public Set<BoardTreeInfo> getBoards() {
			return boards;
		}

		public void setBoards(Set<BoardTreeInfo> boards) {
			this.boards = boards;
		}

		@Override
		public String toString() {
			return "GetTeacherBoardMappingHierarchialRes [grades=" + grades
					+ ", categories=" + categories + ", boards=" + boards + "]";
		}
		

}
