package com.vedantu.lms.cmds.pojo;

public enum QuestionAttemptState {
	EVALUATED, NOT_ATTEMPTED, NOT_EVALUATED
}
