package com.vedantu.lms.cmds.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.vedantu.aws.pojo.UploadFileInfo;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import inti.ws.spring.exception.client.BadRequestException;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GetCMDSUploadFileUrlReq extends AbstractFrontEndReq {
	private String userId;
	private List<UploadFileInfo> fileInfos;
	private UploadFileSourceType uploadFileSourceType;
	private String uploadFileSourceId;

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(userId)) {
			errors.add("userId is absent");
		}
		if (Objects.isNull(uploadFileSourceType)) {
			errors.add("uploadFileSourceType is absent");
		}
		if (StringUtils.isEmpty(uploadFileSourceId)) {
			errors.add("uploadFileSourceId is absent");
		}
		return errors;
	}
}
