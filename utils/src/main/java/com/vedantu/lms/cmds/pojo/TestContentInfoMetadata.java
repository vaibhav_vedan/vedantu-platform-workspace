package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class TestContentInfoMetadata extends AbstractMongoStringIdEntity {
    String testId;
    Long duration;
    Float totalMarks;
    Integer noOfQuestions;
    
    private Integer rank;
    private Float percentage;
    private Float percentile;

    public Long minStartTime;
    public Long maxStartTime;
    boolean hardStop=false;
    public boolean reattemptAllowed = false;
    public TestResultVisibility displayResultOnEnd = TestResultVisibility.VISIBLE;
    public String displayMessageOnEnd;
    private SignUpRestrictionLevel signupHook;

    private Long expiryDate;// Only for subjective public test
    private Long expiryDays;// Only for subjective private test

    public TestContentInfoMetadata() {
        super();
        setId(ObjectId.get().toString());
    }
}
