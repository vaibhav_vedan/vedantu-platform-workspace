package com.vedantu.lms.cmds.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class TestProgressCache {
    private String lastSectionId;
    private String lastQuestionId;
    private Map<String, String> otherSectionsProgress;
    private Integer version;
}
