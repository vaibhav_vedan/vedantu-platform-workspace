package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class StudentBatchProgressReq extends AbstractFrontEndListReq{
	
	private Long userId;
	private Long boardId;
	private String subject;
	private CurriculumEntityName contextType; //OTF_BATCH,COURSE_PLAN
	private String contextId;
	private String contentType;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public CurriculumEntityName getContextType() {
		return contextType;
	}

	public void setContextType(CurriculumEntityName contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (contextType == null) {
            errors.add("contextType");
        }
        if (StringUtils.isEmpty(contextId)) {
            errors.add("contextId");
        }
        if (boardId == null) {
            errors.add("boardId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        
        return errors;
	}

}
