/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;

/**
 *
 * @author parashar
 */
public class QuestionChangeRecord {
    
    private String prevQuestionId;
    private String newQuestionId;
    private String changedBy;
    private Long changedAt;
    private ChangeQuestionActionType changeQuestionActionType = ChangeQuestionActionType.NO_ACTION;


    private float positiveMarks;
    private float negativeMarks;

    public float getprevQuestionPositiveMarks() {
        return prevQuestionPositiveMarks;
    }

    public void setprevQuestionPositiveMarks(float prevQuestionPositiveMarks) {
        this.prevQuestionPositiveMarks = prevQuestionPositiveMarks;
    }

    private float prevQuestionPositiveMarks;

    public float getPositiveMarks() {
        return positiveMarks;
    }

    public void setPositiveMarks(float positiveMarks) {
        this.positiveMarks = positiveMarks;
    }

    public float getNegativeMarks() {
        return negativeMarks;
    }

    public void setNegativeMarks(float negativeMarks) {
        this.negativeMarks = negativeMarks;
    }

    /**
     * @return the prevQuestionId
     */
    public String getPrevQuestionId() {
        return prevQuestionId;
    }

    /**
     * @param prevQuestionId the prevQuestionId to set
     */
    public void setPrevQuestionId(String prevQuestionId) {
        this.prevQuestionId = prevQuestionId;
    }

    /**
     * @return the newQuestionId
     */
    public String getNewQuestionId() {
        return newQuestionId;
    }

    /**
     * @param newQuestionId the newQuestionId to set
     */
    public void setNewQuestionId(String newQuestionId) {
        this.newQuestionId = newQuestionId;
    }

    /**
     * @return the changedBy
     */
    public String getChangedBy() {
        return changedBy;
    }

    /**
     * @param changedBy the changedBy to set
     */
    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    /**
     * @return the changedAt
     */
    public Long getChangedAt() {
        return changedAt;
    }

    /**
     * @param changedAt the changedAt to set
     */
    public void setChangedAt(Long changedAt) {
        this.changedAt = changedAt;
    }

    /**
     * @return the changeQuestionActionType
     */
    public ChangeQuestionActionType getChangeQuestionActionType() {
        return changeQuestionActionType;
    }

    /**
     * @param changeQuestionActionType the changeQuestionActionType to set
     */
    public void setChangeQuestionActionType(ChangeQuestionActionType changeQuestionActionType) {
        this.changeQuestionActionType = changeQuestionActionType;
    }
    
    
}
