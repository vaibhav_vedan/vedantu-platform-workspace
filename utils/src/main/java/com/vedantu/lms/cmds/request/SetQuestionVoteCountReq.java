/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class SetQuestionVoteCountReq extends AbstractFrontEndReq {

    private String questionId;
    private Long count;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (count == null) {
            errors.add("count");
        }
        if (StringUtils.isEmpty(questionId)) {
            errors.add("questionId");
        }
        return errors;
    }

}
