package com.vedantu.lms.cmds.enums;

import org.springframework.util.StringUtils;

public class EnumBasket {
	public static enum Correctness {
		UNKNOWN, CORRECT, INCORRECT, UNJUDGEABLE;

		public static Correctness valueOfKey(String val) {
			Correctness correctness = Correctness.UNKNOWN;
			try {
				correctness = valueOf(defaultIfEmpty(val, UNKNOWN.name().toUpperCase()));
			} catch (Exception e) {
			}

			return correctness;
		}
	}

	public static enum Judgement {
		UNKNOWN, JUDGE, DONT_JUDGE, DONT_CARE;

		public static Judgement valueOfKey(String val) {
			Judgement judgement = Judgement.UNKNOWN;
			judgement = valueOf(defaultIfEmpty(val, UNKNOWN.name()).toUpperCase());
			// if(judgement==UNKNOWN){
			// throw new Exception("judgement type not recognised!");
			// }

			return judgement;
		}
	}

	public static enum Status {
		COMPLETE, SKIP, REVIEW, UNKNOWN;

		public static Status valueOfKey(String val) {
			Status status = Status.COMPLETE;
			status = valueOf(defaultIfEmpty(val, COMPLETE.name()).toUpperCase());
			return status;
		}
	}

	public static enum TestType {
		UNKNOWN, TEST, ASSIGNMENT, QUESTION_LIST, CHALLENGE, ONLINE, OFFLINE;

		public static TestType valueOfKey(String type) {
			TestType testType = UNKNOWN;

			testType = valueOf(defaultIfEmpty(type, TEST.name()).toUpperCase());
			if (testType == UNKNOWN) {
				// throw new Exception("Type is not recognised");
			}

			return testType;
		}
	}
        
        public static enum TestTagType {
		NORMAL, UNIT, PHASE, FULL, ADVANCE, CHAPTER, OTHER;

		public static TestTagType valueOfKey(String type) {
			TestTagType testType = NORMAL;
                        testType = valueOf(defaultIfEmpty(type, NORMAL.name()).toUpperCase());
                        return testType;
		}
	}

	public static enum MarksType {
		AVERAGE, LOWEST, HIGHEST;

		public static MarksType valueOfKey(String type) {
			MarksType marksType = AVERAGE;
			try {
				marksType = valueOf(defaultIfEmpty(type, AVERAGE.name()));
			} catch (Exception e) {

			}
			return marksType;
		}
	}

	public static String defaultIfEmpty(String val, String defaultVal) {
		return StringUtils.isEmpty(val) ? defaultVal : val;
	}
}
