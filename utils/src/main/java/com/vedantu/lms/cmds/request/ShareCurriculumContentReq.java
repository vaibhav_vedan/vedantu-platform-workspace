package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShareCurriculumContentReq extends AbstractFrontEndReq{

	public Node node;//id and batchId also needs to be present - node contents should have the field shared in contents List
	public List<Long> studentIds;
	public String courseName;
	private CurriculumEntityName contextType;
	private String contextId;
	private List<String> sessionIds;
	private String currentSessionId;
	private List<ContentInfo> contents;
	private String nodeId;
	private Long teacherId;
	private String teacherName;
	private boolean newContent;
	private int iterationNo=0;

	public ShareCurriculumContentReq(ShareCurriculumContentReq other) {
		super(other.getCallingUserId(),other.getCallingUserRole());
		this.node = other.node;
		this.studentIds = other.studentIds;
		this.courseName = other.courseName;
		this.contextType = other.contextType;
		this.contextId = other.contextId;
		this.sessionIds = other.sessionIds;
		this.currentSessionId = other.currentSessionId;
		this.contents = other.contents;
		this.nodeId = other.nodeId;
		this.teacherId = other.teacherId;
		this.teacherName = other.teacherName;
		this.newContent = other.newContent;
		this.iterationNo = other.iterationNo;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(nodeId)) {
            errors.add("nodeId");
        }
        if(ArrayUtils.isEmpty(contents)){
        	errors.add("contents");
        }
        if(StringUtils.isEmpty(contextId)){
        	errors.add("contextId");
        }
        if(contextType == null){
        	errors.add("contextType");
        }        
        return errors;
	}
}
