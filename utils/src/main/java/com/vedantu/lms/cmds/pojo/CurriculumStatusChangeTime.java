package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.CurriculumStatus;

public class CurriculumStatusChangeTime {

	private Long changeTime;
	private Long changedBy;
	private CurriculumStatus previousStatus;
	private CurriculumStatus newStatus;

	public Long getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Long changeTime) {
		this.changeTime = changeTime;
	}

	public Long getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(Long changedBy) {
		this.changedBy = changedBy;
	}

	public CurriculumStatus getPreviousStatus() {
		return previousStatus;
	}

	public void setPreviousStatus(CurriculumStatus previousStatus) {
		this.previousStatus = previousStatus;
	}

	public CurriculumStatus getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(CurriculumStatus newStatus) {
		this.newStatus = newStatus;
	}
}
