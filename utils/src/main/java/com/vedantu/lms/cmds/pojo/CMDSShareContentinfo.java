package com.vedantu.lms.cmds.pojo;

import com.vedantu.User.UserBasicInfo;

public class CMDSShareContentinfo {
	private UserBasicInfo teacherInfo;
	private String contentLink;
	private String testId;

	public CMDSShareContentinfo() {
		super();
	}

        public CMDSShareContentinfo(UserBasicInfo teacherInfo, String contentLink) {
            this.teacherInfo = teacherInfo;
            this.contentLink = contentLink;
        }
        
        public CMDSShareContentinfo(UserBasicInfo teacherInfo, String contentLink,String testId) {
            this.teacherInfo = teacherInfo;
            this.contentLink = contentLink;
            this.testId = testId;
        }
        
	public UserBasicInfo getTeacherInfo() {
		return teacherInfo;
	}

	public void setTeacherInfo(UserBasicInfo teacherInfo) {
		this.teacherInfo = teacherInfo;
	}

	public String getContentLink() {
		return contentLink;
	}

	public void setContentLink(String contentLink) {
		this.contentLink = contentLink;
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	@Override
	public String toString() {
		return "CMDSShareContentinfo [teacherInfo=" + teacherInfo + ", contentLink=" + contentLink + ", toString()="
				+ super.toString() + "]";
	}
}
