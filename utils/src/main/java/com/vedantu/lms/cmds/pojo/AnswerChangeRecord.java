/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.pojo;

import java.util.List;

/**
 *
 * @author parashar
 */
public class AnswerChangeRecord {
    private String changedBy;
    private Long changedTime;
    private List<String> previousAnswers;
    private List<String> newAnswers;

    /**
     * @return the changedBy
     */
    public String getChangedBy() {
        return changedBy;
    }

    /**
     * @param changedBy the changedBy to set
     */
    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    /**
     * @return the changedTime
     */
    public Long getChangedTime() {
        return changedTime;
    }

    /**
     * @param changedTime the changedTime to set
     */
    public void setChangedTime(Long changedTime) {
        this.changedTime = changedTime;
    }

    /**
     * @return the previousAnswers
     */
    public List<String> getPreviousAnswers() {
        return previousAnswers;
    }

    /**
     * @param previousAnswers the previousAnswers to set
     */
    public void setPreviousAnswers(List<String> previousAnswers) {
        this.previousAnswers = previousAnswers;
    }

    /**
     * @return the newAnswers
     */
    public List<String> getNewAnswers() {
        return newAnswers;
    }

    /**
     * @param newAnswers the newAnswers to set
     */
    public void setNewAnswers(List<String> newAnswers) {
        this.newAnswers = newAnswers;
    }

    @Override
    public String toString() {
        return "AnswerChangeRecord{" + "changedBy=" + changedBy + ", changedTime=" + changedTime + ", previousAnswers=" + previousAnswers + ", newAnswers=" + newAnswers + '}';
    }
    
}
