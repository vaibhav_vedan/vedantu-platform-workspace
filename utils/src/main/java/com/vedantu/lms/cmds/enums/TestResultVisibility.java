package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 01/05/17.
 */
public enum TestResultVisibility {
    VISIBLE, HIDDEN;

    public static TestResultVisibility valueOfKey(String key) {

        TestResultVisibility resultVisibility = VISIBLE;
        try {
            resultVisibility = valueOf(key.trim().toUpperCase());
        } catch (Throwable e) {}
        return resultVisibility;
    }
}
