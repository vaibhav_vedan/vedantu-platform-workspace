package com.vedantu.lms.cmds.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TestFeedback {
    private List<String> incompleteAttemptFeedbackMessages;
    private String difficulty;
    private String relevancy;
    private String comments;
}
