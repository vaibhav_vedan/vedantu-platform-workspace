package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.ArrayList;
import java.util.Map;

@Data
public class GetCurriculumReq extends AbstractFrontEndReq {

    private String entityId;
    private CurriculumEntityName entityName;
    private List<String> entityIds;
    private String sessionId;
    private Long boardId;
    private SessionType sessionType;
    Map<String,Long> batchInactiveDate;


    public void addEntityId(String entityId){
        if(StringUtils.isEmpty(entityId)){
            return;
        }
            
        if(this.entityIds == null){
            this.entityIds = new ArrayList<>();
        }
        this.entityIds.add(entityId);
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(sessionId)) {
            if (entityName == null) {
                errors.add("entityName");
            }
            if (ArrayUtils.isEmpty(entityIds)) {
                errors.add("entityIds");
            }
        } else {
            if (sessionType == null) {
                errors.add("sessionType");
            }
        }
        return errors;
    }

}
