package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class JoinClanForEventReq extends AbstractFrontEndReq{

	private String clanId;
	private String shareRefCode;
	private Long userId;
	private String shareLink;
	private String event;
	private String category;

	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public String getShareRefCode() {
		return shareRefCode;
	}

	public void setShareRefCode(String shareRefCode) {
		this.shareRefCode = shareRefCode;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getShareLink() {
		return shareLink;
	}

	public void setShareLink(String shareLink) {
		this.shareLink = shareLink;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(shareRefCode)) {
            errors.add("shareRefCode");
        }
        if (userId == null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(category)) {
            errors.add("category");
        }
        return errors;
	}
}
