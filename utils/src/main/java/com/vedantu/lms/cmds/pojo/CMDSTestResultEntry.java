package com.vedantu.lms.cmds.pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;

public class CMDSTestResultEntry {
	private String questionId;
	private int questionIndex;
	private QuestionAttemptState questionAttemptState;
	private Float maxMarks;
	private Float marksGiven;
	private Long evaluatedTime;
	private String questionRemarks;

	
	public CMDSTestResultEntry() {
		super();
	}

	public CMDSTestResultEntry(CMDSTestQuestion cmdsTestQuestion) {
		this(cmdsTestQuestion.getQuestionId(), cmdsTestQuestion.getQuestionIndex(), QuestionAttemptState.NOT_EVALUATED,
				cmdsTestQuestion.getMarks().getPositive(), null, null, null);
	}

	public CMDSTestResultEntry(String questionId, int questionIndex, QuestionAttemptState questionAttemptState,
			Float maxMarks, Float marksGiven, Long evaluatedTime, String questionRemarks) {
		super();
		this.questionId = questionId;
		this.questionIndex = questionIndex;
		this.questionAttemptState = questionAttemptState;
		this.maxMarks = maxMarks;
		this.marksGiven = marksGiven;
		this.evaluatedTime = evaluatedTime;
		this.questionRemarks = questionRemarks;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public int getQuestionIndex() {
		return questionIndex;
	}

	public void setQuestionIndex(int questionIndex) {
		this.questionIndex = questionIndex;
	}

	public QuestionAttemptState getQuestionAttemptState() {
		return questionAttemptState;
	}

	public void setQuestionAttemptState(QuestionAttemptState questionAttemptState) {
		this.questionAttemptState = questionAttemptState;
	}

	public Float getMaxMarks() {
		return maxMarks;
	}

	public void setMaxMarks(Float maxMarks) {
		this.maxMarks = maxMarks;
	}

	public Float getMarksGiven() {
		return marksGiven;
	}

	public void setMarksGiven(Float marksGiven) {
		this.marksGiven = marksGiven;
	}

	public Long getEvaluatedTime() {
		return evaluatedTime;
	}

	public void setEvaluatedTime(Long evaluatedTime) {
		this.evaluatedTime = evaluatedTime;
	}

	public String getQuestionRemarks() {
		return questionRemarks;
	}

	public void setQuestionRemarks(String questionRemarks) {
		this.questionRemarks = questionRemarks;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (StringUtils.isEmpty(this.questionId)) {
			errors.add("questionId");
		}
		return errors;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Missing parameters : " + Arrays.toString(errors.toArray()) + " Entry : " + this.toString());
		}
	}

	@Override
	public String toString() {
		return "CMDSTestResultEntry [questionId=" + questionId + ", questionIndex=" + questionIndex
				+ ", questionAttemptState=" + questionAttemptState + ", maxMarks=" + maxMarks + ", marksGiven="
				+ marksGiven + ", evaluatedTime=" + evaluatedTime + ", questionRemarks=" + questionRemarks
				+ ", toString()=" + super.toString() + "]";
	}
}
