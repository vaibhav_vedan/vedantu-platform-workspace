package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 04/04/17.
 */
public enum UserType
{
    REGISTERED, NOT_REGISTERED
}
