package com.vedantu.lms.cmds.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by somil on 24/05/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestAndAttemptDetails {
    private Integer noOfQuestions;
    private Boolean reattemptAllowed;
    private Long duration;
    private List<TestAttemptInfo> attemptDetails;

}
