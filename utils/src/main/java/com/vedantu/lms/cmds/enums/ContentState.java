package com.vedantu.lms.cmds.enums;

public enum ContentState {
	DRAFT, SHARED, PRIVATE, ATTEMPTED, EVALUATED, EXPIRED, PUBLIC, NOT_UPLOADED
}
