package com.vedantu.lms.cmds.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CMDSStartTestAttemptReq extends AbstractFrontEndReq {
	private String contentInfoId;
	private String testId;
	private UserBasicInfo studentInfo;
	private String notRegisteredUserId;
	private EntityType contextType;
	private String contextId;

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(this.contentInfoId) && StringUtils.isEmpty(this.testId)) {
			errors.add("contentInfoId and testId both empty");
		}
		return errors;
	}
}
