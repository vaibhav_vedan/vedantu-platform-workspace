package com.vedantu.lms.cmds.pojo;

public class SessionCommentPojo {
	
		private String sessionId;
		private String comment;
		
		public String getSessionId() {
			return sessionId;
		}
		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}

	

}
