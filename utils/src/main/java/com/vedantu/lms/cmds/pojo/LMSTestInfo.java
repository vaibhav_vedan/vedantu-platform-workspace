package com.vedantu.lms.cmds.pojo;

import lombok.Data;

@Data
public class LMSTestInfo {

    private Long userId;
    private String testAttemptId;
    private Long testStartTime;
    private Long testDuration;
    private Long attemptDuration;
    private Long creationTime;

}
