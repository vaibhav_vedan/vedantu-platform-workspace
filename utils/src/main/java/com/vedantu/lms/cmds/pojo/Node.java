package com.vedantu.lms.cmds.pojo;

import java.util.List;
import java.util.Set;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Node extends AbstractMongoStringIdEntityBean{
	
	private List<Node> nodes;
	private String parentId;
	private Long startTime;
	private Long endTime;
	private String title;
	private String note;
	private CurriculumStatus status;/* PENDING[DEFAULT], INPROGRESS, DONE */
	private SessionType sessionType; /* OTO, OTF */
	private List<ContentInfo> contents; /* Content need to have sharing state once shared */
	private Boolean edited; /* True/ False , req pojo POST only*/
	private CurriculumEntityName contextType;
	private String contextId;
	private List<SessionCommentCompletePojo> sessionInfos;
	private Set<String> sessionIds;
	private Long boardId;
	private Integer childOrder;
	private Float expectedHours;
	private String courseNodeId;
	private Set<String> topicTags;

	@Override
	public String toString(){
		return "Node{" +
				"parentId='" + parentId + '\'' +
				", title='" + title + '\'' +
				", boardId=" + boardId +
				", childOrder=" + childOrder +
				'}';
	}
}
