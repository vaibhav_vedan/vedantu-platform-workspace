package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.notification.pojos.MoodleContentInfo;

public class ShareContentAndSendMailRes {

	private Node node;
	private List<String> contentInfoIds;
	private List<MoodleContentInfo> moodleContentInfos;

	public List<String> getContentInfoIds() {
		return contentInfoIds;
	}

	public void setContentInfoIds(List<String> contentInfoIds) {
		this.contentInfoIds = contentInfoIds;
	}

	public List<MoodleContentInfo> getMoodleContentInfos() {
		return moodleContentInfos;
	}

	public void setMoodleContentInfos(List<MoodleContentInfo> moodleContentInfos) {
		this.moodleContentInfos = moodleContentInfos;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}
}
