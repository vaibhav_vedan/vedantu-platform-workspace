package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ChangeCurriculumStatusReq extends AbstractFrontEndReq{
	private String nodeId;
	private CurriculumStatus newStatus;
	private List<String> sessionIds;
	private Node node;
	private Long teacherId;
	private Long startTime;
	private CurriculumEntityName contextType;
	private String contextId;
	
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public CurriculumStatus getNewStatus() {
		return newStatus;
	}
	public void setNewStatus(CurriculumStatus newStatus) {
		this.newStatus = newStatus;
	}
	public List<String> getSessionIds() {
		return sessionIds;
	}
	public void setSessionIds(List<String> sessionIds) {
		this.sessionIds = sessionIds;
	}
	public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public CurriculumEntityName getContextType() {
		return contextType;
	}
	public void setContextType(CurriculumEntityName contextType) {
		this.contextType = contextType;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(nodeId)) {
            errors.add("nodeId");
        }
        if (newStatus == null) {
            errors.add("newStatus");
        }
        return errors;
	}
}
