package com.vedantu.lms.cmds.pojo;

public class StudentTestBasicInfo {

	private String title;
	private Long sharedTime;
	private Float score;
	private Float totalScore;
	private Float highestScore;
	private String testId;
	private Long lastAttemptedOn;
	private String testType;
	private String testState;
	private String studentActionLink;
	private String contentInfoId;
	private String teacherId;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getSharedTime() {
		return sharedTime;
	}
	public void setSharedTime(Long sharedTime) {
		this.sharedTime = sharedTime;
	}
	public Float getScore() {
		return score;
	}
	public void setScore(Float score) {
		this.score = score;
	}
	public Float getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Float totalScore) {
		this.totalScore = totalScore;
	}
	public Float getHighestScore() {
		return highestScore;
	}
	public void setHighestScore(Float highestScore) {
		this.highestScore = highestScore;
	}
	public String getTestId() {
		return testId;
	}
	public void setTestId(String testId) {
		this.testId = testId;
	}
	public Long getLastAttemptedOn() {
		return lastAttemptedOn;
	}
	public void setLastAttemptedOn(Long lastAttemptedOn) {
		this.lastAttemptedOn = lastAttemptedOn;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	public String getTestState() {
		return testState;
	}
	public void setTestState(String testState) {
		this.testState = testState;
	}
	public String getStudentActionLink() {
		return studentActionLink;
	}
	public void setStudentActionLink(String studentActionLink) {
		this.studentActionLink = studentActionLink;
	}
	public String getContentInfoId() {
		return contentInfoId;
	}
	public void setContentInfoId(String contentInfoId) {
		this.contentInfoId = contentInfoId;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
}
