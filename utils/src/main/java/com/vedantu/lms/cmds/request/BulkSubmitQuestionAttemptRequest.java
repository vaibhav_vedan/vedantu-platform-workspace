package com.vedantu.lms.cmds.request;

import com.vedantu.exception.BadRequestException;
import com.vedantu.lms.cmds.enums.AttemptStateOfQuestion;
import com.vedantu.lms.cmds.pojo.QuestionAttemptDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by somil on 23/03/17.
 */
public class BulkSubmitQuestionAttemptRequest extends AbstractFrontEndReq {
    private String testAttemptId;
    //private String userId;

    private Map<String, QuestionAttemptDetails> questionAttemptsMap;



    public String getTestAttemptId() {
        return testAttemptId;
    }

    public void setTestAttemptId(String testAttemptId) {
        this.testAttemptId = testAttemptId;
    }


//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }

    public Map<String, QuestionAttemptDetails> getQuestionAttemptsMap() {
        return questionAttemptsMap;
    }

    public void setQuestionAttemptsMap(Map<String, QuestionAttemptDetails> questionAttemptsMap) {
        this.questionAttemptsMap = questionAttemptsMap;
    }

    @Override
    public  List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (Objects.isNull(questionAttemptsMap)) {
            errors.add("QuestionAttemptMap can't be null");
        } else {
            for(String questionId : questionAttemptsMap.keySet()) {
                QuestionAttemptDetails questionAttemptDetails = questionAttemptsMap.get(questionId);
                List<String> attemptDetails = questionAttemptDetails.getAttempt();
                if (ArrayUtils.isEmpty(attemptDetails)) {
                    if(Objects.nonNull(questionAttemptDetails.getAttemptStateOfQuestion())) {
                        if (!questionAttemptDetails.getAttemptStateOfQuestion().equals(AttemptStateOfQuestion.CLEAR_RESPONSE) &&
                                !questionAttemptDetails.getAttemptStateOfQuestion().equals(AttemptStateOfQuestion.TO_BE_REVIEWED)) {
                            errors.add("Attempt for questionId is null " + questionId);
                        }
                    }
                }else {
                    for (String attempt : attemptDetails){
                        if(StringUtils.isEmpty(attempt)){
                            errors.add("Attempt contains null for questionId "+questionId);
                        }
                    }
                }
            // commenting out below lines as attempt state can be empty in old tests
            //  if(Objects.isNull(questionAttemptsMap.get(questionId).getAttemptStateOfQuestion())){
            //        errors.add("AttemptStateOfQuestion could not be null "+questionId);
            //  }
            }
        }
        return errors;
    }


    //TODO: Add validation
}

