package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.subscription.enums.BatchCurriculumStatus;

public class ContentStats {

    private BatchCurriculumStatus batchCurriculumStatus;
    private Integer testShared;
    private Integer testAttempted;
    private Integer assignShared;
    private Integer assignAttempted;

    public BatchCurriculumStatus getBatchCurriculumStatus() {
        return batchCurriculumStatus;
    }

    public void setBatchCurriculumStatus(BatchCurriculumStatus batchCurriculumStatus) {
        this.batchCurriculumStatus = batchCurriculumStatus;
    }

    public Integer getTestShared() {
        return testShared;
    }

    public void setTestShared(Integer testShared) {
        this.testShared = testShared;
    }

    public Integer getTestAttempted() {
        return testAttempted;
    }

    public void setTestAttempted(Integer testAttempted) {
        this.testAttempted = testAttempted;
    }

    public Integer getAssignShared() {
        return assignShared;
    }

    public void setAssignShared(Integer assignShared) {
        this.assignShared = assignShared;
    }

    public Integer getAssignAttempted() {
        return assignAttempted;
    }

    public void setAssignAttempted(Integer assignAttempted) {
        this.assignAttempted = assignAttempted;
    }
}
