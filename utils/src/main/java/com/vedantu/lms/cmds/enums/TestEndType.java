package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 22/03/17.
 */
public enum TestEndType {
    MANUAL, AUTOMATIC
}
