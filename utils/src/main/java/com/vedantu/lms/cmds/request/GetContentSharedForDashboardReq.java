package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetContentSharedForDashboardReq extends AbstractFrontEndReq{
	
	private List<String> studentIds;
	private String batchId;
	public List<String> getStudentIds() {
		return studentIds;
	}
	public void setStudentIds(List<String> studentIds) {
		this.studentIds = studentIds;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

}
