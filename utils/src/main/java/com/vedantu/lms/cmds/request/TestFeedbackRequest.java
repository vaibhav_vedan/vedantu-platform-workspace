package com.vedantu.lms.cmds.request;

import com.vedantu.lms.cmds.pojo.TestFeedback;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class TestFeedbackRequest extends AbstractFrontEndReq {
    private String testAttemptId;
    private TestFeedback feedback;

    public String getTestAttemptId() {
        return testAttemptId;
    }

    public void setTestAttemptId(String testAttemptId) {
        this.testAttemptId = testAttemptId;
    }

    public void setFeedback(TestFeedback feedback) {
        this.feedback = feedback;
    }

    public TestFeedback getFeedback() {
        return feedback;
    }
}
