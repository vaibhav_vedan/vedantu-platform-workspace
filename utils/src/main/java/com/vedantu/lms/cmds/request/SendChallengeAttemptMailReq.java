package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.pojo.ChallengeUserPoints;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SendChallengeAttemptMailReq extends AbstractFrontEndReq{
	
	private List<ChallengeUserPoints> users;

	public List<ChallengeUserPoints> getUsers() {
		return users;
	}

	public void setUsers(List<ChallengeUserPoints> users) {
		this.users = users;
	}

}
