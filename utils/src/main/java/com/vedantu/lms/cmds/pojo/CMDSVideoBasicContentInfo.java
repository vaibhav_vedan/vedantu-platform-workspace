package com.vedantu.lms.cmds.pojo;

import lombok.Data;

@Data
public class CMDSVideoBasicContentInfo {

	private String Id;
	private String videoId;
	private String thumbnailUrl;
	private String exerciseName;
}
