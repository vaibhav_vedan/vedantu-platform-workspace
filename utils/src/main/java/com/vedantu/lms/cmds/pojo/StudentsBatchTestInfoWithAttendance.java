package com.vedantu.lms.cmds.pojo;

import java.util.List;

public class StudentsBatchTestInfoWithAttendance {
	private Long userId;
	private Integer attended;
	private Integer totalAttendance;
	private String remark;
	private Long remarkCreatedOn;
	private List<StudentTestBasicInfo> testScores;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getAttended() {
		return attended;
	}
	public void setAttended(Integer attended) {
		this.attended = attended;
	}
	public Integer getTotalAttendance() {
		return totalAttendance;
	}
	public void setTotalAttendance(Integer totalAttendance) {
		this.totalAttendance = totalAttendance;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getRemarkCreatedOn() {
		return remarkCreatedOn;
	}
	public void setRemarkCreatedOn(Long remarkCreatedOn) {
		this.remarkCreatedOn = remarkCreatedOn;
	}
	public List<StudentTestBasicInfo> getTestScores() {
		return testScores;
	}
	public void setTestScores(List<StudentTestBasicInfo> testScores) {
		this.testScores = testScores;
	}

}
