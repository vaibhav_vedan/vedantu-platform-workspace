package com.vedantu.lms.cmds.pojo;

import java.util.List;

public class LMSStatsForSession {

    private String topicDone;
    private String topicCovered;
    private List<String> testShared;
    private List<String> assingmentShared;
    private Integer attemptedTests;
    private Integer evaluatedTests;
    private Integer attemptedAssignment;
    private Integer evaluatedAssignment;

    public String getTopicDone() {
        return topicDone;
    }

    public void setTopicDone(String topicDone) {
        this.topicDone = topicDone;
    }

    public String getTopicCovered() {
        return topicCovered;
    }

    public void setTopicCovered(String topicCovered) {
        this.topicCovered = topicCovered;
    }

    public List<String> getTestShared() {
        return testShared;
    }

    public void setTestShared(List<String> testShared) {
        this.testShared = testShared;
    }

    public List<String> getAssingmentShared() {
        return assingmentShared;
    }

    public void setAssingmentShared(List<String> assingmentShared) {
        this.assingmentShared = assingmentShared;
    }

    public Integer getAttemptedTests() {
        return attemptedTests;
    }

    public void setAttemptedTests(Integer attemptedTests) {
        this.attemptedTests = attemptedTests;
    }

    public Integer getEvaluatedTests() {
        return evaluatedTests;
    }

    public void setEvaluatedTests(Integer evaluatedTests) {
        this.evaluatedTests = evaluatedTests;
    }

    public Integer getAttemptedAssignment() {
        return attemptedAssignment;
    }

    public void setAttemptedAssignment(Integer attemptedAssignment) {
        this.attemptedAssignment = attemptedAssignment;
    }

    public Integer getEvaluatedAssignment() {
        return evaluatedAssignment;
    }

    public void setEvaluatedAssignment(Integer evaluatedAssignment) {
        this.evaluatedAssignment = evaluatedAssignment;
    }
}
