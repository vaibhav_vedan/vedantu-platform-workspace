package com.vedantu.lms.cmds.pojo;

import java.util.Set;

public class TopicTargetTagsPojo {

    private Set<String> mainTags;
    private Set<String> subjects;
    private Set<String> grades;
    private Set<String> targets;

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    public Set<String> getTargets() {
        return targets;
    }

    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }
}
