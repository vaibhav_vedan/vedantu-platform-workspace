package com.vedantu.lms.cmds.request;

import java.util.List;
import java.util.Set;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EditCurriculumReq extends AbstractFrontEndReq{
	
	private String nodeId;
	private String title;
	private String note;
	private Float expectedHours;
	private List<String> batchIds;

	// Things to be added for making a new curriculum
	private String templateTitle;
	private Set<String> grades;
	private Set<String> targets;
	private Integer year;
	private CurriculumEntityName entityName;
	private String entityId;

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(nodeId)) {
            errors.add("nodeId can't be empty");
        }
        return errors;
	}
}
