package com.vedantu.lms.cmds.pojo;

import com.vedantu.session.pojo.OTFSessionPojoUtils;

public class SessionCommentCompletePojo {

	private String sessionId;
	private String comment;
	private OTFSessionPojoUtils sessionPojo;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public OTFSessionPojoUtils getSessionPojo() {
		return sessionPojo;
	}
	public void setSessionPojo(OTFSessionPojoUtils sessionPojo) {
		this.sessionPojo = sessionPojo;
	}
}
