package com.vedantu.lms.cmds.pojo;

public enum AnalyticsTypes {
    QUESTION_TYPE, SECTION, SUBJECT, DIFFICULTY, TOPIC
}
