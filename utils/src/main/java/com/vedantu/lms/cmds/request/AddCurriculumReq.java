package com.vedantu.lms.cmds.request;

import java.util.List;
import java.util.Set;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddCurriculumReq extends AbstractFrontEndReq{
	
	private CurriculumEntityName entityName;
	private String entityId;
	private Node node;
	private List<String> batchIds;
	private List<String> topicTags;

	// For curriculum structure these values should be populated
	private String title;
	private Set<String> grades;
	private Set<String> targets;
	private Integer year;
	private boolean curriculumStructureChanged=false;
	private String curriculumTemplateId;


	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (entityName == null) {
            errors.add("entityName");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (node == null) {
            errors.add("node");
        }
        if(curriculumStructureChanged){
        	if(StringUtils.isEmpty(title) || ArrayUtils.isEmpty(grades) || ArrayUtils.isEmpty(targets) || year==null){
        		errors.add("Either title or grades or targets or year is empty");
			}
		}else{
        	if(StringUtils.isEmpty(curriculumTemplateId)){
        		errors.add("Curriculum template id cann't be null");
			}
		}
        return errors;
	}
}
