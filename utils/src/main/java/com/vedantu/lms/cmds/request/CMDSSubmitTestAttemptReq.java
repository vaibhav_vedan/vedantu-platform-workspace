package com.vedantu.lms.cmds.request;

import java.util.List;
import java.util.Map;
import com.vedantu.lms.cmds.enums.TestEndType;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptImage;
import com.vedantu.lms.cmds.pojo.TestFeedback;
import com.vedantu.lms.cmds.pojo.QuestionAttemptDetails;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CMDSSubmitTestAttemptReq extends AbstractFrontEndReq {
	private String attemptId;
	private TestEndType testEndType;
	private Map<String, QuestionAttemptDetails> questionAttemptsMap;
	private TestFeedback feedback;
	private List<CMDSTestAttemptImage> fileInfos;
	@Deprecated
	private boolean draftState;

	public CMDSSubmitTestAttemptReq(String attemptId, TestEndType testEndType) {
		this.attemptId=attemptId;
		this.testEndType=testEndType;
	}

	@Override
	protected List<String> collectVerificationErrors(){
		List<String> errors=super.collectVerificationErrors();
		if (StringUtils.isEmpty(this.attemptId)) {
			errors.add("attemptId cann't be null");
		}
		return errors;
	}


}
