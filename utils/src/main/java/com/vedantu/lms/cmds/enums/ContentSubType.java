package com.vedantu.lms.cmds.enums;

public enum ContentSubType {
    TATTVA,
    SIMULATOR,
    CHAPTER,
    SESSION;
}