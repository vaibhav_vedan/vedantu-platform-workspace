package com.vedantu.lms.cmds.pojo;

public class ChallengePoints {

	private Long points;
	private Long time;

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}
}
