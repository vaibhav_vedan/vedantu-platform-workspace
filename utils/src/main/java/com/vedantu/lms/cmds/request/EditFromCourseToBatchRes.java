package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.pojo.Node;

public class EditFromCourseToBatchRes {

	private Node node;
	private List<String> batchIds;
	public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public List<String> getBatchIds() {
		return batchIds;
	}
	public void setBatchIds(List<String> batchIds) {
		this.batchIds = batchIds;
	}
}
