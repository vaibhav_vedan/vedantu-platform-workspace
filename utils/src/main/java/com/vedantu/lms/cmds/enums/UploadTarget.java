/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.enums;

import com.vedantu.util.ConfigUtils;

/**
 *
 * @author ajith
 */
public enum UploadTarget {
    QUESTIONSETS {
        @Override
        public String getFolderPath() {
            return "question-sets";
        }
    },
    QUESTIONS {
        @Override
        public String getFolderPath() {
            return "questions";
        }
    },
    PUBLIC_QUESTIONSETS {
        @Override
        public String getFolderPath() {
            return "public/question-sets";
        }
    },
    PUBLIC_QUESTIONS {
        @Override
        public String getFolderPath() {
            return "public/questions";
        }
    },
    ANSWERS {
        @Override
        public String getFolderPath() {
            return "answers";
        }
    },
    COMMENTS {
        @Override
        public String getFolderPath() {
            return "comments";
        }
    },
    SOLUTIONS {
        @Override
        public String getFolderPath() {
            return "solutions";
        }
    },
    PUBLIC_SOLUTIONS {
        @Override
        public String getFolderPath() {
            return "public/solutions";
        }
    },
    PICTURE_CHALLENGES {
        @Override
        public String getFolderPath() {
            return "picture-challenges";
        }
    },
    CLAN_PICTURES {
        @Override
        public String getFolderPath() {
            return "clan-pictures";
        }
    },
    BOOK_THUMBNAILS {
        @Override
        public String getFolderPath() {
            return "public/books/thumbnails";
        }
    },
    PUBLIC_BOOK_APP_NOTIFICATION {
        @Override
        public String getFolderPath() {
            return "public/bookAppNotification";
        }
    },
    DOUBTS_PICS {
        @Override
        public String getFolderPath() {
            return "doubtPics";
        }
    },
    SEO_TOPIC_PAGES {
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

        @Override
        public String getFolderPath() {
            return "public/seo/topicPages";
        }

        @Override
        public String getBucketName() {
            return bucket;
        }
    },
    APPEND_PDF_PAGES {
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

        @Override
        public String getFolderPath() {
            return "append-pdf";
        }

        @Override
        public String getBucketName() {
            return bucket;
        }
    }, WAVEBOOK_THUMBNAILS {
        //TODO MOVE THIS to platform or elsewhere from lms
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.otm.session.bucket");

        @Override
        public String getFolderPath() {
            return "wavebook-thumbnails";
        }

        @Override
        public String getBucketName() {
            return bucket;
        }
    }, WAVEBOOK_IMAGES {
        //TODO MOVE THIS to platform or elsewhere from lms
        //TODO these images are publicly accessible
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.otm.session.bucket");

        @Override
        public String getFolderPath() {
            return "wavebook-images";
        }

        @Override
        public String getBucketName() {
            return bucket;
        }
    }, SESSION_THUMBNAILS {
        //TODO MOVE THIS to platform or elsewhere from lms
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.otm.session.bucket");

        @Override
        public String getFolderPath() {
            return "session-thumbnails";
        }

        @Override
        public String getBucketName() {
            return bucket;
        }
    }, SESSION_IMAGES {
        //TODO MOVE THIS to platform or elsewhere from lms
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.otm.session.bucket");

        @Override
        public String getFolderPath() {
            return "session-images";
        }

        @Override
        public String getBucketName() {
            return bucket;
        }
    },
    EMAIL_NOTIFICATION {
        @Override
        public String getFolderPath() {
            return "public/email-notification";
        }
    };

    public abstract String getFolderPath();

    public String getBucketName() {
        return null;
    }
}
