package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 24/03/17.
 */
public enum AccessLevel
{
    PRIVATE, PUBLIC, SELECTIVE
}
