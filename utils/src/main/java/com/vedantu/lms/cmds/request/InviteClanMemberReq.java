package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class InviteClanMemberReq extends AbstractFrontEndReq {
	private String clanId;
	private String shareRefCode;
	private List<String> emailIds;

	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public String getShareRefCode() {
		return shareRefCode;
	}

	public void setShareRefCode(String shareRefCode) {
		this.shareRefCode = shareRefCode;
	}

	public List<String> getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(List<String> emailIds) {
		this.emailIds = emailIds;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(clanId)) {
            errors.add("clanId");
        }
        return errors;
	}
}
