package com.vedantu.lms.cmds.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CMDSTestAttemptImage {
	private String imageIndex;
	private String keyName;
	private String fileName;
	private String publicUrl;
	private String evaluatedKeyName;
	private String evaluatedFileName;
	private String evaluatedPublicUrl;
	private Long creationTime;
	private Long lastUpdatedTime;
	private String contentType;

	public void updateEvaluateDetails(CMDSTestAttemptImage newEntry) {
		this.evaluatedKeyName = newEntry.getEvaluatedKeyName();
		this.evaluatedFileName = newEntry.getEvaluatedFileName();
		this.contentType=newEntry.getContentType();
	}

}
