package com.vedantu.lms.cmds.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.vedantu.lms.cmds.enums.EnumBasket.Judgement;
import com.vedantu.lms.cmds.enums.EnumBasket.Status;
import com.vedantu.util.CommonUtils;

public enum QuestionType {
    MCQ {
        @Override
        public boolean isJudgeable() {
            return true;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return getIsCorrect(judgement, answerGiven, correctAnswer, status);
        }
    },
    SCQ {
        @Override
        public boolean isJudgeable() {
            return true;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return getIsCorrect(judgement, answerGiven, correctAnswer, status);
        }
    },
    MATCHING {
        @Override
        public boolean isJudgeable() {
            return false;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return false;
        }
    },
    NUMERIC {
        @Override
        public boolean isJudgeable() {
            return true;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return getIsNumericallyCorrect(judgement, answerGiven, correctAnswer, status);
        }

    },
    TEXT {
        @Override
        public boolean isJudgeable() {
            return false;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return false;
        }
    },
    SHORT_TEXT {
        @Override
        public boolean isJudgeable() {
            return true;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return isShortTextCorrectlyAnswered(judgement, answerGiven, correctAnswer, status);
        }
    },
    PARA {
        @Override
        public boolean isJudgeable() {
            return false;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return false;
        }
    },
    UNKNOWN {
        @Override
        public boolean isJudgeable() {
            return false;
        }

        @Override
        public boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
                Status status) {
            return false;
        }
    };

    private static boolean getIsCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
            Status status) {
        if (status != Status.COMPLETE) {
            return false;
        }
        switch (judgement) {
            case JUDGE:
                return isCorrectlyAnswered(correctAnswer, answerGiven);
            case DONT_JUDGE:
            case DONT_CARE:
            default:
                return false;
        }
    }

    private static boolean getIsNumericallyCorrect(Judgement judgement, List<String> answerGiven,
            List<String> correctAnswer, Status status) {
        if (status != Status.COMPLETE) {
            return false;
        }
        switch (judgement) {
            case JUDGE:
                return isNumericCorrectlyAnswered(correctAnswer, answerGiven);
            case DONT_JUDGE:
            case DONT_CARE:
            default:
                return false;
        }
    }

    public static QuestionType valueOfKey(String key) {
        QuestionType qType = PARA;
        try {
            if (StringUtils.equalsIgnoreCase(key.trim().replaceAll("\\s", ""), "MatchMatrix")) {
                qType = MATCHING;
            } else {
                qType = valueOf(key.trim().toUpperCase());
            }
        } catch (Exception e) {
        }
        return qType;
    }

    public abstract boolean isJudgeable();

    public abstract boolean isCorrect(Judgement judgement, List<String> answerGiven, List<String> correctAnswer,
            Status status);

    public static boolean isNumericCorrectlyAnswered(List<String> correctAnswer, List<String> answerGiven) {
        if (CollectionUtils.isEmpty(answerGiven) || CollectionUtils.isEmpty(correctAnswer)) {
            return false;
        }
        for(String answerGivenValue : answerGiven){
            if(StringUtils.isEmpty(answerGivenValue)){
                return false;
            }
        }
        String answered = answerGiven.get(0).trim();
        String correct = correctAnswer.get(0).trim();
        try {
            Double dCorrect = Double.parseDouble(correct);
            Double dAnswered = Double.parseDouble(answered);
            return dCorrect.equals(dAnswered);
        } catch (Exception ex) {
            return false;
        }

        // Donn't remove the following commented code
    /*
       if (StringUtils.equalsIgnoreCase(answered, correct)) {
            //logger.log4j.info("exact answer!");
            return true;
        } else if (answered.length() < correct.length()) {
            return false;
        } else if (answered.length() > correct.length()) {
            int precision
                    = correct.trim().length();
            //logger.log4j.info("precision: "+precision);
            //logger.log4j.info("answer precison: "+answered.substring(0,precision));
            if (StringUtils.equalsIgnoreCase(answered.substring(0,
                    precision), correct)) {
                return true;
            }
        }
        return false;
        try {
            int correctNumChars = correct.length() - correct.indexOf(".");
            int answeredNumChars = answered.length() - answered.indexOf(".");
            int numChars = 0;
            if (correct.contains(".") && answered.contains(".")) {
                numChars = Math.max(answeredNumChars, correctNumChars) - 1;
                if (answeredNumChars == correctNumChars) {
                    numChars = answeredNumChars;
                }
            } else if (!correct.contains(".") && answered.contains(".")) {
                numChars = answeredNumChars - 1;
            } else if (correct.contains(".") && !answered.contains(".")) {
                numChars = correctNumChars - 1;
            } else if (!correct.contains(".") && !answered.contains(".")) {
                numChars = 1;
            }
            Double dCorrect = Double.parseDouble(correct);
            Double dAnswered = Double.parseDouble(answered);
            if (dCorrect.equals(dAnswered)) {
                return true;
            }
            if ((Math.signum(dCorrect) * dCorrect) > (Math.signum(dAnswered) * dAnswered)) {
                return false;
            }
            Double tolerance = new Double(1) / Math.pow(new Double(10), Math.max(numChars, 1));
            if (Math.abs(dAnswered) - Math.abs(dCorrect) - tolerance <= new Double(1) / Math.pow(new Double(10), 9)) {
                return true;
            }
        } catch (Exception e) {
        }
    */
    }

    public static boolean isCorrectlyAnswered(List<String> correctAnswer, List<String> answerGiven) {
        correctAnswer = getSortedIndex(correctAnswer);
        if (CommonUtils.isEqualCollection(correctAnswer, answerGiven)) {
            return true;
        }
        return false;
    }

    public static List<String> getSortedIndex(List<String> answerGiven) {
        List<Integer> indexes = new ArrayList<Integer>();
        if (answerGiven != null) {
            for (String strInd : answerGiven) {
                try {
                    indexes.add(Integer.parseInt(strInd));
                } catch (NumberFormatException e) {
                    Collections.sort(answerGiven);
                    return answerGiven;
                }
            }
        }
        Collections.sort(indexes);
        answerGiven = new ArrayList<String>();
        for (Integer ind : indexes) {
            answerGiven.add(ind.toString());
        }
        return answerGiven;
    }

    public static boolean isEqualMatrix(Map<String, List<String>> correctAnswer,
            Map<String, List<String>> answerGiven) {
        for (Entry<String, List<String>> entry : correctAnswer.entrySet()) {
            List<String> correctAns = entry.getValue();
            List<String> ansGiven = answerGiven.get(entry.getKey());
            boolean equals = CommonUtils.isEqualCollection(correctAns, ansGiven);
            if (!equals) {
                return false;
            }
        }

        return true;
    }
    
    private static boolean isShortTextCorrectlyAnswered(Judgement judgement, List<String> answerGiven,
        List<String> correctAnswer, Status status) {
        if (status != Status.COMPLETE) {
            return false;
        }
        switch (judgement) {
            case JUDGE:
                return correctAnswer.get(0).equalsIgnoreCase(answerGiven.get(0));
            case DONT_JUDGE:
            case DONT_CARE:
            default:
               return false;
        }
    }
}
