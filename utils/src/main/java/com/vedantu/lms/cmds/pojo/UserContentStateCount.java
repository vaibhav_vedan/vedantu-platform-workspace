package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.session.pojo.EntityType;

public class UserContentStateCount {

	private String teacherId;
	private EntityType contextType;
	private String studentId;
	private String subject;
	private ContentState contentState;
	private Integer count;
	
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public EntityType getContextType() {
		return contextType;
	}
	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public ContentState getContentState() {
		return contentState;
	}
	public void setContentState(ContentState contentState) {
		this.contentState = contentState;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
