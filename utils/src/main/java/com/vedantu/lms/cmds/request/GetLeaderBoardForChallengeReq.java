package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetLeaderBoardForChallengeReq extends AbstractFrontEndListReq{

	private Long userId;
	private LeaderBoardType type;
	private boolean addMyRank = false;//
	private String category;
	private Long startTime;
	private String clanId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public LeaderBoardType getType() {
		return type;
	}

	public void setType(LeaderBoardType type) {
		this.type = type;
	}

	public Boolean getAddMyRank() {
		return addMyRank;
	}

	public void setAddMyRank(Boolean addMyRank) {
		this.addMyRank = addMyRank;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	
	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public void setAddMyRank(boolean addMyRank) {
		this.addMyRank = addMyRank;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (type == null) {
            errors.add("LeaderBoardType");
        }
        if (StringUtils.isEmpty(category)) {
            errors.add("category");
        }
        return errors;
	}
}
