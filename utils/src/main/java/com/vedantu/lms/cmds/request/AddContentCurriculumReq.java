package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddContentCurriculumReq extends AbstractFrontEndReq {

    private String nodeId;
    private List<ContentInfo> contents;
    private Boolean addInBatch;
    private List<String> batchIds;

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public List<ContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<ContentInfo> contents) {
        this.contents = contents;
    }

    public Boolean getAddInBatch() {
        return addInBatch;
    }

    public void setAddInBatch(Boolean addInBatch) {
        this.addInBatch = addInBatch;
    }

    public List<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(List<String> batchIds) {
        this.batchIds = batchIds;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(nodeId)) {
            errors.add("nodeId");
        }
        if (ArrayUtils.isEmpty(contents)) {
            errors.add("contents");
        }
        return errors;
    }

}
