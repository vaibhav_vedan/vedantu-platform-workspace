package com.vedantu.lms.cmds.pojo;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.QuestionType;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by somil on 01/05/17.
 */
@Data
@NoArgsConstructor
public class CategoryAnalytics {

    private String name; // Section Name
    private String subject;
    private String topic;
    private Difficulty difficulty;
    private QuestionType questionType;

    private AnalyticsTypes categoryType;
    private boolean customEvaluation = false;

    private Float marks = 0f;
    private Integer attempted = 0;
    private Integer unattempted = 0;
    private Integer correct = 0;
    private Integer incorrect = 0;
    private Float maxMarks = 0f;
    private Integer duration = 0;

    public void incrementValues(QuestionAnalytics questionAnalytics) {
        if (QuestionAttemptState.NOT_ATTEMPTED.equals(questionAnalytics.getQuestionAttemptState())) {
            this.setUnattempted(this.getUnattempted() + 1);
        } else if (QuestionAttemptState.EVALUATED.equals(questionAnalytics.getQuestionAttemptState())) {
            {
                this.setAttempted(this.getAttempted() + 1);
            }
            if (questionAnalytics.getDuration() != null) {
                this.setDuration(this.getDuration() + questionAnalytics.getDuration().intValue());
            }
            if (questionAnalytics.getMarksGiven() != null) {
                this.setMarks(this.getMarks() + questionAnalytics.getMarksGiven());
            }
            if (EnumBasket.Correctness.CORRECT.equals(questionAnalytics.getCorrectness())) {
                this.setCorrect(this.getCorrect() + 1);
            }
            if (EnumBasket.Correctness.INCORRECT.equals(questionAnalytics.getCorrectness())) {
                this.setIncorrect(this.getIncorrect() + 1);
            }

        }
        this.setMaxMarks(this.getMaxMarks() + questionAnalytics.getMaxMarks());
    }

    public CategoryAnalytics(QuestionAnalytics questionAnalytics) {
        incrementValues(questionAnalytics);
    }

    public static CategoryAnalytics accumulateValues(CategoryAnalytics prev, CategoryAnalytics current) {
        prev.marks += current.marks;
        prev.attempted += current.attempted;
        prev.unattempted += current.unattempted;
        prev.correct += current.correct;
        prev.incorrect += current.incorrect;
        prev.maxMarks += current.maxMarks;
        prev.duration += current.duration;
        return prev;
    }

    @Override
    public String toString() {
        return "CategoryAnalytics{" +
                "name='" + name + '\'' +
                ", marksGiven=" + marks +
                ", attempted=" + attempted +
                ", unattempted=" + unattempted +
                ", correct=" + correct +
                ", incorrect=" + incorrect +
                ", maxMarks=" + maxMarks +
                ", duration=" + duration +
                '}';
    }
}
