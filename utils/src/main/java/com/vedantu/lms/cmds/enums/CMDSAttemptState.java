package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 01/05/17.
 */
public enum CMDSAttemptState {
    ATTEMPT_STARTED, ATTEMPT_COMPLETE, EVALUATE_STARTED, EVALUATE_COMPLETE, ATTEMPT_COMPLETE_SOLUTIONS_NOT_UPLOADED;
}
