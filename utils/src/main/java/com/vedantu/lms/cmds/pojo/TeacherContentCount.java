package com.vedantu.lms.cmds.pojo;

import com.vedantu.session.pojo.EntityType;

public class TeacherContentCount {

	private String teacherId;
	private Integer shared=0;//total
	private Integer attempted=0;//attempted+evaluated
	private Integer evaluated=0;//evaluated
	private EntityType contextType;
	
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public Integer getShared() {
		return shared;
	}
	public void setShared(Integer shared) {
		this.shared = shared;
	}
	public Integer getAttempted() {
		return attempted;
	}
	public void setAttempted(Integer attempted) {
		this.attempted = attempted;
	}
	public Integer getEvaluated() {
		return evaluated;
	}
	public void setEvaluated(Integer evaluated) {
		this.evaluated = evaluated;
	}
	public EntityType getContextType() {
		return contextType;
	}
	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

}
