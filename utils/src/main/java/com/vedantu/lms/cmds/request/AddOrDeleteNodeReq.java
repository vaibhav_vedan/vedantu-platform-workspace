package com.vedantu.lms.cmds.request;

import java.util.List;
import java.util.Set;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddOrDeleteNodeReq extends AbstractFrontEndReq{

	private Node node;
	private List<String> batchIds;

	// Things to be added for making a new curriculum
    private String title;
    private Set<String> grades;
    private Set<String> targets;
    private Integer year;
    private CurriculumEntityName entityName;
    private String entityId;

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (node == null) {
            errors.add("node");
        }
        return errors;
	}
}
