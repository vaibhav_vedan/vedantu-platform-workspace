/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.enums;

/**
 *
 * @author ajith
 */
public enum ContentType {
	TEST, ASSIGNMENT, DOCUMENT, NOTES, VIDEO, WEBINAR, ASSIGNMENT_PDF, EXTERNAL_VIDEO_URL, EXTERNAL_URL
}