package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CreateBatchCurriculumReq extends AbstractFrontEndReq{

	private CurriculumEntityName entityName;
	private String entityId;
	private CurriculumEntityName parentEntityName;
	private String parentEntityId;
	

	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public CurriculumEntityName getEntityName() {
		return entityName;
	}
	public void setEntityName(CurriculumEntityName entityName) {
		this.entityName = entityName;
	}
	public CurriculumEntityName getParentEntityName() {
		return parentEntityName;
	}
	public void setParentEntityName(CurriculumEntityName parentEntityName) {
		this.parentEntityName = parentEntityName;
	}
	public String getParentEntityId() {
		return parentEntityId;
	}
	public void setParentEntityId(String parentEntityId) {
		this.parentEntityId = parentEntityId;
	}
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (entityName == null) {
            errors.add("entityName");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (parentEntityName == null) {
            errors.add("parentEntityName");
        }
        if (StringUtils.isEmpty(parentEntityId)) {
            errors.add("parentEntityId");
        }
        return errors;
	}
}
