package com.vedantu.lms.cmds.pojo;

import java.util.List;

import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.LMSType;
import com.vedantu.lms.cmds.enums.UserType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class ContentInfoResp extends AbstractMongoStringIdEntity{
	
	private UserType userType;
	private String studentId;
	private String studentFullName;
	private String teacherId;
	private String teacherFullName;
	private String contentTitle;
	private String courseName;
	private String subject;
	private String courseId;
	private Long expiryTime;


	private ContentType contentType;
	private ContentInfoType contentInfoType;
	private EngagementType engagementType;
	private ContentState contentState;
	private LMSType lmsType;

	private String contentLink;
	private String studentActionLink;
	private String teacherActionLink;

	private EntityType contextType;
	private String contextId;


	private List<String> tags;


	private TestContentInfoMetadataPojo metadata;
	private AccessLevel accessLevel;

	private Long boardId;

	//TODO: Following fields are added for backward compatibility of moodle and youscore, Do not use it in future
	private String testId;
	private Long duration;
	private Long attemptedTime;
	private Long evaulatedTime;
	private Float totalMarks;
	private Float marksAcheived;
	private Integer noOfQuestions;
	private String attemptId; // Latest attempt id incase of multiple active
	// attempts
	public UserType getUserType() {
		return userType;
	}
	public void setUserType(UserType userType) {
		this.userType = userType;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudentFullName() {
		return studentFullName;
	}
	public void setStudentFullName(String studentFullName) {
		this.studentFullName = studentFullName;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherFullName() {
		return teacherFullName;
	}
	public void setTeacherFullName(String teacherFullName) {
		this.teacherFullName = teacherFullName;
	}
	public String getContentTitle() {
		return contentTitle;
	}
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public Long getExpiryTime() {
		return expiryTime;
	}
	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}
	public ContentType getContentType() {
		return contentType;
	}
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	public ContentInfoType getContentInfoType() {
		return contentInfoType;
	}
	public void setContentInfoType(ContentInfoType contentInfoType) {
		this.contentInfoType = contentInfoType;
	}
	public EngagementType getEngagementType() {
		return engagementType;
	}
	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}
	public ContentState getContentState() {
		return contentState;
	}
	public void setContentState(ContentState contentState) {
		this.contentState = contentState;
	}
	public LMSType getLmsType() {
		return lmsType;
	}
	public void setLmsType(LMSType lmsType) {
		this.lmsType = lmsType;
	}
	public String getContentLink() {
		return contentLink;
	}
	public void setContentLink(String contentLink) {
		this.contentLink = contentLink;
	}
	public String getStudentActionLink() {
		return studentActionLink;
	}
	public void setStudentActionLink(String studentActionLink) {
		this.studentActionLink = studentActionLink;
	}
	public String getTeacherActionLink() {
		return teacherActionLink;
	}
	public void setTeacherActionLink(String teacherActionLink) {
		this.teacherActionLink = teacherActionLink;
	}
	public EntityType getContextType() {
		return contextType;
	}
	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public TestContentInfoMetadataPojo getMetadata() {
		return metadata;
	}
	public void setMetadata(TestContentInfoMetadataPojo metadata) {
		this.metadata = metadata;
	}
	public AccessLevel getAccessLevel() {
		return accessLevel;
	}
	public void setAccessLevel(AccessLevel accessLevel) {
		this.accessLevel = accessLevel;
	}
	public Long getBoardId() {
		return boardId;
	}
	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}
	public String getTestId() {
		return testId;
	}
	public void setTestId(String testId) {
		this.testId = testId;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public Long getAttemptedTime() {
		return attemptedTime;
	}
	public void setAttemptedTime(Long attemptedTime) {
		this.attemptedTime = attemptedTime;
	}
	public Long getEvaulatedTime() {
		return evaulatedTime;
	}
	public void setEvaulatedTime(Long evaulatedTime) {
		this.evaulatedTime = evaulatedTime;
	}
	public Float getTotalMarks() {
		return totalMarks;
	}
	public void setTotalMarks(Float totalMarks) {
		this.totalMarks = totalMarks;
	}
	public Float getMarksAcheived() {
		return marksAcheived;
	}
	public void setMarksAcheived(Float marksAcheived) {
		this.marksAcheived = marksAcheived;
	}
	public Integer getNoOfQuestions() {
		return noOfQuestions;
	}
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}
	public String getAttemptId() {
		return attemptId;
	}
	public void setAttemptId(String attemptId) {
		this.attemptId = attemptId;
	}

}
