package com.vedantu.lms.cmds.enums;

public enum ContentInfoType
{
	OBJECTIVE, SUBJECTIVE, MIXED, NOTES, ASSIGNMENT
}
