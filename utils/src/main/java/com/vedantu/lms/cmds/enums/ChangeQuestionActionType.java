/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.enums;

/**
 *
 * @author parashar
 */
public enum ChangeQuestionActionType {
    
    GRACE_MARKS, NO_MARKS, NO_ACTION, CHANGE_MARKS;
    
}
