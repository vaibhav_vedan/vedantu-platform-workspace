package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 24/05/17.
 */
public enum VideoType {
    VZAAR, YOUTUBE, VIMEO
}
