/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.response;

import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

/**
 *
 * @author ajith
 */
public class VoteRes extends AbstractMongoStringIdEntityBean {

    private int voteValue; 
    private SocialContextType socialContextType;
    private String contextId;
    private Long userId;

    public int getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(int voteValue) {
        this.voteValue = voteValue;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    
}
