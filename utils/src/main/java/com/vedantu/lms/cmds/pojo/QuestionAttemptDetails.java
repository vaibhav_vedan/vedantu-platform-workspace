package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.AttemptStateOfQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by somil on 23/03/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionAttemptDetails {
    private List<String> attempt;
    private Integer duration = 0;
    private AttemptStateOfQuestion attemptStateOfQuestion;
}
