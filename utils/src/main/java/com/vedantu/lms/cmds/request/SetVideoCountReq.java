package com.vedantu.lms.cmds.request;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SetVideoCountReq extends AbstractFrontEndReq {

    private String videoId;
    private Long count;
    private Long disLikeCount;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String questionId) {
        this.videoId = questionId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getDisLikeCount() {
        return disLikeCount;
    }

    public void setDisLikeCount(Long disLikeCount) {
        this.disLikeCount = disLikeCount;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(videoId)) {
            errors.add("videoId");
        }
        return errors;
    }

}
