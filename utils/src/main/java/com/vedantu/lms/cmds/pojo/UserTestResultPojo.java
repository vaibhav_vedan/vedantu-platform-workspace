/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.EnumBasket;
import java.util.List;

/**
 *
 * @author parashar
 */
public class UserTestResultPojo implements Comparable<UserTestResultPojo>{
    
    private Float totalMarks;
    private Float marksAchieved;
    private boolean attempted = false;
    private String batchId;
    private List<TestSectionResult> testSectionResults;
    private String testName;
    private Long time;
    private EnumBasket.TestTagType tag;
    private Float percentile;
    private Float averageMarks;
    private Float zScore;
    private Integer totalQuestions = 0;
    private Integer attemptedQuestion = 0;
    private Integer unAttemptedQuestion = 0;
    private Integer correct = 0;
    private Integer inCorrect = 0;
    
    
    /**
     * @return the totalMarks
     */
    public Float getTotalMarks() {
        return totalMarks;
    }

    /**
     * @param totalMarks the totalMarks to set
     */
    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    /**
     * @return the marksAchieved
     */
    public Float getMarksAchieved() {
        return marksAchieved;
    }

    /**
     * @param marksAchieved the marksAchieved to set
     */
    public void setMarksAchieved(Float marksAchieved) {
        this.marksAchieved = marksAchieved;
    }

    /**
     * @return the attempted
     */
    public boolean isAttempted() {
        return attempted;
    }

    /**
     * @param attempted the attempted to set
     */
    public void setAttempted(boolean attempted) {
        this.attempted = attempted;
    }

    /**
     * @return the contextId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the contextId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the testSectionResults
     */
    public List<TestSectionResult> getTestSectionResults() {
        return testSectionResults;
    }

    /**
     * @param testSectionResults the testSectionResults to set
     */
    public void setTestSectionResults(List<TestSectionResult> testSectionResults) {
        this.testSectionResults = testSectionResults;
    }

    /**
     * @return the testName
     */
    public String getTestName() {
        return testName;
    }

    /**
     * @param testName the testName to set
     */
    public void setTestName(String testName) {
        this.testName = testName;
    }

    /**
     * @return the time
     */
    public Long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * @return the tag
     */
    public EnumBasket.TestTagType getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(EnumBasket.TestTagType tag) {
        this.tag = tag;
    }

    @Override
    public int compareTo(UserTestResultPojo o) {
        
        return (int)(this.getTime() - o.getTime());
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the percentile
     */
    public Float getPercentile() {
        return percentile;
    }

    /**
     * @param percentile the percentile to set
     */
    public void setPercentile(Float percentile) {
        this.percentile = percentile;
    }

    /**
     * @return the averageMarks
     */
    public Float getAverageMarks() {
        return averageMarks;
    }

    /**
     * @param averageMarks the averageMarks to set
     */
    public void setAverageMarks(Float averageMarks) {
        this.averageMarks = averageMarks;
    }

    /**
     * @return the zScore
     */
    public Float getzScore() {
        return zScore;
    }

    /**
     * @param zScore the zScore to set
     */
    public void setzScore(Float zScore) {
        this.zScore = zScore;
    }

    /**
     * @return the totalQuestions
     */
    public Integer getTotalQuestions() {
        return totalQuestions;
    }

    /**
     * @param totalQuestions the totalQuestions to set
     */
    public void setTotalQuestions(Integer totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    /**
     * @return the attemptedQuestion
     */
    public Integer getAttemptedQuestion() {
        return attemptedQuestion;
    }

    /**
     * @param attemptedQuestion the attemptedQuestion to set
     */
    public void setAttemptedQuestion(Integer attemptedQuestion) {
        this.attemptedQuestion = attemptedQuestion;
    }

    /**
     * @return the unAttemptedQuestion
     */
    public Integer getUnAttemptedQuestion() {
        return unAttemptedQuestion;
    }

    /**
     * @param unAttemptedQuestion the unAttemptedQuestion to set
     */
    public void setUnAttemptedQuestion(Integer unAttemptedQuestion) {
        this.unAttemptedQuestion = unAttemptedQuestion;
    }

    /**
     * @return the correct
     */
    public Integer getCorrect() {
        return correct;
    }

    /**
     * @param correct the correct to set
     */
    public void setCorrect(Integer correct) {
        this.correct = correct;
    }

    /**
     * @return the inCorrect
     */
    public Integer getInCorrect() {
        return inCorrect;
    }

    /**
     * @param inCorrect the inCorrect to set
     */
    public void setInCorrect(Integer inCorrect) {
        this.inCorrect = inCorrect;
    }

    @Override
    public String toString() {
        return "UserTestResultPojo{" + "totalMarks=" + totalMarks + ", marksAchieved=" + marksAchieved + ", attempted=" + attempted + ", batchId=" + batchId + ", testSectionResults=" + testSectionResults + ", testName=" + testName + ", time=" + time + ", tag=" + tag + ", percentile=" + percentile + ", averageMarks=" + averageMarks + ", zScore=" + zScore + ", totalQuestions=" + totalQuestions + ", attemptedQuestion=" + attemptedQuestion + ", unAttemptedQuestion=" + unAttemptedQuestion + ", correct=" + correct + ", inCorrect=" + inCorrect + '}';
    }
    
}
