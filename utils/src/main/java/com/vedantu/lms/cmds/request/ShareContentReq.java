package com.vedantu.lms.cmds.request;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.vedantu.lms.cmds.enums.ContentSubType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
public class ShareContentReq extends AbstractFrontEndReq {

	private String title;
	private Long teacherId;
	private String description;
	private ContentType contentType;
	private String url;
	private String contentId;
	private List<Long> studentIds;
	private String courseName;
	private EntityType contextType;
	private String contextId;
	private Long boardId;
	private String teacherName;
	private boolean migration;
	private long creationTime;
	private Set<String> topicNames;
	private ContentSubType contentSubType;

	public ShareContentReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ShareContentReq(String title, Long teacherId, String description, ContentType contentType, String url,
						   String contentId, List<Long> studentIds, String courseName, EntityType contextType, String contextId, Long boardId,
						   Set<String> topicNames, ContentSubType contentSubType) {
		super();
		this.title = title;
		this.teacherId = teacherId;
		this.description = description;
		this.contentType = contentType;
		this.url = url;
		this.contentId = contentId;
		this.studentIds = studentIds;
		this.courseName = courseName;
		this.contextType = contextType;
		this.contextId = contextId;
		this.boardId = boardId;
		this.contentSubType = contentSubType;
		this.topicNames = topicNames;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if(migration){

			if(Objects.isNull(teacherId)){
				errors.add("Teacher id cann't be null");
			}

			if(StringUtils.isEmpty(contentId) && StringUtils.isEmpty(url)){
				errors.add("At least one of contentId or url should be non empty");
			}

			if(StringUtils.isEmpty(contextId) || Objects.isNull(contextType)){
				errors.add("contextId[batchId] or its type cann't be null");
			}

			if(creationTime==0){
				errors.add("Creation time cann't be 0");
			}
		}
		return errors;
	}
	public Set<String> getTopicNames() {
		return topicNames;
	}

	public void setTopicNames(Set<String> topicNames) {
		this.topicNames = topicNames;
	}

	public ContentSubType getContentSubType() {
		return contentSubType;
	}

	public void setContentSubType(ContentSubType contentSubType) {
		this.contentSubType = contentSubType;
	}
}
