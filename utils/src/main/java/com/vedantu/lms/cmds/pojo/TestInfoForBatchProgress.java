package com.vedantu.lms.cmds.pojo;

import java.util.List;

public class TestInfoForBatchProgress {
	
	private Long userId;
	private Long boardId;
	private String contextId;
	private String contextType;
	private Integer shared=0;
	private Integer attempted=0;
	private Integer evaluated=0;
	private List<StudentTestBasicInfo> tests;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getBoardId() {
		return boardId;
	}
	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public String getContextType() {
		return contextType;
	}
	public void setContextType(String contextType) {
		this.contextType = contextType;
	}
	public Integer getShared() {
		return shared;
	}
	public void setShared(Integer shared) {
		this.shared = shared;
	}
	public Integer getAttempted() {
		return attempted;
	}
	public void setAttempted(Integer attempted) {
		this.attempted = attempted;
	}
	public Integer getEvaluated() {
		return evaluated;
	}
	public void setEvaluated(Integer evaluated) {
		this.evaluated = evaluated;
	}
	public List<StudentTestBasicInfo> getTests() {
		return tests;
	}
	public void setTests(List<StudentTestBasicInfo> tests) {
		this.tests = tests;
	}

}
