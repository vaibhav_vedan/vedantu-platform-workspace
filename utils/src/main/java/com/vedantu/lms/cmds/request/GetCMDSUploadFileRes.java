package com.vedantu.lms.cmds.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.aws.response.UploadFileUrlRes;

public class GetCMDSUploadFileRes {
	private List<UploadFileUrlRes> fileInfos = new ArrayList<UploadFileUrlRes>();
	private UploadFileSourceType uploadFileSourceType;
	private String uploadFileSourceId;

	public GetCMDSUploadFileRes() {
		super();
	}

	public GetCMDSUploadFileRes(List<UploadFileUrlRes> fileInfos, UploadFileSourceType uploadFileSourceType,
			String uploadFileSourceId) {
		super();
		this.fileInfos = fileInfos;
		this.uploadFileSourceType = uploadFileSourceType;
		this.uploadFileSourceId = uploadFileSourceId;
	}

	public List<UploadFileUrlRes> getFileInfos() {
		return fileInfos;
	}

	public void addFileInfo(UploadFileUrlRes fileInfo) {
		this.fileInfos.add(fileInfo);
	}

	public UploadFileSourceType getUploadFileSourceType() {
		return uploadFileSourceType;
	}

	public void setUploadFileSourceType(UploadFileSourceType uploadFileSourceType) {
		this.uploadFileSourceType = uploadFileSourceType;
	}

	public String getUploadFileSourceId() {
		return uploadFileSourceId;
	}

	public void setUploadFileSourceId(String uploadFileSourceId) {
		this.uploadFileSourceId = uploadFileSourceId;
	}

	@Override
	public String toString() {
		return "GetCMDSUploadFileRes [fileInfos=" + fileInfos + ", uploadFileSourceType=" + uploadFileSourceType
				+ ", uploadFileSourceId=" + uploadFileSourceId + ", toString()=" + super.toString() + "]";
	}

}
