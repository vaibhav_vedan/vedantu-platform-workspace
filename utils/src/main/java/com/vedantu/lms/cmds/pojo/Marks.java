package com.vedantu.lms.cmds.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by somil on 22/03/17.
 */
@Data
@NoArgsConstructor
public class Marks {
    private float positive;
    private float negative;
}
