package com.vedantu.lms.cmds.request;

import com.vedantu.User.Role;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 * Created by somil on 26/04/17.
 */
public class DeleteTestReq extends AbstractFrontEndReq{
    private String testId;


    public DeleteTestReq() {
    }


    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }


    @Override
    public String toString() {
        return "DeleteTestReq{" +
                "testId='" + testId + '\'' +
                '}';
    }
}
