package com.vedantu.lms.cmds.pojo;

import com.vedantu.User.UserBasicInfo;

public class ChallengeUserPoints {

	private Long userId;
	private Long points;
	private String category;
	private Long time;
	private Integer attempts;
	private UserBasicInfo user;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public UserBasicInfo getUser() {
		return user;
	}

	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	public Integer getAttempts() {
		return attempts;
	}

	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}
	
}
