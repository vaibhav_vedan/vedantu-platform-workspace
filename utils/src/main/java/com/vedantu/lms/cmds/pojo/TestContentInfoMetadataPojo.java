package com.vedantu.lms.cmds.pojo;

import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by somil on 01/05/17.
 */

@Data
@NoArgsConstructor
public class TestContentInfoMetadataPojo extends AbstractMongoStringIdEntity {
    String testId;
    Long duration;
    Float totalMarks;
    Integer noOfQuestions;

    List<CMDSTestAttemptPojo> testAttempts;
    public Long minStartTime;
    public Long maxStartTime;
    public boolean reattemptAllowed = false;
    public TestResultVisibility displayResultOnEnd = TestResultVisibility.VISIBLE;
    public String displayMessageOnEnd;
}
