/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.pojo;

/**
 *
 * @author parashar
 */
public class TestSectionResult {
    
    private Float totalMarks;
    private Float marksAchieved;
    private String sectionName;
    private Integer totalQuestions = 0;
    private Integer attemptedQuestion = 0;
    private Integer unAttemptedQuestion = 0;
    private Integer correct = 0;
    private Integer inCorrect = 0;    
    

    /**
     * @return the totalMarks
     */
    public Float getTotalMarks() {
        return totalMarks;
    }

    /**
     * @param totalMarks the totalMarks to set
     */
    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    /**
     * @return the marksAchieved
     */
    public Float getMarksAchieved() {
        return marksAchieved;
    }

    /**
     * @param marksAchieved the marksAchieved to set
     */
    public void setMarksAchieved(Float marksAchieved) {
        this.marksAchieved = marksAchieved;
    }

    /**
     * @return the sectionName
     */
    public String getSectionName() {
        return sectionName;
    }

    /**
     * @param sectionName the sectionName to set
     */
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    /**
     * @return the totalQuestions
     */
    public Integer getTotalQuestions() {
        return totalQuestions;
    }

    /**
     * @param totalQuestions the totalQuestions to set
     */
    public void setTotalQuestions(Integer totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    /**
     * @return the attemptedQuestion
     */
    public Integer getAttemptedQuestion() {
        return attemptedQuestion;
    }

    /**
     * @param attemptedQuestion the attemptedQuestion to set
     */
    public void setAttemptedQuestion(Integer attemptedQuestion) {
        this.attemptedQuestion = attemptedQuestion;
    }

    /**
     * @return the unAttemptedQuestion
     */
    public Integer getUnAttemptedQuestion() {
        return unAttemptedQuestion;
    }

    /**
     * @param unAttemptedQuestion the unAttemptedQuestion to set
     */
    public void setUnAttemptedQuestion(Integer unAttemptedQuestion) {
        this.unAttemptedQuestion = unAttemptedQuestion;
    }

    /**
     * @return the correct
     */
    public Integer getCorrect() {
        return correct;
    }

    /**
     * @param correct the correct to set
     */
    public void setCorrect(Integer correct) {
        this.correct = correct;
    }

    /**
     * @return the inCorrect
     */
    public Integer getInCorrect() {
        return inCorrect;
    }

    /**
     * @param inCorrect the inCorrect to set
     */
    public void setInCorrect(Integer inCorrect) {
        this.inCorrect = inCorrect;
    }
    
}
