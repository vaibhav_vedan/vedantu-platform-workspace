package com.vedantu.lms.cmds.pojo;


import com.vedantu.lms.cmds.enums.CMDSAttemptState;

/**
 * Created by somil on 24/05/17.
 */
public class TestAttemptInfo {
    private String attemptId;
    private CMDSAttemptState attemptState;


    public String getAttemptId() {
        return attemptId;
    }

    public void setAttemptId(String attemptId) {
        this.attemptId = attemptId;
    }

    public CMDSAttemptState getAttemptState() {
        return attemptState;
    }

    public void setAttemptState(CMDSAttemptState attemptState) {
        this.attemptState = attemptState;
    }

    @Override
    public String toString() {
        return "TestAttemptInfo{" +
                "attemptId='" + attemptId + '\'' +
                ", attemptState=" + attemptState +
                '}';
    }
}
