package com.vedantu.lms.cmds.pojo;

import java.util.Set;


public class AttemptedUserIds {

	private Set<Long> attemptedUserIds;
	private String category;

	public Set<Long> getAttemptedUserIds() {
		return attemptedUserIds;
	}

	public void setAttemptedUserIds(Set<Long> attemptedUserIds) {
		this.attemptedUserIds = attemptedUserIds;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
