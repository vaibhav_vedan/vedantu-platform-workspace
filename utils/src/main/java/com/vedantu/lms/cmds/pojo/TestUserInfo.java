package com.vedantu.lms.cmds.pojo;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import java.util.Map;

/**
 * Created by somil on 09/05/17.
 */
public class TestUserInfo extends AbstractMongoStringIdEntityBean {
    private String name;
    private String grade;
    private String email;
    private String contactNumber;
    private String phoneCode;
    private Map<String, String> userData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Map<String, String> getUserData() {
        return userData;
    }

    public void setUserData(Map<String, String> userData) {
        this.userData = userData;
    }
}
