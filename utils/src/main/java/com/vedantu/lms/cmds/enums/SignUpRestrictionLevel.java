package com.vedantu.lms.cmds.enums;

/**
 * Created by somil on 17/05/17.
 */
public enum SignUpRestrictionLevel {
    NO_SIGNUP, BEFORE_RESULT, BEFORE_TEST
}
