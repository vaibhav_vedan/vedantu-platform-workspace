package com.vedantu.lms.cmds.enums;

public enum DoubtPaidState {
    PAID, UNPAID, FOS
}
