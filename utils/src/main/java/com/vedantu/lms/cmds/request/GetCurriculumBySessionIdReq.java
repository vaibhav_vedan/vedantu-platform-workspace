/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.request;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class GetCurriculumBySessionIdReq extends AbstractReq{
    private List<String> contextIds;
    private CurriculumEntityName contextType;
    private String sessionId;
    private Long afterLastUpdated;
    private Long beforeLastUpdated;

    public List<String> getContextIds() {
        return contextIds;
    }

    public void setContextIds(List<String> contextIds) {
        this.contextIds = contextIds;
    }

    public CurriculumEntityName getContextType() {
        return contextType;
    }

    public void setContextType(CurriculumEntityName contextType) {
        this.contextType = contextType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getAfterLastUpdated() {
        return afterLastUpdated;
    }

    public void setAfterLastUpdated(Long afterLastUpdated) {
        this.afterLastUpdated = afterLastUpdated;
    }

    public Long getBeforeLastUpdated() {
        return beforeLastUpdated;
    }

    public void setBeforeLastUpdated(Long beforeLastUpdated) {
        this.beforeLastUpdated = beforeLastUpdated;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(contextIds)) {
            errors.add("contextId");
        }
        if (contextType == null) {
            errors.add("contextType");
        }
        return errors;
    }
    
}
