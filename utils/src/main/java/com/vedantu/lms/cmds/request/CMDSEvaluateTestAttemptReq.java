package com.vedantu.lms.cmds.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptImage;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CMDSEvaluateTestAttemptReq extends AbstractFrontEndReq {
	private String attemptId;
	private boolean evaluationComplete;
	private String attemptRemarks;
	private List<CMDSTestAttemptImage> fileInfos;
	private List<QuestionAnalytics> results;

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(this.attemptId)) {
			errors.add("attemptId");
		}
		return errors;
	}

}
