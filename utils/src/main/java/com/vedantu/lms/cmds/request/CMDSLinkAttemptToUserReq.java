package com.vedantu.lms.cmds.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CMDSLinkAttemptToUserReq extends AbstractFrontEndReq {
	private String attemptId;
	private String userId;
	
	public CMDSLinkAttemptToUserReq() {
		super();
	}
	
	public String getAttemptId() {
		return attemptId;
	}
	
	public void setAttemptId(String attemptId) {
		this.attemptId = attemptId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		errors.addAll(super.collectVerificationErrors());
		if (StringUtils.isEmpty(this.attemptId)) {
			errors.add("contentInfoId");
		}

		return errors;
	}

	public void validate() throws BadRequestException {
		List<String> errors = collectErrors();
		if (!CollectionUtils.isEmpty(errors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Missing parameters : " + Arrays.toString(errors.toArray()));
		}
	}
	
}
