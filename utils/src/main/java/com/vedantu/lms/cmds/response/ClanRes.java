package com.vedantu.lms.cmds.response;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserMinimalInfo;
import com.vedantu.util.pojo.CloudStorageEntity;

public class ClanRes {

	private String clanId;
    private String title;
    private String tagLine;
    private String event;
    private List<String> allowedCategories;
    private CloudStorageEntity clanPic;
    private String shareRefCode;
    private Long leader;
    private String shareLink;
	private UserMinimalInfo leaderInfo;
	private List<UserMinimalInfo> clanMembers;
	private int memberCount;
	private Long points;

	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public List<String> getAllowedCategories() {
		return allowedCategories;
	}

	public void setAllowedCategories(List<String> allowedCategories) {
		this.allowedCategories = allowedCategories;
	}

	public CloudStorageEntity getClanPic() {
		return clanPic;
	}

	public void setClanPic(CloudStorageEntity clanPic) {
		this.clanPic = clanPic;
	}

	public String getShareRefCode() {
		return shareRefCode;
	}

	public void setShareRefCode(String shareRefCode) {
		this.shareRefCode = shareRefCode;
	}

	public Long getLeader() {
		return leader;
	}

	public void setLeader(Long leader) {
		this.leader = leader;
	}

	public String getShareLink() {
		return shareLink;
	}

	public void setShareLink(String shareLink) {
		this.shareLink = shareLink;
	}

	public UserMinimalInfo getLeaderInfo() {
		return leaderInfo;
	}

	public void setLeaderInfo(UserMinimalInfo leaderInfo) {
		this.leaderInfo = leaderInfo;
	}

	public List<UserMinimalInfo> getClanMembers() {
		return clanMembers;
	}

	public void setClanMembers(List<UserMinimalInfo> clanMembers) {
		this.clanMembers = clanMembers;
	}

	public int getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}
}
