package com.vedantu.lms.cmds.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by somil on 22/03/17.
 */
public class CMDSTestQuestion {
    private String questionId;
    private int questionIndex;
    private Marks marks;
    private boolean graceMarks = false;
    private boolean noMarks = false;
    private Set<String> mainTags;
    private Set<String> analysisTags;


    public boolean isChangeMarks() {
        return changeMarks;
    }

    public void setChangeMarks(boolean changeMarks) {
        this.changeMarks = changeMarks;
    }

    private boolean changeMarks = false;
    private Long questionChangedTime;
    
    private List<QuestionChangeRecord> questionChangeRecords = new ArrayList<>();

    public CMDSTestQuestion() {
        super();
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public int getQuestionIndex() {
        return questionIndex;
    }

    public void setQuestionIndex(int questionIndex) {
        this.questionIndex = questionIndex;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    /**
     * @return the questionChangeRecords
     */
    public List<QuestionChangeRecord> getQuestionChangeRecords() {
        return questionChangeRecords;
    }

    /**
     * @param questionChangeRecords the questionChangeRecords to set
     */
    public void setQuestionChangeRecords(List<QuestionChangeRecord> questionChangeRecords) {
        this.questionChangeRecords = questionChangeRecords;
    }

    /**
     * @return the graceMarks
     */
    public boolean isGraceMarks() {
        return graceMarks;
    }

    /**
     * @param graceMarks the graceMarks to set
     */
    public void setGraceMarks(boolean graceMarks) {
        this.graceMarks = graceMarks;
    }

    /**
     * @return the noMarks
     */
    public boolean isNoMarks() {
        return noMarks;
    }

    /**
     * @param noMarks the noMarks to set
     */
    public void setNoMarks(boolean noMarks) {
        this.noMarks = noMarks;
    }

    /**
     * @return the questionChangedTime
     */
    public Long getQuestionChangedTime() {
        return questionChangedTime;
    }

    /**
     * @param questionChangedTime the questionChangedTime to set
     */
    public void setQuestionChangedTime(Long questionChangedTime) {
        this.questionChangedTime = questionChangedTime;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public Set<String> getAnalysisTags() {
        return analysisTags;
    }

    public void setAnalysisTags(Set<String> analysisTags) {
        this.analysisTags = analysisTags;
    }

    @Override
    public String toString() {
        return "CMDSTestQuestion{" +
                "questionId='" + questionId + '\'' +
                ", questionIndex=" + questionIndex +
                ", marks=" + marks +
                ", graceMarks=" + graceMarks +
                ", noMarks=" + noMarks +
                ", mainTags=" + mainTags +
                ", analysisTags=" + analysisTags +
                ", changeMarks=" + changeMarks +
                ", questionChangedTime=" + questionChangedTime +
                ", questionChangeRecords=" + questionChangeRecords +
                '}';
    }
}
