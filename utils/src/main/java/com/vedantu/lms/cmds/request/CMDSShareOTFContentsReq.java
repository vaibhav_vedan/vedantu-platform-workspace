package com.vedantu.lms.cmds.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.lms.cmds.pojo.CMDSShareContentinfo;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.session.pojo.EntityType;

public class CMDSShareOTFContentsReq {
	private List<UserBasicInfo> students;
	private List<CMDSShareContentinfo> shareInfo;
	private String courseName;
	private Boolean sendCommunication = false;

	private EntityType contextType;
	private String contextId;
	private EngagementType engagementType;

	public CMDSShareOTFContentsReq() {
		super();
		this.students = new ArrayList<>();
		this.shareInfo = new ArrayList<>();
	}

	public List<UserBasicInfo> getStudents() {
		return students;
	}

	public void setStudents(List<UserBasicInfo> students) {
		this.students = students;
	}

	public List<CMDSShareContentinfo> getShareInfo() {
		return shareInfo;
	}

	public void setShareInfo(List<CMDSShareContentinfo> shareInfo) {
		this.shareInfo = shareInfo;
	}

	public void addShareContentInfo(CMDSShareContentinfo a) {
		this.shareInfo.add(a);
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public Boolean getSendCommunication() {
		return sendCommunication;
	}

	public void setSendCommunication(Boolean sendCommunication) {
		this.sendCommunication = sendCommunication;
	}

	public EngagementType getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}

	@Override
	public String toString() {
		return "CMDSShareOTFContentsReq [students=" + students + ", shareInfo=" + shareInfo + ", courseName="
				+ courseName + ", toString()=" + super.toString() + "]";
	}
}
