package com.vedantu.lms.cmds.enums;

public enum AttemptStateOfQuestion {
    MARK_FOR_REVIEW, ANSWERED, CLEAR_RESPONSE, TO_BE_REVIEWED
}
