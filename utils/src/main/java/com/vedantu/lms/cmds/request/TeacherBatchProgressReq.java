package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class TeacherBatchProgressReq extends AbstractFrontEndListReq{

	private Long boardId;
	private String subject;
	private CurriculumEntityName contextType; //OTF_BATCH,COURSE_PLAN
	private String contextId;
	private Long beforeEndTime;
	private List<String> studentIds;
	private Long teacherId;
	public Long getBoardId() {
		return boardId;
	}
	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public CurriculumEntityName getContextType() {
		return contextType;
	}
	public void setContextType(CurriculumEntityName contextType) {
		this.contextType = contextType;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public Long getBeforeEndTime() {
		return beforeEndTime;
	}
	public void setBeforeEndTime(Long beforeEndTime) {
		this.beforeEndTime = beforeEndTime;
	}
	public List<String> getStudentIds() {
		return studentIds;
	}
	public void setStudentIds(List<String> studentIds) {
		this.studentIds = studentIds;
	}
	
	
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (contextType == null) {
            errors.add("contextType");
        }
        if (StringUtils.isEmpty(contextId)) {
            errors.add("contextId");
        }
        if (boardId == null) {
            errors.add("boardId");
        }
        
        return errors;
	}
}
