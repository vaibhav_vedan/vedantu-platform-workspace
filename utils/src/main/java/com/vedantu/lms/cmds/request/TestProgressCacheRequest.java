package com.vedantu.lms.cmds.request;

import com.vedantu.lms.cmds.pojo.TestProgressCache;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class TestProgressCacheRequest extends AbstractFrontEndReq {
    private String testAttemptId;
    private TestProgressCache testProgressCache;
    private long duration = 3 * DateTimeUtils.SECONDS_PER_HOUR;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(StringUtils.isEmpty(testAttemptId) || Objects.isNull(testProgressCache)) {
            errors.add("Parameters missing! Please check request");
        }
        return errors;
    }
}
