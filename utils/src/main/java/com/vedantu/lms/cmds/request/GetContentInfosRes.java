package com.vedantu.lms.cmds.request;

import java.util.List;

import com.vedantu.lms.cmds.pojo.ContentInfoResp;

public class GetContentInfosRes {

	private int count;
	private Integer totalCount;
	private List<ContentInfoResp> list;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public List<ContentInfoResp> getList() {
		return list;
	}
	public void setList(List<ContentInfoResp> list) {
		this.list = list;
	}
}
