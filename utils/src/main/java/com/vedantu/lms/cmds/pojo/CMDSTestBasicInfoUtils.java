/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.cmds.pojo;

import java.util.List;

/**
 *
 * @author ajith
 */
public class CMDSTestBasicInfoUtils {

    private int versionNo = 1;

    public String description;
    public int qusCount;
    public Long duration;
    public Float totalMarks = 0f;

    public String code;
    private boolean hardStop=false;
    public Long minStartTime;
    public Long maxStartTime;
    public boolean reattemptAllowed = false;
    private List<String> instructions;


    public int getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(int versionNo) {
        this.versionNo = versionNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQusCount() {
        return qusCount;
    }

    public void setQusCount(int qusCount) {
        this.qusCount = qusCount;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Long minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Long getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public boolean isReattemptAllowed() {
        return reattemptAllowed;
    }

    public void setReattemptAllowed(boolean reattemptAllowed) {
        this.reattemptAllowed = reattemptAllowed;
    }

    public List<String> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<String> instructions) {
        this.instructions = instructions;
    }

    public boolean isHardStop() {
        return hardStop;
    }

    public void setHardStop(boolean hardStop) {
        this.hardStop = hardStop;
    }
}
