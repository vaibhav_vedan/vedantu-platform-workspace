package com.vedantu.lms.cmds.pojo;

import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.TestEndType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import java.util.List;

/**
 * Created by somil on 01/05/17.
 */
public class CMDSTestAttemptPojo extends AbstractMongoStringIdEntityBean {
    private String testId;
    private String contentInfoId;
    //private String studentId;
    private String teacherId;
    private Long evaluatedViewTime;
    private Long evaluatedTime;
    private Long reportViewed;
    private CMDSAttemptState attemptState;
    private float totalMarks;
    private Float marksAcheived = 0f;
    private Long timeTakenByStudent;
    private String attemptRemarks;

    private List<CMDSTestAttemptImage> imageDetails;


    private Long duration;
    private Long startTime;
    private Long endTime;
    private Long startedAt;
    private Long endedAt;
    private TestEndType testEndType;


    private List<QuestionAnalytics> resultEntries;
    private List<CategoryAnalytics> categoryAnalyticsList;
    private Double percentile;
    private Integer rank;
    private Integer attempted = 0;
    private Integer unattempted = 0;
    private Integer correct = 0;
    private Integer incorrect = 0;

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getContentInfoId() {
        return contentInfoId;
    }

    public void setContentInfoId(String contentInfoId) {
        this.contentInfoId = contentInfoId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public Long getEvaluatedViewTime() {
        return evaluatedViewTime;
    }

    public void setEvaluatedViewTime(Long evaluatedViewTime) {
        this.evaluatedViewTime = evaluatedViewTime;
    }

    public Long getEvaluatedTime() {
        return evaluatedTime;
    }

    public void setEvaluatedTime(Long evaluatedTime) {
        this.evaluatedTime = evaluatedTime;
    }

    public Long getReportViewed() {
        return reportViewed;
    }

    public void setReportViewed(Long reportViewed) {
        this.reportViewed = reportViewed;
    }

    public CMDSAttemptState getAttemptState() {
        return attemptState;
    }

    public void setAttemptState(CMDSAttemptState attemptState) {
        this.attemptState = attemptState;
    }

    public float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public Float getMarksAcheived() {
        return marksAcheived;
    }

    public void setMarksAcheived(Float marksAcheived) {
        this.marksAcheived = marksAcheived;
    }

    public Long getTimeTakenByStudent() {
        return timeTakenByStudent;
    }

    public void setTimeTakenByStudent(Long timeTakenByStudent) {
        this.timeTakenByStudent = timeTakenByStudent;
    }

    public String getAttemptRemarks() {
        return attemptRemarks;
    }

    public void setAttemptRemarks(String attemptRemarks) {
        this.attemptRemarks = attemptRemarks;
    }

    public List<CMDSTestAttemptImage> getImageDetails() {
        return imageDetails;
    }

    public void setImageDetails(List<CMDSTestAttemptImage> imageDetails) {
        this.imageDetails = imageDetails;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Long startedAt) {
        this.startedAt = startedAt;
    }

    public Long getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Long endedAt) {
        this.endedAt = endedAt;
    }

    public TestEndType getTestEndType() {
        return testEndType;
    }

    public void setTestEndType(TestEndType testEndType) {
        this.testEndType = testEndType;
    }

    public List<QuestionAnalytics> getResultEntries() {
        return resultEntries;
    }

    public void setResultEntries(List<QuestionAnalytics> resultEntries) {
        this.resultEntries = resultEntries;
    }

    public List<CategoryAnalytics> getCategoryAnalyticsList() {
        return categoryAnalyticsList;
    }

    public void setCategoryAnalyticsList(List<CategoryAnalytics> categoryAnalyticsList) {
        this.categoryAnalyticsList = categoryAnalyticsList;
    }

    public Double getPercentile() {
        return percentile;
    }

    public void setPercentile(Double percentile) {
        this.percentile = percentile;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getAttempted() {
        return attempted;
    }

    public void setAttempted(Integer attempted) {
        this.attempted = attempted;
    }

    public Integer getUnattempted() {
        return unattempted;
    }

    public void setUnattempted(Integer unattempted) {
        this.unattempted = unattempted;
    }

    public Integer getCorrect() {
        return correct;
    }

    public void setCorrect(Integer correct) {
        this.correct = correct;
    }

    public Integer getIncorrect() {
        return incorrect;
    }

    public void setIncorrect(Integer incorrect) {
        this.incorrect = incorrect;
    }
}
