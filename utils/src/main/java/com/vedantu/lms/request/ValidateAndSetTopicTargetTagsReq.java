package com.vedantu.lms.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;
import java.util.Set;

public class ValidateAndSetTopicTargetTagsReq extends AbstractFrontEndReq {

    private Set<String> targetGrades; // CBSE-10
    private Set<String> subjects;
    private Set<String> topics;

    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    public Set<String> getSubject() {
        return subjects;
    }

    public void setSubject(Set<String> subjects) {
        this.subjects = subjects;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(targetGrades)) {
            errors.add("TARGETGRADES");
        }
        if (ArrayUtils.isEmpty(topics)) {
            errors.add("TOPICS");
        }
//        if(subjects.isEmpty()) {
//            errors.add("SUBJECTS");
//        }
        return errors;
    }

    @Override
    public String toString() {
        return String
                .format("{targetGrades=%s, subjects=%s, topics=%s}",
                        targetGrades, subjects, topics);
    }
}
