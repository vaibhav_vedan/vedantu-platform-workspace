/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.request;

import java.util.List;

/**
 *
 * @author shyam
 */
public class GetTeacherCoursesReq {

    private String teacherId;
    private Integer start;
    private Integer end;
    private List<String> userIds;

    public GetTeacherCoursesReq() {
    }

    public GetTeacherCoursesReq(String teacherId, Integer start, Integer end, List<String> userIds) {
        this.teacherId = teacherId;
        this.start = start;
        this.end = end;
        this.userIds = userIds;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public String toString() {
        return "GetTeacherCoursesReq{" + "teacherId=" + teacherId + ", start=" + start + ", end=" + end + ", userIds=" + userIds + '}';
    }
}
