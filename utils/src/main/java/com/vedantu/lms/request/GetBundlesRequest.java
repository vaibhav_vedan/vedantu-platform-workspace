package com.vedantu.lms.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetBundlesRequest extends AbstractFrontEndReq
{
    private List<String> bundleIds;
    private List<String> displayTags;
    private String grade;
    private String userId;
}
