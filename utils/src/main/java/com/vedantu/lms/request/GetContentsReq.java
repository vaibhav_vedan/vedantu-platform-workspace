/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.request;

import java.util.List;

/**
 *
 * @author shyam
 */
public class GetContentsReq {

    private String studentId;
    private List<String> studentIds;
    private String teacherId;
    private String courseId;
    private String courseName;
    private List<String> contentType;
    private String studentName;
    private String searchFieldValue;
    private String sortParam;
    private Integer start;
    private Integer end;
    private List<String> contentState;
    private List<String> engagementType;
    private Long fromTime;
    private Long thruTime;

    public GetContentsReq() {
    }

    public GetContentsReq(String studentId, String teacherId, String courseId, String courseName, List<String> contentType, String studentName, String searchFieldValue, String sortParam, Integer start, Integer end, List<String> contentState, List<String> engagementType) {
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.courseId = courseId;
        this.courseName = courseName;
        this.contentType = contentType;
        this.studentName = studentName;
        this.searchFieldValue = searchFieldValue;
        this.sortParam = sortParam;
        this.start = start;
        this.end = end;
        this.contentState = contentState;
        this.engagementType = engagementType;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public String getCourseId() {
        return courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public List<String> getContentType() {
        return contentType;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getSearchFieldValue() {
        return searchFieldValue;
    }

    public String getSortParam() {
        return sortParam;
    }

    public Integer getStart() {
        return start;
    }

    public Integer getEnd() {
        return end;
    }

    public List<String> getContentState() {
        return contentState;
    }

    public List<String> getEngagementType() {
        return engagementType;
    }

    public void setContentState(List<String> contentState) {
        this.contentState = contentState;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setContentType(List<String> contentType) {
        this.contentType = contentType;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setSearchFieldValue(String searchFieldValue) {
        this.searchFieldValue = searchFieldValue;
    }

    public void setSortParam(String sortParam) {
        this.sortParam = sortParam;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public void setEngagementType(List<String> engagementType) {
        this.engagementType = engagementType;
    }

    public List<String> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<String> studentIds) {
        this.studentIds = studentIds;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getThruTime() {
        return thruTime;
    }

    public void setThruTime(Long thruTime) {
        this.thruTime = thruTime;
    }

    @Override
    public String toString() {
        return "GetContentsReq{" + "studentId=" + studentId + ", teacherId=" + teacherId + ", courseId=" + courseId + ", courseName=" + courseName + ", contentType=" + contentType + ", studentName=" + studentName + ", searchFieldValue=" + searchFieldValue + ", sortParam=" + sortParam + ", start=" + start + ", end=" + end + ", contentState=" + contentState + ", engagementType=" + engagementType + '}';
    }
}
