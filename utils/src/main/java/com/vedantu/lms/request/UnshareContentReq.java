package com.vedantu.lms.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 * Created by somil on 25/04/17.
 */
public class UnshareContentReq extends AbstractFrontEndReq {
    EntityType contextType;
    String contextId;
    String contentId;

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    @Override
    public String toString() {
        return "UnshareContentReq{" +
                "contextType=" + contextType +
                ", contextId='" + contextId + '\'' +
                ", contentId='" + contentId + '\'' +
                '}';
    }
}
