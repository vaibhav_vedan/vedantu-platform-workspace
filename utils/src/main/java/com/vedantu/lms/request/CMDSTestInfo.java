package com.vedantu.lms.request;

import com.vedantu.lms.cmds.enums.*;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.vedantu.platform.enums.SocialContextType.CMDSTEST;

public class CMDSTestInfo{
	private String id;
	private String testUrl;
	private Long duration;
	private int versionNo = 1;
	private String questionSetId;
    private SocialContextType socialContextType = CMDSTEST;
	public Set<String> boardIds;
	public Set<String> targetIds;
	public Set<String> grades;
	public Set<String> tags;
	public VedantuRecordState recordState = VedantuRecordState.ACTIVE;
	public String name;
	public float totalMarks;
	public ContentInfoType contentInfoType;
	private AccessLevel accessLevel;
	private SignUpRestrictionLevel signupHook;
	private List<String> instructions;
	public EnumBasket.TestTagType testTag;
	public Long minStartTime;
	public Long maxStartTime;
	private boolean savedState = false;
	private long lastUpdated;
	private boolean isAttempted;

	public AccessLevel getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(AccessLevel accessLevel) {
		this.accessLevel = accessLevel;
	}

	public SignUpRestrictionLevel getSignupHook() {
		return signupHook;
	}

	public void setSignupHook(SignUpRestrictionLevel signupHook) {
		this.signupHook = signupHook;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public int getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(int versionNo) {
		this.versionNo = versionNo;
	}

	public String getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(String questionSetId) {
		this.questionSetId = questionSetId;
	}

	public String getTestUrl() {
		return testUrl;
	}

	public void setTestUrl(String testUrl) {
		this.testUrl = testUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<String> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(Set<String> boardIds) {
		this.boardIds = boardIds;
	}

	public Set<String> getTargetIds() {
		return targetIds;
	}

	public void setTargetIds(Set<String> targetIds) {
		this.targetIds = targetIds;
	}

	public Set<String> getGrades() {
		return grades;
	}

	public void setGrades(Set<String> grades) {
		this.grades = grades;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public VedantuRecordState getRecordState() {
		return recordState;
	}

	public void setRecordState(VedantuRecordState recordState) {
		this.recordState = recordState;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public void setTotalMarks(float totalMarks) {
		this.totalMarks = totalMarks;
	}

	public ContentInfoType getContentInfoType() {
		return contentInfoType;
	}

	public void setContentInfoType(ContentInfoType contentInfoType) {
		this.contentInfoType = contentInfoType;
	}

	public Long getMinStartTime() {
		return minStartTime;
	}

	public void setMinStartTime(Long minStartTime) {
		this.minStartTime = minStartTime;
	}

	public Long getMaxStartTime() {
		return maxStartTime;
	}

	public void setMaxStartTime(Long maxStartTime) {
		this.maxStartTime = maxStartTime;
	}

	public List<String> getInstructions() {
            return instructions;
}

	public void setInstructions(List<String> instructions) {
		this.instructions = instructions;
	}

	public EnumBasket.TestTagType getTestTag() {
		return testTag;
	}

	public void setTestTag(EnumBasket.TestTagType testTag) {
            this.testTag = testTag;
        }

	public boolean isSavedState() {
		return savedState;
	}

	public void setSavedState(boolean savedState) {
		this.savedState = savedState;
	}

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

	public long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public boolean isAttempted() {
		return isAttempted;
	}

	public void setAttempted(boolean attempted) {
		isAttempted = attempted;
	}
}
