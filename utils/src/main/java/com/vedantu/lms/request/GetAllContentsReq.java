/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.lms.request;

/**
 *
 * @author shyam
 */
public class GetAllContentsReq {

    private Long startTime;
    private Long endTime;
    private String queryParam;

    public GetAllContentsReq() {
    }

    public GetAllContentsReq(Long startTime, Long endTime, String queryParam) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.queryParam = queryParam;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getQueryParam() {
        return queryParam;
    }

    public void setQueryParam(String queryParam) {
        this.queryParam = queryParam;
    }

    @Override
    public String toString() {
        return "GetAllCoursesReq{" + "startTime=" + startTime + ", endTime=" + endTime + ", queryParam=" + queryParam + '}';
    }
}
