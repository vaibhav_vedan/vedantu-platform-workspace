package com.vedantu.lms.request;

import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class ContentInfoReq {
    private String contextId;
    private Set<String> contextIds;
    private Set<String> fieldsInclude;
    private Set<String> fieldsExclude;
    private Long startTime;
    private Long endTime;
    private boolean canIncludeMetadata = false;
    private Integer start;
    private Integer size;

    public void addFieldsInclude(String field){

        if(fieldsInclude==null)
            fieldsInclude = new HashSet<>();

        fieldsInclude.add(field);

    }

    public void addFieldsExclude(String field){

        if(fieldsExclude==null)
            fieldsExclude = new HashSet<>();

        fieldsExclude.add(field);

    }

    public class Constants{
        public static final String CONTENT_TITLE = "contentTitle";
        public static final String METADATA = "metadata";
        public static final String METADATA_TEST_ATTEMPTS = METADATA + ".testAttempts";
    }
}
