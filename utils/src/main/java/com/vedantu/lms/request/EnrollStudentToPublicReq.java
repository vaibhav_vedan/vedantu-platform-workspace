package com.vedantu.lms.request;

import com.vedantu.User.User;

public class EnrollStudentToPublicReq {
	private String contentId;
	private User studentInfo;
	public EnrollStudentToPublicReq() {
		super();
		
	}
	
	public EnrollStudentToPublicReq(String contentId, User studentInfo) {
		super();
		this.contentId = contentId;
		this.studentInfo = studentInfo;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public User getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(User studentInfo) {
		this.studentInfo = studentInfo;
	}
}
