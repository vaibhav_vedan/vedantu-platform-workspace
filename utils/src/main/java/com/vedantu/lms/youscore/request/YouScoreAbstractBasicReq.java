package com.vedantu.lms.youscore.request;

import java.lang.reflect.Field;

import com.vedantu.lms.youscore.pojo.YouScoreConstants;

public class YouScoreAbstractBasicReq {
	public String AppId = YouScoreConstants.APP_ID;
	public String AppPass = YouScoreConstants.APP_PASS;

	public YouScoreAbstractBasicReq() {
		super();
	}

	public String getAppId() {
		return AppId;
	}

	public void setAppId(String appId) {
		AppId = appId;
	}

	public String getAppPass() {
		return AppPass;
	}

	public void setAppPass(String appPass) {
		AppPass = appPass;
	}

	public String createQueryString() throws IllegalArgumentException, IllegalAccessException {
		return createQueryStringOfObject(YouScoreAbstractBasicReq.class, this);
	}

	public static <T> String createQueryStringOfObject(Class<T> clazz, Object object)
			throws IllegalArgumentException, IllegalAccessException {
		String queryString = "";
		Field[] fields = clazz.getFields();
		for (Field field : fields) {
			Object valueObject = field.get(object);
			String valueString = valueObject == null ? "" : String.valueOf(valueObject);
			queryString += "&" + field.getName() + "=" + valueString;
		}

		return queryString;
	}

	@Override
	public String toString() {
		return "YouScoreAbstractBasicReq [AppId=" + AppId + ", AppPass=" + AppPass + "]";
	}
}
