package com.vedantu.lms.youscore.request;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class YouScoreStudentTeacherMappingReq extends YouScoreAbstractBasicReq {
	@SerializedName("MappingId")
	public List<YouScoreMappingEntry> MappingId = new ArrayList<YouScoreMappingEntry>();

	public YouScoreStudentTeacherMappingReq() {
		super();
	}

	public YouScoreStudentTeacherMappingReq(List<YouScoreMappingEntry> mappingId) {
		super();
		MappingId = mappingId;
	}

	public List<YouScoreMappingEntry> getMappingId() {
		return MappingId;
	}

	public void setMappingId(List<YouScoreMappingEntry> mappingId) {
		MappingId = mappingId;
	}

	public void addMapping(YouScoreMappingEntry youScoreMappingEntry) {
		MappingId.add(youScoreMappingEntry);
	}

	@Override
	public String toString() {
		return "YouScoreStudentTeacherMappingReq [MappingId=" + MappingId + "]";
	}
}