package com.vedantu.lms.youscore.request;

import com.google.gson.annotations.SerializedName;

public class YouScoreMappingEntry {
	@SerializedName("StudentMappingId")
	public String StudentMappingId;
	@SerializedName("TeacherMappingId")
	public String TeacherMappingId;

	public YouScoreMappingEntry() {
		super();
	}

	public YouScoreMappingEntry(String studentMappingId, String teacherMappingId) {
		super();
		StudentMappingId = studentMappingId;
		TeacherMappingId = teacherMappingId;
	}

	public YouScoreMappingEntry(Long studentMappingId, Long teacherMappingId) {
		super();
		StudentMappingId = String.valueOf(studentMappingId);
		TeacherMappingId = String.valueOf(teacherMappingId);
	}

	public String getStudentMappingId() {
		return StudentMappingId;
	}

	public void setStudentMappingId(String studentMappingId) {
		StudentMappingId = studentMappingId;
	}

	public String getTeacherMappingId() {
		return TeacherMappingId;
	}

	public void setTeacherMappingId(String teacherMappingId) {
		TeacherMappingId = teacherMappingId;
	}

	@Override
	public String toString() {
		return "YouScoreMappingEntry [StudentMappingId=" + StudentMappingId + ", TeacherMappingId=" + TeacherMappingId
				+ "]";
	}
}
