package com.vedantu.lms.youscore.pojo;

import com.google.gson.Gson;

public class YouScoreCookie {
	private String userId;
	private String userRole;
	private String token;
	private String expiryTime;

	public YouScoreCookie() {
		super();
	}

	public YouScoreCookie(String userId, String userRole, String token, String expiryTime) {
		super();
		this.userId = userId;
		this.userRole = userRole;
		this.token = token;
		this.expiryTime = expiryTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

	public String getJsonString() {
		return new Gson().toJson(this);
	}

	@Override
	public String toString() {
		return "YouScoreCookie [userId=" + userId + ", userRole=" + userRole + ", token=" + token + ", expiryTime="
				+ expiryTime + "]";
	}
}
