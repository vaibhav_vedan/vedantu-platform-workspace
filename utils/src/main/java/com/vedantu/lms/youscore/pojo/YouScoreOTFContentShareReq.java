package com.vedantu.lms.youscore.pojo;

import java.util.List;

import com.vedantu.User.UserBasicInfo;

public class YouScoreOTFContentShareReq {
	private List<UserBasicInfo> students;
	private List<UserBasicInfo> teachers;
	private List<String> examCodes;

	public YouScoreOTFContentShareReq() {
		super();
	}

	public YouScoreOTFContentShareReq(List<UserBasicInfo> students, List<UserBasicInfo> teachers,
			List<String> examCodes) {
		super();
		this.students = students;
		this.teachers = teachers;
		this.examCodes = examCodes;
	}

	public List<UserBasicInfo> getStudents() {
		return students;
	}

	public void setStudents(List<UserBasicInfo> students) {
		this.students = students;
	}

	public List<UserBasicInfo> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<UserBasicInfo> teachers) {
		this.teachers = teachers;
	}

	public List<String> getExamCodes() {
		return examCodes;
	}

	public void setExamCodes(List<String> examCodes) {
		this.examCodes = examCodes;
	}

	@Override
	public String toString() {
		return "YouScoreOTFContentShareReq [students=" + students + ", teachers=" + teachers + ", examCodes="
				+ examCodes + "]";
	}

}
