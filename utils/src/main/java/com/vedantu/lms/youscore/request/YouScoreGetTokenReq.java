package com.vedantu.lms.youscore.request;

import com.vedantu.User.Role;
import com.vedantu.lms.youscore.pojo.YouScoreConstants;

public class YouScoreGetTokenReq extends YouScoreAbstractBasicReq {
	public String EmailId;
	public String RoleId;

	public YouScoreGetTokenReq() {
		super();
	}

	public YouScoreGetTokenReq(String EmailId, String roleId) {
		super();
		this.EmailId = EmailId;
		this.RoleId = roleId;
	}

	public YouScoreGetTokenReq(String emailId, Role role) {
		this(emailId, fetchRoleId(role));
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getRoleId() {
		return RoleId;
	}

	public void setRoleId(String roleId) {
		RoleId = roleId;
	}

	public static String fetchRoleId(Role role) {
		return (Role.TEACHER.equals(role) ? YouScoreConstants.TEACHER_ROLE : YouScoreConstants.STUDENT_ROLE);
	}

	public String createQueryString() throws IllegalArgumentException, IllegalAccessException {
		return YouScoreAbstractBasicReq.createQueryStringOfObject(this.getClass(), this);
	}

	@Override
	public String toString() {
		return "YouScoreGetTokenReq [EmailId=" + EmailId + ", RoleId=" + RoleId + ", AppId=" + AppId + ", AppPass="
				+ AppPass + "]";
	}
}
