package com.vedantu.lms.youscore.request;

public class YouScoreTestUrlReq {
	public String Token;
	public String expiryTime;
	public String MappingId;
	public String ExamCode;
	public String Ref;

	public YouScoreTestUrlReq() {
		super();
	}

	public YouScoreTestUrlReq(String token, String expiryTime, String studentMappingId, String examCode, String ref) {
		super();
		this.Token = token;
		this.expiryTime = expiryTime;
		this.MappingId = studentMappingId;
		this.ExamCode = examCode;
		this.Ref = ref;
	}

	public String getExamCode() {
		return ExamCode;
	}

	public void setExamCode(String examCode) {
		ExamCode = examCode;
	}

	public String getToken() {
		return Token;
	}

	public void setToken(String token) {
		Token = token;
	}

	public String getMappingId() {
		return MappingId;
	}

	public void setMappingId(String mappingId) {
		MappingId = mappingId;
	}

	public String getRef() {
		return Ref;
	}

	public void setRef(String ref) {
		Ref = ref;
	}

	public String getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

	public String createQueryString() throws IllegalArgumentException, IllegalAccessException {
		return YouScoreAbstractBasicReq.createQueryStringOfObject(this.getClass(), this);
	}

	@Override
	public String toString() {
		return "YouScoreTestUrlReq [Token=" + Token + ", expiryTime=" + expiryTime + ", StudentMappingId=" + MappingId
				+ ", ExamCode=" + ExamCode + ", Ref=" + Ref + "]";
	}
}
