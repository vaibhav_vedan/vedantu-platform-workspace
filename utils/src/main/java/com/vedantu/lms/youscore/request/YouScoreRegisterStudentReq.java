package com.vedantu.lms.youscore.request;

import com.vedantu.lms.youscore.pojo.YouScoreConstants;

public class YouScoreRegisterStudentReq extends YouScoreAbstractBasicReq {
	public String EmailId;
	public String UserName;
	public String MobileNumber;
	public String Gender = "true";
	public String points = YouScoreConstants.YOUSCORE_DEFAULT_POINTS;
	public String city = "delhi";
	public String classId = "";
	public String streamId = "CBSE";
	public String packageId = YouScoreConstants.YOUSCORE_DEFAULT_PACKAGE;
	public String mappingId = "";

	public YouScoreRegisterStudentReq() {
		super();
	}

	public YouScoreRegisterStudentReq(String emailId, String userName, String fullName) {
		super();
		this.EmailId = emailId;
		this.UserName = createValidFullName(fullName);
		this.mappingId = userName;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public String getMappingId() {
		return mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

	public String createQueryString() throws IllegalArgumentException, IllegalAccessException {
		return YouScoreAbstractBasicReq.createQueryStringOfObject(this.getClass(), this);
	}

	public static String createValidFullName(String fullName) {
		fullName = fullName.trim();
		return fullName.replaceAll(" ", "_");
	}

	@Override
	public String toString() {
		return "YouScoreRegisterStudentReq [EmailId=" + EmailId + ", UserName=" + UserName + ", MobileNumber="
				+ MobileNumber + ", Gender=" + Gender + ", points=" + points + ", city=" + city + ", classId=" + classId
				+ ", streamId=" + streamId + ", packageId=" + packageId + ", mappingId=" + mappingId + ", AppId="
				+ AppId + ", AppPass=" + AppPass + "]";
	}
}
