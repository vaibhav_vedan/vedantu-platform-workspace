package com.vedantu.lms.youscore.pojo;

import com.vedantu.util.DateTimeUtils;

public class YouScoreConstants {

	public static final String youScoreBaseUrl = "http://api.scobotic.com/";
	public static final String youScoreGetTokenUrl = youScoreBaseUrl + "api/User/GetToken";
	public static final String youScoreRegisterTokenUrl = youScoreBaseUrl + "api/User/RegisterUser";
	public static final String youScoreRegisterTeacherTokenUrl = youScoreBaseUrl + "api/User/RegisterTeacher";
	public static final String youScoreStudentTeacherMappingUrl = youScoreBaseUrl + "api/User/AddStudentTeacherMapping";
	public static final String youScorePackagePaymentUrl = youScoreBaseUrl + "api/User/PackagePayment";

	public static final String youScoreTestUrl = "http://m.scobotic.com/tokenauth.aspx";
	public static final String YOUSCORE_TEACHER_PORTAL_URL = "http://monitor.scobotic.com/Account/tokenauth?Token=%s&MappingId=%s&expiryTime=%s";
	public static String youScoreContainerUrl;

	public static final String APP_ID = "vedantuzxz";
	public static final String APP_PASS = "zxcxzc";

	public static final String STUDENT_ROLE = "0";
	public static final String TEACHER_ROLE = "1";

	public static final String EMAIL_ID_SUFFIX = "@vedantu.com";
	public static final int expiryDuration = DateTimeUtils.MILLIS_PER_DAY;

	public static final String YOUSCORE_COOKIE = "youScore_vedantu";

	public static final String YOUSCORE_DEFAULT_PACKAGE = "129";
	public static final String YOUSCORE_DEFAULT_POINTS = "100000";

}
