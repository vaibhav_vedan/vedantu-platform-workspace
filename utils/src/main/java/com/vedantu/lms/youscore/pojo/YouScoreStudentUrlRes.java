package com.vedantu.lms.youscore.pojo;

public class YouScoreStudentUrlRes {
	private String testUrl;
	private YouScoreCookie youScoreCookie;

	public YouScoreStudentUrlRes() {
		super();
	}

	public YouScoreStudentUrlRes(String testUrl, YouScoreCookie youScoreCookie) {
		super();
		this.testUrl = testUrl;
		this.youScoreCookie = youScoreCookie;
	}

	public String getTestUrl() {
		return testUrl;
	}

	public void setTestUrl(String testUrl) {
		this.testUrl = testUrl;
	}

	public YouScoreCookie getYouScoreCookie() {
		return youScoreCookie;
	}

	public void setYouScoreCookie(YouScoreCookie youScoreCookie) {
		this.youScoreCookie = youScoreCookie;
	}

	@Override
	public String toString() {
		return "YouScoreStudentUrlRes [testUrl=" + testUrl + ", youScoreCookie=" + youScoreCookie + ", toString()="
				+ super.toString() + "]";
	}
}
