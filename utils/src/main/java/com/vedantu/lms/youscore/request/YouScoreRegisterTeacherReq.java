package com.vedantu.lms.youscore.request;

public class YouScoreRegisterTeacherReq extends YouScoreAbstractBasicReq {
	public String EmailId;
	public String Password;
	public String UserName;
	public String MobileNumber = "0123456789";
	public String Gender = "true";
	public String Location = "delhi";
	public String MappingId;

	public YouScoreRegisterTeacherReq() {
		super();
	}

	public YouScoreRegisterTeacherReq(String emailId, String userName, String fullName) {
		super();
		this.EmailId = emailId;
		this.Password = userName;
		this.UserName = createValidFullName(fullName);
		this.MappingId = userName;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getMappingId() {
		return MappingId;
	}

	public void setMappingId(String mappingId) {
		MappingId = mappingId;
	}

	public String createQueryString() throws IllegalArgumentException, IllegalAccessException {
		return YouScoreAbstractBasicReq.createQueryStringOfObject(this.getClass(), this);
	}

	public static String createValidFullName(String fullName) {
		fullName = fullName.trim();
		return fullName.replaceAll(" ", "_");
	}

	@Override
	public String toString() {
		return "YouScoreRegisterTeacherReq [EmailId=" + EmailId + ", Password=" + Password + ", UserName=" + UserName
				+ ", MobileNumber=" + MobileNumber + ", Gender=" + Gender + ", Location=" + Location + ", MappingId="
				+ MappingId + "]";
	}
}
