package com.vedantu.lms.youscore.request;

import com.vedantu.lms.youscore.pojo.YouScoreConstants;

public class YouScorePackagePaymentReq extends YouScoreAbstractBasicReq {
	public String EmailId;
	public String PackageId = YouScoreConstants.YOUSCORE_DEFAULT_PACKAGE;
	public String PaymentApproved = "true";

	public YouScorePackagePaymentReq() {
		super();
	}

	public YouScorePackagePaymentReq(String emailId) {
		super();
		EmailId = emailId;
	}

	public YouScorePackagePaymentReq(String emailId, String packageId, String paymentApproved) {
		super();
		EmailId = emailId;
		PackageId = packageId;
		PaymentApproved = paymentApproved;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getPackageId() {
		return PackageId;
	}

	public void setPackageId(String packageId) {
		PackageId = packageId;
	}

	public String getPaymentApproved() {
		return PaymentApproved;
	}

	public void setPaymentApproved(String paymentApproved) {
		PaymentApproved = paymentApproved;
	}

	public String createQueryString() throws IllegalArgumentException, IllegalAccessException {
		return YouScoreAbstractBasicReq.createQueryStringOfObject(this.getClass(), this);
	}

	@Override
	public String toString() {
		return "YouScorePackagePaymentReq [EmailId=" + EmailId + ", PackageId=" + PackageId + ", PaymentApproved="
				+ PaymentApproved + "]";
	}
}
