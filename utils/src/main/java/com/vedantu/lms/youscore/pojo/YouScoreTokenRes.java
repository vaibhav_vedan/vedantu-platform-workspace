package com.vedantu.lms.youscore.pojo;

public class YouScoreTokenRes {
	private String token;
	private String expiryTime;

	public YouScoreTokenRes() {
		super();
	}

	public YouScoreTokenRes(String token, String expiryTime) {
		super();
		this.token = token;
		this.expiryTime = expiryTime;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Override
	public String toString() {
		return "YouScoreTokenRes [token=" + token + ", expiryTime=" + expiryTime + "]";
	}
}
