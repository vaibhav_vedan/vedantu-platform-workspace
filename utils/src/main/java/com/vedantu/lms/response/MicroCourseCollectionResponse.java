package com.vedantu.lms.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MicroCourseCollectionResponse
{
    private String displayTag;
    private Long size;
    private String thumbnail;
}
