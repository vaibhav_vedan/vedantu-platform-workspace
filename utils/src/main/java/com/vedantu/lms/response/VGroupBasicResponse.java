package com.vedantu.lms.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VGroupBasicResponse
{
    private String response;
    private HttpStatus status;
}
