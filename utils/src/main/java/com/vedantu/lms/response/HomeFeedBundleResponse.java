package com.vedantu.lms.response;

import com.vedantu.subscription.pojo.bundle.AioPackage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedBundleResponse
{
    private String id;
    private String title;
    private List<String> teacherIds;
    private String teacherName;
    private String bundleImageUrl;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<AioPackage> packages;
    private boolean bought;
    private List<String> subjects;
    private String subject;
}
