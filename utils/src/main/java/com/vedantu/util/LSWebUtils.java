package com.vedantu.util;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.leadsquared.LeadsquaredHelper;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LSWebUtils {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AbstractRedisDAO abstractRedisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LSWebUtils.class);

    public ClientResponse leadsquaredDoBulkCallWithRetries(String serverUrl, HttpMethod requestType, String query, Boolean auth) throws VException {
        ClientResponse clientResponse = null;
        try {

            Integer availableQuota;
            if (serverUrl.contains("LeadManagement.svc/Lead/Bulk/CreateOrUpdate") ||
                    serverUrl.contains("ProspectActivity.svc/Bulk/CustomActivity/Add/ByLeadId")) {
                availableQuota = LeadsquaredHelper.getQuotaForBulkLeadUpsertAPI();

            } else {
                boolean isBulkUpsertProcessing = false;

                String bulkLeadPushProcessCount = abstractRedisDAO.get(LeadsquaredHelper.BULK_LEAD_PUSH_PROCESS_COUNT);

                if (StringUtils.isNotEmpty(bulkLeadPushProcessCount)) {
                    isBulkUpsertProcessing = true;
                }
                availableQuota = LeadsquaredHelper.getQuotaForBulkAPI(isBulkUpsertProcessing);
            }

            int retryCount = 1;
            logger.info("Inside leadsquaredDoBulkCallWithRetries, for url: {} , threshold count: {}", serverUrl, availableQuota);
            while (true) {

                String lsBulkProcessingKeyValue = null;

                String timeTracker = abstractRedisDAO.get(LeadsquaredHelper.BULK_LS_API_PROCESSING_TIME_TRACKER);
                if(StringUtils.isEmpty(timeTracker)){
                    logger.info("bulk timeTracker new slot starts");
                    abstractRedisDAO.del(LeadsquaredHelper.BULK_LS_API_PROCESSING_COUNT);
                    abstractRedisDAO.setex(LeadsquaredHelper.BULK_LS_API_PROCESSING_TIME_TRACKER, "TRUE", 5);
                }else {
                    lsBulkProcessingKeyValue = abstractRedisDAO.get(LeadsquaredHelper.BULK_LS_API_PROCESSING_COUNT);
                    logger.info("bulk usage quota {}", lsBulkProcessingKeyValue);
                }

                if (StringUtils.isEmpty(lsBulkProcessingKeyValue)) {
                    abstractRedisDAO.setex(LeadsquaredHelper.BULK_LS_API_PROCESSING_COUNT, "1", 5);
                    break;
                } else if (Integer.parseInt(lsBulkProcessingKeyValue) < availableQuota) {
                    Integer count = Integer.parseInt(lsBulkProcessingKeyValue);
                    abstractRedisDAO.setex(LeadsquaredHelper.BULK_LS_API_PROCESSING_COUNT, String.valueOf(count+1), 5);
                    break;
                }
                if (retryCount % 30 == 0) {
                    logger.error("waiting from {} second(s) for api call : {}", retryCount, serverUrl);
                }
                retryCount++;
                logger.info("Inside leadsquaredDoBulkCallWithRetries, for url: {} , sleeping for one sec", serverUrl);
                Thread.sleep(1000);
            }

        } catch (Exception e) {
            logger.error("Error while checking the bulk quota, proceeding with api call", e);
        }
        if (auth) {
            clientResponse = WebUtils.INSTANCE.doCall(serverUrl, requestType, query);
        } else {
            clientResponse = WebUtils.INSTANCE.doCall(serverUrl, requestType, query, auth);
        }
        return clientResponse;
    }

}
