package com.vedantu.util.helper;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.LogFactory;
import com.vedantu.util.pojo.StatusChangeHistory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

public class StatusChangeHistoryManageHelper {

    @Autowired
    private static LogFactory logFactory;

    private static Logger logger = LogFactory.getLogger(StatusChangeHistoryManageHelper.class);

    /*
    NOTE: null value for stateChangeHistoryList cannot be considered as reference
     */
    public static void updateStateChangeHistory(List<StatusChangeHistory> stateChangeHistoryList, String currentStatus, String newStatus, Long userId) {

        try {
            currentStatus = currentStatus != null ? currentStatus : "";
            newStatus = newStatus != null ? newStatus : "";

            logger.info("=====currentStatus : {}", currentStatus);
            logger.info("=====newStatus : {}", newStatus);
            logger.info("=====userId : {}", userId);

            if (!currentStatus.equalsIgnoreCase(newStatus)) {
                StatusChangeHistory statusChangeHistory = new StatusChangeHistory.Builder()
                        .prevState(currentStatus)
                        .newStatus(newStatus)
                        .setTimestamp(System.currentTimeMillis())
                        .build();

                if(userId != null)
                    statusChangeHistory.setUpdatedBy(userId);

                logger.info("=====Status history : {}", statusChangeHistory);

                stateChangeHistoryList.add(statusChangeHistory);

                logger.info("=====Status history list: {}", stateChangeHistoryList);

            }
        }catch (Exception e){
            logger.error("=====Error setting status history", e);
        }

    }

}
