/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class ArrayUtils {
    
    public static <E extends Object> List<E> toList(E[] array) {
        
        return null != array ? Arrays.asList(array) : new ArrayList<>();
    }
    
    public static <E extends Object> boolean isEmpty(List<E> list) {
        return null == list || list.isEmpty();
    }
    
    public static <E extends Object> boolean isNotEmpty(List<E> list) {
        return !isEmpty(list);
    }
    
    public static <E extends Object> boolean isEmpty(Set<E> list) {
        return null == list || list.isEmpty();
    }
    
    public static <E extends Object> boolean isNotEmpty(Set<E> list) {
        return !isEmpty(list);
    }    
}
