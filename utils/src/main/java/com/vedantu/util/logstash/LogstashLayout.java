package com.vedantu.util.logstash;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;
import org.apache.logging.log4j.core.util.KeyValuePair;

/**
 *
 * @author somil
 */
@Plugin(name = "LogstashLayout", category = "Core", elementType = "layout", printObject = true)
public class LogstashLayout extends AbstractStringLayout {
    
    private static Random random = new Random();

    private static final Map<String, String> additionalLogAttributes = new HashMap<String, String>();
    public static final TimeZone UTC = TimeZone.getTimeZone("UTC");
    public static final FastDateFormat ISO_DATETIME_TIME_ZONE_FORMAT_WITH_MILLIS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", UTC);
    private static Integer version = 1;
    //private static String hostName;
    private static Gson gson = new Gson();
    private static String environment;
    private static String application;

    public static String dateFormat(long timestamp) {
        return ISO_DATETIME_TIME_ZONE_FORMAT_WITH_MILLIS.format(timestamp);
    }

    protected LogstashLayout(boolean locationInfo, boolean properties, boolean complete, Charset charset, Map<String, String> additionalAttributes) {
        super(charset);
        additionalLogAttributes.putAll(additionalAttributes);
//        try {
//            hostName = java.net.InetAddress.getLocalHost().getHostName();
//        } catch (UnknownHostException e) {
//            hostName = "unknown-host";
//        }
        environment = ConfigUtils.INSTANCE.getStringValue("environment");
        application = ConfigUtils.INSTANCE.getStringValue("application.name");
    }

    @PluginFactory
    public static LogstashLayout createLayout(@PluginAttribute("locationInfo") boolean locationInfo,
            @PluginAttribute("properties") boolean properties,
            @PluginAttribute("complete") boolean complete,
            @PluginAttribute(value = "charset", defaultString = "UTF-8") Charset charset,
            @PluginElement("Pairs") final KeyValuePair[] pairs) {
        final Map<String, String> additionalLogAttributes = new HashMap<String, String>();
        if (pairs != null && pairs.length > 0) {
            for (final KeyValuePair pair : pairs) {
                final String key = pair.getKey();
                if (key == null) {
                    LOGGER.error("A null key is not valid in MapFilter");
                    continue;
                }
                final String value = pair.getValue();
                if (value == null) {
                    LOGGER.error("A null value for key " + key + " is not allowed in MapFilter");
                    continue;
                }
                if (additionalLogAttributes.containsKey(key)) {
                    LOGGER.error("Duplicate entry for key: {} is forbidden!", key);
                }
                additionalLogAttributes.put(key, value);
            }
        }

        return new LogstashLayout(locationInfo, properties, complete, charset, additionalLogAttributes);
    }

    @Override
    public String toSerializable(LogEvent le) {
        return format(le);
    }

    public String format(LogEvent loggingEvent) {
        try {
            JsonObject logstashEvent = new JsonObject();

            Long timestamp = loggingEvent.getTimeMillis();

            logstashEvent.addProperty("@version", version);
            logstashEvent.addProperty("@timestamp", dateFormat(timestamp));

            //logstashEvent.addProperty("source_host", hostName);
            if (loggingEvent.getMessage() != null) {
                logstashEvent.addProperty("message", loggingEvent.getMessage().getFormattedMessage());
            }

            Throwable throwable = loggingEvent.getThrown();

            if (throwable != null) {
                Map<String, Object> exceptionInformation = new HashMap<String, Object>();
                if (throwable.getClass() != null && throwable.getClass().getCanonicalName() != null) {
                    exceptionInformation.put("exception_class", throwable.getClass().getCanonicalName());
                }
                if (throwable.getMessage() != null) {
                    exceptionInformation.put("exception_message", throwable.getMessage());
                }
                ;
                if (throwable.getStackTrace() != null) {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    throwable.printStackTrace(pw);
                    exceptionInformation.put("stacktrace", sw.toString());
                }
                JsonElement exception = gson.toJsonTree(exceptionInformation);
                logstashEvent.add("exception", exception);
            }

            StackTraceElement locationInfo = loggingEvent.getSource();
            if (locationInfo != null) {
                logstashEvent.addProperty("file", locationInfo.getFileName());
                logstashEvent.addProperty("line_number", locationInfo.getLineNumber());
                logstashEvent.addProperty("class", locationInfo.getClassName());
                logstashEvent.addProperty("method", locationInfo.getMethodName());
            }

            logstashEvent.addProperty("logger_name", loggingEvent.getLoggerName());
            try {
                if (loggingEvent.getContextData() != null) {
                    Map<String, String> mdc = loggingEvent.getContextData().toMap();
                    JsonElement mdcMap = gson.toJsonTree(mdc);
                    logstashEvent.add("mdc", mdcMap);
                }
            } catch (Exception ex) {
                LOGGER.error(ex);
            }
            addFromMDC(logstashEvent, "correlation-id");
            addFromMDC(logstashEvent, "callingUserId");
            try {
                if (loggingEvent.getContextStack() != null) {
                    List<String> ndc = loggingEvent.getContextStack().asList();
                    JsonElement ndcList = gson.toJsonTree(ndc);
                    logstashEvent.add("ndc", ndcList);
                }
            } catch (Exception ex) {
                LOGGER.error(ex);
            }
            if (loggingEvent.getLevel() != null) {
                logstashEvent.addProperty("level", loggingEvent.getLevel().toString());
            }
            logstashEvent.addProperty("thread_name", loggingEvent.getThreadName());
            logstashEvent.addProperty("thread_id", loggingEvent.getThreadId());
            logstashEvent.addProperty("environment", environment);
            logstashEvent.addProperty("application", application);

            //return logstashEvent.toString() + "\n";
            return logstashEvent.toString() + "\n";
        } catch (Exception ex) {
            LOGGER.error(ex);
            return gson.toJson(loggingEvent);
        }
    }

    private void addFromMDC(JsonObject logstashEvent, String key) {
        String correlationId = null;
        Object correlationIdObject = ThreadContext.get(key);
        if (correlationIdObject != null) {
            correlationId = correlationIdObject.toString();
        }

        if (StringUtils.isNotEmpty(correlationId)) {
            logstashEvent.addProperty(key, correlationId);
        }
    }
    
    public static void addToMDC(HttpServletRequest request) {
        handleCorrelationId(request);
        handleCallingUserId(request);
    }
    
    
    private static void handleCorrelationId(HttpServletRequest request) {
        try {
            String correlationId = request.getHeader("X-Correlation-Id");
            if (StringUtils.isEmpty(correlationId)) {
                correlationId = generateId();
            }
            ThreadContext.put("correlation-id", correlationId);
            //request.setAttribute("correlation-id", correlationId);
        } catch (Exception ex) {
            LOGGER.error("Error in handleCorrelationId", ex);
        }
    }

    private static void handleCallingUserId(HttpServletRequest request) {
        try {
            String callingUserId = request.getHeader("X-Caller-Id");
            if (StringUtils.isNotEmpty(callingUserId)) {
                ThreadContext.put("callingUserId", callingUserId);
                //request.setAttribute("callingUserId", callingUserId);
            }

        } catch (Exception ex) {
            LOGGER.error("Error in handleCallingUserId", ex);
        }
    }

    private static String generateId() {
        return UUID.randomUUID().toString();
    }


}
