package com.vedantu.util.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.managers.AdminTagManager;
import com.vedantu.util.request.AdminTagAddOrRemoveReq;
import com.vedantu.util.pojo.CollectionName;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/admin")
public class AdminTagController {

    @Autowired
    private AdminTagManager adminTagManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/addAdminTags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addAdminTags(@RequestBody AdminTagAddOrRemoveReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Collections.singletonList(Role.ADMIN), false);
        return adminTagManager.addAdminTags(req.getAdminTags(), req.isRemoveTags(), req.getCollectionName());
    }

    @RequestMapping(value = "/get/collection/names", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CollectionName> getCollectionNames() throws VException {
        sessionUtils.checkIfAllowedList(null, Collections.singletonList(Role.ADMIN), false);
        return adminTagManager.getCollectionNames();
    }

    @RequestMapping(value = "/credential/encrypt/keys", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse encryptKeys(@RequestBody LinkedHashMap<String, String> keyVal) throws VException {
        sessionUtils.checkIfAllowedList(null, Collections.singletonList(Role.ADMIN), false);
        return adminTagManager.encryptKeys(keyVal);
    }

    @RequestMapping(value = "/credential/encrypt/key", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse encryptKey(@RequestParam(value = "key") String key,
                                                     @RequestParam(value = "val") String value) throws VException {
        sessionUtils.checkIfAllowedList(null, Collections.singletonList(Role.ADMIN), false);
        return adminTagManager.encryptKey(key, value);
    }

    @RequestMapping(value = "/credential/get/keys", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getKeys() throws VException {
        sessionUtils.checkIfAllowedList(null, Collections.singletonList(Role.ADMIN), false);
        return adminTagManager.checkProperties();
    }

}
