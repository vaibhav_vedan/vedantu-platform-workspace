package com.vedantu.util.response;

import com.vedantu.util.enums.CommunicationKind;
import com.vedantu.util.request.CommunicationDataReq;
import com.vedantu.User.UserBasicInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommunicationDataRes {
	private List<String> userIds;
	private List<String> batchIds;
	private List<String> otmBundleIds;
	private List<String> aioPackageIds;
	private List<String> emailIds;
	private CommunicationKind communicationKind;
	private String communicationBody;
	private String communicationSubject;

	// This will be used to send all the emails to respective users
	private List<UserBasicInfo> allReceiversUserId;

	public CommunicationDataRes(CommunicationDataReq communicationDataReq,List<UserBasicInfo> allReceiversUserId) {
		this.userIds=communicationDataReq.getUserIds();
		this.batchIds=communicationDataReq.getBatchIds();
		this.otmBundleIds=communicationDataReq.getOtmBundleIds();
		this.aioPackageIds=communicationDataReq.getAioPackageIds();
		this.communicationKind =communicationDataReq.getCommunicationKind();
		this.communicationBody=communicationDataReq.getBody();
		this.communicationSubject=communicationDataReq.getSubject();
		this.emailIds=communicationDataReq.getEmailIds();
		this.allReceiversUserId = allReceiversUserId;
	}
}
