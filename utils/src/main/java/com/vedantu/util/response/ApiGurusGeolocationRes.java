package com.vedantu.util.response;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author ravneet
 */
public class ApiGurusGeolocationRes extends AbstractRes{
    String postal_code;
    String city;
    String country_name;
    String region_name;
    String isp;

    public ApiGurusGeolocationRes(String postal_code, String city, String country_name, String region_name) {
        this.postal_code = postal_code;
        this.city = city;
        this.country_name = country_name;
        this.region_name = region_name;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }
}
