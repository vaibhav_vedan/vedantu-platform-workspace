package com.vedantu.util.response;

import com.vedantu.util.fos.response.AbstractRes;

public class FOSAgentRes extends AbstractRes {
    private String id;
    private String agentEmailId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentEmailId() {
        return agentEmailId;
    }

    public void setAgentEmailId(String agentEmailId) {
        this.agentEmailId = agentEmailId;
    }

}
