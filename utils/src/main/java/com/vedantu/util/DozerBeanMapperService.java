/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

import java.util.Arrays;
import java.util.List;
import javax.annotation.PreDestroy;
import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class DozerBeanMapperService {

    private DozerBeanMapper dozerBean;

    @Bean
    public DozerBeanMapper dozerBeanMapper() {
        System.out.println("creating dozerBeanMapper");
        List<String> mappingFiles = Arrays.asList(
                "dozerBeanMapping.xml"
        );

        dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);
        return dozerBean;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (dozerBean != null) {
                dozerBean.destroy();
            }
        } catch (Exception e) {
            System.err.println("Error in destroying bean mapper " + e.getMessage());
        }
    }
}
