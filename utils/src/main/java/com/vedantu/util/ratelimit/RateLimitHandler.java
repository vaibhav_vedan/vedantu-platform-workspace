package com.vedantu.util.ratelimit;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.vedantu.exception.*;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.redis.RateLimitingRedisDao;
import com.vedantu.util.redis.UtilsRedisDao;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public class RateLimitHandler {

    @Autowired
    private static LogFactory logFactory;

    @SuppressWarnings("static-access")
    private static Logger logger = logFactory.getLogger(RateLimitHandler.class);

    @Autowired
    private UtilsRedisDao utilsRedisDao;

    @Autowired
    private RateLimitingRedisDao rateLimitingRedisDao;

    static String SCRIPT_LIMIT_PER_PERIOD = ""
            + "local notexists = redis.call(\"set\", KEYS[1], 1, \"NX\", \"EX\", tonumber(ARGV[2])) \n"
            + "if (notexists) then \n"
            + "  return 1 \n"
            + "end \n"
            + "local current = tonumber(redis.call(\"get\", KEYS[1])) \n"
            + "if (current == nil) then \n"
            + "  local result = redis.call(\"incr\", KEYS[1]) \n"
            + "  redis.call(\"expire\", KEYS[1], tonumber(ARGV[2]) \n)"
            + "  return result \n"
            + "end \n"
            + "if (current >= tonumber(ARGV[1])) then \n"
            + "  error(\"too many requests\") \n"
            + "end \n"
            + "local result = redis.call(\"incr\", KEYS[1]) \n"
            + "return result \n";

    public void handleAuthRequestLimiting(Long userId, Map<String, Object> requestDetails, String ipAddress) throws RateLimitException {

        String newconfig = ConfigUtils.INSTANCE.getStringValue("rl.config");
        String baseConfig = ConfigUtils.INSTANCE.getStringValue("rl.baseConfig");

        Gson g = new Gson();
        java.lang.reflect.Type type = new TypeToken<HashMap<String, HashMap<String, String>>>() {
        }.getType();
        HashMap<String, HashMap<String, String>> rlConfig = g.fromJson(newconfig, type);

        java.lang.reflect.Type bType = new TypeToken<HashMap<String, String>>() {}.getType();
        HashMap<String, String> baseRLConfig = g.fromJson(baseConfig, bType);
        
        String endpoint = (String) requestDetails.get("requestURI");

        // Direct IPAddress limitation
        /*try {
            utilsRedisDao.loadScriptAndEval(SCRIPT_LIMIT_PER_PERIOD, "RL:" + ipAddress, baseRLConfig.get("requests"), baseRLConfig.get("time"));
        } catch (InternalServerErrorException e) {
            // TODO Auto-generated catch block
        } catch (RateLimitException je) {
//            logger.error("USER BLOCKED: " + ipAddress + "Error: " + je.getMessage());
            throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");
        }*/

        // Handled only the ones that have the config params
        if (rlConfig.containsKey(endpoint)) {
            HashMap<String, String> config = rlConfig.get(endpoint);
            String key;
            if (userId == null) {
                // User not logged in requests
                key = "RL:" + ipAddress + ":" + endpoint;
                try {
                    rateLimitingRedisDao.loadScriptAndEval(SCRIPT_LIMIT_PER_PERIOD, key, config.get("requests"), config.get("time"));
                } catch (InternalServerErrorException e) {
                    logger.info("InternalServerException");
                } catch (RateLimitException je) {
                    //logger.error("USER BLOCKED: " + key + "Error: " + je.getMessage());
                    throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");
                }
            } else {
                // User logged in requests
                key = "RL:" + userId.toString() + ":" + endpoint;
                try {
                    rateLimitingRedisDao.loadScriptAndEval(SCRIPT_LIMIT_PER_PERIOD, key, config.get("requests"), config.get("time"));
                } catch (InternalServerErrorException e) {
                    logger.info("InternalServerException");
                } catch (RateLimitException je) {
                    //logger.error("USER BLOCKED: " + key + "Error: " + je.getMessage());
                    throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");
                }
            }
        }
    }

    /**
     *
     * @param key key to stored in redis
     * @param period
     * @param unit
     * @return
     */
    public long rateLimitCount(String key, int period, TimeUnit unit)  {
        if (period <= 0) {
            throw new VRuntimeException(ErrorCode.RATE_LIMIT_PERIOD_NOT_VALID, "Rate limit period cannot be less" +
                    " than or equal to zero - " + period);
        }
        LocalDateTime now = LocalDateTime.now();
        long currentTimeMillis = System.currentTimeMillis();
        try {
            switch (unit) {
                case SECONDS: {
                    if (period >= 60) {
                        throw new VRuntimeException(ErrorCode.RATE_LIMIT_PERIOD_NOT_VALID, "Cannot use period >=60 for seconds");
                    }
                    int second = now.getSecond();
                    int expireSecond = second / period;
                    int remainingSecond = second % period;
                    int remainingMillis = remainingSecond * DateTimeUtils.MILLIS_PER_SECOND;
                    String keyWithPeriod = key + ":" + expireSecond;
                    return utilsRedisDao.incrAndExpireAt(keyWithPeriod, currentTimeMillis + remainingMillis);
                }
                case MINUTES:{
                    if (period >= 60) {
                        throw new VRuntimeException(ErrorCode.RATE_LIMIT_PERIOD_NOT_VALID, "Cannot use period >=60 for minutes");
                    }
                    int minute = now.getMinute();
                    int expireMinute = minute / period;
                    int remainingMinute = minute % period;
                    int remainingMillis = remainingMinute * DateTimeUtils.MILLIS_PER_MINUTE;
                    String keyWithPeriod = key + ":" + expireMinute;
                    return utilsRedisDao.incrAndExpireAt(keyWithPeriod, currentTimeMillis + remainingMillis);
                }
                case HOURS: {
                    if (period >= 24) {
                        throw new VRuntimeException(ErrorCode.RATE_LIMIT_PERIOD_NOT_VALID, "Cannot use period >=24 for hours");
                    }
                    int hour = now.getHour();
                    int expireHour = hour / period;
                    int remainingHour = hour % period;
                    int remainingMillis = remainingHour * DateTimeUtils.MILLIS_PER_HOUR;
                    String keyWithPeriod = key + ":" + expireHour;
                    return utilsRedisDao.incrAndExpireAt(keyWithPeriod, currentTimeMillis +remainingMillis);
                }
                case DAYS:  {
                    if (period >= 365) {
                        throw new VRuntimeException(ErrorCode.RATE_LIMIT_PERIOD_NOT_VALID, "Cannot use period >=365 for days");
                    }
                    int day = now.getDayOfYear();
                    int expireDay = day / period;
                    int remainingDay = day % period;
                    int remainingMillis = remainingDay * DateTimeUtils.MILLIS_PER_DAY;
                    String keyWithPeriod = key + ":" + expireDay;
                    return utilsRedisDao.incrAndExpireAt(keyWithPeriod, currentTimeMillis + remainingMillis);
                }
                default:
                    throw new VRuntimeException(ErrorCode.RATE_LIMIT_TIME_UNIT_NOT_ALLOWED, "CAN'T USE THIS TIME UNIT FOR RATE LIMITING - " + unit);
            }
        } catch (VException e) {
            throw new VRuntimeException(e.getErrorCode(), e.getErrorMessage());
        }
    }
}
