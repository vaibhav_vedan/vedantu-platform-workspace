package com.vedantu.util;

import java.util.List;

public class ListUtils {
	public static String join(List<String> inputs, String separator) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < inputs.size(); i++) {

			builder.append(inputs.get(i));
			if (inputs.size() - 1 != i) {
				builder.append(separator);
			}

		}
		return builder.toString();
	}
}
