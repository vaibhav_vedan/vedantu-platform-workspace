/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

/**
 *
 * @author Darshit
 */
public class AuditHistoryRes  {

    String auditEntityType;
    String auditEntityId;
    Object entityObject;

    public AuditHistoryRes(String auditEntityType, Object entityObject) {
        this.auditEntityType = auditEntityType;
        this.entityObject = entityObject;
    }

    public String getAuditEntityType() {
        return auditEntityType;
    }

    public void setAuditEntityType(String auditEntityType) {
        this.auditEntityType = auditEntityType;
    }

    public String getAuditEntityId() {
        return auditEntityId;
    }

    public void setAuditEntityId(String auditEntityId) {
        this.auditEntityId = auditEntityId;
    }

    public Object getEntityObject() {
        return entityObject;
    }

    public void setEntityObject(Object entityObject) {
        this.entityObject = entityObject;
    }
}
