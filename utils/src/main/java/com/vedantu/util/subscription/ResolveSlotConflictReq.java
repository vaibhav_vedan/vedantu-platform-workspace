package com.vedantu.util.subscription;

import java.util.Arrays;
import java.util.List;

public class ResolveSlotConflictReq {
	private Long startTime;
	private Long noOfWeeks;
	private String referenceId;
	private String studentId;
	private String teacherId;
	private long[] bitSetLongArray;
	private long[] conflictSlotBits;
	private List<SessionConflict> sessionConflict;

	public ResolveSlotConflictReq() {
		super();
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getNoOfWeeks() {
		return noOfWeeks;
	}

	public void setNoOfWeeks(Long noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public long[] getConflictSlotBits() {
		return conflictSlotBits;
	}

	public void setConflictSlotBits(long[] conflictSlotBits) {
		this.conflictSlotBits = conflictSlotBits;
	}

	public long[] getBitSetLongArray() {
		return bitSetLongArray;
	}

	public void setBitSetLongArray(long[] bitSetLongArray) {
		this.bitSetLongArray = bitSetLongArray;
	}

	public List<SessionConflict> getSessionConflict() {
		return sessionConflict;
	}

	public void setSessionConflict(List<SessionConflict> sessionConflict) {
		this.sessionConflict = sessionConflict;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "ResolveSlotConflictReq [startTime=" + startTime + ", noOfWeeks=" + noOfWeeks + ", referenceId="
				+ referenceId + ", studentId=" + studentId + ", teacherId=" + teacherId + ", bitSetLongArray="
				+ Arrays.toString(bitSetLongArray) + ", conflictSlotBits=" + Arrays.toString(conflictSlotBits)
				+ ", sessionConflict=" + sessionConflict + "]";
	}
}
