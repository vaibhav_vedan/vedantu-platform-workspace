package com.vedantu.util.subscription;

import java.util.Arrays;

public class UpdateCalendarSessionSlotsReq {
	private String studentId;
	private String teacherId;
	private Long subscriptionRequestId;
	private Long startTime;
	private Long endTime;
	private long[] scheduledSlots;

	public UpdateCalendarSessionSlotsReq() {
		super();
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Long getSubscriptionRequestId() {
		return subscriptionRequestId;
	}

	public void setSubscriptionRequestId(Long subscriptionRequestId) {
		this.subscriptionRequestId = subscriptionRequestId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public long[] getScheduledSlots() {
		return scheduledSlots;
	}

	public void setScheduledSlots(long[] scheduledSlots) {
		this.scheduledSlots = scheduledSlots;
	}

	@Override
	public String toString() {
		return "UpdateCalendarSessionSlotsReq [studentId=" + studentId + ", teacherId=" + teacherId
				+ ", subscriptionRequestId=" + subscriptionRequestId + ", startTime=" + startTime + ", scheduledSlots="
				+ Arrays.toString(scheduledSlots) + "]";
	}
}
