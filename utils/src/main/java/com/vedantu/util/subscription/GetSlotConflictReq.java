package com.vedantu.util.subscription;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.session.pojo.SessionSchedule;

public class GetSlotConflictReq {
	private Long studentId;
	private Long teacherId;
	private String referenceId;
	private SessionSchedule schedule;

	public GetSlotConflictReq() {
		super();
	}

	public GetSlotConflictReq(ResolveSlotConflictRes res) {
		super();
		this.studentId = res.getStudentId();
		this.teacherId = res.getTeacherId();
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public SessionSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(SessionSchedule schedule) {
		this.schedule = schedule;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (this.studentId == null || this.studentId <= 0l) {
			errors.add("studentId");
		}

		if (this.teacherId == null || this.teacherId <= 0l) {
			errors.add("teacherId");
		}

		if (this.schedule == null) {
			errors.add("schedule");
		} else {
			if (this.schedule.getStartTime() == null || this.schedule.getStartTime() <= 0l) {
				errors.add("schedule.startTime");
			}

		}

		return errors;
	}

	@Override
	public String toString() {
		return "GetSlotConflictReq [studentId=" + studentId + ", teacherId=" + teacherId + ", referenceId="
				+ referenceId + ", schedule=" + schedule + "]";
	}
}
