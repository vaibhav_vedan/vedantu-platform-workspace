package com.vedantu.util.subscription;

public class SubscriptionSchedule {
	private Integer[] schedule;
	private Integer[] slots;

	
	public SubscriptionSchedule() {
		super();
	}

	public Integer[] getSchedule() {
		return schedule;
	}

	public void setSchedule(Integer[] schedule) {
		this.schedule = schedule;
	}

	public Integer[] getSlots() {
		return slots;
	}

	public void setSlots(Integer[] slots) {
		this.slots = slots;
	}
}
