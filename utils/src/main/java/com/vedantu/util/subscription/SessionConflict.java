package com.vedantu.util.subscription;

import com.vedantu.session.pojo.SessionSlot;

public class SessionConflict {
	private SessionSlot previousSessionSlot;
	private SessionSlot newSessionSlot;

	public SessionConflict() {
		super();
	}

	public SessionSlot getPreviousSessionSlot() {
		return previousSessionSlot;
	}

	public void setPreviousSessionSlot(SessionSlot previousSessionSlot) {
		this.previousSessionSlot = previousSessionSlot;
	}

	public SessionSlot getNewSessionSlot() {
		return newSessionSlot;
	}

	public void setNewSessionSlot(SessionSlot newSessionSlot) {
		this.newSessionSlot = newSessionSlot;
	}

	@Override
	public String toString() {
		return "SessionConflict [previousSessionSlot=" + previousSessionSlot + ", newSessionSlot=" + newSessionSlot
				+ "]";
	}
}
