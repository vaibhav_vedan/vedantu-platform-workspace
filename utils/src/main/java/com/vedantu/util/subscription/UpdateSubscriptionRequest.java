package com.vedantu.util.subscription;

import com.vedantu.util.fos.request.AbstractFrontEndReq;


public class UpdateSubscriptionRequest extends AbstractFrontEndReq {
	private Long id;
	private String reason;
	private String lockBalanceInfo;
	private Long amount;
	private Long hourlyRate;
	private String paymentDetails;

	public UpdateSubscriptionRequest() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getLockBalanceInfo() {
		return lockBalanceInfo;
	}

	public void setLockBalanceInfo(String lockBalanceInfo) {
		this.lockBalanceInfo = lockBalanceInfo;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
}
