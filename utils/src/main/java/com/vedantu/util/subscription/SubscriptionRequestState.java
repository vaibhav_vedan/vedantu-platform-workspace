package com.vedantu.util.subscription;

public enum SubscriptionRequestState {
	DRAFT, // Storing initial info (Not visible to any user)
	ACTIVE, // Request visible to Both Users
	ACCEPTED,  //Request Accepted but Subscription not Created
	SUBSCRIBED, //Request Accepted and Subscription Created
	REJECTED, // Request Rejected 
	CANCELLED, // Request Cancelled
	EXPIRED	// Request Expired
}
