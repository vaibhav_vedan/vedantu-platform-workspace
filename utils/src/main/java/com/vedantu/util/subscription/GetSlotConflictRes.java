package com.vedantu.util.subscription;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.BasicResponse;
import java.util.Arrays;

public class GetSlotConflictRes extends BasicResponse {
	private Long studentId;
	private Long teacherId;
	private Long startTime;
	private Long endTime;
	private List<SessionSlot> conflictSessions=new ArrayList<>();
	private long[] slotBits;
	private long[] conflictSlotBits;
        private Long resolutionStartTime;
        private Long resolutionEndTime;

	public GetSlotConflictRes(Long studentId, Long teacherId, long[] slotBits, Long startTime, Long endTime) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.slotBits = slotBits;
		this.conflictSessions = new ArrayList<>();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public List<SessionSlot> getConflictSessions() {
		return conflictSessions;
	}

	public long[] getSlotBits() {
		return slotBits;
	}

	public void setSlotBits(long[] slotBits) {
		this.slotBits = slotBits;
	}

	public void addConflictSlot(SessionSlot sessionSlot) {
		this.conflictSessions.add(sessionSlot);
	}

	public long[] getConflictSlotBits() {
		return conflictSlotBits;
	}

	public void setConflictSlotBits(long[] conflictSlotBits) {
		this.conflictSlotBits = conflictSlotBits;
	}

	public void setConflictSessions(List<SessionSlot> conflictSessions) {
		this.conflictSessions = conflictSessions;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

        public Long getResolutionStartTime() {
            return resolutionStartTime;
        }

        public void setResolutionStartTime(Long resolutionStartTime) {
            this.resolutionStartTime = resolutionStartTime;
        }

        public Long getResolutionEndTime() {
            return resolutionEndTime;
        }

        public void setResolutionEndTime(Long resolutionEndTime) {
            this.resolutionEndTime = resolutionEndTime;
        }

        @Override
        public String toString() {
            return "GetSlotConflictRes{" + "studentId=" + studentId + ", teacherId=" + teacherId + ", startTime=" + startTime + ", endTime=" + endTime + ", conflictSessions=" + conflictSessions + ", slotBits=" + Arrays.toString(slotBits) + ", conflictSlotBits=" + Arrays.toString(conflictSlotBits) + ", conflictsStartTime=" + resolutionStartTime + ", conflictsEndTime=" + resolutionEndTime + '}';
        }
}
