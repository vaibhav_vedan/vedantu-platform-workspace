package com.vedantu.util.subscription;

import com.vedantu.session.pojo.SessionSchedule;

public class BlockSlotScheduleRes {
	private String studentId;
	private String teacherId;
	private SessionSchedule schedule;

	public BlockSlotScheduleRes() {
		super();
	}

	public BlockSlotScheduleRes(String studentId, String teacherId, SessionSchedule schedule) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.schedule = schedule;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public SessionSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(SessionSchedule schedule) {
		this.schedule = schedule;
	}

	@Override
	public String toString() {
		return "BlockSlotScheduleRes [studentId=" + studentId + ", teacherId=" + teacherId + ", schedule=" + schedule
				+ "]";
	}
}
