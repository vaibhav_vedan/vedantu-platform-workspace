package com.vedantu.util.subscription;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UpdateSubscriptionRequestSubState extends AbstractFrontEndReq {
	private Long id;
	private SubscriptionRequestSubState subState;

	public UpdateSubscriptionRequestSubState() {
		super();
	}

	public UpdateSubscriptionRequestSubState(Long id, SubscriptionRequestSubState subState) {
		super();
		this.id = id;
		this.subState = subState;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubscriptionRequestSubState getSubState() {
		return subState;
	}

	public void setSubState(SubscriptionRequestSubState subState) {
		this.subState = subState;
	}
}
