package com.vedantu.util.subscription;

public enum SubscriptionRequestAction {
	NONE, PAYMENT_PENDING, CONFLICT_PENDING, TEACHER_AWAITING
}
