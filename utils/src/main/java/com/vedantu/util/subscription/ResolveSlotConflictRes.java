package com.vedantu.util.subscription;

import java.util.Arrays;
import java.util.List;

import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.BasicResponse;

public class ResolveSlotConflictRes extends BasicResponse {
	private Long studentId;
	private Long teacherId;
	private Long startTime;
	private Long endTime;
	private long[] slotBits;
	private long[] conflictSlotBits;
	private List<SessionSlot> pendingConflicts;
	private int resolvedConflictCount = 0;

	public ResolveSlotConflictRes() {
		super();
	}

	public ResolveSlotConflictRes(Long studentId, Long teacherId, Long startTime, Long endTime, long[] slotBits,
			int resolvedConflictCount) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.slotBits = slotBits;
		this.resolvedConflictCount = resolvedConflictCount;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public long[] getSlotBits() {
		return slotBits;
	}

	public void setSlotBits(long[] slotBits) {
		this.slotBits = slotBits;
	}

	public long[] getConflictSlotBits() {
		return conflictSlotBits;
	}

	public void setConflictSlotBits(long[] conflictSlotBits) {
		this.conflictSlotBits = conflictSlotBits;
	}

	public List<SessionSlot> getPendingConflicts() {
		return pendingConflicts;
	}

	public void setPendingConflicts(List<SessionSlot> pendingConflicts) {
		this.pendingConflicts = pendingConflicts;
	}

	public int getResolvedConflictCount() {
		return resolvedConflictCount;
	}

	public void setResolvedConflictCount(int resolvedConflictCount) {
		this.resolvedConflictCount = resolvedConflictCount;
	}

	public void incrementResolvedConflictCount() {
		this.resolvedConflictCount++;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "ResolveSlotConflictRes [studentId=" + studentId + ", teacherId=" + teacherId + ", startTime="
				+ startTime + ", endTime=" + endTime + ", slotBits=" + Arrays.toString(slotBits) + ", conflictSlotBits="
				+ Arrays.toString(conflictSlotBits) + ", pendingConflicts=" + pendingConflicts
				+ ", resolvedConflictCount=" + resolvedConflictCount + "]";
	}
}
