package com.vedantu.util.subscription;

/**
 * Created by somil on 13/04/17.
 */
public class TrialSessionInfos {
    private String coursePlanId;
    private int trialSessionsScheduled;
    private int trialSessionsCancelled;
    private int trialSessionsExpired;
    private int trialSessionsForfeited;
    private int trialSessionsEnded;
    private int trialSessionsTotal;

    public int getTrialSessionsScheduled() {
        return trialSessionsScheduled;
    }

    public void setTrialSessionsScheduled(int trialSessionsScheduled) {
        this.trialSessionsScheduled = trialSessionsScheduled;
    }

    public int getTrialSessionsCancelled() {
        return trialSessionsCancelled;
    }

    public void setTrialSessionsCancelled(int trialSessionsCancelled) {
        this.trialSessionsCancelled = trialSessionsCancelled;
    }

    public int getTrialSessionsExpired() {
        return trialSessionsExpired;
    }

    public void setTrialSessionsExpired(int trialSessionsExpired) {
        this.trialSessionsExpired = trialSessionsExpired;
    }

    public int getTrialSessionsEnded() {
        return trialSessionsEnded;
    }

    public void setTrialSessionsEnded(int trialSessionsEnded) {
        this.trialSessionsEnded = trialSessionsEnded;
    }

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public int getTrialSessionsTotal() {
        return trialSessionsTotal;
    }

    public void setTrialSessionsTotal(int trialSessionsTotal) {
        this.trialSessionsTotal = trialSessionsTotal;
    }

    public int getTrialSessionsForfeited() {
        return trialSessionsForfeited;
    }

    public void setTrialSessionsForfeited(int trialSessionsForfeited) {
        this.trialSessionsForfeited = trialSessionsForfeited;
    }
}
