package com.vedantu.util.subscription;

public enum SubscriptionState {
	INACTIVE, // SubscriptionRequestSubstate moved to SUBSCRIPTION_CREATED
	ACTIVE, // SubscriptionRequestSubtate changed to SUCCESSFUL or directly booked Subscription
	ENDED  //Subscription Ended by User
}
