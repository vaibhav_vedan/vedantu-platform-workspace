package com.vedantu.util.subscription;

import com.vedantu.util.enums.SessionModel;
import java.util.Arrays;

public class SessionScheduleReq {
	private Long studentId;
	private Long teacherId;
	private Long subscriptionId;
	private Long subscriptionRequestId;
	private Long scheduledBy;
	public Long boardId;
	private SessionModel model;

	private Long startTime;
	private long[] slotBits;
	private long bitDuration;

	public String topic;
	public String title;
	public String description;

	public SessionScheduleReq() {
		super();
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getSubscriptionRequestId() {
		return subscriptionRequestId;
	}

	public void setSubscriptionRequestId(Long subscriptionRequestId) {
		this.subscriptionRequestId = subscriptionRequestId;
	}

	public Long getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(Long scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public long[] getSlotBits() {
		return slotBits;
	}

	public void setSlotBits(long[] slotBits) {
		this.slotBits = slotBits;
	}

	public long getBitDuration() {
		return bitDuration;
	}

	public void setBitDuration(long bitDuration) {
		this.bitDuration = bitDuration;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

        @Override
        public String toString() {
            return "SubscriptionRequestScheduleReq{" + "studentId=" + studentId + ", teacherId=" + teacherId + ", subscriptionId=" + subscriptionId + ", subscriptionRequestId=" + subscriptionRequestId + ", scheduledBy=" + scheduledBy + ", boardId=" + boardId + ", model=" + model + ", startTime=" + startTime + ", slotBits=" + Arrays.toString(slotBits) + ", bitDuration=" + bitDuration + ", topic=" + topic + ", title=" + title + ", description=" + description + '}';
        }
}
