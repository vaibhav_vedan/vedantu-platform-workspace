package com.vedantu.util.managers;

import com.vedantu.async.AppContextManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class PropertiesManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AbstractRedisDAO abstractRedisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PropertiesManager.class);


    public PlatformBasicResponse setApplicationPropertiesFromRedis() {
        try{
            System.out.println("Loading env application.properties from redis");
            String properties = "";
            String serviceName = ConfigUtils.INSTANCE.properties.getProperty("application.name");
            String key = serviceName + "_" + "application.properties";
            try{
                properties = abstractRedisDAO.get(key);
            }
            catch(Exception e){
                return new PlatformBasicResponse(false, "", e.toString());
            }

            ConfigUtils.INSTANCE.updateApplicationPropertiesFromRedis(properties);
        }
        catch(Exception e){
            logger.info("Error in setting application properties from redis" + e);
            return new PlatformBasicResponse(false, "", e.toString());
        }

        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse reloadEnvSecurityProperties() {
        try{
            ConfigUtils.INSTANCE.reloadEnvSecurityProperties();
        }
        catch(Exception e){
            logger.info("Error in setting security properties from redis" + e);
            return new PlatformBasicResponse(false, "", e.toString());
        }

        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse refreshSpringBean() {
        try{
            ConfigurableApplicationContext configContext = (ConfigurableApplicationContext) AppContextManager.getAppContext();
            configContext.refresh();
        }
        catch(Exception e){
            logger.info("Error in refreshing bean" + e);
            return new PlatformBasicResponse(false, "", e.toString());
        }

        return new PlatformBasicResponse();
    }

}
