
package com.vedantu.util.managers;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.dao.AdminTagDao;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.pojo.AdminTag;
import com.vedantu.util.pojo.CollectionName;
import com.vedantu.util.security.EncryptionUtil;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Service
public class AdminTagManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AdminTagManager.class);

    @Autowired
    public AdminTagDao commonDAO;

    @Autowired
    public HttpSessionUtils sessionUtils;

    public PlatformBasicResponse addAdminTags(List<AdminTag> adminTags, boolean removeTag, CollectionName collectionName) {
        Long callingUserId = sessionUtils.getCallingUserId();
        try {
            Class<?> aClass = Class.forName(collectionName.getClassName());
            if (!AbstractMongoEntity.class.isAssignableFrom(aClass))
                throw new UnsupportedOperationException("Please provide valid collection class name");
            //noinspection unchecked
            Class<AbstractMongoEntity> entityClass = (Class<AbstractMongoEntity>) aClass;
            commonDAO.addOrRemoveAdminTags(adminTags, callingUserId, removeTag, entityClass);
        } catch (ClassNotFoundException e) {
            return new PlatformBasicResponse(false, "", "Please provide valid collection class name");
        } catch (VException e) {
            return new PlatformBasicResponse(false, "", e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new PlatformBasicResponse(false, "", e.getMessage());
        }

        return new PlatformBasicResponse();
    }

    public List<CollectionName> getCollectionNames() {
        return commonDAO.getAllCollectionDetails();
    }

    public PlatformBasicResponse encryptKeys(Map<String, String> properties) throws InternalServerErrorException {
        StringBuilder builder = new StringBuilder();

        try {
            for (String key : properties.keySet()) {
                String value = properties.get(key);
                if (key.trim().isEmpty() || value == null || value.trim().isEmpty()) continue;
                builder.append(key)
                        .append("=")
                        .append(EncryptionUtil.encrypt(value.trim().getBytes()))
                        .append(System.lineSeparator());
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException |
                NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
//            logger.error(e.getMessage(), e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        return new PlatformBasicResponse(true, builder.toString(), "");
    }


    public PlatformBasicResponse encryptKey(String key, String value) throws InternalServerErrorException {
        String encrypt;
        try {
            encrypt = EncryptionUtil.encrypt(value.getBytes());
        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException |
                NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
//            logger.error(e.getMessage(), e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        return new PlatformBasicResponse(true, String.format("%s=\"%s\"", key, encrypt), "");
    }

    public PlatformBasicResponse checkProperties() {
        InputStream is = null;
        Properties properties = new Properties();
        String confDir = "ENV-" + ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase();
        System.out.println("Loading Security Props");
        StringBuilder builder = new StringBuilder();
        try {

            String securityPropPath = confDir + java.io.File.separator + "security.properties";
            is = ConfigUtils.class.getClassLoader().getResourceAsStream(securityPropPath);
            if (is == null) {
                securityPropPath = "security.properties";
                is = ConfigUtils.class.getClassLoader().getResourceAsStream(securityPropPath);
                if (is == null) return null;
            }

            properties.load(is);
            for(String key : properties.stringPropertyNames()) {
                Object value = properties.getProperty(key);
                if (value != null) {
                    byte[] bytes = null;
                    try {
                        bytes = DatatypeConverter.parseHexBinary(value.toString());
                    } catch (Exception ignored) {
                    }
                    if (bytes != null) {
                        value = EncryptionUtil.decrypt(bytes);
                    }
                    builder.append(key)
                            .append("=")
                            .append(value)
                            .append(System.lineSeparator());
                }
            }


        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {

                }
            }
        }
        return new PlatformBasicResponse(true, builder.toString(), "");
    }
}
