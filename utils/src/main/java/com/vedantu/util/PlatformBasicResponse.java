package com.vedantu.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlatformBasicResponse {

    private boolean success = true;
    private String errorCode;
    private String errorMessage;
    private String response;
    private Object responseObject;
    private String errorString;

    public PlatformBasicResponse(boolean success, String response, String errorString) {
        super();
        this.success = success;
        this.response = response;
        this.errorString = errorString;
    }

    public PlatformBasicResponse(boolean success, Object responseObject, String errorString) {
        super();
        this.success = success;
        this.responseObject = responseObject;
        this.errorString = errorString;
    }

    @Override
    public String toString() {
        return "PlatformBasicResponse{" + "success=" + success + ", errorCode=" + errorCode + ", errorMessage="
                + errorMessage + ", response=" + response + ", errorString=" + errorString + '}';
    }
}
