/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class UserRedisManager {

    

    @Autowired
    private AbstractRedisDAO abstractRedisDAO;

    public static final String USER_STATUS_KEY_PREFIX = "USER_STATUS_";
    private final Gson gson = new Gson();

    public UserRedisManager() {
    }

    public String getUserStatusRedisKey(Long userId) {
        return USER_STATUS_KEY_PREFIX + userId;
    }

    
    public String addPasswordChangedAt(Long userId) throws InternalServerErrorException, BadRequestException {

        String key = getUserStatusRedisKey(userId);
        String currentStatus = abstractRedisDAO.get(key);
        UserStatus status;
        if (StringUtils.isNotEmpty(currentStatus)) {
            status = gson.fromJson(currentStatus, UserStatus.class);
        } else {
            status = new UserStatus();
        }
        status.setLastPasswordChangedAt(System.currentTimeMillis());
        int days = ConfigUtils.INSTANCE.getIntValue("auth.userStatusExpiryDays");
        String redisResp = abstractRedisDAO.setex(key, gson.toJson(status, UserStatus.class), days * DateTimeUtils.SECONDS_PER_DAY);
        return redisResp;
    }    
    
    public String addUserStatus(Long userId, Boolean isProfileEnabled) throws InternalServerErrorException, BadRequestException {

        String key = getUserStatusRedisKey(userId);
        String currentStatus = abstractRedisDAO.get(key);
        UserStatus status;
        if (StringUtils.isNotEmpty(currentStatus)) {
            status = gson.fromJson(currentStatus, UserStatus.class);
        } else {
            status = new UserStatus();
        }
        status.setIsProfileEnabled(isProfileEnabled);
        int days = ConfigUtils.INSTANCE.getIntValue("auth.userStatusExpiryDays");
        String redisResp = abstractRedisDAO.setex(key, gson.toJson(status, UserStatus.class), days * DateTimeUtils.SECONDS_PER_DAY);
        return redisResp;
    }

    public UserStatus getUserStatus(Long userId) throws InternalServerErrorException, BadRequestException {
        String key = getUserStatusRedisKey(userId);
        String currentStatus = abstractRedisDAO.get(key);
        UserStatus status = null;
        if (StringUtils.isNotEmpty(currentStatus)) {
            status = gson.fromJson(currentStatus, UserStatus.class);
        }
        return status;
    }
}
