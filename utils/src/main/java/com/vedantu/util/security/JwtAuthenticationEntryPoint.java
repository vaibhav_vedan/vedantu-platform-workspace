/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VExceptionFactory;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(JwtAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e)
            throws IOException, ServletException {

        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        logger.info("Error in authentication for url " + httpServletRequest.getRequestURL());

        String message;
        if (e.getCause() != null) {
            message = e.getCause().getMessage();
        } else {
            message = e.getMessage();
        }
        logger.info("Error Message " + message, e);
        String jsonresp;
        if (StringUtils.isNotEmpty(message) && message.contains("exceeded")) {
            // 429 Too Many Requests
            httpServletResponse.setStatus(429);
            jsonresp = VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.RATE_LIMIT_EXCEEDED.name(), message);
        } else {
            httpServletResponse.setStatus(SC_UNAUTHORIZED);
            jsonresp = VExceptionFactory.INSTANCE.getErrorJSONString(ErrorCode.SERVICE_ERROR.name(), message);
        }
        httpServletResponse.getWriter().print(jsonresp);
    }
}
