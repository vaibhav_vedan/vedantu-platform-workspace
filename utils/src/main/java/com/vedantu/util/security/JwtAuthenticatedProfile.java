package com.vedantu.util.security;

import java.util.ArrayList;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;

public class JwtAuthenticatedProfile implements Authentication {

    private HttpSessionData sessionData;
    private String jwtToken;
    private List<GrantedAuthority> authorities;

    public JwtAuthenticatedProfile() {
    }

    public JwtAuthenticatedProfile(HttpSessionData sessionData, String token) {
        this.sessionData = sessionData;
        this.jwtToken = token;
        authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(sessionData.getRole().name()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return Collections.singletonList(new SimpleGrantedAuthority("ROLE_AJITH"));
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return sessionData.getEmail();
    }

    public HttpSessionData getSessionData() {
        return sessionData;
    }

    public void setSessionData(HttpSessionData sessionData) {
        this.sessionData = sessionData;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    @Override
    public String toString() {
        return "JwtAuthenticatedProfile{" + "sessionData=" + sessionData + ", authorities=" + authorities + '}';
    }
}
