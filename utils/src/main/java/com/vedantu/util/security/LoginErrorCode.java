/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

/**
 *
 * @author ajith
 */
public enum LoginErrorCode {
    NOT_LOGGED_IN, USER_BLOCKED, TNC_NOT_ACCEPTED, PASSWORD_CHANGED
}
