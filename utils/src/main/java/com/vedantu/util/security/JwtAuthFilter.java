/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

import com.google.common.io.CountingOutputStream;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.StringUtils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

//as the token is set in cookie, if a browser does not prevent CORS, then follow this method
//http://stackoverflow.com/questions/27067251/where-to-store-jwt-in-browser-how-to-protect-against-csrf
//reference for response size calculation
//https://stackoverflow.com/questions/3220820/how-to-insert-response-size-and-time-into-the-page-itself-at-least-partially
@Component
public class JwtAuthFilter implements Filter {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(JwtAuthFilter.class);

    @Autowired
    private StatsdClient statsdClient;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    private static final String MONGO_OBJECTID_REGEX = "^[a-fA-F0-9]{24}$";

    private static final String USERID_REGEX = "^[0-9]{16}$";

    static final Pattern objectIDRegex = Pattern.compile(MONGO_OBJECTID_REGEX);
    static final Pattern userIdRegex = Pattern.compile(USERID_REGEX);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest servletRequest = (HttpServletRequest) request;
        String authorization = servletRequest.getHeader("X-Ved-Token");
        if (StringUtils.isEmpty(authorization)) {
            authorization = sessionUtils.getVedTokenFromCookie(servletRequest);
        }
        logger.info("authorization header " + authorization);
        Map<String, Object> requestDetails = new HashMap<>();
        String forwardTo = request.getParameter("forwardTo");
        String urlAccessed = servletRequest.getRequestURI();
        if (StringUtils.isNotEmpty(forwardTo) && (urlAccessed.equals("/platform/postforwarder") || urlAccessed.equals("/platform/getforwarder"))) {
            urlAccessed = formatForwardTo(forwardTo);
        }
        requestDetails.put("requestMethod", servletRequest.getMethod());
        requestDetails.put("requestURI", urlAccessed);
        String requestOrigin = servletRequest.getHeader("Origin");
        requestDetails.put("requestOrigin", requestOrigin);
        requestDetails.put(HttpSessionUtils.Constants.CLIENT, servletRequest.getHeader("X-Ved-Client"));
        requestDetails.put(HttpSessionUtils.Constants.CLIENT_ID, servletRequest.getHeader("X-Ved-Client-Id"));
        requestDetails.put(HttpSessionUtils.Constants.CLIENT_SECRET, servletRequest.getHeader("X-Ved-Client-Secret"));
        requestDetails.put(HttpSessionUtils.Constants.AWS_CHIME_REQUEST_HEADER, servletRequest.getHeader("x-amz-sns-message-type"));

        String ipAddress = servletRequest.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = servletRequest.getRemoteAddr();
        }

        requestDetails.put("ipAddress", ipAddress);
        requestDetails.put("requestURI", urlAccessed);

        JwtAuthToken token = new JwtAuthToken(authorization, requestDetails);
        SecurityContextHolder.getContext().setAuthentication(token);
//        StatusExposingServletResponse nresponse = new StatusExposingServletResponse((HttpServletResponse)response);
//        chain.doFilter(request, nresponse);

//        chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
        HttpServletResponse httpres = (HttpServletResponse) response;
        CountingServletResponse counter = new CountingServletResponse(httpres);
        HttpServletRequest httpreq = (HttpServletRequest) request;
        httpreq.setAttribute("counter", counter);
        chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), counter);
        counter.flushBuffer(); // Push the last bits containing HTML comment.
        logger.info("Response size  " + counter.getByteCount());
        urlAccessed = formatUrlAccessed(urlAccessed);
        if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
            if (httpreq.getAttribute("className") != null && httpreq.getAttribute("methodName") != null) {
                String className = (String) httpreq.getAttribute("className");
                String methodName = (String) httpreq.getAttribute("methodName");
                statsdClient.recordExecutionTime(counter.getByteCount(), "response-size", className, methodName);
            } else {
                statsdClient.recordExecutionTime(counter.getByteCount(), "response-size", urlAccessed);
            }
        }
        logger.info("Response time " + counter.getElapsedTime());

    }

    private String formatForwardTo(String forwardTo) {
        if(StringUtils.isNotEmpty(forwardTo)){
            String[] split = forwardTo.split("\\?");
            if(split.length >= 1){
                String[] newUrl = Arrays.copyOf(split, 1);
                return StringUtils.join(newUrl,"");
            }
        }
        return forwardTo;
    }

    //format url accessed
    private String formatUrlAccessed(String urlAccessed) {
        if(StringUtils.isNotEmpty(urlAccessed)){
            String[] split = urlAccessed.split("/");
            if(split.length >= 1){
                String lastStr = split[split.length - 1];
                if(matchesUserId(lastStr) || matchesObjectId(lastStr)){
                    String[] newUrl = Arrays.copyOf(split, split.length - 1);
                    return StringUtils.join(newUrl,"/");
                }
            }
        }
        return urlAccessed;
    }

    private boolean matchesObjectId(String lastStr) {
        return objectIDRegex.matcher(lastStr).matches();
    }

    private boolean matchesUserId(String lastStr) {
        return userIdRegex.matcher(lastStr).matches();
    }

    public static class Constants {

        public static final String CLIENT = "CLIENT";
        public static final String CLIENT_ID = "CLIENT.ID";
        public static final String CLIENT_SECRET = "CLIENT.SECRET";
    }

    @Override
    public void destroy() {

    }

    public class CountingServletResponse extends HttpServletResponseWrapper {

        private final long startTime;
        private final CountingServletOutputStream output;
        private final PrintWriter writer;

        public CountingServletResponse(HttpServletResponse response) throws IOException {
            super(response);
            startTime = System.nanoTime();
            output = new CountingServletOutputStream(response.getOutputStream());
            writer = new PrintWriter(output, true);
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            return output;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            return writer;
        }

        @Override
        public void flushBuffer() throws IOException {
            writer.flush();
        }

        public long getElapsedTime() {
            return System.nanoTime() - startTime;
        }

        public long getByteCount() throws IOException {
            flushBuffer(); // Ensure that all bytes are written at this point.
            return output.getByteCount();
        }

    }

    public class CountingServletOutputStream extends ServletOutputStream {

        private final CountingOutputStream output;

        public CountingServletOutputStream(ServletOutputStream output) {
            this.output = new CountingOutputStream(output);
        }

        @Override
        public void write(int b) throws IOException {
            output.write(b);
        }

        @Override
        public void flush() throws IOException {
            output.flush();
        }

        public long getByteCount() {
            return output.getCount();
        }

        @Override
        public boolean isReady() {
            logger.error("isReady in CountingServletOutputStream not expected");
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void setWriteListener(WriteListener wl) {
            logger.error("setWriteListener in CountingServletOutputStream not expected");
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

}
