package com.vedantu.util.security;

import com.google.gson.Gson;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Date;

import static java.time.ZoneOffset.UTC;

@Component
public class JwtService {

    private String ISSUER;
    private String secretKey;
    private int webTokenValidityDays;
    private int mobileTokenValidityDays;
    private Long loginByTokenValidityMinutes;
    private final Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(JwtService.class);

    public JwtService() {

    }

    @PostConstruct
    public void init() {
        this.secretKey = ConfigUtils.INSTANCE.getStringValue("auth.secret.key");
        this.ISSUER = ConfigUtils.INSTANCE.getStringValue("auth.issuer");
        this.webTokenValidityDays = ConfigUtils.INSTANCE.getIntValue("auth.webTokenValidityDays", 60);
        this.mobileTokenValidityDays = ConfigUtils.INSTANCE.getIntValue("auth.mobileTokenValidityDays");
        this.loginByTokenValidityMinutes = ConfigUtils.INSTANCE.getLongValue("auth.loginByTokenValidityMinutes");
    }

    public String tokenFor(HttpSessionData sessionData, DevicesAllowed device) throws IOException, URISyntaxException {
        Date expiration = Date.from(LocalDateTime.now(UTC).plusDays(webTokenValidityDays).toInstant(UTC));
        if (DevicesAllowed.ANDROID.equals(device) || DevicesAllowed.IOS.equals(device)) {
            expiration = Date.from(LocalDateTime.now(UTC).plusDays(mobileTokenValidityDays).toInstant(UTC));
        }
        if (sessionData.getLoginType() == LoginType.TOKEN_BASED) {
            expiration = Date.from(LocalDateTime.now(UTC).plusMinutes(loginByTokenValidityMinutes).toInstant(UTC));
        }
        sessionData.setExpiryTime(expiration.getTime());
        return Jwts.builder()
                .setSubject((sessionData.getEmail() != null) ? sessionData.getEmail() : sessionData.getContactNumber())
                .setExpiration(expiration)
                .setIssuer(ISSUER)
                .claim("sessionData", gson.toJson(sessionData, HttpSessionData.class))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }

    public HttpSessionData verify(String token) throws IOException, URISyntaxException {
        Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        String json = (String) claims.getBody().get("sessionData");
        HttpSessionData sessionData = gson.fromJson(json, HttpSessionData.class);
        logger.info("SessionData " + sessionData);
        return sessionData;
    }
}
