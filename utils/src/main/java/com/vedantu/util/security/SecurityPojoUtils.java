/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

import java.util.Objects;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.User;
import com.vedantu.User.UserUtils;
import com.vedantu.User.enums.TeacherCategoryType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;

/**
 *
 * @author ajith
 */


@Service
public class SecurityPojoUtils {

    @Autowired
    private DozerBeanMapper mapper;  
    
    public static SecurityPojoUtils INSTANCE = new SecurityPojoUtils();

    
    public HttpSessionData convertToSessionData(User user) {
        HttpSessionData sessionData = mapper.map(user, HttpSessionData.class);
        if(Objects.nonNull(user)) {
            
        	sessionData.setUserId(user.getId());
        	sessionData.setProfilePicUrl(StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
        			: UserUtils.INSTANCE.toProfilePicUrl(user.getProfilePicPath()));
        	sessionData.setFullName(user.getFullName());
        	//sessionData.settncVersion(user.getTncVersion());
        	if(Objects.nonNull(user.getStudentInfo())) {
        		if(Objects.nonNull(user.getStudentInfo().getGrade())) {
        			sessionData.setGrade(user.getStudentInfo().getGrade());
        		}
        		if(Objects.nonNull(user.getStudentInfo().getBoard()))
        			sessionData.setBoard(user.getStudentInfo().getBoard());
        		if(ArrayUtils.isNotEmpty(user.getStudentInfo().getExamTargets()))
        			sessionData.setExamTargets(user.getStudentInfo().getExamTargets());
        		if(StringUtils.isNotEmpty(user.getStudentInfo().getTarget()))
        			sessionData.setTarget(user.getStudentInfo().getTarget());
        		if(StringUtils.isNotEmpty(user.getStudentInfo().getStream()))
        			sessionData.setStream(user.getStudentInfo().getStream());
        		if(Objects.nonNull(user.getStudentInfo().getDeviceType())){
        			sessionData.setDeviceType(user.getStudentInfo().getDeviceType());
        		}
        	}

    		if (Objects.nonNull(user.getTeacherInfo()) && Objects.nonNull(user.getTeacherInfo().getTeacherCategoryTypes())
						&& user.getTeacherInfo().getTeacherCategoryTypes().contains(TeacherCategoryType.EARLY_LEARNING)) {
				sessionData.setTeacherCategoryType(TeacherCategoryType.EARLY_LEARNING);
			}

        }
        return sessionData;
    }

}
