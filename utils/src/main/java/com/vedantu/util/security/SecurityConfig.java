package com.vedantu.util.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import java.util.Arrays;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableRetry
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthFilter jwtAuthFilter;

    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthEndPoint;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*
        Reason for doing this
        http://stackoverflow.com/questions/30761297/is-a-securitycontext-shared-between-requests-when-using-spring-security
        http://docs.spring.io/spring-security/site/docs/3.1.x/reference/springsecurity-single.html
         */
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.cors().and().csrf().disable();// ignoringAntMatchers("/login");
        String[] patterns = new String[]{
            "/login",
            "/signUp",
            "/static/*",};
        http.authorizeRequests()
                //                .antMatchers(patterns)
                //                .permitAll()
                .anyRequest().authenticated();
//                .hasAuthority("ADMIN")
//                .and()
        http.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthEndPoint);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration globalConfiguration = new CorsConfiguration();
        List<String> allowedOrigins = new ArrayList<>();
        allowedOrigins.add(ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        if(StringUtils.isNotEmpty(ConfigUtils.INSTANCE.getStringValue("platform.baseUrl"))){
            allowedOrigins.add(ConfigUtils.INSTANCE.getStringValue("platform.baseUrl"));
        }
        
        String allowedOriginsStr = ConfigUtils.INSTANCE.getStringValue("allowed.origins");
        if (StringUtils.isNotEmpty(allowedOriginsStr)) {
            String[] splits = allowedOriginsStr.split(",");
            for (int k = 0; k < splits.length; k++) {
                allowedOrigins.add(splits[k]);
            }
        }
        globalConfiguration.setAllowedOrigins(allowedOrigins);
        globalConfiguration.addAllowedHeader("*");
        globalConfiguration.addAllowedMethod("*");
        globalConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        addCorsMapping(source, globalConfiguration, "/dinero/payment/onPaymentReceived/**", Arrays.asList("*"));

        //THIS MUST BE AT THE END
        source.registerCorsConfiguration("/**", globalConfiguration);
        return source;
    }

    private void addCorsMapping(UrlBasedCorsConfigurationSource source, CorsConfiguration globalConfiguration, String path, List<String> allowedOrigins) {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(allowedOrigins);
        config = globalConfiguration.combine(config);
        source.registerCorsConfiguration(path, config);
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
}
