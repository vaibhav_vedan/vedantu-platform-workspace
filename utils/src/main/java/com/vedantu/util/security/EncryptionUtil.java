package com.vedantu.util.security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;

public class EncryptionUtil {
    private static final String KEY = System.getProperty("SECURE_RANDOM_KEY");
    private static final int BYTE_COUNT = 16;

    public static String encrypt(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        SecretKeySpec skeySpec = setKey(KEY, BYTE_COUNT);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] bytes = new byte[32];
        random.nextBytes(bytes);
        byte[] iv = Arrays.copyOfRange(bytes, 0, 16);
        byte[] cipherText = new byte[16 + data.length];
        System.arraycopy(data, 0, cipherText, 16, data.length);
        IvParameterSpec iv_specs = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv_specs);
        byte[] plainTextBytes = cipher.doFinal(cipherText);
        return DatatypeConverter.printHexBinary(plainTextBytes);
    }

    public static String decrypt(byte[] encryptedData) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        SecretKeySpec skeySpec = setKey(KEY, BYTE_COUNT);//new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = Arrays.copyOfRange(encryptedData, 0, 16);
        byte[] cipherText = Arrays.copyOfRange(encryptedData, 16, encryptedData.length);
        IvParameterSpec iv_specs = new IvParameterSpec(iv);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv_specs);
        byte[] plainTextBytes = cipher.doFinal(cipherText);
        return new String(plainTextBytes);
    }

    private static SecretKeySpec setKey(String myKey, int byteCount) {
        SecretKeySpec secretKey;
        byte[] key;
        try {
            key = myKey.getBytes(StandardCharsets.UTF_8); //No I18N
            key = Arrays.copyOf(key, byteCount);
            secretKey = new SecretKeySpec(key, "AES"); //No I18N
            return secretKey;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        String value = "8101D9EEC3507E0244DDD9D475B5C60A58F3969AB024C2F0FE356AD23B36BC04";
        byte[] bytes = null;
        try {
            bytes = DatatypeConverter.parseHexBinary(value);
        } catch (Exception ignored) {
        }
        if (bytes != null) {
            value = EncryptionUtil.decrypt(bytes);
        }
        System.out.println(value);
    }
}
