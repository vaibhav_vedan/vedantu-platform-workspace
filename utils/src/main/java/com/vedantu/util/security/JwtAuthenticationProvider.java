package com.vedantu.util.security;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.RateLimitException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.ratelimit.RateLimitHandler;

import java.util.*;

import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(JwtAuthenticationProvider.class);

    private final JwtService jwtService;

    @Autowired
    private RateLimitHandler rateLimitHandler;

    @Autowired
    private UserRedisManager userRedisManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    //PLATFORM DINERO SUBSCRIPTION USER NOTIFICATION-CENTRE ONETOFEW SCHEDULING LISTING LMS
    private static final String[] exemptedApps = {"DINERO", "SUBSCRIPTION", "NOTIFICATION CENTRE", "ONETOFEW", "SCHEDULING", "LISTING", "USER", "VEDANTUDATA"};
    private static final List<String> exemptedAppsList = Arrays.asList(exemptedApps);
    private static final String appName = ConfigUtils.INSTANCE.getStringValue("application.name");
    private static final String whiteListedIPs = ConfigUtils.INSTANCE.getStringValue("whitelisted.ips");
    private static final String whiteListedUserIds = ConfigUtils.INSTANCE.getStringValue("whitelisted.userids");

    @SuppressWarnings("unused")
    public JwtAuthenticationProvider() {
        this(null);
    }

    @Autowired
    public JwtAuthenticationProvider(JwtService jwtService) {

        this.jwtService = jwtService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        boolean exemptForCheck = false;
        try {
            Map<String, Object> requestDetails = (Map<String, Object>) authentication.getDetails();

            // String requestMethod = (String)
            // requestDetails.get("requestMethod");
            // exemptForCheck = requestMethod.equals("OPTIONS");
            if (!exemptForCheck) {
                if (requestDetails.get(HttpSessionUtils.Constants.AWS_CHIME_REQUEST_HEADER) != null) {
                    exemptForCheck = true;
                    return new JwtAuthenticatedProfile();
                }
            }

            logger.info("***************************");
            logger.info("REQUEST DETAILS: " + requestDetails.toString());
            logger.info("***************************");

            HttpSessionData sessionData = null;
            Long userId = null;
            String token = null;
            try {
                if (authentication.getCredentials() != null) {
                    token = (String) authentication.getCredentials();
                    sessionData = jwtService.verify(token);
                    if (sessionData != null) {
                        userId = sessionData.getUserId();
                        logger.info("USERID: " + userId.toString());
                    }
                }
            } catch (Exception e) {
                logger.info("Failed to verify token " + e.getMessage());
            }


            //allowing access to certified apps
            if (!exemptForCheck) {
                exemptForCheck = sessionUtils.isAllowedApp(requestDetails);
                if (exemptForCheck) {
                    //passing the session data to reuse in other subsystems for authorization checks
                    return new JwtAuthenticatedProfile(sessionData, token);
                }
            }
//            if (!exemptForCheck) {
//                if (StringUtils.isNotEmpty(appName) && exemptedAppsList.contains(appName.toUpperCase())) {
////                    exemptForCheck = true;
//                    logger.error("headers not passed for app " + appName + ", uri " + requestURI);
//                }
//            }

            String ipAddress = (String) requestDetails.get("ipAddress");

            String[] values = whiteListedIPs.split(",");
            Set<String> hashSet = new HashSet<String>(Arrays.asList(values));

            String[] whiteListedUserIdValues = whiteListedUserIds.split(",");
            Set<String> whiteListedUserIdsSet = new HashSet<String>(Arrays.asList(whiteListedUserIdValues));

            String requestURI = (String) requestDetails.get("requestURI");

            try {
                if(userId != null && whiteListedUserIdsSet.contains(userId.toString())){
                    logger.info(userId + " is whitelisted userId");
                }
                else if (!hashSet.contains(ipAddress)) {
                    rateLimitHandler.handleAuthRequestLimiting(userId, requestDetails, ipAddress);
                }
            } catch (Exception e) {
                if (e instanceof RateLimitException) {
                    throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");
                } else {
                    throw new Exception(e.getMessage());
                }
            }

            //check in unrestricted urls
            if (!exemptForCheck) {
                exemptForCheck = sessionUtils.isAllowedUrl(requestURI);
            }

            if (authentication.getCredentials() == null) {
                throw new Exception(LoginErrorCode.NOT_LOGGED_IN.name());
            }
            UserStatus userStatus = null;
            try {
                if (sessionData != null && sessionData.getUserId() != null) {
                    userStatus = userRedisManager.getUserStatus(sessionData.getUserId());
                }
            } catch (Exception e) {
                logger.error("error in fetching user status for JWTverification " + e.getMessage());
            }
            if (userStatus != null) {
                logger.info("userStatus " + userStatus);
                Long lastPasswordChangedAt = userStatus.getLastPasswordChangedAt();
                Boolean isProfileEnabled = userStatus.getIsProfileEnabled();
                if (lastPasswordChangedAt != null
                        && sessionData != null && sessionData.getCreationTime() != null
                        && sessionData.getCreationTime() < lastPasswordChangedAt) {
                    throw new Exception(LoginErrorCode.PASSWORD_CHANGED.name());
                }
                if (isProfileEnabled != null && !isProfileEnabled) {
                    throw new Exception(LoginErrorCode.USER_BLOCKED.name());
                }
            }
            //tnc check
//            String latestTncVersion = ConfigUtils.INSTANCE.getStringValue("auth.latest.tnc.version");
//            String tncVersion = sessionData.getTncVersion();
//            
//            if (!ConfigUtils.INSTANCE.getStringValue("auth.tnc.accept.url").equals(requestURI)
//                    && (StringUtils.isEmpty(tncVersion) || !latestTncVersion.equals(tncVersion))) {
//                throw new Exception(LoginErrorCode.TNC_NOT_ACCEPTED.name());
//            }            
            return new JwtAuthenticatedProfile(sessionData, (String) authentication.getCredentials());
        } catch (Exception e) {
            if (exemptForCheck) {
                return new JwtAuthenticatedProfile();
            } else {
                throw new JwtAuthenticationException("Failed to verify token", e);
            }

        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthToken.class.equals(authentication);
    }

    public static void main(String[] args) {
        if (StringUtils.isNotEmpty(appName) && exemptedAppsList.contains(appName.toUpperCase())) {
            System.err.println(">> empted");
        }
        System.err.println(">> done");
    }

}
