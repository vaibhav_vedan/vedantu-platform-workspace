/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

/**
 *
 * @author ajith
 */
public class UserStatus {

    private Long lastPasswordChangedAt;
    private Boolean isProfileEnabled;

    public UserStatus() {
    }

    public UserStatus(Long lastPasswordChangedAt, Boolean isProfileEnabled) {
        this.lastPasswordChangedAt = lastPasswordChangedAt;
        this.isProfileEnabled = isProfileEnabled;
    }

    public Long getLastPasswordChangedAt() {
        return lastPasswordChangedAt;
    }

    public void setLastPasswordChangedAt(Long lastPasswordChangedAt) {
        this.lastPasswordChangedAt = lastPasswordChangedAt;
    }

    public Boolean getIsProfileEnabled() {
        return isProfileEnabled;
    }

    public void setIsProfileEnabled(Boolean isProfileEnabled) {
        this.isProfileEnabled = isProfileEnabled;
    }

    @Override
    public String toString() {
        return "UserStatus{" + "lastPasswordChangedAt=" + lastPasswordChangedAt + ", isProfileEnabled=" + isProfileEnabled + '}';
    }
    
}
