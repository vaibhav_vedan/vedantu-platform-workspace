/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.security;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.*;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 *
 * @author ajith
 */
@Service
public class HttpSessionUtils {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(HttpSessionUtils.class);

    @Autowired
    private JwtService jwtService;

    private int REFRESH_TOKEN_TIME;

    @Autowired
    private SecurityPojoUtils pojoUtils;

    private String[] allowedUrls;

    public HttpSessionUtils() {
        REFRESH_TOKEN_TIME = ConfigUtils.INSTANCE.getIntValue("auth.refresh.token.hrs") * DateTimeUtils.MILLIS_PER_HOUR;
        String unrestrictedcommonurls = ConfigUtils.INSTANCE.getStringValue("unrestricted.common.urls");
        String mavenArtifactId = ConfigUtils.INSTANCE.getStringValue("maven.artifactId");
        if (StringUtils.isNotEmpty(ConfigUtils.INSTANCE.getStringValue("auth.unrestricted.urls"))) {
            String[] _allowedUrls = ConfigUtils.INSTANCE.getStringValue("auth.unrestricted.urls").split(",");
            String[] unrestrictedcommonurlsArr = null;
            int totalLength = _allowedUrls.length;
            if (StringUtils.isNotEmpty(unrestrictedcommonurls)
                    && StringUtils.isNotEmpty(mavenArtifactId)) {
                unrestrictedcommonurlsArr = unrestrictedcommonurls.split(",");
                totalLength += unrestrictedcommonurlsArr.length;
            }
            this.allowedUrls = new String[totalLength];
            for (int i = 0; i < _allowedUrls.length; i++) {
                this.allowedUrls[i] = _allowedUrls[i].trim();
            }

            if (unrestrictedcommonurlsArr != null) {
                int g = _allowedUrls.length;
                for (int k = 0; k < unrestrictedcommonurlsArr.length; k++) {
                    this.allowedUrls[g + k] = "/" + mavenArtifactId + unrestrictedcommonurlsArr[k].trim();
                }
            }
        }
        System.out.println("<<<<<< ");
        System.out.println(Arrays.toString(this.allowedUrls));
    }

    public String getVedTokenFromCookie(HttpServletRequest req) {
        Cookie cookie = getCookie(req, ConfigUtils.INSTANCE.getStringValue("auth.cookie.header.token.name"));
        if (cookie != null) {
            return cookie.getValue();
        } else {
            return null;
        }

    }

    public HttpSessionData setCookieAndHeaders(HttpServletRequest request, HttpServletResponse response, User user)
            throws IOException, URISyntaxException {
        return setCookieAndHeaders(request, response, user, null);
    }

    public HttpSessionData setCookieAndHeaders(HttpServletRequest request, HttpServletResponse response, User user, LoginType loginType)
            throws IOException, URISyntaxException {
        String deviceStr = request.getHeader(ConfigUtils.INSTANCE.getStringValue("auth.device.type"));
        DevicesAllowed device = DevicesAllowed.WEB;
        if (StringUtils.isNotEmpty(deviceStr)) {
            try {
                device = DevicesAllowed.valueOf(deviceStr);
            } catch (Exception e) {
                logger.warn("Illegal device set in session " + deviceStr);
            }
        }
        HttpSessionData sessionData = pojoUtils.convertToSessionData(user);
        logger.info("After session data conversion, the user to be allowed for onboarding is {}",sessionData.isUserInProcessOfOnboarding());
        sessionData.setCreationTime(System.currentTimeMillis());
        sessionData.setDevice(device);
        sessionData.setLoginType(loginType);

        // HttpSessionData sessionData = new
        // HttpSessionData(System.currentTimeMillis(), device);
        // sessionData.toSessionData(user);
        _setCookieAndHeaders(request, response, sessionData);
        return sessionData;
    }

    public void _setCookieAndHeaders(HttpServletRequest request, HttpServletResponse response,
            HttpSessionData sessionData) throws IOException, URISyntaxException {
        // again
        String token = jwtService.tokenFor(sessionData, sessionData.getDevice());
        response.setHeader("X-Ved-Token", token);
        Cookie tokenCookie = new Cookie("X-Ved-Token", token);
        tokenCookie.setMaxAge(-1);
        tokenCookie.setDomain(ConfigUtils.INSTANCE.getStringValue("auth.cookie.domain"));
        tokenCookie.setSecure(ConfigUtils.INSTANCE.getBooleanValue("auth.cookie.secure"));
        tokenCookie.setPath("/");
        response.addCookie(tokenCookie);
    }
    
    //do not use it for user apis, only use this for internal purpose
    public String getXVedToken(User user)throws IOException, URISyntaxException {
        HttpSessionData sessionData = pojoUtils.convertToSessionData(user);
        sessionData.setCreationTime(System.currentTimeMillis());
        sessionData.setDevice(DevicesAllowed.WEB);
        return jwtService.tokenFor(sessionData, DevicesAllowed.WEB);
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response, HttpSessionData sessionData)
            throws IOException, URISyntaxException {
        Long expiryTime = Optional.ofNullable(sessionData.getExpiryTime()).orElseGet(System::currentTimeMillis);
        long diff = expiryTime - System.currentTimeMillis();
        LoginType loginType = sessionData.getLoginType();
        if (diff > 0 && diff <= REFRESH_TOKEN_TIME && !LoginType.TOKEN_BASED.equals(loginType)) {
            logger.info("refreshing the token");
            sessionData.setRefreshedTime(System.currentTimeMillis());
            _setCookieAndHeaders(request, response, sessionData);
        }
    }

    public HttpSessionData getCurrentSessionData() {
        HttpSessionData sessionData = null;
        try {
            sessionData = getCurrentSessionData(false);
        } catch (Exception e) {
            //swallow
        }
        return sessionData;
    }
    

    public HttpSessionData getCurrentSessionData(boolean throwExceptionForNullData) throws ForbiddenException {
        logger.info("Request for current session data");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof JwtAuthenticatedProfile) {
            JwtAuthenticatedProfile jwtAuthenticatedProfile = (JwtAuthenticatedProfile) authentication;
            HttpSessionData sessionData = jwtAuthenticatedProfile.getSessionData();
            logger.info("returning session data " + jwtAuthenticatedProfile);
            if (sessionData == null && throwExceptionForNullData) {
                throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "not logged in");
            }
            return sessionData;
        } else if (throwExceptionForNullData) {
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "not logged in");
        }
        return null;
    }

    
    public HttpSessionData getSessionDataFromToken(String token) throws IOException, URISyntaxException {
        return jwtService.verify(token);
    }    

    public boolean isAdminOrStudentCare(HttpSessionData sessionData) {
        boolean exposeEmail = false;
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        return exposeEmail;
    }

    public boolean isStudent(HttpSessionData sessionData) {
        boolean isStudent = false;
        if (sessionData != null && (Role.STUDENT.equals(sessionData.getRole()))) {
            isStudent = true;
        }
        return isStudent;
    }



    public Long getCallingUserId() {
        logger.info("Request for calling user id");
        HttpSessionData sessionData = getCurrentSessionData();
        Long callingUserId = null;
        if (sessionData != null) {
            callingUserId = sessionData.getUserId();
        }

        return callingUserId;
    }

    public Role getCallingUserRole() {
        logger.info("Request for calling user role");
        HttpSessionData sessionData = getCurrentSessionData();
        Role callingUserRole = null;
        if (sessionData != null) {
            callingUserRole = sessionData.getRole();
        }

        return callingUserRole;
    }

    public boolean removeVedTokenFromCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = getCookie(request, ConfigUtils.INSTANCE.getStringValue("auth.cookie.header.token.name"));
        if (cookie != null) {
            cookie.setValue("");
            cookie.setDomain(ConfigUtils.INSTANCE.getStringValue("auth.cookie.domain"));
            cookie.setSecure(ConfigUtils.INSTANCE.getBooleanValue("auth.cookie.secure"));
            cookie.setPath("/");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            return true;
        } else {
            return false;
        }
    }

    public Cookie getCookie(HttpServletRequest req, String cookieName) {
        Cookie cookie = null;
        if (req.getCookies() != null) {
            for (Cookie ck : req.getCookies()) {
                if (ck.getName().equals(cookieName)) {
                    cookie = ck;
                    break;
                }
            }
        }
        logger.info("================ cookieName ==== " + cookieName + " cookie " + cookie);
        return cookie;
    }

    public Map<String, Cookie> getCookiesMap(HttpServletRequest req) {
        Map<String, Cookie> cookiesMap = new HashMap<>();
        if (req.getCookies() != null) {
            for (Cookie ck : req.getCookies()) {
                cookiesMap.put(ck.getName(), ck);
            }
        }
        logger.info("================ cookies map ==== " + cookiesMap);
        return cookiesMap;
    }

    public boolean isAllowedUrl(String requestURI) {
        boolean isAllowed = false;
        if (allowedUrls != null) {
            for (String allowedUrl : allowedUrls) {
                if (requestURI.startsWith(allowedUrl)) {
                    isAllowed = true;
                    break;
                }
            }
        }
        return isAllowed;
    }

    public boolean isAllowedApp(Map<String, Object> requestDetails) {
        boolean isAllowed = false;
        if (requestDetails.get(HttpSessionUtils.Constants.CLIENT) != null
                && requestDetails.get(HttpSessionUtils.Constants.CLIENT_ID) != null
                && requestDetails.get(HttpSessionUtils.Constants.CLIENT_SECRET) != null) {
            String client = (String) requestDetails.get(HttpSessionUtils.Constants.CLIENT);
            String clientId = (String) requestDetails.get(HttpSessionUtils.Constants.CLIENT_ID);
            String clientSecret = (String) requestDetails.get(HttpSessionUtils.Constants.CLIENT_SECRET);
            String storedClientId = getProperty(client, HttpSessionUtils.Constants.CLIENT_ID);
            String storedClientSecret = getProperty(client, HttpSessionUtils.Constants.CLIENT_SECRET);
            if (clientId.equals(storedClientId) && clientSecret.equals(storedClientSecret)) {
                // TODO add better checks
                isAllowed = true;
            }
        }
        return isAllowed;
    }

    public void checkIfUserLoggedIn() throws ForbiddenException {
        HttpSessionData sessionData = getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new ForbiddenException(ErrorCode.USER_LOGIN_REQUIRED, "User is not logged in");
        }
    }
    
    // Verifies if the calling user and logged in user are users of the mentioned role and are same
    public void verifyLoggedInAndCallingUser(Role role, Long userId, String errorMessage) throws ForbiddenException {
    	HttpSessionData sessionData = getCurrentSessionData();
    	if(sessionData == null || !sessionData.getRole().equals(role) || (userId != null && !sessionData.getUserId().equals(userId))) {
    		throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED, errorMessage);
    	}
    }

    public void checkIfAllowedList(Long userId, List<Role> allowedRoles, Boolean isAdminAllowed) throws VException {

        boolean userCheck;
        boolean roleCheck;
        boolean sessionDataNullForAdminCheck = false;

        HttpSessionData sessionData = getCurrentSessionData();

        if (isAdminAllowed) {
            if(Objects.nonNull(sessionData)) {
                if (Role.ADMIN.equals(sessionData.getRole()) || Role.STUDENT_CARE.equals(sessionData.getRole())) {
                    return;
                }
            }else{
                sessionDataNullForAdminCheck=true;
            }
        }

        if (userId != null) {
            userCheck = userId.equals(sessionData.getUserId());
        } else {
            userCheck = true;
        }

        if (allowedRoles != null && !allowedRoles.isEmpty() && null != sessionData) {
            roleCheck = allowedRoles.contains(sessionData.getRole());
        } else {
            roleCheck = true;
        }
        if (!roleCheck || !userCheck || sessionDataNullForAdminCheck) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User forbidden to use this API");
        }
    }

    public void checkIfAllowed(Long userId, Role allowedRole, Boolean isAdminAllowed) throws VException {
        List<Role> roles = null;
        if (allowedRole != null) {
            roles = new ArrayList<>();
            roles.add(allowedRole);
        }
        checkIfAllowedList(userId, roles, isAdminAllowed);
    }

    protected String getProperty(String client, String key) {
        StringBuilder sb = new StringBuilder();
        sb.append(client).append(".").append(key);
        return ConfigUtils.INSTANCE.getStringValue(sb.toString());
    }

    public void isAllowedApp(HttpServletRequest servletRequest) throws VException {
        Map<String, Object> requestDetails = new HashMap<>();
        requestDetails.put("requestURI", servletRequest.getRequestURI());
        requestDetails.put(HttpSessionUtils.Constants.CLIENT, servletRequest.getHeader("X-Ved-Client"));
        requestDetails.put(HttpSessionUtils.Constants.CLIENT_ID, servletRequest.getHeader("X-Ved-Client-Id"));
        requestDetails.put(HttpSessionUtils.Constants.CLIENT_SECRET, servletRequest.getHeader("X-Ved-Client-Secret"));
        if (!isAllowedApp(requestDetails)) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Only apps are allowed to access");
        }
    }

    public void fillAbstractFrontEndReq(AbstractFrontEndReq req) {
        HttpSessionData httpSessionData = getCurrentSessionData();
        if (req != null && httpSessionData != null) {
            req.setCallingUserId(httpSessionData.getUserId());
            req.setCallingUserRole(httpSessionData.getRole());
        }
    }

    public String appendSessionDataToQueryString(String queryString) {
        if (StringUtils.isEmpty(queryString)) {
            queryString = "";
        }
        // filtering out data params sent from user, as they cannot be relied on
        String[] queryStringParams = queryString.split("&");
        List<String> queryStringParamList = new ArrayList<>();
        List<String> sessionParamsKeys = new ArrayList<>();
        sessionParamsKeys.add("sessionUserId");
        sessionParamsKeys.add("callingUserId");
        sessionParamsKeys.add("sessionUserRole");
        sessionParamsKeys.add("callingUserRole");
        for (int k = 0; k < queryStringParams.length; k++) {
            String queryParam = queryStringParams[k];
            String[] splits = queryParam.split("=");
            if (splits.length == 2 && sessionParamsKeys.indexOf(splits[0]) == -1) {
                queryStringParamList.add(queryParam);
            }
        }

        // final query string without session params
        queryString = String.join("&", queryStringParamList);
        logger.info("queryString filtered " + queryString);

        // adding session params to query string
        HttpSessionData httpSessionData = getCurrentSessionData();
        List<String> params = new ArrayList<>();
        if (httpSessionData != null) {
            params.add("sessionUserId=" + httpSessionData.getUserId().toString());
            params.add("callingUserId=" + httpSessionData.getUserId().toString());
            params.add("sessionUserRole=" + httpSessionData.getRole().name());
            params.add("callingUserRole=" + httpSessionData.getRole().name());
        }
        if (ArrayUtils.isNotEmpty(params)) {
            queryString = String.join("&", params) + "&" + queryString;// prepending
            // it so
            // it
            // gets
            // priority
        }
        logger.info("queryString with session params " + queryString);
        return queryString;
    }

    public static class Constants {

        public static final String CLIENT = "CLIENT";
        public static final String CLIENT_ID = "CLIENT.ID";
        public static final String CLIENT_SECRET = "CLIENT.SECRET";
        public static final String AWS_CHIME_REQUEST_HEADER = "AWS_CHIME_REQUEST_HEADER";
    }

}
