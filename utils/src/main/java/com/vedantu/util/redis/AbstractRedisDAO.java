/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.redis;

import com.google.common.collect.Lists;
import com.vedantu.exception.*;
import com.vedantu.util.*;
import com.vedantu.util.managers.PropertiesManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisDataException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ajith
 */
/**
 * @author ajith Reference:
 * http://www.programcreek.com/java-api-examples/index.php?source_dir=AIDR-master/aidr-persister/src/main/java/qa/qcri/aidr/redis/JedisConnectionPool.java
 */
@Service
public class AbstractRedisDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AbstractRedisDAO.class);

    private static JedisPool jedisPool;

    ExecutorService executorService = Executors.newFixedThreadPool(1);
    private static final JedisPubSub[] jedisPubSub = new JedisPubSub[1];

    @Autowired
    PropertiesManager propertiesManager;

    public AbstractRedisDAO() {

    }

    @PostConstruct
    public void init() {
        try {
            String host = ConfigUtils.INSTANCE.getStringValue("redis.host");
            String portString = ConfigUtils.INSTANCE.getStringValue("redis.port");
            if (StringUtils.isEmpty(host) || StringUtils.isEmpty(portString)) {
                logger.info("Either host or port for rredis is null. host - " + host + " port - " + portString);
                return;
            }
            Integer port = Integer.parseInt(portString);
            logger.info("creating redis connection with " + host + ":" + port);
            //         jedisClient = new Jedis(host, port);
            if (jedisPool == null) {
                JedisPoolConfig config = new JedisPoolConfig();
                config.setMaxTotal(200);
                config.setMaxIdle(30);
                config.setMinIdle(20);
                config.setMaxWaitMillis(2000);
                jedisPool = new JedisPool(config, host, port);
                createRedisSubscriber();
                if (jedisPool == null) {
                    logger.info("connection not created");
                } else {
                    assignExpiryListener(jedisPool);
                    logger.info("connection  created successfully");
                }
            }
        } catch (JedisConnectionException e) {
            logger.error("Could not establish Redis connection. Is the Redis running?");
            throw e;
        }
    }

    /**
     * @Intent : Update application.properties on the fly, when application is
     * running.
     * @HowToDo : 1. UPDATE application properties in redis. 1.1 KEY =
     * (serviceName_application.properties) Ex.(User_application.properties) 1.2
     * VALUE = Json containing property key and property value. EX
     * "{\"USER_ENDPOINT\" : \"https://user.vedantu.com/user\",
     * \"PLATFORM_ENDPOINT\" : \"https://platform.vedantu.com/platform\"}" 2.
     * PUBLISH the event in redis 2.1 PUBLISH channel message 2.2 channel name :
     * UPDATE_APPLICATION_PROPERTIES_serviceName. Ex .
     * UPDATE_APPLICATION_PROPERTIES_User
     *
     */
    private void createRedisSubscriber() {
        String serviceName = ConfigUtils.INSTANCE.properties.getProperty("application.name");
        String propChannelName = "UPDATE_APPLICATION_PROPERTIES_" + serviceName;
        String securityChannelName = "UPDATE_SECURITY_PROPERTIES_" + serviceName;
        Jedis jedis = getRedisClient();

        CompletableFuture.runAsync(() -> {
            try {

                jedisPubSub[0] = new JedisPubSub() {
                    @Override
                    public void onMessage(String channel, String message) {
                        System.out.println("Channel " + channel + " has sent a message : " + message);
                        if (channel.equals(propChannelName)) {
                            propertiesManager.setApplicationPropertiesFromRedis();
                            propertiesManager.refreshSpringBean();
                        }
                        if (channel.equals(securityChannelName)) {
                            propertiesManager.reloadEnvSecurityProperties();
                            propertiesManager.refreshSpringBean();
                        }
                    }

                    @Override
                    public void onSubscribe(String channel, int subscribedChannels) {
                        System.out.println("Client is Subscribed to channel : " + channel);
                        System.out.println("Client is Subscribed to " + subscribedChannels + " no. of channels");
                    }

                    @Override
                    public void onUnsubscribe(String channel, int subscribedChannels) {
                        System.out.println("Client is Unsubscribed from channel : " + channel);
                        System.out.println("Client is Subscribed to " + subscribedChannels + " no. of channels");
                    }
                };

                jedis.subscribe(jedisPubSub[0], propChannelName, securityChannelName);
            } catch (Exception e) {
                System.out.println("Error in jedis pub sub" + e);
            } finally {
                executorService.shutdownNow();
            }
        }, executorService);
    }

    public void assignExpiryListener(JedisPool jedisPool) {

    }

    private Jedis getRedisClient() {
//        logger.info("current jedisPool Num active " + jedisPool.getNumActive());
//        logger.info("current jedisPool Num idle " + jedisPool.getNumIdle());
//        logger.info("current jedisPool Num waiters " + jedisPool.getNumWaiters());
        return jedisPool.getResource();
        //    return jedisClient;
    }

    private void closeRedisClient(Jedis jedisClient) {
        if (jedisClient != null) {
            logger.info("closing jedis client");
            jedisClient.close();
        }
    }

    public String loadScriptAndEval(String script, String key, String requests, String time) throws InternalServerErrorException, RateLimitException {
        Jedis jedisClient = null;
        String resp = null;
        String checkBlocked = null;
        try {
            jedisClient = getRedisClient();
            checkBlocked = jedisClient.get("BL:" + key);
            if (checkBlocked != null) {
                throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");
            } else {
                Object result = jedisClient.eval(script, 1, key, requests, time);
                logger.info("RL: KEY: " + key + " SCORE: " + result);
            }
        } catch (JedisDataException e) {
            if (checkBlocked == null) {
                int blockTime = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("rl.blockTime"));
                jedisClient.setex("BL:" + key, blockTime, requests);
                logger.error("USER BLOCKED: " + key + "Requests: " + requests);
            }
            throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");

        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long loadScriptAndEval(String script, String key) throws InternalServerErrorException{
        Jedis jedisClient = null;
        Long result = 0L;
        try {
            jedisClient = getRedisClient();
            result = (Long) jedisClient.eval(script, 1, key);
            logger.info("loadScriptAndEval {}",result);
        } catch (JedisDataException e) {
            logger.error("Some error in loadScript for key  " + key +  ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public String set(String key, String value) throws InternalServerErrorException {
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.set(key, value);
        } catch (Exception e) {
            logger.error("Some error in set " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public void setjustex(String key, int expiryInSeconds) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.expire(key, expiryInSeconds);
        } catch (Exception e) {
            logger.error("Some error in setjustex " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
//        return resp;
    }

    public String setex(String key, String value, int expiryInSeconds) throws InternalServerErrorException {
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.setex(key, expiryInSeconds, value);
        } catch (Exception e) {
            logger.error("Some error in setex " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public String get(String key) throws InternalServerErrorException, BadRequestException {
        logger.info("KEY__ is {}",key);
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.get(key);
        } catch (Exception e) {
            logger.error("Some error in get " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long del(String key) throws InternalServerErrorException, BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.del(key);
        } catch (Exception e) {
            logger.error("Some error in del " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long hdel(String key, String... fields) throws InternalServerErrorException, BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.hdel(key, fields);
        } catch (Exception e) {
            logger.error("Some error in hdel " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    // FIXME uses scanning operation, probably avoid running on main redis server
    public List<String> getMultipleValues(String keyPattern) throws InternalServerErrorException {
        Jedis jedisClient = null;
        List<String> values = null;
        try {
            String stackTrace = ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis"));
            logger.error("FIXME: using scanning ops on redis, key pattern {} stack trace {}",
                    keyPattern,
                    stackTrace);
            jedisClient = getRedisClient();
            Set<String> foundKeys = jedisClient.keys(keyPattern);
            if (foundKeys.isEmpty()) {
                return null;
            }
            if (foundKeys.size() > 500) {
                logger.error("FIXME: using multi get on redis, elements {}, stack trace {}",
                        foundKeys.size(),
                        stackTrace);
            }
            values = jedisClient.mget(foundKeys.toArray(new String[foundKeys.size()]));
        } catch (Exception e) {
            logger.error("Some error in getMultipleValues "
                    + keyPattern + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return values;
    }

    public Set<String> getZRevRange(String key, long start, long end) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Set<String> resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.zrevrange(key, start, end);
            if (resp != null && resp.size() > 500) {
                logger.error("FIXME: using zrevrange on redis, elements size {} stack trace {}",
                        resp.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(log(N)+M) Operation On Redis")));
            }
        } catch (Exception e) {
            logger.error("Some error in get " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long zremrangebyscore(String key, long start, long end) throws InternalServerErrorException, BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.zremrangeByScore(key, start, end);
            if (resp != null && resp > 500) {
                logger.error("FIXME: using zremrangeByScore on redis, removed elements {} stack trace {}",
                        resp,
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(log(N)+M) Operation On Redis")));
            }
        } catch (Exception e) {
            logger.error("Some error in del " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Map<String, String> getValuesForKeys(Set<String> keys) throws InternalServerErrorException {
        return getValuesForKeys(new ArrayList<>(keys));
    }

    public Map<String, String> getValuesForKeys(List<String> keys) throws InternalServerErrorException {
        Map<String, String> responseMap = new HashMap<>();
        if (CollectionUtils.isEmpty(keys)) {
            return responseMap;
        }
        Jedis jedisClient = null;
        List<String> values = null;
        try {
            jedisClient = getRedisClient();
            values = jedisClient.mget(keys.toArray(new String[keys.size()]));
            if (values != null && values.size() > 500) {
                logger.error("FIXME: using mget on redis, elements size {} stack trace {}",
                        values.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
            if (CollectionUtils.isNotEmpty(values)) {
                Iterator<String> keyIterator = keys.iterator();
                Iterator<String> valueIterator = values.iterator();

                while (keyIterator.hasNext() && valueIterator.hasNext()) {
                    String key = keyIterator.next();
                    String value = valueIterator.next();
                    responseMap.put(key, value);
                }
            }
        } catch (Exception e) {
            logger.error("Some error in getValuesForKeys "
                    + keys + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return responseMap;
    }

    public void setKeyPairs(Map<String, String> keyPair) throws InternalServerErrorException {
        if (keyPair == null || keyPair.isEmpty()) {
            return;
        }
        Jedis jedisClient = null;
        try {
            if (keyPair.size() > 500) {
                logger.error("FIXME: using set with pipeline on redis, elements size {} stack trace {}",
                        keyPair.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
            jedisClient = getRedisClient();
            Pipeline p = jedisClient.pipelined();
            for (Map.Entry<String, String> entry : keyPair.entrySet()) {
                p.set(entry.getKey(), entry.getValue());
            }
            p.sync();
        } catch (Exception e) {
            logger.error("Some error in setKeyPairs "
                    + keyPair + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
    }

    public String eval(String script, List<String> keys, List<String> values) throws InternalServerErrorException {
        String resp = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            resp = (String) jedisClient.eval(script, keys, values);
        } catch (Exception e) {
            logger.error("Some error in eval  "
                    + script + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long increment(String key, long value) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.incrBy(key, value);
        } catch (Exception e) {
            logger.error("Some error in increment " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        logger.info("New value " + key + ": " + resp);
        return resp;
    }

    public Long incr(String key) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.incr(key);
        } catch (Exception e) {
            logger.error("Some error in increment " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        logger.info("New value " + key + ": " + resp);
        return resp;
    }

    public Long decr(String key) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.decr(key);
        } catch (Exception e) {
            logger.error("Some error in decrement " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        logger.info("New value " + key + ": " + resp);
        return resp;
    }

    public Long incrAndExpireAt(String key, long expireAtUnixTime) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = 0L;
        try {
            jedisClient = getRedisClient();
            Transaction transaction = jedisClient.multi();
            Response<Long> incr = transaction.incr(key);
            transaction.expireAt(key, expireAtUnixTime);
            transaction.exec();
            Long count = incr.get();
            logger.info("New value " + key + ": " + count);
            return count;
        } catch (Exception e) {
            logger.error("Some error in increment " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long publish(String channel, String value) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.publish(channel, value);
        } catch (Exception e) {
            logger.error("Some error in publish to channel " + channel + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        logger.info("Published event to channel" + channel + ", " + value + ": " + resp);
        return resp;
    }

    public Long deleteKeys(String[] keys) throws InternalServerErrorException {
        if (keys == null || keys.length == 0) {
            logger.info("Keys for deletion is empty");
            return 0L;
        }
        Jedis jedisClient = null;
        Long resp = 0L;
        try {
            logger.info("total no of keys requested for del {}", keys.length);
            jedisClient = getRedisClient();
            if (keys.length > 500) {
                logger.error("FIXME: using del on redis, elements size {} stack trace {}",
                        keys.length,
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
            resp = jedisClient.del(keys);
        } catch (Exception e) {
            logger.error("Some error in deleting list" + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public void setexKeyPairs(Map<String, String> keyPair, int expiry) throws InternalServerErrorException {
        if (keyPair == null || keyPair.isEmpty()) {
            return;
        }
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            if (keyPair.size() > 500) {
                logger.error("FIXME: using setex with pipeline on redis, elements size {} stack trace {}",
                        keyPair.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
            Pipeline p = jedisClient.pipelined();
            for (Map.Entry<String, String> entry : keyPair.entrySet()) {
                p.setex(entry.getKey(), expiry, entry.getValue());
            }
            p.sync();
        } catch (Exception e) {
            logger.error("Some error in setKeyPairs "
                    + keyPair + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
    }
    //https://tech.trivago.com/2017/01/25/learn-redis-the-hard-way-in-production/
    // commenting this as scan is costly operation - may lead to redis socket connection timeout- failing every API.
//    public void deleteAllMatchingKeys(String pattern) {
//        Set<String> matchingKeys = new HashSet<>();
//        ScanParams params = new ScanParams();
//        params.match(pattern + "*");
//        Jedis jedisClient = null;
//        try {
//            jedisClient = getRedisClient();
//            String nextCursor = "0";
//            do {
//                ScanResult<String> scanResult = jedisClient.scan(nextCursor, params);
//                List<String> keys = scanResult.getResult();
//                nextCursor = scanResult.getStringCursor();
//                matchingKeys.addAll(keys);
//            } while (!nextCursor.equals("0"));
//            if (!matchingKeys.isEmpty()) {
//                jedisClient.del(matchingKeys.toArray(new String[0]));
//            }
//        } catch (Exception e) {
//            logger.error("Error in deleting keys with error", e);
//        } finally {
//            closeRedisClient(jedisClient);
//        }
//    }

//    public void resetAllMatchingKeys(String pattern) {
//        Set<String> matchingKeys = new HashSet<>();
//        ScanParams params = new ScanParams();
//        params.match(pattern + "*");
//        try (Jedis jedis = new JedisPool().getResource()) {
//            String nextCursor = "0";
//            do {
//                ScanResult<String> scanResult = jedis.scan(nextCursor, params);
//                List<String> keys = scanResult.getResult();
//                nextCursor = scanResult.getStringCursor();
//                matchingKeys.addAll(keys);
//            } while (!nextCursor.equals("0"));
//            if (matchingKeys.isEmpty()) {
//                return;
//            }
//            matchingKeys.stream()
//                    .forEach(key -> jedis.set(key, ""));
//        }
//    }

    /*public void setHashKeyField(String parentKey, String field, String value){
        
        Jedis jedisClient = null;
        try{
            jedisClient = getRedisClient();
            
            jedisClient.hset(parentKey, field, value);
            
        }catch(Exception ex){
            logger.error("Some error in setHashKeyPair " + field + ", Error: " + ex.getMessage());
        }finally{
            closeRedisClient(jedisClient);
        }
        
    }
    
    public String getHashKeyField(String parentKey, String field) throws BadRequestException{
        if (StringUtils.isEmpty(parentKey) || StringUtils.isEmpty(field)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.hget(parentKey, field);
        } catch (Exception e) {
            logger.error("Some error in get " + parentKey +" "+ field + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;        
    }
    
    public Set<String> getKeys(String pattern) throws BadRequestException{
        Set<String> keys = new HashSet<>();
        if(StringUtils.isEmpty(pattern)){
            throw new BadRequestException(ErrorCode.EMPTY_PATTERN, "Empty pattern not allowed");
        }
        Jedis jedisClient = null;
        try{
            jedisClient = getRedisClient();
            keys = jedisClient.keys(pattern);
        }catch(Exception ex){
            logger.error("Error in getting keys for pattern: "+pattern+" exception: "+ex.getMessage());
        }finally{
            closeRedisClient(jedisClient);
        }
        return keys;
    }  
     */
    public Map<String, String> getAllHashFieldValues(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Map<String, String> results = new HashMap<>();
        Jedis jedisClient = null;

        try {
            jedisClient = getRedisClient();
            results = jedisClient.hgetAll(key);
            if (results != null && results.size() > 500) {
                logger.error("FIXME: using hgetall on redis, elements size {} stack trace {}",
                        results.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
        } catch (Exception ex) {
            logger.error("Some error in getting all the key value for hashkey: " + key + " exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return results;
    }

    public String hmset(final String hashBucketKey, Map<String, String> hash) throws BadRequestException {
        if (StringUtils.isEmpty(hashBucketKey)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        String res = null;

        try {
            jedisClient = getRedisClient();
            if (hash != null && hash.size() > 500) {
                logger.error("FIXME: using hmset on redis, elements size {} stack trace {}",
                        hash.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
            res = jedisClient.hmset(hashBucketKey, hash);
        } catch (Exception ex) {
            logger.error("Some error in setting all the key value for hash key: " + hashBucketKey + " exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return res;
    }

    public Long hdel(final String bucketKey, final String field) throws BadRequestException {
        if (StringUtils.isEmpty(bucketKey) || StringUtils.isEmpty(field)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "key/bucketKey can not be empty");
        }
        Jedis jedisClient = null;
        Long res = 0L;

        try {
            jedisClient = getRedisClient();
            res = jedisClient.hdel(bucketKey, field);
        } catch (Exception ex) {
            logger.warn("Some error in deleting bucketKey: {} field: {} exception: {}", bucketKey, field, ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return res;
    }

    public Long hset(final String bucketKey, final String field, final String value) throws BadRequestException {
        if (StringUtils.isEmpty(bucketKey) || StringUtils.isEmpty(field)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "key/bucketKey can not be empty");
        }
        Jedis jedisClient = null;
        Long res = 0L;

        try {
            jedisClient = getRedisClient();
            res = jedisClient.hset(bucketKey, field, value);
        } catch (Exception ex) {
            logger.warn("Some error in setting bucketKey: {} field: {} exception: {}", bucketKey, field, ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return res;
    }

    public String hget(final String bucketKey, final String field) throws BadRequestException {
        if (StringUtils.isEmpty(bucketKey) || StringUtils.isEmpty(field)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "key/bucketKey can not be empty");
        }
        Jedis jedisClient = null;
        String res = null;

        try {
            jedisClient = getRedisClient();
            res = jedisClient.hget(bucketKey, field);
        } catch (Exception ex) {
            logger.warn("Some error in getting bucketKey: {} field: {} exception: {}", bucketKey, field, ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return res;
    }

    public Long hlen(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        Long res = 0L;

        try {
            jedisClient = getRedisClient();
            res = jedisClient.hlen(key);
        } catch (Exception ex) {
            logger.error("Some error in setting all the key value for hashkey: " + key + " exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return res;
    }

    public void addToSet(String key, String value) throws BadRequestException {
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key or value is null");
        }
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.sadd(key, value);

        } catch (Exception ex) {
            logger.error("Error in adding value to set: " + value + " to key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }

    }

    public void addToList(String key, String value) throws BadRequestException {
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key or value is null");
        }
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.rpush(key, value);

        } catch (Exception ex) {
            logger.error("Error in adding value to list: " + value + " to key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }

    }

    public void addToListLeft(String key, String value) throws BadRequestException {
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key or value is null");
        }
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.lpush(key, value);

        } catch (Exception ex) {
            logger.error("Error in adding value to list: " + value + " to key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }

    }

    public Set<String> getSetMembers(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Set<String> result = new HashSet<>();
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.smembers(key);
            if (result != null && result.size() > 500) {
                logger.error("FIXME: using smembers on redis, elements size {} stack trace {}",
                        result.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
        } catch (Exception ex) {
            logger.error("Error in get set for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public List<String> getList(String key, long start, long end) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        List<String> result = new ArrayList<>();
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.lrange(key, start, end);
            if (result != null && result.size() > 500) {
                logger.error("FIXME: using lrange on redis, elements size {} stack trace {}",
                        result.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
        } catch (Exception ex) {
            logger.error("Error in get list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public Set<String> getSet(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Set<String> result = new HashSet<>();
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.smembers(key);
            if (result != null && result.size() > 500) {
                logger.error("FIXME: using smembers on redis, elements size {} stack trace {}",
                        result.size(),
                        ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.REDIS_COSTLY_OPERATION, "O(N) Operation On Redis")));
            }
        } catch (Exception ex) {
            logger.error("Error in get list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public String getElementByIndex(String key, long index) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        String result = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.lindex(key, index);
        } catch (Exception ex) {
            logger.error("Error in get list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public String popFromList(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        String result = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.lpop(key);
        } catch (Exception ex) {
            logger.error("Error in get list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public void trimList(String key, long start, long end) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.ltrim(key, start, end);
        } catch (Exception ex) {
            logger.error("Error in trimming list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
    }

    public Long getLengthOfList(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Long result = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.llen(key);
        } catch (Exception ex) {
            logger.error("Error in gettung length of list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public void popFromSet(String key, String value) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Long result = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.srem(key, value);
        } catch (Exception ex) {
            logger.error("Error in removing element from set, key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
    }


    public String getBucketedNotificationRedisKey(String env, String emailId) {
        return env + "_BUCKET_" + emailId;
    }

    public String getDoubtPoolKey(String env, String doubtId) {
        return env + "_DOUBT_T1_POOL_" + doubtId;
    }

    public String getDoubtChatKey(String env, String doubtId) {
        return env + "_DOUBT_T1_CHAT_" + doubtId;
    }

    public String getUserLatestSessionCacheKey(Long userId) {
        if (userId == null) {
            return null;
        } else {
            return "user_latest_session_" + userId;
        }
    }

    public String getUserLatestSessionCacheKey(String userId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        } else {
            return "user_latest_session_" + userId;
        }
    }

    public String getCachedKeyForUserLatestSessions(String userId, String env) {
        return env + "_USER_3_SESSIONS_" + userId;
    }

    public String getBucketKeyForObjectIdStr(String hashNamePrefix, String objectId, int maxNumberOfBuckets) {
        int bucketNumber = new ObjectId(objectId).getTimestamp() % maxNumberOfBuckets;
        return hashNamePrefix + String.valueOf(bucketNumber);
    }

    public boolean setnx(String key, String value, int seconds) {
        Jedis jedisClient = null;
        boolean success;
        try {
            jedisClient = getRedisClient();
            success = jedisClient.setnx(key, value) == 1L;
            logger.info("success + " + success);
            if(seconds > -1 && success){
                jedisClient.expire(key, seconds);
            }
        } catch (Exception e) {
            success = false;
            logger.error("Some error while setnx " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return success;

    }

    public void setbit(final String key, final long offset, final boolean val) {
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.setbit(key, offset, val);
        } catch (Exception e) {
            logger.warn("Some error while setbit " + key + ", " + val + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
    }

    public boolean getbit(final String key, final long offset) {
        Jedis jedisClient = null;
        boolean bitAtOffset = false;
        try {
            jedisClient = getRedisClient();
            bitAtOffset = jedisClient.getbit(key, offset);
        } catch (Exception e) {
            logger.warn("Some error while getbit {} offset : {} Error: {}", key, offset, e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return bitAtOffset;
    }

    public List<Long> bitfield(final String key, final String... ops) {
        Jedis jedisClient = null;

        try {
            jedisClient = getRedisClient();
            return jedisClient.bitfield(key, ops);
        } catch (Exception e) {
            logger.warn("Some error while bitfield: {}, Error: {}", key, e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }

        return null;
    }

    //    public static void main(String[] args) {
//        Jedis jedis = new Jedis("localhost");
//        String script = "if redis.call('GET', KEYS[1]) then return redis.call('GET', KEYS[1]) else redis.call('SETEX', KEYS[1],ARGV[1],ARGV[2]) return ARGV[2] end";
//        List<String> keys = new ArrayList<>();
//        keys.add("key11");
//        List<String> values = new ArrayList<>();
//        values.add("5");
//        values.add("val21");
//        String resp = (String) jedis.eval(script, keys, values);
//        System.err.println("resp>>>>> " + resp);
//    }
    @PreDestroy
    public void cleanUp() {
        try {
            if (jedisPool != null && !jedisPool.isClosed()) {
                jedisPool.close();
                jedisPool = null;
            }
            if (jedisPubSub != null && jedisPubSub[0] != null && jedisPubSub[0].isSubscribed()) {
                jedisPubSub[0].unsubscribe();
            }
        } catch (Exception e) {
            logger.error("Error in closing redis connection pool ", e);
        }
    }
}
