package com.vedantu.util.redis;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.RateLimitException;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.managers.PropertiesManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisDataException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RateLimitingRedisDao {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AbstractRedisDAO.class);

    private static JedisPool jedisPool;

    @Autowired
    PropertiesManager propertiesManager;

    public RateLimitingRedisDao() {

    }

    @PostConstruct
    public void init() {
        try {
            String host = ConfigUtils.INSTANCE.getStringValue("ratelimit.redis.host");
            String portString = ConfigUtils.INSTANCE.getStringValue("ratelimit.redis.port");
            if (StringUtils.isEmpty(host) || StringUtils.isEmpty(portString)) {
                logger.info("Either host or port for rredis is null. host - " + host + " port - " + portString);
                return;
            }
            Integer port = Integer.parseInt(portString);
            logger.info("creating redis connection with " + host + ":" + port);
            if (jedisPool == null) {
                JedisPoolConfig config = new JedisPoolConfig();
                config.setMaxTotal(200);
                config.setMaxIdle(30);
                config.setMinIdle(20);
                config.setMaxWaitMillis(200);
                jedisPool = new JedisPool(config, host, port);
                if (jedisPool == null) {
                    logger.info("connection not created");
                } else {
                    assignExpiryListener(jedisPool);
                    logger.info("connection  created successfully");
                }
            }
        } catch (JedisConnectionException e) {
            logger.error("Could not establish Redis connection. Is the Redis running?");
            throw e;
        }
    }


    public void assignExpiryListener(JedisPool jedisPool) {

    }

    private Jedis getRedisClient() {
        return jedisPool.getResource();
    }

    private void closeRedisClient(Jedis jedisClient) {
        if (jedisClient != null) {
            logger.info("closing jedis client");
            jedisClient.close();
        }
    }

    public String loadScriptAndEval(String script, String key, String requests, String time) throws InternalServerErrorException, RateLimitException {
        Jedis jedisClient = null;
        String resp = null;
        String checkBlocked = null;
        try {
            jedisClient = getRedisClient();
            checkBlocked = jedisClient.get("BL:" + key);
            if (checkBlocked != null) {
                throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");
            } else {
                Object result = jedisClient.eval(script, 1, key, requests, time);
                logger.info("RL: KEY: " + key + " SCORE: " + result);
            }
        } catch (JedisDataException e) {
            if (checkBlocked == null) {
                int blockTime = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("rl.blockTime"));
                jedisClient.setex("BL:" + key, blockTime, requests);
                logger.error("USER BLOCKED: " + key + "Requests: " + requests);
            }
            throw new RateLimitException(ErrorCode.RATE_LIMIT_EXCEEDED, "rate limit exceeded");

        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public String set(String key, String value) throws InternalServerErrorException {
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.set(key, value);
        } catch (Exception e) {
            logger.error("Some error in set " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }


    public String setex(String key, String value, int expiryInSeconds) throws InternalServerErrorException {
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.setex(key, expiryInSeconds, value);
        } catch (Exception e) {
            logger.error("Some error in setex " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public String get(String key) throws InternalServerErrorException, BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        String resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.get(key);
        } catch (Exception e) {
            logger.error("Some error in get " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long del(String key) throws InternalServerErrorException, BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key value is null");
        }
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.del(key);
        } catch (Exception e) {
            logger.error("Some error in del " + key + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }


    public Map<String, String> getValuesForKeys(Set<String> keys) throws InternalServerErrorException {
        return getValuesForKeys(new ArrayList<>(keys));
    }

    public Map<String, String> getValuesForKeys(List<String> keys) throws InternalServerErrorException {
        Map<String, String> responseMap = new HashMap<>();
        if (CollectionUtils.isEmpty(keys)) {
            return responseMap;
        }
        Jedis jedisClient = null;
        List<String> values = null;
        try {
            jedisClient = getRedisClient();
            values = jedisClient.mget(keys.toArray(new String[keys.size()]));
            if (CollectionUtils.isNotEmpty(values)) {
                Iterator<String> keyIterator = keys.iterator();
                Iterator<String> valueIterator = values.iterator();

                while (keyIterator.hasNext() && valueIterator.hasNext()) {
                    String key = keyIterator.next();
                    String value = valueIterator.next();
                    responseMap.put(key, value);
                }
            }
        } catch (Exception e) {
            logger.error("Some error in getValuesForKeys "
                    + keys + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return responseMap;
    }


    public String eval(String script, List<String> keys, List<String> values) throws InternalServerErrorException {
        String resp = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            resp = (String) jedisClient.eval(script, keys, values);
        } catch (Exception e) {
            logger.error("Some error in eval  "
                    + script + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return resp;
    }

    public Long increment(String key, long value) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.incrBy(key, value);
        } catch (Exception e) {
            logger.error("Some error in increment " + key + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        logger.info("New value " + key + ": " + resp);
        return resp;
    }

    public Long publish(String channel, String value) throws InternalServerErrorException {
        Jedis jedisClient = null;
        Long resp = null;
        try {
            jedisClient = getRedisClient();
            resp = jedisClient.publish(channel, value);
        } catch (Exception e) {
            logger.error("Some error in publish to channel " + channel + ", " + value + ", Error: " + e.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        logger.info("Published event to channel" + channel + ", " + value + ": " + resp);
        return resp;
    }


    public void addToSet(String key, String value) throws BadRequestException {
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key or value is null");
        }
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            jedisClient.sadd(key, value);

        } catch (Exception ex) {
            logger.error("Error in adding value to set: " + value + " to key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }

    }

    public List<String> getList(String key, long start, long end) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        List<String> result = new ArrayList<>();
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.lrange(key, start, end);
        } catch (Exception ex) {
            logger.error("Error in get list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public Set<String> getSetMembers(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Set<String> result = new HashSet<>();
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.smembers(key);
        } catch (Exception ex) {
            logger.error("Error in get set for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    public Long getLengthOfList(String key) throws BadRequestException {
        if (StringUtils.isEmpty(key)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Key is null");
        }
        Long result = null;
        Jedis jedisClient = null;
        try {
            jedisClient = getRedisClient();
            result = jedisClient.llen(key);
        } catch (Exception ex) {
            logger.error("Error in gettung length of list for key: " + key + ",  exception: " + ex.getMessage());
        } finally {
            closeRedisClient(jedisClient);
        }
        return result;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (jedisPool != null && !jedisPool.isClosed()) {
                jedisPool.close();
                jedisPool = null;
            }
        } catch (Exception e) {
            logger.error("Error in closing redis connection pool ", e);
        }
    }
}
