/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith for seeing all possible values, go to the below link
 * https://github.com/nielsbasjes/yauaa/blob/master/analyzer/src/main/resources/UserAgents/OperatingSystemDeviceNames.yaml
 */
@Service
public class UserAgentParser {

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(UserAgentParser.class);

    UserAgentAnalyzer uaa = null;

    public UserAgentParser() {
        logger.info("initing UserAgentAnalyzer");
        uaa = UserAgentAnalyzer
                .newBuilder()
                .hideMatcherLoadStats()
                .withCache(10000)
                .build();
        logger.info("inited UserAgentAnalyzer");
    }

    public UserAgent getUserAgentDetails(String userAgentStr) {
        UserAgent agent = uaa.parse(userAgentStr);

        for (String fieldName : agent.getAvailableFieldNamesSorted()) {
            logger.info(fieldName + " = " + agent.getValue(fieldName));
        }
        return agent;
    }

}
