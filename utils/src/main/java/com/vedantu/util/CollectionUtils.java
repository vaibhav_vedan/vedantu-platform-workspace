package com.vedantu.util;

import java.util.*;
import java.util.function.Supplier;

public class CollectionUtils {

	public static <E extends Object> boolean isEmpty(Collection<E> collection) {

		return null == collection || collection.isEmpty();
	}

	public static <E extends Object> boolean isNotEmpty(Collection<E> collection) {

		return !isEmpty(collection);
	}

	public static <C extends Collection<E>,E> C defaultIfNull(C collection, Supplier<C> supplier) {
		return collection == null ? supplier.get() : collection;
	}


	public static <K, V> V putIfAbsentAndGet(Map<K, V> map, K key, Supplier<? extends V> val ) {
		V v = map.get(key);
		if (v == null) {
			v = val.get();
			map.put(key, v);
		}
		return v;
	}

	public static <K extends Comparable<? super K>, V> TreeMap<K, V> getTreeMapNullFirst() {
		Comparator<? super K> comparator = Comparator.nullsFirst(Comparator.naturalOrder());
		return new TreeMap<>(comparator);
	}

	public static <E> List<E> getEmptyArrayListIfNull(List<E> elements) {
		return Collections.unmodifiableList(getEmptyMutableListIfNull(elements, ArrayList::new));
	}

	public static <E> List<E> getEmptyMutableListIfNull(List<E> elements, Supplier<List<E>> supplier) {
		return elements == null ? supplier.get() : elements;
	}
}