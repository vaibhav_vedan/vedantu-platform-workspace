package com.vedantu.util.threadutil;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author mano
 */
@Service
public final class ForkJoinPoolUtility {

    private final ForkJoinPool pool;

    public ForkJoinPoolUtility() {
        pool = new ForkJoinPool(10);
    }

    private Logger logger = LogFactory.getLogger(ForkJoinPoolUtility.class);

    /**
     * Should not be exposed. Keep private
     * @return executor
     */
    private ExecutorService getPool() {
        return pool;
    }

    public void execute(RecursiveAction command) {
        pool.execute(command);
    }

    public <T> Future<T> submit(ForkJoinTask<T> task) {
        return pool.submit(task);
    }

    public boolean isShutdown() {
        return pool.isShutdown();
    }

    public boolean isTerminated() {
        return pool.isTerminated();
    }

    /**
     * Should not expose shutdown
     */
    public void shutdown() {
        throw new UnsupportedOperationException("shutdown will happen at server restart.");
    }

    /**
     * Should not expose shutdown now
     */
    public List<Runnable> shutdownNow() {
        throw new UnsupportedOperationException("shutdown will happen at server restart.");
    }

    /**
     * Should not expose await termination
     */
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        throw new UnsupportedOperationException("cannot terminate the executor explicitly. will terminate when server restarts");
    }

    @PreDestroy
    private void close() {
        try {
            logger.info("cached thread pool utility executor service shutdown");
            pool.shutdown();
            logger.info("cached thread pool utility executor service shutdown done");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
