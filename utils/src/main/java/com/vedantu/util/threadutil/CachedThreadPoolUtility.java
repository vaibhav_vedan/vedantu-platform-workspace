package com.vedantu.util.threadutil;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author mano
 */
@Service
public final class CachedThreadPoolUtility {

    private final ExecutorService executorService;

    public CachedThreadPoolUtility() {
        executorService = Executors.newCachedThreadPool();
    }

    private Logger logger = LogFactory.getLogger(CachedThreadPoolUtility.class);

    /**
     * Should not be exposed. Keep private
     * @return executor
     */
    private ExecutorService getExecutorService() {
        return executorService;
    }

    public void execute(Runnable command) {
        executorService.execute(command);
    }

    public <T> Future<T> submit(Callable<T> task) {
        return executorService.submit(task);
    }

    public <T> Future<T> submit(Runnable task, T result) {
        return executorService.submit(task, result);
    }

    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return executorService.invokeAll(tasks);
    }

    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return executorService.invokeAll(tasks, timeout, unit);
    }

    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return executorService.invokeAny(tasks);
    }

    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return executorService.invokeAny(tasks, timeout, unit);
    }

    public boolean isShutdown() {
        return executorService.isShutdown();
    }

    public boolean isTerminated() {
        return executorService.isTerminated();
    }

    /**
     * Should not expose shutdown
     */
    public void shutdown() {
        throw new UnsupportedOperationException("shutdown will happen at server restart.");
    }

    /**
     * Should not expose shutdown now
     */
    public List<Runnable> shutdownNow() {
        throw new UnsupportedOperationException("shutdown will happen at server restart.");
    }

    /**
     * Should not expose await termination
     */
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        throw new UnsupportedOperationException("cannot terminate the executor explicitly. will terminate when server restarts");
    }

    @PreDestroy
    private void close() {
        try {
            logger.info("cached thread pool utility executor service shutdown");
            executorService.shutdown();
            logger.info("cached thread pool utility executor service shutdown done");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
