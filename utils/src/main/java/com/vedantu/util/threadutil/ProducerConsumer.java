package com.vedantu.util.threadutil;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ProducerConsumer<T> {
    private final BlockingQueue<T> queue;
    private final IProducer<T> producer;
    private final IConsumer<T> consumer;
    private volatile boolean shutdown;
    private Logger logger = LogFactory.getLogger(ProducerConsumer.class);
    private ExecutorService service;

    public ProducerConsumer(BlockingQueue<T> queue, IProducer<T> producer, IConsumer<T> consumer) {
        this.queue = queue;
        this.producer = producer;
        this.consumer = consumer;
    }

    /**
     * Iterators should synchronize their object
     * or use 1 producer
     */
    public interface IProducer<E> extends Iterable<E> {

    }

    @FunctionalInterface
    public interface IConsumer<E> {
        void consume(E item);
    }

    public void start(int producers, int consumers) {
        int threadCount = producers + consumers;
        if (threadCount > 10) {
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "more than 10 threads");
        }
        service = Executors.newFixedThreadPool(threadCount);
        try {
            Iterator<T> iterator = producer.iterator();
            for (int i = 0; i < producers; i++) {
                service.execute(() -> {
                    logger.info("Creating Producer " + Thread.currentThread().getName());
                    try {
                        while (true) {
                            T next = iterator.next();
                            while (!queue.offer(Objects.requireNonNull(next))) {
                                TimeUnit.MILLISECONDS.sleep(100);
                            }
                        }
                    } catch (NoSuchElementException e) {
                        shutdown = true;
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                });
            }

            for (int i = 0; i < consumers; i++) {
                service.execute(() -> {
                    logger.info("Creating Consumer " + Thread.currentThread().getName());
                    try {
                        T next = null;
                        do {
                            if (shutdown && queue.size() == 0) {
                                logger.info("ClOSING THREAD " + Thread.currentThread().getName());
                                break;
                            }
                            next = queue.poll();
                            TimeUnit.MILLISECONDS.sleep(100);
                        } while (next == null);
                        if (next != null) {
                            consumer.consume(next);
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                });
            }

        } finally {
            service.shutdown();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        if (service != null) {
            List<Runnable> runnables = service.shutdownNow();
            logger.info("Runnable " + runnables.size());
            logger.info("Shutdown");
        }
    }
}
