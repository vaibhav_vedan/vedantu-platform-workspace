package com.vedantu.util.threadutil;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

public class RecursiveActionForCollection<V, C extends Collection<V>> extends RecursiveAction {

    private final int threshold;
    private final Consumer<C> function;
    private final C data;
    public RecursiveActionForCollection(int threshold, Consumer<C> function, C data) {
        this.threshold = threshold;
        this.function = Objects.requireNonNull(function);
        this.data = Objects.requireNonNull(data);
    }

    @Override
    protected void compute() {
        if (threshold > 0 && data.size() > threshold) {
            List<RecursiveActionForCollection<V, C>> subtasks = createSubtasks();
            ForkJoinTask.invokeAll(subtasks);
        } else {
            function.accept(data);
        }
    }

    private List<RecursiveActionForCollection<V, C>> createSubtasks() {

        int i = 0, mid = (data.size() - 1) / 2;
        C a;
        C b;
        try {
            //noinspection unchecked
            a = (C) data.getClass().newInstance();
            //noinspection unchecked
            b = (C) data.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "Can't create new instance for given type");
        }
        for (V datum : data) {
            if (i < mid) {
                a.add(datum);
            } else {
                b.add(datum);
            }
        }

        RecursiveActionForCollection<V, C> aTask = new RecursiveActionForCollection<>(threshold, function, a);
        RecursiveActionForCollection<V, C> bTask = new RecursiveActionForCollection<>(threshold, function, b);
        return Arrays.asList(aTask, bTask);
    }
}
