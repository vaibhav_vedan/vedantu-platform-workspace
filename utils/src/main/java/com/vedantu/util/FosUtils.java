/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.request.GetUserInfosByEmailReq;
import com.vedantu.exception.*;
import com.vedantu.lms.request.ValidateAndSetTopicTargetTagsReq;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.subscription.pojo.bundle.BundleEntityInfo;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.GetUserInfosByEmailReq;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.request.ValidateAndSetTopicTargetTagsReq;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.redis.AbstractRedisDAO;

/**
 *
 * @author ajith
 */
@Service
public class FosUtils {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AbstractRedisDAO abstractRedisDAO;

    @Autowired
    private DozerBeanMapper mapper;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FosUtils.class);

    private String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private String islEndpoint = ConfigUtils.INSTANCE.getStringValue("ISL_ENDPOINT");
    private String userendpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private String lmsEndpoint = ConfigUtils.INSTANCE.getStringValue("platform.lms.direct");
    private final String ENV = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final int USER_INFO_PAGE_SIZE = 50;
    private static final int BOARDS_PAGE_SIZE = 20;
    private final String boardFetchURL = platformEndpoint + "board/getBoardsMap"+"?ids=";

    final Gson gson = new Gson();

    public FosUtils() {
        super();
    }

    // user infos, UserBasicInfo pojo
    public UserBasicInfo getUserBasicInfo(Long userId, boolean exposeEmail) {
        logger.info("Entering userIds:" + userId);
        if (userId == null) {
            return null;
        }
        return getUserBasicInfo(userId.toString(), exposeEmail);
    }

    public UserBasicInfo getUserBasicInfo(String userId, boolean exposeEmail) {

        logger.info("Entering userIds:" + userId);
        if (userId == null) {
            return null;
        }
        Set<String> userIds = new HashSet<>();
        userIds.add(userId);
        List<UserBasicInfo> infos = getUserBasicInfosSet(userIds, exposeEmail);
        UserBasicInfo user = null;
        if (ArrayUtils.isNotEmpty(infos)) {
            user = infos.get(0);
        }
        logger.info("Exiting users:" + user);
        return user;
    }

    public Map<Long, UserBasicInfo> getUserBasicInfosMapFromLongIds(List<Long> userIds, boolean exposeEmail) {
        if (userIds == null) {
            userIds = new ArrayList<>();
        }
        Set<Long> userIdsSet = new HashSet<>(userIds);
        return getUserBasicInfosMapFromLongIds(userIdsSet, exposeEmail);
    }

    public Map<Long, UserBasicInfo> getUserBasicInfosMapFromLongIds(Set<Long> userIdsSet, boolean exposeEmail) {
        Map<Long, UserBasicInfo> result = new HashMap<>();
        if (!CollectionUtils.isEmpty(userIdsSet)) {
            List<UserBasicInfo> basicInfos = getUserBasicInfosSet(
                    userIdsSet.stream().map(String::valueOf).collect(Collectors.toSet()), exposeEmail);
            if (ArrayUtils.isNotEmpty(basicInfos)) {
                result = basicInfos.stream().filter(Objects::nonNull)
                        .collect(Collectors.toMap(UserBasicInfo::getUserId, b -> b));
            }
        }

        return result;
    }

    public Map<String, UserBasicInfo> getUserBasicInfosMap(List<String> userIds, boolean exposeEmail) {
        if (userIds == null) {
            userIds = new ArrayList<>();
        }
        Set<String> userIdsSet = new HashSet<>(userIds);
        return getUserBasicInfosMap(userIdsSet, exposeEmail);
    }

    /**
     * Underlying FosUserService is not at all Happy about getting a String Type
     * Unregistered User and throws Exception So, We have to run a filter here.
     *
     * @param userIdSet
     * @param exposeEmail
     * @return
     */
    public Map<String, UserBasicInfo> getUserBasicInfosMap(Set<String> userIdSet, boolean exposeEmail) {
        Set<String> registeredUsers = new HashSet<>();
        userIdSet.forEach(u -> {
            try {
                Long.parseLong(u);
                registeredUsers.add(u);
            } catch (NumberFormatException e) {
                logger.info("ignoring unregistered user" + u);
            }
        });
        List<UserBasicInfo> basicInfos = getUserBasicInfosSet(registeredUsers, exposeEmail);
        Map<String, UserBasicInfo> result = new HashMap<>();
        if (ArrayUtils.isNotEmpty(basicInfos)) {
            for (UserBasicInfo info : basicInfos) {
                if (info != null) {
                    result.put(info.getUserId().toString(), info);
                }
            }
        }
        return result;
    }

    public List<UserBasicInfo> getUserBasicInfosFromLongIds(Set<Long> userIdsSet, boolean exposeEmail) {
        Set<String> userIds = new HashSet<>();
        for (Long userId : userIdsSet) {
            if (userId != null) {
                userIds.add(userId.toString());
            }
        }
        return getUserBasicInfosSet(userIds, exposeEmail);
    }

    public List<UserBasicInfo> getUserBasicInfos(List<String> userIds, boolean exposeEmail) {
        if (userIds == null) {
            return null;
        }
        Set<String> userIdsSet = new HashSet<>(userIds);
        return getUserBasicInfosSet(userIdsSet, exposeEmail);
    }

//    public List<UserBasicInfo> getUserBasicInfosSetOld(Set<String> userIdsSet, boolean exposeEmail) {
//        int LIMIT = 20;
//
//        logger.info("Entering userIds:" + userIdsSet);
//        if (ArrayUtils.isEmpty(userIdsSet)) {
//            return null;
//        }
//
//        List<String> userIds = new ArrayList<>(userIdsSet);
//
//        List<UserBasicInfo> users = new ArrayList<>();
//
//        int listSize = userIds.size();
//        for (int i = 0; i < listSize; i += LIMIT) {
//            List<String> subList;
//            if (listSize > i + LIMIT) {
//                subList = userIds.subList(i, (i + LIMIT));
//            } else {
//                subList = userIds.subList(i, listSize);
//            }
//
//            logger.info("Fetching from redis : {}", subList);
//            List<UserBasicInfo> usersFromRedis = getUserBasicInfoFromRedis(subList);
//            users.addAll(usersFromRedis);
//
//            List<String> fetchedUserIds = usersFromRedis.parallelStream()
//                    .map(userBasicInfo -> userBasicInfo.getUserId().toString())
//                    .collect(Collectors.toList());
//
//            logger.info("fetched from redis : {}", fetchedUserIds);
//
//            subList = subList.parallelStream()
//                    .filter(val -> !fetchedUserIds.contains(val))
//                    .collect(Collectors.toList());
//
//            if (CollectionUtils.isEmpty(subList)) {
//                continue;
//            }
//
//            logger.info("fetching from user : {}", subList);
//
//            String url = userendpoint + "/getUserBasicInfos" + "?exposeEmail=" + exposeEmail + "&userIdsList="
//                    + org.springframework.util.StringUtils.collectionToDelimitedString(subList, ",");
//            logger.info("sending request to " + url);
//            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
//            String output = response.getEntity(String.class);
//            if (!org.springframework.util.StringUtils.isEmpty(output)) {
//                try {
//                    if (response.getStatus() != 200) {
//                        throw new Exception("getUserBasicInfos: getting Users' BasicInfo failed");
//                    }
//                    Type listType = new TypeToken<ArrayList<UserBasicInfo>>() {
//                    }.getType();
//                    List<UserBasicInfo> result = gson.fromJson(output, listType);
//                    if (result != null && !result.isEmpty()) {
//                        users.addAll(result);
//                    }
//                } catch (Exception ex) {
//                    logger.info("Exception while parsing userBasicInfos", ex);
//                }
//            }
//        }
//
//        if (CollectionUtils.isNotEmpty(users) && !exposeEmail) {
//            for (UserBasicInfo userBasicInfo : users) {
//                if (userBasicInfo != null) {
//                    userBasicInfo.purgeEmail();
//                }
//            }
//        }
//
//        logger.info("Exiting users:" + users);
//        return users;
//    }

    public List<UserBasicInfo> getUserBasicInfosSet(Set<String> userIdsSet, boolean exposeEmail) {

        logger.info("fosutilsMod-- userIds set {}", userIdsSet);
        if (ArrayUtils.isEmpty(userIdsSet)) {
            return null;
        }

        List<String> userIds = new ArrayList<>(userIdsSet);
        List<UserBasicInfo> users = new ArrayList<>();

        List<List<String>> userIdParts = Lists.partition(userIds, USER_INFO_PAGE_SIZE);
        for (List<String> userIdPartition: userIdParts) {
            logger.info("fetching from user : {}", userIdPartition);

            String url = userendpoint + "/getUserBasicInfos" + "?exposeEmail=" + exposeEmail + "&userIdsList="
                    + org.springframework.util.StringUtils.collectionToDelimitedString(userIdPartition, ",");
            logger.info("sending request to " + url);
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            String output = response.getEntity(String.class);
            if (!org.springframework.util.StringUtils.isEmpty(output)) {
                try {
                    if (response.getStatus() != 200) {
                        throw new Exception("getUserBasicInfos: getting Users' BasicInfo failed");
                    }
                    Type listType = new TypeToken<ArrayList<UserBasicInfo>>() {
                    }.getType();
                    List<UserBasicInfo> result = gson.fromJson(output, listType);
                    if (result != null && !result.isEmpty()) {
                        users.addAll(result);
                    }
                } catch (Exception ex) {
                    logger.info("Exception while parsing userBasicInfos", ex);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(users) && !exposeEmail) {
            for (UserBasicInfo userBasicInfo : users) {
                if (userBasicInfo != null) {
                    userBasicInfo.purgeEmail();
                }
            }
        }

        logger.info("Exiting users:" + users);
        return users;
    }

    // user infos, whole USER pojo
    public User getUserInfo(Long userId, boolean exposeEmail) {

        logger.info("Entering userIds:" + userId);
        if (userId == null) {
            return null;
        }
        Set<Long> userIds = new HashSet<>();
        userIds.add(userId);
        List<User> infos = getUserInfos(userIds, exposeEmail);
        User user = null;
        if (ArrayUtils.isNotEmpty(infos)) {
            user = infos.get(0);
        }
        logger.info("Exiting users:" + user);
        return user;
    }

    public Map<Long, User> getUserInfosMap(List<Long> userIds, boolean exposeEmail) {
        if (userIds == null) {
            userIds = new ArrayList<>();
        }
        Set<Long> userIdsSet = new HashSet<>(userIds);
        return getUserInfosMap(userIdsSet, exposeEmail);
    }

    public Map<Long, User> getUserInfosMap(Set<Long> userIdsSet, boolean exposeEmail) {
        List<User> basicInfos = getUserInfos(userIdsSet, exposeEmail);
        Map<Long, User> result = new HashMap<>();
        if (ArrayUtils.isNotEmpty(basicInfos)) {
            for (User info : basicInfos) {
                if (info != null) {
                    result.put(info.getId(), info);
                }
            }
        }
        return result;
    }

    public List<User> getUserInfos(List<Long> userIds, boolean exposeEmail) {
        if (userIds == null) {
            userIds = new ArrayList<>();
        }
        Set<Long> userIdsSet = new HashSet<>(userIds);
        return getUserInfos(userIdsSet, exposeEmail);
    }

    public UserBasicInfo getUserBasicInfoFromEmail(String email, boolean exposeEmail) {
        logger.info("Getting Info for for email : " + email);
        if (StringUtils.isEmpty(email)) {
            return null;
        }

        String queryString = "?email=" + WebUtils.getUrlEncodedValue(email) + "&exposeEmail=" + exposeEmail;
        String url = userendpoint + "/getUserBasicInfoByEmail" + queryString;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);

        UserBasicInfo userBasicInfo = null;

        if (!StringUtils.isEmpty(output)) {
            try {
                if (response.getStatus() == 200) {
                    userBasicInfo = gson.fromJson(output, UserBasicInfo.class);
                 }
            } catch (Exception ex) {
                logger.info("Exception while parsing userBasicInfos", ex);
            }
        }

        return userBasicInfo;
    }

    public Map<String, UserInfo> getUserInfosFromEmails(Set<String> userEmails, boolean exposeEmail) {
        logger.info("Entering user emails:" + userEmails);
        if (ArrayUtils.isEmpty(userEmails)) {
            return null;
        }
        List<String> emails = new ArrayList<>();
        emails.addAll(userEmails);
        GetUserInfosByEmailReq getUserInfosByEmailReq = new GetUserInfosByEmailReq();
        getUserInfosByEmailReq.setEmailIds(emails);
        Map<String, UserInfo> userInfoMap = new HashMap<>();
        String queryString = "?";
        int firstIter = 0;
        for (String email : getUserInfosByEmailReq.getEmailIds()) {
            if (firstIter == 0) {
                queryString = queryString + "emailIds=" + WebUtils.getUrlEncodedValue(email);
                firstIter = 1;
            } else {
                queryString = queryString + "&emailIds=" + WebUtils.getUrlEncodedValue(email);
            }

        }
        queryString = queryString + "&exposeEmail=" + exposeEmail;
        String url = userendpoint + "/getUserInfosByEmail" + queryString;
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);
        List<UserInfo> userInfos = new ArrayList<>();
        if (!StringUtils.isEmpty(output)) {
            try {
                if (response.getStatus() != 200) {
                    throw new Exception("getUserBasicInfos: getting Users' BasicInfo failed");
                }
                Type listType = new TypeToken<ArrayList<UserInfo>>() {
                }.getType();
                List<UserInfo> result = gson.fromJson(output, listType);
                if (result != null && !result.isEmpty()) {
                    userInfos.addAll(result);
                }

            } catch (Exception ex) {
                logger.info("Exception while parsing userBasicInfos", ex);
            }
        }

        for (UserInfo userInfo : userInfos) {
            userInfoMap.put(userInfo.getEmail(), userInfo);
        }

        return userInfoMap;

    }

    public List<User> getUserInfos(Set<Long> userIdsSet, boolean exposeEmail) {

        logger.info("Entering userIds:" + userIdsSet);
        if (ArrayUtils.isEmpty(userIdsSet)) {
            return null;
        }

        List<Long> userIds = new ArrayList<>(userIdsSet);

        int LIMIT = 20;
        List<User> users = new ArrayList<>();
        int listSize = userIds.size();
        for (int i = 0; i < listSize; i += LIMIT) {
            List subList;
            if (listSize > i + LIMIT) {
                subList = userIds.subList(i, (i + LIMIT));
            } else {
                subList = userIds.subList(i, listSize);
            }

            String url = userendpoint + "/getUsersById?exposeEmail=" + exposeEmail;
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(subList), true);
            String output = response.getEntity(String.class);
            if (!org.springframework.util.StringUtils.isEmpty(output)) {
                try {
                    if (response.getStatus() != 200) {
                        throw new Exception("getUserBasicInfos: getting Users' BasicInfo failed");
                    }

                    Type listType = new TypeToken<ArrayList<User>>() {
                    }.getType();
                    List<User> result = gson.fromJson(output, listType);
                    if (result != null && !result.isEmpty()) {
                        users.addAll(result);
                    }
                } catch (Exception ex) {
                    logger.info("Exception while parsing userBasicInfos", ex);
                }
            }
        }
        logger.info("Exiting users:" + users);
        return users;
    }

    // boards
    public Map<Long, Board> getBoardInfoMap(List<Long> boardIds) {
        if (boardIds == null) {
            boardIds = new ArrayList<>();
        }
        Set<Long> boardIdsSet = new HashSet<>(boardIds);
        return getBoardInfoMap(boardIdsSet);
    }

    public Map<Long, Board> getBoardInfoMap(Set<Long> boardIds) {
        Map<Long, Board> resultMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(boardIds)) {
            String ids = "";
            for (Long boardId : boardIds) {
                ids += "ids=" + boardId + "&";
            }
            String url = platformEndpoint + "/board/getBoardsMap" + "?" + ids;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            try {
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                Type type = new TypeToken<Map<Long, Board>>() {
                }.getType();
                resultMap = gson.fromJson(jsonString, type);
            } catch (Exception e) {
                logger.error("Error in fetching getBoardInfoMap " + boardIds.toString(), e);
            }
        }
        return resultMap;
    }

    public Set<String> getSubjectsByBoardTeacherPairs(List<BoardTeacherPair> boardTeacherPairs) {
        Set<Long> boardIds = new HashSet<Long>();
        Set<String> subjects = new HashSet<String>();
        if (!ArrayUtils.isEmpty(boardTeacherPairs)) {
            for (BoardTeacherPair boardTeacherPair : boardTeacherPairs) {
                boardIds.add(boardTeacherPair.getBoardId());
            }
            Map<Long, Board> boardInfoMap = getBoardInfoMap(boardIds);
            if ((boardInfoMap != null) && (boardInfoMap.size() > 0)) {
                for (Long key : boardInfoMap.keySet()) {
                    subjects.add(boardInfoMap.get(key).getName());
                }
            }
        }
        return subjects;
    }

    public String getTeacherBoardMappings(String teacherId) throws VException {

        logger.info("ENTRY teacherId:" + teacherId);

        if (teacherId == null) {
            return null;
        }
        ClientResponse fosResp = WebUtils.INSTANCE.doCall(
                platformEndpoint + "tbm/getTeacherBoardMappingTree?userId=" + teacherId, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
        String resp = fosResp.getEntity(String.class);

        logger.info("EXIT " + resp);
        return resp;
    }

    // subscriptions
    public List<SessionInfo> getSubscriptionSessionInfo(Long subscriptionId) {
        List<SessionInfo> sessionInfos = new ArrayList<>();
        if (subscriptionId != null) {
            String url = schedulingEndpoint + "/session/getSubscriptionSessions" + "?subscriptionId=" + subscriptionId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            try {
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                Type listType = new TypeToken<ArrayList<SessionInfo>>() {
                }.getType();
                List<SessionInfo> result = gson.fromJson(jsonString, listType);
                if (result != null && !result.isEmpty()) {
                    sessionInfos.addAll(result);
                }
            } catch (Exception e) {
                logger.error("Error in fetching getSubscriptionSessions " + subscriptionId, e);
            }
        }
        return sessionInfos;
    }

    // TODO remove this way. It is only being used by rcb manager
    public Map<Long, List<SessionInfo>> getSubscriptionSessionInfosMap(Set<Long> subscriptionIds) {
        Map<Long, List<SessionInfo>> resultMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(subscriptionIds)) {
            for (Long subscriptionId : subscriptionIds) {
                List<SessionInfo> sessionInfos = getSubscriptionSessionInfo(subscriptionId);
                if (sessionInfos != null) {
                    resultMap.put(subscriptionId, sessionInfos);
                }
            }
        }
        return resultMap;
    }

    public List<UserInfo> getAllTeacherInfos() {
        List<UserInfo> users = new ArrayList<>();
        int start = 0;
        int size = 20;
        while (true) {
            List<UserInfo> results = getTeacherInfos(start, size);
            if (CollectionUtils.isEmpty(results)) {
                break;
            }

            users.addAll(results);
            start += size;
        }

        return users;
    }

    public List<UserInfo> getTeacherInfos(int start, int size) {

        logger.info("Entering userIds start:" + start + " size:" + size);
        List<UserInfo> users = new ArrayList<>();
        String url = userendpoint + "/getUsers";
        GetUsersReq getUsersReq = new GetUsersReq();
        getUsersReq.setStart(start);
        getUsersReq.setSize(size);
        getUsersReq.setRole(Role.TEACHER);

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(getUsersReq), true);
        String output = response.getEntity(String.class);
        if (!org.springframework.util.StringUtils.isEmpty(output)) {
            try {
                if (response.getStatus() != 200) {
                    throw new Exception("getUserBasicInfos: getting Users' BasicInfo failed");
                }
                GetUsersRes getUsersRes = gson.fromJson(output, GetUsersRes.class);
                List<UserInfo> result = getUsersRes.getUsers();
                if (result != null && !result.isEmpty()) {
                    users.addAll(result);
                }
            } catch (Exception ex) {
                logger.info("Exception while parsing userBasicInfos", ex);
            }
        }

        logger.info("Exiting users:" + users);
        return users;

    }

    public AbstractTargetTopicEntity setAndGenerateTags(AbstractTargetTopicEntity atte) throws Exception, NotFoundException, BadRequestException {
        String url = lmsEndpoint + "/tree/validateAndSetTopicTargetTags";

        Set<String> mainTags = new HashSet<>();
        ValidateAndSetTopicTargetTagsReq requestTags = new ValidateAndSetTopicTargetTagsReq();
        if (atte != null) {
            if (atte.getTargetGrades() != null) {
                requestTags.setTargetGrades(atte.getTargetGrades());
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "AbstractTargetTopicEntity getTargetGrades is null");
            }
            if (atte.gettTopics() != null) {
                requestTags.setTopics(atte.gettTopics());
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "AbstractTargetTopicEntity gettTopics is null");
            }
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "AbstractTargetTopicEntity is null");
        }

        AbstractTargetTopicEntity result = new AbstractTargetTopicEntity();

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(requestTags), true);
        String output = response.getEntity(String.class);
        if (!org.springframework.util.StringUtils.isEmpty(output)) {
            if (response.getStatus() != 200) {
                throw new Exception("validateAndSetTopicTargetTags: setting valid tags failed");
            }
            result = gson.fromJson(output, AbstractTargetTopicEntity.class);
            if (result != null) {
                if (ArrayUtils.isEmpty(result.getMainTags())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Topics or TargetGrades for MainTags");
                }
                if (ArrayUtils.isEmpty(result.gettSubjects())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Topics for Subjects");
                }
                if (ArrayUtils.isEmpty(result.gettTargets())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid TargetGrades for Targets");
                }
                if (ArrayUtils.isEmpty(result.gettGrades())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid TargetGrades for Grades");
                }
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Topics or TargetGrades");
            }
        }
        return result;
    }

    private List<UserBasicInfo> getUserBasicInfoFromRedis(List<String> ids) {

        List<UserBasicInfo> basicInfos = new ArrayList<>();
        try {

            if (ArrayUtils.isNotEmpty(ids)) {
                List<String> keys = new ArrayList<>();
                for (String id : ids) {
                    if (id != null) {
                        keys.add(getUserBasicInfoKey(id));
                    }
                }
                Map<String, String> valueMap = new HashMap<>();
                try {
                    valueMap = abstractRedisDAO.getValuesForKeys(keys);
                } catch (Exception e) {
                    logger.info("Error in Redis getValuesForKeys : {}", e.getMessage());
                }

                if (valueMap != null && !valueMap.isEmpty()) {
                    for (String id : ids) {
                        if (id != null) {
                            String value = valueMap.get(getUserBasicInfoKey(id));
                            if (StringUtils.isNotEmpty(value)) {
                                UserBasicInfo userBasicInfo = gson.fromJson(value, UserBasicInfo.class);
                                if (userBasicInfo != null) {
                                    basicInfos.add(userBasicInfo);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Error in Redis getValuesForKeys : {}", e.getMessage());
        }

        return basicInfos;
    }

    private String getUserBasicInfoKey(String id) {
        return ENV + "_USER_BASIC_" + id;
    }


    public UserBasicInfo getUserBasicInfoFromContactNumber(String contactNumber) {
        logger.info("Getting Info for for phone : " + contactNumber);
        if (StringUtils.isEmpty(contactNumber)) {
            return null;
        }

        String url = userendpoint + "/getIdByContactNumber/" + WebUtils.getUrlEncodedValue(contactNumber);

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);

        UserBasicInfo userBasicInfo = null;

        if (!StringUtils.isEmpty(output)) {
            try {
                if (response.getStatus() == 200) {
                    userBasicInfo = gson.fromJson(output, UserBasicInfo.class);
                }
            } catch (Exception ex) {
                logger.info("Exception while parsing userBasicInfos", ex);
            }
        }
        return userBasicInfo;
    }

    public UserBasicInfo getUserBasicInfoByContactNumber(String contactNumber) {
        logger.info("Getting Info for for phone : " + contactNumber);
        UserBasicInfo userBasicInfo = null;

        String url = userendpoint + "/info/getUserByContactNumber?contactNumber=" + WebUtils.getUrlEncodedValue(contactNumber);

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);

        if (!StringUtils.isEmpty(output)) {
            try {
                if (response.getStatus() == 200) {
                    userBasicInfo = gson.fromJson(output,UserBasicInfo.class);
                }
            } catch (Exception ex) {
                logger.info("Exception while parsing userBasicInfos", ex);
            }
        }
        return userBasicInfo;

    }

    public List<String> getEarlyLearningTeachers(String courseType) {

        logger.info("Fetching early learning teachers");
        List<String> teacherIdsEL = new ArrayList<>();

        String url = userendpoint + "/getEarlyLearningTeachers?type="+courseType;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type type = new TypeToken<List<String>>() {
            }.getType();
            teacherIdsEL = gson.fromJson(jsonString, type);
        } catch (Exception e) {
            logger.error("Error in fetching earlyLearning teacherIds " + e.getMessage());
        }

        logger.info("Fetched EL teachers: " + teacherIdsEL);
        return teacherIdsEL;

    }

    public ELTeachersProficiencies getELTeachersProficiencies(SessionLabel courseType) {

        logger.info("Fetching early learning teachers Proficiencies");
        ELTeachersProficiencies teacherProficiencyIds = new ELTeachersProficiencies();
        String url = userendpoint + "/eluser/getELTeacherProficiency?type="+courseType;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info("json string : {}",jsonString);
            teacherProficiencyIds = gson.fromJson(jsonString, ELTeachersProficiencies.class);
        } catch (Exception e) {
            logger.error("Error in fetching teacherProficiencyIds " + e.getMessage());
        }
        logger.info("Fetched teacherProficiencyIds: " + teacherProficiencyIds);
        return teacherProficiencyIds;
    }

    public VsatEventDetailsPojo getVsatEventDetails() throws VException {
        VsatEventDetailsPojo vsatEventDetailsPojo = new VsatEventDetailsPojo();
        try {
            String vsatEventDetailsStr = abstractRedisDAO.get("VSAT_EVENT_DETAILS");
            if (vsatEventDetailsStr != null) {
                vsatEventDetailsPojo = gson.fromJson(vsatEventDetailsStr, VsatEventDetailsPojo.class);
                logger.info("vsatEventDetailsPojo from redis : " + vsatEventDetailsPojo);
            }
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.error(e.getErrorMessage(), e);
        }
        if (vsatEventDetailsPojo == null || StringUtils.isEmpty(vsatEventDetailsPojo.getEventName())) {
            String url = islEndpoint + "/vsat/getVsatEventDetails";
            logger.info("sending request to " + url);
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            String output = response.getEntity(String.class);
            if (StringUtils.isNotEmpty(output)) {
                try {
                    if (response.getStatus() != 200) {
                        throw new Exception("getVsatEventDetails: getting Vsat Event Details failed");
                    }
                    VsatEventDetailsPojo result = gson.fromJson(output, VsatEventDetailsPojo.class);
                    logger.info("VsatEventDetailsPojo from getVsatEventDetails : " + vsatEventDetailsPojo);
                    if (result != null) {
                        vsatEventDetailsPojo = result;
                        abstractRedisDAO.set("VSAT_EVENT_DETAILS",new Gson().toJson(vsatEventDetailsPojo));
                    }
                } catch (Exception ex) {
                    logger.info("Exception while parsing userBasicInfos", ex);
                }
            }
        }
        return vsatEventDetailsPojo;
    }

    public void deleteRedisUserBasicInfoByUserId(Long userId) throws BadRequestException, InternalServerErrorException {
        logger.info("userId for Redis User Basic Info deleting : "+ userId);
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId required to remove the userBasic Info from Redis");
        }
        String redisKey = getUserBasicInfoKey(userId.toString());
        Long result = abstractRedisDAO.del(redisKey);
        logger.info("Abstact Redis DAO Result : " + result);
    }

    public String maskEmail(String email) {
        return email.replaceAll("(?<=.)[^@](?=[^@]*?@)|(?:(?<=@.)|(?!^)\\G(?=[^@]*$)).(?=.*\\.)", "*");
    }

    public Map<String, TeacherBasicInfo> getTeacherBasicInfoForApp(Set<String> presenterIds) {

    	logger.info("Entering presenterIds:" + presenterIds);
    	if (ArrayUtils.isEmpty(presenterIds)) {
    		return null;
    	}

    	List<String> teacherIds = new ArrayList<>(presenterIds);

    	List<TeacherBasicInfo> teachers = new ArrayList<>();

    	logger.info("Fetching from redis : {}", teacherIds);
    	List<TeacherBasicInfo> teachersFromRedis = getTeacherBasicInfoFromRedis(teacherIds);
    	teachers.addAll(teachersFromRedis);

    	List<String> fetchedTeacherIds = teachersFromRedis.parallelStream()
    			.map(teacherBasicInfo -> teacherBasicInfo.getUserId().toString())
    			.collect(Collectors.toList());

    	logger.info("Fetched teachers from redis : {}", fetchedTeacherIds);

    	teacherIds = teacherIds.parallelStream()
    			.filter(val -> !fetchedTeacherIds.contains(val))
    			.collect(Collectors.toList());

    	if (!CollectionUtils.isEmpty(teacherIds)) {

    		logger.info("Fetching teachers from user : {}", teacherIds);

    		String url = userendpoint + "/info/getTeacherBasicInfos";
    		String query = new Gson().toJson(teacherIds);
    		logger.info("Sending request to " + url);
    		try {
    			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true);
    			String output = response.getEntity(String.class);
    			if (!org.springframework.util.StringUtils.isEmpty(output)) {

    				if (response.getStatus() != 200) {
    					throw new Exception("Failed to fetch TeacherBasicInfos from user for users : "+teacherIds);
    				}
    				Type listType = new TypeToken<ArrayList<TeacherBasicInfo>>() {
    				}.getType();
    				List<TeacherBasicInfo> result = gson.fromJson(output, listType);
    				if (result != null && !result.isEmpty()) {
    					teachers.addAll(result);
    				}
    			}
    		} catch (Exception ex) {
    			logger.info("Exception while parsing teacherBasicInfos", ex);
    		}

    	}

    	Map<String, TeacherBasicInfo> result = new HashMap<>();

    	if (ArrayUtils.isNotEmpty(teachers)) {
    		for (TeacherBasicInfo info : teachers) {
    			if (info != null) {
    				result.put(info.getUserId().toString(), info);
    			}
    		}
    	}

    	logger.info("Exiting teachers:" + result);
    	return result;


    }

    private List<TeacherBasicInfo> getTeacherBasicInfoFromRedis(List<String> ids) {

    	List<TeacherBasicInfo> teacherBasicInfos = new ArrayList<>();
    	try {

    		if (ArrayUtils.isNotEmpty(ids)) {
    			List<String> keys = new ArrayList<>();
    			for (String id : ids) {
    				if (id != null) {
    					keys.add(getTeacherBasicInfoKey(id));
    				}
    			}
    			Map<String, String> valueMap = new HashMap<>();
    			try {
    				valueMap = abstractRedisDAO.getValuesForKeys(keys);
    			} catch (Exception e) {
    				logger.info("Error in Redis getValuesForKeys : {}", e.getMessage());
    			}

    			if (valueMap != null && !valueMap.isEmpty()) {
    				for (String id : ids) {
    					if (id != null) {
    						String value = valueMap.get(getUserBasicInfoKey(id));
    						if (StringUtils.isNotEmpty(value)) {
    							TeacherBasicInfo teacherBasicInfo = gson.fromJson(value, TeacherBasicInfo.class);
    							if (teacherBasicInfo != null) {
    								teacherBasicInfos.add(teacherBasicInfo);
    							}
    						}
    					}
    				}
    			}
    		}
    	} catch (Exception e) {
    		logger.info("Error in Redis getValuesForKeys : {}", e.getMessage());
    	}

    	return teacherBasicInfos;
    }

    private String getTeacherBasicInfoKey(String id) {
    	return ENV + "_TEACHER_BASIC_INFO_" + id;
    }

    public Map<Long, Board> getBoardInfoMapForApp(List<Long> boardIds) {
    	logger.info("Entering boardIds:" + boardIds);
    	Map<Long, Board> boardsMap = new HashMap<Long, Board>();
    	if (ArrayUtils.isEmpty(boardIds)) {
    		logger.info("ids is null.");
    		return boardsMap;
    	}
    	boardsMap = getBoardsMapFromRedis(boardIds);
    	List<Long> fetchList = new ArrayList<>();

    	if (!boardsMap.isEmpty()) {
    		for (Long id : boardIds) {
    			if (!boardsMap.containsKey(id)) {
    				logger.info("board id" + id);
    				fetchList.add(id);
    			}
    		}
    	} else {
    		fetchList.addAll(boardIds);
    	}

        final String delim = "&";
        final Type listType = new TypeToken<Map<Long,Board>>() {
        }.getType();

    	if (CollectionUtils.isNotEmpty(fetchList)) {
            List<List<Long>> boardIdPartitions = Lists.partition(fetchList, BOARDS_PAGE_SIZE);
    		logger.info("Fetching boards from platform : {}", fetchList);
    		for (List<Long> boardIdPartition : boardIdPartitions) {
    			final String fetchString = boardIdPartition.stream().map(String::valueOf).collect(Collectors.joining(delim));
    			final String url = boardFetchURL + fetchString;
    			logger.info("Sending request to " + url);
    			try {
    				ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
    				String output = response.getEntity(String.class);
    				logger.debug("board map response from platform -> "+output);
    				if (!org.springframework.util.StringUtils.isEmpty(output)) {

    					if (response.getStatus() != 200) {
    						throw new Exception("Failed to fetch Board from platform for board Ids : "+ boardIdPartition);
    					}

    					Map<Long, Board> result = new HashMap<>();
    					result = gson.fromJson(output, listType);
    					if(Objects.nonNull(result) && CollectionUtils.isNotEmpty(result.values())) {
    						boardsMap.putAll(result);
    					}
    					logger.info("board map from platform -> "+result);
    				}
    			}catch(Exception e) {
    				logger.warn("Exception occured while fetching board details -> " + e);
    			}
    		}
    	}

    	return boardsMap;
    }

    public Map<Long, Board> getBoardsMapFromRedis(List<Long> ids) {

    	Map<Long, Board> boardMap = new HashMap<>();
    	if (ArrayUtils.isNotEmpty(ids)) {
    		List<String> keys = new ArrayList<>();
    		for (Long id : ids) {
    			if (null != id) {
    				keys.add(getBoardRedisKey(id));
    			}
    		}
    		Map<String, String> valueMap = new HashMap<>();
    		try {
    			valueMap = abstractRedisDAO.getValuesForKeys(keys);
    		} catch (Exception e) {
    			logger.error("Error in redis " + e.getMessage());
    		}
    		if (null != valueMap && !valueMap.isEmpty()) {
    			for (Long id : ids) {
    				if (null != id) {
    					String value = valueMap.get(getBoardRedisKey(id));
    					if (!StringUtils.isEmpty(value)) {
    						Board board = gson.fromJson(value, Board.class);
    						boardMap.put(id, board);
    					}
    				}
    			}
    		}
    	}
    	return boardMap;
    }

    public String getBoardRedisKey(Long id) {
    	return ENV + "_BOARD_" + id;
    }
}
