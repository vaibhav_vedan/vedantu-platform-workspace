package com.vedantu.util.dbentities.mongo;

import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.DBUtils;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.util.StringUtils;

@EqualsAndHashCode(callSuper = true)
public abstract class AbstractMongoStringIdEntity extends AbstractMongoEntity {

    @Id
    private String id;

    public AbstractMongoStringIdEntity() {
        super();
    }

    public AbstractMongoStringIdEntity(Long creationTime, String createdBy, Long lastUpdated, String lastUpdatedBy) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
    }

    public AbstractMongoStringIdEntity(Long creationTime, String createdBy, Long lastUpdated, String lastUpdatedBy, EntityState entityState) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy, entityState);
    }

    
    public AbstractMongoStringIdEntity(String id, Long creationTime,
            String createdBy, Long lastUpdated, String lastUpdatedBy) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
        this.id = id;
    }

    public AbstractMongoStringIdEntity(String id, Long creationTime,
            String createdBy, Long lastUpdated, String lastUpdatedBy, EntityState entityState) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy, entityState);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setEntityDefaultProperties(String callingUserId) {
        if(StringUtils.isEmpty(callingUserId))
            callingUserId = DBUtils.getCallingUserId();
        if (StringUtils.isEmpty(this.getId())) {
            this.setCreationTime(System.currentTimeMillis());
            this.setCreatedBy(callingUserId);
        }
        this.setLastUpdated(System.currentTimeMillis());
        this.setLastUpdatedBy(callingUserId);
    }

}
