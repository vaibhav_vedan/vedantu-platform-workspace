/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.dbentities.mongo;

import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.pojo.AdminTag;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
@EqualsAndHashCode
public abstract class AbstractMongoEntity {

    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String lastUpdatedBy; // For tracking last updated by
    private EntityState entityState = EntityState.ACTIVE;
    private Set<AdminTag> adminTags;

    public AbstractMongoEntity() {
        super();
    }

    public AbstractMongoEntity(Long creationTime, String createdBy, Long lastUpdated, String lastUpdatedBy) {
        this(creationTime, createdBy, lastUpdated, lastUpdatedBy, EntityState.ACTIVE);
    }

    public AbstractMongoEntity(Long creationTime, String createdBy, Long lastUpdated,
            String lastUpdatedBy, EntityState entityState) {
        this.creationTime = creationTime;
        this.createdBy = createdBy;
        this.lastUpdated = lastUpdated;
        this.lastUpdatedBy = lastUpdatedBy;
        this.entityState = entityState;
    }

    public abstract Object getId();

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public EntityState getEntityState() {
        return entityState;
    }

    public void setEntityState(EntityState entityState) {
        this.entityState = entityState;
    }

    public abstract void setEntityDefaultProperties(String callingUserId);

    public Set<AdminTag> getAdminTags() {
        return adminTags != null ? adminTags : new LinkedHashSet<>();
    }

    public void addOrRemoveAdminTags(List<AdminTag> adminTags, Long callingUserId, boolean removeTags) {
        for (AdminTag adminTag : adminTags) {
            addOrRemoveAdminTags(adminTag, callingUserId, removeTags);
        }
    }

    private void addOrRemoveAdminTags(AdminTag adminTag, Long callingUserId, boolean removeTags) {
        this.adminTags = getAdminTags();
        adminTag.setEntityId(null); // To remove unwanted data in db
        if (this.adminTags.contains(adminTag)) {
            for (AdminTag otherTag : this.adminTags) {
                if (otherTag.equals(adminTag)) {
                    if (removeTags) {
                        otherTag.getTags().removeAll(adminTag.getTags());
                    } else {
                        otherTag.getTags().addAll(adminTag.getTags());
                    }
                    otherTag.setModifiedBy(callingUserId);
                    otherTag.setLastModified(System.currentTimeMillis());
                }
            }
        } else if (!removeTags) {
            adminTag.setAddedBy(callingUserId);
            adminTag.setModifiedBy(callingUserId);
            adminTag.setLastModified(System.currentTimeMillis());
            this.adminTags.add(adminTag);
        }
    }

    public static class Constants {

        public static final String ID = "id";
        public static final String _ID = "_id";
        public static final String CREATION_TIME = "creationTime";
        public static final String LAST_UPDATED = "lastUpdated";
        public static final String LAST_UPDATED_BY = "lastUpdatedBy";
        public static final String CREATED_BY = "createdBy";
        public static final String ENTITY_STATE = "entityState";
        public static final String UPDATE_SET_PARAM = "$set";
        public static final String UPDATE_UNSET_PARAM = "$unset";
    }

    //reference http://howtodoinjava.com/apache-commons/how-to-override-tostring-effectively-with-tostringbuilder/
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
