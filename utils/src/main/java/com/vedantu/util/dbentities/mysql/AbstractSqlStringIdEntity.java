package com.vedantu.util.dbentities.mysql;

import com.vedantu.util.dbutils.DBUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.util.StringUtils;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public abstract class AbstractSqlStringIdEntity extends AbstractSqlEntity {
    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid2")
    public String id;

    public AbstractSqlStringIdEntity(String id, Long creationTime, String createdBy, Long lastUpdatedTime, String lastUpdatedby,
                                   Boolean enabled, String callingUserId) {
        super(creationTime, createdBy, lastUpdatedTime, lastUpdatedby,
                enabled, callingUserId);
        this.id = id;

    }

    public AbstractSqlStringIdEntity() {
        super();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setEntityDefaultProperties(String callingUserId) {
        if(StringUtils.isEmpty(callingUserId))
            callingUserId = DBUtils.getCallingUserId();
        if (StringUtils.isEmpty(this.getId())) {
            this.setCreationTime(System.currentTimeMillis());
            this.setCreatedBy(callingUserId);
        }
        this.setLastUpdatedTime(System.currentTimeMillis());
        this.setLastUpdatedby(callingUserId);
    }
}

