/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.dbentities.mongo;

import java.util.Set;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 *
 * @author parashar
 */
public class AbstractTargetTopicLongIdEntity extends AbstractMongoLongIdEntity{

    //just give targetGrades and tTopics
    //all other fields are derived from above
    public Set<String> targetGrades; // CBSE-10  
    @Indexed(background = true)
    public Set<String> tSubjects;
    public Set<String> tTopics;

    public Set<String> tGrades;
    public Set<String> tTargets;

    @Indexed(background = true)
    public Set<String> mainTags;

    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    public Set<String> gettSubjects() {
        return tSubjects;
    }

    public void settSubjects(Set<String> tSubjects) {
        this.tSubjects = tSubjects;
    }

    public Set<String> gettTopics() {
        return tTopics;
    }

    public void settTopics(Set<String> tTopics) {
        this.tTopics = tTopics;
    }

    public Set<String> gettGrades() {
        return tGrades;
    }

    public void settGrades(Set<String> tGrades) {
        this.tGrades = tGrades;
    }

    public Set<String> gettTargets() {
        return tTargets;
    }

    public void settTargets(Set<String> tTargets) {
        this.tTargets = tTargets;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public void setTags(AbstractTargetTopicEntity entity) {
        if (entity != null) {
            this.targetGrades = entity.getTargetGrades();
            this.tTopics = entity.gettTopics();
            this.mainTags = entity.getMainTags();
            this.tGrades = entity.gettGrades();
            this.tTargets = entity.gettTargets();
            this.tSubjects = entity.gettSubjects();
        }
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String MAIN_TAGS = "mainTags";
        public static final String TSUBJECTS = "tSubjects";
    }    
    
}
