package com.vedantu.util.dbentities.mongo;

import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.DBUtils;
import org.springframework.data.annotation.Id;
import org.springframework.util.StringUtils;

public abstract class AbstractMongoLongIdEntity extends AbstractMongoEntity {

    @Id
    private Long id;

    public AbstractMongoLongIdEntity() {
        super();
    }

    public AbstractMongoLongIdEntity(Long creationTime, String createdBy, Long lastUpdated, String lastUpdatedBy) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
    }

    public AbstractMongoLongIdEntity(Long creationTime, String createdBy, Long lastUpdated, String lastUpdatedBy, EntityState entityState) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy, entityState);
    }

    public AbstractMongoLongIdEntity(Long id, Long creationTime,
            String createdBy, Long lastUpdated, String lastUpdatedBy) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
        this.id = id;
    }

    public AbstractMongoLongIdEntity(Long id, Long creationTime,
            String createdBy, Long lastUpdated, String lastUpdatedBy, EntityState entityState) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy, entityState);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setEntityDefaultProperties(String callingUserId) {
        if(StringUtils.isEmpty(callingUserId))
            callingUserId = DBUtils.getCallingUserId();
        if (this.getCreationTime()==null) {
            this.setCreationTime(System.currentTimeMillis());
            this.setCreatedBy(callingUserId);
        }
        this.setLastUpdated(System.currentTimeMillis());
        this.setLastUpdatedBy(callingUserId);
    }

}
