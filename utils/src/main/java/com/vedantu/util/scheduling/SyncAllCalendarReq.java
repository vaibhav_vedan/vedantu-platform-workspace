package com.vedantu.util.scheduling;

import java.util.List;

import com.vedantu.User.Role;

public class SyncAllCalendarReq {
	private long startTime;
	private long endTime;
	private List<String> userIds;
	private Role role;

	public SyncAllCalendarReq() {
		super();
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public List<String> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "SyncAllCalendarReq [startTime=" + startTime + ", endTime=" + endTime + ", userIds=" + userIds
				+ ", role=" + role + ", toString()=" + super.toString() + "]";
	}
}
