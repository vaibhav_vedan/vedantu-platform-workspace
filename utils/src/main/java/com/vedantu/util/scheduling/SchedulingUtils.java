package com.vedantu.util.scheduling;

import com.vedantu.util.Constants;
import java.util.BitSet;

import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SchedulingUtils {

    public static long[] createSlotBitSetLongValues(SessionSchedule schedule) {
        BitSet bitSet = createSlotBitSet(schedule);
        return bitSet.toLongArray();
    }

    public static BitSet createSlotBitSet(SessionSchedule schedule) {

        Long startTime = CommonCalendarUtils.getDayStartTime(schedule.getStartTime());
        BitSet bitSet = new BitSet();
        Long noOfWeeks = 1l;
        if (schedule.getNoOfWeeks() != null && schedule.getNoOfWeeks() > 0) {
            noOfWeeks = schedule.getNoOfWeeks();
        }
        for (int i = 0; i < noOfWeeks; i++) {
            for (SessionSlot sessionSlot : schedule.getSessionSlots()) {
                int startIndex = CommonCalendarUtils.getSlotStartIndex(
                        sessionSlot.getStartTime() + i * CommonCalendarUtils.MILLIS_PER_WEEK, startTime);
                int endIndex = CommonCalendarUtils
                        .getSlotEndIndex(sessionSlot.getEndTime() + i * CommonCalendarUtils.MILLIS_PER_WEEK, startTime);
                bitSet.set(startIndex, endIndex + 1);
            }
        }

        return bitSet;
    }

    public static List<SessionSlot> createSessionSlots(long[] slotBitSet, Long startTime) {
        BitSet scheduleBitSet = BitSet.valueOf(slotBitSet);
        List<SessionSlot> sessionSlots = new ArrayList<>();
        for (int i = 0; i < scheduleBitSet.length(); i++) {
            int startIndex = scheduleBitSet.nextSetBit(i);
            if (startIndex < 0) {
                break;
            }
            int endIndex = scheduleBitSet.nextClearBit(startIndex);
            SessionSlot sessionSlot = new SessionSlot(startTime + (startIndex * Constants.SLOT_LENGTH),
                    startTime + (endIndex * Constants.SLOT_LENGTH), null,
                    null, null, false);
            sessionSlots.add(sessionSlot);
            i = endIndex;
        }
        return sessionSlots;
    }

    public static List<SessionSlot> getSessionSlotsForAllWeeks(SessionSchedule schedule) {

        List<SessionSlot> sessionSlots;
        if (schedule.getSlotBitSet() != null && schedule.getSlotBitSet().length > 0) {
            sessionSlots = createSessionSlots(schedule.getSlotBitSet(), schedule.getStartTime());
        } else {
            //making sure slots are in order
            sessionSlots = new ArrayList<>();
            Collections.sort(schedule.getSessionSlots(), (SessionSlot o1, SessionSlot o2) -> o1.getStartTime().compareTo(o2.getStartTime()));
            Long noOfWeeks = 1l;
            if (schedule.getNoOfWeeks() != null && schedule.getNoOfWeeks() > 0) {
                noOfWeeks = schedule.getNoOfWeeks();
            }
            for (int i = 0; i < noOfWeeks; i++) {
                for (SessionSlot sessionSlot : schedule.getSessionSlots()) {
                    SessionSlot slot = new SessionSlot(sessionSlot);
                    slot.setStartTime(sessionSlot.getStartTime() + i * CommonCalendarUtils.MILLIS_PER_WEEK);
                    slot.setEndTime(sessionSlot.getEndTime() + i * CommonCalendarUtils.MILLIS_PER_WEEK);
                    sessionSlots.add(slot);
                }
            }
        }
        return sessionSlots;
    }
}
