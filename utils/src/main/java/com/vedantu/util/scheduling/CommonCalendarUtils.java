package com.vedantu.util.scheduling;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.PlatformBasicResponse;

public class CommonCalendarUtils {

	public static final long MILLIS_PER_SECOND = 1000;
	public static final long SECONDS_PER_MINUTE = 60;
	public static final long MINUTES_PER_HOUR = 60;
	public static final long HOURS_PER_DAY = 24;

	public static final long MILLIS_PER_MINUTE = MILLIS_PER_SECOND * SECONDS_PER_MINUTE;
	public static final long MILLIS_PER_HOUR = MILLIS_PER_MINUTE * MINUTES_PER_HOUR;
	public static final long MILLIS_PER_DAY = MILLIS_PER_HOUR * HOURS_PER_DAY;
	public static final long MILLIS_PER_WEEK = 7 * MILLIS_PER_DAY;

	public static final String TIME_ZONE_GMT = "GMT";

	public static final long SLOT_LENGTH = 15 * CommonCalendarUtils.MILLIS_PER_MINUTE;
	private static final int SLOT_COUNT = (int) (CommonCalendarUtils.MILLIS_PER_DAY / SLOT_LENGTH);

	public static long getMillisPerDay() {
		return MILLIS_PER_DAY;
	}

	public static long getDayStartTime(long time) {
		return time - (time % MILLIS_PER_DAY);
	}

	public static Long getDayEndTime(Long time) {
		return getDayStartTime(time) + DateTimeUtils.MILLIS_PER_DAY;
	}

	public static long getDayStartTime_end(long time) {
		return (time % MILLIS_PER_DAY == 0) ? time - MILLIS_PER_DAY : time - (time % MILLIS_PER_DAY);
	}

	public static Long getDayStartTime_IST(Long time) {
		Long startTime = time;
		if (0l != (time % DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE)) {
			Long gmtDiff = time % DateTimeUtils.MILLIS_PER_DAY;
			Long gmtTime = time - gmtDiff;
			startTime = gmtTime + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			if (gmtDiff > DateTimeUtils.IST_TIME_DIFFERENCE) {
				startTime = time - gmtDiff + DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			} else {
				startTime = time - gmtDiff - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			}
		}
		return startTime;
	}

	public static Long getDayEndTime_IST(Long time) {
		if (time != null && time % getDayStartTime(time) != 0) {
			return getDayStartTime_IST(time) + DateTimeUtils.MILLIS_PER_DAY;
		}
		return time;
	}

	public static long getWeekStartTime(long time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_GMT));
		cal.setTimeInMillis(time);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return cal.getTimeInMillis();
	}

    public static boolean isValidDateForAbsentFeedback() {
		int dayOfMonth = LocalDate.now().getDayOfMonth();
		return dayOfMonth == 1 || dayOfMonth == 15;
	}

    public String getListString(List<String> values) {
		// Supported only in JDK 8
		return (values == null ? "" : Arrays.toString(values.toArray()));
	}

	public static <E> E getLastObject(List<E> elements) {
		E element = null;
		if (elements != null) {
			element = elements.get(elements.size() - 1);
		}

		return element;
	}

	public static long getSlotStartTime(long time) {
		return getSlotStartTime(time, SLOT_LENGTH);
	}

	public static long getSlotStartTime(long startTime, long duration) {
		return startTime - (startTime % duration);
	}

	public static long getSlotEndTime(long time) {
		return (time % SLOT_LENGTH == 0) ? time : time + SLOT_LENGTH - (time % SLOT_LENGTH);
	}

	public static int calculateTotalSlots(long startTime, long endTime) {
		Long slotCount = (getSlotStartTime(endTime) - getSlotStartTime(startTime)) / SLOT_LENGTH;
		return slotCount.intValue();
	}

	public static long getSlotLength() {
		return SLOT_LENGTH;
	}

	public static int getCalendarEntrySlotCount() {
		return SLOT_COUNT;
	}

	public static int getCalendarEntryMinIndex() {
		return 0;
	}

	public static int getCalendarEntryMaxIndex() {
		return SLOT_COUNT - 1;
	}

	public static long getAbsoluteSlotTime(int index, long dayStartTime) {
		return dayStartTime + index * getSlotLength();
	}

	public static int getSlotStartIndex(long startTime, long dayStartTime) {
		long SLOT_LENGTH = CommonCalendarUtils.getSlotLength();
		startTime = startTime % SLOT_LENGTH == 0 ? startTime : startTime - (startTime % SLOT_LENGTH);
		return (int) ((startTime - dayStartTime) / SLOT_LENGTH);
	}

	public static int getSlotEndIndex(long startTime, long dayStartTime) {
		long SLOT_LENGTH = CommonCalendarUtils.getSlotLength();
		startTime = startTime % SLOT_LENGTH == 0 ? startTime - SLOT_LENGTH : startTime - (startTime % SLOT_LENGTH);
		return (int) ((startTime - dayStartTime) / SLOT_LENGTH);
	}

	public static void removeOverlapTime(SessionSlot sessionSlot, long startTime, long endTime) {
		if (!(sessionSlot.getStartTime() > endTime || sessionSlot.getEndTime() < startTime)) {
			if (sessionSlot.getStartTime() > startTime) {
				// TODO : Implement this
			}
		}
	}

	public static List<SessionSlot> getTimeIntervalsToCheckForConflicts(Long oldStartTime, Long oldEndTime,
																  Long newStartTime, Long newEndTime) throws VException {
		List<SessionSlot> ranges = new ArrayList<SessionSlot>();

		// validation checks for session update times
		if (null == oldStartTime || null == oldEndTime || null == newStartTime || null == newEndTime) {
			throw new VException(ErrorCode.SESSION_INVALID_TIME_PERIOD, "Invalid timestamps");
		}

		if (oldEndTime < oldStartTime || newEndTime < newStartTime) {
			throw new VException(ErrorCode.SESSION_INVALID_TIME_PERIOD, "Invalid time slots passed");
		}

		// rounding off
		// startTimes to the present 15 min block start time
		oldStartTime = getSlotStartTime(oldStartTime);
		newStartTime = getSlotStartTime(newStartTime);

		// endTimes to the present 15 min block end time
		oldEndTime = getSlotEndTime(oldEndTime);
		newEndTime = getSlotEndTime(newEndTime);

		// no need to update session timings -- return an empty range
		if (oldStartTime.equals(newStartTime) && oldEndTime.equals(newEndTime))
			return ranges;

		// there is no need to unblock the existing session slot, instead search for ranges in new update slot to check for conflicts
		// only six valid permutations of the above four values are valid as oldStartTime > oldEndTime and newStartTime > newEndTime

		// mutually disjoint slots
		if (newStartTime >= oldEndTime) {
			ranges.add(new SessionSlot(newStartTime, newEndTime));
			return ranges;
		}
		if (newEndTime <= oldStartTime) {
			ranges.add(new SessionSlot(newStartTime, newEndTime));
			return ranges;
		}


		// oldStartTime occurs first -- then newStartTime
		if ((oldStartTime <= newStartTime) && (oldEndTime <= newEndTime)) {
			if (oldEndTime < newEndTime) ranges.add(new SessionSlot(oldEndTime, newEndTime));
			return ranges;
		} else if ((oldStartTime <= newStartTime) && (newEndTime <= oldEndTime)) {
			// no conflict slots to check for
			return ranges;
		}

		// newStartTime occurs first -- then oldStartTime
		if (newStartTime<=oldStartTime && oldEndTime <= newEndTime) {
			if (newStartTime < oldStartTime) ranges.add(new SessionSlot(newStartTime, oldStartTime));
			if (oldEndTime < newEndTime) ranges.add(new SessionSlot(oldEndTime, newEndTime));
			return ranges;
		} else if (newStartTime <= oldStartTime && newEndTime <= oldEndTime) {
			if (newStartTime < oldStartTime) ranges.add(new SessionSlot(newStartTime, oldStartTime));
			return ranges;
		}

		return ranges;
	}

}
