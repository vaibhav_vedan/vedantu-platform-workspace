package com.vedantu.util.scheduling;

import java.util.Arrays;
import java.util.BitSet;

public class SyncCalendarSessionsReq {
	private String userId;
	private Long startTime;
	private Long endTime;
	private long[] sessionBitSet;
	private long[] otfSessionBitSet;

	public SyncCalendarSessionsReq() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public long[] getSessionBitSet() {
		return sessionBitSet;
	}

	public void setSessionBitSet(long[] sessionBitSet) {
		this.sessionBitSet = sessionBitSet;
	}

	public long[] getOtfSessionBitSet() {
		return otfSessionBitSet;
	}

	public void setOtfSessionBitSet(long[] otfSessionBitSet) {
		this.otfSessionBitSet = otfSessionBitSet;
	}

	public BitSet fetchSessionBitSet() {
		if (this.sessionBitSet != null) {
			return BitSet.valueOf(this.sessionBitSet);
		} else {
			return new BitSet();
		}
	}

	public BitSet fetchOTFSessionBitSet() {
		if (this.otfSessionBitSet != null) {
			return BitSet.valueOf(this.otfSessionBitSet);
		} else {
			return new BitSet();
		}
	}

	@Override
	public String toString() {
		return "SyncCalendarSessionsReq [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", sessionBitSet=" + Arrays.toString(sessionBitSet) + ", otfSessionBitSet="
				+ Arrays.toString(otfSessionBitSet) + "]";
	}
}
