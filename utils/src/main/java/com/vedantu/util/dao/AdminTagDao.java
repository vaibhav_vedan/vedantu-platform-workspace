package com.vedantu.util.dao;

import com.mongodb.client.MongoCollection;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.dbutils.UtilsMongoClientFactory;
import com.vedantu.util.pojo.CollectionName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.bson.Document;

@Service
public class AdminTagDao extends AbstractMongoDAO {

    public AdminTagDao() {
        super();
    }

    @Autowired
    private UtilsMongoClientFactory mongoClientFactory;

    private final String appName = ConfigUtils.INSTANCE.getStringValue("application.name");

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public List<CollectionName> getAllCollectionDetails() {
        List<CollectionName> names = new ArrayList<>();
        Set<String> collectionNames = getMongoOperations().getCollectionNames();
        for (String name : collectionNames) {
            MongoCollection<Document> collection = getMongoOperations().getCollection(name);
            Document one = collection.find().first();

            String className = String.valueOf(one != null ? one.get("_class") : "");
            CollectionName user = new CollectionName(name, className, appName);
            names.add(user);
        }
        return names;
    }
}
