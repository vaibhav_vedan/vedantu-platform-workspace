package com.vedantu.util;

public enum ContextType {
	
	NONE, USER, SESSION;
}