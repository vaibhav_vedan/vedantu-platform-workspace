package com.vedantu.util;

import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.LocationInfo;
import com.vedantu.util.enums.LocationAddedBy;
import com.vedantu.util.redis.AbstractRedisDAO;
import com.vedantu.util.response.ApiGurusGeolocationRes;

@Service
public class IPUtil {

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(IPUtil.class);

    private final String apigurusUrl = ConfigUtils.INSTANCE.getStringValue("ip_location.apigurus.api");

    @Autowired
    private AbstractRedisDAO abstractRedisDAO;

    public LocationInfo getLocationFromIp(String ip) {
        String jsonString = fetchFromApiGurus(ip);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        }catch(Exception e){
            logger.warn("Error in getting location from apigurus");
            return null;
        }

        if (!jsonObject.has("geolocation_data")) {
            return null;
        }
        JSONObject data = jsonObject.getJSONObject("geolocation_data");
        ApiGurusGeolocationRes res = new Gson().fromJson(data.toString(), ApiGurusGeolocationRes.class);

        LocationInfo nlInfo = new LocationInfo();
        if (res != null) {
            nlInfo.setCity(res.getCity());
            nlInfo.setState(res.getRegion_name());
            nlInfo.setCountry(res.getCountry_name());
            nlInfo.setPincode(res.getPostal_code());
            nlInfo.setIsp(res.getIsp());
            nlInfo.setAddedBy(LocationAddedBy.SYSTEM);
        }

        return nlInfo;
    }

    public LocationInfo getLocationFromIpForBotFilter(String ip) {
        LocationInfo nlInfo = new LocationInfo();
        String jsonString = fetchFromApiGurus(ip);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        }
        catch(Exception e){
            logger.warn("Error in fetching location from apigurus" + e.getMessage());
            return null;
        }
        if (!jsonObject.has("geolocation_data")) {
            if(jsonObject.has("query_status")){
                JSONObject jsonObject1 = jsonObject.getJSONObject("query_status");
                if(jsonObject1.has("query_status_code")){
                    String data = jsonObject1.getString("query_status_code");
                    nlInfo.setQueryStatusCode(data);
                }
                return nlInfo;
            }
            return null;
        }
        JSONObject data = jsonObject.getJSONObject("geolocation_data");
        ApiGurusGeolocationRes res = new Gson().fromJson(data.toString(), ApiGurusGeolocationRes.class);


        if (res != null) {
            nlInfo.setCity(res.getCity());
            nlInfo.setState(res.getRegion_name());
            nlInfo.setCountry(res.getCountry_name());
            nlInfo.setPincode(res.getPostal_code());
            nlInfo.setIsp(res.getIsp());
            nlInfo.setAddedBy(LocationAddedBy.SYSTEM);
        }

        return nlInfo;
    }

    public String fetchFromApiGurus(String ip) {
        if(StringUtils.isEmpty(ip)){
            return "";
        }
        String[] ips = ip.split(","); // if in case multiple addresses separated with `,` => we take first one
        ClientResponse response = WebUtils.INSTANCE.doCall(String.format(apigurusUrl, ips[0]), HttpMethod.GET, null);
        String jsonString = response.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    public String fetchFromApiGurus(String ip, boolean usecache) {
        if (usecache) {
            logger.info("using cache");
            try {
                String resp = abstractRedisDAO.get(getKeyForIPAddressInfo(ip));
                if (StringUtils.isNotEmpty(resp)) {
                    return prepareAPIGurusResponse(resp, ip);
                } else {
                    return fetchFromApiGurus(ip);
                }
            } catch (Exception ex) {
                logger.warn(ex.getMessage());
                return fetchFromApiGurus(ip);
            }
        } else {
            //not saving back in redis, redis info is populated by clickstream only
            return fetchFromApiGurus(ip);
        }
    }

    private String getKeyForIPAddressInfo(String ipaddress) {
        return "IP_" + ipaddress;
    }

    private String prepareAPIGurusResponse(String redisResponse, String ip) {
        //adding extra things to make it look exactly like apigurus response
        JSONObject queryJSON = new JSONObject();
        queryJSON.put("query_status_code", "OK");
        queryJSON.put("query_status_description", "Query successfully performed.");
        JSONObject redisResponseJSON = new JSONObject(redisResponse);

        JSONObject finalResponse = new JSONObject();
        finalResponse.put("query_status", queryJSON);
        finalResponse.put("ip_address", ip);
        finalResponse.put("geolocation_data", redisResponseJSON);
        return finalResponse.toString();
    }
}
