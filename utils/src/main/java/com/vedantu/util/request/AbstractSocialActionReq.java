/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.request;

import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.enums.FeatureContext;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class AbstractSocialActionReq extends AbstractFrontEndReq {

    private SocialContextType socialContextType;//root level entity on which vote is given like video where votecontext is playlist
    private String contextId;
    private FeatureContext featureContext;//context in which the above root entity is residing like playlist
    private String featureContextId;

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public FeatureContext getFeatureContext() {
        return featureContext;
    }

    public void setFeatureContext(FeatureContext featureContext) {
        this.featureContext = featureContext;
    }

    public String getFeatureContextId() {
        return featureContextId;
    }

    public void setFeatureContextId(String featureContextId) {
        this.featureContextId = featureContextId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (socialContextType == null) {
            errors.add("socialContextType");
        }
        if (StringUtils.isEmpty(contextId)) {
            errors.add("contextId");
        }

        return errors;
    }
}
