package com.vedantu.util.request;

public enum OrderEndType {
    USER_CHURN, WRONG_ORDER_MADE, USER_COURSE_CHANGE, TRIAL_END, OTHERS
}
