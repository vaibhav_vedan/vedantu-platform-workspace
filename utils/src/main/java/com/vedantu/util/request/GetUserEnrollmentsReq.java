package com.vedantu.util.request;

import com.vedantu.User.Role;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;
import java.util.Set;

/**
 * Created by Darshit
 */
public class GetUserEnrollmentsReq extends AbstractFrontEndReq {

    private Long userId;
    private boolean includeCourseId;
    private Integer size;
    private Integer start;

    public GetUserEnrollmentsReq() {

    }

    public GetUserEnrollmentsReq(long userId, boolean includeCourseId) {
        this.userId=userId;
        this.includeCourseId=includeCourseId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean getIncludeCourseId() {
        return includeCourseId;
    }

    public void setIncludeCourseId(boolean includeCourseId) {
        this.includeCourseId = includeCourseId;
    }

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}
}
