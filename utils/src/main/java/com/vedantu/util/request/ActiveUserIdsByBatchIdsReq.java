package com.vedantu.util.request;

import com.vedantu.User.Role;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchChangeTime;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Darshit
 */
public class ActiveUserIdsByBatchIdsReq extends AbstractFrontEndReq {

    ArrayList<String> batchIds;
    int start;
    int size;
    private List<BatchChangeTime> batchChangeTime;

    public ArrayList<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(ArrayList<String> batchIds) {
        this.batchIds = batchIds;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<BatchChangeTime> getBatchChangeTime() {
        return batchChangeTime;
    }

    public void setBatchChangeTime(List<BatchChangeTime> batchChangeTimes) {
        this.batchChangeTime = batchChangeTimes;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (size > AbstractMongoDAO.MAX_ALLOWED_FETCH_SIZE) {
            errors.add("size can not be greate then 2000");
        }
            
        return errors;
    }
}
