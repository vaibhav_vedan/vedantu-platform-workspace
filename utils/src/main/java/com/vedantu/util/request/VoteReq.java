/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.request;

import java.util.List;

/**
 *
 * @author ajith
 */
public class VoteReq extends AbstractSocialActionReq {

    private int voteValue; //only for voting or like/dislike

    public int getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(int voteValue) {
        this.voteValue = voteValue;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        return super.collectVerificationErrors();
    }

}
