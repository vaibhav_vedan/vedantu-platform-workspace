package com.vedantu.util.request;

import com.vedantu.util.pojo.AdminTag;
import com.vedantu.util.pojo.CollectionName;
import lombok.Data;

import java.util.List;

@Data
public class AdminTagAddOrRemoveReq {
    private List<AdminTag> adminTags;
    private boolean removeTags;
    private CollectionName collectionName;
}
