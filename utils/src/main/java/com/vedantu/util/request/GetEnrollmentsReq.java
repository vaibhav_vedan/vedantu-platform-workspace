package com.vedantu.util.request;

import com.vedantu.User.Role;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by somil on 27/01/17.
 */
public class GetEnrollmentsReq extends AbstractFrontEndReq {

    private EntityStatus status;
    private List<EntityStatus> statuses;
    private Integer start;
    private Integer size;
    private Long fromTime;
    private Long tillTime;
    private Long userId;
    private Role role;
    private EnrollmentState state;
    private Boolean fillBatchCourseInfos = Boolean.TRUE;
    private List<String> enrollmentIds;
    private String batchId;
    private String entityId;
    private EntityType entityType;
    private Boolean batchIdExists;
    private Set<String> multipleBatchIds;
    private boolean contentCountRequired = false;
    private Long lastUpdated;
    private Boolean removeDeEnrolled;


    private List<String> includeFields;
    private List<String> excludeFields;

    public Boolean getRemoveDeEnrolled() {
        return removeDeEnrolled;
    }

    public void setRemoveDeEnrolled(Boolean removeDeEnrolled) {
        this.removeDeEnrolled = removeDeEnrolled;
    }

    public EntityStatus getStatus() {
        return status;
    }

    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public EnrollmentState getState() {
        return state;
    }

    public void setState(EnrollmentState state) {
        this.state = state;
    }

    public List<String> getEnrollmentIds() {
        return enrollmentIds;
    }

    public void setEnrollmentIds(List<String> ids) {
        this.enrollmentIds = ids;
    }

    public Boolean getFillBatchCourseInfos() {
        return fillBatchCourseInfos;
    }

    public void setFillBatchCourseInfos(Boolean fillBatchCourseInfos) {
        this.fillBatchCourseInfos = fillBatchCourseInfos;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the entityType
     */
    public EntityType getEntityType() {
        return entityType;
    }

    /**
     * @param entityType the entityType to set
     */
    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Boolean getBatchIdExists() {
        return batchIdExists;
    }

    public void setBatchIdExists(Boolean batchIdExists) {
        this.batchIdExists = batchIdExists;
    }

    public List<EntityStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<EntityStatus> statuses) {
        this.statuses = statuses;
    }

    public Set<String> getMultipleBatchIds() {
        return multipleBatchIds;
    }

    public void setMultipleBatchIds(Set<String> multipleBatchIds) {
        this.multipleBatchIds = multipleBatchIds;
    }

    /**
     * @return the contentCountRequired
     */
    public boolean isContentCountRequired() {
        return contentCountRequired;
    }

    /**
     * @param contentCountRequired the contentCountRequired to set
     */
    public void setContentCountRequired(boolean contentCountRequired) {
        this.contentCountRequired = contentCountRequired;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<String> getIncludeFields() {
        if(includeFields == null){
            includeFields= new ArrayList<>();
        }
        return includeFields;
    }

    public void setIncludeFields(List<String> includeFields) {
        this.includeFields = includeFields;
    }

    public List<String> getExcludeFields() {
        return excludeFields;
    }

    public void setExcludeFields(List<String> excludeFields) {
        this.excludeFields = excludeFields;
    }
}
