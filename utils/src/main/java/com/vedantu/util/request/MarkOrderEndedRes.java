package com.vedantu.util.request;

import com.vedantu.util.enums.ResponseCode;
import com.vedantu.util.fos.response.AbstractRes;

public class MarkOrderEndedRes extends AbstractRes {
    private ResponseCode responseCode = ResponseCode.SUCCESS;
    private String orderId;

    public ResponseCode getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
