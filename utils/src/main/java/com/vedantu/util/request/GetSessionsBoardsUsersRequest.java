package com.vedantu.util.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetSessionsBoardsUsersRequest extends AbstractFrontEndReq {

	public List<Long> subscriptionIds;
	public List<Long> userIds;
	public List<Long> boardIds;

	public GetSessionsBoardsUsersRequest() {
		super();
	}


	public GetSessionsBoardsUsersRequest(List<Long> subscriptionIds,
			List<Long> userIds, List<Long> boardIds) {
		super();
		this.subscriptionIds = subscriptionIds;
		this.userIds = userIds;
		this.boardIds = boardIds;
	}

	public List<Long> getSubscriptionIds() {
		return subscriptionIds;
	}

	public void setSubscriptionIds(List<Long> subscriptionIds) {
		this.subscriptionIds = subscriptionIds;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	public List<Long> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(List<Long> boardIds) {
		this.boardIds = boardIds;
	}

}
