/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.request;

import java.util.List;

/**
 *
 * @author ajith
 */
public class BookmarkReq extends AbstractSocialActionReq {

    private boolean bookmark;

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        return super.collectVerificationErrors();

    }

}
