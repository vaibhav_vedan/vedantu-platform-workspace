package com.vedantu.util.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.enums.CommunicationKind;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommunicationDataReq extends AbstractFrontEndReq {

	private List<String> userIds;
	private List<String> batchIds;
	private List<String> otmBundleIds;
	private List<String> aioPackageIds;
	private List<String> emailIds;
	private CommunicationKind communicationKind;
	private String body;
	private String subject;
	private long scheduledTime;
	private long expiryTime;

	// For notice-board implementation
	private List<String> allUsers;
	private String manualNotificationId;

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if(Objects.isNull(communicationKind)){
			errors.add("You need to specify the kind of communication");
		}

		if(CommunicationKind.EMAIL.equals(communicationKind) && Objects.isNull(subject)){
			errors.add("Subject can't be null for email");
		}

		if(Objects.isNull(body)){
			errors.add("Message body couldn't be null");
		}

		if(ArrayUtils.isNotEmpty(emailIds) && !CommunicationKind.EMAIL.equals(communicationKind)){
			errors.add("If standalone email ids are specified then you need to send an email only");
		}

		if(ArrayUtils.isEmpty(userIds) && ArrayUtils.isEmpty(batchIds) && ArrayUtils.isEmpty(otmBundleIds) &&  ArrayUtils.isEmpty(aioPackageIds) && ArrayUtils.isEmpty(emailIds)){
			errors.add("You need to specify one of userIds, batchIds, otmBundleIds or aioPackageIds or emailIds");
		}

		if(Objects.isNull(communicationKind)){
			errors.add("You need to specify the type of communication");
		}

		if(CommunicationKind.SMS.equals(communicationKind)){
			if(body.length()>160){
				errors.add("Message size of SMS couldn't exceed 160 characters");
			}
		}

		if(CommunicationKind.NOTICE_BOARD.equals(communicationKind)){
			if(body.length()>240){
				errors.add("Message size of SMS couldn't exceed 160 characters");
			}
			if(expiryTime==0){
				errors.add("Expiry time in mandatory in case of notice board");
			}
			if(expiryTime != 0 && expiryTime < System.currentTimeMillis()){
				errors.add("You can't expire a notice in past");
				}
			if(scheduledTime != 0 && expiryTime != 0 && scheduledTime >= expiryTime){
				errors.add("Schedule time can't be greater or equal to the expiry time");
			}
		}
		if(ArrayUtils.isNotEmpty(userIds) && userIds.size() > 1000){
			errors.add("Not allowed to send more than 1000 userIds");
		}
		if(ArrayUtils.isNotEmpty(emailIds) && emailIds.size() > 1000){
			errors.add("Not allowed to send more than 1000 emails");
		}
//		if(ArrayUtils.isNotEmpty(batchIds) && batchIds.size() > 10){
//			errors.add("Not allowed to send more than 10 batches");
//		}
//		if(ArrayUtils.isNotEmpty(aioPackageIds) && aioPackageIds.size() > 10){
//			errors.add("Not allowed to send more than 10 aioPackageIds");
//		}
//		if(ArrayUtils.isNotEmpty(otmBundleIds) && otmBundleIds.size() > 10){
//			errors.add("Not allowed to send more than 10 otmBundleIds");
//		}

		return errors;
	}
}
