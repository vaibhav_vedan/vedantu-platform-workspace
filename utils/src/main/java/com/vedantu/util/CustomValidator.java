/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

/**
 *
 * @author somil
 */
@Service
public class CustomValidator {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CustomValidator.class);

    private static Pattern pattern;
//    static final String emailExpression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
//    static final String emailExpressionAddedAlias = "^[\\w\\.-]+[\\+]{0,1}+[0-9]{0,9}+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
    static final String emailExpression = "^[\\w\\.-]+@(\\d*[A-Z]{1,64}\\d*\\.)+[A-Z]{2,4}$";
    static final String emailExpressionAddedAlias = "^[\\w\\.-]+[\\+]{0,1}+[0-9]{0,9}+@(\\d*[A-Z]{1,64}\\d*\\.)+[A-Z]{2,4}$";
    static final String phoneRegex = "^[0-9]{6,14}$";
    static final String userIdRegex = "^[0-9]{16}$";

    public CustomValidator() {
        pattern = Pattern.compile(ConfigUtils.INSTANCE.getBooleanValue("email.added.alias.enabled") ? emailExpressionAddedAlias
                : emailExpression, Pattern.CASE_INSENSITIVE);
    }

    public static boolean validEmail(String email) {
        if (StringUtils.isNotEmpty(email) && email.endsWith("@vedantu.com")) {
            return true;
        } else {
            pattern = Pattern.compile(ConfigUtils.INSTANCE.getBooleanValue("email.added.alias.enabled") ? emailExpressionAddedAlias
                    : emailExpression, Pattern.CASE_INSENSITIVE);
            return StringUtils.isNotEmpty(email)
                    && pattern.matcher(email).matches();
        }
    }

    public static boolean isVedantuEmail(String email) {
        if (null != email && email.toLowerCase().endsWith("@vedantu.com")) {
            if (email.toLowerCase().contains("student")) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean validUserId(String userId) {
        pattern = Pattern.compile(userIdRegex);
        return StringUtils.isNotEmpty(userId) && pattern.matcher(userId).matches();
    }

    public static boolean validPhoneNumber(String phoneNo) {

        pattern = Pattern.compile(phoneRegex);
        return StringUtils.isNotEmpty(phoneNo) && pattern.matcher(phoneNo).matches();

//        if (StringUtils.isEmpty(phoneNo)) {
//            return false;
//        } else // validate phone numbers of format "1234567890"
//        if (phoneNo.matches("\\d{10}")) {
//            return true;
//        } // validating phone number with -, . or spaces
//        else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) {
//            return true;
//        } // validating phone number where area code is in braces ()
//        else if (phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) {
//            return true;
//        } // return false if nothing matches the input
//        else {
//            return false;
//        }
    }

    public static boolean validPassword(String password) {
        if (StringUtils.isEmpty(password)) {
            return false;
        } else if (password.length() < 5) {
            return false;
        }
        return true;
    }

}
