package com.vedantu.util.logger;

import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class LoggingMarkers {
    public static final Marker JSON_MASK = MarkerManager.getMarker("JSON-MASK");
}
