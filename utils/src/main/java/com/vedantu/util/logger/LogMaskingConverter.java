package com.vedantu.util.logger;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Plugin(name = "LogMaskingConverter", category = "Converter")
@ConverterKeys({"cm"})
public class LogMaskingConverter extends LogEventPatternConverter {
    private static final String NAME = "cm";
   /* private static final String CREDIT_CARD_REGEX = "([0-9]{16})";;
    private static final Pattern CREDIT_CARD_PATTERN = Pattern.compile(CREDIT_CARD_REGEX);
    private static final String CAREDIT_CARD_REPLACEMENT_REGEX = "XXXXXXXXXXXXXXXX";*/

   private static final String MATCHER = "[\\w$:=_%]*";

    private static final String SECRET_KEY_REGEX =
            "(secretKey" +
            "|accessKey" +
            "|access_code" +
            "|merchant_id" +
            "|wstoken" +
            "|apikey" +
            "|password" +
            "|Authorization" +
            "|key" +
            "|password" +
            "|secret" +
            "|code" +
            "|X-Ved-Client" +
            "|X-Ved-Client-Id" +
            "|X-Ved-Client-Secret" +
            "|passwd" +
                    ")" + MATCHER;

    private static final Pattern SECRET_KEY_PATTERN = Pattern.compile(SECRET_KEY_REGEX, Pattern.CASE_INSENSITIVE);
    private static final String SECRET_KEY_REPLACEMENT_REGEX = "XXXXXXXXXXXXXXXX";

    protected LogMaskingConverter(String name, String style) {
        super(name, style);
    }

    LogMaskingConverter(String[] options) {
        super(NAME, NAME);
    }

    public static LogMaskingConverter newInstance(String[] options) {
        return new LogMaskingConverter(options);
    }

    @Override
    public void format(LogEvent event, StringBuilder outputMessage) {

        final StackTraceElement element = event.getSource();

        if (element != null) {
            outputMessage.append(element.getMethodName());
        }

        String message = event.getMessage().getFormattedMessage();

        if(message.length()>10000)
            message = message.substring(0, 10000);

        String maskedMessage = message;

        if (event.getMarker()!=null && event.getMarker().getName() == LoggingMarkers.JSON_MASK.getName()) {

            try {
                maskedMessage = mask(message);

            } catch (Exception e) {
                maskedMessage = message;
            }

        }

        outputMessage.append(maskedMessage);

    }

    private String mask(String message) {
        Matcher matcher =null;
        StringBuffer buffer = new StringBuffer();

        matcher = SECRET_KEY_PATTERN.matcher(message);
        maskMatcher(matcher, buffer,SECRET_KEY_REPLACEMENT_REGEX);

        /*message=buffer.toString();
        buffer.setLength(0);
        matcher = CREDIT_CARD_PATTERN.matcher(message);
        maskMatcher(matcher, buffer,CAREDIT_CARD_REPLACEMENT_REGEX);*/

        return buffer.toString();
    }

    private StringBuffer maskMatcher(Matcher matcher, StringBuffer buffer, String maskStr)
    {
        while (matcher.find()) {
            matcher.appendReplacement(buffer,maskStr);
        }
        matcher.appendTail(buffer);
        return buffer;
    }
}
