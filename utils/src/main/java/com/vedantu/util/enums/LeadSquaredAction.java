package com.vedantu.util.enums;

public enum LeadSquaredAction {
	POST_LEAD_DATA, POST_LEAD_ACTIVITY, SYNC_USER_DATA, SYNC_SESSION_DATA, RETRY_UPDATE
}
