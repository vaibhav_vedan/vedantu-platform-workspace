/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.enums;

public enum MessageType {
	SESSION_SCHEDULE, SESSION_RESCHEDULE, SESSION_CANCELLATION, CUSTOM_PACKAGE, ASK_DOUBT, AVAIL_FREE_SESSION, OTHER, CUSTOM_PLAN, PKG_BOOK_SESSION, TRIAL_SESSION,
        FIND_ME_TEACHER, POST_YOUR_REQUIREMENT;
}
