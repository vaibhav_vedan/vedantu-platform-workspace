/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.enums;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public enum SortOrder {
	ASC, DESC;

	public static SortOrder valueQuitentOf(String value) {
		SortOrder sortOrder = SortOrder.ASC;
		for (SortOrder sOrder : values()) {
			if (StringUtils.equalsIgnoreCase(value, sOrder.name())) {
				sortOrder = sOrder;
			}
		}
		return sortOrder;
	}
}

