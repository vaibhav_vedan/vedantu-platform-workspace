package com.vedantu.util.enums;

public enum OfferingType {
	DOUBT {
		@Override
		public String getDisplayString() {
			return "Doubt Solving";
		}
	},
	TOPIC {
		@Override
		public String getDisplayString() {
			return "Topic Booster";
		}
	},
	TERM {
		@Override
		public String getDisplayString() {
			return "Exam ";
		}
	},
	TUITION {
		@Override
		public String getDisplayString() {
			return "Monthly Tuition ";
		}
	},
	@Deprecated
	TUTION {
		@Override
		public String getDisplayString() {
			return null;
		}
	};

	public abstract String getDisplayString();
}
