package com.vedantu.util.enums;

public enum OfferingSubType {
	DEFAULT, STRUCTURED, US_OFFERING, UAE_OFFERING, BLENDED, EXAM_PREPARATION, BUNDLED, INTERNATIONAL
}
