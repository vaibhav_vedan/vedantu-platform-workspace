/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.enums;

/**
 *
 * @author jeet
 */
public enum SNSTopic implements SNSTopicIFace {
    SESSION_EVENTS,
    SESSION_DELAYED_EVENTS,
    DELETE_GTT_RECORDING,
    ENROLL_BUNDLE_REFERRAL,
    CMS_WEBINAR_EVENTS,
    PROCESS_OTF_POLLS_DATA,
    DATA_TO_S3_DONE,
    EQ_DONE,
    REPLAY_SESSION_PREPARED,
    SESSION_EVENTS_TEMP,
    USER_EVENTS,
    DATA_TO_S3_DONE_OTF,
    UPDATE_LEAD_PAYMENT_LS,
    WEBINAR_HOMEDEMO_LEADSQUARED,
    SALES_DEMO_LEADSQUARED,
    REVISEINDIA_POSTCLASS,
    REVISEINDIA_POSTTEST,
    UPDATE_LEAD_GPS_TO_LS,
    PAGE_SPEED_LIGHT_HOUSE,
    REVISE_JEE_POSTTEST,
    UPDATE_MEMBER_COUNT,
    TARGET_JEE_NEET_POSTTEST,
    TEACHER_CONTENT_DASHBOARD_UPDATE,
    SCHEDULING_REDIS_OPS,
    DINERO_REDIS_OPS,
    EXPORT_ORDERS_CSV_SNS,
    BATCH_CURRICULUM_UPDATE,
    VSAT_EVENT_UPDATED,
    UPDATE_COURSE_EVENTS;

    public String getTopicName(String env) {
        String queueName = this.name() + "_" + env.toUpperCase();
        return queueName;
    }
}
