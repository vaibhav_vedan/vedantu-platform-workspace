package com.vedantu.util.enums;

import com.vedantu.util.StringUtils;

/**
 * Created by somil on 31/08/17.
 */
public enum SQSQueue {

    //todo remove the below fifo queue
    NOTIFICATION_FIFO_QUEUE("notification", true, "30", 100000d, 21600d, "2"), NOTIFICATION_FIFO_QUEUE_DL("notificationDeadLetter", true, "300", 10d, 21600d, "1"),
    NOTIFICATION_QUEUE("notificationNonFifo", false, "30", 500000d, 21600d, "5"), NOTIFICATION_QUEUE_DL("notificationNonFifoDeadLetter", false, "300", 10d, 21600d, "1"), CHALLENGES_QUEUE("challenges", true), OTF_RECORDING("otfRecording", true, "7200"), OTF_RECORDING_DL("otfRecordingDeadLetter", true),
    POST_SESSION_SUMMARY_EMAILS("postSessionSummaryEmailsNonFifo", false, "1800", 100000d, 21600d, "5"), POST_SESSION_SUMMARY_EMAILS_DL("postSessionSummaryEmailsNonFifoDl", false),
    OTF_SESSION_QUEUE("otfSession", true, "10800"), OTF_SESSION_QUEUE_DL("otfSessionDeadLetter", true), CLEVERTAP_QUEUE("cleverTap", false, "600", 100000d, 21600d),
    OTF_POSTSESSION_QUEUE("otfPostSession", true, "1800"),
    CONSUMPTION_QUEUE("consumption", false, "1200"), TRIGGER_SESSION_CONSUMPTION_QUEUE("triggerSessionConsumption", false, "1200"), CONSUMPTION_DL("consumptionDeadLetter", false), TRIGGER_SESSION_CONSUMPTION_DL("triggerSessionConsumptionDeadLetter", false),
    LEADSQUARED_QUEUE("leadSquaredNonFifo", false, "60",1000d,21600d,"5"), LEADSQUARED_QUEUE_DL("leadSquaredDeadLetterNonFifo", false), DINERO_INVOICES_QUEUE("studentInvoices", true),DINERO_INVOICES_QUEUE_DL("studentInvoicesDeadLetter", true),
    POST_CHANGE_ANSWER_QUEUE("postChangeAnswer", true, "1800"), POST_CHANGE_ANSWER_QUEUE_DL("postChangeAnswerDeadLetter", true),
    POST_CHANGE_QUESTION_QUEUE("postChangeQuestion", true, "1800"), POST_CHANGE_QUESTION_QUEUE_DL("postChangeQuestionDeadLetter", true),
    ACAD_MENTOR_DASHBOARD_QUEUE("acadmentor", false, "1800"), ACAD_MENTOR_DASHBOARD_DL("acadmentorDeadLetter", false),
    POST_TEST_ATTEMPT_OPS("postTestAttemptOps", true, "1800"), POST_TEST_ATTEMPT_OPS_DL("postTestAttemptOpsDeadLetter", true),
    AUDIT_QUEUE("audit", true, "1800", 1000000d, 86400d), VEDANTU_DATA_LEADSQUARED_QUEUE("vedantuDataLeadSquared", true, "1800", 10000d, 21600d), VEDANTU_DATA_LEADSQUARED_QUEUE_DL("vedantuDataLeadSquareDeadLetter", true),
    OTO_POSTSESSION_OPS("otoPostSessionOps", true, "1800"), OTO_POSTSESSION_OPS_DL("otoPostSessionOpsDeadLetter", true),
    BUNDLE_OPS("bundleOps", true, "30" , 1000d, 21600d, "3-25" ), BUNDLE_OPS_DL("bundleOpsDl", true),
    BUNDLE_OPS_2("bundleOps2", true, "300"  ), BUNDLE_OPS_2_DL("bundleOpsDl2", true),
    BUNDLE_AUTO_ENROLL_OPS("bundleAutoEnrollOps", true, "300"), BUNDLE_AUTO_ENROLL_OPS_DL("bundleAutoEnrollOpsDl", true),
    BUNDLE_NEW_ENROLL_OPS("bundleNewEnrollOps", true, "300"), BUNDLE_NEW_ENROLL_OPS_DL("bundleNewEnrollOpsDl", true),
    BUNDLE_ENROLL_STATUS_QUEUE("bundleEnrollStatusOps", true, "300"), BUNDLE_ENROLL_STATUS_QUEUE_DL("bundleEnrollStatusOpsDl", true),
    BATCH_ENROLL_STATUS_QUEUE("batchEnrollStatusOps", true, "300"), BATCH_ENROLL_STATUS_QUEUE_DL("batchEnrollStatusOpsDl", true),
    BUNDLE_CALENDAR_OPS("bundleCalendarOps", true, "300"), BUNDLE_CALENDAR_OPS_DL("bundleCalendarOpsDl", true),
    BUNDLE_CONTENT_SHARE_OPS("bundleContentShareOps", true, "300", 1000d, 21600d, "3-7"), BUNDLE_CONTENT_SHARE_OPS_DL("bundleContentShareOpsDl", true),
    BUNDLE_GTT_CREATION_OPS("bundleGTTCreationOps", true, "300"), BUNDLE_GTT_CREATION_OPS_DL("bundleGTTCreationOpsDl", true),
    UPDATE_LEADSQUARED_QUEUE("updateLeadsquaredQueue", true, "1800", 100000d, 86400d), UPDATE_LEADSQUARED_QUEUE_DL("updateLeadsquaredQueueDeadLetter", true),
    LEADSQUARED_EVENTS("leadsquaredEvents", false, "300",1000d,21600d), LEADSQUARED_EVENTS_DL("leadsquaredEventsDeadLetter", false, "300"),
    POST_TEST_END_OPS("postTestEndOps", false, "1800"), POST_TEST_END_OPS_DL("postTestEndOpsDeadLetter", false, "1800"),
    TEST_END_OPS("testEndOps", false, "1800"), TEST_END_OPS_DL("testEndOpsDeadLetter", false, "1800"),
    MIGRATION_NON_FIFO_OPS("migrationNonFifoOps", false, "1800"), MIGRATION_NON_FIFO_OPS_DL("migrationNonFifoOpsDl", false, "1800"),
    BASE_INSTALMENT_OPS("baseInstalmentOps", true, "1800"), BASE_INSTALMENT_OPS_DL("baseInstalmentOpsDl", true, "1800"),
    GAME_JOURNEY_OPS("gameJourney", true, "60"), GAME_JOURNEY_OPS_DL("gameJourneyOpsDl", true, "60"),
    GAME_JOURNEY_SESSION_STANDARD("game_journey_session_standard", false, "60", 1000d, 21600d, "3-7"), GAME_JOURNEY_SESSION_STANDARD_DL("game_journey_session_standard_dl", false, "60", 10d, 21600d, "3-7"),
    GAME_JOURNEY_SESSION_DOWNLOAD_OPS("gameJourneySessionDownload", true, "1800"), GAME_JOURNEY_SESSION_DOWNLOAD_OPS_DL("gameJourneySessionDownloadDl", true, "1800"),
    GAME_JOURNEY_TEST_OPS("gameJourneyTest", true, "1800"), GAME_JOURNEY_TEST_OPS_DL("gameJourneyOpsTestDl", true, "1800"),
    OTF_PRESESSION_STUDENTLIST_QUEUE("otfPreSessionStudentList", false, "30"),
    END_ENROLLMENT_OPS("endEnrollmentOps", true, "30"), END_ENROLLMENT_OPS_DL("endEnrollmentOpsDl", true, "30"),
    SUBSCRIPTION_PACKAGE_OPS("subscriptionPackageOps", true, "1800"), SUBSCRIPTION_PACKAGE_OPS_DL("subscriptionPackageOpsDl", true, "1800"),
    OTF_POSTSESSION_QUEUE_NON_FIFO("otfPostSessionNonFifo", false, "300", 25000d, 43200d), OTF_POSTSESSION_QUEUE_NON_FIFO_DL("otfPostSessionNonFifoDl", false, "300"),
    SEND_GPS_LOCALS_TO_LS("sendgpslocalstoLS", false, "300",1000d,21600d), SEND_GPS_LOCALS_TO_LS_DL("sendgpslocalstoLSDl", false, "300"),
    GAMIFICATION_TOOL_CSV_DOWNLOAD("gameToolCSVDownload", true, "300"), GAMIFICATION_TOOL_CSV_DOWNLOAD_DL("gameToolCSVDownloadDl", true, "300"),
    SEO_PAGE_REGENERATE_CONTENT_IMAGE("seoPageRegenerateContentImage", true, "1800"), SEO_PAGE_REGENERATE_CONTENT_IMAGE_DL("seoPageRegenerateContentImageDl", true, "1800"),
    PAGE_SPEED_LIGHT_HOUSE_SQS("pageSpeedLightHouseSQS", false, "1800"), PAGE_SPEED_LIGHT_HOUSE_SQS_DL("pageSpeedLightHouseSQSDl", false, "1800"),
    CALENDAR_OPS("calendarOps", false, "600"), CALENDAR_OPS_DL("calendarOpsDl", false, "30"),
    CALENDAR_OPS_FIFO("calendarOpsFifo", true, "600"), CALENDAR_OPS_FIFO_DL("calendarOpsFifoDl", true, "30"),
    CALENDAR_BATCH_OPS("calendar_batch_ops", false, "600", 1000d, 21600d, "1-5"), CALENDAR_BATCH_OPS_DL("calendar_batch_ops_dl", false, "30", 10d, 21600d, "1-5"),
    CALENDAR_BATCH_FIFO_OPS("calendar_batch_fifo_ops", true, "600", 1000d, 21600d, "1-5"), CALENDAR_BATCH_FIFO_OPS_DL("calendar_batch_fifo_ops_dl", true, "30", 10d, 21600d, "1-5"),
    SHARE_TEST_QUEUE("share_test", false, "600"), SHARE_TEST_DL_QUEUE("share_test_dl", false, "30"),
    SHARE_ASSIGNMENT_QUEUE("share_assignmentnew", false, "600"), SHARE_ASSIGNMENT_DL_QUEUE("share_assignmentnew_dl", false, "30"),
    SLASHRTC_ACTIVITY("slashrtc", false, "300"),
    VGROUP_QUEUE("vgroupQueue", false, "1800"), VGROUP_QUEUE_DL_QUEUE("vgroupQueueDl", false),
    GTT_MARK_DELETE_OPS_FIFO("gttDeleteOpsFifo", true, "600", 1000d,21600d, "1-5"), GTT_MARK_DELETE_OPS_FIFO_DL("gttDeleteOpsFifoDl", true, "1200"),
    USER_CITY_UPDATE_QUEUE("userCityUpdate", false, "600"), USER_CITY_UPDATE_DL_QUEUE("userCityUpdate_dl", false, "30"),
    GTT_UPDATE_OPS_FIFO("gttUpdateOpsFifo", true, "30", 1000d, 21600d, "1-15"), GTT_UPDATE_OPS_FIFO_DL("gttUpdateOpsFifoDl", true, "1200"),
    GTT_BULK_UPDATE_OPS_FIFO("gttBulkUpdateOpsFifo", true, "300", 1000d, 21600d, "1-5"), GTT_BULK_UPDATE_OPS_FIFO_DL("gttBulkUpdateOpsFifoDl", true, "1200"),
    GTT_ENROLMENT_OPS("gttEnrolmentOps", true, "30", 1000d, 21600d, "1-5"), GTT_ENROLMENT_OPS_DL("gttEnrolmentOpsDl", true, "1200"),
    COMMUNICATE_IN_BULK_QUEUE("communicateInBulk", true, "1800"), COMMUNICATE_IN_BULK_DL_QUEUE("communicateInBulk_dl", true, "30"),
    START_UPCOMING_WEBINAR("startUpcomingWebunars", false, "1800"),START_UPCOMING_WEBINAR_DL("startUpcomingWebunarsDl", false, "30"),
    SECTION_QUEUE("section_ops", true, "1800"), SECTION_DL_QUEUE("section_ops_dl", true),MASTERTALK_FEEDBACK("mastertalkFeedback", false, "600"), MASTERTALK_FEEDBACK_DL("mastertalkFeedbackDL", false),
    SECTION_NOTIFICATIONS_QUEUE("section_notifications_ops", false, "1800"), SECTION_NOTIFICATIONS_DL_QUEUE("section_notifications_ops_dl", false),
    SIGNUP_TASKS_QUEUE("signUpTasks", true, "600"), SIGNUP_TASKS_DL_QUEUE("signUpTasks_dl", true, "30"),
    VQUIZ_REFERRAL_QUEUE("vQuizReferral", false, "600"), VQUIZ_REFERRAL_DL_QUEUE("vQuizReferral_dl", false, "30"),
    VQUIZ_REMINDER_QUEUE("vQuizReminder", false, "600"), VQUIZ_REMINDER_DL_QUEUE("vQuizReminder_dl", false, "30"),
    TEACHER_CONTENT_DASHBOARD_OPS_QUEUE("teacherContentDashboardOps", false, "1800"), TEACHER_CONTENT_DASHBOARD_OPS_DL_QUEUE("teacherContentDashboardOpsDl", false),
    COURSE_PLAN_PLATFORM_OPS("course_plan_platform_ops", true, "90"), COURSE_PLAN_PLATFORM_OPS_DL("course_plan_platform_ops_dl", true),
    COURSE_PLAN_HOURS_TRANSACTION("course_plan_hours_transaction", true, "90"), COURSE_PLAN_HOURS_TRANSATION_DL("course_plan_hours_transaction_dl", true),
    ENROLLMENT_CREATED_LEADSQUARED_QUEUE("enrollmentCreatedLeadSquared", false, "100",1000d,21600d,"5"), ENROLLMENT_CREATED_LEADSQUARED_QUEUE_DL("enrollmentCreatedLeadSquareDeadLetter", false),
    ENROLLMENT_UPDATED_LEADSQUARED_QUEUE("enrollmentUpdatedLeadSquared", false, "300",1000d,21600d,"5"), ENROLLMENT_UPDATED_LEADSQUARED_QUEUE_DL("enrollmentUpdatedLeadSquareDeadLetter", false),
    CREATE_NEW_USER_ACCOUNT_QUEUE("create_new_user_account", true, "600", 1000d, 21600d, "3-7"), CREATE_NEW_USER_ACCOUNT_DL_QUEUE("create_new_user_account_dl", true, "30"),
    SEO_QUEUE("seoQueue", false, "100"), SEO_QUEUE_DL("seoQueueDl", false, "100"),
    PRE_LOGIN_DATA_QUEUE("preloginData", false, "180"), PRE_LOGIN_DATA_QUEUE_DL("preloginQueue_dl", false, "180"),
    VGLANCE_SESSION_UPDATE_DL("vglance_session_update_dl", false, "30"), VGLANCE_SESSION_UPDATE("vglance_session_update", false, "300", 5000d, 21600d, "5"),
    OTF_SESSION_GTT_CREATE_QUEUE("otf_session_gtt_create", true, "60", 1000d, 21600d, "1-3"), OTF_SESSION_GTT_CREATE_DL_QUEUE("otf_session_gtt_create_dl", true, "60", 1000d, 21600d, "1-3"),
    REGISTER_GTT_QUEUE("register_gtt_queue", false, "1800", 1000d, 21600d, "1-7"), REGISTER_GTT_DL_QUEUE("register_gtt_dl_queue", false, "1800", 1000d, 21600d, "1-7"),
    WEBINAR_GTT_DETAILS_UPSERT("webinarGttDetailsUpsert", false, "30"), WEBINAR_GTT_DETAILS_UPSERT_DL("webinarGttDetailsUpsertDl", false, "30"),
    WEBINAR_SESSION_DETAILS_UPSERT("webinarSessionDetailsUpsert", false, "30"), WEBINAR_SESSION_DETAILS_UPSERT_DL("webinarSessionDetailsUpsertDl", false, "30"),
    WEBINAR_REGISTER_QUEUE("webinarRegister", false, "180"), WEBINAR_REGISTER_QUEUE_DL("webinarRegisterDl", false, "30"),
    AUTO_ENROLLMENT_QUEUE_DL("auto_enrollment_queue_dl", false, "30"), AUTO_ENROLLMENT_QUEUE("auto_enrollment_queue", false, "300", 5000d, 21600d, "5");


    private final String name;
    private final boolean fifo;
    private final String visibilityTimeout;
    private Double visibleThreshold;
    private Double ageThreshold;
    private String maxConcurrency = "3";

    private SQSQueue(String name, boolean fifo) {
        this.name = name;
        this.fifo = fifo;
        this.visibilityTimeout = "300";
        setAlarmThreshold(name);
    }

    private SQSQueue(String name, boolean fifo, String vt) {
        this.name = name;
        this.fifo = fifo;
        if (StringUtils.isEmpty(vt)) {
            this.visibilityTimeout = "300";
        } else {
            this.visibilityTimeout = vt;
        }
        setAlarmThreshold(name);
    }

    SQSQueue(String name, boolean fifo, String vt, Double visibleThreshold, Double ageThreshold) {
        this.name = name;
        this.fifo = fifo;
        if (StringUtils.isEmpty(vt)) {
            this.visibilityTimeout = "300";
        } else {
            this.visibilityTimeout = vt;
        }
        this.visibleThreshold = visibleThreshold;
        this.ageThreshold = ageThreshold;
    }

    SQSQueue(String name, boolean fifo, String vt, Double visibleThreshold, Double ageThreshold, String maxConcurrency) {
        this.name = name;
        this.fifo = fifo;
        if (StringUtils.isEmpty(vt)) {
            this.visibilityTimeout = "300";
        } else {
            this.visibilityTimeout = vt;
        }
        this.visibleThreshold = visibleThreshold;
        this.ageThreshold = ageThreshold;
        this.maxConcurrency = maxConcurrency;
    }

    public String getName() {
        return name;
    }

    public boolean getFifo() {
        return fifo;
    }

    public String getVisibilityTimeout() {
        return visibilityTimeout;
    }

    public Double getVisibleThreshold() {
        return visibleThreshold;
    }

    public Double getAgeThreshold() {
        return ageThreshold;
    }

    public String getMaxConcurrency() {
        return maxConcurrency;
    }

    public String getQueueName(String env) {
        String queueName = this.getName() + "_" + env.toLowerCase();
        if (this.getFifo()) {
            queueName += ".fifo";
        }
        return queueName;
    }

    private void setAlarmThreshold(String name) {

        name = name.toLowerCase();
        if (name.endsWith("dl") || name.endsWith("deadletter")) {
            this.visibleThreshold = 10d;

        } else {
            this.visibleThreshold = 1000d;
        }

        this.ageThreshold = 21600d;
    }

}
