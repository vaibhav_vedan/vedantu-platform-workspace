package com.vedantu.util.enums;

public enum Scope {

	PUBLIC, PRIVATE;
}