package com.vedantu.util.enums;

import org.springframework.util.StringUtils;

public enum RequestSource {

	WEB, MOBILE, AUTO_ADDITION_TRIAL, ANDROID, IOS;

	public static RequestSource identifyDeviceSource(SessionSource sessionSource) {
		if (sessionSource != null) {
			return identifyDeviceSource(sessionSource.name());
		} else {
			return null;
		}
	}

	public static RequestSource identifyDeviceSource(String sessionSourceString) {
		// Expected input is DEVICE_MODEL_SOURCE. ex: MOBILE_TRIAL_PUBLICPROFILE

		// Note: This might fail if there is any overlapping of the names. like
		// CHAT and PROFILE_CHAT. No idea why AUTO_ADDITION_TRIAL is a request
		// source

		RequestSource[] values = RequestSource.values();
		if (!StringUtils.isEmpty(sessionSourceString)) {
			for (RequestSource value : values) {
				if (sessionSourceString.contains(value.name())) {
					return value;
				}
			}
		}
		return null;
	}
}
