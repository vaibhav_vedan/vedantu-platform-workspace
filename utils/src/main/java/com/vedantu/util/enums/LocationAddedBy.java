package com.vedantu.util.enums;

public enum LocationAddedBy {
	
	SYSTEM, USER;

}
