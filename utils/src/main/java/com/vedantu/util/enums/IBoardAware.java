package com.vedantu.util.enums;

import java.util.Set;

public interface IBoardAware {
	public Set<String> _getBoardIdsInString();
	public Set<Long> _getBoardIds();
}
