/**
 * 
 */
package com.vedantu.util.enums;

/**
 * @author subarna
 *
 */
public enum SortByField {
	
	LAST_UPDATED("lastUpdated"), LAST_UPDATED_BY("lastUpdatedBy"), START_TIME("startTime"), END_TIME("endTime"), CREATION_TIME("creationTime"), CREATED_BY("createdBy"),
	SUBJECT("subject"), SUBJECTS("subjects");

	private String name;

	SortByField(String name) {
		this.name= name;
	}

}
