package com.vedantu.util.enums;

public enum ProposalPaymentMode {
	WALLET, TEACHER_SUBSCRIPTION, COURSE_SUBSCRIPTION
}
