package com.vedantu.util.enums;

public interface SNSTopicIFace {

    String name();

    default String getTopicName(String env) {
        String queueName = this.name() + "_" + env.toUpperCase();
        return queueName;
    }
}
