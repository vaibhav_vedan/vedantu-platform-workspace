package com.vedantu.util.enums;

public enum ReferralType {
    SIGNUP("SignUp"), VQUIZ_SIGNUP("ViquizSignUp");

    private String statusString;

    ReferralType(String statusString) {
        this.statusString = statusString;
    }

    public static String getStatusString(ReferralType referralType){
        return referralType.statusString;
    }

}
