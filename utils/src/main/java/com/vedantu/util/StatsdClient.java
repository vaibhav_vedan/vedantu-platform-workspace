/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class StatsdClient {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StatsdClient.class);

    private StatsDClient statsd;
    private String statsdPrefix;
    private final String STATSD_PREFIX_SEPARATOR = ".";

    @PostConstruct
    public void init() {
        statsdPrefix = ConfigUtils.INSTANCE.getStringValue("application.name")
                + STATSD_PREFIX_SEPARATOR + ConfigUtils.INSTANCE.getStringValue("environment");
        statsd = new NonBlockingStatsDClient(statsdPrefix, "stats.vedantu.com", 8125);
    }

    //counting no of times api is called
    public void recordCount(Long i, String key) {
        if (i == null) {
            i = 1l;
        }               
        if (StringUtils.isNotEmpty(key)) {
            if(key.contains("RequestForwarderController.getforwarder") || key.contains("RequestForwarderController.postforwarder")){
                logger.info("Skipping: "+key);
                return;
            }            
            
            logger.info("sending " + key + " : " + i);
            statsd.count(key, i);
        }
    }

    public void recordCount(String... prefixes) {
        String key = createKey(prefixes);
        recordCount(1l, key);
    }

    //execution times 
    public void recordExecutionTime(Long i, String key) {
        if (i != null && StringUtils.isNotEmpty(key)) {
            
            if(key.contains("RequestForwarderController.getforwarder") || key.contains("RequestForwarderController.postforwarder")){
                logger.info("Skipping: "+key);
                return;
            }              

            logger.info("sending " + key + " : " + i);
            statsd.recordExecutionTime(key, i);
        }
    }

    public void recordExecutionTime(Long executionTime, String... prefixes) {
        String key = createKey(prefixes);
        recordExecutionTime(executionTime, key);
    }

    //response size
    public void recordResponseSize(Long i, String key) {
        if (i != null && StringUtils.isNotEmpty(key)) {
            logger.info("sending " + key + " : " + i);
            statsd.recordExecutionTime(key, i);
        }
    }

    public void recordResponseSize(Long responseSize, String... prefixes) {
        String key = createKey(prefixes);
        recordResponseSize(responseSize, key);
    }

    private String createKey(String... prefixes) {
        StringBuilder sb = new StringBuilder();
        for (String prefix : prefixes) {
            if (StringUtils.isNotEmpty(prefix)) {
                sb.append(STATSD_PREFIX_SEPARATOR);
                sb.append(prefix);
            }
        }
        return sb.toString();
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (statsd != null) {
                statsd.stop();
            }
        } catch (Exception e) {
            logger.error("Error in closing statsd connection ", e);
        }
    }
}
