package com.vedantu.util;

import com.vedantu.User.User;

public enum EntityType {

//	NONE(null), USER(User.class), SESSION(Session.class), OFFERING(
//			Offering.class), PLAN(null), OTF(null), SUBSCRIPTION(null), INSTALEARN(null), SUBSCRIPTION_REQUEST(null);
	
	NONE(null), USER(User.class), OFFERING(
			null), PLAN(null),  SUBSCRIPTION(null),
                        INSTALEARN(null), SUBSCRIPTION_REQUEST(null),
                        OTO_COURSE_PLAN(null),OTF_COURSE(null);

	private EntityType(Class<?> clazz) {
		this.clazz = clazz;
	}

	private Class<?> clazz;

	public Class<?> getClazz() {
		return clazz;
	}

}