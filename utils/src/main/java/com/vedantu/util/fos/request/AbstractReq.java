/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.fos.request;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ajith
 */
@EqualsAndHashCode
public abstract class AbstractReq {

    protected List<String> collectVerificationErrors() {
        return new ArrayList<>();
    }

    public void verify() throws BadRequestException {

        List<String> verificationErrors = collectVerificationErrors();
        if (CollectionUtils.isNotEmpty(verificationErrors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, String.join(", ", verificationErrors));
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
