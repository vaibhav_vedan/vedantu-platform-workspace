package com.vedantu.util.fos.request;

import com.vedantu.User.Role;
import com.vedantu.util.enums.RequestSource;
import java.util.Set;

public abstract class AbstractFrontEndListReq extends AbstractFrontEndReq {

    private Integer start;
    private Integer size = 20;//this is size 

    public AbstractFrontEndListReq(Integer start, Integer limit) {
        super();
        this.start = start;
        this.size = limit;
    }

    public AbstractFrontEndListReq(Long callingUserId, Role callingUserRole, Integer start, Integer limit) {
        super(callingUserId, callingUserRole);
        this.start = start;
        this.size = limit;
    }

    public AbstractFrontEndListReq(Long callingUserId, Role callingUserRole, RequestSource requestSource,
            Integer start, Integer limit) {
        super(callingUserId, callingUserRole, requestSource);
        this.start = start;
        this.size = limit;
    }

    public AbstractFrontEndListReq() {
        super();
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
    
}
