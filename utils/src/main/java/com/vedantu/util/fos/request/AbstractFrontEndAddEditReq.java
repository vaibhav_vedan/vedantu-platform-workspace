/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.fos.request;

import java.util.Set;

/**
 *
 * @author ajith
 */
public class AbstractFrontEndAddEditReq extends AbstractFrontEndReq {

    private Set<String> targetGrades;
    private Set<String> topics;

    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

}
