package com.vedantu.util.fos.request;

public class AbstractFrontEndUserReq extends AbstractFrontEndReq {

	private Long userId;

	public Long getUserId() {
		return userId;
	}

	public AbstractFrontEndUserReq() {
		super();
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public AbstractFrontEndUserReq(Long userId) {
		super();
		this.userId = userId;
	}
        
}
