package com.vedantu.util.fos.request;

public abstract class AbstractGetListReq extends AbstractReq {

	private Long start;
	// THIS IS NOT size. ITS end.
	private Long limit;

	public AbstractGetListReq() {

		start = new Long(0);
		limit = new Long(-1);
	}

	public Long getStart() {

		return start;
	}

	public void setStart(long start) {

		this.start = start;
	}

	public Long getLimit() {

		return limit;
	}

	public void setLimit(long limit) {

		this.limit = limit;
	}

}
