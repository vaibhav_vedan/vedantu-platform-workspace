package com.vedantu.util.fos.request;

import com.vedantu.User.Role;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.RequestSource;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
public abstract class AbstractFrontEndReq extends AbstractReq {

    private Long callingUserId;//this will be set by system from jwt token or by if triggered by system will be set as 0, user will not pass this ones
    private Role callingUserRole;//this will be set by system from jwt token, user will not pass this ones
    private RequestSource requestSource;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String ipAddress;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String geoCountry;
    private Set<String> mainTags;
    private String userAgent;
    private String appVersionCode;
    private String appDeviceId;

    public AbstractFrontEndReq() {
        super();
    }

    public AbstractFrontEndReq(Long callingUserId, Role callingUserRole) {
        this(callingUserId, callingUserRole, null, null, null);
    }

    public AbstractFrontEndReq(Long callingUserId, Role callingUserRole, RequestSource requestSource) {
        this(callingUserId, callingUserRole, requestSource, null, null);
    }

    public AbstractFrontEndReq(Long callingUserId, Role callingUserRole, RequestSource requestSource, String ipAddress, String geoCountry) {
        this.callingUserId = callingUserId;
        this.callingUserRole = callingUserRole;
        this.requestSource = requestSource;
        this.ipAddress = ipAddress;
        this.geoCountry = geoCountry;
    }

    public String getIpAddress() {

        return ipAddress;
    }

    public String getGeoCountry() {
        return geoCountry;
    }

    public Long getCallingUserId() {

        return callingUserId;
    }

    public void setCallingUserId(Long callingUserId) {

        this.callingUserId = callingUserId;
    }

    public Role getCallingUserRole() {
        return callingUserRole;
    }

    public void setCallingUserRole(Role callingUserRole) {
        this.callingUserRole = callingUserRole;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setGeoCountry(String geoCountry) {
        this.geoCountry = geoCountry;
    }

    public RequestSource getRequestSource() {
        return requestSource;
    }

    public void setRequestSource(RequestSource requestSource) {
        this.requestSource = requestSource;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(String appVersionCode) {
        this.appVersionCode = appVersionCode;
    }

    public String getAppDeviceId() {
        return appDeviceId;
    }

    public void setAppDeviceId(String appDeviceId) {
        this.appDeviceId = appDeviceId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        return new ArrayList<>();
    }

    //TODO remove this hard coding
    //
    public void removeTargetBoardsFromMainTags() {
        if (ArrayUtils.isNotEmpty(mainTags)) {//&& StringUtils.isNotEmpty(appDeviceId)
            this.mainTags = StringUtils.removeTargetBoardsFromMainTags(mainTags);
        }
    }

    public static class Constants {

        public static final String CALLING_USER_ID = "callingUserId";
        public static final String CALLING_USER_ROLE = "callingUserRole";
        public static final String REQUEST_SOURCE = "requestSource";
        public static final String GEO_COUNTRY = "geoCountry";
        public static final String IP_ADDRESS = "ipAddress";
        public static final String USER_ID = "userId";
        public static final Long CALLING_USER_ID_SYSTEM = 0l;
        public static final String CALLING_USER_ID_SYSTEM_DB = "SYSTEM";
    }

}
