/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.fos.request;

import com.vedantu.util.ArrayUtils;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.datanucleus.util.StringUtils;

/**
 *
 * @author ajith
 */
public class ValidStringListValidator implements ConstraintValidator<ValidStringList, List<String>> {

    @Override
    public void initialize(ValidStringList notEmptyFields) {
    }

    @Override
    public boolean isValid(List<String> objects, ConstraintValidatorContext context) {

//        return objects.stream().allMatch(nef -> !nef.trim().isEmpty());
        boolean valid = true;
        if (ArrayUtils.isNotEmpty(objects)) {
            for (String object : objects) {
                if (StringUtils.isEmpty(object)) {
                    valid = false;
                    break;
                }
                if (object.length() > ReqLimits.NAME_TYPE_MAX) {
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    }

}
