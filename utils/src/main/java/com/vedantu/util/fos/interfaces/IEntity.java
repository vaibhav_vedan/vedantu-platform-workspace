package com.vedantu.util.fos.interfaces;

public interface IEntity {

	public void preStore();

	public void postStore();
}
