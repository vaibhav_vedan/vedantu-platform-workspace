/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.fos.request;

/**
 *
 * @author ajith
 */
public class ReqLimits {

    public static final int NAME_TYPE_MAX = 128;
    public static final int REASON_TYPE_MAX = 1024;
    public static final int COMMENT_TYPE_MAX = 2048;
    public static final int VSTORY_TITLE_MAX = 30;
    public static final int VSTORY_TAG_MAX = 100;

}
