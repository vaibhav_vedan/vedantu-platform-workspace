/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.fos.request;

/**
 *
 * @author ajith
 */
public class ReqLimitsErMsgs {

    public static final String MAX_NAME_TYPE = "max length 128 chars";    
    public static final String MAX_COMMENT_TYPE = "max length 2048 chars";
    public static final String MAX_REASON_TYPE = "max length 1024 chars";
    public static final String RQD = "required";
    public static final String STRING_LIST_MAX = "null values or max length 128 chars";
    public static final String REASON_STRING_LIST_MAX = "null values or max length 1024 chars";
    public static final String VSTORY_TITLE_MAX = "max length 30 chars";
    public static final String VSTORY_TAG_MAX = "max length 100 chars";

}
