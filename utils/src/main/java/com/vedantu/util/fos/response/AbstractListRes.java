package com.vedantu.util.fos.response;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractListRes<T> extends AbstractRes{

	private List<T> list = new ArrayList<>();

	private int count;

	public AbstractListRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AbstractListRes(List<T> list, int count) {
		super();
		this.list = list;
		this.count = count;
	}
        
        
	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
		calculateCount();
	}

	public void addItem(T item) {
		this.list.add(item);
		calculateCount();
	}

	public int getCount() {
		return count;
	}

	public void calculateCount() {
		if (this.list != null) {
			this.count = this.list.size();
		} else {
			this.count = 0;
		}
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("{list=").append(list).append(", count=").append(count).append("}");
		return builder.toString();
	}

}
