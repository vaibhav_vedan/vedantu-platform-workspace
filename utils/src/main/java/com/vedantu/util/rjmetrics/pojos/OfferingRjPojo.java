package com.vedantu.util.rjmetrics.pojos;

import com.vedantu.User.AbstractEntityRes;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.offering.pojo.TeacherPlanInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.enums.OfferingSubType;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.enums.Scope;
import java.util.ArrayList;

public class OfferingRjPojo extends AbstractEntityRes {

    private String title;

    //private OfferingIndex index;

    private List<String> grades;
    private Set<String> boardIds;
    private Text description;

    // who it for field
    private Text targetAudienceText;

    // price per hour basis
    private Long perHrPrice;

    // this will be bulk price
    private Long price;
    private Long displayPrice;
    private String currencyCode;
    private Text recommendedSchedule;
    private List<String> tags;
    private List<String> examTags;
    private Long totalDays;
    private Long totalHours;
    private OfferingType type;
    private List<String> imageUrls;
    private List<String> videoUrls;
    private List<String> contents;
    private List<String> teacherIds;
    private List<String> categories;
    private List<String> parentTopics;
    private VisibilityState state = VisibilityState.VISIBLE;
    private Integer priority = 0;
    private List<String> takeAways;
    private Long createdByUserId;
    private Scope scope = Scope.PUBLIC;
    private Boolean isFeatured;
    private List<OfferingSubType> subtypes;

    private List<TeacherPlanInfo> teacherPlans;

    // e.g Full Year (April 2015 - Jan 2016)
    private String offeringLength;

    public OfferingRjPojo() {
    }


    public OfferingRjPojo(String title, List<String> grades, Set<String> boardIds, Text description, Text targetAudienceText, Long perHrPrice, Long price, Long displayPrice, String currencyCode, Text recommendedSchedule, List<String> tags, List<String> examTags, Long totalDays, Long totalHours, OfferingType type, List<String> imageUrls, List<String> videoUrls, List<String> contents, List<String> teacherIds, List<String> categories, List<String> parentTopics, List<String> takeAways, Long createdByUserId, Boolean isFeatured, List<OfferingSubType> subtypes, List<TeacherPlanInfo> teacherPlans, String offeringLength, Long id, Long creationTime, Long lastUpdated) {
        super(id, creationTime, lastUpdated);
        this.title = title;
        this.grades = grades;
        this.boardIds = boardIds;
        this.description = description;
        this.targetAudienceText = targetAudienceText;
        this.perHrPrice = perHrPrice;
        this.price = price;
        this.displayPrice = displayPrice;
        this.currencyCode = currencyCode;
        this.recommendedSchedule = recommendedSchedule;
        this.tags = tags;
        this.examTags = examTags;
        this.totalDays = totalDays;
        this.totalHours = totalHours;
        this.type = type;
        this.imageUrls = imageUrls;
        this.videoUrls = videoUrls;
        this.contents = contents;
        this.teacherIds = teacherIds;
        this.categories = categories;
        this.parentTopics = parentTopics;
        this.takeAways = takeAways;
        this.createdByUserId = createdByUserId;
        this.isFeatured = isFeatured;
        this.subtypes = subtypes;
        this.teacherPlans = teacherPlans;
        this.offeringLength = offeringLength;
    }
    
    

    public VisibilityState getState() {
        return state;
    }

    public void setState(VisibilityState state) {
        this.state = state;
    }

    public List<String> getTakeAways() {
        return takeAways;
    }

    public void setTakeAways(List<String> takeAways) {
        this.takeAways = takeAways;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getExamTags() {
        return examTags;
    }

    public void setExamTags(List<String> examTags) {
        this.examTags = examTags;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public void setVideoUrls(List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public List<String> getContents() {
        return contents;
    }

    public void setContents(List<String> contents) {
        this.contents = contents;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public Text getDescription() {
        return description;
    }

    public void setDescription(Text description) {
        this.description = description;
    }

    public Text getTargetAudienceText() {
        return targetAudienceText;
    }

    public void setTargetAudienceText(Text targetAudienceText) {
        this.targetAudienceText = targetAudienceText;
    }

    public Long getPerHrPrice() {
        return perHrPrice;
    }

    public void setPerHrPrice(Long perHrPrice) {
        this.perHrPrice = perHrPrice;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(Long displayPrice) {
        this.displayPrice = displayPrice;
    }

    public Text getRecommendedSchedule() {
        return recommendedSchedule;
    }

    public void setRecommendedSchedule(Text recommendedSchedule) {
        this.recommendedSchedule = recommendedSchedule;
    }

    public List<String> getTopics() {
        return tags;
    }

    public void setTopics(List<String> topics) {
        this.tags = topics;
    }

    public Long getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(Long totalDays) {
        this.totalDays = totalDays;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public OfferingType getType() {
        return type;
    }

    public void setType(OfferingType type) {
        this.type = type;
    }

    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrl(List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public Set<String> getBoardIds() {
        return boardIds;
    }

    public void setBoardIds(Set<String> boardIds) {
        this.boardIds = boardIds;
    }

    public List<String> getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(List<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getParentTopics() {
        return parentTopics;
    }

    public void setParentTopics(List<String> parentTopics) {
        this.parentTopics = parentTopics;
    }

    public void setPriority(Integer priority) {
        this.priority = priority == null ? 0 : priority;
    }

    public Integer getPriority() {
        return priority;
    }

    public Long getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(Long createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Boolean getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getOfferingLength() {
        return offeringLength;
    }

    public void setOfferingLength(String offeringLength) {
        this.offeringLength = offeringLength;
    }

    public List<OfferingSubType> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(List<OfferingSubType> subtypes) {
        this.subtypes = subtypes;
    }

    public List<TeacherPlanInfo> getTeacherPlans() {
        return teacherPlans;
    }

    public void setTeacherPlans(List<TeacherPlanInfo> teacherPlans) {
        this.teacherPlans = teacherPlans;
    }

    public Set<Long> getBoardIdsInLong() {
        Set<Long> boardIdsLong = new HashSet<>();
        for (String boardId : this.boardIds) {
            boardIdsLong.add(Long.parseLong(boardId));
        }
        return boardIdsLong;
    }

    public List<Long> getTeacherIdsInLong() {
        List<Long> teacherIdsLong = new ArrayList<>();
        for (String teacherId : this.teacherIds) {
            teacherIdsLong.add(Long.parseLong(teacherId));
        }
        return teacherIdsLong;
    }

    @Override
    public String toString() {
        return "Offering [title=" + title + ", grades=" + grades 
                + ", boardIds=" + boardIds + ", description=" + description + ", targetAudienceText="
                + targetAudienceText + ", perHrPrice=" + perHrPrice + ", price=" + price + ", displayPrice="
                + displayPrice + ", currencyCode=" + currencyCode + ", recommendedSchedule=" + recommendedSchedule
                + ", tags=" + tags + ", examTags=" + examTags + ", totalDays=" + totalDays + ", totalHours="
                + totalHours + ", type=" + type + ", imageUrls=" + imageUrls + ", videoUrls=" + videoUrls
                + ", contents=" + contents + ", teacherIds=" + teacherIds + ", categories=" + categories
                + ", parentTopics=" + parentTopics + ", state=" + state + ", priority=" + priority + ", takeAways="
                + takeAways + ", createdByUserId=" + createdByUserId + ", scope=" + scope + ", isFeatured=" + isFeatured
                + ", subtypes=" + subtypes + ", teacherPlans=" + teacherPlans + ", offeringLength=" + offeringLength
                + "]";
    }

}
