package com.vedantu.util.rjmetrics.pojos;

import com.vedantu.User.*;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 21/02/17.
 */
public class UserRjPojo extends AbstractMongoStringIdEntityBean {
    private Boolean profileEnabled = Boolean.TRUE;
    private String email;
    private String contactNumber;
    private String phoneCode;
    private String firstName;
    private String lastName;
    private String fullName;
    private Gender gender;
    private Role role;
    private Boolean isEmailVerified = Boolean.FALSE;
    private Long profilePicId;
    private String profilePicUrl; // this will be used in case of social signup
    private String profilePicPath;

    private List<String> languagePrefs;
    private StudentInfo studentInfo = new StudentInfo();
    private TeacherInfo teacherInfo;
    private LocationInfo locationInfo;
    private SocialInfo socialInfo;
    private String tncVersion;
    private String socialSource;

    private String utm_source;
    private String utm_medium;
    private String utm_campaign;
    private String utm_term;
    private String utm_content;
    private String channel;
    private Long appId;
    private Boolean isContactNumberVerified;
    private Boolean isContactNumberDND;
    private Boolean isContactNumberWhitelisted;

    private String referralCode;

    // userId of the referrer user
    private String referrerCode;

    // bonus in paisa
    private Integer referrerBonus;

    private Boolean referrerSuccess;
    private List<PhoneNumber> phones = new ArrayList<PhoneNumber>();

    private boolean isNewUser;

    private Boolean parentRegistration;

    private String signUpURL;

    private FeatureSource signUpFeature;

    private String ipAddress;
    private Long otfSubscriptionCount = 0l;
    private Long subscriptionRequestCount = 0l;
    private String deviceId;

    public UserRjPojo() {
    }

    public UserRjPojo(String id, Long creationTime, String createdBy, Long lastUpdated, String callingUserId, Boolean profileEnabled, String email, String contactNumber, String phoneCode, String firstName, String lastName, String fullName, String password, Gender gender, Role role, Boolean isEmailVerified, Long profilePicId, String profilePicUrl, String profilePicPath, List<String> languagePrefs, StudentInfo studentInfo, TeacherInfo teacherInfo, LocationInfo locationInfo, SocialInfo socialInfo, String tncVersion, String socialSource, String utm_source, String utm_medium, String utm_campaign, String utm_term, String utm_content, String channel, Long appId, Boolean isContactNumberVerified, Boolean isContactNumberDND, Boolean isContactNumberWhitelisted, String referralCode, String referrerCode, Integer referrerBonus, Boolean referrerSuccess, List<PhoneNumber> phones, boolean isNewUser, Boolean parentRegistration, String signUpURL, FeatureSource signUpFeature, String ipAddress, Long otfSubscriptionCount, Long subscriptionRequestCount, String deviceId) {
        super(id, creationTime, createdBy, lastUpdated, callingUserId);
        this.profileEnabled = profileEnabled;
        this.email = email;
        this.contactNumber = contactNumber;
        this.phoneCode = phoneCode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
//        this.password = password;
        this.gender = gender;
        this.role = role;
        this.isEmailVerified = isEmailVerified;
        this.profilePicId = profilePicId;
        this.profilePicUrl = profilePicUrl;
        this.profilePicPath = profilePicPath;
        this.languagePrefs = languagePrefs;
        this.studentInfo = studentInfo;
        this.teacherInfo = teacherInfo;
        this.locationInfo = locationInfo;
        this.socialInfo = socialInfo;
        this.tncVersion = tncVersion;
        this.socialSource = socialSource;
        this.utm_source = utm_source;
        this.utm_medium = utm_medium;
        this.utm_campaign = utm_campaign;
        this.utm_term = utm_term;
        this.utm_content = utm_content;
        this.channel = channel;
        this.appId = appId;
        this.isContactNumberVerified = isContactNumberVerified;
        this.isContactNumberDND = isContactNumberDND;
        this.isContactNumberWhitelisted = isContactNumberWhitelisted;
        this.referralCode = referralCode;
        this.referrerCode = referrerCode;
        this.referrerBonus = referrerBonus;
        this.referrerSuccess = referrerSuccess;
        this.phones = phones;
        this.isNewUser = isNewUser;
        this.parentRegistration = parentRegistration;
        this.signUpURL = signUpURL;
        this.signUpFeature = signUpFeature;
        this.ipAddress = ipAddress;
        this.otfSubscriptionCount = otfSubscriptionCount;
        this.subscriptionRequestCount = subscriptionRequestCount;
        this.deviceId = deviceId;
    }

    public Boolean getProfileEnabled() {
        return profileEnabled;
    }

    public void setProfileEnabled(Boolean profileEnabled) {
        this.profileEnabled = profileEnabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean emailVerified) {
        isEmailVerified = emailVerified;
    }

    public Long getProfilePicId() {
        return profilePicId;
    }

    public void setProfilePicId(Long profilePicId) {
        this.profilePicId = profilePicId;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public List<String> getLanguagePrefs() {
        return languagePrefs;
    }

    public void setLanguagePrefs(List<String> languagePrefs) {
        this.languagePrefs = languagePrefs;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public SocialInfo getSocialInfo() {
        return socialInfo;
    }

    public void setSocialInfo(SocialInfo socialInfo) {
        this.socialInfo = socialInfo;
    }

    public String getTncVersion() {
        return tncVersion;
    }

    public void setTncVersion(String tncVersion) {
        this.tncVersion = tncVersion;
    }

    public String getSocialSource() {
        return socialSource;
    }

    public void setSocialSource(String socialSource) {
        this.socialSource = socialSource;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Boolean getIsContactNumberVerified() {
        return isContactNumberVerified;
    }

    public void setIsContactNumberVerified(Boolean contactNumberVerified) {
        isContactNumberVerified = contactNumberVerified;
    }

    public Boolean getIsContactNumberDND() {
        return isContactNumberDND;
    }

    public void setIsContactNumberDND(Boolean contactNumberDND) {
        isContactNumberDND = contactNumberDND;
    }

    public Boolean getIsContactNumberWhitelisted() {
        return isContactNumberWhitelisted;
    }

    public void setIsContactNumberWhitelisted(Boolean contactNumberWhitelisted) {
        isContactNumberWhitelisted = contactNumberWhitelisted;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public Integer getReferrerBonus() {
        return referrerBonus;
    }

    public void setReferrerBonus(Integer referrerBonus) {
        this.referrerBonus = referrerBonus;
    }

    public Boolean getReferrerSuccess() {
        return referrerSuccess;
    }

    public void setReferrerSuccess(Boolean referrerSuccess) {
        this.referrerSuccess = referrerSuccess;
    }

    public List<PhoneNumber> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneNumber> phones) {
        this.phones = phones;
    }

    public boolean isIsNewUser() {
        return isNewUser;
    }

    public void setIsNewUser(boolean isNewUser) {
        this.isNewUser = isNewUser;
    }

    public Boolean getParentRegistration() {
        return parentRegistration;
    }

    public void setParentRegistration(Boolean parentRegistration) {
        this.parentRegistration = parentRegistration;
    }

    public String getSignUpURL() {
        return signUpURL;
    }

    public void setSignUpURL(String signUpURL) {
        this.signUpURL = signUpURL;
    }

    public FeatureSource getSignUpFeature() {
        return signUpFeature;
    }

    public void setSignUpFeature(FeatureSource signUpFeature) {
        this.signUpFeature = signUpFeature;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Long getOtfSubscriptionCount() {
        return otfSubscriptionCount;
    }

    public void setOtfSubscriptionCount(Long otfSubscriptionCount) {
        this.otfSubscriptionCount = otfSubscriptionCount;
    }

    public Long getSubscriptionRequestCount() {
        return subscriptionRequestCount;
    }

    public void setSubscriptionRequestCount(Long subscriptionRequestCount) {
        this.subscriptionRequestCount = subscriptionRequestCount;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }



}
