package com.vedantu.util.rjmetrics.pojos;

import com.vedantu.review.pojo.SessionReviewInfo;
import java.util.List;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class ReviewRjPojo extends AbstractMongoStringIdEntityBean {

    private String userId;
    private String entityId;
    private EntityType entityType;
    private Integer rating;
    private Text review;
    // this is just to make sure that we dont use != query when we want to
    // fetch users who has only given rating, instead we will use
    // isReviewed value (true, false)
    private Boolean isReviewed;
    private VisibilityState state = VisibilityState.VISIBLE;

    private ContextType contextType;
    private String contextId;
    private List<String> reason;
    private Integer priority;
    private SessionReviewInfo reviewExtraInfo;

    public ReviewRjPojo(String userId, String entityId, EntityType entityType,
            Integer rating, Text review, Boolean isReviewed, ContextType contextType,
            String contextId, List<String> reason, Integer priority, SessionReviewInfo reviewExtraInfo,
            String id, Long creationTime, String createdBy, Long lastUpdated, String callingUserId) {
        super(id, creationTime, createdBy, lastUpdated, callingUserId);
        this.userId = userId;
        this.entityId = entityId;
        this.entityType = entityType;
        this.rating = rating;
        this.review = review;
        this.isReviewed = isReviewed;
        this.contextType = contextType;
        this.contextId = contextId;
        this.reason = reason;
        this.priority = priority;
        this.reviewExtraInfo = reviewExtraInfo;
    }

    public ReviewRjPojo() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Text getReview() {
        return review;
    }

    // public String getReviewString() {
    // if (review != null) {
    // return review.getValue();
    // } else {
    // return null;
    // }
    // }
    public void setReview(Text review) {
        this.review = review;
    }

    public Boolean getIsReviewed() {
        return isReviewed;
    }

    public void setIsReviewed(Boolean isReviewed) {
        this.isReviewed = isReviewed;
    }

    public VisibilityState getState() {
        return state;
    }

    public void setState(VisibilityState state) {
        this.state = state;
    }

    public ContextType getContextType() {
        return contextType;
    }

    public void setContextType(ContextType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public List<String> getReason() {
        return reason;
    }

    public String getReasonString() {
        if (reason != null && !reason.isEmpty()) {
            return StringUtils.join(reason.toArray(), ",");
        } else {
            return null;
        }
    }

    public void setReason(List<String> reason) {
        this.reason = reason;
    }

    public SessionReviewInfo getReviewExtraInfo() {
        return reviewExtraInfo;
    }

    public void setReviewExtraInfo(SessionReviewInfo reviewExtraInfo) {
        this.reviewExtraInfo = reviewExtraInfo;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Review [userId=" + userId + ", entityId=" + entityId
                + ", entityType=" + entityType + ", rating=" + rating
                + ", review=" + review + ", isReviewed=" + isReviewed
                + ", state=" + state + ", contextType=" + contextType
                + ", contextId=" + contextId + ", reason=" + reason
                + ", reviewInfo=" + reviewExtraInfo + ", priority=" + priority
                + "]";
    }

    public static class Constants {

        public static final String USER_ID = "userId";
        public static final String REVIEW = "review";
        public static final String RATING = "rating";
        public static final String IS_REVIEWED = "isReviewed";
        public static final String REASON = "reason";
        public static final String CONTEXT_TYPE = "contextType";
        public static final String CONTEXT_ID = "contextId";
        public static final String ENTITY_TYPE = "entityType";
        public static final String ENTITY_ID = "entityId";
        public static final String PRIORITY = "priority";
        public static final String state = "state";

    }
}
