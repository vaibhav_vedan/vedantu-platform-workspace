package com.vedantu.util.rjmetrics.pojos;

import com.vedantu.User.AbstractInfo;
import com.vedantu.User.TeacherContentLinkType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 21/02/17.
 */
public class TeacherContentLinkRjPojo extends AbstractInfo {

    private static final long serialVersionUID = 1L;

    private TeacherContentLinkType type;
    private String url;
    private String title;
    private String embedUrl;
    private String thumbnail;
    private String source;
    private String description;
    private Text data; //contains extra data as json like view count, duration etc

    public TeacherContentLinkRjPojo() {
        super();
    }

    public TeacherContentLinkType getType() {
        return type;
    }

    public void setType(TeacherContentLinkType type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmbedUrl() {
        return embedUrl;
    }

    public void setEmbedUrl(String embedUrl) {
        this.embedUrl = embedUrl;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Text getData() {
        return data;
    }

    public void setData(Text data) {
        this.data = data;
    }

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<String>();
        return errors;
    }

    @Override
    public String toString() {
        return "TeacherContentLink{" + "type=" + type + ", url=" + url + ", title=" + title + ", embedUrl=" + embedUrl + ", thumbnail=" + thumbnail + ", source=" + source + ", description=" + description + ", data=" + data + '}';
    }
}
