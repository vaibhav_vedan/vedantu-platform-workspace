package com.vedantu.util.rjmetrics.pojos;

import com.vedantu.User.FeatureSource;
import com.vedantu.scheduling.pojo.session.GTMGTTSessionDetails;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SessionModel;
import org.dozer.Mapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 21/02/17.
 */
public class SessionRjPojo extends AbstractMongoStringIdEntityBean {

    private String subject;
    private String topic;
    private String title;
    private String description;
    private Long startTime;
    private Long endTime;
    private SessionState state;
    private Long scheduledBy;
    private Long startedAt;
    private Long startedBy;
    private Long endedAt;
    private Long endedBy;
    private String openTokSessionId;
    private String openTokToken;
    private SessionPayoutType type;
    private Integer chargeRate;
    private Integer payRate;
    private List<String> replayLinks;
    private Long subscriptionId;
    private Long offeringId;

    @Mapping("studentIds")
    private Long studentId;
    private Long teacherId;
    private Text remark;
    private Long proposalId;
    private FeatureSource sessionSource;

    @Mapping("model")
    private SessionModel sessionModel;
    private RequestSource deviceSource;
    private List<RescheduleData> rescheduleData=new ArrayList<>();
    private LiveSessionPlatformType liveSessionPlatformType=LiveSessionPlatformType.DEFAULT;
    private GTMGTTSessionDetails liveSessionPlatformDetails;

    private EntityType contextType;
    private String contextId;    

    public SessionRjPojo() {
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public SessionState getState() {
        return state;
    }

    public void setState(SessionState state) {
        this.state = state;
    }

    public Long getScheduledBy() {
        return scheduledBy;
    }

    public void setScheduledBy(Long scheduledBy) {
        this.scheduledBy = scheduledBy;
    }

    public Long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Long startedAt) {
        this.startedAt = startedAt;
    }

    public Long getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(Long startedBy) {
        this.startedBy = startedBy;
    }

    public Long getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Long endedAt) {
        this.endedAt = endedAt;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public String getOpenTokSessionId() {
        return openTokSessionId;
    }

    public void setOpenTokSessionId(String openTokSessionId) {
        this.openTokSessionId = openTokSessionId;
    }

    public String getOpenTokToken() {
        return openTokToken;
    }

    public void setOpenTokToken(String openTokToken) {
        this.openTokToken = openTokToken;
    }

    public SessionPayoutType getType() {
        return type;
    }

    public void setType(SessionPayoutType type) {
        this.type = type;
    }

    public Integer getChargeRate() {
        return chargeRate;
    }

    public void setChargeRate(Integer chargeRate) {
        this.chargeRate = chargeRate;
    }

    public Integer getPayRate() {
        return payRate;
    }

    public void setPayRate(Integer payRate) {
        this.payRate = payRate;
    }

    public List<String> getReplayLinks() {
        return replayLinks;
    }

    public void setReplayLinks(List<String> replayLinks) {
        this.replayLinks = replayLinks;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Text getRemark() {
        return remark;
    }

    public void setRemark(Text remark) {
        this.remark = remark;
    }

    public Long getProposalId() {
        return proposalId;
    }

    public void setProposalId(Long proposalId) {
        this.proposalId = proposalId;
    }

    public FeatureSource getSessionSource() {
        return sessionSource;
    }

    public void setSessionSource(FeatureSource sessionSource) {
        this.sessionSource = sessionSource;
    }

    public SessionModel getSessionModel() {
        return sessionModel;
    }

    public void setSessionModel(SessionModel sessionModel) {
        this.sessionModel = sessionModel;
    }

    public RequestSource getDeviceSource() {
        return deviceSource;
    }

    public void setDeviceSource(RequestSource deviceSource) {
        this.deviceSource = deviceSource;
    }

    public List<RescheduleData> getRescheduleData() {
        return rescheduleData;
    }

    public void setRescheduleData(List<RescheduleData> rescheduleData) {
        this.rescheduleData = rescheduleData;
    }

    public LiveSessionPlatformType getLiveSessionPlatformType() {
        return liveSessionPlatformType;
    }

    public void setLiveSessionPlatformType(LiveSessionPlatformType liveSessionPlatformType) {
        this.liveSessionPlatformType = liveSessionPlatformType;
    }

    public GTMGTTSessionDetails getLiveSessionPlatformDetails() {
        return liveSessionPlatformDetails;
    }

    public void setLiveSessionPlatformDetails(GTMGTTSessionDetails liveSessionPlatformDetails) {
        this.liveSessionPlatformDetails = liveSessionPlatformDetails;
    }

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }
}
