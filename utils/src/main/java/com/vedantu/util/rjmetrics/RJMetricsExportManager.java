/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.rjmetrics;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.vedantu.util.LogFactory;
import java.util.List;

import com.vedantu.util.logger.LoggingMarkers;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class RJMetricsExportManager {

    private Client client;
    private String RJMETRICS_BASE_URL;

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RJMetricsExportManager.class);

    public RJMetricsExportManager() {
        client = Client.create();
        RJMETRICS_BASE_URL = "https://connect.rjmetrics.com/v2/client/{{clientId}}/table/{{table}}/data?apikey={{apiKey}}";
//        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{clientId}}",
//                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.id"));
//        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{apiKey}}",
//                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.apiKey"));
    }

    public int postData(List<Object> entities, String postUrl) {

        logger.info(LoggingMarkers.JSON_MASK, "=====================Posting Data to RJMetrics [url:"
                + postUrl);
        int count = 0;
        logger.info("size of datapush to rj : "+entities.size());
        logger.info(LoggingMarkers.JSON_MASK, "posting to url : " + postUrl);
        WebResource webResource = client.resource(postUrl);
        JsonArray jsonArray = new JsonArray();
        int i = 0;
        for (Object entity : entities) {
            i++;
            RJMetricsRecordExport rjMetricsRecordExport = new RJMetricsRecordExport(entity);
            JsonObject jsonObject = rjMetricsRecordExport.toJson();
            if (i < 100) {
                jsonArray.add(jsonObject);
            } else {
                i = 0;
                sendDataToRJMetrics(webResource, jsonArray);
                jsonArray = new JsonArray();
            }

        }
        if (i < 100) {
            sendDataToRJMetrics(webResource, jsonArray);
        }
        count = entities.size();
        return count;
    }

    private void sendDataToRJMetrics(WebResource webResource,
            JsonArray jsonArray) {       
        logger.info("jsonArray: "+jsonArray.toString());
        if(jsonArray.size() > 0) {
            ClientResponse response = webResource.type("application/json").post(
                    ClientResponse.class, jsonArray.toString());
            logger.info("sendDataToRJMetrics: RJMatrix Response : " + response + response.getEntity(String.class));
        }
    }

//    public void sendEmailData(Map<String, Object> values) {
//        String postUrl = RJMETRICS_BASE_URL.replace("{{table}}", "emails");
//        String jsonString = RJMetricsRecordExport.gson.toJson(values);
//        WebResource webResource = client.resource(postUrl);
//        ClientResponse response = webResource.type("application/json").post(
//                ClientResponse.class, jsonString);
//        logger.info("sendEmailData : RJMatrix Response : " + response);
//    }
}
