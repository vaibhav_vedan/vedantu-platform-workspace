package com.vedantu.util.rjmetrics.pojos;

import com.vedantu.User.AbstractInfo;
import com.vedantu.User.TeacherCurrentStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 21/02/17.
 */
public class TeacherInfoRjPojo extends AbstractInfo {

    private static final long serialVersionUID = 1L;

    // payoutRate in paisa
    private Integer payoutRate;

    private TeacherCurrentStatus currentStatus = TeacherCurrentStatus.ACTIVE;

    // chargerate per session per hour in paisa
    private Integer chargeRate;

    private String experience;
    private String latestEducation;
    private String latestJob;
    private Text subjectExpertise;
    private Text journey;
    private Text philosophy;
    private Text interests;
    private Long primarySubject;
    private Long secondarySubject;
    private String biggestStrength;
    private String professionalCategory;
    private List<String> grades = new ArrayList<String>();
    private List<String> categories = new ArrayList<String>();
    ;
    private Text awards;
    private Integer primaryCallingNumberCode;
    private Integer extensionNumber;
    private Long responseTime;
    private Boolean active;
    private Long sessionHours = 0l;
    private Long sessions = 0l;
    private Long noOfSubscriptions = 0l;
    private Float retainabilityIndex = 100f;
    private Float punctualityIndex = 100f;
    private Float regularityIndex = 100f;
    private Float contentSharingIndex = 0f;
    private Long responseTimeIndex = 0l;
    private Long usersChattedWith = 0l;
    private List<TeacherContentLinkRjPojo> contentLinks = new ArrayList<>();


    public Integer getPayoutRate() {
        return payoutRate;
    }

    public void setPayoutRate(Integer payoutRate) {
        this.payoutRate = payoutRate;
    }

    public TeacherCurrentStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(TeacherCurrentStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Integer getChargeRate() {
        return chargeRate;
    }

    public void setChargeRate(Integer chargeRate) {
        this.chargeRate = chargeRate;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getLatestEducation() {
        return latestEducation;
    }

    public void setLatestEducation(String latestEducation) {
        this.latestEducation = latestEducation;
    }

    public String getLatestJob() {
        return latestJob;
    }

    public void setLatestJob(String latestJob) {
        this.latestJob = latestJob;
    }

    public Text getSubjectExpertise() {
        return subjectExpertise;
    }

    public void setSubjectExpertise(Text subjectExpertise) {
        this.subjectExpertise = subjectExpertise;
    }

    public Text getJourney() {
        return journey;
    }

    public void setJourney(Text journey) {
        this.journey = journey;
    }

    public Text getPhilosophy() {
        return philosophy;
    }

    public void setPhilosophy(Text philosophy) {
        this.philosophy = philosophy;
    }

    public Text getInterests() {
        return interests;
    }

    public void setInterests(Text interests) {
        this.interests = interests;
    }

    public Long getPrimarySubject() {
        return primarySubject;
    }

    public void setPrimarySubject(Long primarySubject) {
        this.primarySubject = primarySubject;
    }

    public Long getSecondarySubject() {
        return secondarySubject;
    }

    public void setSecondarySubject(Long secondarySubject) {
        this.secondarySubject = secondarySubject;
    }

    public String getBiggestStrength() {
        return biggestStrength;
    }

    public void setBiggestStrength(String biggestStrength) {
        this.biggestStrength = biggestStrength;
    }

    public String getProfessionalCategory() {
        return professionalCategory;
    }

    public void setProfessionalCategory(String professionalCategory) {
        this.professionalCategory = professionalCategory;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public Text getAwards() {
        return awards;
    }

    public void setAwards(Text awards) {
        this.awards = awards;
    }

    public Integer getPrimaryCallingNumberCode() {
        return primaryCallingNumberCode;
    }

    public void setPrimaryCallingNumberCode(Integer primaryCallingNumberCode) {
        this.primaryCallingNumberCode = primaryCallingNumberCode;
    }

    public Integer getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(Integer extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getSessionHours() {
        return sessionHours;
    }

    public void setSessionHours(Long sessionHours) {
        this.sessionHours = sessionHours;
    }

    public Long getSessions() {
        return sessions;
    }

    public void setSessions(Long sessions) {
        this.sessions = sessions;
    }

    public Long getNoOfSubscriptions() {
        return noOfSubscriptions;
    }

    public void setNoOfSubscriptions(Long noOfSubscriptions) {
        this.noOfSubscriptions = noOfSubscriptions;
    }

    public Float getRetainabilityIndex() {
        return retainabilityIndex;
    }

    public void setRetainabilityIndex(Float retainabilityIndex) {
        this.retainabilityIndex = retainabilityIndex;
    }

    public Float getPunctualityIndex() {
        return punctualityIndex;
    }

    public void setPunctualityIndex(Float punctualityIndex) {
        this.punctualityIndex = punctualityIndex;
    }

    public Float getRegularityIndex() {
        return regularityIndex;
    }

    public void setRegularityIndex(Float regularityIndex) {
        this.regularityIndex = regularityIndex;
    }

    public Float getContentSharingIndex() {
        return contentSharingIndex;
    }

    public void setContentSharingIndex(Float contentSharingIndex) {
        this.contentSharingIndex = contentSharingIndex;
    }

    public Long getResponseTimeIndex() {
        return responseTimeIndex;
    }

    public void setResponseTimeIndex(Long responseTimeIndex) {
        this.responseTimeIndex = responseTimeIndex;
    }

    public Long getUsersChattedWith() {
        return usersChattedWith;
    }

    public void setUsersChattedWith(Long usersChattedWith) {
        this.usersChattedWith = usersChattedWith;
    }

    public List<TeacherContentLinkRjPojo> getContentLinks() {
        return contentLinks;
    }

    public void setContentLinks(List<TeacherContentLinkRjPojo> contentLinks) {
        this.contentLinks = contentLinks;
    }
}
