/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.rjmetrics;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vedantu.User.User;

/**
 *
 * @author somil
 */
public class RJMetricsRecordExport {
	public static final Gson gson = new Gson();

	private Object entity;

	public RJMetricsRecordExport(Object entity) {
		this.entity = entity;
	}

	public JsonObject toJson() {
		JsonObject jsonObject = gson.toJsonTree(entity).getAsJsonObject();
		if (entity instanceof User) {
			jsonObject.getAsJsonObject().remove("password");
		}
		String[] keyArray = gson.fromJson("['id']", String[].class);
		jsonObject.add("keys", gson.toJsonTree(keyArray));

		return jsonObject;
	}


}

