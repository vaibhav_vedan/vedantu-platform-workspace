package com.vedantu.util.rjmetrics.pojos;

import java.util.List;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class FeedbackRjPojo extends AbstractMongoStringIdEntityBean {

	private String receiverId;
	private String senderId;
	private String sessionId;
	private int rating;
	private Text sessionMessage;
	private Text partnerMessage;
	private List<String> reason;
	// Sent to student only if the feedback given by teacher
	private String optionalMessage;
	private String sessionCoverage;
	private String nextSessionPlan;
	private String homeWork;
	private List<String> studentPerformance;

	public FeedbackRjPojo() {
		super();
	}

    public FeedbackRjPojo(String receiverId, String senderId, String sessionId, int rating, Text sessionMessage, Text partnerMessage, List<String> reason, String optionalMessage, String sessionCoverage, String nextSessionPlan, String homeWork, List<String> studentPerformance, String id, Long creationTime, String createdBy, Long lastUpdated, String callingUserId) {
        super(id, creationTime, createdBy, lastUpdated, callingUserId);
        this.receiverId = receiverId;
        this.senderId = senderId;
        this.sessionId = sessionId;
        this.rating = rating;
        this.sessionMessage = sessionMessage;
        this.partnerMessage = partnerMessage;
        this.reason = reason;
        this.optionalMessage = optionalMessage;
        this.sessionCoverage = sessionCoverage;
        this.nextSessionPlan = nextSessionPlan;
        this.homeWork = homeWork;
        this.studentPerformance = studentPerformance;
    }
        
        
        

	public Text getSessionMessage() {
			return sessionMessage;
	}

	public void setSessionMessage(Text sessionMessage) {

		this.sessionMessage = sessionMessage;
	}

	public Text getPartnerMessage() {
			return partnerMessage;
	}

	public void setPartnerMessage(Text partnerMessage) {

		this.partnerMessage = partnerMessage;
	}

	public String getReceiverId() {

		return receiverId;
	}

	public void setReceiverId(String receiverId) {

		this.receiverId = receiverId;
	}

	public String getSessionId() {

		return sessionId;
	}

	public void setSessionId(String sessionId) {

		this.sessionId = sessionId;
	}

	public String getSenderId() {

		return senderId;
	}

	public void setSenderId(String senderId) {

		this.senderId = senderId;
	}

	public int getRating() {

		return rating;
	}

	public void setRating(int rating) {

		this.rating = rating;
	}

	public List<String> getReason() {
		return reason;
	}

	public void setReason(List<String> reason) {
		this.reason = reason;
	}

	public String getOptionalMessage() {
		return optionalMessage;
	}

	public void setOptionalMessage(String optionalMessage) {
		this.optionalMessage = optionalMessage;
	}

	public String getSessionCoverage() {
		return sessionCoverage;
	}

	public void setSessionCoverage(String sessionCoverage) {
		this.sessionCoverage = sessionCoverage;
	}

	public String getNextSessionPlan() {
		return nextSessionPlan;
	}

	public void setNextSessionPlan(String nextSessionPlan) {
		this.nextSessionPlan = nextSessionPlan;
	}

	public String getHomeWork() {
		return homeWork;
	}

	public void setHomeWork(String homeWork) {
		this.homeWork = homeWork;
	}

	public List<String> getStudentPerformance() {
		return studentPerformance;
	}

	public void setStudentPerformance(List<String> studentPerformance) {
		this.studentPerformance = studentPerformance;
	}

	@Override
	public String toString() {
		return "Feedback [receiverId=" + receiverId + ", senderId=" + senderId
				+ ", sessionId=" + sessionId + ", rating=" + rating
				+ ", sessionMessage=" + sessionMessage + ", partnerMessage="
				+ partnerMessage + ", reason=" + reason + ", optionalMessage="
				+ optionalMessage + ", sessionCoverage=" + sessionCoverage
				+ ", nextSessionPlan=" + nextSessionPlan + ", homeWork="
				+ homeWork + ", studentPerformance=" + studentPerformance + "]";
	}

}
