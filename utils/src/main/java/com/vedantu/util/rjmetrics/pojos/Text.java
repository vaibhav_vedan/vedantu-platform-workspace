/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.rjmetrics.pojos;

import org.dozer.Mapping;

/**
 *
 * @author somil
 */
public class Text {
    String value;

    public Text(String value) {
        this.value = value;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Text{" + "value=" + value + '}';
    }
    
    
}
