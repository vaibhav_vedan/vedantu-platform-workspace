package com.vedantu.util;

import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

import javax.ws.rs.core.MediaType;

import com.vedantu.util.logger.LoggingMarkers;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.MultiPart;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.security.JwtAuthenticatedProfile;

import javax.annotation.PreDestroy;
import javax.ws.rs.core.MultivaluedMap;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Service("WebUtils")
public class WebUtils {

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(WebUtils.class);

    private final Client client;
    private final Client shortclient;
    public static final WebUtils INSTANCE = new WebUtils();
    private static final int HTTP_GET_REQUEST_MAX_LENGTH = 8000;
    private final List<String> partialServiceUrls = new ArrayList<>();

    public WebUtils() {
        this.client = Client.create();
        this.client.setConnectTimeout(20000);
        this.client.setReadTimeout(60000);

        this.shortclient = Client.create();
        this.shortclient.setConnectTimeout(6000);
        this.shortclient.setReadTimeout(6000);
        partialServiceUrls.add("/platform");
        partialServiceUrls.add("/user");
        partialServiceUrls.add("/dinero");
        partialServiceUrls.add("/scheduling");
        partialServiceUrls.add("/subscription");
        partialServiceUrls.add("/notification-centre");
        partialServiceUrls.add("/lms");
        partialServiceUrls.add("/listing");
        partialServiceUrls.add("/vedantudata");

    }

    public Client getClient() {
        return this.client;
    }

    public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query) {
        return doCall(serverUrl, requestType, query, true, false);
    }

    public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query, boolean auth) {
        return doCall(serverUrl, requestType, query, auth, false);
    }

    public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query, boolean auth, boolean useshortclient) {

        // WebResource webResource = getClient().resource(serverUrl);
        logger.info(LoggingMarkers.JSON_MASK,"calling " + serverUrl);
        logger.info("with query " + query);

        serverUrl = handleRequestRejectedException(serverUrl);

        if (HttpMethod.GET.equals(requestType)
                && StringUtils.isNotEmpty(serverUrl) && serverUrl.length() > HTTP_GET_REQUEST_MAX_LENGTH) {
            logger.error(LoggingMarkers.JSON_MASK,"HTTP_GET_REQUEST_MAX_LENGTH exceed for " + serverUrl);
        }

        WebResource.Builder webResource;
        if (useshortclient) {
            webResource = shortclient.resource(serverUrl).getRequestBuilder();
        } else {
            webResource = client.resource(serverUrl).getRequestBuilder();
        }

        handleCorrelationId(webResource);
        handleCallingUserId(webResource);

        if (auth) {
            webResource = webResource.header("X-Ved-Client", "VEDANTU_BACKEND").header("X-Ved-Client-Id", "1F9B7")
                    .header("X-Ved-Client-Secret", "6r605RTxER60IYkA89k2340mxxA2FqR6");
            if (StringUtils.isNotEmpty(getJWTTokenFromProfile())) {
                webResource = webResource.header("X-Ved-Token", getJWTTokenFromProfile());
            }
        }

        return call(webResource, requestType, query);
    }
    public ClientResponse doPostFormEncoded(String serverUrl, MultivaluedMap formData) {
        return doPostFormEncoded(serverUrl,formData,new HashMap<String,String>());
    }

    public ClientResponse doPostFormEncoded(String serverUrl, MultivaluedMap formData, Map<String, String> headers) {
        logger.info(LoggingMarkers.JSON_MASK,"calling " + serverUrl);
        logger.info("with data " + formData);
        WebResource.Builder webResource = client.resource(serverUrl).getRequestBuilder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                webResource = webResource.header(key, value);
            }
        }
        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
        return response;
    }

    public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query, String authHeader) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", authHeader);
        return doCall(serverUrl, requestType, query, headers);
    }

    public ClientResponse doCall(String serverUrl, HttpMethod requestType, String query, Map<String, String> headers) {
        logger.info(LoggingMarkers.JSON_MASK,"calling " + serverUrl);
        logger.info("with query " + query);
        logger.info(LoggingMarkers.JSON_MASK,"with headers " + headers);

        serverUrl = handleRequestRejectedException(serverUrl);

        if (HttpMethod.GET.equals(requestType)
                && StringUtils.isNotEmpty(serverUrl) && serverUrl.length() > HTTP_GET_REQUEST_MAX_LENGTH) {
            logger.error(LoggingMarkers.JSON_MASK,"HTTP_GET_REQUEST_MAX_LENGTH exceed for " + serverUrl);
        }

        WebResource.Builder webResource = client.resource(serverUrl).getRequestBuilder();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                webResource = webResource.header(key, value);
            }
        }
        return call(webResource, requestType, query);
    }

    public ClientResponse doPost(String serverUrl, MultiPart multiPart, boolean auth) {
        WebResource.Builder webResource = client.resource(serverUrl).getRequestBuilder();
        if (auth) {
            webResource = webResource.header("X-Ved-Client", "VEDANTU_BACKEND").header("X-Ved-Client-Id", "1F9B7")
                    .header("X-Ved-Client-Secret", "6r605RTxER60IYkA89k2340mxxA2FqR6");
            if (StringUtils.isNotEmpty(getJWTTokenFromProfile())) {
                webResource = webResource.header("X-Ved-Token", getJWTTokenFromProfile());
            }
        }
        ClientResponse response = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class,
                multiPart);
        return response;
    }

    public ClientResponse call(WebResource.Builder webResource, HttpMethod requestType, String query) {
        ClientResponse response = null;
        switch (requestType) {
            case POST:
                response = webResource.type("application/json").post(ClientResponse.class, query);
                break;
            case PUT:
                response = webResource.type("application/json").put(ClientResponse.class, query);
                break;
            case GET:
                response = webResource.type("application/json").get(ClientResponse.class);
                break;
            case DELETE:
                response = webResource.type("application/json").delete(ClientResponse.class, query);
                break;
            default:
                return null;
        }
        return response;
    }

    public String createQueryStringOfObject(Object object) {
        return createQueryStringOfObject(object, object.getClass());
    }

    public String createQueryStringOfObject(Object object, Class<?> queryClazz) {
        if (queryClazz == null) {
            queryClazz = object.getClass();
        }

        String queryString = "";
        while (queryClazz != null) {
            Field[] fields = queryClazz.getDeclaredFields();
            for (Field field : fields) {
                try {
                    field.setAccessible(true);
                    Object valueObject = field.get(object);
                    if (valueObject == null) {
                        continue;
                    }

                    if (Collection.class.isAssignableFrom(field.getType())) {
                        @SuppressWarnings("unchecked")
                        Collection<Object> collectionValues = (Collection<Object>) field.get(object);
                        for (Object element : collectionValues) {
                            if (element != null) {
                                queryString += "&" + field.getName() + "=" + getUrlEncodedValue(String.valueOf(element));
                            }
                        }
                    } else {
                        if (valueObject != null) {
                            queryString += "&" + field.getName() + "=" + getUrlEncodedValue(String.valueOf(valueObject));
                        }
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    logger.warn("Error in createQueryStringOfObject " + object.toString(), ex);
                }
            }
            queryClazz = queryClazz.getSuperclass();
        }

        return queryString;
    }

    public static String getUrlEncodedValue(String value) {
        String returnValue = value;
        try {
            returnValue = URLEncoder.encode(value, StandardCharsets.UTF_8.name());
        } catch (Exception ex) {
        }
        return returnValue;
    }

    private WebResource.Builder handleCorrelationId(WebResource.Builder webResource) {
        String correlationId = null;
        Object correlationIdObject = ThreadContext.get("correlation-id");
        if (correlationIdObject != null) {
            correlationId = correlationIdObject.toString();
        }

        if (StringUtils.isNotEmpty(correlationId)) {
            webResource = webResource.header("X-Correlation-Id", correlationId);
        }
        return webResource;
    }

    private WebResource.Builder handleCallingUserId(WebResource.Builder webResource) {
        String callingUserId = null;
        Object callingUserIdObject = ThreadContext.get("callingUserId");
        if (callingUserIdObject != null) {
            callingUserId = callingUserIdObject.toString();
        }

        if (StringUtils.isNotEmpty(callingUserId)) {
            webResource = webResource.header("X-Caller-Id", callingUserId);
        }
        return webResource;
    }

    public String getJWTTokenFromProfile() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof JwtAuthenticatedProfile) {
            JwtAuthenticatedProfile jwtAuthenticatedProfile = (JwtAuthenticatedProfile) authentication;
            return jwtAuthenticatedProfile.getJwtToken();
        }
        return null;
    }

    private Map<String, String> getURLShortenAuthHeader() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + Base64.getEncoder().encodeToString(
                String.format("%s:%s", ConfigUtils.INSTANCE.getStringValue("urlshorten.username"), ConfigUtils.INSTANCE.getStringValue("urlshorten.password"))
                        .getBytes()
        ));
        return headers;
    }

    public String shortenUrl(String url) {
        String finalUrl = url;
        if (StringUtils.isNotEmpty(url)) {
            try {
                String urlShortner = "http://vdnt.in/api/shorten";
                JSONObject json = new JSONObject();
                json.put("url", url);
                ClientResponse urlshortresp = WebUtils.INSTANCE.doCall(urlShortner, HttpMethod.POST,
                        json.toString(), getURLShortenAuthHeader());
                VExceptionFactory.INSTANCE.parseAndThrowException(urlshortresp);
                String urlshortrespStr = urlshortresp.getEntity(String.class);
                logger.info("respone of urlshortner " + urlshortrespStr);
                JSONObject responseObj = new JSONObject(urlshortrespStr);
                if (!StringUtils.isEmpty(responseObj.getString("shortUrl"))) {
                    finalUrl = responseObj.getString("shortUrl");
                }
            } catch (ClientHandlerException | UniformInterfaceException | VException | JSONException e) {
                //swallow
                logger.info("error in creating shortened url " + e.getMessage());
            }
        }
        return finalUrl;
    }

    private String handleRequestRejectedException(String serverUrl) {
        for (String partialServiceUrl : partialServiceUrls) {
            String p = partialServiceUrl + "///";
            String p1 = partialServiceUrl + "//";
            if (serverUrl.contains(p)) {
                serverUrl = serverUrl.replace(p, p.substring(0, p.length() - 2));
                logger.info(LoggingMarkers.JSON_MASK,"new serverUrl " + serverUrl);
                return serverUrl;
            } else if (serverUrl.contains(p1)) {
                serverUrl = serverUrl.replace(p1, p1.substring(0, p1.length() - 1));
                logger.info(LoggingMarkers.JSON_MASK,"new serverUrl " + serverUrl);
                return serverUrl;
            }
        }
        return serverUrl;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.destroy();
            }
        } catch (Exception e) {
            logger.error("Error in closing webutils connection ", e);
        }
    }
}
