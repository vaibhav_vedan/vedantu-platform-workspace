package com.vedantu.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import static java.time.temporal.TemporalAdjusters.firstInMonth;

/**
 * Created by ujjawal on 27/6/14.
 */
public class DateTimeUtils {

	public static final int MILLIS_PER_SECOND = 1000;
	public static final int SECONDS_PER_MINUTE = 60;
	public static final int MINUTES_PER_HOUR = 60;
	public static final int HOURS_PER_DAY = 24;
	public static final int HOURS_PER_2DAYS = 48;
	public static final int SECONDS_PER_DAY = 86400;
	public static final int SECONDS_PER_WEEK = 604800;

	public static final int MILLIS_PER_MINUTE = MILLIS_PER_SECOND * SECONDS_PER_MINUTE;
	public static final int MILLIS_PER_HOUR = MILLIS_PER_MINUTE * MINUTES_PER_HOUR;
	public static final int MILLIS_PER_DAY = MILLIS_PER_HOUR * HOURS_PER_DAY;

	public static final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;

	public static final Long MILLIS_PER_WEEK = (long) (7 * MILLIS_PER_DAY);
	public static final Long IST_TIME_DIFFERENCE = 66600000l;
	public static final Long IST_TIME_DIFFERENCE_NEGATIVE = 19800000l;

	public static final String TIME_ZONE_GMT = "GMT";
	public static final String TIME_ZONE_IN = "Asia/Kolkata";

	public static String printableTimeString(int duration) {
		int hr = duration / SECONDS_PER_HOUR;
		int reminder = duration % SECONDS_PER_HOUR;
		int min = reminder / SECONDS_PER_MINUTE;
		int sec = reminder % SECONDS_PER_MINUTE;

		return (hr < 1 ? "" : (getPrintableString(hr) + ":")) + getPrintableString(min) + ":" + getPrintableString(sec);
	}

	/**
	 *
	 * @param duration
	 *            in seconds
	 * @return
	 */
	public static String printableTimeStringHHmmSS(int duration) {
		int hr = duration / SECONDS_PER_HOUR;
		int reminder = duration % SECONDS_PER_HOUR;
		int min = reminder / SECONDS_PER_MINUTE;
		int sec = reminder % SECONDS_PER_MINUTE;

		return (hr < 1 ? "00" : getPrintableString(hr)) + ":" + getPrintableString(min) + ":" + getPrintableString(sec);
	}


	public static String printableTimeStringHHmmSS(Long duration) {
                if(duration!=null){
                    duration=duration/DateTimeUtils.MILLIS_PER_SECOND;
                    return printableTimeStringHHmmSS(duration.intValue());
                }else{
                    return null;
                }
	}


	public static Long getISTMonthStartTime(Long time){

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
		calendar.setTime(new Date(time));
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Long dateTime = calendar.getTime().getTime();
		return dateTime;

	}

        public static Long getISTHourStartTime(Long time){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
		calendar.setTime(new Date(time));
		calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Long dateTime = calendar.getTime().getTime();
		return dateTime;

        }

	public static Long getISTMonthEndTime(Long time){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
		calendar.setTime(new Date(time));
		if(calendar.get(Calendar.MONTH) == Calendar.DECEMBER){
			calendar.roll(Calendar.YEAR, true);
		}

		calendar.roll(Calendar.MONTH, true);
		Long dateTime = calendar.getTime().getTime();
		dateTime = dateTime - 1l;
		return dateTime;
	}

	public static int getISTHourCurrentTime(){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour;
	}

	private static String getPrintableString(int value) {

		return value < 10 ? ("0" + value) : ("" + value);
	}

	public static Date getStartOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static long getGMTDateWithoutTimeInMillis(){
		return getStartOfDay(new Date(System.currentTimeMillis())).getTime();
	}

	public static long getOnlyDateInMillis(long datetime){
		return getStartOfDay(new Date(datetime)).getTime();
	}

	public static Long getISTDayStartTime(Long time) {
		Long startTime = time;
		if (0l != (time % DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE)) {
			Long gmtDiff = time % DateTimeUtils.MILLIS_PER_DAY;
			Long gmtTime = time - gmtDiff;
			startTime = gmtTime + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			if (gmtDiff > DateTimeUtils.IST_TIME_DIFFERENCE) {
				startTime = time - gmtDiff + DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			} else {
				startTime = time - gmtDiff - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			}
		}
		return startTime;
	}

	public static ZonedDateTime getZonedDate(String date, DateTimeFormatter formatter) {
		return date == null || date.trim().isEmpty() ? null : LocalDate.parse(date, formatter).atTime(LocalTime.MIDNIGHT)
				.atZone(ZoneId.systemDefault());
	}

	public static Long getEpochMillis(String date, DateTimeFormatter formatter) {
		return date == null || date.trim().isEmpty() ? null : getZonedDate(date, formatter).toInstant().toEpochMilli();
	}

	public static ZonedDateTime getZonedDate(String date, DateTimeFormatter formatter, ZoneId zoneId) {
		return date == null || date.trim().isEmpty() ? null : LocalDate.parse(date, formatter).atTime(LocalTime.MIDNIGHT)
				.atZone(zoneId);
	}

	public static Long getEpochMillis(String date, DateTimeFormatter formatter, ZoneId zoneId) {
		return date == null || date.trim().isEmpty() ? null : getZonedDate(date, formatter, zoneId).toInstant().toEpochMilli();
	}

	public static ZonedDateTime getZonedDateTimeIst(Long time) {
		return time == null ? null : Instant.ofEpochMilli(time).atZone(ZoneId.of("Asia/Kolkata"));
	}

	public static List<List<Long>> getNumberOfWeeks(int year, int month) {

		List<List<Long>> weekdates = new ArrayList<List<Long>>();
		List<Long> dates;
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Kolkata")));
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
	    dates = new ArrayList<Long>();
	    // first day is first day
	    dates.add(c.getTime().getTime());
	    // now we need to figure out the coming Sunday
	    while(c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
	        c.add(Calendar.DAY_OF_MONTH, 1);
	    }
	    dates.add(c.getTime().getTime());
	    // now first week is done
	    weekdates.add(dates);

	    while (c.get(Calendar.MONTH) == month) {
	        dates = new ArrayList<Long>();
	        dates.add(c.getTime().getTime());
	        c.add(Calendar.DAY_OF_MONTH, 7);
	        // handling the case when adding 7 days might go outside the month
	        if(c.get(Calendar.MONTH) > month) {
	            c.add(Calendar.DAY_OF_MONTH, -7);
	            c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
	            c.set(Calendar.HOUR_OF_DAY, 23);
        	    c.set(Calendar.MINUTE, 59);
        	    c.set(Calendar.SECOND, 59);
        	    c.set(Calendar.MILLISECOND, 999);
        	    dates.add(c.getTime().getTime());
	            weekdates.add(dates);
	            break;
	        }
	        dates.add(c.getTime().getTime());
	        weekdates.add(dates);
	    }
		return weekdates;

	}

	public static Long getStartTimeOfMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Kolkata")));
		calendar.set(Calendar.YEAR,year);
		calendar.set(Calendar.MONTH,month);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Long monthFirstDay = calendar.getTime().getTime();

		return monthFirstDay;
	}

	public static Long getEndTimeOfMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Kolkata")));
		calendar.set(Calendar.YEAR,year);
		calendar.set(Calendar.MONTH,month);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		Long monthLastDay = calendar.getTime().getTime();
		 return monthLastDay;
	}

	public static Long getTimestampForTomorrowMidnight() {

		LocalDate today = LocalDate.now(ZoneId.of("Asia/Kolkata"));
		LocalDateTime todayMidnight = LocalDateTime.of(today, LocalTime.MIDNIGHT);
		LocalDateTime tomorrowMidnight = todayMidnight.plusDays(2);
		Instant instant = tomorrowMidnight.atZone(ZoneId.of("Asia/Kolkata")).toInstant();
		Long timeInMillis = instant.toEpochMilli();
		return timeInMillis;

	}

    public static String getDayOfMonthSuffix(int n) {
        if (n >= 11 && n <= 13) {
            return n + "th";
        }
        switch (n % 10) {
            case 1:  return n + "st";
            case 2:  return n + "nd";
            case 3:  return n + "rd";
            default: return n + "th";
        }
    }

    /**
     * gives the date in format : 1st september, 3 : 35 pm
     * @param startTime
     * @return
     */
    public static String getDateInMMMMForamatWithTime(Long startTime){
        Long timeInMillies = startTime + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timeInMillies);
        int day = date.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpleformat = new SimpleDateFormat("MMMM");
        SimpleDateFormat simpleformat1 = new SimpleDateFormat("hh:mm a");
        String dateWithSuffix = getDayOfMonthSuffix(day);
        String month = simpleformat.format(date.getTime());
        String time = simpleformat1.format(date.getTime());
        return dateWithSuffix + " " + month + ", " + time;

	}

	public static String getDateFromTimeStamp (Long timestamp) {
		LocalDate date = Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDate();
		return date.format(DateTimeFormatter.ofPattern("dd MMM"));
    }

    public static LocalDate getLocalStartOfDayFromTimeStamp(Long timestamp) {
		return Instant.ofEpochMilli(getOnlyDateInMillis(timestamp)).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	/**
	 * returns Weekwise date range for a particular month
	 * to group the data..
	 *
	 * @param startTime
	 * @param istZoneId
	 * @return
	 */
	public static List<String> getWeekWiseDateRange(Long startTime, Long endTime, String istZoneId) {

		LocalDate incomingDate = Instant.ofEpochMilli(startTime)
				.atZone(ZoneId.of(istZoneId))
				.toLocalDate();
		LocalDate firstMondayOfMonth = incomingDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		LocalDate lastDayOfMonth = Instant.ofEpochMilli(endTime)
				.atZone(ZoneId.of(istZoneId))
				.toLocalDate();
		System.out.println(incomingDate + " " + firstMondayOfMonth + " " + lastDayOfMonth);

		List<String> weekWiseTimestamps = new LinkedList<>();
		if (incomingDate.isBefore(firstMondayOfMonth) || incomingDate.isEqual(firstMondayOfMonth)) {
			Long start = incomingDate.atTime(0, 0, 0, 0).atZone(ZoneId.of(istZoneId)).toInstant().toEpochMilli();
			Long end = firstMondayOfMonth.minusDays(1).atTime(23, 59, 59, 999).atZone(ZoneId.of(istZoneId)).toInstant().toEpochMilli();
			weekWiseTimestamps.add(start + "-" + end);
		}

		while (firstMondayOfMonth.isBefore(lastDayOfMonth)) {
			Long start = Timestamp.valueOf(firstMondayOfMonth.atStartOfDay()).getTime();
			firstMondayOfMonth = firstMondayOfMonth.plusDays(7);
			if (firstMondayOfMonth.isAfter(lastDayOfMonth)) {
				firstMondayOfMonth = firstMondayOfMonth.minusDays(7);
				break;
			}
			Long end = firstMondayOfMonth.minusDays(1).atTime(23, 59, 59, 999).atZone(ZoneId.of(istZoneId)).toInstant().toEpochMilli();
			weekWiseTimestamps.add(start + "-" + end);
		}

		if (firstMondayOfMonth.isBefore(lastDayOfMonth) || firstMondayOfMonth.isEqual(lastDayOfMonth)) {
			Long start = Timestamp.valueOf(firstMondayOfMonth.atStartOfDay()).getTime();
			Long end = lastDayOfMonth.atTime(23, 59, 59, 999).atZone(ZoneId.of(istZoneId)).toInstant().toEpochMilli();
			weekWiseTimestamps.add(start + "-" + end);
		}
		return weekWiseTimestamps;
	}

}
