package com.vedantu.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.vedantu.User.Role;
import com.vedantu.User.User;

@Service("PlatformTools")
public class PlatformTools {
	public static String PLATFORM_URL =  ConfigUtils.INSTANCE.getStringValue("platform.fetch.url");

	private Client client;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PlatformTools.class);

	private PlatformTools() {
		super();
		SSLContext ctx;
		ClientConfig config = new DefaultClientConfig();

		try {

			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
						throws CertificateException {

				}
			} };
			ctx = SSLContext.getInstance("SSL");
			ctx.init(null, trustAllCerts, null);
			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, ctx));
			client = Client.create(config);
		} catch (NoSuchAlgorithmException | KeyManagementException e1) {
			logger.error(e1);
		}

		
	}

	public String addURLParamter(String url, String parameter, String value) {
		if (!url.contains("?")) {
			url = url + "?";
		} else {
			url = url + "&";
		}
		return url + parameter + "=" + value;
	}


	public PlatformBasicResponse postPlatformData(String location, String body, boolean isJsonReq,
			HttpMethod httpMethod) {
		logger.info(new Object[] { location, body, isJsonReq, httpMethod });
		PlatformBasicResponse basicResponse = new PlatformBasicResponse();
//		WebResource webResource = client.resource(location);
                WebResource.Builder webResource = client.resource(location).getRequestBuilder();
                webResource = webResource
                    .header("X-Ved-Client", "VEDANTU_BACKEND")
                    .header("X-Ved-Client-Id", "1F9B7")
                    .header("X-Ved-Client-Secret", "6r605RTxER60IYkA89k2340mxxA2FqR6");
                
		client.setReadTimeout(60000);

		ClientResponse response = null;
		switch (httpMethod) {
		case POST:
			response = webResource.type("application/json").post(ClientResponse.class, body);
			break;
		case PUT:
			response = webResource.type("application/json").put(ClientResponse.class, body);
			break;
		case GET:
			response = webResource.get(ClientResponse.class);
			break;
		case DELETE:
			response = webResource.type("application/json").delete(ClientResponse.class, body);
			break;
		default:
			break;
		}

		logger.info("respone Platform is "+response.toString());

		String output = response.getEntity(String.class);
		logger.info(output);
		if (response.getStatus() != 200 && response.getStatus() != 201) {
			logger.error(response.getEntity(String.class));
			basicResponse.setErrorString(output);
			basicResponse.setSuccess(false);
			// throw new RuntimeException(output);
		} else {
			basicResponse.setResponse(output);
		}
		return basicResponse;
	}

	public static String getSessionUserId(HttpServletRequest req) {
		try {
			return (String) req.getSession().getAttribute("userId");
		} catch (Exception ex) {
			return null;
		}
	}

	public static Role getSessionUserRole(HttpServletRequest req) {
		try {
			String role = (String) req.getSession().getAttribute(User.Constants.ROLE);
			return Role.valueOf(role);
		} catch (Exception ex) {
			return null;
		}
	}

	public static String getBody(HttpServletRequest request) throws IOException {

		String body = null;
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;

		try {
			InputStream inputStream = request.getInputStream();
			if (inputStream != null) {
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				char[] charBuffer = new char[128];
				int bytesRead = -1;
				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ex) {
					throw ex;
				}
			}
		}

		body = stringBuilder.toString();
		return body;
	}
}
