package com.vedantu.util;

public class TimeInterval {
	public long startTime;
	public long endTime;

	
	public TimeInterval() {
		super();
	}

	public TimeInterval(long startTime, long endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "TimeInterval [startTime=" + startTime + ", endTime=" + endTime + ", toString()=" + super.toString()
				+ "]";
	}
}
