package com.vedantu.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.vedantu.util.logger.LoggingMarkers;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.http.HttpMethod;


import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@Service
public class WebCommunicator{

	private static final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private static LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private static Logger logger = logFactory.getLogger(WebCommunicator.class);

	public static String loadPostWebData(String url, List<NameValuePair> reqParams) {
		return loadPostWebData(url, reqParams, null);
	}

	public static String jsonFetchWithUrlFetchService(String location, String reqParamsJson, HttpMethod httpMethod, Integer timeout) {
		return fetchWithUrlFetchService(location,
				Arrays.asList(new NameValuePair[] { (NameValuePair) new BasicNameValuePair("json", reqParamsJson) }),
				httpMethod, true, timeout);
	}

	public static String fetchWithUrlFetchService(String location, List<NameValuePair> reqParams,
			HttpMethod httpMethod) {
		return fetchWithUrlFetchService(location, reqParams, httpMethod, false, null);
	}

	public static String fetchWithUrlFetchService(String location, List<NameValuePair> reqParams, HttpMethod httpMethod,
			boolean isJsonReq, Integer timeout) {
		try {
			String reqBodyString = null;
			if (reqParams != null) {
				reqBodyString = isJsonReq ? reqParams.get(0).getValue() : getQuery(reqParams);
			}
			return fetchWithUrlFetchService(location, reqBodyString, httpMethod, isJsonReq, timeout);
		} catch (Exception ex) {
			logger.error("WebCommunicator", "loadPostWebData", ex);
			return null;
		}
	}

	public static String fetchWithUrlFetchService(String location, String reqBodyString, HttpMethod httpMethod,
			boolean isJsonReq, Integer timeout) {
		try {
			logger.info(LoggingMarkers.JSON_MASK, "url: " + location);
			URL url = new URL(location);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
                        
                        if(timeout != null){
                            connection.setConnectTimeout(timeout);
                            connection.setReadTimeout(timeout);
                        }
                        
			if (isJsonReq) {
				connection.setRequestProperty("Content-Type", "application/json");
			}
			if (httpMethod.equals(HttpMethod.POST)) {
				connection.setRequestMethod(HttpMethod.POST.name());
			}

			if (StringUtils.isNotEmpty(reqBodyString)) {
				OutputStream os = connection.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				logger.info("reqBodyString: " + reqBodyString);
				writer.write(reqBodyString);
				writer.flush();
				writer.close();
				os.close();
			}
			connection.connect();
			StringBuilder sb = new StringBuilder();
                        // Thread.sleep(10000);

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				InputStreamReader in = null;
				try {
					// OK
					in = new InputStreamReader((InputStream) connection.getContent());
					BufferedReader buff = new BufferedReader(in);

					String line = null;
					while ((line = buff.readLine()) != null) {
						sb.append(line);
					}

				} catch (Throwable t) {
					logger.error("GoogleManager", "fetchWithUrlFetchService", t);
				} finally {
					if (in != null) {
						in.close();
					}
				}
				logger.info("response: " + sb.toString());
				return sb.toString();
			} else {
				logger.warn("code:" + connection.getResponseCode() + ", message:" + connection.getResponseMessage());
			}

		} catch (Throwable t) {
			logger.error("GoogleManager", "fetchWithUrlFetchService", t);
		}
		return null;
	}

	private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;

		for (NameValuePair pair : params) {
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
		}

		return result.toString();
	}

	public static String loadPostWebData(String url, List<NameValuePair> reqParams, Map<String, String> headers) {

		HttpPost httpPost = new HttpPost(url);
		try {
			httpPost.setHeader("content-type", "application/x-www-form-urlencoded");
			if (headers != null && !headers.isEmpty()) {
				for (Entry<String, String> e : headers.entrySet()) {
					httpPost.setHeader(e.getKey(), e.getValue());
				}
			}
			httpPost.setEntity(new UrlEncodedFormEntity(reqParams, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error("WebCommunicator", "loadPostWebData", e);
		}
		return getWebData(httpPost);
	}

	public static String loadGetWebData(String url) {

		HttpGet httpGet = new HttpGet(url);

		return getWebData(httpGet);
	}

	private static String getWebData(HttpRequestBase httpRequestBase) {

		httpRequestBase.addHeader("User-Agent", USER_AGENT);
		@SuppressWarnings("resource")
		HttpClient httpClient = new DefaultHttpClient();
		logger.info(
				"loading info for url : " + httpRequestBase.getURI() + ", headers: " + httpRequestBase.getAllHeaders());
		try {

			HttpResponse httpResopnse = httpClient.execute(httpRequestBase);

			if (httpResopnse != null) {

				HttpEntity entity = httpResopnse.getEntity();
				String resString = EntityUtils.toString(entity);

				logger.info("url: " + httpRequestBase.getURI() + ", response : " + resString);
				EntityUtils.consume(entity);
				if (httpResopnse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					logger.warn("httpstatus: " + httpResopnse.getStatusLine().getStatusCode() + ", reason: "
							+ httpResopnse.getStatusLine().getReasonPhrase());
				}

				return resString;
			}

		} catch (Throwable e) {
			logger.error("WebCommunicator", "getWebData", e);
			httpRequestBase.abort();
		} finally {

			httpClient.getConnectionManager().shutdown();
		}

		return null;
	}

	public static InputStream downloadFile(String location) {

		InputStream in = null;
		try {
			URL url = new URL(location);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.connect();
			// file = connection.getContent();
			in = new BufferedInputStream(url.openStream());

		} catch (IOException e) {
			logger.error("WebCommunicator", "downloadFile", e);
		}
		return in;
	}

	public static String getShortUrl(String url) {
		String shortURL = url;
		String json = "{\"longUrl\":\"" + url + "\"}";
		List<NameValuePair> reqParams = new ArrayList<NameValuePair>();
		reqParams.add(new BasicNameValuePair("key", json));
		String apiKey = ConfigUtils.INSTANCE.getStringValue("google.url.shortner.api.key");
		if (apiKey != null && !apiKey.isEmpty()) {
			String response = fetchWithUrlFetchService("https://www.googleapis.com/urlshortener/v1/url?key=" + apiKey,
					reqParams, HttpMethod.POST, true, null);
			if (response != null) {
				JsonParser jsonParser = new JsonParser();
				JsonElement element = jsonParser.parse(response);

				shortURL = element.getAsJsonObject().get("id").getAsString();
				logger.info("Short URL response:" + response);
			}
		}
		return shortURL;
	}

//	@Override
//	protected String getLogTag() {
//		return "WebCommunicatorsss";
//	}
}
