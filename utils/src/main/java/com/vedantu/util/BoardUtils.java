package com.vedantu.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.board.pojo.Board;
import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.util.enums.IBoardAware;

@Service
public class BoardUtils {

	@Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(BoardUtils.class);
    
    public BoardUtils() {
    }

    public List<BoardTreeInfo> toBoardTree(IBoardAware mapping,
            Map<Long, Board> boardInfoMap) {
        List<BoardTreeInfo> boardTree = new ArrayList<>();
        if (mapping._getBoardIds() == null || boardInfoMap == null) {
            return boardTree;
        }

        Map<Long, List<BoardTreeInfo>> parentChildMap = new HashMap<>();
        for (String boardId : mapping._getBoardIdsInString()) {
            if (boardId == null) {
                continue;
            }
            Board board = boardInfoMap.get(Long.parseLong(boardId));
            if(board == null){
            	logger.warn("Board is null. " + boardId);
            	continue;
            }
            if (board.getParentId() == null
                    || board.getParentId() == Board.DEFAULT_PARENT_ID) {
                boardTree.add(new BoardTreeInfo(board));
                continue;
            }

            if (parentChildMap.get(board.getParentId()) == null) {
                parentChildMap.put(board.getParentId(),
                        new ArrayList<>());
            }
            parentChildMap.get(board.getParentId()).add(
                    new BoardTreeInfo(boardInfoMap.get(Long.parseLong(boardId))));
        }

        for (BoardTreeInfo bTree : boardTree) {
            List<BoardTreeInfo> boardInfoTree = parentChildMap.get(bTree
                    .getId());
            if (boardInfoTree != null) {
                Collections.sort(boardInfoTree, new StringValueComparator());
            }
            bTree.setChildren(boardInfoTree);
        }
        Collections.sort(boardTree, new StringValueComparator());
        return boardTree;
    }
}
