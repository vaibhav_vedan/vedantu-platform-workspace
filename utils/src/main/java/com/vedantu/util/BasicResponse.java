package com.vedantu.util;

import com.vedantu.util.enums.ResponseCode;

public class BasicResponse {

	private ResponseCode responseCode = ResponseCode.SUCCESS;

	public BasicResponse() {
		super();
	}

	public BasicResponse(ResponseCode responseCode) {
		super();
		this.responseCode = responseCode;
	}

	public ResponseCode getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(ResponseCode responseCode) {
		this.responseCode = responseCode;
	}

	@Override
	public String toString() {
		return "AbstractResponse [responseCode=" + responseCode + "]";
	}
}
