/*
 * Copyright (c) 2014.
 * Vedantu
 */

package com.vedantu.util;

public class MoneyUtils {

	public static final int PAISE_PER_RUPEE = 100;

	public static Integer toRupees(Integer paise) {

		if (null == paise || paise < 0) {
			return 0;
		}
		return paise / PAISE_PER_RUPEE;
	}

}
