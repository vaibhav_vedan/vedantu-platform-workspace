/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.pojo;

import com.vedantu.util.enums.DoubtFeedType;

/**
 *
 * @author parashar
 */
public class DoubtFeedPojo {
    private String thumbnailUrl;
    private DoubtFeedType cardSubText;
    private String cardTitle;
    private Long time;
    private String entityId;
    private String webLink;
    private String title;
    private String tagInfo;
    private String seoUrl;
    private String level;
    private boolean opened;
    private String doubtState;

    
    public DoubtFeedPojo(){
        
    }

    public DoubtFeedPojo(String thumbnailUrl, DoubtFeedType cardSubText, String cardTitle, Long time) {
        this.thumbnailUrl = thumbnailUrl;
        this.cardSubText = cardSubText;
        this.cardTitle = cardTitle;
        this.time = time;
    }
    
    /**
     * @return the thumbnailUrl
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * @param thumbnailUrl the thumbnailUrl to set
     */
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    /**
     * @return the cardSubText
     */
    public DoubtFeedType getCardSubText() {
        return cardSubText;
    }

    /**
     * @param cardSubText the cardSubText to set
     */
    public void setCardSubText(DoubtFeedType cardSubText) {
        this.cardSubText = cardSubText;
    }

    /**
     * @return the cardTitle
     */
    public String getCardTitle() {
        return cardTitle;
    }

    /**
     * @param cardTitle the cardTitle to set
     */
    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    /**
     * @return the time
     */
    public Long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTagInfo() {
        return tagInfo;
    }

    public void setTagInfo(String tagInfo) {
        this.tagInfo = tagInfo;
    }

    public String getSeoUrl() {
        return seoUrl;
    }

    public void setSeoUrl(String seoUrl) {
        this.seoUrl = seoUrl;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public String getDoubtState() {
        return doubtState;
    }

    public void setDoubtState(String doubtState) {
        this.doubtState = doubtState;
    }

}
