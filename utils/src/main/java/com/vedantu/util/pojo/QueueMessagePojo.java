/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.pojo;

import com.vedantu.util.enums.SQSMessageType;

/**
 *
 * @author pranavm
 */
public class QueueMessagePojo {

    private SQSMessageType messageType;
    private String body;

    public QueueMessagePojo(SQSMessageType messageType, String body) {
        this.messageType = messageType;
        this.body = body;
    }

    public QueueMessagePojo() {

    }

    public SQSMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(SQSMessageType messageType) {
        this.messageType = messageType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "QueueMessagePojo{" + "messageType=" + messageType + ", body=" + body + '}';
    }

}
