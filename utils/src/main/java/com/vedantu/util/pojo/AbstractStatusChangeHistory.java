package com.vedantu.util.pojo;

import lombok.Data;

@Data
public abstract class AbstractStatusChangeHistory {
    protected Long updatedBy;
    protected Long timestamp;

}