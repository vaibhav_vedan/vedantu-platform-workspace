package com.vedantu.util.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CubeAutoCallRecorder {
    private String duration;
    @SerializedName("loc")
    private String location;
    @SerializedName("callee")
    private String leadPhoneNo;
    @SerializedName("addr")
    private String address;
    private String direction;
    private String sharedLink;
    private String jsonFileName;
    private String audioFileName;
    private String created;
    private Date serverModifiedDate;
    private boolean pushedToLeadSquared = false;

    public CubeAutoCallRecorder(String sharedLink, String audioFileName, Date serverModifiedDate) {
        this.sharedLink = sharedLink;
        this.audioFileName = audioFileName;
        this.created = serverModifiedDate.toString();
        this.serverModifiedDate = serverModifiedDate;
    }

    public CubeAutoCallRecorder merge(CubeAutoCallRecorder other) {
        if (StringUtils.isNotEmpty(other.duration)) {
            this.duration = other.duration;
        }
        if (StringUtils.isNotEmpty(other.location)) {
            this.location = other.location;
        }
        if (StringUtils.isNotEmpty(other.leadPhoneNo)) {
            this.leadPhoneNo = other.leadPhoneNo;
        }
        if (StringUtils.isNotEmpty(other.address)) {
            this.address = other.address;
        }
        if (StringUtils.isNotEmpty(other.direction)) {
            this.direction = other.direction;
        }
        if (StringUtils.isNotEmpty(other.sharedLink)) {
            this.sharedLink = other.sharedLink;
        }
        if (StringUtils.isNotEmpty(other.jsonFileName)) {
            this.jsonFileName = other.jsonFileName;
        }
        if (StringUtils.isNotEmpty(other.audioFileName)) {
            this.audioFileName = other.audioFileName;
        }
        if (StringUtils.isNotEmpty(other.created)) {
            this.created = other.created;
        }
        return this;
    }
}
