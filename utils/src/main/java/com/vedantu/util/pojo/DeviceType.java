/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.pojo;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public enum DeviceType {

    MOBILE, DESKTOP, TABLET, NO_DEVICE_AVAILABLE;

    public static DeviceType valueOfKey(String key) {
        DeviceType deviceType = null;
        try {
            if (StringUtils.equalsIgnoreCase(key, "Phone") || StringUtils.equalsIgnoreCase(key, "Mobile")) {
                deviceType = MOBILE;
            } else {
                deviceType = valueOf(key.trim().toUpperCase());
            }
        } catch (Exception e) {
        }
        return deviceType;
    }
}
