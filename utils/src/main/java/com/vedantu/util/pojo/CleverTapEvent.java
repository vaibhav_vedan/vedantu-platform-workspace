/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.pojo;

import com.vedantu.util.fos.response.AbstractRes;
import java.util.Map;

/**
 *
 * @author jeet
 */
public class CleverTapEvent extends AbstractRes{
    private Long ts;
    private String type = "event";
    private String evtName;
    private Map<String, Object> evtData;
    private Map<String, Object> profileData;
    private String identity;

    public CleverTapEvent(String identity, String evtName) {
        this.identity = identity;
        this.evtName = evtName;
    }

    public CleverTapEvent() {
    }
    
    public CleverTapEvent(String identity, String evtName, Map<String, Object> evtData) {
        this.identity = identity;
        this.evtName = evtName;
        this.evtData = evtData;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEvtName() {
        return evtName;
    }

    public void setEvtName(String evtName) {
        this.evtName = evtName;
    }

    public Map<String, Object> getEvtData() {
        return evtData;
    }

    public void setEvtData(Map<String, Object> evtData) {
        this.evtData = evtData;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public Map<String, Object> getProfileData() {
        return profileData;
    }

    public void setProfileData(Map<String, Object> profileData) {
        this.profileData = profileData;
    }
}
