package com.vedantu.util.pojo;

import com.vedantu.util.security.HttpSessionUtils;
import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class StatusChangeHistory extends AbstractStatusChangeHistory {

    private String prevStatus;
    private String newStatus;

    public static class Builder{

        private StatusChangeHistory stateChange;

        public  Builder() {

            this.stateChange = new StatusChangeHistory();
        }

        public Builder prevState(String prevStatus){
            this.stateChange.prevStatus = prevStatus;
            return this;
        }

        public Builder newStatus(String newStatus){
            this.stateChange.newStatus = newStatus;
            return this;
        }

        public Builder updatedBy(Long updatedBy){
            this.stateChange.updatedBy = updatedBy;
            return this;
        }

        public Builder setTimestamp(Long timestamp){
            this.stateChange.timestamp = timestamp;
            return this;
        }

        public StatusChangeHistory build(){

            return this.stateChange;
        }
    }

    @Override
    public String toString() {
        return "StatusChangeHistory{" +
                "prevStatus='" + prevStatus + '\'' +
                ", newStatus='" + newStatus + '\'' +
                ", updatedBy=" + updatedBy +
                ", timestamp=" + timestamp +
                '}';
    }
}
