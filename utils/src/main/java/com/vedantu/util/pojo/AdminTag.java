package com.vedantu.util.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;
import java.util.TreeSet;

@Data
@EqualsAndHashCode(exclude = { "tags", "addedBy", "modifiedBy", "lastModified" })
public class AdminTag {
    private String entityId;
    private String contextId;
    private String contextType;
    private Set<String> tags;
    private Long addedBy;
    private Long modifiedBy;
    private long lastModified;

    public Set<String> getTags() {
        return tags != null ? tags : new TreeSet<>(String::compareToIgnoreCase);
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }
}
