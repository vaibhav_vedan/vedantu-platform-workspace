package com.vedantu.util.pojo;

import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class CircularQueue<T> implements Iterable<T> {

    private ArrayDeque<T> queue;
    private int threshold = 20;

    public CircularQueue() {
        this.queue = new ArrayDeque<>();
    }

    public CircularQueue(int threshold) {
        Assert.isTrue(threshold >= 1, "threshold should be greater than 1");
        this.threshold = threshold;
        this.queue = new ArrayDeque<>();
    }

    public void add(T t) {
        while (this.threshold <= queue.size()) {
            queue.removeFirst();
        }
        queue.addLast(t);
    }

    public int size() {
        return queue.size();
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return queue.iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        queue.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return queue.spliterator();
    }
}
