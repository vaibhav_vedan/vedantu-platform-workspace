/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.pojo;

/**
 *
 * @author ajith
 */
public class DeviceDetails {

    private DeviceType deviceType;
    private String deviceName;
    private String deviceBrand;
    private String browserName;
    private String browserVersion;
    private String browserVersionMajor;

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getBrowserVersionMajor() {
        return browserVersionMajor;
    }

    public void setBrowserVersionMajor(String browserVersionMajor) {
        this.browserVersionMajor = browserVersionMajor;
    }

    @Override
    public String toString() {
        return "DeviceDetails{" + "deviceType=" + deviceType + ", deviceName=" + deviceName + ", deviceBrand=" + deviceBrand + ", browserName=" + browserName + ", browserVersion=" + browserVersion + ", browserVersionMajor=" + browserVersionMajor + '}';
    }

}
