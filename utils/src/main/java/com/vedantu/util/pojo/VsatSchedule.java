package com.vedantu.util.pojo;

/**
 *
 * @author mnpk
 */

public class VsatSchedule {

    private Long startTime;
    private Long maxStartTime;
    private Long duration;

    public VsatSchedule() {}

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "VsatSchedule { "
                + "startTime = " + startTime
                + ", maxStartTime= "+ maxStartTime
                + ", duration= "+ duration
                + " }";
    }
}
