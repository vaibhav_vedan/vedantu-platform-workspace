/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.pojo;

import com.vedantu.lms.cmds.enums.ImageQuality;
import com.vedantu.lms.cmds.enums.StorageType;
import com.vedantu.util.StringUtils;

/**
 *
 * @author ajith
 */
public class CloudStorageEntity {

    private String fileName;//only key can also contain folder str but not <env>/<uploadtarget>
    private String publicUrl;
    private String displayUrl;
    private String bucketName;
    private Long publicUrlExpiryTime;
    private StorageType storageType = StorageType.AMAZON_S3;
    private Float aspectRatio;
    private CloudStorageEntityType type;
    private ImageQuality quality;

    public String getPublicUrl() {
        return publicUrl;
    }

    public void setPublicUrl(String publicUrl) {
        this.publicUrl = publicUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileNameWithoutExtension() {
        if (StringUtils.isNotEmpty(fileName) && fileName.contains(".")) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        }
        return fileName;
    }

    public Long getPublicUrlExpiryTime() {
        return publicUrlExpiryTime;
    }

    public void setPublicUrlExpiryTime(Long publicUrlExpiryTime) {
        this.publicUrlExpiryTime = publicUrlExpiryTime;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public Float getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(Float aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public CloudStorageEntityType getType() {
        return type;
    }

    public void setType(CloudStorageEntityType type) {
        this.type = type;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public ImageQuality getQuality() {
        return quality;
    }

    public void setQuality(ImageQuality quality) {
        this.quality = quality;
    }

    @Override
    public String toString() {
        return "CloudStorageEntity{" + "fileName=" + fileName + ", publicUrl=" + publicUrl + ", displayUrl=" + displayUrl + ", bucketName=" + bucketName + ", publicUrlExpiryTime=" + publicUrlExpiryTime + ", storageType=" + storageType + ", aspectRatio=" + aspectRatio + ", type=" + type + ", quality=" + quality + '}';
    }

}
