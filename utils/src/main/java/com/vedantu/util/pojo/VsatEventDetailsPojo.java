package com.vedantu.util.pojo;

import java.util.List;


/**
 *
 * @author mnpk
 */

public class VsatEventDetailsPojo {
    private String eventName;
    private List<String> categoryNames;
    private List<VsatSchedule> vsatSchedules;
    private Long resultTime;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public List<String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(List<String> categoryNames) {
        this.categoryNames = categoryNames;
    }

    public List<VsatSchedule> getVsatSchedules() {
        return vsatSchedules;
    }

    public void setVsatSchedules(List<VsatSchedule> vsatSchedules) {
        this.vsatSchedules = vsatSchedules;
    }

    public Long getResultTime() {
        return resultTime;
    }

    public void setResultTime(Long resultTime) {
        this.resultTime = resultTime;
    }

    @Override
    public String toString() {
        return "VsatEventDetails { "
                + "eventName= " + eventName
                + ", categoryNames= " + categoryNames
                + ", vsatSchedules= " + vsatSchedules
                + ", resultTime= " + resultTime
                + " }";
    }
}
