package com.vedantu.util;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author mano
 */
public class Maps {
    public static <K, V, M extends Map<K, V>> Builder<K, V, M > create(@NotNull Supplier<M> supplier) {
        return new Builder<>(supplier);
    }

    public static class Builder <K, V, M extends Map<K, V>> {
        private M map;

        private Builder(Supplier<M> supplier) {
            this.map = supplier.get();
        }

        public Builder<K, V, M> put(K key, V val) {
            this.map.put(key, val);
            return this;
        }

        public Map<K, V> build() {
            return Collections.unmodifiableMap(map);
        }
    }
}
