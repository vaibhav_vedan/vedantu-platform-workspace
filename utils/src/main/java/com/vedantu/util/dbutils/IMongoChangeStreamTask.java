package com.vedantu.util.dbutils;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import org.bson.Document;

/**
 * @author mano
 */
@FunctionalInterface
public interface IMongoChangeStreamTask<T extends AbstractMongoEntity> {
    /**
     * @param entity watched document of type T
     */
    void consumeWatchedEntity(T entity);
}
