package com.vedantu.util.dbutils;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.logging.log4j.ThreadContext;

/**
 * Created by somil on 01/02/17.
 */
public class DBUtils {
    public static String getCallingUserId() {
        String callingUserId = AbstractFrontEndReq.Constants.CALLING_USER_ID_SYSTEM_DB;
        Object callingUserIdObject = ThreadContext.get("callingUserId");
        if(callingUserIdObject!=null) {
            callingUserId = callingUserIdObject.toString();
        }
        return callingUserId;
    }
}
