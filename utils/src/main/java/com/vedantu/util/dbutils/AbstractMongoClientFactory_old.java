package com.vedantu.util.dbutils;

import com.mongodb.MongoClientSettings;
import com.mongodb.ReadPreference;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Service
public abstract class AbstractMongoClientFactory_old {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(AbstractMongoClientFactory_old.class);

    private MongoOperations mongoOperations = null;
    private MongoClient mongoClient = null;
//
//    private String hosts;
//    private String port;
//    private boolean useAuthentication;
//    private String mongoUsername;
//    private String mongoPassword;
//    private String mongoDBName;
//    private int connectionsPerHost;

    public AbstractMongoClientFactory_old() {
        super();
    }

    public final MongoOperations getMongoOperations() {
        return mongoOperations;
    }

    protected final void initMongoOperations(String hosts, String port, boolean useAuthentication,
            String mongoUsername, String mongoPassword, String mongoDBName, int connectionsPerHost,
            ConnectionStringType connectionStringType) {

        try {

            logger.info("clientFactory initialization started");
            //http://mongodb.github.io/mongo-java-driver/3.7/driver/getting-started/quick-start-pojo/
            CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    fromProviders(PojoCodecProvider.builder().automatic(true).build()));
//        MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
//        builder.codecRegistry(pojoCodecRegistry);
//        //if useAuthentication is true, then using ssl else not (made this hackish check to not use ssl for QA/local servers)
//        builder.sslEnabled(useAuthentication);
//        MongoClientOptions options = builder.connectionsPerHost(connectionsPerHost).build();
//
//        try {
//            List<ServerAddress> seeds = new ArrayList<>();
//
//            if (hosts != null && !hosts.isEmpty() && port != null && !port.isEmpty()) {
//                String[] hostArray = hosts.split(",");
//                for (String host : hostArray) {
//                    seeds.add(new ServerAddress(host, Integer.parseInt(port)));
//                }
//            }
//            if (useAuthentication) {
//                if (ConnectionStringType.DNS_SEEDLIST.equals(connectionStringType)) {
//                    //hosts contains the entire connection string with the format mongodb+srv//....
//                    logger.info("connecting via DNS_SEEDLIST");
//                    MongoClientURI mongoClientURI = new MongoClientURI(hosts, builder);
//                    mongoClient = new MongoClient(mongoClientURI);
//                } else {
//                    logger.info("connecting via STANDARD_HOST_BASED");
//                    List<MongoCredential> credentials = new ArrayList<>();
//                    credentials.add(MongoCredential.createScramSha1Credential(mongoUsername,
//                            mongoDBName,
//                            mongoPassword.toCharArray()));
//                    mongoClient = new MongoClient(seeds, credentials, options);
//                }
//            } else {
//                mongoClient = new MongoClient(seeds, options);
//            }
            MongoClientSettings.Builder settings = MongoClientSettings.builder().codecRegistry(pojoCodecRegistry).applyToSslSettings(ssl -> ssl.enabled(useAuthentication));

            logger.info("before mongoClient");
            mongoClient = MongoClients.create(settings.build());
            logger.info("mongoClient created");
            MongoTemplate mongoTemp = new MongoTemplate(mongoClient, mongoDBName);
            logger.info("template created");
            mongoTemp.setReadPreference(ReadPreference.primaryPreferred());
            this.mongoOperations = mongoTemp;
            logger.info("MongoClientFactory is initialized");
        }catch (Exception e) {
            logger.error("Error connecting to mongo", e);
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (mongoClient != null) {
                mongoClient.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing mongo connection ", e);
        }
    }

    public enum ConnectionStringType {
        STANDARD_HOST_BASED, DNS_SEEDLIST
    }
}
