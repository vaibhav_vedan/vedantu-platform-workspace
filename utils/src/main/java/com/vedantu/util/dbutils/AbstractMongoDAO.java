/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.dbutils;

import com.amazonaws.util.EC2MetadataUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.mongodb.bulk.BulkWriteError;
import com.mongodb.MongoBulkWriteException;
import com.mongodb.MongoException;
import com.mongodb.MongoExecutionTimeoutException;
import com.mongodb.ReadPreference;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.ChangeStreamIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.lang.NonNull;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.*;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.AuditHistoryRes;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.pojo.AdminTag;
import com.vedantu.util.ratelimit.RateLimitHandler;
import com.vedantu.util.redis.UtilsRedisDao;
import com.vedantu.util.threadutil.CachedThreadPoolUtility;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.QueryMapper;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.CloseableIterator;
import org.springframework.http.HttpMethod;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author ajith
 */
public abstract class AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AbstractMongoDAO.class);

    public static final ObjectId MIN_KEY_OBJECT_ID = new ObjectId("000000000000000000000000");
    public static final ObjectId MAX_KEY_OBJECT_ID = new ObjectId("ffffffffffffffffffffffff");

    public final long NO_START = 0;
    public final static long NO_LIMIT = 0;
    public static final long UNINITIALIZED = -1L;

    public static final int DEFAULT_FETCH_SIZE = 20;
    public static final int MAX_ALLOWED_FETCH_SIZE = 2000;
    private final long maxTimeoutMillis;
    private final long maxTimeoutMillisQueryCount;
    private final long maxTimeoutMillisFindAndModify;
    private static final long BULK_OPS_MAX = 500;

    public static final String INSERTION_DATE = "insertionDate";

    private static final List<String> AUDIT_LIST = new ArrayList<>(Arrays.asList("BaseInstalment",
            "Course", "BundleEnrolment", "Orders", "Enrollment",
            "Instalment", "Transaction", "CMDSQuestion", "CMDSTest", "ContentInfo",
            "GTTAttendeeDetails", "OTFSession", "Session", "OTMBundle", "SessionAttendee",
            "BundlePackage", "VStory", "VGroup"));

    @Autowired
    private AbstractAwsSQSManagerNew abstractAwsSQSManagerNew;

    @Autowired
    private UtilsRedisDao utilsRedisDao;

    @Autowired
    private UtilsMongoClientFactory secondaryClientFactory;

    @Autowired
    private CachedThreadPoolUtility cachedThreadPoolUtility;

    @Autowired
    private RateLimitHandler rateLimitHandler;

    private static ReadPreference readPreference;
    private final String SERVICE_NAME = ConfigUtils.INSTANCE.getStringValue("SERVICE_NAME");

    public AbstractMongoDAO() {
        super();
        maxTimeoutMillis = ConfigUtils.INSTANCE.getLongValue("mongo.query.maxTimeoutMillis");
        maxTimeoutMillisQueryCount = ConfigUtils.INSTANCE.getLongValue("mongo.query.maxTimeoutMillisQueryCount");
        maxTimeoutMillisFindAndModify = ConfigUtils.INSTANCE.getLongValue("mongo.query.maxTimeoutMillisFindAndModify");
    }

    protected abstract MongoOperations getMongoOperations();

    private MongoOperations getPrimaryMongoClient() {
        return getMongoOperations();
    }

    private MongoOperations getSecondaryMongoClient() {
        return null == secondaryClientFactory ? null : secondaryClientFactory.getMongoOperations();
    }

    protected <E extends AbstractMongoEntity> void saveEntity(E p, String callingUserId) {
        p.setEntityDefaultProperties(callingUserId);
        getPrimaryMongoClient().save(p, p.getClass().getSimpleName());
        try {
            if (AUDIT_LIST.contains(p.getClass().getSimpleName())) {
                AuditHistoryRes auditHistoryRes = new AuditHistoryRes(p.getClass().getSimpleName(), p);
                abstractAwsSQSManagerNew.sendToSQS(SQSQueue.AUDIT_QUEUE, SQSMessageType.DO_AUDIT, new Gson().toJson(auditHistoryRes));
            }
        } catch (Exception ex) {
            logger.info("error saving audit  " + ex.getMessage());
        }
    }

    protected <E extends AbstractMongoEntity> void saveEntityInAnnotatedCollection(E p, String callingUserId) {
        p.setEntityDefaultProperties(callingUserId);
        getPrimaryMongoClient().save(p);
        try {
            if (AUDIT_LIST.contains(p.getClass().getSimpleName())) {
                AuditHistoryRes auditHistoryRes = new AuditHistoryRes(p.getClass().getSimpleName(), p);
                abstractAwsSQSManagerNew.sendToSQS(SQSQueue.AUDIT_QUEUE, SQSMessageType.DO_AUDIT, new Gson().toJson(auditHistoryRes));
            }
        } catch (Exception ex) {
            logger.info("error saving audit  " + ex.getMessage());
        }
    }

    protected <E extends AbstractMongoEntity> void saveEntity(E p) {
        saveEntity(p, null);
    }

    protected <E extends AbstractMongoEntity> void insertAllEntities(Collection<E> entities, String collectionName, String callingUserId) {
        setEntityDefaultProperties(entities, callingUserId);
        getPrimaryMongoClient().insert(entities, collectionName);
    }

    public <E extends AbstractMongoEntity> void insertAllEntities(Collection<E> entities, String collectionName) {
        insertAllEntities(entities, collectionName, null);
    }

    public <T extends AbstractMongoEntity> void upsertEntity(Query q, Update u, Class<T> clazz) {
        getPrimaryMongoClient().upsert(q, u, clazz.getSimpleName());
    }

    public <E extends AbstractMongoEntity> void upsertEntity(E entity, Query q, Update u, String callingUserId) {
        entity.setEntityDefaultProperties(callingUserId);
        getPrimaryMongoClient().upsert(q, u, entity.getClass().getSimpleName());
    }

    public <E extends AbstractMongoEntity> CloseableIterator<E> streamEntities(Class<E> clazz, Query query) {
        return getPreferedMongoClient(query).stream(query, clazz, clazz.getSimpleName());
    }

    public <E extends AbstractMongoEntity> CloseableIterator<E> streamEntitiesAnnotatedCollection(Class<E> clazz, Query query) {
        return getPreferedMongoClient(query).stream(query, clazz);
    }

    public <T extends AbstractMongoEntity> int updateFirst(Query q, Update u, Class<T> clazz) {
        if (u != null) {
            u.set(AbstractMongoEntity.Constants.LAST_UPDATED, System.currentTimeMillis());
        }
        UpdateResult result = getPrimaryMongoClient().updateFirst(q, u, clazz.getSimpleName());
        long modifiedCount = result.getModifiedCount();
        return Long.valueOf(modifiedCount).intValue();
    }

    public <T extends AbstractMongoEntity> int updateFirstForAnnotatedCollection(Query q, Update u, Class<T> clazz) {
        if (u != null) {
            u.set(AbstractMongoEntity.Constants.LAST_UPDATED, System.currentTimeMillis());
        }
        UpdateResult result = getPrimaryMongoClient().updateFirst(q, u, clazz);
        long modifiedCount = result.getModifiedCount();
        return Long.valueOf(modifiedCount).intValue();
    }

    protected <T extends AbstractMongoEntity> void updateMulti(Query q, Update u, Class<T> clazz) {
        if (u != null) {
            u.set(AbstractMongoEntity.Constants.LAST_UPDATED, System.currentTimeMillis());
        }
        getPrimaryMongoClient().updateMulti(q, u, clazz.getSimpleName());
    }

    protected <T extends AbstractMongoEntity> void updateMultiWithAnnotatedCollName(Query q, Update u, Class<T> clazz) {
        if (u != null) {
            u.set(AbstractMongoEntity.Constants.LAST_UPDATED, System.currentTimeMillis());
        }
        getPrimaryMongoClient().updateMulti(q, u, clazz);
    }

    protected <T extends AbstractMongoEntity> T findAndModifyEntity(Query q, Update u, FindAndModifyOptions o, Class<T> clazz) {
        setQueryMaxTimeoutFindAndModify(q);
        if (u != null) {
            u.set(AbstractMongoEntity.Constants.LAST_UPDATED, System.currentTimeMillis());
        }
        return getPrimaryMongoClient().findAndModify(q, u, o, clazz, clazz.getSimpleName());
    }

    public <T extends AbstractMongoEntity> T getEntityById(String id, Class<T> clazz) {
        Query query = new Query(Criteria.where("_id").is(id));
        setQueryMaxTimeout(query);
        T result;
        try {
            result = getPrimaryMongoClient().findOne(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    private void checkLimitLogger(Query query){
        if(null != query && 0 == query.getLimit()){
            //logger.error("size param missing" + query);
        }
    }

    public <T extends AbstractMongoEntity> T getEntityById(String id, Class<T> clazz,
            List<String> includeFields) {
        Query query = new Query(Criteria.where("_id").is(id));
        if (ArrayUtils.isNotEmpty(includeFields)) {
            includeFields.forEach((fieldName) -> {
                query.fields().include(fieldName);
            });
        }
        setQueryMaxTimeout(query);
        T result;
        try {
            result = getPrimaryMongoClient().findOne(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> T getEntityById(Long id, Class<T> clazz) {
        Query query = new Query(Criteria.where("_id").is(id));
        setQueryMaxTimeout(query);
        T result;
        try {
            result = getPrimaryMongoClient().findOne(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> T getEntityById(Long id, Class<T> clazz,
            List<String> includeFields) {
        Query query = new Query(Criteria.where("_id").is(id));
        if (ArrayUtils.isNotEmpty(includeFields)) {
            includeFields.forEach((fieldName) -> {
                query.fields().include(fieldName);
            });
        }
        setQueryMaxTimeout(query);
        T result = null;
        try {
            getPrimaryMongoClient().findOne(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> List<T> runQueryForAnnotatedCollection(Query query, Class<T> clazz) {
        if (query != null && query.getLimit() > MAX_ALLOWED_FETCH_SIZE) {
            query.limit(MAX_ALLOWED_FETCH_SIZE);
        }
        setQueryMaxTimeout(query);
        List<T> result;
        try {
            result = getPreferedMongoClient(query).find(query, clazz);
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }

        if (!ArrayUtils.isEmpty(result) && result.size() > MAX_ALLOWED_FETCH_SIZE) {
            logger.info("query without Limit  for query  :-  " + query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> List<T> runQuery(Query query, Class<T> clazz) {
        if (query != null && query.getLimit() > MAX_ALLOWED_FETCH_SIZE) {
            query.limit(MAX_ALLOWED_FETCH_SIZE);
        }
        setQueryMaxTimeout(query);
        checkLimitLogger(query);
        List<T> result;
        try {
            result = getPreferedMongoClient(query).find(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }

        if (!ArrayUtils.isEmpty(result) && result.size() > MAX_ALLOWED_FETCH_SIZE) {
            logger.info("query without Limit  for query  :-  " + query);
        }
        return result;
    }


    public <T extends AbstractMongoEntity> List<T> runQuery(Query query, Class<T> clazz, String collectionName) {
        if (query != null && query.getLimit() > 1000) {
            query.limit(MAX_ALLOWED_FETCH_SIZE);
        }
        setQueryMaxTimeout(query);
        checkLimitLogger(query);
        List<T> result;
        try {
            result = getPreferedMongoClient(query).find(query, clazz, collectionName);
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }

        if (!ArrayUtils.isEmpty(result) && result.size() > MAX_ALLOWED_FETCH_SIZE) {
            logger.info("query without Limit  for query  :-  " + query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> List<T> runQuery(Query query, Class<T> clazz, List<String> fields) {
        if (query != null && query.getLimit() > 1000) {
            query.limit(MAX_ALLOWED_FETCH_SIZE);
        }
        if (fields != null && query != null) {
            for (String field : fields) {
                query.fields().include(field);
            }
        }
        setQueryMaxTimeout(query);
        checkLimitLogger(query);
        List<T> result;
        try {
            result = getPreferedMongoClient(query).find(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }

        if (!ArrayUtils.isEmpty(result) && result.size() > MAX_ALLOWED_FETCH_SIZE) {
            logger.info("query without Limit  for query  :-  " + query);
        }
        return result;
    }

    private MongoOperations getPreferedMongoClient(Query query) {
        if ("platform".equalsIgnoreCase(SERVICE_NAME) || !"prod".equalsIgnoreCase(ConfigUtils.INSTANCE.getEnvironmentSlug())) {
            return getPrimaryMongoClient();
        }

        if (query != null) {
            Document document = query.getQueryObject();
            Object _id = document.get("_id");
            Object id = document.get("id");
            if (Objects.nonNull(_id) || Objects.nonNull(id)) {
                return getPrimaryMongoClient();
            } else {
                if (readPreference == null) {
                    try {
                        setPreferenceForServer();
                    } catch (InternalServerErrorException | BadRequestException e) {
                        logger.error(e.getErrorMessage(), e);
                    }
                }

                if (readPreference != null) {
                    if (readPreference == ReadPreference.primaryPreferred()) {
                        return getPrimaryMongoClient();
                    } else if (readPreference == ReadPreference.secondaryPreferred() && getSecondaryMongoClient() != null) {
                        return getSecondaryMongoClient();
                    } else {
                        return getPrimaryMongoClient();
                    }
                } else {
                    return getPrimaryMongoClient();
                }
            }
        }
        return getPrimaryMongoClient();
    }

    private void setPreferenceForServer() throws InternalServerErrorException, BadRequestException {
        String name = ConfigUtils.INSTANCE.getStringValue("SERVICE_NAME");
        if (name != null) {
            String macAddress = EC2MetadataUtils.getMacAddress();

            if (StringUtils.isNotBlank(macAddress)) {
                String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
                String prefKey = "MONGO_PREF_" + name.toUpperCase() + "_"
                        + StringUtils.defaultIfBlank(env, "").toUpperCase();
                String key = prefKey + "_" + macAddress.toUpperCase();
                String keyVal = utilsRedisDao.get(key);
                if (StringUtils.isNotBlank(keyVal)) {
                    if ("PRIMARY".equals(keyVal)) {
                        readPreference = ReadPreference.primaryPreferred();
                    } else if ("SECONDARY".equals(keyVal)) {
                        readPreference = ReadPreference.secondaryPreferred();
                    } else {
                        readPreference = ReadPreference.primaryPreferred();
                    }
                } else {
                    Long prefVal = utilsRedisDao.increment(prefKey, 1);

                    if (prefVal % 2 == 0) {
                        readPreference = ReadPreference.primaryPreferred();
                        utilsRedisDao.setex(key, "PRIMARY", 21600);
                    } else {
                        readPreference = ReadPreference.secondaryPreferred();
                        utilsRedisDao.setex(key, "SECONDARY", 21600);
                    }
                }
            } else {
                readPreference = ReadPreference.primaryPreferred();
            }
        } else {
            readPreference = ReadPreference.primaryPreferred();
        }
    }

    public <T extends AbstractMongoEntity> T findOne(Query query, Class<T> clazz) {
        setQueryMaxTimeout(query);
        T result;
        try {
            result = getPrimaryMongoClient().findOne(query, clazz, clazz.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> T findOne(Query query, Class<T> clazz, String collectionName) {
        setQueryMaxTimeout(query);
        T result;
        try {
            result = getPrimaryMongoClient().findOne(query, clazz, collectionName);
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity, R> List<R> findDistinct(Query query, String distinctField,
                                                          Class<T> entityClass, Class<R> distinctFieldClass) {
        setQueryMaxTimeout(query);
        checkLimitLogger(query);
        List<R> result;
        try {
            result = getPrimaryMongoClient().findDistinct(query, distinctField, entityClass, distinctFieldClass);
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <T extends AbstractMongoEntity> long queryCount(Query query, Class<T> clazz) {
        setQueryMaxTimeoutQueryCount(query);
        return getPreferedMongoClient(query).count(query, clazz, clazz.getSimpleName());
    }

    public <T extends AbstractMongoEntity> int deleteEntityById(String id, Class<T> clazz) {
        Query query = new Query(Criteria.where("_id").is(id));
        DeleteResult result = getPrimaryMongoClient().remove(query, clazz,
                clazz.getSimpleName());
        return Long.valueOf(result.getDeletedCount()).intValue();
    }

    public <T extends AbstractMongoEntity> int deleteEntityById(Long id, Class<T> clazz) {
        Query query = new Query(Criteria.where("_id").is(id));
        DeleteResult result = getPrimaryMongoClient().remove(query, clazz,
                clazz.getSimpleName());
        return Long.valueOf(result.getDeletedCount()).intValue();
    }

    public <T extends AbstractMongoEntity> int deleteEntities(Query query, Class<T> clazz) {
        DeleteResult result = getPrimaryMongoClient().remove(query, clazz,
                clazz.getSimpleName());
        return Long.valueOf(result.getDeletedCount()).intValue();
    }

    // aggregation queries
    public <T extends AbstractMongoEntity> List<T> getEntitiesByUpdationTimeAndLimit(Long fromTime, Long tillTime, Integer limit, Class<T> clazz) {
        Query query = new Query();
        if ((fromTime != null && fromTime > 0)
                || (tillTime != null && tillTime > 0)) {

            Criteria criteria = null;
            if (fromTime != null && fromTime > 0) {
                criteria = Criteria.where(AbstractMongoEntity.Constants.LAST_UPDATED).gte(fromTime);
            }

            if (tillTime != null && tillTime > 0) {
                if (criteria != null) {
                    criteria = criteria.andOperator(Criteria.where(AbstractMongoEntity.Constants.LAST_UPDATED).lt(tillTime));
                } else {
                    criteria = Criteria.where(AbstractMongoEntity.Constants.LAST_UPDATED).lt(tillTime);
                }
            }
            query.addCriteria(criteria);
        }

        if (limit != null) {
            query.limit(limit);
        } else {
            query.limit(MAX_ALLOWED_FETCH_SIZE);
        }
        setQueryMaxTimeout(query);
        checkLimitLogger(query);
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, AbstractMongoEntity.Constants.LAST_UPDATED));
        query.with(Sort.by(orderList));
        return runQuery(query, clazz);
    }

    public <E extends AbstractMongoEntity> void setEntityDefaultProperties(Collection<E> entities, String callingUserId) {
        if (entities != null && !entities.isEmpty()) {
            for (E entity : entities) {
                entity.setEntityDefaultProperties(callingUserId);
            }
        }
    }

    public void setFetchParameters(Query query, AbstractFrontEndListReq abstractFrontEndListReq) {
        setFetchParameters(query, abstractFrontEndListReq.getStart(), abstractFrontEndListReq.getSize());
    }

    public void setFetchParameters(Query query, Integer start, Integer limit) {
        int fetchStart = 0;
        if (start != null && start >= 0) {
            fetchStart = start;
        }
        int fetchSize = DEFAULT_FETCH_SIZE;
        if (limit != null && limit >= 0) {
            fetchSize = limit;
        }
        query.skip(fetchStart);
        query.limit(fetchSize);
    }

    public void setQueryFieldsFromAbstractEntity(Query query) {
        query.fields().include(AbstractMongoEntity.Constants.CREATION_TIME);
        query.fields().include(AbstractMongoEntity.Constants.LAST_UPDATED);
        query.fields().include(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
        query.fields().include(AbstractMongoEntity.Constants.CREATED_BY);
    }

    public void includeFieldsForProjection(Query query, List<String> includeFields) {
        if (Objects.nonNull(query)) {
            Optional.ofNullable(includeFields).orElseGet(ArrayList::new).forEach(query.fields()::include);
        }
    }

    private void setQueryMaxTimeout(Query query) {
        if (query != null && query.getMeta().getMaxTimeMsec() == null) {
            query.maxTimeMsec(maxTimeoutMillis);
        }
    }

    private void setQueryMaxTimeoutQueryCount(Query query) {
        if (query != null && query.getMeta().getMaxTimeMsec() == null) {
            query.maxTimeMsec(maxTimeoutMillisQueryCount);
        }
    }

    private void setQueryMaxTimeoutFindAndModify(Query query) {
        if (query != null && query.getMeta().getMaxTimeMsec() == null) {
            query.maxTimeMsec(maxTimeoutMillisFindAndModify);
        }
    }

    public <E extends AbstractMongoEntity, T> List<E> getEntitiesForIds(Collection<T> ids, Class<E> eClass) {
        Query query = new Query(Criteria.where("_id").in(ids));
        setQueryMaxTimeout(query);
        checkLimitLogger(query);
        List<E> result;
        try {
            result = getPrimaryMongoClient().find(query, eClass, eClass.getSimpleName());
        } catch (MongoExecutionTimeoutException e) {
            logger.error("MongoExecutionTimeoutException " + query);
            throw new MongoQueryTimeoutException(maxTimeoutMillis, e, query);
        }
        return result;
    }

    public <E extends AbstractMongoEntity> void addOrRemoveAdminTags(List<AdminTag> adminTags, Long callingUserId,
            boolean removeTags, Class<E> eClass) throws VException {
        List<List<AdminTag>> partition = Lists.partition(adminTags, 20);
        boolean longIdEntityType = AbstractMongoLongIdEntity.class.isAssignableFrom(eClass);

        Set<String> entityIds = adminTags.stream().map(AdminTag::getEntityId).collect(Collectors.toSet());

        for (List<AdminTag> tags : partition) {
            Set<Object> ids;
            try {
                ids = longIdEntityType ? tags.stream().map(AdminTag::getEntityId).mapToLong(Long::parseLong).boxed().collect(Collectors.toSet())
                        : tags.stream().map(AdminTag::getEntityId).collect(Collectors.toSet());
            } catch (Exception e) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid entity ids provided. Please check");
            }

            List<E> entities = this.getEntitiesForIds(ids, eClass);
            Set<Object> idsPresent = entities.stream().map(AbstractMongoEntity::getId).collect(Collectors.toSet());
            entityIds.removeAll(idsPresent.stream().map(String::valueOf).collect(Collectors.toSet()));
            Map<String, List<AdminTag>> collect = tags.stream().collect(Collectors.groupingBy(AdminTag::getEntityId));
            for (E entity : entities) {

                List<AdminTag> tagList = collect.get(String.valueOf(entity.getId()));
                if (ArrayUtils.isNotEmpty(tagList)) {
                    entity.addOrRemoveAdminTags(tagList, callingUserId, removeTags);
                    this.saveEntity(entity, String.valueOf(callingUserId));
                }
            }
        }

        if (!entityIds.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Some of the ID's not found in DB : " + String.join(",", entityIds));
        }
    }

    public <E extends AbstractMongoEntity> ChangeStreamIterable<Document> getChangeStreamDocument(Class<E> clazz) {
        MongoTemplate operations = (MongoTemplate) (getSecondaryMongoClient() == null ? getPrimaryMongoClient() : getSecondaryMongoClient());
        return operations.getCollection(operations.getCollectionName(clazz)).watch();
    }

    public ChangeStreamIterable<Document> getChangeStreamDocument(String collection) {
        MongoTemplate operations = (MongoTemplate) (getSecondaryMongoClient() == null ? getPrimaryMongoClient() : getSecondaryMongoClient());
        return operations.getCollection(collection).watch();
    }

    public <E extends AbstractMongoEntity> ChangeStreamIterable<Document> getChangeStreamDocument(Class<E> clazz, List<? extends Bson> pipeline) {
        MongoOperations operations = getSecondaryMongoClient() == null ? getPrimaryMongoClient() : getSecondaryMongoClient();
        return operations.getCollection(operations.getCollectionName(clazz)).watch(pipeline);
    }

    public ChangeStreamIterable<Document> getChangeStreamDocument(String collection, List<? extends Bson> pipeline) {
        MongoOperations operations = getSecondaryMongoClient() == null ? getPrimaryMongoClient() : getSecondaryMongoClient();
        return operations.getCollection(collection).watch(pipeline);
    }

    public <E extends AbstractMongoEntity> E convert(Document document, Class<E> entityClass) {
        MongoConverter converter = getPrimaryMongoClient().getConverter();
        return converter.read(entityClass, document);
    }

    /**
     * Use with CAUTION as horizontal scaling is not out of the box
     * https://stackoverflow.com/questions/54295043/what-is-a-good-horizontal-scaling-strategy-for-a-mongodb-change-stream-reader
     *
     * If consumption of watched documents takes longer time to process it's work
     * then the subsequent documents may be fall behind
     * and over time the difference gap might significantly increase
     * and possibility of not processing data which fell behind increased when server restarted
     *
     * @param task
     * @param entityClass
     * @param pipeline
     * @param <E>
     */
    public <E extends AbstractMongoEntity> void watchDocument(final IMongoChangeStreamTask<E> task, Class<E> entityClass,
            List<Bson> pipeline) {
        // OTHER QA ENV DON'T HAVE REPLICA SET AND WIRED TIGER ENGINE
        if (!"prod".equalsIgnoreCase(ConfigUtils.INSTANCE.getEnvironmentSlug())
                && !"dev4".equalsIgnoreCase(ConfigUtils.INSTANCE.getEnvironmentSlug())) {
            return;
        }
        cachedThreadPoolUtility.execute(() -> {
            while (true) {
                String name = entityClass.getSimpleName().toUpperCase();
                logger.info("WATCHING " + name);
                try {
                    ChangeStreamIterable<Document> stream = getChangeStreamDocument(entityClass, pipeline)
                            .fullDocument(FullDocument.UPDATE_LOOKUP);
                    for (ChangeStreamDocument<Document> streamDocument : stream) {
                        try {
                            Document document = streamDocument.getFullDocument();
                            E convert = convert(document, entityClass);
                            task.consumeWatchedEntity(convert);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                } catch (Exception e) {
                    try {
                        String keyMinute = "WATCH_TASK_ERROR_RATE_PER_MINUTE_" + name + "_" + EC2MetadataUtils.getMacAddress();
                        long count = rateLimitHandler.rateLimitCount(keyMinute, 5, TimeUnit.MINUTES);

                        if (count > 15) {
                            String keyHour = "WATCH_TASK_ERROR_RATE_PER_HOUR_" + name + "_" + EC2MetadataUtils.getMacAddress();
                            long countPerHour = rateLimitHandler.rateLimitCount(keyHour, 1, TimeUnit.HOURS);
                            if (countPerHour >= 5) {
                                logger.error("WATCH TASK CONTINUOUSLY FAILED FOR " + ConfigUtils.INSTANCE.getServiceName().toUpperCase());
                                TimeUnit.MINUTES.sleep(1);
                                break;
                            }

                            logger.warn(e.getMessage(), e);
                            TimeUnit.SECONDS.sleep(10);
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                } finally {
                    logger.info(name + " STREAM CLOSED");
                }
            }
        });
    }

    // supports homogeneous operations for now
    // all bulk insert ops are unordered for now
    public <E extends AbstractMongoEntity> long bulkInsert(List<E> docs,
            @NonNull Class<E> clazz,
            String callingUserId) {

        final MongoCollection<Document> collection = getPrimaryMongoClient().getCollection(clazz.getSimpleName());
        final MongoConverter converter = getPrimaryMongoClient().getConverter();
        final BulkWriteOptions bulkWriteOptions = new BulkWriteOptions()
                .ordered(false).bypassDocumentValidation(true);
        long insertedCount = 0L;

        if (!CollectionUtils.isEmpty(docs) && docs.size() > BULK_OPS_MAX) {
            logger.error("Processing bulkWriteInsert request of size more than "
                    + BULK_OPS_MAX + " for entityType : " + clazz.getSimpleName());
        }

        // define write operations
        setEntityDefaultProperties(docs, callingUserId);
        List<WriteModel<Document>> writes = new ArrayList<>();
        for (E doc : docs) {
            if (doc == null) {
                continue;
            }
            Document insertDoc = new Document();
            converter.write(doc, insertDoc);
            writes.add(new InsertOneModel<>(insertDoc));
        }

        // execute bulk operations
        BulkWriteResult bulkWriteResult;
        try {
            bulkWriteResult = collection.bulkWrite(writes, bulkWriteOptions);
            insertedCount += bulkWriteResult.getInsertedCount();
            logger.info("bulk insert for all docs : success");
        } catch (MongoBulkWriteException bwe) {
            List<BulkWriteError> bulkWriteErrors = bwe.getWriteErrors();
            List<WriteModel<Document>> retryDocs = new ArrayList<>();

            for (BulkWriteError bulkWriteError : bulkWriteErrors) {
                retryDocs.add(writes.get(bulkWriteError.getIndex()));
            }

            try {
                bulkWriteResult = collection.bulkWrite(retryDocs, bulkWriteOptions);
                insertedCount += bulkWriteResult.getInsertedCount();
            } catch (MongoBulkWriteException e) {
                logger.error(bwe.getWriteErrors());
            } catch (MongoException me) {
                logger.error("Bulk write operation failed for inserting docs " + me.getMessage() + retryDocs);
            }

        } catch (MongoException me) {
            logger.error("Bulk write operation failed for inserting docs " + me.getMessage() + docs);
        }

        return Math.max(insertedCount, 0L);
    }

    public <E extends AbstractMongoEntity> long bulkUpdate(List<? extends Bson> queries,
            List<? extends Bson> updates,
            boolean upsert, @NonNull Class<E> clazz,
            String callingUserId) {
        if (queries.size() != updates.size()) {
            throw new MongoException("Number of queries - update docs mismatch");
        }

        if (updates.size() > BULK_OPS_MAX) {
            logger.error("Processing a bulkUpdate write request of size more than " + BULK_OPS_MAX + "of entity " + clazz.getSimpleName());
        }

        final MongoCollection<Document> collection = getPrimaryMongoClient().getCollection(clazz.getSimpleName());
        final BulkWriteOptions bulkWriteOptions = new BulkWriteOptions()
                .ordered(false).bypassDocumentValidation(true);
        final UpdateOptions updateOptions = new UpdateOptions().upsert(upsert);

        Iterator<? extends Bson> queryIterator = queries.iterator();
        Iterator<? extends Bson> updateIterator = updates.iterator();
        List<WriteModel<Document>> actions = new ArrayList<>(updates.size());
        long modifiedCount = 0L;
        Bson query = queries.get(0);

        while (updateIterator.hasNext()) {
            if (queries.size() != 1) {
                query = queryIterator.next();
            }
            Bson update = updateIterator.next();
            if (update instanceof Document) {
                Document setDocument = (Document) ((Document) update).get(AbstractMongoEntity.Constants.UPDATE_SET_PARAM);
                setDocument.append(AbstractMongoEntity.Constants.LAST_UPDATED, System.currentTimeMillis());
                if (StringUtils.isNotEmpty(callingUserId)) {
                    setDocument.append(AbstractMongoEntity.Constants.LAST_UPDATED_BY, callingUserId);
                }
            }
            actions.add(new UpdateOneModel<>(query, update, updateOptions));
        }

        BulkWriteResult bulkWriteResult;

        try {
            bulkWriteResult = collection.bulkWrite(actions, bulkWriteOptions);
            modifiedCount += bulkWriteResult.getModifiedCount();
        } catch (MongoBulkWriteException bwe) {
            List<BulkWriteError> bulkWriteErrors = bwe.getWriteErrors();
            List<WriteModel<Document>> retryDocs = new ArrayList<>();

            for (BulkWriteError bulkWriteError : bulkWriteErrors) {
                retryDocs.add(actions.get(bulkWriteError.getIndex()));
            }

            try {
                bulkWriteResult = collection.bulkWrite(retryDocs, bulkWriteOptions);
                modifiedCount += bulkWriteResult.getModifiedCount();
            } catch (MongoBulkWriteException e) {
                logger.error(bwe.getWriteErrors());
            } catch (MongoException me) {
                logger.error("Bulk write operation failed for updating docs " + me.getMessage() + retryDocs);
            }

        } catch (MongoException me) {
            logger.error("Bulk write operation failed for updating docs " + me.getMessage() + updates);
        }

        return Math.max(0L, modifiedCount);
    }


    public <E extends AbstractMongoEntity> Document getParsedQueryDocument(Query query, Class<E> entityClass) throws NoSuchFieldException, IllegalAccessException {
        MongoTemplate template = (MongoTemplate) getMongoOperations();
        Field mappingContext = template.getClass().getDeclaredField("mappingContext");
        mappingContext.setAccessible(true);
        Field queryMapper = template.getClass().getDeclaredField("queryMapper");
        queryMapper.setAccessible(true);

        MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> ctx = (MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty>) mappingContext.get(template);
        MongoPersistentEntity<?> entity = ctx.getPersistentEntity(entityClass);
        QueryMapper mapper = (QueryMapper) queryMapper.get(template);
        return mapper.getMappedObject(query.getQueryObject(), entity);
    }

    protected Query reportsUpsertQuery(Long timeToBeUpdatedForDocument){
        Query query=new Query();
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.CREATION_TIME).is(timeToBeUpdatedForDocument));
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.CREATED_BY).is("SYSTEM"));
        return query;
    }

    protected void throwInvalidOrMissingParameterException() throws BadRequestException {
        throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Some of the parameters are missing or invalid please have a look.", Level.ERROR);
    }

    public void createIndex(String collection, String field, Long expiryInSeconds) {
        try {
            MongoCollection<Document> dbCollection  = getPrimaryMongoClient().getCollection(collection);
            if (Objects.nonNull(expiryInSeconds)){
                dbCollection.createIndex(Indexes.ascending(field),new IndexOptions().expireAfter(expiryInSeconds, TimeUnit.SECONDS).background(true));
            }else{
                dbCollection.createIndex(Indexes.ascending(field),new IndexOptions().background(true));
            }
        }catch (MongoException me) {
            logger.error("Index creation is failed" + me.getMessage());
        }

    }

    public void dropIndex(String collection, String field) {
        try {
            MongoCollection<Document> dbCollection  = getPrimaryMongoClient().getCollection(collection);
            dbCollection.dropIndex(Indexes.ascending(field));
        }catch (MongoException me) {
            logger.error("Index dropping operation is failed" + me.getMessage());
        }

    }

}
