package com.vedantu.util.dbutils;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Service
public abstract class AbstractMongoClientFactory {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(AbstractMongoClientFactory.class);

    private MongoClient mongoClient = null;
    private MongoOperations mongoOperations = null;

    public final MongoOperations getMongoOperations() {
        return mongoOperations;
    }

    public AbstractMongoClientFactory() {
        super();
    }

    protected final void initMongoOperations(String hosts, String port, boolean useAuthentication,
            String mongoUsername, String mongoPassword, String mongoDBName, int connectionsPerHost,
            AbstractMongoClientFactory.ConnectionStringType connectionStringType) {
        logger.info("initializing mongo connection");
        mongoClient = getMongoClient(hosts, port, useAuthentication, mongoUsername, mongoPassword, mongoDBName,
                connectionsPerHost, connectionStringType);

        // dbFactory()
        MongoDatabaseFactory mongoDatabaseFactory = new SimpleMongoClientDatabaseFactory(mongoClient, mongoDBName);

        // MongoCustomConversions
        MongoCustomConversions conversions = new MongoCustomConversions(Collections.emptyList());

        //MongoMappingContext
        // in spring data mongodb 3, auto index creation is disabled. So, we need set it explicitly
        MongoMappingContext mongoMappingContext = new MongoMappingContext();
        mongoMappingContext.setAutoIndexCreation(true); // this is to enable auto index creation based on annotations
        mongoMappingContext.setSimpleTypeHolder(conversions.getSimpleTypeHolder());
        mongoMappingContext.afterPropertiesSet();

        //MappingMongoConverter using dbFactory, mongoMappingContext, MongoCustomConversions;
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDatabaseFactory);
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
        converter.setCustomConversions(conversions);
        converter.setCodecRegistryProvider(mongoDatabaseFactory);
        converter.afterPropertiesSet();

        //mongoTemplate
        MongoTemplate template = new MongoTemplate(mongoDatabaseFactory, converter);
        logger.info("connection created");
        mongoOperations = template;
    }

    private MongoClient getMongoClient(String hosts, String port, boolean useAuthentication,
            String mongoUsername, String mongoPassword, String mongoDBName, int connectionsPerHost,
            AbstractMongoClientFactory.ConnectionStringType connectionStringType) {
        logger.info("creating mongoClient");
        MongoClientSettings.Builder builder = MongoClientSettings.builder();

        // codecRegistry
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        builder.codecRegistry(pojoCodecRegistry);

        //if useAuthentication is true, then using ssl else not (made this hackish check to not use ssl for QA/local servers)
        builder.applyToSslSettings(server -> server.enabled(useAuthentication));

        //setting connectionsPerHost
        builder.applyToConnectionPoolSettings(conn -> conn.maxSize(connectionsPerHost));

        List<ServerAddress> seeds = new ArrayList<>();

        if (hosts != null && !hosts.isEmpty() && port != null && !port.isEmpty()) {
            String[] hostArray = hosts.split(",");
            for (String host : hostArray) {
                seeds.add(new ServerAddress(host, Integer.parseInt(port)));
            }
        }

        MongoClient _mongoClient = null;
        if (useAuthentication) {
            if (ConnectionStringType.DNS_SEEDLIST.equals(connectionStringType)) {
                //hosts contains the entire connection string with the format mongodb+srv//....
                logger.info("connecting via DNS_SEEDLIST");
                ConnectionString connectionString = new ConnectionString(hosts);
                builder.applyConnectionString(connectionString);
                _mongoClient = MongoClients.create(builder.build());
            } else {
                logger.info("connecting via STANDARD_HOST_BASED");
                MongoCredential credential = MongoCredential.createScramSha1Credential(mongoUsername, mongoDBName, mongoPassword.toCharArray());
                builder.credential(credential);
                // setting hosts
                builder.applyToClusterSettings(cluster -> cluster.hosts(seeds));
                _mongoClient = MongoClients.create(builder.build());
            }
        } else {
            //for local and qa envs where no username and password are used
            // setting hosts
            builder.applyToClusterSettings(cluster -> cluster.hosts(seeds));
            _mongoClient = MongoClients.create(builder.build());
        }
        logger.info("mongoClient created");
        return _mongoClient;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (mongoClient != null) {
                mongoClient.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing mongo connection ", e);
        }
    }

    public enum ConnectionStringType {
        STANDARD_HOST_BASED, DNS_SEEDLIST
    }

}
