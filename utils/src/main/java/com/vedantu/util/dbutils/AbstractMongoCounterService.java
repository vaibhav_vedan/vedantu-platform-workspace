/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.dbutils;

import com.vedantu.util.dbentities.mongo.Counter;
import java.util.Random;
import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import org.springframework.data.mongodb.core.MongoOperations;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import org.springframework.data.mongodb.core.query.Update;

/**
 *
 * @author somil
 */
//References
//https://docs.mongodb.com/v3.0/tutorial/create-an-auto-incrementing-field/
//https://gerrydevstory.com/2013/05/16/auto-incrementing-field-on-spring-data-mongodb/
public abstract class AbstractMongoCounterService {

    private static final Random RANDOM = new Random();

    public AbstractMongoCounterService() {
    }

    protected abstract MongoOperations getMongoOperations();

    public Long getNextSequence(String fieldName) {
        int inc = 1 + RANDOM.nextInt(9999);
        if (inc % 2 != 0) {//to ensure we always add an even increment
            inc += 1;
        }
        return getNextSequence(fieldName, inc);
    }

    public Long getNextSequentialSequence(String fieldName) {
        int inc = 1;
        return getNextSequence(fieldName, inc);
    }    
    
    public Long getNextSequence(String fieldName, int seqInc) {
        Counter counter = getMongoOperations().findAndModify(
                query(where("_id").is(fieldName)),
                new Update().inc("seq", seqInc),
                options().returnNew(true).upsert(true),
                Counter.class);
        return counter.getSeq();
    }
}

