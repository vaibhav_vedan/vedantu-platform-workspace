package com.vedantu.util.dbutils;

import com.mongodb.ReadPreference;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class UtilsMongoClientFactory extends AbstractMongoClientFactory {


    private final Logger logger = LogFactory.getLogger(UtilsMongoClientFactory.class);

    private final String hosts = ConfigUtils.INSTANCE.getStringValue("MONGO_HOST");
    private final String port = ConfigUtils.INSTANCE.getStringValue("MONGO_PORT");
    private final boolean useAuthentication = ConfigUtils.INSTANCE.getBooleanValue("useAuthentication");
    private final String mongoUsername = ConfigUtils.INSTANCE.getStringValue("MONGO_USERNAME");
    private final String mongoPassword = ConfigUtils.INSTANCE.getStringValue("MONGO_PASSWD");
    private final String mongoDBName = ConfigUtils.INSTANCE.getStringValue("MONGO_DB_NAME");
    private final int connectionsPerHost = 200;
    private final String connectionStringType = ConfigUtils.INSTANCE.getStringValue("mongo.connection.string.type", ConnectionStringType.STANDARD_HOST_BASED.name());
    ConnectionStringType _connectionStringType = ConnectionStringType.valueOf(connectionStringType);        

    public UtilsMongoClientFactory() {
        super();
        initMongoOperations(hosts, port, useAuthentication, mongoUsername,
                mongoPassword, mongoDBName, connectionsPerHost,_connectionStringType);
        MongoOperations mongoOperations = getMongoOperations();
        ((MongoTemplate) mongoOperations).setReadPreference(ReadPreference.secondary());
        logger.info("Mongo connection created successfully, connections created: " + connectionsPerHost);
    }
}