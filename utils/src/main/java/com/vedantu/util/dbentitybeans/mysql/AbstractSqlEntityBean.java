package com.vedantu.util.dbentitybeans.mysql;

/**
 * Created by somil on 31/01/17.
 */
public class AbstractSqlEntityBean {

    public Long id;
    public Long creationTime;
    public String createdBy;
    public Long lastUpdatedTime;
    public String lastUpdatedby;
    private Boolean enabled;
    private String callingUserId;

    public AbstractSqlEntityBean() {
        super();
    }

    public AbstractSqlEntityBean(Long id, Long creationTime, String createdBy, Long lastUpdatedTime, String lastUpdatedby,
                             Boolean enabled, String callingUserId) {
        super();
        this.id = id;
        this.creationTime = creationTime;
        this.createdBy = createdBy;
        this.lastUpdatedTime = lastUpdatedTime;
        this.lastUpdatedby = lastUpdatedby;
        this.enabled = enabled;
        this.callingUserId = callingUserId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getLastUpdatedby() {
        return lastUpdatedby;
    }

    public void setLastUpdatedby(String lastUpdatedby) {
        this.lastUpdatedby = lastUpdatedby;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getCallingUserId() {
        return callingUserId;
    }

    public void setCallingUserId(String callingUserId) {
        this.callingUserId = callingUserId;
    }

    @Override
    public String toString() {
        return "AbstractSqlEntity [id=" + id + ", creationTime=" + creationTime + ", createdBy=" + createdBy
                + ", lastUpdatedTime=" + lastUpdatedTime + ", lastUpdatedby=" + lastUpdatedby + ", enabled=" + enabled
                + ", callingUserId=" + callingUserId + "]";
    }

    public static class Constants {
        public static final String ID = "id";
        public static final String CREATION_TIME = "creationTime";
        public static final String CREATED_BY = "createdBy";
        public static final String LAST_UPDATED_TIME = "lastUpdatedTime";
        public static final String LAST_UPDATED_BY = "lastUpdatedby";
        public static final String ENABLED = "enabled";
    }

}
