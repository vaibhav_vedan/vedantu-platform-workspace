package com.vedantu.util.dbentitybeans.mongo;

public enum EntityState {
	ACTIVE, INACTIVE, DELETED
}
