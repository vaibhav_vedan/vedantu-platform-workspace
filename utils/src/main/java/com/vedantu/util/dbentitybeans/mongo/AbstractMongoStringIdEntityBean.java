/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.dbentitybeans.mongo;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/**
 *
 * @author ajith
 */
public class AbstractMongoStringIdEntityBean extends AbstractMongoEntityBean {

    private String id;

    public AbstractMongoStringIdEntityBean() {
    }

    public AbstractMongoStringIdEntityBean(String id, Long creationTime, String createdBy, Long lastUpdated, String callingUserId) {
        super(creationTime, createdBy, lastUpdated, callingUserId);
        this.id = id;
    }
    
    public AbstractMongoStringIdEntityBean(AbstractMongoStringIdEntity entity) {
        super(entity.getCreationTime(), entity.getCreatedBy(), 
                entity.getLastUpdated(), entity.getLastUpdatedBy());
        this.id = entity.getId();
    }    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
