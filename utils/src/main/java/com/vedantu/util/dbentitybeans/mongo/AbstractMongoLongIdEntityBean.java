/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.dbentitybeans.mongo;

/**
 *
 * @author ajith
 */
public class AbstractMongoLongIdEntityBean extends AbstractMongoEntityBean {

    private Long id;

    public AbstractMongoLongIdEntityBean() {
    }

    public AbstractMongoLongIdEntityBean(Long id, Long creationTime, String createdBy, Long lastUpdated, String lastUpdatedBy) {
        super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
