package com.vedantu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

public class CommonUtils {

	public static int getNextSetIndex(Integer[] array, int index) {
		if (index < array.length) {
			for (int i = index; i < array.length; i++) {
				if (array[i] == 1) {
					return i;
				}
			}
		}

		return -1;
	}

	public static int getNextClearIndex(Integer[] array, int index) {
		for (int i = index; i < array.length; i++) {
			if (array[i] == 0) {
				return i;
			}
		}

		return array.length;
	}

	// Returns if both the collections have the same elements irrespective of
	// the index of the elements
	public static boolean isEqualCollection(List<String> col1, List<String> col2) {
		if (CollectionUtils.isEmpty(col1) && CollectionUtils.isEmpty(col2)) {
			return true;
		} else if (CollectionUtils.isEmpty(col1) || CollectionUtils.isEmpty(col2)) {
			return false;
		} else if (col1.size() != col2.size()) {
			return false;
		} else {
			Set<String> colSet1 = new HashSet<String>(col1);
			Set<String> colSet2 = new HashSet<String>(col2);
			return colSet1.equals(colSet2);
		}
	}

	public static File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
		File convFile = new File(multipart.getOriginalFilename());
		multipart.transferTo(convFile);
		return convFile;
	}

	public static byte[] getFileBytes(File file) throws IOException {
		FileInputStream is = new FileInputStream(file);
		// convert file into array of bytes
		byte[] bFile = new byte[(int) file.length()];
		is.read(bFile);
		is.close();
		return bFile;
	}

	public static Map<String, String> getParameterMapFromUrl(String urlString) throws MalformedURLException {
		URL url = new URL(urlString);
		String query = url.getQuery();
		Map<String, String> parameters = new HashMap<>();
                if(StringUtils.isEmpty(query)){
                    return parameters;
                }
		String[] params = query.split("&");
		for (int i = 0; i < params.length; i++) {
			String[] split = params[i].split("=");
			parameters.put(split[0], split[1]);
		}

		return parameters;
	}

	public static float roundFloat(float d) {
		return roundFloat(d, Constants.FLOAT_DECIMAL_DIGIT_ACCURACY);
	}

	public static float roundFloat(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.floatValue();
	}
	
	public static String getListString(List<String> values) {
		// String join is introduced in JDK8. Upgrade in case of error.
		return String.join(", ", values);
	}

	public static <T> Predicate<T> not(Predicate<T> t) {
		return t.negate();
	}


	public static Boolean isValidEmailId(String emailId){

		// Corner cases
		if (emailId == null || emailId.isEmpty())
			return false;

		// Regex pattern for emailId
		String emailRegex = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|((\\d*[a-zA-Z]{1,64}\\d*\\.)+[a-zA-Z]{2,256}))$";

		Pattern pattern = Pattern.compile(emailRegex);
		return pattern.matcher(emailId).matches();
	}
}
