/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.util.aop;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.security.JwtAuthenticatedProfile;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;


/*
References
http://www.byteslounge.com/tutorials/spring-aop-example
http://www.byteslounge.com/tutorials/spring-aop-pointcut-advice-example
http://howtodoinjava.com/spring/spring-aop/writing-spring-aop-aspectj-pointcut-expressions-with-examples/
https://guptavikas.wordpress.com/2010/04/15/aspectj-pointcut-expressions/
http://stackoverflow.com/questions/8428589/spring-aop-annotation-pointcut-not-causing-advice-to-execute
 */
public class AbstractAOPLayer {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private StatsdClient statsdClient;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AbstractAOPLayer.class);

    @Autowired
    private HttpSessionUtils sessionUtils;
    
    
//    @Pointcut("execution(* com.vedantu.platform.*.controllers.*.*(..))")
//    public void allCtrlMethods() {
//    }
    @Pointcut("within(com.vedantu.*.controllers..*)")
    public void allCtrlMethods() {
    }

//    @Pointcut("execution(* com.vedantu.platform.*.managers.*.*(..))")
//    public void allManagerMethods() {
//    }
    @Pointcut("allManagerLayerMethods() || allServiceLayerMethods()")
    public void allManagerMethods() {
    }


    @Pointcut("within(com.vedantu.*.managers.AwsSNS*)")
    public void allSNSManagerMethods() {
    }

    @Pointcut("within(com.vedantu.*.managers..*)")
    public void allManagerLayerMethods() {
    }

    @Pointcut("within(com.vedantu.*.service..*)")
    public void allServiceLayerMethods() {
    }

    @Pointcut("within(com.vedantu.*.listeners..*)")
    public void allListenerMethods() {
    }

    //@Before("execution(* com.vedantu.controllers.OrdersController.*(..))")
    @Before("allCtrlMethods() || allManagerMethods()")
    public void beforeExecution(JoinPoint jp) {
        Object[] signatureArgs = jp.getArgs();
        StringBuilder sb = new StringBuilder();
        sb.append(jp.getTarget().getClass().getSimpleName());
        sb.append(" ");
        sb.append(jp.getSignature().getName());
        sb.append(": ENTRY ");
        int count = 1;
        for (Object signatureArg : signatureArgs) {
            //void myMetod(@ParamName("foo") Object foo, @ParamName("bar") Object bar); using this would have helped
            sb.append("Arg ");
            sb.append(count);
            sb.append(": ");
            sb.append(signatureArg);
            sb.append("; ");
            count++;
        }
        logger.info(sb.toString());
    }

    @Before("allCtrlMethods()")
    public void fillRequestPojo(JoinPoint jp) {
        Object[] signatureArgs = jp.getArgs();
        String className = jp.getTarget().getClass().getSimpleName();
        String methodName = jp.getSignature().getName();
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        if (httpServletRequest != null) {
            httpServletRequest.setAttribute("className", className);
            httpServletRequest.setAttribute("methodName", methodName);
        }
        if (signatureArgs != null && signatureArgs.length > 0) {
            if (signatureArgs[0] instanceof AbstractFrontEndReq) {
                AbstractFrontEndReq request = (AbstractFrontEndReq) signatureArgs[0];
                sessionUtils.fillAbstractFrontEndReq(request);
                //setting ip only when it is not there
                //this condition is to avoid setting the subsytem ip in case of service to service calls
                if (StringUtils.isEmpty(request.getIpAddress())) {
                    if (httpServletRequest != null) {
                        String ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR");
                        if (ipAddress == null) {
                            ipAddress = httpServletRequest.getRemoteAddr();
                        }
                        request.setIpAddress(ipAddress);
                    }
                }
                fillUserAgent(httpServletRequest, request);
            }
        }
    }

    @AfterReturning(pointcut = "allManagerMethods()", returning = "result")
    public void afterReturningExecution(JoinPoint jp, Object result) {
        StringBuilder sb = new StringBuilder();
        sb.append(jp.getTarget().getClass().getSimpleName());
        sb.append(" ");
        sb.append(jp.getSignature().getName());
        sb.append(": RETURN ");
        sb.append(result);
        logger.info(sb.toString());
    }

//    @AfterThrowing(pointcut = "allCtrlMethods()||allManagerMethods()", throwing = "ex")
//    public void afterThrowingExecution(JoinPoint jp, Exception ex) {
//        StringBuilder sb = new StringBuilder();
//        sb.append(jp.getTarget().getClass().getSimpleName());
//        sb.append(" ");
//        sb.append(jp.getSignature().getName());
//        sb.append(": THROWING ");
//        logger.error(sb.toString(), ex);
//    }
    @After("allCtrlMethods() || allManagerMethods()")
    public void afterExecution(JoinPoint jp) {//similar to finally, called after afterThrowingExecution || afterReturningExecution
        StringBuilder sb = new StringBuilder();
        sb.append(jp.getTarget().getClass().getSimpleName());
        sb.append(" ");
        sb.append(jp.getSignature().getName());
        sb.append(": EXIT ");
        logger.info(sb.toString());
    }

    @Around("allDAOMethods() || allListenerMethods()")
    public Object logTimeMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Long reqStartTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        Long timeTaken = System.currentTimeMillis() - reqStartTime;
        String className = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();
        if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
            statsdClient.recordCount(className, methodName, "apicount");
            statsdClient.recordExecutionTime(timeTaken, className, methodName);
        }
        logger.info("responseTime(failed or success) for " + className + " " + methodName + " : " + timeTaken);
        return retVal;
    }

    @Around("allCtrlMethods()")
    public Object logTimeMethodForManager(ProceedingJoinPoint joinPoint) throws Throwable {
        Long reqStartTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        Long timeTaken = System.currentTimeMillis() - reqStartTime;
        String className = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();
        if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
            statsdClient.recordCount(className, methodName, "apicount");
            statsdClient.recordExecutionTime(timeTaken, className, methodName);
        }
        logger.info("responseTime(failed or success) for " + className + " " + methodName + " : " + timeTaken);
        return retVal;
    }

    @Around("allSNSManagerMethods()")
    public Object logTimeMethodForSNSManager(ProceedingJoinPoint joinPoint) throws Throwable {
        Long reqStartTime = System.currentTimeMillis();
        Object retVal = joinPoint.proceed();
        Long timeTaken = System.currentTimeMillis() - reqStartTime;
        String className = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();
        if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
            statsdClient.recordCount(className, methodName, "apicount");
            statsdClient.recordExecutionTime(timeTaken, className, methodName);
        }
        logger.info("responseTime(failed or success) for " + className + " " + methodName + " : " + timeTaken);
        return retVal;
    }

    protected void fillUserAgent(HttpServletRequest httpServletRequest, AbstractFrontEndReq request) {
        if (httpServletRequest != null && StringUtils.isEmpty(request.getUserAgent())) {
            String userAgent = null;
            if (StringUtils.isNotEmpty(httpServletRequest.getHeader("user-agent"))) {
                userAgent = httpServletRequest.getHeader("user-agent");
            } else if (StringUtils.isNotEmpty(httpServletRequest.getHeader("User-Agent"))) {
                userAgent = httpServletRequest.getHeader("User-Agent");
            }
            if (StringUtils.isNotEmpty(userAgent)) {
                request.setUserAgent(userAgent);
            }
        }
    }
    @AfterReturning(pointcut = "allCtrlMethods()", returning = "result")
    public void afterReturningExecutionForCtrlMethods(JoinPoint jp, Object result) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes.getRequest() == null) {
            return;
        }
        //refresh token
        HttpServletRequest request = servletRequestAttributes.getRequest();
        boolean doNotRefreshToken;
        //check in unrestricted urls
        doNotRefreshToken = sessionUtils.isAllowedUrl(request.getRequestURI());
        if (doNotRefreshToken) {
            return;
        }
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getResponse();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.info("authentication after filter " + authentication);
        if (authentication != null && authentication instanceof JwtAuthenticatedProfile) {
            JwtAuthenticatedProfile jwtAuthenticatedProfile = (JwtAuthenticatedProfile) authentication;
            HttpSessionData sessionData = jwtAuthenticatedProfile.getSessionData();
            if (sessionData != null && sessionData.getExpiryTime() != null) {
                Long expiry = sessionData.getExpiryTime();
                logger.info("Expiring at " + new Date(expiry).toString());
                try {
                    sessionUtils.refreshToken(request, response, sessionData);
                    logger.info("exiting from refersh token");
                } catch (URISyntaxException ex) {
                    logger.warn("Error in refresh token ", ex);
                } catch (IOException ex) {
                    logger.warn("Error in refresh token ", ex);
                }
            }
        } else {
            logger.warn(">>>>>>>>>>>>>>>> authentication is not of type JwtAuthenticatedProfile" + authentication
                    + ", class" + (authentication != null ? authentication.getClass() : ""));
        }

    }

}
