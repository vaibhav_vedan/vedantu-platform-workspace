package com.vedantu.util.swagger;

import com.vedantu.util.ConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private boolean environmentSpeficicBooleanFlag = true;

    public SwaggerConfig() {
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (env != null && "PROD".equals(env.toUpperCase())) {
            environmentSpeficicBooleanFlag = false;
        }
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo()).enable(environmentSpeficicBooleanFlag);
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "All subsystems",
                "APIs for front end usage",
                "",
                "",
                "admin@vedantu.com",
                "",
                "");
        return apiInfo;
    }

    /*
        private boolean environmentSpeficicBooleanFlag = true;

    public SwaggerConfig() {
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (env.equals("PROD")) {
            environmentSpeficicBooleanFlag = false;
        }
    }
    
    
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(apiInfo()).enable(environmentSpeficicBooleanFlag);
    }
     */
}
