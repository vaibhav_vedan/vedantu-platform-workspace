package com.vedantu.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SearchJanitorUtils {

	static final Set<String> stopWords = new HashSet<String>(Arrays.asList("a",
			"an", "and", "are", "as", "at", "be", "but", "by", "for", "if",
			"in", "into", "is", "it", "no", "not", "of", "on", "or", "such",
			"that", "the", "their", "then", "there", "these", "they", "this",
			"to", "was", "will", "with"));

	private static final int MIN_GRAM = 3;

	/**
	 * Uses english stemming (snowball + lucene) + stopwords for getting the
	 * words.
	 * 
	 * @param index
	 * @return it only return prefix based tokens (i.e for welcome -> wel, welc,
	 *         welco, welcom, welcome)
	 * 
	 */
	public static Set<String> getTokensForIndexingOrQuery(String input,
			boolean calculateNgrams) {

		String indexCleanedOfHTMLTags = input.replaceAll("\\<.*?,>", " ")
				.trim().toLowerCase();

		Set<String> returnSet = new HashSet<String>();
		returnSet.add(indexCleanedOfHTMLTags);
		String completeStringMatching = "";

		for (String val : indexCleanedOfHTMLTags.split(" ")) {
			if (!val.trim().isEmpty()) {
				val = val.trim();
				completeStringMatching += val + " ";
				returnSet.add(completeStringMatching.trim());
				if (stopWords.contains(val)) {
					continue;
				}
				int length = val.length();
				if (calculateNgrams && length > MIN_GRAM) {
					for (int i = MIN_GRAM; i < length; i++) {
						returnSet.add(val.substring(0, i));
					}
				}
				returnSet.add(val);
			}
		}

		return returnSet;

	}

	public static void main(String[] args) {
		String s = "Welcomee shankar singh";
		System.out.println(getTokensForIndexingOrQuery(s, true));
	}

}
