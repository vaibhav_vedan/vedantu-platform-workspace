package com.vedantu.util;

/**
 * Contains the client IDs and scopes for allowed clients consuming your API.
 */
public class Constants {

	public static final String WEB_CLIENT_ID = "replace this with your web client application ID";
	public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;
	public static final String ANDROID_CLIENT_ID = "replace this with your Android client ID";
	public static final String IOS_CLIENT_ID = "replace this with your iOS client ID";

	// VEDANTU SPECIFIC CONSTANTS
	public static final String ENV_PREFIX = "ENV-";
	public static final String KEY_APPLICATION_MODE = "APPLICATION_MODE";

	// COMMON CONSTANTS
	public static final String COUNTRY_HEADER = "X-AppEngine-Country";
	public static final String COUNTRY_CODE_IN = "IN";
	public static final String IS_DOMESTIC_USER = "isDomesticUser";

	public static final long SLOT_LENGTH = 15 * DateTimeUtils.MILLIS_PER_MINUTE;
	public static final long SESSION_MAXIMUM_DURATION = 10800000l;
	public static final int DEFAULT_ASYNC_BATCH_SIZE = 25;

	// One-to-few
	public static final String WHITEBOARD_DATA_MIGRATED = "WHITEBOARD_DATA_MIGRATED";
	public static final String POST_SESSION_DATA_PROCESSED = "POST_SESSION_DATA_PROCESSED";
	public static final String OTF_FEEDBACK_HANDLED = "OTF_FEEDBACK_HANDLED";

	// DINERO
	public static final String NON_PAYMENT_OF_INSTALMENT_DUES = "NON_PAYMENT_OF_INSTALMENT_DUES";
	public static final int FLOAT_DECIMAL_DIGIT_ACCURACY = 2;
        
	public static final String DEFAULT_CHARSET = "UTF-8";

	public static final String VSAT_EVENT_DETAILS = "VSAT_EVENT_DETAILS";

}
