package com.vedantu.util;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.util.security.EncryptionUtil;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.Assert;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;

public class ConfigUtils {

    public final static ConfigUtils INSTANCE = new ConfigUtils();

    public Properties properties = new Properties();

    private final Gson gson = new Gson();

    public ConfigUtils() {

        System.out.println("Config Utils started");

        loadRootCountryProperties();
        loadRootCommonProperties();
        loadRootApplicationProperties();
        loadRootCommonSecurityProperties(); // utils/resources/security.props
        loadRootApplicationSecurityProperties(); // {service}/resources/security.props

        loadEnvCommonProperties();
        loadEnvApplicationProperties();
        loadEnvCommonSecurityProperties(); // utils/resources/ENV-<env>/security.props
        loadEnvSecurityProperties(); // {service}/resources/ENV-<env>/security.props # only prod configured
    }


    public void updateApplicationPropertiesFromRedis(String properties){
        Type mapType = new TypeToken<HashMap<Object, Object>>() {
        }.getType();
        HashMap<Object, Object> props = gson.fromJson(properties, mapType);
        for (Entry<Object, Object> entry : props.entrySet()) {
            this.properties.put(entry.getKey(), entry.getValue());
        }
    }

    public void reloadEnvSecurityProperties() {
        loadRootCommonSecurityProperties();
        loadRootApplicationSecurityProperties();
        loadEnvCommonSecurityProperties();
        loadEnvSecurityProperties();

    }


    private void loadRootCommonSecurityProperties() {
        System.out.println("LOADING ROOT COMMON SECURITY PROPERTIES");
        try {
            String environment = this.properties.getProperty("environment").toLowerCase();
            if ("true".equalsIgnoreCase(this.properties.getProperty("ENABLE_COMMON_SECRET_STORE"))
            && !"local".equalsIgnoreCase(environment)) {
                loadAWSSecretStore(this.properties.getProperty("COMMON_SECRET_STORE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void loadRootApplicationSecurityProperties() {
        System.out.println("LOADING ROOT APP SECURITY PROPERTIES");
        try {
            String environment = this.properties.getProperty("environment").toLowerCase();
            if ("true".equalsIgnoreCase(this.properties.getProperty("ENABLE_ROOT_SECRET_STORE"))
            && !"local".equalsIgnoreCase(environment)) {
                loadAWSSecretStore(this.properties.getProperty("ROOT_SECRET_STORE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void loadEnvCommonSecurityProperties() {
        System.out.println("LOADING ENV COMMON SECURITY PROPERTIES");
        String environment = this.properties.getProperty("environment").toLowerCase();
        try {
            if ("true".equalsIgnoreCase(this.properties.getProperty("ENABLE_ENV_COMMON_SECRET_STORE"))) {
                loadAWSSecretStore(environment + "/" + this.properties.getProperty("COMMON_SECRET_STORE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void loadRootCommonProperties() {
        // String confDir = "classpath:" + applicationPropertiesFilePath;
        System.out.println("LOADING ROOT COMMON PROPS");
        try {
            Resource commonResources = new ClassPathResource("/commons.properties");
            updateProperties(commonResources);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void loadRootApplicationProperties() {
        // String confDir = "classpath:" + applicationPropertiesFilePath;
        System.out.println("LOADING ROOT APP PROPS");
        try {
            Resource resource = new ClassPathResource("/application.properties");
            updateProperties(resource);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void loadRootCountryProperties() {
        // String confDir = "classpath:" + applicationPropertiesFilePath;
        System.out.println("LOADING ROOT COUNTRY PROPS");
        try {
            Resource resource = new ClassPathResource("/countrycode.properties");
            updateProperties(resource);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void updateProperties(Resource resource) throws IOException {
        Properties props = PropertiesLoaderUtils.loadProperties(resource);
        for (Entry<Object, Object> entry : props.entrySet()) {
            this.properties.put(entry.getKey(), entry.getValue());
        }
//        System.out.println("PROPS" + props);
    }

    private void loadEnvCommonProperties() {
        System.out.println("LOADING ENV COMMON PROPS");

        String confDir = "/ENV-" + this.properties.getProperty("environment");
        try {
            final String commonPropsPropertiesFilePath = confDir + java.io.File.separator + "commons.properties";
            Resource resource = new ClassPathResource(commonPropsPropertiesFilePath);
            updateProperties(resource);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void loadEnvApplicationProperties() {
        System.out.println("LOADING ENV APP PROPS");

        String confDir = "/ENV-" + this.properties.getProperty("environment");
        try {
            final String applicationPropPath = confDir + java.io.File.separator + "application.properties";
            Resource resource = new ClassPathResource(applicationPropPath);
            updateProperties(resource);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }

    }

    private void loadEnvSecurityProperties() {
        System.out.println("LOADING ENV SECURITY PROPS");

        String confDir = "ENV-" + this.properties.getProperty("environment");
        String securityEnvPropPath = confDir + java.io.File.separator + "security.properties";
        try (InputStream inner = ConfigUtils.class.getClassLoader().getResourceAsStream(securityEnvPropPath)) {
            loadOrOverrideFromStream(inner);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        if (this.properties.getProperty("ENABLE_SECRET_STORE") != null && this.properties.getProperty("ENABLE_SECRET_STORE").equals("true")) {
            String environment = this.properties.getProperty("environment").toLowerCase();
            loadAWSSecretStore(environment + "/" + this.properties.getProperty("SECRET_STORE"));
        }
    }

    private void loadAWSSecretStore(String secretName) {

        System.out.println("Loading Secrets from AWS");
        //Converting env to lowercase, as I dont want to migrate the Secret stores.
        String region = "ap-south-1";

        // Create a Secrets Manager client
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .build();

        // In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        // See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        // We rethrow the exception by default.
        String secret, decodedBinarySecret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        }

        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        try {

            if (getSecretValueResult.getSecretString() != null) {
                secret = getSecretValueResult.getSecretString();
                addSecretsToProperties(secret);
            } else {
                decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
                addSecretsToProperties(decodedBinarySecret);
            }
        } catch (JSONException e) {
            //Swallow for now
        }
    }

    public void addSecretsToProperties(String t) throws JSONException {
//        System.out.println("PROPS JSON " + t);
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = jObject.getString(key);

            this.properties.put(key, value);

        }
    }

    private void loadOrOverrideFromStream(InputStream inputStream) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        if (inputStream != null) {
            Properties properties = new Properties();
            properties.load(inputStream);
            for (Entry<Object, Object> entry : properties.entrySet()) {
                Object value = entry.getValue();
                if (value != null) {
                    byte[] bytes = null;
                    try {
                        bytes = DatatypeConverter.parseHexBinary(value.toString());
                    } catch (Exception ignored) {
                    }
                    if (bytes != null) {
                        value = EncryptionUtil.decrypt(bytes);
                    }
                }

                this.properties.put(entry.getKey(), value);
            }
        }
    }

    public String getStringValue(String key) {

        String value = this.properties.getProperty(key);
        return value == null ? value : value.trim();
    }

    public String getStringValue(String key, String defaultVal) {
        String value = getStringValue(key);
        if (StringUtils.isNotEmpty(value)) {
            return value;
        } else {
            return defaultVal;
        }
    }

    public int getIntValue(String key) {

        return Integer.parseInt(properties.getProperty(key).trim());
    }

    public int getIntValue(String key, int defaultVal) {
        if (StringUtils.isEmpty(properties.getProperty(key))) {
            return defaultVal;
        }
        return Integer.parseInt(properties.getProperty(key).trim());
    }

    public long getLongValue(String key) {

        return Long.parseLong(properties.getProperty(key).trim());
    }

    public long getLongValue(String key, long defaultVal) {
        if (StringUtils.isEmpty(properties.getProperty(key))) {
            return defaultVal;
        }
        return Long.parseLong(properties.getProperty(key).trim());
    }

    public boolean getBooleanValue(String key) {

        return properties.getProperty(key) == null ? false : Boolean.parseBoolean(properties.getProperty(key).trim());
    }

    public float getFloatValue(String key) {

        return Float.parseFloat(properties.getProperty(key).trim());
    }

    public float getFloatValue(String key, float defaultVal) {
        if (StringUtils.isEmpty(properties.getProperty(key))) {
            return defaultVal;
        }
        return Float.parseFloat(properties.getProperty(key).trim());
    }

    public String getEnvironmentSlug() {
        return properties.getProperty("environment").toLowerCase();
    }

    public String getPlatformEndpoint() {
        Assert.hasText(properties.getProperty("PLATFORM_ENDPOINT"), "PLATFORM_ENDPOINT Property does not exist");
        return properties.getProperty("PLATFORM_ENDPOINT");
    }
    public String getSchedulingEndpoint() {
        Assert.hasText(properties.getProperty("SCHEDULING_ENDPOINT"), "SCHEDULING_ENDPOINT Property does not exist");
        return properties.getProperty("SCHEDULING_ENDPOINT");
    }
    public String getDineroEndpoint() {
        Assert.hasText(properties.getProperty("DINERO_ENDPOINT"), "DINERO_ENDPOINT Property does not exist");
        return properties.getProperty("DINERO_ENDPOINT");
    }
    public String getUserEndpoint() {
        Assert.hasText(properties.getProperty("USER_ENDPOINT"), "USER_ENDPOINT Property does not exist");
        return properties.getProperty("USER_ENDPOINT");
    }
    public String getLmsEndpoint() {
        Assert.hasText(properties.getProperty("LMS_ENDPOINT"), "LMS_ENDPOINT Property does not exist");
        return properties.getProperty("LMS_ENDPOINT");
    }
    public String getLoamEndpoint() {
        Assert.hasText(properties.getProperty("LOAM_ENDPOINT"), "LOAM_ENDPOINT Property does not exist");
        return properties.getProperty("LOAM_ENDPOINT");
    }
    public String getNotificationEndpoint() {
        Assert.hasText(properties.getProperty("NOTIFICATION_ENDPOINT"), "NOTIFICATION_ENDPOINT Property does not exist");
        return properties.getProperty("NOTIFICATION_ENDPOINT");
    }
    public String getSubscriptionEndpoint() {
        Assert.hasText(properties.getProperty("SUBSCRIPTION_ENDPOINT"), "SUBSCRIPTION_ENDPOINT Property does not exist");
        return properties.getProperty("SUBSCRIPTION_ENDPOINT");
    }
    public String getIslEndpoint() {
        Assert.hasText(properties.getProperty("ISL_ENDPOINT"), "ISL_ENDPOINT Property does not exist");
        return properties.getProperty("ISL_ENDPOINT");
    }
    public String getGrowthEndpoint() {
        String endpoint = properties.getProperty("GROWTH_ENDPOINT");
        Assert.hasText(endpoint, "GROWTH_ENDPOINT Property does not exist");
        return endpoint.trim().endsWith("/") ? endpoint.trim().substring(0, endpoint.lastIndexOf("/")) : endpoint.trim();
    }
    public String getWaveSessionEndpoint() {
        String endpoint = properties.getProperty("FOS_SESSION_ENDPOINT");
        Assert.hasText(endpoint, "FOS_SESSION_ENDPOINT Property does not exist");
        return endpoint.trim().endsWith("/") ? endpoint.trim().substring(0, endpoint.lastIndexOf("/")) : endpoint.trim();
    }

    public String getServiceName() {
        Assert.hasText(properties.getProperty("SERVICE_NAME"), "SERVICE_NAME Property does not exist");
        return properties.getProperty("SERVICE_NAME");
    }

}
