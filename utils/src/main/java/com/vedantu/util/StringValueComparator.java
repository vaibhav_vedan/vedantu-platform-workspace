package com.vedantu.util;

import java.util.Comparator;

import com.vedantu.util.enums.IValueComparable;

public class StringValueComparator implements Comparator<IValueComparable> {

		@Override
		public int compare(IValueComparable o1, IValueComparable o2) {

			return o1 != null && o2 != null ? o1._getComparableValue().compareTo(
					o2._getComparableValue()) : 0;
		}

}
