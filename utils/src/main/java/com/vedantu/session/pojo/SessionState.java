package com.vedantu.session.pojo;

public enum SessionState {
    SCHEDULED {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_SCHEDULED;
        }

    }, CANCELED {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_CANCELED;
        }
    }, STARTED {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_STARTED;
        }
    }, ACTIVE {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_ACTIVE;
        }
    }, ENDED {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_ENDED;
        }
    }, EXPIRED {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_EXPIRED;
        }
    }, FORFEITED {
        @Override
        public SessionEvents getSessionEvent() {
            return SessionEvents.SESSION_FORFEITED;
        }
    };

    public abstract SessionEvents getSessionEvent();
}
