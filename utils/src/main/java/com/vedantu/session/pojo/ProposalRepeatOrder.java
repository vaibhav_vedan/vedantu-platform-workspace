package com.vedantu.session.pojo;

public enum ProposalRepeatOrder {
	ONCE, DAILY, WEEKLY
}
