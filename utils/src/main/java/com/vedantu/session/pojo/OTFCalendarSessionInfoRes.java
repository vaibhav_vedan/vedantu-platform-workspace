/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.session.pojo;

import com.vedantu.util.fos.response.AbstractListRes;
import java.util.List;

/**
 *
 * @author ajith
 */
public class OTFCalendarSessionInfoRes extends AbstractListRes<OTFCalendarSessionInfo>{

    public OTFCalendarSessionInfoRes() {
    }

    public OTFCalendarSessionInfoRes(List<OTFCalendarSessionInfo> list, int count) {
        super(list, count);
    }
    
}
