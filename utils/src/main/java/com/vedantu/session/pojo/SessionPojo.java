package com.vedantu.session.pojo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.scheduling.pojo.session.OTMSessionType;
import org.springframework.util.StringUtils;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.util.Constants;

public class SessionPojo {
	private String id;
	private String batchId;
	private Long boardId;
	private String title;
	private String description;
	private String presenter;
	private Long startTime;
	private Long endTime;
	private String sessionURL;
	private String presenterURL;
	private String replayVideoURL;
	private List<String> replayUrls;
	private boolean isRescheduled;
	private com.vedantu.onetofew.enums.SessionState state;
	private OTMSessionType otmSessionType;
	private String rescheduledFromId;
	private List<String> attendees;
	private List<OTFSessionAttendeeInfo> attendeeInfos;
	private OTFCourseBasicInfo courseInfo;
	private String organizerAccessToken;
	private UserBasicInfo presenterInfo;
	private com.vedantu.onetofew.enums.EntityType type;
	private String batchState;
	private String meetingId;
	private String presenterUrl;
	private String remarks;
	private Boolean teacherJoined;
	private Long creationTime;
	private String createdBy;
	private Long lastUpdated;
	private String callingUserId;
	private OTFSessionToolType sessionToolType;
	private OTFSessionContextType sessionContextType;
	private String sessionContextId;
	private Set<String> flags = new HashSet<>();
	private RemarkResp previousSessionRemark;
	private String previousPresenter;
        private String vimeoId;
        private List<ContentInfo> preSessionContents;
        private List<ContentInfo> postSessionContents;
        private String ownerId;
        private String ownerComment;

	public SessionPojo() {
		super();
	}

	public String getRescheduledFromId() {
		return rescheduledFromId;
	}

	public void setRescheduledFromId(String rescheduledFromId) {
		this.rescheduledFromId = rescheduledFromId;
	}

	public OTMSessionType getOtmSessionType() {
		return otmSessionType;
	}

	public void setOtmSessionType(OTMSessionType otmSessionType) {
		this.otmSessionType = otmSessionType;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPresenter() {
		return presenter;
	}

	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getSessionURL() {
		return sessionURL;
	}

	public void setSessionURL(String sessionURL) {
		this.sessionURL = sessionURL;
	}

	public String getReplayVideoURL() {
		return replayVideoURL;
	}

	public void setReplayVideoURL(String replayVideoURL) {
		this.replayVideoURL = replayVideoURL;
	}

	public boolean isRescheduled() {
		return isRescheduled;
	}

	public void setRescheduled(boolean isRescheduled) {
		this.isRescheduled = isRescheduled;
	}

	public com.vedantu.onetofew.enums.SessionState getState() {
		return state;
	}

	public void setState(com.vedantu.onetofew.enums.SessionState state) {
		this.state = state;
	}

	public List<String> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<String> attendees) {
		this.attendees = attendees;
	}

	public String getPresenterURL() {
		return presenterURL;
	}

	public void setPresenterURL(String presenterURL) {
		this.presenterURL = presenterURL;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public OTFCourseBasicInfo getCourseInfo() {
		return courseInfo;
	}

	public void setCourseInfo(OTFCourseBasicInfo courseInfo) {
		this.courseInfo = courseInfo;
	}

	public String getOrganizerAccessToken() {
		return organizerAccessToken;
	}

	public void setOrganizerAccessToken(String organizerAccessToken) {
		this.organizerAccessToken = organizerAccessToken;
	}

	public UserBasicInfo getPresenterInfo() {
		return presenterInfo;
	}

	public void setPresenterInfo(UserBasicInfo presenterInfo) {
		this.presenterInfo = presenterInfo;
	}

	public com.vedantu.onetofew.enums.EntityType getType() {
		return type;
	}

	public void setType(com.vedantu.onetofew.enums.EntityType type) {
		this.type = type;
	}

	public SessionSlot createSessionSlot() {
		return new SessionSlot(startTime, endTime);
	}

	public String getBatchState() {
		return batchState;
	}

	public void setBatchState(String batchState) {
		this.batchState = batchState;
	}

	public boolean isIsRescheduled() {
		return isRescheduled;
	}

	public void setIsRescheduled(boolean isRescheduled) {
		this.isRescheduled = isRescheduled;
	}

	public String getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}

	public String getPresenterUrl() {
		return presenterUrl;
	}

	public void setPresenterUrl(String presenterUrl) {
		this.presenterUrl = presenterUrl;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Boolean getTeacherJoined() {
		return teacherJoined;
	}

	public void setTeacherJoined(Boolean teacherJoined) {
		this.teacherJoined = teacherJoined;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCallingUserId() {
		return callingUserId;
	}

	public void setCallingUserId(String callingUserId) {
		this.callingUserId = callingUserId;
	}

	public OTFSessionToolType getSessionToolType() {
		return sessionToolType;
	}

	public void setSessionToolType(OTFSessionToolType sessionToolType) {
		this.sessionToolType = sessionToolType;
	}

	public List<OTFSessionAttendeeInfo> getAttendeeInfos() {
		return attendeeInfos;
	}

	public void setAttendeeInfos(List<OTFSessionAttendeeInfo> attendeeInfos) {
		this.attendeeInfos = attendeeInfos;
	}

	public List<String> getReplayUrls() {
		return replayUrls;
	}

	public void setReplayUrls(List<String> replayUrls) {
		this.replayUrls = replayUrls;
	}

	public void updateUserBasicInfo(UserBasicInfo userBasicInfo) {
		for (int i = 0; i < this.attendeeInfos.size(); i++) {
			OTFSessionAttendeeInfo sessionAttendeeInfo = this.attendeeInfos.get(i);
			if (sessionAttendeeInfo.getUserId() != null && sessionAttendeeInfo.getUserId() > 0l
					&& sessionAttendeeInfo.getUserId().equals(userBasicInfo.getUserId())) {
				sessionAttendeeInfo.updateUserDetails(userBasicInfo);
				this.attendeeInfos.set(i, sessionAttendeeInfo);
				return;
			}
		}

		// Create a new attendee
		OTFSessionAttendeeInfo otfSessionAttendeeInfo = new OTFSessionAttendeeInfo(userBasicInfo.getUserId());
		otfSessionAttendeeInfo.updateUserDetails(userBasicInfo);
		this.attendeeInfos.add(otfSessionAttendeeInfo);
	}

	public boolean containsWhiteboardDataMigrated() {
		return flags.contains(Constants.WHITEBOARD_DATA_MIGRATED);
	}

	public void addWhiteboardDataMigrated(boolean whiteboardDataMigrated) {
		if (whiteboardDataMigrated) {
			flags.add(Constants.WHITEBOARD_DATA_MIGRATED);
		} else {
			flags.remove(Constants.WHITEBOARD_DATA_MIGRATED);
		}
	}

	public Set<String> getFlags() {
		return flags;
	}

	public void setFlags(Set<String> flags) {
		this.flags = flags;
	}

	public boolean containsFlag(String flag) {
		return !(StringUtils.isEmpty(flag) || !this.flags.contains(flag));
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public OTFSessionContextType getSessionContextType() {
		return sessionContextType;
	}

	public void setSessionContextType(OTFSessionContextType sessionContextType) {
		this.sessionContextType = sessionContextType;
	}

	public String getSessionContextId() {
		return sessionContextId;
	}

	public void setSessionContextId(String sessionContextId) {
		this.sessionContextId = sessionContextId;
	}

	public RemarkResp getPreviousSessionRemark() {
		return previousSessionRemark;
	}

	public void setPreviousSessionRemark(RemarkResp previousSessionRemark) {
		this.previousSessionRemark = previousSessionRemark;
	}

	public String getPreviousPresenter() {
		return previousPresenter;
	}

	public void setPreviousPresenter(String previousPresenter) {
		this.previousPresenter = previousPresenter;
	}

        public String getVimeoId() {
            return vimeoId;
        }

        public void setVimeoId(String vimeoId) {
            this.vimeoId = vimeoId;
        }
        
		public String getOwnerId() {
			return ownerId;
		}

		public void setOwnerId(String ownerId) {
			this.ownerId = ownerId;
		}

		public String getOwnerComment() {
			return ownerComment;
		}

		public void setOwnerComment(String ownerComment) {
			this.ownerComment = ownerComment;
		}

	public List<ContentInfo> getPreSessionContents() {
			return preSessionContents;
	}

	public void setPreSessionContents(List<ContentInfo> preSessionContents) {
			this.preSessionContents = preSessionContents;
	}

	public List<ContentInfo> getPostSessionContents() {
			return postSessionContents;
	}

	public void setPostSessionContents(List<ContentInfo> postSessionContents) {
			this.postSessionContents = postSessionContents;
	}


	@Override
	public String toString() {
		return "SessionPojo [id=" + id + ", batchId=" + batchId + ", boardId=" + boardId + ", title=" + title
				+ ", description=" + description + ", presenter=" + presenter + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", sessionURL=" + sessionURL + ", presenterURL=" + presenterURL
				+ ", replayVideoURL=" + replayVideoURL + ", replayUrls=" + replayUrls + ", isRescheduled="
				+ isRescheduled + ", state=" + state + ", attendees=" + attendees + ", attendeeInfos=" + attendeeInfos
				+ ", courseInfo=" + courseInfo + ", organizerAccessToken=" + organizerAccessToken + ", presenterInfo="
				+ presenterInfo + ", type=" + type + ", batchState=" + batchState + ", meetingId=" + meetingId
				+ ", presenterUrl=" + presenterUrl + ", remarks=" + remarks + ", teacherJoined=" + teacherJoined
				+ ", creationTime=" + creationTime + ", createdBy=" + createdBy + ", lastUpdated=" + lastUpdated
				+ ", callingUserId=" + callingUserId + ", sessionToolType=" + sessionToolType + ", sessionContextType="
				+ sessionContextType + ", sessionContextId=" + sessionContextId + ", flags=" + flags + ", vimeoId=" + vimeoId + ", toString()="
				+ super.toString() + ", otmSessionType="+ otmSessionType +"]";
	}
}
