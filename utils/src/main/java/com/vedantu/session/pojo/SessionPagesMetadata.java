/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.session.pojo;

/**
 *
 * @author ajith
 */
public class SessionPagesMetadata {

    //label:"Slide 1", seqNo:54,hidden:true/false,slideID:
    private String label;
    private Integer seqNo;
    private boolean hidden;
    private String slideID;
    private boolean selected;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getSlideID() {
        return slideID;
    }

    public void setSlideID(String slideID) {
        this.slideID = slideID;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "SessionPagesMetadata{" + "label=" + label + ", seqNo=" + seqNo + ", hidden=" + hidden + ", slideID=" + slideID + ", selected=" + selected + '}';
    }
    
}
