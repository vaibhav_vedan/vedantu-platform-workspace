package com.vedantu.session.pojo;

import java.util.List;

import com.vedantu.User.UserBasicInfo;

public class OTFCourseBasicInfo {
	private String id;
	private String title;
	private Long launchDate;
	private List<String> contents;
	private Long duration;
	private List<UserBasicInfo> teachers;

	// Stored in bits, each with 30 min duration.
	private String batchTimings;
	private String curriculum;
	private String targetAudience;
	private VisibilityState state;
	private String faq;
	private List<String> grades;
	private int liveBatches;
	private int upcomingBatches;
	private List<String> subjects;
	private List<String> targets;
	private int priority;

	// grades, subjects, targets, course title will be added and removed by
	// default
	private List<String> tags;
	private String demoVideoUrl;
	private Long exitDate;
	private int startPrice;

	public OTFCourseBasicInfo() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getLaunchDate() {
		return launchDate;
	}

	public void setLaunchDate(Long launchDate) {
		this.launchDate = launchDate;
	}

	public List<String> getContents() {
		return contents;
	}

	public void setContents(List<String> contents) {
		this.contents = contents;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public List<UserBasicInfo> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<UserBasicInfo> teachers) {
		this.teachers = teachers;
	}

	public String getBatchTimings() {
		return batchTimings;
	}

	public void setBatchTimings(String batchTimings) {
		this.batchTimings = batchTimings;
	}

	public String getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(String curriculum) {
		this.curriculum = curriculum;
	}

	public String getTargetAudience() {
		return targetAudience;
	}

	public void setTargetAudience(String targetAudience) {
		this.targetAudience = targetAudience;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public String getFaq() {
		return faq;
	}

	public void setFaq(String faq) {
		this.faq = faq;
	}

	public List<String> getGrades() {
		return grades;
	}

	public void setGrades(List<String> grades) {
		this.grades = grades;
	}

	public int getLiveBatches() {
		return liveBatches;
	}

	public void setLiveBatches(int liveBatches) {
		this.liveBatches = liveBatches;
	}

	public int getUpcomingBatches() {
		return upcomingBatches;
	}

	public void setUpcomingBatches(int upcomingBatches) {
		this.upcomingBatches = upcomingBatches;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getTargets() {
		return targets;
	}

	public void setTargets(List<String> targets) {
		this.targets = targets;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getDemoVideoUrl() {
		return demoVideoUrl;
	}

	public void setDemoVideoUrl(String demoVideoUrl) {
		this.demoVideoUrl = demoVideoUrl;
	}

	public Long getExitDate() {
		return exitDate;
	}

	public void setExitDate(Long exitDate) {
		this.exitDate = exitDate;
	}

	public int getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(int startPrice) {
		this.startPrice = startPrice;
	}

	@Override
	public String toString() {
		return "OTFCourseBasicInfo [id=" + id + ", title=" + title + ", launchDate=" + launchDate + ", contents="
				+ contents + ", duration=" + duration + ", teachers=" + teachers + ", batchTimings=" + batchTimings
				+ ", curriculum=" + curriculum + ", targetAudience=" + targetAudience + ", state=" + state + ", faq="
				+ faq + ", grades=" + grades + ", liveBatches=" + liveBatches + ", upcomingBatches=" + upcomingBatches
				+ ", subjects=" + subjects + ", targets=" + targets + ", priority=" + priority + ", tags=" + tags
				+ ", demoVideoUrl=" + demoVideoUrl + ", exitDate=" + exitDate + ", startPrice=" + startPrice
				+ ", toString()=" + super.toString() + "]";
	}
}
