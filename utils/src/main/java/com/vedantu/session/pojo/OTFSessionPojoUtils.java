package com.vedantu.session.pojo;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.util.fos.request.AbstractReq;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class OTFSessionPojoUtils extends AbstractReq {

    private String id;
    private Set<String> batchIds;
    private Long boardId;
    private String title;
    private String description;
    private String presenter;
    private Set<Long> taIds;
    private Long startTime;
    private Long endTime;
    private String sessionURL;
    private String presenterURL;
    private String replayVideoURL;
    private List<String> replayUrls;
    private boolean isRescheduled;
    private com.vedantu.onetofew.enums.SessionState state;
    private OTMSessionType otmSessionType;
    private String rescheduledFromId;
    private List<String> attendees;
    private List<OTFSessionAttendeeInfo> attendeeInfos;
    private List<OTFCourseBasicInfo> courseInfos;
    private List<RescheduleData> rescheduleData;
    private String organizerAccessToken;
    private UserBasicInfo presenterInfo;
    private com.vedantu.onetofew.enums.EntityType type;
    private String meetingId;
    private String presenterUrl;
    private String remarks;
    private Boolean teacherJoined;
    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String callingUserId;
    private OTFSessionToolType sessionToolType;
    private OTFSessionContextType sessionContextType;
    private String sessionContextId;
    private Set<OTFSessionFlag> flags = new HashSet<>();
    private RemarkResp previousSessionRemark;
    private String previousPresenter;
    private String vimeoId;
    private String agoraVimeoId;
    private List<ContentInfo> preSessionContents;
    private List<ContentInfo> postSessionContents;
    private String ownerId;
    private String ownerComment;
    private String adminJoined;
    private String techSupport;
    private String finalIssueStatus;
    private List<String> curricullumTopics;
    private List<String> baseTreeTopics;
    private  String primaryBatchId;
    private String parentSessionId;
    private List<String> groupNames;
    private Set<SessionLabel> labels;
    private Set<EntityTag> entityTags;


    /**
     * Any chages made here needs to be added in OTFSession#hasVedantuWaveFeatures
     * @return
     */
    public boolean hasVedantuWaveFeatures() {
        return OTFSessionToolType.VEDANTU_WAVE.equals(this.getSessionToolType()) ||
                OTFSessionToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(this.getSessionToolType()) ||
                this.getEntityTags().contains(EntityTag.BIG_WHITE_BOARD) ||
                OTFSessionToolType.VEDANTU_WAVE_OTO.equals(this.getSessionToolType()) ||
                EntityType.ONE_TO_ONE == this.getType();
    }



    public List<String> getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(List<String> groupNames) {
        this.groupNames = groupNames;
    }

    public String getParentSessionId() {
        return parentSessionId;
    }

    public void setParentSessionId(String parentSessionId) {
        this.parentSessionId = parentSessionId;
    }

    public String getPrimaryBatchId() {
        return primaryBatchId;
    }

    public void setPrimaryBatchId(String primaryBatchId) {
        this.primaryBatchId = primaryBatchId;
    }

    public OTFSessionPojoUtils() {
        super();
    }

    public String getRescheduledFromId() {
        return rescheduledFromId;
    }

    public void setRescheduledFromId(String rescheduledFromId) {
        this.rescheduledFromId = rescheduledFromId;
    }

    public OTMSessionType getOtmSessionType() {
        return otmSessionType;
    }

    public void setOtmSessionType(OTMSessionType otmSessionType) {
        this.otmSessionType = otmSessionType;
    }

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPresenter() {
        return presenter;
    }

    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = taIds;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getSessionURL() {
        return sessionURL;
    }

    public void setSessionURL(String sessionURL) {
        this.sessionURL = sessionURL;
    }

    public String getReplayVideoURL() {
        return replayVideoURL;
    }

    public void setReplayVideoURL(String replayVideoURL) {
        this.replayVideoURL = replayVideoURL;
    }

    public boolean isRescheduled() {
        return isRescheduled;
    }

    public void setRescheduled(boolean isRescheduled) {
        this.isRescheduled = isRescheduled;
    }

    public com.vedantu.onetofew.enums.SessionState getState() {
        return state;
    }

    public void setState(com.vedantu.onetofew.enums.SessionState state) {
        this.state = state;
    }

    public List<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<String> attendees) {
        this.attendees = attendees;
    }

    public String getPresenterURL() {
        return presenterURL;
    }

    public void setPresenterURL(String presenterURL) {
        this.presenterURL = presenterURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<OTFCourseBasicInfo> getCourseInfos() {
        return courseInfos;
    }

    public void setCourseInfos(List<OTFCourseBasicInfo> courseInfos) {
        this.courseInfos = courseInfos;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public UserBasicInfo getPresenterInfo() {
        return presenterInfo;
    }

    public void setPresenterInfo(UserBasicInfo presenterInfo) {
        this.presenterInfo = presenterInfo;
    }

    public com.vedantu.onetofew.enums.EntityType getType() {
        return type;
    }

    public void setType(com.vedantu.onetofew.enums.EntityType type) {
        this.type = type;
    }

    public SessionSlot createSessionSlot() {
        return new SessionSlot(startTime, endTime);
    }

    public boolean isIsRescheduled() {
        return isRescheduled;
    }

    public void setIsRescheduled(boolean isRescheduled) {
        this.isRescheduled = isRescheduled;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public String getPresenterUrl() {
        return presenterUrl;
    }

    public void setPresenterUrl(String presenterUrl) {
        this.presenterUrl = presenterUrl;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean getTeacherJoined() {
        return teacherJoined;
    }

    public void setTeacherJoined(Boolean teacherJoined) {
        this.teacherJoined = teacherJoined;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCallingUserId() {
        return callingUserId;
    }

    public void setCallingUserId(String callingUserId) {
        this.callingUserId = callingUserId;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public List<OTFSessionAttendeeInfo> getAttendeeInfos() {
        return attendeeInfos;
    }

    public void setAttendeeInfos(List<OTFSessionAttendeeInfo> attendeeInfos) {
        this.attendeeInfos = attendeeInfos;
    }

    public List<String> getReplayUrls() {
        return replayUrls;
    }

    public void setReplayUrls(List<String> replayUrls) {
        this.replayUrls = replayUrls;
    }

    public void updateUserBasicInfo(UserBasicInfo userBasicInfo) {
        for (int i = 0; i < this.attendeeInfos.size(); i++) {
            OTFSessionAttendeeInfo sessionAttendeeInfo = this.attendeeInfos.get(i);
            if (sessionAttendeeInfo.getUserId() != null && sessionAttendeeInfo.getUserId() > 0l
                    && sessionAttendeeInfo.getUserId().equals(userBasicInfo.getUserId())) {
                sessionAttendeeInfo.updateUserDetails(userBasicInfo);
                this.attendeeInfos.set(i, sessionAttendeeInfo);
                return;
            }
        }

        // Create a new attendee
        OTFSessionAttendeeInfo otfSessionAttendeeInfo = new OTFSessionAttendeeInfo(userBasicInfo.getUserId());
        otfSessionAttendeeInfo.updateUserDetails(userBasicInfo);
        if (Role.STUDENT.equals(otfSessionAttendeeInfo.getRole())) {
            this.attendeeInfos.add(otfSessionAttendeeInfo);
        }
    }

    public boolean containsWhiteboardDataMigrated() {
        return flags.contains(OTFSessionFlag.WHITEBOARD_DATA_MIGRATED);
    }

    public void addWhiteboardDataMigrated(boolean whiteboardDataMigrated) {
        if (whiteboardDataMigrated) {
            flags.add(OTFSessionFlag.WHITEBOARD_DATA_MIGRATED);
        } else {
            flags.remove(OTFSessionFlag.WHITEBOARD_DATA_MIGRATED);
        }
    }

    public Set<OTFSessionFlag> getFlags() {
        return flags;
    }

    public void setFlags(Set<OTFSessionFlag> flags) {
        this.flags = flags;
    }

    public boolean containsFlag(OTFSessionFlag flag) {
        return this.flags.contains(flag);
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public OTFSessionContextType getSessionContextType() {
        return sessionContextType;
    }

    public void setSessionContextType(OTFSessionContextType sessionContextType) {
        this.sessionContextType = sessionContextType;
    }

    public String getSessionContextId() {
        return sessionContextId;
    }

    public void setSessionContextId(String sessionContextId) {
        this.sessionContextId = sessionContextId;
    }

    public RemarkResp getPreviousSessionRemark() {
        return previousSessionRemark;
    }

    public void setPreviousSessionRemark(RemarkResp previousSessionRemark) {
        this.previousSessionRemark = previousSessionRemark;
    }

    public String getPreviousPresenter() {
        return previousPresenter;
    }

    public void setPreviousPresenter(String previousPresenter) {
        this.previousPresenter = previousPresenter;
    }

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public String getAgoraVimeoId() {
        return agoraVimeoId;
    }

    public void setAgoraVimeoId(String agoraVimeoId) {
        this.agoraVimeoId = agoraVimeoId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerComment() {
        return ownerComment;
    }

    public void setOwnerComment(String ownerComment) {
        this.ownerComment = ownerComment;
    }

    public List<ContentInfo> getPreSessionContents() {
        return preSessionContents;
    }

    public void setPreSessionContents(List<ContentInfo> preSessionContents) {
        this.preSessionContents = preSessionContents;
    }

    public List<ContentInfo> getPostSessionContents() {
        return postSessionContents;
    }

    public void setPostSessionContents(List<ContentInfo> postSessionContents) {
        this.postSessionContents = postSessionContents;
    }

    /**
     * @return the adminJoined
     */
    public String getAdminJoined() {
        return adminJoined;
    }

    /**
     * @param adminJoined the adminJoined to set
     */
    public void setAdminJoined(String adminJoined) {
        this.adminJoined = adminJoined;
    }

    /**
     * @return the techSupport
     */
    public String getTechSupport() {
        return techSupport;
    }

    /**
     * @param techSupport the techSupport to set
     */
    public void setTechSupport(String techSupport) {
        this.techSupport = techSupport;
    }

    /**
     * @return the finalIssueStatus
     */
    public String getFinalIssueStatus() {
        return finalIssueStatus;
    }

    /**
     * @param finalIssueStatus the finalIssueStatus to set
     */
    public void setFinalIssueStatus(String finalIssueStatus) {
        this.finalIssueStatus = finalIssueStatus;
    }

    /**
     * @return the rescheduleData
     */
    public List<RescheduleData> getRescheduleData() {
        return rescheduleData;
    }

    /**
     * @param rescheduleData the rescheduleData to set
     */
    public void setRescheduleData(List<RescheduleData> rescheduleData) {
        this.rescheduleData = rescheduleData;
    }

    public List<String> getCurricullumTopics() {
        return curricullumTopics;
    }

    public void setCurricullumTopics(List<String> curricullumTopics) {
        this.curricullumTopics = curricullumTopics;
    }

    public List<String> getBaseTreeTopics() {
        return baseTreeTopics;
    }

    public void setBaseTreeTopics(List<String> baseTreeTopics) {
        this.baseTreeTopics = baseTreeTopics;
    }

    public Set<SessionLabel> getLabels() {
        return labels;
    }

    public void setLabels(Set<SessionLabel> labels) {
        this.labels = labels;
    }



}
