package com.vedantu.session.pojo.proposal;

import java.io.Serializable;

import com.vedantu.util.Constants;

public class ProposalSlotBasic implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long startTime;
	private Long endTime;
	private String title;

	// proposalSlot.window.time.millis=300000
	public static final long PROPOSAL_SLOT_WINDOW = 300000;

	public ProposalSlotBasic() {
		super();
	}

	public ProposalSlotBasic(Long startTime, Long endTime, String title) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.title = title;
	}

	public ProposalSlotBasic(ProposalSlot proposalSlot) {
		super();
		this.startTime = proposalSlot.getStartTime();
		this.endTime = proposalSlot.getEndTime();
		this.title = proposalSlot.getTitle();
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean validate() {
		if (startTime == null || startTime <= 0 || endTime == null || endTime <= 0 || startTime > endTime) {
			return false;
		} else if (startTime < (System.currentTimeMillis() + PROPOSAL_SLOT_WINDOW)) {
			return false;
		} else if ((endTime - startTime) > Constants.SESSION_MAXIMUM_DURATION) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "ProposalSlotBasic [startTime=" + startTime + ", endTime=" + endTime + ", title=" + title + "]";
	}
}
