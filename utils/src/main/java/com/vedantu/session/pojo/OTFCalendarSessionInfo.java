package com.vedantu.session.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.EntityTag;
import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.util.fos.response.AbstractRes;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class OTFCalendarSessionInfo extends AbstractRes {

    private String subject;
    private Long boardId;
    private String title;
    private String description;
    private Long startTime;
    private Long endTime;
    private com.vedantu.onetofew.enums.SessionState state;
    private com.vedantu.onetofew.enums.SessionState displayState;
    private List<UserSessionInfo> attendees;
    private String otfSessionId;
    private Set<String> otfBatchIds;
    private String otfSessionUrl;
    private String otfPresenterURL;
    private List<OTFCourseBasicInfo> otfCourseInfos;
    private List<String> replayUrls;
    private Set<OTFSessionFlag> flags = new HashSet<>();
    private Set<SessionLabel> labels = new HashSet<>();
    private Set<EntityTag> entityTags = new HashSet<>();
    private String vimeoId;
    private String agoraVimeoId;
    private OTFSessionToolType sessionToolType;
    private List<ContentInfo> postSessionContents;
    private String id;
    private List<String> groupNames;
    private UserBasicInfo presenterInfo;
    private List<OTFSessionAttendeeInfo> attendeeInfos;
    private List<String> curricullumTopics;
    private List<String> baseTreeTopics;


    public OTFCalendarSessionInfo(OTFSessionPojoUtils session) {
        if (session == null) {
            return;
        }
        this.otfSessionId = session.getId();
        this.otfBatchIds = session.getBatchIds();
        this.description = session.getDescription();
        this.title = session.getTitle();
        // populate attendees - later
        // Update boards - later
        this.startTime = session.getStartTime();
        this.endTime = session.getEndTime();
        this.state = session.getState();
        this.displayState = session.getState();
        this.otfSessionUrl = session.getSessionURL();
        this.otfPresenterURL = session.getPresenterURL();
        this.otfCourseInfos = session.getCourseInfos();
        this.replayUrls = session.getReplayUrls();
        this.flags = session.getFlags();
        this.entityTags = session.getEntityTags();
        this.labels = session.getLabels();
        this.boardId = session.getBoardId();
        this.vimeoId = session.getVimeoId();
        this.agoraVimeoId = session.getAgoraVimeoId();
        this.sessionToolType = session.getSessionToolType();
        this.postSessionContents = session.getPostSessionContents();
        this.attendeeInfos = session.getAttendeeInfos();
        this.presenterInfo = session.getPresenterInfo();
        this.curricullumTopics = session.getCurricullumTopics();
        this.baseTreeTopics = session.getBaseTreeTopics();
    }
}
