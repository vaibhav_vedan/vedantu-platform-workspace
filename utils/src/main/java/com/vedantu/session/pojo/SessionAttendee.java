package com.vedantu.session.pojo;

import com.vedantu.User.AbstractEntityRes;
import com.vedantu.User.Role;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class SessionAttendee extends AbstractMongoStringIdEntityBean {

	private Long sessionId;
	private Long userId;
	private Long startTime;
	private Long endTime;
	private Role role;
	private Integer billingPeriod;
	private int lockedAmount;
	private int chargedAmount;
	private int paidAmount;
	private SessionUserState userState;
	private Long joinTime;

	public SessionAttendee() {

		super();
	}

	public Long getSessionId() {

		return sessionId;
	}

	public void setSessionId(Long sessionId) {

		this.sessionId = sessionId;
	}

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public Long getStartTime() {

		return startTime;
	}

	public void setStartTime(Long startTime) {

		this.startTime = startTime;
	}

	public Long getEndTime() {

		return endTime;
	}

	public void setEndTime(Long endTime) {

		this.endTime = endTime;
	}

	public Role getRole() {

		return role;
	}

	public void setRole(Role role) {

		this.role = role;
	}

	public int getChargedAmount() {

		return chargedAmount;
	}

	public void setChargedAmount(int chargedAmount) {

		this.chargedAmount = chargedAmount;
	}

	public int getLockedAmount() {

		return lockedAmount;
	}

	public void setLockedAmount(int lockedAmount) {

		this.lockedAmount = lockedAmount;
	}

	public int getPaidAmount() {

		return paidAmount;
	}

	public void setPaidAmount(int paidAmount) {

		this.paidAmount = paidAmount;
	}

	public Integer getBillingPeriod() {

		return billingPeriod;
	}

	public void setBillingPeriod(Integer billingPeriod) {

		this.billingPeriod = billingPeriod;
	}

	public SessionUserState getUserState() {
		return userState == null ? SessionUserState.UNKNOWN : userState;
	}

	public void setUserState(SessionUserState userState) {
		this.userState = userState;
	}

	public Long getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(Long joinTime) {
		this.joinTime = joinTime;
	}

	@Override
	public String toString() {

		return "{id:" + getId() + ", creationTime:" + getCreationTime()
				+ ", sessionId:" + sessionId + ", userId:" + userId + "}";
	}

	public enum SessionUserState {
		JOINED, NOT_JOINED, UNKNOWN;
	}

	public static class Constants extends AbstractEntityRes.Constants {

		public static final String USER_ID = "userId";
		public static final String SESSION_ID = "sessionId";
		public static final String SESSION_START_TIME = "startTime";
		public static final String SESSION_END_TIME = "endTime";
		public static final String USER_ROLE = "role";
		public static final String SUBSCRIPTION_ID = "subscriptionId";
	}
}
