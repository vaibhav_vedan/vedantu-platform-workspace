package com.vedantu.session.pojo;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.enums.SessionModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SessionSchedule implements Cloneable {

    private Long startTime;
    private Long endTime;
    private List<SessionSlot> sessionSlots;
    private Long noOfWeeks;
    private long[] slotBitSet;
    private long[] conflictSlotBitSet;
    private Integer conflictCount;
    private String scheduleType;

    public List<SessionSlot> getSessionSlots() {
        return sessionSlots;
    }

    public void setSessionSlots(List<SessionSlot> sessionSlots) {
        this.sessionSlots = sessionSlots;
    }

    public Long getNoOfWeeks() {
        return noOfWeeks;
    }

    public void setNoOfWeeks(Long noOfWeeks) {
        this.noOfWeeks = noOfWeeks;
    }

    public long[] getSlotBitSet() {
        return slotBitSet;
    }

    public void setSlotBitSet(long[] slotBitSet) {
        this.slotBitSet = slotBitSet;
    }

    public long[] getConflictSlotBitSet() {
        return conflictSlotBitSet;
    }

    public void setConflictSlotBitSet(long[] conflictSlotBitSet) {
        this.conflictSlotBitSet = conflictSlotBitSet;
    }

    public Integer getConflictCount() {
        return conflictCount;
    }

    public void setConflictCount(Integer conflictCount) {
        this.conflictCount = conflictCount;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getStartDate() {
        Collections.sort(sessionSlots, new Comparator<SessionSlot>() {
            @Override
            public int compare(SessionSlot o1, SessionSlot o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());
            }
        });
        if (!sessionSlots.isEmpty()) {
            return sessionSlots.get(0).getStartTime();
        }

        return 0l;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public Long getEndDate() {
        Collections.sort(sessionSlots, new Comparator<SessionSlot>() {
            @Override
            public int compare(SessionSlot o1, SessionSlot o2) {
                return o2.getEndTime().compareTo(o1.getEndTime());
            }
        });
        if (!sessionSlots.isEmpty()) {
            return sessionSlots.get(0).getEndTime();
        }
        return 0l;
    }

    public Long calculateTotalHours() {
        Long hours = 0l;
        if(ArrayUtils.isNotEmpty(sessionSlots)){
            for (SessionSlot s : sessionSlots) {
                hours += (s.getEndTime() - s.getStartTime());
            }        
        }
        if (noOfWeeks == null || noOfWeeks < 2) {
            noOfWeeks = 1l;
        }            
        return hours * noOfWeeks;
    }

    public List<String> collectVerificationErrors(List<SessionSlot> allSlots) {
        return collectVerificationErrors(allSlots,null);
    }
    
    public List<String> collectVerificationErrors(List<SessionSlot> allSlots,SessionModel reqModel) {
        List<String> errors = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(allSlots)) {
            Collections.sort(allSlots, new Comparator<SessionSlot>() {
                @Override
                public int compare(SessionSlot o1, SessionSlot o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
            int size = allSlots.size();
            Long currentMillis = System.currentTimeMillis();
            Long maxSessionDuration = ConfigUtils.INSTANCE.getLongValue("max.session.duration") * DateTimeUtils.MILLIS_PER_MINUTE;
            for (int i = 0; i < size; i++) {
                SessionSlot slot = allSlots.get(i);
                if (i < size - 1) {
                    SessionSlot nextSlot = allSlots.get(i + 1);
                    if (slot.getEndTime() > nextSlot.getStartTime()) {
                        errors.add("overlapping slots ");
                        break;
                    }
                }
                if (slot.getStartTime() < currentMillis) {
                    errors.add("slot in the past " + slot);
                    break;
                }
                if (slot.getStartTime() > slot.getEndTime()) {
                    errors.add("slot starttime > endTime" + slot);
                    break;
                }
                if (slot.getEndTime() - slot.getStartTime() > maxSessionDuration) {
                    errors.add("session time greater than max session duration "
                            + maxSessionDuration + " for slot " + slot);
                    break;
                }
                if(reqModel!=null&&!reqModel.equals(slot.getModel())){
                    errors.add("wrong model" + slot);
                    break;
                }
            }
        }

        return errors;
    }

    @Override
    public String toString() {
        return "SessionSchedule [startTime=" + startTime + ", endTime=" + endTime + ", sessionSlots=" + sessionSlots
                + ", noOfWeeks=" + noOfWeeks + ", slotBitSet=" + Arrays.toString(slotBitSet) + ", conflictSlotBitSet="
                + Arrays.toString(conflictSlotBitSet) + ", conflictCount=" + conflictCount + ", scheduleType="
                + scheduleType + "]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
