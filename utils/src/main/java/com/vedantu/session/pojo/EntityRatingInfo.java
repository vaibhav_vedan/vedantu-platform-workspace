package com.vedantu.session.pojo;

public class EntityRatingInfo {

	private Long totalRatingCount = 0L;
	private Float avgRating = new Float(0);

	public EntityRatingInfo(CumilativeRating rating) {
		super();
		if (rating == null) {
			return;
		}
		this.avgRating = rating.getAvgRating();
		this.totalRatingCount = rating.getTotalRatingCount();
	}

	public Long getTotalRatingCount() {
		return totalRatingCount;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	@Override
	public String toString() {
		return String.format("{totalRatingCount=%s, avgRating=%s}",
				totalRatingCount, avgRating);
	}

}
