package com.vedantu.session.pojo;

import com.vedantu.util.EntityType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class CumilativeRating extends AbstractMongoStringIdEntityBean {

	private String entityId;
	private EntityType entityType;
	private Long totalRatingCount = 0L;
	private Long totalRating = 0L;
	private Float avgRating = new Float(0);
	private Boolean baseRatingAssigned;

	public CumilativeRating() {
		super();
	}

	public CumilativeRating(String entityId, EntityType entityType) {
		super();
		this.entityId = entityId;
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public Long getTotalRatingCount() {
		return totalRatingCount;
	}

	public void setTotalRatingCount(Long totalRatingCount) {
		this.totalRatingCount = totalRatingCount;
	}

	public Long getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Long totalRating) {
		this.totalRating = totalRating;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Float avgRating) {
		this.avgRating = avgRating;
	}

	public Boolean getBaseRatingAssigned() {
		return baseRatingAssigned == null ? false : baseRatingAssigned;
	}

	public void setBaseRatingAssigned(Boolean baseRatingAssigned) {
		this.baseRatingAssigned = baseRatingAssigned;
	}

	public void calculateAvgRating() {
		if (totalRatingCount != null && totalRatingCount > 0) {
			avgRating = ((float) totalRating / totalRatingCount);
		}
	}

	public void incRating(int rating) {
		this.totalRating += rating;
	}

	public void decRating(int rating) {

		if (this.totalRating > 0 && this.totalRating > rating) {
			this.totalRating -= rating;
		}
	}

	public void incTotalCount(int count) {
		this.totalRatingCount += count;
	}

	public void decTotalCount(int count) {

		if (this.totalRatingCount > 0 && this.totalRatingCount > count) {
			this.totalRatingCount -= count;
		}
	}

	@Override
	public String toString() {
		return String
				.format("{entityId=%s, entityType=%s, totalCount=%s, totalRating=%s, avgRating=%s}",
						entityId, entityType, totalRatingCount, totalRating,
						avgRating);
	}

}
