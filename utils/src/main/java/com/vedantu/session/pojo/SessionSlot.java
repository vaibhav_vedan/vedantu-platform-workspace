package com.vedantu.session.pojo;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.enums.SessionModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SessionSlot implements Comparator<SessionSlot>, Comparable<SessionSlot>, Cloneable {

    public Long startTime;
    public Long endTime;
    public String topic;
    public String title;
    public String description;
    public Boolean isConflicted;
    private SessionModel model;
    private SessionPayoutType payoutType = SessionPayoutType.PAID;

    public SessionSlot() {
        super();
    }

    public SessionSlot(Long startTime, Long endTime, String topic, String title, String description,
            Boolean isConflicted) {
        super();
        this.startTime = startTime;
        this.endTime = endTime;
        this.topic = topic;
        this.title = title;
        this.description = description;
        this.isConflicted = isConflicted;
    }

    public SessionSlot(SessionSlot s) {
        super();
        this.startTime = s.getStartTime();
        this.endTime = s.getEndTime();
        this.topic = s.getTopic();
        this.title = s.getTitle();
        this.description = s.getDescription();
        this.isConflicted = s.getIsConflicted();
    }

    public SessionSlot(Long startTime, Long endTime) {
        super();
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Boolean getIsConflicted() {
        return isConflicted;
    }

    public void setIsConflicted(Boolean isConflicted) {
        this.isConflicted = isConflicted;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(SessionSlot o) {
        return (int) (this.startTime.compareTo(o.getStartTime()));
    }

    @Override
    public int compare(SessionSlot o1, SessionSlot o2) {
        return (int) (o1.compareTo(o2));
    }

    public List<SessionSlot> splitSlotsByInterval(long splitDuration) {
        List<SessionSlot> slots = new ArrayList<>();
        if (endTime - startTime < splitDuration) {
            slots.add(this);
            return slots;
        }

        for (long i = startTime; i < endTime; i += splitDuration) {
            slots.add(new SessionSlot(i, i + splitDuration, null, null, null, null));
        }
        return slots;
    }

    public static Long getStartTimeFromSlots(List<SessionSlot> slots) {
        if (CollectionUtils.isEmpty(slots)) {
            return null;
        }

        Long startTime = slots.get(0).getStartTime();
        for (SessionSlot slot : slots) {
            if (startTime > slot.getStartTime()) {
                startTime = slot.getStartTime();
            }
        }

        return startTime;
    }

    public static Long getEndTimeFromSlots(List<SessionSlot> slots) {
        if (CollectionUtils.isEmpty(slots)) {
            return null;
        }

        Long endTime = slots.get(0).getEndTime();
        for (SessionSlot slot : slots) {
            if (endTime < slot.getEndTime()) {
                endTime = slot.getEndTime();
            }
        }

        return endTime;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public SessionPayoutType getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(SessionPayoutType payoutType) {
        this.payoutType = payoutType;
    }

    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "SessionSlot{" + "startTime=" + startTime + ", endTime=" + endTime + ", topic=" + topic + ", title=" + title + ", description=" + description + ", isConflicted=" + isConflicted + ", model=" + model + ", payoutType=" + payoutType + '}';
    }

}
