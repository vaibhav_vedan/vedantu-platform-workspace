package com.vedantu.session.pojo;

public enum SessionPayoutType {
	FREE, //free for student but we pay the teacher
        FREE_STUDENT_PAID_TEACHER_NOT_PAID,
        FREE_STUDENT_NOT_PAID_TEACHER_NOT_PAID,//same as FREE
        FREE_STUDENT_PAID_TEACHER_PAID,
        FREE_STUDENT_NOT_PAID_TEACHER_PAID,        
        PAID;//STUDENT is charged and teacher is paid
}
