package com.vedantu.session.pojo;

import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.session.pojo.SessionAttendee.SessionUserState;

public class UserSessionInfo extends UserBasicInfo {

	private Integer billingPeriod;
	private int chargedAmount;
	private int paidAmount;
	private SessionUserState userState;
	// only for TEACHER this field will be populated
	private EntityRatingInfo ratingInfo;
	private Long joinTime;

	public UserSessionInfo() {
		super();
	}

	public UserSessionInfo(User user) {
		super(user, false);
	}

	public UserSessionInfo(User user, SessionAttendee attendee) {

		super(user, false);
		this.billingPeriod = attendee.getBillingPeriod();
		this.chargedAmount = attendee.getChargedAmount();
		this.paidAmount = attendee.getPaidAmount();
		this.userState = attendee.getUserState();
		this.joinTime = attendee.getJoinTime();
	}

	public Integer getBillingPeriod() {

		return billingPeriod;
	}

	public void setBillingPeriod(Integer billingPeriod) {

		this.billingPeriod = billingPeriod;
	}

	public int getChargedAmount() {

		return chargedAmount;
	}

	public void setChargedAmount(int chargedAmount) {

		this.chargedAmount = chargedAmount;
	}

	public int getPaidAmount() {

		return paidAmount;
	}

	public void setPaidAmount(int paidAmount) {

		this.paidAmount = paidAmount;
	}

	public SessionUserState getUserState() {
		return userState;
	}

	public void setUserState(SessionUserState userState) {
		this.userState = userState;
	}

	public void setRatingInfo(EntityRatingInfo ratingInfo) {
		this.ratingInfo = ratingInfo;
	}

	public EntityRatingInfo getRatingInfo() {
		return ratingInfo;
	}

	public Long getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(Long joinTime) {
		this.joinTime = joinTime;
	}

        @Override
	public void updateUserDetails(UserBasicInfo userBasicInfo) {
		if (userBasicInfo != null) {
			super.updateUserDetails(userBasicInfo);
		}

		// TODO : Need to check if ratingInfo is required
	}

	@Override
	public String toString() {
		return super.toString() +"UserSessionInfo [billingPeriod=" + billingPeriod + ", chargedAmount=" + chargedAmount + ", paidAmount="
				+ paidAmount + ", userState=" + userState + ", ratingInfo=" + ratingInfo + ", joinTime=" + joinTime
				+ "]";
	}

}
