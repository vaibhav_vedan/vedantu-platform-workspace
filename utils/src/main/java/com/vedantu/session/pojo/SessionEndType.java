package com.vedantu.session.pojo;

/**
 *
 * @author manishkumarsingh
 */
public enum SessionEndType {
    MANUAL, AUTO;
}
