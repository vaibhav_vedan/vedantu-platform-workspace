package com.vedantu.session.pojo.proposal;

import java.io.Serializable;

public class ProposalSlot extends ProposalSlotBasic implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long conflictSessionId;
	private Long conflictSessionStartTime;
	private Long conflictSessionEndTime;
	private String conflictSessiontitle;
	private Long conflictSessionUserId;

	public ProposalSlot() {
		super();
	}

	public ProposalSlot(Long startTime, Long endTime, String title) {
		super(startTime, endTime, title);
	}

	public ProposalSlot(ProposalSlot proposalSlot) {
		super(proposalSlot);
	}

	public Long getConflictSessionId() {
		return conflictSessionId;
	}

	public void setConflictSessionId(Long conflictSessionId) {
		this.conflictSessionId = conflictSessionId;
	}

	public Long getConflictSessionStartTime() {
		return conflictSessionStartTime;
	}

	public void setConflictSessionStartTime(Long conflictSessionStartTime) {
		this.conflictSessionStartTime = conflictSessionStartTime;
	}

	public Long getConflictSessionEndTime() {
		return conflictSessionEndTime;
	}

	public void setConflictSessionEndTime(Long conflictSessionEndTime) {
		this.conflictSessionEndTime = conflictSessionEndTime;
	}

	public String getConflictSessiontitle() {
		return conflictSessiontitle;
	}

	public void setConflictSessiontitle(String conflictSessiontitle) {
		this.conflictSessiontitle = conflictSessiontitle;
	}

	public Long getConflictSessionUserId() {
		return conflictSessionUserId;
	}

	public void setConflictSessionUserId(Long conflictSessionUserId) {
		this.conflictSessionUserId = conflictSessionUserId;
	}

	@Override
	public String toString() {
		return super.toString()+" ProposalSlot [conflictSessionId=" + conflictSessionId + ", conflictSessionStartTime="
				+ conflictSessionStartTime + ", conflictSessionEndTime=" + conflictSessionEndTime
				+ ", conflictSessiontitle=" + conflictSessiontitle + ", conflictSessionUserId=" + conflictSessionUserId
				+ "]";
	}

}
