/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.session.pojo;

/**
 *
 * @author somil
 */
public enum SessionEvents {
    SESSION_SCHEDULED, SESSION_RESCHEDULED, SESSION_STARTED, SESSION_ACTIVE,
    SESSION_CANCELED, SESSION_FORFEITED, SESSION_ENDED, SESSION_EXPIRED, SESSION_PAYOUT_CALCULATED
}
