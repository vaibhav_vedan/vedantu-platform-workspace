package com.vedantu.session.pojo;

import com.vedantu.session.pojo.SessionSlot;
import java.util.ArrayList;
import java.util.List;

public class SlotCheckPojo {

	List<Long> studentIds = new ArrayList<Long>();
	List<Long> teacherIds = new ArrayList<Long>();
	List<SessionSlot> slots = new ArrayList<SessionSlot>();

	
	public SlotCheckPojo() {
		super();
	}

	public SlotCheckPojo(List<Long> studentIds, List<Long> teacherIds, List<SessionSlot> slots) {
		super();
		this.studentIds = studentIds;
		this.teacherIds = teacherIds;
		this.slots = slots;
	}

	public List<SessionSlot> getSlots() {
		return slots;
	}

	public void setSlots(List<SessionSlot> slots) {
		this.slots = slots;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public List<Long> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(List<Long> teacherIds) {
		this.teacherIds = teacherIds;
	}

	@Override
	public String toString() {
		return "SlotCheckPojo [studentIds=" + studentIds + ", teacherIds=" + teacherIds + ", slots=" + slots
				+ ", toString()=" + super.toString() + "]";
	}
}
