package com.vedantu.session.pojo;

public enum VisibilityState {
	VISIBLE, INVISIBLE;

	public static VisibilityState getVisibilityState(String taskname) {

		VisibilityState visibilityState = null;
		try {
			visibilityState = VisibilityState.valueOf(taskname.trim().toUpperCase());
		} catch (Throwable e) {
		}
		return visibilityState;
	}
}
