package com.vedantu.offering.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetMyOfferingsRequest extends AbstractFrontEndListReq {

		private String offeringId;
		private String teacherId;

		public String getOfferingId() {
			return offeringId;
		}

		public void setOfferingId(String offeringId) {
			this.offeringId = offeringId;
		}

		public String getTeacherId() {
			return teacherId;
		}

		public void setTeacherId(String teacherId) {
			this.teacherId = teacherId;
		}

		@Override
		public String toString() {
			return String.format("{offeringId=%s}", offeringId);
		}
}
