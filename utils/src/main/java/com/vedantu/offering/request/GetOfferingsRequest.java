package com.vedantu.offering.request;

import java.util.List;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetOfferingsRequest extends AbstractFrontEndListReq {

    private OfferingType offeringType;
    private List<String> tags;
    private String exam;
    private String grade;
    private String boardId;
    private String boardSlug;
    private String category;
    private String userId;
    private VisibilityState offeringState;
    private boolean featured;
    private String query;
    private Boolean onlyRequiredFields;
    private List<String> offeringIds;
    private List<String> subtypes;

    public GetOfferingsRequest() {
        super();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public VisibilityState getOfferingState() {
        return offeringState;
    }

    public void setOfferingState(VisibilityState offeringState) {
        this.offeringState = offeringState;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getBoardSlug() {
        return boardSlug;
    }

    public void setBoardSlug(String boardSlug) {
        this.boardSlug = boardSlug;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public OfferingType getOfferingType() {
        return offeringType;
    }

    public void setOfferingType(OfferingType offeringType) {
        this.offeringType = offeringType;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getExam() {
        return exam;
    }

    public void setExamTag(String examTag) {
        this.exam = examTag;
    }

    public boolean isFeatured() {
        return featured;
    }

    public String getQuery() {
        return query;
    }

    public Boolean getOnlyRequiredFields() {
        return onlyRequiredFields;
    }

    public void setOnlyRequiredFields(Boolean onlyRequiredFields) {
        this.onlyRequiredFields = onlyRequiredFields;
    }

    public List<String> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(List<String> subtypes) {
        this.subtypes = subtypes;
    }

    public List<String> getOfferingIds() {
        return offeringIds;
    }

    public void setOfferingIds(List<String> offeringIds) {
        this.offeringIds = offeringIds;
    }

}
