package com.vedantu.offering.request;

import java.util.List;

import com.vedantu.offering.response.StudyPlanInfo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddStudyPlanRequest extends AbstractFrontEndReq {

	private String offeringId;
	private String targetUserId;
	private String userId;
	private String note;
	private Long targetDate;
	private String name;
	private Long noOfSessions;
	private String status;

	public AddStudyPlanRequest() {
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Long targetDate) {
		this.targetDate = targetDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNoOfSessions() {
		return noOfSessions;
	}

	public void setNoOfSessions(Long noOfSessions) {
		this.noOfSessions = noOfSessions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (targetUserId == null || targetUserId.isEmpty()) {
			errors.add(StudyPlanInfo.Constants.TARGET_USER_ID);
		}

		return errors;
	}

	@Override
	public String toString() {
		return "AddStudyPlanReq [offeringId=" + offeringId + ", targetUserId="
				+ targetUserId + ", note=" + note + ", targetDate="
				+ targetDate + ", name=" + name + ", noOfSessions="
				+ noOfSessions + ", status=" + status + "]";
	}
}
