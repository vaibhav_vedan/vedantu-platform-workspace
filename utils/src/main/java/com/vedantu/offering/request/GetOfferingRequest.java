package com.vedantu.offering.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetOfferingRequest extends AbstractFrontEndReq {

    private Long offeringId;
    private String userId;

    public GetOfferingRequest(Long offeringId, Long userId, Long callingUserId) {

        if (offeringId != null) {
            this.offeringId = offeringId;
        }

        if (userId != null) {
            this.userId = userId.toString();
        }
        if (callingUserId != null) {
            super.setCallingUserId(callingUserId);
        }
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "GetOfferingRequest{" + "offeringId=" + offeringId + ", userId=" + userId + '}';
    }

}
