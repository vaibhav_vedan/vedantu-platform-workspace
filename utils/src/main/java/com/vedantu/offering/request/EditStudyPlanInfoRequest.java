package com.vedantu.offering.request;

import java.util.List;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class EditStudyPlanInfoRequest extends AbstractFrontEndReq {

	private String id;

	private String note;
	private Long targetDate;
	private VisibilityState state;
	private String name;
	private Long noOfSessions;
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Long targetDate) {
		this.targetDate = targetDate;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNoOfSessions() {
		return noOfSessions;
	}

	public void setNoOfSessions(Long noOfSessions) {
		this.noOfSessions = noOfSessions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if (id == null || id.isEmpty()) {
			errors.add("ID");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "EditStudyPlanInfoRequest [id=" + id + ", note=" + note
				+ ", targetDate=" + targetDate + ", state=" + state + ", name="
				+ name + ", noOfSessions=" + noOfSessions + ", status="
				+ status + "]";
	}
}
