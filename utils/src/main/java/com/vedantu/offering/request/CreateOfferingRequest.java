package com.vedantu.offering.request;

import java.util.HashSet;
import java.util.List;

import com.vedantu.offering.pojo.Offering;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.OfferingSubType;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.enums.Scope;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CreateOfferingRequest extends AbstractFrontEndReq {
	private String title;
	private List<String> grades;
	private List<String> subjects;
	private List<String> boardIds;
	private String description;

	private String targetAudienceText;

	// price per hour basis
	private Long perHrPrice;

	private Long price;
	private Long displayPrice;
	private String currencyCode;// default to INR
	private List<String> tags;
	private List<String> examTags;
	private String recommendedSchedule;
	private Long totalDays;
	private Long totalHours;
	private OfferingType type;
	private VisibilityState state;
	private List<String> imageUrls;
	private List<String> videoUrls;
	private List<String> contents;
	private List<String> teacherIds;
	private List<String> categories;
	private List<String> parentTopics;
	private Integer priority;
	private List<String> takeAways;
	private Scope scope;
	private Boolean isFeatured;
	private String offeringLength;
	private List<OfferingSubType> subtypes;

	// @Override
	// protected void populate(HttpServletRequest req) {
	// this.title = getStringFieldValue(Offering.Constants.TITLE, req);
	// this.grades = getListFieldValue(Offering.Constants.GRADES, req);
	// this.subjects = getListFieldValue(Offering.Constants.SUBJECTS, req);
	// this.boardIds = getListFieldLongValue(Offering.Constants.BOARD_IDS, req);
	// this.description = getStringFieldValue(Offering.Constants.DESCRIPTION,
	// req);
	// this.targetAudienceText =
	// getStringFieldValue(Offering.Constants.TARGET_AUDIENCE_TEXT, req);
	//
	// this.perHrPrice = getLongFieldValue(Offering.Constants.PER_HR_PRICE,
	// req);
	// this.price = getLongFieldValue(Offering.Constants.PRICE, req);
	// this.displayPrice = getLongFieldValue(Offering.Constants.DISPLAY_PRICE,
	// req);
	// this.currencyCode = getStringFieldValue(Offering.Constants.CURRENCY_CODE,
	// req);
	// this.tags = getListFieldValue(Offering.Constants.TAGS, req);
	// this.examTags = getListFieldValue(Offering.Constants.EXAM_TAGS, req);
	// this.recommendedSchedule =
	// getStringFieldValue(Offering.Constants.RECOMMENDED_SCHEDULE, req);
	// this.totalDays = getLongFieldValue(Offering.Constants.TOTAL_DAYS, req);
	// this.totalHours = getLongFieldValue(Offering.Constants.TOTAL_HOURS, req);
	// this.type =
	// OfferingType.valueOf(getStringFieldValue(Offering.Constants.TYPE, req));
	// this.state =
	// VisibilityState.valueOf(getStringFieldValue(Offering.Constants.STATE,
	// req));
	// this.imageUrls = getListFieldValue(Offering.Constants.IMAGE_URLS, req);
	// this.videoUrls = getListFieldValue(Offering.Constants.VIDEO_URLS, req);
	// this.contents = getListFieldValue(Offering.Constants.CONTENTS, req);
	// this.teacherIds = getListFieldLongValue(Offering.Constants.TEACHER_IDS,
	// req);
	// this.categories = getListFieldValue(Offering.Constants.CATEGORIES, req);
	// this.parentTopics = getListFieldValue(Offering.Constants.PARENT_TOPICS,
	// req);
	// this.priority = getIntFieldValue(Offering.Constants.PRIORITY, req);
	// this.takeAways = getListFieldValue(Offering.Constants.TAKEAWAYS, req);
	// this.scope = Scope.valueOf(getStringFieldValue(Offering.Constants.SCOPE,
	// req));
	// this.isFeatured = getBooleanFieldValue(Offering.Constants.IS_FEATURED,
	// req);
	// this.offeringLength =
	// getStringFieldValue(Offering.Constants.OFFERING_LENGTH, req);
	// this.subtypes = new ArrayList<OfferingSubType>();
	// for (int i = 0;; i++) {
	// String subType = getStringFieldValue("subtypes[" + i + "]", req);
	// if (StringUtils.isEmpty(subType)) {
	// break;
	// }
	// this.subtypes.add(OfferingSubType.valueOf(subType));
	// }
	// }

	public List<String> getTakeAways() {
		return takeAways;
	}

	public void setTakeAways(List<String> takeAways) {
		this.takeAways = takeAways;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getGrades() {
		return grades;
	}

	public void setGrades(List<String> grades) {
		this.grades = grades;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(List<String> boardIds) {
		this.boardIds = boardIds;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTargetAudienceText() {
		return targetAudienceText;
	}

	public void setTargetAudienceText(String targetAudienceText) {
		this.targetAudienceText = targetAudienceText;
	}

	public Long getPerHrPrice() {
		return perHrPrice;
	}

	public void setPerHrPrice(Long perHrPrice) {
		this.perHrPrice = perHrPrice;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(Long displayPrice) {
		this.displayPrice = displayPrice;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getExamTags() {
		return examTags;
	}

	public void setExamTags(List<String> examTags) {
		this.examTags = examTags;
	}

	public String getRecommendedSchedule() {
		return recommendedSchedule;
	}

	public void setRecommendedSchedule(String recommendedSchedule) {
		this.recommendedSchedule = recommendedSchedule;
	}

	public Long getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(Long totalDays) {
		this.totalDays = totalDays;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public OfferingType getType() {
		return type;
	}

	public void setType(OfferingType type) {
		this.type = type;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public List<String> getImageUrls() {
		return imageUrls;
	}

	public void setImageUrls(List<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

	public List<String> getVideoUrls() {
		return videoUrls;
	}

	public void setVideoUrls(List<String> videoUrls) {
		this.videoUrls = videoUrls;
	}

	public List<String> getContents() {
		return contents;
	}

	public void setContents(List<String> contents) {
		this.contents = contents;
	}

	public List<String> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(List<String> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getParentTopics() {
		return parentTopics;
	}

	public void setParentTopics(List<String> parentTopics) {
		this.parentTopics = parentTopics;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Boolean getIsFeatured() {
		return isFeatured;
	}

	public void setIsFeatured(Boolean isFeatured) {
		this.isFeatured = isFeatured;
	}

	public String getOfferingLength() {
		return offeringLength;
	}

	public void setOfferingLength(String offeringLength) {
		this.offeringLength = offeringLength;
	}

	public List<OfferingSubType> getSubtypes() {
		return subtypes;
	}

	public void setSubtypes(List<OfferingSubType> subtypes) {
		this.subtypes = subtypes;
	}

	public Offering toOffering() {

		Offering offering = new Offering();
		offering.setTags(tags);
		offering.setExamTags(examTags);
		offering.setTitle(title);
		offering.setDescription(description);
		offering.setPrice(price);
		offering.setDisplayPrice(displayPrice);
		offering.setCurrencyCode(currencyCode);
		offering.setRecommendedSchedule(recommendedSchedule);
		offering.setTotalDays(totalDays);
		offering.setTotalHours(totalHours);
		offering.setImageUrls(imageUrls);
		offering.setVideoUrls(videoUrls);
		offering.setType(type);
		offering.setState(state);
		offering.setContents(contents);
		offering.setGrades(grades);

		offering.setSubjects(subjects);
		offering.setBoardIds(boardIds == null ? new HashSet<String>()
				: new HashSet<String>(boardIds));
		offering.setTeacherIds(teacherIds);
		offering.setCategories(categories);
		offering.setParentTopics(parentTopics);
		offering.setPriority(priority);
		offering.setTakeAways(takeAways);
		offering.setCreatedByUserId(getCallingUserId());
		offering.setScope(scope);
		offering.setIsFeatured(isFeatured);
		offering.setPerHrPrice(perHrPrice);
		offering.setTargetAudienceText(targetAudienceText);
		offering.setOfferingLength(offeringLength);
		offering.setSubtypes(subtypes);
		return offering;
	}

	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == type) {
			errors.add(Offering.Constants.TYPE);
		}

		if (StringUtils.isEmpty(title)) {
			errors.add(Offering.Constants.TITLE);
		}

		if (null == price) {
			errors.add(Offering.Constants.PRICE);
		}

		if (null == displayPrice) {
			errors.add(Offering.Constants.DISPLAY_PRICE);
		}

		if (StringUtils.isEmpty(currencyCode)) {
			errors.add(Offering.Constants.CURRENCY_CODE);
		}

		if (OfferingType.TOPIC.equals(type)) {

			if (CollectionUtils.isEmpty(boardIds)) {
				errors.add(Offering.Constants.BOARD_IDS);
			}
			if (StringUtils.isEmpty(description)) {
				errors.add(Offering.Constants.DESCRIPTION);
			}
			if (CollectionUtils.isEmpty(grades)) {
				errors.add(Offering.Constants.GRADES);
			}
			if (StringUtils.isEmpty(recommendedSchedule)) {
				errors.add(Offering.Constants.RECOMMENDED_SCHEDULE);
			}
		}
		return errors;
	}

	@Override
	public String toString() {
		return "CreateOfferingReq [title=" + title + ", grades=" + grades
				+ ", subjects=" + subjects + ", boardIds=" + boardIds
				+ ", description=" + description + ", targetAudienceText="
				+ targetAudienceText + ", perHrPrice=" + perHrPrice
				+ ", price=" + price + ", displayPrice=" + displayPrice
				+ ", currencyCode=" + currencyCode + ", tags=" + tags
				+ ", examTags=" + examTags + ", recommendedSchedule="
				+ recommendedSchedule + ", totalDays=" + totalDays
				+ ", totalHours=" + totalHours + ", type=" + type + ", state="
				+ state + ", imageUrls=" + imageUrls + ", videoUrls="
				+ videoUrls + ", contents=" + contents + ", teacherIds="
				+ teacherIds + ", categories=" + categories + ", parentTopics="
				+ parentTopics + ", priority=" + priority + ", takeAways="
				+ takeAways + ", scope=" + scope + ", isFeatured=" + isFeatured
				+ ", offeringLength=" + offeringLength + ", subtypes="
				+ subtypes + "]";
	}
}
