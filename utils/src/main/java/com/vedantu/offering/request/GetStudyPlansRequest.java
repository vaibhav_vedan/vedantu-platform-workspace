package com.vedantu.offering.request;

import java.util.List;

import com.vedantu.offering.response.StudyPlanInfo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetStudyPlansRequest extends AbstractFrontEndReq {

	private String offeringId;
	private String targetUserId;
	private String userId;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if (targetUserId == null || targetUserId.isEmpty()) {
			errors.add(StudyPlanInfo.Constants.TARGET_USER_ID);
		}
		return errors;
	}

	@Override
	public String toString() {
		return "GetStudyPlansReq [offeringId=" + offeringId + ", targetUserId="
				+ targetUserId + ", userId=" + userId + "]";
	}
}
