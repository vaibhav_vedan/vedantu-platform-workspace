package com.vedantu.offering.request;

import java.util.List;

public class EditOfferingRequest extends CreateOfferingRequest {

	private Long offeringId;
	private List<String> editList;
	private boolean updatePlan;

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public List<String> getEditList() {
		return editList;
	}

	public void setEditList(List<String> editList) {
		this.editList = editList;
	}

	public boolean isUpdatePlan() {
		return updatePlan;
	}

	public void setUpdatePlan(boolean updatePlan) {
		this.updatePlan = updatePlan;
	}

	@Override
	public String toString() {
		return String.format("{offeringId=%s, editList=%s, toString()=%s}",
				offeringId, editList, super.toString());
	}
}
