package com.vedantu.offering.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.User.AbstractInfo;

public class TeacherPlanInfo extends AbstractInfo {
	/**
	 * 
	 */

	private static final long serialVersionUID = 6592265630660118722L;
	private Long teacherId;
	private String planId;

	public TeacherPlanInfo() {
		super();
	}

	public TeacherPlanInfo(Long teacherId, String planId) {
		super();
		this.teacherId = teacherId;
		this.planId = planId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public static Map<String, String> fetchTeacherIdMap(
			List<TeacherPlanInfo> previousPlans) {
		Map<String, String> map = new HashMap<String, String>();
		if (previousPlans != null && !previousPlans.isEmpty()) {
			for (TeacherPlanInfo planInfo : previousPlans) {
				map.put(String.valueOf(planInfo.getTeacherId()),
						planInfo.getPlanId());
			}
		}

		return map;
	}

	@Override
	public String toString() {
		return "TeacherPlanInfo [teacherId=" + teacherId + ", planId=" + planId
				+ "]";
	}

	@Override
	public List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<String>();
		return errors;
	}
}
