package com.vedantu.offering.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.pojo.Subscription;
import com.vedantu.util.enums.OfferingSubType;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.enums.Scope;

public class OfferingInfo {

    private String offeringId;
    private String title;
    private List<String> grades;
    private List<String> subjects;
    private List<BoardTreeInfo> boards;
    private String description;
    private String targetAudienceText;
    private Long price;
    private Long displayPrice;
    private String currencyCode;
    private String recommendedSchedule;
    private List<String> tags;
    private List<String> examTags;
    private Long totalDays;
    private Long totalHours;
    private OfferingType type;
    private VisibilityState state;
    private List<String> imageUrls;
    private List<String> videoUrls;
    private List<String> contents;
    private List<String> teacherIds;
    private List<UserBasicInfo> teachersInfo;
    private List<String> categories;
    private List<String> parentTopics;
    private Integer priority;
    private List<String> takeAways;
    private Subscription subscription;
    private Scope scope;
    private Boolean isFeatured;
    private String offeringLength;
    private PlanInfo planInfo;
    private List<OfferingSubType> subtypes;
    private List<TeacherPlanInfo> teacherPlans;

    public OfferingInfo() {

    }

    public OfferingInfo(Offering offering, Map<String, UserBasicInfo> teachersMap) {
        this.offeringId = offering.getId();
        this.title = offering.getTitle();
        if (offering.getDescription() != null) {
            this.description = offering.getDescription();
        }
        if (offering.getTargetAudienceText() != null) {
            this.targetAudienceText = offering.getTargetAudienceText();
        }
        this.recommendedSchedule = offering.getRecommendedSchedule();
        this.price = offering.getPrice();
        this.displayPrice = offering.getDisplayPrice();
        this.currencyCode = offering.getCurrencyCode();
        this.tags = offering.getTags();
        this.examTags = offering.getExamTags();
        this.subjects = offering.getSubjects();
        this.grades = offering.getGrades();
        this.totalDays = offering.getTotalDays();
        this.totalHours = offering.getTotalHours();
        this.type = offering.getType();
        this.state = offering.getState();
        this.imageUrls = offering.getImageUrls();
        this.videoUrls = offering.getVideoUrls();
        this.contents = offering.getContents();
        List<UserBasicInfo> teacherInfos = new ArrayList<>();
        for (String teacherId : offering.getTeacherIds()) {
            if (teachersMap.containsKey(teacherId)) {
                teacherInfos.add(teachersMap.get(teacherId));
            }
        }
        this.teachersInfo = teacherInfos;
        this.categories = offering.getCategories();
        this.parentTopics = offering.getParentTopics();
        this.priority = offering.getPriority();
        this.takeAways = offering.getTakeAways();
        this.scope = offering.getScope();
        this.isFeatured = offering.getIsFeatured();
        this.offeringLength = offering.getOfferingLength();
        this.subtypes = offering.getSubtypes();
        this.teacherPlans = offering.getTeacherPlans();
    }

    public OfferingInfo(Offering offering) {
        this(offering, null);
    }

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public List<BoardTreeInfo> getBoards() {
        return boards;
    }

    public void setBoards(List<BoardTreeInfo> boards) {
        this.boards = boards;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetAudienceText() {
        return targetAudienceText;
    }

    public void setTargetAudienceText(String targetAudienceText) {
        this.targetAudienceText = targetAudienceText;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(Long displayPrice) {
        this.displayPrice = displayPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getRecommendedSchedule() {
        return recommendedSchedule;
    }

    public void setRecommendedSchedule(String recommendedSchedule) {
        this.recommendedSchedule = recommendedSchedule;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getExamTags() {
        return examTags;
    }

    public void setExamTags(List<String> examTags) {
        this.examTags = examTags;
    }

    public Long getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(Long totalDays) {
        this.totalDays = totalDays;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public OfferingType getType() {
        return type;
    }

    public void setType(OfferingType type) {
        this.type = type;
    }

    public VisibilityState getState() {
        return state;
    }

    public void setState(VisibilityState state) {
        this.state = state;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public List<String> getContents() {
        return contents;
    }

    public void setContents(List<String> contents) {
        this.contents = contents;
    }

    public List<String> getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(List<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    public List<UserBasicInfo> getTeachersInfo() {
        return teachersInfo;
    }

    public void setTeachersInfo(List<UserBasicInfo> teachersInfo) {
        this.teachersInfo = teachersInfo;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getParentTopics() {
        return parentTopics;
    }

    public void setParentTopics(List<String> parentTopics) {
        this.parentTopics = parentTopics;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<String> getTakeAways() {
        return takeAways;
    }

    public void setTakeAways(List<String> takeAways) {
        this.takeAways = takeAways;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Boolean getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getOfferingLength() {
        return offeringLength;
    }

    public void setOfferingLength(String offeringLength) {
        this.offeringLength = offeringLength;
    }

    public PlanInfo getPlanInfo() {
        return planInfo;
    }

    public void setPlanInfo(PlanInfo planInfo) {
        this.planInfo = planInfo;
    }

    public List<OfferingSubType> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(List<OfferingSubType> subtypes) {
        this.subtypes = subtypes;
    }

    public List<TeacherPlanInfo> getTeacherPlans() {
        return teacherPlans;
    }

    public void setTeacherPlans(List<TeacherPlanInfo> teacherPlans) {
        this.teacherPlans = teacherPlans;
    }

    @Override
    public String toString() {
        return "OfferingInfo [offeringId=" + offeringId + ", title=" + title
                + ", grades=" + grades + ", subjects=" + subjects + ", boards="
                + boards + ", description=" + description
                + ", targetAudienceText=" + targetAudienceText + ", price="
                + price + ", displayPrice=" + displayPrice + ", currencyCode="
                + currencyCode + ", recommendedSchedule=" + recommendedSchedule
                + ", tags=" + tags + ", examTags=" + examTags + ", totalDays="
                + totalDays + ", totalHours=" + totalHours + ", type=" + type
                + ", state=" + state + ", imageUrls=" + imageUrls
                + ", videoUrls=" + videoUrls + ", contents=" + contents
                + ", teacherIds=" + teacherIds + ", teachersInfo="
                + teachersInfo + ", categories=" + categories
                + ", parentTopics=" + parentTopics + ", priority=" + priority
                + ", takeAways=" + takeAways + ", subscription=" + subscription
                + ", scope=" + scope + ", isFeatured=" + isFeatured
                + ", offeringLength=" + offeringLength + ", planInfo="
                + planInfo + ", subtypes=" + subtypes + "]";
    }
}
