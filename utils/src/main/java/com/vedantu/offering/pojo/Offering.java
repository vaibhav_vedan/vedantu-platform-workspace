package com.vedantu.offering.pojo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.User.AbstractEntityRes;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.SearchJanitorUtils;
import com.vedantu.util.enums.IBoardAware;
import com.vedantu.util.enums.OfferingSubType;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.enums.Scope;

public class Offering implements IBoardAware {

	private String id;
	private Long creationTime;
	private String title;

	private Set<String> keywords;

	private List<String> grades;
	private List<String> subjects;
	private Set<String> boardIds;
	private String description;

	// who it for field
	private String targetAudienceText;

	// price per hour basis
	private Long perHrPrice;

	// this will be bulk price
	private Long price;
	private Long displayPrice;
	private String currencyCode;
	private String recommendedSchedule;
	private List<String> tags;
	private List<String> examTags;
	private Long totalDays;
	private Long totalHours;
	private OfferingType type;
	private List<String> imageUrls;
	private List<String> videoUrls;
	private List<String> contents;
	private List<String> teacherIds;
	private List<String> categories;
	private List<String> parentTopics;
	private VisibilityState state = VisibilityState.VISIBLE;
	private Integer priority = 0;
	private List<String> takeAways;
	private Long createdByUserId;
	private Scope scope = Scope.PUBLIC;
	private Boolean isFeatured;
	private List<OfferingSubType> subtypes;

	private List<TeacherPlanInfo> teacherPlans;

	// e.g Full Year (April 2015 - Jan 2016)
	private String offeringLength;

        public Offering() {
        }

        
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

        
	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public List<String> getTakeAways() {
		return takeAways;
	}

	public void setTakeAways(List<String> takeAways) {
		this.takeAways = takeAways;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getExamTags() {
		return examTags;
	}

	public void setExamTags(List<String> examTags) {
		this.examTags = examTags;
	}

	public List<String> getImageUrls() {
		return imageUrls;
	}

	public void setImageUrls(List<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

	public void setVideoUrls(List<String> videoUrls) {
		this.videoUrls = videoUrls;
	}

	public List<String> getContents() {
		return contents;
	}

	public void setContents(List<String> contents) {
		this.contents = contents;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		this.setKeywords(SearchJanitorUtils.getTokensForIndexingOrQuery(
				title, true));
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTargetAudienceText() {
		return targetAudienceText;
	}

	public void setTargetAudienceText(String targetAudienceText) {
		this.targetAudienceText = targetAudienceText;
	}

	public Long getPerHrPrice() {
		return perHrPrice;
	}

	public void setPerHrPrice(Long perHrPrice) {
		this.perHrPrice = perHrPrice;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(Long displayPrice) {
		this.displayPrice = displayPrice;
	}

	public String getRecommendedSchedule() {
		return recommendedSchedule;
	}

	public void setRecommendedSchedule(String recommendedSchedule) {
		this.recommendedSchedule = recommendedSchedule;
	}

	public List<String> getTopics() {
		return tags;
	}

	public void setTopics(List<String> topics) {
		this.tags = topics;
	}

	public Long getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(Long totalDays) {
		this.totalDays = totalDays;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public OfferingType getType() {
		return type;
	}

	public void setType(OfferingType type) {
		this.type = type;
	}

	public List<String> getVideoUrls() {
		return videoUrls;
	}

	public void setVideoUrl(List<String> videoUrls) {
		this.videoUrls = videoUrls;
	}

	public List<String> getGrades() {
		return grades;
	}

	public void setGrades(List<String> grades) {
		this.grades = grades;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public Set<String> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(Set<String> boardIds) {
		this.boardIds = boardIds;
	}

	public List<String> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(List<String> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getParentTopics() {
		return parentTopics;
	}

	public void setParentTopics(List<String> parentTopics) {
		this.parentTopics = parentTopics;
	}

	public void setPriority(Integer priority) {
		this.priority = priority == null ? 0 : priority;
	}

	public Integer getPriority() {
		return priority;
	}

	public Long getCreatedByUserId() {
		return createdByUserId;
	}

	public void setCreatedByUserId(Long createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Boolean getIsFeatured() {
		return isFeatured;
	}

	public void setIsFeatured(Boolean isFeatured) {
		this.isFeatured = isFeatured;
	}

	public Set<Long> _getBoardIds() {
		Set<Long> boardIdsLong = new HashSet<Long>();

		for (String boardId : boardIds) {
			boardIdsLong.add(Long.parseLong(boardId));
		}

		return boardIdsLong;
	}

	public String getOfferingLength() {
		return offeringLength;
	}

	public void setOfferingLength(String offeringLength) {
		this.offeringLength = offeringLength;
	}

	public List<OfferingSubType> getSubtypes() {
		return subtypes;
	}

	public void setSubtypes(List<OfferingSubType> subtypes) {
		this.subtypes = subtypes;
	}

	public List<TeacherPlanInfo> getTeacherPlans() {
		return teacherPlans;
	}

	public void setTeacherPlans(List<TeacherPlanInfo> teacherPlans) {
		this.teacherPlans = teacherPlans;
	}

        public Set<String> getKeywords() {
            return keywords;
        }

        public void setKeywords(Set<String> keywords) {
            this.keywords = keywords;
        }

        @Override
        public String toString() {
            return "Offering{" + "id=" + id + ", creationTime=" + creationTime + ", title=" + title + ", keywords=" + keywords + ", grades=" + grades + ", subjects=" + subjects + ", boardIds=" + boardIds + ", description=" + description + ", targetAudienceText=" + targetAudienceText + ", perHrPrice=" + perHrPrice + ", price=" + price + ", displayPrice=" + displayPrice + ", currencyCode=" + currencyCode + ", recommendedSchedule=" + recommendedSchedule + ", tags=" + tags + ", examTags=" + examTags + ", totalDays=" + totalDays + ", totalHours=" + totalHours + ", type=" + type + ", imageUrls=" + imageUrls + ", videoUrls=" + videoUrls + ", contents=" + contents + ", teacherIds=" + teacherIds + ", categories=" + categories + ", parentTopics=" + parentTopics + ", state=" + state + ", priority=" + priority + ", takeAways=" + takeAways + ", createdByUserId=" + createdByUserId + ", scope=" + scope + ", isFeatured=" + isFeatured + ", subtypes=" + subtypes + ", teacherPlans=" + teacherPlans + ", offeringLength=" + offeringLength + '}';
        }



	public static class Constants extends AbstractEntityRes.Constants {

		public static final String OFFERING_ID = "offeringId";
		public static final String CATEGORY = "category";
		public static final String TITLE = "title";
		public static final String TITLE_SEARCH = "titleSearch";
		public static final String PACKAGE_NAME = "packageName";
		public static final String GRADES = "grades";
		public static final String GRADE = "grade";
		public static final String SUBJECT = "subject";
		public static final String SUBJECTS = "subjects";
		public static final String BOARD_ID = "boardId";
		public static final String BOARD_IDS = "boardIds";
		public static final String DESCRIPTION = "description";
		public static final String PRICE = "price";
		public static final String DISPLAY_PRICE = "displayPrice";
		public static final String CURRENCY_CODE = "currencyCode";
		public static final String RECOMMENDED_SCHEDULE = "recommendedSchedule";
		public static final String TOTAL_DAYS = "totalDays";
		public static final String TOTAL_HOURS = "totalHours";
		public static final String IMAGE_URLS = "imageUrls";
		public static final String VIDEO_URLS = "videoUrls";
		public static final String CONTENTS = "contents";
		public static final String TYPE = "type";
		public static final String STATE = "state";
		public static final String TAGS = "tags";
		public static final String EXAM_TAGS = "examTags";
		public static final String EXAM = "exam";
		public static final String TEACHER_IDS = "teacherIds";
		public static final String CATEGORIES = "categories";
		public static final String PARENT_TOPICS = "parentTopics";
		public static final String PRIORITY = "priority";
		public static final String TAKEAWAYS = "takeAways";
		public static final String IS_FEATURED = "isFeatured";
		public static final String OFFERING_LENGTH = "offeringLength";

		public static final String TARGET_AUDIENCE_TEXT = "targetAudienceText";
		public static final String PER_HR_PRICE = "perHrPrice";

		public static final String OFFERING_SUB_TYPE = "subtypes";

	}

	@Override
	public Set<String> _getBoardIdsInString() {
		// TODO Auto-generated method stub
		return boardIds;
	}
}
