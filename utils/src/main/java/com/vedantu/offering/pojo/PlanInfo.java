package com.vedantu.offering.pojo;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.subscription.SubscriptionState;

public class PlanInfo {
	private Long id;
	private String name;
	private Integer rate;
	private Integer displayRate;
	private Integer hours;
	private String currency;
	private Integer maxHour;
	private SubscriptionState state;

	private Long teacherId;
	private Long offeringId;

	private Integer scheduleDays;
	private ScheduleType scheduleType;
	// this value will only be used when the onSale value true
	private String saleId;

	private PaymentType paymentType;

	public PlanInfo() {
		super();

	}

	// public PlanInfo(Plan plan) {
	// super();
	// this.id = plan.getId();
	// this.name = plan.getName();
	// this.rate = plan.getRate();
	// this.displayRate = plan.getDisplayRate();
	// this.hours = plan.getHours();
	// this.currency = plan.getCurrency();
	// this.maxHour = plan.getMaxHour();
	// this.state = plan.getState();
	// this.teacherId = plan.getTeacherId();
	// this.offeringId = plan.getOfferingId();
	// this.scheduleDays = plan.getScheduleDays();
	// this.scheduleType = plan.getScheduleType();
	// this.paymentType = plan.getPaymentType();
	//
	// }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getDisplayRate() {
		return displayRate;
	}

	public void setDisplayRate(int displayRate) {
		this.displayRate = displayRate;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSaleId() {
		return saleId;
	}

	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}

	public int getMaxHour() {
		return maxHour;
	}

	public void setMaxHour(int maxHour) {
		this.maxHour = maxHour;
	}

	public SubscriptionState getState() {
		return state;
	}

	public void setState(SubscriptionState state) {
		this.state = state;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Integer getScheduleDays() {
		return scheduleDays;
	}

	public ScheduleType getScheduleType() {
		return scheduleType;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	@Override
	public String toString() {
		return String
				.format("{id=%s, name=%s, rate=%s, displayRate=%s, hours=%s, currency=%s, maxHour=%s, state=%s, teacherId=%s, offeringId=%s, scheduleDays=%s, scheduleType=%s, saleId=%s, paymentType=%s}",
						id, name, rate, displayRate, hours, currency, maxHour,
						state, teacherId, offeringId, scheduleDays,
						scheduleType, saleId, paymentType);
	}
}
