package com.vedantu.offering.response;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.offering.pojo.OfferingInfo;
import com.vedantu.util.fos.response.AbstractRes;

public class GetOfferingsResponse extends AbstractRes {
		public List<OfferingInfo> offeringInfoList = new ArrayList<OfferingInfo>();
		
		public List<OfferingInfo> getOfferingInfoList() {
			return offeringInfoList;
		}

		public void setOfferingInfoList(List<OfferingInfo> offeringInfoList) {
			this.offeringInfoList = offeringInfoList;
		}

		@Override
		public String toString() {
			return "GetOfferingsRes {offeringInfoList:" + offeringInfoList + "}";
		}
}
