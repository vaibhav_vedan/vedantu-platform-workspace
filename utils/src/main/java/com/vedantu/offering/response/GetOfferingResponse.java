package com.vedantu.offering.response;

import java.util.List;

import com.vedantu.offering.pojo.OfferingInfo;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.fos.response.AbstractRes;

public class GetOfferingResponse extends AbstractRes {

	private OfferingInfo offeringInfo;

	private List<SessionInfo> sessionsInfo;

	public OfferingInfo getOfferingInfo() {
		return offeringInfo;
	}

	public void setOfferingInfo(OfferingInfo offeringInfo) {
		this.offeringInfo = offeringInfo;
	}

	public List<SessionInfo> getSessionsInfo() {
		return sessionsInfo;
	}

	public void setSessionsInfo(List<SessionInfo> sessionsInfo) {
		this.sessionsInfo = sessionsInfo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{offeringInfo=").append(offeringInfo)
				.append(", sessionsInfo=").append(sessionsInfo).append("}");
		return builder.toString();
	}

}
