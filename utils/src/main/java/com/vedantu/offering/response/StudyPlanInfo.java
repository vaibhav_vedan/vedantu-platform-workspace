package com.vedantu.offering.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.fos.response.AbstractRes;

public class StudyPlanInfo extends AbstractRes {

	private String id;
	private String offeringId;
	private String targetUserId;
	private String note;
	private Long targetDate;
	private UserBasicInfo user;
	private Long lastUpdated;
	private VisibilityState state;
	private String name;
	private Long noOfSessions;
	private String status;

	public StudyPlanInfo() {
		super();
	}

	public String getId() {
		return id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public String getTargetUserId() {
		return targetUserId;
	}

	public String getNote() {
		return note;
	}

	public UserBasicInfo getUser() {
		return user;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public Long getTargetDate() {
		return targetDate;
	}

	public VisibilityState getState() {
		return state;
	}

	public String getName() {
		return name;
	}

	public Long getNoOfSessions() {
		return noOfSessions;
	}

	public String getStatus() {
		return status;
	}
	
	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "{ id=" + id + ", offeringId=" + offeringId + ", targetUserId="
				+ targetUserId + ", note=" + note + ", targetDate="
				+ targetDate + ", user=" + user + ", lastUpdated="
				+ lastUpdated + ", state=" + state + ", name=" + name
				+ ", noOfSessions=" + noOfSessions + ", status=" + status + "}";
	}
	
	public static class Constants {

		public static final String OFFERING_ID = "offeringId";
		public static final String TARGET_USER_ID = "targetUserId";
		public static final String TARGET_DATE = "targetDate";
		public static final String NOTE = "note";

	}

}
