package com.vedantu.offering.response;

import java.util.List;

import com.vedantu.offering.pojo.Offering;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.fos.response.AbstractRes;

public class CreateOfferingResponse extends AbstractRes {

	private String title;
	private String description;
	private String targetAudienceText;
	private Long perHrPrice;
	private Long price;
	private Long displayPrice;
	private List<String> tags;
	private String recommendedSchedule;
	private Long totalDays;
	private Long totalHours;
	private OfferingType type;
	private VisibilityState state;
	private List<String> imageUrls;
	private List<String> videoUrls;
	private List<String> contents;
	private List<String> categories;
	private List<String> parentTopics;
	private Integer priority;
	private List<String> takeAways;
	private String offeringLength;

	public VisibilityState getState() {
		return state;
	}

	public List<String> getTakeAways() {
		return takeAways;
	}

	public List<String> getContents() {
		return contents;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getTargetAudienceText() {
		return targetAudienceText;
	}

	public Long getPerHrPrice() {
		return perHrPrice;
	}

	public Long getPrice() {
		return price;
	}

	public Long getDisplayPrice() {
		return displayPrice;
	}

	public List<String> getTags() {
		return tags;
	}

	public String getRecommendedSchedule() {
		return recommendedSchedule;
	}

	public Long getTotalDays() {
		return totalDays;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public OfferingType getType() {
		return type;
	}

	public List<String> getImageUrls() {
		return imageUrls;
	}

	public List<String> getVideoUrls() {
		return videoUrls;
	}

	public List<String> getCategories() {
		return categories;
	}

	public List<String> getParentTopics() {
		return parentTopics;
	}

	public Integer getPriority() {
		return priority;
	}

	public String getOfferingLength() {
		return offeringLength;
	}

	public CreateOfferingResponse(Offering offering) {

		if (null == offering) {
			return;
		}
		this.description = offering.getDescription();
		this.tags = offering.getTags();
		this.targetAudienceText = offering.getTargetAudienceText();
		this.perHrPrice = offering.getPerHrPrice();
		this.displayPrice = offering.getDisplayPrice();
		this.price = offering.getPrice();
		this.title = offering.getTitle();
		this.recommendedSchedule = offering.getRecommendedSchedule();
		this.totalDays = offering.getTotalDays();
		this.totalHours = offering.getTotalHours();
		this.type = offering.getType();
		this.state = offering.getState();
		this.imageUrls = offering.getImageUrls();
		this.videoUrls = offering.getVideoUrls();
		this.contents = offering.getContents();
		this.categories = offering.getCategories();
		this.parentTopics = offering.getParentTopics();
		this.priority = offering.getPriority();
		this.takeAways = offering.getTakeAways();
		this.offeringLength = offering.getOfferingLength();
	}

	@Override
	public String toString() {
		return String
				.format("{title=%s, description=%s, targetAudienceText=%s, perHrPrice=%s, price=%s, displayPrice=%s, tags=%s, recommendedSchedule=%s, totalDays=%s, totalHours=%s, type=%s, state=%s, imageUrls=%s, videoUrls=%s, contents=%s, categories=%s, parentTopics=%s, priority=%s, takeAways=%s, offeringLength=%s}",
						title, description, targetAudienceText, perHrPrice,
						price, displayPrice, tags, recommendedSchedule,
						totalDays, totalHours, type, state, imageUrls,
						videoUrls, contents, categories, parentTopics,
						priority, takeAways, offeringLength);
	}
}
