package com.vedantu.offering.response;

import java.util.List;

import com.vedantu.offering.pojo.Offering;
import com.vedantu.util.enums.OfferingType;
import com.vedantu.util.fos.response.AbstractRes;

public class EditOfferingResponse extends AbstractRes {

	private String offeringId;
	private List<String> editList;
	private String title;
	private List<String> grades;
	private String description;
	private Long price;
	private Long displayPrice;
	private String recommendedSchedule;
	private List<String> tags;
	private Long totalDays;
	private Long totalHours;
	private OfferingType type;
	private List<String> imageUrls;
	private List<String> videoUrls;
	private List<String> contents;
	private List<String> teacherIds;
	private List<String> categories;
	private List<String> parentTopics;
	private Integer priority;
	List<String> takeAways;

	public EditOfferingResponse() {

	}

	public EditOfferingResponse(Offering offering) {
		if (offering == null) {
			return;
		}
		this.offeringId = offering.getId();
		this.title = offering.getTitle();
		this.grades = offering.getGrades();
		if (offering.getDescription() != null) {
			this.description = offering.getDescription();
		}
		this.price = offering.getPrice();
		this.displayPrice = offering.getDisplayPrice();
		if (offering.getRecommendedSchedule() != null) {
			this.recommendedSchedule = offering.getRecommendedSchedule();
		}
		this.tags = offering.getTags();
		this.totalDays = offering.getTotalDays();
		this.totalHours = offering.getTotalHours();
		this.type = offering.getType();
		this.imageUrls = offering.getImageUrls();
		this.videoUrls = offering.getVideoUrls();
		this.contents = offering.getContents();
		this.teacherIds = offering.getTeacherIds();
		this.categories = offering.getCategories();
		this.parentTopics = offering.getParentTopics();
		this.priority = offering.getPriority();
		this.takeAways = offering.getTakeAways();
	}

	public List<String> getTakeAways() {
		return takeAways;
	}

	public void setTakeAways(List<String> takeAways) {
		this.takeAways = takeAways;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public List<String> getEditList() {
		return editList;
	}

	public void setEditList(List<String> editList) {
		this.editList = editList;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getGrades() {
		return grades;
	}

	public void setGrades(List<String> grades) {
		this.grades = grades;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(Long displayPrice) {
		this.displayPrice = displayPrice;
	}

	public String getRecommendedSchedule() {
		return recommendedSchedule;
	}

	public void setRecommendedSchedule(String recommendedSchedule) {
		this.recommendedSchedule = recommendedSchedule;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Long getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(Long totalDays) {
		this.totalDays = totalDays;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public OfferingType getType() {
		return type;
	}

	public void setType(OfferingType type) {
		this.type = type;
	}

	public List<String> getImageUrls() {
		return imageUrls;
	}

	public void setImageUrls(List<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

	public List<String> getVideoUrls() {
		return videoUrls;
	}

	public void setVideoUrls(List<String> videoUrls) {
		this.videoUrls = videoUrls;
	}

	public List<String> getContents() {
		return contents;
	}

	public void setContents(List<String> contents) {
		this.contents = contents;
	}

	public List<String> getTeacherIds() {
		return teacherIds;
	}

	public void setTeacherIds(List<String> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setExams(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getParentTopics() {
		return parentTopics;
	}

	public void setParentTopics(List<String> parentTopics) {
		this.parentTopics = parentTopics;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return String
				.format("{offeringId=%s, editList=%s, title=%s, grades=%s, description=%s, price=%s, displayPrice=%s, recommendedSchedule=%s, tags=%s, totalDays=%s, totalHours=%s, type=%s, imageUrls=%s, videoUrls=%s, contents=%s, teacherIds=%s, categories=%s, parentTopics=%s, priority=%s, takeAways=%s}",
						offeringId, editList, title, grades, description,
						price, displayPrice, recommendedSchedule, tags,
						totalDays, totalHours, type, imageUrls, videoUrls,
						contents, teacherIds, categories, parentTopics,
						priority, takeAways);
	}
}
