package com.vedantu.aws;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.misc.*;

@Service
public class AbstractAwsSQSManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AbstractAwsSQSManager.class);

    public AmazonSQSAsync sqsClient;
    private String arn;
    private String env;
    private String queueArnBuilder;
    private String queueUrlBuilder;

    public AbstractAwsSQSManager() {
        logger.info("initializing AwsSQSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
            queueArnBuilder = ConfigUtils.INSTANCE.getStringValue("aws.sqs.arn.builder");
            queueUrlBuilder = ConfigUtils.INSTANCE.getStringValue("aws.sqs.url.builder");
            
            sqsClient = AmazonSQSAsyncClientBuilder.standard()
                    .withEndpointConfiguration(new EndpointConfiguration("sqs.eu-west-1.amazonaws.com",
                            Region.getRegion(Regions.EU_WEST_1).getName())).build();
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

    }


    public void sendToSQS(SQSQueue queue, SQSMessageType messageType, String message) {
        sendToSQS(queue, messageType, message, null);
    }
    
    
    
    public String getQueueArn(SQSQueue queue) {
        return String.format(queueArnBuilder,Regions.EU_WEST_1.getName(), arn, queue.getQueueName(env));
    }
    
    public String getQueueURL(SQSQueue queue) {
        return String.format(queueUrlBuilder,Regions.EU_WEST_1.getName(), arn, queue.getQueueName(env));
    }

    public SendMessageResult sendToSQS(SQSQueue queue, SQSMessageType messageType,
                                       String message, String messageGroupId) {
        
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return null;
        }
        SendMessageRequest sendMessageRequest = new SendMessageRequest();

        String queueUrl = getQueueURL(queue);
        logger.info("Queue url for " + queue +  " is " + queueUrl);
        if (queue.getFifo()) {
            // You must provide a non-empty MessageGroupId when sending messages to a FIFO queue
            if (StringUtils.isEmpty(messageGroupId)) {
                messageGroupId = UUID.randomUUID().toString();
            }
            sendMessageRequest.setMessageGroupId(messageGroupId);
            sendMessageRequest.setMessageDeduplicationId(UUID.randomUUID().toString());
        }

        sendMessageRequest.setQueueUrl(queueUrl);
        Map<String, MessageAttributeValue> attributeMap = new HashMap<>();
        attributeMap.put("messageType", new MessageAttributeValue().withDataType("String").withStringValue(messageType.toString()));
        sendMessageRequest.setMessageAttributes(attributeMap);
        sendMessageRequest.setMessageBody(message);
        logger.info("sendMessageRequest: " + sendMessageRequest);
        SendMessageResult sendMessageResult = sqsClient.sendMessage(sendMessageRequest);
        String sequenceNumber = sendMessageResult.getSequenceNumber();
        String messageId = sendMessageResult.getMessageId();
        logger.info("SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber);
        return sendMessageResult;
    }

    public ReceiveMessageResult recieveMessageFromQueueManual(ReceiveMessageRequest recieveMessageRequest) {
        return sqsClient.receiveMessage(recieveMessageRequest);
    }
    
    public void deleteMessageByHandle(String queueUrlString,String receiptHandle){
        sqsClient.deleteMessage(queueUrlString, receiptHandle);
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (sqsClient != null) {
                sqsClient.shutdown();
            }
            com.amazonaws.http.IdleConnectionReaper.shutdown();
            AwsSdkMetrics.unregisterMetricAdminMBean();            
        } catch (Exception e) {
            logger.error("Error in closing sqs connection ", e);
        }
    }

}
