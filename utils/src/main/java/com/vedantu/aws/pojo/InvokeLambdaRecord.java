package com.vedantu.aws.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvokeLambdaRecord {
	/*
	 * {\ "Records": [\ {\ "EventVersion": "1.0",\ "EventSubscriptionArn":
	 * "arn:aws:sns:EXAMPLE",\ "EventSource": "aws:sns",\ "Sns": {\
	 * "SignatureVersion": "1",\ "Timestamp": "1970-01-01T00:00:00.000Z",\
	 * "Signature": "EXAMPLE",\ "SigningCertUrl": "EXAMPLE",\ "MessageId":
	 * "95df01b4-ee98-5cb9-9903-4c221d41eb5e",\ "Message": "{\\"sessionId\\":
	 * 
	 * payload2= }",\ "MessageAttributes": {\ "Test": {\ "Type": "String",\
	 * "Value": "TestString"\ },\ "TestBinary": {\ "Type": "Binary",\ "Value":
	 * "TestBinary"\ }\ },\ "Type": "Notification",\ "UnsubscribeUrl":
	 * "EXAMPLE",\ "TopicArn": "arn:aws:sns:EXAMPLE",\ "Subject":
	 * "REPLAY_SESSION_PREPARED"\ }\ }\ ]\ }
	 * 
	 */

	private String eventVersion = "1.0";
	private String eventSubscriptionArn = "arn:aws:sns:EXAMPLE";
	private String eventSource = "aws:sns";
	private InvokeLambdaSNSreq sns;

	public InvokeLambdaRecord(String subject, String message) {
		super();
		this.sns = new InvokeLambdaSNSreq(subject, message);
	}

	@JsonProperty("EventVersion")
	public String getEventVersion() {
		return eventVersion;
	}

	@JsonProperty("EventSubscriptionArn")
	public String getEventSubscriptionArn() {
		return eventSubscriptionArn;
	}

	@JsonProperty("EventSource")
	public String getEventSource() {
		return eventSource;
	}

	@JsonProperty("Sns")
	public InvokeLambdaSNSreq getSns() {
		return sns;
	}

	@Override
	public String toString() {
		return "InvokeLambdaReq [eventVersion=" + eventVersion + ", eventSubscriptionArn=" + eventSubscriptionArn
				+ ", eventSource=" + eventSource + ", sns=" + sns + ", toString()=" + super.toString() + "]";
	}
}
