/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.aws.pojo;

/**
 *
 * @author jeet
 */
public enum CronTopic {
    CRON_CHIME_1_Minute,
    CRON_CHIME_3_Minutes,
    CRON_CHIME_4_Minutes,
    CRON_CHIME_5_Minutes,
    CRON_CHIME_15_Minutes,
    CRON_CHIME_30_Minutes,
    CRON_CHIME_HOURLY,
    CRON_CHIME_2HOURLY,
    CRON_CHIME_3HOURLY,
    CRON_CHIME_DAILY,
    CRON_CHIME_DAILY_1AM_IST,
    CRON_CHIME_DAILY_2PM_IST,
    CRON_CHIME_DAILY_3PM_IST,
    CRON_CHIME_DAILY_11PM,
    CRON_CHIME_WEEKLY,
    CRON_CHIME_FORTNIGHTLY,
    CRON_CHIME_MONTHLY,
    ADMIN_ALERTS,
    CRON_CHIME_LISTING_DAILY_COUNT,
    ISL_csv_uploaded,
    MyNotify,
    NotifyMe,
    TEST_CRON,
    TEST,
    DELETE_GTT_RECORDING,
    Devops,
    OTF_SESSION_FEEDBACK_GENERATED,
    UPDATE_GTT_SESSION_DATA,
    Test,
    CRON_CHIME_DAILY_6_30AM_IST;

    public String getTopicName(String env) {
        String queueName = this.name() + "_" + env.toUpperCase();
        return queueName;
    }
}
