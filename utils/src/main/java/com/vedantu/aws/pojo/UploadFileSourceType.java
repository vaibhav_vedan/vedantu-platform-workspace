package com.vedantu.aws.pojo;

public enum UploadFileSourceType {
	CMDS_ANSWER_UPLOAD, VSAT_RESULT_SALES, TEACHER_IMAGE_UPLOAD
}
