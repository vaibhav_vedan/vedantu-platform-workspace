package com.vedantu.aws.pojo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvokeLambdaReq {
	private List<InvokeLambdaRecord> Records;

	public InvokeLambdaReq() {
		super();
		Records = new ArrayList<>();
	}

	@JsonProperty("Records")
	public List<InvokeLambdaRecord> getRecords() {
		return Records;
	}

	public void setRecords(List<InvokeLambdaRecord> records) {
		Records = records;
	}

	public void addRecords(InvokeLambdaRecord record) {
		this.Records.add(record);
	}

	@Override
	public String toString() {
		return "InvokeLambdaReq [Records=" + Records + ", toString()=" + super.toString() + "]";
	}
}