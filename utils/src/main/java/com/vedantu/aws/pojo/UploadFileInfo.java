package com.vedantu.aws.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UploadFileInfo {
	private String fileName;
	private String contentType;
}
