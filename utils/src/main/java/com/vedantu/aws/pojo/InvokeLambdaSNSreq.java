package com.vedantu.aws.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvokeLambdaSNSreq {
	/*
	 * "Sns": {\ "SignatureVersion": "1",\ "Timestamp":
	 * "1970-01-01T00:00:00.000Z",\ "Signature": "EXAMPLE",\ "SigningCertUrl":
	 * "EXAMPLE",\ "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",\
	 * "Message": "{\\"sessionId\\":
	 * 
	 * payload2= }",\ "MessageAttributes": {\ "Test": {\ "Type": "String",\
	 * "Value": "TestString"\ },\ "TestBinary": {\ "Type": "Binary",\ "Value":
	 * "TestBinary"\ }\ },\ "Type": "Notification",\ "UnsubscribeUrl":
	 * "EXAMPLE",\ "TopicArn": "arn:aws:sns:EXAMPLE",\ "Subject":
	 * "REPLAY_SESSION_PREPARED"\ }\
	 * 
	 */

	private String signatureVersion = "1";
	private String Timestamp = "1970-01-01T00:00:00.000Z";
	private String Signature = "EXAMPLE";
	private String SigningCertUrl = "EXAMPLE";
	private String messageId = "95df01b4-ee98-5cb9-9903-4c221d41eb5e";
	private String message;
	private MessageAttributes MessageAttributes = new MessageAttributes();
	private String type = "Notification";
	private String unsubscribeUrl = "EXAMPLE";
	private String topicArn = "arn:aws:sns:EXAMPLE";
	private String subject;

	public InvokeLambdaSNSreq(String subject, String message) {
		super();
		this.subject = subject;
		this.message = message;
	}

	@JsonProperty("SignatureVersion")
	public String getSignatureVersion() {
		return signatureVersion;
	}

	public void setSignatureVersion(String signatureVersion) {
		this.signatureVersion = signatureVersion;
	}

	@JsonProperty("Timestamp")
	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	@JsonProperty("Signature")
	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

	@JsonProperty("SigningCertUrl")
	public String getSigningCertUrl() {
		return SigningCertUrl;
	}

	public void setSigningCertUrl(String signingCertUrl) {
		SigningCertUrl = signingCertUrl;
	}

	@JsonProperty("MessageId")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@JsonProperty("Message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@JsonProperty("MessageAttributes")
	public MessageAttributes getMessageAttributes() {
		return MessageAttributes;
	}

	public void setMessageAttributes(MessageAttributes messageAttributes) {
		MessageAttributes = messageAttributes;
	}

	@JsonProperty("Type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("UnsubscribeUrl")
	public String getUnsubscribeUrl() {
		return unsubscribeUrl;
	}

	public void setUnsubscribeUrl(String unsubscribeUrl) {
		this.unsubscribeUrl = unsubscribeUrl;
	}

	@JsonProperty("TopicArn")
	public String getTopicArn() {
		return topicArn;
	}

	public void setTopicArn(String topicArn) {
		this.topicArn = topicArn;
	}

	@JsonProperty("Subject")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	private class MessageAttributes {
		private keyValueTuple test = new keyValueTuple("String", "TestString");
		private keyValueTuple testBinary = new keyValueTuple("Binary", "TestBinary");

		@JsonProperty("Test")
		public keyValueTuple getTest() {
			return test;
		}

		@JsonProperty("TestBinary")
		public keyValueTuple getTestBinary() {
			return testBinary;
		}

		@Override
		public String toString() {
			return "MessageAttributes [Test=" + test + ", TestBinary=" + testBinary + ", toString()=" + super.toString()
					+ "]";
		}
	}

	private class keyValueTuple {
		private String type;
		private String value;

		public keyValueTuple(String type, String value) {
			super();
		}

		@JsonProperty("Type")
		public String getType() {
			return type;
		}

		@JsonProperty("Value")
		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return "keyValueTuple [type=" + type + ", value=" + value + ", toString()=" + super.toString() + "]";
		}
	}

	@Override
	public String toString() {
		return "InvokeLambdaSNSreq [signatureVersion=" + signatureVersion + ", Timestamp=" + Timestamp + ", Signature="
				+ Signature + ", SigningCertUrl=" + SigningCertUrl + ", messageId=" + messageId + ", message=" + message
				+ ", MessageAttributes=" + MessageAttributes + ", type=" + type + ", unsubscribeUrl=" + unsubscribeUrl
				+ ", topicArn=" + topicArn + ", toString()=" + super.toString() + "]";
	}
}
