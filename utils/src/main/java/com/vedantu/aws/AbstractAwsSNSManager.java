/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import com.vedantu.util.enums.SNSTopicIFace;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author jeet
 */

abstract public class AbstractAwsSNSManager {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AbstractAwsSNSManager.class);

    private String arn;
    public String env;
    public String SELF_ENDPOINT;
    private String snsArnBuilder;
    public AmazonSNSAsync snsClient;

    public AbstractAwsSNSManager() {
        logger.info("Initializing AbstractAwsSNSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
            SELF_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SELF_ENDPOINT");
            snsArnBuilder = ConfigUtils.INSTANCE.getStringValue("aws.sns.arn.builder");
            snsClient = AmazonSNSAsyncClientBuilder.standard()
                            .withRegion(Regions.AP_SOUTHEAST_1)
                            .build();
        } catch (Exception e) {
            logger.error("Error in initializing AbstractAwsSNSManager " + e.getMessage());
        }
    }
    public String getTopicArn(SNSTopic topic) {
        return String.format(snsArnBuilder,Regions.AP_SOUTHEAST_1.getName(), arn, topic.getTopicName(env));
    }
    public String getTopicArn(SNSTopicIFace topic) {
        return String.format(snsArnBuilder,Regions.AP_SOUTHEAST_1.getName(), arn, topic.getTopicName(env));
    }

    public String getCronArn(CronTopic topic) {
        return String.format(snsArnBuilder,Regions.AP_SOUTHEAST_1.getName(), arn, topic.name());
    }
    public void triggerSNS(SNSTopic topic, String subject, String message) {
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":"+topic.getTopicName(env);
        PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
        logger.info("publishRequest "+publishRequest);
        if (StringUtils.isEmpty(env) || "LOCAL".equals(env)) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }
    public void triggerSNS(CronTopic topic, String subject, String message) {
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":"+topic.name();
        PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
        logger.info("publishRequest "+publishRequest);
        if (StringUtils.isEmpty(env) || "LOCAL".equals(env)) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }

    public void createCronSubscription(CronTopic topic, String url){
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not creating any crons subscription");
            return;
        }
        if(StringUtils.isEmpty(url) ||  topic == null){
            logger.error("Got empty url or null cron while creating subscription cron:"+topic+" url:"+url);
            return;
        }
        logger.info("Creating cron subscription");
        String endpoint = SELF_ENDPOINT.trim().endsWith("/") && url.trim().startsWith("/")
                ? SELF_ENDPOINT + url.replaceFirst("/", "")
                : SELF_ENDPOINT + url;
        SubscribeRequest sr = new SubscribeRequest(getCronArn(topic), "https", endpoint);
        logger.info("cron subscription req:"+sr);
        try{
            logger.info("req:"+sr);
            SubscribeResult r=snsClient.subscribe(sr);
            logger.info("Cron subscription result:"+r);
        } catch (Exception er){
            logger.error("there was a error creating cron subscription:"+sr+" error:"+er.getMessage());
        }
    }

    public void createCronSubscription(SNSTopicIFace topic, String url){
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not creating any crons subscription");
            return;
        }
        if(StringUtils.isEmpty(url) ||  topic == null){
            logger.error("Got empty url or null cron while creating subscription cron:"+topic+" url:"+url);
            return;
        }
        logger.info("Creating cron subscription");
        String endpoint = SELF_ENDPOINT.trim().endsWith("/") && url.trim().startsWith("/")
                ? SELF_ENDPOINT + url.replaceFirst("/", "")
                : SELF_ENDPOINT + url;

        SubscribeRequest sr = new SubscribeRequest(getTopicArn(topic), "https", endpoint);
        logger.info("cron subscription req:"+sr);
        try{
            logger.info("req:"+sr);
            SubscribeResult r=snsClient.subscribe(sr);
            logger.info("Cron subscription result:"+r);
        } catch (Exception er){
            logger.error("there was a error creating cron subscription:"+sr+" error:"+er.getMessage());
        }
    }

    protected void createSNSTopic(SNSTopicIFace topic){
        if (StringUtils.isEmpty(env) || "LOCAL".equals(env)) {
            logger.info("Env is local, so not creating any topics");
            return;
        }
        if(topic == null){
            logger.error("Got request to create a null topic");
            return;
        }
        logger.info("Creating sns topic"+topic);
        CreateTopicRequest createTopicRequest = new CreateTopicRequest(topic.getTopicName(env));
        logger.info("create topic request:"+createTopicRequest);
        try{
            snsClient.createTopic(createTopicRequest);
        } catch (Exception er){
            logger.error("there was a error creating topic:"+topic+" error:"+er.getMessage());
        }
    }

    protected void createSNSTopic(CronTopic topic){
        if (StringUtils.isEmpty(env) || "LOCAL".equals(env)) {
            logger.info("Env is local, so not creating any topics");
            return;
        }
        if(topic == null){
            logger.error("Got request to create a null topic");
            return;
        }
        logger.info("Creating sns topic"+topic);
        CreateTopicRequest createTopicRequest = new CreateTopicRequest(topic.name());
        logger.info("create topic request:"+createTopicRequest);
        try{
            snsClient.createTopic(createTopicRequest);
        } catch (Exception er){
            logger.error("there was a error creating topic:"+topic+" error:"+er.getMessage());
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing sqs connection "+e.getMessage());
        }
    }

    @PostConstruct
    private void postConstruct(){
        logger.info("Trying to create topics and subscriptions.");
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        logger.info("Created a thread pool for the same.");
        CompletableFuture.runAsync(() -> {
            logger.info("Asynchronously creating the same.");
            try {
                logger.info("Going for creation of sns topics");
                createTopics();
                logger.info("Going for creation of cron subscription");
                createSubscriptions();
            } catch (Exception e) {
                logger.error("Error in postContruct " + e);
            }
            finally {
                logger.info("Shutting down the executor service.");
                executorService.shutdownNow();
            }
        }, executorService);
        logger.info("Post construct done.");
    }
    
    abstract public void createTopics();
    abstract public void createSubscriptions();
}