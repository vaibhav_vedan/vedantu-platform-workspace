package com.vedantu.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.ComparisonOperator;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmRequest;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmResult;
import com.amazonaws.services.cloudwatch.model.Statistic;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
public class AwsCloudWatchManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsCloudWatchManager.class);

    private AmazonCloudWatch amazonCloudWatch;

    private String env;

    private String arn;

    private String serviceName;

    private AmazonSQS sqsClient;

    private Regions region = Regions.AP_SOUTHEAST_1;

    public AwsCloudWatchManager(){

        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        serviceName = ConfigUtils.INSTANCE.getStringValue("application.name").toUpperCase();

        configureAWSClients();

    }

    public AwsCloudWatchManager(Regions regions) {

        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        serviceName = ConfigUtils.INSTANCE.properties.getProperty("application.name").toUpperCase();

        this.region = regions;

        configureAWSClients();
    }

    public void configureAWSClients(){

        this.amazonCloudWatch = AmazonCloudWatchClientBuilder.standard()
                .withRegion(region)
                .build();

        this.sqsClient = AmazonSQSClientBuilder.standard()
                .withRegion(region)
                .build();
    }

    private void createSqsAlarm(String alarmName, String metrixName, Double threshold, String queueName, int period, String description, String topic, Regions region) {

        Dimension dimension = new Dimension()
                .withName("QueueName")
                .withValue(queueName);

        PutMetricAlarmRequest request = new PutMetricAlarmRequest();
        request.setMetricName(metrixName);
        request.setAlarmName(alarmName);
        request.setComparisonOperator(ComparisonOperator.GreaterThanThreshold);
        request.setPeriod(period);
        request.setStatistic(Statistic.Average);
        request.withNamespace("AWS/SQS");
        request.withThreshold(threshold);
        request.withDatapointsToAlarm(3);
        request.setEvaluationPeriods(3);
        request.withDimensions(dimension);
        request.setTreatMissingData("notBreaching");
        request.setAlarmDescription(description);
        request.withActionsEnabled(true);
        request.setAlarmActions(Arrays.asList("arn:aws:sns:" + region.getName().toLowerCase() + ":" + arn + ":" + topic));

        PutMetricAlarmResult response = amazonCloudWatch.putMetricAlarm(request);

    }

    private void creatAlarm(SQSQueue sqsQueue) {

        if (canCreateAlarm()) {
            logger.info("Creating alarm for {}", sqsQueue.getQueueName(env));
            String queueName = sqsQueue.getQueueName(env);
            creatAlarmByRegion(queueName, sqsQueue.getVisibleThreshold(), sqsQueue.getAgeThreshold(), Regions.AP_SOUTHEAST_1);
        }

    }

    private boolean canCreateAlarm() {
        return !StringUtils.isEmpty(env) && env.equalsIgnoreCase("prod");
    }

    public void creatAlarmByRegion(String queueName, Double visibleThreshold, Double ageThreshold, Regions regions) {

        int period = DateTimeUtils.SECONDS_PER_MINUTE * 5;

        String visibleAlarmName = "ALARM_SQS_" + serviceName + "_VISIBLE_" + queueName;
        String visibleMetrixName = "ApproximateNumberOfMessagesVisible";

        String description = visibleAlarmName;
        String topic = getTopicForAlarm(serviceName);

        createSqsAlarm(visibleAlarmName, visibleMetrixName, visibleThreshold, queueName, period, description, topic, regions);

        logger.info("Created alarm for {}", visibleAlarmName);

        String ageAlarmName = "ALARM_SQS_" + serviceName + "_AGE_" + queueName;
        String ageMetrixName = "ApproximateAgeOfOldestMessage";

        description = ageAlarmName;

        createSqsAlarm(ageAlarmName, ageMetrixName, ageThreshold, queueName, period, description, topic, regions);

        logger.info("Created alarm for {}", ageAlarmName);

    }

    public void creatAlarmServiceSpecific(SQSQueue sqsQueue, String service) {

        serviceName = service;
        creatAlarm(sqsQueue);

    }

    public void creatAlarm(String sqsQueue, Double visibleThreshold, Double ageThreshold, String service, Regions regions) {

        if (canCreateAlarm()) {
            serviceName = service;
            creatAlarmByRegion(sqsQueue, visibleThreshold, ageThreshold, regions);
        }
    }

    public void createAlarms(Set<SQSQueue> queueListToCreateAlarm) {

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        CompletableFuture.runAsync(() -> {
            try {
                for (SQSQueue sqsQueue : queueListToCreateAlarm) {
                    creatAlarm(sqsQueue);
                }

                logger.info("Alarm created for the Enum list {}", queueListToCreateAlarm);

                if("VEDANTUDATA".equalsIgnoreCase(serviceName) && env.equalsIgnoreCase("prod")){

                    AwsCloudWatchManager cloudWatchManager = new AwsCloudWatchManager(Regions.AP_SOUTH_1);
                    cloudWatchManager.createAlarmsForAllManuallyCreatedQueues();

                    cloudWatchManager = new AwsCloudWatchManager(Regions.AP_SOUTHEAST_1);
                    cloudWatchManager.createAlarmsForAllManuallyCreatedQueues();

                    cloudWatchManager = new AwsCloudWatchManager(Regions.EU_WEST_1);
                    cloudWatchManager.createAlarmsForAllManuallyCreatedQueues();

                }

            } catch (Exception e) {
                logger.warn("Error in createAlarms " + e.getMessage(), e);
            } finally {
                executorService.shutdownNow();
            }
        }, executorService);
    }

    public void createAlarmsForAllManuallyCreatedQueues(){

        try {

            List<String> queueNamesToExclude = getIgnorableQueues(env);
            queueNamesToExclude.addAll(getIgnorableQueues("qa"));
            queueNamesToExclude.addAll(getIgnorableQueues("dev2"));

            logger.info("queueNamesToExclude : {}", queueNamesToExclude.size());

            List<String> allQueues = getAllQueuesFromAws();

            logger.info("allQueues : {}", allQueues.size());

            allQueues = allQueues.parallelStream()
                    .filter(queue -> !queueNamesToExclude.contains(queue))
                    .collect(Collectors.toList());

            logger.info("allQueues final list : {}", allQueues.size());

            for(String sqsQueue : allQueues){

                Double visibleThreshold = 1000d;
                Double ageThreshold = 21600d;

                if(Regions.AP_SOUTH_1.equals(region) || sqsQueue.toLowerCase().endsWith("deadletter_" + env) || sqsQueue.toLowerCase().endsWith("dl_" + env) ) {
                    visibleThreshold = 10d;

                    if(sqsQueue.contains("VEDANTU_ETL") || sqsQueue.equalsIgnoreCase("test-etl-python-prod-clone.fifo")){
                        visibleThreshold = 100d;
                    }
                }

                creatAlarm(sqsQueue, visibleThreshold, ageThreshold, "CORE_TECH", region);
            }

        }catch (Exception e){
            logger.warn("Error creating alarm : ", e);
        }

    }

    private List<String> getIgnorableQueues(String envName){
        List<SQSQueue> allDefinedQueues = Arrays.asList(SQSQueue.values());

        logger.info("allDefinedQueues : {}", allDefinedQueues.size());

        return allDefinedQueues.parallelStream()
                .map(queue -> queue.getQueueName(envName))
                .collect(Collectors.toList());
    }

    public List<String> getAllQueuesFromAws(){

        ListQueuesResult lq_result  = sqsClient.listQueues();
        List<String> queueUrls = lq_result.getQueueUrls();

        List<String> queues = new ArrayList<>();

        for(String queueUrl : queueUrls){

            String queue = queueUrl.substring(queueUrl.lastIndexOf("/") + 1);
            queues.add(queue);
        }

        return queues;
    }

    private String getTopicForAlarm(String service) {
        switch (service) {
            case "SUBSCRIPTION":
            case "SCHEDULING":
            case "MOODLE":
            case "LOAM":
                return "LEO_ALERTS_EMAIL_NOTIFICATION";
            case "DINERO":
                return "FINPOD_ALERTS_EMAIL_NOTIFICATION";
            case "PLATFORM":
                return "GROWTH_ALERTS_EMAIL_NOTIFICATION";
            case "WAVE":
                return "WAVE_ALERTS_EMAIL_NOTIFICATION";
            default:
                return "CORE_TECH_ALERTS_EMAIL_NOTIFICATION";
        }
    }

}
