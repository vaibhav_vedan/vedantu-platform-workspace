package com.vedantu.aws.response;

public class UploadFileUrlRes {
	private String fileName;
	private String keyName;
	private String downloadUrl;
	private String uploadUrl;
	private long uploadExpiryTime;
	private long downloadExpiryTime;
        private String contentType;

	public UploadFileUrlRes() {
		super();
	}

	public UploadFileUrlRes(String fileName, String keyName, String downloadUrl, String uploadUrl,
			long uploadExpiryTime, long downloadExpiryTime,String contentType) {
		super();
		this.fileName = fileName;
		this.keyName = keyName;
		this.downloadUrl = downloadUrl;
		this.uploadUrl = uploadUrl;
		this.uploadExpiryTime = uploadExpiryTime;
		this.downloadExpiryTime = downloadExpiryTime;
                this.contentType=contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getUploadUrl() {
		return uploadUrl;
	}

	public void setUploadUrl(String uploadUrl) {
		this.uploadUrl = uploadUrl;
	}

	public long getUploadExpiryTime() {
		return uploadExpiryTime;
	}

	public void setUploadExpiryTime(long uploadExpiryTime) {
		this.uploadExpiryTime = uploadExpiryTime;
	}

	public long getDownloadExpiryTime() {
		return downloadExpiryTime;
	}

	public void setDownloadExpiryTime(long downloadExpiryTime) {
		this.downloadExpiryTime = downloadExpiryTime;
	}

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        

	@Override
	public String toString() {
		return "UploadFileUrlRes [fileName=" + fileName + ", keyName=" + keyName + ", downloadUrl=" + downloadUrl
				+ ", uploadUrl=" + uploadUrl + ", uploadExpiryTime=" + uploadExpiryTime + ", downloadExpiryTime="
				+ downloadExpiryTime + ", contentType=" + contentType  + ", toString()=" + super.toString() + "]";
	}
}
