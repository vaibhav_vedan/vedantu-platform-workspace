package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.managers.StudentSubjectsManager;
import com.vedantu.reports.request.StudentRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("studentSubjects")
public class StudentSubjectsController {

    @Autowired
    private StudentSubjectsManager studentSubjectsManager;

    public Logger logger = LogFactory.getLogger(StudentSubjectsManager.class);

    @RequestMapping(value = "/getStudentSubjects", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<String> getStudentSubjects(@ModelAttribute StudentRequest req) throws BadRequestException {
        req.verify();
        return studentSubjectsManager.getStudentSubjects(req.getStudentId(),req.getPageType());
    }
}
