package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.Attendance;
import com.vedantu.reports.managers.AttendanceManager;
import com.vedantu.reports.request.MySubjectAttendanceGraphRequest;
import com.vedantu.reports.request.StudentProgressRequest;
import com.vedantu.reports.request.StudentSubjectProgressRequest;
import com.vedantu.reports.response.MyWeeklySubjectAttendanceResponse;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("attendance")
public class AttendanceController {

    @Autowired
    private AttendanceManager attendanceManager;

    public Logger logger = LogFactory.getLogger(AttendanceController.class);

    @RequestMapping(value = "/getSubjectAttendanceCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Attendance getSubjectAttendanceCount(@ModelAttribute StudentSubjectProgressRequest req) throws BadRequestException {
        req.verify();
        return attendanceManager.getSubjectAttendanceCount(req.getStudentId(), req.getSubject(), req.getFromDate());
    }

    @RequestMapping(value = "/getTotalAttendanceCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Attendance getTotalAttendanceCount(@ModelAttribute StudentProgressRequest req) throws BadRequestException{
        req.verify();
        return attendanceManager.getTotalAttendanceCount(req.getStudentId(), req.getFromDate());
    }

    @RequestMapping(value = "/getGraphSubjectAttendance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MyWeeklySubjectAttendanceResponse> getGraphSubjectAttendance(@ModelAttribute MySubjectAttendanceGraphRequest req) throws BadRequestException {
        req.verify();
        return attendanceManager.getGraphSubjectAttendance(req.getStudentId(), req.getSubject(), req.getFromDate());
    }
}
