package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.managers.TimeSpentOnPlatformManager;
import com.vedantu.reports.request.StudentProgressRequest;
import com.vedantu.reports.request.StudentRequest;
import com.vedantu.reports.response.MyActivityTimeResponse;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("timeSpentOnPlatform")
public class TimeSpentOnPlatformController {

    @Autowired
    private TimeSpentOnPlatformManager timeSpentOnPlatformManager;

    public Logger logger = LogFactory.getLogger(TimeSpentOnPlatformController.class);

    @RequestMapping(value = "/getAvgTimeSpentOnPlatform", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Double getAvgTimeSpentOnPlatform(@ModelAttribute StudentRequest req) throws BadRequestException {
        req.verify();
        return timeSpentOnPlatformManager.getAvgTimeSpentOnPlatform(req.getStudentId());
    }

    @RequestMapping(value = "/getGraphAvgTimeSpentOnPlatform", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MyActivityTimeResponse> getGraphAvgTimeSpentOnPlatform(@ModelAttribute StudentProgressRequest req) throws BadRequestException {
        req.verify();
        return timeSpentOnPlatformManager.getGraphAvgTimeSpentOnPlatform(req.getStudentId(), req.getFromDate());
    }

}
