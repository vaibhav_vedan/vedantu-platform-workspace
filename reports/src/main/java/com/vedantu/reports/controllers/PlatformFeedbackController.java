package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.PlatformFeedback;
import com.vedantu.reports.managers.PlatformFeedbackManager;
import com.vedantu.reports.request.PlatformFeedbackRequest;
import com.vedantu.reports.request.StudentRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("platformFeedback")
public class PlatformFeedbackController {

    @Autowired
    private PlatformFeedbackManager platformFeedbackManager;

    public Logger logger = LogFactory.getLogger(PlatformFeedbackController.class);

    @RequestMapping(value = "/getFeedback", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PlatformFeedback> getPlatformFeedback(@ModelAttribute StudentRequest req) throws BadRequestException {
        req.verify();
        return platformFeedbackManager.getFeedback(req.getStudentId());
    }

    @RequestMapping(value = "/addFeedback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addFeedback(@ModelAttribute PlatformFeedbackRequest req) throws BadRequestException {
        req.verify();
        platformFeedbackManager.addFeedback(new PlatformFeedback(req.getStudentId(), req.getFeedback()));
    }
}
