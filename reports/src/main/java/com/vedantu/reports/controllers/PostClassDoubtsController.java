package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.PostClassDoubtsAsked;
import com.vedantu.reports.managers.PostClassDoubtsManager;
import com.vedantu.reports.request.StudentProgressRequest;
import com.vedantu.reports.request.StudentSubjectProgressRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("postClassDoubts")
public class PostClassDoubtsController {

    @Autowired
    private PostClassDoubtsManager postClassDoubtsManager;

    public Logger logger = LogFactory.getLogger(PostClassDoubtsController.class);

    @RequestMapping(value = "/getSubjectPostClassDoubtsAsked", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PostClassDoubtsAsked getSubjectPostClassDoubtsAsked(@ModelAttribute StudentSubjectProgressRequest req) throws BadRequestException {
        req.verify();
        return postClassDoubtsManager.getSubjectPostClassDoubtsAsked(req.getStudentId(), req.getSubject(), req.getFromDate());
    }

    @RequestMapping(value = "/getTotalPostClassDoubtsAsked", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PostClassDoubtsAsked getTotalPostClassDoubtsAsked(@ModelAttribute StudentProgressRequest req) throws BadRequestException {
        req.verify();
        return postClassDoubtsManager.getTotalPostClassDoubtsAsked(req.getStudentId(), req.getFromDate());
    }
}
