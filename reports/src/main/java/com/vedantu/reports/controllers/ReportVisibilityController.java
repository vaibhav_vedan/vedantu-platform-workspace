package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.managers.ReportVisibilityManager;
import com.vedantu.reports.request.StudentRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("visibility")
public class ReportVisibilityController {

    @Autowired
    private ReportVisibilityManager reportVisibilityManager;

    public Logger logger = LogFactory.getLogger(ReportVisibilityController.class);

    @RequestMapping(value = "/isReportVisible", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean isReportVisible(@ModelAttribute StudentRequest req) throws BadRequestException {
        req.verify();
        return reportVisibilityManager.isReportVisible(req.getStudentId());
    }
}
