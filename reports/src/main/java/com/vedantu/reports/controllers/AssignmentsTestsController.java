package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.AssignmentsTests;
import com.vedantu.reports.managers.AssignmentsTestsManager;
import com.vedantu.reports.request.StudentProgressRequest;
import com.vedantu.reports.request.StudentSubjectProgressRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("assignmentsTests")
public class AssignmentsTestsController {

    @Autowired
    private AssignmentsTestsManager assignmentsTestsManager;

    public Logger logger = LogFactory.getLogger(AssignmentsTestsController.class);

    @RequestMapping(value = "/getSubjectAssignmentsTestsInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AssignmentsTests getSubjectAssignmentsTestsInfo(@ModelAttribute StudentSubjectProgressRequest req) throws BadRequestException {
        req.verify();
        return assignmentsTestsManager.getSubjectAssignmentsTestsInfo(req.getStudentId(), req.getSubject(), req.getFromDate());
    }

    @RequestMapping(value = "/getTotalAssignmentsTestsInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AssignmentsTests getTotalAssignmentsTestsInfo(@ModelAttribute StudentProgressRequest req) throws BadRequestException {
        req.verify();
        return assignmentsTestsManager.getTotalAssignmentsTestsInfo(req.getStudentId(), req.getFromDate());
    }
}
