package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.TotalCorrectAnswers;
import com.vedantu.reports.managers.TotalCorrectAnswerManager;
import com.vedantu.reports.request.StudentSubjectRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("totalCorrectAnswer")
public class TotalCorrectAnswerController {
    @Autowired
    private TotalCorrectAnswerManager totalCorrectAnswerManager;

    public Logger logger = LogFactory.getLogger(TotalCorrectAnswerController.class);

    @RequestMapping(value = "/getTotalCorrectAnswer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TotalCorrectAnswers> getTotalCorrectAnswer(@ModelAttribute StudentSubjectRequest req) throws BadRequestException {
        req.verify();
        return totalCorrectAnswerManager.getTotalCorrectAnswer(req);
    }
}
