package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.FocusAreas;
import com.vedantu.reports.managers.FocusAreaManager;
import com.vedantu.reports.request.StudentSubjectRequest;
import com.vedantu.reports.response.FocusAreaResponse;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("focusArea")
public class FocusAreaController {

    @Autowired
    private FocusAreaManager focusAreaManager;

    public Logger logger = LogFactory.getLogger(FocusAreaController.class);

    @RequestMapping(value = "/getFocusAreaOfStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FocusAreaResponse getFocusAreaOfStudent(@ModelAttribute StudentSubjectRequest req) throws BadRequestException {
        req.verify();
        return focusAreaManager.getFocusAreaOfStudent(req);
    }

}
