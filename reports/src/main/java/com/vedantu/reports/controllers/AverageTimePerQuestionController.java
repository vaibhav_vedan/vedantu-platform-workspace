package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.AverageTimePerQuestion;
import com.vedantu.reports.managers.AverageTimePerQuestionManager;
import com.vedantu.reports.request.StudentSubjectRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("averageTimePerQuestion")
public class AverageTimePerQuestionController {
    @Autowired
    private AverageTimePerQuestionManager averageTimePerQuestionManager;

    public Logger logger = LogFactory.getLogger(AverageTimePerQuestionController.class);

    @RequestMapping(value = "/getAverageTimePerQuestion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AverageTimePerQuestion getAverageTimePerQuestion(@ModelAttribute StudentSubjectRequest req) throws BadRequestException {
        req.verify();
        return averageTimePerQuestionManager.getAverageTimePerQuestion(req);
    }
}
