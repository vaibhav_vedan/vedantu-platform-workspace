package com.vedantu.reports.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.managers.MySubjectScoresManager;
import com.vedantu.reports.request.MySubjectScoreRequest;
import com.vedantu.reports.request.SubjectPercentileScoreRequest;
import com.vedantu.reports.response.MySubjectScoresAverageResponse;
import com.vedantu.reports.response.MySubjectScoresSubjectWiseResponse;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("mySubjectScores")
public class MySubjectScoresController {

    @Autowired
    private MySubjectScoresManager mySubjectScoresManager;

    public Logger logger = LogFactory.getLogger(MySubjectScoresController.class);

    @RequestMapping(value = "/getMyAggregatedScore", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MySubjectScoresAverageResponse> getMyAggregatedScore(@ModelAttribute MySubjectScoreRequest req) throws BadRequestException {
        req.verify();
        return mySubjectScoresManager.getMyAggregatedScore(req);
    }

    @RequestMapping(value = "/getMySubjectWisePercentagePercentileScore", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MySubjectScoresSubjectWiseResponse> getMySubjectWisePercentagePercentileScore(@ModelAttribute SubjectPercentileScoreRequest req) throws BadRequestException {
        req.verify();
        return mySubjectScoresManager.getMySubjectWisePercentagePercentileScore(req);
    }

}
