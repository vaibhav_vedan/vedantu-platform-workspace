package com.vedantu.reports.request;

import com.vedantu.reports.enums.PageType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class StudentRequest extends AbstractFrontEndReq {
    private String studentId;
    private PageType pageType=PageType.PROGRESS;

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId)){
            errors.add("[studentId] can't be null");
        }
        return errors;
    }
}