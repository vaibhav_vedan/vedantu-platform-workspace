package com.vedantu.reports.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class SubjectPercentileScoreRequest extends AbstractFrontEndReq {
    private String studentId;
    private String subject;
    private Long fromTime;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(fromTime)){
            errors.add("[studentId] or [fromTime] or [tillTime] or [subject] cann't be null");
        }
        return errors;
    }
}
