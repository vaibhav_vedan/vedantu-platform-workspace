package com.vedantu.reports.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class StudentSubjectRequest extends AbstractFrontEndReq {
    private String studentId;
    private String subject;
    private Long fromDate;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId)){
            errors.add("[studentId] can't be null");
        }
        return errors;
    }
}
