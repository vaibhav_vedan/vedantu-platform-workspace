package com.vedantu.reports.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class PlatformFeedbackRequest extends AbstractFrontEndReq {
    private String studentId;
    private Integer feedback;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId) || Objects.isNull(feedback)){
            errors.add("Missing parameters in feedback request!");
        }
        return errors;
    }
}
