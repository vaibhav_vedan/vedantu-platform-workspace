package com.vedantu.reports.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class MySubjectAttendanceGraphRequest extends AbstractFrontEndReq {
    private String studentId;
    private Long fromDate;
    private String subject;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId) || Objects.isNull(fromDate) || StringUtils.isEmpty(subject)){
            errors.add("[studentId] || [fromDate] || [subject]");
        }
        return errors;
    }
}
