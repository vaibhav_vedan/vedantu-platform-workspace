package com.vedantu.reports.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class StudentSubjectProgressRequest extends AbstractFrontEndReq {
    private String studentId;
    private String subject;
    private Long fromDate;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(fromDate)){
            errors.add("[studentId] || [subject] || [fromDate] can't be null");
        }
        return errors;
    }
}
