package com.vedantu.reports.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class MySubjectScoreRequest extends AbstractFrontEndReq {
    private String studentId;
    private Long fromDate;
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId) || Objects.isNull(fromDate)){
            errors.add("[studentId] || [fromDate]");
        }
        return errors;
    }
}
