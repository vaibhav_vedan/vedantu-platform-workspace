package com.vedantu.reports.aws;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.aws.response.UploadFileUrlRes;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class AwsS3Manager {

    @Autowired
    private AmazonClient amazonClient;

    private AmazonS3Client s3Client;

    private Logger logger = LogFactory.getLogger(AwsS3Manager.class);

    private static final String OTF_RECORDING_BUCKET_NAME = ConfigUtils.INSTANCE.getStringValue("vedantu.otf.recordings.bucket.name");

    @PostConstruct
    public void init() {

        s3Client = new AmazonS3Client();
    }

    public UploadFileUrlRes generatePresignedUploadUrl(UploadFileSourceType uploadFileSourceType, String fileName,
            String contentType) throws VException {
        logger.info("Request : " + uploadFileSourceType + " fileName : " + fileName + " contentType : " + contentType);
        UploadFileUrlRes uploadFileUrlRes = amazonClient.getPresignedUploadUrl(uploadFileSourceType, fileName,
                contentType);
        logger.info("Response : " + uploadFileUrlRes.toString());
        return uploadFileUrlRes;
    }

    public String getS3Location(UploadFileSourceType sourceType) throws VException {
        return amazonClient.getS3Location(sourceType);
    }

    public String getPreSignedUrl(String bucket, String key, String contentType) {
        String presignedUrl = "";

        try {
            java.util.Date expiration = new java.util.Date();
            long milliSeconds = expiration.getTime();
            milliSeconds += 1000 * 60 * 60; // Add 1 hour.
            expiration.setTime(milliSeconds);

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key);
            generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
            generatePresignedUrlRequest.setExpiration(expiration);
            generatePresignedUrlRequest.setContentType(contentType);

            presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();

            logger.info("preSignedUrl:" + presignedUrl);
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }

    public List<String> getBucketContents(String bucket, String prefix) {
        List<String> contents = new ArrayList<>();

        try {
            s3Client.listObjects(bucket, prefix).getObjectSummaries().forEach(objectSummary -> {
                contents.add(s3Client.getUrl(bucket, objectSummary.getKey()).toString());
            });
        } catch (Exception ex) {
        }

        return contents;
    }

    public Boolean uploadFile(String bucket, String key, File contentFile) {
        Boolean isUploadContentFileSuccessfull = false;

        try {
            s3Client.putObject(bucket, key, contentFile);

            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            isUploadContentFileSuccessfull = false;
        }

        return isUploadContentFileSuccessfull;
    }

    public String getPublicBucketDownloadUrl(String bucket, String key) {
        return s3Client.getUrl(bucket, key).toExternalForm();
    }

    public boolean checkRecordingExists(String key) {
        return s3Client.doesObjectExist(getOTFBucketName(), key + ".mp4");
    }

    private String getOTFBucketName() {
        return OTF_RECORDING_BUCKET_NAME + "/PROD";// + ConfigUtils.INSTANCE.getStringValue("environment");
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
