package com.vedantu.reports.aws;

import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CreateInvalidationRequest;
import com.amazonaws.services.cloudfront.model.InvalidationBatch;
import com.amazonaws.services.cloudfront.model.Paths;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.List;

@Service
public class AwsCloudFrontManager {

    private Logger logger = LogFactory.getLogger(AwsCloudFrontManager.class);

    private AmazonCloudFrontClient cloudFrontClient;

    public static final String ACADTACK_DISTRIBUTION_ID = ConfigUtils.INSTANCE.getStringValue("aws.cloudfront.acadstack.distribution.id");
    public static final String VEDANTU_DISTRIBUTION_ID = ConfigUtils.INSTANCE.getStringValue("aws.cloudfront.vedantu.distribution.id");

    @Autowired
    private AmazonClient amazonClient;

    public AwsCloudFrontManager() {
        cloudFrontClient = new AmazonCloudFrontClient();
    }

    public void invalidateCache(String distributionId, List<String> paths) {
        Paths path = new Paths();
        path.setItems(paths);
        InvalidationBatch batch = new InvalidationBatch(path, String.valueOf(System.currentTimeMillis()));
        CreateInvalidationRequest request = new CreateInvalidationRequest(distributionId, batch);
        cloudFrontClient.createInvalidation(request);
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (cloudFrontClient != null) {
                cloudFrontClient.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
