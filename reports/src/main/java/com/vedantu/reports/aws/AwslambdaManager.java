package com.vedantu.reports.aws;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambdaAsyncClient;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.google.gson.Gson;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import javax.annotation.PreDestroy;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Service
public class AwslambdaManager {

    private Logger logger = LogFactory.getLogger(AwslambdaManager.class);

    private static Gson gson = new Gson();

    private final AWSLambdaClient lambdaClient;

    private final AWSLambdaAsyncClient asynclambdaClient;

    public AwslambdaManager() {
        lambdaClient = new AWSLambdaClient();
        lambdaClient.setRegion(Region.getRegion(Regions.AP_NORTHEAST_1));

        asynclambdaClient = new AWSLambdaAsyncClient();
        asynclambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
    }

    public String invokePdfToImageConverterLambda(String key) {

        logger.info("invokePdfToImageConverterLambda called with key {}", key);

        logger.info(ConfigUtils.INSTANCE.getStringValue("environment"));

        String SEO_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

        Map<String, String> lamdaPayload = new HashMap<>();
        lamdaPayload.put("bucket",SEO_BUCKET);
        lamdaPayload.put("key",key);
        String inputJSON = gson.toJson(lamdaPayload);

        logger.info(inputJSON);

        String functionName = "pdftoimageConverterMigration";

        InvokeRequest invokeRequest = new InvokeRequest();
        invokeRequest.setFunctionName(functionName);
        invokeRequest.setPayload(inputJSON);

        lambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        InvokeResult lmbResult = lambdaClient.invoke(invokeRequest);

        return new String(lmbResult.getPayload().array(), Charset.forName("UTF-8"));
    }

    public String invokeCloudfrontClearCacheLambda(String key) {

        logger.info("invokeCloudfrontClearCacheLambda called with key {}", key);

        String inputJSON = gson.toJson(key);

        logger.info(inputJSON);

        String functionName = "invalidateCFCache";

        InvokeRequest invokeRequest = new InvokeRequest();
        invokeRequest.setFunctionName(functionName);
        invokeRequest.setPayload(inputJSON);

        lambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        InvokeResult lmbResult = lambdaClient.invoke(invokeRequest);

        return new String(lmbResult.getPayload().array(), Charset.forName("UTF-8"));
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (lambdaClient != null) {
                lambdaClient.shutdown();
            }
            if (asynclambdaClient != null) {
                asynclambdaClient.shutdown();
            }            
        } catch (Exception e) {
            logger.error("Error in shutting down asynclambdaClient, lambdaClient ", e);
        }
    }         
}
