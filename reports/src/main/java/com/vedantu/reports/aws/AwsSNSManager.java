/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.reports.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.reports.async.AsyncTaskName;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

@Service
public class AwsSNSManager extends AbstractAwsSNSManager{

    private Logger logger = LogFactory.getLogger(AwsSNSManager.class);

    private String env;
    private String arn;
    private AmazonSNSAsync snsClient;
    private String snsArnBuilder;
    
    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public AwsSNSManager() {
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        snsArnBuilder = ConfigUtils.INSTANCE.getStringValue("aws.sns.arn.builder");
        snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
        logger.info("initializing AwsSNSManager");
        try {
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                //creating SNS topic CMS_WEBINAR_EVENTS
                CreateTopicRequest createTopicRequest = new CreateTopicRequest(SNSTopic.CMS_WEBINAR_EVENTS.getTopicName(env));
                snsClient.createTopic(createTopicRequest);
            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSNSManager " + e.getMessage());
        }
        
    }

    @Override
    public void triggerSNS(SNSTopic topic, String subject, String message) {
        String topicArn = getTopicArn(topic); 
        PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
        logger.info("publishRequest "+publishRequest);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }               
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }
    
    public void triggerAsyncSNS(SNSTopic topic, String subject, String message, Long delayMillis) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("topic", topic);
        payload.put("subject", subject);
        payload.put("message", message);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }           
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_SNS, payload, delayMillis);
        asyncTaskFactory.executeTask(params);
    }
    
    @Override
    public String getTopicArn(SNSTopic topic) {
        return String.format(snsArnBuilder,Regions.AP_SOUTHEAST_1.getName(), arn, topic.getTopicName(env));
    }
    
    public SubscribeResult createSubscription(SNSTopic topic, String type, String typeArn) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(getTopicArn(topic), type, typeArn);
        SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
        return subscribeResult;
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSNSManager connection ", e);
        }
    }              

    @Override
    public void createTopics() {
    }

    @Override
    public void createSubscriptions() {
    }
}
