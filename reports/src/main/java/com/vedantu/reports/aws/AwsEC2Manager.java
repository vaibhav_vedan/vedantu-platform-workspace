/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.reports.aws;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.ShutdownBehavior;
import com.amazonaws.services.ec2.model.Tag;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Service
public class AwsEC2Manager {

    private AmazonEC2 ec2Client;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsEC2Manager.class);

    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final String WEBINAR_LAUNCH_SECURITY_GROUP = ConfigUtils.INSTANCE.getStringValue("webinar.launch.security_group");
    private static final String SESSION_RECORDER_IAM = ConfigUtils.INSTANCE.getStringValue("vedantu.oto.recorder.iam");
    private static final String WEBINAR_LAUNCHER_AMI_ID = ConfigUtils.INSTANCE.getStringValue("vedantu.webinar.launcher.ami_id");
    private static final String WEBINAR_LAUNCH_SUBNET = ConfigUtils.INSTANCE.getStringValue("vedantu.webinar.launch.subnet");
    private static final String WEBINAR_LAUNCHER_KEY = ConfigUtils.INSTANCE.getStringValue("webinar.launcher.key");

    @PostConstruct
    public void init() {
        if (!ENV.equalsIgnoreCase("LOCAL")) {
            ec2Client = AmazonEC2ClientBuilder.defaultClient();
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (ec2Client != null) {
                ec2Client.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in shutting down ec2client ", e);
        }
    }        
}
