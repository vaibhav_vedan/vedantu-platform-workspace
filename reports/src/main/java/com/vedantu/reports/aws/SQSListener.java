package com.vedantu.reports.aws;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.enums.SQSMessageType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.util.Objects;

@Component
public class SQSListener implements MessageListener {

    @Autowired
    private StatsdClient statsdClient;

    private Logger logger = LogFactory.getLogger(SQSListener.class);

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            String queueName = ((Queue)textMessage.getJMSDestination()).getQueueName();
            logger.info("queueName "+queueName);
            long startTime = System.currentTimeMillis();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(queueName, sqsMessageType, textMessage.getText());
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if(Objects.nonNull(sqsMessageType)) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }

    private void handleMessage(String queueName, SQSMessageType sqsMessageType, String text) throws Exception {


        logger.info("Message handled");
    }

}

