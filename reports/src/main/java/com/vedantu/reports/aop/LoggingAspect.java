/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.reports.aop;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.StringUtils;
import com.vedantu.util.aop.AbstractAOPLayer;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Component
@Aspect
public class LoggingAspect extends AbstractAOPLayer {

    private Logger logger = LogFactory.getLogger(LoggingAspect.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private StatsdClient statsdClient;

    private static final String VED_REQUEST_START_TIME = "VED_REQUEST_START_TIME";

    @Before("allCtrlMethods()")
    public void beforeExecutionForCtrlMethods(JoinPoint jp) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes == null || servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest request = servletRequestAttributes.getRequest();
        request.setAttribute(VED_REQUEST_START_TIME, System.currentTimeMillis());

        handleCorrelationId(request);
        handleCallingUserId(request);

    }

    @AfterThrowing(pointcut = "allCtrlMethods()", throwing = "ex")
    public void afterThrowingExecutionForCtrlMethods(JoinPoint jp, Exception ex) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes == null || servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest request = servletRequestAttributes.getRequest();
        sendStatsdStats(request, jp, true);
    }

    private void sendStatsdStats(HttpServletRequest request, JoinPoint jp, boolean isFailed) {
        logger.info("VED_REQUEST_START_TIME: " + request.getAttribute(VED_REQUEST_START_TIME));
        if (request.getAttribute(VED_REQUEST_START_TIME) != null) {
            Long initTime = (Long) request.getAttribute(VED_REQUEST_START_TIME);
            Long responseTime = System.currentTimeMillis() - initTime;
            logger.info("responseTime(failed or success) for " + request.getRequestURI() + " : " + responseTime);
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = jp.getTarget().getClass().getSimpleName();
                String methodName = jp.getSignature().getName();
                statsdClient.recordCount(className, methodName, "apicount");
                if (!isFailed) {
                    statsdClient.recordExecutionTime(responseTime, className, methodName);
                } else {
                    statsdClient.recordCount(className, methodName, "failed");
                }
            }
        }
//        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (servletRequestAttributes.getRequest() == null) {
//            return;
//        }
//
//        HttpServletResponse response = servletRequestAttributes.getResponse();
//
//        StatusExposingServletResponse nresponse = new StatusExposingServletResponse(response);
//        logger.info("<>>>>>>>>>>>" + nresponse.getContentLength());
//        logger.info("<>>>>>>>>>>>" + nresponse.getStatus());
    }

    @Before("allCtrlMethods()")
    @Override
    public void fillRequestPojo(JoinPoint jp) {
        Object[] signatureArgs = jp.getArgs();
        if (signatureArgs != null && signatureArgs.length > 0) {
            if (signatureArgs[0] instanceof AbstractFrontEndReq) {
                AbstractFrontEndReq request = (AbstractFrontEndReq) signatureArgs[0];
                sessionUtils.fillAbstractFrontEndReq(request);
                if (RequestContextHolder.getRequestAttributes() == null) {
                    return;
                }
                ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                if (servletRequestAttributes == null || servletRequestAttributes.getRequest() == null) {
                    return;
                }

                HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
                //checking if the req is coming from appengine, then using the ip address from headers
                //as this header captures the correct ip of the user accessing appengine
                String ipAddress = httpServletRequest.getHeader("FROM-APPENGINE-USER-IP");
                if (StringUtils.isEmpty(ipAddress)) {
                    ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR");
                    if (ipAddress == null) {
                        ipAddress = httpServletRequest.getRemoteAddr();
                    }
                }
                request.setIpAddress(ipAddress);
                super.fillUserAgent(httpServletRequest, request);
            }
        }
    }

    private void handleCorrelationId(HttpServletRequest request) {
        try {
            String correlationId = request.getHeader("X-Correlation-Id");
            if (StringUtils.isEmpty(correlationId)) {
                correlationId = generateId();
            }
            ThreadContext.put("correlation-id", correlationId);
            //request.setAttribute("correlation-id", correlationId);
        } catch (Exception ex) {
            logger.error("Error in handleCorrelationId", ex);
        }
    }

    private void handleCallingUserId(HttpServletRequest request) {
        try {
            String callingUserId = request.getHeader("X-Caller-Id");
            if (StringUtils.isEmpty(callingUserId)) {
                HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
                if (sessionData != null && sessionData.getUserId() != null) {
                    callingUserId = sessionData.getUserId().toString();
                }
            }
            if (StringUtils.isNotEmpty(callingUserId)) {
                ThreadContext.put("callingUserId", callingUserId);
                //request.setAttribute("callingUserId", callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Error in handleCallingUserId", ex);
        }
    }

    private String generateId() {
        return UUID.randomUUID().toString();
    }

    //tracking dao methods calls
    @Pointcut("within(com.vedantu.growth.dao..*)")
    public void allDAOMethods() {
    }
}
