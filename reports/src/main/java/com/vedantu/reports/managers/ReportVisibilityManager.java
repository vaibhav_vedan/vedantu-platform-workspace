package com.vedantu.reports.managers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.FosUtils;
import com.vedantu.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ReportVisibilityManager {

    @Autowired
    private FosUtils fosUtils;

    public boolean isReportVisible(String studentId) {
        try {
            UserBasicInfo userInfo = fosUtils.getUserBasicInfo(studentId, false);
            if (Objects.nonNull(userInfo) && StringUtils.isNotEmpty(userInfo.getGrade())) {
                int grade = Integer.parseInt(userInfo.getGrade());
                return (grade > 10);
            }
        } catch (Exception ignore) {}
        return false;
    }
}
