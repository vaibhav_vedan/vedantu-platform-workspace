package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.entities.board.Board;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.reports.dao.BoardDAO;
import com.vedantu.reports.dao.OTFSessionUtilDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class OTFSessionKafkaManager {

    @Autowired
    private OTFSessionUtilDAO otfSessionUtilDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public Logger logger = LogFactory.getLogger(OTFSessionKafkaManager.class);

    public void processOTFSessionSubjects(OTFSessionInfo otfSessionInfo) throws BadRequestException {
        logger.info("Processing OTF session with id {}", otfSessionInfo.getId());
        if(Objects.nonNull(otfSessionInfo.getBoardId())) {
            Long boardId = otfSessionInfo.getBoardId();
            List<Long> boardIdList = new ArrayList<>();
            boardIdList.add(boardId);
            List<Board> boards = boardDAO.getBoards(boardIdList);
            if(ArrayUtils.isNotEmpty(boards)){
                otfSessionUtilDAO.updateSessionSubjects(otfSessionInfo.getId(), boards.get(0).getName(), otfSessionInfo.getTitle());
            } else {
                try {
                    Map<Long, com.vedantu.board.pojo.Board> resp = fosUtils.getBoardInfoMap(boardIdList);
                    if (resp != null && resp.containsKey(boardId)) {
                        otfSessionUtilDAO.updateSessionSubjects(otfSessionInfo.getId(), resp.get(boardId).getName(), otfSessionInfo.getTitle());
                        boardDAO.upsert(mapper.map(resp.get(boardId), Board.class));
                    } else {
                        logger.error("Unable to fetch board info from fos utils for otf session {}", otfSessionInfo.getId());
                    }
                } catch (Exception e) {
                    logger.error("Exception occurred while fetching otf session subject ", e);
                }
            }
        } else {
            logger.info("OTFSession {} doesn't have board info", otfSessionInfo.getId());
        }
    }
}
