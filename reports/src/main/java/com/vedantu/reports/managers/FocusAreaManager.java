package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.reports.dao.FocusAreasDAO;
import com.vedantu.reports.entities.FocusAreas;
import com.vedantu.reports.pojo.FocusArea;
import com.vedantu.reports.pojo.OverallFocusAreaPojo;
import com.vedantu.reports.request.StudentSubjectRequest;
import com.vedantu.reports.response.FocusAreaResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FocusAreaManager {

    @Autowired
    private FocusAreasDAO focusAreasDAO;

    public Logger logger = LogFactory.getLogger(FocusAreaManager.class);

    public FocusAreaResponse getFocusAreaOfStudent(StudentSubjectRequest req) throws BadRequestException {
        FocusAreas focusAreaOfStudent = focusAreasDAO.getFocusAreaOfStudent(req.getStudentId());
        List<OverallFocusAreaPojo> overallFocusAreaPojoList=new ArrayList<>();

        if(Objects.nonNull(focusAreaOfStudent)){
            List<FocusArea> focusAreas = focusAreaOfStudent.getFocusAreas();
            if(ArrayUtils.isNotEmpty(focusAreas)){
                String subject = req.getSubject();
                if(StringUtils.isEmpty(subject)) {
                    throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Subject list can't be empty");
                }
                FocusArea focusAreaOfSubject =
                        focusAreas.stream()
                                .filter(focusArea -> subject.equals(focusArea.getSubject()))
                                .findFirst()
                                .orElse(null);
                if(Objects.nonNull(focusAreaOfSubject)){
                    if("Overall".equals(subject)) {
                        for (String topicToFocus : focusAreaOfSubject.getTopicsToFocus()) {
                            for (FocusArea focusArea : focusAreas) {
                                if (!(subject.equals(focusArea.getSubject())) && focusArea.getTopicsToFocus().contains(topicToFocus)) {
                                    overallFocusAreaPojoList.add(new OverallFocusAreaPojo(focusArea.getSubject(), topicToFocus));
                                }
                            }
                        }
                    }else{
                        overallFocusAreaPojoList.addAll(
                            focusAreaOfSubject.getTopicsToFocus()
                                    .stream()
                                    .map(topicToFocus->new OverallFocusAreaPojo(subject,topicToFocus))
                                    .collect(Collectors.toList())
                        );
                    }
                }
            }
        }
        return new FocusAreaResponse(overallFocusAreaPojoList);
    }
}
