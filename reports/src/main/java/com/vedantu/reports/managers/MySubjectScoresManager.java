package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.MySubjectScoresDAO;
import com.vedantu.reports.dao.TestScoreAggregatorUtilDAO;
import com.vedantu.reports.entities.MySubjectScores;
import com.vedantu.reports.entities.TestScoreAggregatorUtil;
import com.vedantu.reports.pojo.SubjectScore;
import com.vedantu.reports.request.MySubjectScoreRequest;
import com.vedantu.reports.request.SubjectPercentileScoreRequest;
import com.vedantu.reports.response.MySubjectScoresAverageResponse;
import com.vedantu.reports.response.MySubjectScoresSubjectWiseResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MySubjectScoresManager {

    @Autowired
    private MySubjectScoresDAO mySubjectScoresDAO;

    @Autowired
    private TestScoreAggregatorUtilDAO testScoreAggregatorUtilDAO;

    public Logger logger = LogFactory.getLogger(MySubjectScoresManager.class);


    public List<MySubjectScoresAverageResponse> getMyAggregatedScore(MySubjectScoreRequest req) throws BadRequestException {

        logger.info("getMyAggregatedScore studentId - {}, fromDate - {}",req.getStudentId(),req.getFromDate());

        List<MySubjectScoresAverageResponse> responses=new ArrayList<>();


        Map<String, Double> subjectTotalMarks=new HashMap<>();
        Map<String, Integer> subjectCount=new HashMap<>();
        int start=0;
        int size=100;

        Set<String> testIds=mySubjectScoresDAO.getAllTestIdsForStudent(req.getStudentId(),req.getFromDate());

        logger.info("Test ids retrieved for student are {}",testIds);
        if(ArrayUtils.isEmpty(testIds)){
            return responses;
        }

        Map<String, Double> averageSubjectScore=new HashMap<>();

        List<TestScoreAggregatorUtil> testScoreAggregatorUtils=testScoreAggregatorUtilDAO.getAllTestScoreDataForTestIds(testIds);
        Set<String> includedTestIds=new HashSet<>();
        for(TestScoreAggregatorUtil testScoreAggregatorUtil:testScoreAggregatorUtils){
            includedTestIds.add(testScoreAggregatorUtil.getTestId());
            List<SubjectScore> subjectScoreList = testScoreAggregatorUtil.getSubjectScoreList();
            for(SubjectScore subjectScore:subjectScoreList){
                String subject = subjectScore.getSubject();
                if(!("Overall").equals(subject)) {
                    averageSubjectScore.putIfAbsent(subject, 0d);
                    averageSubjectScore.put(subject, averageSubjectScore.get(subject) + ((subjectScore.getAverageScore()/subjectScore.getMaxScore())*100));
                }
            }
        }

        logger.info("averageSubjectScore - {}",averageSubjectScore);

        while(true){
            List<MySubjectScores> mySubjectScoresList=mySubjectScoresDAO.getSubjectScoresForStudentId(req.getStudentId(),req.getFromDate(),start,size);
            if(ArrayUtils.isEmpty(mySubjectScoresList)){
                break;
            }
            logger.info("start - {}, size - {}",start,size);
            logger.info("mySubjectScoresList - {}",mySubjectScoresList);
            for(MySubjectScores mySubjectScore:mySubjectScoresList){
                if(includedTestIds.contains(mySubjectScore.getTestId())) {
                    String subject = mySubjectScore.getSubject();
                    subjectTotalMarks.putIfAbsent(subject, 0d);
                    subjectCount.putIfAbsent(subject, 0);

                    subjectTotalMarks.put(subject, subjectTotalMarks.get(subject) + mySubjectScore.getPercentage());
                    subjectCount.put(subject, subjectCount.get(subject) + 1);
                }
            }
            start+=size;
        }

        int totalTests=includedTestIds.size();

        logger.info("totalTests - {}",totalTests);

        for(String subject:averageSubjectScore.keySet()){
            responses.add(new MySubjectScoresAverageResponse(subject,(subjectTotalMarks.get(subject)/subjectCount.get(subject)),(averageSubjectScore.get(subject)/subjectCount.get(subject))));
        }

        return responses;
    }

    public List<MySubjectScoresSubjectWiseResponse> getMySubjectWisePercentagePercentileScore(SubjectPercentileScoreRequest req) throws BadRequestException {
        List<MySubjectScoresSubjectWiseResponse> responses=new ArrayList<>();
        List<MySubjectScores> mySubjectWisePercentagePercentileScore = mySubjectScoresDAO.getMySubjectWisePercentagePercentileScore(req.getStudentId(), req.getSubject(), req.getFromTime());
        if(ArrayUtils.isEmpty(mySubjectWisePercentagePercentileScore)){
            return responses;
        }

        List<TestScoreAggregatorUtil> allTestScoreDataForTestIds = testScoreAggregatorUtilDAO.getAllTestScoreDataForTestIds(
                mySubjectWisePercentagePercentileScore.stream().map(MySubjectScores::getTestId).collect(Collectors.toSet()),
                Arrays.asList(TestScoreAggregatorUtil.Constants.TEST_ID, TestScoreAggregatorUtil.Constants.TEST_NAME));
        Map<String, String> testIdNameMap = new HashMap<>();
        for(TestScoreAggregatorUtil util:allTestScoreDataForTestIds){
            testIdNameMap.put(util.getTestId(),util.getTestName());
        }

        for(MySubjectScores mySubjectScore:mySubjectWisePercentagePercentileScore){
            String testName = testIdNameMap.get(mySubjectScore.getTestId());
            if(StringUtils.isNotEmpty(testName)){
                MySubjectScoresSubjectWiseResponse response=new MySubjectScoresSubjectWiseResponse(
                        testName,
                        mySubjectScore.getPercentage(),
                        mySubjectScore.getPercentile(),
                        mySubjectScore.getCreationTime()
                );
                responses.add(response);
            }
            
        }

        responses.sort(Comparator.comparingLong(MySubjectScoresSubjectWiseResponse::getTestTakenDate));

        return responses;
    }
}
