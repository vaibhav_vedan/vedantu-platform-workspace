package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.AssignmentsTestsDAO;
import com.vedantu.reports.dao.BoardDAO;
import com.vedantu.reports.dao.GlobalReportFieldsDAO;
import com.vedantu.reports.dao.KafkaEntryAuditDAO;
import com.vedantu.reports.dao.StudentSubjectUtilDAO;
import com.vedantu.reports.entities.AssignmentsTests;
import com.vedantu.reports.entities.GlobalReportFieldsCounts;
import com.vedantu.reports.entities.board.Board;
import com.vedantu.reports.entities.lms.CMDSTestAttempt;
import com.vedantu.reports.entities.lms.ContentInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AssignmentsTestsManager {

    @Autowired
    private AssignmentsTestsDAO assignmentsTestsDAO;

    @Autowired
    private GlobalReportFieldsDAO globalReportFieldsDAO;

    @Autowired
    private KafkaEntryAuditDAO kafkaEntryAuditDAO;

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private StudentSubjectUtilDAO studentSubjectUtilDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private DozerBeanMapper mapper;

    public Logger logger = LogFactory.getLogger(AssignmentsTestsManager.class);
    private static final String SOCIAL_STUDIES = "Social Studies";
    private static final String SOCIAL_SCIENCE = "Social Science";
    private static final String MATHEMATICS = "Mathematics";
    private static final String MATHS = "Maths";


    public AssignmentsTests getSubjectAssignmentsTestsInfo(String studentId, String subject, Long fromDate) throws BadRequestException {

        if (SOCIAL_STUDIES.equals(subject)) {
            subject = SOCIAL_SCIENCE;
        }

        if (MATHEMATICS.equals(subject)) {
            subject = MATHS;
        }

        List<AssignmentsTests> assignmentsTestsList = assignmentsTestsDAO.getSubjectAssignmentTestCount(studentId, subject, DateTimeUtils.getOnlyDateInMillis(fromDate));
        int assignmentsShared = 0;
        int assignmentsAttempted = 0;
        int testsShared = 0;
        int testsCompleted = 0;
        for (AssignmentsTests assignmentsTests : assignmentsTestsList) {
            assignmentsAttempted += Objects.nonNull(assignmentsTests.getAssignmentsAttempted()) ? assignmentsTests.getAssignmentsAttempted() : 0;
            assignmentsShared += Objects.nonNull(assignmentsTests.getAssignmentsShared()) ? assignmentsTests.getAssignmentsShared() : 0;
            testsCompleted += Objects.nonNull(assignmentsTests.getTestsCompleted()) ? assignmentsTests.getTestsCompleted() : 0;
            testsShared += Objects.nonNull(assignmentsTests.getTestsShared()) ? assignmentsTests.getTestsShared() : 0;
        }
        return new AssignmentsTests(studentId, subject, testsCompleted, testsShared, assignmentsAttempted, assignmentsShared);
    }

    public AssignmentsTests getTotalAssignmentsTestsInfo(String studentId, Long fromDate) throws BadRequestException {
        List<GlobalReportFieldsCounts> totalAssignmentsTestsList = globalReportFieldsDAO.getAssignmentsTestsCount(studentId, DateTimeUtils.getOnlyDateInMillis(fromDate));
        int assignmentsShared = 0;
        int assignmentsAttempted = 0;
        int testsShared = 0;
        int testsCompleted = 0;
        for (GlobalReportFieldsCounts assignmentsTests : totalAssignmentsTestsList) {
            assignmentsAttempted += Objects.nonNull(assignmentsTests.getAssignmentsAttempted()) ? assignmentsTests.getAssignmentsAttempted() : 0;
            assignmentsShared += Objects.nonNull(assignmentsTests.getAssignmentsShared()) ? assignmentsTests.getAssignmentsShared() : 0;
            testsCompleted += Objects.nonNull(assignmentsTests.getTestsCompleted()) ? assignmentsTests.getTestsCompleted() : 0;
            testsShared += Objects.nonNull(assignmentsTests.getTestsShared()) ? assignmentsTests.getTestsShared() : 0;
        }
        return new AssignmentsTests(studentId, testsCompleted, testsShared, assignmentsAttempted, assignmentsShared);
    }

    public void processAssignmentTestAttemptedCounts(CMDSTestAttempt cmdsTestAttempt) throws BadRequestException {
        logger.info("Processing assignments Tests attempt counts");
        logger.info("CMDS test received: {}", cmdsTestAttempt);
        Set<String> subjects =
                Optional.ofNullable(cmdsTestAttempt.getSubjects())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(this::mapMathematicsToMathIfNeeded)
                        .collect(Collectors.toSet());

        long documentCreationTime=cmdsTestAttempt.getCreationTime();

        if(ArrayUtils.isEmpty(subjects)) {
            logger.error("Subject list empty in CMDSTestAttempt with attempt id {}", cmdsTestAttempt.getId());
            return;
        }
        if(cmdsTestAttempt.getAttemptIndex() == 1) {
            if (Objects.nonNull(cmdsTestAttempt.getDuration()) && cmdsTestAttempt.getDuration() > 0) {
                for (String subject : subjects) {
                    assignmentsTestsDAO.updateAssignmentAndTestCount(cmdsTestAttempt.getStudentId(), subject, 1, AssignmentsTests.Constants.TESTS_COMPLETED,documentCreationTime);
                }
                globalReportFieldsDAO.updateGlobalReportCount(cmdsTestAttempt.getStudentId(), GlobalReportFieldsCounts.Constants.TESTS_COMPLETED, 1, documentCreationTime);
            } else {
                for (String subject : subjects) {
                    assignmentsTestsDAO.updateAssignmentAndTestCount(cmdsTestAttempt.getStudentId(), subject, 1, AssignmentsTests.Constants.ASSIGNMENTS_ATTEMPTED,documentCreationTime);
                }
                globalReportFieldsDAO.updateGlobalReportCount(cmdsTestAttempt.getStudentId(), GlobalReportFieldsCounts.Constants.ASSIGNMENTS_ATTEMPTED, 1,documentCreationTime);
            }
        }

        // update student subjects
        Set<String> updatesSubject = studentSubjectUtilDAO.getStudentSubjects(cmdsTestAttempt.getStudentId());
        logger.info("Subjects which are already there are {}",updatesSubject);
        logger.info("Subjects which are about to go inside are {}",subjects);
        if(!updatesSubject.containsAll(subjects)) {
            updatesSubject.addAll(subjects);
            studentSubjectUtilDAO.updateStudentSubjects(cmdsTestAttempt.getStudentId(), updatesSubject);
        }
    }

    public void processAssignmentTestSharedCount(ContentInfo contentInfo) throws BadRequestException {
        logger.info("Processing assignments Tests shared counts");
        logger.info("Content info received: {}", contentInfo);
        Long duration = contentInfo.getMetadata().getDuration();
        Set<String> subjects=
                Optional.ofNullable(contentInfo.getSubjects())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(this::mapMathematicsToMathIfNeeded)
                        .collect(Collectors.toSet());
        Set<Long> contentInfoBoardIds = contentInfo.getBoardIds();
        long documentCreationTime=contentInfo.getCreationTime();
        if (ArrayUtils.isEmpty(contentInfoBoardIds)) {
            logger.error("No board info found in content info {}", contentInfo.getId());
            return;
        }
        if(ArrayUtils.isEmpty(subjects) || contentInfoBoardIds.size()!=subjects.size()) {
            List<Board> boards = boardDAO.getBoards(contentInfoBoardIds);
            if ((ArrayUtils.isNotEmpty(boards) && boards.size() != contentInfoBoardIds.size()) || ArrayUtils.isEmpty(boards)) {
                try {
                    Map<Long, com.vedantu.board.pojo.Board> resp = fosUtils.getBoardInfoMap(contentInfoBoardIds);
                    if (Objects.isNull(resp)) {
                        logger.error("Unable to fetch subject information for content info with id {}", contentInfo.getId());
                    }
                    boards = new ArrayList<>();
                    for (Long boardId : resp.keySet()) {
                        Board board = mapper.map(resp.get(boardId), Board.class);
                        boardDAO.upsert(board);
                        boards.add(board);
                    }
                } catch (Exception e) {
                    logger.error("Exception occurred while fetching board details from fos utils", e);
                }
            }
            if(ArrayUtils.isNotEmpty(boards)) {
                subjects = boards.stream().map(Board::getName).map(this::mapMathematicsToMathIfNeeded).collect(Collectors.toSet());
            }
        }
        if(ArrayUtils.isNotEmpty(subjects)) {
            if (Objects.nonNull(duration) && duration > 0) {
                globalReportFieldsDAO.updateGlobalReportCount(contentInfo.getStudentId(), AssignmentsTests.Constants.TESTS_SHARED, 1, documentCreationTime);
                for (String subject : subjects) {
                    assignmentsTestsDAO.updateAssignmentAndTestCount(contentInfo.getStudentId(), subject, 1, AssignmentsTests.Constants.TESTS_SHARED, documentCreationTime);
                }
            } else {
                for (String subject  : subjects) {
                    assignmentsTestsDAO.updateAssignmentAndTestCount(contentInfo.getStudentId(), subject, 1, AssignmentsTests.Constants.ASSIGNMENTS_SHARED, documentCreationTime);
                }
                globalReportFieldsDAO.updateGlobalReportCount(contentInfo.getStudentId(), GlobalReportFieldsCounts.Constants.ASSIGNMENTS_SHARED, 1, documentCreationTime);
            }

            // update student subjects
            Set<String> updatesSubject = studentSubjectUtilDAO.getStudentSubjects(contentInfo.getStudentId());
            if(!updatesSubject.containsAll(subjects)) {
                updatesSubject.addAll(subjects);
                studentSubjectUtilDAO.updateStudentSubjects(contentInfo.getStudentId(), updatesSubject);
            }
        }
    }

    private String mapMathematicsToMathIfNeeded(String subject) {
        return MATHEMATICS.equals(subject) ?MATHS:subject;
    }
}
