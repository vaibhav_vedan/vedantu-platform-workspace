package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.reports.dao.AttendanceDAO;
import com.vedantu.reports.dao.GlobalReportFieldsDAO;
import com.vedantu.reports.dao.KafkaEntryAuditDAO;
import com.vedantu.reports.dao.OTFSessionUtilDAO;
import com.vedantu.reports.dao.StudentSubjectUtilDAO;
import com.vedantu.reports.entities.Attendance;
import com.vedantu.reports.entities.GlobalReportFieldsCounts;
import com.vedantu.reports.entities.OTFSessionUtil;
import com.vedantu.reports.response.MyWeeklySubjectAttendanceResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

@Service
public class AttendanceManager {

    @Autowired
    private AttendanceDAO attendanceDAO;

    @Autowired
    private GlobalReportFieldsDAO globalReportFieldsDAO;

    @Autowired
    private OTFSessionUtilDAO otfSessionUtilDAO;

    @Autowired
    private KafkaEntryAuditDAO kafkaEntryAuditDAO;

    @Autowired
    private StudentSubjectUtilDAO studentSubjectUtilDAO;

    public Logger logger = LogFactory.getLogger(AttendanceManager.class);

    public Attendance getSubjectAttendanceCount(String studentId, String subject, Long fromDate) throws BadRequestException {
        List<Attendance> subjectAttendanceList = attendanceDAO.getSubjectAttendance(studentId, subject, DateTimeUtils.getOnlyDateInMillis(fromDate));
        Integer subjectAttendanceCount = Optional.ofNullable(subjectAttendanceList)
                                            .map(Collection::stream)
                                            .orElseGet(Stream::empty)
                                            .map(Attendance::getCount)
                                            .filter(Objects::nonNull)
                                            .mapToInt(Integer::intValue).sum();

        return new Attendance(studentId, subject, subjectAttendanceCount);
    }

    public Attendance getTotalAttendanceCount(String studentId, Long fromDate) throws BadRequestException {
        Integer totalLiveClasses = globalReportFieldsDAO.getTotalLiveClassesAttended(studentId, DateTimeUtils.getOnlyDateInMillis(fromDate));
        return new Attendance(studentId, totalLiveClasses);
    }

    public List<MyWeeklySubjectAttendanceResponse> getGraphSubjectAttendance(String studentId, String subject, Long fromDate) throws BadRequestException {
        List<MyWeeklySubjectAttendanceResponse> resp = new ArrayList<>();
        List<Attendance> attendanceList = attendanceDAO.getSubjectAttendance(studentId, subject, DateTimeUtils.getOnlyDateInMillis(fromDate));

        if(ArrayUtils.isEmpty(attendanceList)) {
            return resp;
        }
        Map<Long, Integer> attendancePerDayMap = new HashMap<>();
        for(Attendance dayAttendance : attendanceList) {
            long date = DateTimeUtils.getOnlyDateInMillis(dayAttendance.getCreationTime());
            int count = Objects.isNull(dayAttendance.getCount())? 0 : dayAttendance.getCount();
            if (attendancePerDayMap.containsKey(date)) {
                attendancePerDayMap.put(date, attendancePerDayMap.get(date) + count);
            } else {
                attendancePerDayMap.put(date, count);
            }
        }
        //weekly aggregate of subject attendance over the time period starting from fromDate
        LocalDate startDate = DateTimeUtils.getLocalStartOfDayFromTimeStamp(fromDate);
        LocalDate endDate = DateTimeUtils.getLocalStartOfDayFromTimeStamp(System.currentTimeMillis());
        int dayCount = 0;
        int weeklyAttendanceCount = 0;
        long dateTimeStamp = DateTimeUtils.getOnlyDateInMillis(fromDate);
        boolean firstNonZeroFound = false;
        MyWeeklySubjectAttendanceResponse respObj = new MyWeeklySubjectAttendanceResponse();
        for(; startDate.isBefore(endDate.plusDays(1)); startDate = startDate.plusDays(1)) {
            dateTimeStamp = startDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
            if (dayCount == 0) {
                respObj = new MyWeeklySubjectAttendanceResponse();
                String fromDateString = DateTimeUtils.getDateFromTimeStamp(dateTimeStamp);
                respObj.setFromDate(fromDateString);
            }
            if (attendancePerDayMap.containsKey(dateTimeStamp)) {
                weeklyAttendanceCount += attendancePerDayMap.get(dateTimeStamp);
            }
            if (weeklyAttendanceCount > 0) {
                firstNonZeroFound = true;
            }
            dayCount++;
            if (dayCount == 7) {
                if (firstNonZeroFound) {
                    respObj.setAttendanceCount(weeklyAttendanceCount);
                    String toDateString = DateTimeUtils.getDateFromTimeStamp(dateTimeStamp);
                    respObj.setToDate(toDateString);
                    resp.add(respObj);
                    firstNonZeroFound = true;
                }
                dayCount = 0;
                weeklyAttendanceCount = 0;
            }
        }
        // if there are still days left, add them to response
        if (weeklyAttendanceCount > 0 || Objects.isNull(respObj.getToDate())) {
            String toDateString = DateTimeUtils.getDateFromTimeStamp(dateTimeStamp);
            respObj.setToDate(toDateString);
            respObj.setAttendanceCount(weeklyAttendanceCount);
            resp.add(respObj);
        }
        logger.info("Attendance graph response: {}", resp);
        return resp;
    }

    public void updateAttendanceCount(GTTAttendeeDetailsInfo gttAttendeeDetailsInfo) throws BadRequestException {
        if(gttAttendeeDetailsInfo.getTimeInSession() > 0 && ArrayUtils.isNotEmpty(gttAttendeeDetailsInfo.getJoinTimes())) {
            List<OTFSessionUtil> otfSessionInfo = otfSessionUtilDAO.getInfoForSession(gttAttendeeDetailsInfo.getSessionId());
            long documentUpdateTime = gttAttendeeDetailsInfo.getStudentSessionJoinTime();
            if (ArrayUtils.isNotEmpty(otfSessionInfo)) {
                String subject = otfSessionInfo.get(0).getSubject();
                if(StringUtils.isNotEmpty(subject)) {
                    if(subject.equals("Mathematics")) {
                        subject = "Maths";
                    }
                    attendanceDAO.updateSubjectAttendance(gttAttendeeDetailsInfo.getUserId(), subject, 1, documentUpdateTime);
                    globalReportFieldsDAO.updateGlobalReportCount(gttAttendeeDetailsInfo.getUserId(), GlobalReportFieldsCounts.Constants.LIVE_CLASSES_ATTENDED, 1, documentUpdateTime);

                    // update student subjects
                    Set<String> updatesSubject = studentSubjectUtilDAO.getStudentSubjects(gttAttendeeDetailsInfo.getUserId());
                    if(!updatesSubject.contains(subject)) {
                        updatesSubject.add(subject);
                        studentSubjectUtilDAO.updateStudentSubjects(gttAttendeeDetailsInfo.getUserId(), updatesSubject);
                    }
                } else {
                    logger.info("Subject info not tagged with otf session {} while marking attendance for student {}", otfSessionInfo.get(0).getId(), gttAttendeeDetailsInfo.getUserId());
                }
            }
        }
    }
}
