package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.MySubjectScoresDAO;
import com.vedantu.reports.dao.StudentSubjectUtilDAO;
import com.vedantu.reports.enums.PageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class StudentSubjectsManager {

    @Autowired
    private StudentSubjectUtilDAO studentSubjectUtilDAO;

    @Autowired
    private MySubjectScoresDAO mySubjectScoresDAO;

    public Set<String> getStudentSubjects(String studentId, PageType pageType) throws BadRequestException {
        Set<String> studentSubjects=new HashSet<>();
        if(PageType.PROGRESS.equals(pageType)) {
            studentSubjects = studentSubjectUtilDAO.getStudentSubjects(studentId);
        }else if(PageType.PERFORMANCE.equals(pageType)){
            studentSubjects = mySubjectScoresDAO.getAllSubjectsForPerformance(studentId);
        }
        return studentSubjects;
    }
}
