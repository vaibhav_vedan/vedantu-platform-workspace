package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.PlatformFeedbackDAO;
import com.vedantu.reports.entities.PlatformFeedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlatformFeedbackManager {

    @Autowired
    private PlatformFeedbackDAO platformFeedbackDAO;

    public List<PlatformFeedback> getFeedback(String studentId) throws BadRequestException {
        return platformFeedbackDAO.getFeedback(studentId);
    }

    public void addFeedback(PlatformFeedback feedback) throws BadRequestException {
        platformFeedbackDAO.save(feedback);
    }
}
