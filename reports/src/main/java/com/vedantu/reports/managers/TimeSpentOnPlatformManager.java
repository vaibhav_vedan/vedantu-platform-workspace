package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.reports.dao.GlobalReportFieldsDAO;
import com.vedantu.reports.entities.GlobalReportFieldsCounts;
import com.vedantu.reports.entities.lms.QuestionAttempt;
import com.vedantu.reports.response.MyActivityTimeResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;

import static com.vedantu.reports.entities.GlobalReportFieldsCounts.Constants.TIME_SPENT_ON_PLATFORM;

@Service
public class TimeSpentOnPlatformManager {

    @Autowired
    private GlobalReportFieldsDAO globalReportFieldsDAO;

    public Logger logger = LogFactory.getLogger(TimeSpentOnPlatformManager.class);

    public double getAvgTimeSpentOnPlatform(String studentId) throws BadRequestException {
        // taking a weekly average here
        long weekAgoTimestamp = (DateTimeUtils.getGMTDateWithoutTimeInMillis() - DateTimeUtils.MILLIS_PER_WEEK);
        List<GlobalReportFieldsCounts> lastWeekTimeSpentList = globalReportFieldsDAO.getTimeSpentOnPlatform(studentId, weekAgoTimestamp);
        if (ArrayUtils.isEmpty(lastWeekTimeSpentList)) {
            return 0d;
        }
        long totalTimeSpentInMillis = 0;
        for(GlobalReportFieldsCounts timeSpentInDay : lastWeekTimeSpentList) {
            totalTimeSpentInMillis += Objects.nonNull(timeSpentInDay.getTimeSpentOnPlatform()) ? timeSpentInDay.getTimeSpentOnPlatform() : 0;
        }
        return (double)(totalTimeSpentInMillis/7);
    }

    public List<MyActivityTimeResponse> getGraphAvgTimeSpentOnPlatform(String studentId, long fromDate) throws BadRequestException {
        List<MyActivityTimeResponse> resp = new ArrayList<>();
        List<GlobalReportFieldsCounts> timeSpentOnPlatformList = globalReportFieldsDAO.getTimeSpentOnPlatform(studentId, DateTimeUtils.getOnlyDateInMillis(fromDate));
        if(ArrayUtils.isEmpty(timeSpentOnPlatformList)) {
            return resp;
        }
        boolean firstNonZeroFound = false;
        TreeMap<Long, Long> timeSpentPerDayMap = new TreeMap<>();
        for (GlobalReportFieldsCounts timeSpentInDayObj : timeSpentOnPlatformList) {
            Long date = DateTimeUtils.getOnlyDateInMillis(timeSpentInDayObj.getCreationTime());
            if (Objects.nonNull(timeSpentInDayObj.getTimeSpentOnPlatform()) && timeSpentInDayObj.getTimeSpentOnPlatform() > 0) {
                firstNonZeroFound = true;
            }
            if (firstNonZeroFound) {
                long timeInMillis = Objects.isNull(timeSpentInDayObj.getTimeSpentOnPlatform()) ? 0 : timeSpentInDayObj.getTimeSpentOnPlatform();
                if (timeSpentPerDayMap.containsKey(date)) {
                    timeSpentPerDayMap.put(date, timeSpentPerDayMap.get(date) + timeInMillis);
                } else {
                    timeSpentPerDayMap.put(date, timeInMillis);
                }
            }
        }
        if (!timeSpentPerDayMap.isEmpty()) {
            LocalDate firstNonZeroDate = DateTimeUtils.getLocalStartOfDayFromTimeStamp(timeSpentPerDayMap.firstKey());
            LocalDate lastDate = DateTimeUtils.getLocalStartOfDayFromTimeStamp(timeSpentPerDayMap.lastKey());
            LocalDate date = firstNonZeroDate;
            long dateTimeStamp;
            for(; date.isBefore(lastDate); date = date.plusDays(1)) {
                dateTimeStamp = date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
                if (!timeSpentPerDayMap.containsKey(dateTimeStamp)) {
                    timeSpentPerDayMap.put(dateTimeStamp, 0L);
                }
            }
            int remainder = timeSpentPerDayMap.size() % 7;
            // add an extra day to get correct graph ranges ( 7 - remainder + 1)
            date = firstNonZeroDate.minusDays((8 - remainder)%7);
            for (; date.isBefore(firstNonZeroDate); date = date.plusDays(1)) {
                dateTimeStamp = date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
                timeSpentPerDayMap.put(dateTimeStamp, 0L);
            }
        }
        for(Long date : timeSpentPerDayMap.keySet()) {
            resp.add(new MyActivityTimeResponse(DateTimeUtils.getDateFromTimeStamp(date), timeSpentPerDayMap.get(date)));
        }
        return resp;
    }

    public void processLiveSessionDetails(GTTAttendeeDetailsInfo gttAttendeeDetailsInfo) throws BadRequestException {
        long mainSessionTimeInMillis = gttAttendeeDetailsInfo.getTimeInSession();
        if (mainSessionTimeInMillis > 0) {
            globalReportFieldsDAO.updateGlobalReportCount(gttAttendeeDetailsInfo.getUserId(), TIME_SPENT_ON_PLATFORM, mainSessionTimeInMillis, gttAttendeeDetailsInfo.getStudentSessionJoinTime());
        }
    }

    public void processReplayDuration(GTTAttendeeDetailsInfo gttAttendeeDetailsInfo, GTTAttendeeDetailsInfo previousDocumentInfo) throws BadRequestException{
        long currentTotalReplayTimeMillis = gttAttendeeDetailsInfo.getTotalReplayWatchedDuration();
        long previousTotalReplayTimeMillis = previousDocumentInfo.getTotalReplayWatchedDuration();
        if(currentTotalReplayTimeMillis > previousTotalReplayTimeMillis ) {
            long currentReplayDurationWatched = currentTotalReplayTimeMillis - previousTotalReplayTimeMillis;
            globalReportFieldsDAO.updateGlobalReportCount(gttAttendeeDetailsInfo.getUserId(), TIME_SPENT_ON_PLATFORM, currentReplayDurationWatched, gttAttendeeDetailsInfo.getLastUpdated());
        }
    }

    public void processTestDurationTime(QuestionAttempt questionAttempt) throws BadRequestException {
        long documentCreationTime=questionAttempt.getCreationTime();
        if(Objects.nonNull(questionAttempt.getDuration()) && questionAttempt.getDuration() > 0) {
            long testAttemptDurationMillis = questionAttempt.getDuration();
            globalReportFieldsDAO.updateGlobalReportCount(questionAttempt.getUserId(), TIME_SPENT_ON_PLATFORM, testAttemptDurationMillis, documentCreationTime);
        }
    }
}
