package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.KafkaEntryAuditDAO;
import com.vedantu.reports.entities.KafkaEntityAudit;
import com.vedantu.reports.enums.ProcessingStatus;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;

@Service
public class KafkaEntityAuditManager {

    @Autowired
    private KafkaEntryAuditDAO kafkaEntryAuditDAO;

    public Logger logger = LogFactory.getLogger(KafkaEntityAuditManager.class);

    public boolean isDocumentAlreadyProcessed(String documentId) throws BadRequestException {
        KafkaEntityAudit audit = kafkaEntryAuditDAO.getRecentlyProcessedDocument(documentId, Collections.singletonList(KafkaEntityAudit.Constants.PROCESSING_STATUS));
        return Objects.nonNull(audit) && ProcessingStatus.PROCESSED.equals(audit.getProcessingStatus());
    }
}
