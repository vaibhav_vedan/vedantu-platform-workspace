package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.AverageTimePerQuestionDAO;
import com.vedantu.reports.entities.AverageTimePerQuestion;
import com.vedantu.reports.request.StudentSubjectRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AverageTimePerQuestionManager {

    @Autowired
    private AverageTimePerQuestionDAO averageTimePerQuestionDAO;

    public Logger logger = LogFactory.getLogger(TotalCorrectAnswerManager.class);

    public AverageTimePerQuestion getAverageTimePerQuestion(StudentSubjectRequest req) throws BadRequestException {
        List<AverageTimePerQuestion> averageTimeDataOfStudent = averageTimePerQuestionDAO.getAverageTimeDataOfStudent(req.getStudentId(), req.getSubject(), req.getFromDate());
        if(ArrayUtils.isEmpty(averageTimeDataOfStudent)){
            return null;
        }
        Double totalTime = 0D;
        Integer count = 0;
        for(AverageTimePerQuestion averageTimePerQuestion:averageTimeDataOfStudent){
            totalTime += averageTimePerQuestion.getTotalTime();
            count += averageTimePerQuestion.getCount();
        }
        Double averageTime = (count > 0)? (totalTime/count) : totalTime;
        return new AverageTimePerQuestion(req.getStudentId(),req.getSubject(),averageTime,count);
    }
}
