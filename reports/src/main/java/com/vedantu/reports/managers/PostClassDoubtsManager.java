package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.GlobalReportFieldsDAO;
import com.vedantu.reports.dao.PostClassDoubtsDAO;
import com.vedantu.reports.dao.StudentSubjectUtilDAO;
import com.vedantu.reports.entities.GlobalReportFieldsCounts;
import com.vedantu.reports.entities.PostClassDoubtsAsked;
import com.vedantu.reports.entities.lms.Doubt;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PostClassDoubtsManager {

    @Autowired
    private PostClassDoubtsDAO postClassDoubtsDAO;

    @Autowired
    private GlobalReportFieldsDAO globalReportFieldsDAO;

    @Autowired
    private StudentSubjectUtilDAO studentSubjectUtilDAO;

    public Logger logger = LogFactory.getLogger(PostClassDoubtsManager.class);

    public PostClassDoubtsAsked getSubjectPostClassDoubtsAsked(String studentId, String subject, Long fromDate) throws BadRequestException  {
        Integer subjectDoubts = postClassDoubtsDAO.getSubjectPostClassDoubtsAsked(studentId, subject, DateTimeUtils.getOnlyDateInMillis(fromDate));
        return new PostClassDoubtsAsked(studentId, subject, subjectDoubts);
    }

    public PostClassDoubtsAsked getTotalPostClassDoubtsAsked(String studentId, Long fromDate) throws BadRequestException {
        Integer totalDoubts = globalReportFieldsDAO.getTotalPostClassDoubtsAsked(studentId, DateTimeUtils.getOnlyDateInMillis(fromDate));
        return new PostClassDoubtsAsked(studentId, totalDoubts);
    }

    public void processDoubts(Doubt doubt) throws BadRequestException {
        logger.info("Processing doubts");
        logger.info("Doubt received: {}",doubt);
        long documentCreationTime = doubt.getCreationTime();
        Set<String> subjects=
                Optional.ofNullable(doubt.gettSubjects())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(this::mapMathematicsToMathIfNeeded)
                        .collect(Collectors.toSet());

        if(ArrayUtils.isNotEmpty(subjects)) {
            for (String subject : subjects) {
                postClassDoubtsDAO.updatePostClassDoubtsAsked(doubt.getStudentId(), subject, 1, documentCreationTime);
            }
            // update student subjects
            Set<String> updatesSubject = studentSubjectUtilDAO.getStudentSubjects(doubt.getStudentId());
            if(!updatesSubject.containsAll(subjects)) {
                updatesSubject.addAll(subjects);
                studentSubjectUtilDAO.updateStudentSubjects(doubt.getStudentId(), updatesSubject);
            }
        }
        globalReportFieldsDAO.updateGlobalReportCount(doubt.getStudentId(), GlobalReportFieldsCounts.Constants.POST_CLASS_DOUBTS_ASKED, 1, documentCreationTime);
    }

    private String mapMathematicsToMathIfNeeded(String subject) {
        return subject.equals("Mathematics")?"Maths":subject;
    }
}
