package com.vedantu.reports.managers;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.lms.cmds.pojo.AnalyticsTypes;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import com.vedantu.reports.dao.AverageTimePerQuestionDAO;
import com.vedantu.reports.dao.FocusAreasDAO;
import com.vedantu.reports.dao.KafkaEntryAuditDAO;
import com.vedantu.reports.dao.MySubjectScoresDAO;
import com.vedantu.reports.dao.TotalCorrectAnswersDAO;
import com.vedantu.reports.entities.KafkaEntityAudit;
import com.vedantu.reports.entities.lms.CMDSTestAttempt;
import com.vedantu.reports.enums.ProcessingStatus;
import com.vedantu.reports.pojo.FocusArea;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CMDSTestAttemptKafkaManager {

    @Autowired
    private TotalCorrectAnswersDAO totalCorrectAnswersDAO;

    @Autowired
    private AverageTimePerQuestionDAO averageTimePerQuestionDAO;

    @Autowired
    private MySubjectScoresDAO mySubjectScoresDAO;

    @Autowired
    private FocusAreasDAO focusAreasDAO;

    @Autowired
    private KafkaEntryAuditDAO kafkaEntryAuditDAO;

    public Logger logger = LogFactory.getLogger(CMDSTestAttemptKafkaManager.class);
    private static final Gson gson=new Gson();


    public ProcessingStatus processForTestAttemptData(CMDSTestAttempt cmdsTestAttempt) throws BadRequestException {
        ProcessingStatus processingStatus = ProcessingStatus.NOT_USED;
        long documentCreationTime=cmdsTestAttempt.getCreationTime();
        logger.info("processForTestAttemptData");
        logger.info("cmdsTestAttempt {}",cmdsTestAttempt);
        if(Objects.nonNull(cmdsTestAttempt.getRank())){
            logger.info("This test attempt has rank so processing for the same");
            if(cmdsTestAttempt.getReevaluationNumber()==0){
                logger.info("Processing for test attempt id {}",cmdsTestAttempt.getId());
                logger.info("Processing for a fresh evaluation with category analytics value...");
                deletePreviousDocumentProgress(cmdsTestAttempt,documentCreationTime);
                insertCurrentDocumentProgress(cmdsTestAttempt,documentCreationTime);
                processingStatus=ProcessingStatus.PROCESSED;
            }else if(cmdsTestAttempt.getReevaluationNumber()>0){
                logger.info("Processing for reevaluation for attempt id {}",cmdsTestAttempt.getId());
                deletePreviousDocumentProgress(cmdsTestAttempt,documentCreationTime);
                insertCurrentDocumentProgress(cmdsTestAttempt,documentCreationTime);
                processingStatus=ProcessingStatus.PROCESSED;
            }else{
                logger.info("The document is already processed for the first attempt.");
            }
        }
        return processingStatus;
    }

    private void insertCurrentDocumentProgress(CMDSTestAttempt cmdsTestAttempt, long documentCreationTime) throws BadRequestException {

        logger.info("insertCurrentDocumentProgress");

        List<CategoryAnalytics> averageAnalyticsObjects = cmdsTestAttempt.getCategoryAnalyticsList().stream().filter(this::isDataSuitableForAveraging).peek(logger::info).collect(Collectors.toList());

        String studentId = cmdsTestAttempt.getStudentId();
        String testId = cmdsTestAttempt.getTestId();
        if(ArrayUtils.isNotEmpty(averageAnalyticsObjects)){
            logger.info("Inserting objects");
            for(CategoryAnalytics categoryAnalytics:averageAnalyticsObjects){
                logger.info("Inserting for analytics data {}",categoryAnalytics);
                totalCorrectAnswersDAO.updateCorrectIncorrectCountForUser(studentId,categoryAnalytics.getAttempted()+categoryAnalytics.getUnattempted(),categoryAnalytics.getCorrect(),categoryAnalytics.getSubject(),documentCreationTime);
                averageTimePerQuestionDAO.updateQuestionCountAndTimeForUser(studentId,categoryAnalytics.getAttempted(),categoryAnalytics.getDuration(),categoryAnalytics.getSubject(),documentCreationTime);
                mySubjectScoresDAO.updatePercentageMySubjectScoresForTest(studentId, testId,categoryAnalytics.getSubject(),(categoryAnalytics.getMarks()*100)/categoryAnalytics.getMaxMarks(),documentCreationTime);
            }
        }

        Map<String, List<CategoryAnalytics>> topicObjectMap = cmdsTestAttempt.getCategoryAnalyticsList().stream().filter(this::isTopicData).collect(Collectors.groupingBy(CategoryAnalytics::getSubject));
        List<FocusArea> focusAreas=new ArrayList<>();
        for(String subject:topicObjectMap.keySet()){
            List<CategoryAnalytics> categoryAnalytics=topicObjectMap.get(subject);
            categoryAnalytics.sort(Comparator.comparingDouble(CategoryAnalytics::getMarks));
            List<String> weakTopics = categoryAnalytics.stream().map(CategoryAnalytics::getTopic).collect(Collectors.toList());
            if(ArrayUtils.isNotEmpty(weakTopics) && weakTopics.size()>3){
                weakTopics=weakTopics.subList(0,3);
            }
            focusAreas.add(new FocusArea(subject,weakTopics));
        }
        if(ArrayUtils.isNotEmpty(focusAreas)) {
            focusAreasDAO.updateFocusAreasAccordingToRecentTestScore(studentId, testId, focusAreas);
        }
    }

    private void deletePreviousDocumentProgress(CMDSTestAttempt cmdsTestAttempt, long documentCreationTime) throws BadRequestException {

        logger.info("deletePreviousDocumentProgress");

        KafkaEntityAudit kafkaEntityAudit=kafkaEntryAuditDAO.getRecentlyProcessedDocument(cmdsTestAttempt.getId(),null);
        if(Objects.isNull(kafkaEntityAudit)){
            return;
        }
        String document=kafkaEntityAudit.getDocument();
        CMDSTestAttempt previousAttempt=gson.fromJson(document, CMDSTestAttempt.class);
        if(Objects.nonNull(previousAttempt)) {
            logger.info("Reverting for reevaluation no {} for current evaluation no {}",previousAttempt.getReevaluationNumber(),cmdsTestAttempt.getReevaluationNumber());
            List<CategoryAnalytics> averageAnalyticsObjects =
                    Optional.ofNullable(previousAttempt.getCategoryAnalyticsList())
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .filter(this::isDataSuitableForAveraging)
                            .peek(logger::info)
                            .collect(Collectors.toList());

            String studentId = previousAttempt.getStudentId();
            if (ArrayUtils.isNotEmpty(averageAnalyticsObjects)) {
                logger.info("Removing previous data");
                for (CategoryAnalytics categoryAnalytics : averageAnalyticsObjects) {
                    logger.info("Reverting for analytics data {}", categoryAnalytics);
                    averageTimePerQuestionDAO.updateQuestionCountAndTimeForUser(studentId,-1*categoryAnalytics.getAttempted(),-1*categoryAnalytics.getDuration(),categoryAnalytics.getSubject(),documentCreationTime);
                    totalCorrectAnswersDAO.updateCorrectIncorrectCountForUser(studentId, -1 * (categoryAnalytics.getAttempted() + categoryAnalytics.getUnattempted()), -1 * categoryAnalytics.getCorrect(), categoryAnalytics.getSubject(), documentCreationTime);
                }
            }
        }else{
            logger.info("No data available for processing.");
        }
    }

    private boolean isTopicData(CategoryAnalytics categoryAnalytics) {
        return AnalyticsTypes.TOPIC.equals(categoryAnalytics.getCategoryType());
    }

    private boolean isDataSuitableForAveraging(CategoryAnalytics categoryAnalytics) {
        return AnalyticsTypes.SUBJECT.equals(categoryAnalytics.getCategoryType());
    }

}
