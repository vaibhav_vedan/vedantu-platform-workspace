package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.lms.cmds.pojo.AnalyticsTypes;
import com.vedantu.reports.dao.KafkaEntryAuditDAO;
import com.vedantu.reports.dao.TestScoreAggregatorUtilDAO;
import com.vedantu.reports.entities.KafkaEntityAudit;
import com.vedantu.reports.entities.lms.CMDSTest;
import com.vedantu.reports.enums.ProcessingStatus;
import com.vedantu.reports.kafka.model.MessageMeta;
import com.vedantu.reports.pojo.PerformanceReportSection;
import com.vedantu.reports.pojo.SubjectScore;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CMDSTestKafkaManager {

    @Autowired
    private TestScoreAggregatorUtilDAO testScoreAggregatorUtilDAO;

    @Autowired
    private KafkaEntryAuditDAO kafkaEntryAuditDAO;

    public Logger logger = LogFactory.getLogger(CMDSTestKafkaManager.class);

    public void processDataForTestInfo(CMDSTest cmdsTest) throws BadRequestException {

        logger.info("processDataForTestInfo");

        if(ArrayUtils.isNotEmpty(cmdsTest.getAnalytics())){
            logger.info("Going for calculation of analytics");
            List<SubjectScore> subjectScores =
                    cmdsTest.getAnalytics()
                            .stream()
                            .filter(this::getFilteredSubjectInfo)
                            .map(this::convertIntoSubjectScoreObject)
                            .collect(Collectors.toList());

            if(ArrayUtils.isNotEmpty(subjectScores)) {
                testScoreAggregatorUtilDAO.updateDataForTestScore(cmdsTest.getId(), cmdsTest.getName(), subjectScores);
            }
        }
    }

    private SubjectScore convertIntoSubjectScoreObject(PerformanceReportSection performanceReportSection) {
        return new SubjectScore(performanceReportSection.getSubject(),performanceReportSection.getAverage(),performanceReportSection.getTotal());
    }

    private boolean getFilteredSubjectInfo(PerformanceReportSection performanceReportSection) {
        return AnalyticsTypes.SUBJECT.equals(performanceReportSection.getType());
    }
}