package com.vedantu.reports.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.TotalCorrectAnswersDAO;
import com.vedantu.reports.entities.TotalCorrectAnswers;
import com.vedantu.reports.request.StudentSubjectRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TotalCorrectAnswerManager {

    @Autowired
    private TotalCorrectAnswersDAO totalCorrectAnswersDAO;

    public Logger logger = LogFactory.getLogger(TotalCorrectAnswerManager.class);

    public List<TotalCorrectAnswers> getTotalCorrectAnswer(StudentSubjectRequest req) throws BadRequestException {
        return totalCorrectAnswersDAO.getTotalCorrectAnswerDataOfStudent(req.getStudentId(),req.getSubject(), req.getFromDate());
    }
}
