package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.OTFSessionUtil;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.vedantu.reports.entities.OTFSessionUtil.Constants.*;

@Repository
public class OTFSessionUtilDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(OTFSessionUtilDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public OTFSessionUtilDAO() {
        super();
    }

    public void updateSessionSubjects(String sessionId, String subject, String sessionTitle) throws BadRequestException {
        if(StringUtils.isEmpty(sessionId) || StringUtils.isEmpty(subject)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(SESSION_ID).is(sessionId));

        Update update = new Update();
        update.set(SUBJECT, subject);
        update.set(SESSION_TITLE , sessionTitle);
        update.set(LAST_UPDATED, System.currentTimeMillis());
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);

        upsertEntity(query, update, OTFSessionUtil.class);
    }

    public List<OTFSessionUtil> getInfoForSession(String sessionId) throws BadRequestException {
        if(StringUtils.isEmpty(sessionId)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(SESSION_ID).is(sessionId));
        logger.info("Fetching info for sessionId {}", sessionId);

        return runQuery(query, OTFSessionUtil.class);
    }
}
