package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.TimeSpentOnPlatform;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class TimeSpentOnPlatformDAO extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(TimeSpentOnPlatformDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public TimeSpentOnPlatformDAO() {
        super();
    }

//    public void updateTimeSpentOnPlatform(String studentId, String subject, Integer count) throws BadRequestException  {
//        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(count)) {
//            throwInvalidOrMissingParameterException();
//        }
//
//        Query query = reportsUpsertQuery();
//        query.addCriteria(Criteria.where(TimeSpentOnPlatform.Constants.STUDENT_ID).is(studentId));
//        query.addCriteria(Criteria.where(TimeSpentOnPlatform.Constants.SUBJECT).is(subject));
//
//        Update update = new Update();
//        update.set(TimeSpentOnPlatform.Constants.COUNT, count);
//        update.set(TimeSpentOnPlatform.Constants.LAST_UPDATED_BY, TimeSpentOnPlatform.Constants.UPDATED_BY_SYSTEM);
//        update.set(TimeSpentOnPlatform.Constants.LAST_UPDATED, System.currentTimeMillis());
//
//        upsertEntity(query, update, TimeSpentOnPlatform.class);
//    }
}
