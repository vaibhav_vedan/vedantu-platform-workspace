package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.KafkaEntityAudit;
import com.vedantu.reports.enums.ProcessingStatus;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static com.vedantu.reports.entities.KafkaEntityAudit.Constants.*;

@Repository
public class KafkaEntryAuditDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(KafkaEntryAuditDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public KafkaEntryAuditDAO() {
        super();
    }

    public void save(KafkaEntityAudit audit) throws BadRequestException {
        if(Objects.isNull(audit)){
            throwInvalidOrMissingParameterException();
        }

        saveEntity(audit);
    }

    public KafkaEntityAudit getProcessingStatusForMessageId(String id) throws BadRequestException {

        if(StringUtils.isEmpty(id)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(MESSAGE_ID).is(id));

        query.fields().include(PROCESSING_STATUS);

        return findOne(query,KafkaEntityAudit.class);
    }

    public void updateFinalProcessingStatus(String id, ProcessingStatus processingStatus) throws BadRequestException {
        updateFinalProcessingStatus(id, processingStatus, null);
    }

    public void updateFinalProcessingStatus(String id, ProcessingStatus processingStatus, String document) throws BadRequestException {

        if(StringUtils.isEmpty(id) || Objects.isNull(processingStatus)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(MESSAGE_ID).is(id));

        Update update=new Update();
        update.set(PROCESSING_STATUS, processingStatus);
        update.set(PROCESSING_END_TIME,System.currentTimeMillis());

        if(StringUtils.isNotEmpty(document)) {
            update.set(DOCUMENT, document);
        }
        logger.info("updateFinalProcessingStatus - query - {}",query);
        updateFirst(query,update,KafkaEntityAudit.class);
    }

    public KafkaEntityAudit getRecentlyProcessedDocument(String documentId, List<String> includeFields) throws BadRequestException {

        if(StringUtils.isEmpty(documentId)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(DOCUMENT_ID).is(documentId));
        query.addCriteria(Criteria.where(PROCESSING_STATUS).is(ProcessingStatus.PROCESSED));

        query.with(Sort.by(Sort.Direction.DESC, _ID));

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        return findOne(query,KafkaEntityAudit.class);
    }

}
