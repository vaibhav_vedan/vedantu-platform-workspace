package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.Attendance;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import static com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity.Constants.*;


@Repository
public class AttendanceDAO extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(AttendanceDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public AttendanceDAO() {
        super();
    }

    public void updateSubjectAttendance(String studentId, String subject, Integer count, long documentUpdateTime) throws BadRequestException  {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(count)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = reportsUpsertQuery(documentUpdateTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));

        Update update = new Update();
        update.inc(COUNT, count);
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);
        update.set(LAST_UPDATED, System.currentTimeMillis());

        upsertEntity(query, update, Attendance.class);
    }

    public List<Attendance> getSubjectAttendance(String studentId, String subject, Long fromDate) throws BadRequestException {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));
        if (Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        }

        return runQuery(query, Attendance.class);
    }
 }
