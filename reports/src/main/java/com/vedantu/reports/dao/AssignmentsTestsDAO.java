package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.AssignmentsTests;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import static com.vedantu.reports.entities.AssignmentsTests.Constants.*;

@Repository
public class AssignmentsTestsDAO extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(AssignmentsTestsDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public AssignmentsTestsDAO() {
        super();
    }

    public void save(AssignmentsTests test) {
        try {
            saveEntity(test);
        } catch (Exception ex) {
            throw new RuntimeException("Update assignment and test error " + test.getId(), ex);
        }
    }

    public void updateAssignmentAndTestCount(String studentId, String subject, Integer count, String option, long documentCreationTime) throws BadRequestException {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(count) || StringUtils.isEmpty(option)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = reportsUpsertQuery(documentCreationTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));

        Update update = new Update();
        switch (option) {
            case "assignmentsShared":
                update.inc(ASSIGNMENTS_SHARED, count);
                break;
            case "assignmentsAttempted":
                update.inc(ASSIGNMENTS_ATTEMPTED, count);
                break;
            case "testsShared":
                update.inc(TESTS_SHARED, count);
                break;
            case "testsCompleted":
                update.inc(TESTS_COMPLETED, count);
                break;
            default:
                return;
        }
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);
        update.set(LAST_UPDATED, System.currentTimeMillis());

        upsertEntity(query, update, AssignmentsTests.class);
    }

    public List<AssignmentsTests> getSubjectAssignmentTestCount(String studentId, String subject, Long fromDate) throws BadRequestException {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));
        if(Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        }
        return runQuery(query, AssignmentsTests.class);
    }
}
