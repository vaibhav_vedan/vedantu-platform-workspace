package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.MySubjectScores;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vedantu.reports.entities.MySubjectScores.Constants.*;

@Repository
public class MySubjectScoresDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(MySubjectScoresDAO.class);
    private static final String OVERALL = "Overall";


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public MySubjectScoresDAO() {
        super();
    }

    public void updatePercentageMySubjectScoresForTest(String studentId, String testId, String subject, float percentage, long documentCreationTime) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(testId) || StringUtils.isEmpty(subject)){
            throwInvalidOrMissingParameterException();
        }

        Query query = reportsUpsertQuery(documentCreationTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));
        query.addCriteria(Criteria.where(TEST_ID).is(testId));

        Update update = new Update();
        update.set(PERCENTAGE, percentage);
        update.set(LAST_UPDATED_BY, "SYSTEM");
        update.set(LAST_UPDATED, System.currentTimeMillis());

        upsertEntity(query, update, MySubjectScores.class);
    }

    public List<MySubjectScores> getSubjectScoresForStudentId(String studentId, Long fromDate, int start, int size) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || Objects.isNull(fromDate)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        query.skip(start);
        query.limit(size);

        logger.info("getSubjectScoresForStudentId  - query - {}",query);

        return runQuery(query,MySubjectScores.class);
    }

    public Set<String> getAllTestIdsForStudent(String studentId, Long fromDate) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || Objects.isNull(fromDate)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));

        query.fields().include(TEST_ID);

        query.fields().exclude(_ID);

        logger.info("getAllTestIdsForStudent - query - {}",query);

        return Optional.ofNullable(runQuery(query,MySubjectScores.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(MySubjectScores::getTestId)
                        .collect(Collectors.toSet());
    }

    public List<MySubjectScores> getMySubjectWisePercentagePercentileScore(String studentId, String subject, Long fromTime) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || Objects.isNull(fromTime) || StringUtils.isEmpty(subject)){
            throwInvalidOrMissingParameterException();
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));
        query.addCriteria(Criteria.where(CREATION_TIME).gte(fromTime));

        query.fields()
                .include(PERCENTAGE)
                .include(PERCENTILE)
                .include(TEST_ID)
                .include(CREATION_TIME)
                .exclude(_ID);

        return runQuery(query,MySubjectScores.class);
    }

    public Set<String> getAllSubjectsForPerformance(String studentId) {
        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        return Optional.ofNullable(findDistinct(query,SUBJECT,MySubjectScores.class,String.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .filter(subject ->!OVERALL.equals(subject))
                        .collect(Collectors.toSet());
    }
}
