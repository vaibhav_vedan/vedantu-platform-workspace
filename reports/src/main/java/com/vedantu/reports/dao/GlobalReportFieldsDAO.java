package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.GlobalReportFieldsCounts;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static com.vedantu.reports.entities.GlobalReportFieldsCounts.Constants.*;


@Repository
public class GlobalReportFieldsDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(GlobalReportFieldsDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public GlobalReportFieldsDAO() {
        super();
    }

    public int getTotalPostClassDoubtsAsked(String studentId, Long fromDate) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        if (Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        }
        query.fields().include(POST_CLASS_DOUBTS_ASKED);
        query.fields().exclude(_ID);

        return Optional.ofNullable(runQuery(query, GlobalReportFieldsCounts.class))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(GlobalReportFieldsCounts::getPostClassDoubtsAsked)
                .filter(Objects::nonNull)
                .mapToInt(Integer::intValue).sum();
    }

    public int getTotalLiveClassesAttended(String studentId, Long fromDate) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        if (Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        }

        query.fields().include(LIVE_CLASSES_ATTENDED);
        query.fields().exclude(_ID);
        return Optional.ofNullable(runQuery(query, GlobalReportFieldsCounts.class))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(GlobalReportFieldsCounts::getLiveClassesAttended)
                .filter(Objects::nonNull)
                .mapToInt(Integer::intValue).sum();
    }

    public List<GlobalReportFieldsCounts> getAssignmentsTestsCount(String studentId, Long fromDate) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        if (Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        }

        query.fields().include(ASSIGNMENTS_ATTEMPTED);
        query.fields().include(ASSIGNMENTS_SHARED);
        query.fields().include(TESTS_SHARED);
        query.fields().include(TESTS_COMPLETED);

        return runQuery(query, GlobalReportFieldsCounts.class);
    }

    public List<GlobalReportFieldsCounts> getTimeSpentOnPlatform(String studentId, long timeStamp) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(CREATION_TIME).gte(timeStamp));

        query.fields().include(TIME_SPENT_ON_PLATFORM);
        query.fields().include(CREATION_TIME);
        query.fields().exclude(_ID);

        return runQuery(query, GlobalReportFieldsCounts.class);
    }

    public void updateGlobalReportCount(String studentId, String option, long count, long documentCreationTime) throws BadRequestException  {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(option)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = reportsUpsertQuery(documentCreationTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        Update update = new Update();
        switch (option) {
            case "assignmentsShared":
                update.inc(ASSIGNMENTS_SHARED, count);
                break;
            case "assignmentsAttempted":
                update.inc(ASSIGNMENTS_ATTEMPTED, count);
                break;
            case "testsShared":
                update.inc(TESTS_SHARED, count);
                break;
            case "testsCompleted":
                update.inc(TESTS_COMPLETED, count);
                break;
            case "postClassDoubtsAsked":
                update.inc(POST_CLASS_DOUBTS_ASKED, count);
                break;
            case "timeSpentOnPlatform":
                update.inc(TIME_SPENT_ON_PLATFORM, count);
                break;
            case "liveClassesAttended":
                update.inc(LIVE_CLASSES_ATTENDED, count);
                break;
            default:
                return;
        }
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);
        update.set(LAST_UPDATED, System.currentTimeMillis());

        upsertEntity(query, update, GlobalReportFieldsCounts.class);
    }
}
