package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.PlatformFeedback;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import static com.vedantu.reports.entities.PlatformFeedback.Constants.STUDENT_ID;

@Repository
public class PlatformFeedbackDAO extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(PlatformFeedbackDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public PlatformFeedbackDAO() {
        super();
    }

    public void save(PlatformFeedback feedback) throws BadRequestException {
        if (Objects.isNull(feedback)) {
            throwInvalidOrMissingParameterException();
        }
        try{
            saveEntity(feedback);
        } catch (Exception e) {
            throw new RuntimeException("Save platform feedback error " + feedback.getId(), e);
        }
    }

    public List<PlatformFeedback> getFeedback(String studentId) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        return runQuery(query, PlatformFeedback.class);
    }
}
