package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.TotalCorrectAnswers;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import static com.vedantu.reports.entities.TotalCorrectAnswers.Constants.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

@Repository
public class TotalCorrectAnswersDAO extends AbstractMongoDAO{

    private Logger logger = LogFactory.getLogger(TotalCorrectAnswersDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public TotalCorrectAnswersDAO() {
        super();
    }

    public void updateCorrectIncorrectCountForUser(String studentId, int totalQuestions, Integer correct, String subject, long documentCreationTime) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || Objects.isNull(correct) || Math.abs(correct)>Math.abs(totalQuestions) || StringUtils.isEmpty(subject)){
            throwInvalidOrMissingParameterException();
        }

        Query query=reportsUpsertQuery(documentCreationTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));

        Update update=new Update();
        update.inc(COUNT,totalQuestions);
        update.inc(CORRECT_ANSWERS,correct);
        update.set(SUBJECT,subject);
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);
        update.set(LAST_UPDATED,System.currentTimeMillis());

        logger.info("updateCorrectIncorrectCountForUser - query - {}",query);
        logger.info("updateCorrectIncorrectCountForUser - update - {}",update);

        upsertEntity(query,update,TotalCorrectAnswers.class);
    }

    public List<TotalCorrectAnswers> getTotalCorrectAnswerDataOfStudent(String studentId, String subject, Long fromDate) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(fromDate)){
            throwInvalidOrMissingParameterException();
        }

        Criteria studentCriteria = Criteria.where(STUDENT_ID).is(studentId);
        Criteria subjectCriteria;
        Criteria dateFilterCriteria = Criteria.where(CREATION_TIME).gte(DateTimeUtils.getOnlyDateInMillis(fromDate));
        if(StringUtils.isNotEmpty(subject)) {
            subjectCriteria=Criteria.where(SUBJECT).is(subject);
        }else{
            subjectCriteria=Criteria.where(SUBJECT).is("Overall");
        }

        Criteria filter = new Criteria().andOperator(studentCriteria,subjectCriteria,dateFilterCriteria);

        AggregationOptions aggregationOptions= Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation =
                newAggregation(
                    match(filter),
                    group(STUDENT_ID).sum(CORRECT_ANSWERS).as(CORRECT_ANSWERS).sum(COUNT).as(COUNT),
                    project(CORRECT_ANSWERS,COUNT))
                .withOptions(aggregationOptions);
        logger.info("aggregation query is {}",aggregation);
        AggregationResults<TotalCorrectAnswers> groupResult = getMongoOperations().aggregate(aggregation, TotalCorrectAnswers.class.getSimpleName(), TotalCorrectAnswers.class);
        return groupResult.getMappedResults();
    }
}
