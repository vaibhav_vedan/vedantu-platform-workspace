package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.StudentSubjectUtil;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.vedantu.reports.entities.StudentSubjectUtil.Constants.STUDENT_ID;
import static com.vedantu.reports.entities.StudentSubjectUtil.Constants.SUBJECTS;

@Repository
public class StudentSubjectUtilDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(StudentSubjectUtilDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public StudentSubjectUtilDAO() {
        super();
    }

    public Set<String> getStudentSubjects(String studentId) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        StudentSubjectUtil studentSubject = findOne(query, StudentSubjectUtil.class);

        if(Objects.nonNull(studentSubject) && ArrayUtils.isNotEmpty(studentSubject.getSubjects())){
            return new HashSet<>(studentSubject.getSubjects());
        }
        return new HashSet<>();
    }

    public void updateStudentSubjects(String studentId, Set<String> subjects) throws BadRequestException {
        if(StringUtils.isEmpty(studentId) || ArrayUtils.isEmpty(subjects)) {
            throwInvalidOrMissingParameterException();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        Update update = new Update();
        update.set(SUBJECTS, new ArrayList<>(subjects));

        upsertEntity(query, update, StudentSubjectUtil.class);
    }
}
