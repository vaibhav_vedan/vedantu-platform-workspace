package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.TestScoreAggregatorUtil;
import com.vedantu.reports.pojo.SubjectScore;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

import static com.vedantu.reports.entities.TestScoreAggregatorUtil.Constants.*;

@Repository
public class TestScoreAggregatorUtilDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(TestScoreAggregatorUtilDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public TestScoreAggregatorUtilDAO() {
        super();
    }

    public void updateDataForTestScore(String testId, String testName, List<SubjectScore> subjectScores) throws BadRequestException {

        if(StringUtils.isEmpty(testId) || ArrayUtils.isEmpty(subjectScores)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(TEST_ID).is(testId));
        query.addCriteria(Criteria.where(TEST_NAME).is(testName));

        Update update=new Update();
        update.set(SUBJECT_SCORES_LIST,subjectScores);

        upsertEntity(query,update,TestScoreAggregatorUtil.class);
    }

    public List<TestScoreAggregatorUtil> getAllTestScoreDataForTestIds(Set<String> testIds) throws BadRequestException {

        if(ArrayUtils.isEmpty(testIds)){
            throwInvalidOrMissingParameterException();
        }

        return getAllTestScoreDataForTestIds(testIds,null);
    }
    public List<TestScoreAggregatorUtil> getAllTestScoreDataForTestIds(Set<String> testIds,List<String> includeFields) throws BadRequestException {

        if(ArrayUtils.isEmpty(testIds)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(TEST_ID).in(testIds));

        logger.info("getAllTestScoreDataForTestIds - query - {}",query);

        return runQuery(query,TestScoreAggregatorUtil.class,includeFields);
    }
}
