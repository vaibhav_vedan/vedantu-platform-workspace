package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.AverageTimePerQuestion;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import static com.vedantu.reports.entities.AverageTimePerQuestion.Constants.*;

@Repository
public class AverageTimePerQuestionDAO extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(AverageTimePerQuestionDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public AverageTimePerQuestionDAO() {
        super();
    }

    public void updateQuestionCountAndTimeForUser(String studentId, Integer attempted, Integer duration, String subject, long documentCreationTime) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || Objects.isNull(attempted) || Objects.isNull(duration) || StringUtils.isEmpty(subject)){
            throwInvalidOrMissingParameterException();
        }

        Query query = reportsUpsertQuery(documentCreationTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));

        Update update = new Update();
        update.inc(COUNT, attempted);
        update.inc(TOTAL_TIME, duration);
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);
        update.set(LAST_UPDATED, System.currentTimeMillis());

        upsertEntity(query, update, AverageTimePerQuestion.class);
    }

    public List<AverageTimePerQuestion> getAverageTimeDataOfStudent(String studentId, String subject, Long fromDate) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(fromDate)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        if(StringUtils.isNotEmpty(subject)) {
            query.addCriteria(Criteria.where(SUBJECT).is(subject));
        }else{
            query.addCriteria(Criteria.where(SUBJECT).is("Overall"));
        }
        if(Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(DateTimeUtils.getOnlyDateInMillis(fromDate)));
        }
        return runQuery(query,AverageTimePerQuestion.class);
    }
}
