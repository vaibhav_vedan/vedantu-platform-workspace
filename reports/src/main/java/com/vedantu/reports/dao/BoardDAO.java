package com.vedantu.reports.dao;

import com.google.gson.Gson;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.board.Board;
import com.vedantu.reports.redis.RedisDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BoardDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(BoardDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    private final Gson gson = new Gson();
    private static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static Integer redisExpiry = ConfigUtils.INSTANCE.getIntValue("redis.expiry.sessionvsserverkey");

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public BoardDAO() {
        super();
    }

    public void upsert(Board p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public List<Board> getBoards(Collection<Long> ids) {
        logger.info("getBoards"+ ids);
        List<Board> boards = null;
        if (CollectionUtils.isEmpty(ids)) {
            return boards;
        }
        boards = new ArrayList<>();
        Map<Long, Board> boardsMap = getBoardsMapFromRedis(ids);

        List<Long> fetchList = new ArrayList<>();

        if (!boardsMap.isEmpty()) {
            for (Long id : ids) {
                if (!boardsMap.keySet().contains(id)) {
                    fetchList.add(id);
                }else {
                    boards.add(boardsMap.get(id));
                }
            }
        } else {
            fetchList.addAll(ids);
        }
        if (ArrayUtils.isNotEmpty(fetchList)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Board.Constants.ID).in(ids));
            List<Board> boardsNew = runQuery(query, Board.class);
            if (ArrayUtils.isNotEmpty(boardsNew)) {
                Map<String, String> redisMap = new HashMap<>();
                for (Board board : boardsNew) {
                    boards.add(board);
                    redisMap.put(getBoardRedisKey(board.getId()), gson.toJson(board));
                }
                try {
                    redisDAO.setexKeyPairs(redisMap, redisExpiry);
                } catch (Exception e) {
                    logger.error("error in redis " + e.getMessage());
                }
                logger.info("getBoardsMap size " + boardsNew.size());
            }
        }

        return boards;
    }

    public Map<Long, Board> getBoardsMapFromRedis(Collection<Long> ids) {

        Map<Long, Board> boardMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            List<String> keys = new ArrayList<>();
            for (Long id : ids) {
                if (id != null) {
                    keys.add(getBoardRedisKey(id));
                }
            }
            Map<String, String> valueMap = new HashMap<>();
            try {
                valueMap = redisDAO.getValuesForKeys(keys);
            } catch (Exception e) {
                logger.error("Error in redis " + e.getMessage());
            }
            if (valueMap != null && !valueMap.isEmpty()) {
                for (Long id : ids) {
                    if (id != null) {
                        String value = valueMap.get(getBoardRedisKey(id));
                        if (!StringUtils.isEmpty(value)) {
                            Board board = gson.fromJson(value, Board.class);
                            boardMap.put(id, board);
                        }
                    }
                }
            }
        }
        return boardMap;
    }

    public String getBoardRedisKey(Long id) {
        return env + "_BOARD_" + id;
    }
}
