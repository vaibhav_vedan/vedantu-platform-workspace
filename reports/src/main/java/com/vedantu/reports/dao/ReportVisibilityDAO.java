package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.ReportsVisibility;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Objects;

import static com.vedantu.reports.entities.PlatformFeedback.Constants.STUDENT_ID;

@Repository
public class ReportVisibilityDAO extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(ReportVisibilityDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public ReportVisibilityDAO() {
        super();
    }

    public boolean isReportVisible(String studentId) throws BadRequestException {
        if(StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        return Objects.nonNull(runQuery(query, ReportsVisibility.class));
    }
}
