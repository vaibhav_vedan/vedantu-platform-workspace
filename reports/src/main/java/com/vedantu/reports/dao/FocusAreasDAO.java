package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.FocusAreas;
import com.vedantu.reports.pojo.FocusArea;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.vedantu.reports.entities.FocusAreas.Constants.*;

@Repository
public class FocusAreasDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(FocusAreasDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public FocusAreasDAO() {
        super();
    }

    public void updateFocusAreasAccordingToRecentTestScore(String studentId, String testId, List<FocusArea> focusAreas) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(testId) || ArrayUtils.isEmpty(focusAreas)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        Update update=new Update();
        update.set(TEST_ID,testId);
        update.set(FOCUS_AREAS,focusAreas);

        logger.info("updateFocusAreasAccordingToRecentTestScore - query - ",query);
        logger.info("updateFocusAreasAccordingToRecentTestScore - update - ",update);

        upsertEntity(query,update,FocusAreas.class);
    }

    public FocusAreas getFocusAreaOfStudent(String studentId) throws BadRequestException {

        if(StringUtils.isEmpty(studentId)){
            throwInvalidOrMissingParameterException();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));

        return findOne(query,FocusAreas.class);
    }
}
