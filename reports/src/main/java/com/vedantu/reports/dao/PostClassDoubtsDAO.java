package com.vedantu.reports.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.dao.abstraction.MongoClientFactory;
import com.vedantu.reports.entities.PostClassDoubtsAsked;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity.Constants.*;

@Repository
public class PostClassDoubtsDAO extends AbstractMongoDAO {
    private Logger logger = LogFactory.getLogger(PostClassDoubtsDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public PostClassDoubtsDAO() {
        super();
    }

    public void save(PostClassDoubtsAsked p) {
        try {
            saveEntity(p);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Error adding post class doubts info" + p, ex);
        }
    }

    public void updatePostClassDoubtsAsked(String studentId, String subject, Integer count, long documentCreationTime) throws BadRequestException  {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject) || Objects.isNull(count)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = reportsUpsertQuery(documentCreationTime);
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));

        Update update = new Update();
        update.inc(COUNT, count);
        update.set(LAST_UPDATED_BY, UPDATED_BY_SYSTEM);
        update.set(LAST_UPDATED, System.currentTimeMillis());

        upsertEntity(query, update, PostClassDoubtsAsked.class);
    }

    public int getSubjectPostClassDoubtsAsked(String studentId, String subject, Long fromDate) throws BadRequestException {
        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(subject)) {
            throwInvalidOrMissingParameterException();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(SUBJECT).is(subject));
        if (Objects.nonNull(fromDate)) {
            query.addCriteria(Criteria.where(CREATION_TIME).gte(fromDate));
        }
        query.fields().include(COUNT);
        query.fields().exclude(_ID);

        return Optional.ofNullable(runQuery(query, PostClassDoubtsAsked.class))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(PostClassDoubtsAsked::getCount)
                .filter(Objects::nonNull)
                .mapToInt(Integer::intValue).sum();
    }
}
