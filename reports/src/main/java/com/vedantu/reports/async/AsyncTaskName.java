/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.reports.async;

import com.vedantu.async.IAsyncTaskName;

public enum AsyncTaskName implements IAsyncTaskName {
    TRIGGER_SNS(AsyncQueueName.DEFAULT_QUEUE)
    ;

    private final AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
