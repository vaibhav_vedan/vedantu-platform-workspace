package com.vedantu.reports.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MySubjectScoresAverageResponse{
    private String subject;
    private Double yourScore;
    private Double averageScore;
}
