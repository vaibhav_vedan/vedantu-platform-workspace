package com.vedantu.reports.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyActivityTimeResponse {
    private String date;
    private Long timeSpentInDayMillis;
}
