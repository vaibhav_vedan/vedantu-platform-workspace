package com.vedantu.reports.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MySubjectScoresSubjectWiseResponse {
    private String testName;
    private Double percentage;
    private Double percentile;
    private Long testTakenDate;
}
