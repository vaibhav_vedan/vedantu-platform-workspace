package com.vedantu.reports.response;

import com.vedantu.reports.entities.FocusAreas;
import com.vedantu.reports.pojo.OverallFocusAreaPojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FocusAreaResponse {
    private List<OverallFocusAreaPojo> subjectPojoList;
}
