package com.vedantu.reports.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyWeeklySubjectAttendanceResponse {
    private String fromDate;
    private String toDate;
    private Integer attendanceCount;
}
