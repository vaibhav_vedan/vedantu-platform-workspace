
package com.vedantu.reports.kafka.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$timestamp"
})
public class ClusterTime {

    @JsonProperty("$timestamp")
    private com.vedantu.reports.kafka.model.$timestamp $timestamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("$timestamp")
    public com.vedantu.reports.kafka.model.$timestamp get$timestamp() {
        return $timestamp;
    }

    @JsonProperty("$timestamp")
    public void set$timestamp(com.vedantu.reports.kafka.model.$timestamp $timestamp) {
        this.$timestamp = $timestamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
