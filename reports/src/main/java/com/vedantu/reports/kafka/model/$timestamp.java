
package com.vedantu.reports.kafka.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "t",
    "i"
})
public class $timestamp {

    @JsonProperty("t")
    private Long t;
    @JsonProperty("i")
    private Integer i;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("t")
    public Long getT() {
        return t;
    }

    @JsonProperty("t")
    public void setT(Long t) {
        this.t = t;
    }

    @JsonProperty("i")
    public Integer getI() {
        return i;
    }

    @JsonProperty("i")
    public void setI(Integer i) {
        this.i = i;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
