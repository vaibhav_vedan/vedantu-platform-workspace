package com.vedantu.reports.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageMeta {
    private String id;
    private Long time;
    private String operationType;
}
