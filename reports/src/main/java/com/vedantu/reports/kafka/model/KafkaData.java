
package com.vedantu.reports.kafka.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "operationType",
    "fullDocument",
    "documentKey",
        "ns",
        "_id",
        "clusterTime"
})
public class KafkaData {



    @JsonProperty("_id")
    private Id id;

    @JsonProperty("clusterTime")
    private ClusterTime clusterTime;

    @JsonProperty("operationType")
    private String operationType;

    @JsonProperty("fullDocument")
    private JsonNode fullDocument;

    @JsonProperty("documentKey")
    private DocumentKey documentKey;

    @JsonProperty("ns")
    private Ns ns;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



    @JsonProperty("operationType")
    public String getOperationType() {
        return operationType;
    }

    @JsonProperty("operationType")
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    @JsonProperty("fullDocument")
    public JsonNode getFullDocument() {
        return fullDocument;
    }

    @JsonProperty("fullDocument")
    public void setFullDocument(JsonNode fullDocument) {
        this.fullDocument = fullDocument;
    }


    @JsonProperty("documentKey")
    public DocumentKey getDocumentKey() {
        return documentKey;
    }

    @JsonProperty("documentKey")
    public void setDocumentKey(DocumentKey documentKey) {
        this.documentKey = documentKey;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Ns getNs() {
        return ns;
    }

    public void setNs(Ns ns) {
        this.ns = ns;
    }

    @JsonProperty("_id")
    public Id getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(Id id) {
        this.id = id;
    }

    public ClusterTime getClusterTime() {
        return clusterTime;
    }

    public void setClusterTime(ClusterTime clusterTime) {
        this.clusterTime = clusterTime;
    }
}
