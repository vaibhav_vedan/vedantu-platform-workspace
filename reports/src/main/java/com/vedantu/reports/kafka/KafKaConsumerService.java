package com.vedantu.reports.kafka;


import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.reports.kafka.model.KafkaData;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;


@Service
public class KafKaConsumerService
{

    @Autowired
    private MessageProcessor messageProcessor;

    private final Logger logger = LogFactory.getLogger(KafKaConsumerService.class);
    private static final Gson gson = new Gson();


    @KafkaListener(topics = "${general.topic.name}",
            groupId = "${group.id}")
    public void consume(String message) {
        logger.info("Processing for generalized topic.");
        logger.info("Message received -> {}", message);
    }

    @KafkaListener(topics = "#{'${topic.names}'.split(',')}",
            groupId = "${group.id}",
            containerFactory = "kafkaDataListenerContainerFactory")
    public void consume(KafkaData kafkaData) throws BadRequestException {
        long startTime = System.currentTimeMillis();

        logger.info("Processing for particular topic.");
//        logger.info("The entire data is {}", gson.toJson(kafkaData));
//        if(kafkaData != null && kafkaData.getFullDocument() != null && kafkaData.getNs() != null) {
//            logger.info("Message from DB -> {} and collection -> {} and fulldocument -> {}", kafkaData.getNs().getDb(), kafkaData.getNs().getColl(), kafkaData.getFullDocument().toString());
//        }
        //TODO Can be because of can't deseriliaze the data or data value itself is null. Acc
        if (kafkaData == null) {
            return;
        }
        messageProcessor.handleMessage(kafkaData);

        if (kafkaData.getFullDocument() != null && kafkaData.getNs() != null) {
            logger.info("Time taken in millis for collection name {} is {}", kafkaData.getNs().getColl(), (System.currentTimeMillis() - startTime));
        }
    }

}
