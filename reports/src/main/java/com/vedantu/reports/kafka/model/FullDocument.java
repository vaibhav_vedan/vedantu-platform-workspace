
package com.vedantu.reports.kafka.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_id",
    "studentId",
    "text",
    "userRemark"
})
public class FullDocument {

    @JsonProperty("_id")
    private Id_ id;
    @JsonProperty("studentId")
    private String studentId;
    @JsonProperty("text")
    private String text;
    @JsonProperty("userRemark")
    private String userRemark;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_id")
    public Id_ getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(Id_ id) {
        this.id = id;
    }

    @JsonProperty("studentId")
    public String getStudentId() {
        return studentId;
    }

    @JsonProperty("studentId")
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("userRemark")
    public String getUserRemark() {
        return userRemark;
    }

    @JsonProperty("userRemark")
    public void setUserRemark(String userRemark) {
        this.userRemark = userRemark;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
