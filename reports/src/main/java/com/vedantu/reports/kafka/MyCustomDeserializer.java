package com.vedantu.reports.kafka;
import com.google.gson.Gson;
import com.vedantu.util.LogFactory;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.lang.Nullable;

public class MyCustomDeserializer<T> extends JsonDeserializer<T>{


    private final Logger logger = LogFactory.getLogger(MyCustomDeserializer.class);

    public MyCustomDeserializer(Class<? super T> targetType) {
        super(targetType);
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        try {
            return super.deserialize(topic,headers,data);
        } catch (SerializationException e) {

            logger.info("Wrong kafka json data for a  topic {}. Unable to deserialize",topic);
            //We need to add this
            return null;
        }
    }

    public T deserialize(String topic, @Nullable byte[] data){

        try {
            return super.deserialize(topic,data);
        } catch (SerializationException e) {

            logger.info("Wrong kafka data for a  topic {}. Unable to deserialize",topic);
            //We need to handle the error where worng json data.
            return null;
        }

    }
}
