package com.vedantu.reports.kafka;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.reports.dao.KafkaEntryAuditDAO;
import com.vedantu.reports.entities.KafkaEntityAudit;
import com.vedantu.reports.entities.lms.CMDSTest;
import com.vedantu.reports.entities.lms.CMDSTestAttempt;
import com.vedantu.reports.entities.lms.ContentInfo;
import com.vedantu.reports.entities.lms.Doubt;
import com.vedantu.reports.entities.lms.QuestionAttempt;
import com.vedantu.reports.enums.ProcessingStatus;
import com.vedantu.reports.kafka.model.KafkaData;
import com.vedantu.reports.kafka.model.MessageMeta;
import com.vedantu.reports.managers.AssignmentsTestsManager;
import com.vedantu.reports.managers.AttendanceManager;
import com.vedantu.reports.managers.CMDSTestAttemptKafkaManager;
import com.vedantu.reports.managers.CMDSTestKafkaManager;
import com.vedantu.reports.managers.KafkaEntityAuditManager;
import com.vedantu.reports.managers.OTFSessionKafkaManager;
import com.vedantu.reports.managers.PostClassDoubtsManager;
import com.vedantu.reports.managers.TimeSpentOnPlatformManager;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class MessageProcessor {

    @Autowired
    private CMDSTestAttemptKafkaManager cmdsTestAttemptKafkaManager;

    @Autowired
    private PostClassDoubtsManager postClassDoubtsManager;

    @Autowired
    private CMDSTestKafkaManager cmdsTestKafkaManager;

    @Autowired
    private KafkaEntryAuditDAO kafkaEntryAuditDAO;

    @Autowired
    private AssignmentsTestsManager assignmentsTestsManager;

    @Autowired
    private TimeSpentOnPlatformManager timeSpentOnPlatformManager;

    @Autowired
    private AttendanceManager attendanceManager;

    @Autowired
    private OTFSessionKafkaManager otfSessionKafkaManager;

    @Autowired
    private KafkaEntityAuditManager kafkaEntityAuditManager;

    public Logger logger = LogFactory.getLogger(MessageProcessor.class);
    private static final Gson gson = new Gson();

    void handleMessage(KafkaData kafkaData) throws BadRequestException {

        String database = kafkaData.getNs().getDb();
        if(StringUtils.isEmpty(database)){
            logger.error("Database name can't be null");
            return;
        }
        MessageMeta messageMeta=new MessageMeta();
        messageMeta.setId(kafkaData.getId().getData());
        messageMeta.setTime(kafkaData.getClusterTime().get$timestamp().getT());
        messageMeta.setOperationType(kafkaData.getOperationType());


        KafkaEntityAudit kafkaEntityAudit=kafkaEntryAuditDAO.getProcessingStatusForMessageId(messageMeta.getId());

        if(Objects.isNull(kafkaEntityAudit)) {

            KafkaEntityAudit audit=new KafkaEntityAudit(messageMeta,database);
            String collectionName = kafkaData.getNs().getColl();

            Document originalDoc=Document.parse(kafkaData.getFullDocument().toString());
            JsonWriterSettings relaxed = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();
            String document = originalDoc.toJson(relaxed);

            if(StringUtils.isEmpty(collectionName)){
                logger.error("Collection name can't be empty anyway");
                return;
            }

            String id;
            if(collectionName.equals("Doubt")){
                id = originalDoc.getLong(AbstractMongoLongIdEntity.Constants._ID).toString();
            }else {
                id = originalDoc.getObjectId(AbstractMongoStringIdEntity.Constants._ID).toHexString();
            }

            audit.setCollection(collectionName);
            audit.setDocumentId(id);
            kafkaEntryAuditDAO.save(audit);

            boolean isDocumentAlreadyProcessed = kafkaEntityAuditManager.isDocumentAlreadyProcessed(id);

            logger.info("handleDBChanges - audit - {}", audit);
            logger.info("handleDBChanges - messageMeta - {}", messageMeta);
            logger.info("handleDBChanges - isDocumentAlreadyProcessed - {}", isDocumentAlreadyProcessed);
            logger.info("handleDBChanges - database - {}", database);
            logger.info("handleDBChanges - collectionName - {}", collectionName);
            //logger.info("handleDBChanges - document - {}", document);
            logger.info("handleDBChanges - id - {}", id);

            switch (database) {
                case "vedantumoodle":
                    handleVedantuMoodleChanges(messageMeta, collectionName, document, id, isDocumentAlreadyProcessed);
                    break;
                case "vedantuscheduling":
                    handleSchedulingChanges(messageMeta, collectionName, document, id, isDocumentAlreadyProcessed);
                    break;
                default:
                    audit.setProcessingStatus(ProcessingStatus.REJECTED);
                    audit.setProcessingEndTime(System.currentTimeMillis());
                    kafkaEntryAuditDAO.save(audit);
                    logger.info("Not processing currently for DB {}", database);
                    break;
            }
        }else{
            logger.info("Message with message id {} is captured 2 times by the consumer",messageMeta.getId());
        }

    }

    private void handleSchedulingChanges(MessageMeta messageMeta, String collectionName, String document, String id, boolean isDocumentAlreadyProcessed) throws BadRequestException {

        switch (collectionName) {
            case "GTTAttendeeDetails":
                GTTAttendeeDetailsInfo gttAttendeeDetailsInfo = gson.fromJson(document, GTTAttendeeDetailsInfo.class);
                gttAttendeeDetailsInfo.setId(id);
                gttAttendeeDetailsInfo.setCreationTime(DateTimeUtils.getOnlyDateInMillis(gttAttendeeDetailsInfo.getCreationTime()));
                gttAttendeeDetailsInfo.setStudentSessionJoinTime(DateTimeUtils.getOnlyDateInMillis(gttAttendeeDetailsInfo.getStudentSessionJoinTime()));
                gttAttendeeDetailsInfo.setLastUpdated(DateTimeUtils.getOnlyDateInMillis(gttAttendeeDetailsInfo.getLastUpdated()));
                processLiveSessionAndAttendanceDetails(gttAttendeeDetailsInfo, messageMeta, isDocumentAlreadyProcessed);
                break;
            case "OTFSession":
                OTFSessionInfo otfSessionInfo = gson.fromJson(document, OTFSessionInfo.class);
                otfSessionInfo.setId(id);
                processOTFSessionDetails(otfSessionInfo, messageMeta);
                break;
            default:
                kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), ProcessingStatus.COLLECTION_NOT_PROCESSED);
                logger.info("Not processing for collection {}",collectionName);
                break;
        }
    }

    private void handleVedantuMoodleChanges(MessageMeta messageMeta, String collectionName, String document, String id, boolean isDocumentAlreadyProcessed) throws BadRequestException {

        switch (collectionName){
            case "Doubt":
                Doubt doubt= gson.fromJson(document, Doubt.class);
                doubt.setId(Long.parseLong(id));
                doubt.setCreationTime(DateTimeUtils.getOnlyDateInMillis(doubt.getCreationTime()));
                processDoubts(doubt, messageMeta, isDocumentAlreadyProcessed);
                break;
            case "CMDSTestAttempt":
                CMDSTestAttempt cmdsTestAttempt = gson.fromJson(document, CMDSTestAttempt.class);
                cmdsTestAttempt.setId(id);
                cmdsTestAttempt.setCreationTime(DateTimeUtils.getOnlyDateInMillis(cmdsTestAttempt.getCreationTime()));
                processCMDSTestAttempt(cmdsTestAttempt, messageMeta, isDocumentAlreadyProcessed);
                break;
            case "CMDSTest":
                CMDSTest cmdsTest= gson.fromJson(document,CMDSTest.class);
                cmdsTest.setId(id);
                processCMDSTest(cmdsTest, messageMeta);
                break;
            case "ContentInfo":
                ContentInfo contentInfo = gson.fromJson(document, ContentInfo.class);
                contentInfo.setId(id);
                contentInfo.setCreationTime(DateTimeUtils.getOnlyDateInMillis(contentInfo.getCreationTime()));
                processContentInfo(contentInfo, messageMeta, isDocumentAlreadyProcessed);
                break;
            case "QuestionAttempt":
                QuestionAttempt questionAttempt=gson.fromJson(document, QuestionAttempt.class);
                questionAttempt.setId(id);
                questionAttempt.setCreationTime(DateTimeUtils.getOnlyDateInMillis(questionAttempt.getCreationTime()));
                processQuestionAttempt(questionAttempt, messageMeta, isDocumentAlreadyProcessed);
                break;
            default:
                kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), ProcessingStatus.COLLECTION_NOT_PROCESSED);
                logger.info("Not processing for collection {}",collectionName);
                break;
        }
    }

    private void processQuestionAttempt(QuestionAttempt questionAttempt, MessageMeta messageMeta, boolean isDocumentAlreadyProcessed) throws BadRequestException {
        ProcessingStatus processingStatus = ProcessingStatus.ALREADY_PROCESSED;
        if (!isDocumentAlreadyProcessed) {
            timeSpentOnPlatformManager.processTestDurationTime(questionAttempt);
            processingStatus = ProcessingStatus.PROCESSED;
        }
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), processingStatus);
    }

    private void processCMDSTest(CMDSTest cmdsTest, MessageMeta messageMeta) throws BadRequestException {
        cmdsTestKafkaManager.processDataForTestInfo(cmdsTest);
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), ProcessingStatus.PROCESSED);
    }

    private void processCMDSTestAttempt(CMDSTestAttempt cmdsTestAttempt, MessageMeta messageMeta, boolean isDocumentAlreadyProcessed) throws BadRequestException {
        ProcessingStatus processingStatus = cmdsTestAttemptKafkaManager.processForTestAttemptData(cmdsTestAttempt);
        if (!isDocumentAlreadyProcessed) {
            assignmentsTestsManager.processAssignmentTestAttemptedCounts(cmdsTestAttempt);
            processingStatus = ProcessingStatus.PROCESSED;
        }
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), processingStatus, gson.toJson(cmdsTestAttempt));
    }

    private void processDoubts(Doubt doubt, MessageMeta messageMeta, boolean isDocumentAlreadyProcessed) throws BadRequestException {
        ProcessingStatus processingStatus = ProcessingStatus.ALREADY_PROCESSED;
        if(!isDocumentAlreadyProcessed) {
            postClassDoubtsManager.processDoubts(doubt);
            processingStatus = ProcessingStatus.PROCESSED;
        }
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), processingStatus);
    }

    private void processLiveSessionAndAttendanceDetails(GTTAttendeeDetailsInfo gttAttendeeDetailsInfo, MessageMeta messageMeta, boolean isDocumentAlreadyProcessed) throws BadRequestException {
        // Before the start of session, a GTTAttendee record is created for all students in the session with empty/0 values and later replaced/updated by the
        // values of timeInSession and join times when the session ends (state moved from PROCESSING -> PROCESSED). When the document is processed for the second time,
        // we evaluate for attendance and main time in session and keep state in PROCESSED. Thereafter whenever document is updated we process for change in replay time
        // and keep state in PROCESSED
        ProcessingStatus processingStatus = ProcessingStatus.PROCESSED_EMPTY;
        if(gttAttendeeDetailsInfo.getTimeInSession() > 0 && ArrayUtils.isNotEmpty(gttAttendeeDetailsInfo.getJoinTimes())) {
            KafkaEntityAudit kafkaEntityAudit = kafkaEntryAuditDAO.getRecentlyProcessedDocument(gttAttendeeDetailsInfo.getId(), null);
            if(Objects.isNull(kafkaEntityAudit)) {
                attendanceManager.updateAttendanceCount(gttAttendeeDetailsInfo);
                timeSpentOnPlatformManager.processLiveSessionDetails(gttAttendeeDetailsInfo);
                processingStatus = ProcessingStatus.PROCESSED;
            } else {
                ProcessingStatus previousProcessingStatus = kafkaEntityAudit.getProcessingStatus();
                if(ProcessingStatus.PROCESSED_EMPTY.equals(previousProcessingStatus)) {
                    attendanceManager.updateAttendanceCount(gttAttendeeDetailsInfo);
                    timeSpentOnPlatformManager.processLiveSessionDetails(gttAttendeeDetailsInfo);
                    processingStatus = ProcessingStatus.PROCESSED;
                } else if(ProcessingStatus.PROCESSED.equals(previousProcessingStatus)) {
                    GTTAttendeeDetailsInfo previousDocumentInfo = gson.fromJson(kafkaEntityAudit.getDocument(), GTTAttendeeDetailsInfo.class);
                    timeSpentOnPlatformManager.processReplayDuration(gttAttendeeDetailsInfo, previousDocumentInfo);
                    processingStatus = ProcessingStatus.PROCESSED;
                }
            }
        }
        logger.info("GTTAttendee processing status {}", processingStatus);
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), processingStatus, gson.toJson(gttAttendeeDetailsInfo));
    }

    private void processContentInfo(ContentInfo contentInfo, MessageMeta messageMeta, boolean isDocumentAlreadyProcessed) throws BadRequestException {
        ProcessingStatus processingStatus = ProcessingStatus.ALREADY_PROCESSED;
        if (!isDocumentAlreadyProcessed) {
            assignmentsTestsManager.processAssignmentTestSharedCount(contentInfo);
            processingStatus=ProcessingStatus.PROCESSED;
        }
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), processingStatus);
    }

    private void processOTFSessionDetails(OTFSessionInfo otfSessionInfo, MessageMeta messageMeta) throws BadRequestException {
        otfSessionKafkaManager.processOTFSessionSubjects(otfSessionInfo);
        kafkaEntryAuditDAO.updateFinalProcessingStatus(messageMeta.getId(), ProcessingStatus.PROCESSED);
    }
}
