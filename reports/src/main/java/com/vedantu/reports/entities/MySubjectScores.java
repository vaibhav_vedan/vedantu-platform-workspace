package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity;
import com.vedantu.reports.entities.abstractions.AbstractReportsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "MySubjectScores")
public class MySubjectScores extends AbstractReportsEntity {
    private Double percentile=0.0;
    private Double percentage=0.0;
    private String testId;
    public static class Constants extends AbstractReportsCountEntity.Constants {
        public static final String TEST_ID = "testId";
        public static final String PERCENTAGE = "percentage";
        public static final String PERCENTILE = "percentile";
    }
}
