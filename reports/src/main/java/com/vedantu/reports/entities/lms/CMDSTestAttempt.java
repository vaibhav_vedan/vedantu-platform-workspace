package com.vedantu.reports.entities.lms;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.TestEndType;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptImage;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import com.vedantu.lms.cmds.pojo.TestFeedback;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CMDSTestAttempt extends AbstractMongoStringIdEntity {
    private String testId;
    private String contentInfoId;
    private String teacherId;
    private String studentId;
    private Long evaluatedViewTime;
    private Long evaluatedTime;
    private Long reportViewed;
    private CMDSAttemptState attemptState;
    private Set<String> subjects = new HashSet<>();
    private float totalMarks;
    private Float marksAcheived = 0f;
    private Long timeTakenByStudent;
    private String attemptRemarks;
    private List<CMDSTestAttemptImage> imageDetails;
    private Long duration;
    private Long startTime;
    private Long endTime;
    private Long startedAt;
    private Long endedAt;
    private TestEndType testEndType;
    private List<QuestionAnalytics> resultEntries;
    private List<CategoryAnalytics> categoryAnalyticsList;
    private Double percentile;
    private Integer rank;
    private Integer totalStudents;
    private Integer batchAvg;
    private Integer attempted = 0;
    private Integer unattempted = 0;
    private Integer correct = 0;
    private Integer incorrect = 0;
    private float attemptMarksPercent;
    private float testPercentile;
    private Long doNotShowResultsTill;
    private TestFeedback feedback;
    private int attemptIndex=0;

    // Reevaluation parameters
    private Integer reevaluationNumber = 0;
    private Long reevaluationTime;

    // Marketing related temporary field
    private String marketingRedirectUrl;

}
