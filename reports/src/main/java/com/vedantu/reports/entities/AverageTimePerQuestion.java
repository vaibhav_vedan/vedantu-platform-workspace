package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "AverageTimePerQuestion")
public class AverageTimePerQuestion extends AbstractReportsCountEntity {
    private Double totalTime;

    public AverageTimePerQuestion(String studentId, String subject, Double totalTime, Integer count) {
        this.setStudentId(studentId);
        this.setSubject(subject);
        this.setTotalTime(totalTime);
        this.setCount(count);
    }

    public static class Constants extends AbstractReportsCountEntity.Constants {
        public static final String TOTAL_TIME = "totalTime";
    }
}
