package com.vedantu.reports.entities.lms;

import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.lms.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContentInfo extends AbstractMongoStringIdEntity {
    private UserType userType;

    private String studentId;
    private String studentFullName;
    private String teacherId;
    private String teacherFullName;

    private String contentTitle;

    private String courseName;
    private String courseId;
    private String subject;
    private Set<String> subjects = new HashSet<>();

    private Long expiryTime;

    private ContentType contentType;
    private ContentInfoType contentInfoType;
    private EngagementType engagementType;
    private ContentState contentState;
    private LMSType lmsType;

    private String contentLink;
    private String studentActionLink;
    private String teacherActionLink;

    private EntityType contextType;

    private String contextId;

    private List<String> tags;

    private TestContentInfoMetadata metadata;
    private AccessLevel accessLevel;

    private Long boardId;
    private Set<Long> boardIds = new HashSet<>();

    private String testId;
    private Long duration;
    private Long attemptedTime;
    private Long evaulatedTime;
    private Float totalMarks;
    private Float marksAcheived;
    private Integer noOfQuestions;
    private String attemptId; // Latest attempt id in case of multiple active attempts
}