package com.vedantu.reports.entities;

import com.vedantu.reports.enums.ProcessingStatus;
import com.vedantu.reports.kafka.model.MessageMeta;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "KafkaEntityAudit")
public class KafkaEntityAudit extends AbstractMongoStringIdEntity {
    @Indexed(unique = true,background = true,direction = IndexDirection.DESCENDING)
    private String messageId;
    private String operationType;
    private ProcessingStatus processingStatus;
    private String db;
    private String collection;
    private String document;

    @Indexed(background = true,direction = IndexDirection.DESCENDING)
    private String documentId;

    private Long processingStartTime;
    private Long processingEndTime;

    public KafkaEntityAudit(MessageMeta messageMeta,String db) {
        this.setCreationTime(System.currentTimeMillis());
        this.setCreatedBy("SYSTEM");
        this.setLastUpdated(System.currentTimeMillis());
        this.setLastUpdatedBy("SYSTEM");
        this.messageId=messageMeta.getId();
        this.operationType=messageMeta.getOperationType();
        this.processingStatus=ProcessingStatus.PROCESSING;
        this.db=db;

        this.processingStartTime=System.currentTimeMillis();
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String OPERATION_TYPE = "operationType";
        public static final String PROCESSING_STATUS = "processingStatus";
        public static final String MESSAGE_ID = "messageId";
        public static final String DOCUMENT_ID = "documentId";
        public static final String PROCESSING_END_TIME = "processingEndTime";
        public static final String DOCUMENT = "document";
    }
}
