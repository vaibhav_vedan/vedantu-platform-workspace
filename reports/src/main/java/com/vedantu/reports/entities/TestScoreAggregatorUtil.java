package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity;
import com.vedantu.reports.pojo.SubjectScore;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "TestScoreAggregatorUtil")
public class TestScoreAggregatorUtil extends AbstractMongoStringIdEntity {
    private String testId;
    private String testName;
    private List<SubjectScore> subjectScoreList;
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String TEST_ID = "testId";
        public static final String TEST_NAME = "testName";
        public static final String SUBJECT_SCORES_LIST = "subjectScoreList";
    }
}
