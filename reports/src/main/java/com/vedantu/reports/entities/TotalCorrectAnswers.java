package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity;
import com.vedantu.reports.entities.abstractions.AbstractReportsEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "TotalCorrectAnswers")
public class TotalCorrectAnswers extends AbstractReportsCountEntity {
    private Long correctAnswers;
    public static class Constants extends AbstractReportsCountEntity.Constants {
        public static final String CORRECT_ANSWERS = "correctAnswers";
    }
}
