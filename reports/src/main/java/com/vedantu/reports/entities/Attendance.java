package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "Attendance")
public class Attendance extends AbstractReportsCountEntity {
    public Attendance(String studentId, String subject, Integer count) {
        this.setStudentId(studentId);
        this.setSubject(subject);
        this.setCount(count);
    }

    public Attendance(String studentId, Integer count) {
        this.setStudentId(studentId);
        this.setCount(count);
    }
}
