package com.vedantu.reports.entities.abstractions;

import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AbstractReportsEntity extends AbstractMongoStringIdEntity {
    private String studentId;
    private String subject;
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String STUDENT_ID = "studentId";
        public static final String SUBJECT = "subject";
        public static final String UPDATED_BY_SYSTEM = "SYSTEM";
    }
}
