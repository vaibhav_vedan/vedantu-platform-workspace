package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsEntity;
import com.vedantu.reports.pojo.FocusArea;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FocusAreas extends AbstractMongoStringIdEntity {
    @Indexed(unique = true,background = true)
    private String studentId;
    private String testId;
    private List<FocusArea> focusAreas;

    public FocusAreas(FocusAreas other) {
        this.studentId = other.studentId;
        this.testId = other.testId;
        this.focusAreas = other.focusAreas;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String TEST_ID = "testId";
        public static final String STUDENT_ID = "studentId";
        public static final String FOCUS_AREAS = "focusAreas";
    }
}
