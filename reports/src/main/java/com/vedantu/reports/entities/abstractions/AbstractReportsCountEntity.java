package com.vedantu.reports.entities.abstractions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AbstractReportsCountEntity extends AbstractReportsEntity {
    private Integer count;
    public static class Constants extends AbstractReportsEntity.Constants {
        public static final String COUNT = "count";
    }

}
