package com.vedantu.reports.entities.board;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Board extends AbstractMongoLongIdEntity {
    public static final Long DEFAULT_PARENT_ID = 0L;

    private String name;
    private String slug;
    private Long parentId;
    private VisibilityState state;

    // tags
    private Set<String> categories;
    private Set<String> grades;
    private Set<String> exams;
    private Long userId;
}
