package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "AssignmentsTests")
@EqualsAndHashCode(callSuper = false)
public class AssignmentsTests extends AbstractReportsEntity {
    private Integer testsCompleted;
    private Integer testsShared;
    private Integer assignmentsAttempted;
    private Integer assignmentsShared;

    public AssignmentsTests(String studentId, String subject, Integer testsCompleted, Integer testsShared, Integer assignmentsAttempted, Integer assignmentsShared) {
        this.setStudentId(studentId);
        this.setSubject(subject);
        this.testsCompleted = testsCompleted;
        this.testsShared = testsShared;
        this.assignmentsAttempted = assignmentsAttempted;
        this.assignmentsShared = assignmentsShared;
    }

    public AssignmentsTests(String studentId, Integer testsCompleted, Integer testsShared, Integer assignmentsAttempted, Integer assignmentsShared) {
        this.setStudentId(studentId);
        this.testsCompleted = testsCompleted;
        this.testsShared = testsShared;
        this.assignmentsAttempted = assignmentsAttempted;
        this.assignmentsShared = assignmentsShared;
    }

    public static class Constants extends AbstractReportsEntity.Constants {
        public static final String TESTS_COMPLETED = "testsCompleted";
        public static final String TESTS_SHARED = "testsShared";
        public static final String ASSIGNMENTS_ATTEMPTED = "assignmentsAttempted";
        public static final String ASSIGNMENTS_SHARED = "assignmentsShared";
    }
}
