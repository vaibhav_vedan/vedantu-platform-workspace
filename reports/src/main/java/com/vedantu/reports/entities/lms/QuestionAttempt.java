package com.vedantu.reports.entities.lms;

import com.vedantu.lms.cmds.enums.AttemptStateOfQuestion;
import com.vedantu.reports.enums.QuestionAttemptContext;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;

import java.util.List;

@Data
public class QuestionAttempt extends AbstractMongoStringIdEntity {

    private String userId;
    private Integer duration;
    private String questionId;
    private List<String> answerGiven;
    private QuestionAttemptContext contextType;
    private String contextId;
    private String attemptId;
    private Boolean correct;
    private AttemptStateOfQuestion attemptStateOfQuestion;
    
}
