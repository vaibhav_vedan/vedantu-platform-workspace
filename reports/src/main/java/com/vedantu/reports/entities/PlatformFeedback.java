package com.vedantu.reports.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "PlatformFeedback")
public class PlatformFeedback extends AbstractMongoStringIdEntity {
    private String studentId;
    private Integer feedback;

    public PlatformFeedback(String studentId, Integer feedback) {
        this.studentId = studentId;
        this.feedback = feedback;
    }
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String STUDENT_ID = "studentId";
    }
}
