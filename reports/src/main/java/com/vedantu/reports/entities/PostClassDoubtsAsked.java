package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsCountEntity;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "PostClassDoubtsAsked")
public class PostClassDoubtsAsked extends AbstractReportsCountEntity {
    public PostClassDoubtsAsked(String studentId, String subject, Integer count) {
        this.setStudentId(studentId);
        this.setSubject(subject);
        this.setCount(count);
    }

    public PostClassDoubtsAsked(String studentId, Integer count) {
        this.setStudentId(studentId);
        this.setCount(count);
    }
}
