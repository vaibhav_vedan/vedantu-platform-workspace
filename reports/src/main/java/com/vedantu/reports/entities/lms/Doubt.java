/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.reports.entities.lms;


import com.vedantu.reports.pojo.DoubtSolverPojo;
import com.vedantu.reports.pojo.DoubtState;
import com.vedantu.reports.pojo.DoubtStateChangePojo;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.ArrayList;
import java.util.List;


public class Doubt extends AbstractTargetTopicLongIdEntity {
    
    @Indexed(background = true)
    private String studentId;

    private Long lastActivityTime;

    @Indexed(background = true)    
    private DoubtState doubtState = DoubtState.DOUBT_POSTED;

    private String picUrl;
    private String text;
    private List<DoubtStateChangePojo> doubtStateChanges = new ArrayList<>();

    private List<DoubtSolverPojo> doubtSolverPojoList = new ArrayList<>();


    public Doubt() {
    }    

    /**
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }


    /**
     * @return the lastActivityTime
     */
    public Long getLastActivityTime() {
        return lastActivityTime;
    }

    /**
     * @param lastActivityTime the lastActivityTime to set
     */
    public void setLastActivityTime(Long lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }


    public static class Constants extends AbstractTargetTopicLongIdEntity.Constants {
        public static final String LAST_ACTIVITY_TIME = "lastActivityTime";
        public static final String STUDENT_ID = "studentId";
        public static final String DOUBT_STATE = "doubtState";
        public static final String DOUBT_STATE_CHANGES = "doubtStateChanges";
        public static final String DOUBT_SOLVER_POJO_LIST = "doubtSolverPojoList";
        public static final String PICURL = "picUrl";
        public static final String TEXT = "text";
    }    

    /**
     * @return the doubtState
     */
    public DoubtState getDoubtState() {
        return doubtState;
    }

    /**
     * @param doubtState the doubtState to set
     */
    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    /**
     * @return the doubtSolverPojoList
     */
    public List<DoubtSolverPojo> getDoubtSolverPojoList() {
        return doubtSolverPojoList;
    }

    /**
     * @param doubtSolverPojoList the doubtSolverPojoList to set
     */
    public void setDoubtSolverPojoList(List<DoubtSolverPojo> doubtSolverPojoList) {
        this.doubtSolverPojoList = doubtSolverPojoList;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the doubtStateChanges
     */
    public List<DoubtStateChangePojo> getDoubtStateChanges() {
        return doubtStateChanges;
    }

    /**
     * @param doubtStateChanges the doubtStateChanges to set
     */
    public void setDoubtStateChanges(List<DoubtStateChangePojo> doubtStateChanges) {
        this.doubtStateChanges = doubtStateChanges;
    }

	
}
