package com.vedantu.reports.entities;

import com.vedantu.reports.entities.abstractions.AbstractReportsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "GlobalReportFieldsCounts")
@EqualsAndHashCode(callSuper = false)
public class GlobalReportFieldsCounts extends AbstractReportsEntity {
    private Integer liveClassesAttended;
    private Integer postClassDoubtsAsked;
    private Integer assignmentsAttempted;
    private Integer testsCompleted;
    private Integer assignmentsShared;
    private Integer testsShared;
    private Long timeSpentOnPlatform;
    
    public static class Constants extends AbstractReportsEntity.Constants {
        public static final String LIVE_CLASSES_ATTENDED = "liveClassesAttended";
        public static final String POST_CLASS_DOUBTS_ASKED = "postClassDoubtsAsked";
        public static final String ASSIGNMENTS_ATTEMPTED = "assignmentsAttempted";
        public static final String TESTS_COMPLETED = "testsCompleted";
        public static final String ASSIGNMENTS_SHARED = "assignmentsShared";
        public static final String TESTS_SHARED = "testsShared";
        public static final String TIME_SPENT_ON_PLATFORM = "timeSpentOnPlatform";
    }
}
