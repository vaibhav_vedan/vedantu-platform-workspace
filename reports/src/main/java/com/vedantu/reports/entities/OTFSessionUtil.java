package com.vedantu.reports.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "OTFSessionUtil")
public class OTFSessionUtil extends AbstractMongoStringIdEntity {
    private String sessionId;
    private String sessionTitle;
    private String subject;
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String SESSION_ID = "sessionId";
        public static final String SESSION_TITLE = "sessionTitle";
        public static final String SUBJECT = "subject";
        public static final String UPDATED_BY_SYSTEM = "SYSTEM";
    }
}
