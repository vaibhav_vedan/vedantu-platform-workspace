package com.vedantu.reports.entities.lms;


import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.reports.entities.abstractions.AbstractCMDSEntity;
import com.vedantu.reports.enums.CMDSApprovalStatus;
import com.vedantu.reports.enums.CMDSTestState;
import com.vedantu.reports.enums.TestMode;
import com.vedantu.reports.enums.TypeOfObjectiveTest;
import com.vedantu.reports.pojo.CustomSectionEvaluationRule;
import com.vedantu.reports.pojo.PerformanceReportSection;
import com.vedantu.reports.pojo.SectionWiseInstruction;
import com.vedantu.reports.pojo.TestMetadata;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

import static com.vedantu.platform.enums.SocialContextType.CMDSTEST;

@Data
@NoArgsConstructor
public class CMDSTest extends AbstractCMDSEntity {

    private CMDSTestState testState;
    private int versionNo = 1;
    private String questionSetId;
    private SocialContextType socialContextType = CMDSTEST;
    public String description;
    public int qusCount;
    public Long duration;
    public Float totalMarks = 0f;
    public ContentInfoType contentInfoType;
    private boolean visibleInApp = false;
    public List<TestMetadata> metadata;
    public EnumBasket.TestType type;
    public EnumBasket.TestTagType testTag;
    public TestMode mode;
    public String code; // unique code for test inside an organization(if applicable)
    public long attempts;
    public boolean published;
    public List<String> childrenIds; // if this is testGroup then members of this group
    public String parentId; // if this is part of a testGroup (testGroupId)
    public TestResultVisibility resultVisibility;
    public String resultVisibilityMessage;
    private Long doNotShowResultsTill;
    private boolean hardStop=false;
    public Long minStartTime;
    public Long maxStartTime;
    public boolean reattemptAllowed = false;
    public boolean globalRankRequired = false;
    private SignUpRestrictionLevel signupHook;
    private List<String> instructions;
    private List<SectionWiseInstruction> sectionWiseInstructions;
    private List<CustomSectionEvaluationRule> customSectionEvaluationRules;
    private String marketingRedirectUrl; // Marketing related dummy fields for now
    private Long expiryDate;// for public subjective test
    private Long expiryDays;// for private subjective test
    private TypeOfObjectiveTest typeOfObjectiveTest; // for objective test only
    private CMDSApprovalStatus approvalStatus=CMDSApprovalStatus.MAKER_APPROVED;

    private Long assignedChecker;
    private List<PerformanceReportSection> analytics; //for competitive only.
    private Set<String> analyticsTags; //for competitive only.

}

