package com.vedantu.reports.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "StudentSubjectUtil")
@EqualsAndHashCode(callSuper = false)
public class StudentSubjectUtil extends AbstractMongoStringIdEntity {
    @Indexed(background = true)
    private String studentId;
    private List<String> subjects;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String STUDENT_ID = "studentId";
        public static final String SUBJECTS = "subjects";
    }
}
