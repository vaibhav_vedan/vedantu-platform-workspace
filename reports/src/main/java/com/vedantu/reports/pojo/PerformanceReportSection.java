/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.reports.pojo;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.AnalyticsTypes;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @author ashok
 */
@Data
@NoArgsConstructor
public class PerformanceReportSection {

    //Score Analytics
    private Float topper;
    private Float top10;
    private Float average;
    private Float self;
    private Float total;

    //Time Analytics
    private Integer topperDuration;
    private Integer top10Duration;
    private Integer averageDuration;
    private Integer selfDuration;

    //MetaData
    private String title;
    private String subject;
    private String topic;
    private Difficulty difficulty;
    private QuestionType questionType;
    private AnalyticsTypes type;

    public PerformanceReportSection(CategoryAnalytics categoryAnalytics) {

        if(Objects.nonNull(categoryAnalytics.getMarks())) {
            this.self = categoryAnalytics.getMarks();
        }
        if(Objects.nonNull(categoryAnalytics.getMaxMarks())) {
            this.total = categoryAnalytics.getMaxMarks();
        }
        if(Objects.nonNull(categoryAnalytics.getDuration())) {
            this.selfDuration = categoryAnalytics.getDuration();
        }
        if(Objects.nonNull(categoryAnalytics.getName())) {
            this.title = categoryAnalytics.getName();
        }
        if(Objects.nonNull(categoryAnalytics.getSubject())) {
            this.subject = categoryAnalytics.getSubject();
        }
        if(Objects.nonNull(categoryAnalytics.getTopic())) {
            this.topic = categoryAnalytics.getTopic();
        }
        if(Objects.nonNull(categoryAnalytics.getDifficulty())) {
            this.difficulty = categoryAnalytics.getDifficulty();
        }
        if(Objects.nonNull(categoryAnalytics.getQuestionType())) {
            this.questionType = categoryAnalytics.getQuestionType();
        }
        if(Objects.nonNull(categoryAnalytics.getCategoryType())) {
            this.type = categoryAnalytics.getCategoryType();
        }
    }
}
