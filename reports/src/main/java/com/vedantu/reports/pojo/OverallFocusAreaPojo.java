package com.vedantu.reports.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OverallFocusAreaPojo {
    private String subject;
    private String topic;
}
