package com.vedantu.reports.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 *
 * @author parashar
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomSectionEvaluationRule {
    
    private int bestOf;
    private Set<String> testMetadataKey;
    
}
