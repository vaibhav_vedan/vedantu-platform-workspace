package com.vedantu.reports.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectScore {
    private String subject;
    private Float averageScore;
    private Float maxScore;
}
