package com.vedantu.reports.pojo;

import lombok.Data;

import java.util.List;

@Data
public class SectionWiseInstruction {
    private String section;
    private List<String> instructions;
}
