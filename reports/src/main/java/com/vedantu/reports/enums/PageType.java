package com.vedantu.reports.enums;

public enum PageType {
    PROGRESS,
    PERFORMANCE
}
