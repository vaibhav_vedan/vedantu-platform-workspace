package com.vedantu.reports.enums;

public enum CMDSTestState {
	DRAFT, PRIVATE, PUBLIC
}
