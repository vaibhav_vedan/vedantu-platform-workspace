package com.vedantu.reports.enums;

public enum ProcessingStatus {
    PROCESSING, PROCESSED, REJECTED, NOT_USED, ALREADY_PROCESSED, PROCESSED_EMPTY, COLLECTION_NOT_PROCESSED;
}
