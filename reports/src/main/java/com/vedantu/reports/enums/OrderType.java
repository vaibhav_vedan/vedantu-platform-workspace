package com.vedantu.reports.enums;

/**
 * Created by somil on 20/03/17.
 */
public enum OrderType {
    ORIGINAL,RANDOM,SPECIFIED;
    public static OrderType valueOfKey(String key){
        OrderType type = ORIGINAL;
        try{
            type = valueOf(key.trim().toUpperCase());
        }
        catch(Exception e){

        }
        return type;
    }
}
