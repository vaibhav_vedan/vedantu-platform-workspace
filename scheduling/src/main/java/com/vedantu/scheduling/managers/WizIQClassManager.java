/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.serializers.SessionDAO;
import com.vedantu.scheduling.pojo.session.GTMGTTSessionDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LiveSessionPlatformType;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Encoder;

/**
 *
 * @author jeet
 */
@Service
public class WizIQClassManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(WizIQClassManager.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private SessionDAO sessionDAO;

    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    private static final String HASH_ALGO = "HmacSHA1";
    private static final String CHAR_ENCODING = "UTF-8";
    private static final String CREATE_METHOD = "create";
    private static final String RESCHEDULE_METHOD = "modify";
    private static final String DELETE_METHOD = "cancel";
    private static final String ADD_ATTENDEE = "add_attendees";
    public static final String SIGNATURE_PARAM_NAME = "signature";
    public static final String ACCESS_KEY_PARAM_NAME = "access_key";
    public static final String TIMESTAMP_PARAM_NAME = "timestamp";
    public static final String SECRET_KEY_PRAM_NAME = "secret_key";
    public static final String METHOD_PARAM_NAME = "method";

    private static final SimpleDateFormat wizIQTime = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final String secretkey = ConfigUtils.INSTANCE.getStringValue("wiziq.api.secretkey");
    private static final String accesskey = ConfigUtils.INSTANCE.getStringValue("wiziq.api.accesskey");
    private static final String webServiceUrl = ConfigUtils.INSTANCE.getStringValue("wiziq.api.webserviceurl");
    private static final String returnUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
    private static final String schedulingDirect = ConfigUtils.INSTANCE.getStringValue("platform.scheduling.direct");

    public void scheduleWizIQClass(Session session) throws Exception {
        if (!LiveSessionPlatformType.WIZIQ.equals(session.getLiveSessionPlatformType())) {
            return;
        }
        if (StringUtils.isNotEmpty(session.getWizIQclassId())) {
            reschedule(session);
        } else {
            create(session);
        }
    }

    public void create(Session session) throws Exception {
        if (session == null) {
            return;
        }
        logger.info("creating wiziq class for session:" + session.getId());
        String timeStamp = getTimeStamp();
        List<Long> studentIds = session.getStudentIds();
        if (ArrayUtils.isEmpty(studentIds)) {
            return;
        }
        UserBasicInfo teacher = fosUtils.getUserBasicInfo(session.getTeacherId(), true);
        UserBasicInfo student = fosUtils.getUserBasicInfo(studentIds.get(0), true);
        String signatureDigest = generateSignatureBase(CREATE_METHOD, timeStamp);
        MultivaluedMap formData = getPostFormCommonParams(timeStamp, signatureDigest);
        formData.add("title", String.valueOf(session.getId()));
        formData.add("presenter_id", String.valueOf(session.getTeacherId()));
        wizIQTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String startTime = wizIQTime.format(session.getStartTime());
        formData.add("start_time", startTime);
        int duration = (int) Math.ceil((session.getEndTime() - session.getStartTime()) / DateTimeUtils.MILLIS_PER_MINUTE);
        formData.add("duration", String.valueOf(duration));
        formData.add("time_zone", TIME_ZONE_IN);
        formData.add("create_recording", "true");
        formData.add("return_url", returnUrl);
        formData.add("presenter_name", teacher.getFirstName());
//        formData.add("presenter_email", teacher.getEmail());
        formData.add("language_culture_name", "en-US");
        formData.add("status_ping_url", schedulingDirect + "session/wiziq/statusping");
        ClientResponse resp = WebUtils.INSTANCE.doPostFormEncoded(webServiceUrl + "?method=" + CREATE_METHOD, formData);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        JSONObject response = XML.toJSONObject(jsonString);
        logger.info("response:" + response);
        if (!response.has("rsp")) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject rsp = response.getJSONObject("rsp");
        if (rsp == null || rsp.has("error") || !rsp.has(CREATE_METHOD)) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject create = rsp.getJSONObject(CREATE_METHOD);
        if (create == null || !create.has("class_details")) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject class_details = create.getJSONObject("class_details");
        if (class_details == null || !class_details.has("class_id") || !class_details.has("recording_url") || !class_details.has("presenter_list")) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject presenter_list = class_details.getJSONObject("presenter_list");
        if (presenter_list == null || !presenter_list.has("presenter")) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject presenter = presenter_list.getJSONObject("presenter");
        if (presenter == null || !presenter.has("presenter_url")) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        String classId = String.valueOf(class_details.getLong("class_id"));
        String recording_url = class_details.getString("recording_url");
        String presenter_url = presenter.getString("presenter_url");
        if (StringUtils.isEmpty(classId) || StringUtils.isEmpty(recording_url) || StringUtils.isEmpty(presenter_url)) {
            logger.error("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        String student_url = getStudentJoinUrl(classId, student.getUserId(), student.getFirstName());
        if (StringUtils.isEmpty(student_url)) {
            logger.info("there is some error creating wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        GTMGTTSessionDetails gTMGTTSessionDetails = new GTMGTTSessionDetails();
        gTMGTTSessionDetails.setStudentLink(student_url);
        gTMGTTSessionDetails.setTeacherLink(presenter_url);
        session.setLiveSessionPlatformDetails(gTMGTTSessionDetails);
        session.setWizIQclassId(classId);
        session.setReplayUrl(recording_url);
        sessionDAO.update(session, null);
    }

    public String getStudentJoinUrl(String classId, Long studentId, String name) throws Exception {
        logger.info("creating attendee url class for studentId:" + studentId + " classId: " + classId);
        String url = null;
        String timeStamp = getTimeStamp();
        String signatureDigest = generateSignatureBase(ADD_ATTENDEE, timeStamp);
        MultivaluedMap formData = getPostFormCommonParams(timeStamp, signatureDigest);
        String attendeeXml = "<attendee_list>"
                + "<attendee> "
                + "<attendee_id><![CDATA[" + studentId + "]]></attendee_id>"
                + " <screen_name><![CDATA[" + name + "]]></screen_name>"
                + "<language_culture_name><![CDATA[en-US]]></language_culture_name>"
                + "</attendee>"
                + "</attendee_list>";
        formData.add("class_id", classId);
        formData.add("attendee_list", attendeeXml);
        ClientResponse resp = WebUtils.INSTANCE.doPostFormEncoded(webServiceUrl + "?method=" + ADD_ATTENDEE, formData);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        JSONObject response = XML.toJSONObject(jsonString);
        logger.info("response:" + response);
        if (!response.has("rsp")) {
            logger.error("there is some error creating attendee url class for studentId:" + studentId + " classId: " + classId + "resp: " + response.toString());
            return url;
        }
        JSONObject rsp = response.getJSONObject("rsp");
        if (rsp == null || rsp.has("error") || !rsp.has(ADD_ATTENDEE)) {
            logger.error("there is some error creating attendee url class for studentId:" + studentId + " classId: " + classId + "resp: " + response.toString());
            return url;
        }
        JSONObject add = rsp.getJSONObject(ADD_ATTENDEE);
        if (add == null || !add.has("attendee_list")) {
            logger.error("there is some error creating attendee url class for studentId:" + studentId + " classId: " + classId + "resp: " + response.toString());
            return url;
        }
        JSONObject list = add.getJSONObject("attendee_list");
        if (list == null || !list.has("attendee")) {
            logger.error("there is some error creating attendee url class for studentId:" + studentId + " classId: " + classId + "resp: " + response.toString());
            return url;
        }
        JSONObject attendee = list.getJSONObject("attendee");
        if (attendee == null || !attendee.has("attendee_url")) {
            logger.error("there is some error creating attendee url class for studentId:" + studentId + " classId: " + classId + "resp: " + response.toString());
            return url;
        }
        String attendee_url = attendee.getString("attendee_url");
        if (StringUtils.isEmpty(attendee_url)) {
            logger.error("there is some error creating attendee url class for studentId:" + studentId + " classId: " + classId + "resp: " + response.toString());
            return url;
        }
        return attendee_url;
    }

    public void reschedule(Session session) throws Exception {
        logger.info("rescheduling wiziq session sessionId:" + session.getId() + " classId: " + session.getWizIQclassId());
        String timeStamp = getTimeStamp();
        String signatureDigest = generateSignatureBase(RESCHEDULE_METHOD, timeStamp);
        MultivaluedMap formData = getPostFormCommonParams(timeStamp, signatureDigest);
        formData.add("class_id", session.getWizIQclassId());
        wizIQTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String startTime = wizIQTime.format(session.getStartTime());
        formData.add("start_time", startTime);
        int duration = (int) Math.ceil((session.getEndTime() - session.getStartTime()) / DateTimeUtils.MILLIS_PER_MINUTE);
        formData.add("duration", String.valueOf(duration));
        ClientResponse resp = WebUtils.INSTANCE.doPostFormEncoded(webServiceUrl + "?method=" + RESCHEDULE_METHOD, formData);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        JSONObject response = XML.toJSONObject(jsonString);
        if (!response.has("rsp")) {
            logger.error("there is some error rescheduling wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject rsp = response.getJSONObject("rsp");
        if (rsp == null || rsp.has("error") || !rsp.has(RESCHEDULE_METHOD)) {
            logger.error("there is some error rescheduling wizIq class for :" + session.getId() + "resp: " + response.toString());
        }
    }

    public void delete(Session session) throws Exception {
        logger.info("deleting wiziq session sessionId:" + session.getId() + " classId: " + session.getWizIQclassId());
        String timeStamp = getTimeStamp();
        String signatureDigest = generateSignatureBase(DELETE_METHOD, timeStamp);
        MultivaluedMap formData = getPostFormCommonParams(timeStamp, signatureDigest);
        formData.add("class_id", session.getWizIQclassId());
        ClientResponse resp = WebUtils.INSTANCE.doPostFormEncoded(webServiceUrl + "?method=" + DELETE_METHOD, formData);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        JSONObject response = XML.toJSONObject(jsonString);
        if (!response.has("rsp")) {
            logger.error("there is some error deleting wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        JSONObject rsp = response.getJSONObject("rsp");
        if (rsp == null || rsp.has("error") || !rsp.has(DELETE_METHOD)) {
            logger.error("there is some error deleting wizIq class for :" + session.getId() + "resp: " + response.toString());
            return;
        }
        session.setLiveSessionPlatformDetails(null);
        session.setWizIQclassId(null);
        session.setReplayUrl(null);
        sessionDAO.update(session, null);
    }

    public MultivaluedMap getPostFormCommonParams(String timeStamp, String signatureDigest) throws Exception {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add(ACCESS_KEY_PARAM_NAME, accesskey);
        formData.add(TIMESTAMP_PARAM_NAME, timeStamp);
        formData.add(SIGNATURE_PARAM_NAME, signatureDigest);
        return formData;
    }

    private String generateSignatureBase(String method, String timeStamp) throws Exception {
        StringBuilder w_strbuffer = new StringBuilder();
        w_strbuffer.append(ACCESS_KEY_PARAM_NAME).append("=").append(accesskey);
        w_strbuffer.append("&");
        w_strbuffer.append(TIMESTAMP_PARAM_NAME).append("=").append(timeStamp);
        w_strbuffer.append("&");
        w_strbuffer.append(METHOD_PARAM_NAME).append("=").append(method);
        String data = w_strbuffer.toString();
        Mac mac = Mac.getInstance(HASH_ALGO);
        String w_privatekey = URLEncoder.encode(secretkey, CHAR_ENCODING);
        SecretKeySpec secret = new SecretKeySpec(w_privatekey.getBytes(),
                HASH_ALGO);
        mac.init(secret);
        byte[] databytes = data.getBytes(CHAR_ENCODING);
        byte[] messagedigest = mac.doFinal(databytes);
        BASE64Encoder base64 = new BASE64Encoder();
        return base64.encode(messagedigest);
    }

    private String getTimeStamp() {
        return Long.toString(System.currentTimeMillis() / 1000);
    }

    public JSONObject extractEntryExitTimes(JSONObject attendenceResponse) throws ParseException {
        JSONObject result = new JSONObject();
        //sample response
//{"attendance_report":{"class_duration":14,"class_id":7696333,"attendee_list":{"attendee":[{"exit_time":"10/9/2018 9:26:27 AM","entry_time":"10/9/2018 9:12:00 AM","attended_minutes":11,"screen_name":"Sim","attendee_id":"60633249"},{"exit_time":"10/9/2018 9:26:17 AM","entry_time":"10/9/2018 9:12:41 AM","attended_minutes":8,"presenter":true,"screen_name":"Niveditha test","attendee_id":"5638741783740416"}]}}}                
        if (attendenceResponse.has("attendance_report")
                && attendenceResponse.getJSONObject("attendance_report").has("attendee_list")
                && attendenceResponse.getJSONObject("attendance_report").getJSONObject("attendee_list").has("attendee")) {
            JSONObject attendee_list = attendenceResponse.getJSONObject("attendance_report").getJSONObject("attendee_list");
            Object attendeeObj = attendee_list.get("attendee");
            if (attendeeObj instanceof JSONArray) {
                JSONArray attendees = (JSONArray) attendeeObj;
                for (int i = 0; i < attendees.length(); i++) {
                    JSONObject attendee = attendees.getJSONObject(i);
                    insertTimes(result, attendee);
                }
            } else if (attendeeObj instanceof JSONObject) {
                insertTimes(result, (JSONObject) attendeeObj);
            }
        }
        return result;
    }

    private void insertTimes(JSONObject result, JSONObject attendee) throws ParseException {
        SimpleDateFormat datefb = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");      
        datefb.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        Long entryTime = datefb.parse(attendee.getString("entry_time")).getTime();
        Long exitTime = datefb.parse(attendee.getString("exit_time")).getTime();
        if (attendee.has("presenter")) {
            result.put("teacher_entry_time", entryTime);
            result.put("teacher_exit_time", exitTime);
        } else {
            result.put("student_entry_time", entryTime);
            result.put("student_exit_time", exitTime);
        }
    }
    
    public static void main(String[] ards) throws ParseException{
        String str="10/9/2018 9:5:54 AM";
        SimpleDateFormat datefb = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
        datefb.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        System.out.println(datefb.parse(str));
    }
}
