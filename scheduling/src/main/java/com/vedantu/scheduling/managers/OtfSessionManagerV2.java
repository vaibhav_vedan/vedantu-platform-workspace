package com.vedantu.scheduling.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.*;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.request.AddOTFSessionsReq;
import com.vedantu.onetofew.request.GetOTFSessionsReq;
import com.vedantu.onetofew.request.OTFJoinSessionReq;
import com.vedantu.onetofew.request.UpdateReplayUrlReq;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.pojo.UpcomingSessionsPojo;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.request.AddBatchSessionReq;
import com.vedantu.scheduling.request.CancelOrDeleteSessionReq;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.scheduling.response.UpdateSessionRes;
import com.vedantu.session.pojo.OTFCalendarSessionInfo;
import com.vedantu.session.pojo.OTFCourseBasicInfo;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OtfSessionManagerV2 {

    private static final Gson GSON = new Gson();


    private final Logger logger = LogFactory.getLogger(OtfSessionManagerV2.class);
    private final OTFSessionManager otfSessionManager;
    private final CalendarEntryManager calendarEntryManager;
    private final SessionRecordingManager sessionRecordingManager;
    private final AwsSQSManager awsSQSManager;
    private final FosUtils fosUtils;

    @Autowired
    public OtfSessionManagerV2(OTFSessionManager otfSessionManager,
                               CalendarEntryManager calendarEntryManager,
                               SessionRecordingManager sessionRecordingManager,
                               AwsSQSManager awsSQSManager,
                               FosUtils fosUtils) {
        this.otfSessionManager = otfSessionManager;
        this.calendarEntryManager = calendarEntryManager;
        this.sessionRecordingManager = sessionRecordingManager;
        this.awsSQSManager = awsSQSManager;
        this.fosUtils = fosUtils;
    }

    public OTFSession getOtfSessionById(String id) {
        return otfSessionManager.getOtfSessionById(id);
    }

    public List<OTFSessionPojo> getUpcomingSessions(GetOTFUpcomingSessionReq req)
            throws VException {

        List<OTFSessionPojo> upcomingSessions = otfSessionManager.getUpcomingSessions(req.getAfterEndTime(),
                req.getTeacherId(), req.getStudentId(), null, req.getStart(), req.getLimit(), req.getQuery(), false);

        logger.info("Response count :{} ", upcomingSessions == null ? 0 : upcomingSessions.size());
        return upcomingSessions;
    }

    public List<OTFSessionPojo> getUpcomingSessionsWithAttendeeInfos(long startTime, long endTime, Integer start, Integer size, String sessionId) throws VException {
        logger.info("getting upcoming sessions with attendee infos");
        List<OTFSessionPojo> sessionPojos = otfSessionManager.getUpcomingSessionsWithAttendeeInfos(startTime, endTime, start, size, sessionId);


        populatePresenterInfos(sessionPojos, false);
        populateAttendeeInfos(sessionPojos, false);
        return sessionPojos;
    }

    private void populatePresenterInfos(List<OTFSessionPojo> sessions, boolean exposeEmail) {
        if (!com.vedantu.util.CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojo session : sessions) {
                userIds.add(session.getPresenter());
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojo session : sessions) {
                if (userMap.containsKey(session.getPresenter())) {
                    session.setPresenterInfo(userMap.get(session.getPresenter()));
                }
            }
        }
    }

    private void populateAttendeeInfos(List<OTFSessionPojo> sessions, boolean exposeEmail) {
        if (!com.vedantu.util.CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojo session : sessions) {
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendees())) {
                    userIds.addAll(session.getAttendees());
                }
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendeeInfos())) {
                    Set<String> allAttendees = session.getAttendeeInfos().stream()
                            .map(OTFSessionAttendeeInfo::getUserId).filter(Objects::nonNull).map(String::valueOf)
                            .collect(Collectors.toSet());
                    userIds.addAll(allAttendees);
                }
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojo session : sessions) {
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendees())) {
                    for (String studentId : session.getAttendees()) {
                        if (userMap.containsKey(studentId)) {
                            session.updateUserBasicInfo(userMap.get(studentId));
                        }
                    }
                }
            }
        }
    }

    public List<OTFSessionPojo> getPastSessions(GetOTFSessionsReq otfSessionsReq, boolean exposeEmail)
            throws VException {
        logger.info("getting past sessions for {}", GSON.toJson(otfSessionsReq));
        List<OTFSessionPojo> sessions = otfSessionManager.getPastSessions(otfSessionsReq);

        logger.info("Response count :{}", sessions == null ? 0 : sessions.size());
        populatePresenterInfos(sessions, exposeEmail);
        populateAttendeeInfos(sessions, exposeEmail);
        return sessions;
    }

    public List<OTFSessionPojo> getUserPastSessions(GetOTFSessionsReq getOTFSessionsReq)
            throws VException {
        switch (getOTFSessionsReq.getCallingUserRole()) {
            case STUDENT:
                getOTFSessionsReq.setStudentId(getOTFSessionsReq.getCallingUserId());
                break;
            case TEACHER:
                getOTFSessionsReq.setTeacherId(getOTFSessionsReq.getCallingUserId());
                break;
            default:
                throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED,
                        "Calling user role is not supported for this request");
        }
        return getPastSessions(getOTFSessionsReq, false);
    }

    public String getJoinSessionUrl(OTFJoinSessionReq otfJoinSessionReq, HttpSessionData sessionData)
            throws VException {
        logger.info("join session for {},  session {}", GSON.toJson(otfJoinSessionReq), GSON.toJson(sessionData));
        Long callingUserId = sessionData.getUserId();
        Role callingUserRole = sessionData.getRole();

        if (!Role.ADMIN.equals(callingUserRole)) {
            otfJoinSessionReq.setUserId(String.valueOf(callingUserId));
            otfJoinSessionReq.setRole(callingUserRole);
            otfJoinSessionReq.setAdmin(false);
        } else {
            if (StringUtils.isEmpty(otfJoinSessionReq.getUserId())) {
                otfJoinSessionReq.setUserId(String.valueOf(callingUserId));
            }

            User user = fosUtils.getUserInfo(Long.parseLong(otfJoinSessionReq.getUserId()), false);
            otfJoinSessionReq.setRole(user.getRole());
            otfJoinSessionReq.setAdmin(true);
        }

        String joinSession = otfSessionManager.joinSession(otfJoinSessionReq);
        logger.info("join session json {}", joinSession);
        return joinSession;
    }

    public List<OTFSessionPojo> getSessions(Long afterStartTime, Long beforeStartTime, Long afterEndTime, String teacherId, String studentId) throws VException {
        logger.info("get session query");
        logger.info("getSessions {} {} {} {}", afterStartTime, beforeStartTime, teacherId, studentId);

        List<OTFSessionPojo> sessions = otfSessionManager.getOtfSessions(afterStartTime, beforeStartTime, afterEndTime, teacherId, studentId);

        populatePresenterInfos(sessions, false);
        return sessions;
    }

    public void addBatchSession(AddBatchSessionReq req) throws Exception {
        otfSessionManager.addBatchSession(req);
    }

    public UpdateSessionRes cancelSession(CancelOrDeleteSessionReq req) throws VException {
        UpdateSessionRes updateSessionRes = otfSessionManager.cancelSession(req);
        OTFSession cancelledSession = updateSessionRes.getPreUpdateSession();

//        awsSQSManager.sendToSQS(SQSMessageType.CANCEL_SESSION_CALENDAR_ENTRY_TASK.getQueue(),
//                SQSMessageType.CANCEL_SESSION_CALENDAR_ENTRY_TASK, GSON.toJson(updateSessionRes));

        // Update calendar
        logger.info("send sqs task for marking calendar {}", cancelledSession.getId());

        AddCalendarEntryReq calendarEntryReq = new AddCalendarEntryReq(cancelledSession.getPresenter(),
                cancelledSession.getStartTime(),cancelledSession.getEndTime(),
                com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION,
                CalendarReferenceType.OTF_BATCH, cancelledSession.getId());
        List<AddCalendarEntryReq> addCalendarEntryReqList = Collections.singletonList(calendarEntryReq);
        Map<String, Object> payload = new HashMap<>();
        payload.put("mark", Boolean.FALSE);
        payload.put("req", addCalendarEntryReqList);
        awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.MARK_CALENDAR_ENTRIES, GSON.toJson(payload));
        return updateSessionRes;
    }

    /**
     * cancel otf session calendar unmark task from sqs
     *
     * @param updateSessionRes cancel session response
     */
    public void cancelSessionTask(OTFUpdateSessionRes updateSessionRes) throws VException {
        OTFSessionPojoUtils cancelledSession = updateSessionRes.getPreUpdateSession();
        if (cancelledSession == null) {
            logger.warn("cancelled session is null");
            return;
        }
        if (cancelledSession.getStartTime() < System.currentTimeMillis()) {
            logger.info("Session is in past/forfeited");
            return;
        }
        List<BatchEnrolmentInfo> batchEnrolmentInfo = getBatchEnrolmentsByIds(cancelledSession.getBatchIds());
        List<Long> studentIds = new ArrayList<>();
        batchEnrolmentInfo.forEach(a -> a.getEnrolledStudentIds().forEach(b -> studentIds.add((Long.parseLong(b)))));
        calendarEntryManager.unblockCalenderEntries(cancelledSession, studentIds);
    }

    private List<BatchEnrolmentInfo> getBatchEnrolmentsByIds(Set<String> batchIds) throws VException {

        if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }

        ClientResponse resp = WebUtils.INSTANCE
                .doCall(ConfigUtils.INSTANCE.getSubscriptionEndpoint() + "batch/getEnrolmentsByBatchIds?", HttpMethod.POST, GSON.toJson(batchIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BatchEnrolmentInfo>>() {
        }.getType();
        return GSON.fromJson(jsonString, listType);
    }

    public void handleSessionRecordings(String sessionId) {
        try {
            HandleOTFRecordingLambda sessionRecording = sessionRecordingManager.handleSessionRecording(sessionId);
            logger.info("handle session response for id : {} response {} ", sessionId, GSON.toJson(sessionRecording));

            if (!CollectionUtils.isEmpty(sessionRecording.getRecordings())) {
                sendRecordingRequestToSQS(sessionRecording);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private void sendRecordingRequestToSQS(HandleOTFRecordingLambda handleOTFRecordingLambda) {
        if (handleOTFRecordingLambda.getSessionId() != null) {
            OTFSession session = getOtfSessionById(handleOTFRecordingLambda.getSessionId());
            try {
                if (StringUtils.isEmpty(session.getOrganizerAccessToken())) {
                    // Without organizer token we cannot delete the recording
                    logger.error("No orgainzer token for session id : " + session.getId());
                    return;
                }

                String credentials = ConfigUtils.INSTANCE
                        .getStringValue("gtt.account.credentials." + session.getOrganizerAccessToken());
                if (StringUtils.isEmpty(credentials)) {
                    logger.error("Credentials missing for session id : " + session.getId());
                    return;
                }
                String[] credentialArr = credentials.split("-");
                HandleOTFRecordingLambda.Credentials messageObject = new HandleOTFRecordingLambda.Credentials(credentialArr[0], credentialArr[1]);
                handleOTFRecordingLambda.setCredentials(messageObject);
                awsSQSManager.sendToSQS(SQSQueue.OTF_RECORDING, SQSMessageType.HANDLE_OTF_RECORDING, new Gson().toJson(handleOTFRecordingLambda));
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    public void invokeSessionRecordingLambda(HandleOTFRecordingLambda lambda) {
        sendRecordingRequestToSQS(lambda);
    }

    public PlatformBasicResponse updateRecordings(Long afterEndTime, Long beforeEndTime) {
        List<HandleOTFRecordingLambda> lambdaList = sessionRecordingManager.handleSesssionRecordingsBulk(afterEndTime, beforeEndTime);
        logger.info("Sessions: {}", GSON.toJson(lambdaList));

        if (!CollectionUtils.isEmpty(lambdaList)) {
            List<String> errors = new ArrayList<>();
            for (HandleOTFRecordingLambda request : lambdaList) {
                try {
                    sendRecordingRequestToSQS(request);
                } catch (Exception ex) {
                    String errorMessage = "Error invoking ex:" + ex.toString() + " message:" + ex.getMessage()
                            + " request:" + request.toString();
                    logger.info(errorMessage);
                    errors.add(errorMessage);
                }
            }

            if (!CollectionUtils.isEmpty(errors)) {
                String errorString = String.join(",", errors);
                logger.info("RecordingLambdaErrors: {}", errorString);
            }
        }

        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setResponse(GSON.toJson(lambdaList));
        return response;
    }

    public PlatformBasicResponse updateSessionReplayUrls(Long afterEndTime, Long beforeEndTime) {
        List<OTFSession> sessions = sessionRecordingManager.updateSessionReplayUrls(afterEndTime, beforeEndTime);
        String json = GSON.toJson(sessions);
        logger.info("Sessions: {}", json);
        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setResponse(json);
        return response;
    }

    public PlatformBasicResponse updateReplayUrlforGTW(UpdateReplayUrlReq req) throws BadRequestException, NotFoundException {
        otfSessionManager.updateReplayUrlforGTW(req);
        return new PlatformBasicResponse();
    }

    public List<OTFSessionPojo> teacherAddSessionToBatch(AddOTFSessionsReq req, HttpSessionData sessionData) throws VException {
        if(sessionData != null && sessionData.getUserId() != null){
            UserBasicInfo teacher = fosUtils.getUserBasicInfo(sessionData.getUserId(),true);
            if(teacher != null && StringUtils.isEmpty(teacher.getOrgId())){
                throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule the sessions");
            }
            //TODO techer orgId check with batch orgId
        }
        else{
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule the sessions");
        }

        if (req.getSessionPojo().size() > 5) {
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule more than 5 sessions at once");
        }
        return otfSessionManager.addOTFSessionsToBatch(req);
    }

    public List<OTFCalendarSessionInfo> getUserUpcomingSessions(Integer start, Integer size, String batchId,
                                                                Long userId, Role userRole, String query, Long startTime, Long endTime, Set<String> batchIds) throws VException {

        List<OTFSessionPojo> upcomingSessions = otfSessionManager.getUpcomingSessions(String.valueOf(userId), userRole, query, start, size, batchId, startTime, endTime, batchIds);

        String json = GSON.toJson(upcomingSessions);
        logger.info("getUserUpcomingSessions Response : {}", json);
        logger.info("Response count : {}", upcomingSessions == null ? 0 : upcomingSessions.size());
        return populateUsersAndBoards(GSON.fromJson(json, new TypeToken<List<OTFSessionPojoUtils>>(){}.getType()));
    }

    public List<OTFCalendarSessionInfo> getUserPastSessions(Integer start, Integer size, String batchId, Long userId,
                                                            Role userRole, String query, Long startTime, Long endTime, Set<String> batchIds) throws VException {


        List<OTFSessionPojo> pastSessions = otfSessionManager.getPastSessions(String.valueOf(userId), userRole, query, start, size, batchId, startTime, endTime, batchIds);
        String json = GSON.toJson(pastSessions);
        logger.info("getUserPastSessions Response : {}", json);
        logger.info("getUserPastSessions Response count : {}", pastSessions == null ? 0 : pastSessions.size());
        return populateUsersAndBoards(GSON.fromJson(json, new TypeToken<List<OTFSessionPojoUtils>>(){}.getType()));
    }

    private List<OTFCalendarSessionInfo> populateUsersAndBoards(List<OTFSessionPojoUtils> sessions) {
        List<OTFCalendarSessionInfo> calendarSessionInfos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(sessions)) {
            Set<Long> boardIds = new HashSet<>();
            Set<Long> userIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                if (session.getBoardId() != null) {
                    boardIds.add(session.getBoardId());
                } else {
                    if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                        for(OTFCourseBasicInfo courseBasicInfo:session.getCourseInfos()){
                            List<String> subjects = courseBasicInfo.getSubjects();
                            if (!CollectionUtils.isEmpty(subjects)) {
                                boardIds.add(Long.parseLong(subjects.get(0)));
                            }
                        }
                    }
                }

                if (!StringUtils.isEmpty(session.getPresenter())) {
                    userIds.add(Long.parseLong(session.getPresenter()));
                }
            }

            // Fetch boards and users
            Map<Long, Board> boards = fosUtils.getBoardInfoMap(boardIds);
            Map<Long, User> users = fosUtils.getUserInfosMap(userIds, false);

            for (OTFSessionPojoUtils session : sessions) {
                OTFCalendarSessionInfo calendarSessionInfo = new OTFCalendarSessionInfo(session);
                if (session.getBoardId() != null && session.getBoardId() > 0l) {
                    Board board = boards.get(session.getBoardId());
                    if (board != null) {
                        calendarSessionInfo.setSubject(board.getName());
                    }
                } else {

                    if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                        for(OTFCourseBasicInfo courseBasicInfo:session.getCourseInfos()){
                            List<String> subjects = courseBasicInfo.getSubjects();
                            if (!CollectionUtils.isEmpty(subjects)) {
                                Board board = boards.get(Long.parseLong(subjects.get(0)));
                                if (board != null) {
                                    calendarSessionInfo.setSubject(board.getName());
                                }
                            }
                        }
                    }
                }

                List<UserSessionInfo> attendees = new ArrayList<>();
                if (!StringUtils.isEmpty(session.getPresenter())) {
                    User user = users.get(Long.parseLong(session.getPresenter()));
                    if (user != null) {
                        attendees.add(new UserSessionInfo(user));
                    }
                }
                if (!CollectionUtils.isEmpty(session.getAttendees())) {
                    for (String studentId : session.getAttendees()) {
                        User user = users.get(Long.parseLong(studentId));
                        if (user != null) {
                            attendees.add(new UserSessionInfo(user));
                        }
                    }
                }
                if(ArrayUtils.isNotEmpty(session.getGroupNames())) {
                    calendarSessionInfo.setGroupNames(session.getGroupNames());
                }
                calendarSessionInfo.setAttendees(attendees);
                calendarSessionInfo.setId(calendarSessionInfo.getOtfSessionId());
                calendarSessionInfos.add(calendarSessionInfo);

            }
        }

        return calendarSessionInfos;
    }

    public List<OTFCalendarSessionInfo> getPastSessionsForUserWithFilter(Integer start, Integer size, String batchId, Long userId, Role role, String query, Long startTime, Long endTime, Set<String> batchIds) throws UnsupportedEncodingException, VException {
        logger.info("getPastSessionsForUserWithFilter {} {}", userId, query);
        List<OTFSessionPojo> pojos = otfSessionManager.getUserPastSessionsWithFilter(String.valueOf(userId), role, query, start, size, batchId, startTime, endTime, batchIds);
        List<OTFSessionPojoUtils> sessions = GSON.fromJson(GSON.toJson(pojos), new TypeToken<List<OTFSessionPojoUtils>>(){}.getType());
        logger.info("getPastSessionsForUserWithFilter res size {}", sessions.size());
        return populateUsersAndBoards(sessions);
    }

    public List<OTFCalendarSessionInfo> getOTFSessionsForCalendar(Long beforeStartTime, Long afterStartTime,
                                                                  Long beforeEndTime, Long afterEndTime, String userId,
                                                                  Role userRole, String query) throws VException {
        List<OTFSessionPojo> sessions = otfSessionManager.getCalendarSessions(afterStartTime, beforeStartTime, beforeEndTime, afterEndTime,
                userId, userRole, query);

        String json = GSON.toJson(sessions);
        logger.info("Response : {}", json);
        logger.info("Response count : {}", sessions == null ? 0 : sessions.size());
        return populateUsersAndBoards(GSON.fromJson(json, new TypeToken<List<OTFSessionPojoUtils>>(){}.getType()));
    }

    public List<UpcomingSessionsPojo> upcomingSessionsAndWebinarForUser(String userId, Role role, Integer start, Integer limit) throws VException {
        return otfSessionManager.upcomingSessionsAndWebinarForUser(userId, role, start, limit);
    }
}
