package com.vedantu.scheduling.managers;

import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.vedantu.onetofew.pojo.EnrollmentPojo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.dao.serializers.ScheduleDAO;
import com.vedantu.scheduling.pojo.GTTSessionInfoRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public abstract class ScheduleManager {

	private static int MAX_CONNECTIONS = 1000;

	private static int SCHEDULE_INTERVAL;

	public String[] authAccessArray;

	public Map<String, String> overwriteAccessKeys;

	public List<String> authAccessList;

	@Autowired
	private ScheduleDAO scheduleDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ScheduleManager.class);

	public abstract int getAccessArrayLength();

	public abstract String getAccessToken(int slot);

	public abstract int getAccessTokenSlot(String accessToken);

	public abstract String getPostOverwriteAccessToken(String accessToken);

	public abstract String getOrganizerKeyForToken(String accessToken);

	public abstract BitSet getScheduleBitSet(Schedule schedule);

	public abstract void createSessionLink(OTFSession oTFSession) throws Exception;

	public abstract List<GTTAttendeeDetails> registerAttendees(OTFSession oTFSession,Map<String,List<EnrollmentPojo>> userEnrollmentMap)
			throws InternalServerErrorException, NotFoundException;

	public abstract GTTAttendeeDetails registerAttendee(OTFSession oTFSession, String userId,List<EnrollmentPojo> enrollmentId)
			throws NotFoundException, InternalServerErrorException;

	public abstract String getTeacherJoinUrl(OTFSession oTFSession, String teacherId) throws NotFoundException;

	public abstract String getStudentJoinUrl(OTFSession oTFSession, String studentId)
			throws NotFoundException, InternalServerErrorException;

	public abstract void handleFeedback(OTFSession oTFSession) throws VException, InternalServerErrorException;
        
        public abstract List<GTTAttendeeDetails> createGTTAttendeeDetails(OTFSession session,Map<String,List<EnrollmentPojo>> userEnrollmentMap,AttendeeContext context) throws InternalServerErrorException;

	public abstract void userJoined(OTFSession oTFSession, String userId);
        
        public abstract List<GTTSessionInfoRes> getGTTSessionInfos(OTFSession oTFSession) throws NotFoundException;

	@PostConstruct
	public void init() {
		SCHEDULE_INTERVAL = Integer.parseInt(ConfigUtils.INSTANCE.properties.getProperty("intervalsInSeconds"));
	}

	public int scheduleAvailableSlot(long startTime, long endTime) {
		endTime = endTime + SCHEDULE_INTERVAL;
		List<Schedule> schedules = fetchSchedules(startTime, endTime);
		Map<Long, Schedule> resultSchedules = new HashMap<Long, Schedule>();
		if (schedules != null) {
			for (Schedule schedule : schedules) {
				resultSchedules.put(schedule.getStartTime(), schedule);
			}
		}

		BitSet resultBitSet = new BitSet(MAX_CONNECTIONS);
		long firstTimeStamp = startTime - (startTime % SCHEDULE_INTERVAL);
		long lastTimeStamp = (endTime % SCHEDULE_INTERVAL == 0) ? (endTime - SCHEDULE_INTERVAL)
				: (endTime - (endTime % SCHEDULE_INTERVAL));
		for (long timeStamp = firstTimeStamp; timeStamp <= lastTimeStamp; timeStamp = timeStamp + SCHEDULE_INTERVAL) {
			BitSet bitSet = null;
			Schedule schedule = resultSchedules.get(timeStamp);
			if (schedule == null) {
				schedule = new Schedule(timeStamp);
				resultSchedules.put(timeStamp, schedule);
			}

			bitSet = getScheduleBitSet(schedule);
			logger.info("resultBitSet : " + resultBitSet.toString() + ", bitSet : " + bitSet.toString());
			resultBitSet.or(bitSet);
		}

		logger.info("scheduleAvailableSlot resultBitSet" + resultBitSet.toString() + " for startTime : " + startTime
				+ " to endTime : " + endTime);

		// TODO : Next clear bit randomize
		int organizerRef = resultBitSet.nextClearBit(0);
		if (organizerRef < getAccessArrayLength()) {
			for (Map.Entry<Long, Schedule> temp : resultSchedules.entrySet()) {
				getScheduleBitSet(temp.getValue()).set(organizerRef);
				try {
					scheduleDAO.save(temp.getValue());
				} catch (Exception ex) {
					logger.error("scheduleAvailableSlot : Error updating schedule for start time : "
							+ temp.getValue().getStartTime());
					organizerRef = -1;
					break;
				}
			}
		} else {
			organizerRef = -1;
		}

		logger.info("scheduleAvailableSlot organizerRef : " + organizerRef + " for startTime : " + startTime
				+ " to endTime : " + endTime);
		return organizerRef;
	}
        
        public boolean isSlotAvailable(long startTime, long endTime) {
		endTime = endTime + SCHEDULE_INTERVAL;
		List<Schedule> schedules = fetchSchedules(startTime, endTime);
		Map<Long, Schedule> resultSchedules = new HashMap<Long, Schedule>();
		if (schedules != null) {
			for (Schedule schedule : schedules) {
				resultSchedules.put(schedule.getStartTime(), schedule);
			}
		}

		BitSet resultBitSet = new BitSet(MAX_CONNECTIONS);
		long firstTimeStamp = startTime - (startTime % SCHEDULE_INTERVAL);
		long lastTimeStamp = (endTime % SCHEDULE_INTERVAL == 0) ? (endTime - SCHEDULE_INTERVAL)
				: (endTime - (endTime % SCHEDULE_INTERVAL));
		for (long timeStamp = firstTimeStamp; timeStamp <= lastTimeStamp; timeStamp = timeStamp + SCHEDULE_INTERVAL) {
			BitSet bitSet = null;
			Schedule schedule = resultSchedules.get(timeStamp);
			if (schedule == null) {
				schedule = new Schedule(timeStamp);
				resultSchedules.put(timeStamp, schedule);
			}

			bitSet = getScheduleBitSet(schedule);
			logger.info("resultBitSet : " + resultBitSet.toString() + ", bitSet : " + bitSet.toString());
			resultBitSet.or(bitSet);
		}

		logger.info("isSlotAvailable resultBitSet" + resultBitSet.toString() + " for startTime : " + startTime
				+ " to endTime : " + endTime);

		// TODO : Next clear bit randomize
		int organizerRef = resultBitSet.nextClearBit(0);
		if (organizerRef < getAccessArrayLength()) {
                    return true;
		}
                    
                return false;
	}

	public void descheduleSlot(long startTime, long endTime, int organizerRef) {
		if (organizerRef < 0) {
			logger.info("organizer token is negative : " + organizerRef);
			return;
		}
		endTime = endTime + SCHEDULE_INTERVAL;
		List<Schedule> schedules = fetchSchedules(startTime, endTime);
		long firstTimeStamp = startTime - (startTime % SCHEDULE_INTERVAL);
		long lastTimeStamp = (endTime % SCHEDULE_INTERVAL == 0) ? (endTime - SCHEDULE_INTERVAL)
				: (endTime - (endTime % SCHEDULE_INTERVAL));
		int expectedSize = (int) ((lastTimeStamp - firstTimeStamp) / SCHEDULE_INTERVAL);
		if (schedules == null || (schedules.size() - 1) != expectedSize) {
			logger.error(
					"descheduleSlot : descheduleSlotError - Schedule entries not found for descheduling. startTime : "
							+ firstTimeStamp + " endTime : " + endTime + "OrganizerRef : " + organizerRef);
			return;
		}

		for (Schedule schedule : schedules) {
			if (!getScheduleBitSet(schedule).get(organizerRef)) {
				logger.error(
						"descheduleSlot : descheduleSlotError - Schedule entry is already marked false for organizer. StartTime : "
								+ schedule.getStartTime() + " OrganizerRef : " + organizerRef);
				continue;
			}
			getScheduleBitSet(schedule).set(organizerRef, false);
			try {
				scheduleDAO.save(schedule);
			} catch (Exception ex) {
				logger.error("descheduleSlotError : Error descheduling for start time : " + schedule.getStartTime()
						+ "for OrganizerRef" + organizerRef);
			}
		}
		logger.info("descheduleSlot " + " for startTime : " + startTime + " to endTime : " + endTime);
	}

	private List<Schedule> fetchSchedules(Long startTime, Long endTime) {
		long firstTimeStamp = startTime - (startTime % SCHEDULE_INTERVAL);
		long lastTimeStamp = (endTime % SCHEDULE_INTERVAL == 0) ? (endTime - SCHEDULE_INTERVAL)
				: (endTime - (endTime % SCHEDULE_INTERVAL));
		Query query = new Query();
		query.addCriteria(Criteria.where(Schedule.Constants.START_TIME).gte(firstTimeStamp)
				.andOperator(Criteria.where(Schedule.Constants.START_TIME).lte(lastTimeStamp)));
		List<Schedule> schedules = scheduleDAO.runQuery(query, Schedule.class);
		return schedules;
	}

	public static Long getScheduleInterval() {
		return Long.valueOf(SCHEDULE_INTERVAL);
	}
}
