/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class AwsSNSManager extends AbstractAwsSNSManager{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);
    
    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public AwsSNSManager() {
        super();
        logger.info("initializing AwsSNSManager");
    }

    @Override
    public void createSubscriptions() {

        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "cron/sendOtmTrialSessionEmails");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "cron/sendLiveSessionEmails");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "cron/sendModularRegularSessionEmails");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "session/sendPostSessionEmails");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "onetofew/vwavesession/cacheschedulingcollectionscron");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "onetofew/session/syncPollsData");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "onetofew/session/sendNotificationToAcadOps");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "microcourses/sendRemainderSMS");
        createCronSubscription(CronTopic.CRON_CHIME_1_Minute, "onetofew/vwavesession/cacheschedulingcollectionscron");
        createCronSubscription(CronTopic.CRON_CHIME_30_Minutes, "onetofew/vwavesession/cachegttattendeecollectionscron");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "onetofew/vwavesession/vedantuwavesessionscron");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_3PM_IST, "onetofew/session/sendPostSessionEmails");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "onetofew/vwavesession/cachegttattendeecollectionscron");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "cron/sendOtmNoShowEmails");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "onetofew/vwavesession/sendAbsenteeSMS");

        createCronSubscription(CronTopic.CRON_CHIME_1_Minute, "gttattendee/cron/game/sessionactivity");
        //EARLY LEARNING SESSION REMINDER
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "cron/sendEarlyLearningClassReminder");
        createCronSubscription(SNSTopicOTF.SESSION_EVENTS_OTF, "onetofew/session/handleSessionEventsForOTF");
        createCronSubscription(SNSTopic.SCHEDULING_REDIS_OPS, "cron/performRedisOps");
        createCronSubscription(SNSTopicOTF.BATCH_EVENTS_OTF, "onetofew/session/handleBatchEventsForOTF");
        // exp/segRedis migrated for redis cluster separation from platform
        createCronSubscription(SNSTopic.SESSION_EVENTS, "session/handleSessionEventsForLatestSession");
    }
    @Override
    public void createTopics() {
        createSNSTopic(CronTopic.CRON_CHIME_1_Minute);
        createSNSTopic(SNSTopic.PROCESS_OTF_POLLS_DATA);
        createSNSTopic(CronTopic.UPDATE_GTT_SESSION_DATA);
        createSNSTopic(SNSTopicOTF.SESSION_EVENTS_OTF);
        createSNSTopic(SNSTopicOTF.BATCH_EVENTS_OTF);
        createSNSTopic(SNSTopic.SCHEDULING_REDIS_OPS);
    }
    
}
