package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.exception.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.scheduling.dao.serializers.SessionDAO;
import com.vedantu.scheduling.dao.serializers.SessionStateDetailsDAO;
import com.vedantu.scheduling.dao.sql.entity.SessionStateDetails;
import com.vedantu.scheduling.utils.SessionUtils;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class SessionStateDetailsManager {

	@Autowired
	private SessionStateDetailsDAO sessionStateDetailsDAO;

	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@Autowired
	private SessionDAO sessionDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionStateDetailsManager.class);

	public SessionStateDetails createSessionStateDetails(Long sessionId) throws VException {
		logger.info("Request : " + sessionId);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			// Check if the entry already exist for this session
			SessionStateDetails sessionStateDetails = getSessionStateDetailsBySessionId(sessionId, session);
			if (sessionStateDetails != null) {
				String errorMessage = "sessionStateDetails already exists for the request id : " + sessionId;
				logger.error(errorMessage);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
			}

			sessionStateDetails = new SessionStateDetails(sessionId);
			transaction.begin();
			sessionStateDetailsDAO.create(sessionStateDetails, session);
			transaction.commit();
			logger.info("Response " + sessionStateDetails);
			return sessionStateDetails;
		} catch (VException e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public List<SessionStateDetails> createSessionStateDetails(List<Long> sessionIds) throws VException {
		if (CollectionUtils.isEmpty(sessionIds)) {
			logger.error("SessionIds empty");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session ids empty");
		}
		logger.info("Request : " + Arrays.toString(sessionIds.toArray()));

		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			// Check if the entry already exist for this session
			List<SessionStateDetails> existingResults = getSessionStateDetailsBySessionId(sessionIds, session);
			if (!CollectionUtils.isEmpty(existingResults)) {
				String errorMessage = "sessionStateDetails already exists for the request ids : "
						+ Arrays.toString(sessionIds.toArray()) + " existing entries : "
						+ Arrays.toString(existingResults.toArray());
				logger.error(errorMessage);
			}

			// TODO : handle existing entries. Test the existing code and see
			// how it work in exception in single entry
			List<SessionStateDetails> sessionStateDetails = new ArrayList<>();
			for (Long sessionId : sessionIds) {
				sessionStateDetails.add(new SessionStateDetails(sessionId));
			}
			transaction.begin();
			sessionStateDetailsDAO.updateAll(sessionStateDetails, session);
			transaction.commit();
			logger.info("Response " + sessionStateDetails);
			return sessionStateDetails;
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public SessionStateDetails getSessionStateDetailsBySessionId(Long sessionId) throws Exception {
		logger.info("Request : " + sessionId);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			SessionStateDetails sessionStateDetails = getSessionStateDetailsBySessionId(sessionId, session);
			logger.info("Response " + sessionStateDetails);
			return sessionStateDetails;
		} catch (Exception e) {
			logger.error("getSessionStateDetailsBySessionId - Error fetching the session with id : " + sessionId);
			throw e;
		} finally {
			session.close();
		}
	}

	public SessionStateDetails getSessionStateDetailsBySessionId(Long sessionId, Session session) {
		return getSessionStateDetailsBySessionId(sessionId, session, null);
	}        
        
	public SessionStateDetails getSessionStateDetailsBySessionId(Long sessionId, Session session, LockMode lockMode) {
		logger.info("Request : " + sessionId);
		return sessionStateDetailsDAO.getBySessionId(sessionId, session, lockMode);
	}

	public List<SessionStateDetails> getSessionStateDetailsBySessionId(List<Long> sessionIds, Session session) {
		logger.info("Request : " + Arrays.toString(sessionIds.toArray()));
		return sessionStateDetailsDAO.getBySessionId(sessionIds, session);
	}

	public SessionStateDetails userJoined(Long sessionId, Long startTime, Long endTime, Long allowedJoinTime,
			Long expiryTime) throws VException, NotFoundException {
		logger.info("Request - sessionId:" + sessionId + " startTime:" + startTime + " endTime:" + endTime
				+ " expiryTime:" + expiryTime + ", allowedJoinTime: " + allowedJoinTime);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();

			// Fetch the session state details
			SessionStateDetails sessionStateDetails = getSessionStateDetailsBySessionId(sessionId, session);
			if (sessionStateDetails == null) {
				String errorMessage = "sessionStateDetails does not exist for the request id : " + sessionId;
				if (endTime >= System.currentTimeMillis()) {
					// Remove sentry error if the endTime is in the past
					logger.warn(errorMessage);
					//added this to avoid errors which resulted to mysql credentails> secrets deployment failure
					createSessionStateDetails(sessionId);
					throw new ForbiddenException(ErrorCode.SESSION_EXPIRED, "Session is expired");
				} else {
					throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
				}
			}

			// Check if session can be joined based on current state
			SessionState displayState = SessionUtils.getDisplayState(sessionStateDetails.getSessionState(), expiryTime);
			logger.info(" displayState : " + displayState);
			canSessionBeJoined(displayState, allowedJoinTime, endTime);

			// Update if the session state is scheduled.
			if (SessionState.SCHEDULED.equals(displayState)) {
				sessionStateDetails.setSessionState(SessionState.STARTED);
				logger.info("Session state set from scheduling to started");
				sessionStateDetailsDAO.update(sessionStateDetails, session);
			} else {
				logger.info("Session state not schedule : " + sessionId + " displayState:" + displayState);
			}

			logger.info("Response " + sessionStateDetails);
			transaction.commit();
			return sessionStateDetails;
		} catch (VException e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public SessionStateDetails endSession(Long sessionId, Long startTime, Long endTime, Long expiryTime)
			throws VException, NotFoundException {
		logger.info("Request - sessionId:" + sessionId + " expiryTime:" + expiryTime);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();
//                LockOptions lockOptions = new LockOptions(LockMode.PESSIMISTIC_WRITE);
//                Session.LockRequest lockRequest = session.buildLockRequest(lockOptions);
            logger.info("Lock obtained");
			// Fetch the session state details
			SessionStateDetails sessionStateDetails = getSessionStateDetailsBySessionId(sessionId, session,LockMode.PESSIMISTIC_WRITE);                        
			if (sessionStateDetails == null) {
				String errorMessage = "sessionStateDetails does not exists for the request id : " + sessionId;
				if (endTime >= (System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY)) {
					// Remove sentry error if the endTime is in the past
					logger.error(errorMessage);
				}
				logger.info(errorMessage);
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
			}
                        
//                        lockRequest.lock(sessionStateDetails);

			// Check if session can be joined based on current state
			SessionState displayState = SessionUtils.getDisplayState(sessionStateDetails.getSessionState(), expiryTime);
			logger.info(" displayState : " + displayState);

			// Update if the session state is scheduled.
			SessionState finalState = calculateFinalSessionState(displayState);
			logger.info(
					"Updating Session state from - " + sessionStateDetails.getSessionState() + " to - " + finalState);
			sessionStateDetails.setSessionState(finalState);
			sessionStateDetailsDAO.update(sessionStateDetails, session);
			logger.info("Response " + sessionStateDetails);
			transaction.commit();
			return sessionStateDetails;
		} catch (VException e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public SessionStateDetails markExpired(Long sessionId) throws VException {
		logger.info("Request - sessionId:" + sessionId);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();

			// Fetch the session state details
			SessionStateDetails sessionStateDetails = getSessionStateDetailsBySessionId(sessionId, session);
			if (sessionStateDetails == null) {
				String errorMessage = "sessionStateDetails does not exists for the request id : " + sessionId;
				logger.error(errorMessage);
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
			}

			if (!SessionState.SCHEDULED.equals(sessionStateDetails.getSessionState()) &&
					!SessionState.STARTED.equals(sessionStateDetails.getSessionState())
					&& !SessionState.EXPIRED.equals(sessionStateDetails.getSessionState())) {
				String errorMessage = "Invalid session state details to mark the seession as expired. state:" + sessionStateDetails.toString() + " id:" + sessionId;
				logger.error(errorMessage);
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
			}

			// Update if the session state is Expired.
			sessionStateDetails.setSessionState(SessionState.EXPIRED);
			sessionStateDetailsDAO.update(sessionStateDetails, session);
			logger.info("Response " + sessionStateDetails);
			transaction.commit();
			return sessionStateDetails;
		} catch (VException e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public SessionStateDetails markActive(Long sessionId, Long expiryTime) throws VException {
		logger.info("Request - sessionId:" + sessionId + " expiryTime:" + expiryTime);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();

			// Fetch the session state details
			SessionStateDetails sessionStateDetails = getSessionStateDetailsBySessionId(sessionId, session);
			if (sessionStateDetails == null) {
				String errorMessage = "sessionStateDetails does not exists for the request id : " + sessionId;
				logger.error(errorMessage);
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
			}

			// Check if session can be joined based on current state
			SessionState displayState = SessionUtils.getDisplayState(sessionStateDetails.getSessionState(), expiryTime);
			logger.info(" displayState : " + displayState);

			// Update if the session state is scheduled.
			SessionState nextState = calculateActiveSessionState(sessionStateDetails.getSessionState());
			logger.info(
					"Updating Session state from - " + sessionStateDetails.getSessionState() + " to - " + nextState);
			sessionStateDetails.setSessionState(nextState);
			sessionStateDetailsDAO.update(sessionStateDetails, session);
			logger.info("Response " + sessionStateDetails);
			transaction.commit();
			return sessionStateDetails;
		} catch (VException e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public void checkActiveAndStartedSessions(List<Long> sessionIds) throws VException {
		logger.info("Request - sessionIds:" + Arrays.toString(sessionIds.toArray()));
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {

			// Fetch the session state details
			List<SessionStateDetails> sessionStateDetails = sessionStateDetailsDAO
					.getActiveAndStartedSessions(sessionIds, session);
			if (!CollectionUtils.isEmpty(sessionStateDetails)) {
				String errorMessage = "Active session found : " + Arrays.toString(sessionStateDetails.toArray());
				// logger.error(errorMessage);
				throw new ConflictException(ErrorCode.SUBSCRIPTION_SESSION_ACTIVE, errorMessage);
			}
		} catch (VException ex) {
			logger.info("Exception occured with checkActiveSessions : " + ex.toString() + " sessionIds: "
					+ Arrays.toString(sessionIds.toArray()));
			throw ex;
		} finally {
			session.close();
		}
	}

	private void canSessionBeJoined(SessionState displayState, long allowedJoinTime, long endTime) throws VException {
		logger.info("SessionState " + displayState + " allowedJoinTime:" + allowedJoinTime + " endTime:" + endTime);
		Long currentTime = System.currentTimeMillis();

		switch (displayState) {
		case CANCELED:
		case ENDED:
		case FORFEITED:
			throw new ForbiddenException(ErrorCode.SESSION_NOT_ACTIVE,
					"Session is not active anymore. CurrentState : " + displayState);
		case EXPIRED:
			throw new ForbiddenException(ErrorCode.SESSION_EXPIRED, "Session is expired");
		case SCHEDULED:
		case STARTED:
		case ACTIVE:
			if (currentTime < allowedJoinTime) {
				throw new ForbiddenException(ErrorCode.SESSION_NOT_STARTED, "Session can not be joined now");
			} else if (currentTime > endTime) {
				throw new ForbiddenException(ErrorCode.SESSION_ALREADY_ENDED, "Session can not be joined now");
			}
			break;
		}
	}

	private SessionState calculateFinalSessionState(SessionState displayState) throws VException {
		switch (displayState) {
		case CANCELED:
			throw new BadRequestException(ErrorCode.SESSION_ALREADY_CANCELED, "Session already cancelled");
		case FORFEITED:
			throw new BadRequestException(ErrorCode.SESSION_ALREADY_ENDED, "Session already forfeited");
		case ENDED:
			throw new BadRequestException(ErrorCode.SESSION_ALREADY_ENDED, "Session already ended");
		case EXPIRED:
			throw new BadRequestException(ErrorCode.SESSION_EXPIRED, "Session is already expired");
		case SCHEDULED:
			return SessionState.CANCELED;
		case STARTED:
		case ACTIVE:
			return SessionState.ENDED;
		default:
			logger.error("Display state not found : " + displayState);
			throw new VException(ErrorCode.SERVICE_ERROR, "Display state not found");
		}
	}

	private SessionState calculateActiveSessionState(SessionState displayState) throws VException {
		switch (displayState) {
		case CANCELED:
		case FORFEITED:
		case ENDED:
		case EXPIRED:
			return SessionState.ENDED;
		case SCHEDULED:
		case STARTED:
			return SessionState.ACTIVE;
		case ACTIVE:
			throw new BadRequestException(ErrorCode.SESSION_ALREADY_ACTIVE, "Session already active");
		default:
			throw new VException(ErrorCode.SERVICE_ERROR, "Display state not found");
		}
	}

	public void migrateSessionStateDetails(List<com.vedantu.scheduling.dao.entity.Session> sessions, Integer size) {
		logger.info("Session id : " + sessions.size());
		int batchCount = 20;
		if (size != null) {
			batchCount = size;
		}

		for (int i = 0; i < sessions.size(); i += batchCount) {
			SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.getTransaction();
			try {
				transaction.begin();
				int start = i;
				int end = i + batchCount;
				for (int j = start; (j < end) && (j < sessions.size()); j++) {
					com.vedantu.scheduling.dao.entity.Session sessionEntry = sessions.get(j);
					SessionStateDetails sessionStateDetails = new SessionStateDetails(sessionEntry.getId());
					sessionStateDetails.setCreationTime(sessionEntry.getCreationTime());
					sessionStateDetails.setLastUpdatedTime(sessionEntry.getLastUpdated());

					SessionStateDetails result = sessionStateDetailsDAO.getBySessionId(sessionEntry.getId(), session);
					if (result == null) {
						sessionStateDetailsDAO.create(sessionStateDetails, session, sessionEntry.getCreatedBy());
						logger.info("Response sessionId:" + sessionEntry.getId() + " sessionDetials:"
								+ sessionStateDetails.getId());
					} else {
						logger.info("Already exists : " + result.getId());
					}
				}

				transaction.commit();
			} finally {
				session.close();
			}
		}
	}

	public void migrateOneSessionStateDetails(com.vedantu.scheduling.dao.entity.Session sessionEntry) {
		logger.info("Session id : " + sessionEntry.getId());

		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();
			SessionStateDetails sessionStateDetails = new SessionStateDetails(sessionEntry.getId());
			sessionStateDetails.setCreationTime(sessionEntry.getCreationTime());
			sessionStateDetails.setLastUpdatedTime(sessionEntry.getLastUpdated());

			SessionStateDetails result = sessionStateDetailsDAO.getBySessionId(sessionEntry.getId(), session);
			if (result == null) {
				sessionStateDetailsDAO.create(sessionStateDetails, session, sessionEntry.getCreatedBy());
				logger.info("Response sessionId:" + sessionEntry.getId() + " sessionDetials:"
						+ sessionStateDetails.getId());
			} else {
				logger.info("Already exists : " + result.getId());
			}

			transaction.commit();
		} finally {
			session.close();
		}
	}

	public void getStartedSessionStateDetails(long startTime, long endTime) {
		logger.info("startTime : " + startTime + " endTime:" + endTime);
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();

			List<SessionStateDetails> results = sessionStateDetailsDAO.getStartedSessionStateDetails(startTime, endTime, session);
			if (!CollectionUtils.isEmpty(results)) {
				Set<Long> sessionIds = new HashSet<Long>();
				for (SessionStateDetails sessionStateDetails : results) {
					logger.info("Entry :" + sessionStateDetails.toString());
					com.vedantu.scheduling.dao.entity.Session sessionEntry = sessionDAO.getById(sessionStateDetails.getId());
					sessionStateDetails.setSessionState(sessionEntry.getState());
					sessionStateDetailsDAO.update(sessionStateDetails, session);
				}
			}

			

			transaction.commit();
		} finally {
			session.close();
		}
	}
}