package com.vedantu.scheduling.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.serializers.CalendarEntryDAO;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.scheduling.utils.SessionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import java.util.HashMap;
import java.util.Map;

@Service
public class SessionCalendarManager {

	@Autowired
	private CalendarEntryDAO calendarEntryDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionCalendarManager.class);

	public void checkAndBlockCalendar(Session session) throws VException {
		logger.info("Request:" + session.toString());
		if (CollectionUtils.isEmpty(session.getStudentIds())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student ids are empty in session entry");
		}

		long sessionStartTime = session.getStartTime();
		long sessionEndTime = session.getEndTime();

		// Validate the session timings.
		if (session.getEndTime() < System.currentTimeMillis()) {
			throw new ForbiddenException(ErrorCode.SESSION_INVALID_TIME_PERIOD,
					"Cannot schedule sessions in past : " + session.toString());
		}

		// There is a possibility of Session start time being in past. In
		// instalearn, we try to book a session within next one minitue. If
		// start time is in the past, we block from the current time
		if (session.getStartTime() < System.currentTimeMillis()) {
			sessionStartTime = System.currentTimeMillis();
		}

		List<Long> dayStartTimes = SessionUtils.getDayStartTimes(session);
		List<CalendarEntry> modifiedCalendarEntries = new ArrayList<CalendarEntry>();
		for (long i : dayStartTimes) {
			int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(sessionStartTime, i);
			int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(sessionEndTime, i);
			if (slotStartIndex < 0) {
				slotStartIndex = 0;
			}

			if (slotEndIndex >= CommonCalendarUtils.getCalendarEntrySlotCount()) {
				slotEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
			}

			Query blockedEntriesquery = new Query();
			List<String> ids = SessionUtils.convertLongListToStringList(SessionUtils.getAttendeeIds(session));
			blockedEntriesquery.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
			blockedEntriesquery.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));
			blockedEntriesquery.addCriteria(Criteria.where(CalendarEntry.Constants.BOOKED)
					.in(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));

			List<CalendarEntry> results = calendarEntryDAO.runQuery(blockedEntriesquery, CalendarEntry.class);
			if (!CollectionUtils.isEmpty(results)) {
				throw new ConflictException(ErrorCode.USER_NOT_AVAILABLE,
						"Request slot is not available for the user id :" + results.get(0).getUserId());
			}

			// Fetch existing entries
			Query allExistingEntriesquery = new Query();
			allExistingEntriesquery.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
			allExistingEntriesquery.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));

			results = calendarEntryDAO.runQuery(allExistingEntriesquery, CalendarEntry.class);
			logger.info("Existing entries : " + results == null ? 0 : results.size());
			Map<String, CalendarEntry> entriesMap = getCalendarEntrySlugMap(results);
			for (String id : ids) {
				CalendarEntry entry = entriesMap.get(getCalendarEntryMapKey(id, i));
				if (entry == null) {
					entry = new CalendarEntry(id, i);
				}
				entry.addSlotEntries(CalendarEntrySlotState.SESSION, slotStartIndex, slotEndIndex);
				modifiedCalendarEntries.add(entry);
			}
		}

		// Commit the modified entries
		if (!CollectionUtils.isEmpty(modifiedCalendarEntries)) {
			for (CalendarEntry entry : modifiedCalendarEntries) {
				calendarEntryDAO.save(entry);
			}
		}
	}

	public List<Session> checkAndBlockCalendar(List<Session> sessions) {
		if (CollectionUtils.isEmpty(sessions)) {
			return sessions;
		}

		// TODO : Need to rewrite this. For high no of sessions, it wont make
		// sense to make queries equivalent to no of session
		List<Session> availableSessions = new ArrayList<Session>();
		for (Session session : sessions) {
			try {
				checkAndBlockCalendar(session);
				availableSessions.add(session);
			} catch (VException ex) {
				logger.info("Session entry not available : " + session.toString() + " ex: " + ex.toString());
			}
		}

		return availableSessions;
	}

	public void unblockCalendar(Session session) throws VException {
		logger.info("Request:" + session.toString());
		if (CollectionUtils.isEmpty(session.getStudentIds())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student ids are empty in session entry");
		}

		long sessionStartTime = session.getStartTime();
		long sessionEndTime = session.getEndTime();

		// Validate the session timings
		if (sessionEndTime < System.currentTimeMillis()) {
			logger.info("sessionEndTime in past : " + sessionEndTime);
			return;
		}

		// If sessionStartTime is in the past, pick system current time
		if (session.getStartTime() < System.currentTimeMillis()) {
			logger.info("Session start time is in the past : " + session.getStartTime());
			sessionStartTime = System.currentTimeMillis();
		}

		List<Long> dayStartTimes = SessionUtils.getDayStartTimes(session);
		List<CalendarEntry> modifiedCalendarEntries = new ArrayList<CalendarEntry>();
		for (long i : dayStartTimes) {
			int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(sessionStartTime, i);
			int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(sessionEndTime, i);
			if (slotStartIndex < 0) {
				slotStartIndex = 0;
			}

			if (slotEndIndex >= CommonCalendarUtils.getCalendarEntrySlotCount()) {
				slotEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
			}

			Query query = new Query();
			List<String> ids = SessionUtils.convertLongListToStringList(SessionUtils.getAttendeeIds(session));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));

			List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);

			for (CalendarEntry entry : results) {
				ids.remove(entry.getUserId());

				// Check if slots are blocked
				BitSet bitSet = entry.fetchBookedBitSet();
				if (bitSet.nextClearBit(slotStartIndex) <= slotEndIndex) {
					// TODO : Instalearn might give a false alarm here.
					logger.info("Found data inconsistency during unblocking calendar. Calendar is not blocked : "
							+ entry.toString());
				}

				// Remove the blocked entries
				entry.removeSlotEntries(CalendarEntrySlotState.SESSION, slotStartIndex, slotEndIndex);
				modifiedCalendarEntries.add(entry);
			}

			if (!ids.isEmpty()) {
				logger.info("Identified an unblock request on a free slot for user id : "
						+ Arrays.toString(ids.toArray()) + " request : " + session.toString());
			}
		}

		// Commit the modified entries
		if (!CollectionUtils.isEmpty(modifiedCalendarEntries)) {
			for (CalendarEntry entry : modifiedCalendarEntries) {
				calendarEntryDAO.save(entry);
			}
		}
	}

	public void rescheduleSlot(Session session, Long startTime) throws VException {
		// Capture session timings before reschedule as we are doing the
		// blocking first and then unblocking the calendar.
		long startTimePreUpdate = session.getStartTime();
		long endTimePreUpdate = session.getEndTime();

		// Update the session with the new timings and try to block the
		// calendar.
		session.setEndTime(startTime + (session.getEndTime() - session.getStartTime()));
		session.setStartTime(startTime);
		checkAndBlockCalendar(session);

		// Revert to the session old timings and unblock the previous slot
		session.setStartTime(startTimePreUpdate);
		session.setEndTime(endTimePreUpdate);
		unblockCalendar(session);
	}

	private static Map<String, CalendarEntry> getCalendarEntrySlugMap(List<CalendarEntry> entries) {
		Map<String, CalendarEntry> entriesMap = new HashMap<String, CalendarEntry>();
		if (!CollectionUtils.isEmpty(entries)) {
			for (CalendarEntry entry : entries) {
				entriesMap.put(getCalendarEntryMapKey(entry), entry);
			}
		}
		return entriesMap;
	}

	private static String getCalendarEntryMapKey(CalendarEntry entry) {
		return getCalendarEntryMapKey(entry.getUserId(), entry.getDayStartTime());
	}

	private static String getCalendarEntryMapKey(String userId, long dayStartTime) {
		return userId + ":" + dayStartTime;
	}
}
