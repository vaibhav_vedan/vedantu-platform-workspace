package com.vedantu.scheduling.managers;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Data
public class GroupOfUserEmail {
    private List<String> userIds;
    private String sessionId;
    private HashMap<String, Object> bodyScopeOfEmail;
    private long expectedAt;
}
