package com.vedantu.scheduling.managers;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.util.LogFactory;

@Service("gttProScheduleManager")
public class GTTProScheduleManager extends GTTScheduleManager {
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTTProScheduleManager.class);

	private String[] authAccessArray = { "gttProSeat1", "gttProSeat2", "gttProSeat3" };

	protected static Map<String, String> overwriteAccessKeys = Collections.unmodifiableMap(new HashMap<String, String>() {

		private static final long serialVersionUID = 4675157667161370845L;
		{
			/*
			 * cc01lpU5Syjii9cQxUx2j6PgKeKa FMsfunrWYexi2lZDGQwnfkgMmOzs
			 * A3dCcY36fjXZuaPLCQk7twUPNTb8
			 */
			put("gttProSeat1", "lUOmx9eQKa1S02HvjUoa7y1bDtuU"); // gttSeatPro1
			put("gttProSeat2", "Cp0KFWUT0BbBTtf8sk068HF3x0WL"); // gttSeatPro2
			put("gttProSeat3", "TSff9J14Kli3x8At2hQgXFOTPchy"); // gttSeatPro3
		}
	});

	protected static Map<String, String> organizerKeys = Collections.unmodifiableMap(new HashMap<String, String>() {
		private static final long serialVersionUID = 2357808695344388664L;

		{
			/*
			 * 338783005116884236 7585387066858300684 4674740094600373516
			 */
			put("gttProSeat1", "338783005116884236"); // gttSeatPro1
			put("gttProSeat2", "7585387066858300684"); // gttSeatPro2
			put("gttProSeat3", "4674740094600373516"); // gttSeatPro3
		}
	});

	private List<String> authAccessList = Arrays.asList(authAccessArray);

	@Override
	public int getAccessArrayLength() {
		return authAccessArray.length;
	}

	@Override
	public String getAccessToken(int slot) {
		if (slot != -1 && slot < authAccessList.size()) {
			return authAccessList.get(slot);
		}
		return null;
	}

	@Override
	public int getAccessTokenSlot(String accessToken) {
		int returnVal = -1;
		if (!StringUtils.isEmpty(accessToken)) {
			returnVal = authAccessList.indexOf(accessToken);
		}
		return returnVal;
	}

	@Override
	public String getPostOverwriteAccessToken(String accessToken) {
		if (overwriteAccessKeys.containsKey(accessToken)) {
			return overwriteAccessKeys.get(accessToken);
		} else {
			return accessToken;
		}
	}

	@Override
	public BitSet getScheduleBitSet(Schedule schedule) {
		return schedule.getGttProScheduleBitSet();
	}

	@Override
	public String getOrganizerKeyForToken(String accessToken) {
		return organizerKeys.get(accessToken);
	}
}
