package com.vedantu.scheduling.managers.earlylearning;

import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.Interfaces.ITeacherProficiencyManager;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.OverBookingDAO;
import com.vedantu.scheduling.pojo.SlotBookingCount;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.scheduling.response.earlylearning.AllottedTeacherRes;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import nl.basjes.shaded.com.google.common.collect.Sets;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AnalysisManager {

    @Autowired
    protected ITeacherProficiencyManager teacherProficiencyManager;

    @Autowired
    private OverBookingDAO overBookingDAO;

    @Autowired
    private OTFSessionDAO otfSessionDAO;

    protected Logger logger = LogFactory.getLogger(AnalysisManager.class);

    public List<String> getAvailableTeachersForSlot(ProficiencyAnalyticsRequest request) throws VException {

        ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
                .getTeacherProficiencies(request.getEarlyLearningCourseType());
        if (Objects.isNull(proficiencyIds)) {
            return null;
        }
        return teacherProficiencyManager.getAvailableTeacherForSlot(request, proficiencyIds);
    }

    public Map<Long, Map<ProficiencyType, Long>> getClosedBookings(Long startTime, Long endTime,
                                                                                   SessionLabel courseType) {
        List<SlotBookingCount> slotBookingCount = overBookingDAO.getOverBookingAggregationWithSlotTime(startTime, endTime, courseType);
        Map<Long, Map<ProficiencyType, Long>> slotBookingMapping = slotBookingCount.stream().filter(Objects::nonNull)
                .collect(Collectors.groupingBy(SlotBookingCount::getSlotStartTime,
                        Collectors.toMap(SlotBookingCount::getProficiencyType, SlotBookingCount::getCount)));
        return slotBookingMapping;
    }

    public List<AllottedTeacherRes> getAllottedTeacherForSlot(ProficiencyAnalyticsRequest request) throws VException {

        List<OverBooking> overBookings;
        if(StringUtils.isNotEmpty(request.getPresenter())) {
            String studentIdForSession;
            studentIdForSession = otfSessionDAO.getActiveELSessionByPresenter(request.getStartTime(), request.getEndTime(), request.getPresenter(),
                    request.getEarlyLearningCourseType()).getAttendees().get(0);
            request.setStudentId(studentIdForSession);
        }
        overBookings = overBookingDAO.assignedTeacherForBooking(request.getStartTime(), request.getEndTime(),
                request.getEarlyLearningCourseType(), request.getStart(), Math.min(request.getLimit(), 20),request.getStudentId() ,request.getSelectedProficiencyType());

        ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
                .getTeacherProficiencies(request.getEarlyLearningCourseType());
        List<String> sessionIds = Optional.ofNullable(overBookings).orElseGet(ArrayList::new).stream().map(OverBooking::getSessionId).filter(Objects::nonNull).collect(Collectors.toList());
        Map<String, String> sessionPresenterMap = otfSessionDAO.getByIdsWithProjection(Sets.newHashSet(sessionIds), Arrays.asList(OTFSession.Constants.PRESENTER),0, sessionIds.size())
                .stream()
                .collect(Collectors.toMap(
                        OTFSession:: getId,
                        OTFSession:: getPresenter
                ));
        List<AllottedTeacherRes> allottedTeacherResList = new ArrayList<>();
        overBookings.forEach( overBooking -> {
                    String presenter = sessionPresenterMap.get(overBooking.getSessionId());
                    AllottedTeacherRes allottedTeacher = AllottedTeacherRes.builder().bookingId(overBooking.getId())
                            .studentId(overBooking.getStudentId()).presenter(presenter)
                            .proficiencyType(proficiencyIds.getTeacherProficiencyType(presenter))
                            .build();
                    allottedTeacherResList.add(allottedTeacher);
                }
        );
        return allottedTeacherResList;
    }
}
