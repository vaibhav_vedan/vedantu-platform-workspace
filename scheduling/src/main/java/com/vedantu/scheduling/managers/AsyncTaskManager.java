package com.vedantu.scheduling.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentUpdatePojo;
import com.vedantu.onetofew.request.GetOTFSessionsListReq;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.serializers.GTTAttendeeDetailsDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.pojo.MarkCalendarBatchPojo;
import com.vedantu.scheduling.pojo.MarkCalendarSessionPojo;
import com.vedantu.scheduling.pojo.StudentMarkCalendarPojo;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.response.BasicRes;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.*;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.threadutil.CachedThreadPoolUtility;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 * @author pranavm
 */
@Service
public class AsyncTaskManager {

    private final int CAL_SESSION_PARTITION_SIZE = 25;
    private final int CAL_STUDENTS_PARTION_SIZE = 100;
    private static final int MAX_FETCH_SIZE = 200;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AsyncTaskManager asyncTaskManager;

    @Autowired
    private CalendarEntryManager calendarEntryManager;

    @Autowired
    private OTFSessionDAO oTFSessionDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private OTFSessionManager otfSessionManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private CachedThreadPoolUtility cachedThreadPoolUtility;

    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AsyncTaskManager.class);
    private String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
    private static Gson gson = new Gson();


    public void updateAsyncEnrollmentTask(EnrollmentUpdatePojo pojoReq) throws BadRequestException {
        pojoReq.verify();
        Map<String, Object> payload = new HashMap<>();
        payload.put("newEnrollment", pojoReq.getNewEnrollment());
        if (pojoReq.getPreviousEnrollment() != null) {
            payload.put("previousEnrollment", pojoReq.getPreviousEnrollment());
        }
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_ENROLLMENT_EVENTS, payload);
        asyncTaskFactory.executeTask(params);

    }

    public void createGttAttendee(EnrollmentUpdatePojo pojoReq) throws VException {
        pojoReq.verify();
        asyncTaskManager.createGTTAttendeeForEnrollment(null, pojoReq.getNewEnrollment());
    }

    public void createAsyncGttAttendeeTask(EnrollmentUpdatePojo pojoReq) throws BadRequestException {
        pojoReq.verify();
        Map<String, Object> payload = new HashMap<>();
        payload.put("newEnrollment", pojoReq.getNewEnrollment());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_GTT_ATTENDEES_NEW_ENROLLMENT, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void markCalendarForEnrollment(EnrollmentPojo previousEnrollment, EnrollmentPojo newEnrollment) {

        if (newEnrollment != null) {
            if (previousEnrollment != null) {
                if (!previousEnrollment.getBatchId().equals(newEnrollment.getBatchId())) {
                    GetOTFSessionsListReq req = new GetOTFSessionsListReq();
                    req.setBatchId(previousEnrollment.getBatchId());
                    req.setFromTime(System.currentTimeMillis());
                    List<CalendarEntryReq> umark = getCalendarEntryRequests(Arrays.asList(previousEnrollment.getUserId()),
                            previousEnrollment.getBatchId(), getOTFSessions(req), CalendarReferenceType.OTF_BATCH);
                    updateCalendarForRequest(umark, false);

                }
            }

            GetOTFSessionsListReq req = new GetOTFSessionsListReq();
            req.setBatchId(newEnrollment.getBatchId());
            req.setFromTime(System.currentTimeMillis());
            List<CalendarEntryReq> mark = getCalendarEntryRequests(Arrays.asList(newEnrollment.getUserId()),
                    newEnrollment.getBatchId(), getOTFSessions(req), CalendarReferenceType.OTF_BATCH);

            if (EntityStatus.ACTIVE.equals(newEnrollment.getStatus())) {
                updateCalendarForRequest(mark, true);
            } else {
                updateCalendarForRequest(mark, false);
            }

        }

    }


    public List<CalendarEntryReq> getCalendarEntryRequests(List<String> userIds, String refId, List<OTFSession> sessions,
                                                           CalendarReferenceType refType) {
        List<SessionSlot> slots = getSessionSlots(sessions);
        List<CalendarEntryReq> request = new ArrayList<>();
        for (String userId : userIds) {
            for (SessionSlot sessionSlot : slots) {
                CalendarEntryReq calendarEntryReq = new CalendarEntryReq((userId), sessionSlot.getStartTime(),
                        sessionSlot.getEndTime(), CalendarEntrySlotState.SESSION, refType, refId);
                request.add(calendarEntryReq);
            }
        }
        return request;
    }

    public void updateCalendarForRequest(List<CalendarEntryReq> requests, boolean mark) {
        if (ArrayUtils.isNotEmpty(requests)) {
            if (mark) {
                calendarEntryManager.upsertCalendarEntry(requests);
            } else {
                for (CalendarEntryReq req : requests) {
                    try {
                        unmarkCalendarEntries(req);
                    } catch (NotFoundException e) {
                        logger.info("calendar not found for" + req);
                    }

                }

            }
        }

    }

    public void unmarkCalendarEntries(CalendarEntryReq calendarEntryReq) throws NotFoundException {
        calendarEntryManager.removeCalendarEntrySlotsUsingUserId(calendarEntryReq);
    }

    private static List<SessionSlot> getSessionSlots(List<OTFSession> sessions) {
        List<SessionSlot> sessionSlots = new ArrayList<>();

        if (!CollectionUtils.isEmpty(sessions)) {
            for (OTFSession session : sessions) {
                if (session != null) {
                    sessionSlots.add(new SessionSlot(session.getStartTime(), session.getEndTime()));
                }
            }
        }
        return sessionSlots;
    }

    // gtts for new enrolments are created here
    public void createGTTAttendeeForEnrollment(EnrollmentPojo previousEnrollment, EnrollmentPojo newEnrollment) throws InternalServerErrorException, VException {
        if (newEnrollment != null) {
            if (previousEnrollment != null && !previousEnrollment.getBatchId().equalsIgnoreCase(newEnrollment.getBatchId())) {
                return;
            }
            if (EntityStatus.ACTIVE.equals(newEnrollment.getStatus())) {
                logger.info("inside createGTTAttendeeForEnrollment");
                GetOTFSessionsListReq req = new GetOTFSessionsListReq();
                req.setBatchId(newEnrollment.getBatchId());
                req.setTillTime(System.currentTimeMillis());
                OTFSession session = getLastEnrolledSession(newEnrollment.getUserId(), newEnrollment.getId());
                AttendeeContext context = AttendeeContext.LATE_JOIN;
                if (session != null && session.getEndTime() != null) {
                    req.setFromTime(session.getEndTime());
                    context = AttendeeContext.STATUS_CHANGE;
                }
                // createGTTAttendeeDetails(getOTFSessions(req), newEnrollment.getUserId(), newEnrollment.getId(), context);

                List<OTFSession> sessionsForEnrollment = getOTFSessions(req);
                if (!CollectionUtils.isEmpty(sessionsForEnrollment)) {
                    List<List<OTFSession>> sessionPartitions = Lists.partition(sessionsForEnrollment, 400);
                    for (List<OTFSession> sessionPartition : sessionPartitions) {
                        createGTTAttendees(sessionPartition, newEnrollment.getUserId(), newEnrollment, context);
                    }
                }

                GetOTFSessionsListReq req2 = new GetOTFSessionsListReq();
                req2.setBatchId(newEnrollment.getBatchId());
                req2.setFromTime(System.currentTimeMillis());
                req2.setTillTime(System.currentTimeMillis()+ 86400000L);

                req2.setSessionURLExists(Boolean.TRUE);
                List<OTFSession> sessions = getOTFSessions(req2);
                if (ArrayUtils.isNotEmpty(sessions)) {
                    for (OTFSession iterSession : sessions) {
                        logger.info("inside createGTTAttendeeForEnrollment enrolment");
                        otfSessionManager.registerAttendees(iterSession, newEnrollment.getUserId(), Collections.singletonList(newEnrollment));
                    }

                }
//                createGTTAttendeeDetails(getOTFSessions(req2), newEnrollment.getUserId(), newEnrollment.getId(), null);
            }
        }
    }

    public OTFSession getLastEnrolledSession(String userId, String enrollId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENROLLMENT_ID).is(enrollId));
        query.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.CREATION_TIME));
        gttAttendeeDetailsDAO.setFetchParameters(query, 0, 1);

        List<GTTAttendeeDetails> attendees = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        if (ArrayUtils.isNotEmpty(attendees)) {
            GTTAttendeeDetails attendee = attendees.get(0);
            return oTFSessionDAO.getById(attendee.getSessionId());

        }
        return null;
    }

    public List<GTTAttendeeDetails> createGTTAttendeeDetails(List<OTFSession> sessions, String userId, String enrollmentId, AttendeeContext context) throws InternalServerErrorException {
        List<GTTAttendeeDetails> enrolled = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSession session : sessions) {
                GTTAttendeeDetails tempGTTAttendee = new GTTAttendeeDetails();
                tempGTTAttendee.setSessionId(session.getId());
                tempGTTAttendee.setTraningId(session.getMeetingId());
                tempGTTAttendee.setSessionStartTime(session.getStartTime());
                tempGTTAttendee.setSessionEndTime(session.getEndTime());
                //String orgToken = otfSessionManager.fetchOrganizerAccessTokenFromAccessToken(session);
                String orgToken = session.getOrganizerAccessToken();
                tempGTTAttendee.setOrganizerAccessToken(orgToken);
                tempGTTAttendee.setUserId(userId);
                tempGTTAttendee.setEnrollmentId(enrollmentId);
                if (context != null) {
                    tempGTTAttendee.setAttendeeType(context);
                }
                enrolled.add(tempGTTAttendee);
            }
            gttAttendeeDetailsDAO.insertAllEntities(enrolled, GTTAttendeeDetails.class.getSimpleName());
        }
        return enrolled;
    }

    // creating gtts for one user
    public void createGTTAttendees(List<OTFSession> sessions, String userId, EnrollmentPojo enrollmentPojo, AttendeeContext context) {
        if (CollectionUtils.isEmpty(sessions)) {
            return;
        }

        Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();
        sessions.forEach(s -> attendeeMap.put(s.getId(), null));

        int start = 0;
        List<String> includeFields = Arrays.asList(GTTAttendeeDetails.Constants._ID, GTTAttendeeDetails.Constants.ENROLLMENT_INFOS,
                GTTAttendeeDetails.Constants.SESSION_ID);

        while (true) {
            List<GTTAttendeeDetails> fetchedEntries = gttAttendeeDetailsDAO.getAttendeeDetailForSessionList(userId, attendeeMap.keySet(),
                    includeFields, start, MAX_FETCH_SIZE);
            if (CollectionUtils.isEmpty(fetchedEntries) || fetchedEntries.size() > sessions.size()) {
                break;
            }
            fetchedEntries.forEach(fe -> attendeeMap.put(fe.getSessionId(), fe));
            start += MAX_FETCH_SIZE;
        }

        // accommodate multiple enrolments for the same combination of sessionId and userId
        List<Document> updates = new LinkedList<>();
        List<Document> queries = new LinkedList<>();
        List<GTTAttendeeDetails> inserts = new ArrayList<>();

        // TODO use bifunction and apply to generate updates and inserts - use builder to construct new gtt objects
        for (OTFSession session :  sessions) {
            GTTAttendeeDetails attendee = attendeeMap.get(session.getId());
            if (Objects.isNull(attendee)) {
                // insert new
                attendee = new GTTAttendeeDetails();
                attendee.setSessionId(session.getId());
                attendee.setTraningId(session.getMeetingId());
                attendee.setSessionStartTime(session.getStartTime());
                attendee.setSessionEndTime(session.getEndTime());
                attendee.setOrganizerAccessToken(session.getOrganizerAccessToken());
                attendee.setUserId(userId);
                attendee.setEnrollmentId(enrollmentPojo.getId());
                attendee.addEnrollmentInfo(enrollmentPojo);
                if (context != null) {
                    attendee.setAttendeeType(context);
                }
                inserts.add(attendee);
            } else {
                // push enrolment id to existing gtt's set of enrols
                attendee.addEnrollmentInfo(enrollmentPojo);
                Document query = new Document();
                query.append(GTTAttendeeDetails.Constants._ID, new ObjectId(attendee.getId()));
                queries.add(query);

                Document updateDocument = new Document();
                Document setDocument = new Document();
                setDocument.append(GTTAttendeeDetails.Constants.ENROLLMENT_INFOS, attendee.getEnrollmentInfos());
                if (context != null) {
                    setDocument.append(GTTAttendeeDetails.Constants.ATTENDEE_TYPE, context.name());
                }
                updateDocument.append(AbstractMongoEntity.Constants.UPDATE_SET_PARAM,setDocument);
                updates.add(updateDocument);
            }

        }

        if (!CollectionUtils.isEmpty(inserts)) {
            logger.info("bulk inserting gtt attendees : {}", inserts.size());
            gttAttendeeDetailsManager.queueAttendeesAsBulk(inserts);
//            long insertedCount = gttAttendeeDetailsDAO.bulkInsert(inserts, GTTAttendeeDetails.class, null);
//            logger.info("bulk insert count : {}", insertedCount);
        }

        if (!CollectionUtils.isEmpty(updates)) {
            logger.info("bulk inserting gtt attendees : {}", inserts.size());
            long modifiedCount = gttAttendeeDetailsDAO.bulkUpdate(queries, updates, false, GTTAttendeeDetails.class, null);
            logger.info("bulk insert count : {}", modifiedCount);
        }

    }

    public List<OTFSession> getOTFSessions(GetOTFSessionsListReq req) {
        if (StringUtils.isEmpty(req.getBatchId())) {
            throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, "batch id is mandatory");
        }

        Query sessionQuery = new Query();
        if (req.getFromTime() != null && req.getFromTime() > 0l) {
            if (req.getTillTime() != null && req.getTillTime() > 0l) {
                sessionQuery.addCriteria(
                        Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()).lt(req.getTillTime()));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()));
            }
        } else if (req.getTillTime() != null && req.getTillTime() > 0l) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(req.getTillTime()));
        }

        if (StringUtils.isNotEmpty(req.getBatchId())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(req.getBatchId()));
        }

        if (req.getTeacherId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(req.getTeacherId().toString()));
        }

        if (req.getBoardId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(req.getBoardId()));
        }
        if (ArrayUtils.isNotEmpty(req.getMeetingIds())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.MEETING_ID).in(req.getMeetingIds()));
        }
        if (ArrayUtils.isNotEmpty(req.getStates())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(req.getStates()));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(SessionState.SCHEDULED));
        }
        if (req.getSessionURLExists() != null) {

            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.MEETING_ID).exists(req.getSessionURLExists()));

        }
        sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.END_TIME));
        List<OTFSession> sessions = oTFSessionDAO.runQuery(sessionQuery, OTFSession.class);
        return sessions;
    }

    //fifo here may not work as expected in marking students calendar as there are some async tasks are involved
    public BasicRes updateCalendarForAddedSessions(List<OTFSession> addedSessions, boolean mark, String fifoGrpId) throws VException {
        logger.info("markCalendarForAddedSessions Started");
        if (CollectionUtils.isEmpty(addedSessions)) {
            return new BasicRes();
        }

        List<MarkCalendarSessionPojo> sessions = addedSessions
                .stream()
                .filter(Objects::nonNull)
                .filter(s -> s.getStartTime() != null && s.getEndTime() != null)
                .map(s -> mapper.map(s, MarkCalendarSessionPojo.class))
                .collect(Collectors.toList());
        logger.info("SESSIONS " + sessions);
        updateTeacherCalendar(sessions, mark, fifoGrpId);
        //updateStudentCalendar(sessions, mark, fifoGrpId);

        logger.info("markCalendarForAddedSessions Finished");
        return new BasicRes();
    }

    private void updateStudentCalendar(List<MarkCalendarSessionPojo> sessions, boolean mark, String fifoGrpId) throws VException {
        // mark student calendar entries
        logger.info("Marking Student Calendar");

        Map<String, List<MarkCalendarSessionPojo>> map = new HashMap<>();
        for (MarkCalendarSessionPojo session : sessions) {
            Set<String> batchIds = CollectionUtils.defaultIfNull(session.getBatchIds(), HashSet::new);
            for (String batchId : batchIds) {
                List<MarkCalendarSessionPojo> pojos = CollectionUtils.putIfAbsentAndGet(map, batchId, ArrayList::new);
                pojos.add(session);
            }
        }

        logger.info("BATCH SESSION MAP " + map);


        for (String batchId : map.keySet()) {
            List<MarkCalendarSessionPojo> value = map.get(batchId);
            List<List<MarkCalendarSessionPojo>> partition = Lists.partition(value, CAL_SESSION_PARTITION_SIZE);
            logger.info("Partition Size: " + partition.size());
            for (List<MarkCalendarSessionPojo> list : partition) {
                try {
                    MarkCalendarBatchPojo pojo = new MarkCalendarBatchPojo();
                    pojo.setBatchId(batchId);
                    pojo.setSessions(list);
                    pojo.setMark(mark);
                    pojo.setFifoGrpId(fifoGrpId);
                    logger.info("To SQS : " + pojo);
                    if(StringUtils.isEmpty(fifoGrpId)){
                        awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_OPS, SQSMessageType.MARK_CALENDAR_BATCH_TASK, gson.toJson(pojo));
                    }else{
                        awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_FIFO_OPS, SQSMessageType.MARK_CALENDAR_BATCH_TASK, gson.toJson(pojo), fifoGrpId);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        logger.info("Marking Student Calendar Done");
    }

    public void markStudentCalendarForBatch(MarkCalendarBatchPojo pojo) throws VException {
        logger.info("MARKING CALENDAR FOR BATCH AND SESSION " + pojo);

        int start = 0, limit = CAL_STUDENTS_PARTION_SIZE;
        do {
            String url = subscriptionEndPoint + "enroll/getEnrolledStudentsInBatch?batchId=" + pojo.getBatchId()
                    + "&start=" + start + "&size=" + limit;
            logger.info("URL = " + url);
            ClientResponse response = WebUtils.INSTANCE.doCall(url,
                    HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            String jsonString = response.getEntity(String.class);


            final List<String> studentIds = gson.fromJson(jsonString, new TypeToken<List<String>>() {
            }.getType());
            if (CollectionUtils.isEmpty(studentIds)) {
                break;
            }
            start += limit;

            cachedThreadPoolUtility.execute(() -> markStudentCalendarForSessionSendToSqs(pojo.getBatchId(),
                    pojo.getSessions(), studentIds, pojo.isMark(), pojo.getFifoGrpId()));
        } while (true);
        logger.info("MARKING CALENDAR FOR BATCH AND SESSION FINISHED");
    }

    private void markStudentCalendarForSessionSendToSqs(String batchId, List<MarkCalendarSessionPojo> sessions,
                                                        List<String> studentIds, boolean mark, String fifoGrpId) {
        StudentMarkCalendarPojo pojo = new StudentMarkCalendarPojo();
        pojo.setBatchId(batchId);
        pojo.setSessions(sessions);
        pojo.setStudentIds(studentIds);
        pojo.setMark(mark);
        pojo.setFifoGrpId(fifoGrpId);
        logger.info("SQS SESSION TASK - " + pojo);
        if(StringUtils.isEmpty(fifoGrpId)){
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_OPS, SQSMessageType.MARK_CALENDAR_SESSION_TASK, gson.toJson(pojo));
        } else {
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_FIFO_OPS, SQSMessageType.MARK_CALENDAR_SESSION_TASK, gson.toJson(pojo), fifoGrpId);
        }
        
    }

    public void markStudentCalendarForSession(StudentMarkCalendarPojo pojo) {

        logger.info("MARKING FOR STUDENT SESSIONS " + pojo);
        final String batchId = pojo.getBatchId();
        List<MarkCalendarSessionPojo> sessions = pojo.getSessions();
        List<String> studentIds = pojo.getStudentIds();
        boolean mark=pojo.isMark();
        String fifoGrpId=pojo.getFifoGrpId();
        
        
        logger.info("Marking Calendar For Batch " + batchId + " for students " + studentIds);
        logger.info("Sessions " + sessions);
        // init with common fields
        BiFunction<MarkCalendarSessionPojo, String, CalendarEntryReq> calendarEntryReqBiFunction = (session, userId) -> {
            CalendarEntryReq req = new CalendarEntryReq();
            req.setState(CalendarEntrySlotState.SESSION);
            req.setReferenceType(CalendarReferenceType.OTF_BATCH);
            req.setStartTime(session.getStartTime());
            req.setEndTime(session.getEndTime());
            req.setUserId(userId);
            req.setReferenceId(batchId);
            return req;
        };

        logger.info("SESSIONS SIZE " + sessions.size() + " <= " + CAL_SESSION_PARTITION_SIZE);
        logger.info("STUDENTS SIZE " + studentIds.size() + " <= " + CAL_STUDENTS_PARTION_SIZE);

        for (MarkCalendarSessionPojo session : sessions) {
            Map<String, Object> payload = new HashMap<>();
            List<CalendarEntryReq> reqs = studentIds.stream()
                    .map(e -> calendarEntryReqBiFunction.apply(session, e))
                    .collect(Collectors.toList());
            logger.info("Req: " + reqs);
            payload.put("req", reqs);
            payload.put("mark", mark);
            if (StringUtils.isEmpty(fifoGrpId)) {
                awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload));
            } else {
                awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS_FIFO, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload), fifoGrpId);
            }
        }
        logger.info("MARKING FOR STUDENT SESSIONS FINISHED");
    }

    private void updateTeacherCalendar(List<MarkCalendarSessionPojo> sessions, boolean mark, String fifoGrpId) {
        logger.info("Marking Teacher Calendar");
        // mark teacher calendar entries
        List<CalendarEntryReq> teacherCalendarEntryReqs = sessions.stream()
                .filter(e -> StringUtils.isNotEmpty(e.getPresenter()))
                .map(e -> new CalendarEntryReq(e.getPresenter(), e.getStartTime(), e.getEndTime(),
                        CalendarEntrySlotState.SESSION, CalendarReferenceType.OTF_BATCH,
                        Optional.ofNullable(e.getPrimaryBatchId()).orElse(e.getBatchIds()
                                .stream().findFirst().orElse(null))))
                .collect(Collectors.toList());


        Map<String, Object> payload = new HashMap<>();
        payload.put("req", teacherCalendarEntryReqs);
        payload.put("mark", mark);
        logger.info("Teacher Params " + payload);
        if (StringUtils.isEmpty(fifoGrpId)) {
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload));
        } else {
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS_FIFO, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload), fifoGrpId);
        }

        logger.info("calendar entries for teacher marked");
    }

    
    //TODO remove the attendees field itself from otfsession table
    public void addAttendeesToSessionFromBatchIds(List<String> batchIds, String sessionId) throws VException {
        Set<String> attendees = new HashSet<>();
        for (String batchId : batchIds) {
            int start = 0, limit = 1000;
            do {
                String url = subscriptionEndPoint + "enroll/getEnrolledStudentsInBatch?batchId=" + batchId
                        + "&start=" + start + "&size=" + limit;
                logger.info("URL = " + url);
                ClientResponse response = WebUtils.INSTANCE.doCall(url,
                        HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                String jsonString = response.getEntity(String.class);

                final List<String> studentIds = gson.fromJson(jsonString, new TypeToken<List<String>>() {
                }.getType());
                attendees.addAll(studentIds);
                if (CollectionUtils.isEmpty(studentIds)) {
                    break;
                }
                start += limit;
            } while (true);
        }

        OTFSession session = oTFSessionDAO.getById(sessionId);
        session.setAttendees(new ArrayList<>(attendees));
        oTFSessionDAO.save(session);
    }
}

