package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.entity.CalendarEvent;
import com.vedantu.scheduling.dao.serializers.CalendarEntryDAO;
import com.vedantu.scheduling.dao.serializers.CalendarEventDAO;
import com.vedantu.scheduling.pojo.BitSetEntry;
import com.vedantu.scheduling.pojo.BitSetUtils;
import com.vedantu.scheduling.pojo.CalendarEventDetails;
import com.vedantu.scheduling.pojo.CalendarEventState;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.RetryCalendarEventReq;
import com.vedantu.scheduling.request.CancelCalendarEventReq;
import com.vedantu.scheduling.request.GetCalendarEventReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;

@Service
public class CalendarEventManager {

	@Autowired
	private CalendarEventDAO calendarEventDAO;

	@Autowired
	private CalendarUtilsManager calendarUtilsManager;

	@Autowired
	private CalendarEntryDAO calendarEntryDAO;


	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEventManager.class);

	public void updateCalendarEvent(CalendarEvent calendarEvent) {
		logger.info("updateCalendarEventReq : " + calendarEvent.toString());
		calendarEventDAO.save(calendarEvent);
		logger.info("updateCalendarEventRes : " + calendarEvent.toString());
	}

	public List<CalendarEvent> getAvailabilityRemainingEvents(String userId) {
		logger.info("getAvailabilityRemainingEventsReq - userId " + userId);
		List<CalendarEventState> states = new ArrayList<CalendarEventState>();
		states.add(CalendarEventState.PENDING);
		states.add(CalendarEventState.PROCESSING);
		return getAvailabilityCalendarEvents(userId, states);
	}

	public List<CalendarEvent> getAvailabilityCalendarEvents(String userId, List<CalendarEventState> states) {
		logger.info("getAvailabilityCalendarEventsReq - userId " + userId + " states : "
				+ Arrays.toString(states.toArray()));
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEvent.Constants.STATE).in(states));
		query.addCriteria(Criteria.where(CalendarEvent.Constants.USER_ID).is(userId));
		query.addCriteria(Criteria.where(CalendarEvent.Constants.SLOT_STATE).is(CalendarEntrySlotState.AVAILABLE));

		List<CalendarEvent> results = calendarEventDAO.runQuery(query, CalendarEvent.class);
		logger.info("getAvailabilityCalendarEventsRes - result count " + (results == null ? 0 : results.size()));
		return results;
	}

	public List<CalendarEvent> getCalendarEvents(GetCalendarEventReq req) {
		logger.info("getCalendarEvents - " + req.toString());
		Query query = new Query();
		if (!StringUtils.isEmpty(req.getUserIds())) {
			String[] userIdArray = StringUtils.commaDelimitedListToStringArray(req.getUserIds());
			if (userIdArray != null && userIdArray.length > 0) {
				query.addCriteria(Criteria.where(CalendarEvent.Constants.USER_ID).in(Arrays.asList(userIdArray)));
			}
		}

		if (!StringUtils.isEmpty(req.getEventStates())) {
			String[] eventStates = StringUtils.commaDelimitedListToStringArray(req.getEventStates());
			if (eventStates != null && eventStates.length > 0) {
				query.addCriteria(Criteria.where(CalendarEvent.Constants.STATE).in(Arrays.asList(eventStates)));
			}
		}

		if (req.getCreationStartTime() != null && req.getCreationStartTime() > 0l) {
			Long creationEndTime = req.getCreationEndTime();
			if (creationEndTime == null || creationEndTime <= 0l) {
				creationEndTime = System.currentTimeMillis();
			}

			query.addCriteria(Criteria.where(CalendarEvent.Constants.START_TIME).gte(req.getCreationStartTime())
					.andOperator(Criteria.where(CalendarEvent.Constants.START_TIME).lte(req.getCreationStartTime())));
		}

		List<CalendarEvent> calendarEvents = calendarEventDAO.runQuery(query, CalendarEvent.class);
		logger.info("getCalendarEvents - count " + calendarEvents != null ? calendarEvents.size() : 0);
		return calendarEvents;
	}

	public List<CalendarEvent> cancelEvents(CancelCalendarEventReq req) {
		List<CalendarEvent> results = new ArrayList<CalendarEvent>();
		if (!StringUtils.isEmpty(req.getEventId())) {
			CalendarEvent calendarEvent = calendarEventDAO.getById(req.getEventId());
			if (calendarEvent != null && !CalendarEventState.CANCELLED.equals(calendarEvent.getState())
					&& !CalendarEventState.DONE.equals(calendarEvent.getState())) {
				logger.info("cancelEvents - " + calendarEvent.toString() + " req : " + req.toString());
				calendarEvent.setState(CalendarEventState.CANCELLED);
				calendarEventDAO.save(calendarEvent);
				results.add(calendarEvent);
			}
		} else if (!StringUtils.isEmpty(req.getUserId())) {
			Query query = new Query();
			query.addCriteria(Criteria.where(CalendarEvent.Constants.USER_ID).is(req.getUserId()));
			List<CalendarEventState> calendarEventStates = new ArrayList<CalendarEventState>();
			calendarEventStates.add(CalendarEventState.PENDING);
			calendarEventStates.add(CalendarEventState.PROCESSING);
			query.addCriteria(Criteria.where(CalendarEvent.Constants.STATE).in(calendarEventStates));
			List<CalendarEvent> calendarEvents = calendarEventDAO.runQuery(query, CalendarEvent.class);
			if (calendarEvents != null) {
				for (CalendarEvent calendarEvent : calendarEvents) {
					if (!CalendarEventState.CANCELLED.equals(calendarEvent.getState())
							&& !CalendarEventState.DONE.equals(calendarEvent.getState())) {
						calendarEvent.setState(CalendarEventState.CANCELLED);
						calendarEventDAO.save(calendarEvent);
						results.add(calendarEvent);
					}
				}
			}
		}

		return results;
	}

	public List<CalendarEvent> retryEvents(RetryCalendarEventReq req) throws BadRequestException {
		logger.info("retryEvents : " + req.toString());
		List<CalendarEvent> results = new ArrayList<CalendarEvent>();
		if (!StringUtils.isEmpty(req.getEventId())) {
			CalendarEvent calendarEvent = calendarEventDAO.getById(req.getEventId());
			if (calendarEvent != null && !CalendarEventState.CANCELLED.equals(calendarEvent.getState())
					&& !CalendarEventState.DONE.equals(calendarEvent.getState())) {
				logger.info("cancelEvents - " + calendarEvent.toString() + " req : " + req.toString());
				retryCalendarEvent(calendarEvent);
				results.add(calendarEvent);
			}
		} else {
			Query query = new Query();
			if (!StringUtils.isEmpty(req.getUserId())) {
				query.addCriteria(Criteria.where(CalendarEvent.Constants.USER_ID).is(req.getUserId()));
			}

			if (req.getCreationStartTime() != null && req.getCreationStartTime() > 0l) {
				Long creationEndTime = req.getCreationEndTime();
				if (creationEndTime == null || creationEndTime <= 0l) {
					creationEndTime = System.currentTimeMillis();
				}

				query.addCriteria(
						Criteria.where(CalendarEvent.Constants.START_TIME).gte(req.getCreationStartTime()).andOperator(
								Criteria.where(CalendarEvent.Constants.START_TIME).lte(req.getCreationStartTime())));
			}

			List<CalendarEventState> calendarEventStates = new ArrayList<CalendarEventState>();
			calendarEventStates.add(CalendarEventState.PENDING);
			calendarEventStates.add(CalendarEventState.PROCESSING);
			query.addCriteria(Criteria.where(CalendarEvent.Constants.STATE).in(calendarEventStates));
			List<CalendarEvent> calendarEvents = calendarEventDAO.runQuery(query, CalendarEvent.class);
			logger.info("retryEventsQueryResult - " + calendarEvents.toString());
			if (calendarEvents != null) {
				for (CalendarEvent calendarEvent : calendarEvents) {
					if (!CalendarEventState.CANCELLED.equals(calendarEvent.getState())
							&& !CalendarEventState.DONE.equals(calendarEvent.getState())) {
						retryCalendarEvent(calendarEvent);
						results.add(calendarEvent);
					}
				}
			}
		}

		logger.info("results : " + results.toString());
		return results;
	}

	public void retryCalendarEvent(CalendarEvent calendarEvent) throws BadRequestException {
		logger.info("retryCalendarEvent : " + calendarEvent.toString());
		// TODO : dependency on calendar manager
		updateCalendarEntries(calendarEvent);
		logger.info("retryCalendarEvent : " + calendarEvent.toString());
	}

	public void updateCalendarEntries(CalendarEvent calendarEvent) throws BadRequestException {
		// Validate calendar event
		Long executionStartTime = System.currentTimeMillis();
		List<String> errors = calendarEvent.validate();
		if (!errors.isEmpty()) {
			String errorMessage = "updateCalendarEntriesInvalid calendarEvent : " + calendarEvent.toString()
					+ " errors : " + Arrays.toString(errors.toArray());
			logger.error(errorMessage);
//			String displayMessage = ConfigUtils.INSTANCE.getStringValue("bad.request.display.message");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		}

		// Set calendar state to PROCESSING
		calendarEvent.incrementRetryCount();
		calendarEvent.setState(CalendarEventState.PROCESSING);
		updateCalendarEvent(calendarEvent);
		logger.info("calendarEventProcessing : " + calendarEvent);

		// validate event details
		String details = calendarEvent.getDetails();
		CalendarEventDetails eventDetails = new Gson().fromJson(details, CalendarEventDetails.class);
		errors = eventDetails.collectErrors();
		if (!errors.isEmpty()) {
			String errorMessage = "updateCalendarEntriesInvalid request : " + eventDetails.toString()
					+ " calendarEvent : " + calendarEvent.toString() + " errors : " + Arrays.toString(errors.toArray());
			logger.error(errorMessage);
//			String displayMessage = ConfigUtils.INSTANCE.getStringValue("bad.request.display.message");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		}

		// Identify modified bits
		BitSet modifiedBitSet = BitSetUtils.getModifiedBitSet(eventDetails.getOldBitSetLongArray(),
				eventDetails.getNewBitSetLongArray());
		logger.info("modifedBitSet : " + Arrays.toString(modifiedBitSet.toLongArray()));
		BitSetEntry bitSetEntry = new BitSetEntry(eventDetails, calendarEvent.getSlotState());

		// Capture Day Start Times
		List<Long> dayStartTimes = new ArrayList<>();
		Long startTime = CommonCalendarUtils.getDayStartTime(eventDetails.getStartTime());
		for (Long i = startTime; i < eventDetails.getEndTime(); i += CommonCalendarUtils.MILLIS_PER_DAY) {
			int index = bitSetEntry.getBitSetIndex(i);
			if ((index < -CalendarEntryManager.SLOT_COUNT)
					|| (index + CalendarEntryManager.SLOT_COUNT > BitSetUtils.bitSetSize(bitSetEntry))) {
				continue;
			}
			if (index < 0) {
				index = 0;
			}
			int nextModifiedBit = modifiedBitSet.nextSetBit(index);
			if ((nextModifiedBit >= 0) && (nextModifiedBit < index + CalendarEntryManager.SLOT_COUNT)) {
				dayStartTimes.add(i);
			}
		}

		// Create calendar entry map for the updated entries
		List<CalendarEntry> calendarEntries = calendarUtilsManager.getCalendarEntries(calendarEvent.getUserId(),
				dayStartTimes);
		Map<Long, CalendarEntry> calendarEntriesMap = calendarUtilsManager.getCalendarEntryMap(calendarEntries);
		for (long i : dayStartTimes) {
			CalendarEntry calendarEntry = null;
			if (calendarEntriesMap.containsKey(i)) {
				calendarEntry = calendarEntriesMap.get(i);
			} else {
				calendarEntry = new CalendarEntry(calendarEvent.getUserId(), i);
			}
			calendarEntry.updateByBitSet(bitSetEntry);
			logger.info("updateCalendarEntriesPostUpdate : " + calendarEntry.toString());
			calendarEntryDAO.save(calendarEntry);
		}

		logger.info("updateCalendarEntriesUpdateCount : update count is " + calendarEntries.size());

		// Update calendar event to DONE state
		eventDetails.setExecutionTime(System.currentTimeMillis() - executionStartTime);
		calendarEvent.setState(CalendarEventState.DONE);
		updateCalendarEvent(calendarEvent);
		logger.info("calendarEventDone : " + calendarEvent);
	}
}
