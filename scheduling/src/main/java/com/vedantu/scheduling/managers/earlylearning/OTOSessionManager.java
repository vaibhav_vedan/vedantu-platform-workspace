package com.vedantu.scheduling.managers.earlylearning;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.google.common.collect.Sets;
import com.vedantu.onetofew.enums.*;
import com.vedantu.scheduling.enums.EarlyLearningSessionFlag;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.enums.earlyLeaning.AnalyticsType;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.scheduling.request.earlylearning.ReScheduleDemoRequest;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.serializers.CalendarEntryDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.managers.AwsSQSManager;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.managers.EarlyLearningSessionManager;
import com.vedantu.scheduling.managers.GTTAttendeeDetailsManager;
import com.vedantu.scheduling.managers.OTFSessionManager;
import com.vedantu.scheduling.managers.VedantuWaveSessionManager;
import com.vedantu.scheduling.pojo.EarlyLearningSessionPojo;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.pojo.earlylearning.CourseProperties;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.earlylearning.AddDemoRequest;
import com.vedantu.scheduling.request.earlylearning.JoinDemoRequest;
import com.vedantu.scheduling.response.AvailabilitySlotStringRes;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.scheduling.response.earlylearning.JoinDemoResponse;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionUtils;

@Service("otoManager")
public class OTOSessionManager extends AbstractEarlyLearning {

	@Override
	public EarlyLearningSessionPojo hasEarlyLearningDemo(String studentId, SessionLabel earlyLearningCourse)
			throws ForbiddenException {

		OTFSession scheduledSession = otfSessionDAO.getELDemoSessionForStudent(studentId, earlyLearningCourse);
		logger.info("scheduledSession : " + scheduledSession);
		if (Objects.nonNull(scheduledSession)) {
			EarlyLearningSessionPojo elSessionPojo = EarlyLearningSessionPojo.builder().demoId(scheduledSession.getId())
					.presenter(scheduledSession.getPresenter()).startTime(scheduledSession.getStartTime())
					.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO)
					.endTime(scheduledSession.getEndTime()).state(scheduledSession.getState()).build();
			// Early Learning Session exists
			if (scheduledSession.getStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
				return elSessionPojo;
			}
			List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager
					.getJoinedSessionsAttendeeDetailForUser(Collections.singleton(scheduledSession.getId()), studentId);
			if (!CollectionUtils.isEmpty(joinedGtt)) {
				throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION,
						"User has successfully attended an EL session");
			}
		}
		// user attended no EL sessions in the past
		return new EarlyLearningSessionPojo();
	}

	@Override
	public AvailabilitySlotStringRes getCalendarAvailability(AvailabilitySlotStringReq req) throws VException {

		Long startTime = CommonCalendarUtils.getDayStartTime(req.getStartTime());
		Long endTime = CommonCalendarUtils.getDayEndTime(req.getEndTime());
		
		//TODO: Move this to req.verify method
		if (req.getEndTime() - req.getStartTime() > CommonCalendarUtils.MILLIS_PER_WEEK) {
			logger.error("availability fetch for duration more than 1 week");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Availability fetch for duration more than 1 week");
		}

		ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
				.getTeacherProficiencies(req.getEarlyLearningCourseType());
		String availabilitySlots = getSlotAvailabilityManager(req.getEarlyLearningCourseType())
				.getBookingAvailabilityBitString(req.getStartTime(), req.getEndTime(), proficiencyIds, req.getGrade(),
						req.getEarlyLearningCourseType());
		
		return AvailabilitySlotStringRes.builder().startTime(startTime).endTime(endTime)
				.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO)
				.slotState(CalendarEntrySlotState.AVAILABLE).bitSetString(availabilitySlots).build();
	}

	@Override
	public EarlyLearningSessionPojo addDemo(AddDemoRequest request)
			throws BadRequestException, ForbiddenException, ConflictException, NotFoundException, VException {

		Long studentId = request.getStudentId() != null ? request.getStudentId() : httpSessionUtils.getCallingUserId();

		if (!Objects.isNull(otfSessionDAO.getELDemoSessionForStudent(studentId, request.getEarlyLearningCourse()))) {
			throw new NotFoundException(ErrorCode.FORBIDDEN_ERROR, Constants.USER_ALREADY_BOOKED);
		}

		// TODO: Assign the presenter/ fetch the presenter value;
		String presenter = getProficientTeacher(request.getStartTime(), request.getEndTime(), request.getGrade(),
				request.getEarlyLearningCourse());

		if (Objects.isNull(presenter)) {
			throw new ForbiddenException(ErrorCode.ALREADY_BOOKED_EL_SESSION,
					"Booking Already Closed For Early Learning OTO Session Slot by the time you clicked, Please try other slot");
		}

		logger.info("Course Type: " + request.getEarlyLearningCourse());

		Set<SessionLabel> earlyLearningFlag = new HashSet<>();
		earlyLearningFlag.add(SessionLabel.WEBINAR);
		if (Objects.nonNull(request.getEarlyLearningCourse())) {
			earlyLearningFlag.add(request.getEarlyLearningCourse());
		}
		Set<EntityTag> entityTags = Sets.newHashSet(EntityTag.WEBINAR);

		OTFSession otfSession = OTFSession.builder().canvasStreamingType(CanvasStreamingType.VEDANTU)
				.attendees(Collections.singletonList(String.valueOf(studentId))).title(Constants.EARLY_LEARNING_TITLE)
				.otmSessionType(OTMSessionType.OTO_NURSERY).sessionToolType(OTFSessionToolType.VEDANTU_WAVE)
				.sessionContextType(OTFSessionContextType.WEBINAR).startTime(request.getStartTime())
				.endTime(request.getEndTime()).presenter(presenter).state(SessionState.SCHEDULED)
				.labels(earlyLearningFlag).entityTags(entityTags).randomUniqueIndex(SessionState.SCHEDULED.name()).serviceProvider(OTMServiceProvider.AGORA).build();

		otfSessionDAO.save(otfSession);

		// Post session schedule tasks
		// 1. generate session link and create GTTAttendeeDetails
		if (otfSession.getStartTime() != null && (StringUtils.isEmpty(otfSession.getMeetingId())
				|| StringUtils.isEmpty(otfSession.getSessionURL()))) {
			/*Map<String, Object> payload = new HashMap<>();
			payload.put("session", otfSession);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
			asyncTaskFactory.executeTask(params);*/
			otfSessionManager.sendToQForGeneratingSessionLinkAndGTT(otfSession);
		}

		// 2. blocking teacher calendar for session duration
		CalendarEntryReq calendarEntryReq = new CalendarEntryReq(presenter, request.getStartTime(),
				request.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION,
				otfSession.getId());
		calendarEntryManager.upsertCalendarEntry(calendarEntryReq);

		// 3. send email to presenter and student
		try {
			logger.info("sending email to student and parent");
			awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_SESSION_MAIL,
					gson.toJson(otfSession));
		} catch (Exception ex) {
			logger.error("Error while sending Early Learning Demo session mail: " + otfSession + " error:"
					+ ex.getMessage());
		}

		try {
			Map<String, Object> payload = new HashMap<>();
			payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
			payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
			asyncTaskFactory.executeTask(params);
		} catch (Exception e) {
			logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
		}

		leadSquareManager.pushAddDemoSession(request,
				otfSessionManager.getFormattedDateForLeadSquared(otfSession.getStartTime(), true), presenter,
				otfSession.getId(), studentId);

		vedantuWaveSessionManager.sendSessionsToNodeForCaching(Arrays.asList(otfSession));
		if (otfSession.getStartTime() < (System.currentTimeMillis() + 10 * DateTimeUtils.MILLIS_PER_MINUTE)) {
			vedantuWaveSessionManager.initSessionRecordingFromNode(otfSession);
		}
		logger.info("saved addEarlyLearningSession session: " + otfSession.toString());

		return EarlyLearningSessionPojo.builder().demoId(otfSession.getId())
				.presenter(otfSession.getPresenter()).startTime(otfSession.getStartTime())
				.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO)
				.endTime(otfSession.getEndTime()).state(otfSession.getState()).build();
	}

	@Override
	public JoinDemoResponse joinDemo(JoinDemoRequest request, CourseProperties courseProperties)
			throws BadRequestException, ConflictException, VException {
		return null;
	}

	@Override
	public EarlyLearningSessionPojo reScheduleDemo(ReScheduleDemoRequest request) throws VException {
		return null;
	}

	@Override
	public Map<AnalyticsType, Object> proficiencyAnalytics(ProficiencyAnalyticsRequest request) {
		return null;
	}

	public static class Constants {
		public static final String EARLY_LEARNING_TITLE = "Early learning session";
		public static final String USER_ALREADY_BOOKED = "User has already booked an early learning session";
		public static final String SLOT_BOOKED = "Looks like this slot is already booked by the time you clicked. Please try with some other slot";
	}
}
