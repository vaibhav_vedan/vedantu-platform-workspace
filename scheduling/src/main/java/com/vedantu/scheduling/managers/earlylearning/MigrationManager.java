package com.vedantu.scheduling.managers.earlylearning;

import com.google.common.collect.Lists;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.Interfaces.ITeacherProficiencyManager;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.dao.serializers.OverBookingDAO;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.SlotBookingCount;
import com.vedantu.scheduling.pojo.earlylearning.CourseConfiguration;
import com.vedantu.scheduling.pojo.earlylearning.CourseProperties;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MigrationManager {

    @Autowired
    private CalendarEntryManager calendarEntryManager;

    @Autowired
    private ITeacherProficiencyManager teacherProficiencyManager;

    @Autowired
    private OverBookingDAO overBookingDAO;

    @Autowired
    private OverBookingSessionManager overBookingSessionManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    protected HttpSessionUtils httpSessionUtils;

    protected Logger logger = LogFactory.getLogger(MigrationManager.class);

    public static double overBookingFactor;

    // For Testing Purpose

    public Map<String, Object> getAvailableBookingAndTeacher(AvailabilitySlotStringReq request) throws VException {
    	logger.info("Entered Availability Teacher API.");
        final ELTeachersProficiencies teachersProficiencies = teacherProficiencyManager.getTeacherProficiencies(request.getEarlyLearningCourseType());
        logger.info("Fetched Teacher Proficiency API.");
        Map<TeacherCategory.ProficiencyType, List<String>> availableTeacher = getAvailableTeacher(teachersProficiencies,request);
        logger.info("Available Teachers Map: " + availableTeacher);
        Map<Long, Map<TeacherCategory.ProficiencyType, Long>> bookings = getAvailableProficiencyCategoryForDemo(request.getStartTime(),
                request.getEndTime(), request.getEarlyLearningCourseType());
        logger.info("Bookings Map: " + bookings);
        Map<TeacherCategory.ProficiencyType, Long> closedBookings = new HashMap<>();
        CourseProperties courseProperties = CourseConfiguration.getCourseProperties(request.getEarlyLearningCourseType());
        logger.info("OverBooking Factor: " + courseProperties.getOverBookingFactor());
        for (TeacherCategory.ProficiencyType proficiencyType : TeacherCategory.ProficiencyType.values()) {
            logger.info("Proficiency Type: " + proficiencyType);
            Long bookingCount = bookings.getOrDefault(request.getStartTime(), new HashMap<>()).getOrDefault(proficiencyType, 0L);
            logger.info("Proficiency Type: " + proficiencyType + ", Booking Count: "+ bookingCount);
            //double availableCount = Math.floor(availableTeacher.getOrDefault(proficiencyType, new ArrayList<>()).size() * overBookingFactor) - bookingCount;
            closedBookings.put(proficiencyType, bookingCount);
        }
        Map<String, Object> response = new HashMap<>();
        response.put("Available Teacher", availableTeacher);
        response.put("closedBookings", closedBookings);
        response.put("Total Teachers", teacherProficiencyManager.getTeacherProficiencies(request.getEarlyLearningCourseType()).getProficiencyToTeacherMap());
        response.put("Overbooking Factor", courseProperties.getOverBookingFactor());
        return response;
    }

    private Map<TeacherCategory.ProficiencyType, List<String>> getAvailableTeacher(final ELTeachersProficiencies teachersProficiencies,AvailabilitySlotStringReq request) throws VException {

        if (request.getEndTime() - request.getStartTime() > CommonCalendarUtils.MILLIS_PER_DAY) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, AbstractEarlyLearning.Constants.ERROR_AVAILABILITY_FETCH);
        }
        Map<TeacherCategory.ProficiencyType, List<String>> mp = new HashMap<>();
        for (TeacherCategory.ProficiencyType proficiencyType : TeacherCategory.ProficiencyType.values()) {
            logger.info("Proficiency Type: " + proficiencyType.name());
            List<String> availableTeacherIds = getAvailableTeacherForTesting(
                    teachersProficiencies.getTeachersForProficiency(proficiencyType), request.getStartTime(), request.getEndTime());
            mp.put(proficiencyType, availableTeacherIds);
        }
        return mp;
    }

    //For Testing Purpose
    private List<String> getAvailableTeacherForTesting(List<String> teacherIds, Long startTime, Long endTime) {
        List<String> elTeachers = new ArrayList<>();
        if (ArrayUtils.isEmpty(teacherIds)) {
            return new ArrayList<>();
        }
        List<List<String>> allTeachersPartitions = Lists.partition(teacherIds, 100);
        for (List<String> teachersPartition : allTeachersPartitions) {
            elTeachers.addAll(calendarEntryManager.getAvailableUsers(startTime, endTime, teachersPartition));
        }
        return elTeachers;
    }

    private Map<Long, Map<TeacherCategory.ProficiencyType, Long>> getAvailableProficiencyCategoryForDemo(Long startTime, Long endTime,
                                                                                                         SessionLabel earlyLearningCourseType) {
        List<SlotBookingCount> slotBookingCount = overBookingDAO.getOverBookingAggregationWithSlotTime(startTime, endTime, earlyLearningCourseType);
        return slotBookingCount.stream().filter(Objects::nonNull)
                .collect(Collectors.groupingBy(SlotBookingCount::getSlotStartTime,
                        Collectors.toMap(SlotBookingCount::getProficiencyType, SlotBookingCount::getCount)));
    }

    @Deprecated
    public Map<String, Object> migrateBookings(Integer limit) throws VException {
        Long dayStartTime = System.currentTimeMillis();
        Map<String, Object> response = new HashMap<>();
        List<String> bookingId = new ArrayList<>();
        List<String> updatedBookingId = new ArrayList<>();
        int failed = 0;
        limit = limit < 500 ? limit : 500;
        List<OverBooking> bookings = overBookingDAO.getBookingsForMigration(dayStartTime, limit);
        for (OverBooking booking : bookings) {
            try {
                SessionLabel earlyLearningCourseType = booking.getLabels().contains(SessionLabel.SUPER_CODER)
                        ? SessionLabel.SUPER_CODER : SessionLabel.SUPER_READER;
                String grade = fosUtils.getUserBasicInfo(booking.getStudentId(), Boolean.FALSE).getGrade();

                TeacherCategory.ProficiencyType proficiencyType = overBookingSessionManager.getElTeacherProficiencyType(booking.getSlotStartTime(), booking.getSlotEndTime(),
                        4, earlyLearningCourseType);
                if (proficiencyType != null) {
                    booking.setProficiencyType(proficiencyType);
                    overBookingDAO.save(booking);
                    updatedBookingId.add(booking.getId());
                } else {
                    bookingId.add(booking.getId());
                }
            } catch (Exception ex) {
                failed = failed + 1;
                bookingId.add(booking.getId());
                logger.error("Proficiency migration failed for overbookingId : {}", booking.getId());
            }
        }
        response.put("failed booking", failed);
        response.put("Update failed in booking Id", bookingId);
        response.put("Updated Successfully in ids",updatedBookingId);
        return response;
    }
}

