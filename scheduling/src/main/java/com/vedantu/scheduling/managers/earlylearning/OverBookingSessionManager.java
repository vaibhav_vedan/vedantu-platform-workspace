package com.vedantu.scheduling.managers.earlylearning;

import com.google.common.collect.Sets;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.*;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.dao.serializers.OverBookingDAO;
import com.vedantu.scheduling.enums.ELSessionStatus;
import com.vedantu.scheduling.enums.EarlyLearningSessionFlag;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.enums.earlyLeaning.AnalyticsType;
import com.vedantu.scheduling.pojo.EarlyLearningSessionPojo;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.pojo.earlylearning.CourseProperties;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.earlylearning.AddDemoRequest;
import com.vedantu.scheduling.request.earlylearning.JoinDemoRequest;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.scheduling.request.earlylearning.ReScheduleDemoRequest;
import com.vedantu.scheduling.response.AvailabilitySlotStringRes;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.scheduling.response.earlylearning.AllottedTeacherRes;
import com.vedantu.scheduling.response.earlylearning.JoinDemoResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("overbookingManager")
public class OverBookingSessionManager extends AbstractEarlyLearning {

	@Autowired
	private OverBookingDAO overBookingDAO;

	@Autowired
	private AnalysisManager analysisManager;

	public static final String TIME_ZONE_IN = "Asia/Kolkata";

	@Override
	public EarlyLearningSessionPojo hasEarlyLearningDemo(String studentId, SessionLabel earlyLearningCourse)
			throws ForbiddenException {

		OverBooking scheduledBooking = overBookingDAO.getEarlyLearningBookingByStudentId(studentId,
				earlyLearningCourse);

		if (Objects.nonNull(scheduledBooking)) {
			// Early Learning Booking exists
			if (scheduledBooking.getSlotStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
				return EarlyLearningSessionPojo.builder().demoId(scheduledBooking.getId())
						.startTime(scheduledBooking.getSlotStartTime()).endTime(scheduledBooking.getSlotEndTime())
						.alternatePhoneNumber(Optional.ofNullable(scheduledBooking.getAlternatePhoneNumber()).orElse(StringUtils.EMPTY))
						.state(scheduledBooking.getState())
						.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO_OVERBOOKING).build();
			}
			String pastSessionId = scheduledBooking.getSessionId();
			if (StringUtils.isNotEmpty(pastSessionId)) {
				// Check if the User has already attempted the session
				List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager
						.getJoinedSessionsAttendeeDetailForUser(Collections.singleton(pastSessionId), studentId);
				if (ArrayUtils.isNotEmpty(joinedGtt)) {
					validateStudentDemoSessionSuccessful(studentId, pastSessionId, joinedGtt);
				}
			}
		}

		if (SessionLabel.SUPER_CODER.equals(earlyLearningCourse)) {
			Long currentTimeInMillis = System.currentTimeMillis(); // let this variable 18 Jul 12 AM
			// Check if the User has already booked a session
			OTFSession otfSession = otfSessionDAO.getELOldBookedSession(studentId, currentTimeInMillis);
			if (otfSession != null) {
				List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager
						.getJoinedSessionsAttendeeDetailForUser(Collections.singleton(otfSession.getId()), studentId);
				if (ArrayUtils.isNotEmpty(joinedGtt)) {
					validateStudentDemoSessionSuccessful(studentId, otfSession.getId(), joinedGtt);
				}
			}

		}
		return new EarlyLearningSessionPojo();
	}

	private void validateStudentDemoSessionSuccessful(String studentId, String pastSessionId, List<GTTAttendeeDetails> joinedGtt) throws ForbiddenException {
		GTTAttendeeDetails gttAttendeeDetails = Optional.ofNullable(joinedGtt).orElseGet(ArrayList::new)
				.stream()
				.findFirst()
				.orElse(null);
		if(Objects.nonNull(gttAttendeeDetails)) {
			// 1601577000000L is 2nd Oct 2020, check added for backward compatibility
			// Session attended post 2nd oct will be considered valid for certificate generation,
			// only if student has joined the session and teacher has marked it successful!
			if(gttAttendeeDetails.getSessionStartTime() > 1601577000000L) {
				if ((Objects.isNull(gttAttendeeDetails.getSessionSuccessful()) || ELSessionStatus.NO.equals(gttAttendeeDetails.getSessionSuccessful()))) {
					logger.info("Student {} , demo session - {}  was not successful", studentId, pastSessionId);
				} else {
					throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION,
							"User has already attended the Early Learning Session");
				}
			} else {
				throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION,
						"User has already attended the Early Learning Session");
			}
		}
	}

	@Override
	public AvailabilitySlotStringRes getCalendarAvailability(AvailabilitySlotStringReq request) throws VException {

		Long startTime = CommonCalendarUtils.getDayStartTime(request.getStartTime());
		Long endTime = CommonCalendarUtils.getDayEndTime(request.getEndTime());

		if (request.getEndTime() - request.getStartTime() > CommonCalendarUtils.MILLIS_PER_WEEK) {
			logger.error("availability fetch for duration more than 1 week");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"Availability fetch for duration more than 1 week");
		}

		ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
				.getTeacherProficiencies(request.getEarlyLearningCourseType());

		if (org.springframework.util.CollectionUtils.isEmpty(proficiencyIds.getElTeachersByGrade(request.getGrade()))) {
			return AvailabilitySlotStringRes.builder().startTime(startTime).endTime(endTime)
					.slotState(CalendarEntrySlotState.AVAILABLE).bitSetString(AbstractSlotAvailabilityManager.Constants.NOT_AVAILABLE_BIT).build();
		}
		String availabilityBitString = getSlotAvailabilityManager(request.getEarlyLearningCourseType())
				.getBookingAvailabilityBitString(request.getStartTime(), request.getEndTime(), proficiencyIds,
						request.getGrade(), request.getEarlyLearningCourseType());
		return AvailabilitySlotStringRes.builder().startTime(request.getStartTime()).endTime(request.getEndTime())
				.slotState(CalendarEntrySlotState.AVAILABLE).bitSetString(availabilityBitString)
				.slotDuration(request.getSlotDuration())
				.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO_OVERBOOKING).build();
	}

	@Override
	public EarlyLearningSessionPojo addDemo(AddDemoRequest request) throws VException {

		Long studentId = request.getStudentId() != null ? request.getStudentId() : httpSessionUtils.getCallingUserId();

		logger.info("Overbooking AddDemo studentId : {}", studentId);
		OverBooking scheduledBooking = overBookingDAO.getEarlyLearningBookingByStudentId(studentId,
				request.getEarlyLearningCourse());

		if (Objects.nonNull(scheduledBooking)
				&& scheduledBooking.getSlotStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
			throw new ConflictException(ErrorCode.ALREADY_BOOKED_EL_SESSION, Constants.DEMO_ALREADY_EXIST);
		}

		Long startTime = CommonCalendarUtils.getSlotStartTime(request.getStartTime());
		Long endTime = CommonCalendarUtils.getSlotEndTime(request.getEndTime());
		logger.info("Booking StartTime : " + startTime + ", EndTime: " + endTime);

		ProficiencyType proficiencyType = getElTeacherProficiencyType(startTime, endTime, request.getGrade(),
				request.getEarlyLearningCourse());
		logger.info("AddDemo available ProficiencyType : {} for studentId {} at Time : {}", proficiencyType, studentId,
				System.currentTimeMillis());

		if (Objects.isNull(proficiencyType)) {
			throw new ForbiddenException(ErrorCode.ALREADY_BOOKED_EL_SESSION, Constants.BOOKING_CLOSED);
		}

		Set<SessionLabel> earlyLearningFlag = new HashSet<>();
		earlyLearningFlag.add(SessionLabel.WEBINAR);
		earlyLearningFlag.add(request.getEarlyLearningCourse());
		OverBooking overBooking = OverBooking.builder().studentId(String.valueOf(studentId)).slotStartTime(startTime)
				.slotEndTime(endTime).state(SessionState.SCHEDULED).labels(earlyLearningFlag)
				.grade(String.valueOf(request.getGrade()))
				.proficiencyType(proficiencyType).build();

		overBookingDAO.save(overBooking);

		// 3. send email to student
		try {
			logger.info("sending email and sms to student");
			awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_BOOKING_MAIL,
					gson.toJson(overBooking));
		} catch (Exception ex) {
			logger.error("Error while sending Early Learning Demo session mail: " + overBooking + " error:"
					+ ex.getMessage());
		}

		leadSquareManager.pushAddDemoBooking(overBooking.getSlotStartTime(), overBooking.getId(), studentId,
				overBooking.getAlternatePhoneNumber(),request.getEarlyLearningCourse().name());

		return EarlyLearningSessionPojo.builder().demoId(overBooking.getId()).startTime(overBooking.getSlotStartTime())
				.endTime(overBooking.getSlotEndTime())
				.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO_OVERBOOKING).state(overBooking.getState())
				.build();
	}

	@Override
	public JoinDemoResponse joinDemo(JoinDemoRequest request, CourseProperties courseProperties)
			throws BadRequestException, ConflictException, VException {

		Long studentId = request.getStudentId() != null ? request.getStudentId() : httpSessionUtils.getCallingUserId();

		logger.info("Overbooking joinDemo studentId : {}", studentId);
		OverBooking overBooking = overBookingDAO.getOverBookingById(request.getDemoId());

		if (Objects.isNull(overBooking)) {
			throw new ConflictException(ErrorCode.EL_BOOKING_NOT_FOUND, Constants.EL_BOOKING_NOT_FOUND);
		}

		Long startTime = CommonCalendarUtils.getSlotStartTime(overBooking.getSlotStartTime());
		Long endTime = CommonCalendarUtils.getSlotStartTime(overBooking.getSlotEndTime());

		logger.info("joinDemo startTime : {} endTime : {}", startTime, endTime);
		if (endTime - startTime > DateTimeUtils.MILLIS_PER_HOUR) {
			throw new BadRequestException(ErrorCode.EL_SESSION_SLOT_DURATION_ERROR, Constants.INVALID_SESSION_DURATION);
		}

		if (System.currentTimeMillis() < startTime
				- courseProperties.getTimeDifference() * DateTimeUtils.MILLIS_PER_MINUTE) {
			throw new ForbiddenException(ErrorCode.SESSION_NOT_STARTED, Constants.SESSION_NOT_STARTED);
		}
		if (System.currentTimeMillis() > endTime) {
			throw new ForbiddenException(ErrorCode.SESSION_ALREADY_ENDED, Constants.SESSION_ALREADY_ENDED);
		}

		if (StringUtils.isNotEmpty(overBooking.getSessionId())) {
			OTFSession scheduledSession = otfSessionDAO.getById(overBooking.getSessionId());
			logger.info("joinDemo preScheduledSessionId : " + scheduledSession.getId() + ", for Demo ID: "
					+ request.getDemoId());
			if (Objects.nonNull(scheduledSession)) {
				return JoinDemoResponse.builder().demoId(scheduledSession.getId()).build();
			}
		} else {
			if (Objects.isNull(overBooking.getIsTeacherAssigned())
					|| !OverBookingStatus.NO.equals(overBooking.getIsTeacherAssigned())) {
				if (!redisDAO.setnx(request.getStudentId() + "_" + startTime, Long.toString(System.currentTimeMillis()),
						5)) {
					throw new VException(ErrorCode.REQUEST_PROCESSING, Constants.REQUEST_PROCESSING);
				}
			}
		}

		String presenter = getProficientTeacher(overBooking.getSlotStartTime(), overBooking.getSlotEndTime(),
				request.getGrade(), request.getEarlyLearningCourse());
		logger.info("Presenter assigned : {} for overbookingId : {}", presenter, overBooking.getId());

		if (StringUtils.isEmpty(presenter)) {
			overBooking.setIsTeacherAssigned(OverBookingStatus.NO);
			overBookingDAO.save(overBooking);
			leadSquareManager.noTeacherAvailableLeadSquaredEvent(studentId, overBooking.getSlotStartTime(),
					overBooking.getId());
			throw new VException(ErrorCode.TEACHER_NOT_AVAILABLE, Constants.TEACHER_NOT_AVAILABLE);
		}

		Set<SessionLabel> earlyLearningFlag = Sets.newHashSet(request.getEarlyLearningCourse(), SessionLabel.WEBINAR);
		Set<EntityTag> entityTags = Sets.newHashSet(EntityTag.WEBINAR);

		OTFSession otfSession = OTFSession.builder().serviceProvider(OTMServiceProvider.AGORA).canvasStreamingType(CanvasStreamingType.VEDANTU)
				.attendees(Collections.singletonList(String.valueOf(studentId))).title(Constants.EARLY_LEARNING_TITLE)
				.otmSessionType(OTMSessionType.OTO_NURSERY).sessionToolType(OTFSessionToolType.VEDANTU_WAVE).type(EntityType.ONE_TO_ONE)
				.sessionContextType(OTFSessionContextType.WEBINAR).startTime(overBooking.getSlotStartTime())
				.endTime(overBooking.getSlotEndTime()).presenter(presenter).state(SessionState.SCHEDULED)
				.labels(earlyLearningFlag).entityTags(entityTags).randomUniqueIndex(SessionState.SCHEDULED.name()).build();

		otfSessionDAO.save(otfSession);
		logger.info("Join Demo SessionId : {} for OverBooking Id {}", otfSession.getId(), request.getDemoId());

		// Post session schedule tasks
		// 1. generate session link and create GTTAttendeeDetails
		if (otfSession.getStartTime() != null && (StringUtils.isEmpty(otfSession.getMeetingId())
				|| StringUtils.isEmpty(otfSession.getSessionURL()))) {
			/*Map<String, Object> payload = new HashMap<>();
			payload.put("session", otfSession);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
			asyncTaskFactory.executeTask(params);*/
			otfSessionManager.sendToQForGeneratingSessionLinkAndGTT(otfSession);
		}

		// 2. blocking teacher calendar for session duration
		CalendarEntryReq calendarEntryReq = new CalendarEntryReq(presenter, startTime, endTime,
				CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION, otfSession.getId());
		calendarEntryManager.upsertCalendarEntry(calendarEntryReq);
		logger.info("calendarBlocked for the OverbookingId : {}", request.getDemoId());
		try {
			Map<String, Object> payload = new HashMap<>();
			payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
			payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
			asyncTaskFactory.executeTask(params);
		} catch (Exception e) {
			logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
		}
		// 3. send email to presenter
		try {
			logger.info("sending email and sms to Teacher");
			awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_SESSION_MAIL,
					gson.toJson(otfSession));
		} catch (Exception ex) {
			logger.error("Error while sending Early Learning Demo session mail: " + otfSession + " error:"
					+ ex.getMessage());
		}
		logger.info("otfSession Id : " + otfSession.getId());
		vedantuWaveSessionManager.sendSessionsToNodeForCaching(Arrays.asList(otfSession));
		if (otfSession.getStartTime() < (System.currentTimeMillis() + 10 * DateTimeUtils.MILLIS_PER_MINUTE)) {
			vedantuWaveSessionManager.initSessionRecordingFromNode(otfSession);
		}
		logger.info("saved addEarlyLearningSession session: " + otfSession.toString());

		return JoinDemoResponse.builder().demoId(otfSession.getId()).build();

	}

	@Override
	public EarlyLearningSessionPojo reScheduleDemo(ReScheduleDemoRequest request) throws VException {
		Long startTime = CommonCalendarUtils.getSlotStartTime(request.getStartTime());
		Long endTime = CommonCalendarUtils.getSlotEndTime(request.getEndTime());
		logger.info("ReScheduleDemo : Booking StartTime : " + startTime + ", EndTime: " + endTime);
		OverBooking scheduledBooking = overBookingDAO.getOverBookingById(request.getDemoId());
		if (Objects.isNull(scheduledBooking)) {
			throw new NotFoundException(ErrorCode.EL_BOOKING_NOT_FOUND,
					"Early Booking Not Found : " + request.getDemoId());
		}
		if (SessionState.CANCELLED.equals(scheduledBooking.getState())
				|| SessionState.FORFEITED.equals(scheduledBooking.getState())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session already cancelled");
		}
		if (System
				.currentTimeMillis() > (scheduledBooking.getSlotStartTime() - DateTimeUtils.MILLIS_PER_MINUTE * 10L)) {
			throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,
					"ReScheduling allowed only until 10 minutes before booking start time");
		}
		scheduledBooking.setState(SessionState.CANCELLED);
		scheduledBooking.setCancelledBY(httpSessionUtils.getCallingUserId().toString());
		scheduledBooking.setRemarks(OverBookingSessionManager.Constants.PARENT_RESCHEDULED);
		overBookingDAO.save(scheduledBooking);

		leadSquareManager.cancelDemoBooking(scheduledBooking.getSlotStartTime(), scheduledBooking.getId(),
				scheduledBooking.getStudentId());
		Set<SessionLabel> earlyLearningFlag = new HashSet<>();
		earlyLearningFlag.add(SessionLabel.WEBINAR);
		earlyLearningFlag.add(request.getEarlyLearningCourse());
		ProficiencyType proficiencyType = getElTeacherProficiencyType(startTime, endTime, request.getGrade(),
				request.getEarlyLearningCourse());
		if (Objects.isNull(proficiencyType)) {
			throw new ForbiddenException(ErrorCode.ALREADY_BOOKED_EL_SESSION,
					"Booking Already Closed For Early Learning Slot");
		}
		OverBooking overBooking = OverBooking.builder().studentId(String.valueOf(httpSessionUtils.getCallingUserId()))
				.slotStartTime(startTime).slotEndTime(endTime).state(SessionState.SCHEDULED).labels(earlyLearningFlag)
				.grade(String.valueOf(request.getGrade()))
				.proficiencyType(proficiencyType).alternatePhoneNumber(scheduledBooking.getAlternatePhoneNumber())
				.build();
		overBookingDAO.save(overBooking);
		// 3. send email to student
		try {
			logger.info("sending email and sms to student");
			awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_BOOKING_MAIL,
					gson.toJson(overBooking));
		} catch (Exception ex) {
			logger.error("Error while sending Early Learning Demo session mail: " + overBooking + " error:"
					+ ex.getMessage());
		}
		leadSquareManager.pushAddDemoBooking(overBooking.getSlotStartTime(), overBooking.getId(),
				Long.valueOf(scheduledBooking.getStudentId()), overBooking.getAlternatePhoneNumber()
		,request.getEarlyLearningCourse().name());
		return EarlyLearningSessionPojo.builder().demoId(overBooking.getId())
				.startTime(overBooking.getSlotStartTime()).endTime(overBooking.getSlotEndTime())
				.alternatePhoneNumber(Optional.ofNullable(scheduledBooking.getAlternatePhoneNumber()).orElse(StringUtils.EMPTY))
				.earlyLearningSessionFlag(EarlyLearningSessionFlag.OTO_OVERBOOKING).state(overBooking.getState())
				.build();
	}

	@Override
	public Map<AnalyticsType, Object> proficiencyAnalytics(ProficiencyAnalyticsRequest request) throws VException, ParseException {


		SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
		Date date = sdfTime.parse(request.getSelectedTime());
		request.setStartTime(date.getTime());
		request.setEndTime(request.getStartTime() + CommonCalendarUtils.MILLIS_PER_HOUR);
		Map<AnalyticsType , Object> response = new HashMap<>();
		Long startTime = CommonCalendarUtils.getSlotStartTime(request.getStartTime());
		Long endTime = CommonCalendarUtils.getSlotEndTime(request.getEndTime());
		if(AnalyticsType.AVAILABLE_TEACHER.equals(request.getAnalyticsType())) {
			 List<String> availableTeacher = analysisManager.getAvailableTeachersForSlot(request);
			 response.put(AnalyticsType.AVAILABLE_TEACHER,  availableTeacher);
		} else {
			List<AllottedTeacherRes> allottedTeacherRes = analysisManager.getAllottedTeacherForSlot(request);
			Map<Long, Map<ProficiencyType, Long>> closedBookings = analysisManager.getClosedBookings(startTime, endTime,
					request.getEarlyLearningCourseType());
			response.put(AnalyticsType.CLOSED_BOOKINGS, closedBookings.getOrDefault(startTime, new HashMap<>()));
			response.put(AnalyticsType.ASSIGNED_TEACHER, allottedTeacherRes);
		}
		return response;
	}

	public static class Constants {

		public static final String EL_BOOKING_NOT_FOUND = "Early learning demo booking not found for the user, please book a demo.";
		public static final String SESSION_NOT_STARTED = "Early learning session has not started yet.";
		public static final String SESSION_ALREADY_ENDED = "Early learning Session has been ended.";
		public static final String DEMO_ALREADY_EXIST = "Booking already exists for early learning.";
		public static final String BOOKING_CLOSED = "Booking is closed for the selected slot, please check other slots.";
		public static final String EARLY_LEARNING_TITLE = "Early learning session";
		public static final String INVALID_SESSION_DURATION = "Slot duration is more than one hour";
		public static final String PARENT_RESCHEDULED = "Parent rescheduled";
		public static final String REQUEST_PROCESSING = "Your request is processing in our system, Session access link will open automatically on new tab";
		public static final String TEACHER_NOT_AVAILABLE = "Teachers are not available at this time, our team will get back to you soon.";

	}
}
