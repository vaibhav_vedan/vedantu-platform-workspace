package com.vedantu.scheduling.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientHandlerException;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.pojo.HandleOTFRecordingLambda;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.SessionActivity;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.SessionActivityDao;
import com.vedantu.scheduling.pojo.GTTRecording;
import com.vedantu.subscription.pojo.game.GameSessionActivity;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class SessionRecordingManager {

    @Autowired
    private OTFSessionDAO oTFSessionDAO;

    @Autowired
    @Qualifier("gttScheduleManager")
    private GTTScheduleManager gttScheduleManager;

    @Autowired
    @Qualifier("gttProScheduleManager")
    private GTTProScheduleManager gttProScheduleManager;

    @Autowired
    @Qualifier("gtt100ScheduleManager")
    private GTT100ScheduleManager gtt100ScheduleManager;

    @Autowired
    @Qualifier("gtwScheduleManager")
    private GTWScheduleManager gtwScheduleManager;

    @Autowired
    @Qualifier("gttWebinarScheduleManager")
    private GTTWebinarScheduleManager gttWebinarScheduleManager;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private SessionActivityDao sessionActivityDao;


    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionRecordingManager.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private static final Gson gson = new Gson();
    private static final List<OTFSessionToolType> SUPPORTED_SESSION_TOOL_TYPE_LIST = Arrays
            .asList(OTFSessionToolType.GTT, OTFSessionToolType.GTT_PRO, OTFSessionToolType.GTT100, OTFSessionToolType.GTW, OTFSessionToolType.GTT_WEBINAR);

    public HandleOTFRecordingLambda handleSessionRecording(String sessionId)
            throws NotFoundException, BadRequestException, InternalServerErrorException, ConflictException {
        logger.info("SessionId:" + sessionId);
        OTFSession oTFSession = oTFSessionDAO.getById(sessionId);
        if (oTFSession == null) {
            String errorMessage = "Session does not exist for req : " + sessionId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        return handleSessionRecording(oTFSession);
    }

    public HandleOTFRecordingLambda handleSessionRecording(OTFSession otfSession)
            throws ConflictException, BadRequestException, InternalServerErrorException, NotFoundException {

        // Fetch download urls
        GTTScheduleManager scheduleManager = null;
        if (otfSession.getSessionToolType() == OTFSessionToolType.GTT) {
            scheduleManager = gttScheduleManager;
        } else if (otfSession.getSessionToolType() == OTFSessionToolType.GTT_PRO) {
            scheduleManager = gttProScheduleManager;
        } else if (otfSession.getSessionToolType() == OTFSessionToolType.GTT100) {
            scheduleManager = gtt100ScheduleManager;
        } else if (otfSession.getSessionToolType() == OTFSessionToolType.GTW) {
            scheduleManager = gtwScheduleManager;
        } else if (otfSession.getSessionToolType() == OTFSessionToolType.GTT_WEBINAR) {
            scheduleManager = gttWebinarScheduleManager;
        } else if (otfSession.hasVedantuWaveFeatures()) {
            //TODO handle recording
        } else {
            throw new BadRequestException(ErrorCode.OTF_UNSUPPORTED_SESSION_TOOL,
                    "Recording not supported for this session tool type:" + otfSession.getSessionToolType() + " session:"
                            + otfSession.toString());
        }

        List<GTTRecording> downloadRecordings = scheduleManager.getRecordingDownloadUrl(otfSession);

        // Check if the recording upload need to be done
        if (CollectionUtils.isEmpty(downloadRecordings)) {
            logger.info("No recordings found");
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Recordings not found for session:" + otfSession.getId());
        }

        if (!CollectionUtils.isEmpty(otfSession.getReplayUrls())
                && downloadRecordings.size() == otfSession.getReplayUrls().size()) {
            logger.info("Recording already uploaded");
            throw new ConflictException(ErrorCode.OTF_RECORDING_ALREADY_UPDATED, "Recording already updated");
        }

        // TODO : Sort recordings by start time
        Collections.sort(downloadRecordings);

        // Upload each recording and capture the recording id
        List<String> replayUrls = new ArrayList<>();
        List<String> downloadUrls = new ArrayList<>();

        boolean invokeLambda = false;
        HandleOTFRecordingLambda req = new HandleOTFRecordingLambda(ConfigUtils.INSTANCE.getStringValue("environment"),
                otfSession.getId());

        logger.info("Handle recording req : " + req.toString());
        for (GTTRecording recording : downloadRecordings) {
            try {
                if (!amazonS3Manager.keyExists(getRecordingKeyName(recording.getRecordingId(), otfSession.getId()))) {
                    // Invoke aws
                    invokeLambda = true;
                }

                replayUrls.add(recording.getRecordingId());
                downloadUrls.add(recording.getDownloadUrl());
                req.addRecordingEntry(recording.getRecordingId(), recording.getDownloadUrl());
            } catch (Exception ex) {
                logger.info("Exception ex:" + ex.toString() + " recording:" + recording.toString());
            }
        }

        if (!invokeLambda && downloadRecordings.size() > 1 && !amazonS3Manager.keyExists(otfSession.getId())) {
            invokeLambda = true;
        }

        if (!invokeLambda) {

            // Update the session with urls
            logger.info("Replay urls:" + Arrays.toString(replayUrls.toArray()));
            otfSession.setReplayUrls(replayUrls);
            oTFSessionDAO.save(otfSession);
            logger.info("Session:" + otfSession.toString());
            throw new ConflictException(ErrorCode.OTF_RECORDING_ALREADY_UPDATED,
                    "Recording already updated for sessionId:" + otfSession.getId());
        }

        if (OTFSessionToolType.GTW.equals(otfSession.getSessionToolType())) {
            req.setDeleteRecordings(Boolean.FALSE);
        }

        return req;
    }

    public List<HandleOTFRecordingLambda> handleSesssionRecordingsBulk(Long afterEndTime, Long beforeEndTime) {
        logger.info("Request : " + afterEndTime + " endTime:" + beforeEndTime);
        List<HandleOTFRecordingLambda> requests = new ArrayList<HandleOTFRecordingLambda>();

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).in(SUPPORTED_SESSION_TOOL_TYPE_LIST));

        List<OTFSession> oTFSessions = oTFSessionDAO.runQuery(query, OTFSession.class);
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                if (recordingUpdated(oTFSession)) {
                    continue;
                }

                try {
                    HandleOTFRecordingLambda req = handleSessionRecording(oTFSession);
                    requests.add(req);
                } catch (NotFoundException | BadRequestException | InternalServerErrorException | ConflictException
                        | ClientHandlerException ex) {
                    logger.info("ReqException : " + ex.toString() + " message:" + ex.getMessage());
                } catch (Exception ex) {
                    logger.error("Error occured fetching lambda req for session:" + oTFSession.getId() + " ex:"
                            + ex.toString() + " ex:" + ex.getMessage());
                }
            }
        }

        return requests;
    }

    public List<OTFSession> updateSessionReplayUrls(Long afterEndTime, Long beforeEndTime) {
        logger.info("Request : " + afterEndTime + " endTime:" + beforeEndTime);

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).in(SUPPORTED_SESSION_TOOL_TYPE_LIST));

        List<OTFSession> otfSessions = oTFSessionDAO.runQuery(query, OTFSession.class);
        if (!CollectionUtils.isEmpty(otfSessions)) {
            List<String> errors = new ArrayList<>();
            for (OTFSession oTFSession : otfSessions) {
                if (!recordingUpdated(oTFSession)) {
                    try {
                        handleSessionRecording(oTFSession);
                    } catch (NotFoundException | ConflictException | ClientHandlerException ex) {
                        String errorMessage = "updateSessionReplayUrlsEx:" + ex.toString() + " message:"
                                + ex.getMessage() + " session:" + oTFSession.toString();
                        logger.info(errorMessage);
                    } catch (Exception ex) {
                        String errorMessage = "updateSessionReplayUrlsEx:" + ex.toString() + " message:"
                                + ex.getMessage() + " session:" + oTFSession.toString();
                        logger.info(errorMessage);
                        errors.add(errorMessage);
                    }
                }
            }

            if (!CollectionUtils.isEmpty(errors)) {
                logger.info("updateSessionReplayUrlsAllErrors : " + Arrays.toString(errors.toArray()));
            }
        }

        return otfSessions;
    }

    private boolean recordingUpdated(OTFSession oTFSession) {
        if (CollectionUtils.isEmpty(oTFSession.getReplayUrls())) {
            return false;
        }

        if (oTFSession.getReplayUrls().size() > 1 && !amazonS3Manager.keyExists(oTFSession.getId())) {
            return false;
        }

        for (String recordingId : oTFSession.getReplayUrls()) {
            logger.info("replayUrl:" + recordingId);
            if (!amazonS3Manager.keyExists(getRecordingKeyName(recordingId, oTFSession.getId()))) {
                return false;
            }
        }

        return true;
    }

    public String getDownloadUrl(String sessionId, String recordingId) throws NotFoundException {
        logger.info("SessionId:" + sessionId + " recordingId:" + recordingId);
        OTFSession oTFSession = oTFSessionDAO.getById(sessionId);
        if (oTFSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Session not found for id:" + sessionId + " recordingId:" + recordingId);
        }

        String keyName = "";
        if (amazonS3Manager.keyExists(sessionId)) {
            keyName = sessionId;
        } else {
            keyName = getRecordingKeyName(recordingId, sessionId);
            logger.info("No session key found for multiple recordings:" + sessionId + " recordingId:" + recordingId);
        }
        logger.info("Recording key:" + keyName);
        return amazonS3Manager.getPreSignedUrl(keyName);
    }

    public String getSessionNotesDownloadUrl(String sessionId, Long userId) {
        String url = amazonS3Manager.getPreSignedSessionNotesUrl(sessionId);
        SessionActivity activity = new SessionActivity();
        activity.setSessionId(sessionId);
        activity.setUserId(userId);
        sessionActivityDao.findAndAddDownloadActivity(sessionId, userId, url);

//        GameSessionActivity game = GameSessionActivity.builder()
//                .sessionId(sessionId)
//                .userId(userId)
//                .taskType(GameSessionActivity.TaskType.DOWNLOAD)
//                .sessionNotesDownloadTime(System.currentTimeMillis())
//                .build();
//        game.setCreationTime(System.currentTimeMillis());
//        game.setLastUpdated(System.currentTimeMillis());
        // todo enable for gamification
        // awsSQSManager.sendToSQS(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS, SQSMessageType.GAME_SESSION_DOWNLOAD_ACTIVITY,
        //        gson.toJson(game), String.valueOf(userId));
        return url;
    }

    public String getSessionNotesUploadUrl(String sessionId) {
        return amazonS3Manager.getPreSignedSessionNotesUploadUrl(sessionId);
    }

    public String getDownloadUrl(String sessionId) {
        return amazonS3Manager.getPreSignedUrl(sessionId);
    }

    public static String getRecordingKeyName(String recordingId, String sessionId) {
        return sessionId + "-" + recordingId;
    }

    public static long getMillisFromDate(String formatDate) throws ParseException {
        return sdf.parse(formatDate).getTime();
    }

    public void downloadAndUploadToAWSS3(GTTRecording recording, String sessionId) throws InternalServerErrorException {
        File file = new File(String.valueOf(sessionId));
        try {
            URL downloadUrl = new URL(recording.getDownloadUrl());
            ReadableByteChannel rbc = Channels.newChannel(downloadUrl.openStream());
            @SuppressWarnings("resource")
            FileOutputStream fos = new FileOutputStream(file);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            logger.info("File size : " + file.getTotalSpace());
            amazonS3Manager.uploadFile(getRecordingKeyName(recording.getRecordingId(), sessionId), file);
            logger.info("uploaded complete");
        } catch (Exception ex) {
            String errorMessage = "Error uploading to s3" + ex.getMessage() + " sessionId : " + sessionId
                    + " recording : " + recording.toString();
            logger.error(errorMessage);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
        } finally {
            file.delete();
        }
    }

}
