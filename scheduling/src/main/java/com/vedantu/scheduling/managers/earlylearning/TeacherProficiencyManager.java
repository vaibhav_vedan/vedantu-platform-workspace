package com.vedantu.scheduling.managers.earlylearning;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.Interfaces.ITeacherProficiencyManager;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TeacherProficiencyManager implements ITeacherProficiencyManager {

	@Autowired
	private CalendarEntryManager calendarEntryManager;

	@Autowired
	private RedisDAO redisDAO;

	private Logger logger = LogFactory.getLogger(AbstractEarlyLearning.class);

	private final Gson gson = new Gson();

	@Autowired
	private FosUtils fosUtils;

	@Override
	public String assignTeacher(Integer grade, Long startTime, Long endTime,
			ELTeachersProficiencies elTeachersProficiencies) {
		String presenter = StringUtils.EMPTY;
		for (ProficiencyType proficiencyType : TeacherCategory.getGradeType(grade).getProficiencies()) {
			logger.info("Proficiency Type: " + proficiencyType.name());
			presenter = getTeacherIdsByProficiencyCategory(
					elTeachersProficiencies.getTeachersForProficiency(proficiencyType), startTime, endTime);
			logger.info("Teacher Assigned: " + presenter + " for proficiency type: " + proficiencyType.name());
			if (StringUtils.isNotEmpty(presenter)) {
				return presenter;
			}
		}

		return presenter;
	}

	private void saveProficiencyInRedis(ELTeachersProficiencies teacherProficiency,
			SessionLabel earlyLeaningCourseType) {

		Map<String, String> proficiencyMap = teacherProficiency.getProficiencyToTeacherMap().keySet().stream()
				.collect(Collectors.toMap(x -> ELTeachersProficiencies.getELRedisKey(earlyLeaningCourseType, x),
						x -> gson.toJson(teacherProficiency.getProficiencyToTeacherMap().get(x))));

		try {
			// TODO update Redis key when new early learning teachers are added
			redisDAO.setKeyPairs(proficiencyMap);
		} catch (InternalServerErrorException e) {
			logger.error("Error while setting values for redis key {}", proficiencyMap);
		}
	}

	@Override
	public ELTeachersProficiencies getTeacherProficiencies(SessionLabel earlyLearningCourseType)
			throws VException {
		ELTeachersProficiencies teacherProficiencies = getProficiencyFromRedis(earlyLearningCourseType);
		if (Objects.isNull(teacherProficiencies)) {
			teacherProficiencies = fosUtils.getELTeachersProficiencies(earlyLearningCourseType);
			saveProficiencyInRedis(teacherProficiencies, earlyLearningCourseType);
		}

		return teacherProficiencies;
	}

	private ELTeachersProficiencies getProficiencyFromRedis(SessionLabel earlyLearningCourseType)
			throws VException {
		ELTeachersProficiencies elTeachersProficiencies = new ELTeachersProficiencies();

		List<String> keys = new ArrayList<>();
		EnumSet.allOf(ProficiencyType.class).forEach(proficiencyType -> {
			String key = ELTeachersProficiencies.getELRedisKey(earlyLearningCourseType, proficiencyType);
			keys.add(key);
			elTeachersProficiencies.addKeyToProficiency(key, proficiencyType);
		});

		logger.info("keys : " + keys);
		Map<String, String> proficiencyJson = redisDAO.getValuesForKeys(keys);
		logger.info("proficiencyJson : {}", proficiencyJson);

		Optional.ofNullable(proficiencyJson).ifPresent(proficiencyJsonMap -> {
			keys.forEach(key -> {
				Optional.ofNullable(proficiencyJsonMap.get(key)).ifPresent(teacherIdsJson -> {
					elTeachersProficiencies.setTeachersForProficiecy(key, parseGsonToList(teacherIdsJson));
				});
			});
		});
		if (!elTeachersProficiencies.getProficiencyToTeacherMap().isEmpty()
				|| elTeachersProficiencies.teachersAvailable()) {
			return elTeachersProficiencies;
		}

		return null;
	}

	private List<String> parseGsonToList(String json) {
		Type type = new TypeToken<List<String>>() {
		}.getType();
		return gson.fromJson(json, type);
	}

	private String getTeacherIdsByProficiencyCategory(List<String> teacherIds, Long startTime, Long endTime) {
		List<CalendarEntry> availableELTeachers = new ArrayList<>();
		if (ArrayUtils.isEmpty(teacherIds)) {
			return StringUtils.EMPTY;
		}
		List<List<String>> allTeachersPartitions = Lists.partition(teacherIds, 100);
		for (List<String> teachersPartition : allTeachersPartitions) {
			availableELTeachers.addAll(calendarEntryManager.getAvilableTeachersForOverBooking(startTime, endTime, teachersPartition));
		}
		Collections.sort(availableELTeachers, (CalendarEntry o1, CalendarEntry o2) -> {
			int bookedCount1 = o1.getBooked().size();
			int bookedCount2 = o2.getBooked().size();
			int comp = bookedCount1 - bookedCount2;
			if(comp != 0) {
				return comp;
			}
			return o2.getAvailability().size() - o1.getAvailability().size();
		});
		logger.info("Sorted List In Assign Teacher : "+availableELTeachers);
		while (availableELTeachers.size() > 0) {
			if (redisDAO.setnx(availableELTeachers.get(0) + "_" + startTime,
					Long.toString(System.currentTimeMillis()), 10)) {
				return availableELTeachers.get(0).getUserId();
			}
			availableELTeachers.remove(0);
		}
		return StringUtils.EMPTY;
	}
	@Override
	public List<String> getAvailableTeacherForSlot(ProficiencyAnalyticsRequest request, ELTeachersProficiencies proficiencyIds) throws VException {

		List<String> availableTeacher = new ArrayList<>();
		if (StringUtils.isNotEmpty(request.getPresenter())) {
			availableTeacher.addAll(getTeachers(
					Arrays.asList(request.getPresenter()), request.getStartTime(), request.getEndTime()));
		} else {
		for (ProficiencyType proficiencyType : ProficiencyType.values()) {
				if (request.getSelectedProficiencyType() == null || proficiencyType.equals(request.getSelectedProficiencyType())) {
					availableTeacher.addAll(getTeachers(
							proficiencyIds.getTeachersForProficiency(proficiencyType), request.getStartTime(), request.getEndTime()));
				}
			}
		}
		int start = Math.min(request.getStart() ,availableTeacher.size() - 1);
		start = start < 0 ? 0 : start;
		int end = Math.min(request.getStart() + request.getLimit(), availableTeacher.size());
		end = end < 0 ? 0 : end;
		return availableTeacher.subList(start, end);
	}

	private List<String> getTeachers(List<String> teacherIds, Long startTime, Long endTime) {
		List<String> availableELTeachers = new ArrayList<>();
		if (ArrayUtils.isEmpty(teacherIds)) {
			return new ArrayList<>();
		}
		List<List<String>> allTeachersPartitions = Lists.partition(teacherIds, 100);
		for (List<String> teachersPartition : allTeachersPartitions) {
			availableELTeachers.addAll(calendarEntryManager.getAvailableUsers(startTime, endTime, teachersPartition));
		}
		return availableELTeachers;
	}
}
