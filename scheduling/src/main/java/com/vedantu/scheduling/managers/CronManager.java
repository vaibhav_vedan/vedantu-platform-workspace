/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.util.LogFactory;
import java.util.HashMap;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class CronManager {
    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CronManager.class);
    
    public void sendOtmNoShowEmailsAsync(){
        logger.info("sendOtmNoShowEmailsAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTM_NO_SHOW_EMAIL, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }
    
    public void sendLiveSessionEmails(){
        logger.info("sendOtmTrialClassEmailsAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_LIVE_SESSION_EMAILS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendEarlyLearningClassSmsAsync() {
        logger.info("sendEarlyLearningClassSmsAsync");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_EARLY_LEARNING_CLASS_SMS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendOtmTrialSessionEmailsAsync(){
        logger.info("sendOtmTrialClassEmailsAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTM_TRIAL_SESSION_EMAILS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendModularRegularSessionEmailsAsync(){
        logger.info("sendOtmTrialClassEmailsAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_MODULAR_REGULAR_SESSION_EMAILS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

}
