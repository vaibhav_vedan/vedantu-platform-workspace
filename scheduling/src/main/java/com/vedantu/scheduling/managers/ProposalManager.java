/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.Proposal;
import com.vedantu.scheduling.dao.serializers.ProposalDAO;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.proposal.AddProposalReq;
import com.vedantu.scheduling.request.proposal.CancelSubscriptionProposalsReq;
import com.vedantu.scheduling.request.proposal.GetProposalReq;
import com.vedantu.scheduling.request.proposal.GetProposalsReq;
import com.vedantu.scheduling.request.proposal.UpdateProposalReq;
import com.vedantu.scheduling.response.proposal.GetProposalsRes;
import com.vedantu.scheduling.response.proposal.ProposalRes;
import com.vedantu.session.pojo.proposal.ProposalSlot;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.ProposalStatus;
import org.dozer.DozerBeanMapper;

/**
 *
 * @author ajith
 */
@Service
public class ProposalManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ProposalManager.class);

	@Autowired
	private ProposalDAO proposalDAO;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

        @Autowired
        private DozerBeanMapper mapper;

	public ProposalRes addProposal(AddProposalReq addProposalReq) {
		logger.info("addProposalReq " + addProposalReq);
		Proposal proposal = Proposal.toProposal(addProposalReq);
		// proposal.setCreatedBy(addProposalReq.getFromUserId().toString());

		// TODO not doing conflict check for now
		boolean conflictOccured = false;
		// if (req.getNoConflictCheck() == null || !req.getNoConflictCheck()) {
		// updateSessionConflicts(proposal, true);
		// }

		if (addProposalReq.isCustomProposal()) {
			// Nullify the values in case of custom proposals. Flow depends
			// solely on proposalSlots
			proposal.setStartTime(null);
			proposal.setEndTime(null);
			proposal.setRepeatOrder(null);
			proposal.setRepeatFrequency(null);
		}

		if (conflictOccured) {
			proposal.setProposalStatus(ProposalStatus.UNSENT);
		}

		if (!ProposalStatus.UNSENT.equals(proposal.getProposalStatus())) {
			proposal.incrementFlowCount();
		}
		logger.info("inserting " + proposal);
		proposalDAO.insert(proposal, addProposalReq.getFromUserId());
		ProposalRes res = proposal.toProposalRes(mapper);
		if (!conflictOccured) {
			// insert amount deduction logic here.
			res.setConflictOccured(false);
		} else {
			res.setConflictOccured(true);
		}

		if (!ProposalStatus.UNSENT.equals(proposal.getProposalStatus())) {
			// TODO can this be done in single query
			List<ProposalSlot> proposalSlots = proposal.getProposalSlots();
			for (ProposalSlot slot : proposalSlots) {
				Map<String, Object> payload1 = new HashMap<>();
				CalendarEntryReq calendarEntryReq1 = new CalendarEntryReq(proposal.getFromUserId().toString(),
						slot.getStartTime(), slot.getEndTime(), CalendarEntrySlotState.SESSION_REQUEST,
						CalendarReferenceType.PROPOSAL, String.valueOf(proposal.getId()));
				payload1.put("calendarEntryReq", calendarEntryReq1);
				AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.ADD_CALENDAR_ENTRY_SLOT, payload1);
				asyncTaskFactory.executeTask(params1);

				Map<String, Object> payload2 = new HashMap<>();
				CalendarEntryReq calendarEntryReq2 = new CalendarEntryReq(proposal.getToUserId().toString(),
						slot.getStartTime(), slot.getEndTime(), CalendarEntrySlotState.SESSION_REQUEST,
						CalendarReferenceType.PROPOSAL, String.valueOf(proposal.getId()));
				payload2.put("calendarEntryReq", calendarEntryReq2);
				AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.ADD_CALENDAR_ENTRY_SLOT, payload2);
				asyncTaskFactory.executeTask(params2);
			}
		}
		return res;
	}

	public ProposalRes updateProposal(UpdateProposalReq updateProposalReq) {
		return null;
	}

	public ProposalRes confirmProposal(UpdateProposalReq req)
			throws NotFoundException, ForbiddenException, BadRequestException {
		Long id = req.getId();
		Proposal proposal = proposalDAO.getProposalById(id);

		logger.info("proposal " + proposal);

		if (proposal == null) {
			throw new NotFoundException(ErrorCode.PROPOSAL_DOES_NOT_EXIST,
					"Given proposal does not exist for this user");
		}

		// Validate the from and to user
		ProposalStatus status = req.getProposalStatus();
		Long callingUserId = req.getCallingUserId();
		if (!callingUserId.equals(proposal.getFromUserId())) {
			if (!callingUserId.equals(proposal.getToUserId())) {
				throw new ForbiddenException(ErrorCode.PROPOSAL_NO_ACCESS,
						"Given proposal is not accessible for this user");
			} else if (status != ProposalStatus.CANCELED) {
				Long toUserId = proposal.getFromUserId();
				proposal.setFromUserId(callingUserId);
				proposal.setToUserId(toUserId);
				proposal.incrementFlowCount();
			}
		}

		if (Arrays.asList(ProposalStatus.CONFIRMED, ProposalStatus.REJECTED, ProposalStatus.CANCELED)
				.contains(proposal.getProposalStatus())) {
			throw new NotFoundException(ErrorCode.PROPOSAL_ACCEPTED_REJECTED_CANCELED,
					"Given proposal has already been rejected or confirmed or canceled");
		}

		boolean conflictOccured = false;// TODO not making conflict checks will do later
		if (status == ProposalStatus.REJECTED || status == ProposalStatus.CANCELED) {
			String reason = req.getReason();
			if (reason != null && !reason.isEmpty()) {
				proposal.setReason(reason);
			}
			conflictOccured = false;
		} else {
			List<ProposalSlot> proposalSlots = proposal.getProposalSlots();
			// TODO add the conflict check here
			// conflictOccured = updateSessionConflicts(proposal, true);

			if (!conflictOccured) {
				if (req.getSubscriptionId() != null && req.getSubscriptionId() > 0) {
					proposal.setSubscriptionId(req.getSubscriptionId());
				}

				if (req.getOfferingId() != null) {
					proposal.setOfferingId(req.getOfferingId());
				}

				if (req.getPaymentMode() != null) {
					proposal.setPaymentMode(req.getPaymentMode());
				}

				// int sessionAmount = computeRequiredAmount(proposal, null);
				// TODO check the required amount is available or locked
				// Title is mandatory for a Session.
				String proposalTitle = proposal.getTitle();
				for (ProposalSlot proposalSlot : proposalSlots) {
					String title = proposalSlot.getTitle();
					if (StringUtils.isEmpty(title)) {
						if (StringUtils.isEmpty(proposalTitle)) {
							throw new BadRequestException(ErrorCode.TITLE_NOT_FOUND,
									"No title found for this session.");
						} else {
							title = proposalTitle;
							proposalSlot.setTitle(title);
						}
					}
				}
			}
		}
		proposal.setProposalStatus(status);
		if (req.isCustomProposal()) {
			// Nullify the values in case of custom proposals. Flow depends
			// solely on proposalSlots
			proposal.setStartTime(null);
			proposal.setEndTime(null);
			proposal.setRepeatOrder(null);
			proposal.setRepeatFrequency(null);
		}

		ProposalRes res = proposal.toProposalRes(mapper);
		res.setCustomProposal(req.isCustomProposal());
		if (!conflictOccured) {
			proposalDAO.updateProposal(proposal, req.getCallingUserId());
			// Mark previous slots
			for (ProposalSlot slot : proposal.getProposalSlots()) {
				Map<String, Object> payload1 = new HashMap<>();
				CalendarEntryReq calendarEntryReqFromUser = new CalendarEntryReq(proposal.getFromUserId().toString(),
						slot.getStartTime(), slot.getEndTime(), CalendarEntrySlotState.SESSION_REQUEST,
						CalendarReferenceType.PROPOSAL, String.valueOf(proposal.getId()));
				payload1.put("calendarEntryReq", calendarEntryReqFromUser);
				AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.REMOVE_CALENDAR_ENTRY_SLOT, payload1);
				asyncTaskFactory.executeTask(params1);

				Map<String, Object> payload2 = new HashMap<>();
				CalendarEntryReq calendarEntryReqToUser = new CalendarEntryReq(proposal.getToUserId().toString(),
						slot.getStartTime(), slot.getEndTime(), CalendarEntrySlotState.SESSION_REQUEST,
						CalendarReferenceType.PROPOSAL, String.valueOf(proposal.getId()));
				payload2.put("calendarEntryReq", calendarEntryReqToUser);
				AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.REMOVE_CALENDAR_ENTRY_SLOT, payload2);
				asyncTaskFactory.executeTask(params2);
			}
		} else {
			res.setConflictOccured(conflictOccured);
		}
		logger.info("Exit " + res);
		return res;
	}

	public ProposalRes getProposal(GetProposalReq req) throws NotFoundException, ForbiddenException {
		// Enabling admin user to fetch proposals for other users
		Long callingUserId = req.getCallingUserId();

		Long id = req.getId();
		Proposal proposal = proposalDAO.getProposalById(id);
		if (proposal == null) {
			throw new NotFoundException(ErrorCode.PROPOSAL_DOES_NOT_EXIST,
					"Given Proposal does not exist for this user");
		}
		if (!callingUserId.equals(proposal.getFromUserId()) && !callingUserId.equals(proposal.getToUserId())) {
			throw new ForbiddenException(ErrorCode.PROPOSAL_NO_ACCESS,
					"Given proposal is not accessible for this user");
		}
		ProposalRes res = proposal.toProposalRes(mapper);
		return res;
	}

	public GetProposalsRes getProposals(GetProposalsReq getProposalsReq) {

		Role callingUserRole = getProposalsReq.getCallingUserRole();
		boolean sendAllProposals = false;
		if (Role.ADMIN.equals(callingUserRole) || Role.STUDENT_CARE.equals(callingUserRole)) {
			sendAllProposals = true;
		}
		Long callingUserId = getProposalsReq.getCallingUserId();

		List<Proposal> proposals = proposalDAO.fetchProposals(getProposalsReq);

		GetProposalsRes res = new GetProposalsRes();
		if (!proposals.isEmpty()) {
			Iterator<Proposal> proposalIterator = proposals.iterator();
			while (proposalIterator.hasNext()) {
				Proposal proposal = proposalIterator.next();

				// Filter out UNSENT proposals based on From userId
				if (!sendAllProposals && ProposalStatus.UNSENT.equals(proposal.getProposalStatus())
						&& !proposal.getFromUserId().equals(callingUserId)) {
					continue;
				}

				ProposalRes proposalRes = proposal.toProposalRes(mapper);

				res.addItem(proposalRes);
			}
		}
		return res;
	}

	public BasicResponse cancelSubscriptionProposals(CancelSubscriptionProposalsReq req) throws BadRequestException {
		req.verify();
		// fetch all proposal first
		List<ProposalStatus> states = new ArrayList<>();
		states.add(ProposalStatus.PENDING);
		states.add(ProposalStatus.UNSENT);
		GetProposalsReq getProposalsReq = new GetProposalsReq();
		getProposalsReq.setStatusList(states);
		getProposalsReq.setSubscriptionId(req.getSubscriptionId());
		getProposalsReq.setSize(1000);// not sure if this sufficient
		List<Proposal> proposals = proposalDAO.fetchProposals(getProposalsReq);
		if (proposals.size() == getProposalsReq.getSize()) {
			logger.error("More proposals to cancel than 1000, please cancel the remaining");
		}

		// cancelling all proposals in one query
		proposalDAO.cancelSubscriptionProposals(req.getSubscriptionId());

		// unblock calendar slots
		// TODO do this in a single query
		for (Proposal proposal : proposals) {
			for (ProposalSlot slot : proposal.getProposalSlots()) {
				Map<String, Object> payload1 = new HashMap<>();
				CalendarEntryReq calendarEntryReqFromUser = new CalendarEntryReq(proposal.getFromUserId().toString(),
						slot.getStartTime(), slot.getEndTime(), CalendarEntrySlotState.SESSION_REQUEST,
						CalendarReferenceType.PROPOSAL, String.valueOf(proposal.getId()));
				payload1.put("calendarEntryReq", calendarEntryReqFromUser);
				AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.REMOVE_CALENDAR_ENTRY_SLOT, payload1);
				asyncTaskFactory.executeTask(params1);

				Map<String, Object> payload2 = new HashMap<>();
				CalendarEntryReq calendarEntryReqToUser = new CalendarEntryReq(proposal.getToUserId().toString(),
						slot.getStartTime(), slot.getEndTime(), CalendarEntrySlotState.SESSION_REQUEST,
						CalendarReferenceType.PROPOSAL, String.valueOf(proposal.getId()));
				payload2.put("calendarEntryReq", calendarEntryReqToUser);
				AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.REMOVE_CALENDAR_ENTRY_SLOT, payload2);
				asyncTaskFactory.executeTask(params2);
			}
		}

		return new BasicResponse();
	}
}
