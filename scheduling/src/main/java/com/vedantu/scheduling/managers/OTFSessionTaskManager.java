package com.vedantu.scheduling.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.onetofew.pojo.BatchEnrolmentInfo;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class OTFSessionTaskManager {

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
    private Logger logger = LogFactory.getLogger(OTFSessionManager.class);

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private OTFSessionDAO otfSessionDAO;

    private static final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static Gson gson = new Gson();

    // todo-- retrieve enrolled students in pages
    public List<BatchEnrolmentInfo> getBatchEnrolmentsByIds(Set<String> batchIds) throws VException {
        if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }

        ClientResponse resp = WebUtils.INSTANCE
                .doCall(subscriptionEndPoint + "batch/getEnrolmentsByBatchIds?", HttpMethod.POST, gson.toJson(batchIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BatchEnrolmentInfo>>() {
        }.getType();

        return gson.fromJson(jsonString, listType);
    }

    public List<String> getSessionAttedeeCacheKeys(OTFSessionPojoUtils sessionPojo) {
        List<String> cacheKeys = new ArrayList<>();
        String teacher;
        if (sessionPojo.getPresenter() != null) {
            teacher = redisDAO.getUserLatestSessionCacheKey(sessionPojo.getPresenter());
            if (!org.springframework.util.StringUtils.isEmpty(teacher)) {
                cacheKeys.add(teacher);
            }
            String teacherKey = redisDAO.getCachedKeyForUserLatestSessions(sessionPojo.getPresenter(), env);
            if (!org.springframework.util.StringUtils.isEmpty(teacherKey)) {
                cacheKeys.add(teacherKey);
            }
        }
        if (sessionPojo.getPreviousPresenter() != null) {
            String prevTeacher = redisDAO.getUserLatestSessionCacheKey(sessionPojo.getPreviousPresenter());
            if (!org.springframework.util.StringUtils.isEmpty(prevTeacher)) {
                cacheKeys.add(prevTeacher);
            }
            String prevTeacherKey = redisDAO.getCachedKeyForUserLatestSessions(sessionPojo.getPreviousPresenter(), env);
            if (!org.springframework.util.StringUtils.isEmpty(prevTeacherKey)) {
                cacheKeys.add(prevTeacherKey);
            }

        }
        try {
            if (ArrayUtils.isNotEmpty(sessionPojo.getAttendees())) {
                Set<String> attendeesSet = new HashSet<>(sessionPojo.getAttendees());
                List<BatchEnrolmentInfo> batchEnrolmentInfo = getBatchEnrolmentsByIds(sessionPojo.getBatchIds());
                if (ArrayUtils.isNotEmpty(batchEnrolmentInfo)) {
                    for (BatchEnrolmentInfo enrollmentPojo : batchEnrolmentInfo) {
                        attendeesSet.addAll(enrollmentPojo.getEnrolledStudentIds());
                    }
                }
                sessionPojo.setAttendees(new ArrayList<>(attendeesSet));
            }
        } catch (Exception ex) {
            logger.error("Error in getting user active enrollments: " + ex.getMessage());
        }

        if (ArrayUtils.isNotEmpty(sessionPojo.getAttendees())) {
            for (String attendee : sessionPojo.getAttendees()) {
                String student = redisDAO.getUserLatestSessionCacheKey(attendee);
                if (!StringUtils.isEmpty(student)) {
                    cacheKeys.add(student);
                }
                String studentSessionKey = redisDAO.getCachedKeyForUserLatestSessions(attendee, env);
                if (!StringUtils.isEmpty(studentSessionKey)) {
                    cacheKeys.add(studentSessionKey);
                }
            }
        }
        return cacheKeys;
    }

    public void handleSessionEventsForOTF(SessionEventsOTF eventType, String message) {
        OTFSessionPojoUtils sessionPojo = gson.fromJson(message, OTFSessionPojoUtils.class);
        if (sessionPojo != null) {
            if (ArrayUtils.isEmpty(sessionPojo.getAttendees())) {
                String id = sessionPojo.getId();
                if (id == null) {
                    return;
                }
                OTFSession otfSession = otfSessionDAO.getById(id);
                sessionPojo.setAttendees(otfSession.getAttendees());
            }
            List<String> cacheKeys = getSessionAttedeeCacheKeys(sessionPojo);
            if (ArrayUtils.isEmpty(cacheKeys)) {
                return;
            }
            logger.info("cacheKeys: " + cacheKeys);
            switch (eventType) {
                //TODO:reschedule of webinar
                case WEBINAR_SESSION_CANCELED:
                case WEBINAR_SESSION_SCHEDULED:
                case OTF_SESSION_SCHEDULED:
                case OTF_SESSION_CANCELED:
                case OTF_SESSION_RESCHEDULED:
                case ADD_BATCH_IDS_TO_SESSION:
                case OTF_SESSION_ENDED:
                case CLEAR_CACHE:
                    deleteUserSessionCachedKeys(cacheKeys);
                    break;
                default:
                    logger.info("Default handling as event is not handled : " + eventType);
                    deleteUserSessionCachedKeys(cacheKeys);
            }
        }
    }

    private void deleteUserSessionCachedKeys(List<String> cacheKeys) {
        if (ArrayUtils.isNotEmpty(cacheKeys)) {
            try {
                List<List<String>> partition = Lists.partition(cacheKeys, 500);
                for (List<String> list : partition) {
                    redisDAO.deleteKeys(list.toArray(new String[]{}));
                }
            } catch (Exception e) {
                logger.info("Error in deleting Keys." + e);
            }
        }
    }

    public void handleBatchEventsForOTF(BatchEventsOTF eventType, String message)  {

        if (Objects.isNull(eventType)) {
            logger.error("subject for sns req handleBatchEventsForOTF is empty");
        }
        switch(eventType){

            case USER_ACTIVE:
            case USER_INACTIVE:
            case USER_ENROLLED:
            case USER_ENDED:
                EnrollmentPojo enrollment = gson.fromJson(message, EnrollmentPojo.class);
                if(enrollment != null && !StringUtils.isEmpty(enrollment.getUserId())){
                    String[] cacheKey = new String[1];
                    String key = redisDAO.getUserLatestSessionCacheKey(enrollment.getUserId());
                    if(!StringUtils.isEmpty(key)){
                        cacheKey[0] = key;
                        try{
                            redisDAO.deleteKeys(cacheKey);
                        }catch(InternalServerErrorException e){
                            logger.warn("Error in deleting Keys : {}", e.getMessage());
                        }
                    }
                    key = redisDAO.getCachedKeyForUserLatestSessions(enrollment.getUserId(),env);
                    if(!StringUtils.isEmpty(key)){
                        cacheKey[0] = key;
                        try{
                            redisDAO.deleteKeys(cacheKey);
                        }catch(Exception e){
                            logger.info("Error in deleting Keys."+e);
                        }
                    }
                }
                break;

            case BATCH_ACTIVE:
            case BATCH_INACTIVE:
                List<String> cacheKeys = new ArrayList<>();
                BatchInfo batchInfo = gson.fromJson(message, BatchInfo.class);
                if(batchInfo != null){
                    // neither set anywhere in the code, nor in use, but we were deleting in platform - so omitting this operation
                    // cacheKeys.add(getBatchCacheKey(batchInfo.getBatchId()));
                    if(ArrayUtils.isNotEmpty(batchInfo.getTeachers())){
                        for(UserBasicInfo user:batchInfo.getTeachers()){
                            if(user.getUserId() != null){
                                cacheKeys.add(redisDAO.getUserLatestSessionCacheKey(user.getUserId()));
                                cacheKeys.add(redisDAO.getCachedKeyForUserLatestSessions(user.getUserId().toString(),env));
                            }
                        }
                    }

                    if(ArrayUtils.isNotEmpty(cacheKeys)){
                        String[] deleteKeys = new String[cacheKeys.size()];
                        try{
                            redisDAO.deleteKeys(cacheKeys.toArray(deleteKeys));
                        } catch(Exception e){
                            logger.info("Error in deleting Keys."+e);
                        }
                    }
                }
                break;

            default:
                logger.error("invalid subject handleBatchEventsForOTF : {}", eventType);
        }
    }

}


