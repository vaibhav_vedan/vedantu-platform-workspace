package com.vedantu.scheduling.managers.earlylearning;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import com.vedantu.subscription.enums.EarlyLearningCourseType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.managers.AwsSQSManager;
import com.vedantu.scheduling.request.earlylearning.AddDemoRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;

@Service
public class LeadSquareUtilityManager {

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@Autowired
	private AwsSQSManager awsSQSManager;

	private Logger logger = LogFactory.getLogger(LeadSquareUtilityManager.class);

	public void pushAddDemoSession(AddDemoRequest request, String sessionStartTime, String presenter, String sessionId,
			Long studentId) {
		try {
			Map<String, String> userLead = new HashMap<>();
			userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, String.valueOf(studentId));
			userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, sessionStartTime);
			userLead.put(LeadSquaredRequest.Constants.DEMO_TEACHER, presenter);
			userLead.put(LeadSquaredRequest.Constants.SESSION_ID, sessionId);
			userLead.put(LeadSquaredRequest.Constants.CALLING_USER_ID,
					String.valueOf(httpSessionUtils.getCallingUserId()));
			userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "DEMO BOOKED");

			logger.info("BOOK_DEMO_SESSION_SUPERKIDS userLeads : " + userLead);
			LeadSquaredRequest leadSquaredRequest = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
					LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_BOOK, null, userLead, null);
			String messageGroupId = request.getStudentId() + "_DEMO";
			awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOK_DEMO_SESSION_SUPERKIDS,
					new Gson().toJson(leadSquaredRequest), messageGroupId);
		} catch (Exception ex) {
			logger.error("error sending data in Leadsquare sessionId: " + sessionId + " error:" + ex.getMessage());
		}
	}

	public void pushAddDemoBooking(Long sessionStartTime, String bookingId, Long studentId, String alterateNumber,
								   String courseType) {
		try {
			Map<String, String> userLead = new HashMap<>();
			userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, String.valueOf(studentId));
			userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME,
					getFormattedDateForLeadSquared(sessionStartTime));
			userLead.put(LeadSquaredRequest.Constants.COURSE_NAME,
					courseType);
			userLead.put(LeadSquaredRequest.Constants.BOOKING_ID, bookingId);
			userLead.put(LeadSquaredRequest.Constants.CALLING_USER_ID,
					String.valueOf(httpSessionUtils.getCallingUserId()));
			userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "DEMO BOOKED");
			if (StringUtils.isNotEmpty(alterateNumber)) {
				userLead.put(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER, alterateNumber);
			}
			logger.info("BOOK_DEMO_SESSION_SUPERKIDS userLeads : " + userLead);
			LeadSquaredRequest leadRequest = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
					LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_BOOK, null, userLead, null);
			String messageGroupId = studentId + "_DEMO";
			awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOK_DEMO_SESSION_SUPERKIDS,
					new Gson().toJson(leadRequest), messageGroupId);
		} catch (Exception ex) {
			logger.error("error sending data in Leadsquare bookingId: " + bookingId + " error:" + ex.getMessage());
		}
	}

	public void cancelDemoBooking(Long startTime, String bookingId, String studentId) {

		try {
			Map<String, String> userLead = new HashMap();
			userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, getFormattedDateForLeadSquared(startTime));
			userLead.put(LeadSquaredRequest.Constants.BOOKING_ID, bookingId);
			userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Signed Cancelled");
			userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, studentId);
			logger.info("Superkids Cancel Session : " + userLead);
			LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
					LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_CANCEL, null, userLead, null);
			String messageGorupId = bookingId + "_CANCEL";
			awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.CANCEL_SESSION_SUPERKIDS,
					new Gson().toJson(request), messageGorupId);

		} catch (Exception ex) {
			logger.error("error sending data in Leadsquare OverbookingId" + bookingId + " error:" + ex.getMessage());
		}
	}

	public void noTeacherAvailableLeadSquaredEvent(Long studentId, Long demoSessionTime, String bookingId) {

		try {
			Map<String, String> userLead = new HashMap<>();
			userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, String.valueOf(studentId));
			userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME,
					getFormattedDateForLeadSquared(demoSessionTime));
			userLead.put(LeadSquaredRequest.Constants.BOOKING_ID, bookingId);
			userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Overbooking Cancelled");

			logger.info("noTeacherAvailableLeadSquaredEvent userLeads : " + userLead);
			LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
					LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_CANCEL, null, userLead, null);
			String messageGroupId = studentId + "_NO_TEACHER_AVAILABLE";
			awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOKING_CANCELLED_BY_OVERBOOKING,
					new Gson().toJson(request), messageGroupId);
		} catch (Exception ex) {
			logger.error("error sending data in LeadSquare for bookingId: " + bookingId + " error:" + ex.getMessage());
		}
	}

	public String getFormattedDateForLeadSquared(Long epoch) {
		Date date = new Date(epoch);
		DateFormat format;
		format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		String formattedDate = format.format(date);
		return formattedDate;
	}
}
