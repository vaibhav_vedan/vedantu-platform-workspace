package com.vedantu.scheduling.managers;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.onetofew.pojo.EnrollmentPojo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.scheduling.pojo.GTMCreateSessionReq;
import com.vedantu.scheduling.pojo.GTMCreateSessionRes;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.pojo.GTTSessionInfoRes;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service
public class GTMScheduleManager extends ScheduleManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTMScheduleManager.class);

	private static Gson gson = new Gson();
	public static String citrixURL = ConfigUtils.INSTANCE.getStringValue("otf.citrix.url");

	private String[] authAccessArray = { 
			"9XOBcSJv2G4efT559RoIwhNioAVM", // Gaurav
			"Ik3uXhEABi7J1xMNwA2qQ7tWYl0X", // Shubha"
			"n6zcimB2ndw60Hn6i9FF3awPWi1X", // Amol
			"2dv66ID8XJhyMKGF2TZ09HzdxoPn", // Rhine
			"bW6d6g31zMGXU58iqCHU7UfdwiNf", // Shyam
			"rOPdCFr8oyQiSbn4752e2A7AGFby", // Kuntal
			"HLHIA32nbf8D3p4IaAAR1MAjyAoT", // Sundar
			"R0OBs7kedUVipZRKUZosBD3CfpGT", // Sumit
			"D2TGJhtDavjDQxIxGQjQPImaRo0m", // Sridhar
			"bmZSYidw6EKSmCqeu3xyxAbKoUcr", // Nisharg
			"WTTZiiYkVM1YL4I0x4BaLC8bG5dG", // v02
			"MxYXRMfni3QLsTcbKo4f3zCIquW0", // v03
			"uGZ3lqslEnAZpdET5gtjTAwvUk7F", // v04
			"hwTZf75IC9GuCtkN5Se7YxGTCFvA", // v05
			"9A6iRHcLJYYGNJR0F1pG9JAMDUEh", // v06
			"E5BIHZtciQNFjWu8zUg8mQPxzCxA", // v07
			"NEI1QA3EZ4Gzl2dbsD4GfPwNYq8K", // v08
			"ADI3xAzaTtEuUjOGFXlqL4FHYpvl", // v09
			"ehLqTUNyAEsBWQLv1HaHAvu73Sdq", // v10
			"PEDGpxvY2bErJsw1n1MHnAzOJdvi", // v01
			"DXbg2RMPCbp0DTKgc6IlsjWkS0uO", // v11
			"yFKEJ5v2su4ycTvAqmEYu4kLwLTi", // v12
			"Cohnv0Aquy6dYl9XLLHGZLT5KVPX", // v13
			"AVg0OghsCnLcdXQYmImqeArGokwP", // v14
			"xTq3nSGlYYFIHKFKhWoMjLJ93GPR", // v15
			"seat26", "seat27", "seat28", "seat29", "seat30", "seat31", "seat32", "seat33", "seat34", "seat35" 
			};

	private static Map<String, String> overwriteAccessKeys = Collections.unmodifiableMap(new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("9XOBcSJv2G4efT559RoIwhNioAVM", "hP2ImenBsgGUjgjY3FHG3y6fJem9"); // gtm1
			put("Ik3uXhEABi7J1xMNwA2qQ7tWYl0X", "i85wETxDCWSloXqekTZj09Zx7hJM"); // gtm2
			put("n6zcimB2ndw60Hn6i9FF3awPWi1X", "jUhGJAM6IsSp2YziR5B4gcPxoC7e"); // gtm3
			put("2dv66ID8XJhyMKGF2TZ09HzdxoPn", "RFvLn92PSTjF5h1c4lFQIW5AePUb"); // gtm4
			put("bW6d6g31zMGXU58iqCHU7UfdwiNf", "jqwPOnKmaGGyurilSDUMnjogdAVV"); // gtm5
			put("rOPdCFr8oyQiSbn4752e2A7AGFby", "ngwxOZDE57bgolNyAfA493Au09wZ"); // gtm6
			put("HLHIA32nbf8D3p4IaAAR1MAjyAoT", "haEeyIj6kGkgmH2b4ksZpzq13A06"); // gtm7
			put("R0OBs7kedUVipZRKUZosBD3CfpGT", "No2zMqVf6ryd8iDq1ZqWQ0ZTpNlA"); // gtm8
			put("D2TGJhtDavjDQxIxGQjQPImaRo0m", "nOwJMxnkHQdC9PU9rEugAPamBL02"); // gtm9
			put("bmZSYidw6EKSmCqeu3xyxAbKoUcr", "4gHyrBEM0SvnoWGsSQefjkNDmeX9"); // gtm10
			put("WTTZiiYkVM1YL4I0x4BaLC8bG5dG", "Y9GNn57E5BMGzUeYFWKAj1NsVtBq"); // gtm11
			put("MxYXRMfni3QLsTcbKo4f3zCIquW0", "GRi2z909GorMINx5jb0ZmpRDAkiE"); // gtm12
			put("uGZ3lqslEnAZpdET5gtjTAwvUk7F", "8Z7styHLC9UGGbBcuaAKFwG4C4G2"); // gtm13
			put("hwTZf75IC9GuCtkN5Se7YxGTCFvA", "bMgD15EmKHbY5ntNgGbjrOc4cBGL"); // gtm14
			put("9A6iRHcLJYYGNJR0F1pG9JAMDUEh", "1v0sRTGcKvBFEFU0dJEhwCYlYnM0"); // gtm15
			put("E5BIHZtciQNFjWu8zUg8mQPxzCxA", "Mc0sxtARfcmR92h8H4lXNmw1IGxa"); // gtm16
			put("NEI1QA3EZ4Gzl2dbsD4GfPwNYq8K", "ltgIwzeCimDHbGiLWNX58Kha5N51"); // gtm17
			put("ADI3xAzaTtEuUjOGFXlqL4FHYpvl", "sunnHPT9p3vXhdLwOP5y1iWv3ozs"); // gtm18
			put("ehLqTUNyAEsBWQLv1HaHAvu73Sdq", "jKxi36ITfQDJrXO4JM9wahuMWwGX"); // gtm19
			put("PEDGpxvY2bErJsw1n1MHnAzOJdvi", "G4EN2sHVcvsdf8zo6EwGw2CXWhQK"); // gtm20

			put("DXbg2RMPCbp0DTKgc6IlsjWkS0uO", "4KJdRq3jURHzLcytBIsWBWtqGgyw"); // gtm21
			put("yFKEJ5v2su4ycTvAqmEYu4kLwLTi", "HCus3OoSbUh64QWG3rHpiqlkcN1T"); // gtm22
			put("Cohnv0Aquy6dYl9XLLHGZLT5KVPX", "Vv4BgzA037A3VPfoD44FOGB2Av9f"); // gtm23
			put("AVg0OghsCnLcdXQYmImqeArGokwP", "0bSGWfLHfBYQt3QxPNMkvlTmQ68d"); // gtm24
			put("xTq3nSGlYYFIHKFKhWoMjLJ93GPR", "YCEjLmdW9xrKyDG7loIhgfK2Fjv5"); // gtm25

			put("seat26", "O4BseKhFCEPRNIMcOohn8AKUCVs8"); // gtm 26
			put("seat27", "PRP1zHrXGJYAHpH5EXNSZUOdxvqj"); // gtm 27
			put("seat28", "QgNIiM0GHlXAMLdLwVSXVmOMW28h"); // gtm 28
			put("seat29", "cKXzQLRJSFGepSsFeJlWCGymSc3S"); // gtm 29
			put("seat30", "eQvjPfP92mcRc9NML7lxTgB3Jn27"); // gtm 30
			put("seat31", "9UmOuOSHyGKRAtTP866jbeObdRdg"); // gtm 31
			put("seat32", "4lpZ20BHHGcGK1eXLG7kG3Ku4Ujh"); // gtm 32
			put("seat33", "03nIWbMUCzSKPalrAPxBdgqjqnqF"); // gtm 33
			put("seat34", "KCsuNyp06MdAq0Q53mHAxG5JqA6V"); // gtm 34
			put("seat35", "ugihOovSgmWEUvGTX5mLizafAU54"); // gtm 35
		}
	});

	private List<String> authAccessList = Arrays.asList(authAccessArray);

	private static Type gtmCreateSessionResListType = new TypeToken<List<GTMCreateSessionRes>>() {
	}.getType();

	public String getAccessToken(int slot) {
		if (slot != -1 && slot < authAccessList.size()) {
			return authAccessList.get(slot);
		}
		return null;
	}

	public int getAccessTokenSlot(String accessToken) {
		int returnVal = -1;
		if (!StringUtils.isEmpty(accessToken)) {
			returnVal = authAccessList.indexOf(accessToken);
		}
		return returnVal;
	}

	public String getPostOverwriteAccessToken(String accessToken) {
		if (overwriteAccessKeys.containsKey(accessToken)) {
			return overwriteAccessKeys.get(accessToken);
		} else {
			return accessToken;
		}
	}

	public int getAccessArrayLength() {
		return authAccessArray.length;
	}

	public BitSet getScheduleBitSet(Schedule schedule) {
		return schedule.getScheduleBitSet();
	}

	@Override
	public void createSessionLink(OTFSession oTFSession) throws Exception {
		// Use session start time, session end time, session title and session
		// organizer seat

		String url = citrixURL + "/G2M/rest/meetings";
		GTMCreateSessionReq gtmCreateSessionReq = new GTMCreateSessionReq(oTFSession);
		logger.info("Request : " + gtmCreateSessionReq.toString());
		ClientResponse cRes = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(gtmCreateSessionReq),
				getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken()));
		String resp = cRes.getEntity(String.class);
		logger.info("createLinks clientResponse : " + resp);
		List<GTMCreateSessionRes> gtmCreateSessionRes = gson.fromJson(resp, gtmCreateSessionResListType);
		if (CollectionUtils.isEmpty(gtmCreateSessionRes)) {
			throw new Exception("Error response from GTM : " + resp + " session:" + oTFSession.toString());
		}
		gtmCreateSessionRes.get(0).validate();
		oTFSession.updateGTMDetails(gtmCreateSessionRes.get(0));
	}

	@Override
	public String getTeacherJoinUrl(OTFSession oTFSession, String teacherId) throws NotFoundException {
		String meetingId = oTFSession.getMeetingId();
		if (StringUtils.isEmpty(meetingId)) {
			meetingId = extractMeetingId(oTFSession);
			if (StringUtils.isEmpty(meetingId)) {
				String errorMessage = "Meeting id not found for session : " + oTFSession.toString() + " joining teacher : "
						+ teacherId;
				logger.info(errorMessage);
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
			}
		}

		try {
			String serverUrl = citrixURL + "/G2M/rest/meetings/" + meetingId + "/start";
			logger.info(
					"Calling url: " + serverUrl + " for organizer access key: " + oTFSession.getOrganizerAccessToken());
			ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
					getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken()));
			logger.info("Response is: " + cRes.toString());
			String resp = cRes.getEntity(String.class);
			// {"hostURL":"https://global.gotomeeting.com/host/983495853?authorizationCode=eyJhbGciOiJSUzUxMiJ9"}

			JsonParser jsonParser = new JsonParser();
			JsonObject jsonObject = jsonParser.parse(resp).getAsJsonObject();
			String joinUrl = jsonObject.get("hostURL").getAsString();
			logger.info("Teacher url is: " + joinUrl);
			return joinUrl;
		} catch (Exception ex) {
			logger.info("Error occured generating the teacher link - " + ex.getMessage() + " session:"
					+ oTFSession.toString());
			return null;
		}
	}

	@Override
	public String getStudentJoinUrl(OTFSession oTFSession, String studentId) throws NotFoundException {
		return oTFSession.getSessionURL();
	}

	private static String extractMeetingId(OTFSession oTFSession) {
		if (oTFSession != null) {
			return oTFSession.getSessionURL().substring(oTFSession.getSessionURL().lastIndexOf('/') + 1,
					oTFSession.getSessionURL().length());
		}
		return "";
	}


	@Override
	public String getOrganizerKeyForToken(String accessToken) {
		return null;
	}

	@Override
	public List<GTTAttendeeDetails> registerAttendees(OTFSession oTFSession,Map<String,List<EnrollmentPojo>> studentMap)
			throws InternalServerErrorException, NotFoundException {
		// No registration required
		return null;
	}

	@Override
	public GTTAttendeeDetails registerAttendee(OTFSession oTFSession, String userId, List<EnrollmentPojo> enrollmentPojos)
			throws NotFoundException, InternalServerErrorException {
		// No registration required
		return null;
	}

	@Override
	public void handleFeedback(OTFSession oTFSession) throws NotFoundException, InternalServerErrorException {
		// No feedback cycle in gtm
	}

	@Override
	public void userJoined(OTFSession oTFSession, String userId) {
		// TODO Auto-generated method stub
	}

        @Override
        public List<GTTAttendeeDetails> createGTTAttendeeDetails(OTFSession session,Map<String,List<EnrollmentPojo>> userEnrollmentMap,AttendeeContext context) throws InternalServerErrorException {
            return null;
        }

        @Override
        public List<GTTSessionInfoRes> getGTTSessionInfos(OTFSession oTFSession) throws NotFoundException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
}
