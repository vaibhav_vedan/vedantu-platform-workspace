package com.vedantu.scheduling.managers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.pojo.BatchEnrolmentInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import com.vedantu.scheduling.dao.entity.GTTSessionRecordingInfo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.dao.serializers.GTTAttendeeDetailsDAO;
import com.vedantu.scheduling.dao.serializers.GTTOrganizerTokenDAO;
import com.vedantu.scheduling.dao.serializers.GTTSessionRecordingInfoDAO;
import com.vedantu.scheduling.pojo.GTTCreateSessionReq;
import com.vedantu.scheduling.pojo.GTTGetRecordingRes;
import com.vedantu.scheduling.pojo.GTTGetRegistrantDetailsRes;
import com.vedantu.scheduling.pojo.GTTRecording;
import com.vedantu.scheduling.pojo.GTTRegisterAttendeeReq;
import com.vedantu.scheduling.pojo.GTTRegisterAttendeeRes;
import com.vedantu.scheduling.pojo.GTTSessionAttendeeInfo;
import com.vedantu.scheduling.pojo.GTTSessionInfoRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;

@Service("gttScheduleManager")
public class GTTScheduleManager extends ScheduleManager {

	@Autowired
	private GTTAttendeeDetailsManager gttAttendeeDetailsManager;
	
	@Autowired
	private OTFSessionManager oTFSessionManager;

	@Autowired
	private GTTSessionRecordingInfoDAO gttSessionRecordingInfoDAO;

	@Autowired
	private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;
        
	@Autowired
	private GTTOrganizerTokenDAO gttOrganizerTokenDAO;

	@Autowired
	private OTFSessionTaskManager otfSessionTaskManager;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTTScheduleManager.class);

	private static Gson gson = new Gson();
	public static String citrixURL = ConfigUtils.INSTANCE.getStringValue("otf.citrix.url");
	private static Type GTT_SESSION_LIST_TYPE = new TypeToken<List<GTTSessionInfoRes>>() {
	}.getType();

	private static Type GTT_SESSION_ATTENDEE_LIST_TYPE = new TypeToken<List<GTTSessionAttendeeInfo>>() {
	}.getType();

	private String[] authAccessArray = { //"gttSeat1", "gttSeat2"
        };

	private static Map<String, String> overwriteAccessKeys = Collections.unmodifiableMap(new HashMap<String, String>() {

		private static final long serialVersionUID = 4675157667161370845L;

		{
//                        put("gttSeat1", "qZoYsPIz3e4klbr14XOa8L2Pow2J"); // gtt1
//			put("gttSeat2", "2izZJnXckYWyGzSqLCno2QCWf2sP"); // gtt2
                        

		}
	});

	private static Map<String, String> organizerKeys = Collections.unmodifiableMap(new HashMap<String, String>() {
		private static final long serialVersionUID = 2357808695344388664L;

		{
//                    put("gttSeat1", "6879452220571615237"); // gtt1
//                    put("gttSeat2", "384883325903405061");

		}
	});

	private List<String> authAccessList = Arrays.asList(authAccessArray);

	@Override
	public int getAccessArrayLength() {
		return authAccessArray.length;
	}

	@Override
	public String getAccessToken(int slot) {
		if (slot != -1 && slot < authAccessList.size()) {
			return authAccessList.get(slot);
		}
		return null;
	}

	@Override
	public int getAccessTokenSlot(String accessToken) {
		int returnVal = -1;
		if (!StringUtils.isEmpty(accessToken)) {
			returnVal = authAccessList.indexOf(accessToken);
		}
		return returnVal;
	}

	@Override
        public String getPostOverwriteAccessToken(String accessToken) {
            GTTOrganizerToken token = gttOrganizerTokenDAO.getTokenForSeat(accessToken);
            if (token != null && !StringUtils.isEmpty(token.getOrganizerAccessToken()) && token.getExpiresIn() > System.currentTimeMillis()) {
                return token.getOrganizerAccessToken();
            } else {
                logger.error("Just as reference to look into : " + token);
                if (overwriteAccessKeys.containsKey(accessToken)) {
                    
                    return overwriteAccessKeys.get(accessToken);
                }
                return accessToken;
            }
        }

	@Override
	public BitSet getScheduleBitSet(Schedule schedule) {
		return schedule.getGttScheduleBitset();
	}

	@Override
	public void createSessionLink(OTFSession oTFSession) throws InternalServerErrorException, BadRequestException {
		// Validate session for creation
		if (oTFSession.getStartTime() < System.currentTimeMillis()) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session is in past");
		}

		// https://api.citrixonline.com/G2T/rest/organizers/6879452220571615237/trainings
		String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
		String organizerKey = getOrganizerKeyForToken(oTFSession.getOrganizerAccessToken());
		if (StringUtils.isEmpty(organizerAccessToken) || StringUtils.isEmpty(organizerKey)) {
			String errorMessage = "Organizertoken or key is missing :" + organizerAccessToken + " key : " + organizerKey
					+ " session:" + oTFSession.toString();
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		String createSessionUrl = citrixURL+"/G2T/rest/organizers/" + organizerKey + "/trainings";
		GTTCreateSessionReq gttCreateSessionReq = new GTTCreateSessionReq(oTFSession, organizerAccessToken, organizerKey);
		logger.info("Request : " + gttCreateSessionReq.toString());
		ClientResponse cRes = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
				gson.toJson(gttCreateSessionReq), organizerAccessToken);
		String resp = cRes.getEntity(String.class);
		switch (cRes.getStatus()) {
		case 200:
		case 201:
			break;
		case 429:
			try {
				Thread.sleep(1000);
				createSessionLink(oTFSession);
				return;
			} catch (Exception ex) {
				logger.info("Exception ex:" + ex.toString() + " message:" + ex.getMessage());
			}
		case 400:
			String errorMessage = "Invalid response from gtt during creation of training : "
					+ cRes.getEntity(String.class) + " session:" + oTFSession.toString() + " req:"
					+ gson.toJson(gttCreateSessionReq);
			logger.error(errorMessage);
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		default:
			String defaultErrorMessage = "Invalid response from gtt during creation of training : " + resp + " session:"
					+ oTFSession.toString() + " req:" + gson.toJson(gttCreateSessionReq);
			logger.error(defaultErrorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, defaultErrorMessage);
		}

		logger.info("createLinks clientResponse : " + resp);
		String trainingId = resp.replace("\"", "");
		oTFSession.setMeetingId(trainingId);
		oTFSession.setSessionURL(getTrainingUrl(trainingId, organizerKey));
		// TODO : Reset other relavent fields
	}

	@Override
	public String getTeacherJoinUrl(OTFSession oTFSession, String teacherId) throws NotFoundException {
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED, "Meeting not scheduled for the session");
		}
		
		if(OTFSessionToolType.GTW.equals(oTFSession.getSessionToolType())){
			try{
				return getAttendeeUrl(oTFSession, teacherId);
			}catch (Exception ex) {
				logger.info(
						"Error occured generating teacher link - " + ex.getMessage() + " session:" + oTFSession.toString());
				return null;
			}			
		}

		// https://api.citrixonline.com/G2T/rest/trainings/893408553371535874/start
		String meetingId = getSessionMeetingId(oTFSession);
		try {
//			String attendeeUrl = getAttendeeUrl(session, teacherId);
//			return attendeeUrl;
			String serverUrl = citrixURL+"/G2T/rest/trainings/" + meetingId + "/start";
			logger.info(
					"Calling url: " + serverUrl + " for organizer access key: " + oTFSession.getOrganizerAccessToken());
			ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
					getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken()));
			logger.info("Response is: " + cRes.toString());
			String resp = cRes.getEntity(String.class);

			// {"hostURL":"https://global.gotomeeting.com/host/983495853?authorizationCode=eyJhbGciOiJSUzUxMiJ9"}
			switch (cRes.getStatus()) {
			case 200:
				JsonParser jsonParser = new JsonParser();
				JsonObject jsonObject = jsonParser.parse(resp).getAsJsonObject();
				String joinUrl = jsonObject.get("hostURL").getAsString();
				try {
					registerAttendee(oTFSession, teacherId,null);
				} catch (Exception ex) {
					logger.info("Exception :" + ex.toString());
				}

				logger.info("Teacher url is: " + joinUrl);
				return joinUrl;
			case 409:
				// 409 is thrown if the meeting is already live. Need to send
				// the attendee URL of the teacher.
				logger.info("Teacher trying to join when the meeting is already live - " + resp + " session:"
						+ oTFSession.toString());
				String attendeeUrl = getAttendeeUrl(oTFSession, teacherId);
				logger.info("Attendee Url : " + attendeeUrl);
				return attendeeUrl;
			default:
				String errorMessage = "Error occured while registering the student. response : " + cRes.toString()
						+ "user:" + teacherId + " session:" + oTFSession.toString();
				logger.info(errorMessage);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
			}
		} catch (Exception ex) {
			logger.info(
					"Error occured generating teacher link - " + ex.getMessage() + " session:" + oTFSession.toString());
			return null;
		}
	}

	@Override
	public String getStudentJoinUrl(OTFSession oTFSession, String studentId)
			throws InternalServerErrorException, NotFoundException {
		return getAttendeeUrl(oTFSession, studentId);
	}

	private String getAttendeeUrl(OTFSession oTFSession, String userId)
			throws InternalServerErrorException, NotFoundException {
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED, "Meeting not scheduled for the session");
		}

		// Fetch from the attendee details
		GTTAttendeeDetails gttAttendeeDetails = gttAttendeeDetailsManager.getAttendeeDetails(oTFSession.getId(),
				oTFSession.getMeetingId(), userId);
		if (gttAttendeeDetails != null) {
			String joinUrl = gttAttendeeDetails.getJoinUrl();
			if (!StringUtils.isEmpty(joinUrl)) {
				return joinUrl;
			}
		}

		// If not present, register the user
		UserBasicInfo user = fosUtils.getUserBasicInfo(userId, true);
		gttAttendeeDetails = registerAttendee(oTFSession, user, getSessionOrganizerKey(oTFSession),
				getSessionMeetingId(oTFSession),null,null);
		return gttAttendeeDetails.getJoinUrl();
	}

	@Override
	public List<GTTAttendeeDetails> registerAttendees(OTFSession oTFSession,Map<String,List<EnrollmentPojo>> studentEnrollmentMap)
			throws InternalServerErrorException, NotFoundException {
		// Register attendees
		List<GTTAttendeeDetails> attendeeDetailsList = new ArrayList<>();
		if (CollectionUtils.isEmpty(oTFSession.getAttendees()) || StringUtils.isEmpty(oTFSession.getMeetingId())) {
			// No attendees found
			return attendeeDetailsList;
		}

		// Fetch users
		List<UserBasicInfo> users = fosUtils.getUserBasicInfos(oTFSession.getAttendees(), true);
		if (CollectionUtils.isEmpty(users)) {
			String errorMessage = "Error fetching users. Users not found or fetch size mismatch. attendees :"
					+ oTFSession.getAttendees() + " users:" + (users == null ? "" : Arrays.toString(users.toArray()));
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		// Validate user count
		if (users.size() != oTFSession.getAttendees().size()) {
			String errorMessage = "Error fetching users. fetch size mismatch. attendees :" + oTFSession.getAttendees()
					+ " users:" + (users == null ? "" : Arrays.toString(users.toArray()));
			logger.info(errorMessage);
		}

		// Validate organizer details
		String organizerKey = getSessionOrganizerKey(oTFSession);
                String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());

		// Validate meeting id
		String meetingId = getSessionMeetingId(oTFSession);

		// Register the users
		for (UserBasicInfo user : users) {
			attendeeDetailsList.add(registerAttendee(oTFSession, user, organizerKey, meetingId,studentEnrollmentMap.get(user.getUserId().toString()),organizerAccessToken));
		}
		return attendeeDetailsList;
	}

	@Override
	public GTTAttendeeDetails registerAttendee(OTFSession oTFSession, String userId,List<EnrollmentPojo> enrollmentPojos)
			throws NotFoundException, InternalServerErrorException {
		UserBasicInfo user = fosUtils.getUserBasicInfo(userId, true);
		if (user == null) {
			throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "User not found for id - " + userId);
		}

		return registerAttendee(oTFSession, user, getSessionOrganizerKey(oTFSession), getSessionMeetingId(oTFSession),enrollmentPojos,null);
	}

	public GTTAttendeeDetails registerAttendee(OTFSession oTFSession, UserBasicInfo user, String organizerKey,
			String meetingId,List<EnrollmentPojo> enrollmentPojos,String organizerAccessToken) throws NotFoundException, InternalServerErrorException {
		GTTAttendeeDetails gttAttendeeDetails = null;

		// Check if meeting id exists
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED, "OTF meeting not scheduled");
		}
                
                if(StringUtils.isEmpty(organizerAccessToken)){
                    organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
                }
                
		String serverUrl = getRegisterUrl(organizerKey, meetingId);
		GTTRegisterAttendeeReq gttRegisterAttendeeReq = new GTTRegisterAttendeeReq(user);
		logger.info("GTT register req : " + gttRegisterAttendeeReq.toString());
//		String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
		ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(gttRegisterAttendeeReq),
				organizerAccessToken);
		logger.info("Response is: " + cRes.toString());
		String resp = cRes.getEntity(String.class);
                
                if(ArrayUtils.isEmpty(enrollmentPojos) && Role.STUDENT.equals(user.getRole())){
                    GetEnrollmentsReq getEnrollmentsReq = new GetEnrollmentsReq();
                    getEnrollmentsReq.setUserId(user.getUserId());
                    getEnrollmentsReq.setStatus(EntityStatus.ACTIVE);
                    getEnrollmentsReq.setMultipleBatchIds(oTFSession.getBatchIds());

                    enrollmentPojos =  oTFSessionManager.getEnrollmentIdForUser(getEnrollmentsReq);
                }
                
		switch (cRes.getStatus()) {
		case 201:
			GTTRegisterAttendeeRes gttRegisterAttendeeRes = gson.fromJson(resp, GTTRegisterAttendeeRes.class);

			// Update the gtt attendee details
			gttAttendeeDetails = gttAttendeeDetailsManager.createAttendeeDetails(gttRegisterAttendeeRes,
					organizerAccessToken, organizerKey, oTFSession, user.getUserId(),enrollmentPojos);

			break;
		case 409:
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(resp).getAsJsonObject();
			String registrantKey = jsonObject.get("registrantKey").getAsString();
			GTTGetRegistrantDetailsRes registrantDetailsRes = getRegistrantDetails(registrantKey, meetingId,
					organizerKey, oTFSession,organizerAccessToken);

			/*
			 * { "errorCode":"DuplicateRegistrant",
			 * "description":"Registration failed, email address already in use." ,
			 * "incident":216774595198496512, "registrantKey":8437663606892038401}
			 */

			// Update the gtt attendee details
			gttAttendeeDetails = gttAttendeeDetailsManager.createAttendeeDetails(registrantDetailsRes,
					organizerAccessToken, organizerKey, oTFSession,enrollmentPojos);

			break;
		case 429:
			try {
				Thread.sleep(1000);
				return registerAttendee(oTFSession, user, organizerKey, meetingId,enrollmentPojos,organizerAccessToken);
			} catch (Exception ex) {
				logger.info("Exception ex:" + ex.toString() + " message:" + ex.getMessage());
			}

		default:
			String errorMessage = "Error occured while registering the student. response : " + cRes.toString() + "user:"
					+ user.toString() + " session:" + oTFSession.toString();
			logger.info(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		return gttAttendeeDetails;
	}

	public List<GTTRecording> getRecordingDownloadUrl(OTFSession oTFSession)
			throws InternalServerErrorException, NotFoundException {
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED, "Meeting not scheduled for the session");
		}

		// Check if already exists in db
		GTTSessionRecordingInfo existingEntry = gttSessionRecordingInfoDAO.getGTTSessionRecordingInfo(oTFSession.getId(),
				oTFSession.getMeetingId());
		if (existingEntry != null) {
			return existingEntry.getGttRecordings();
		}

		String trainingKey = oTFSession.getMeetingId();
		if (StringUtils.isEmpty(trainingKey)) {
			logger.info("meeting id does not exist");
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
					"Meeting id does not exist for session:" + oTFSession.toString());
		}
		// https://api.citrixonline.com/G2T/rest/trainings/5958566152465130241/recordings
		String serverUrl = citrixURL + "/G2T/rest/trainings/" + trainingKey + "/recordings";
		logger.info("Calling url: " + serverUrl + " for organizer access key: " + oTFSession.getOrganizerAccessToken());
		ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
				getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken()));
		String resp = cRes.getEntity(String.class);
		logger.info("Response is: " + resp);

		switch (cRes.getStatus()) {
		case 200:
			GTTGetRecordingRes recordingRes = gson.fromJson(resp, GTTGetRecordingRes.class);
			List<GTTRecording> downloadRecordings = new ArrayList<GTTRecording>();

			if (recordingRes != null && !CollectionUtils.isEmpty(recordingRes.getRecordingList())) {
				List<GTTRecording> recordings = recordingRes.getRecordingList();
				for (GTTRecording recording : recordings) {
					String downloadUrl = recording.getDownloadUrl();
					if (StringUtils.isEmpty(downloadUrl)) {
						continue;
					}

					logger.info("Download url : " + downloadUrl);
					downloadRecordings.add(recording);
				}
			}

			if (CollectionUtils.isEmpty(downloadRecordings)) {
				logger.info("Recording not found");
				if (oTFSession.getEndTime() < (System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY)) {
					GTTSessionRecordingInfo gttSessionRecordingInfo = new GTTSessionRecordingInfo(oTFSession,
							downloadRecordings);
					gttSessionRecordingInfoDAO.update(gttSessionRecordingInfo);
				}
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Recording not found");
			}

			if (downloadRecordings.size() > 1) {
				logger.info("Multiple recordings found for session : " + oTFSession.toString() + " downloadUrls:"
						+ Arrays.toString(downloadRecordings.toArray()));
			}

			GTTSessionRecordingInfo gttSessionRecordingInfo = new GTTSessionRecordingInfo(oTFSession, downloadRecordings);
			gttSessionRecordingInfoDAO.update(gttSessionRecordingInfo);
			logger.info("GTT session recording info : " + gttSessionRecordingInfo.toString());
			return downloadRecordings;
		default:
			String errorMessage = "Error fetching download url cRes: " + cRes.toString() + " session:"
					+ oTFSession.toString();
			logger.info(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Recording not found");
		}
	}

	@Override
	public void userJoined(OTFSession oTFSession, String userId) {
		gttAttendeeDetailsManager.userJoined(oTFSession, userId);
	}
        
        @Override
        public List<GTTSessionInfoRes> getGTTSessionInfos(OTFSession oTFSession) throws NotFoundException {
            logger.info("Request:" + oTFSession.getId());
            List<GTTSessionInfoRes> gttSessions = new ArrayList<>();
            if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
                    logger.info("No training is scheduled for this session");
                    return gttSessions;
            }

            if (oTFSession.getEndTime() > System.currentTimeMillis()) {
                    logger.info("Too early to handle feedback for this session");
                    return gttSessions;
            }

            // Validate if attendees can be fetched

            // Fetch sessions for this training
            String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
            String organizerKey = getOrganizerKeyForToken(oTFSession.getOrganizerAccessToken());
            String sessionUrl = getSessionsUrl(oTFSession.getMeetingId(), organizerKey);
            logger.info("SessionUrl:" + sessionUrl);

            ClientResponse cRes = WebUtils.INSTANCE.doCall(sessionUrl, HttpMethod.GET, null,
                            organizerAccessToken);
            if (cRes.getStatus() != 200) {
                    String errorMessage = "Error fetching the sessions- status code not 200. response : " + cRes.toString()
                                    + " req: " + oTFSession.toString();
                    logger.info(errorMessage);
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Registrant not found : " + errorMessage);
            }

            logger.info("Response is: " + cRes.toString());
            String resp = cRes.getEntity(String.class);
            if (StringUtils.isEmpty(resp)) {
                    logger.info(
                                    "Emptry response while fetching sessions:" + cRes.toString() + " session:" + oTFSession.toString());
            }
            gttSessions = gson.fromJson(resp, GTT_SESSION_LIST_TYPE);
            return gttSessions;
        }

	// Updates the GTT session attendee info and set the attendee list in
	// session
	@Override
	public void handleFeedback(OTFSession oTFSession) throws VException, InternalServerErrorException {
		List<GTTSessionInfoRes> gttSessions =getGTTSessionInfos(oTFSession);
                String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
                String organizerKey = getOrganizerKeyForToken(oTFSession.getOrganizerAccessToken());
//		if(gttSessions.size()>1){
//				   logger.error("Number of GttSessions are more than 1 for session: " + session.toString());
//		}

		if (!CollectionUtils.isEmpty(gttSessions)) {

			// Fetch enrolled students
                        List<String> enrolledStudents=new ArrayList<>();
			if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {
                                List<BatchEnrolmentInfo> batches = otfSessionTaskManager.getBatchEnrolmentsByIds(oTFSession.getBatchIds());
                                if (ArrayUtils.isNotEmpty(batches)) {
                                    for (BatchEnrolmentInfo batch : batches) {
                                        if (batch != null && !CollectionUtils.isEmpty(batch.getEnrolledStudentIds())) {
                                            enrolledStudents.addAll(batch.getEnrolledStudentIds());
                                        }
                                    }
                                }
			}

			// Create attendee map
			Set<String> totalEnrolled = null;//check_check batch.getEnrolledStudents();
			if(totalEnrolled == null){
				totalEnrolled = new HashSet<String>();
			}
			List<GTTAttendeeDetails> gttAttendeeDetailsList = gttAttendeeDetailsManager
					.getAttendeeDetailsByTraining(oTFSession.getMeetingId());
			Map<String, GTTAttendeeDetails> attendeeMap = createAttendeeMap(gttAttendeeDetailsList);
			Map<String, GTTAttendeeDetails> updatedAttendeeMap = new HashMap<>();

			List<String> errors = new ArrayList<>();
			for (GTTSessionInfoRes gttSession : gttSessions) {
//			GTTSessionInfoRes gttSession = gttSessions.get(0);
				logger.info("GTTSession:" + gttSession.toString());
				String attendeeUrl = getAttendeesUrl(gttSession.getSessionKey(), organizerKey);
	
				ClientResponse cAttendeeRes = WebUtils.INSTANCE.doCall(attendeeUrl, HttpMethod.GET, null,
						organizerAccessToken);
				if (cAttendeeRes.getStatus() != 200) {
					String errorMessage = "Error fetching the attendees details- status code not 200. response : "
							+ cAttendeeRes.toString() + " req: " + oTFSession.toString();
					logger.info(errorMessage);
					throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Registrant not found : " + errorMessage);
				}
	
				logger.info("Response is: " + cAttendeeRes.toString());
				String attendeeResp = cAttendeeRes.getEntity(String.class);
				logger.info("AttendeeResp:" + attendeeResp);
				if (!StringUtils.isEmpty(attendeeResp)) {
					List<GTTSessionAttendeeInfo> attendeeInfos = gson.fromJson(attendeeResp,
							GTT_SESSION_ATTENDEE_LIST_TYPE);
					if (!CollectionUtils.isEmpty(attendeeInfos)) {
						String trainingKey = oTFSession.getMeetingId();
						for (GTTSessionAttendeeInfo sessionAttendeeInfo : attendeeInfos) {
							logger.info("Attendee info:" + sessionAttendeeInfo.toString());
							String userId = GTTAttendeeDetailsManager.extractUserId(sessionAttendeeInfo.getEmail());

							if (StringUtils.isEmpty(userId) || !enrolledStudents.contains(userId)) {
								continue;
							}
	
							String attendeeKey = getAttendeeMapKey(trainingKey, userId);
							GTTAttendeeDetails gttAttendeeDetails = updatedAttendeeMap.get(attendeeKey);
							if (gttAttendeeDetails == null) {
								gttAttendeeDetails = attendeeMap.get(attendeeKey);
							}
	
							if (gttAttendeeDetails == null) {
								gttAttendeeDetails = new GTTAttendeeDetails();
								gttAttendeeDetails.setUserId(userId);
								gttAttendeeDetails.setOrganizerAccessToken(oTFSession.getOrganizerAccessToken());
								gttAttendeeDetails.setOrganizerKey(getSessionOrganizerKey(oTFSession));
								gttAttendeeDetails.setTraningId(oTFSession.getMeetingId());
								gttAttendeeDetails.setSessionId(oTFSession.getId());
							}
	
							if(totalEnrolled.contains(userId)){
								totalEnrolled.remove(userId);
							}
	
	
							try {
								gttAttendeeDetails.updateSessionAttendeeInfo(sessionAttendeeInfo);
								// todo enable for gamification
								// gttAttendeeDetailsManager.processGttAttendeeForGameAsync(gttAttendeeDetails);
								attendeeMap.put(attendeeKey, gttAttendeeDetails);
								updatedAttendeeMap.put(attendeeKey, gttAttendeeDetails);
							} catch (Exception ex) {
								String errorMessage = "Error updating gtt attendee details ex:" + ex.toString()
										+ " message:" + ex.getMessage() + " req:" + sessionAttendeeInfo.toString();
								logger.info(errorMessage);
								errors.add(errorMessage);
							}
						}
					}
				}
				if (!CollectionUtils.isEmpty(errors)) {
					logger.error(
							"Error occured updating gtt attendee report : " + Arrays.toString(errors.toArray()));
				}
			}


			if(!totalEnrolled.isEmpty()){
				Collection<GTTAttendeeDetails> absentEnrolled = new ArrayList<GTTAttendeeDetails>();
				for (String user_id: totalEnrolled){
					GTTAttendeeDetails tempGTTAttendee = new GTTAttendeeDetails();
					tempGTTAttendee.setSessionId(oTFSession.getId());
					tempGTTAttendee.setTraningId(oTFSession.getMeetingId());
					tempGTTAttendee.setOrganizerAccessToken(oTFSession.getOrganizerAccessToken());
					tempGTTAttendee.setOrganizerKey(getSessionOrganizerKey(oTFSession));
					tempGTTAttendee.setUserId(user_id);
					absentEnrolled.add(tempGTTAttendee);
				}
				gttAttendeeDetailsDAO.insertAllEntities(absentEnrolled, GTTAttendeeDetails.class.getSimpleName());
			}


			if (!updatedAttendeeMap.isEmpty()) {
				logger.info("Updated entries : " + updatedAttendeeMap.size() + " session:" + oTFSession.toString());
				List<String> attendees = new ArrayList<>();
				for (GTTAttendeeDetails gttAttendeeDetails : updatedAttendeeMap.values()) {
					logger.info("Gtt attendee details:" + gttAttendeeDetails.toString());
					gttAttendeeDetailsManager.updateGTTAttendeeDetails(gttAttendeeDetails);
					if (!CollectionUtils.isEmpty(gttAttendeeDetails.getActiveIntervals())
							&& !attendees.contains(gttAttendeeDetails.getUserId())) {
						attendees.add(gttAttendeeDetails.getUserId());
					}
				}

//			check_check	if (batch != null) {
					oTFSession.setAttendees(attendees);
//		check_check		}
				oTFSession.addFlag(OTFSessionFlag.POST_SESSION_DATA_PROCESSED);
			} else {
				logger.info("No entries updated for session:" + oTFSession.toString());
			}
		}
	}

	private static String getTrainingUrl(String trainingId, String organizerKey) {
		return citrixURL+ "/G2T/rest/organizers/" + organizerKey + "/trainings/" + trainingId;
	}

	private static String extractMeetingId(OTFSession oTFSession) {
		if (oTFSession != null && !StringUtils.isEmpty(oTFSession.getSessionURL())) {
			return oTFSession.getSessionURL().substring(oTFSession.getSessionURL().lastIndexOf('/') + 1,
					oTFSession.getSessionURL().length());
		}
		return "";
	}

	private static String getRegisterUrl(String organizerKey, String trainingKey) {
		// https://api.citrixonline.com/G2T/rest/organizers/6879452220571615237/trainings/893408553371535874/registrants
		return citrixURL + "/G2T/rest/organizers/" + organizerKey + "/trainings/" + trainingKey
				+ "/registrants";
	}

	private String getSessionMeetingId(OTFSession oTFSession) throws NotFoundException {
		String meetingId = oTFSession.getMeetingId();
		if (StringUtils.isEmpty(meetingId)) {
			meetingId = extractMeetingId(oTFSession);
			if (StringUtils.isEmpty(meetingId)) {
				String errorMessage = "Traning id not found for session : " + oTFSession.toString();
				logger.info(errorMessage);
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
			}
		}

		return meetingId;
	}

	private String getSessionOrganizerKey(OTFSession oTFSession) throws InternalServerErrorException {
		String organizerKey = getOrganizerKeyForToken(oTFSession.getOrganizerAccessToken());
		if (StringUtils.isEmpty(organizerKey)) {
			String errorMessage = "Organizer key is missing :" + " key : " + organizerKey
					+ " session:" + oTFSession.toString();
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}
		return organizerKey;
	}

	private GTTGetRegistrantDetailsRes getRegistrantDetails(String registrantKey, String trainigKey,
			String organizerKey, OTFSession oTFSession,String organizerAccessToken) throws NotFoundException {
		// https://api.citrixonline.com/G2T/rest/organizers/6879452220571615237/trainings/893408553371535874/registrants/8437663606892038401
		String serverUrl = getRegistrantUrl(registrantKey, trainigKey, organizerKey);
		logger.info("url : " + serverUrl);
                if(StringUtils.isEmpty(organizerAccessToken)){
                    organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());                    
                }
                
		ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,organizerAccessToken);
		if (cRes.getStatus() == 429) {
			try {
				Thread.sleep(1000);
				return getRegistrantDetails(registrantKey, trainigKey, organizerKey, oTFSession,organizerAccessToken);
			} catch (Exception ex) {
				logger.info("Exception ex:" + ex.toString() + " message:" + ex.getMessage());
			}
		}
		if (cRes.getStatus() != 200) {
			String errorMessage = "Error fetching the registrant details - status code not 200. response : "
					+ cRes.toString() + " req: " + registrantKey + " trainigKey:" + trainigKey + " organizerKey:"
					+ organizerKey + " session:" + oTFSession.toString();
			logger.info(errorMessage);
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Registrant not found : " + errorMessage);
		}
		logger.info("Response is: " + cRes.toString());
		String resp = cRes.getEntity(String.class);
		GTTGetRegistrantDetailsRes registrantDetailsRes = gson.fromJson(resp, GTTGetRegistrantDetailsRes.class);
		logger.info("Registrant details : " + registrantDetailsRes.toString());
		return registrantDetailsRes;
	}

	private static String getRegistrantUrl(String registrantKey, String trainigKey, String organizerKey) {
		return citrixURL + "/G2T/rest/organizers/" + organizerKey + "/trainings/" + trainigKey
				+ "/registrants/" + registrantKey;
	}

	private static String getSessionsUrl(String trainigKey, String organizerKey) {
		// https://api.citrixonline.com/G2T/rest/reports/organizers/8963749134346584069/trainings/8313269260191941633

		return citrixURL+"/G2T/rest/reports/organizers/" + organizerKey + "/trainings/" + trainigKey;
	}

	private static String getAttendeesUrl(String sessionId, String organizerKey) {
		// https://api.citrixonline.com/G2T/rest/reports/organizers/8963749134346584069/sessions/4621041/attendees
		return citrixURL+ "/G2T/rest/reports/organizers/" + organizerKey + "/sessions/" + sessionId
				+ "/attendees";
	}

	private static String getAttendeeMapKey(String trainingKey, String userId) {
		return trainingKey + "_" + userId;
	}

	private static Map<String, GTTAttendeeDetails> createAttendeeMap(List<GTTAttendeeDetails> attendees) {
		Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(attendees)) {
			for (GTTAttendeeDetails gttAttendeeDetails : attendees) {
				gttAttendeeDetails.resetFeedbackData();
				attendeeMap.put(getAttendeeMapKey(gttAttendeeDetails.getTraningId(), gttAttendeeDetails.getUserId()),
						gttAttendeeDetails);
			}
		}

		return attendeeMap;
	}

	@Override
	public String getOrganizerKeyForToken(String accessToken) {
		return organizerKeys.get(accessToken);
	}
        
        @Override
        public List<GTTAttendeeDetails> createGTTAttendeeDetails(OTFSession session,Map<String,List<EnrollmentPojo>> userEnrollmentMap,AttendeeContext context) throws InternalServerErrorException{
            List<GTTAttendeeDetails> enrolled = new ArrayList<>();
            if(ArrayUtils.isNotEmpty(userEnrollmentMap.keySet())){
		for (String user_id: userEnrollmentMap.keySet()){
			GTTAttendeeDetails gttAttendeeDetails = gttAttendeeDetailsDAO.getAttendeeDetail( user_id, session.getId(), EntityState.ACTIVE);
			if (gttAttendeeDetails == null) {
				GTTAttendeeDetails tempGTTAttendee = new GTTAttendeeDetails();
				tempGTTAttendee.setSessionId(session.getId());
				tempGTTAttendee.setTraningId(session.getMeetingId());
				tempGTTAttendee.setOrganizerAccessToken(getPostOverwriteAccessToken(session.getOrganizerAccessToken()));
				tempGTTAttendee.setOrganizerKey(getSessionOrganizerKey(session));
				tempGTTAttendee.setUserId(user_id);
				final List<EnrollmentPojo> enrollmentPojos = userEnrollmentMap.get(user_id);
				if (ArrayUtils.isNotEmpty(enrollmentPojos)) {
					tempGTTAttendee.setEnrollmentId(enrollmentPojos.get(enrollmentPojos.size() - 1).getId());
					tempGTTAttendee.addEnrollmentInfos(enrollmentPojos);
				}

				if (context != null) {
					tempGTTAttendee.setAttendeeType(context);
				}
				enrolled.add(tempGTTAttendee);
			}
		}				
		gttAttendeeDetailsDAO.insertAllEntities(enrolled, GTTAttendeeDetails.class.getSimpleName());
            }
            return enrolled;
        }
}
