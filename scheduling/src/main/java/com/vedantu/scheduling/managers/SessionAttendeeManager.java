package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.User.Role;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.scheduling.dao.serializers.SessionAttendeeDAO;
import com.vedantu.scheduling.request.session.ProcessSessionPayoutReq;
import com.vedantu.scheduling.utils.SessionUtils;
import com.vedantu.session.pojo.SessionAttendee.SessionUserState;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class SessionAttendeeManager {

	@Autowired
	private SessionAttendeeDAO sessionAttendeeDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionAttendeeManager.class);

	public List<SessionAttendee> createSessionAttendees(Session session) throws VException {
		logger.info("Session id : " + session.getId());
		if (session.getId() == null || session.getId() <= 0l) {
			String errorMessage = "Create session attendee called without session id : " + session.toString();
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		List<SessionAttendee> attendees = SessionAttendee.createSessionAttendees(session);
		sessionAttendeeDAO.insertAll(attendees);
		return attendees;
	}

	public List<SessionAttendee> createSessionAttendees(List<Session> sessions) throws VException {
		if (CollectionUtils.isEmpty(sessions) || sessions.size() != SessionUtils.getSessionIds(sessions).size()) {
			String errorMessage = "Create session attendee called with missing session ids : "
					+ (sessions == null ? "null" : Arrays.toString(sessions.toArray()));
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}
		logger.info("Session ids : " + SessionUtils.getSessionIds(sessions));

		List<SessionAttendee> attendees = SessionAttendee.createSessionAttendees(sessions);
		sessionAttendeeDAO.insertAll(attendees);
		return attendees;
	}

	public List<SessionAttendee> updateSessionAttendeesStartTime(Session session) throws VException {
		logger.info("Session id : " + session.getId());
		if (session.getId() == null || session.getId() <= 0l) {
			String errorMessage = "Update session attendee called without session id : " + session.toString();
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		List<SessionAttendee> attendees = getSessionAttendees(session.getId());
		logger.info("attendees : " + attendees.size());
		for (SessionAttendee attendee : attendees) {
			attendee.setStartTime(session.getStartTime());
			sessionAttendeeDAO.update(attendee);
			logger.info("sessionAttendee updated : " + attendee.toString());
		}
		return attendees;
	}

	public List<SessionAttendee> getSessionAttendees(Long sessionId) {
		logger.info("Request : " + sessionId);
		List<SessionAttendee> attendees = sessionAttendeeDAO.getAttendees(sessionId);

		// TODO : Below log needs optimization
		logger.info("Session Attendees : " + Arrays.toString(attendees.toArray()));
		return attendees;
	}

	public SessionAttendee getSessionAttendees(Long sessionId, long userId) throws VException {
		logger.info("Request : " + sessionId);
		List<SessionAttendee> attendees = sessionAttendeeDAO.getAttendees(sessionId, userId);
		if (CollectionUtils.isEmpty(attendees)) {
			throw new NotFoundException(ErrorCode.SESSION_ATTENDEE_NOT_FOUND,
					"Attendee not found for sessionId : " + sessionId + " userId : " + userId);
		}

		if (attendees.size() > 1) {
			String errorMessage = "Multiple attendees found for userId " + userId + "and sessionId" + sessionId;
			throw new ConflictException(ErrorCode.SESSION_MULTIPLE_ATTENDEES_FOUND, errorMessage);
		}
		// TODO : Below log needs optimization
		logger.info("Session Attendees : " + Arrays.toString(attendees.toArray()));
		return attendees.get(0);
	}

	public List<SessionAttendee> getSessionAttendees(List<Long> sessionIds) {
		logger.info("Request : " + sessionIds == null ? "null" : Arrays.toString(sessionIds.toArray()));
		List<SessionAttendee> attendees = sessionAttendeeDAO.getAttendees(sessionIds);

		// TODO : Below log needs optimization
		logger.info("Session Attendees : " + Arrays.toString(attendees.toArray()));
		return attendees;
	}

	public void markJoined(Long sessionId, Long userId) throws NotFoundException {
		logger.info("Request : " + sessionId + " userId : " + userId);
		List<SessionAttendee> attendees = sessionAttendeeDAO.getAttendees(sessionId, userId);

		if (CollectionUtils.isEmpty(attendees)) {
			String errorMessage = "No attendee found for sessionId: " + sessionId + " userId:" + userId;
			logger.error(errorMessage);
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
		}

		if (attendees.size() > 1) {
			logger.error("Multiple attendees found for session id : " + sessionId + " userId:" + userId);
		}

		markAttendeeJoined(attendees.get(0));
	}

	public void markJoined(Long sessionId) throws NotFoundException {
		logger.info("sessionId: " + sessionId);
		List<SessionAttendee> sessionAttendees = getSessionAttendees(sessionId);
		if (CollectionUtils.isEmpty(sessionAttendees)) {
			String errorMessage = "No attendee found for sessionId: " + sessionId;
			logger.error(errorMessage);
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
		}

		for (SessionAttendee sessionAttendee : sessionAttendees) {
			if (SessionUserState.NOT_JOINED.equals(sessionAttendee.getUserState())) {
				markAttendeeJoined(sessionAttendee);
			}
		}
	}

	public List<SessionAttendee> updateAttendeePostPayout(ProcessSessionPayoutReq req) throws VException {
		logger.info("Request : " + req.toString());
		List<SessionAttendee> attendees = getSessionAttendees(req.getSessionId());
		if (CollectionUtils.isEmpty(attendees)) {
			logger.error("No attendees found for req : " + req.toString());
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Session attendees not found : " + req.toString());
		}

		for (SessionAttendee sessionAttendee : attendees) {
			sessionAttendee.setBillingPeriod(req.getBillingDuration().intValue());
			switch (sessionAttendee.getRole()) {
			case TEACHER:
				sessionAttendee.setPaidAmount(req.getTeacherPayout());
				break;
			case STUDENT:
				sessionAttendee.setChargedAmount(req.getStudentCharge());
				break;
			default:
				logger.error("Invalid role of attendee : " + sessionAttendee.toString());
			}

			logger.info("Post update : " + sessionAttendee.toString());
			sessionAttendeeDAO.update(sessionAttendee);
		}
		logger.info("Attendees updated : " + Arrays.toString(attendees.toArray()));
		return attendees;
	}

	private void markAttendeeJoined(SessionAttendee sessionAttendee) {
		if (SessionUserState.NOT_JOINED.equals(sessionAttendee.getUserState())) {
			logger.info("Attendee pre update : " + sessionAttendee);
			sessionAttendee.setUserState(SessionUserState.JOINED);
			// since allowing teacher to join before the session start time, so
			// setting
			// join time to = starttime incase it is less than start time
			Long currentTime = System.currentTimeMillis();
			if (currentTime < sessionAttendee.getStartTime()) {
				sessionAttendee.setJoinTime(sessionAttendee.getStartTime());
			} else {
				sessionAttendee.setJoinTime(currentTime);
			}

			sessionAttendeeDAO.update(sessionAttendee);
			logger.info("Attendee with id : " + sessionAttendee.getId() + " marked joined");
		}
	}

	public List<Long> checkForTeacherJoinTime(List<Long> sessionIds) {

		List<Long> lateJoinSessions = new ArrayList<>();
		if (CollectionUtils.isEmpty(sessionIds)) {
			return lateJoinSessions;
		}
		Long time_5 = new Long(DateTimeUtils.MILLIS_PER_MINUTE * 5);
		Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).in(sessionIds));
		query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_ROLE).is(Role.TEACHER));
		query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_STATE).is(SessionUserState.JOINED));
		query.fields().include(SessionAttendee.Constants.SESSION_ID);
		query.fields().include(SessionAttendee.Constants.USER_ID);
		query.fields().include(SessionAttendee.Constants.START_TIME);
		query.fields().include(SessionAttendee.Constants.JOIN_TIME);

		List<SessionAttendee> sessionAttendees = sessionAttendeeDAO.runQuery(query, SessionAttendee.class);
		if (!CollectionUtils.isEmpty(sessionAttendees)) {
			for (SessionAttendee sessionAttendee : sessionAttendees) {
				if (sessionAttendee.getJoinTime() != null && sessionAttendee.getStartTime() != null
						&& (sessionAttendee.getJoinTime() - sessionAttendee.getStartTime()) > time_5) {
					lateJoinSessions.add(sessionAttendee.getSessionId());
				}
			}
		}
		return lateJoinSessions;
	}

	public long getEndedSessionDurationBySessionIds(Set<Long> sessionIds) {
		// TODO : If we are planning for multiple students for same session, then need to use student id
		// and definitely not student role.
		return sessionAttendeeDAO.getEndedSessionDurationBySessionIds(sessionIds);
	}
        
        public long getStudentNotJoinedSessionCount(Set<Long> sessionIds) {
                Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).in(sessionIds));
		query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_STATE).is(SessionUserState.NOT_JOINED));
                query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_ROLE).is(Role.STUDENT));
                return sessionAttendeeDAO.queryCount(query, SessionAttendee.class);
        }
        
        public long getTeacherNotJoinedSessionCount(Set<Long> sessionIds) {
                Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).in(sessionIds));
		query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_STATE).is(SessionUserState.NOT_JOINED));
                query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_ROLE).is(Role.TEACHER));
                return sessionAttendeeDAO.queryCount(query, SessionAttendee.class);
        }
}
