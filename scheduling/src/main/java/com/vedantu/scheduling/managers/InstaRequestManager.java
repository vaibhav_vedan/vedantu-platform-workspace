package com.vedantu.scheduling.managers;

import com.vedantu.User.Role;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.Logger;
import com.vedantu.scheduling.dao.entity.InstaRequest;
import com.vedantu.scheduling.dao.entity.TeacherSubscription;
import com.vedantu.scheduling.dao.serializers.InstaRequestDAO;
import com.vedantu.scheduling.dao.serializers.TeacherSubscriptionDAO;
import com.vedantu.scheduling.enums.instalearn.InstaState;
import com.vedantu.scheduling.pojo.instalearn.InstaPayload;
import com.vedantu.scheduling.request.instalearn.AddInstaRequestReq;
import com.vedantu.scheduling.request.instalearn.GetInstaRequestsReq;
import com.vedantu.scheduling.request.instalearn.SetTeacherSubscriptionReq;
import com.vedantu.scheduling.request.instalearn.UpdateInstaRequestReq;
import com.vedantu.scheduling.response.InstaActiveMissedResponse;
import com.vedantu.scheduling.response.instalearn.GetInstaRequestsRes;
import com.vedantu.scheduling.response.instalearn.SubmitInstaRequestRes;
import com.vedantu.scheduling.response.instalearn.TeacherSubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.dozer.DozerBeanMapper;

@Service
public class InstaRequestManager {

    @Autowired
    public InstaRequestDAO instaRequestDAO;

    @Autowired
    public TeacherSubscriptionDAO teacherSubscriptionDAO;

    @Autowired
    private LogFactory logFactory;

    private final Long allowedTimeWindowToAcceptRequest = 600000l;

    @Autowired
    private DozerBeanMapper mapper;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstaRequestManager.class);

    public SubmitInstaRequestRes addInstaRequest(AddInstaRequestReq addInstaRequestReq) {
        InstaRequest instaRequest = new InstaRequest(addInstaRequestReq.getTo(), addInstaRequestReq.getFrom(), addInstaRequestReq.getData());
        instaRequestDAO.save(instaRequest);
        SubmitInstaRequestRes instaProposalResponse = mapper.map(instaRequest, SubmitInstaRequestRes.class);
        instaProposalResponse.setCurrentRequest(instaRequest.checkIfCurrentRequest());
        return instaProposalResponse;
    }

    public SubmitInstaRequestRes submitInstaRequest(UpdateInstaRequestReq updateInstaRequestReq) throws NotFoundException, ConflictException, ForbiddenException {
        InstaRequest instaRequest = instaRequestDAO.getById(updateInstaRequestReq.getId());
        if (instaRequest == null) {
            throw new NotFoundException(ErrorCode.INSTALEARN_REQUEST_NOT_FOUND, "Instalearn request is not found");
        }

        Long callingUserId = updateInstaRequestReq.getCallingUserId();
        if (!Role.ADMIN.equals(updateInstaRequestReq.getCallingUserRole()) && (callingUserId.equals(instaRequest.getFrom())
                && callingUserId.equals(instaRequest.getTo()))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "User forbidden to use this API");
        }

        if (InstaState.MISSED.equals(instaRequest.getData().getState()) || isMissed(instaRequest)) {
            if (InstaState.INPROGRESS.equals(instaRequest.getData().getState())) {
                instaRequest.getData().setState(InstaState.MISSED);
                instaRequestDAO.save(instaRequest);
            }
            throw new ConflictException(ErrorCode.INSTALEARN_REQUEST_MISSED, "Instalearn request is missed");
        }
        //updating
        instaRequest.setFrom(updateInstaRequestReq.getFrom());
        instaRequest.setTo(updateInstaRequestReq.getTo());
        if(updateInstaRequestReq.getData()!=null){
            InstaPayload updateData=updateInstaRequestReq.getData();
            InstaPayload data = instaRequest.getData();
            if(updateData.getState()!=null){
                data.setState(updateData.getState());
            }
            if(updateData.getDoubtText()!=null){
                data.setDoubtText(updateData.getDoubtText());
            }
            if(updateData.getGrade()!=null){
                data.setGrade(updateData.getGrade());
            }
            if(updateData.getBoardId()!=null){
                data.setBoardId(updateData.getBoardId());
            }  
            instaRequest.setData(data);
        }
        
        instaRequestDAO.save(instaRequest);
        SubmitInstaRequestRes instaProposalResponse = mapper.map(instaRequest, SubmitInstaRequestRes.class);
        instaProposalResponse.setCurrentRequest(instaRequest.checkIfCurrentRequest());
        return instaProposalResponse;
    }

    public SubmitInstaRequestRes getInstaRequest(String id) throws NotFoundException {
        InstaRequest instaRequest = instaRequestDAO.getById(id);
        if (instaRequest == null) {
            throw new NotFoundException(ErrorCode.INSTALEARN_REQUEST_NOT_FOUND, "Instalearn request is not found");
        }
        checkAndUpdatedMissedRequest(instaRequest);
        SubmitInstaRequestRes instaProposalResponse = mapper.map(instaRequest, SubmitInstaRequestRes.class);
        instaProposalResponse.setCurrentRequest(instaRequest.checkIfCurrentRequest());
        return instaProposalResponse;
    }

    public GetInstaRequestsRes getInstaRequests(GetInstaRequestsReq getInstaRequestsReq) {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, InstaRequest.Constants.CREATION_TIME));

        int start = 0;
        if (getInstaRequestsReq.getStart() != null) {
            start = getInstaRequestsReq.getStart();
        }
        int limit = AbstractMongoDAO.DEFAULT_FETCH_SIZE;
        if (getInstaRequestsReq.getSize() != null) {
            limit = getInstaRequestsReq.getSize();
        }
        List<InstaState> states = getInstaRequestsReq.getStates();

        query.skip(start);
        query.limit(limit);

        if (getInstaRequestsReq.getUserId() != null) {
            query.addCriteria(new Criteria().orOperator(Criteria.where("to").is(getInstaRequestsReq.getUserId()),
                    Criteria.where("from").is(getInstaRequestsReq.getUserId())));
        }

        if (ArrayUtils.isNotEmpty(states)) {
            query.addCriteria(Criteria.where("data.state").in(states));
        }

        logger.info("query :" + query);

        List<InstaRequest> instaRequests = instaRequestDAO.runQuery(query, InstaRequest.class);
        checkAndUpdatedMissedRequests(instaRequests);

        List<SubmitInstaRequestRes> instaResponses = new ArrayList<>();
        for (InstaRequest instaRequest : instaRequests) {
            SubmitInstaRequestRes instaProposalResponse = mapper.map(instaRequest, SubmitInstaRequestRes.class);
            instaProposalResponse.setCurrentRequest(instaRequest.checkIfCurrentRequest());
            instaResponses.add(instaProposalResponse);
        }
        GetInstaRequestsRes getReqRes = new GetInstaRequestsRes();
        getReqRes.setList(instaResponses);
        logger.info("DB Query response:" + instaRequests);
        return getReqRes;
    }

    public InstaActiveMissedResponse getInstaActiveMissedResponseByUserId(Long userId) {
        Query query1 = new Query();
        Query query2 = new Query();

        if (userId != null) {
            query1.addCriteria(new Criteria().orOperator(Criteria.where("to").is(userId),
                    Criteria.where("from").is(userId)));
            query2.addCriteria(new Criteria().orOperator(Criteria.where("to").is(userId),
                    Criteria.where("from").is(userId)));
        }

        Long curTime = System.currentTimeMillis();
        Long fiveHoursBefore = curTime - 5 * 3600 * 1000;

        query1.addCriteria(Criteria.where("data.state").is(InstaState.INPROGRESS));

        List<InstaRequest> instaRequests = instaRequestDAO.runQuery(query1, InstaRequest.class);
        checkAndUpdatedMissedRequests(instaRequests);

        query2.addCriteria(Criteria.where("data.state").is(InstaState.MISSED));
        query2.addCriteria(Criteria.where("creationTime").gte(fiveHoursBefore));

        Long active = instaRequestDAO.queryCount(query1, InstaRequest.class);
        Long missed = instaRequestDAO.queryCount(query2, InstaRequest.class);

        return new InstaActiveMissedResponse(active.intValue(), missed.intValue());
    }

    public TeacherSubscriptionResponse setTeacherSubscription(SetTeacherSubscriptionReq setTeacherSubscriptionReq) {
        Long userId = setTeacherSubscriptionReq.getUserId();
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));

        TeacherSubscription teacherSubscription;
        List<TeacherSubscription> teacherSubscriptions = teacherSubscriptionDAO.runQuery(query, TeacherSubscription.class);
        if (ArrayUtils.isNotEmpty(teacherSubscriptions)) {
            teacherSubscription = teacherSubscriptions.get(0);
            teacherSubscription.setSubscribed(setTeacherSubscriptionReq.getSubscribed());
        } else {
            teacherSubscription = new TeacherSubscription(userId, setTeacherSubscriptionReq.getSubscribed());
        }
        teacherSubscriptionDAO.save(teacherSubscription);
        TeacherSubscriptionResponse teacherSubscriptionResponse = new TeacherSubscriptionResponse(teacherSubscription.getUserId(), teacherSubscription.getSubscribed());
        return teacherSubscriptionResponse;
    }

    public TeacherSubscriptionResponse getTeacherSubscription(Long userId) {
        TeacherSubscription teacherSubscription;
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        List<TeacherSubscription> teacherSubscriptions = teacherSubscriptionDAO.runQuery(query, TeacherSubscription.class);
        if (ArrayUtils.isNotEmpty(teacherSubscriptions)) {
            teacherSubscription = teacherSubscriptions.get(0);
        } else {
            teacherSubscription = new TeacherSubscription(userId, Boolean.FALSE);
        }
        TeacherSubscriptionResponse teacherSubscriptionResponse = new TeacherSubscriptionResponse(teacherSubscription.getUserId(), teacherSubscription.getSubscribed());
        return teacherSubscriptionResponse;
    }

    private InstaRequest checkAndUpdatedMissedRequest(InstaRequest instaRequest) {
        Long curTime = System.currentTimeMillis();

        if (instaRequest.getData().getState() == InstaState.INPROGRESS
                && (curTime - instaRequest.getLastUpdated()) >= 600000L) {
            instaRequest.getData().setState(InstaState.MISSED);
        }
        instaRequestDAO.save(instaRequest);
        return instaRequest;
    }

    private void checkAndUpdatedMissedRequests(List<InstaRequest> instaRequests) {
        Long curTime = System.currentTimeMillis();

        List<InstaRequest> instaRequestsToUpdate = new ArrayList<>();
        for (InstaRequest instaRequest : instaRequests) {
            if (instaRequest.getData().getState() == InstaState.INPROGRESS
                    && (curTime - instaRequest.getLastUpdated()) >= 600000L) {
                instaRequest.getData().setState(InstaState.MISSED);
                instaRequestsToUpdate.add(instaRequest);
            }
        }
        instaRequestDAO.saveAll(instaRequestsToUpdate);
    }

    private boolean isMissed(InstaRequest instaRequest) {
        Long lastUpdated = instaRequest.getLastUpdated();
        return InstaState.INPROGRESS.equals(instaRequest.getData().getState()) && (System.currentTimeMillis() - lastUpdated) >= allowedTimeWindowToAcceptRequest;
    }
}
