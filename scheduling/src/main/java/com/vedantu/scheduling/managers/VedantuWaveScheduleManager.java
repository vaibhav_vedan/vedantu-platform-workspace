/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.dao.serializers.GTTAttendeeDetailsDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.pojo.GTTSessionInfoRes;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import com.vedantu.scheduling.enums.OTMServiceProvider;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import static org.postgresql.core.Utils.toHexString;

import java.io.IOException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * @author ajith
 */
@Service
public class VedantuWaveScheduleManager extends ScheduleManager {
    @Autowired
    private OTFSessionDAO oTFSessionDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VedantuWaveScheduleManager.class);

    private static Gson gson = new Gson();

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private static final String SOCIALVID_DOMAIN = "http://vedantu-one2many.socialvid.in";
    private static final String LOGIN_URL = SOCIALVID_DOMAIN + "/adminapi/v1/login";
    private static final String ADD_CONFERENCE = SOCIALVID_DOMAIN + "/adminapi/v1/user/conference/add";
    private static final String GET_CONFERENCE = SOCIALVID_DOMAIN + "/adminapi/v1/user/conference/get";
    private static final String DELETE_CONFERENCE = SOCIALVID_DOMAIN + "/adminapi/v1/user/conference/delete";
    private static final String GET_RECORDING = SOCIALVID_DOMAIN + "/adminapi/v1/user/recording/get";
    private static final String STATUS = SOCIALVID_DOMAIN + "/adminapi/v1/status";

    private static final String SOCIALVID_EMAIL = "vedantu@socialvid.in";
    private static final String SOCIALVID_PASSWORD = "vedantuIntegration";
    private static String SOCIALVID_TOKEN = null;
    private Long lastLoggedInAt = null;

    private static final String IMPARTUS_DOMAIN =  ConfigUtils.INSTANCE.getStringValue("IMPARTUS_DOMAIN");
    private static final String IMPARTUS_API_KEY = ConfigUtils.INSTANCE.getStringValue("IMPARTUS_API_KEY");
    private static final String IMPARTUS_API_SECRET = ConfigUtils.INSTANCE.getStringValue("IMPARTUS_API_SECRET");
    private static final String HMAC_SHA1_ALGORITHM = ConfigUtils.INSTANCE.getStringValue("IMPARTUS_HMAC_SHA1_ALGORITHM");;

    @Override
    public int getAccessArrayLength() {
        return 1;
    }

    @Override
    public String getAccessToken(int slot) {
        return "";
    }

    @Override
    public int getAccessTokenSlot(String accessToken) {
        return 1;
    }

    @Override
    public String getPostOverwriteAccessToken(String accessToken) {
        return "";
    }

    @Override
    public String getOrganizerKeyForToken(String accessToken) {
        return "";
    }

    @Override
    public BitSet getScheduleBitSet(Schedule schedule) {
        return null;
    }

    @Override
    public void createSessionLink(OTFSession oTFSession) throws Exception {
        if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
            oTFSession.setMeetingId(oTFSession.getId());
        }
//        if (StringUtils.isNotEmpty(oTFSession.getSessionURL())) {
//            logger.info("teacherurl already present, not doing anything " + oTFSession.getSessionURL());
//            return;
//        }
//        loginToSocialVid();
//        String conferenceId = addConference(oTFSession.getId());
//        if (StringUtils.isEmpty(conferenceId)) {
//            throw new InternalServerErrorException(ErrorCode.ADD_SOCIALVID_CONFERENCE_FAILED,
//                    "add socialvid conf failed for " + oTFSession.getId());
//        }
//        setTeacherJoinUrlFromConference(conferenceId, oTFSession);
    }

    @Override
    public GTTAttendeeDetails registerAttendee(OTFSession otfSession, String userId, List<EnrollmentPojo> enrollmentPojos) throws NotFoundException, InternalServerErrorException {
        logger.info("registering user {} for session {}", userId, otfSession.getId());
        //TODO use upsert query
        try {
            GTTAttendeeDetails gttAttendeeDetails = gttAttendeeDetailsDAO.getAttendeeDetail(userId, otfSession.getId(), EntityState.ACTIVE);
            if (gttAttendeeDetails == null) {
                gttAttendeeDetails = new GTTAttendeeDetails(otfSession.getId(), userId, enrollmentPojos);
                if (ArrayUtils.isNotEmpty(enrollmentPojos)) {
                    gttAttendeeDetails.setEnrollmentId(enrollmentPojos.get(enrollmentPojos.size() - 1).getId());
                    gttAttendeeDetails.addEnrollmentInfos(enrollmentPojos);
                }
                gttAttendeeDetails.setSessionEndTime(otfSession.getEndTime());
                gttAttendeeDetails.setSessionStartTime(otfSession.getStartTime());
                gttAttendeeDetails.setTraningId(otfSession.getMeetingId());

            } else {
                gttAttendeeDetails.addEnrollmentInfos(enrollmentPojos);
            }

//        gttAttendeeDetailsDAO.update(gttAttendeeDetails);
            gttAttendeeDetailsManager.queueAttendee(gttAttendeeDetails);
            logger.info("Post update : " + gttAttendeeDetails.toString());
            return gttAttendeeDetails;
        } catch (Exception e) {
            logger.error("register attendee err " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public String getTeacherJoinUrl(OTFSession oTFSession, String teacherId) throws NotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getStudentJoinUrl(OTFSession oTFSession, String studentId) throws NotFoundException, InternalServerErrorException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleFeedback(OTFSession oTFSession) throws VException, InternalServerErrorException {
        //do nothing
    }

    @Override
    public List<GTTAttendeeDetails> createGTTAttendeeDetails(OTFSession session, Map<String, List<EnrollmentPojo>> userEnrollmentMap, AttendeeContext context) throws InternalServerErrorException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void userJoined(OTFSession oTFSession, String userId) {
        gttAttendeeDetailsManager.userJoined(oTFSession, userId);
    }

    @Override
    public List<GTTSessionInfoRes> getGTTSessionInfos(OTFSession oTFSession) throws NotFoundException {
        return new ArrayList<>();
    }

    @Override
    public List<GTTAttendeeDetails> registerAttendees(OTFSession oTFSession, Map<String, List<EnrollmentPojo>> studentEnrollmentMap)
            throws InternalServerErrorException, NotFoundException {

        logger.info("registering users");
        // Register attendees
        List<GTTAttendeeDetails> attendeeDetailsList = new ArrayList<>();
        if (CollectionUtils.isEmpty(oTFSession.getAttendees())) {
            // No attendees found
            return attendeeDetailsList;
        }

        // Fetch users
        List<UserBasicInfo> users = fosUtils.getUserBasicInfos(oTFSession.getAttendees(), true);
        if (CollectionUtils.isEmpty(users)) {
            String errorMessage = "Error fetching users. Users not found or fetch size mismatch. attendees :"
                    + oTFSession.getAttendees() + " users:" + (users == null ? "" : Arrays.toString(users.toArray()));
            logger.error(errorMessage);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
        }

        // Validate user count
        if (users.size() != oTFSession.getAttendees().size()) {
            String errorMessage = "Error fetching users. fetch size mismatch. attendees :" + oTFSession.getAttendees()
                    + " users:" + (users == null ? "" : Arrays.toString(users.toArray()));
            logger.info(errorMessage);
        }

        // Register the users
        for (UserBasicInfo user : users) {
            logger.info("users {}", user);
            attendeeDetailsList.add(registerAttendee(oTFSession, user.getUserId().toString(), studentEnrollmentMap.get(user.getUserId().toString())));
        }
        return attendeeDetailsList;
    }

    //overriding all methods
    @Override
    public int scheduleAvailableSlot(long startTime, long endTime) {
        return 1; //no room concept so always available
    }

    @Override
    public boolean isSlotAvailable(long startTime, long endTime) {
        return true;//no room concept so always available
    }

    @Override
    public void descheduleSlot(long startTime, long endTime, int organizerRef) {
        //do nothing
    }

    private void loginToSocialVid() throws VException {
        logger.info("logging into socialvid");
        if ((lastLoggedInAt != null && (System.currentTimeMillis() - lastLoggedInAt) > (long) DateTimeUtils.MILLIS_PER_HOUR)
                || StringUtils.isEmpty(SOCIALVID_TOKEN)) {
            logger.info("logging into socialvid");
            JSONObject loginReq = new JSONObject();
            loginReq.put("email", SOCIALVID_EMAIL);
            loginReq.put("password", SOCIALVID_PASSWORD);
            ClientResponse resp = WebUtils.INSTANCE.doCall(LOGIN_URL, HttpMethod.POST,
                    loginReq.toString());
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info(jsonString);
            JSONObject loginResp = new JSONObject(jsonString);
            if (StringUtils.isNotEmpty(loginResp.getString("session"))) {
                SOCIALVID_TOKEN = loginResp.getString("session");
                lastLoggedInAt = System.currentTimeMillis();
            }
        }
    }

    public void changeServiceProviderToImpartus(OTFSession otfSession)
            throws VException, IOException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        String conferenceId = addImpartusConf(otfSession);
        logger.info("got confernece id" + conferenceId);
        if (StringUtils.isEmpty(conferenceId)) {
            throw new InternalServerErrorException(ErrorCode.ADD_IMPARTUS_CONFERENCE_FAILED,
                    "add impartus conf failed for " + otfSession.getId());
        }
        otfSession.setMeetingId(conferenceId);
        otfSession.setServiceProvider(OTMServiceProvider.IMPARTUS);
    }

    private String calculateRFC2104HMAC(String data, String key) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
        mac.init(signingKey);
        return toHexString(mac.doFinal(data.getBytes()));
    }

    private String addImpartusConf(OTFSession oTFSession) throws VException, NoSuchAlgorithmException, InvalidKeyException, IOException {
        logger.info("add impartus conf" + oTFSession.getId());
        JSONObject token = new JSONObject();
        token.put("apiKey", IMPARTUS_API_KEY);
        token.put("date", Math.round(System.currentTimeMillis() / 1000));
        token.put("nonce", Math.round(Math.random() * 100000));
        String msg = token.getString("apiKey") + token.getLong("date") + token.getLong("nonce");
        String hmac = calculateRFC2104HMAC(msg, IMPARTUS_API_SECRET);
        token.put("signature", hmac);
        String authHeader = Base64.getEncoder().encodeToString(token.toString().getBytes("UTF-8"));

        JSONObject impartusOptions = new JSONObject();
        impartusOptions.put("mediaMode", "routed");
        impartusOptions.put("duration", Long.parseLong("3600"));

        JSONObject impartusOptionsReq = new JSONObject();
        impartusOptionsReq.put("options", impartusOptions);

        ClientResponse resp = WebUtils.INSTANCE.doCall(IMPARTUS_DOMAIN, HttpMethod.POST, impartusOptionsReq.toString(), "VCAuth " + authHeader);
        logger.info("response from impartus" + resp);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        JSONObject addConfResp = new JSONObject(jsonString);
        if (addConfResp.has("sessionId")) {
            return Integer.toString(addConfResp.getInt("sessionId"));
        } else {
            return null;
        }
    }

    private String addConference(String sessionId) throws VException {
        logger.info("addConference in socialvid  for " + sessionId);

//        var args = {"email": configProps.get('socialvid').email, "session": configProps.getVal('socialVidToken'),
//            "name": sessionId, "mode": "group", "autoRecord": true,
//            "maxBitrateKbps": "512", "maxParticipants": "100"};
//        var params = {data: args, headers: {"Content-Type": "application/json"}};        
        JSONObject addConfReq = new JSONObject();
        addConfReq.put("email", SOCIALVID_EMAIL);
        addConfReq.put("session", SOCIALVID_TOKEN);
        addConfReq.put("name", sessionId);
        addConfReq.put("mode", "presenter");//presenter
        addConfReq.put("autoRecord", true);
        addConfReq.put("maxBitrateKbps", "512");
        addConfReq.put("maxParticipants", "9");
        ClientResponse resp = WebUtils.INSTANCE.doCall(ADD_CONFERENCE, HttpMethod.POST,
                addConfReq.toString());
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        JSONObject addConfResp = new JSONObject(jsonString);
        if (StringUtils.isNotEmpty(addConfResp.getString("id"))) {
            return addConfResp.getString("id");
        } else {
            return null;
        }
    }

    public void setTeacherJoinUrlFromConference(String conferenceId, OTFSession session) throws VException {
//               var args = {"email": configProps.get('socialvid').email, "session": configProps.getVal('socialVidToken'),
//            "id": conferenceId };
        logger.info("getConference for " + conferenceId);
        JSONObject getConfReq = new JSONObject();
        getConfReq.put("email", SOCIALVID_EMAIL);
        getConfReq.put("session", SOCIALVID_TOKEN);
        getConfReq.put("id", conferenceId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(GET_CONFERENCE, HttpMethod.POST,
                getConfReq.toString());
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        //{"id":"ba4e68e75712b408","name":"randomId db17f773-85b5-411c-862f-a022f592fa3d","autoRecord":true,"maxParticipants":100,"maxBitrateKbps":"512","mode":"group","urls":{"audioOnly":{"moderator":"https://vedantu2.socialvid.in/guest.html?conferenceId=ba4e68e75712b408&audio=1&video=0&dialout=0&mode=group&moderator=1&c=8e69ecb4d586ad47","participant":"https://vedantu2.socialvid.in/guest.html?conferenceId=ba4e68e75712b408&audio=1&video=0&dialout=0&mode=group&moderator=0&c=fa2a7a528437767f"},"audioVideo":{"moderator":"https://vedantu2.socialvid.in/guest.html?conferenceId=ba4e68e75712b408&audio=1&video=1&dialout=0&mode=group&moderator=1&c=a20760da9fdd1750","participant":"https://vedantu2.socialvid.in/guest.html?conferenceId=ba4e68e75712b408&audio=1&video=1&dialout=0&mode=group&moderator=0&c=83407ab38f405b44"},"shareOnly":{"moderator":"https://vedantu2.socialvid.in/guest.html?conferenceId=ba4e68e75712b408&audio=0&video=0&dialout=1&mode=group&moderator=1&c=2fce0e527f6ff850","participant":"https://vedantu2.socialvid.in/guest.html?conferenceId=ba4e68e75712b408&audio=0&video=0&dialout=1&mode=group&moderator=0&c=2abeeabc0d352c36"}}}
        JSONObject getConfResp = new JSONObject(jsonString);
        if (getConfResp.getJSONObject("urls") != null && getConfResp.getJSONObject("urls").getJSONObject("audioVideo") != null) {
            JSONObject audioVideoUrls = getConfResp.getJSONObject("urls").getJSONObject("audioVideo");
            session.setSessionURL(audioVideoUrls.getString("moderator"));
            session.setSocialVidConfResp(jsonString);
            session.setMeetingId(conferenceId);
        }
    }

    public static void main(String[] args) throws VException {
        VedantuWaveScheduleManager obj = new VedantuWaveScheduleManager();
        obj.loginToSocialVid();
        String conferenceId = obj.addConference("randomId " + UUID.randomUUID());
//        SOCIALVID_TOKEN="b3ec232160c7432a";
//        String conferenceId="7a0f13f88c5ce785";
        obj.setTeacherJoinUrlFromConference(conferenceId, null);
    }

    //TODO token expiry, max bitrate, max participants

}
