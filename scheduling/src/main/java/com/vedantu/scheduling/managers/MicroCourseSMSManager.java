package com.vedantu.scheduling.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.request.MicroCourseSMSReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MicroCourseSMSManager {

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFSessionManager.class);

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private OTFSessionDAO otfSessionDAO;

    private static Gson gson = new Gson();

    private static final Long oneHourInMillis = 3600000L;
    private static final Long fifteenMinInMillis = 900000L;
    private static final Long twoMinInMillis = 120000L;

    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";


    public void sendRemainderSMSAsync() {
        logger.info("SEND REMAINDER SMS");
        List<OTFSession> oneHourRemainderList = new ArrayList<>();
        List<OTFSession> fifteenMinRemainderList = new ArrayList<>();
        List<OTFSession> liveRemainderList = new ArrayList<>();

        Long oneHourStartTime = System.currentTimeMillis() + oneHourInMillis - twoMinInMillis;
        Long oneHourEndTime = System.currentTimeMillis() + oneHourInMillis + twoMinInMillis;

        Long fifteenMinStartTime = System.currentTimeMillis() + fifteenMinInMillis - twoMinInMillis;
        Long fifteenMinEndTime = System.currentTimeMillis() + fifteenMinInMillis + twoMinInMillis;

        Long liveStartTime = System.currentTimeMillis() - twoMinInMillis;
        Long liveEndTime = System.currentTimeMillis() + twoMinInMillis;

        oneHourRemainderList = otfSessionDAO.getSessionsBetweenTimeForSMS(oneHourStartTime, oneHourEndTime);
        fifteenMinRemainderList = otfSessionDAO.getSessionsBetweenTimeForSMS(fifteenMinStartTime, fifteenMinEndTime);
        liveRemainderList = otfSessionDAO.getSessionsBetweenTimeForSMS(liveStartTime, liveEndTime);


        if(oneHourRemainderList.size() > 0) {
            logger.info("Sending Hourly SMS");
            MicroCourseSMSReq hourlyRemainderReq = new MicroCourseSMSReq();
            hourlyRemainderReq.setSessionDataList(oneHourRemainderList);

            Map<String, Object> payload = new HashMap<>();
            payload.put("hourlyRemainderReq", hourlyRemainderReq);
            AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.SEND_HOURLY_MICROCOURSES_REMAINDER, payload);
            asyncTaskFactory.executeTask(asyncTaskParams);
        }

        if(fifteenMinRemainderList.size() > 0) {
            logger.info("Sending Fifteen Min SMS");
            MicroCourseSMSReq fifteenMinRemReq = new MicroCourseSMSReq();
            fifteenMinRemReq.setSessionDataList(fifteenMinRemainderList);

            Map<String, Object> payload = new HashMap<>();
            payload.put("fifteenMinRemReq", fifteenMinRemReq);
            AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.SEND_FIFTEEN_MINS_MICROCOURSES_REMAINDER, payload);
            asyncTaskFactory.executeTask(asyncTaskParams);
        }


        if(liveRemainderList.size() > 0) {
            logger.info("Sending Live Class SMS");
            MicroCourseSMSReq liveRemReq = new MicroCourseSMSReq();
            liveRemReq.setSessionDataList(liveRemainderList);

            Map<String, Object> payload = new HashMap<>();
            payload.put("liveRemReq", liveRemReq);
            AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.SEND_LIVE_MICROCOURSES_REMAINDER, payload);
            asyncTaskFactory.executeTask(asyncTaskParams);
        }

    }

    public void sendHourlySMS(MicroCourseSMSReq hourlySMSReq) {
        try {
            ClientResponse resp1 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/microcourses/sendHourlyRemainderSMS",
                    HttpMethod.POST, new Gson().toJson(hourlySMSReq));
            VExceptionFactory.INSTANCE.parseAndThrowFosException(resp1);
        } catch (VException ex) {
            logger.info("Error in sending Hourly SMS :" + ex);
        }
    }

    public void sendFifteenMinSMS(MicroCourseSMSReq fifteenMinSMSReq) {
        try {
            ClientResponse resp2 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/microcourses/sendFifteenMinRemainderSMS",
                    HttpMethod.POST, new Gson().toJson(fifteenMinSMSReq));
            VExceptionFactory.INSTANCE.parseAndThrowFosException(resp2);
        } catch (VException ex) {
            logger.info("Error in sending fifteen min remainder SMS :" + ex);
        }
    }

    public void sendLiveSMS(MicroCourseSMSReq liveSMSReq) {
        try {
            ClientResponse resp3 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/microcourses/sendLiveRemainderSMS",
                    HttpMethod.POST, new Gson().toJson(liveSMSReq));
            VExceptionFactory.INSTANCE.parseAndThrowFosException(resp3);
        } catch (VException ex) {
            logger.info("Error in sending live microcourses remainder SMS :" + ex);
        }
    }
}
