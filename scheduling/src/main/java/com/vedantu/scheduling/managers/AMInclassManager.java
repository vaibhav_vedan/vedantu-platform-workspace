package com.vedantu.scheduling.managers;

import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.enums.InsightGrades;
import com.vedantu.scheduling.pojo.AttemptStat;
import com.vedantu.scheduling.pojo.GTTAttendanceBasic;
import com.vedantu.scheduling.response.*;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.scheduling.controllers.OTFSessionController;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.serializers.AMInclassDAO;
import com.vedantu.scheduling.pojo.AMInclassAggregateResp;
import com.vedantu.scheduling.pojo.GTTAttendeeDetailsWithTopics;

@Service
public class AMInclassManager {

	private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

	@Autowired
	private OTFSessionDAO oTFSessionDAO;

	@Autowired
	private AMInclassDAO amInclassDAO;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private OTFSessionController otfSessionController;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(AMInclassManager.class);

	public AMInclassAggregateResp getAMInClassAggregate(Long studentId, Long fromTime, Long thruTime) {
		List<GTTAttendeeDetails> attendedSessionDetails = amInclassDAO.getAttendedGTTAttendeeDetailsInRange(studentId,
				fromTime, thruTime);
		List<GTTAttendeeDetails> allSessionDetails = amInclassDAO.getAllGTTAttendeeDetailsInRange(studentId, fromTime,
				thruTime);
		return prepareAMInclassEngagementAggregate(attendedSessionDetails, allSessionDetails.size());
	}

	public StudentInclassDetailedRes getAMInClassDetailed(Long studentId, Long fromTime, Long thruTime, Boolean includeSession)
			throws VException {
		List<GTTAttendeeDetails> details = amInclassDAO.getAllGTTAttendeeDetailsInRange(studentId, fromTime, thruTime);
		Map<Long, List<GTTAttendeeDetailsWithTopics>> boardIdAttendeeDetailsMap = new HashMap<>();
		Set<Long> boardIds = new HashSet<>();
		List<OTFSession> sessionList = new ArrayList<>();
		for (GTTAttendeeDetails each : details) {
			OTFSession otfSession = otfSessionController.getSessionById(each.getSessionId());
			boardIds.add(otfSession.getBoardId());
			List<String> topics = null; // get list of topics taken in the session
			GTTAttendeeDetailsWithTopics gttAttendeeDetailsWithTopics = new GTTAttendeeDetailsWithTopics(topics, each);
			if (boardIdAttendeeDetailsMap.containsKey(otfSession.getBoardId())) {
				List<GTTAttendeeDetailsWithTopics> boardAttendeeDetails = boardIdAttendeeDetailsMap
						.get(otfSession.getBoardId());
				boardAttendeeDetails.add(gttAttendeeDetailsWithTopics);
				boardIdAttendeeDetailsMap.put(otfSession.getBoardId(), boardAttendeeDetails);
			} else {
				List<GTTAttendeeDetailsWithTopics> boardAttendeeDetails = new ArrayList<>();
				boardAttendeeDetails.add(gttAttendeeDetailsWithTopics);
				boardIdAttendeeDetailsMap.put(otfSession.getBoardId(), boardAttendeeDetails);
			}
			sessionList.add(otfSession);
		}
		logger.info("Logger info: subjectwiseDetails" + boardIdAttendeeDetailsMap);

		FosUtils fosUtils = new FosUtils();
		Map<Long, Board> boardInfoMap = fosUtils.getBoardInfoMap(boardIds);

		List<AMInclassDetailedRes> amInclassDetailedResList = new ArrayList<>();
		for (Long key : boardInfoMap.keySet()) {
			AMInclassDetailedRes amInclassDetailedRes = new AMInclassDetailedRes(key, boardInfoMap.get(key).getName(),
					boardIdAttendeeDetailsMap.get(key));
			amInclassDetailedResList.add(amInclassDetailedRes);
		}

		StudentInclassDetailedRes studentInclassDetailedRes = new StudentInclassDetailedRes(getUserByUserId(studentId),
				amInclassDetailedResList);
		if(includeSession){
			studentInclassDetailedRes.setSessionList(sessionList);
		}

		return studentInclassDetailedRes;
	}


//	public List<Map<String,Object>> loam_getInClassAttendanceStats(Long studentId, Long fromTime, Long thruTime, Long boardId)
//			throws VException {
//		List<Map<String,Object>> res=new ArrayList<>();
//
//		//Fetch all student attendance records from time range given
//		List<GTTAttendeeDetails> rawGtt = amInclassDAO.loam_getAttendedGTTAttendeeDetailsInSessionRange(studentId, fromTime, thruTime);
//		List<String> sids=new ArrayList<>();
//
//		//Collect all session ids from student attendance
//		sids = rawGtt.parallelStream().map(gttAttendeeDetails -> gttAttendeeDetails.getSessionId()).collect(Collectors.toList());
//
//		List<OTFSession> sessionList;
//		if(boardId==-1){
//			sessionList= oTFSessionDAO.getBy_Ids(sids);
//		}else{
//			sessionList = oTFSessionDAO.getByBoardIdInSessions(boardId,sids);
//		}
//		sids.clear();
//		long newFromTime=thruTime;
//		long newThruTime=fromTime;
//		for(OTFSession os:sessionList){
//			if(null == os.getStartTime() || null == os.getEndTime()){
//				sids.add(os.getId());
//				continue;
//			}
//			if(os.getStartTime()<newFromTime){
//				newFromTime=os.getStartTime();
//			}
//			if(os.getEndTime()>newThruTime){
//				newThruTime=os.getEndTime();
//			}
//			sids.add(os.getId());
//		}
//		logger.info("SESSSION_ID*****************"+sids);
//		//For getting total number of students suppose to attend the session and calculate individual attendance
//		List<GTTAttendeeDetails> forMap = amInclassDAO.loam_getGTTAttendeeDetailsForSessions(sids);
//		logger.info("#########################"+forMap);
//		Map<String,Integer> sessionTotalStudentsMap=new HashMap<>();
//
//		LinkedHashMap<String, Double> weekWiseSessionInd = new LinkedHashMap<>();
//
//		Map<String,List<String>> ss=new HashMap<>();
//		for(GTTAttendeeDetails gt:forMap){
//			if (gt.getEnrollmentId() != null) {
//				if (sessionTotalStudentsMap.containsKey(gt.getSessionId())) {
//					sessionTotalStudentsMap.put(gt.getSessionId(), sessionTotalStudentsMap.get(gt.getSessionId()) + 1);
//				} else {
//					sessionTotalStudentsMap.put(gt.getSessionId(), 1);
//				}
//			}
//
//			if(gt.getEnrollmentId() != null && gt.getUserId().equalsIgnoreCase(studentId.toString())){
//
//				int weekCount=1;
//
//				for(long st=newFromTime,et=0;st<=newThruTime;st=st+604800000){
//					et=st+604800000;
//					if(null != gt.getSessionStartTime() && gt.getSessionStartTime()>=st && gt.getSessionStartTime()<=et){
//						if(ArrayUtils.isNotEmpty(gt.getJoinTimes())){
//							if(weekWiseSessionInd.containsKey("Week "+weekCount)){
//								weekWiseSessionInd.put("Week "+weekCount,weekWiseSessionInd.get("Week "+weekCount)+1);
//								//to see log for sessions
//								List<String> ses=ss.get("Week "+weekCount);
//								ses.add(gt.getSessionId());
//								ss.put("Week "+weekCount,ses);
//							}else{
//								weekWiseSessionInd.put("Week "+weekCount,1.0);
//								List<String> ses=new ArrayList<>();
//								ses.add(gt.getSessionId());
//								ss.put("Week "+weekCount,ses);
//							}
//						}
//					}
//					weekCount++;
//				}
//			}
//
//		}
//
//		logger.info("WeekwiseSessions***********************8:"+ss);
//		LinkedHashMap<String, Object> weekWiseSessionInd1 = new LinkedHashMap<>();
//
//		//For the average calulation
//		HashMap<String, Object> weekWiseSession = new HashMap<>();
//		int weekCountAvg=1;
//		double totalWeekCount = 0;
//		for(long st=newFromTime,et=0;st<=newThruTime;st=st+604800000){
//			et=st+604800000;
//			double weekAverage=0.0;
//			int totalSessionInWeek=0;
//			for (OTFSession session:sessionList) {
//
//				if(null == session.getStartTime()){
//					totalSessionInWeek+= 1;
//					continue;
//				}
//				if(session.getStartTime()>=st && session.getStartTime()<=et && sessionTotalStudentsMap.containsKey(session.getId())){
//					double a=sessionTotalStudentsMap.get(session.getId());
//					weekAverage+=null!=session.getUniqueStudentAttendants()?(session.getUniqueStudentAttendants()/a):0;
//					totalSessionInWeek+= 1;
//					logger.info("total session:- "+sessionTotalStudentsMap.get(session.getId()));
//					logger.info("weekAverage:-"+weekAverage);
//				}
//			}
//			double weekInd=0.0;
//			if(totalSessionInWeek==0){
//				weekAverage=0;
//			}else{
//				weekAverage=(weekAverage/totalSessionInWeek) *100;
//				weekInd=null!=weekWiseSessionInd.get("Week "+weekCountAvg)?(weekWiseSessionInd.get("Week "+weekCountAvg)/totalSessionInWeek)*100:0;
//				logger.info("if else weekavg"+weekAverage);
//				logger.info("if else weekend"+weekInd);
//			}
//
//			weekWiseSession.put("Week "+weekCountAvg,weekAverage);
//			weekWiseSessionInd1.put("Week "+weekCountAvg,weekInd);
//			//counting total attendance individual
//			totalWeekCount = totalWeekCount + weekInd;
//			weekCountAvg += 1;
//		}
//		//counting total week count average
//		Double totalWeekAverage = totalWeekCount / (weekCountAvg-1);
//		Map<String,Object> averageMap = new HashMap<>();
//		averageMap.put("insight_mark",totalWeekAverage);
//		averageMap.put("insight_average", loam_calculateAttendanceInsight(totalWeekAverage));
////		if(InsightGrades.getGrade(totalWeekAverage)!=null)
////			averageMap.put("insight_average", InsightGrades.getGrade(totalWeekAverage).getGrade());
////		else
////			averageMap.put("insight_average", null);
//
//
////        List<GTTAttendeeDetails> studentGtt = amInclassDAO.getAllGTTAttendeeDetailsInRangeForSessions(studentId,sids,fromTime,thruTime);
////        Map<String, Double> weekWiseSessionInd = new HashMap<>();
////        int weekCount=1;
////        for(long st=fromTime,et=0;st<=thruTime;st=st+604800000){
////            et=st+604800000;
////            double weekTotal=0;
////            for(GTTAttendeeDetails gtt:studentGtt){
////
////                if(gtt.getSessionStartTime()>=st && gtt.getSessionStartTime()<=et){
////                    if(ArrayUtils.isNotEmpty(gtt.getJoinTimes())){
////                        weekTotal++;
////                    }
////                }
////            }
////            weekWiseSessionInd.put("Week "+weekCount,weekTotal);
////            weekCount++;
////        }
//		res.add(weekWiseSessionInd1);
//		logger.info(weekWiseSessionInd);
//		res.add(weekWiseSession);
//		logger.info(weekWiseSession);
//		res.add(averageMap);
//		logger.info("Attendance Insight average {}",averageMap);
//		return res;
//	}

//	/**
//	 * gets the grade for attendance
//	 *
//	 * @param totalWeekAverage
//	 * @return
//	 */
//	private String loam_calculateAttendanceInsight(Double totalWeekAverage) {
//		String[] good = ConfigUtils.INSTANCE.getStringValue("GOOD").split("\\$");
//		String[] poor = ConfigUtils.INSTANCE.getStringValue("POOR").split("\\$");
//		String[] bad = ConfigUtils.INSTANCE.getStringValue("BAD").split("\\$");
//		logger.info("loam_calculateAttendanceInsight - good : {}, poor: {}, bad: {}", good, poor, bad);
//		if (totalWeekAverage >= Double.parseDouble(good[1])) {
//			return good[0];
//		} else if (totalWeekAverage >= Double.parseDouble(poor[1].split("-")[0]) && totalWeekAverage <= Double.parseDouble(poor[1].split("-")[1])) {
//			return poor[0];
//		} else {
//			return bad[0];
//		}
//	}

	/*public Map<String, Object> loam_getInClassEngagement(Long studentId, Long fromTime, Long thruTime, Long boardId)
			throws VException {
		List<OTFSession> sessionList;
		if(boardId==-1){
			sessionList= loam.loam_getAllSessions(fromTime,thruTime);
		}else{
			sessionList = oTFSessionDAO.loam_getByBoardId(boardId,fromTime,thruTime);
		}
		List<String> sessionIds=new ArrayList<>();
		for (OTFSession session:sessionList) {
			sessionIds.add(session.getId());
		}
		List<GTTAttendeeDetails> details = amInclassDAO.loam_getAllGTTAttendeeDetailsInRangeForSessions(studentId,sessionIds, fromTime, thruTime);
		Map<String, AMSessionWiseDataRes> sessionIdGTTAttendeeDetailMap = new HashMap<>();
		Map<String, List<OTMSessionEngagementData>> otmSessionEngagementDataMap = new HashMap<>();
		Map<String,Object> insightSessionMap = new HashMap<>();
		List<OTMSessionEngagementData> otmSessionEngagementDataList = amInclassDAO.loam_getOTMSessionEngagementDataList(sessionIds);
		for(OTMSessionEngagementData data : otmSessionEngagementDataList){
			if(otmSessionEngagementDataMap.containsKey(data.getSessionId())){
				List<OTMSessionEngagementData> list = otmSessionEngagementDataMap.get(data.getSessionId());
				list.add(data);
				otmSessionEngagementDataMap.put(data.getSessionId(), list);
			}
			else{
				List<OTMSessionEngagementData> list = new ArrayList<>();
				list.add(data);
				otmSessionEngagementDataMap.put(data.getSessionId(), list);
			}
		}
		for (GTTAttendeeDetails each : details) {

			GTTAttendanceBasic avg= loam_calculateAvgAttemptStat(otmSessionEngagementDataMap.get(each.getSessionId()));
			GTTAttendanceBasic ind= loam_calculateAttemptStat(each);
			AMSessionWiseDataRes am=new AMSessionWiseDataRes(ind,avg, each.getCreationTime());
			sessionIdGTTAttendeeDetailMap.put(each.getSessionId(),am);

		}
		insightSessionMap.put("sessions",sessionIdGTTAttendeeDetailMap);
		insightSessionMap.put("insights",loam_calculateInsightsForEngagement(sessionIdGTTAttendeeDetailMap));
		return insightSessionMap;
	}

	*//**
	 * Calculating total engagement average for student
	 *
	 * @param
	 * @return
	 *//*
	private Map<String, Object> loam_calculateInsightsForEngagement(Map<String, AMSessionWiseDataRes> sessionIdGTTAttendeeDetailMap) {

		double correctQuiz = 0;
		double totalQuiz = 0;
		double correctHotspot = 0;
		double totalHotspot = 0;
		double percentQuiz =0;
		double percentHotspot =0;
		Map<String, Object> insightMap = new HashMap<>();
		//get correct and total values for quiz and hotspot
		for (Map.Entry<String, AMSessionWiseDataRes> entry : sessionIdGTTAttendeeDetailMap.entrySet()) {
			AMSessionWiseDataRes amSessionWiseData = entry.getValue();
			AttemptStat quiz = amSessionWiseData.getIndividual().getQuizNumbers();
			AttemptStat hotspot = amSessionWiseData.getIndividual().getHotspotNumbers();

			if (quiz != null) {
				correctQuiz += quiz.getCorrect();
				totalQuiz += quiz.getCorrect() + quiz.getWrong() + quiz.getUnAttempted();
			}
			if (hotspot != null) {
				correctHotspot += hotspot.getCorrect();
				totalHotspot += hotspot.getCorrect() + hotspot.getWrong() + hotspot.getUnAttempted();
			}
		}
		//calculating percentage
		if(totalQuiz > 0){
			percentQuiz = (correctQuiz *100) /totalQuiz;
		}
		if(totalHotspot >0){
			percentHotspot = (correctHotspot * 100)/totalHotspot;
		}
		insightMap.put("insight_quiz_mark",percentQuiz);
		if(null != InsightGrades.getGrade(percentQuiz))
			insightMap.put("insight_quiz_grade",InsightGrades.getGrade(percentQuiz).getGrade());
		else
			insightMap.put("insight_quiz_grade", null);

		insightMap.put("insight_hotspot_mark",percentHotspot);
		if(null != InsightGrades.getGrade(percentHotspot))
			insightMap.put("insight_hotspot_grade",InsightGrades.getGrade(percentHotspot).getGrade());
		else
			insightMap.put("insight_hotspot_grade",null);
		logger.info("insight map details: {}",insightMap);
		return insightMap;
	}

	private GTTAttendanceBasic loam_calculateAvgAttemptStat(List<OTMSessionEngagementData>  sessionEngagementData) {
		AttemptStat quiz = new AttemptStat();
		quiz.setWrong(0.0);
		quiz.setCorrect(0.0);
		quiz.setUnAttempted(0.0);
		AttemptStat hotspot = new AttemptStat();
		hotspot.setCorrect(0.0);
		hotspot.setUnAttempted(0.0);
		hotspot.setWrong(0.0);
		int quizStudentCount = 0;
		int hotspotStudentCount = 0;
		if(null == sessionEngagementData || sessionEngagementData.size() == 0){
			return new GTTAttendanceBasic(quiz, hotspot);
		}
		logger.info(sessionEngagementData);
		for(OTMSessionEngagementData data : sessionEngagementData){
			if(null != data.getContext() && data.getContext().toString().equalsIgnoreCase("QUIZ")){
				double correctQuiz = 0.0;
				if(null != data.getTotalCorrectResponses()){
					correctQuiz = quiz.getCorrect() + data.getTotalCorrectResponses();
					quiz.setCorrect(correctQuiz);
				}
				double wrongQuiz = 0.0;
				if(null != data.getTotalIncorrectResponses()){
					wrongQuiz = quiz.getWrong() + data.getTotalIncorrectResponses();
					quiz.setWrong(wrongQuiz);
				}
				double unAttemptedQuiz = 0.0;
				if(null != data.getTotalUnattempts()){
					unAttemptedQuiz = quiz.getUnAttempted() + data.getTotalUnattempts();
					quiz.setUnAttempted(unAttemptedQuiz);
				}
				if(null != data.getStudentCountAtStart()) {
					quizStudentCount = data.getStudentCountAtStart();
				}
			}
			else if(null != data.getContext() && data.getContext().toString().equalsIgnoreCase("HOTSPOT")){

				double correctHotspot = 0.0;
				if(null != data.getTotalCorrectResponses()) {
					correctHotspot = hotspot.getCorrect() + data.getTotalCorrectResponses();
					hotspot.setCorrect(correctHotspot);
				}
				double wrongHotspot = 0.0;
				if(null != data.getTotalIncorrectResponses()){
					wrongHotspot = hotspot.getWrong() + data.getTotalIncorrectResponses();
					hotspot.setWrong(wrongHotspot);
				}
				double unAttemptedHotspot = 0.0;
				if(null != data.getTotalUnattempts()){
					unAttemptedHotspot = hotspot.getUnAttempted() + data.getTotalUnattempts();
					hotspot.setUnAttempted(unAttemptedHotspot);
				}
				if(null != data.getStudentCountAtStart()){
					hotspotStudentCount = data.getStudentCountAtStart();
				}
			}
		}
		if(quizStudentCount > 0 ) {
			quiz.setCorrect(quiz.getCorrect()/quizStudentCount);
			quiz.setWrong(quiz.getWrong()/quizStudentCount);
			quiz.setUnAttempted(quiz.getUnAttempted()/quizStudentCount);
		}
		if(hotspotStudentCount  > 0){
			hotspot.setCorrect(hotspot.getCorrect()/hotspotStudentCount );
			hotspot.setWrong(hotspot.getWrong()/hotspotStudentCount );
			hotspot.setUnAttempted(hotspot.getUnAttempted()/hotspotStudentCount);
		}
		return new GTTAttendanceBasic(quiz, hotspot);
	}
*/
	private GTTAttendanceBasic loam_calculateAttemptStat(GTTAttendeeDetails gtt){
		AttemptStat quiz=new AttemptStat();
		AttemptStat hotspot=new AttemptStat();
		if(gtt.getQuizNumbers()!=null){
			quiz.setCorrect((double)gtt.getQuizNumbers().getNumberCorrect());
			quiz.setUnAttempted((double)(gtt.getQuizNumbers().getNumberAsked()-gtt.getQuizNumbers().getNumberAttempted()));
			quiz.setWrong((double)(gtt.getQuizNumbers().getNumberAttempted()-gtt.getQuizNumbers().getNumberCorrect()));
		}else {
			quiz=null;
		}
		if(gtt.getHotspotNumbers()!=null){
			hotspot.setCorrect((double)gtt.getHotspotNumbers().getNumberCorrect());
			hotspot.setUnAttempted((double)(gtt.getHotspotNumbers().getNumberAsked()-gtt.getHotspotNumbers().getNumberAttempted()));
			hotspot.setWrong((double)(gtt.getHotspotNumbers().getNumberAttempted()-gtt.getHotspotNumbers().getNumberCorrect()));
		}else{
			hotspot=null;
		}

		return  new GTTAttendanceBasic(quiz,hotspot);
	}

	public AMInclassAggregateResp prepareAMInclassEngagementAggregate(List<GTTAttendeeDetails> details,
			long totalSessionsInRange) {
		long noOfSessionsAttended = details.size();

		long quizAsked = 0;
		long quizAttempted = 0;
		long quizCorrect = 0;
		long quizIncorrect = 0;

		long hotspotAsked = 0;
		long hotspotAttempted = 0;
		long hotspotCorrect = 0;
		long hotspotIncorrect = 0;

		long doubtsAsked = 0;
		long doubtsResolved = 0;

		for (GTTAttendeeDetails each : details) {
			if (each.getQuizNumbers() != null && each.getQuizNumbers().getNumberAttempted() != null
					&& each.getQuizNumbers().getNumberCorrect() != null
					&& each.getQuizNumbers().getNumberAsked() != null) {
				quizAsked += each.getQuizNumbers().getNumberAsked();
				quizAttempted += each.getQuizNumbers().getNumberAttempted();
				quizCorrect += each.getQuizNumbers().getNumberCorrect();
			}

			if (each.getHotspotNumbers() != null && each.getHotspotNumbers().getNumberAttempted() != null
					&& each.getHotspotNumbers().getNumberCorrect() != null
					&& each.getHotspotNumbers().getNumberAsked() != null) {
				hotspotAsked += each.getHotspotNumbers().getNumberAsked();
				hotspotAttempted += each.getHotspotNumbers().getNumberAttempted();
				hotspotCorrect += each.getHotspotNumbers().getNumberCorrect();
			}

			if (each.getDoubtNumbers() != null && each.getDoubtNumbers().getNumberAsked() != null
					&& each.getDoubtNumbers().getNumberResolved() != null) {
				doubtsAsked += each.getDoubtNumbers().getNumberAsked();
				doubtsResolved += each.getDoubtNumbers().getNumberResolved();
			}
		}

		quizIncorrect = quizAttempted - quizCorrect;
		hotspotIncorrect = hotspotAttempted - hotspotCorrect;

		return new AMInclassAggregateResp(totalSessionsInRange, noOfSessionsAttended, quizAsked, quizAttempted,
				quizCorrect, quizIncorrect, hotspotAsked, hotspotAttempted, hotspotCorrect, hotspotIncorrect,
				doubtsAsked, doubtsResolved);
	}

	private User getUserByUserId(final Long userId) throws VException {
		String url = USER_ENDPOINT + "/" + "getUserById?userId=" + userId;
		logger.info("Logger: url composed is" + url);
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);
		User user = new Gson().fromJson(respString, User.class);
		logger.info("Logger: Callresponse user: " + user);
		return user;
	}

	public boolean hasBadAttendanceInPast3Sessions(Long studentId, Long creationTime) {
		List<GTTAttendeeDetails> last3Sessions = amInclassDAO.getLastThreeSessionsOfStudent(studentId);
		if (last3Sessions.size() == 3 && last3Sessions.get(0).getCreationTime() >= creationTime
				&& (last3Sessions.get(0).getStudentSessionJoinTime() == null
						|| last3Sessions.get(0).getStudentSessionJoinTime() == 0l)
				&& (last3Sessions.get(1).getStudentSessionJoinTime() == null
						|| last3Sessions.get(1).getStudentSessionJoinTime() == 0l)
				&& (last3Sessions.get(2).getStudentSessionJoinTime() == null
						|| last3Sessions.get(2).getStudentSessionJoinTime() == 0l)) {
			return true;
		}
		return false;
	}

	public boolean hasBadAttendanceInPastMonth(Long studentId, Long creationTime) {
		Long fromTime = creationTime - 30 * DateTimeUtils.MILLIS_PER_DAY;
		long allCount = amInclassDAO.countAllSessions(studentId, fromTime);
		long attendedCount = amInclassDAO.countAttendedSessions(studentId, fromTime);
		logger.info("studentId: " + studentId + ", allcount: " + allCount + ", attendedCount: " + attendedCount);
		if ( allCount !=0  && ( (float) attendedCount / allCount < 0.3) ) {
			return true;
		}
		return false;
	}

	public boolean isLessThan20PercentEngagement(Long studentId, Long creationTime) {
		List<GTTAttendeeDetails> last3AttendedSessions = amInclassDAO.getLastThreeAttendedSessionsOfStudent(studentId);
		if (last3AttendedSessions.size() == 3 && last3AttendedSessions.get(0).getCreationTime() >= creationTime) {
			long engagementAsked = 0;
			long engagementAttempted = 0;
			for (GTTAttendeeDetails session : last3AttendedSessions) {
				if (session.getQuizNumbers() != null && session.getQuizNumbers().getNumberAsked() != null)
					engagementAsked += session.getQuizNumbers().getNumberAsked();
				if (session.getHotspotNumbers() != null && session.getHotspotNumbers().getNumberAsked() != null)
					engagementAsked += session.getHotspotNumbers().getNumberAsked();
				if (session.getQuizNumbers() != null && session.getQuizNumbers().getNumberAttempted() != null)
					engagementAttempted += session.getQuizNumbers().getNumberAttempted();
				if (session.getHotspotNumbers() != null && session.getHotspotNumbers().getNumberAttempted() != null)
					engagementAttempted += session.getHotspotNumbers().getNumberAttempted();
			}
			logger.info("studentId: " +studentId + ", engagementAsked: "+ engagementAsked + "engagementAttempted: " +engagementAttempted);
			if ( engagementAsked != 0 && ( (float) engagementAttempted / engagementAsked <= 0.2) ) {
				return true;
			}
		}
		return false;
	}

}
