package com.vedantu.scheduling.managers.earlylearning;

import com.google.gson.Gson;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.Interfaces.ITeacherProficiencyManager;
import com.vedantu.scheduling.dao.serializers.CalendarEntryDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.enums.earlyLeaning.AnalyticsType;
import com.vedantu.scheduling.managers.*;
import com.vedantu.scheduling.pojo.EarlyLearningSessionPojo;
import com.vedantu.scheduling.pojo.earlylearning.CourseConfiguration;
import com.vedantu.scheduling.pojo.earlylearning.CourseProperties;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.scheduling.request.earlylearning.AddDemoRequest;
import com.vedantu.scheduling.request.earlylearning.JoinDemoRequest;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.scheduling.request.earlylearning.ReScheduleDemoRequest;
import com.vedantu.scheduling.response.AvailabilitySlotStringRes;
import com.vedantu.scheduling.response.earlylearning.JoinDemoResponse;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.ParseException;
import java.util.Map;
import java.util.Objects;

public abstract class AbstractEarlyLearning {

	@Autowired
	protected VedantuWaveSessionManager vedantuWaveSessionManager;

	@Autowired
	protected LeadSquareUtilityManager leadSquareManager;

	@Autowired
	protected DozerBeanMapper mapper;

	@Autowired
	protected AwsSQSManager awsSQSManager;

	@Autowired
	protected AsyncTaskFactory asyncTaskFactory;

	@Autowired
	protected RedisDAO redisDAO;

	@Autowired
	protected OTFSessionManager otfSessionManager;

	@Autowired
	protected CalendarEntryDAO calendarEntryDAO;

	@Autowired
	protected HttpSessionUtils httpSessionUtils;

	@Autowired
	protected CalendarEntryManager calendarEntryManager;

	@Autowired
	protected OTFSessionDAO otfSessionDAO;

	@Autowired
	protected GTTAttendeeDetailsManager gttAttendeeDetailsManager;

	@Autowired
	protected ITeacherProficiencyManager teacherProficiencyManager;

	@Autowired
	@Qualifier("overBookingSlotAvailabilityManager")
	private AbstractSlotAvailabilityManager overbookingSlotAvailabilityManager;

	protected Gson gson = new Gson();

	@Autowired
	@Qualifier("otoSlotAvailabilityManager")
	private AbstractSlotAvailabilityManager otoSlotAvailabilityManager;

	protected Logger logger = LogFactory.getLogger(AbstractEarlyLearning.class);

	public abstract EarlyLearningSessionPojo hasEarlyLearningDemo(String studentId,
			SessionLabel earlyLearningCourse) throws ForbiddenException;

	public abstract AvailabilitySlotStringRes getCalendarAvailability(AvailabilitySlotStringReq req)
			throws BadRequestException, VException;

	public abstract EarlyLearningSessionPojo addDemo(AddDemoRequest request) throws VException;

	public abstract JoinDemoResponse joinDemo(JoinDemoRequest request, CourseProperties courseProperties)
			throws VException;

	public abstract EarlyLearningSessionPojo reScheduleDemo(ReScheduleDemoRequest request) throws VException;

	public abstract Map<AnalyticsType, Object> proficiencyAnalytics(ProficiencyAnalyticsRequest request) throws VException, ParseException;


	public String getProficientTeacher(Long startTime, Long endTime, Integer grade,
			SessionLabel earlyLearningCourseType) throws VException {

		if (endTime - startTime > CommonCalendarUtils.MILLIS_PER_DAY) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Constants.ERROR_AVAILABILITY_FETCH);
		}
		ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
				.getTeacherProficiencies(earlyLearningCourseType);
		if (Objects.isNull(proficiencyIds)) {
			return StringUtils.EMPTY;
		}
		return teacherProficiencyManager.assignTeacher(grade, startTime, endTime, proficiencyIds);
	}

	public ProficiencyType getElTeacherProficiencyType(Long startTime, Long endTime, Integer grade,
			SessionLabel earlyLearningCourseType) throws VException {

		if (endTime - startTime > CommonCalendarUtils.MILLIS_PER_DAY) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Constants.ERROR_AVAILABILITY_FETCH);
		}
		ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
				.getTeacherProficiencies(earlyLearningCourseType);
		if (Objects.isNull(proficiencyIds)) {
			return null;
		}
		return getSlotAvailabilityManager(earlyLearningCourseType).getAvailableProficiencyCategoryForDemo(startTime,
				endTime, proficiencyIds, grade, earlyLearningCourseType);
	}

	public static class Constants {
		public static final String ERROR_AVAILABILITY_FETCH = "Availability fetch for duration more than 1 day";
	}

	protected AbstractSlotAvailabilityManager getSlotAvailabilityManager(SessionLabel courseType) {
		if (CourseConfiguration.getCourseProperties(courseType).getOverBookingFactor() == 1.0) {
			return otoSlotAvailabilityManager;
		}
		return overbookingSlotAvailabilityManager;
	}
}
