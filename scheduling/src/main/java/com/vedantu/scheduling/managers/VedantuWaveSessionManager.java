/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.EventName;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.request.EndOTMSessionReq;
import com.vedantu.onetofew.request.OTFJoinSessionReq;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.DoubtNumbers;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.HotspotNumbers;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import com.vedantu.scheduling.dao.entity.PollNumbers;
import com.vedantu.scheduling.dao.entity.QuizNumbers;
import com.vedantu.scheduling.dao.serializers.GTTAttendeeDetailsDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.OTMSessionEngagementDataDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.enums.OTMSessionRole;
import com.vedantu.scheduling.enums.wavesession.InteractionType;
import com.vedantu.scheduling.pojo.AggregatedGTTAttendeeCountForSession;
import com.vedantu.scheduling.pojo.OTMSessionConfig;
import com.vedantu.scheduling.pojo.ReviseStatsPojo;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.wavesession.DoubtData;
import com.vedantu.scheduling.pojo.wavesession.InteractionData;
import com.vedantu.scheduling.request.UpdateOTFSessionReq;
import com.vedantu.scheduling.request.UpdateSessionConfigReq;
import com.vedantu.scheduling.request.wavesession.OTMSessionEngagementDataReq;
import com.vedantu.scheduling.request.wavesession.UpdateSessionAttendeeDataReq;
import com.vedantu.scheduling.response.ActiveSessionsTAResp;
import com.vedantu.scheduling.response.EnrollmentCountForUserBatchRes;
import com.vedantu.scheduling.response.JoinVedantuWaveSessionRes;
import com.vedantu.scheduling.utils.AwsEC2Manager;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.subscription.request.section.TeacherInSectionReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.vedantu.util.enums.SQSMessageType.GTT_UPDATE_SESSION_ATTENDEE_TASK;
import static com.vedantu.util.enums.SQSMessageType.SEND_OTF_SESSION_TO_NODE_FOR_CACHING;
import static com.vedantu.util.enums.SQSQueue.OTF_POSTSESSION_QUEUE;

/**
 * @author ajith
 */
@Service
public class VedantuWaveSessionManager {

    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private OTFSessionDAO otfSessionDao;

    @Autowired
    private OTMSessionEngagementDataDAO oTMSessionEngagementDataDAO;

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private GTTAttendeeDetailsManager gTTAttendeeDetailsManager;

    @Autowired
    private VedantuWaveScheduleManager vedantuWaveScheduleManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VedantuWaveSessionManager.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsEC2Manager awsEC2Manager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private static Gson gson = new Gson();

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private RedisDAO redisDAO;

    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private final String OTMNODE_CACHE_COLLECTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("otmnode.cache.collection.endpoint");
    private final String OTMNODE_CACHE_GTT_ATTENDEE_COLLECTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("otmnode.cache.gtt.attendee.collection.endpoint");

    private final static Long OTM_RECORDER_CRON_TIME = 5 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long CACHE_SCHEDULING_COLLECTIONS_CRON_TIME_45 = 45 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long CACHE_SCHEDULING_COLLECTIONS_CRON_TIME_31 = 31 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long CACHE_SCHEDULING_COLLECTIONS_CRON_WINDOW = 3 * Long.valueOf(DateTimeUtils.MILLIS_PER_HOUR);
    private final static Long CACHE_GTT_ATTENDEE_COLLECTIONS_CRON_TIME = 30 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long CACHE_GTT_ATTENDEE_COLLECTIONS_CRON_WINDOW = 2 * Long.valueOf(DateTimeUtils.MILLIS_PER_HOUR);

    private final static Long OTM_RECORDING_EXTRA_BUFFER_AFTER_END_TIME = (long) DateTimeUtils.MILLIS_PER_HOUR;

    //7min-12min
    private final static Long DID_NOT_RECORD_FOR_MIN_LIMIT = OTM_RECORDER_CRON_TIME + 2 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long DID_NOT_RECORD_FOR_MAX_LIMIT = DID_NOT_RECORD_FOR_MIN_LIMIT + OTM_RECORDER_CRON_TIME;
    private final static Long DID_NOT_JOIN_FOR_MIN_LIMIT = 10 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long DID_NOT_JOIN_FOR_MAX_LIMIT = DID_NOT_JOIN_FOR_MIN_LIMIT + 5 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long BUFFER_TIME_TO_END_ACTIVE_SESSIONS_MILLIS = ConfigUtils.INSTANCE.getLongValue("buffer.time.to.end.active.sessions.seconds") * 1000;
    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final String SESSION_RECORDER_ENV = ConfigUtils.INSTANCE.getStringValue("environment");
    private final String NODE_SERVER_URL_WEBINAR = ConfigUtils.INSTANCE.getStringValue("NODE_SERVER_URL_WEBINAR");

    public JoinVedantuWaveSessionRes joinVedantuWaveSession(OTFJoinSessionReq req, Long userId, Role role)
            throws VException {
        String sessionId = req.getSessionId();
        OTFSession session = otfSessionDao.getById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }

        if (!session.hasVedantuWaveFeatures()) {
            throw new NotFoundException(ErrorCode.NON_VEDANTU_WAVE_SESSION, "not a vedantu wave session" + sessionId);
        }

        OTMSessionConfig config = session.getOtmSessionConfig();
        if (config == null) {
            config = new OTMSessionConfig();
        }

        boolean isTwoTeacherModelSession = session.getType() == EntityType.TWO_TEACHER_MODEL;
        boolean isTwoTeacherModelTA = false;

        switch (role) {
            case TEACHER:
                if(isTwoTeacherModelSession){
                    // if teacher is neither MT nor CT, throw exception
                    if(!(userId.toString().equals(session.getPresenter())) && !isTAPresentInSession(session,userId)) {
                        throw new ForbiddenException(ErrorCode.SESSION_FORBIDDEN_FOR_USER,
                                "Action not allowed for the user : " + userId + " for sessionId: " + session.getId());
                    }
                    if(!(userId.toString().equals(session.getPresenter())))
                        isTwoTeacherModelTA = true;
                }
                else{
                    if (!(userId.toString().equals(session.getPresenter())
                            || (session.getTaIds() != null && session.getTaIds().contains(userId)))) {
                        throw new ForbiddenException(ErrorCode.SESSION_FORBIDDEN_FOR_USER,
                                "Action not allowed for the user : " + userId + " for sessionId: " + session.getId());
                    }
                }
                break;
            case STUDENT:
                if (!session.isWebinarSession()) {
                    if (session.getBatchIds().isEmpty()) {
                        throw new ForbiddenException(ErrorCode.SESSION_FORBIDDEN_FOR_USER,
                                "Action not allowed for the user : " + userId + " for sessionId: " + session.getId());
                    }
                    ClientResponse resp2 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                                    + "enroll/getActiveEnrollmentCountForUserInBatchIds?userId=" + userId + "&batchIds=" + String.join(",", session.getBatchIds()),
                            HttpMethod.GET, null);
//                    VExceptionFactory.INSTANCE.parseAndThrowException(resp2);
                    String jsonString2 = resp2.getEntity(String.class);
                    logger.info("Join-Vedantu-Wave-Session: " + jsonString2);
                    EnrollmentCountForUserBatchRes resp = gson.fromJson(jsonString2, EnrollmentCountForUserBatchRes.class);
                    logger.info(resp);
                    if (resp == null || resp.getCount() <= 0) {
                        throw new ForbiddenException(ErrorCode.SESSION_FORBIDDEN_FOR_USER,
                                "Action not allowed for the user : " + userId + " for sessionId: " + session.getId());
                    }
                }
                //TODO check if the user registered for webinar
                break;
            case ADMIN:
            case STUDENT_CARE:
                break;
            default:
                throw new ForbiddenException(ErrorCode.SESSION_FORBIDDEN_FOR_USER,
                        "Action not allowed for the user : " + userId + " for sessionId: " + session.getId());
        }

        OTMSessionRole sessionRole = OTMSessionRole.STUDENT;
        if (userId.toString().equals(session.getPresenter())) {
            sessionRole = OTMSessionRole.TEACHER;
        } else if ((session.getTaIds() != null && session.getTaIds().contains(userId)) || isTwoTeacherModelTA) {
            sessionRole = OTMSessionRole.TA;
        }

        int prejoinStudentWebinarMins = ConfigUtils.INSTANCE.getIntValue("otm.session.pre.join.time.student.webinar.min", 0);
        Long allowedWebinarStudentTime = session.getStartTime() - (prejoinStudentWebinarMins * DateTimeUtils.MILLIS_PER_MINUTE);

        if (SessionState.SCHEDULED.equals(session.getState())) {
            if (null == session.getProgressState()) {
                int prejoinMins = ConfigUtils.INSTANCE.getIntValue("otm.session.pre.join.time.min", 0);
                Long allowedJoinTime = session.getStartTime() - (7 * prejoinMins * DateTimeUtils.MILLIS_PER_MINUTE);
                logger.info("allowedJoinTime " + allowedJoinTime);

                if (userId.toString().equals(session.getPresenter()) || Role.ADMIN.equals(role) || Role.STUDENT_CARE.equals(role) || (isTwoTeacherModelSession && isTwoTeacherModelTA)) {
                    if (System.currentTimeMillis() < allowedJoinTime) {
                        throw new ForbiddenException(ErrorCode.SESSION_NOT_ACTIVE, "session not allowed to join before " + prejoinMins + " mins");
                    }
                } else {
                    if (System.currentTimeMillis() < session.getStartTime() && !session.isWebinarSession()) {
                        throw new ForbiddenException(ErrorCode.SESSION_NOT_ACTIVE, "session not allowed to join before start time");
                    }
                    if (System.currentTimeMillis() < allowedWebinarStudentTime && session.isWebinarSession()) {
                        throw new ForbiddenException(ErrorCode.SESSION_NOT_ACTIVE, "session not allowed to join 15 mins before webinar start time");
                    }
                }
                if (!userId.toString().equals(session.getPresenter())) {
                    if (System.currentTimeMillis() > session.getEndTime()) {
                        throw new ForbiddenException(ErrorCode.SESSION_ALREADY_ENDED, "it is past end time and teacher did not join, so no entry");
                    }
                }
                // session marked active scenarios :
                //    1. presenter attempts to join the session before session end time
                //    2. Only applicable for TWO_TEACHER_MODEL -- when CT attempts to join the session
                if ((userId.toString().equals(session.getPresenter()) || (isTwoTeacherModelSession && isTwoTeacherModelTA)) && System.currentTimeMillis() < session.getEndTime()) {
                    logger.info("marking the session active");
                    session.setProgressState(OTMSessionInProgressState.ACTIVE);
                    session.setStartedAt(System.currentTimeMillis());
                    session.setSessionStartedBy(userId);
                    otfSessionDao.save(session);
                }
            } else {
                switch (session.getProgressState()) {
                    case ENDED:
                        if (!userId.toString().equals(session.getPresenter())) {
                            throw new ForbiddenException(ErrorCode.SESSION_ALREADY_ENDED, "session already ended");
                        }
                    case ACTIVE:
                        if (Role.STUDENT.equals(role) && System.currentTimeMillis() < session.getStartTime() && !session.isWebinarSession()) {
                            throw new ForbiddenException(ErrorCode.SESSION_NOT_ACTIVE, "session not allowed to join before start time even if active");
                        }
                        if (Role.STUDENT.equals(role) && System.currentTimeMillis() < allowedWebinarStudentTime && session.isWebinarSession()) {
                            throw new ForbiddenException(ErrorCode.SESSION_NOT_ACTIVE, "session not allowed to join 15 mins before webinar start time");
                        }
                        break;
                    default:
                        break;
                }
            }
        } else {
            throw new ForbiddenException(ErrorCode.valueOf("SESSION_" + session.getState().name()), "session " + session.getState());
        }

        JoinVedantuWaveSessionRes res = new JoinVedantuWaveSessionRes();
        res.setOtmSessionConfig(config);
        res.setSessionId(sessionId);
        res.setTitle(session.getTitle());
        res.setPagesMetadata(session.getPagesMetaData());
        res.setNodeServerPort("443");
        if (session.isWebinarSession()) {
            res.setWebinarId(session.getWebinarId());
            res.setNodeServerUrl(ConfigUtils.INSTANCE.getStringValue("NODE_SERVER_URL_WEBINAR"));
        } else {
            res.setNodeServerUrl(ConfigUtils.INSTANCE.getStringValue("NODE_SERVER_URL"));
        }
        res.setServiceProvider(session.getServiceProvider());
        res.setSessionUrl(session.getSessionURL());
        res.setMeetingId(session.getMeetingId());
        res.setTeacherId(session.getPresenter());
        res.setTaIds(session.getTaIds());
        res.setSessionRole(sessionRole);
        res.setStartTime(session.getStartTime());
        res.setEndTime(session.getEndTime());
        res.setSessionToolType(session.getSessionToolType());
        res.setCanvasStreamingType(session.getCanvasStreamingType());
        res.setProgressState(session.getProgressState());
        res.setServerTime(System.currentTimeMillis());
        res.setType(session.getType());
        res.setLabels(session.getLabels());
        res.setEntityTags(session.getEntityTags());
        if (Role.TEACHER.equals(role)) {
            res.setAgoraStartTime(session.getAgoraStartTime());
            res.setParentSessionId(session.getParentSessionId());
            res.setAgoraReplayUrl(session.getAgoraReplayUrl());
        }
        res.setOtmSessionType(session.getOtmSessionType());

        if (session.isOtoSession()) {
            res.setAttendees(session.getAttendees());
        }

        if (session.getSessionContextType() != null) {
            res.setSessionContextType(session.getSessionContextType().toString());
        }

        if (Role.TEACHER.equals(role) || Role.STUDENT.equals(role)) {//handles teacher/student/TA roles
            vedantuWaveScheduleManager.userJoined(session, userId.toString());
        }

        res.setIsBigWhiteBoard(session.isBigWhiteBoard());

        Map<String, Object> payload = new HashMap<>();
        payload.put("sessionId", session.getId());
        payload.put("userId", userId.toString());
        payload.put("ipAddress", req.getIpAddress());
        payload.put("userAgent", req.getUserAgent());

        // AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ADD_JOIN_DETAILS, payload);
        // asyncTaskFactory.executeTask(params);
        String msgGroupId = userId.toString();
        awsSQSManager.sendToSQS(SQSQueue.GTT_UPDATE_OPS_FIFO, SQSMessageType.ADD_JOIN_DETAILS, gson.toJson(payload), msgGroupId);

        if (session.isWebinarSession()) {
            vedantuWaveScheduleManager.registerAttendee(session, userId.toString(), null);
            res.setNodeServerUrl(ConfigUtils.INSTANCE.getStringValue("NODE_SERVER_URL_WEBINAR"));
        }

        return res;
    }

    private boolean isTAPresentInSession(OTFSession session, Long userId) throws VException {
        TeacherInSectionReq req = new TeacherInSectionReq();
        req.setBatchIds(new ArrayList<>(session.getBatchIds()));
        req.setUserId(userId.toString());

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                        + "/section/isTeacherPartOfSection?"
                        + WebUtils.INSTANCE.createQueryStringOfObject(req),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = gson.fromJson(jsonString,PlatformBasicResponse.class);
        logger.info("isSuccess {}",platformBasicResponse.isSuccess());
        return platformBasicResponse.isSuccess();
    }

    public PlatformBasicResponse endVedantuWaveSession(EndOTMSessionReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException, Exception {
        String sessionId = req.getSessionId();
        OTFSession session = otfSessionDao.getById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }

        if (!OTMSessionInProgressState.ACTIVE.equals(session.getProgressState())) {
            throw new ConflictException(ErrorCode.SESSION_ALREADY_ENDED, "session already ended");
        }

        session.setProgressState(OTMSessionInProgressState.ENDED);
        session.setEndedBy(req.getUserId());
        session.setEndedAt(System.currentTimeMillis());
        otfSessionDao.save(session);

        // Only sessionId is required to call postSessionApis
        JsonObject json = new JsonObject();
        json.addProperty("sessionId", sessionId);
        awsSQSManager.sendToSQS(OTF_POSTSESSION_QUEUE, SQSMessageType.PREPARE_WAVE_SESSION_DATA_INSIGHTS, json.toString());
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse triggerSQSForDataInsights(String sessionId) throws VException {
        OTFSession session = otfSessionDao.getById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }

        JsonObject json = new JsonObject();
        json.addProperty("sessionId", sessionId);
        awsSQSManager.sendToSQS(OTF_POSTSESSION_QUEUE, SQSMessageType.PREPARE_WAVE_SESSION_DATA_INSIGHTS, json.toString());
        return new PlatformBasicResponse();
    }

    public void endActiveSessionsAsync() {
        logger.info("Invoking endActiveSessionsAsync");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.END_OTM_VEDANTU_WAVE_ACTIVE_SESSIONS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void endActiveSessions() {
        logger.info("endActiveSessions");
        Long currentTime = System.currentTimeMillis();
        List<OTFSession> sessions = otfSessionDao.getOTFSessions(null, null, SessionState.SCHEDULED, OTMSessionInProgressState.ACTIVE, 0, 200);
        logger.info("endActiveSessions got sessions: " + sessions.size());
        if (sessions.size() >= 200) {
            logger.error("more than 200 parallel session running. Logic has to be changed for end active sessions cron");
        }
        for (OTFSession session : sessions) {
            if (session.hasVedantuWaveFeatures()) {
                logger.info("checking " + session.getId());
                Long diff = currentTime - session.getEndTime();
                if (diff >= BUFFER_TIME_TO_END_ACTIVE_SESSIONS_MILLIS) {
                    logger.info("ending the session " + session.getId() + ", end time " + session.getEndTime());
                    try {
                        EndOTMSessionReq req = new EndOTMSessionReq();
                        req.setSessionId(session.getId());
                        endVedantuWaveSession(req);
                    } catch (Exception e) {
                        logger.error("error in ending otm session " + session.getId() + " , error" + e.getMessage());
                    }
                }
            }
        }
    }

    public void sendLateJoinSMSAsync() {
        logger.info("Invoking sendLateJoinSMS");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_VEDANTU_WAVE_SESSION_LATE_JOIN_SMS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendLateJoinSMS() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("sendLateJoinSMS at " + currentTime);
        List<OTFSession> sessions = otfSessionDao.getOTFSessions(null, null, SessionState.SCHEDULED, OTMSessionInProgressState.ACTIVE, 0, 200);
        logger.info("sendLateJoinSMS got sessions: " + sessions.size());
        if (sessions.size() >= 200) {
            logger.error("more than 200 parallel session running. Logic has to be changed for sms check");
        }
        for (OTFSession session : sessions) {
            try {
                if (session.hasVedantuWaveFeatures()) {
                    //since the cron in 5min interval will check for not attended happening for more then 10 min and less than 15 min,
                    //max check of 15 min is put to generate only 1 sms
                    Long diff = currentTime - session.getStartTime();
                    if (diff >= DID_NOT_JOIN_FOR_MIN_LIMIT && diff < DID_NOT_JOIN_FOR_MAX_LIMIT) {
                        logger.info("checking if the session " + session.getId() + " has any not joined guys for more than 10mins");
                        List<GTTAttendeeDetails> attendees = gTTAttendeeDetailsManager.getAbsentAttendees(session);
                        if (ArrayUtils.isNotEmpty(sessions)) {
                            logger.info("sending sms to " + attendees.size());
                            for (GTTAttendeeDetails attendeeDetails : attendees) {
                                communicationManager.sendOTMLateJoinCommunication(session, attendeeDetails);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Error in sending late join sms " + e.getMessage());
            }
        }
    }

    public PlatformBasicResponse updateSessionConfig(UpdateSessionConfigReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException {
        OTFSession oTFSession = otfSessionDao.getById(req.getSessionId());
        if (oTFSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + req.getSessionId());
        }
        OTMSessionConfig config = oTFSession.getOtmSessionConfig();
        if (config == null) {
            config = new OTMSessionConfig();
            oTFSession.setOtmSessionConfig(config);
        }
        if (req.getChatEnabled() != null) {
            config.setChatEnabled(req.getChatEnabled());
        }
        if (req.getChatMode() != null) {
            config.setChatMode(req.getChatMode());
        }
        if (req.getHandRaiseMode() != null) {
            config.setHandRaiseMode(req.getHandRaiseMode());
        }
        if (req.getShowEmojis() != null) {
            config.setShowEmojis(req.getShowEmojis());
        }
        if (req.getWelcomeMessage() != null) {
            config.setWelcomeMessage(req.getWelcomeMessage());
        }
        if (req.getDoubtStream() != null) {
            config.setDoubtStream(req.getDoubtStream());
        }
        if (req.getUpvote() != null) {
            config.setUpvote(req.getUpvote());
        }
        if (req.getShowStudentsListTeacher() != null) {
            config.setShowStudentsListTeacher(req.getShowStudentsListTeacher());
        }
        if (req.getShowStudentsListTA() != null) {
            config.setShowStudentsListTA(req.getShowStudentsListTA());
        }
        if (ArrayUtils.isNotEmpty(req.getStudentListTabsTeacher())) {
            config.setStudentListTabsTeacher(req.getStudentListTabsTeacher());
        }
        if (ArrayUtils.isNotEmpty(req.getStudentListTabsTA())) {
            config.setStudentListTabsTA(req.getStudentListTabsTA());
        }
        if (req.getLeaderBoardStreak() != null) {
            config.setLeaderBoardStreak(req.getLeaderBoardStreak());
        }
        if (req.getQuizStreak() != null) {
            config.setQuizStreak(req.getQuizStreak());
        }
        if (req.getSectionPhase3() != null) {
            config.setSectionPhase3(req.getSectionPhase3());
        }
        logger.info(oTFSession.getOtmSessionConfig());
        otfSessionDao.save(oTFSession);
        return new PlatformBasicResponse();
    }

    public OTMSessionConfig getSessionConfig(String sessionId)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException {
        OTFSession oTFSession = otfSessionDao.getById(sessionId);
        if (oTFSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }
        return oTFSession.getOtmSessionConfig();
    }

    public PlatformBasicResponse savePagesMetadata(List<SessionPagesMetadata> pages, String sessionId)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException {
        OTFSession oTFSession = otfSessionDao.getById(sessionId);
        if (oTFSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }
        oTFSession.setPagesMetaData(pages);
        otfSessionDao.save(oTFSession);
        return new PlatformBasicResponse();
    }

    public void createCurrentDaySocialVidConferences() {
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_CURRENT_DAY_SOCIALVID_CONFERENCES, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    // TODO only querying and looping.  what is the purpose?
    public void createCurrentDaySocialVidConferencesTask() {
        Long startTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis());

        logger.info("createCurrentDaySocialVidConferencesTask : " + startTime);
        Long endTime = startTime + DateTimeUtils.MILLIS_PER_DAY + DateTimeUtils.MILLIS_PER_HOUR; //taking extra hr

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime).lt(endTime));

        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).exists(true).ne(null));
        logger.info("session query : " + query);
        List<OTFSession> sessions = otfSessionDao.runQuery(query, OTFSession.class);
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSession session : sessions) {
                if (!(session.hasVedantuWaveFeatures()
                        || session.getOtmSessionConfig() == null)) {
                    continue;
                }

            }
        }

    }

    public PlatformBasicResponse launchOTMSessionRecorder(String sessionId, Long timeout) {
        awsEC2Manager.createOTMSessionRecorder(sessionId, timeout);
        return new PlatformBasicResponse();
    }

    public void launchOTMSessionRecordersAsync() {
        logger.info("Invoking SESSION Recorders async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LAUNCH_OTM_SESSION_RECORDERS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void checkOTMSessionRecordingsAsync() {
        logger.info("Invoking checkOTMSessionRecordingsAsync");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CHECK_OTM_SESSION_RECORDERINGS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void checkOTMSessionRecordings() {
        logger.info("checkOTMSessionRecordings");

        Long currentTime = System.currentTimeMillis();
        List<OTFSession> sessions = otfSessionDao.getOTFSessions(null, null, SessionState.SCHEDULED, OTMSessionInProgressState.ACTIVE, 0, 200);
        logger.info("checkOTMSessionRecordings got sessions: " + sessions.size());
        if (sessions.size() >= 200) {
            logger.error("more than 200 parallel session running. Logic has to be changed for recordings check");
        }
        for (OTFSession session : sessions) {
            if (session.hasVedantuWaveFeatures()) {
                logger.info("recorded till  for session " + session.getId() + " : " + session.getRecordedTill());
                //since the cron in 5min interval will check for no recording happening for more then 7 min and less than 12 min,
                //max check of 12 min is put to generate only 1 sentry error/alert
                Long recordedTill = session.getRecordedTill();
                if (recordedTill == null) {
                    recordedTill = session.getStartTime();
                }
                Long diff = currentTime - recordedTill;
                if (diff >= DID_NOT_RECORD_FOR_MIN_LIMIT && diff <= DID_NOT_RECORD_FOR_MAX_LIMIT) {
                    logger.error("session recording for " + session.getId() + ", stopped at " + recordedTill + ", please look into it");
                }
            }
        }
    }

    public void launchOTMSessionRecorders() throws Exception {
        logger.info("Launching SESSION Recorders");
        Long currentTime = System.currentTimeMillis();
        logger.info("launchOTMSessionRecorders starting at time : " + currentTime);
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long afterStartTime = currentTime + OTM_RECORDER_CRON_TIME;
        Long beforeStartTime = afterStartTime + OTM_RECORDER_CRON_TIME - 1;
        logger.info("launchOTMSessionRecorders beforeStartTime " + beforeStartTime + " afterStartTime " + afterStartTime);

        List<OTFSession> sessions = otfSessionDao.getOTFSessions(afterStartTime, beforeStartTime, SessionState.SCHEDULED, null, 0, 200);
        logger.info("launchOTMSessionRecorders got sessions: " + sessions.size());
        if (sessions.size() >= 100) {
            logger.error("more than 100 parallel session running. Logic has to be changed before recordings stop");
        }
        for (OTFSession session : sessions) {
            try {
                if (session.hasVedantuWaveFeatures()) {
                    // info: migrated to otmnode
                    launchOTMSessionRecorder(session);
                }
            } catch (Exception ex) {
                logger.error("Launching Recorder got error" + ex);
            }

        }
    }

    public void launchOTMSessionRecorder(OTFSession session) throws Exception {
        Long timeout = session.getEndTime() - session.getStartTime();
        Long currentTime = System.currentTimeMillis();
        timeout += session.getStartTime() - currentTime;
        timeout += OTM_RECORDING_EXTRA_BUFFER_AFTER_END_TIME;//1hr
        timeout /= 1000;
        logger.info("Launching SESSION Recorder for timeOut:" + timeout + " session:" + session);
        awsEC2Manager.createOTMSessionRecorder(session.getId(), timeout);
    }

    public void updateSessionAttendeeData(UpdateSessionAttendeeDataReq req) throws VException {
        String sessionId = req.getSessionId();
        OTFSession session = otfSessionDao.getById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }

        logger.info("sending to queue");
        SQSMessageType task = GTT_UPDATE_SESSION_ATTENDEE_TASK;
        final String groupId = req.getSessionId() + req.getUserId();
        awsSQSManager.sendToSQS(task.getQueue(), task, gson.toJson(req), groupId);
    }

    public void updateSessionAttendeeDataFromQueue(UpdateSessionAttendeeDataReq req) throws VException {
        logger.info("recieved message from queue for update attendee {}", req);
        String sessionId = req.getSessionId();
        OTFSession session = otfSessionDao.getById(sessionId);
        String userId = req.getUserId();
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.getAttendeeDetails(userId, sessionId, EntityState.ACTIVE);
        GTTAttendeeDetails details = null;
        if (ArrayUtils.isEmpty(attendeeDetails)) {
            if (!session.isSalesDemoSession()) {
                //will be hackish for now, and removed later
                boolean notFound = true;
                List<GTTAttendeeDetails> deteledAttendeeDetails = gttAttendeeDetailsDAO.getAttendeeDetails(userId, sessionId, EntityState.DELETED);
                if (ArrayUtils.isNotEmpty(deteledAttendeeDetails)) {
                    //if the deenrollment happened after the session start time, we will store the data
                    if (deteledAttendeeDetails.get(0).getSessionStartTime() != null
                            && deteledAttendeeDetails.get(0).getLastUpdated() > deteledAttendeeDetails.get(0).getSessionStartTime()) {
                        details = deteledAttendeeDetails.get(0);
                        notFound = false;
                    }
                }
                if (notFound) {
                    Set<String> batchIds = Optional.ofNullable(session.getBatchIds()).orElseGet(HashSet::new);
                    String url = ConfigUtils.INSTANCE.getSubscriptionEndpoint() + "/enroll/getEnrollmentForBatches?userId=" + req.getUserId()
                            + "&batchIds=" + String.join(",", batchIds);
                    ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(response);
                    String entity = response.getEntity(String.class);
                    List<EnrollmentPojo> pojos = gson.fromJson(entity, new TypeToken<List<EnrollmentPojo>>() {
                    }.getType());
                    EnrollmentPojo enrollmentPojo = Optional.ofNullable(pojos).orElseGet(ArrayList::new).stream()
                            .min(Comparator.comparing(EnrollmentPojo::getCreationTime)).orElse(null);
                    details = new GTTAttendeeDetails(session.getId(), userId, pojos);
                    details.setEnrollmentId(enrollmentPojo == null ? null : enrollmentPojo.getId());
                    details.setSessionEndTime(session.getEndTime());
                    details.setSessionStartTime(session.getStartTime());
                    details.setTraningId(session.getMeetingId());
                    AttendeeContext attendeeContext = AttendeeContext.LATE_JOIN;
                    if (enrollmentPojo != null && enrollmentPojo.getCreationTime() != null && session.getStartTime() > enrollmentPojo.getCreationTime()) {
                        if (session.getStartTime() < System.currentTimeMillis()) {
                            attendeeContext = AttendeeContext.STATUS_CHANGE;
                        } else {
                            attendeeContext = null;
                        }
                    }
                    details.setAttendeeType(attendeeContext);
                }
            }
        } else {
            if (attendeeDetails.size() > 1) {
                logger.warn("multiple attendee found to update for "
                        + sessionId + ", " + userId);
            }
            details = attendeeDetails.get(0);
        }

        if (details != null) {
            details.setStudentSessionJoinTime(req.getStudentSessionJoinTime());
            details.setStudentLastDisconnectTime(req.getStudentLastDisconnectTime());
            details.setTotalDisconnections(req.getTotalDisconnections());
            details.setStudentChatCount(req.getStudentChatCount());
            details.setThumbsDownCount(req.getThumbsDownCount());
            details.setThumbsUpCount(req.getThumbsUpCount());
            details.setAverageResponseTime(req.getAverageResponseTime());
            details.setConnectTimes(req.getConnectTimes());
            details.setDisconnectTimes(req.getDisconnectTimes());
            details.setInteractionDatas(req.getInteractionDatas());

            int quizAsked = session.getQuizCount(), quizAttempted = 0, quizCorrect = 0, hotspotAsked = session.getHotspotCount(),
                    hotspotAttempted = 0, hotspotCorrect = 0, pollAsked = session.getPollCount(),
                    pollYes = 0, pollNo = 0, doubtAsked = 0, doubtResolved = 0;

            if (ArrayUtils.isNotEmpty(req.getInteractionDatas())) {
                for (InteractionData data : req.getInteractionDatas()) {
                    logger.info("anaylysing data " + data);
                    if (null != data.getInteractionType()) {
                        switch (data.getInteractionType()) {
                            case QUIZ:
                                if (StringUtils.isNotEmpty(data.getSelected())) {
                                    quizAttempted++;
                                    if (data.isIsCorrect()) {
                                        quizCorrect++;
                                    }
                                }
                                break;
                            case HOTSPOT:
                                if (StringUtils.isNotEmpty(data.getSelected())) {
                                    hotspotAttempted++;
                                    if (data.isIsCorrect()) {
                                        hotspotCorrect++;
                                    }
                                }
                                break;
                            case POLL:
                                if ("YES".equalsIgnoreCase(data.getSelected())) {
                                    pollYes++;
                                } else if ("NO".equalsIgnoreCase(data.getSelected())) {
                                    pollNo++;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            if (ArrayUtils.isNotEmpty(req.getDoubtDatas())) {
                for (DoubtData data : req.getDoubtDatas()) {
                    if ("RESOLVED".equalsIgnoreCase(data.getStatus())) {
                        doubtResolved++;
                    }
                }
                doubtAsked = req.getDoubtDatas().size();
            }

            PollNumbers pollNumber = new PollNumbers(pollAsked, pollYes, pollNo);
            DoubtNumbers doubtNumbers = new DoubtNumbers(doubtAsked, doubtResolved);
            QuizNumbers quizNumbers = new QuizNumbers(quizAsked, quizAttempted, quizCorrect);
            HotspotNumbers hotspotNumbers = new HotspotNumbers(hotspotAsked, hotspotAttempted, hotspotCorrect);
            details.setPollNumbers(pollNumber);
            details.setDoubtNumbers(doubtNumbers);
            details.setQuizNumbers(quizNumbers);
            details.setHotspotNumbers(hotspotNumbers);
            details.setDoubtDatas(req.getDoubtDatas());
            details.setLbRank(req.getLbRank());
            details.setLbPercentile(req.getLbPercentile());
            details.setLbPoints(req.getLbPoints());
            details.setStudentOnlineDuration(req.getStudentOnlineDuration());
            if (req.getStudentOnlineDuration() != null) {
                details.setTimeInSession((long) req.getStudentOnlineDuration());
            }
            gTTAttendeeDetailsManager.queueAttendee(details);
            try {
                //sending session update to vglance queue..
                if (!session.isSalesDemoSession() &&
                        !EntityType.BIG_SESSION.equals(session.getType()) &&
                        details.getTimeInSession() != 0 &&
                        !AttendeeContext.LATE_JOIN.equals(details.getAttendeeType()) && SessionState.SCHEDULED.equals(session.getState()) &&
                        (session.getBoardId() != null || session.isWebinarSession())) {
                    awsSQSManager.sendToSQS(SQSQueue.VGLANCE_SESSION_UPDATE, SQSMessageType.VGLANCE_SESSION_UPDATE, new Gson().toJson(details), SQSMessageType.VGLANCE_SESSION_UPDATE.name());
                }
            } catch (Exception e) {
                logger.error("Error while sending session data to vglance : ", e);
            }

            //tigger sns with webinar details, forward to leadsquared
            if (ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD")) {

                String[] batchIdsForHomeDemo = ConfigUtils.INSTANCE.getStringValue("batchids.push.to.leadsquared.homedemo").split(",");

                Set<String> batchIdsForHomeDemoFromSession = session.getBatchIds();

                if (ArrayUtils.isNotEmpty(batchIdsForHomeDemoFromSession)) {
                    for (String bId : batchIdsForHomeDemo) {
                        if (batchIdsForHomeDemoFromSession.contains(bId)) {
                            Map<String, String> payload = new HashMap<>();
                            payload.put("userId", userId);
                            payload.put("sessionId", sessionId);
                            payload.put("webinarId", session.getWebinarId());
                            payload.put("timeInSession", details.getTimeInSession().toString());
                            payload.put("sessionStartTime", details.getSessionStartTime().toString());
                            payload.put("sessionEndTime", details.getSessionEndTime().toString());
                            awsSNSManager.triggerSNS(SNSTopic.WEBINAR_HOMEDEMO_LEADSQUARED, SNSTopic.WEBINAR_HOMEDEMO_LEADSQUARED.name(), new Gson().toJson(payload));
                            break;
                        }
                    }
                }

                // trigger sqs for update sales demo to leadsquared
                // check if the session is a demo session
                if (session.isSalesDemoSession()) {
                    Map<String, String> payload = new HashMap<>();
                    payload.put("userId", details.getUserId());
                    Long sessionDurationMins = (session.getEndTime() - session.getStartTime()) / (1000 * 60);
                    payload.put("sessionDuration", String.valueOf(sessionDurationMins));
                    Integer studentOnlineMins = (null != details.getStudentOnlineDuration()) ? details.getStudentOnlineDuration() / (1000 * 60) : 0;
                    payload.put("sessionOnlineDuration", String.valueOf(studentOnlineMins));
                    Long timeInSessionMins = (null != details.getTimeInSession()) ? details.getTimeInSession() / (1000 * 60) : 0;
                    payload.put("timeInSession", String.valueOf(timeInSessionMins));
                    int quizRatio = 0;
                    if (null != details.getQuizNumbers() && details.getQuizNumbers().getNumberAsked() != 0) {
                        quizRatio = (details.getQuizNumbers().getNumberAttempted() * 100) / details.getQuizNumbers().getNumberAsked();
                    }
                    payload.put("quizAttemptedRatio", String.valueOf(quizRatio));
                    if (null != details.getQuizNumbers()) {
                        payload.put("quizNumbersCorrect", String.valueOf(details.getQuizNumbers().getNumberCorrect()));
                    } else {
                        payload.put("quizNumbersCorrect", "0");
                    }

                    int hotspotRatio = 0;
                    if (details.getHotspotNumbers().getNumberAsked() != 0) {
                        hotspotRatio = (details.getHotspotNumbers().getNumberAttempted() * 100) / details.getHotspotNumbers().getNumberAsked();
                    }
                    payload.put("hotSpotAttemptedRatio", String.valueOf(hotspotRatio));
                    payload.put("sessionId", session.getId());
                    payload.put("sessionStartTime", String.valueOf(session.getStartTime()));
                    payload.put("teacherJoinTime", String.valueOf(session.getTeacherJoinTime()));
                    payload.put("studentJoinTime", String.valueOf(details.getStudentSessionJoinTime()));
                    payload.put("teacherId", session.getPresenter());
                    if (CollectionUtils.isNotEmpty(session.getTaIds())) {
                        List<String> newList = new ArrayList<String>(session.getTaIds().size());
                        for (Long taId : session.getTaIds()) {
                            newList.add(String.valueOf(taId));
                        }
                        String delim = ",";
                        String res = String.join(delim, newList);
                        payload.put("taIds", res);
                    }
                    awsSNSManager.triggerSNS(SNSTopic.SALES_DEMO_LEADSQUARED, SNSTopic.SALES_DEMO_LEADSQUARED.name(), new Gson().toJson(payload));
                }
            }

            // revise Jee trigger sns


            ReviseStatsPojo pojo = new ReviseStatsPojo();
            pojo.setUserId(details.getUserId());
            pojo.setType("POST_SESSION");
            pojo.setSessionId(details.getSessionId());
            pojo.setEventName(EventName.TARGET_JEE_NEET);
            awsSNSManager.triggerSNS(SNSTopic.TARGET_JEE_NEET_POSTTEST, SNSSubject.UPDATE_JEE_STATS_POST_SESSION.name(), new Gson().toJson(pojo));

        }
    }

    public void setStudentEngagementData(OTMSessionEngagementDataReq req) throws VException {
        if (StringUtils.isEmpty(req.getSessionId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "sessionId missing");
        }
        OTFSession session = otfSessionDao.getById(req.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + req.getSessionId());
        }

        if (ArrayUtils.isEmpty(req.getList())) {
            return;
        }

        logger.info("processing for " + req.getList().size());
        //post session op so no problem with thread block for 10secs or so
        for (OTMSessionEngagementData data : req.getList()) {
            logger.info("adding for data " + data);
            if (StringUtils.isEmpty(data.getSlideId()) || (StringUtils.isEmpty(data.getContextId())
                    && !InteractionType.THUMBS_DOWN.equals(data.getContext())
                    && !InteractionType.THUMBS_UP.equals(data.getContext()))) {
                logger.error("invalid data for OTMSessionEngagementDataReq " + data);
                continue;
            }
            data.setSessionId(req.getSessionId());
            OTMSessionEngagementData prevData = oTMSessionEngagementDataDAO.getPreviousSavedEngagementData(req.getSessionId(),
                    data.getSlideId(), data.getContextId(), data.getContext());
            if (prevData != null) {
                data.setId(prevData.getId());
            }
            oTMSessionEngagementDataDAO.save(data);
        }
    }

    public List<ActiveSessionsTAResp> getActiveSessionsTAs() {
        List<OTFSession> sessions = otfSessionDao.getActiveSessionsTAs(SessionState.SCHEDULED,
                OTMSessionInProgressState.ACTIVE, OTFSessionToolType.VEDANTU_WAVE, 0, 200);
        logger.info("getActiveSessionsTAs got active sessions: " + sessions.size());
        if (sessions.size() >= 200) {
            logger.warn("more than 200 parallel session running.");
        }

        List<ActiveSessionsTAResp> sessionsTARespList = new ArrayList<>();
        ActiveSessionsTAResp activeSessionsTAResp;
        for (OTFSession session : sessions) {
            activeSessionsTAResp = mapper.map(session, ActiveSessionsTAResp.class);
            sessionsTARespList.add(activeSessionsTAResp);
        }

        return sessionsTARespList;
    }

    public void cacheSchedulingCollectionsAsync(int cronTime) {
        logger.info("Invoking cacheSchedulingCollectionsAsync");
        Map<String, Object> payload = new HashMap<>();
        payload.put("cronTime", cronTime);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CACHE_SCHEDULING_COLLECTIONS, payload);
        asyncTaskFactory.executeTask(params);
    }

    /*
    Avg otfSession doc size = 2KB
    100 parallel sessions = 200KB payload
     */
    public void cacheSchedulingCollections(int cronTime) throws VException {
        long currentTime = System.currentTimeMillis();
        logger.info("Launching cacheSchedulingCollections at time: {}", currentTime);
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long afterStartTime;
        Long beforeStartTime;
        if (cronTime == 15) {
            afterStartTime = currentTime + CACHE_SCHEDULING_COLLECTIONS_CRON_TIME_45; // currentTime + 45 minutes
            beforeStartTime = afterStartTime + CACHE_SCHEDULING_COLLECTIONS_CRON_WINDOW; // currentTime + 45 minutes + 3hrs
        } else {
            afterStartTime = currentTime + CACHE_SCHEDULING_COLLECTIONS_CRON_TIME_31; // currentTime + 31 minutes
            beforeStartTime = currentTime + CACHE_SCHEDULING_COLLECTIONS_CRON_TIME_45; // currentTime + 45 minutes
        }

        logger.info("cacheSchedulingCollections beforeStartTime {} afterStartTime {}", beforeStartTime, afterStartTime);
        List<OTFSession> sessions;
        int limit = 10;
        int count = 0;
        String lastFetchedId = null;
        do {
            sessions = otfSessionDao.getOTFSessions(afterStartTime, beforeStartTime, SessionState.SCHEDULED, null, lastFetchedId, limit);
            if (CollectionUtils.isEmpty(sessions)) {
                return;
            }
            logger.info("cacheSchedulingCollections got sessions: {}", sessions.size());
            // for logging only
            List<String> sessionIds = sessions.stream().map(AbstractMongoStringIdEntity::getId).collect(Collectors.toList());

            logger.info("sessionIds for caching in node: {}", sessionIds);
            try {
                awsSQSManager.sendToSQS(SEND_OTF_SESSION_TO_NODE_FOR_CACHING.getQueue(), SEND_OTF_SESSION_TO_NODE_FOR_CACHING, gson.toJson(sessions));
            } catch (Exception ignored) {
                try {
                    sendSessionsToNodeForCaching(sessions);
                } catch (VException e) {
                    logger.warn(e.getErrorMessage(), e);
                }
            }
            count += sessions.size();
            lastFetchedId = sessionIds.isEmpty() ? lastFetchedId : sessionIds.get(sessions.size() - 1);
            logger.info("lastFetched id for caching for query {}", lastFetchedId);
        } while (ArrayUtils.isNotEmpty(sessions));
        logger.info("sessions sent to queue {}", count);
    }

    public void sendSessionsToNodeForCaching(List<OTFSession> sessions) throws VException {
        String ids = Optional.ofNullable(sessions)
                .orElseGet(ArrayList::new)
                .stream().map(OTFSession::getId).collect(Collectors.joining(","));
        logger.info("sending to node for caching {}", ids);
        String payLoad = gson.toJson(sessions);
        ClientResponse resp = WebUtils.INSTANCE.doCall(OTMNODE_CACHE_COLLECTION_ENDPOINT,
                HttpMethod.POST, payLoad);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
    }

    public void cacheGTTAttendeeCollectionsAsync() {
        logger.info("Invoking cacheGTTAttendeeCollectionsAsync");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CACHE_GTT_ATTENDEE_COLLECTIONS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    /*
    Avg gttAttendee doc size = 1KB
    Avg Students per class = 50
    100 parallel sessions = 5MB payload
     */
    public void cacheGTTAttendeeCollections() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("Launching cacheGTTAttendeeCollections at time: " + currentTime);
//        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_HOUR;
//        logger.info("Launching cacheGTTAttendeeCollections currentTime: " + currentTime);
        Long afterStartTime = 0l;
        Long beforeStartTime = 0l;
        afterStartTime = currentTime + CACHE_GTT_ATTENDEE_COLLECTIONS_CRON_TIME; // currentTime + 30 mins
        beforeStartTime = afterStartTime + CACHE_GTT_ATTENDEE_COLLECTIONS_CRON_WINDOW; // currentTime + 30mins + 2hr

        logger.info("cacheGTTAttendeeCollections beforeStartTime " + beforeStartTime + " afterStartTime " + afterStartTime);

        List<OTFSession> oTFSessions = otfSessionDao.getOTFSessions(afterStartTime, beforeStartTime, SessionState.SCHEDULED, null, 0, 200);
        logger.info("cacheGTTAttendeeCollections got sessions: " + oTFSessions.size());
        if (oTFSessions.size() >= 100) {
            logger.warn("more than 100 sessions scheduled");
        }
        sendGttAttendeeDetailsToNodeForCaching(oTFSessions);
    }

    public void sendGttAttendeeDetailsToNodeForCaching(List<OTFSession> oTFSessions) throws VException {
        //Get the GTTAttendeeDetails Objects
        Set<String> sessionIds = new HashSet<>();
        for (OTFSession oTFSession : oTFSessions) {
            sessionIds.add(oTFSession.getId());
        }
        int attendeeSize = 500;
        int start = 0;
        List<GTTAttendeeDetails> gttAttendeeDetailsList = Collections.EMPTY_LIST;
        do {
            logger.info("start: " + start);
            try {
                gttAttendeeDetailsList = gTTAttendeeDetailsManager
                        .getAttendeeDetailsForSessions(sessionIds, start, attendeeSize);
                logger.info("attendees fetched: " + gttAttendeeDetailsList.size());
                Gson gson = new Gson();
                String payLoad = gson.toJson(gttAttendeeDetailsList);
                ClientResponse resp = WebUtils.INSTANCE.doCall(OTMNODE_CACHE_GTT_ATTENDEE_COLLECTION_ENDPOINT,
                        HttpMethod.POST, payLoad);
                String jsonString = resp.getEntity(String.class);
                logger.info(jsonString);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            } catch (Exception ex) {
                logger.error("sendGttAttendeeDetailsToNodeForCaching-error: " + start + " ,ex: " + ex);
            }
            start += gttAttendeeDetailsList.size();
        } while (gttAttendeeDetailsList.size() == attendeeSize);
    }

    public PlatformBasicResponse saveOtfSession(UpdateOTFSessionReq req) throws VException {
//        OTFSession session = oTFSessionDAO.getById(otfSessionRequest.getSessionId());
//        if (session == null) {
//            String errorMessage = "Session does not exist for req : " + otfSessionRequest.getSessionId();
//            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
//        }
//        session = mapper.map(otfSessionRequest, OTFSession.class);
//        session.setId(otfSessionRequest.getSessionId());
//        session.addFlag(OTFSessionFlag.NODE_TO_SCHEDULING.name());
//        logger.info("saveOtfSession: " + session);
//        oTFSessionDAO.save(session);
//        return new PlatformBasicResponse();
        OTFSession session = otfSessionDao.getById(req.getId());
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + req.getId();
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        if (Objects.nonNull(req.getFlags())) {
            session.setFlags(req.getFlags());
        }
        session.setPollCount(req.getPollCount());
        session.setQuizCount(req.getQuizCount());
        session.setHotspotCount(req.getHotspotCount());
        session.setNoticeCount(req.getNoticeCount());
        session.setChatCount(req.getChatCount());
        session.setUniqueStudentAttendants(req.getUniqueStudentAttendants());
        session.setTotalDoubtsAsked(req.getTotalDoubtsAsked());
        session.setTotalDoubtsResolved(req.getTotalDoubtsResolved());
        session.setSessionDuration(req.getSessionDuration());
        session.setPagesMetaData(req.getPagesMetaData());
        session.setOtmSessionConfig(req.getOtmSessionConfig());
        session.setProgressState(req.getProgressState());
        session.setStartedAt(req.getStartedAt());
        session.setRecordingStartTime(req.getRecordingStartTime());
        session.setRecordedTill(req.getRecordedTill());
        session.setVimeoId(req.getVimeoId());
        session.setAgoraVimeoId(req.getAgoraVimeoId());
        session.setEndedAt(req.getEndedAt());
        session.setEndedBy(req.getEndedBy());
        session.setAgoraStartTime(req.getAgoraStartTime());
        session.setAverageStudentDuration(req.getAverageStudentDuration());


        Set<OTFSessionFlag> sessionFlags = session.getFlags();
        for (OTFSessionFlag flag : sessionFlags) {
            session.addFlag(flag);
        }
        session.addFlag(OTFSessionFlag.NODE_TO_SCHEDULING);
        logger.info("saveOtfSession: " + session);
        otfSessionDao.save(session);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse saveGttAttendeeDetails(List<GTTAttendeeDetails> attendeeDetailsReqs) throws VException {
        if (attendeeDetailsReqs.size() == 0) {
            logger.info("got empty list");
            return new PlatformBasicResponse();
        }
        logger.info("saveGttAttendeeDetails got attendees: " + attendeeDetailsReqs.size());
        updateGttAttendeeFromNodeCache(attendeeDetailsReqs);
        // todo enable for gamification
//        for (GTTAttendeeDetails attendeeReq : attendeeDetailsReqs) {
//            try {
//                gTTAttendeeDetailsManager.processGttAttendeeForGameAsync(attendeeReq);
//            } catch (Exception e) {
//                logger.error("saveGttAttendeeDetails error: " + e);
//            }
//        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse updateGttAttendeeFromNodeCache(List<GTTAttendeeDetails> attendeeDetailsReq) throws VException {
        if (attendeeDetailsReq.isEmpty()) {
            logger.info("got empty list");
            throw new ForbiddenException(ErrorCode.GTT_CACHE_NO_ENTRY, "GTT Cache size 0");
        }
        logger.info("saveGttAttendeeDetails got attendees: " + attendeeDetailsReq.size());
        int MAX_SIZE = 100;
        if (attendeeDetailsReq.size() >= MAX_SIZE) {
            logger.info("got list size greater than 100");
            throw new ForbiddenException(ErrorCode.GTT_CACHE_MORE_THAN_100, "GTT Cache size more than 100");
        }

        Set<String> userIds = new HashSet<>();
        Set<String> sessionIds = new HashSet<>();
        Map<String, List<GTTAttendeeDetails>> attendeeDetailsStore = new HashMap<>();
        Map<String, GTTAttendeeDetails> attendeeReqMap = new HashMap<>();
        for (GTTAttendeeDetails attendeeReq : attendeeDetailsReq) {
            if (StringUtils.isEmpty(attendeeReq.getSessionId()) || StringUtils.isEmpty(attendeeReq.getUserId())) {
                throw new ForbiddenException(ErrorCode.SESSION_ID_OR_USER_ID_MISSING, "session id or user id missing");
            }
            userIds.add(attendeeReq.getUserId());
            sessionIds.add(attendeeReq.getSessionId());
            String key = attendeeReq.getUserId() + "_" + attendeeReq.getSessionId();
            attendeeDetailsStore.put(key, new ArrayList<>());
            attendeeReqMap.put(key, attendeeReq);
        }

        if (sessionIds.size() > userIds.size()) {
            //since userIds are less, will loop through them and get gtt attendeeDetails
            for (String userId : userIds) {
                int start = 0;
                while (true) {
                    List<GTTAttendeeDetails> attendeeDetailsList = gttAttendeeDetailsDAO
                            .getAttendeeDetailForSessionList(userId, sessionIds,
                                    Arrays.asList(GTTAttendeeDetails.Constants.SESSION_ID), start, MAX_SIZE);
                    if (ArrayUtils.isEmpty(attendeeDetailsList)) {
                        break;
                    } else {
                        for (GTTAttendeeDetails attendeeDetails : attendeeDetailsList) {
                            String key = userId + "_" + attendeeDetails.getSessionId();
                            attendeeDetailsStore.get(key).add(attendeeDetails);
                        }
                    }
                    start += MAX_SIZE;
                }
            }
        } else {
            //since sessionIds are less, will loop through them and get gtt attendeeDetails
            for (String sessionId : sessionIds) {
                int start = 0;
                while (true) {
                    List<GTTAttendeeDetails> attendeeDetailsList = gttAttendeeDetailsDAO
                            .getAttendeeDetailForUserList(sessionId, userIds,
                                    Arrays.asList(GTTAttendeeDetails.Constants.USER_ID), start, MAX_SIZE);
                    if (ArrayUtils.isEmpty(attendeeDetailsList)) {
                        break;
                    } else {
                        for (GTTAttendeeDetails attendeeDetails : attendeeDetailsList) {
                            String key = attendeeDetails.getUserId() + "_" + sessionId;
                            attendeeDetailsStore.get(key).add(attendeeDetails);
                        }
                    }
                    start += MAX_SIZE;
                }
            }
        }

        List<GTTAttendeeDetails> toBulkInsert = new ArrayList<>();
        List<Document> forBulkUpdateUpdates = new ArrayList<>();
        List<Document> forBulkUpdateQueries = new ArrayList<>();
        for (GTTAttendeeDetails attendeeReq : attendeeReqMap.values()) {
            String key = attendeeReq.getUserId() + "_" + attendeeReq.getSessionId();
            List<GTTAttendeeDetails> attendeeDetails = attendeeDetailsStore.get(key);
            if (attendeeDetails == null || attendeeDetails.isEmpty()) {
                logger.info("attendee not found for session id " + attendeeReq.getSessionId()
                        + " user Id " + attendeeReq.getUserId() + " saving attendee detail");
                toBulkInsert.add(attendeeReq);
            } else {
                for (GTTAttendeeDetails attendee : attendeeDetails) {
                    Document query = new Document();
                    query.append(GTTAttendeeDetails.Constants._ID, new ObjectId(attendee.getId()));
                    Document updateDocument = new Document();
                    Document setDocument = new Document();
                    setDocument.append(GTTAttendeeDetails.Constants.JOIN_TIMES, attendeeReq.getJoinTimes());
                    setDocument.append(GTTAttendeeDetails.Constants.LOCATION_INFO, attendeeReq.getLocationInfo());
                    setDocument.append(GTTAttendeeDetails.Constants.DEVICE_DETAILS, attendeeReq.getDeviceDetails());
                    updateDocument.append(GTTAttendeeDetails.Constants.UPDATE_SET_PARAM, setDocument);
                    forBulkUpdateUpdates.add(updateDocument);
                    forBulkUpdateQueries.add(query);
                }
            }
        }
        if (ArrayUtils.isNotEmpty(toBulkInsert)) {
            logger.info("bulk inserting " + toBulkInsert.size());
//            long insertCount = gttAttendeeDetailsDAO.bulkInsert(toBulkInsert, GTTAttendeeDetails.class, null);
            gTTAttendeeDetailsManager.queueAttendeesAsBulk(toBulkInsert);
//            logger.info("insertCount " + insertCount);
        }

        if (ArrayUtils.isNotEmpty(forBulkUpdateQueries)) {
            logger.info("bulk updating " + forBulkUpdateQueries.size());
            long modifiedcount = gttAttendeeDetailsDAO.bulkUpdate(forBulkUpdateQueries, forBulkUpdateUpdates,
                    false, GTTAttendeeDetails.class, null);
            logger.info("modifiedcount " + modifiedcount);
        }
        return new PlatformBasicResponse();
    }

    private static final int SEND_SMS_IF_SESSION_ABSENT = 3;

    public PlatformBasicResponse sendAbsenteeSMS() {
        // TODO optimisation required for aggregation
        if (true) {
            return new PlatformBasicResponse();
        }

        Long currentTime = new java.util.Date().getTime();
        Long endTime = currentTime + 60 * 60 * 1000l;
        Long daysToLookInThePast = currentTime - 45 * 24 * 60 * 60 * 1000l;
        List<OTFSession> scheduledSessions = otfSessionDao.getAllSessions(currentTime, endTime);
        if (ArrayUtils.isEmpty(scheduledSessions)) {
            return new PlatformBasicResponse();
        }
        logger.info("sessions count " + scheduledSessions.size());
        for (OTFSession session : scheduledSessions) {
            if (!SessionState.SCHEDULED.equals(session.getState())
                    || session.isWebinarSession()) {
                continue;
            }
            String sessionLink = WebUtils.INSTANCE.shortenUrl(FOS_ENDPOINT + "/session/" + session.getId());
            logger.info("Session Link for Session: " + session.getId() + " : " + sessionLink);

            List<GTTAttendeeDetails> attendees = gTTAttendeeDetailsManager.getAttendeeDetailsForSession(session.getId());
            logger.info("attendees count " + attendees.size());
            List<String> attendeeIds = attendees.stream().map(GTTAttendeeDetails::getUserId).distinct().collect(Collectors.toList());
            List<List<String>> attendeeIdChunks = Lists.partition(attendeeIds, 100);

            for (List<String> attendeeIdChunk : attendeeIdChunks) {

                try {
                    AggregationResults<AggregatedGTTAttendeeCountForSession> result = gTTAttendeeDetailsManager.getAbsenteeInLastClasses(attendeeIdChunk, SEND_SMS_IF_SESSION_ABSENT, daysToLookInThePast, currentTime);
                    List<AggregatedGTTAttendeeCountForSession> attendanceCountList = result.getMappedResults();

                    logger.info(attendanceCountList);

                    Set<String> userIds = new HashSet<>();
                    for (AggregatedGTTAttendeeCountForSession attendanceCountForUser : attendanceCountList) {
                        if (attendanceCountForUser.getCount() == SEND_SMS_IF_SESSION_ABSENT) {
                            userIds.add(attendanceCountForUser.getId());
                        }
                    }

                    List<UserBasicInfo> absentees = fosUtils.getUserBasicInfosSet(userIds, true);

                    logger.info(absentees);

                    if (ArrayUtils.isNotEmpty(absentees)) {
                        absentees.forEach(user -> {
                            if (Role.STUDENT.equals(user.getRole())) {
                                TextSMSRequest textSMSRequest = new TextSMSRequest();
                                textSMSRequest.setPhoneCode(user.getPhoneCode());
                                textSMSRequest.setTo(user.getContactNumber());
                                textSMSRequest.setType(CommunicationType.VEDANTU_WAVE_ABSENTEE_SMS);
                                Map<String, Object> scopeParams = new HashMap<>();// userName,url
                                scopeParams.put("sessionCount", SEND_SMS_IF_SESSION_ABSENT);
                                scopeParams.put("userName", user.getFullName());
                                scopeParams.put("url", sessionLink);
                                textSMSRequest.setScopeParams(scopeParams);
                                textSMSRequest.setRole(Role.STUDENT);
                                logger.info(textSMSRequest);
                                try {
                                    communicationManager.sendSMS(textSMSRequest);
                                } catch (VException e) {
                                    logger.error("Exception in sending sms " + e);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    logger.error("Error in finding absentees " + e.getMessage());
                }
            }
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse changeServiceProvider(List<String> sessionIds, OTMServiceProvider serviceProvider)
            throws VException {
        Map<String, String> otfSessionInfo = new HashMap<>();

        List<OTFSession> oTFSessions = otfSessionDao.getBy_Ids(sessionIds);
        if (oTFSessions == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "sessions not found");
        }
        int failedFor = 0;
        switch (serviceProvider) {
            case IMPARTUS:
                for (OTFSession otfSession : oTFSessions) {
                    try {
                        vedantuWaveScheduleManager.changeServiceProviderToImpartus(otfSession);
                        otfSessionDao.save(otfSession);
                        otfSessionInfo.put(otfSession.getId(), otfSession.getMeetingId());
                    } catch (Exception ex) {
                        failedFor++;
                        logger.error("changeServiceProvider-error: " + otfSession.getId() + " " + ex);
                    }
                }
                break;
            case AGORA:
                for (OTFSession otfSession : oTFSessions) {
                    try {
                        otfSession.setServiceProvider(OTMServiceProvider.AGORA);
                        otfSessionDao.save(otfSession);
                    } catch (Exception ex) {
                        failedFor++;
                        logger.error("changeServiceProvider-error: " + otfSession.getId() + " " + ex);
                    }
                }
                break;
            default:
                // do nothing
        }

        if (failedFor > 0) {
            throw new ForbiddenException(ErrorCode.CHANGE_SERVICE_PROVIDER_FAILED, "failed for " + failedFor + " sessions");
        }
        return new PlatformBasicResponse(true, otfSessionInfo, null);
    }

    public PlatformBasicResponse updateMultiSessionsConfig(List<String> sessionIds, UpdateSessionConfigReq otmSessionConfigReq) throws VException {
        if (otmSessionConfigReq == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "session config object is missing");
        }
        if (ArrayUtils.isEmpty(sessionIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "sessionids are missing");
        }
        if (sessionIds.size() > 100) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "do not send more than 100 sessionIds at once");
        }
        try {
            otfSessionDao.setUpdateOtmSessionConfig(otmSessionConfigReq, sessionIds);
        } catch (Exception ex) {
            throw new ForbiddenException(ErrorCode.SET_SESSION_CONFIG_FAILED, "failed for " + " sessions");
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse changeSessionParameters(String sessionId, Long startTime, Long endTime, String otmSessionType, String progressState, String vimeoId, String agoraVimeoId) throws VException {
        if (sessionId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "sessionId is missing");
        }
        try {
            Update update = new Update();
            if (startTime != null) {
                update.set(OTFSession.Constants.START_TIME, startTime);
            }
            if (endTime != null) {
                update.set(OTFSession.Constants.END_TIME, endTime);
            }
            if (otmSessionType != null) {
                update.set(OTFSession.Constants.OTMSESSION_TYPE, otmSessionType);
            }
            if (progressState != null) {
                update.set(OTFSession.Constants.PROGRESS_STATE, progressState);
            }
            if (vimeoId != null) {
                update.set(OTFSession.Constants.VIMEO_ID, vimeoId);
            }
            if (agoraVimeoId != null) {
                update.set(OTFSession.Constants.AGORA_VIMEO_ID, agoraVimeoId);
            }

            // session state not changed via this flow
            otfSessionDao.updateSession(update, sessionId);
        } catch (Exception ex) {
            throw new ForbiddenException(ErrorCode.CHANGE_SESSION_PARAMETERS, "sessionId " + sessionId);
        }
        return new PlatformBasicResponse();
    }

    public void initSessionRecordingFromNode(OTFSession session) throws VException {

        // String recorderEndPt = "https://" + NODE_SERVER_URL_WEBINAR + "/recorder/checkAndLaunchSessionRecordersCron";
        // ClientResponse resp = WebUtils.INSTANCE.doCall(recorderEndPt, HttpMethod.POST, null);
        // String otfSessionRecorderEndPt = "https://" + NODE_SERVER_URL_WEBINAR + "/recorder/launchOtfSessionRecorder";
        String otfAgoraRecorderEndPt = "https://" + NODE_SERVER_URL_WEBINAR + "/recorder/launchOtfAgoraRecorder";
        String otfSessionRecorderSQSEndpt = "https://" + NODE_SERVER_URL_WEBINAR + "/recorder/sendSQSOtfSessionRecorder";

        JSONObject sessionJson = new JSONObject();
        sessionJson.put(OTFSession.Constants._ID, session.getId());
        sessionJson.put(OTFSession.Constants.ID, session.getId());
        sessionJson.put(OTFSession.Constants.CANVAS_STREAMING_TYPE, session.getCanvasStreamingType());
        sessionJson.put(OTFSession.Constants.START_TIME, session.getStartTime());
        sessionJson.put(OTFSession.Constants.END_TIME, session.getEndTime());
        sessionJson.put(OTFSession.Constants.SESSION_TOOL_TYPE, session.getSessionToolType());
        sessionJson.put(OTFSession.Constants.SESSION_CONTEXT_TYPE, session.getSessionContextType());
        sessionJson.put(OTFSession.Constants.SESSION_LABELS, session.getLabels());
        sessionJson.put(OTFSession.Constants.ENTITY_TAGS, session.getEntityTags());
        sessionJson.put(OTFSession.Constants.OTMSESSION_TYPE, session.getOtmSessionType());
        sessionJson.put(OTFSession.Constants.FLAGS, session.getFlags());

        JSONObject otfSessionRecorderBody = new JSONObject();
        otfSessionRecorderBody.put("session", sessionJson);
        // ClientResponse ec2RecorderResp = WebUtils.INSTANCE.doCall(otfSessionRecorderEndPt, HttpMethod.POST, otfSessionRecorderBody.toString());
        // VExceptionFactory.INSTANCE.parseAndThrowException(ec2RecorderResp);
        // ec2RecorderResp.getEntity(String.class);

        ClientResponse otfSessionRecorderSQSResp = WebUtils.INSTANCE.doCall(otfSessionRecorderSQSEndpt, HttpMethod.POST, otfSessionRecorderBody.toString());
        VExceptionFactory.INSTANCE.parseAndThrowException(otfSessionRecorderSQSResp);
        otfSessionRecorderSQSResp.getEntity(String.class);

        if (!session.isSalesDemoSession()) {
            ClientResponse agoraRecorderResp = WebUtils.INSTANCE.doCall(otfAgoraRecorderEndPt, HttpMethod.POST, sessionJson.toString());
            VExceptionFactory.INSTANCE.parseAndThrowException(agoraRecorderResp);
            agoraRecorderResp.getEntity(String.class);
        }
    }

}
