/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            String env = ConfigUtils.INSTANCE.getStringValue("environment");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                CreateQueueRequest cqr = null;
                try {
                    cqr = new CreateQueueRequest(SQSQueue.NOTIFICATION_QUEUE
                            .getQueueName(env));
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(cqr);
                    logger.info("created queue for notification");
                } catch (Exception e) {
                    logger.error("error in creating queue notification " + e.getMessage());
                }

                try {
                    CreateQueueRequest otfsessionqueuedl = new CreateQueueRequest(SQSQueue.OTF_SESSION_QUEUE_DL
                            .getQueueName(env));
                    otfsessionqueuedl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(otfsessionqueuedl);
                    logger.info("created queue for otfsessionqueuedl deadletter");

                    CreateQueueRequest otfsessionqueue = new CreateQueueRequest(SQSQueue.OTF_SESSION_QUEUE
                            .getQueueName(env));
                    otfsessionqueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    otfsessionqueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTF_SESSION_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(otfsessionqueue);

                    assignDeadLetterQueue(SQSQueue.OTF_SESSION_QUEUE
                            .getQueueName(env), SQSQueue.OTF_SESSION_QUEUE_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("error in creating queue otfsessionqueue and dl " + e.getMessage());
                }

                try {
                    cqr = new CreateQueueRequest(SQSQueue.OTF_POSTSESSION_QUEUE
                            .getQueueName(env));
                    cqr.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    cqr.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTF_POSTSESSION_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(cqr);
                    logger.info("created queue for postsession tasks");
                } catch (Exception e) {
                    logger.error("error in creating queue otfPostSession " + e.getMessage());
                }

                try {
                    CreateQueueRequest postSessionQueueNonFifo_dl = new CreateQueueRequest(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO_DL
                            .getQueueName(env));
                    sqsClient.createQueue(postSessionQueueNonFifo_dl);
                    logger.info("created queue for postSessionQueueNonFifo_dl deadletter");

                    CreateQueueRequest postSessionQueueNonFifo = new CreateQueueRequest(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO
                            .getQueueName(env));
                    postSessionQueueNonFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO.getVisibilityTimeout());
                    sqsClient.createQueue(postSessionQueueNonFifo);

                    assignDeadLetterQueue(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO
                            .getQueueName(env), SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO_DL
                            .getQueueName(env), 1);
                } catch (Exception e) {
                    logger.error("error in creating queue postSessionQueueNonFifo and dl " + e.getMessage());
                }

                try {
                    // init-- queues for calendar ops
                    CreateQueueRequest calendarOpsDl = new CreateQueueRequest(SQSQueue.CALENDAR_OPS_DL
                            .getQueueName(env));
                    sqsClient.createQueue(calendarOpsDl);
                    logger.info("created queue for CALENDAR_OPS_DL deadletter");

                    CreateQueueRequest calendarOps = new CreateQueueRequest(SQSQueue.CALENDAR_OPS
                            .getQueueName(env));
                    calendarOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.CALENDAR_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(calendarOps);
                    logger.info("sqs queue created-- " + SQSQueue.CALENDAR_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.CALENDAR_OPS
                            .getQueueName(env), SQSQueue.CALENDAR_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error("error in creating queue calendarOps and dl " + e.getMessage());
                }

                try {
                    // init-- queues for calendar ops
                    CreateQueueRequest calendarBatchOpsDl = new CreateQueueRequest(SQSQueue.CALENDAR_BATCH_OPS_DL
                            .getQueueName(env));
                    sqsClient.createQueue(calendarBatchOpsDl);
                    logger.info("created queue for CALENDAR_BATCH_OPS_DL deadletter");

                    CreateQueueRequest calendarBatchOps = new CreateQueueRequest(SQSQueue.CALENDAR_BATCH_OPS
                            .getQueueName(env));
                    calendarBatchOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.CALENDAR_BATCH_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(calendarBatchOps);
                    logger.info("sqs queue created-- " + SQSQueue.CALENDAR_BATCH_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.CALENDAR_BATCH_OPS
                            .getQueueName(env), SQSQueue.CALENDAR_BATCH_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error("error in creating queue calendarBatchOps and dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest otfpresessionqueue = new CreateQueueRequest(SQSQueue.OTF_PRESESSION_STUDENTLIST_QUEUE
                            .getQueueName(env));
                    otfpresessionqueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTF_PRESESSION_STUDENTLIST_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(otfpresessionqueue);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest calendarOpsFifoDl = new CreateQueueRequest(SQSQueue.CALENDAR_OPS_FIFO_DL
                            .getQueueName(env));
                    calendarOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(calendarOpsFifoDl);
                    logger.info("created queue for calendarOpsFifoDl deadletter");

                    CreateQueueRequest calendarOpsFifo = new CreateQueueRequest(SQSQueue.CALENDAR_OPS_FIFO
                            .getQueueName(env));
                    calendarOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    calendarOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.CALENDAR_OPS_FIFO.getVisibilityTimeout());
                    sqsClient.createQueue(calendarOpsFifo);

                    assignDeadLetterQueue(SQSQueue.CALENDAR_OPS_FIFO
                            .getQueueName(env), SQSQueue.CALENDAR_OPS_FIFO_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("error in creating queue calendarOpsFifo and dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest calendarBatchOpsFifoDl = new CreateQueueRequest(SQSQueue.CALENDAR_BATCH_FIFO_OPS_DL
                            .getQueueName(env));
                    calendarBatchOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(calendarBatchOpsFifoDl);
                    logger.info("created queue for calendarBatchOpsFifoDl deadletter");

                    CreateQueueRequest calendarBatchOpsFifo = new CreateQueueRequest(SQSQueue.CALENDAR_BATCH_FIFO_OPS
                            .getQueueName(env));
                    calendarBatchOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    calendarBatchOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.CALENDAR_BATCH_FIFO_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(calendarBatchOpsFifo);

                    assignDeadLetterQueue(SQSQueue.CALENDAR_BATCH_FIFO_OPS
                            .getQueueName(env), SQSQueue.CALENDAR_BATCH_FIFO_OPS_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("error in creating queue calendarBatchOpsFifo and dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest gttMarkDeleteOpsFifoDl = new CreateQueueRequest(SQSQueue.GTT_MARK_DELETE_OPS_FIFO_DL
                            .getQueueName(env));
                    gttMarkDeleteOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gttMarkDeleteOpsFifoDl);
                    logger.info("created-- gtt mark delete ops fifo dl");

                    CreateQueueRequest gttMarkDeleteOpsFifo = new CreateQueueRequest(SQSQueue.GTT_MARK_DELETE_OPS_FIFO
                            .getQueueName(env));
                    gttMarkDeleteOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    gttMarkDeleteOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            SQSQueue.GTT_MARK_DELETE_OPS_FIFO.getVisibilityTimeout());
                    sqsClient.createQueue(gttMarkDeleteOpsFifo);
                    logger.info("created-- gtt mark delete ops fifo ");

                    assignDeadLetterQueue(SQSQueue.GTT_MARK_DELETE_OPS_FIFO
                            .getQueueName(env), SQSQueue.GTT_MARK_DELETE_OPS_FIFO_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("Error while creating gttMarkDeleteOpsFifoDl or dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest gttUpdateOpsFifoDl = new CreateQueueRequest(SQSQueue.GTT_UPDATE_OPS_FIFO_DL
                            .getQueueName(env));
                    gttUpdateOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gttUpdateOpsFifoDl);
                    logger.info("created-- gtt update ops fifo dl");

                    CreateQueueRequest gttUpdateOpsFifo = new CreateQueueRequest(SQSQueue.GTT_UPDATE_OPS_FIFO
                            .getQueueName(env));
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            SQSQueue.GTT_UPDATE_OPS_FIFO.getVisibilityTimeout());
                    sqsClient.createQueue(gttUpdateOpsFifo);
                    logger.info("created-- gtt update ops fifo ");

                    assignDeadLetterQueue(SQSQueue.GTT_UPDATE_OPS_FIFO
                            .getQueueName(env), SQSQueue.GTT_UPDATE_OPS_FIFO_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("Error while creating gttUpdateOpsFifo or dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest gttUpdateOpsFifoDl = new CreateQueueRequest(SQSQueue.GTT_BULK_UPDATE_OPS_FIFO_DL
                            .getQueueName(env));
                    gttUpdateOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gttUpdateOpsFifoDl);
                    logger.info("created-- gtt update ops fifo dl");

                    CreateQueueRequest gttUpdateOpsFifo = new CreateQueueRequest(SQSQueue.GTT_BULK_UPDATE_OPS_FIFO
                            .getQueueName(env));
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            SQSQueue.GTT_BULK_UPDATE_OPS_FIFO.getVisibilityTimeout());
                    sqsClient.createQueue(gttUpdateOpsFifo);
                    logger.info("created-- gtt update ops fifo ");

                    assignDeadLetterQueue(SQSQueue.GTT_BULK_UPDATE_OPS_FIFO
                            .getQueueName(env), SQSQueue.GTT_BULK_UPDATE_OPS_FIFO_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("Error while creating gttUpdateOpsFifo or dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest gttUpdateOpsFifoDl = new CreateQueueRequest(SQSQueue.GTT_ENROLMENT_OPS_DL
                            .getQueueName(env));
                    gttUpdateOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gttUpdateOpsFifoDl);
                    logger.info("created-- gtt enrollment update ops fifo dl");

                    CreateQueueRequest gttUpdateOpsFifo = new CreateQueueRequest(SQSQueue.GTT_ENROLMENT_OPS
                            .getQueueName(env));
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            SQSQueue.GTT_ENROLMENT_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(gttUpdateOpsFifo);
                    logger.info("created-- gtt enrollment update ops fifo ");

                    assignDeadLetterQueue(SQSQueue.GTT_ENROLMENT_OPS
                            .getQueueName(env), SQSQueue.GTT_ENROLMENT_OPS_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("Error while creating gttUpdateOpsFifo or dl " + e.getMessage());
                }

                try {
                    CreateQueueRequest postSessionSummaryEmailsDl = new CreateQueueRequest(SQSQueue.POST_SESSION_SUMMARY_EMAILS_DL
                            .getQueueName(env));
                    sqsClient.createQueue(postSessionSummaryEmailsDl);
                    logger.info("created-- post session summary emails non fifo dl");

                    CreateQueueRequest postSessionSummaryEmails = new CreateQueueRequest(SQSQueue.POST_SESSION_SUMMARY_EMAILS
                            .getQueueName(env));
                    postSessionSummaryEmails.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            SQSQueue.POST_SESSION_SUMMARY_EMAILS.getVisibilityTimeout());
                    sqsClient.createQueue(postSessionSummaryEmails);
                    logger.info("created-- postSessionSummaryEmails non fifo");

                    assignDeadLetterQueue(SQSQueue.POST_SESSION_SUMMARY_EMAILS
                            .getQueueName(env), SQSQueue.POST_SESSION_SUMMARY_EMAILS_DL
                            .getQueueName(env), 2);
                } catch (Exception e) {
                    logger.error("Error while creating postSessionSummaryEmails or dl " + e.getMessage());
                }

                try {
                    SQSQueue dlQueue = SQSQueue.OTF_SESSION_GTT_CREATE_DL_QUEUE;
                    CreateQueueRequest gttUpdateOpsFifoDl = new CreateQueueRequest(dlQueue
                            .getQueueName(env));
                    gttUpdateOpsFifoDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gttUpdateOpsFifoDl);
                    logger.info("created-- OTF_SESSION_GTT_CREATE_DL_QUEUE");

                    SQSQueue mainQueue = SQSQueue.OTF_SESSION_GTT_CREATE_QUEUE;
                    CreateQueueRequest gttUpdateOpsFifo = new CreateQueueRequest(mainQueue
                            .getQueueName(env));
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    gttUpdateOpsFifo.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            mainQueue.getVisibilityTimeout());
                    sqsClient.createQueue(gttUpdateOpsFifo);
                    logger.info("created-- OTF_SESSION_GTT_CREATE_QUEUE");

                    assignDeadLetterQueue(mainQueue
                            .getQueueName(env), dlQueue
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("Error while creating OTF_SESSION_GTT_CREATE_QUEUE or dl " + e.getMessage());
                }


                try {
                    logger.info("creating register gtt queue");
                    SQSQueue dlQueue = SQSQueue.REGISTER_GTT_DL_QUEUE;
                    CreateQueueRequest dlCreateQueueRequest = new CreateQueueRequest(dlQueue
                            .getQueueName(env));
                    sqsClient.createQueue(dlCreateQueueRequest);
                    logger.info("created-- REGISTER_GTT_DL_QUEUE");

                    SQSQueue mainQueue = SQSQueue.REGISTER_GTT_QUEUE;
                    CreateQueueRequest createQueueRequest = new CreateQueueRequest(mainQueue
                            .getQueueName(env));
                    createQueueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(),
                            mainQueue.getVisibilityTimeout());
                    sqsClient.createQueue(createQueueRequest);
                    logger.info("created-- REGISTER_GTT_QUEUE");

                    assignDeadLetterQueue(mainQueue
                            .getQueueName(env), dlQueue
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error("Error while creating OTF_SESSION_GTT_CREATE_QUEUE or dl " + e.getMessage(), e);
                }

            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

    }

    private void assignDeadLetterQueue(String src_queue_name, String dl_queue_name, Integer maxReceive) {
        if (maxReceive == null) {
            maxReceive = 1;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                        .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\"" + maxReceive + "\", \"deadLetterTargetArn\":\""
                        + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: " + dl_queue_name + "  to:" + src_queue_name);

    }

    @PostConstruct
    public void postQueueCreation(){

    }

}
