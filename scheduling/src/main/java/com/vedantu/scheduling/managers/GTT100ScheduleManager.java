package com.vedantu.scheduling.managers;

import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.dao.serializers.GTTOrganizerTokenDAO;
import com.vedantu.util.LogFactory;

@Service("gtt100ScheduleManager")
public class GTT100ScheduleManager extends GTTScheduleManager {
	@Autowired
	private LogFactory logFactory;
        
        @Autowired
	private GTTOrganizerTokenDAO gttOrganizerTokenDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTT100ScheduleManager.class);

	private String[] authAccessArray = { 
//            "gtt100Seat1" , "gtt100Seat2", "gtt100Seat3","gtt100Seat4","gtt100Seat5","gtt100Seat6"
//                ,"gtt100Seat7","gtt100Seat8","gtt100Seat9","gtt100Seat10",
//                        "gtt100Seat11","gtt100Seat12","gtt100Seat13","gtt100Seat14","gtt100Seat15","gtt100Seat16",
//                        "gtt100Seat17","gtt100Seat18","gtt100Seat19","gtt100Seat21","gtt100Seat22","gtt100Seat23","gtt100Seat24",
//                        "gtt100Seat25","gtt100Seat26","gtt100Seat27","gtt100Seat28","gtt100Seat29","gtt100Seat30",
//                        "gtt100Seat31"
        };
//, 
	protected static Map<String, String> overwriteAccessKeys = Collections.unmodifiableMap(new HashMap<String, String>() {

		private static final long serialVersionUID = 4675157667161370845L;
		{

//			put("gtt100Seat1", "8DyP9XAcOkbVZ9BBobTpt6CeMgnj"); // gtt201@vedantu.com
//			put("gtt100Seat2", "1qYbXULdMTvBrTo14JRc3ungBqxB"); // gtt202@vedantu.com
//			put("gtt100Seat3", "hqO9PqnjbVgTyD0l6r1JzfLHsZKh"); // gtt203@vedantu.com
//			put("gtt100Seat4", "tigMZlhGkILkLoHqLH2PU1kE5I1d"); // gtt204@vedantu.com
//			put("gtt100Seat5", "pre3vYq7kWHRGp3db8yGv3XVXieX"); // gtt205@vedantu.com
//			put("gtt100Seat6", "a80aAxhFOI7S27XQf37HSFpJ2FEv"); // gtt206@vedantu.com
//                        put("gtt100Seat7", "oAuo0o211j0pBb2PxvWvX8eLnHRi"); // gtt207@vedantu.com
//                        put("gtt100Seat8", "LBTj5X0GcVqx1POCHJqp0wDlMqgj"); // gtt208@vedantu.com
//                        put("gtt100Seat9", "9iYi5Ue99jMpVfgK0jas8IXl9Gw8"); // gtt209@vedantu.com
//                        put("gtt100Seat10", "LnKZOw8fzae9Qaf8anMAS96msgpj"); // gtt210@vedantu.com
//                        
//                        put("gtt100Seat11", "PQMEt6G48s4rEBRr28SH5QjO7F8b"); // gtt211@vedantu.com
//                        put("gtt100Seat12", "SaQSEztAVpDk2I7nN68O6nSTjGFT"); // gtt212@vedantu.com
//                        put("gtt100Seat13", "mT0jl6AVVOOEk4GH2YyEaDxt4OtA"); // gtt213@vedantu.com
//                        put("gtt100Seat14", "jmoGbzPGEUIVwEi8jWjC187GekKM"); // gtt214@vedantu.com
//                        put("gtt100Seat15", "BbesN2uZuKynQVJhqeu3O0rcFHNJ"); // gtt215@vedantu.com
//                        put("gtt100Seat16", "Lhq2j8eP3hlib8BmHPr3R8xm04gJ"); // gtt216@vedantu.com
//                        put("gtt100Seat17", "nlJzrmC4WrnMOzIme2umjG9xToOL"); // gtt217@vedantu.com
//                        put("gtt100Seat18", "iHbMGQueCzSe0VgQ19F2uBGbX3YH"); // gtt218@vedantu.com
//                        put("gtt100Seat19", "6kDE9MwoPk4tB9PK17AY3zysyM3f"); // gtt219@vedantu.com
//                        put("gtt100Seat21", "ONluAAROt08plfdPILHbGHgtYldL"); // gtt221@vedantu.com
//                        put("gtt100Seat22", "qJM6BAOSDFNJMk9RfBJPZMvmnbiQ"); // gtt222@vedantu.com
//                        put("gtt100Seat23", "TaziAmlQkuNBsmhstHf9AmPuDKmZ"); // gtt223@vedantu.com
//                        put("gtt100Seat24", "bKMcvUjhkQ7ucsgjZlQAifnNsJaa"); // gtt224@vedantu.com
//                        put("gtt100Seat25", "ADgeqA4C13qABfofvnALO7QU9RrZ"); // gtt225@vedantu.com
//                        put("gtt100Seat26", "ALq0HbXCSkxEHvDnqvri9GedSPT9"); // gtt226@vedantu.com
//                        put("gtt100Seat27", "k9FHUy5g8togoZfLzqDT7ANBrGYB"); // gtt227@vedantu.com
//                        put("gtt100Seat28", "9CFGY41XrWv1BzYnAANB1ZEgc7SC"); // gtt228@vedantu.com
//                        put("gtt100Seat29", "0yCweYmQFgHSZkuptnkSq7OoqCow"); // gtt229@vedantu.com
//                        put("gtt100Seat30", "MgOnHU7e0kaZbAHYshbbGBYJjmJ4"); // gtt230@vedantu.com
//                        put("gtt100Seat31", "GjFvPjkHnI19CSdNFKK3umwu2P4N"); // gtt231@vedantu.com
                        
                        //put("gtt100Seat20", "wGZzZ4yECuHw0YVuj1FCd2M0Bfy6"); // gtt220@vedantu.com
                        
//			put("gtt100Seat21", "cFagwmw73mzG0VA1wGmArgTAMkc1"); // gtt221@vedantu.com - rohit.bhardwaj@vedantu.com
//			put("gtt100Seat22", "4MEQF0DbTTEsscrotouAHchHEPEW"); // gtt222@vedantu.com - sathish.ss@vedantu.com
//			put("gtt100Seat23", "SAMGR0Eh7lEvMhOp2N2g4AAygfCI"); // gtt223@vedantu.com - anand.gautam@vedantu.com
//			put("gtt100Seat10", "Nk9zJpVRPmCFR833ofjvORyRG9ux"); // gtt210@vedantu.com - anshul.goswami@vedantu.com
		}
	});

	protected static Map<String, String> organizerKeys = Collections.unmodifiableMap(new HashMap<String, String>() {
		private static final long serialVersionUID = 2357808695344388664L;

		{

//			put("gtt100Seat1", "7435517037865464070"); // gtt100Seat1
//			put("gtt100Seat2", "4963599394345971974"); // gtt100Seat2
//			put("gtt100Seat3", "3271812738524246278"); // gtt100Seat3
//			put("gtt100Seat4", "2085562838365456901"); // gtt100Seat4
//			put("gtt100Seat5", "4219832555622844933"); // gtt100Seat5
//                        
//                        
//			put("gtt100Seat6", "366900819826002437"); // gtt100Seat6
//                        put("gtt100Seat7", "610274423591820037"); // gtt100Seat7
//			put("gtt100Seat8", "78913439236594181"); // gtt100Seat8
//			put("gtt100Seat9", "9142785921319672325"); // gtt100Seat9
//			put("gtt100Seat10", "9118518600183028229"); // gtt100Seat10
//
//                        
//			put("gtt100Seat11", "9005415137568594437"); // gtt100Seat11
//			put("gtt100Seat12", "3731558136917760517"); // gtt100Seat12
//			put("gtt100Seat13", "5202229705858849285"); // gtt100Seat13
//			put("gtt100Seat14", "6638725154873776645"); // gtt100Seat14
//			put("gtt100Seat15", "7171158163066165765"); // gtt100Seat15
//                        
//                        put("gtt100Seat16", "6565304166417406469"); // gtt100Seat16
//			put("gtt100Seat17", "8112985529767926277"); // gtt100Seat17
//			put("gtt100Seat18", "628669253124594181"); // gtt100Seat18
//			put("gtt100Seat19", "1295507662207949317"); // gtt100Seat19
//			//put("wGZzZ4yECuHw0YVuj1FCd2M0Bfy6", "3784316003353336325"); // gtt100Seat20
//                        put("gtt100Seat21", "3046470035918190348"); // gtt100Seat21
//                        put("gtt100Seat22", "6297834770902972172"); // gtt100Seat22
//                        put("gtt100Seat23", "2968199101671700236"); // gtt100Seat23
//                        put("gtt100Seat24", "6465190336255129356"); // gtt100Seat24
//                        put("gtt100Seat25", "1744462451166312204"); // gtt100Seat25
//                        put("gtt100Seat26", "8371112379144369932"); // gtt100Seat26
//                        put("gtt100Seat27", "6077360699301328652"); // gtt100Seat27
//                        put("gtt100Seat28", "653085010412599052"); // gtt100Seat28
//                        put("gtt100Seat29", "1595639154300319500"); // gtt100Seat29
//                        put("gtt100Seat30", "5269787000885901068"); // gtt100Seat30
//                        put("gtt100Seat31", "6959105253103269644"); // gtt100Seat31
                        

//                        put("cFagwmw73mzG0VA1wGmArgTAMkc1", "5374078972381728518"); // gtt100Seat21
//                        put("4MEQF0DbTTEsscrotouAHchHEPEW", "1684681999095938309"); // gtt100Seat22
//                        put("SAMGR0Eh7lEvMhOp2N2g4AAygfCI", "6894136198961578757"); // gtt100Seat23
			
		}
	});

	private List<String> authAccessList = Arrays.asList(authAccessArray);

	@Override
	public int getAccessArrayLength() {
		return authAccessArray.length;
	}

	@Override
	public String getAccessToken(int slot) {
		if (slot != -1 && slot < authAccessList.size()) {
			return authAccessList.get(slot);
		}
		return null;
	}

	@Override
	public int getAccessTokenSlot(String accessToken) {
		int returnVal = -1;
		if (!StringUtils.isEmpty(accessToken)) {
			returnVal = authAccessList.indexOf(accessToken);
		}
		return returnVal;
	}

	@Override
        public String getPostOverwriteAccessToken(String accessToken) {
            GTTOrganizerToken token = gttOrganizerTokenDAO.getTokenForSeat(accessToken);
            if (token != null && !StringUtils.isEmpty(token.getOrganizerAccessToken()) && token.getExpiresIn() > System.currentTimeMillis()) {
                return token.getOrganizerAccessToken();
            } else {
                logger.error("Just as reference to look into : " + token);
                if (overwriteAccessKeys.containsKey(accessToken)) {
                    return overwriteAccessKeys.get(accessToken);
                }
                return accessToken;
            }
        }

	@Override
	public BitSet getScheduleBitSet(Schedule schedule) {
		return schedule.getGtt100ScheduleBitSet();
	}

	@Override
	public String getOrganizerKeyForToken(String accessToken) { //seat Name for accessToken
		return organizerKeys.get(accessToken);
	}
}
