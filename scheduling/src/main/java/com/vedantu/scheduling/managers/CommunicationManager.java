/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.*;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.SMSPriorityType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.review.response.FeedbackInfo;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.threadutil.CachedThreadPoolUtility;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.apache.logging.log4j.core.net.Priority;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.WordUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class CommunicationManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private AmazonS3Manager awsS3Manager;

    @Autowired
    private CachedThreadPoolUtility cachedThreadPoolUtility;    
    
    @Autowired
    private OTFSessionManager oTFSessionManager;

    private Logger logger = logFactory.getLogger(CommunicationManager.class);
    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
    private static final String FOS_SESSION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT");
    private String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
    private static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
    private static final SimpleDateFormat sdfDay = new SimpleDateFormat("E");
    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mma");
    private static final SimpleDateFormat sdfDateDay = new SimpleDateFormat("E, dd MMM yyyy");
    private static final SimpleDateFormat addToCalenderDate = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat addToCalenderTime = new SimpleDateFormat("kkmm");
    private static final SimpleDateFormat sdfDayDateTime = new SimpleDateFormat("E, dd MMM 'at' hh:mm a z");
    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    final Gson gson = new Gson();

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail" + gson.toJson(request));
        if (Objects.nonNull(request.getType()) && request.getType().equals(CommunicationType.OTF_POST_SESSION_TO_STUDENT_NEW_WAVE)) {
            awsSQSManager.sendToSQS(SQSQueue.POST_SESSION_SUMMARY_EMAILS, SQSMessageType.OTF_SESSION_SUMMARY_EMAIL, gson.toJson(request));
        } else {
            awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, gson.toJson(request));
        }return null;
    }

    public String sendSMS(TextSMSRequest request) throws VException {
        if (StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: " + request);
            return null;
        }
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(request));
        return null;
    }

    public String getStundentTestAttemptUrl(ContentInfoResp info) {
        String link = ConfigUtils.INSTANCE.getStringValue("contentinfo.test.studentAttemptLink");
        return String.format(link, info.getId());
    }

    private void sendCommunicationToParent(User user, Map<String, Object> subjectScopes, Map<String, Object> bodyScopes,
            CommunicationType communicationType, boolean includeHeaderFooter) throws AddressException, VException {
        sendCommunicationToParent(user, subjectScopes, bodyScopes, communicationType, includeHeaderFooter, null, null);
    }

    private void sendCommunicationToParent(User user, Map<String, Object> subjectScopes, Map<String, Object> bodyScopes,
            CommunicationType communicationType, boolean includeHeaderFooter, List<InternetAddress> ccList, List<InternetAddress> replyToList)
            throws AddressException, VException {
        Set<String> parentEmails = user.getStudentInfo().getParentEmails(user.getEmail());

        // Validate all  parent emailIds

        for(String emailId:parentEmails){
            if(!CommonUtils.isValidEmailId(emailId)){
                logger.info("Email Id :" + emailId  + "is invalid for userId:" + user.getUserId());
                return;
            }
        }


        if (CollectionUtils.isNotEmpty(parentEmails)) {
            ArrayList<InternetAddress> parentToAddress = new ArrayList<>();

            for (String parentEmail : parentEmails) {
                if (StringUtils.isEmpty(parentEmail)) {
                    continue;
                }
                parentToAddress.add(new InternetAddress(parentEmail));
            }

            if (!parentToAddress.isEmpty()) {
                EmailRequest emailRequest = new EmailRequest(parentToAddress, subjectScopes, bodyScopes,
                        communicationType, Role.PARENT);
                if (ArrayUtils.isNotEmpty(ccList)) {
                    emailRequest.setCc(ccList);
                }

                if (ArrayUtils.isNotEmpty(replyToList)) {
                    emailRequest.setReplyTo(replyToList);
                }
                emailRequest.setIncludeHeaderFooter(includeHeaderFooter);
                sendEmail(emailRequest);
            }
        }
    }

    public HashMap<String, Object> getBasicOTMSessionInfoMap(OTFSession session) {
        HashMap<String, Object> bodyScopes = new HashMap<>();
        if (session == null) {
            return null;
        }
        bodyScopes.put("sessionTitle", session.getTitle());
        sdfDateDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfDateDay.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionDate", timestr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfTime.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionTime", timestr);
        String teacherId = session.getPresenter();
        String dateTime = DateTimeFormatter.ofPattern(sdfDayDateTime.toPattern())
                .withZone(ZoneId.of("Asia/Calcutta"))
                .format(Instant.ofEpochMilli(session.getStartTime()));
        bodyScopes.put("sessionDateTime", dateTime);
        if (StringUtils.isEmpty(teacherId)) {
            return null;
        }
        UserBasicInfo teacherInfo = fosUtils.getUserBasicInfo(teacherId, true);
        if (teacherInfo == null) {
            return null;
        }
        Long duration = session.getEndTime() - session.getStartTime();
        duration = (Long) (duration / DateTimeUtils.MILLIS_PER_MINUTE);
        bodyScopes.put("teacherName", teacherInfo.getFullName());
        bodyScopes.put("teacherExt", teacherInfo.getExtensionNumber());
        bodyScopes.put("teacherProfileUrl", FOS_ENDPOINT + "/v/teacher/" + teacherId);
        bodyScopes.put("otmSession", true);
        bodyScopes.put("sessionDuration", duration);
        bodyScopes.put("mySessionsLink", FOS_ENDPOINT + "/v/mysessions");
        bodyScopes.put("sessionLink",  WebUtils.INSTANCE.shortenUrl(FOS_SESSION_ENDPOINT + "session/" + session.getId()));
        bodyScopes.put("replayLink", FOS_ENDPOINT + "/v/otfsessionreplay/" + session.getId());
        bodyScopes.put("notesLink", FOS_ENDPOINT + "/v/otfSessionNotes/" + session.getId());
        return bodyScopes;
    }

    public void sendOtfPostSessionEmail(GTTAttendeeDetails gTTAttendeeDetails, HashMap<String, Object> bodyScopes,
            HashMap<String, Object> subjectScopes, CommunicationType type)
            throws UnsupportedEncodingException, VException, AddressException {
        if (gTTAttendeeDetails == null) {
            return;
        }
        User userBasicInfo = fosUtils.getUserInfo(Long.valueOf(gTTAttendeeDetails.getUserId()), true);

        if (userBasicInfo == null) {
            logger.error("sendOtfPostSessionEmail - userBasicInfo not found:" + gTTAttendeeDetails.getUserId());
            return;
        }
        if (!Role.STUDENT.equals(userBasicInfo.getRole())) {
            logger.info("attendee is not student, not sending email");
            return;
        }

        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        if (ArrayUtils.isNotEmpty(gTTAttendeeDetails.getPollAnswered())) {
            bodyScopes.put("pollsAnswered", String.valueOf(gTTAttendeeDetails.getPollAnswered().size()));
        } else {
            bodyScopes.put("pollsAnswered", "0");
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();

        // EmailId validation
        Boolean isValidEmail = CommonUtils.isValidEmailId(userBasicInfo.getEmail());

        // If invalid emailId
        if(!isValidEmail){
            logger.info("This emailId : " +  userBasicInfo.getEmail()  + " is invalid for userId :" + userBasicInfo.getUserId() );
            return;
        }

        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));


        boolean includeHeaderFooter = true;
        if (CommunicationType.OTF_POST_SESSION_TO_STUDENT_NEW_WAVE.equals(type)) {
            includeHeaderFooter = false;
        }
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes,
                type, userBasicInfo.getRole());
        request.setIncludeHeaderFooter(false);
        sendEmail(request);
        sendCommunicationToParent(userBasicInfo, subjectScopes, bodyScopes, type, false);
    }

    public void sendOtoPostSessionEmail(FeedbackInfo feedback, Session session) throws AddressException, IOException, VException {

        List<Long> studentIds = session.getStudentIds();
        if (ArrayUtils.isEmpty(studentIds)) {
            return;
        }
        UserBasicInfo teacher = fosUtils.getUserBasicInfo(session.getTeacherId(), true);
        User student = fosUtils.getUserInfo(studentIds.get(0), true);
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", student.getFullName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("teacherImg", teacher.getProfilePicUrl());
        if (feedback != null) {
            bodyScopes.put("feedbackGiven", true);
            bodyScopes.put("feedback", feedback.getSessionMessage());
            bodyScopes.put("feedbackPartnerMessage", feedback.getPartnerMessage());
            List<String> reason = feedback.getReason();
            if (reason != null && !reason.isEmpty()) {
                bodyScopes.put("feedbackReason", ListUtils.join(reason, ","));
            } else {
                bodyScopes.put("feedbackReason", "NA");
            }
            bodyScopes.put("feedbackOptionalMessage",
                    StringUtils.isNotEmpty(feedback.getOptionalMessage()) ? feedback.getOptionalMessage() : "NA");
            bodyScopes.put("feedbackSessionCoverage",
                    StringUtils.isNotEmpty(feedback.getSessionCoverage()) ? feedback.getSessionCoverage() : "NA");
            bodyScopes.put("feedbackNextSessionPlan",
                    StringUtils.isNotEmpty(feedback.getNextSessionPlan()) ? feedback.getNextSessionPlan() : "NA");
            bodyScopes.put("feedbackHomeWork",
                    StringUtils.isNotEmpty(feedback.getHomeWork()) ? feedback.getHomeWork() : "NA");
            List<String> studentPerformance = feedback.getStudentPerformance();
            if (studentPerformance != null && !studentPerformance.isEmpty()) {
                bodyScopes.put("feedbackStudentPerformance", ListUtils.join(studentPerformance, ","));
            } else {
                bodyScopes.put("feedbackStudentPerformance", "NA");
            }
        }

        bodyScopes.put("sessionTitle", session.getTitle());
        sdfDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfDate.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionDate", timestr);
        sdfDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfDay.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionDay", timestr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfTime.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionTime", timestr);

        bodyScopes.put("replayLink", FOS_ENDPOINT + "/v/replaysession/" + session.getId());
        bodyScopes.put("notesLink", FOS_ENDPOINT + "/v/replaysession/" + session.getId() + "?saveNotes=true");

        // EmailId validation
        Boolean isValidEmail = CommonUtils.isValidEmailId(student.getEmail());

        // If invalid emailId
        if(!isValidEmail){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"This emailId : " +  student.getEmail()  + " is invalid for userId :" + student.getUserId() );
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.OTO_POST_SESSION,
                student.getRole());
        request.setIncludeHeaderFooter(false);
        sendEmail(request);
        sendCommunicationToParent(student, bodyScopes, bodyScopes, CommunicationType.OTO_POST_SESSION, false);
    }

    public void sendNewSessionRelatedEmail(Long userId, HashMap<String, Object> bodyScopes, CommunicationType communicationType, boolean sms) throws UnsupportedEncodingException, VException, AddressException {
        sendNewSessionRelatedEmail(userId, bodyScopes, communicationType, sms, System.currentTimeMillis());
    }
    public void sendNewSessionRelatedEmail(Long userId, HashMap<String, Object> bodyScopes,
                                           CommunicationType communicationType, boolean sms,
                                           long expectedDelivery) throws UnsupportedEncodingException, VException, AddressException {
        if (userId == null || communicationType == null) {
            return;
        }
        User user = fosUtils.getUserInfo(userId, true);
        if (user == null) {
            return;
        }
        bodyScopes.put("userName", user.getFullName());
        if (Role.TEACHER.equals(user.getRole())) {
            bodyScopes.put("otherUserRole", "student");
        } else {
            bodyScopes.put("otherUserRole", "teacher");
        }
        ArrayList<InternetAddress> toList = new ArrayList<>();

        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));

        logger.info("Send Mail Type :" + communicationType);
        if (communicationType == CommunicationType.SESSION_HOUR_REMINDER || communicationType == CommunicationType.SESSION_LIVE_REMINDER) {
            EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                    user.getRole());
            request.setExpectedDeliveryMillis(expectedDelivery);
            sendEmail(request);
            logger.info("Mail Request Sent");
            if (StringUtils.isNotEmpty(user.getContactNumber()) && sms) {
                TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                        user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
                textSMSRequest.setExpectedDeliveryMillis(expectedDelivery);
                sendSMS(textSMSRequest);
                logger.info("SMS Request Sent");
            }
        } else {
            EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                    user.getRole());
            request.setExpectedDeliveryMillis(expectedDelivery);
            sendEmail(request);
            if (StringUtils.isNotEmpty(user.getContactNumber()) && sms) {
                TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                        user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
                textSMSRequest.setExpectedDeliveryMillis(expectedDelivery);
                sendSMS(textSMSRequest);
            }
            if (Role.STUDENT.equals(user.getRole())) {
                sendCommunicationToParent(user, bodyScopes, bodyScopes, communicationType, true);
            }
        }
    }

    public void sendOtmNewSessionRelatedEmailToAllParticipantsAsync(OTFSession session, HashMap<String, Object> bodyScopes, CommunicationType communicationType, boolean teacher, boolean sms) throws VException {
        Map<String, Object> payload = new HashMap<>();
        payload.put("session", session);
        payload.put("bodyScopes", bodyScopes);
        payload.put("communicationType", communicationType);
        payload.put("teacher", teacher);
        payload.put("sms", sms);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTM_SESSION_RELATED_EMAILS, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void sendOtmNewSessionRelatedEmailToAllParticipants(OTFSession session, HashMap<String, Object> bodyScopes, CommunicationType communicationType, boolean teacher, boolean sms) throws VException {
        if (session == null || communicationType == null) {
            return;
        }
        Type mapType = new TypeToken<HashMap<String, Object>>() {
        }.getType();

        List<String> userIds=new ArrayList<>();
        
        if (ArrayUtils.isNotEmpty(session.getBatchIds())) {
            int start = 0, limit = 1000;
            //TODO remove this hackish code, for now done this to avoid the api call fetching 8MB data from subscription
            for (String batchId : session.getBatchIds()) {
                do {
                    String url = SUBSCRIPTION_ENDPOINT + "enroll/getEnrolledStudentsInBatch?batchId=" + batchId
                            + "&start=" + start + "&size=" + limit;
                    logger.info("URL = " + url);
                    ClientResponse response = WebUtils.INSTANCE.doCall(url,
                            HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(response);
                    String jsonString = response.getEntity(String.class);

                    final List<String> studentIds = gson.fromJson(jsonString, new TypeToken<List<String>>() {
                    }.getType());
                    if (CollectionUtils.isEmpty(studentIds)) {
                        break;
                    }
                    userIds.addAll(studentIds);//TODO possible memory leak later
                    start += limit;
                } while (true);
            }
            for (String userId : userIds) {
                HashMap<String, Object> scope = gson.fromJson(gson.toJson(bodyScopes, mapType), mapType);
                try {
                    sendNewSessionRelatedEmail(Long.valueOf(userId), scope, communicationType, sms);
                } catch (Exception ex) {
                    logger.error("error in sending " + communicationType + " session:" + session.getId() + " email to userId: " + userId + " error:" + ex.getMessage());
                }
            }            
        }

        logger.info("Got userIds to send email to " + userIds.size());
        if (teacher) {
            HashMap<String, Object> scope = gson.fromJson(gson.toJson(bodyScopes, mapType), mapType);
            try {
                sendNewSessionRelatedEmail(Long.valueOf(session.getPresenter()), scope, communicationType, sms);
            } catch (Exception ex) {
                logger.error("error in sending " + communicationType + " session:" + session.getId() + " email to teacher error:" + ex.getMessage());
            }
        }
    }

    public void sendSessionCancelationEmail(OTFSession session) throws VException {
        if (session == null || !SessionState.CANCELLED.equals(session.getState())) {
            return;
        }
        HashMap<String, Object> bodyScopes = getBasicOTMSessionInfoMap(session);
        if (bodyScopes == null) {
            return;
        }
        bodyScopes.put("reason", session.getRemarks());
        sendOtmNewSessionRelatedEmailToAllParticipantsAsync(session, bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED, true, true);
    }

    public void sendSessionRescheduledEmail(OTFSession preUpdate, OTFSession session) throws VException {
        if (preUpdate == null || session == null) {
            return;
        }
        HashMap<String, Object> bodyScopes = getBasicOTMSessionInfoMap(session);
        if (bodyScopes == null) {
            return;
        }
        sdfDateDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfDateDay.format(new Date(preUpdate.getStartTime()));
        bodyScopes.put("preSessionDate", timestr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfTime.format(new Date(preUpdate.getStartTime()));
        bodyScopes.put("preSessionTime", timestr);
        sendOtmNewSessionRelatedEmailToAllParticipantsAsync(session, bodyScopes, CommunicationType.OTM_OTO_SESSION_RESCHEDULE, true, true);
    }

    public void sendOtmExtraClassEmail(OTFSession session) throws VException {
        if (session == null) {
            return;
        }
        HashMap<String, Object> bodyScopes = getBasicOTMSessionInfoMap(session);
        if (bodyScopes == null) {
            return;
        }
        Long boardId = session.getBoardId();
        Board board = null;
        if (boardId != null) {
            List<Long> boardIds = new ArrayList();
            boardIds.add(boardId);
            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            if (boardMap != null && boardMap.containsKey(boardId)) {
                board = boardMap.get(boardId);
            }
        }
        if (board != null) {
            bodyScopes.put("sessionSubject", board.getName());
        }
        sendOtmNewSessionRelatedEmailToAllParticipantsAsync(session, bodyScopes, CommunicationType.OTM_EXTRA_SESSION_SCHEDULED, true, true);
    }

    public void sendOtmTrialSessionEmail(OTFSession session) throws VException {
        if (session == null) {
            return;
        }
        HashMap<String, Object> bodyScopes = getBasicOTMSessionInfoMap(session);
        if (bodyScopes == null) {
            return;
        }
        Long boardId = session.getBoardId();
        Board board = null;
        if (boardId != null) {
            List<Long> boardIds = new ArrayList();
            boardIds.add(boardId);
            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            if (boardMap != null && boardMap.containsKey(boardId)) {
                board = boardMap.get(boardId);
            }
        }
        if (board != null) {
            bodyScopes.put("sessionSubject", board.getName());
        }
        sendOtmNewSessionRelatedEmailToAllParticipantsAsync(session, bodyScopes, CommunicationType.OTM_TRIAL_SESSION, false, true);
    }

    public void sendOtmNoShowEmail(String userId, OTFSession upcoming, List<OTFSession> missed) throws VException, UnsupportedEncodingException, AddressException {
        if (StringUtils.isEmpty(userId) || ArrayUtils.isEmpty(missed)) {
            return;
        }
        HashMap<String, Object> bodyScopes = new HashMap<>();
        List<HashMap<String, Object>> oldSessions = new ArrayList<>();
        for (OTFSession session : missed) {
            HashMap<String, Object> map = getBasicOTMSessionInfoMap(session);
            if (map != null) {
                oldSessions.add(map);
            }
        }
        bodyScopes.put("oldSessionList", oldSessions);
        HashMap<String, Object> nextSession = getBasicOTMSessionInfoMap(upcoming);
        if (nextSession != null) {
            bodyScopes.put("nextSession", nextSession);
        }
        sendNewSessionRelatedEmail(Long.valueOf(userId), bodyScopes, CommunicationType.OTM_NO_SHOW_STUDENT, true);
    }

    public void sendOTMLateJoinCommunication(OTFSession session, GTTAttendeeDetails attendeeDetails) throws VException {
        HashMap<String, Object> bodyScopes = getBasicOTMSessionInfoMap(session);
        UserBasicInfo user = fosUtils.getUserBasicInfo(attendeeDetails.getUserId(), true);
        if (user == null || !Role.STUDENT.equals(user.getRole())) {
            return;
        }
        bodyScopes.put("studentName", user.getFirstName());

        String sessionLink = WebUtils.INSTANCE.shortenUrl(FOS_ENDPOINT + "/session/" + session.getId());
        bodyScopes.put("sessionLink", sessionLink);

        TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                user.getPhoneCode(), bodyScopes, CommunicationType.VEDANTU_WAVE_SESSION_LATE_JOIN_SMS, user.getRole());
        sendSMS(textSMSRequest);
    }

    public void sendEarlyLearningSessionMailAndSms(OTFSession otfSession) throws IOException, VException {
        sendEarlyLearningSessionMailAndSms(otfSession,false);
    }

    public void sendEarlyLearningSessionMailAndSms(OTFSession otfSession, boolean reScheduledSession) throws IOException, VException {

        EmailRequest emailRequest = new EmailRequest();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String subject = "Early learning session booked";

        emailRequest.setSubject(subject);
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        // collect email addresses of all attendees
        String studentId = Optional.ofNullable(otfSession.getAttendees()).orElseGet(ArrayList::new)
                    .stream()
                    .findFirst()
                    .orElse(null);

        if (StringUtils.isEmpty(studentId)) {
            logger.info("student id is null, failed to send mail");
            return;
        }
        String sessionLink = FOS_ENDPOINT + "/session/" + otfSession.getId();
        bodyScopes.put("sessionLink", sessionLink);
        bodyScopes.put("sessionId", otfSession.getId());
        sdfDateDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timeStr = sdfDateDay.format(new Date(otfSession.getStartTime()));
        bodyScopes.put("sessionDate", timeStr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timeStr = sdfTime.format(new Date(otfSession.getStartTime()));
        bodyScopes.put("sessionTime", timeStr);
        String addToCalenderDateStr = addToCalenderDate.format(new Date(otfSession.getStartTime()));
        String addToCalenderStartTimeStr = addToCalenderTime.format(new Date(otfSession.getStartTime()));
        String addToCalenderEndTimeStr = addToCalenderTime.format(new Date(otfSession.getEndTime()));
        String addToCalendarLink = "http://www.google.com/calendar/event?action=TEMPLATE&dates=" + addToCalenderDateStr
                + "T" + addToCalenderStartTimeStr +"00Z%2F"+addToCalenderDateStr +"T"+addToCalenderEndTimeStr+"00Z&ctz="+TIME_ZONE_IN+"&text=Vedantu%20SuperKids%20demo%20session&location=Click%20on%20the%20session%20joining%20link : "+ sessionLink +"&details=Check%20email%20from%20Vedantu%20with%20the%20session%20joining%20link.%0A";
        logger.info("addToCalenderLink : "+addToCalendarLink);
        bodyScopes.put("addToCalendarLink", addToCalendarLink);

        List<User> userInfos = new ArrayList<>();
        userInfos = fosUtils.getUserInfos(Arrays.asList(Long.valueOf(studentId), Long.valueOf(otfSession.getPresenter())), true);

        for(User userInfo : userInfos){
            if(Objects.nonNull(userInfo)){
                if(Role.STUDENT.equals(userInfo.getRole())){
                    String studentName = Optional.ofNullable(userInfo.getFullName()).orElse("");
                    bodyScopes.put("studentName", WordUtils.capitalizeFully(studentName));
                    if(Objects.nonNull(userInfo.getStudentInfo())) {
                        String parentfirstName = Optional.ofNullable(userInfo.getStudentInfo().getParentFirstName()).orElse("");
                        String studentGrade = Optional.ofNullable(userInfo.getStudentInfo().getGrade()).orElse("");
                        bodyScopes.put("studentGrade", WordUtils.capitalizeFully(studentGrade));
                        bodyScopes.put("parentName", WordUtils.capitalizeFully(parentfirstName));
                    }
                } else if(Role.TEACHER.equals(userInfo.getRole())){
                    String teacherName = Optional.ofNullable(userInfo.getFullName()).orElse("");
                    bodyScopes.put("teacherName", WordUtils.capitalizeFully(teacherName));
                }
            }
        }
        boolean isSuperCoder = otfSession.getLabels().contains(SessionLabel.SUPER_CODER);
        if(isSuperCoder) {
            emailRequest.setType(CommunicationType.EARLY_LEARNING_DEMO_SESSION_BOOKED);
            bodyScopes.put("courseName", SessionLabel.SUPER_CODER);
        } else {
            emailRequest.setType(CommunicationType.EARLY_LEARNING_SUPER_READER_PRE_DEMO_SESSION_BOOKED);
            bodyScopes.put("courseName", SessionLabel.SUPER_READER);
        }
        emailRequest.setIsEarlyLearning(Boolean.TRUE);
        for(User userInfo : userInfos) {
            if (Objects.nonNull(userInfo)) {
                // Enable this for Normal booking in case of overbooking student mail is not required on join session button
//                if (Role.STUDENT.equals(userInfo.getRole())) {
//                    if (reScheduledSession || !isSuperCoder || isSuperCoder) {
//                        continue;// for manual teacher allocation and superReader JoinSession button student mail won't trigger
//                    }
//                    emailRequest.setRole(Role.STUDENT);
//                    bodyScopes.put("certificateEligibility",Boolean.FALSE);
//                    // sending Demo Class Scheduled SMS
//                    if (StringUtils.isNotEmpty(userInfo.getContactNumber()) && isSuperCoder) {
//                        TextSMSRequest textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(), userInfo.getPhoneCode(), bodyScopes,
//                                CommunicationType.SUPER_KIDS_DEMO_CLASS_SCHEDULED_REMINDER, Role.STUDENT);
//                        try {
//                            logger.info("textSMSRequest" + textSMSRequest);
//                            sendSMS(textSMSRequest);
//                        } catch (VException e) {
//                            logger.error("Error while sending early session demo sms" + userInfo.getUserId());
//                        }
//
//                    }
                //} else
                if (Role.TEACHER.equals(userInfo.getRole())) {
                    if (isSuperCoder) {
                        bodyScopes.put("certificateEligibility", Boolean.TRUE);
                    }
                    emailRequest.setRole(Role.TEACHER);
                    if (StringUtils.isNotEmpty(userInfo.getContactNumber())) {
                        TextSMSRequest textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(), userInfo.getPhoneCode(), bodyScopes,
                                CommunicationType.SUPER_KIDS_DEMO_CLASS_SCHEDULED_REMINDER, Role.TEACHER);
                        try {
                            logger.info("textSMSRequest" + textSMSRequest);
                            textSMSRequest.setPriorityType(SMSPriorityType.HIGH);
                            sendSMS(textSMSRequest);
                        } catch (VException e) {
                            logger.error("Error while sending early session demo sms" + userInfo.getUserId());
                        }

                    }
                    try {
                        if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                            emailRequest.setBodyScopes(bodyScopes);
                            emailRequest.setTo(Collections.singletonList(new InternetAddress(userInfo.getEmail())));
                            sendEmail(emailRequest);
                        }
                    } catch (VException | AddressException exc) {
                        logger.error("Error while sending early session demo email" + userInfo.getUserId());
                    }
                }
            }
        }
    }

    public void sendEarlyLearningBookingMailAndSms(OverBooking overBooking) throws VException {

        EmailRequest emailRequest = new EmailRequest();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String subject = "Early learning session booked";

        emailRequest.setSubject(subject);
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        // collect email addresses of all attendees
        String studentId = overBooking.getStudentId();

        if (StringUtils.isEmpty(studentId)) {
            logger.info("student id is null, failed to send mail");
            return;
        }
        bodyScopes.put("bookingId", overBooking.getId());
        sdfDateDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timeStr = sdfDateDay.format(new Date(overBooking.getSlotStartTime()));
        bodyScopes.put("sessionDate", timeStr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timeStr = sdfTime.format(new Date(overBooking.getSlotStartTime()));
        bodyScopes.put("sessionTime", timeStr);
        String addToCalenderDateStr = addToCalenderDate.format(new Date(overBooking.getSlotStartTime()));
        String addToCalenderStartTimeStr = addToCalenderTime.format(new Date(overBooking.getSlotStartTime()));
        String addToCalenderEndTimeStr = addToCalenderTime.format(new Date(overBooking.getSlotEndTime()));
        String addToCalendarLink = "http://www.google.com/calendar/event?action=TEMPLATE&dates=" + addToCalenderDateStr
                + "T" + addToCalenderStartTimeStr +"00Z%2F"+addToCalenderDateStr +"T"+addToCalenderEndTimeStr+"00Z&ctz="+TIME_ZONE_IN+"&text=Vedantu%20SuperKids%20demo%20session" +"&details=Check%20email%20from%20Vedantu%20with%20the%20session%20joining%20link.%0A";
        logger.info("addToCalenderLink : "+addToCalendarLink);
        bodyScopes.put("addToCalendarLink", addToCalendarLink);

        List<User> userInfos=fosUtils.getUserInfos(Arrays.asList(Long.valueOf(studentId)), true);

        for(User userInfo : userInfos){
            if(Objects.nonNull(userInfo)){
                if(Role.STUDENT.equals(userInfo.getRole())){
                    String studentName = Optional.ofNullable(userInfo.getFullName()).orElse("");
                    bodyScopes.put("studentName", WordUtils.capitalizeFully(studentName));
                    if(Objects.nonNull(userInfo.getStudentInfo())) {
                        String parentfirstName = Optional.ofNullable(userInfo.getStudentInfo().getParentFirstName()).orElse("");
                        String studentGrade = Optional.ofNullable(userInfo.getStudentInfo().getGrade()).orElse("");
                        bodyScopes.put("studentGrade", WordUtils.capitalizeFully(studentGrade));
                        bodyScopes.put("parentName", WordUtils.capitalizeFully(parentfirstName));
                    }
                }
            }
        }
        String loginLink = FOS_ENDPOINT;
        boolean isSuperCoder = overBooking.getLabels().contains(SessionLabel.SUPER_CODER);
        if(isSuperCoder) {
            loginLink = loginLink + "/superkids/freetrial?showLogin=true";
            emailRequest.setType(CommunicationType.EARLY_LEARNING_DEMO_SESSION_BOOKED);
        } else {
            loginLink = loginLink + "/earlylearning?earlyLearningCourseType=SUPER_READER";
            emailRequest.setType(CommunicationType.EARLY_LEARNING_SUPER_READER_PRE_DEMO_SESSION_BOOKED);
        }
        bodyScopes.put("loginLink", loginLink);
        emailRequest.setBodyScopes(bodyScopes);

        for(User userInfo : userInfos) {
            if (Objects.nonNull(userInfo)) {
                if (Role.STUDENT.equals(userInfo.getRole())) {
                    emailRequest.setRole(Role.STUDENT);
                    // sending Demo Class Scheduled SMS
                    if (StringUtils.isNotEmpty(userInfo.getContactNumber()) && isSuperCoder) {
                        TextSMSRequest textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(), userInfo.getPhoneCode(), bodyScopes,
                                CommunicationType.SUPER_KIDS_DEMO_CLASS_SCHEDULED_REMINDER, Role.STUDENT);
                        try {
                            logger.info("textSMSRequest" + textSMSRequest);
                            sendSMS(textSMSRequest);
                        } catch (VException e) {
                            logger.error("Error while sending early session demo sms" + userInfo.getUserId());
                        }

                    }
                    logger.info("sending early learning session email -- " + userInfo.getUserId());

                    try {
                        if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                            emailRequest.setTo(Collections.singletonList(new InternetAddress(userInfo.getEmail())));
                            sendEmail(emailRequest);
                        }
                    } catch (VException | AddressException exc) {
                        logger.error("Error while sending early session demo email" + userInfo.getUserId());
                    }
                }
            }
        }
    }

    public void sendSuperReaderSessionMail(OTFSession earlyLearningSession) {

        EmailRequest emailRequest = new EmailRequest();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String subject = "Early Learning Session Completed Successfully";

        emailRequest.setSubject(subject);
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        // collect email addresses of all attendees
        String studentId = Optional.ofNullable(earlyLearningSession.getAttendees()).orElseGet(ArrayList::new)
                .stream()
                .findFirst()
                .orElse(null);

        if (StringUtils.isEmpty(studentId)) {
            logger.info("student id is null, failed to send mail");
            return;
        }

        List<User> userInfos = new ArrayList<>();
        userInfos = fosUtils.getUserInfos(Arrays.asList(Long.valueOf(studentId)), true);

        for(User userInfo : userInfos){
            if(Objects.nonNull(userInfo)){
                if(Role.STUDENT.equals(userInfo.getRole())){
                    String studentName = Optional.ofNullable(userInfo.getFirstName()).orElse("");
                    bodyScopes.put("studentName", StringUtils.toLowerCase(studentName));
                    if(Objects.nonNull(userInfo.getStudentInfo())) {
                        String parentfirstName = Optional.ofNullable(userInfo.getStudentInfo().getParentFirstName()).orElse("");
                        String studentGrade = Optional.ofNullable(userInfo.getStudentInfo().getGrade()).orElse("");
                        bodyScopes.put("studentGrade", WordUtils.capitalizeFully(studentGrade));
                        bodyScopes.put("parentName", WordUtils.capitalizeFully(parentfirstName));
                    }
                }
            }
        }
        emailRequest.setBodyScopes(bodyScopes);
        emailRequest.setType(CommunicationType.EARLY_LEARNING_SUPER_READER_POST_DEMO_SESSION_MAIL);

        for(User userInfo : userInfos) {
            if (Objects.nonNull(userInfo)) {
                if (Role.STUDENT.equals(userInfo.getRole())) {
                    emailRequest.setRole(Role.STUDENT);
                    // sending Demo Class Scheduled SMS
                    logger.info("sending early learning session email -- " + userInfo.getUserId());

                    try {
                        if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                            emailRequest.setTo(Collections.singletonList(new InternetAddress(userInfo.getEmail())));
                            sendEmail(emailRequest);
                        }
                    } catch (VException | AddressException exc) {
                        logger.error("Error while sending early session demo email" + userInfo.getUserId());
                    }
                }
            }
        }

    }

    public void sendELDemoSessionCertificate(OTFSession otfSession) throws VException, IOException {

        EmailRequest emailRequest = new EmailRequest();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String subject = "Early learning Demo Session Certificate";

        emailRequest.setSubject(subject);
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        // collect email addresses of all attendees
        String studentId = Optional.ofNullable(otfSession.getAttendees()).orElseGet(ArrayList::new)
                .stream()
                .findFirst()
                .orElse(null);

        if (StringUtils.isEmpty(studentId)) {
            logger.info("student id is null, failed to send mail");
            return;
        }
        String sessionLink = FOS_ENDPOINT + "/session/" + otfSession.getId();
        bodyScopes.put("sessionLink", sessionLink);
        bodyScopes.put("sessionId", otfSession.getId());
        sdfDateDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timeStr = sdfDateDay.format(new Date(otfSession.getStartTime()));
        bodyScopes.put("sessionDate", timeStr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timeStr = sdfTime.format(new Date(otfSession.getStartTime()));
        bodyScopes.put("sessionTime", timeStr);

        List<User> userInfos = new ArrayList<>();
        userInfos = fosUtils.getUserInfos(Arrays.asList(Long.valueOf(studentId), Long.valueOf(otfSession.getPresenter())), true);

        for(User userInfo : userInfos){
            if(Objects.nonNull(userInfo)){
                if(Role.STUDENT.equals(userInfo.getRole())){
                    String studentName = Optional.ofNullable(userInfo.getFullName()).orElse("");
                    bodyScopes.put("studentName", WordUtils.capitalizeFully(studentName));
                    if(Objects.nonNull(userInfo.getStudentInfo())) {
                        String parentfirstName = Optional.ofNullable(userInfo.getStudentInfo().getParentFirstName()).orElse("");
                        String studentGrade = Optional.ofNullable(userInfo.getStudentInfo().getGrade()).orElse("");
                        bodyScopes.put("studentGrade", WordUtils.capitalizeFully(studentGrade));
                        bodyScopes.put("parentName", WordUtils.capitalizeFully(parentfirstName));
                    }
                } else if(Role.TEACHER.equals(userInfo.getRole())){
                    String teacherName = Optional.ofNullable(userInfo.getFullName()).orElse("");
                    bodyScopes.put("teacherName", WordUtils.capitalizeFully(teacherName));
                }
            }
        }
        emailRequest.setType(CommunicationType.EARLY_LEARNING_DEMO_COMPLETION_CERTIFICATE);
        emailRequest.setIsEarlyLearning(Boolean.TRUE);
        bodyScopes.put("certificateEligibility",Boolean.TRUE);
        emailRequest.setBodyScopes(bodyScopes);
        for(User userInfo : userInfos) {
            if (Objects.nonNull(userInfo)) {
                if (Role.STUDENT.equals(userInfo.getRole())) {
                    if(System.currentTimeMillis() < otfSession.getStartTime()){
                        continue;
                    }
                    emailRequest.setRole(Role.STUDENT);
                } else if (Role.TEACHER.equals(userInfo.getRole())) {
                        continue;
                }
                try {
                    if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                        emailRequest.setTo(Collections.singletonList(new InternetAddress(userInfo.getEmail())));
                        sendEmail(emailRequest);
                    }
                } catch (VException | AddressException exc) {
                    logger.error("Error while sending early session demo email" + userInfo.getUserId());
                }
            }
        }
    }
}
