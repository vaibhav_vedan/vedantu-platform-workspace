package com.vedantu.scheduling.managers;


import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Field;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.*;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.LoginTokenContext;
import com.vedantu.User.request.CreateUserAuthenticationTokenReq;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.User.response.CreateUserAuthenticationTokenRes;
import com.vedantu.app.pojos.OTFSessionPojoForApp;
import com.vedantu.app.requests.OTFSessionReq;
import com.vedantu.app.responses.WeeklySessionDetails;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.request.*;
import com.vedantu.onetofew.response.AddRemoveBatchIdsToSessionRes;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.scheduling.Interfaces.ITeacherProficiencyManager;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.*;
import com.vedantu.scheduling.dao.serializers.*;
import com.vedantu.scheduling.enums.DeleteContext;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.enums.wavesession.InteractionType;
import com.vedantu.scheduling.pojo.*;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.TeacherSessionDashboard;
import com.vedantu.scheduling.pojo.wavesession.InteractionData;
import com.vedantu.scheduling.request.*;
import com.vedantu.scheduling.request.session.GetUserPastSessionReq;
import com.vedantu.scheduling.request.session.GetUserUpcomingSessionReq;
import com.vedantu.scheduling.request.wavesession.CreateTownhallSessionReq;
import com.vedantu.scheduling.request.wavesession.SetTAsToSessionRequest;
import com.vedantu.scheduling.response.*;
import com.vedantu.scheduling.response.session.CounselorDashboardSessionsInfoRes;
import com.vedantu.scheduling.response.session.OTFBatchSessionReportRes;
import com.vedantu.scheduling.response.session.OTFSessionPojoApp;
import com.vedantu.scheduling.response.session.OTFSessionStats;
import com.vedantu.scheduling.response.wavesession.CreateTownhallSessionRes;
import com.vedantu.session.pojo.*;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.request.BatchDetailInfoReq;
import com.vedantu.subscription.request.section.BatchSectionReq;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.OTMSessionDashboardInfo;
import com.vedantu.subscription.response.StudentAttendanceInfo;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.*;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.pojo.DeviceDetails;
import com.vedantu.util.pojo.DeviceType;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.threadutil.CachedThreadPoolUtility;
import com.vedantu.util.threadutil.ForkJoinPoolUtility;
import com.vedantu.util.threadutil.RecursiveActionForCollection;
import io.swagger.annotations.ApiOperation;
import nl.basjes.parse.useragent.UserAgent;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.codehaus.jackson.map.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vedantu.scheduling.dao.entity.OTFSession.Constants.*;
import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;
import static java.util.stream.Collectors.*;
import static com.vedantu.util.CollectionUtils.getEmptyArrayListIfNull;
import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@Service
public class OTFSessionManager {

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private OTFSessionDAO otfSessionDAO;

    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private GTTSessionRecordingInfoDAO gttSessionRecordingInfoDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private GTMScheduleManager gtmScheduleManager;

    @Autowired
    private OverBookingDAO overBookingDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private GTTOrganizerTokenDAO gttOrganizerTokenDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private OTFSessionTaskManager otfSessionTaskManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    @Qualifier("gttScheduleManager")
    private GTTScheduleManager gttScheduleManager;

    @Autowired
    @Qualifier("gttProScheduleManager")
    private GTTProScheduleManager gttProScheduleManager;

    @Autowired
    @Qualifier("gtt100ScheduleManager")
    private GTT100ScheduleManager gtt100ScheduleManager;

    @Autowired
    @Qualifier("gtwScheduleManager")
    private GTWScheduleManager gtwScheduleManager;

    @Autowired
    @Qualifier("gttWebinarScheduleManager")
    private GTTWebinarScheduleManager gttWebinarScheduleManager;

    @Autowired
    private VedantuWaveScheduleManager vedantuWaveScheduleManager;

    @Autowired
    private WebinarAttendeeDAO webinarAttendeeDAO;

    @Autowired
    private AsyncTaskManager asyncTaskManager;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private UserAgentParser userAgentParser;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private VedantuWaveSessionManager vedantuWaveSessionManager;

    @Autowired
    private CalendarEntryManager calendarEntryManager;

    @Autowired
    private CachedThreadPoolUtility cachedThreadPoolUtility;

    @Autowired
    private ForkJoinPoolUtility forkJoinPoolUtility;

    @Autowired
    protected ITeacherProficiencyManager teacherProficiencyManager;


    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFSessionManager.class);

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
    private final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
    //    private  static  final String FOS_SESSION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT");
    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final String NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String VEDANTUDATA_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("VEDANTUDATA_ENDPOINT");
    private static final Integer MAX_ENROLLMENTS = 500;

    private static final SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
    private static final SimpleDateFormat sdfDateWithMonth = new SimpleDateFormat("MMMM dd yyyy");
    private static final SimpleDateFormat sdfDay = new SimpleDateFormat("E");
    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mma");
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    private static final String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private static long BATCH_TOOL_TYPE_UPDATE_SESSION_WINDOW = DateTimeUtils.MILLIS_PER_DAY;

    private static long GTT_ATTENDEE_CAPTURE_START_TIME = 1491762600000L; // April

    private static final Long sessionUpdateAllowedMin = ConfigUtils.INSTANCE.getLongValue("otfSessionUpdateAllowedMin");
    // 10th
    // 2017
    public final static Long syncPollsDataCronRunTime = 15 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    public final static Long sendPostSessionEmailCronRunTime = Long.valueOf(DateTimeUtils.MILLIS_PER_DAY);
    public final static Long sendNoShowEmailCronRunTime = Long.valueOf(DateTimeUtils.MILLIS_PER_HOUR);
    private static int LATEST_SESSION_CACHE_EXPIRY_MINUTE = 30;
    private static Long TOKEN_EXPIRY_CHECK_MINUTE = 10L * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static Long BUFFER_TIME_TO_SCHEDULE_SIM_LIVE_SESSIONS = 2 * Long.valueOf(DateTimeUtils.MILLIS_PER_HOUR); // 2 hours

    private static final List<OTFSessionToolType> allowedToolTypes = Arrays.asList(OTFSessionToolType.GTT,
            OTFSessionToolType.GTT_PRO, OTFSessionToolType.GTT100, OTFSessionToolType.GTW,
            OTFSessionToolType.GTT_WEBINAR);

    @Autowired
    private DozerBeanMapper mapper;

    public static List<OTFSessionToolType> SUPPORTED_SESSION_TOOL_TYPES = Arrays.asList(OTFSessionToolType.GTT,
            OTFSessionToolType.GTT_PRO, OTFSessionToolType.GTT100, OTFSessionToolType.GTW,
            OTFSessionToolType.GTT_WEBINAR, OTFSessionToolType.VEDANTU_WAVE, OTFSessionToolType.VEDANTU_WAVE_BIG_WHITEBOARD,
            OTFSessionToolType.VEDANTU_WAVE_OTO);

    private static Gson gson = new Gson();

    private static final int MAX_COUNT = 500;

	private static final String JOIN_SESSION_URL = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT") + "session/" ;

	private static final String REPLAY_SESSION_URL = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT") + "session-native-replay/" ;

    public OTFSession getOtfSessionById(String id) {
        return otfSessionDAO.getById(id);
    }

    public OTFSessionPojo addBatchSession(@RequestBody AddBatchSessionReq req) throws Exception {
        // Validate request
        //TODO validate student,presenter,taIds roles.
        //TODO teacher cannot be both taId and presenter
        List<String> errors = req.validate();
        if (!errors.isEmpty()) {
            String errorString = "Invalid request for AddBatchSessionReqError : " + gson.toJson(req) + ". Errors : "
                    + CommonUtils.getListString(errors);
            logger.info(errorString);
            throw new IllegalArgumentException(errorString);
        }

        // Validate Session
        OTFSession otfSession = req.getSession();
        errors = otfSession.validate();
        if (!errors.isEmpty()) {
            String errorString = "Invalid request for AddBatchSessionReqError : " + gson.toJson(req) + ". Errors : "
                    + CommonUtils.getListString(errors);
            logger.info(errorString);
            throw new IllegalArgumentException(errorString);
        }

        // Validate batch
        Map<String, BatchBasicInfo> batchesMap = getBatchBasicInfosMapByIds(otfSession.getBatchIds());
        for (String batchId : otfSession.getBatchIds()) {
            BatchBasicInfo batch = batchesMap.get(batchId);
            if (batch == null) {
                String errorString = "Batch not found for id : " + batchId + ". Request : "
                        + req.toString();
                logger.info(errorString);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
            }

            if (EntityStatus.INACTIVE.equals(batch.getBatchState())) {
                throw new IllegalArgumentException(
                        "Batch is not active : " + batchId + ". Request : " + req.toString());
            }
            //no problem keep resetting in the loop
            if (otfSession.getSessionToolType() == null) {
                otfSession.setSessionToolType(batch.getSessionToolType());
            }
        }

        // Session save
        if (!allowSessionUpdate(otfSession.getStartTime()) || !StringUtils.isEmpty(otfSession.getId())) {
            throw new IllegalArgumentException("Session creation is not allowed for the requested session : "
                    + req.toString()
                    + ". Session creation is only allowed one day before the session start time and no session update is allowed.");
        }

        otfSession.setState(SessionState.SCHEDULED);
        setEntityTypeForSession(otfSession, otfSession.getBatchIds(), otfSession.getType());
        otfSession.setRemarks(req.getReason());
        // session.setCallingUserId(req.getCallingUserId());
        if (!isSlotAvailable(otfSession)) {
            throw new ConflictException(ErrorCode.SEAT_NOT_AVAILABLE, getTimeSlotStringArray(new ArrayList<>(Arrays.asList(otfSession))));
        }
        scheduleSessionByBitSet(otfSession);
        otfSessionDAO.save(otfSession, req.getCallingUserId() != null ? req.getCallingUserId() : null);

        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
            payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
        }
        if (otfSession.getStartTime() != null
                && otfSession.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
            if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", otfSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
                sendToQForGeneratingSessionLinkAndGTT(otfSession);
            }
        }
        if (otfSession.isExtraTypeSession()) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("session", otfSession);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTM_EXTRA_CLASS_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
        }

        // async-- mark calendar for all added sessions
        Map<String, Object> payload = new HashMap<>();
        payload.put("sessions", Arrays.asList(otfSession));
        payload.put("fifoGrpId", otfSession.getId());
        payload.put("mark", true);
        awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS_FIFO, SQSMessageType.INITIATE_ADDED_SESSIONS_MARK_CALENDAR, gson.toJson(payload), otfSession.getId());

        return getSessionInfo(otfSession, null);
    }

    public List<OTFSession> getLatestSession(List<String> batchIds) throws VException {
        List<OTFSession> otfSessions = otfSessionDAO.getLatestSessionForBatchIds(batchIds);
        return otfSessions;
    }

    public List<OTFSessionPojo> getPastSessions(GetOTFSessionsReq sessionsReq) throws VException {
        // Validate request
        sessionsReq.validate();

        List<OTFSessionPojo> results = new ArrayList<>();
        List<GTTAttendeeDetails> gttAttendeeDetails = new ArrayList<>();
        List<OTFSession> otfSessions = new ArrayList<>();
        if (CollectionUtils.isEmpty(sessionsReq.getBatchIds())) {

            // Update batchIds based on course id
            Set<String> batchIds = new HashSet<>();

            // Update batchIds based on student id
            if (sessionsReq.getStudentId() != null
                    && !StringUtils.isEmpty(sessionsReq.getStudentId().toString())) {
                GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
                enrolReq.setStatus(EntityStatus.ACTIVE);
                enrolReq.setUserId(sessionsReq.getStudentId());

                List<EnrollmentPojo> enrollments = getEnrollmentsForUser(enrolReq);
                if (!CollectionUtils.isEmpty(enrollments)) {
                    enrollments.forEach(a -> batchIds.add(a.getBatchId()));
                } else {
                    // For no enrollments return empty list
                    return results;
                }
            }

            sessionsReq.setBatchIds(batchIds);
        }

        if (sessionsReq.getSessionId() != null) {
            OTFSession otfSession = otfSessionDAO.getById(sessionsReq.getSessionId());
            if (otfSession == null) {
                throw new BadRequestException(ErrorCode.SESSION_NOT_FOUND, "SESSION_NOT_FOUND");
            }
            otfSessions.add(otfSession);
        } else {
            otfSessions.addAll(otfSessionDAO.getPastSessions(sessionsReq));
        }

        // Create batch map
        BatchDetailInfoReq req = new BatchDetailInfoReq();
        req.setBatchDataIncludeFields(Arrays.asList(
                BatchBasicInfo.Constants._ID,
                BatchBasicInfo.Constants.COURSE_ID
        ));
        req.setCourseDataIncludeFields(Arrays.asList(
                BatchBasicInfo.Constants._ID,
                BatchBasicInfo.Constants.TITLE
        ));
        Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(otfSessions, req);
        List<OTFSessionPojo> oTFSessionPojos = getSessionInfos(otfSessions, sessionAdditionalInfoMap);

        // Collect all training ids
        Set<String> trainingIds = new HashSet<>();
        Set<String> vedantuWaveSessionIds = new HashSet<>();
        oTFSessionPojos.forEach(s -> {
            if (!StringUtils.isEmpty(s.getMeetingId())
                    && SUPPORTED_SESSION_TOOL_TYPES.contains(s.getSessionToolType())) {
                if (!OTFSessionToolType.VEDANTU_WAVE.equals(s.getSessionToolType())) {
                    trainingIds.add(s.getMeetingId());
                } else {
                    vedantuWaveSessionIds.add(s.getId());
                }
            }
        });

        //fetching the attendees only if the api is called for a studentId else not returning attendeeinfos are there are thousands in number
        if (!ArrayUtils.isEmpty(trainingIds) && sessionsReq.getStudentId() != null) {
            gttAttendeeDetails = gttAttendeeDetailsManager.getAttendeeDetailsByTrainings(trainingIds, sessionsReq.getStudentId().toString());
        }

        //fetching the attendees only if the api is called for a studentId else not returning attendeeinfos are there are thousands in number
        if (!ArrayUtils.isEmpty(vedantuWaveSessionIds) && sessionsReq.getStudentId() != null) {
            logger.info("vedantuWaveSessionIds: " + vedantuWaveSessionIds);
            List<GTTAttendeeDetails> vedantuWaveAttendeeDetails = gttAttendeeDetailsManager.getActiveAttendeeDetailsForSessions(vedantuWaveSessionIds, sessionsReq.getStudentId().toString());
            if (!ArrayUtils.isEmpty(vedantuWaveAttendeeDetails)) {
                gttAttendeeDetails.addAll(vedantuWaveAttendeeDetails);
            }
        }

        Map<String, List<GTTAttendeeDetails>> attendeeListMap = getSessionAttendeeMap(gttAttendeeDetails);
        for (OTFSessionPojo otfSessionPojo : oTFSessionPojos) {
            if (ArrayUtils.isEmpty(otfSessionPojo.getBatchIds())) {
                continue;
            }

            if (ArrayUtils.isEmpty(attendeeListMap.get(otfSessionPojo.getId()))) {
                otfSessionPojo.setAttendeeInfos(new ArrayList<>());
                otfSessionPojo.setAttendees(new ArrayList<>());
                continue;
            }

            List<String> attendees = new ArrayList<>();
            List<GTTAttendeeDetails> attendeeDetailsList = attendeeListMap.get(otfSessionPojo.getId());

            logger.info(attendeeDetailsList);

            List<OTFSessionAttendeeInfo> attendeeInfosList = new ArrayList<>();
            List<String> userIds = new ArrayList<>();
            userIds.add(otfSessionPojo.getPresenter());
            for (GTTAttendeeDetails gTTAttendeeDetails : attendeeDetailsList) {
                OTFSessionAttendeeInfo sessionAttendeeInfo = new OTFSessionAttendeeInfo(gTTAttendeeDetails.getUserId());
                attendees.add(gTTAttendeeDetails.getUserId());
                userIds.add(gTTAttendeeDetails.getUserId());
                if (otfSessionPojo.getSessionToolType() != null && SUPPORTED_SESSION_TOOL_TYPES.contains(otfSessionPojo.getSessionToolType())
                        && !StringUtils.isEmpty(otfSessionPojo.getMeetingId())) {
                    sessionAttendeeInfo.setJoinTimes(gTTAttendeeDetails.getJoinTimes());
                    sessionAttendeeInfo.setTimeInSession(gTTAttendeeDetails.getTimeInSession());
                    sessionAttendeeInfo.setActiveIntervals(gTTAttendeeDetails.getActiveIntervals());
                }
                attendeeInfosList.add(sessionAttendeeInfo);
            }
            Map<String, UserBasicInfo> userBasicInfosMap = fosUtils.getUserBasicInfosMap(userIds, true);
            attendeeInfosList.forEach(attendeeInfo -> {
                String userId = Long.toString(attendeeInfo.getUserId());
                if (userBasicInfosMap.containsKey(userId)) {
                    UserBasicInfo user = userBasicInfosMap.get(userId);
                    attendeeInfo.setFirstName(user.getFirstName());
                    attendeeInfo.setLastName(user.getLastName());
                    attendeeInfo.setContactNumber(user.getContactNumber());
                    attendeeInfo.setPhoneCode(user.getPhoneCode());
                    attendeeInfo.setRole(user.getRole());
                    attendeeInfo.setEmail(user.getEmail());
                }
            });
            otfSessionPojo.setAttendeeInfos(attendeeInfosList);
            otfSessionPojo.setAttendees(attendees);
            otfSessionPojo.setPresenterInfo(userBasicInfosMap.get(otfSessionPojo.getPresenter()));
        }

        return oTFSessionPojos;
    }

    public Map<String, List<GTTAttendeeDetails>> getSessionAttendeeMap(List<GTTAttendeeDetails> attendeeDetailsList) {

        Map<String, List<GTTAttendeeDetails>> resultMap = new HashMap<>();

        for (GTTAttendeeDetails gTTAttendeeDetails : attendeeDetailsList) {
            if (!resultMap.containsKey(gTTAttendeeDetails.getSessionId())) {
                resultMap.put(gTTAttendeeDetails.getSessionId(), new ArrayList<>());
            }
            resultMap.get(gTTAttendeeDetails.getSessionId()).add(gTTAttendeeDetails);
        }

        return resultMap;

    }

    public Map<String, SessionAdditionalInfo> createBatchIdCourseMap(List<OTFSession> oTFSessions) throws VException {
        return createBatchIdCourseMap(oTFSessions, null);
    }

    public Map<String, SessionAdditionalInfo> createBatchIdCourseMap(List<OTFSession> oTFSessions,
            BatchDetailInfoReq req) throws VException {
        Map<String, SessionAdditionalInfo> batchCourseMap = new HashMap<>();

        // Get batch ids
        Set<String> batchIds = new HashSet<>();

        if (oTFSessions != null) {
            oTFSessions.stream()
                    .filter(s -> s != null)
                    .filter(s -> ArrayUtils.isNotEmpty(s.getBatchIds()))
                    .forEach(s -> batchIds.addAll(s.getBatchIds()));
        }
        if (!CollectionUtils.isEmpty(batchIds)) {

            // Fetch batches and create batch map
            if (req == null) {
                req = new BatchDetailInfoReq(new ArrayList<>(batchIds));
                req.setBatchDataIncludeFields(Arrays.asList(
                        BatchBasicInfo.Constants._ID,
                        BatchBasicInfo.Constants.COURSE_ID,
                        BatchBasicInfo.Constants.GROUP_NAME
                ));
                req.setCourseDataIncludeFields(Arrays.asList(
                        BatchBasicInfo.Constants._ID,
                        BatchBasicInfo.Constants.TITLE,
                        BatchBasicInfo.Constants.GRADES
                ));
            } else {
                req.setBatchIds(new ArrayList<>(batchIds));
            }
            logger.info("req file createBatchIdCourseMap: " + req);
            List<BatchBasicInfo> batches = getDetailedBatchInfo(req);
            batches.forEach(b -> {
                SessionAdditionalInfo sessionAdditionalInfo = new SessionAdditionalInfo(b.getCourseInfo(),
                        b.getGroupName());
                batchCourseMap.put(b.getId(), sessionAdditionalInfo);
            });
        }
        return batchCourseMap;
    }

    public List<OTFSession> getEndedSessions(Long afterEndTime, Long beforeEndTime, Integer start, Integer limit) {
        logger.info("getEndedSessions - " + afterEndTime + " start : " + start + " limit : " + limit);
        Query query = new Query();
        if (beforeEndTime == null || beforeEndTime <= 0L) {
            beforeEndTime = System.currentTimeMillis();
        }

        if (afterEndTime != null && afterEndTime > 0L) {
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lte(beforeEndTime));
        } else {
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(beforeEndTime));
        }

        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        otfSessionDAO.setFetchParameters(query, start, limit);
        List<OTFSession> results = otfSessionDAO.runQuery(query, OTFSession.class);
        logger.info("getEndedSessions - " + (results == null ? 0 : results.size()));
        return results;
    }

    public long getTotalSessionDuration(Long afterEndTime, Long beforeEndTime) throws VException {
        if (afterEndTime != null && beforeEndTime != null && beforeEndTime < afterEndTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Invalid request params. before end time is less than after end time");
        }
        return getTotalSessionDurationPreGTTAttendeeCapture(afterEndTime, beforeEndTime)
                + getTotalSessionDurationPostGTTAttendeeCapture(afterEndTime, beforeEndTime);
    }

    public long getTotalSessionDurationPreGTTAttendeeCapture(Long afterEndTime, Long beforeEndTime) throws VException {
        logger.info("Request - afterEndTime:" + afterEndTime + " beforeEndTime:" + beforeEndTime);

        long totalHourCount = 0L;
        // Only calculates till gtt attendee capture start time
        if (beforeEndTime > GTT_ATTENDEE_CAPTURE_START_TIME) {
            if (afterEndTime > GTT_ATTENDEE_CAPTURE_START_TIME) {
                return totalHourCount;
            } else {
                beforeEndTime = GTT_ATTENDEE_CAPTURE_START_TIME;
            }
        }

        Long duration = 0L;
        for (int start = 0, limit = 1000;; start += limit) {
            List<OTFSession> results = getEndedSessions(afterEndTime, beforeEndTime, start, limit);
            if (CollectionUtils.isEmpty(results)) {
                break;
            }

            Map<String, Integer> batchEnrollmentCountStore = new HashMap<>();

            for (OTFSession oTFSession : results) {
                try {
                    int attendeesSize = 0;
                    if (!CollectionUtils.isEmpty(oTFSession.getAttendees())) {
                        attendeesSize = oTFSession.getAttendees().size();
                    } else if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {
                        for (String batchId : oTFSession.getBatchIds()) {
                            if (batchEnrollmentCountStore.containsKey(batchId)) {
                                attendeesSize = batchEnrollmentCountStore.get(batchId);
                            } else {
                                String url = SUBSCRIPTION_ENDPOINT + "enroll/getEnrollmentCountInBatch?batchId=" + batchId;
                                logger.info("URL = " + url);
                                ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                                String jsonString = response.getEntity(String.class);
                                Long enrollmentCount = gson.fromJson(jsonString, Long.class);
                                if (enrollmentCount != null) {
                                    attendeesSize = enrollmentCount.intValue();
                                    batchEnrollmentCountStore.put(batchId, enrollmentCount.intValue());
                                }
                            }
                        }
                        // batchMap.put(batch.getId(), batch);
                    }
                    duration += (attendeesSize * (oTFSession.getEndTime() - oTFSession.getStartTime()));
                } catch (Exception ex) {
                    logger.info("getTotalSessionDurationPreGTTAttendeeCapture processing failed for session - "
                            + oTFSession.getId() + " ex " + ex.getMessage());
                }
            }
        }

        logger.info("getEndedSessionDuration - " + duration);
        return duration;
    }

    public long getTotalSessionDurationPostGTTAttendeeCapture(Long afterEndTime, Long beforeEndTime) {
        logger.info("Request - afterEndTime:" + afterEndTime + " beforeEndTime:" + beforeEndTime);

        long totalHourCount = 0L;
        // Only calculates from gtt attendee capture start time
        if (afterEndTime < GTT_ATTENDEE_CAPTURE_START_TIME) {
            if (beforeEndTime < GTT_ATTENDEE_CAPTURE_START_TIME) {
                return totalHourCount;
            } else {
                afterEndTime = GTT_ATTENDEE_CAPTURE_START_TIME;
            }
        }

        int start = 0;
        int limit = AbstractMongoDAO.DEFAULT_FETCH_SIZE;

        for (;; start += limit) {
            Query sessionQuery = new Query();
            sessionQuery
                    .addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(afterEndTime).lte(beforeEndTime));
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).in(allowedToolTypes));
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES + ".0").exists(true));
            sessionQuery.fields().include(OTFSession.Constants.START_TIME);
            sessionQuery.fields().include(OTFSession.Constants.END_TIME);
            sessionQuery.fields().include(OTFSession.Constants.MEETING_ID);
            sessionQuery.fields().include(OTFSession.Constants.ATTENDEES);
            otfSessionDAO.setFetchParameters(sessionQuery, start, limit);

            List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
            if (CollectionUtils.isEmpty(oTFSessions)) {
                break;
            }

            Map<String, Long> durationMap = new HashMap<>();
            Set<String> trainingIds = new HashSet<>();
            Set<String> attendeeIds = new HashSet<>();
            for (OTFSession oTFSession : oTFSessions) {
                durationMap.put(oTFSession.getId(), (oTFSession.getEndTime() - oTFSession.getStartTime()));
                trainingIds.add(oTFSession.getMeetingId());
                attendeeIds.addAll(oTFSession.getAttendees());
            }

            Query gttAttendeeQuery = new Query();
            gttAttendeeQuery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).in(trainingIds));
            gttAttendeeQuery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(attendeeIds));
            gttAttendeeQuery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TIME_IN_SESSION).gt(0L));
            gttAttendeeQuery.fields().include(GTTAttendeeDetails.Constants.SESSION_ID);
            gttAttendeeQuery.limit(Integer.MAX_VALUE);
            List<GTTAttendeeDetails> gttAttendeeDetailsList = gttAttendeeDetailsDAO.runQuery(gttAttendeeQuery,
                    GTTAttendeeDetails.class);

            for (GTTAttendeeDetails entry : gttAttendeeDetailsList) {
                if (durationMap.containsKey(entry.getSessionId())) {
                    totalHourCount += durationMap.get(entry.getSessionId());
                }
            }
        }

        logger.info("totalHourCount between afterEndTIme:" + afterEndTime + " beforeEndTime:" + beforeEndTime
                + " value:" + totalHourCount);
        return totalHourCount;
    }

    public OTFSessionPojo createSessionLink(String id) throws VException {
        logger.info("session id for creating gtt {}", id);
        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id is missing");
        }

        OTFSession oTFSession = otfSessionDAO.getById(id);
        logger.info("session {}", oTFSession);
        if (oTFSession == null) {
            String errorMessage = "Session does not exist for req : " + id;
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        List<CourseBasicInfo> courseBasicInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {

            Map<String, BatchBasicInfo> batchesMap = getBatchBasicInfosMapByIds(oTFSession.getBatchIds());
            logger.info("Batch Map of Session {}", batchesMap);
            for (String batchId : oTFSession.getBatchIds()) {
                BatchBasicInfo batch = batchesMap.get(batchId);
                logger.info("Batch for session {}", batch);
                if (batch == null) {
                    String errorString = "Batch not found for id : " + batchId;
                    logger.info(errorString);
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
                }

                if (EntityStatus.INACTIVE.equals(batch.getBatchState())) {
                    throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,
                            "Batch is not active : " + batch.toString());
                }
                courseBasicInfos.add(batch.getCourseInfo());
            }
        }
        if (StringUtils.isEmpty(oTFSession.getOrganizerAccessToken())) {
            logger.info("Organizer Token is Empty for SessionId: " + oTFSession.getId());
            scheduleSessionByBitSet(oTFSession);
            otfSessionDAO.save(oTFSession);
            logger.info("Organizer Token set to " + oTFSession.getMeetingId() + " for SessionId " + oTFSession.getId());
        }

        if (StringUtils.isEmpty(oTFSession.getMeetingId()) || StringUtils.isEmpty(oTFSession.getSessionURL())) {
            logger.info("creating gtt for session cron {}", id);
            createSessionLink(oTFSession);
        }

        // Map<String, UserBasicInfo> userBasicInfos =
        // fosUtils.getUserBasicInfosMap(course.getTeacherIds());
        OTFSessionPojo oTFSessionPojo = getSessionInfo(oTFSession, courseBasicInfos);
        return oTFSessionPojo;
    }

    public void createSessionLinkAndRegisterGTT(String sessionId) throws VException {
        OTFSession session = otfSessionDAO.getById(sessionId);
        createSessionLink(session);
    }

    public void createSessionLink(OTFSession otfSession)
            throws InternalServerErrorException, BadRequestException, ConflictException {
        logger.info("creating links for session = " + otfSession);
        ScheduleManager scheduleManager = getScheduleManagerByToolType(otfSession);
        if (scheduleManager == null) {
            return;
        }
        try {
            // Validate the timings
            if(!otfSession.isEarlyLearningSession() && !otfSession.isWebinarSession()) {
                if (otfSession.getStartTime() < System.currentTimeMillis()
                        || otfSession.getEndTime() < System.currentTimeMillis()) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session timing is in past");
                }
            }

            if (StringUtils.isEmpty(otfSession.getOrganizerAccessToken())) {
                logger.info("scheduling - bit set calc");
                scheduleSessionByBitSet(otfSession);
            }
            // Create session links
            scheduleManager.createSessionLink(otfSession);
            otfSessionDAO.save(otfSession);

            if(otfSession.isEarlyLearningSession() && otfSession.isWebinarSession()) {
                String studentId = otfSession.getAttendees().get(0);
                SessionLabel earlyLearningCourseType = otfSession.getLabels().contains(SessionLabel.SUPER_CODER)?SessionLabel.SUPER_CODER:SessionLabel.SUPER_READER;
                ELTeachersProficiencies proficiencyIds = teacherProficiencyManager
                        .getTeacherProficiencies(earlyLearningCourseType);
                OverBooking earlyLearningBooking = overBookingDAO.getEarlyLearningBookingByStudentId(studentId, earlyLearningCourseType);
                if (earlyLearningBooking != null) {
                    earlyLearningBooking.setSessionId(otfSession.getId());
                    earlyLearningBooking.setIsTeacherAssigned(OverBookingStatus.YES);
                    earlyLearningBooking.setTeacherProficiency(proficiencyIds.getTeacherProficiencyType(otfSession.getPresenter()));
                    overBookingDAO.save(earlyLearningBooking);
                }
            }

            // Register sessions if required
            try {
                logger.info("registering gtt");
                Map<String, String> map = new HashMap<>();
                map.put("sessionId", otfSession.getId());
                awsSQSManager.sendToSQS(SQSQueue.REGISTER_GTT_QUEUE, SQSMessageType.REGISTER_GTT_FOR_SESSION, gson.toJson(map));
//                registerAttendees(oTFSession, null);
            } catch (Exception ex) {
                logger.info("Registration failed for session : " + otfSession.toString() + " ex: " + ex.toString(), ex);
            }
        } catch (Exception ex) {
            if (ex instanceof VException) {
                VException exception = (VException) ex;
                if (ErrorCode.OTF_SESSION_ALREADY_EXISTS.equals(exception.getErrorCode())) {
                    logger.warn("OTF_SESSION_ALREADY_EXISTS ", ex);
                } else {
                    logger.error("Error occured generating link. ex : " + ex.toString() + " session : " + otfSession.getId(), ex);
                }
            }else{
                logger.error("Error occured generating link. ex : " + ex.toString() + " session : " + otfSession.getId(), ex);
            }
        }
    }

    public String joinSession(OTFJoinSessionReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException, ConflictException {
        OTFSession oTFSession = otfSessionDAO.getById(req.getSessionId());
        if (oTFSession == null) {
            String errorMessage = "session not found with id" + req.getSessionId() + " req:" + req.toString();
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        // Check if training is scheduled
        if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
            throw new BadRequestException(ErrorCode.OTF_MEETING_NOT_SCHEDULED,
                    "Meeting not scheduled for the session. Session can be joined only for sessions with meeting scheduled");
        }

        // TODO : Need to consider the network call delay in this request and
        // come up
        // with some time interval
        if (oTFSession.getEndTime() < System.currentTimeMillis()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Session end time is in the past for session:" + oTFSession.toString());
        }

        ScheduleManager scheduleManager = getScheduleManagerByToolType(oTFSession);
        if (scheduleManager == null) {
            throw new ConflictException(ErrorCode.NO_SCHEDULE_MANAGER_FOUND, "NO_SCHEDULE_MANAGER_FOUND");
        }
        String joinUrl = "";
        switch (req.getRole()) {
            case TEACHER:
                // Validate teacherId
                if (!oTFSession.getPresenter().equals(req.getUserId())) {
                    throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                            "Action not allowed for this user. teacher id mismatch");
                }

                joinUrl = scheduleManager.getTeacherJoinUrl(oTFSession, req.getUserId());
                if (StringUtils.isEmpty(joinUrl)) {
                    logger.info("Teacher join url not found : " + oTFSession.toString() + " userId : " + req.getUserId());
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
                            "Error generating teacher link. Kindly retry");
                }
                logger.info("Presenter Url for session : " + req.getSessionId() + " is " + joinUrl);

                // Update session
                oTFSession.setPresenterUrl(joinUrl);
                if (!Boolean.TRUE.equals(oTFSession.getTeacherJoined())) {
                    oTFSession.setTeacherJoined(true);
                    oTFSession.setTeacherJoinTime(System.currentTimeMillis());
                }
                otfSessionDAO.save(oTFSession);
                break;
            case ADMIN:
            case STUDENT_CARE:
            case STUDENT:
                joinUrl = scheduleManager.getStudentJoinUrl(oTFSession, req.getUserId());
                if (StringUtils.isEmpty(joinUrl)) {
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
                            "Error generating student url : " + oTFSession.toString());
                }

                if (!StringUtils.isEmpty(req.getUserId())) {
                    List<String> attendees = oTFSession.getAttendees();
                    if (attendees == null) {
                        attendees = new ArrayList<String>();
                    }

                    if (!Boolean.TRUE.equals(req.getAdmin()) && !attendees.contains(
                            req.getUserId())) {
                        attendees.add(req.getUserId());
                        oTFSession.setAttendees(attendees);
                        otfSessionDAO.save(oTFSession);
                    }
                }
                break;
            default:
                break;
        }

        // Capture joining times
        if (!Boolean.TRUE.equals(req.getAdmin()) && oTFSession.getSessionToolType() != null
                && SUPPORTED_SESSION_TOOL_TYPES.contains(oTFSession.getSessionToolType())
                && oTFSession.getStartTime() > (System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR)) {
            scheduleManager.userJoined(oTFSession, req.getUserId());
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("sessionId", oTFSession.getId());
        payload.put("userId", req.getUserId());
        payload.put("ipAddress", req.getIpAddress());
        payload.put("userAgent", req.getUserAgent());
        // AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ADD_JOIN_DETAILS, payload);
        // asyncTaskFactory.executeTask(params);

        String msgGroupId = req.getUserId();
        awsSQSManager.sendToSQS(SQSQueue.GTT_UPDATE_OPS_FIFO, SQSMessageType.ADD_JOIN_DETAILS, gson.toJson(payload), msgGroupId);


        return joinUrl;
    }

    public List<OTFSessionPojo> getOtfSessions(Long afterStartTime, Long beforeStartTime, Long afterEndTime, String teacherId, String studentId) throws VException {
        List<OTFSession> oTFSessions = new ArrayList<>();
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<>();
        if (StringUtils.isEmpty(studentId) && StringUtils.isEmpty(teacherId)
                && (afterStartTime == null || afterStartTime <= 0l)) {
            return oTFSessionPojos;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

        if (!StringUtils.isEmpty(studentId) || !StringUtils.isEmpty(teacherId)) {
            Set<String> batchIds = new HashSet<>();
            GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
            enrolReq.setStatus(EntityStatus.ACTIVE);
            if (!StringUtils.isEmpty(studentId)) {
                enrolReq.setUserId(Long.parseLong(studentId));
            } else {
                enrolReq.setUserId(Long.parseLong(teacherId));
            }

            List<EnrollmentPojo> enrollments = getEnrollmentsForUser(enrolReq);
            if (!CollectionUtils.isEmpty(enrollments)) {
                enrollments.forEach(a -> batchIds.add(a.getBatchId()));
            } else {
                // For no enrollments return empty list
                return oTFSessionPojos;
            }
            query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
            if (afterEndTime != null && afterEndTime > 0l) {
                query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime));
            }
        }

        if (afterStartTime != null && afterStartTime > 0l) {
            if (beforeStartTime == null || beforeStartTime <= 0l) {
                beforeStartTime = afterStartTime + 300000;
            }
            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime).lt(beforeStartTime));
        }

        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).exists(true).ne(null));
        logger.info("session query : " + query);
        oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        logger.info("session pre processing : " + oTFSessions == null ? 0 : oTFSessions.size());
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap =
                    createBatchIdCourseMap(oTFSessions);
            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);
        }

        logger.info("session post processing : " + oTFSessionPojos == null ? 0 : oTFSessionPojos.size());
        return oTFSessionPojos;
    }

    public void addJoinDetails(String oTFSessionId, String userId, String ipAddress, String userAgent) {

        OTFSession oTFSession = otfSessionDAO.getById(oTFSessionId);
        if (oTFSession == null) {
            logger.error("OTF session not found " + oTFSessionId);
            return;
        }
        GTTAttendeeDetails attendeeDetails = gttAttendeeDetailsManager.getAttendeeDetails(oTFSession.getId(),
                oTFSession.getMeetingId(), userId);
        if (attendeeDetails == null) {
            logger.warn("Updating join details but no attendeeDetails found for " + oTFSession.getId() + ", userId " + userId);
        } else {
            try {
                ipAddress = ipAddress.split(", ")[0]; // can be more than 1 IP
                LocationInfo locationInfo = null;
                try {
                    locationInfo = ipUtil.getLocationFromIp(ipAddress);
                } catch (Exception e) {
                    logger.warn(e.getMessage(), e);
                }
                attendeeDetails.setLocationInfo(locationInfo);
                if (StringUtils.isNotEmpty(userAgent)) {
                    UserAgent details = userAgentParser.getUserAgentDetails(userAgent);
                    DeviceDetails deviceDetails = new DeviceDetails();
                    if (StringUtils.isNotEmpty(details.getValue("DeviceClass"))) {
                        deviceDetails.setDeviceType(DeviceType.valueOfKey(details.getValue("DeviceClass")));
                    }
                    if (StringUtils.isNotEmpty(details.getValue("DeviceName"))) {
                        deviceDetails.setDeviceName(details.getValue("DeviceName"));
                    }

                    if (StringUtils.isNotEmpty(details.getValue("DeviceBrand"))) {
                        deviceDetails.setDeviceBrand(details.getValue("DeviceBrand"));
                    }
                    if (StringUtils.isNotEmpty(details.getValue("AgentName"))) {
                        deviceDetails.setBrowserName(details.getValue("AgentName"));
                    }
                    if (StringUtils.isNotEmpty(details.getValue("AgentVersion"))) {
                        deviceDetails.setBrowserVersion(details.getValue("AgentVersion"));
                    }
                    if (StringUtils.isNotEmpty(details.getValue("AgentVersionMajor"))) {
                        deviceDetails.setBrowserVersionMajor(details.getValue("AgentVersionMajor"));
                    }
                    attendeeDetails.setDeviceDetails(deviceDetails);
                }
                logger.info("updating the details " + attendeeDetails);
                gttAttendeeDetailsDAO.update(attendeeDetails);
                // todo enable for gamification
                // gttAttendeeDetailsManager.processGttAttendeeForGameAsync(attendeeDetails);
            } catch (Exception e) {
                logger.error("Error in updating join details " + e.getMessage());
            }
        }
    }

    public void scheduleSessionByBitSet(OTFSession otfSession)
            throws InternalServerErrorException, BadRequestException {
        logger.info("Trying to schedule session for " + otfSession);
        ScheduleManager scheduleManager = getScheduleManagerByToolType(otfSession);
        int slot = scheduleManager.scheduleAvailableSlot(otfSession.getStartTime(), otfSession.getEndTime());
        if (slot != -1) {
            logger.info("available slot for startTime = " + otfSession.getStartTime() + " is: " + slot);
            otfSession.setOrganizerAccessToken(scheduleManager.getAccessToken(slot));
            if (otfSession.isOtoSession()) {
                otfSession.setSessionURL(null);
            }
        } else {
            logger.info("slot not available for startTime = " + otfSession.getStartTime());
        }
    }

    public boolean isSlotAvailable(OTFSession oTFSession)
            throws InternalServerErrorException, BadRequestException {
        logger.info("Trying to check if schedule available for session " + oTFSession);
        ScheduleManager scheduleManager = getScheduleManagerByToolType(oTFSession);
        if (scheduleManager == null) {
            return true;
        }
        return scheduleManager.isSlotAvailable(oTFSession.getStartTime(), oTFSession.getEndTime());
    }

    public String getTimeSlotStringArray(List<OTFSession> sessions)
            throws InternalServerErrorException, BadRequestException {
        List<Map<String, Long>> res = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSession session : sessions) {
                Map<String, Long> map = new HashMap<>();
                map.put("startTime", session.getStartTime());
                map.put("endTime", session.getEndTime());
                res.add(map);
            }
        }
        return res.toString();
    }

    public void descheduleSessionByBitSet(OTFSession oTFSession)
            throws InternalServerErrorException, BadRequestException {
        logger.info("descheduling session " + oTFSession);
        ScheduleManager scheduleManager = getScheduleManagerByToolType(oTFSession);
        if (scheduleManager == null) {
            return;
        }
        scheduleManager.descheduleSlot(oTFSession.getStartTime(), oTFSession.getEndTime(),
                scheduleManager.getAccessTokenSlot(oTFSession.getOrganizerAccessToken()));
        oTFSession.setOrganizerAccessToken(null);
        oTFSession.setSessionURL(null);
        oTFSession.setPresenterUrl(null);
        if (!StringUtils.isEmpty(oTFSession.getOrganizerAccessToken())) {
            logger.info("DeschedulingFailed for organzier token : " + oTFSession.getOrganizerAccessToken()
                    + " session : " + oTFSession);
        }
    }

    public void updateBatchToolType(String batchId, OTFSessionToolType sessionToolType) {
        logger.info("batchId : " + batchId + " sessionToolType:" + sessionToolType);
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME)
                .gt(System.currentTimeMillis() + BATCH_TOOL_TYPE_UPDATE_SESSION_WINDOW));

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                if (!sessionToolType.equals(oTFSession.getSessionToolType())) {
                    try {
                        // Deschedule session
                        descheduleSessionByBitSet(oTFSession);
                        otfSessionDAO.save(oTFSession);
                        logger.info("Session descheduled:" + oTFSession.toString());

                        // Schedule session
                        oTFSession.setSessionToolType(sessionToolType);
                        scheduleSessionByBitSet(oTFSession);
                        otfSessionDAO.save(oTFSession);
                        logger.info("Session scheduled:" + oTFSession.toString());
                    } catch (Exception ex) {
                        logger.info("Error updating the session tool type for session : " + oTFSession.toString()
                                + " sessionToolType:" + sessionToolType + " ex:" + ex.getMessage());
                    }
                }
            }
        }
    }

    private ScheduleManager getScheduleManagerByToolType(OTFSession otfSession)
            throws InternalServerErrorException, BadRequestException {
        OTFSessionToolType sessionToolType = getSessionToolType(otfSession);
        if (sessionToolType == null) {
            sessionToolType = OTFSessionToolType.GTM;
        }

        if (sessionToolType == OTFSessionToolType.GTM) {
            return gtmScheduleManager;
        } else if (sessionToolType == OTFSessionToolType.GTT) {
            return gttScheduleManager;
        } else if (sessionToolType == OTFSessionToolType.GTT_PRO) {
            return gttProScheduleManager;
        } else if (sessionToolType == OTFSessionToolType.GTT100) {
            return gtt100ScheduleManager;
        } else if (sessionToolType == OTFSessionToolType.GTW) {
            return gtwScheduleManager;
        } else if (sessionToolType == OTFSessionToolType.GTT_WEBINAR) {
            return gttWebinarScheduleManager;
        } else if (otfSession.hasVedantuWaveFeatures()) {
            return vedantuWaveScheduleManager;
        }
        String errorMessage = "Session tool type not supported : " + otfSession.toString();
        logger.error(errorMessage);
        throw new InternalServerErrorException(ErrorCode.OTF_UNSUPPORTED_SESSION_TOOL, errorMessage);
    }

    public List<GTTAttendeeDetails> registerAttendees(String sessionId, String userId) throws VException {
        logger.info("registering gtt for session id {} and user id {}", sessionId, userId);
        OTFSession otfSession = otfSessionDAO.getById(sessionId);
        if (otfSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Session not found : " + sessionId + " userId : " + userId);
        }
        return registerAttendees(otfSession, userId);
    }

    public List<GTTAttendeeDetails> registerAttendees(OTFSession otfSession, String userId) throws VException {
        return registerAttendees(otfSession, userId, null);
    }

    public List<GTTAttendeeDetails> registerAttendees(OTFSession otfSession, String userId, List<EnrollmentPojo> enrollmentPojos) throws VException {

        OTFSessionToolType sessionToolType = getSessionToolType(otfSession);
        if (sessionToolType == null) {
            throw new NotFoundException(ErrorCode.OTF_UNSUPPORTED_SESSION_TOOL,
                    "sessionToolType is null for the session");
        }

        List<GTTAttendeeDetails> attendeeDetails = new ArrayList<>();
        Map<String, List<EnrollmentPojo>> studentEnrollmentMap = new HashMap<>();
        if (sessionToolType == OTFSessionToolType.GTW || sessionToolType == OTFSessionToolType.GTT100 || sessionToolType == OTFSessionToolType.GTT || sessionToolType == OTFSessionToolType.GTT_WEBINAR) {
            if (StringUtils.isEmpty(otfSession.getMeetingId())) {
                throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED,
                        "meeting not scheduled for the session");
            }

            ScheduleManager scheduleManager = getScheduleManagerByToolType(otfSession);

            if (StringUtils.isEmpty(userId) || "null".equals(userId)) {
                if (ArrayUtils.isNotEmpty(otfSession.getBatchIds())) {
                    List<EnrollmentPojo> enrollments = getBatchIdsActiveEnrollments(otfSession.getBatchIds(), false);
                    if (!CollectionUtils.isEmpty(enrollments)) {
                        studentEnrollmentMap = enrollments.stream().collect(groupingBy(EnrollmentPojo::getUserId));
                        List<String> enrolledStudents = new ArrayList<>();
                        enrolledStudents.addAll(studentEnrollmentMap.keySet());
                        otfSession.setAttendees(enrolledStudents);
                    }
                }
                switch (sessionToolType) {
                    case GTT_WEBINAR:
                        attendeeDetails.addAll(scheduleManager.createGTTAttendeeDetails(otfSession, studentEnrollmentMap, null));
                        return attendeeDetails;
                }

                attendeeDetails.addAll(scheduleManager.registerAttendees(otfSession, studentEnrollmentMap));

                // Register teacher as an attendee
                attendeeDetails.add(scheduleManager.registerAttendee(otfSession, otfSession.getPresenter(), null));
            } else {
                attendeeDetails.add(scheduleManager.registerAttendee(otfSession, userId, enrollmentPojos));
            }
            return attendeeDetails;
        } else if (sessionToolType == OTFSessionToolType.GTT_PRO) {// Removing pre registration
            return new ArrayList<>();
        } else if (sessionToolType == OTFSessionToolType.GTM) {
            throw new BadRequestException(ErrorCode.OTF_SESSION_TOOL_REGISTRATION_NOT_SUPPORTED,
                    "Attendee registration not supported for tool : " + sessionToolType + " session:"
                            + otfSession.toString());
        } else if (otfSession.hasVedantuWaveFeatures()) {
            logger.info("registering wave gtt");
            if (StringUtils.isEmpty(userId) || "null".equals(userId)) {
                if (ArrayUtils.isNotEmpty(otfSession.getBatchIds())) {
                    List<EnrollmentPojo> enrollments = Optional.ofNullable(getBatchIdsActiveEnrollments(otfSession.getBatchIds(), false)).orElseGet(ArrayList::new);
                    logger.info("enrolled user in batch {}", enrollments.stream().map(EnrollmentPojo::getUserId).collect(joining(",")));
                    if (!CollectionUtils.isEmpty(enrollments)) {
                            /*studentEnrollmentMap = enrollments.stream().collect(Collectors.groupingBy(EnrollmentPojo::getUserId));
                            List<String> enrolledStudents = new ArrayList<>();
                            enrolledStudents.addAll(studentEnrollmentMap.keySet());
                            otfSession.setAttendees(enrolledStudents);*/

                        enrollments.sort(Comparator.comparing(EnrollmentPojo::getUserId));
                        List<List<EnrollmentPojo>> lists = Lists.partition(enrollments, 30);
                        for (List<EnrollmentPojo> list : lists) {
                            try {
                                SessionEnrollmentsPojo pojo = new SessionEnrollmentsPojo(otfSession, list);
                                awsSQSManager.sendToSQS(SQSQueue.REGISTER_GTT_QUEUE, SQSMessageType.REGISTER_GTT_FOR_USER_WITH_WAVE_SESSION, gson.toJson(pojo));
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                                for (EnrollmentPojo pojo : list) {
                                    attendeeDetails.add(vedantuWaveScheduleManager.registerAttendee(otfSession, pojo.getUserId(), Collections.singletonList(pojo)));
                                }
                            }
                        }
                    }
                }

                // Register teacher as an attendee
                attendeeDetails.add(vedantuWaveScheduleManager.registerAttendee(otfSession, otfSession.getPresenter(), null));
                if (!CollectionUtils.isEmpty(otfSession.getTaIds())) {
                    for (Long taId : otfSession.getTaIds()) {
                        if (taId != null) {
                            attendeeDetails.add(vedantuWaveScheduleManager.registerAttendee(otfSession, taId.toString(), null));
                        }
                    }
                }

            } else {
                attendeeDetails.add(vedantuWaveScheduleManager.registerAttendee(otfSession, userId, enrollmentPojos));
            }
            return attendeeDetails;
        }

        throw new BadRequestException(ErrorCode.OTF_UNSUPPORTED_SESSION_TOOL,
                "Attendee registration not supported for tool : " + sessionToolType + " session:"
                        + otfSession.toString());
    }

    private OTFSessionToolType getSessionToolType(OTFSession oTFSession) throws BadRequestException {
        return oTFSession.getSessionToolType();
    }

    public List<OTFSession> getSessionsBasicInfos(Long afterEndTime, Long beforeEndTime) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        if (afterEndTime != null && afterEndTime > 0L) {
            if (beforeEndTime == null || beforeEndTime <= 0L) {
                beforeEndTime = afterEndTime + 300000;
            }
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
        }
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).exists(true).ne(null));
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        // making a query for batches to get attendees
        Set<String> batchIds = new HashSet<>();
        for (OTFSession oTFSession : oTFSessions) {
            batchIds.addAll(oTFSession.getBatchIds());
        }
        if (batchIds.size() > 0) {
            Map<String, BatchEnrolmentInfo> batchMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(batchIds)) {
                List<BatchEnrolmentInfo> batches = otfSessionTaskManager.getBatchEnrolmentsByIds(batchIds);
                if (ArrayUtils.isNotEmpty(batches)) {
                    batchMap = batches.stream().collect(Collectors.toMap(BatchEnrolmentInfo::getBatchId, c -> c));
                }
            }
            for (OTFSession oTFSession : oTFSessions) {
                if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {
                    for (String batchId : oTFSession.getBatchIds()) {
                        BatchEnrolmentInfo b = batchMap.get(batchId);
                        if (b != null && ArrayUtils.isNotEmpty(b.getEnrolledStudentIds())) {
                            oTFSession.addAttendees(b.getEnrolledStudentIds());
                        }
                    }
                }
            }
        }
        return oTFSessions;
    }

    public List<OTFSessionPojo> getUserUpcomingScheduledSessions(Long startTime, Long endTime, String teacherId,
            String studentId, Integer start, Integer limit) throws VException {
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<>();
        if (StringUtils.isEmpty(studentId) && StringUtils.isEmpty(teacherId)) {
            return oTFSessionPojos;
        }

        // Fetch all the active batches
        Set<String> batchIds = new HashSet<>();
        if (!StringUtils.isEmpty(studentId)) {
            GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
            enrolReq.setStatus(EntityStatus.ACTIVE);
            enrolReq.setUserId(Long.parseLong(studentId));

            List<EnrollmentPojo> enrollments = getEnrollmentsForUser(enrolReq);
            if (!CollectionUtils.isEmpty(enrollments)) {
                enrollments.forEach(a -> batchIds.add(a.getBatchId()));
            } else {
                // For no enrollments return empty list
                return oTFSessionPojos;
            }
        }

        Query sessionQuery = new Query();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        }
        // sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_ID).in(batchMap.keySet()));
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        if (!StringUtils.isEmpty(teacherId)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(teacherId));
        }
        if (startTime != null && startTime > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(startTime));
        }
        if (endTime != null && endTime > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(endTime));
        }

        if (start != null && limit != null && limit > 0) {
            sessionQuery.skip(start);
            sessionQuery.limit(limit);
        }
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        logger.info("Sessions:" + (oTFSessions == null ? 0 : oTFSessions.size()));

        // Get webinar sessions
        GetOTFWebinarSessionsReq getOTFWebinarSessionsReq = new GetOTFWebinarSessionsReq();
        if (!StringUtils.isEmpty(teacherId)) {
            getOTFWebinarSessionsReq.setTeacherId(teacherId);
        }
        if (!StringUtils.isEmpty(studentId)) {
            getOTFWebinarSessionsReq.setStudentId(studentId);
        }
        if (startTime != null && startTime > 0L) {
            getOTFWebinarSessionsReq.setAfterStartTime(startTime);
        }
        if (endTime != null && endTime > 0L) {
            getOTFWebinarSessionsReq.setBeforeEndTime(endTime);
        }
        getOTFWebinarSessionsReq.addSessionStates(SessionState.SCHEDULED);
        List<OTFSession> webinarSessions = getWebinarSessions(getOTFWebinarSessionsReq, true);
        if (!CollectionUtils.isEmpty(webinarSessions)) {
            if (oTFSessions == null) {
                oTFSessions = webinarSessions;
            } else {
                oTFSessions.addAll(webinarSessions);
            }
        }

        // Create session infos
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                oTFSessionPojos.add(getSessionInfo(oTFSession, null));
            }
        }
        return oTFSessionPojos;
    }

    public List<OTFSessionPojo> getPastSessions(String callingUserId, Role callingUserRole, String query, Integer start,
                                                Integer size, String batchId, Long startTime, Long endTime, Set<String> batchIds) throws VException {
        return getPastSessions(callingUserId, callingUserRole, query, start, size, batchId, true, startTime, endTime, batchIds);
    }

    public List<OTFSessionPojo> getPastSessions(String callingUserId, Role callingUserRole, String query, Integer start,
                                                Integer size, String batchId, boolean nonCancelledSessions, Long startTime,
                                                Long endTime, Set<String> batchIds) throws VException {
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<OTFSessionPojo>();
        if (StringUtils.isEmpty(callingUserId)
                || (!Role.STUDENT.equals(callingUserRole) && !Role.TEACHER.equals(callingUserRole))) {
            return oTFSessionPojos;
        }

        Set<String> sessionIds = new HashSet<>();
        Query sessionQuery = new Query();
        Long currentTime = System.currentTimeMillis();
        Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();

        if (Role.STUDENT.equals(callingUserRole)) {
            int gttStart = 0;
            int gttLimit = 500;
            int gttLoopCount = 0;
            List<GTTAttendeeDetails> attendeeDetailsList = new ArrayList<>();
            do {
                List<GTTAttendeeDetails> tmpList = gttAttendeeDetailsDAO.getGTTAttendeeDetailsForUserIdForPast(callingUserId, startTime, endTime, gttStart, gttLimit);
                gttStart += gttLimit;
                gttLoopCount++;
                attendeeDetailsList.addAll(Optional.ofNullable(tmpList).orElseGet(ArrayList::new));
                if (ArrayUtils.isEmpty(tmpList) || tmpList.size() < gttLimit || gttLoopCount > 1) {
                    break;
                }
            } while (true);


            if (ArrayUtils.isNotEmpty(attendeeDetailsList)) {
                logger.info("gttforpast: " + attendeeDetailsList.stream().map(GTTAttendeeDetails::getId).collect(toList()));
                for (GTTAttendeeDetails attendeeDetails : attendeeDetailsList) {
                    sessionIds.add(attendeeDetails.getSessionId());
                    attendeeMap.put(attendeeDetails.getSessionId(), attendeeDetails);
                }
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants._ID).in(sessionIds));
            } else {
//                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(callingUserId));
                return new ArrayList<>();
            }
        } else if (Role.TEACHER.equals(callingUserRole)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(callingUserId));
        }

        if (StringUtils.isNotEmpty(query)) {
            int index = Math.min(query.length(), 30);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(query.substring(0, index), "i"));
        }

        if (!StringUtils.isEmpty(batchId)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        } else if (ArrayUtils.isNotEmpty(batchIds)) {
            sessionQuery.addCriteria(Criteria.where(BATCH_IDS).in(batchIds));
        }

        if (nonCancelledSessions) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        }

        if (startTime != null && startTime > 0 && endTime != null && endTime > 0 && startTime <= endTime) {
            sessionQuery.addCriteria(Criteria.where(END_TIME).gte(startTime).lte(endTime).lt(currentTime));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(currentTime));
        }

        sessionQuery.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));

        otfSessionDAO.setFetchParameters(sessionQuery, start, size);

        sessionQuery.fields().exclude(OTFSession.Constants.ATTENDEES);
        logger.info("get past session query {} {} {}",start, size, sessionQuery);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);

        if (ArrayUtils.isNotEmpty(oTFSessions)) {
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(oTFSessions);
            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);

            Set<String> presenterIds = new HashSet<>();

            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                presenterIds.add(oTFSessionPojo.getPresenter());
            }
            presenterIds.add(callingUserId);

            Map<String, UserBasicInfo> teachers = fosUtils.getUserBasicInfosMap(presenterIds, false);
            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                String presenter = oTFSessionPojo.getPresenter();
                UserBasicInfo info = teachers.get(presenter);
                logger.info("otf session presenter info {} {}", presenter, info);
                OTFSessionAttendeeInfo teacherOTFSessionAttendeeInfo = mapper.map(info, OTFSessionAttendeeInfo.class);
                oTFSessionPojo.getAttendeeInfos().add(teacherOTFSessionAttendeeInfo);
                OTFSessionAttendeeInfo oTFSessionAttendeeInfo = mapper.map(teachers.get(callingUserId), OTFSessionAttendeeInfo.class);
                if (attendeeMap.containsKey(oTFSessionPojo.getId())) {
                    GTTAttendeeDetails gTTAttendeeDetails = attendeeMap.get(oTFSessionPojo.getId());
                    oTFSessionAttendeeInfo.setActiveIntervals(gTTAttendeeDetails.getActiveIntervals());
                    oTFSessionAttendeeInfo.setJoinTimes(gTTAttendeeDetails.getJoinTimes());
                    oTFSessionAttendeeInfo.setTimeInSession(gTTAttendeeDetails.getTimeInSession());
                    oTFSessionAttendeeInfo.setStudentSessionJoinTime(gTTAttendeeDetails.getStudentSessionJoinTime());
                }
                oTFSessionPojo.getAttendeeInfos().add(oTFSessionAttendeeInfo);
                // presenterIds.add(oTFSessionPojo.getPresenter());
            }

        }

        fillCourseInfo(oTFSessionPojos);
        return oTFSessionPojos;
    }

    public List<OTFCalendarSessionInfo> getSessionsForDuration(String callingUserId, Role callingUserRole, String query, Integer start,
                                                               Integer size, String batchId, boolean nonCancelledSessions, Long startTime,
                                                               Long endTime, Set<String> batchIds) throws Exception {
        List<OTFSessionPojo> otfSessionPojos = new ArrayList<OTFSessionPojo>();
        if (StringUtils.isEmpty(callingUserId)
                || (!Role.STUDENT.equals(callingUserRole) && !Role.TEACHER.equals(callingUserRole))) {
            return new ArrayList<>();
        }

        Query sessionQuery = new Query();
        Long currentTime = System.currentTimeMillis();
        Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();

        Set<String> batchIdsFromEnrollment = new HashSet<>();
        Criteria pastSessionsCriteria = null, futureSessionsCriteria = null;
        if (Role.STUDENT.equals(callingUserRole)) {
            logger.info("currentTime {}, startTime {}, endTime {}", currentTime, startTime, endTime);
            if (startTime >= currentTime || endTime >= currentTime) {
                logger.info("batchids {} batchid {}", batchIds, batchId);
                if (ArrayUtils.isEmpty(batchIds) && StringUtils.isEmpty(batchId)) {
                    GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
                    enrolReq.setStatus(EntityStatus.ACTIVE);
                    enrolReq.setUserId(Long.parseLong(callingUserId));

                    List<EnrollmentPojo> enrollments = getEnrollmentsDataForSessionStrip(enrolReq);
                    if (!CollectionUtils.isEmpty(enrollments)) {
                        for (EnrollmentPojo enrollment : enrollments) {
                            batchIdsFromEnrollment.add(enrollment.getBatchId());
                        }
                        futureSessionsCriteria = Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIdsFromEnrollment);
                    }
                }
            }

            if (startTime <= currentTime || endTime <= currentTime) {

                int gttStart = 0, gttLimit = 100;
                Set<String> sessionIds = new HashSet<>();
                while (true) {
                    List<GTTAttendeeDetails> gttAttendeeDetails = gttAttendeeDetailsDAO.getGTTAttendeeDetailsForUserIdForDuration(callingUserId, startTime, endTime, gttStart, gttLimit);
                    logger.info("skip {} limit {} gtt details: {}", gttStart, gttLimit, gttAttendeeDetails);
                    gttStart += gttLimit;
                    if (ArrayUtils.isNotEmpty(gttAttendeeDetails)) {
                        for (GTTAttendeeDetails attendeeDetails : gttAttendeeDetails) {
                            sessionIds.add(attendeeDetails.getSessionId());
                            attendeeMap.put(attendeeDetails.getSessionId(), attendeeDetails);
                        }
                        pastSessionsCriteria = Criteria.where(OTFSession.Constants._ID).in(sessionIds);
                    } else {
                        break;
                    }
                }
            }

        } else if (Role.TEACHER.equals(callingUserRole)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(callingUserId));
        }

        if (StringUtils.isNotEmpty(query)) {
            int length = query.length();
            int index = Math.min(length, 30);
            String substring = query.substring(0, index);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(substring, "i"));
        }

        boolean timeCriteria = true;
        if (!StringUtils.isEmpty(batchId)) {
            logger.info("adding batch query {}", batchId);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        } else if (ArrayUtils.isNotEmpty(batchIds)) {
            logger.info("adding batch query {}", batchIds);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        } else if (pastSessionsCriteria != null && futureSessionsCriteria != null) {
            timeCriteria = false;
            logger.info("adding past and future criteria");
            sessionQuery.addCriteria(new Criteria().orOperator(
                    pastSessionsCriteria.and(START_TIME).gte(startTime).and(END_TIME).lt(endTime),
                    futureSessionsCriteria.and(START_TIME).gte(startTime).and(END_TIME).lt(endTime)
            ));
        }
        if (timeCriteria) {
            logger.info("adding time criteria");
            sessionQuery.addCriteria(Criteria.where(START_TIME).gte(startTime));
            sessionQuery.addCriteria(Criteria.where(END_TIME).lt(endTime));
        }

        if (pastSessionsCriteria != null && futureSessionsCriteria == null) {
            sessionQuery.addCriteria(pastSessionsCriteria);
        } else if (futureSessionsCriteria != null && pastSessionsCriteria == null) {
            sessionQuery.addCriteria(futureSessionsCriteria);
        } else if (futureSessionsCriteria == null && pastSessionsCriteria == null &&
                StringUtils.isEmpty(batchId) && ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }

        if (nonCancelledSessions) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        }


        sessionQuery.with(Sort.by(Direction.ASC, START_TIME));

        if (start != null && size != null) {
            otfSessionDAO.setFetchParameters(sessionQuery, start, size);
        } else {
            start = start == null ? 0 : start;
            size = size == null ? 20 : size;
        }
        sessionQuery.fields().exclude(OTFSession.Constants.ATTENDEES);

        List<OTFSession> otfSessions;
        if (startTime <= currentTime && endTime >= currentTime) {

            List<Serializable> conditions = Arrays.asList(new Document("$and", Arrays.asList(
                    new Document("$lte", Arrays.asList("$" + START_TIME, currentTime)),
                    new Document("$gte", Arrays.asList("$" + END_TIME, currentTime)))), 0, 1);
            List<Bson> bsons = Arrays.asList(
                    new Document("$match", otfSessionDAO.getParsedQueryDocument(sessionQuery, OTFSession.class)),
                    Aggregates.project(Projections.exclude(OTFSession.Constants.ATTENDEES)),
                    Aggregates.addFields(new Field<>("live_sessions", new Document("$cond", conditions))),
                    Aggregates.sort(Sorts.ascending("live_sessions", START_TIME)),
                    Aggregates.skip(start), Aggregates.limit(size)
            );
            otfSessions = otfSessionDAO.getSessionDurtaionAggregation(bsons);
            logger.info("session aggreagation query for get sesions for duration {}, {}, {} ", size, start, bsons);
        } else {
            logger.info("session query for get sesions for duration {}, {}, {} ", start, size, sessionQuery);
            otfSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        }



        if (ArrayUtils.isNotEmpty(otfSessions)) {
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(otfSessions);
            otfSessionPojos = getSessionInfos(otfSessions, sessionAdditionalInfoMap);

            Set<String> presenterIds = new HashSet<>();

            for (OTFSessionPojo oTFSessionPojo : otfSessionPojos) {
                presenterIds.add(oTFSessionPojo.getPresenter());
            }
            presenterIds.add(callingUserId);

            Map<String, UserBasicInfo> teachers = fosUtils.getUserBasicInfosMap(presenterIds, false);
            for (OTFSessionPojo oTFSessionPojo : otfSessionPojos) {
                OTFSessionAttendeeInfo teacherOTFSessionAttendeeInfo = mapper.map(teachers.get(oTFSessionPojo.getPresenter()), OTFSessionAttendeeInfo.class);
                oTFSessionPojo.getAttendeeInfos().add(teacherOTFSessionAttendeeInfo);
                OTFSessionAttendeeInfo oTFSessionAttendeeInfo = mapper.map(teachers.get(callingUserId), OTFSessionAttendeeInfo.class);
                if (attendeeMap.containsKey(oTFSessionPojo.getId())) {
                    GTTAttendeeDetails gTTAttendeeDetails = attendeeMap.get(oTFSessionPojo.getId());
                    oTFSessionAttendeeInfo.setActiveIntervals(gTTAttendeeDetails.getActiveIntervals());
                    oTFSessionAttendeeInfo.setJoinTimes(gTTAttendeeDetails.getJoinTimes());
                    oTFSessionAttendeeInfo.setTimeInSession(gTTAttendeeDetails.getTimeInSession());
                    oTFSessionAttendeeInfo.setStudentSessionJoinTime(gTTAttendeeDetails.getStudentSessionJoinTime());
                }
                oTFSessionPojo.getAttendeeInfos().add(oTFSessionAttendeeInfo);
                // presenterIds.add(oTFSessionPojo.getPresenter());
            }

        }

        /*boolean exposeEmail = callingUserRole == Role.ADMIN || callingUserRole == Role.STUDENT_CARE;
        if (callingUserRole == Role.STUDENT) {
            populateAttendeeInfoInOtfPojoForUser(otfSessionPojos, callingUserId, exposeEmail);
        } else {
            populateAttendeeInfosInOtfPojo(otfSessionPojos, exposeEmail);
        }
        populatePresenterInfosInOtfPojo(otfSessionPojos, exposeEmail);*/

        fillCourseInfo(otfSessionPojos);
        return populateUsersAndBoards(otfSessionPojos);
    }

    private List<OTFCalendarSessionInfo> populateUsersAndBoards(List<OTFSessionPojo> sessionPojos) {
        List<OTFSessionPojoUtils> sessions = gson.fromJson(gson.toJson(sessionPojos), new TypeToken<List<OTFSessionPojoUtils>>() {}.getType());
        List<OTFCalendarSessionInfo> calendarSessionInfos = new ArrayList<>();
        if (!com.vedantu.util.CollectionUtils.isEmpty(sessions)) {
            Set<Long> boardIds = new HashSet<>();
            Set<Long> userIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                if (session.getBoardId() != null) {
                    boardIds.add(session.getBoardId());
                } else {
                    if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                        for(OTFCourseBasicInfo courseBasicInfo:session.getCourseInfos()){
                            List<String> subjects = courseBasicInfo.getSubjects();
                            if (!com.vedantu.util.CollectionUtils.isEmpty(subjects)) {
                                boardIds.add(Long.parseLong(subjects.get(0)));
                            }
                        }
                    }
                }

                if (!org.springframework.util.StringUtils.isEmpty(session.getPresenter())) {
                    userIds.add(Long.parseLong(session.getPresenter()));
                }
            }

            // Fetch boards and users
            Map<Long, Board> boards = fosUtils.getBoardInfoMap(boardIds);
            Map<Long, User> users = fosUtils.getUserInfosMap(userIds, false);

            for (OTFSessionPojoUtils session : sessions) {
                OTFCalendarSessionInfo calendarSessionInfo = new OTFCalendarSessionInfo(session);
                if (session.getBoardId() != null && session.getBoardId() > 0l) {
                    Board board = boards.get(session.getBoardId());
                    if (board != null) {
                        calendarSessionInfo.setSubject(board.getName());
                    }
                } else {

                    if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                        for(OTFCourseBasicInfo courseBasicInfo:session.getCourseInfos()){
                            List<String> subjects = courseBasicInfo.getSubjects();
                            if (!com.vedantu.util.CollectionUtils.isEmpty(subjects)) {
                                Board board = boards.get(Long.parseLong(subjects.get(0)));
                                if (board != null) {
                                    calendarSessionInfo.setSubject(board.getName());
                                }
                            }
                        }
                    }
                }

                List<UserSessionInfo> attendees = new ArrayList<>();
                if (!org.springframework.util.StringUtils.isEmpty(session.getPresenter())) {
                    User user = users.get(Long.parseLong(session.getPresenter()));
                    if (user != null) {
                        attendees.add(new UserSessionInfo(user));
                    }
                }
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendees())) {
                    for (String studentId : session.getAttendees()) {
                        User user = users.get(Long.parseLong(studentId));
                        if (user != null) {
                            attendees.add(new UserSessionInfo(user));
                        }
                    }
                }
                if(ArrayUtils.isNotEmpty(session.getGroupNames())) {
                    calendarSessionInfo.setGroupNames(session.getGroupNames());
                }
                calendarSessionInfo.setAttendees(attendees);
                calendarSessionInfo.setId(calendarSessionInfo.getOtfSessionId());
                calendarSessionInfos.add(calendarSessionInfo);

            }
        }

        return calendarSessionInfos;
    }

    private Map<String, BatchBasicInfo> getBatchDetailedInfoMap(List<String> batchIds) throws VException {
        if (ArrayUtils.isEmpty(batchIds)) {
            return new HashMap<>();
        }

        // Fetch batches and create batch map
        BatchDetailInfoReq req = new BatchDetailInfoReq(batchIds);
        req.setBatchDataIncludeFields(Arrays.asList(
                BatchBasicInfo.Constants._ID,
                BatchBasicInfo.Constants.COURSE_ID,
                BatchBasicInfo.Constants.GROUP_NAME,
                BatchBasicInfo.Constants.SEARCH_TERMS
        ));
        req.setCourseDataIncludeFields(Arrays.asList(
                BatchBasicInfo.Constants._ID,
                BatchBasicInfo.Constants.TITLE,
                BatchBasicInfo.Constants.GRADES
        ));
        List<BatchBasicInfo> batchInfo = getDetailedBatchInfo(req);
        return batchInfo.stream()
                .collect(Collectors.toMap(BatchBasicInfo::getBatchId, Function.identity()));
    }

    private void fillCourseInfo(List<OTFSessionPojo> pojos) throws VException {
        if (ArrayUtils.isEmpty(pojos)) {
            return;
        }
        List<String> batchIdsList = pojos.stream().map(OTFSession::getBatchIds)
                .flatMap(Set::stream).distinct().collect(toList());
        Map<String, BatchBasicInfo> batchDetailedInfoMap = getBatchDetailedInfoMap(batchIdsList);
        for (OTFSessionPojo pojo : pojos) {
            List<CourseBasicInfo> courseBasicInfos = pojo.getBatchIds().stream()
                    .map(batchDetailedInfoMap::get)
                    .filter(Objects::nonNull)
                    .map(BatchBasicInfo::getCourseInfo)
                    .collect(toList());
            pojo.setCourseInfos(courseBasicInfos);
        }
    }

    public List<OTFSessionPojo> getUserPastSessionsWithFilter(String callingUserId, Role callingUserRole, String query, Integer start, Integer size, String batchId, Long startTime, Long endTime, Set<String> batchIdsSet) throws VException, UnsupportedEncodingException {

        query = query != null ? URLDecoder.decode(query, "UTF-8") : null;
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<>();
        List<OTFSession> finalOTFSessions = new ArrayList<>();
        if (callingUserRole == null) {
            callingUserRole = httpSessionUtils.getCurrentSessionData().getRole();
        }
        if (StringUtils.isEmpty(callingUserId)
                || (!Role.STUDENT.equals(callingUserRole) && !Role.TEACHER.equals(callingUserRole))) {
            return oTFSessionPojos;
        }
        List<String> batchIds = new ArrayList<>();
        if (StringUtils.isNotEmpty(batchId)) {
            batchIds.add(batchId);
        } else if (ArrayUtils.isNotEmpty(batchIds)) {
            batchIds.addAll(batchIdsSet);
        } else {
            GetEnrollmentsReq req = new GetEnrollmentsReq();
            req.setUserId(Long.valueOf(callingUserId));
            batchIds = getBatchIdsOfActiveEnrollmentsOfUser(req);
        }
        Query sessionQuery = new Query();
        Long currentTime = System.currentTimeMillis();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            if (StringUtils.isNotEmpty(batchId)) {

                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
            } else {
                Criteria orCriteria = new Criteria();
                orCriteria.orOperator(Criteria.where(OTFSession.Constants.ATTENDEES).in(callingUserId), Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
                sessionQuery.addCriteria(orCriteria);
            }
        }
        if (startTime != null && startTime > 0 && endTime != null && endTime > 0 && startTime <= endTime) {
            sessionQuery.addCriteria(Criteria.where(END_TIME).gte(startTime).lte(endTime).lt(currentTime));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(currentTime));
        }
        sessionQuery.addCriteria(Criteria.where(STATE).is(SessionState.SCHEDULED));
        sessionQuery.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        if (StringUtils.isNotEmpty(query)) {
            int length = query.length();
            int index = Math.min(length, 30);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(query.substring(0, index), "i"));
        }
        if (start != null && size != null) {
            otfSessionDAO.setFetchParameters(sessionQuery, start, size);
        }

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);

        Map<String, OTFSession> oTFSessionMap = new HashMap<>();
        Set<String> sessionIds = new HashSet<>();
        oTFSessions.forEach(oTFSession -> {
            oTFSessionMap.put(oTFSession.getId(), oTFSession);
            sessionIds.add(oTFSession.getId());
        });
        List<GTTAttendeeDetails> gttAttendeeDetails = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sessionIds)) {
            Boolean processing = Boolean.TRUE;
            int startCount = 0;
            int sizeCount = 500;
            while (Boolean.TRUE.equals(processing)) {
                List<GTTAttendeeDetails> attendeeDetailsList = gttAttendeeDetailsDAO.getAttendeeDetailForSessionList(callingUserId, sessionIds, Arrays.asList(GTTAttendeeDetails.Constants.SESSION_ID), startCount, sizeCount);
                if (ArrayUtils.isNotEmpty(attendeeDetailsList)) {
                    gttAttendeeDetails.addAll(attendeeDetailsList);
                    startCount += attendeeDetailsList.size();
                } else {
                    processing = Boolean.FALSE;
                }
            }
        }
        if (ArrayUtils.isNotEmpty(gttAttendeeDetails)) {
            gttAttendeeDetails.forEach((gTTAttendeeDetail) -> {
                if (oTFSessionMap.containsKey(gTTAttendeeDetail.getId())) {
                    finalOTFSessions.add(oTFSessionMap.get(gTTAttendeeDetail.getId()));
                }
            });
        }
        if (ArrayUtils.isNotEmpty(oTFSessions)) {
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(oTFSessions);
            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);
            Set<String> presenterIds = new HashSet<>();
            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                presenterIds.add(oTFSessionPojo.getPresenter());
            }
            presenterIds.add(callingUserId);
            Map<String, UserBasicInfo> teachers = fosUtils.getUserBasicInfosMap(presenterIds, false);
            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                String presenter = oTFSessionPojo.getPresenter();
                UserBasicInfo info = teachers.get(presenter);
                logger.info("presenter ids {} - {}", presenter, info);
                OTFSessionAttendeeInfo teacherOTFSessionAttendeeInfo = mapper.map(info, OTFSessionAttendeeInfo.class);
                oTFSessionPojo.getAttendeeInfos().add(teacherOTFSessionAttendeeInfo);
            }
        }
        fillCourseInfo(oTFSessionPojos);
        return oTFSessionPojos;
    }

    public List<String> getAllBatchIdsOfUser(String userId) throws VException {
        ClientResponse resp2 = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "enroll/getAllBatchIdsOfUser" + "?userId=" + userId,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp2);
        String jsonString2 = resp2.getEntity(String.class);
        logger.info("=========================================================== 0.5 " + jsonString2);
        return gson.fromJson(jsonString2, ArrayList.class);

    }

    public List<UpcomingSessionsPojo> upcomingSessionsAndWebinarForUser(String userId, Role role, Integer start, Integer limit) throws VException {

        if (StringUtils.isEmpty(userId)
                || (Role.STUDENT != role && Role.TEACHER != role)) {
            return new ArrayList<>();
        }

        List<SessionInfo> otoSessions = getEmptyArrayListIfNull(getUpcominOtoSessions(userId, role, start, limit));
        Map<String, CoursePlanBasicInfo> coursePlanMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(otoSessions)) {
            logger.info("oto sessions count {}", otoSessions.size());
            String coursePlanIds = otoSessions.stream()
                    .map(Session::getContextId)
                    .filter(Objects::nonNull)
                    .distinct()
                    .collect(Collectors.joining(","));
            if (StringUtils.isNotEmpty(coursePlanIds)) {
                String url = ConfigUtils.INSTANCE.getSubscriptionEndpoint() +
                        "/courseplan/getBasicInfosForHomePage?coursePlanIds=" + coursePlanIds;
                ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                String entity = response.getEntity(String.class);
                List<CoursePlanBasicInfo> infos = gson.fromJson(entity, new TypeToken<List<CoursePlanBasicInfo>>() {}.getType());
                coursePlanMap = infos.stream().collect(Collectors.toMap(CoursePlanBasicInfo::getId, e->e));
            }
        }

        List<WebinarSessionInfo> webinarSessionInfos = new ArrayList<>();
        Set<String> batchIds = new HashSet<>();
        if (role == Role.STUDENT) {
            webinarSessionInfos = getEmptyArrayListIfNull(getUpcomingWebinarSessionIds(start, limit));
            batchIds = getUserEnrolledBatchIds(userId);
            // For no enrollments return empty list
            if (ArrayUtils.isEmpty(batchIds) && ArrayUtils.isEmpty(webinarSessionInfos) && ArrayUtils.isEmpty(otoSessions)) {
                return new ArrayList<>();
            }
        }
        logger.info("webinar sessions size {}", webinarSessionInfos.size());

        Map<String, WebinarSessionInfo> webinarInfoMap = new HashMap<>();
        Map<String, BatchBasicInfo> batchDetailedInfoMap = new HashMap<>();
        List<OTFSession> otfSessions = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(batchIds) || ArrayUtils.isNotEmpty(webinarSessionInfos)) {
            webinarInfoMap = webinarSessionInfos.stream()
                    .collect(Collectors.toMap(WebinarSessionInfo::getSessionId, e -> e));

            // Converting OTF Session and Webinar and OTO Session to Response Format
            otfSessions = getEmptyArrayListIfNull(
                    otfSessionDAO.getUpcomingOtfSessions(userId, role, start, (limit * 2), webinarInfoMap.keySet(), batchIds));
            logger.info("otf session size {}", otfSessions.size());

            List<String> sessionBatchIds = otfSessions.stream().map(OTFSession::getBatchIds)
                    .flatMap(Set::stream).distinct().collect(toList());
            batchDetailedInfoMap = getBatchDetailedInfoMap(sessionBatchIds);
        }

        Map<String, UserBasicInfo> userBasicInfo = Stream.concat(
                otfSessions.stream().map(OTFSession::getPresenter).filter(Objects::nonNull),
                otoSessions.stream().map(Session::getTeacherId).filter(Objects::nonNull).map(String::valueOf))
                .collect(Collectors.collectingAndThen(toList(), e -> fosUtils.getUserBasicInfosMap(e, false)));

        Map<Long, Board> boardMap = otfSessions.stream().map(OTFSession::getBoardId).filter(Objects::nonNull).filter(e -> e > 0)
                .collect(Collectors.collectingAndThen(Collectors.toSet(), e -> fosUtils.getBoardInfoMap(e)));


        List<UpcomingSessionsPojo> otfPojos = UpcomingSessionsPojo.convertFromOtfSessions(otfSessions, batchDetailedInfoMap, userBasicInfo, boardMap, webinarInfoMap);
        logger.info("otfPojos size {}", otfPojos.size());
        List<UpcomingSessionsPojo> otoPojos = UpcomingSessionsPojo.convertFromOtoSessions(otoSessions, coursePlanMap, userBasicInfo);
        logger.info("otoPojos size {}", otoPojos.size());

        return Stream.concat(otfPojos.stream(), otoPojos.stream())
                .sorted(Comparator.comparing(UpcomingSessionsPojo::getStartTime)
                        .thenComparing(UpcomingSessionsPojo::getSessionType)
                        .thenComparing(UpcomingSessionsPojo::getSessionSubType)
                        .thenComparing(UpcomingSessionsPojo::getSearchTerms))
                .limit(limit)
                .collect(Collectors.toList());
    }




    private Set<String> getUserEnrolledBatchIds(String userId) throws VException {
        GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
        enrolReq.setStatus(EntityStatus.ACTIVE);
        enrolReq.setUserId(Long.parseLong(userId));

        List<EnrollmentPojo> enrollments = getEnrollmentsDataForSessionStrip(enrolReq);
        return Optional.ofNullable(enrollments).orElseGet(ArrayList::new)
                .stream().map(EnrollmentPojo::getBatchId)
                .collect(Collectors.toSet());
    }

    private List<SessionInfo> getUpcominOtoSessions(String userId, Role role, Integer start, Integer limit) throws VException {
        GetUserUpcomingSessionReq req = new GetUserUpcomingSessionReq();
        req.setSessionStates(Arrays.asList(
                com.vedantu.session.pojo.SessionState.SCHEDULED,
                com.vedantu.session.pojo.SessionState.ACTIVE,
                com.vedantu.session.pojo.SessionState.STARTED
                ));
        req.setCallingUserId(Long.valueOf(userId));
        req.setCallingUserRole(role);
        req.setStart(start);
        req.setSize(limit);
        req.setPopulateAttendees(false);
        return sessionManager.getUserUpcomingSessions(req);
    }

    private List<WebinarSessionInfo> getUpcomingWebinarSessionIds(Integer start, Integer limit) throws VException {
        String url = ConfigUtils.INSTANCE.getGrowthEndpoint() + "/cms/webinar/upcomingWebinarSessionIdForUser?start="
                +  start + "&size=" + limit;
        ClientResponse clientResponse = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(clientResponse);
        String entity = clientResponse.getEntity(String.class);
        return gson.fromJson(entity, new TypeToken<List<WebinarSessionInfo>>() {}.getType());
    }

    public List<OTFSessionPojo> getUpcomingSessions(String callingUserId, Role callingUserRole, String query, Integer start,
                                                    Integer size, String batchId, Long startTime, Long endTime, Set<String> batchIds) throws VException {
        return getUpcomingSessions(callingUserId, callingUserRole, query, start, size, batchId, true, new HashMap<String, EnrollmentPojo>(), startTime, endTime, batchIds);
    }

    public List<OTFSessionPojo> getUpcomingSessions(String callingUserId, Role callingUserRole, String query, Integer start,
                                                    Integer size, String batchId, boolean nonCancelledSessions,
                                                    Map<String, EnrollmentPojo> enrollmentMap, Long startTime, Long endTime, Set<String> batchIds) throws VException {
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<OTFSessionPojo>();
        if (StringUtils.isEmpty(callingUserId)
                || (!Role.STUDENT.equals(callingUserRole) && !Role.TEACHER.equals(callingUserRole))) {
            return oTFSessionPojos;
        }

        Set<String> batchIdsFromEnrollment = new HashSet<>();
        Set<String> sessionIds = new HashSet<>();
        Query sessionQuery = new Query();
        Long currentTime = System.currentTimeMillis();
        if (Role.STUDENT.equals(callingUserRole)) {
            GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
            enrolReq.setStatus(EntityStatus.ACTIVE);
            enrolReq.setUserId(Long.parseLong(callingUserId));

            List<EnrollmentPojo> enrollments = getEnrollmentsDataForSessionStrip(enrolReq);
            if (!CollectionUtils.isEmpty(enrollments)) {
                for (EnrollmentPojo enrollment : enrollments) {
                    batchIdsFromEnrollment.add(enrollment.getBatchId());
                    enrollmentMap.put(enrollment.getBatchId(), enrollment);
                }
            } else {
                // For no enrollments return empty list
                //sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(callingUserId));
                return oTFSessionPojos;
            }

        } else if (Role.TEACHER.equals(callingUserRole)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(callingUserId));
        }

        if (ArrayUtils.isNotEmpty(batchIds)) {
            // only active enrollments
            batchIds.retainAll(batchIdsFromEnrollment);
        } else {
            batchIds = batchIdsFromEnrollment;
        }

        if (!StringUtils.isEmpty(batchId)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        } else if (ArrayUtils.isNotEmpty(batchIds)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        }

        if (StringUtils.isNotEmpty(query)) {
            int length = query.length();
            int max = (length < ReqLimits.NAME_TYPE_MAX) ? length : ReqLimits.NAME_TYPE_MAX;
            query = query.substring(0, max);
            int index = length < 30 ? length : 30;
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(query.substring(0, index), "i"));
        }

        if (nonCancelledSessions) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        }

//        this is wrong, always false
//        if (OTFSession.Constants.FLAGS.contains("SALES_DEMO")) {
//            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(OTMSessionInProgressState.ACTIVE));
//        }

        if (startTime != null && startTime > 0 && endTime != null && endTime > 0 && startTime <= endTime) {
            sessionQuery.addCriteria(Criteria.where(END_TIME).gte(currentTime).gte(startTime).lte(endTime));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(currentTime));
        }

        sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.END_TIME));

        if (start != null && size != null) {
            otfSessionDAO.setFetchParameters(sessionQuery, start, size);
        }
        sessionQuery.fields().exclude(OTFSession.Constants.ATTENDEES);
        logger.info("get upcoming session query {}", sessionQuery);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);

        if (ArrayUtils.isNotEmpty(oTFSessions)) {
//            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(oTFSessions);
//            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);
            Set<String> presenterIds = new HashSet<>();
//
//            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
//                presenterIds.add(oTFSessionPojo.getPresenter());
//            }

            for (OTFSession otfSession : oTFSessions) {
                oTFSessionPojos.add(mapper.map(otfSession, OTFSessionPojo.class));
                presenterIds.add(otfSession.getPresenter());
            }

            Map<String, UserBasicInfo> teachers = fosUtils.getUserBasicInfosMap(presenterIds, false);
            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                oTFSessionPojo.getAttendeeInfos().add(mapper.map(teachers.get(oTFSessionPojo.getPresenter()), OTFSessionAttendeeInfo.class));
                // presenterIds.add(oTFSessionPojo.getPresenter());
            }

        }
        fillCourseInfo(oTFSessionPojos);
        return oTFSessionPojos;
    }

    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

    public List<BatchCalendarPojo> getCalendarSessionByEntity(Long afterStartTime, Long beforeStartTime, Long beforeEndTime,
            Long afterEndTime, List<String> grades, List<String> identifiers, String type) throws VException {

        List<BatchCalendarPojo> response = new ArrayList<>();

        List<String> batchesFromGroupName = new ArrayList<>();

        if (StringUtils.isNotEmpty(type) && type.equals("GROUP_NAME")) {
            logger.info("insideGroupName");
            if (com.vedantu.util.CollectionUtils.isNotEmpty(identifiers)) {

                logger.info("insideGroupNameIdent");

                String c = "";
                for (String groupName : identifiers) {
                    try {
                        String encodeURL = URLEncoder.encode(groupName, "UTF-8");
                        c += "groupName=" + encodeURL + "&";
                        logger.info(c);
                    } catch (UnsupportedEncodingException e) {

                    }
                }

                c = removeLastChar(c);

                // Get batches
                ClientResponse resp2 = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "batch/getBatchByGroupName" + "?" + c,
                        HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp2);
                String jsonString2 = resp2.getEntity(String.class);
                batchesFromGroupName = gson.fromJson(jsonString2, ArrayList.class);
                logger.info("BATCH-IDS");
                logger.info(batchesFromGroupName);
            }
        }

        //Grade
//        List<String> courseIds = new ArrayList<>();
//        if(com.vedantu.util.CollectionUtils.isNotEmpty(grades)) {
//
//            String g = "";
//            for (String grade : grades) {
//                g += "grades=" + grade + "&";
//            }
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/course/getCoursesByGrades" + "?" + g,
//                    HttpMethod.GET, null);
//            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//            String jsonString = resp.getEntity(String.class);
//            courseIds = gson.fromJson(jsonString, ArrayList.class);
//            logger.info("COURSES-IDS");
//            logger.info(courseIds);
//
//            String c = "";
//            for (String course : courseIds) {
//                c += "courseIds=" + course + "&";
//            }
//
//            ClientResponse resp2 = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/getBatchByCourses" + "?" + c,
//                    HttpMethod.GET, null);
//            VExceptionFactory.INSTANCE.parseAndThrowException(resp2);
//            String jsonString2 = resp2.getEntity(String.class);
//            batches = gson.fromJson(jsonString2, ArrayList.class);
//            logger.info("BATCH-IDS");
//            logger.info(batches);
//
//        }
        if ((StringUtils.isNotEmpty(type) && type.equals("BATCH") && com.vedantu.util.CollectionUtils.isNotEmpty(identifiers))
                || (StringUtils.isNotEmpty(type) && type.equals("GROUP_NAME") && com.vedantu.util.CollectionUtils.isNotEmpty(batchesFromGroupName))) {

            List<String> batches = new ArrayList<>();

            if (type.equals("BATCH")) {
                batches = identifiers;
            } else {
                batches = batchesFromGroupName;
            }

            for (String b : batches) {
                Query query = new Query();
                query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(b));
                query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

                // End time queries
                if (afterEndTime != null && afterEndTime > 0L) {
                    if (beforeEndTime != null && beforeEndTime > 0L) {
                        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
                    } else {
                        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime));
                    }
                } else if (beforeEndTime != null && beforeEndTime > 0L) {
                    query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(beforeEndTime));
                }

                // Start time queries
                if (afterStartTime != null && afterStartTime > 0L) {
                    if (beforeStartTime != null && beforeStartTime > 0L) {
                        query.addCriteria(
                                Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime).lt(beforeStartTime));
                    } else {
                        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime));
                    }
                } else if (beforeStartTime != null && beforeStartTime > 0L) {
                    query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(beforeStartTime));
                }

                query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
                List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

                BatchCalendarPojo bcp = new BatchCalendarPojo();
                bcp.setBatchId(b);

                List<String> batchBasicInfo = new ArrayList<>();
                batchBasicInfo.add(b);
                List<BatchBasicInfo> bbi = getBatchBasicInfosByIds(batchBasicInfo);
                if (com.vedantu.util.CollectionUtils.isNotEmpty(bbi)) {
                    if (bbi.get(0).getCourseInfo() != null) {
                        bcp.setCourseTitle(bbi.get(0).getCourseInfo().getTitle());
                    }
                    if (StringUtils.isNotEmpty(bbi.get(0).getGroupName())) {
                        bcp.setGroupName(bbi.get(0).getGroupName());
                    }
                }

                List<SessionCalendarPojo> sessionsData = new ArrayList<>();
                if (com.vedantu.util.CollectionUtils.isNotEmpty(oTFSessions)) {
                    for (OTFSession s : oTFSessions) {
                        SessionCalendarPojo scp = new SessionCalendarPojo();
                        scp.setTitle(s.getTitle());
                        scp.setId(s.getId());
                        scp.setTutor(s.getPresenter());
                        scp.setEnd(s.getEndTime());
                        scp.setStart(s.getStartTime());
                        scp.setSessionType(s.getOtmSessionType());
                        scp.setState(s.getState());
                        scp.setSubject(s.getBoardId());
                        sessionsData.add(scp);
                    }
                }
                bcp.setSessions(sessionsData);
                response.add(bcp);
            }
        }

        return response;

    }

    public List<OTFSessionPojo> getCalendarSessions(Long afterStartTime, Long beforeStartTime, Long beforeEndTime,
            Long afterEndTime, String callingUserId, Role callingUserRole, String query) throws VException {
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<OTFSessionPojo>();
        if (StringUtils.isEmpty(callingUserId)
                || (!Role.STUDENT.equals(callingUserRole) && !Role.TEACHER.equals(callingUserRole))) {
            return oTFSessionPojos;
        }

        // Fetch active batches of the student/teacher
        Set<String> batchIds = new HashSet<>();
        Set<String> sessionIds = new HashSet<>();
        Query sessionQuery = new Query();
        Long currentTime = System.currentTimeMillis();
        Long nextDayStartTime = DateTimeUtils.getISTDayStartTime(currentTime) + DateTimeUtils.MILLIS_PER_DAY - 5 * DateTimeUtils.MILLIS_PER_HOUR - 30 * DateTimeUtils.MILLIS_PER_MINUTE;
        if (Role.STUDENT.equals(callingUserRole)) {

            if ((afterEndTime != null && afterEndTime >= currentTime) || (beforeEndTime != null && beforeEndTime >= currentTime)
                    || (afterStartTime != null && afterStartTime >= currentTime) || (beforeStartTime != null && beforeStartTime >= currentTime)) {
                logger.info("In the required block");
                GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
                enrolReq.setStatus(EntityStatus.ACTIVE);
                enrolReq.setUserId(Long.parseLong(callingUserId));
                enrolReq.setSize(MAX_ENROLLMENTS);
                enrolReq.setStart(0);
                enrolReq.getIncludeFields().add("batchId");
                List<EnrollmentPojo> enrollments = getEnrollmentsForUser(enrolReq);
                if (!CollectionUtils.isEmpty(enrollments)) {
                    enrollments.forEach(a -> batchIds.add(a.getBatchId()));
                } else {
                    // For no enrollments return empty list
//                    sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(callingUserId));
                    return new ArrayList<>();
                }
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));

            } else {
                List<GTTAttendeeDetails> gTTAttendeeDetailses = new ArrayList<>();
                int start = 0;
                int size = 100;
                while (true) {
                    List<GTTAttendeeDetails> details = gttAttendeeDetailsDAO.getGTTAttendeeDetailsForUserId(callingUserId, start, size);

                    if (ArrayUtils.isEmpty(details)) {
                        break;
                    }

                    gTTAttendeeDetailses.addAll(details);
                    if (details.size() < size) {
                        break;
                    }

                    start = start + size;

                }
                if (ArrayUtils.isNotEmpty(gTTAttendeeDetailses)) {
                    for (GTTAttendeeDetails attendeeDetails : gTTAttendeeDetailses) {
                        sessionIds.add(attendeeDetails.getSessionId());
                    }
                    sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ID).in(sessionIds));
                } else {
                    return new ArrayList<>();
//                    sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(callingUserId));
                }
            }
        }
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

        if (!StringUtils.isEmpty(query)) {
            int length = query.length();
            int index = length < 30 ? length : 30;
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(query.substring(0, index), "i"));
        }
        // End time queries
        if (afterEndTime != null && afterEndTime > 0L) {
            if (beforeEndTime != null && beforeEndTime > 0L) {
                sessionQuery
                        .addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime));
            }
        } else if (beforeEndTime != null && beforeEndTime > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(beforeEndTime));
        }

        // Start time queries
        if (afterStartTime != null && afterStartTime > 0L) {
            if (beforeStartTime != null && beforeStartTime > 0L) {
                sessionQuery.addCriteria(
                        Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime).lt(beforeStartTime));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime));
            }
        } else if (beforeStartTime != null && beforeStartTime > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(beforeStartTime));
        }

        if (Role.TEACHER.equals(callingUserRole)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(callingUserId));
        }
        sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        sessionQuery.fields().exclude(OTFSession.Constants.ATTENDEES);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);

        // Fill the course and batch details
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            // Fetch course infos
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(oTFSessions);
            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);

        }

        // Get Webinar sessions
        if (Role.STUDENT.equals(callingUserRole) || Role.TEACHER.equals(callingUserRole)) {
            GetOTFWebinarSessionsReq getOTFWebinarSessionsReq = new GetOTFWebinarSessionsReq();
            getOTFWebinarSessionsReq.setAfterStartTime(afterStartTime);
            getOTFWebinarSessionsReq.setBeforeStartTime(beforeStartTime);
            getOTFWebinarSessionsReq.setAfterEndTime(afterEndTime);
            getOTFWebinarSessionsReq.setBeforeEndTime(beforeEndTime);
            if (Role.STUDENT.equals(callingUserRole)) {
                getOTFWebinarSessionsReq.setStudentId(callingUserId);
            }
            if (Role.TEACHER.equals(callingUserRole)) {
                getOTFWebinarSessionsReq.setTeacherId(callingUserId);
            }
            getOTFWebinarSessionsReq.addSessionStates(SessionState.SCHEDULED);
            List<OTFSessionPojo> webinarSessions = getWebinarSessionInfos(getOTFWebinarSessionsReq, false);
            if (!CollectionUtils.isEmpty(webinarSessions)) {
                Set<String> set = oTFSessionPojos.stream().map(OTFSessionPojo::getId).filter(Objects::nonNull).collect(toSet());
                webinarSessions.stream().filter(e -> set.add(e.getId())).forEach(oTFSessionPojos::add);
            }
        }

        return oTFSessionPojos;
    }

    public List<OTFSessionPojo> getUpcomingSessionsWithAttendeeInfos(Long startTime, Long endTime, Integer start,
            Integer size, String sessionId) throws VException {
        List<OTFSessionPojo> response = new ArrayList<>();
        List<OTFSession> oTFSessions = new ArrayList<>();
        if (sessionId != null) {
            if (!ObjectId.isValid(sessionId)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "invalid sessionId");
            } else {
                oTFSessions.add(otfSessionDAO.getById(sessionId));
            }
        } else {
            oTFSessions.addAll(otfSessionDAO.getNonDeletedSessionByInTimeRange(startTime, endTime, start, size));
        }

        // Get all batch ids
        Set<String> batchIds = new HashSet<>();
        Set<String> sessionIds = new HashSet<>();
        for (OTFSession oTFSession : oTFSessions) {
            if (oTFSession == null) {
                continue;
            }
            if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {
                batchIds.addAll(oTFSession.getBatchIds());
            }
            sessionIds.add(oTFSession.getId());
        }
        Map<String, BatchBasicInfo> batchMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            BatchDetailInfoReq req = new BatchDetailInfoReq(new ArrayList<>(batchIds));
            req.setBatchDataIncludeFields(Arrays.asList(
                    BatchBasicInfo.Constants._ID,
                    BatchBasicInfo.Constants.COURSE_ID
            ));
            req.setCourseDataIncludeFields(Arrays.asList(
                    BatchBasicInfo.Constants._ID,
                    BatchBasicInfo.Constants.TITLE
            ));
            List<BatchBasicInfo> batches = getDetailedBatchInfo(req);
            if (ArrayUtils.isNotEmpty(batches)) {
                batchMap = batches.stream().collect(Collectors.toMap(BatchBasicInfo::getBatchId, c -> c));
            }
        }
        // Create session infos
        for (OTFSession oTFSession : oTFSessions) {
            if (oTFSession == null) {
                continue;
            }
            List<CourseBasicInfo> courseBasicInfos = new ArrayList<>();
            Set<String> batchIdsForSession = new LinkedHashSet<>();
            if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {
                for (String batchId : oTFSession.getBatchIds()) {
                    if (!batchMap.containsKey(batchId)) {
                        continue;
                    }
                    BatchBasicInfo batch = batchMap.get(batchId);
                    courseBasicInfos.add(batch.getCourseInfo());
                    batchIdsForSession.add(batchId);
                }
            }

            OTFSessionPojo oTFSessionPojo = getSessionInfo(oTFSession, courseBasicInfos);
            oTFSessionPojo.setBatchIds(batchIdsForSession);
            // Fill the GTT attendees
            logger.info(oTFSessionPojo);
            response.add(oTFSessionPojo);
        }

        populatePresenterInfosInOtfPojo(response, true);
        populateAttendeeInfosInOtfPojo(response, true);
        return response;
    }

    public List<OTFSession> updateAttendeeReport(Long afterEndTime, Long beforeEndTime)
            throws InternalServerErrorException, BadRequestException {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).in(SUPPORTED_SESSION_TOOL_TYPES));
        // query.addCriteria(Criteria.where(Session.Constants.BATCH_ID).exists(true).ne(null));

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            List<String> errors = new ArrayList<>();
            for (OTFSession oTFSession : oTFSessions) {
                try {
                    ScheduleManager scheduleManager = getScheduleManagerByToolType(oTFSession);
                    if (scheduleManager == null) {
                        continue;
                    }
                    if (StringUtils.isEmpty(oTFSession.getMeetingId())) {//will take care of vedantuwave sessions
                        logger.info("no meeting id found for this session");
                        continue;
                    }

                    scheduleManager.handleFeedback(oTFSession);
                    otfSessionDAO.save(oTFSession);
                } catch (Exception ex) {
                    String errorMessage = "Error occured handling feedback, ex:" + ex.toString() + " message:"
                            + ex.getMessage() + " req:" + oTFSession.toString();
                    logger.info(errorMessage);
                    errors.add(errorMessage);
                }
            }

            if (!CollectionUtils.isEmpty(errors)) {
                logger.info("Error message : " + Arrays.toString(errors.toArray()));
            }
        }

        return oTFSessions;
    }

    public List<OTFSessionPojo> getSessionsForRecordingDeletion(GetOTFSessionsForRecordingDeletionReq req)
            throws BadRequestException {
        // Validate req
        req.verify();

        List<OTFSession> oTFSessions = otfSessionDAO.getSessionsForRecordingDeletion(req);
        if (oTFSessions == null) {
            logger.info("No sessions found for the requested time");
            return new ArrayList<>();
        }

        logger.info("Size : " + oTFSessions.size());
        return getSessionInfos(oTFSessions);
    }

    public void updateSessionFlags(UpdateOTFSessionFlag updateOTFSessionFlag) throws BadRequestException, ConflictException {
        logger.info("Request:" + updateOTFSessionFlag.toString());

        // Validate request
        updateOTFSessionFlag.validate();

        // Fetch the sessions
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.ID).in(updateOTFSessionFlag.getSessionIds()));
        List<OTFSession> otfSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        // Add the flags
        if (!CollectionUtils.isEmpty(otfSessions)) {
            for (OTFSession otfSession : otfSessions) {
                logger.info("Session pre update:" + otfSession.toString());
                otfSession.addFlags(updateOTFSessionFlag.getFlags());
                otfSessionDAO.save(otfSession);
                logger.info("Session post update:" + otfSession.toString());
            }
        }
    }

    public List<OTFSessionPojo> getUpcomingSessions(Long afterEndTime, String teacherId, String studentId, String batchId,
            Integer start, Integer limit, String query, boolean upcomingOnly) throws VException {
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<>();

        // Fetch active batches of the student/teacher
        Query sessionQuery = new Query();
        Set<String> batchIds = new HashSet<>();
        if (!StringUtils.isEmpty(studentId)) {
            GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
            enrolReq.setStatus(EntityStatus.ACTIVE);
            enrolReq.setUserId(Long.parseLong(studentId));

            List<String> enrolledBatchIds = getBatchIdsOfActiveEnrollmentsOfUser(enrolReq);
            if (!CollectionUtils.isEmpty(enrolledBatchIds)) {
                enrolledBatchIds.forEach(enrolledBatchId -> batchIds.add(enrolledBatchId));
            } else {
                // For no enrollments return empty list
                return oTFSessionPojos;
            }
            if (!StringUtils.isEmpty(batchId) && !batchIds.contains(batchId)) {
                // student is not part of the batch requested
                return oTFSessionPojos;
            }
            if (StringUtils.isEmpty(batchId)) {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
            }
        } else {
            if (!StringUtils.isEmpty(batchId)) {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
            }
        }

        if (!StringUtils.isEmpty(query)) {
            int length = query.length();
            int index = length < 30 ? length : 30;
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(query.substring(0, index), "i"));
        }

        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        if (!StringUtils.isEmpty(teacherId)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(teacherId));
        }
        if (afterEndTime != null && afterEndTime > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gt(afterEndTime));
        }

        if (upcomingOnly) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).ne(OTMSessionInProgressState.ENDED));
        }

        if (start != null) {
            sessionQuery.skip(start);
        }
        if (limit != null && limit > 0) {
            sessionQuery.limit(limit);
        }
        sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);

        // Fill the course and batch details
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(oTFSessions);
            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);
        }
        return oTFSessionPojos;
    }

    public GetLatestUpComingOrOnGoingSessionResponse getUpcomingSessionsForTicker(Long afterEndTime, String teacherId,
            String studentId, Integer start, Integer limit) throws VException {
        // OTF sessions
        GetLatestUpComingOrOnGoingSessionResponse res = new GetLatestUpComingOrOnGoingSessionResponse();
        List<OTFSessionPojo> oTFSessionPojos = getUpcomingSessions(afterEndTime, teacherId, studentId, null, start, limit, null, true);
        if (!CollectionUtils.isEmpty(oTFSessionPojos)) {
            OTFSessionPojoUtils sessionPojo = mapper.map(oTFSessionPojos.get(0), OTFSessionPojoUtils.class);
            res.setOtfSession(sessionPojo);
        }

        // OTF webinar sessions
        GetOTFWebinarSessionsReq req = new GetOTFWebinarSessionsReq();
        req.setAfterEndTime(afterEndTime);
        req.setTeacherId(teacherId);
        req.setStudentId(studentId);
        req.setStart(start);
        req.setLimit(limit);
        req.addSessionStates(SessionState.SCHEDULED);
        List<OTFSessionPojo> webinarSessions = getWebinarSessionInfos(req, true);
        if (!CollectionUtils.isEmpty(webinarSessions)) {
            OTFSessionPojo webinarSession = webinarSessions.get(0);
            //do not want to duplicate the same session in webinar res too
            if (res.getOtfSession() != null && !res.getOtfSession().getId().equals(webinarSession.getId())) {
                OTFSessionPojoUtils sessionPojo = mapper.map(webinarSession, OTFSessionPojoUtils.class);
                res.setWebinarSession(sessionPojo);
            }
        }
        return res;
    }

    public List<SessionPojoWithRefId> addWebinarSessions(OTFWebinarSessionReq req)
            throws BadRequestException, InternalServerErrorException, ConflictException {
        List<SessionPojoWithRefId> updatedEntries = new ArrayList<>();

        // Validate the request
        req.validate();

        List<String> errors = new ArrayList<>();
        Map<String, OTFSession> existingSessions = getExistingSessions(extractSessionIds(req.getSessions()));
        OTFSession closestFutureSession = null;
        for (OTFWebinarSession webinarSession : req.getSessions()) {
            OTFSession otfSession = null;
            if (StringUtils.isEmpty(webinarSession.getId())) {
                otfSession = OTFSession.createWebinarSession(webinarSession, req);
                scheduleSessionByBitSet(otfSession);
            } else {
                otfSession = existingSessions.get(webinarSession.getId());
                if (otfSession == null) {
                    errors.add("Session not found for id : " + webinarSession.getId() + " webinar:"
                            + webinarSession.toString());
                    continue;
                }

                // Cannot update session in past
                if (otfSession.getStartTime() < System.currentTimeMillis()) {
                    continue;
                }

                // Update session timings if required
                if (otfSession.getStartTime() != webinarSession.getStartTime()
                        || otfSession.getEndTime() != webinarSession.getEndTime()
                        && !otfSession.getSessionToolType().equals(webinarSession.getSessionToolType())) {
                    // De-schedule the session
                    if (!StringUtils.isEmpty(otfSession.getOrganizerAccessToken())) {
                        descheduleSessionByBitSet(otfSession);
                    }

                    // Update session timings
                    otfSession.setStartTime(webinarSession.getStartTime());
                    otfSession.setEndTime(webinarSession.getEndTime());
                    if (webinarSession.getSessionToolType() != null) {
                        otfSession.setSessionToolType(webinarSession.getSessionToolType());
                    }
                    // Schedule the session
                    scheduleSessionByBitSet(otfSession);
                }

                // Update other session parameters
                logger.info("Session pre update : " + otfSession.toString());
                otfSession.updateWebinarSession(webinarSession);
            }

            if (closestFutureSession == null) {
                closestFutureSession = otfSession;
            } else if (closestFutureSession != null
                    && otfSession.getStartTime() < closestFutureSession.getStartTime()) {
                closestFutureSession = otfSession;
            }
            otfSession.updateAttendees(req.getEnrolledStudents());
            otfSessionDAO.save(otfSession);
            if (otfSession.getStartTime() != null
                    && otfSession.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
                if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                    /*Map<String, Object> payload = new HashMap<>();
                    payload.put("session", oTFSession);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                    asyncTaskFactory.executeTask(params);*/
                    sendToQForGeneratingSessionLinkAndGTT(otfSession);
                }
            }
            OTFSessionPojo oTFSessionPojo = getSessionInfo(otfSession, null);
            SessionPojoWithRefId sessionPojoWithRefId = mapper.map(oTFSessionPojo, SessionPojoWithRefId.class);
            sessionPojoWithRefId.setReferenceId(webinarSession.getReferenceId());
            logger.info("Webinar session created : " + otfSession.toString());
            updatedEntries.add(sessionPojoWithRefId);
        }

        if (!CollectionUtils.isEmpty(errors)) {
            logger.error("Error updating webinar sessions - " + Arrays.toString(errors.toArray()));
        }
        try {
            if (closestFutureSession != null) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("sessionInfo", mapper.map(closestFutureSession, OTFSessionPojo.class));
                payload.put("event", SessionEventsOTF.WEBINAR_SESSION_SCHEDULED);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
                asyncTaskFactory.executeTask(params);
            }
        } catch (Exception e) {
            logger.info("error in async task while scheduling webinar session");
        }
        return updatedEntries;
    }

    public List<OTFSessionPojo> getWebinarSessionInfos(GetOTFWebinarSessionsReq req, boolean upcomingOnly) {
        return getSessionInfos(getWebinarSessions(req, upcomingOnly));
    }

    public List<OTFSession> getWebinarSessions(GetOTFWebinarSessionsReq req, boolean upcomingOnly) {
        logger.info("Request : " + req.toString());
        Query query = new Query();
        Criteria criteria = new Criteria().orOperator(
                Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE).is(OTFSessionContextType.WEBINAR),
                Criteria.where(OTFSession.Constants.SESSION_LABELS).in(SessionLabel.WEBINAR)
        );
        query.addCriteria(criteria);
        if (!CollectionUtils.isEmpty(req.getBundleIds())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_ID).in(req.getBundleIds()));
        }
        if (!CollectionUtils.isEmpty(req.getSessionStates())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(req.getSessionStates()));
        } else {
            query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        }
        if (req.getAfterStartTime() != null && req.getAfterStartTime() > 0L) {
            if (req.getBeforeStartTime() != null && req.getBeforeStartTime() > 0L) {
                query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getAfterStartTime())
                        .lt(req.getBeforeStartTime()));
            } else {
                query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getAfterStartTime()));
            }
        } else if (req.getBeforeStartTime() != null && req.getBeforeStartTime() > 0L) {
            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(req.getBeforeStartTime()));
        }

        if (req.getAfterEndTime() != null && req.getAfterEndTime() > 0L) {
            if (req.getBeforeEndTime() != null && req.getBeforeEndTime() > 0L) {
                query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(req.getAfterEndTime())
                        .lt(req.getBeforeEndTime()));
            } else {
                query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(req.getAfterEndTime()));
            }
        } else if (req.getBeforeEndTime() != null && req.getBeforeEndTime() > 0L) {
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(req.getBeforeEndTime()));
        }

        if (!StringUtils.isEmpty(req.getTeacherId())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(req.getTeacherId()));
        }
        if (!StringUtils.isEmpty(req.getStudentId())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(req.getStudentId()));
        }

        if (upcomingOnly) {
            query.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).ne(OTMSessionInProgressState.ENDED));
        }

        if (req.getStart() != null && req.getStart() >= 0L) {
            query.skip(req.getStart());
        }

        if (req.getLimit() != null && req.getLimit() > 0L) {
            query.limit(req.getLimit());
        }

        query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
        return otfSessionDAO.runQuery(query, OTFSession.class);
    }

    public List<OTFSessionPojo> cancelWebinarSession(CancelOTFWebinarSessionsReq req) throws BadRequestException {
        logger.info("Request : " + req.toString());
        if (StringUtils.isEmpty(req.getBundleId()) && CollectionUtils.isEmpty(req.getSessionIds())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Missing bundleId and sessionIds");
        }

        // Validate the request
        req.validate();

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(
                Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE).is(OTFSessionContextType.WEBINAR));
        if (!StringUtils.isEmpty(req.getBundleId())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_ID).is(req.getBundleId()));
        }
        if (!CollectionUtils.isEmpty(req.getSessionIds())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.ID).in(req.getSessionIds()));
        }

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            OTFSession closestFutureSession = null;
            List<String> errors = new ArrayList<>();
            for (OTFSession oTFSession : oTFSessions) {
                try {
                    // Should not cancel previous session
                    if (oTFSession.getStartTime() < System.currentTimeMillis()) {
                        continue;
                    } else if (closestFutureSession == null) {
                        closestFutureSession = oTFSession;
                    } else if (closestFutureSession != null
                            && oTFSession.getStartTime() < closestFutureSession.getStartTime()) {
                        closestFutureSession = oTFSession;
                    }

                    // Deschedule the session to free the organizer token
                    logger.info("Session pre update : " + oTFSession.toString());
                    if (!StringUtils.isEmpty(oTFSession.getOrganizerAccessToken())) {
                        descheduleSessionByBitSet(oTFSession);
                    }
                    oTFSession.setState(SessionState.CANCELLED);
                    otfSessionDAO.save(oTFSession);
                } catch (Exception ex) {
                    logger.info("ErrorCancelling the session : " + oTFSession.toString());
                }
            }

            if (!CollectionUtils.isEmpty(errors)) {
                logger.error("ErrorCancelling sessions for req:" + req.toString() + " error:"
                        + Arrays.toString(errors.toArray()));
            }
            try {
                if (closestFutureSession != null) {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("sessionInfo", mapper.map(closestFutureSession, OTFSessionPojo.class));
                    payload.put("event", SessionEventsOTF.WEBINAR_SESSION_CANCELED);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
                    asyncTaskFactory.executeTask(params);
                }
            } catch (Exception e) {
                logger.info("error in async task while canceling webinar session");
            }
        }

        return getSessionInfos(oTFSessions);
    }

    public List<OTFSessionPojo> updateWebinarSessionAttendee(UpdateWebinarSessionsAttendenceReq req)
            throws BadRequestException, NotFoundException, ConflictException {
        // Validate the request
        req.validate();

        // Fetch the session
        List<OTFSession> oTFSessions = new ArrayList<>();
        if (!StringUtils.isEmpty(req.getSessionId())) {
            OTFSession otfSession = otfSessionDAO.getById(req.getSessionId());
            if (otfSession == null) {
                String errorMessage = "Session Not found for id:" + req.getSessionId() + " req:" + req.toString();
                logger.info(errorMessage);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
            }
            if (!SessionState.SCHEDULED.equals(otfSession.getState())) {
                String errorMessage = "Enrollment only allowed for schedule sessions - session:" + otfSession.toString()
                        + " req:" + req.toString();
                logger.info(errorMessage);
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
            }
            if (!otfSession.isWebinarSession()) {
                String errorMessage = "Enrollment only allowed for webinar sessions - session" + otfSession.toString()
                        + " req:" + req.toString();
                logger.info(errorMessage);
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
            }

            oTFSessions.add(otfSession);
        } else {
            Query sessionQuery = new Query();
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
            sessionQuery.addCriteria(
                    Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE).is(OTFSessionContextType.WEBINAR));
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gt(System.currentTimeMillis()));
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_ID).is(req.getBundleId()));
            oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        }

        if (CollectionUtils.isEmpty(oTFSessions)) {
            // String errorMessage = "No sessions found to register the student
            // req:" +
            // req.toString();
            // logger.info(errorMessage);
            // throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
            // errorMessage);
            return getSessionInfos(oTFSessions);
        }

        // Fetch existing webinar attendees
        Map<String, WebinarAttendee> existingWebinarMap = getUserWebinarSessionAttendeeMap(req.getStudentId(), null);

        // Add the student id for the sessions
        for (OTFSession oTFSession : oTFSessions) {
            logger.info("Session pre update : " + oTFSession.toString());
            oTFSession.updateAttendee(req.getStudentId(), req.getEntityStatus());
            otfSessionDAO.save(oTFSession);
            logger.info("Session post update : " + oTFSession.toString());

            // Create attendee registration entry
            if (Boolean.TRUE.equals(req.getAttendeeConfirmed())) {
                WebinarAttendee webinarAttendee = null;
                if (existingWebinarMap.containsKey(oTFSession.getId())) {
                    webinarAttendee = existingWebinarMap.get(oTFSession.getId());
                } else {
                    webinarAttendee = new WebinarAttendee(oTFSession.getId(), req.getStudentId());
                    webinarAttendee.setEntityStatus(EntityStatus.ACTIVE);
                }

                webinarAttendeeDAO.create(webinarAttendee);
            }
        }
        try {
            Map<String, Object> payload = new HashMap<>();
            EnrollmentPojo enrollment = new EnrollmentPojo();
            enrollment.setUserId(req.getStudentId());
            payload.put("enrollment", enrollment);
            switch (req.getEntityStatus()) {
                case ENDED:
                    payload.put("event", BatchEventsOTF.USER_ENDED);
                case ACTIVE:
                    payload.put("event", BatchEventsOTF.USER_ACTIVE);
                case INACTIVE:
                    payload.put("event", BatchEventsOTF.USER_INACTIVE);
            }
            // AsyncTaskParams params = new
            // AsyncTaskParams(AsyncTaskName.ENROLLMENT_EVENTS_TRIGGER,
            // payload);
            // asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.error("Error in creating async event.");
        }

        return getSessionInfos(oTFSessions);
    }

    public Map<String, WebinarAttendee> getUserWebinarSessionAttendeeMap(String userId, List<String> sessionIds) {
        Map<String, WebinarAttendee> webinarMap = new HashMap<>();
        Query webinarAttendeeQuery = new Query();
        webinarAttendeeQuery.addCriteria(Criteria.where(WebinarAttendee.Constants.USER_ID).is(userId));
        if (!CollectionUtils.isEmpty(sessionIds)) {
            webinarAttendeeQuery.addCriteria(Criteria.where(WebinarAttendee.Constants.SESSION_ID).in(sessionIds));
        }

        List<WebinarAttendee> webinarAttendees = webinarAttendeeDAO.runQuery(webinarAttendeeQuery,
                WebinarAttendee.class);
        if (!CollectionUtils.isEmpty(webinarAttendees)) {
            for (WebinarAttendee webinarAttendee : webinarAttendees) {
                webinarMap.put(webinarAttendee.getSessionId(), webinarAttendee);
            }
        }

        return webinarMap;
    }

    public static Map<String, GTTAttendeeDetails> createAttendeeInfoMap(List<GTTAttendeeDetails> attendeeDetails) {
        Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(attendeeDetails)) {
            for (GTTAttendeeDetails gttAttendeeDetails : attendeeDetails) {
                attendeeMap.put(getAttendeeInfoMapKey(gttAttendeeDetails), gttAttendeeDetails);
            }
        }

        return attendeeMap;
    }

    public static String getAttendeeInfoMapKey(GTTAttendeeDetails gttAttendeeDetails) {
        return gttAttendeeDetails.getTraningId() + ":" + gttAttendeeDetails.getUserId();
    }

    public static String getAttendeeInfoMapKey(String meetingId, String userId) {
        return meetingId + ":" + userId;
    }

    private Map<String, OTFSession> getExistingSessions(Set<String> sessionIds) {
        Map<String, OTFSession> existingSessions = new HashMap<>();
        if (!CollectionUtils.isEmpty(sessionIds)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(OTFSession.Constants.ID).in(sessionIds));
            List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
            if (!CollectionUtils.isEmpty(oTFSessions)) {
                for (OTFSession oTFSession : oTFSessions) {
                    existingSessions.put(oTFSession.getId(), oTFSession);
                }
            }
        }
        return existingSessions;
    }

    private static Set<String> extractSessionIds(List<OTFWebinarSession> sessions) {
        Set<String> sessionIds = new HashSet<>();
        if (!CollectionUtils.isEmpty(sessions)) {
            for (OTFWebinarSession session : sessions) {
                if (!StringUtils.isEmpty(session.getId())) {
                    sessionIds.add(session.getId());
                }
            }
        }

        return sessionIds;
    }

    private List<OTFSessionPojo> getSessionInfos(List<OTFSession> oTFSessions) {
        return getSessionInfos(oTFSessions, null);
    }

    public List<OTFSessionPojo> getSessionInfos(List<OTFSession> oTFSessions,
            Map<String, SessionAdditionalInfo> sessionAddtionalInfoMap) {
        List<OTFSessionPojo> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                Set<String> batchIds = new LinkedHashSet<>();
                if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds()) && sessionAddtionalInfoMap != null) {
                    List<CourseBasicInfo> courseBasicInfos = new ArrayList<>();
                    List<String> groupNames = new ArrayList<>();
                    for (String batchId : oTFSession.getBatchIds()) {
                        if (sessionAddtionalInfoMap.containsKey(batchId)) {
                            SessionAdditionalInfo sessionAdditionalInfo = sessionAddtionalInfoMap.get(batchId);
                            CourseBasicInfo course = sessionAdditionalInfo != null ? sessionAdditionalInfo.getCourse() : null;
                            if (course != null) {
                                batchIds.add(batchId);
                                courseBasicInfos.add(course);
                                if (StringUtils.isNotEmpty(sessionAdditionalInfo.getGroupName())) {
                                    groupNames.add(sessionAdditionalInfo.getGroupName());
                                }
                            }
                        }
                    }
                    OTFSessionPojo oTFSessionPojo = getSessionInfo(oTFSession, courseBasicInfos);
                    oTFSessionPojo.setBatchIds(batchIds);
                    oTFSessionPojo.setGroupNames(groupNames);
                    results.add(oTFSessionPojo);
                } else {
                    results.add(getSessionInfo(oTFSession, null));
                }
            }
        }

        return results;
    }

    public List<String> getSessionIdsForCurriculum(Long startTime, Long endTime, Long teacherId, String batchId) {
        List<String> sessionIds = new ArrayList<>();
        if (startTime == null || endTime == null || teacherId == null || StringUtils.isEmpty(batchId)) {
            return sessionIds;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(teacherId.toString()));
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(startTime).lt(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.TEACHER_JOIN_TIME).exists(true));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        query.fields().include(OTFSession.Constants.ID);
        query.fields().include(OTFSession.Constants._ID);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        if (ArrayUtils.isNotEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                sessionIds.add(oTFSession.getId());
            }
        }
        return sessionIds;
    }

    public OTFAttendance getStudentAttendenceCountWithTotal(StudentBatchProgressReq req) throws BadRequestException {
        req.verify();
        OTFAttendance otfAttendance = new OTFAttendance();
        TeacherBatchProgressReq teacherBatchProgressReq = new TeacherBatchProgressReq();
        teacherBatchProgressReq.setContextType(req.getContextType());
        teacherBatchProgressReq.setContextId(req.getContextId());
        teacherBatchProgressReq.setBoardId(req.getBoardId());
        teacherBatchProgressReq.setStudentIds(Arrays.asList(req.getUserId().toString()));

        List<OTFAttendance> attendances = getAttendanceForStudentsAndBoard(teacherBatchProgressReq);

        if (ArrayUtils.isNotEmpty(attendances)) {
            for (OTFAttendance attendance : attendances) {
                if (attendance.getStudentId().equals(req.getUserId())) {
                    return attendance;
                }
            }
        }
        return otfAttendance;
    }

    public List<OTFSessionInfo> getTeacherForBatchIdAndBoardId(Long boardId, String batchId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        if (boardId != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(boardId));
        }
        // query.addCriteria(Criteria.where(Session.Constants.TEACHER_JOIN_TIME).exists(true).gt(0));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        otfSessionDAO.setFetchParameters(query, 0, 2);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        List<OTFSessionInfo> sessionInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                OTFSessionInfo sessionInfo = new OTFSessionInfo();
                sessionInfo.setId(oTFSession.getId());
                sessionInfo.setPresenter(oTFSession.getPresenter());
                sessionInfo.setStartTime(oTFSession.getStartTime());

                sessionInfos.add(sessionInfo);
            }

        }
        return sessionInfos;
    }

    //This will fetch start time of next OTFSession for given subject(boardId) and batch(contextId)
    public Long getNextSessionTimeForBatchIdAndBoardId(List<Long> boardIds, String batchId, Long startTime) {

        int size = 1;
    	List<OTFSession> otfSessions = otfSessionDAO.getNextSessionTimeForBatchIdAndBoardId(boardIds, batchId, startTime, size);
    	logger.info("Output size :  {} ",otfSessions.size());
        Long sessionStartTime = null;
        if (ArrayUtils.isNotEmpty(otfSessions)) {
            sessionStartTime = otfSessions.get(0).getStartTime();
        }
        logger.info("startTime : {} ",sessionStartTime);
		return sessionStartTime;
    }

    public List<OTFAttendance> getAttendanceForStudentsAndBoard(TeacherBatchProgressReq req)
            throws BadRequestException {
        // logger.info("########   START: getAttendanceForStudentsAndBoard   ########");
        // logger.info("req: "+req);
        req.verify();
        if (ArrayUtils.isEmpty(req.getStudentIds())) {
            throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND, "No students found in Req");
        }
        if (req.getStudentIds().size() > 5) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "can't process for more 5 students");
        }
        List<OTFAttendance> studentAttendance = new ArrayList<>();

        Long teacherId = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(req.getContextId()));
        query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(req.getBoardId()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(System.currentTimeMillis()));

        // query.addCriteria(Criteria.where(Session.Constants.TEACHER_JOIN_TIME).exists(true));
        // query.with(Sort.by(Sort.Direction.DESC,
        // Session.Constants.END_TIME));
        query.fields().include(OTFSession.Constants.ID);
        query.fields().include(OTFSession.Constants._ID);
        query.fields().include(OTFSession.Constants.PRESENTER);
        query.fields().include(OTFSession.Constants.TEACHER_JOIN_TIME);

        query.fields().include(OTFSession.Constants.ATTENDEES);
        query.fields().include(OTFSession.Constants.MEETING_ID);
        query.fields().include(OTFSession.Constants.STATE);
        query.fields().include(OTFSession.Constants.PROGRESS_STATE);

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        // logger.info("\noTFSessions: "+oTFSessions);
        List<String> sessionIds = new ArrayList<>();
        Map<String, OTFAttendance> studentAttendanceMap = new HashMap<>();
        for (String userId : req.getStudentIds()) {
            studentAttendanceMap.put(userId, new OTFAttendance());
        }
        // logger.info("\nstudentAttendanceMap: "+studentAttendanceMap);
        if (ArrayUtils.isNotEmpty(oTFSessions)) {

            teacherId = Long.parseLong(oTFSessions.get(0).getPresenter());
            for (OTFSession oTFSession : oTFSessions) {
                if (!StringUtils.isEmpty(oTFSession.getId())) {
                    sessionIds.add(oTFSession.getId());
                }
            }

            // Map<String,List<String>> sessionAttended =
            // dashboardManager.getAttendeeListForTeacherDashboard(sessionIds);
            // logger.info("sessionIds: "+sessionIds);
            Map<String, SessionAttendanceIdsPojo> attendanceMap = getAttendeeMapForSessionIds(sessionIds, new HashSet<>(req.getStudentIds()));
            // logger.info("\nattendanceMap: START");
            // logger.info("\nattendanceMap: END");
            if (attendanceMap == null) {
                attendanceMap = new HashMap<>();
            } else {
                attendanceMap.forEach((k, v) -> logger.info(k + " " + v.toString()));
            }
            for (OTFSession oTFSession : oTFSessions) {
                // logger.info("\nInside loop oTFSession - "+oTFSession);
                if (oTFSession.getId() != null && attendanceMap.containsKey(oTFSession.getId())
                        && attendanceMap.get(oTFSession.getId()) != null) {
                    SessionAttendanceIdsPojo attendancePojo = attendanceMap.get(oTFSession.getId());
                    if ((ArrayUtils.isNotEmpty(attendancePojo.getAttended()) || ArrayUtils.isNotEmpty(attendancePojo.getAbsentees()))
                            && SessionState.SCHEDULED.equals(oTFSession.getState())
                            && OTMSessionInProgressState.ENDED.equals(oTFSession.getProgressState())) { // valid
                        // session
                        for (String userId : req.getStudentIds()) {
                            OTFAttendance attendanceObj = studentAttendanceMap.get(userId);
                            if (attendancePojo.getAttended().contains(userId)) {
                                attendanceObj.setAttended(attendanceObj.getAttended() + 1);
                                attendanceObj.setTotal(attendanceObj.getTotal() + 1);
                            } else if (ArrayUtils.isNotEmpty(attendancePojo.getAbsentees())
                                    && attendancePojo.getAbsentees().contains(userId)) {
                                attendanceObj.setTotal(attendanceObj.getTotal() + 1);
                            }
                        }
                    }
                }
            }
        }
        for (String studentId : req.getStudentIds()) {
            OTFAttendance otfAttendance = new OTFAttendance();
            otfAttendance.setStudentId(Long.parseLong(studentId));
            if (studentAttendanceMap.containsKey(studentId)) {
                otfAttendance.setTotal(studentAttendanceMap.get(studentId).getTotal());
                otfAttendance.setAttended(studentAttendanceMap.get(studentId).getAttended());
            }
            otfAttendance.setTeacherId(teacherId);
            studentAttendance.add(otfAttendance);
        }
        // logger.info("studentAttendance: "+studentAttendance);
        // logger.info("########   END: getAttendanceForStudentsAndBoard   ########");
        return studentAttendance;
    }

    // TODO Removal
    @Deprecated
    public List<OTFSessionInfo> getPastSessionsForBatchProgress(TeacherBatchProgressReq req) throws NotFoundException, BadRequestException {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(req.getContextId()));
        query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(req.getBoardId()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        if (req.getBeforeEndTime() != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(req.getBeforeEndTime()));
        } else {
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lt(System.currentTimeMillis()));
        }

        // query.addCriteria(Criteria.where(Session.Constants.TEACHER_JOIN_TIME).exists(true));

        otfSessionDAO.setFetchParameters(query, req);
        query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        List<String> sessionIds = new ArrayList<>();
        List<OTFSessionInfo> response = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(oTFSessions)) {
            for (OTFSession oTFSession : oTFSessions) {
                if (!StringUtils.isEmpty(oTFSession.getId())) {
                    sessionIds.add(oTFSession.getId());
                }
            }

            Map<String, SessionAttendanceIdsPojo> attendanceMap = getAttendeeMapForSessionIds(sessionIds, null);

            if (attendanceMap == null) {
                attendanceMap = new HashMap<>();
            }
            for (OTFSession oTFSession : oTFSessions) {
                OTFSessionInfo otfSessionInfo = new OTFSessionInfo();
                otfSessionInfo.setPresenter(oTFSession.getPresenter());
                otfSessionInfo.setId(oTFSession.getId());
                otfSessionInfo.setTitle(oTFSession.getTitle());
                otfSessionInfo.setBatchIds(oTFSession.getBatchIds());
                otfSessionInfo.setDuration(oTFSession.getEndTime() - oTFSession.getStartTime());
                otfSessionInfo.setTeacherJoinTime(oTFSession.getTeacherJoinTime());
                otfSessionInfo.setStartTime(oTFSession.getStartTime());
                otfSessionInfo.setState(oTFSession.getState());
                otfSessionInfo.setPollsdata(oTFSession.getPollsdata());
                if (attendanceMap.containsKey(oTFSession.getId()) && attendanceMap.get(oTFSession.getId()) != null) {
                    SessionAttendanceIdsPojo attPojo = attendanceMap.get(oTFSession.getId());
                    // if
                    // (attPojo.getAttended().contains(session.getPresenter()))
                    // {
                    // otfSessionInfo.setAttended(new ArrayList<>());
                    // otfSessionInfo.getAttended().addAll(attPojo.getAttended());
                    // }
                    otfSessionInfo.setAttended(new ArrayList<>());
                    if (ArrayUtils.isNotEmpty(attPojo.getAttended())) {
                        otfSessionInfo.getAttended().addAll(attPojo.getAttended());
                    }
                    otfSessionInfo.setAbsentees(new ArrayList<>());
                    if (ArrayUtils.isNotEmpty(attPojo.getAbsentees())) {
                        otfSessionInfo.getAbsentees().addAll(attPojo.getAbsentees());
                    }
                }
                response.add(otfSessionInfo);
            }
            return response;
        } else {
            return response;
        }

    }

    public OTFSessionPojo getSessionInfo(OTFSession oTFSession, List<CourseBasicInfo> courseInfos) {
        OTFSessionPojo oTFSessionPojo = mapper.map(oTFSession, OTFSessionPojo.class);
        oTFSessionPojo.setCourseInfos(courseInfos);
        return oTFSessionPojo;
    }

    public Map<String, SessionAttendanceIdsPojo> getAttendeeMapForSessionIds(List<String> sessionIds, Set<String> userIds) throws BadRequestException {
        if (ArrayUtils.isEmpty(sessionIds)) {
            return null;
        }
        if (ArrayUtils.isEmpty(userIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No users in the request");
        }
        if (ArrayUtils.isNotEmpty(userIds) && userIds.size() > 5) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "can't process for more than 5 users at a time");
        }
        Map<String, SessionAttendanceIdsPojo> sessionIdAttendanceMap = new HashMap<>();

        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(userIds));
        query.fields().include(GTTAttendeeDetails.Constants.SESSION_ID);
        query.fields().include(GTTAttendeeDetails.Constants.USER_ID);
        query.fields().include(GTTAttendeeDetails.Constants.JOIN_TIMES);
        query.with(Sort.by(Direction.ASC, GTTAttendeeDetails.Constants.USER_ID, GTTAttendeeDetails.Constants.TRAINING_ID));

        Boolean processing = Boolean.TRUE;
        int start = 0;
        int size = 500;
        while (Boolean.TRUE.equals(processing)) {
            query.skip(start);
            query.limit(size);
            List<GTTAttendeeDetails> attendeeDetailsList = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
            if (ArrayUtils.isNotEmpty(attendeeDetailsList)) {
                try {
                    processSessionAttendance(sessionIdAttendanceMap, attendeeDetailsList);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                start += attendeeDetailsList.size();
            } else {
                processing = Boolean.FALSE;
            }
        }

        return sessionIdAttendanceMap;
    }

    private void processSessionAttendance(Map<String, SessionAttendanceIdsPojo> sessionIdAttendanceMap, List<GTTAttendeeDetails> attendeeDetails) {
        for (GTTAttendeeDetails gttAttendeeDetails : attendeeDetails) {
            if (!StringUtils.isEmpty(gttAttendeeDetails.getSessionId())
                    && sessionIdAttendanceMap.containsKey(gttAttendeeDetails.getSessionId())) {
                logger.info("gttAttendeeDetails = " + gttAttendeeDetails.getJoinTimes());
                if (ArrayUtils.isNotEmpty(gttAttendeeDetails.getJoinTimes())) {
                    sessionIdAttendanceMap.get(gttAttendeeDetails.getSessionId()).getAttended()
                            .add(gttAttendeeDetails.getUserId());
                } else {
                    sessionIdAttendanceMap.get(gttAttendeeDetails.getSessionId()).getAbsentees()
                            .add(gttAttendeeDetails.getUserId());
                }
            } else {
                SessionAttendanceIdsPojo idsPojo = new SessionAttendanceIdsPojo();
                idsPojo.setAbsentees(new HashSet<>());
                idsPojo.setAttended(new HashSet<>());
                if (ArrayUtils.isNotEmpty(gttAttendeeDetails.getJoinTimes())) {
                    idsPojo.getAttended().add(gttAttendeeDetails.getUserId());
                } else {
                    idsPojo.getAbsentees().add(gttAttendeeDetails.getUserId());
                }
                sessionIdAttendanceMap.put(gttAttendeeDetails.getSessionId(), idsPojo);
            }
        }
    }

    public Map<String, BatchBasicInfo> createBatchMap(Set<String> batchIds) {
        Query query = new Query();
        // query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
        List<BatchBasicInfo> batches = null; // batchDAO.runQuery(query,
        // Batch.class);
        Map<String, BatchBasicInfo> batchMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(batches)) {
            batches.forEach(b -> batchMap.put(b.getId(), b));
        }
        return batchMap;

    }

    public UpdateSessionRes cancelSession(CancelOrDeleteSessionReq req)
            throws BadRequestException, NotFoundException, InternalServerErrorException, ForbiddenException, VException {
        req.verify();
        OTFSession otfSession = otfSessionDAO.getById(req.getSessionId());
        if (otfSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + req.getSessionId());
        }

        if (SessionState.CANCELLED.equals(otfSession.getState())
                || SessionState.FORFEITED.equals(otfSession.getState())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session already cancelled");
        }
        boolean isVedantuWaveSession = OTFSessionToolType.VEDANTU_WAVE.equals(otfSession.getSessionToolType());
        if (isVedantuWaveSession && (otfSession.getStartTime() < (System.currentTimeMillis() + 60 * DateTimeUtils.MILLIS_PER_MINUTE))) {
            throw new ForbiddenException(ErrorCode.DELETING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME, "DELETING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME");
        }
        if (otfSession.getStartTime() != null && otfSession.getStartTime() < System.currentTimeMillis()) {
            if (otfSession.getEndTime() != null
                    && otfSession.getEndTime() > (System.currentTimeMillis() - (4 * DateTimeUtils.MILLIS_PER_HOUR))) {
                throw new ForbiddenException(ErrorCode.SESSION_ALREADY_ACTIVE,
                        "Session forfeit is not allowed for the requested session : " + req.toString()
                        + ". Session forfeit is only allowed for past sessions which ended 4 hours in past.");
            }

            Query query = new Query();

            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).is(otfSession.getMeetingId()));
            query.with(Sort.by(Direction.DESC, GTTAttendeeDetails.Constants.TIME_IN_SESSION));

            List<GTTAttendeeDetails> attendees = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);

            if (ArrayUtils.isNotEmpty(attendees)) {
                GTTAttendeeDetails attendee = attendees.get(0);
                if (attendee.getTimeInSession() != null
                        && attendee.getTimeInSession() > 30 * DateTimeUtils.SECONDS_PER_MINUTE) {
                    throw new BadRequestException(ErrorCode.SESSION_MINIMUM_DURATION_EXCEEDED,
                            "Attendee time in session exceeds 30 minutes");
                }
            }
            otfSession.setState(SessionState.FORFEITED);
            // session.setForfeitedBy(req.getCallingUserId().toString());
            if (req.getCallingUserId() != null) {
                otfSession.setForfeitedBy(req.getCallingUserId().toString());
            }
        } else {
            otfSession.setState(SessionState.CANCELLED);
            if (req.getCallingUserId() != null) {
                otfSession.setCancelledBy(req.getCallingUserId().toString());
            }

            if (!StringUtils.isEmpty(otfSession.getOrganizerAccessToken())) {
                descheduleSessionByBitSet(otfSession);
            }
        }
        logger.info("updateSession : " + otfSession);
        otfSession.setRemarks(req.getRemark());
        otfSessionDAO.save(otfSession);
        gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedBulk(otfSession.getId(), DeleteContext.SESSION_STATE_CHANGE);

        // TODO move unblocking sessions code for all session types from platform to here -- for now webinar sessions are unblocked
        if (otfSession.isWebinarSession()) {
            CalendarEntryReq calendarEntryReq = new CalendarEntryReq(otfSession.getPresenter(), otfSession.getStartTime(),
                    otfSession.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION, otfSession.getId());
            BasicRes response = calendarEntryManager.removeCalendarEntrySlotsUsingUserId(calendarEntryReq);

        }

        if (isVedantuWaveSession || otfSession.isOtoSession()) {
            List<OTFSession> otfSessionList = new ArrayList<>();
            otfSessionList.add(otfSession);
            vedantuWaveSessionManager.sendSessionsToNodeForCaching(otfSessionList);
            vedantuWaveSessionManager.sendGttAttendeeDetailsToNodeForCaching(otfSessionList);
        }
        communicationManager.sendSessionCancelationEmail(otfSession);
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
            payload.put("event", SessionEventsOTF.OTF_SESSION_CANCELED);
            logger.info(payload);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);            // Batch batch = batchManager.getBatch(otfSession.getBatchIds());
            // if (batch == null) {
            // String errorString = "Batch not found for id : " +
            // session.getBatchIds() + ". Request : " + req.toString();
            // throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
            // errorString);
            // }
            //
            // if (EntityStatus.INACTIVE.equals(batch.getBatchState())) {
            // throw new IllegalArgumentException(
            // "Batch is not active : " + batch.toString() + ". Request : " +
            // req.toString());
            // }
            //
            // // Batch Agenda save
            // List<Agenda> agendas = batch.getAgenda();
            // if (ArrayUtils.isNotEmpty(agendas)) {
            // Agenda toRemove = null;
            // for (Agenda agenda : agendas) {
            // if (AgendaType.SESSION.equals(agenda.getType())
            // && session.getId().equals(agenda.getSessionId())) {
            // toRemove = agenda;
            // break;
            // }
            // }
            // if (toRemove != null) {
            // agendas.remove(toRemove);
            // }
            // }
            // batch.setAgenda(agendas);
            // batchManager.updateBatch(batch);
            // batchInfo=batchManager.getBatchInfo(batch);
        } catch (Exception e) {
            logger.error("Error in removing the session entry from batch");
        }

        return new UpdateSessionRes(otfSession, null);
    }

    public void updateReplayUrlforGTW(UpdateReplayUrlReq req) throws BadRequestException, NotFoundException {
        req.verify();
        Query query = new Query();
        if (!StringUtils.isEmpty(req.getSessionId())) {
            query.addCriteria(Criteria.where(OTFSession.Constants._ID).is(req.getSessionId()));
        }
        if (!StringUtils.isEmpty(req.getMeetingId())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.MEETING_ID).is(req.getMeetingId()));
        }
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).is(OTFSessionToolType.GTW));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        if (ArrayUtils.isEmpty(oTFSessions) || oTFSessions.size() > 1) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found : " + req.toString());
        }

        OTFSession oTFSession = oTFSessions.get(0);
        logger.info("session to add replay to :" + oTFSession);
        List<GTTRecording> downloadRecordings = new ArrayList<GTTRecording>();
        for (String url : req.getDownloadUrl()) {
            if (!StringUtils.isEmpty(url)) {
                GTTRecording recording = new GTTRecording();
                recording.setName(oTFSession.getTitle());
                recording.setRecordingId(String.valueOf(System.currentTimeMillis()));
                recording.setDownloadUrl(url);
                downloadRecordings.add(recording);
            }
        }
        if (ArrayUtils.isEmpty(downloadRecordings)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "downloadurls are empty");
        }

        GTTSessionRecordingInfo gttSessionRecordingInfo = gttSessionRecordingInfoDAO
                .getGTTSessionRecordingInfo(oTFSession.getId(), oTFSession.getMeetingId());
        if (gttSessionRecordingInfo == null) {
            gttSessionRecordingInfo = new GTTSessionRecordingInfo(oTFSession, downloadRecordings);
        } else {
            gttSessionRecordingInfo.setGttRecordings(downloadRecordings);
        }
        gttSessionRecordingInfoDAO.update(gttSessionRecordingInfo);
        logger.info("GTT session recording info : " + gttSessionRecordingInfo.toString());
    }

    public void createLinkSQS(GetOTFWebinarSessionsReq req) throws VException {
        Query query = new Query();
        if (req.getBeforeStartTime() == null || req.getAfterStartTime() == null) {
            logger.error("BadRequestException" + " not start or endtime");
            req.setBeforeStartTime(System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY);
            req.setAfterStartTime(System.currentTimeMillis());
        }
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gt(req.getAfterStartTime())
                .lt(req.getBeforeStartTime()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        for (OTFSession oTFSession : oTFSessions) {
            try {
                logger.info("create link for session" + oTFSession.getId());
                if (oTFSession.getStartTime() < System.currentTimeMillis()) {
                    logger.info("Session timing is in past id:" + oTFSession.getId());
                    continue;
                }

                if (!StringUtils.isEmpty(oTFSession.getSessionURL())) {
                    continue;
                }
                Map<String, Object> payload = new HashMap<>();
                payload.put("sessionId", oTFSession.getId());
                awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.CREATE_SESSION_LINK, gson.toJson(payload));

            } catch (Exception e) {
                logger.error("Error in create link for session Id " + oTFSession.getId() + e.getMessage());
            }
        }
    }

    // controller
    public UpdateSessionRes updateSession(OTFSession otfSession) throws VException {
        otfSession.validate();
        if (otfSession.getEndTime() - otfSession.getStartTime() > 5 * CommonCalendarUtils.MILLIS_PER_HOUR) {
            throw new BadRequestException(ErrorCode.MAX_SESSION_DURATION_EXCEEDED, "Session duration should not be more than 5 hours");
        }
        OTFSession result = otfSessionDAO.getById(otfSession.getId());
        boolean isVedantuWaveOTOSession = otfSession.isOtoSession();
        if (result == null) {
            logger.error("Session not found for id : " + otfSession.getId());
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + otfSession.getId());
        }

        logger.info("session update request received: getStartTime" + result.getStartTime() + "System.currentTimeMillis(): " + System.currentTimeMillis() + "60*DateTimeUtils.MILLIS_PER_MINUTE" + 60 * DateTimeUtils.MILLIS_PER_MINUTE);
        boolean isVedantuWaveSession = OTFSessionToolType.VEDANTU_WAVE.equals(result.getSessionToolType())
                || result.hasBigWhiteBoardFeature();
        if (isVedantuWaveSession && (System.currentTimeMillis() < result.getEndTime()) && (result.getStartTime() < (System.currentTimeMillis() + 60 * DateTimeUtils.MILLIS_PER_MINUTE))) {
            throw new ForbiddenException(ErrorCode.SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME, "SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME");
        }
        OTFSession preUpdateSession = gson.fromJson(gson.toJson(result), OTFSession.class);

        if (!OTFSessionManager.SUPPORTED_SESSION_TOOL_TYPES.contains(otfSession.getSessionToolType())) {
            throw new BadRequestException(ErrorCode.OTF_UNSUPPORTED_SESSION_TOOL,
                    "Unsupported session tool type provided. Supported types:"
                    + Arrays.toString(OTFSessionManager.SUPPORTED_SESSION_TOOL_TYPES.toArray()));
        }

        String newPresenter = otfSession.getPresenter();
        result.setPresenter(newPresenter);

        // check slot availability
        if (preUpdateSession.getPresenter().equals(otfSession.getPresenter())) {

            // if presenter is same, get ranges and check for conflicts
            List<SessionSlot> rangesToCheck = CommonCalendarUtils.getTimeIntervalsToCheckForConflicts(
                    preUpdateSession.getStartTime(), preUpdateSession.getEndTime(),
                    otfSession.getStartTime(), otfSession.getEndTime());
            for (SessionSlot slot : rangesToCheck) {
                checkSlotAvailabilityForUser(slot, newPresenter);
            }
        } else {
            // if presenter is different, check for entire slot
            checkSlotAvailabilityForUser(new SessionSlot(otfSession.getStartTime(), otfSession.getEndTime()),
                    newPresenter);
        }

        result.setPrimaryBatchId(otfSession.getPrimaryBatchId());
        result.setTitle(otfSession.getTitle());
        result.setDescription(otfSession.getDescription());
        result.setReplayUrls(otfSession.getReplayUrls());
        if (StringUtils.isEmpty(otfSession.getPresenter())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "presenter is null");
        }
        if (!otfSession.getPresenter().equalsIgnoreCase(result.getPresenter())) {
            Map<String, BatchBasicInfo> batchesMap = getBatchBasicInfosMapByIds(result.getBatchIds());
            String teacherId = otfSession.getPresenter();
            for (String batchId : batchesMap.keySet()) {
                BatchBasicInfo batchBasicInfo = batchesMap.get(batchId);
                if (!batchBasicInfo.getTeacherIds().contains(teacherId)) {
                    throw new BadRequestException(ErrorCode.TEACHER_NOT_PART_OF_BATCH, "teacher not part of batch " + batchId);
                }
            }

        }
        // logger.info(otfSession.getCurricullumTopics().toString());
        result.setCurricullumTopics(otfSession.getCurricullumTopics());
        result.setBaseTreeTopics(otfSession.getBaseTreeTopics());
        result.setPresenter(otfSession.getPresenter());
        result.setBoardId(otfSession.getBoardId());
        result.setOtmSessionType(otfSession.getOtmSessionType());
        if (otfSession.getState() != null) {
            result.setState(otfSession.getState());
        }
        result.setPreSessionContents(otfSession.getPreSessionContents());
        result.setPostSessionContents(otfSession.getPostSessionContents());
        result.setTaIds(otfSession.getTaIds());
        if (otfSession.hasVedantuWaveFeatures()) {
            result.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
        }

        result.setFlags(otfSession.getFlags());
        setEntityTypeForSession(result,result.getBatchIds(),otfSession.getType());

        if ((!(result.getStartTime().equals(otfSession.getStartTime())
                && result.getEndTime().equals(otfSession.getEndTime()))
                || !result.getSessionToolType().equals(otfSession.getSessionToolType())
                && otfSession.getSessionToolType() != null)
                && allowSessionUpdate(result.getStartTime())) {
            logger.info("updateSession update required for session with id : " + otfSession.getId());
            if (!isSlotAvailable(otfSession)) {
                throw new ConflictException(ErrorCode.SEAT_NOT_AVAILABLE, getTimeSlotStringArray(new ArrayList<>(Arrays.asList(otfSession))));
            }
            if (!StringUtils.isEmpty(result.getOrganizerAccessToken())) {
                descheduleSessionByBitSet(result);
            }
            otfSessionDAO.save(result);

            result.setStartTime(otfSession.getStartTime());
            if (!isVedantuWaveSession) {
                result.rescheduleToNewStartTime(otfSession.getStartTime(), otfSession.getEndTime());
            }
            result.setSessionToolType(otfSession.getSessionToolType());
            scheduleSessionByBitSet(result);
        }

        if (otfSession.isSimLiveSession()) {
            setupSimLiveSession(otfSession, result);
        }

        logger.info("updateSession : " + result);
        try {
//            if (otfSession.getType() != null) {
//                result.setType(otfSession.getType());
//            }

            result.setStartTime(otfSession.getStartTime());
            result.setEndTime(otfSession.getEndTime());
            result.setLabels(otfSession.getLabels());
            result.setEntityTags(otfSession.getEntityTags());
            if(isVedantuWaveOTOSession) {
                setAttendeesForWaveOTO( result );
            }
            otfSessionDAO.save(result);
            if (isVedantuWaveSession || otfSession.isOtoSession()) {
                List<OTFSession> otfSessionList = new ArrayList<>();
                otfSessionList.add(result);
                vedantuWaveSessionManager.sendSessionsToNodeForCaching(otfSessionList);
            }
        } catch (Exception ex) {
            logger.error("updateSession - Error updating the session : send to node caching failed " + otfSession);
        }

        if(result.isEarlyLearningSession() && result.isWebinarSession()) {
            try {
                logger.info("sending email to presenter");
                awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_TEACHER_CHANGE_SESSION_MAIL, gson.toJson(result));
            } catch (Exception ex) {
                logger.error("Error while sending Early Learning Demo session mail: " + otfSession + " error:" + ex.getMessage());
            }
        }

        if (result.getStartTime() != null && result.getStartTime() > System.currentTimeMillis()
                && result.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
            if (StringUtils.isEmpty(result.getMeetingId()) || StringUtils.isEmpty(result.getSessionURL())) {
                //delete existing Attenddee detail
                gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedBulk(otfSession.getId(), DeleteContext.SESSION_TIME_CHANGE);
                sendToQForGeneratingSessionLinkAndGTT(result);
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", result);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
            }
        }
        // Fetch batch
        try {
            Map<String, Object> payload = new HashMap<>();
            OTFSessionPojoUtils sessionInfo = mapper.map(result, OTFSessionPojoUtils.class);
            if (sessionInfo != null && preUpdateSession != null) {
                sessionInfo.setPreviousPresenter(preUpdateSession.getPresenter());
            }
            payload.put("sessionInfo", sessionInfo);
            payload.put("event", SessionEventsOTF.OTF_SESSION_RESCHEDULED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for session reschedule." + e);
        }

        if (preUpdateSession != null) {
            if (!preUpdateSession.getStartTime().equals(result.getStartTime())) {
                try {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("preUpdateSession", preUpdateSession);
                    payload.put("result", result);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.RESCHEDULE_SESSION_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                } catch (Exception e) {
                    logger.info("Error in async reschedule email. " + e);
                }
            }
        }

        if (!Objects.equals(preUpdateSession.getStartTime(), result.getStartTime())
                || !Objects.equals(preUpdateSession.getEndTime(), result.getEndTime())
                || !Objects.equals(preUpdateSession.getPresenter(), result.getPresenter())) {

            Map<String, Object> payload = new HashMap<>();
            payload.put("sessions", Arrays.asList(preUpdateSession));
            payload.put("fifoGrpId", preUpdateSession.getId());
            payload.put("mark", false);
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS_FIFO, SQSMessageType.INITIATE_ADDED_SESSIONS_MARK_CALENDAR, gson.toJson(payload), otfSession.getId());

            Map<String, Object> payloadnew = new HashMap<>();
            payloadnew.put("sessions", Arrays.asList(result));
            payloadnew.put("fifoGrpId", result.getId());
            payloadnew.put("mark", true);
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS_FIFO, SQSMessageType.INITIATE_ADDED_SESSIONS_MARK_CALENDAR, gson.toJson(payloadnew), otfSession.getId());

            Map<String, String> gttPayload = new HashMap<>();
            gttPayload.put("otf_session", gson.toJson(result));
            awsSQSManager.sendToSQS(SQSQueue.GTT_UPDATE_OPS_FIFO, SQSMessageType.GTT_SESSION_TIME_CHANGE_TASK, gson.toJson(gttPayload), result.getId());

        }

        return new UpdateSessionRes(preUpdateSession, result);
    }

    public static boolean allowSessionUpdate(long startTime) {
        long currentTime = System.currentTimeMillis();
        if (startTime < (currentTime + sessionUpdateAllowedMin * DateTimeUtils.MILLIS_PER_MINUTE)) {
            return false;
        } else {
            return true;
        }
    }

    public List<OTFSessionPojo> addOTFSessionsToBatch(AddOTFSessionsReq req) throws VException { // here
        req.verify();
        List<OTFSessionPojo> sessions = new ArrayList<>();

        Set<String> reqBatchIds = req.getBatchIds();

        Map<String, BatchBasicInfo> batchesMap = getBatchBasicInfosMapByIds(reqBatchIds);
        Long globalMinBatchStart = null;
        for (String batchId : reqBatchIds) {
            BatchBasicInfo batch = batchesMap.get(batchId);
            if (batch == null) {
                String errorString = "Batch not found for id : " + batchId + ". Request : "
                        + req.toString();
                logger.info(errorString);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
            }

            if (globalMinBatchStart != null) {
                if (globalMinBatchStart > batch.getStartTime()) {
                    globalMinBatchStart = batch.getStartTime();
                }
            }

            if (globalMinBatchStart == null) {
                globalMinBatchStart = batch.getStartTime();
            }

            if (EntityStatus.INACTIVE.equals(batch.getBatchState())) {
                throw new IllegalArgumentException(
                        "Batch is not active : " + batch.toString() + ". Request : " + req.toString());
            }
        }
        List<String> topics = new ArrayList<String>();
        List<OTFSession> parsedSessions = new ArrayList<>();
        List<OTFSession> errorSessions = new ArrayList<>();

        for (OTFSessionPojoUtils pojo : req.getSessionPojo()) {
            OTFSession otfSession = mapper.map(pojo, OTFSession.class);
            if (otfSession.getCurricullumTopics() != null) {
                topics.addAll(otfSession.getCurricullumTopics());
            }
            Long minBatchStartTime = null;
            Set<String> batchIds = otfSession.getBatchIds();
            boolean isVedantuWaveSession = OTFSessionToolType.VEDANTU_WAVE.equals(otfSession.getSessionToolType()) || otfSession.isOtoSession();
            boolean isSimLiveSession = isVedantuWaveSession && otfSession.isSimLiveSession();
            boolean isVedantuWaveOTOSession = otfSession.isOtoSession();
            if (isSimLiveSession) {
                setupSimLiveSession(otfSession, null);
            }

            if (isVedantuWaveSession) {
                otfSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
                if (OTMSessionType.DEMO.equals(otfSession.getOtmSessionType()) || isSimLiveSession) {
                    otfSession.addLabels(SessionLabel.WEBINAR);
                }
            }
            if (isVedantuWaveSession && (otfSession.getStartTime() < (System.currentTimeMillis() + 60 * DateTimeUtils.MILLIS_PER_MINUTE))) {
                throw new ForbiddenException(ErrorCode.SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME, "SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME");
            }
            if (ArrayUtils.isEmpty(batchIds)) {
                if (ArrayUtils.isEmpty(reqBatchIds)) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "no batchIds for " + otfSession);
                }
                otfSession.setBatchIds(reqBatchIds);

                if (otfSession.getStartTime() < globalMinBatchStart) {
                    throw new BadRequestException(ErrorCode.SESSION_START_TIME_LESS_THAN_BATCH_START_TIME, "Session can't have start time less than batch startTime");
                }

            } else {
                Set<String> sessionBatchIds = new HashSet<>();
                for (String batchId : otfSession.getBatchIds()) {
                    if (batchesMap.containsKey(batchId)) {

                        if (minBatchStartTime == null) {
                            minBatchStartTime = batchesMap.get(batchId).getStartTime();
                        }

                    } else {
                        sessionBatchIds.add(batchId);
                    }
                }

                Map<String, BatchBasicInfo> tempBatchMap = getBatchBasicInfosMapByIds(sessionBatchIds);
                batchesMap.putAll(tempBatchMap);

                for (String batchId : otfSession.getBatchIds()) {
                    BatchBasicInfo basicInfo = batchesMap.get(batchId);
                    if (basicInfo == null) {
                        String errorString = "Batch not found for id : " + batchId + ". Request : "
                                + req.toString();
                        logger.info(errorString);
                        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
                    }

                    if (minBatchStartTime == null) {
                        minBatchStartTime = basicInfo.getStartTime();
                    }

                    if (minBatchStartTime > basicInfo.getStartTime()) {
                        minBatchStartTime = basicInfo.getStartTime();
                    }

                }

                if (otfSession.getStartTime() < minBatchStartTime) {
                    throw new BadRequestException(ErrorCode.SESSION_START_TIME_LESS_THAN_BATCH_START_TIME, "Session can't have start time less than batch startTime");
                }

            }
            if (isVedantuWaveOTOSession) {
                logger.info("isVedantuWaveOTOSession");
                setAttendeesForWaveOTO(otfSession);
                // using agora as default
                // try {
                //     // vedantuWaveScheduleManager.changeServiceProviderToImpartus(otfSession);
                // } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException | IOException e) {
                //     logger.error("Error occured while changing the serviceProvier to impartus for sessionId: " + otfSession.getId());
                //     e.printStackTrace();
                // }
            }

            otfSession.setState(SessionState.SCHEDULED);
            setEntityTypeForSession(otfSession, otfSession.getBatchIds(), otfSession.getType());


            if (otfSession.getSessionToolType() == null) {
                otfSession.setSessionToolType(OTFSessionToolType.VEDANTU_WAVE);
            }

            List<String> errors = otfSession.validate();
            if (!errors.isEmpty()) {
                String errorString = "Invalid request for AddBatchSessionReqError : " + gson.toJson(req) + ". Errors : "
                        + CommonUtils.getListString(errors);
                logger.info(errorString);
                throw new IllegalArgumentException(errorString);
            }

            // Schedule session
            if (!allowSessionUpdate(otfSession.getStartTime()) || !StringUtils.isEmpty(otfSession.getId())) {
                throw new IllegalArgumentException("Session creation is not allowed for the requested session : "
                        + req.toString()
                        + ". Session creation is only allowed one day before the session start time and no session update is allowed.");
            }
            if (!isSlotAvailable(otfSession)) {
                errorSessions.add(otfSession);
            }
            parsedSessions.add(otfSession);
        }
        if (ArrayUtils.isNotEmpty(errorSessions)) {
            throw new ConflictException(ErrorCode.SEAT_NOT_AVAILABLE, getTimeSlotStringArray(errorSessions));
        }
        if (!topics.isEmpty()) {
            for (String batchId : reqBatchIds) {
                List<String> curricullumTopics = getTopicsByEntity(batchId, CurriculumEntityName.OTF_BATCH);
                validateSessionTopics(topics, curricullumTopics);
            }
        }
        List<SessionSnapshot> sessionSnapshots = new ArrayList<>();
        for (OTFSession otfSession : parsedSessions) {
            scheduleSessionByBitSet(otfSession);
            logger.info("addOTFSessionsToBatch saving session: " + otfSession);
            otfSessionDAO.save(otfSession);
            SessionSnapshot sessionSnapshot = new SessionSnapshot(otfSession.getId(), otfSession.getStartTime(),
                    otfSession.getEndTime(), new ArrayList<String>(otfSession.getBatchIds()), otfSession.getOtmSessionType());

            sessionSnapshots.add(sessionSnapshot);
            sessions.add(getSessionInfo(otfSession, null));
            if (otfSession.getStartTime() != null
                    && otfSession.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
                if (StringUtils.isEmpty(otfSession.getMeetingId())
                        || StringUtils.isEmpty(otfSession.getSessionURL())) {
                    /*Map<String, Object> payload = new HashMap<>();
                    payload.put("session", otfSession);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                    asyncTaskFactory.executeTask(params);*/
                    sendToQForGeneratingSessionLinkAndGTT(otfSession);
                }
            }
            if (otfSession.isExtraTypeSession()) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("session", otfSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTM_EXTRA_CLASS_EMAIL, payload);
                asyncTaskFactory.executeTask(params);
            }
        }

        parsedSessions.sort(Comparator.comparing(OTFSession::getStartTime));

        if (ArrayUtils.isNotEmpty(parsedSessions)) {
            OTFSession oTFSession = parsedSessions.get(0);
            try {
                Map<String, Object> payload = new HashMap<>();
                payload.put("sessionInfo", mapper.map(oTFSession, OTFSessionPojo.class));
                payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
                logger.info(payload);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
                asyncTaskFactory.executeTask(params);
            } catch (Exception e) {
                logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
            }
        }

        // async-- mark calendar for all added sessions
        Map<String, Object> payload = new HashMap<>();
        payload.put("sessions", parsedSessions);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ADDED_SESSIONS_MARK_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);

        updateSnapshot(sessionSnapshots);

        return sessions;
    }

    public void setEntityTypeForSession(OTFSession otfSession, Set<String> batchIds,
                                        EntityType reqEntityType) throws VException {
        // -- If FE sends type and its standard, no computation to be done
        if(Objects.nonNull(reqEntityType) && reqEntityType.equals(EntityType.STANDARD)) {
            otfSession.setType(EntityType.STANDARD);
            return;
        }

        if (reqEntityType == EntityType.TWO_TEACHER_MODEL) {
            if (isTwoTeacherApplicable(batchIds)) {
                otfSession.setType(EntityType.TWO_TEACHER_MODEL);
            } else {
                throw new BadRequestException(ErrorCode.TWO_TEACHER_NOT_POSSIBLE_FOR_MIXED_BATCHES, "For TWO_TEACHER_MODEL session, all batches should have sections");
            }
        } else if(reqEntityType == EntityType.OTF_SESSION){
            if (isOtfSessionApplicable(batchIds)) {
                otfSession.setType(EntityType.OTF_SESSION);
            } else {
                throw new BadRequestException(ErrorCode.OTF_SESSION_NOT_POSSIBLE_IN_SECTIONAL_BATCH, "For OTF_SESSION, all batches should be non-sectional");
            }
        } else if (reqEntityType != null) {
            otfSession.setType(reqEntityType);
        } else {
            otfSession.setType(EntityType.STANDARD);
        }
    }

    private Boolean isTwoTeacherApplicable(Set<String> batchIds) throws VException {
        logger.info("In_isTwoTeacherApplicable batchIds {}",batchIds);
        List<String> bIds = new ArrayList<>(batchIds);
        if(bIds.isEmpty())
            return false;

        BatchSectionReq req = new BatchSectionReq();
        req.setBatchIds(bIds);
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "section/isTwoTeacherModelApplicable?",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("jsonString {}",jsonString);

        PlatformBasicResponse platformBasicResponse = gson.fromJson(jsonString,PlatformBasicResponse.class);
        logger.info("isSuccess {}",platformBasicResponse.isSuccess());
        return platformBasicResponse.isSuccess();
    }

    private Boolean isOtfSessionApplicable(Set<String> batchIds) throws VException {
        logger.info("isOtfSessionApplicable_batchIds {}",batchIds);
        List<String> bIds = new ArrayList<>(batchIds);
        if(bIds.isEmpty())
            return false;

        BatchSectionReq req = new BatchSectionReq();
        req.setBatchIds(bIds);
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "section/isOtfSessionApplicable?",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("jsonString {}",jsonString);

        PlatformBasicResponse platformBasicResponse = gson.fromJson(jsonString,PlatformBasicResponse.class);
        logger.info("isSuccess {}",platformBasicResponse.isSuccess());
        return platformBasicResponse.isSuccess();
    }

    public void updateSnapshot(List<SessionSnapshot> sessionSnapshots) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/addSnapshots/",
                HttpMethod.POST, gson.toJson(sessionSnapshots));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

    }

    public Map<String, OTFSessionStats> getOTFSessionStats(List<String> batchIds, Long timestamp) {

        Map<String, OTFSessionStats> otfSessionStatsMap = new HashMap<>();
        List<OTFSession> otfSessionList = otfSessionDAO.getOTFSessionsForBatches(batchIds, null, null);

        for (OTFSession otfSession : otfSessionList) {
            if (ArrayUtils.isEmpty(otfSession.getBatchIds())) {
                continue;
            }
            for (String batchId : otfSession.getBatchIds()) {
                OTFSessionStats otfSessionStats;
                if (!otfSessionStatsMap.containsKey(batchId)) {
                    otfSessionStats = new OTFSessionStats();
                    otfSessionStats.setSessionsPlanned(0);
                    otfSessionStats.setExtraSessions(0);
                    otfSessionStats.setSessionsCancelled(0);
                    otfSessionStats.setSessionsRescheduled(0);
                    otfSessionStats.setSessionsDone(0);
                    otfSessionStats.setSessionsScheduled(0);

                } else {
                    otfSessionStats = otfSessionStatsMap.get(batchId);
                }

                otfSessionStats.incSessionsPlanned(1);
                if (otfSession.getStartTime() > timestamp && SessionState.SCHEDULED.equals(otfSession.getState())) {
                    otfSessionStats.incSessionsScheduled(1);
                }
                if (otfSession.getStartTime() < timestamp && SessionState.SCHEDULED.equals(otfSession.getState())) {
                    otfSessionStats.incSessionsDone(1);
                }

                if (SessionState.CANCELLED.equals(otfSession.getState()) || SessionState.FORFEITED.equals(otfSession.getState())) {
                    otfSessionStats.incSessionsCancelled(1);
                }

                if (otfSession.getRescheduledFromId() != null || otfSession.getRescheduleData().size() > 0) {
                    otfSessionStats.incSessionsRescheduled(1);
                }

                if (otfSession.isExtraTypeSession()) {

                    otfSessionStats.incExtraSessions(1);
                }

                otfSessionStatsMap.put(batchId, otfSessionStats);
            }
        }

        return otfSessionStatsMap;

    }

    public void descheduleSessions(String batchId) throws VException {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));

        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);
        if (oTFSessions != null) {
            List<MarkCalendarSessionPojo> sessionPojosToRemoveOnlyThisBatchCalendarSlots = new ArrayList<>();
            for (OTFSession oTFSession : oTFSessions) {
                logger.info("PreSessionDescheduled : " + oTFSession.toString());
                if (oTFSession.getStartTime() < System.currentTimeMillis()) {
                    logger.info("Session on going or is in the past");
                    continue;
                }
                Set<String> batchIds = oTFSession.getBatchIds();
                if (batchIds.size() > 1) {
                    batchIds.remove(batchId);
                    MarkCalendarSessionPojo sessionPojo = mapper.map(oTFSession, MarkCalendarSessionPojo.class);
                    sessionPojosToRemoveOnlyThisBatchCalendarSlots.add(sessionPojo);
                    otfSessionDAO.save(oTFSession);
                    continue;
                } else {
                    //since this session has only this batch, we will unblock everyones calendar in this session/batch
                    Map<String, Object> payloadunmark = new HashMap<>();
                    payloadunmark.put("sessions", Arrays.asList(oTFSession));
                    payloadunmark.put("mark", false);
                    awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.INITIATE_ADDED_SESSIONS_MARK_CALENDAR, gson.toJson(payloadunmark));
                }

                if (!StringUtils.isEmpty(oTFSession.getOrganizerAccessToken())
                        && oTFSession.getStartTime() > System.currentTimeMillis()) {
                    String organizerAccessToken = oTFSession.getOrganizerAccessToken();
                    try {
                        descheduleSessionByBitSet(oTFSession);
                    } catch (Exception ex) {
                        logger.error("InvalidOrganizerAccessToken for organzier token : " + organizerAccessToken
                                + " session : " + oTFSession);
                    }
                }

                oTFSession.setState(SessionState.CANCELLED);

                // Unnecessary additional checks
                oTFSession.setMeetingId(null);
                oTFSession.setSessionURL(null);
                oTFSession.setPresenterUrl(null);
                logger.info("PostSessionDescheduled : " + oTFSession.toString());
                otfSessionDAO.save(oTFSession);
            }
            MarkCalendarBatchPojo pojo = new MarkCalendarBatchPojo();
            pojo.setBatchId(batchId);
            pojo.setMark(false);
            pojo.setSessions(sessionPojosToRemoveOnlyThisBatchCalendarSlots);
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_OPS, SQSMessageType.MARK_CALENDAR_BATCH_TASK, gson.toJson(pojo));
        }
    }

    public Map<String, List<OTFAttendance>> getAttendanceForBatch(String batchId, Long beforeEndTime)
            throws BadRequestException, NotFoundException {
        if (StringUtils.isEmpty(batchId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchId is null");
        }

        Map<String, List<OTFAttendance>> mapIdToAttendance = new HashMap<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        if (beforeEndTime == null || beforeEndTime < 0L) {
            beforeEndTime = System.currentTimeMillis();
        }
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(beforeEndTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).exists(true));

        query.fields().include(OTFSession.Constants.BATCH_IDS);
        query.fields().include(OTFSession.Constants.ID);
        query.fields().include(OTFSession.Constants.END_TIME);
        query.fields().include(OTFSession.Constants.STATE);
        query.fields().include(OTFSession.Constants.PRESENTER);
        query.fields().include(OTFSession.Constants.TITLE);
        query.fields().include(OTFSession.Constants.ATTENDEES);
        query.fields().include(OTFSession.Constants.BOARD_ID);
        query.fields().include(OTFSession.Constants.MEETING_ID);
        query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        //
        if (ArrayUtils.isEmpty(oTFSessions)) {
            return mapIdToAttendance;
        }
        List<String> sessionIds = new ArrayList<>();
        List<String> meetingIds = new ArrayList<>();
        Map<String, Long> titles = new HashMap<>();
        Map<Long, Integer> total_count = new HashMap<>();
        Set<String> top_5 = new HashSet<>();
        Set<String> userIds = new HashSet<>();
        Map<Long, String> subjectTeacher = new HashMap<>();
        for (OTFSession oTFSession : oTFSessions) {
            if (oTFSession.getBoardId() != null) {
                sessionIds.add(oTFSession.getId());
                if (!StringUtils.isEmpty(oTFSession.getMeetingId())) {
                    meetingIds.add(oTFSession.getMeetingId());

                    titles.put(oTFSession.getId(), oTFSession.getBoardId());
                    if (oTFSession.getAttendees() != null) {
                        userIds.addAll(oTFSession.getAttendees());
                    }
                    if (total_count.containsKey(oTFSession.getBoardId())) {
                        total_count.put(oTFSession.getBoardId(), total_count.get(oTFSession.getBoardId()) + 1);
                    } else {
                        total_count.put(oTFSession.getBoardId(), 1);
                    }
                    if (total_count.get(oTFSession.getBoardId()) < 5) {
                        top_5.add(oTFSession.getId());
                    }
                }
                subjectTeacher.put(oTFSession.getBoardId(), oTFSession.getPresenter());
            }
        }

        List<GTTAttendeeDetails> attendeeDetails = getAttendanceForMeetingIds(meetingIds);

        Map<String, HashMap<Long, OTFAttendance>> attendanceMap = new HashMap<>();
        for (String userId : userIds) {
            for (Entry<Long, Integer> entry_temp : total_count.entrySet()) {
                OTFAttendance otfAttendance = new OTFAttendance();
                otfAttendance.setBoardId(entry_temp.getKey());
                otfAttendance.setTotal(entry_temp.getValue());
                otfAttendance.setTeacherId(Long.parseLong(subjectTeacher.get(entry_temp.getKey())));
                HashMap<Long, OTFAttendance> temp = new HashMap<>();
                temp.put(entry_temp.getKey(), otfAttendance);
                attendanceMap.put(userId, temp);
            }
        }

        if (ArrayUtils.isNotEmpty(attendeeDetails)) {

            for (GTTAttendeeDetails attendeeDetail : attendeeDetails) {
                Long title = titles.get(attendeeDetail.getSessionId());
                HashMap<Long, OTFAttendance> temp;
                Boolean top = top_5.contains(attendeeDetail.getSessionId());
                if (attendanceMap.containsKey(attendeeDetail.getUserId())) { // 2
                    // conditions
                    temp = attendanceMap.get(attendeeDetail.getUserId());
                    OTFAttendance otfAttendance;
                    if (temp.containsKey(title)) {
                        otfAttendance = temp.get(title);
                        otfAttendance.setAttended(otfAttendance.getAttended() + 1);
                    } else {
                        otfAttendance = new OTFAttendance();
                        otfAttendance.setBoardId(title);
                        otfAttendance.setAttended(otfAttendance.getAttended() + 1);
                        otfAttendance.setTotal(total_count.get(title));
                    }
                    if (top) {
                        otfAttendance.setLastFive(otfAttendance.getLastFive() + 1);
                    }
                    temp.put(title, otfAttendance);

                } else {
                    temp = new HashMap<>();
                    OTFAttendance attendance = new OTFAttendance();
                    attendance.setBoardId(title);
                    attendance.setAttended(attendance.getAttended() + 1);
                    attendance.setTotal(total_count.get(title));
                    temp.put(title, attendance);
                }
                attendanceMap.put(attendeeDetail.getUserId(), temp);
            }
        }
        for (Entry<String, HashMap<Long, OTFAttendance>> entry : attendanceMap.entrySet()) {
            List<OTFAttendance> list = new ArrayList<>(entry.getValue().values());
            mapIdToAttendance.put(entry.getKey(), list);
        }
        return mapIdToAttendance;
    }

    public List<GTTAttendeeDetails> getAttendanceForSessions(List<String> sessionIds, Set<String> userIds) {
        if (ArrayUtils.isEmpty(sessionIds)) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        if (ArrayUtils.isNotEmpty(userIds)) {
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(userIds));
        }
        query.addCriteria(Criteria.where("joinTimes.0").exists(true));
        query.fields().include(GTTAttendeeDetails.Constants.SESSION_ID);
        query.fields().include(GTTAttendeeDetails.Constants.USER_ID);
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getAttendanceForMeetingIds(List<String> meetingIds) {
        if (ArrayUtils.isEmpty(meetingIds)) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).in(meetingIds));

        query.addCriteria(Criteria.where("joinTimes.0").exists(true));
        query.fields().include(GTTAttendeeDetails.Constants.SESSION_ID);
        query.fields().include(GTTAttendeeDetails.Constants.USER_ID);
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<OTFSessionPojo> viewAttendanceForUser(ViewAttendanceForUserBatchReq req) throws BadRequestException {
        String userId = req.getUserId();
        String batchId = req.getBatchId();

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(batchId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId or batchId is null");
        }
        List<OTFSessionPojo> results = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        otfSessionDAO.setFetchParameters(query, req);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(query, OTFSession.class);

        List<String> sessionIds = new ArrayList<>();

        if (ArrayUtils.isEmpty(oTFSessions)) {
            return results;
        }
        for (OTFSession oTFSession : oTFSessions) {
            sessionIds.add(oTFSession.getId());
        }
        Query gttquery = new Query();
        gttquery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        gttquery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        gttquery.addCriteria(Criteria.where("joinTimes.0").exists(true));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(attendeeDetails)) {
            for (GTTAttendeeDetails gttAttendeeDetails : attendeeDetails) {
                attendeeMap.put(gttAttendeeDetails.getSessionId(), gttAttendeeDetails);
            }
        }
        for (OTFSession oTFSession : oTFSessions) {
            OTFSessionAttendeeInfo sessionAttendeeInfo = new OTFSessionAttendeeInfo(userId);
            if (attendeeMap.containsKey(oTFSession.getId())) {
                GTTAttendeeDetails attendeeDetail = attendeeMap.get(oTFSession.getId());
                sessionAttendeeInfo.setJoinTimes(attendeeDetail.getJoinTimes());
                sessionAttendeeInfo.setTimeInSession(attendeeDetail.getTimeInSession());
                sessionAttendeeInfo.setActiveIntervals(attendeeDetail.getActiveIntervals());
            }
            OTFSessionPojo oTFSessionPojo = getSessionInfo(oTFSession, null);
            oTFSessionPojo.setAttendeeInfos(Arrays.asList(sessionAttendeeInfo));
            results.add(oTFSessionPojo);
        }
        return results;
    }

    public Map<Long, TeacherSessionDashboard> getTeacherSessionForDashboard(Long startTime, Long endTime) {
        List<OTFSession> oTFSessions = new ArrayList<>();
        List<String> sessionIds = new ArrayList<>();
        int start = 0, limit = 100;
        Long time_5 = new Long(DateTimeUtils.MILLIS_PER_MINUTE * 5);
        Map<Long, TeacherSessionDashboard> teacherSessionCountMap = new HashMap<>();
        while (true) {
            Query query = new Query();
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(startTime).lt(endTime));
            query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
            query.fields().include(OTFSession.Constants._ID);
            query.fields().include(OTFSession.Constants.ID);
            query.fields().include(OTFSession.Constants.PRESENTER);
            query.fields().include(OTFSession.Constants.ATTENDEES);
            query.fields().include(OTFSession.Constants.STATE);
            query.fields().include(OTFSession.Constants.START_TIME);
            query.fields().include(OTFSession.Constants.TEACHER_JOIN_TIME);
            query.fields().include(OTFSession.Constants.RESCHEDULE_DATA);
            query.fields().include(OTFSession.Constants.CANCELLED_BY);
            otfSessionDAO.setFetchParameters(query, start, limit);
            query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.LAST_UPDATED));
            List<OTFSession> sessionsTemp = (otfSessionDAO.runQuery(query, OTFSession.class));
            start = start + limit;
            if (ArrayUtils.isNotEmpty(sessionsTemp)) {
                oTFSessions.addAll(sessionsTemp);
            } else {
                break;
            }
        }
        if (ArrayUtils.isEmpty(oTFSessions)) {
            return teacherSessionCountMap;
        }

        for (OTFSession oTFSession : oTFSessions) {
            sessionIds.add(oTFSession.getId());
        }
        Map<String, List<String>> sessionIdGTTAttendeeMap = getAttendeeListForTeacherDashboard(sessionIds);
        for (OTFSession oTFSession : oTFSessions) {
            Long teacherId = Long.parseLong(oTFSession.getPresenter());
            TeacherSessionDashboard teacherSessionDashboard;
            List<String> attendees = null;
            if (sessionIdGTTAttendeeMap != null) {
                attendees = sessionIdGTTAttendeeMap.get(oTFSession.getId());
            }
            if (teacherSessionCountMap.containsKey(teacherId)) {
                teacherSessionDashboard = teacherSessionCountMap.get(teacherId);
            } else {
                teacherSessionDashboard = new TeacherSessionDashboard();
                teacherSessionDashboard.setTeacherId(teacherId);
                teacherSessionDashboard.setAttendees(new HashSet<String>());
            }
            // TODO : after reschedule in master
            if (oTFSession.getTeacherJoinTime() != null && oTFSession.getStartTime() != null
                    && (oTFSession.getTeacherJoinTime() - oTFSession.getStartTime()) > time_5) {
                teacherSessionDashboard.setLateJoined(teacherSessionDashboard.getLateJoined() + 1);
            }
            int state = 0;
            if (ArrayUtils.isNotEmpty(attendees)) {
                teacherSessionDashboard.getAttendees().addAll(attendees);

                for (String id : attendees) {
                    if (id.equalsIgnoreCase(oTFSession.getPresenter())) {
                        state = 1;// normal
                        break;
                    }
                }
                if (attendees.size() == 1 && state == 1) {
                    state = 2;// only teacher showed up
                }
                if (state == 0) {
                    state = 3;// teacher no show
                }
            } else {
                state = 4;// no one showed up
            }
            incrementTeacherSessionDashboard(teacherSessionDashboard, oTFSession, state);
            teacherSessionCountMap.put(teacherId, teacherSessionDashboard);
        }

        return teacherSessionCountMap;
    }

    public Map<String, List<String>> getAttendeeListForTeacherDashboard(List<String> sessionIds) {

        Map<String, List<String>> sessionIdGTTAttendeeMap = new HashMap<>();
        Criteria criteria = new Criteria();
        Criteria sessionCriteria = Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds);
        Criteria joinTimeCriteria = Criteria.where("joinTimes.0").exists(true);
        GroupOperation attendeeSet = Aggregation.group(GTTAttendeeDetails.Constants.SESSION_ID)
                .addToSet(GTTAttendeeDetails.Constants.USER_ID).as("attendees");
        criteria.andOperator(sessionCriteria, joinTimeCriteria);
        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = Aggregation.newAggregation(Aggregation.match(criteria), attendeeSet,
                Aggregation.project("attendees").and(GTTAttendeeDetails.Constants.SESSION_ID).previousOperation()).withOptions(aggregationOptions);
        AggregationResults<SessionGTTAttendeePojo> groupResults = gttAttendeeDetailsDAO.getMongoOperations()
                .aggregate(agg, GTTAttendeeDetails.class.getSimpleName(), SessionGTTAttendeePojo.class);
        List<SessionGTTAttendeePojo> results = groupResults.getMappedResults();
        if (ArrayUtils.isNotEmpty(results)) {
            for (SessionGTTAttendeePojo pojo : results) {
                if (ArrayUtils.isNotEmpty(pojo.getAttendees())) {
                    sessionIdGTTAttendeeMap.put(pojo.getSessionId(), pojo.getAttendees());
                }
            }
        }
        return sessionIdGTTAttendeeMap;
    }

    public void incrementTeacherSessionDashboard(TeacherSessionDashboard teacherSessionDashboard, OTFSession oTFSession,
            Integer state) {

        if (ArrayUtils.isNotEmpty(oTFSession.getRescheduleData())) {
            teacherSessionDashboard
                    .setRescheduled(teacherSessionDashboard.getRescheduled() + oTFSession.getRescheduleData().size());
        }

        switch (oTFSession.getState()) {
            case SCHEDULED:
                teacherSessionDashboard.setBooked(teacherSessionDashboard.getBooked() + 1);
                switch (state) {
                    case 1:
                        teacherSessionDashboard.setEnded(teacherSessionDashboard.getEnded() + 1);
                        break;
                    case 2:
                        teacherSessionDashboard.setForfeited(teacherSessionDashboard.getForfeited() + 1);
                        break;
                    case 3:
                        teacherSessionDashboard.setTeacherNoShow(teacherSessionDashboard.getTeacherNoShow() + 1);
                        break;
                    case 4:
                        teacherSessionDashboard.setExpired(teacherSessionDashboard.getExpired() + 1);
                        break;
                    default:
                        break;
                }
                break;
            case CANCELLED:
                if (!StringUtils.isEmpty(oTFSession.getCancelledBy())) {
                    if (oTFSession.getCancelledBy().equalsIgnoreCase(oTFSession.getPresenter())) {
                        teacherSessionDashboard.setCancelledByTeacher(teacherSessionDashboard.getCancelledByTeacher() + 1);
                    } else if (ArrayUtils.isNotEmpty(oTFSession.getAttendees())
                            && oTFSession.getAttendees().contains(oTFSession.getCancelledBy())) {
                        teacherSessionDashboard.setCancelledByStudent(teacherSessionDashboard.getCancelledByStudent() + 1);
                    } else {
                        teacherSessionDashboard.setCancelledByAdmin(teacherSessionDashboard.getCancelledByAdmin() + 1);
                    }
                }
                teacherSessionDashboard.setCancelled(teacherSessionDashboard.getCancelled() + 1);
                teacherSessionDashboard.setBooked(teacherSessionDashboard.getBooked() + 1);
                break;
            default:
                break;
        }
    }

    public List<OTFSessionForUserRes> getSessionsForUser(GetOTFSessionsListReq req) throws ForbiddenException, BadRequestException, VException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Forbidden access");
        }

        if (StringUtils.isEmpty(req.getBatchId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchId is missing");
        }

        if (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole())) {
            return getUsersOTFSessions(req);
        }
        GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
        enrolReq.setBatchId(req.getBatchId());
        enrolReq.setUserId(sessionData.getUserId());
        enrolReq.setIncludeFields(Arrays.asList(
                EnrollmentPojo.Constants.STATUS,
                EnrollmentPojo.Constants.LAST_UPDATED
        ));
        enrolReq.setExcludeFields(Arrays.asList(
                EnrollmentPojo.Constants._ID
        ));
        Long latestInactiveTime = null;
        List<EnrollmentPojo> enrollments = getEnrollmentsForUser(enrolReq);
        List<OTFSessionForUserRes> oTFSessionPojos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(enrollments)) {
            Collections.sort(enrollments, Comparator.comparing(EnrollmentPojo::getLastUpdated));
            for (EnrollmentPojo enrollmentPojo : enrollments) {
                if (EntityStatus.ACTIVE.equals(enrollmentPojo.getStatus())) {
                    latestInactiveTime = null;
                } else {
                    if (latestInactiveTime == null) {
                        latestInactiveTime = enrollmentPojo.getLastUpdated();
                    } else if (latestInactiveTime < enrollmentPojo.getLastUpdated()) {
                        latestInactiveTime = enrollmentPojo.getLastUpdated();
                    }
                }
            }
            if (latestInactiveTime != null) {
                req.setTillTime(latestInactiveTime);
            }
            oTFSessionPojos = getUsersOTFSessions(req);
        }
        return oTFSessionPojos;
    }

    public List<OTFSessionBasicInfo> getOTFSessionsBasicsInfos(GetOTFSessionsListReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, VException {
        logger.info("getSession fetch :", req.toString());
        List<OTFSessionPojo> sessions = getOTFSessions(req);
        List<OTFSessionBasicInfo> result = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSessionPojo pojo : sessions) {
                result.add(mapper.map(pojo, OTFSessionBasicInfo.class));
            }
        }
        return result;
    }

    public List<OTFSessionPojo> getOTFSessions(GetOTFSessionsListReq req) {
        List<OTFSessionPojo> result = new ArrayList<>();
        Query sessionQuery = new Query();
        if (req.getFromTime() != null && req.getFromTime() > 0L) {
            if (req.getTillTime() != null && req.getTillTime() > 0L) {
                sessionQuery.addCriteria(
                        Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()).lt(req.getTillTime()));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()));
            }
        } else if (req.getTillTime() != null && req.getTillTime() > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(req.getTillTime()));
        }

        if (!StringUtils.isEmpty(req.getBatchId())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(req.getBatchId()));
        }

        if (req.getTeacherId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(req.getTeacherId().toString()));
        }

        if (req.getBoardId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(req.getBoardId()));
        }
        if (ArrayUtils.isNotEmpty(req.getStates())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(req.getStates()));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        }

        if (req.getSortOrder() != null) {
            sessionQuery.with(Sort.by(Direction.valueOf(req.getSortOrder().name()), OTFSession.Constants.END_TIME));
        } else {
            sessionQuery.with(Sort.by(Direction.ASC, OTFSession.Constants.END_TIME));
        }
        sessionQuery.fields().exclude(OTFSession.Constants.ATTENDEES);
        otfSessionDAO.setFetchParameters(sessionQuery, req);
        List<OTFSession> sessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        if (ArrayUtils.isNotEmpty(sessions)) {
            result = getSessionInfos(sessions, null);
        }
        return result;
    }

    public List<OTFSessionForUserRes> getUsersOTFSessions(GetOTFSessionsListReq req) {
        List<OTFSessionForUserRes> result = new ArrayList<>();
        Query sessionQuery = new Query();
        if (req.getFromTime() != null && req.getFromTime() > 0L) {
            if (req.getTillTime() != null && req.getTillTime() > 0L) {
                sessionQuery.addCriteria(
                        Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()).lt(req.getTillTime()));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()));
            }
        } else if (req.getTillTime() != null && req.getTillTime() > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(req.getTillTime()));
        }

        if (!StringUtils.isEmpty(req.getBatchId())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(req.getBatchId()));
        }

        if (req.getTeacherId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(req.getTeacherId().toString()));
        }

        if (req.getBoardId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(req.getBoardId()));
        }
        if (ArrayUtils.isNotEmpty(req.getStates())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(req.getStates()));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        }
        if (req.getSortOrder() != null) {
            sessionQuery.with(Sort.by(Direction.valueOf(req.getSortOrder().name()), OTFSession.Constants.END_TIME));
        } else {
            sessionQuery.with(Sort.by(Direction.ASC, OTFSession.Constants.END_TIME));
        }

        otfSessionDAO.setFetchParameters(sessionQuery, req);
        List<OTFSession> sessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        if (ArrayUtils.isNotEmpty(sessions)) {
            sessions.forEach(session -> result.add(mapper.map(session, OTFSessionForUserRes.class)));
        }
        return result;
    }

    public List<OTFSessionPojo> getOtfSessionsForDailyStats(GetOTFSessionsListReq req) {

        List<OTFSessionPojo> result = new ArrayList<>();
        Long fromTime = req.getFromTime();
        Long tillTime = req.getTillTime();
        List<SessionState> states = req.getStates();
        List<OTFSessionToolType> sessionToolTypes = req.getSessionToolType();
        List<OTMSessionType> otmSessionTypes = req.getOtmSessionType();
        List<OTMSessionInProgressState> progressStates = req.getProgressState();
        SortOrder sortOrder = req.getSortOrder();
        Integer start = req.getStart();
        Integer size = req.getSize();

        List<OTFSession> sessions = otfSessionDAO.getOtfSessionsForDailyStats(fromTime, tillTime, states, sessionToolTypes, otmSessionTypes, progressStates, sortOrder, start, size);
        if (ArrayUtils.isNotEmpty(sessions)) {
            result = getSessionInfos(sessions, null);
        }
        return result;

    }

    public BatchBasicInfo getBatch(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/getBatchById/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BatchBasicInfo info = gson.fromJson(jsonString, BatchBasicInfo.class);
        return info;
    }

    public Map<String, BatchBasicInfo> getBatchBasicInfosMapByIds(Set<String> batchIds) throws VException {
        List<BatchBasicInfo> batches = getBatchBasicInfosByIds(batchIds);
        Map<String, BatchBasicInfo> batchesMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(batches)) {
            for (BatchBasicInfo batch : batches) {
                if (batch != null) {
                    batchesMap.put(batch.getId(), batch);
                }
            }
        }
        return batchesMap;
    }

    public List<BatchBasicInfo> getBatchBasicInfosByIds(List<String> batchIds) throws VException {
        return getBatchBasicInfosByIds(new HashSet<>(batchIds));
    }

    public List<BatchBasicInfo> getBatchBasicInfosByIds(Set<String> batchIds) throws VException {

        if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }

//        String queryString = "";
//        if (ArrayUtils.isNotEmpty(batchIds)) {
//            for (String id : batchIds) {
//                try {
//                    String encodeURL = URLEncoder.encode(id, "UTF-8");
//                    queryString += ("batchIds=" + encodeURL + "&");
//                } catch (UnsupportedEncodingException e) {
//
//                }
//            }
//        }
//        queryString = removeLastChar(queryString);
//                }
//            }
//        }
//        queryString = removeLastChar(queryString);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(subscriptionEndPoint + "batch/getBatchBasicInfosByIds?", HttpMethod.POST, gson.toJson(batchIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BatchBasicInfo>>() {
        }.getType();
        List<BatchBasicInfo> infos = gson.fromJson(jsonString, listType);
        return infos;
    }

    public List<BatchBasicInfo> getDetailedBatchInfo(BatchDetailInfoReq req) throws VException {

        if (ArrayUtils.isEmpty(req.getBatchIds())) {
            return new ArrayList<>();
        }

        ClientResponse resp = WebUtils.INSTANCE
                .doCall(subscriptionEndPoint + "batch/getDetailedBatchInfo", HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BatchBasicInfo>>() {
        }.getType();
        List<BatchBasicInfo> infos = gson.fromJson(jsonString, listType);
        return infos;
    }

    public List<EnrollmentPojo> getEnrollmentsForUser(GetEnrollmentsReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String getEnrollmentsUrl = subscriptionEndPoint + "enroll/getEnrollmentsByEnrollmentIds";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> slist = gson.fromJson(jsonString, listType);

        return slist;
    }

    public List<EnrollmentPojo> getEnrollmentsDataForSessionStrip(GetEnrollmentsReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String getEnrollmentsUrl = subscriptionEndPoint + "enroll/getEnrollmentsDataForSessionStrip";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> slist = gson.fromJson(jsonString, listType);

        return slist;
    }

    public List<String> getBatchIdsOfActiveEnrollmentsOfUser(GetEnrollmentsReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String getEnrollmentsUrl = subscriptionEndPoint + "enroll/getBatchIdsOfActiveEnrollmentsOfUser";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<String>>() {
        }.getType();
        List<String> batchIds = gson.fromJson(jsonString, listType);

        return batchIds;
    }

    public List<EnrollmentPojo> getEnrollmentIdForUser(GetEnrollmentsReq req) {
        try {
            return getEnrollmentsForUser(req);
        } catch (Exception e) {
            logger.error("Error in fetching enrollments from subscription : " + req + " . Error " + e);

        }
        return new ArrayList<>();
    }

    public OTFSessionPojo addUpdateSessionContent(AddUpdateSessionContentReq req) throws Exception {
        OTFSession result = otfSessionDAO.getById(req.getSessionId());
        if (result == null) {
            logger.error("Session not found for id : " + req.getSessionId());
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + req.getSessionId());
        }
        result.setPreSessionContents(req.getPreSessionContents());
        result.setPostSessionContents(req.getPostSessionContents());

        otfSessionDAO.save(result);
        return getSessionInfo(result, null);
    }

    public AddRemoveBatchIdsToSessionRes addBatchIdsToSession(AddRemoveBatchIdsToSessionReq req) throws VException {
        req.verify();
        // Validate batch
        Map<String, BatchBasicInfo> batchesMap = getBatchBasicInfosMapByIds(req.getBatchIds());
        for (String batchId : req.getBatchIds()) {
            BatchBasicInfo batch = batchesMap.get(batchId);
            if (batch == null) {
                String errorString = "Batch not found for id : " + batchId + ". Request : "
                        + req.toString();
                logger.info(errorString);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
            }

            if (EntityStatus.INACTIVE.equals(batch.getBatchState())) {
                throw new IllegalArgumentException(
                        "Batch is not active : " + batchId + ". Request : " + req.toString());
            }
        }

        //validating session
        OTFSession session = otfSessionDAO.getById(req.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found for " + req.getSessionId());
        }

        /**
         * Allowing to merge batches session in two cases. 1. if sessionToolType
         * is not VEDANTU_WAVE, then you can merge batches 24hrs before starting
         * the session 2. if sessionToolType is VEDANTU_WAVE, then you can merge
         * batches 30min before starting the session
         */
        boolean isVedantuWaveSession = OTFSessionToolType.VEDANTU_WAVE.equals(session.getSessionToolType());

        if (session.getStartTime() != null
                && (!isVedantuWaveSession && (session.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY))
                || (isVedantuWaveSession && (session.getStartTime() < (System.currentTimeMillis() + 60 * DateTimeUtils.MILLIS_PER_MINUTE)))) {
            if ((req.getSecret() != null && "Afv3PzXYHcF4j49R".equals(req.getSecret()))) {
                //no error, let it go through, please generate links manually after doing this
            } else {
                throw new ForbiddenException(ErrorCode.SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME, "SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME");
            }
        }

        String teacherId = session.getPresenter();
        for (String batchId : batchesMap.keySet()) {
            BatchBasicInfo batchBasicInfo = batchesMap.get(batchId);
            if (!batchBasicInfo.getTeacherIds().contains(teacherId)) {
                throw new BadRequestException(ErrorCode.TEACHER_NOT_PART_OF_BATCH, "teacher not part of batch " + batchId);
            }
        }

        Set<String> existingBatchIds = session.getBatchIds();
        req.getBatchIds().removeAll(existingBatchIds);
        if (ArrayUtils.isEmpty(req.getBatchIds())) {
            throw new BadRequestException(ErrorCode.SESSION_ALREADY_COMMON_FOR_BATCHES, "SESSION_ALREADY_COMMON_FOR_BATCHES");
        }

        session.getBatchIds().addAll(req.getBatchIds());

        Set<String> toValidateBatchIds = new HashSet<>(session.getBatchIds());
        if(ArrayUtils.isNotEmpty(req.getToBeDeletedBatchIds())) {
            toValidateBatchIds.removeAll(req.getToBeDeletedBatchIds());
        }

        setEntityTypeForSession(session,toValidateBatchIds,req.getType());

        otfSessionDAO.save(session);

        AddRemoveBatchIdsToSessionRes res = new AddRemoveBatchIdsToSessionRes();
        res.setAddedBatchIds(req.getBatchIds());
        res.setUpdatedSession(mapper.map(session, OTFSessionPojoUtils.class));

        try {
            SessionSnapshot sessionSnapshot = new SessionSnapshot(session.getId(), session.getStartTime(), session.getEndTime(), new ArrayList<>(session.getBatchIds()), session.getOtmSessionType());
            updateSessionSnapshotPostMerging(sessionSnapshot);
        } catch (Exception e) {
            logger.error("Error in adding session snapshot to new batch snapshot: " + e);
        }

        try {

            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", mapper.map(session, OTFSessionPojo.class));
            payload.put("event", SessionEventsOTF.ADD_BATCH_IDS_TO_SESSION);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);

            if (session.getStartTime() != null && session.getStartTime() > System.currentTimeMillis()
                    && session.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
                gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedBulk(session.getId(), DeleteContext.BATCH_ADDED_OR_REMOVED);
                sendToQForGeneratingSessionLinkAndGTT(session);
            }
        } catch (Exception e) {
            logger.info("Error in async event for ADD_BATCH_IDS_TO_SESSION " + e);
        }

        for (String batchId : req.getBatchIds()) {
            logger.info("blocking calendar for students of batchId");
            MarkCalendarBatchPojo pojo = new MarkCalendarBatchPojo();
            pojo.setBatchId(batchId);
            pojo.setMark(true);
            MarkCalendarSessionPojo sessionPojo = mapper.map(session, MarkCalendarSessionPojo.class);
            pojo.setSessions(Arrays.asList(sessionPojo));
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_OPS, SQSMessageType.MARK_CALENDAR_BATCH_TASK, gson.toJson(pojo));
        }

        // async-- add attendees to session document
        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("batchIds", new ArrayList<>(session.getBatchIds()));
        payload2.put("sessionId", session.getId());
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.ADD_ATTENDEES_TO_SESSION_ON_BATCHIDS_ADDITION, payload2);
        asyncTaskFactory.executeTask(params2);

        return res;
    }

    public void sendToQForGeneratingSessionLinkAndGTT(OTFSession session) {
        String id = session.getId();
        logger.info("session id {} session obj {}",id, session);
        if (StringUtils.isNotEmpty(id)) {
            Map<String, String> payload = new HashMap<>();
            payload.put("sessionId", id);
            awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_GTT_CREATE_QUEUE, SQSMessageType.CREATE_LINK_AND_REGISTER_GTT, gson.toJson(payload), id);
        } else {
            int retry = 1;
            while (retry < 5) {
                retry++;
                try {
                    logger.error("session id is null saving via async task manager: retry {}, session {}", retry, session);
                    Map<String, Object> createSessionLinkPayLoad = new HashMap<>();
                    createSessionLinkPayLoad.put("session", session);
                    AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, createSessionLinkPayLoad);
                    if (retry > 2) {
                        asyncTaskParams.setDelayMillis(500L);
                    }
                    asyncTaskFactory.executeTask(asyncTaskParams);
                    break;
                } catch (Exception e) {
                    if (retry <= 3) {
                        logger.warn(e.getMessage(), e);
                    } else {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
        }
    }

    public void updateSessionSnapshotPostMerging(SessionSnapshot sessionSnapshot) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/updateSnapshot/", HttpMethod.POST, gson.toJson(sessionSnapshot));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
    }

    public AddRemoveBatchIdsToSessionRes removeBatchIdsFromSession(AddRemoveBatchIdsToSessionReq req) throws VException {
        req.verify();
        // Validate batch
        Map<String, BatchBasicInfo> batchesMap = getBatchBasicInfosMapByIds(req.getBatchIds());
        for (String batchId : req.getBatchIds()) {
            BatchBasicInfo batch = batchesMap.get(batchId);
            if (batch == null) {
                String errorString = "Batch not found for id : " + batchId + ". Request : "
                        + req.toString();
                logger.info(errorString);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
            }

            if (EntityStatus.INACTIVE.equals(batch.getBatchState())) {
                throw new IllegalArgumentException(
                        "Batch is not active : " + batchId + ". Request : " + req.toString());
            }
        }

        //validating session
        OTFSession session = otfSessionDAO.getById(req.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found for " + req.getSessionId());
        }

        boolean isVedantuWaveSession = OTFSessionToolType.VEDANTU_WAVE.equals(session.getSessionToolType());

        if (session.getStartTime() != null
                && (!isVedantuWaveSession && (session.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY))
                || (isVedantuWaveSession && (session.getStartTime() < (System.currentTimeMillis() + 60 * DateTimeUtils.MILLIS_PER_MINUTE)))) {
            throw new ForbiddenException(ErrorCode.SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME, "SCHEDULING_NOT_ALLOWED_WITHIN_1_HR_OF_START_TIME");
        }

        Set<String> existingBatchIds = session.getBatchIds();
        Set<String> notPartOf = new HashSet<>();
        for (String batchId : req.getBatchIds()) {
            if (!existingBatchIds.contains(batchId)) {
                notPartOf.add(batchId);
            }
        }

        if (ArrayUtils.isNotEmpty(notPartOf)) {
            throw new BadRequestException(ErrorCode.SESSION_NOT_PART_OF_ALL_BATCHES, "not part of " + Arrays.toString(notPartOf.toArray()));
        }

        existingBatchIds.removeAll(req.getBatchIds());

        if (ArrayUtils.isEmpty(existingBatchIds)) {
            throw new BadRequestException(ErrorCode.CANNOT_REMOVE_ALL_BATCH_IDS_OF_SESSION, "not part of " + Arrays.toString(req.getBatchIds().toArray()));
        }
        session.setBatchIds(existingBatchIds);

        setEntityTypeForSession(session,session.getBatchIds(),req.getType());

        // async-- add attendees to session document
        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("batchIds", new ArrayList<>(existingBatchIds));
        payload2.put("sessionId", session.getId());
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.ADD_ATTENDEES_TO_SESSION_ON_BATCHIDS_ADDITION, payload2);
        asyncTaskFactory.executeTask(params2);

        otfSessionDAO.save(session);

        if (session.getStartTime() != null && session.getStartTime() > System.currentTimeMillis()
                && session.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
            gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedBulk(session.getId(), DeleteContext.BATCH_ADDED_OR_REMOVED);
            sendToQForGeneratingSessionLinkAndGTT(session);
            /*Map<String, Object> createSessionLinkPayLoad = new HashMap<>();
            createSessionLinkPayLoad.put("session", session);
            AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, createSessionLinkPayLoad);
            asyncTaskFactory.executeTask(asyncTaskParams);*/
        }

        AddRemoveBatchIdsToSessionRes res = new AddRemoveBatchIdsToSessionRes();
        res.setRemovedBatchIds(req.getBatchIds());
        res.setUpdatedSession(mapper.map(session, OTFSessionPojoUtils.class));

        for (String batchId : req.getBatchIds()) {
            logger.info("unblocking calendar for students of batchId");
            MarkCalendarBatchPojo pojo = new MarkCalendarBatchPojo();
            pojo.setBatchId(batchId);
            pojo.setMark(false);
            MarkCalendarSessionPojo sessionPojo = mapper.map(session, MarkCalendarSessionPojo.class);
            pojo.setSessions(Arrays.asList(sessionPojo));
            awsSQSManager.sendToSQS(SQSQueue.CALENDAR_BATCH_OPS, SQSMessageType.MARK_CALENDAR_BATCH_TASK, gson.toJson(pojo));
        }

        return res;
    }

    public BatchInfo getBatchInfo(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/getBatchByIdWithEnrollments/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BatchInfo info = gson.fromJson(jsonString, BatchInfo.class);
        return info;
    }

    public List<EnrollmentPojo> getBatchIdsActiveEnrollments(Set<String> batchIds, boolean returnTeacherEnrollments) throws VException {
        String queryString = "returnTeacherEnrollments=" + returnTeacherEnrollments + "&";
        if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        } else {
            for (String batchId : batchIds) {
                queryString += "batchIds=" + batchId + "&";
            }
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/getBatchIdsActiveEnrollments?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> infos = gson.fromJson(jsonString, listType);
        return infos;
    }

    public PlatformBasicResponse isStudentPartOfSession(String userId, String sessionId) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        res.setSuccess(false);
        GTTAttendeeDetails gTTAttendeeDetails = gttAttendeeDetailsDAO.getAttendeeDetail(userId, sessionId);
        if (gTTAttendeeDetails != null) {
            if (EntityState.ACTIVE.equals(gTTAttendeeDetails.getEntityState())) {
                res.setSuccess(true);
            }
        }
        return res;
    }

    public List<OTFSessionPojo> createSessions(CreateOTFSessionsReq req) throws VException {
        req.verify();
        AddOTFSessionsReq newreq = new AddOTFSessionsReq();
        //TODO validate student,presenter,taIds roles.
        //TODO teacher cannot be both taId and presenter
        Set<String> batchIds = new HashSet<>();
        batchIds.add(req.getBatchId());
        newreq.setBatchIds(batchIds);
        newreq.setSessionToolType(req.getSessionToolType());
        List<OTFSessionPojoUtils> sessionPojos = new ArrayList<>();
        for (AgendaPojo agendaPojo : req.getAgendaPojoList()) {
            if (agendaPojo.getSessionPOJO() != null) {
                sessionPojos.add(agendaPojo.getSessionPOJO());
            }
        }
        newreq.setSessionPojo(sessionPojos);
        return addOTFSessionsToBatch(newreq);
    }

    public List<BatchFirstRegularSessionPojo> getFirstRegularSessionForBatch(List<String> batchIds) {
        return otfSessionDAO.getFirstRegularSessionForBatch(batchIds);
    }

    public Map<String, OTMSessionDashboardInfo> getOTMDashboardPartialInfo(List<String> batchIds, Integer start, Integer size) {
        List<OTFSession> otfSessionList = otfSessionDAO.getOTFSessionsForBatches(batchIds, start, size);
        List<String> sessionIds = new ArrayList<>();
        if (ArrayUtils.isEmpty(otfSessionList)) {

            return new HashMap<>();
        }
        for (OTFSession otfSession : otfSessionList) {
            sessionIds.add(otfSession.getId());
        }
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.getAttendeesForSessionIds(sessionIds, start, size);
        Map<String, Integer> sessionPresentAttendees = new HashMap<>();
        Map<String, Integer> sessionAbsentAttendees = new HashMap<>();
        logger.info("GTTAttendeeDetails: " + attendeeDetails);
        List<Long> userIds = new ArrayList<>();
        for (GTTAttendeeDetails gTTAttendeeDetails : attendeeDetails) {
            userIds.add(Long.parseLong(gTTAttendeeDetails.getUserId()));
        }

        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);

        for (GTTAttendeeDetails gttAttendeeDetails : attendeeDetails) {

            UserBasicInfo userBasicInfo = userMap.get(Long.parseLong(gttAttendeeDetails.getUserId()));

            if (userBasicInfo == null) {
                logger.error("UserBasicInfo can't be found for userId: " + gttAttendeeDetails.getUserId());
                continue;
            }

            if (!Role.STUDENT.equals(userBasicInfo.getRole())) {
                continue;
            }

            if (gttAttendeeDetails.getJoinTimes().size() > 0) {
                if (!sessionPresentAttendees.containsKey(gttAttendeeDetails.getSessionId())) {
                    sessionPresentAttendees.put(gttAttendeeDetails.getSessionId(), 1);
                } else {
                    int presentAttendee = sessionPresentAttendees.get(gttAttendeeDetails.getSessionId());
                    sessionPresentAttendees.put(gttAttendeeDetails.getSessionId(), presentAttendee + 1);
                }
            } else {

                if (!sessionAbsentAttendees.containsKey(gttAttendeeDetails.getSessionId())) {
                    sessionAbsentAttendees.put(gttAttendeeDetails.getSessionId(), 1);
                } else {
                    int absentAttendees = sessionAbsentAttendees.get(gttAttendeeDetails.getSessionId());
                    sessionAbsentAttendees.put(gttAttendeeDetails.getSessionId(), absentAttendees + 1);
                }
            }

            if (!sessionAbsentAttendees.containsKey(gttAttendeeDetails.getSessionId())) {
                sessionAbsentAttendees.put(gttAttendeeDetails.getSessionId(), 0);
            }

            if (!sessionPresentAttendees.containsKey(gttAttendeeDetails.getSessionId())) {
                sessionPresentAttendees.put(gttAttendeeDetails.getSessionId(), 0);
            }

        }
        Map<String, OTMSessionDashboardInfo> otmSessionDashboardInfoMap = new HashMap<>();

        for (OTFSession otfSession : otfSessionList) {
            OTMSessionDashboardInfo otmSessionDashboardInfo = new OTMSessionDashboardInfo();
            otmSessionDashboardInfo.setBatchIds(new ArrayList<>(otfSession.getBatchIds()));
            otmSessionDashboardInfo.setSessionTitle(otfSession.getTitle());
            otmSessionDashboardInfo.setSessionId(otfSession.getId());
            otmSessionDashboardInfo.setSessionStartTime(otfSession.getStartTime());
            if (otfSession.getRescheduleData().size() > 0 || otfSession.getRescheduledFromId() != null) {
                otmSessionDashboardInfo.setRescheduled(true);
            }
            otmSessionDashboardInfo.setStartTime(otfSession.getStartTime());
            otmSessionDashboardInfo.setEndTime(otfSession.getEndTime());
            otmSessionDashboardInfo.setTopicToBeCovered(otfSession.getDescription());
            otmSessionDashboardInfo.setOtmSessionType(otfSession.getOtmSessionType());
            otmSessionDashboardInfo.setSessionStatus(otfSession.getState());
            otmSessionDashboardInfo.setVimeoReplayId(otfSession.getVimeoId());
            otmSessionDashboardInfo.setPollsdata(otfSession.getPollsdata());
            if (sessionPresentAttendees.get(otfSession.getId()) != null) {
                if (sessionPresentAttendees.get(otfSession.getId()) < 1) {
                    otmSessionDashboardInfo.setStudentsAttendance(0f);
                    otmSessionDashboardInfo.setStudentPresent(0);
                    if (sessionAbsentAttendees.get(otfSession.getId()) != null) {
                        otmSessionDashboardInfo.setStudentTotal(sessionAbsentAttendees.get(otfSession.getId()));
                    } else {
                        otmSessionDashboardInfo.setStudentTotal(0);
                    }
                } else {
                    int present;
                    present = sessionPresentAttendees.get(otfSession.getId());
                    logger.info("Present attendees for session: " + otfSession.getId() + " :: " + present);
                    int total = present + sessionAbsentAttendees.get(otfSession.getId());
                    otmSessionDashboardInfo.setStudentPresent(present);
                    otmSessionDashboardInfo.setStudentTotal(total);
                    logger.info("Total attendees for sessionId: " + otfSession.getId() + " :: " + total);
                    otmSessionDashboardInfo.setStudentsAttendance((float) (present * 100) / total);
                }
            } else {
                otmSessionDashboardInfo.setStudentsAttendance(0f);
            }

            otmSessionDashboardInfo.setTeacherId(otfSession.getPresenter());
            otmSessionDashboardInfo.setTeacherJoinTime(otfSession.getTeacherJoinTime());
            otmSessionDashboardInfo.setBoardId(otfSession.getBoardId());
            otmSessionDashboardInfoMap.put(otfSession.getId(), otmSessionDashboardInfo);

        }

        return otmSessionDashboardInfoMap;

    }

    public List<StudentAttendanceInfo> getOTMSessionAttendanceInfo(String sessionId, Integer start, Integer size) throws VException {

        List<String> sessionsIds = new ArrayList<>();
        sessionsIds.add(sessionId);
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.getAttendeesForSessionIds(sessionsIds, start, size);
        Set<String> userIds = new HashSet<>();
        Map<String, GTTAttendeeDetails> gttAttendeeDetailsMap = new HashMap<>();
        for (GTTAttendeeDetails gttAttendeeDetails : attendeeDetails) {
            userIds.add(gttAttendeeDetails.getUserId());
            gttAttendeeDetailsMap.put(gttAttendeeDetails.getUserId(), gttAttendeeDetails);
        }

        List<UserBasicInfo> userBasicInfoList = fosUtils.getUserBasicInfosSet(userIds, true);
        if (userBasicInfoList == null) {
            throw new VException(ErrorCode.SERVICE_ERROR, "UserBasicInfo not available for Session id: " + sessionId);
        }

        if (userBasicInfoList.size() < userIds.size()) {
            logger.error("Mismatch: Users doesn't exist for GTTAttendeeDetails");
        }

        List<StudentAttendanceInfo> studentAttendanceInfoList = new ArrayList<>();

        for (UserBasicInfo userBasicInfo : userBasicInfoList) {
            if (!userBasicInfo.getRole().equals(Role.STUDENT)) {
                continue;
            } else {
                StudentAttendanceInfo studentAttendanceInfo = new StudentAttendanceInfo();
                studentAttendanceInfo.setEmail(userBasicInfo.getEmail());
                studentAttendanceInfo.setPhoneCode(userBasicInfo.getPhoneCode());
                studentAttendanceInfo.setPhoneNumber(userBasicInfo.getContactNumber());
                studentAttendanceInfo.setsName(userBasicInfo.getFullName());
                if (gttAttendeeDetailsMap.get(userBasicInfo.getUserId().toString()) != null && gttAttendeeDetailsMap.get(userBasicInfo.getUserId().toString()).getJoinTimes().size() > 0) {
                    studentAttendanceInfo.setPresent(true);
                    studentAttendanceInfo.setTimeInSession(gttAttendeeDetailsMap.get(userBasicInfo.getUserId().toString()).getTimeInSession());
                } else {
                    studentAttendanceInfo.setPresent(false);
                }

                studentAttendanceInfoList.add(studentAttendanceInfo);
            }
        }

        return studentAttendanceInfoList;

    }

    public Map<String, CounselorDashboardSessionsInfoRes> getSessionInfosForBatchIds(List<String> batchIds) {
        Map<String, CounselorDashboardSessionsInfoRes> response = new HashMap();
        if (ArrayUtils.isEmpty(batchIds)) {
            return response;
        }
        List<CounselorDashboardSessionsInfoRes> coursePlanSessionsInfoReses = otfSessionDAO.getDashboardSessionInfosByBatchIds(batchIds);
        if (ArrayUtils.isNotEmpty(coursePlanSessionsInfoReses)) {
            for (CounselorDashboardSessionsInfoRes res : coursePlanSessionsInfoReses) {
                response.put(res.getId(), res);
            }
        }
        return response;
    }

    public OTFSession getUserJoinedOtfSession(Long userId, Sort.Direction direction) {
        if (userId == null) {
            return null;
        }
        GTTAttendeeDetails attendeeDetails = gttAttendeeDetailsManager.getOneJoinedSessionAttendeeDetailForUser(userId.toString(), direction);
        if (attendeeDetails == null) {
            return null;
        }

        return otfSessionDAO.getById(attendeeDetails.getSessionId());
    }

    public List<OTFBatchSessionReportRes> getOtfBatchSessionReport(List<String> batchIds, String userId) {
        List<OTFBatchSessionReportRes> response = new ArrayList<>();
        if (ArrayUtils.isEmpty(batchIds)) {
            return response;
        }
        Long currentTime = System.currentTimeMillis();

        List<SessionReportAggregation> results = otfSessionDAO.getOTFBatchSessionReportAggregation(batchIds);
        if (ArrayUtils.isNotEmpty(results)) {
            for (SessionReportAggregation aggregation : results) {
                OTFBatchSessionReportRes entity = new OTFBatchSessionReportRes();
                entity.setBatchId(aggregation.getId());
                entity.setTotalSessionCount(aggregation.getTotalSessionCount());
                entity.setFirstSessionTime(aggregation.getFirstSessionStartTime());
                long upcomingSessionTime = 0L;
                long lastSessionTime = 0L;
                long completedSessionCount = 0L;
                for (Long startTime : aggregation.getStartTimes()) {
                    if (startTime >= currentTime && (upcomingSessionTime == 0L || startTime < upcomingSessionTime)) {
                        upcomingSessionTime = startTime;
                    }
                    if (startTime < currentTime && (lastSessionTime == 0L || startTime > lastSessionTime)) {
                        lastSessionTime = startTime;
                    }
                }
                for (Long endTime : aggregation.getEndTimes()) {
                    if (endTime <= currentTime) {
                        completedSessionCount += 1;
                    }
                }
                entity.setCompletedSessionCount(completedSessionCount);
                entity.setUpcomingSessionTime(upcomingSessionTime);
                entity.setLastSessionTime(lastSessionTime);
                List<GTTAttendeeDetails> details = gttAttendeeDetailsManager.getJoinedSessionsAttendeeDetailForUser(aggregation.getSessionIds(), userId);
                entity.setSessionAttendedByUser(details.size());
                response.add(entity);
            }
        }
        return response;
    }

    public String fetchOrganizerAccessTokenFromAccessToken(OTFSession oTFSession) {

        if (oTFSession.getSessionToolType() == null || StringUtils.isEmpty(oTFSession.getOrganizerAccessToken())) {
            return null;
        }
        try {
            ScheduleManager scheduleManager = getScheduleManagerByToolType(oTFSession);
            if (scheduleManager == null) {
                return null;
            }
            return scheduleManager.getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
        } catch (Exception e) {
            logger.error("error in fetching schedule." + e);
        }
        return oTFSession.getOrganizerAccessToken();
    }

    public void resetOrganizerTokens(Long startTime, String organizerToken)
            throws InternalServerErrorException, BadRequestException {

        if (startTime == null || startTime < 0L) {
            startTime = System.currentTimeMillis();
        }
        if (StringUtils.isEmpty(organizerToken)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bad request organizer token");
        }
        logger.info("resetTokens + " + startTime);
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime));
//		query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).is(OTFSessionToolType.GTT));
        query.addCriteria(Criteria.where(OTFSession.Constants.ORGANIZER_ACCESS_TOKEN).is(organizerToken));

        List<OTFSession> results = otfSessionDAO.runQuery(query, OTFSession.class);
        int noTokensCount = 0;
        logger.info("Session count:" + results.size());
        List<String> errorString = new ArrayList<>();
        if (!CollectionUtils.isEmpty(results)) {
            // Deschedule all sessions in future
            int processed = 0;
            for (OTFSession session : results) {
                String organizer = "PreOrganizer:"
                        + session.getOrganizerAccessToken();
                if (StringUtils.isEmpty(session.getOrganizerAccessToken())) {
                    noTokensCount++;
                }
                try {
                    logger.info("pre update:" + session.getId() + " organizer:"
                            + organizer);
                    if (!StringUtils.isEmpty(session.getOrganizerAccessToken())) {
                        try {
                            descheduleSessionByBitSet(session);

                        } catch (Exception e) {
                            logger.info("Error while desceduling session." + e);
                        }
                    }
                    session.setOrganizerAccessToken(null);
                    session.setPresenterUrl(null);
                    session.setMeetingId(null);
                    scheduleSessionByBitSet(session);

                    otfSessionDAO.save(session);
                } catch (Exception ex) {
                    logger.error("Error scheduling new session for " + session.getId());
                    errorString.add("Deschedule:" + session.getId() + " organiser:"
                            + organizer);
                }
            }

            logger.info("processed:" + processed);
        }

        logger.info("No access tokesn :" + noTokensCount + " total:" + results.size());

    }

    public PlatformBasicResponse updatePollsMetadataForSession(UpdateSessionPollsDataReq req) throws VException {
        req.verify();
        PlatformBasicResponse response = new PlatformBasicResponse();
        OTFSession session = otfSessionDAO.getById(req.getSessionId());
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + req.getSessionId();
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        Pollsdata pollsdata = req.getPollsdata();
        if (pollsdata != null) {
            List<PollQuestion> pollQuestions = pollsdata.getPollQuestions();
            if (ArrayUtils.isNotEmpty(pollQuestions)) {
                int totalPolls = pollQuestions.size();
                int totalAnswers = 0;
                for (PollQuestion question : pollQuestions) {
                    totalAnswers += question.getTotalAnswers();
                    if (StringUtils.isEmpty(question.getQuestion())) {
                        logger.error("received question empty when updating polls data sessionId:" + session.getId() + " req:" + req);
                        return response;
                    }
                }
                int avgAnswers = totalAnswers / totalPolls;
                pollsdata.setTotalPolls(totalPolls);
                pollsdata.setTotalAnswers(totalAnswers);
                pollsdata.setAvgAnswers(avgAnswers);
                Update update = new Update();
                update.set(OTFSession.Constants.POLLS_DATA, pollsdata);
                // session state not changed via this flow
                otfSessionDAO.updateSession(update, session.getId());
            }
        }
        List<StudentPollDataReq> studentPollDatas = req.getStudentPollDatas();
        if (ArrayUtils.isNotEmpty(studentPollDatas)) {
            for (StudentPollDataReq studentPollDataReq : studentPollDatas) {
                try {
                    if (StringUtils.isEmpty(studentPollDataReq.getUserId())) {
                        throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "userId is empty in request");
                    }
                    gttAttendeeDetailsManager.updatePollAnswered(studentPollDataReq, session.getId());
                } catch (Exception e) {
                    logger.error("error in updating studentPollData:" + studentPollDataReq + "sessionId:" + session.getId() + " error:" + e.getMessage());
                }
            }
        }
        return response;
    }

    public PlatformBasicResponse launchPollsDataLambdaForSession(OTFSession session) throws NotFoundException, InternalServerErrorException, BadRequestException {
        PlatformBasicResponse response = new PlatformBasicResponse();
        if (session == null) {
            String errorMessage = "Session does not exist for req ";
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        List<GTTSessionInfoRes> gttSessionInfos = getScheduleManagerByToolType(session).getGTTSessionInfos(session);
        if (ArrayUtils.isNotEmpty(gttSessionInfos)) {
            if (StringUtils.isEmpty(session.getOrganizerAccessToken())) {
                // Without organizer token we cannot process polls
                logger.error("No orgainzer token for session id : " + session.getId());
                return response;
            }

            String credentials = ConfigUtils.INSTANCE
                    .getStringValue("gtt.account.credentials." + session.getOrganizerAccessToken());
            if (StringUtils.isEmpty(credentials)) {
                logger.error("Credentials missing for session id : " + session.getId());
                return response;
            }
            String[] credentialArr = credentials.split("-");
            HandleOTFRecordingLambda lambdaReq = new HandleOTFRecordingLambda();
            lambdaReq.setSessionId(session.getId());
            HandleOTFRecordingLambda.Credentials messageObject = new HandleOTFRecordingLambda.Credentials(credentialArr[0], credentialArr[1]);
            lambdaReq.setCredentials(messageObject);
            List<String> keys = new ArrayList<>();
            for (GTTSessionInfoRes sessioninfo : gttSessionInfos) {
                keys.add(sessioninfo.getSessionKey());
            }
            lambdaReq.setSessionkeys(keys);
            awsSNSManager.triggerSNS(SNSTopic.PROCESS_OTF_POLLS_DATA, SNSTopic.PROCESS_OTF_POLLS_DATA.name(), gson.toJson(lambdaReq));
        }
        return response;
    }

    public void syncPollsDataAsync() {
        logger.info("syncPollsDataAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SYNC_OTF_POLLS_DATA, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void syncPollsData() {
        logger.info("syncPollsDataOtf");
        try {
            Long currentTime = System.currentTimeMillis();
            logger.info("syncPollsData starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterEndTime = currentTime - 1 * DateTimeUtils.MILLIS_PER_HOUR - syncPollsDataCronRunTime;
            Long beforeEndTime = afterEndTime + syncPollsDataCronRunTime - 1;
            logger.info("syncPollsData afterStartTime " + afterEndTime + " beforeStartTime " + beforeEndTime);
            GetOTFSessionsReq getOTFSessionsReq = new GetOTFSessionsReq();
            getOTFSessionsReq.setStartTime(afterEndTime);
            getOTFSessionsReq.setEndTime(beforeEndTime);
            getOTFSessionsReq.setSize(1000);
            List<OTFSession> sessions = otfSessionDAO.getPastSessions(getOTFSessionsReq);
            if (ArrayUtils.isNotEmpty(sessions)) {
                if (sessions.size() == 1000) {
                    logger.error("Number of session ended more than 1000 not processing between afterStartTime " + afterEndTime + " beforeStartTime " + beforeEndTime);
                    return;
                }
                for (OTFSession session : sessions) {
                    if (OTFSessionToolType.GTW.equals(session.getSessionToolType())
                            || session.hasVedantuWaveFeatures()) {
                        continue;
                    }
                    try {
                        launchPollsDataLambdaForSession(session);
                    } catch (Exception ex) {
                        logger.error("error in launching PollsDataLambdaForSession: " + session + " error:" + ex.getMessage());
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("error in syncPollsDataOtf " + ex.getMessage());
        }
    }

    public void sendOtmNoShowEmails() {
        logger.info("sendOtmNoShowEmails");
        try {
            Long currentTime = System.currentTimeMillis();
            logger.info("sendOtmNoShowEmails starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterEndTime = currentTime - 2 * sendNoShowEmailCronRunTime;
            Long beforeEndTime = afterEndTime + sendNoShowEmailCronRunTime;
            afterEndTime += 1;
            logger.info("sendOtmNoShowEmails afterEndTime " + afterEndTime + " beforeEndTime " + beforeEndTime);
            GetOTFSessionsReq getOTFSessionsReq = new GetOTFSessionsReq();
            getOTFSessionsReq.setStartTime(afterEndTime);
            getOTFSessionsReq.setEndTime(beforeEndTime);
            getOTFSessionsReq.setSize(1000);
            getOTFSessionsReq.setTeacherJoined(Boolean.TRUE);
            List<OTFSession> sessions = otfSessionDAO.getPastSessions(getOTFSessionsReq);
            if (ArrayUtils.isNotEmpty(sessions)) {
                if (sessions.size() == 1000) {
                    logger.error("sendOtmNoShowEmails:Number of session ended more than 1000 not processing between afterStartTime " + afterEndTime + " beforeStartTime " + beforeEndTime);
                    return;
                }
                for (OTFSession session : sessions) {
                    try {
                        sendOtmNoShowEmail(session);
                    } catch (Exception ex) {
                        logger.error("error in sendOtmNoShowEmails: " + session + " error:" + ex.getMessage());
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("error in sendOtmNoShowEmails " + ex.getMessage());
        }
    }

    public void sendOtmNoShowEmail(OTFSession session) throws VException {
        logger.info("sending no show email for session :" + session);
        if (session == null) {
            return;
        }
        if (!Boolean.TRUE.equals(session.getTeacherJoined())) {
            return;
        }
        List<GTTAttendeeDetails> attendees = gttAttendeeDetailsManager.getAbsentAttendees(session);
        logger.info("Absent Attendees:" + attendees);
        if (ArrayUtils.isEmpty(attendees)) {
            return;
        }
        if (ArrayUtils.isEmpty(session.getBatchIds())) {
            return;
        }
        Set<String> userIds = new HashSet<>();
        for (GTTAttendeeDetails attendee : attendees) {
            userIds.add(attendee.getUserId());
        }
        for (String batchId : session.getBatchIds()) {
            HashMap<String, List<String>> userBatchMap = getUserIdEnrollmentBatchMap(batchId, userIds);
            logger.info(" Attendees Batch Map with respect to this session and batchId:" + batchId + " map:" + userBatchMap);
            checkAndSendNoShowEmail(session, userBatchMap);
        }
    }

    public void checkAndSendNoShowEmail(OTFSession session, HashMap<String, List<String>> userBatchMap) {
        Long endTime = session.getEndTime() + 2;
        HashMap<String, List<OTFSession>> batchIdPastSessionsMap = new HashMap<>();
        HashMap<String, OTFSession> batchIdUpcomingSessionsMap = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : userBatchMap.entrySet()) {
            logger.info(" Sending no show email for the entry:" + entry);
            try {
                String userId = entry.getKey();
                List<String> batchIds = entry.getValue();
                if (ArrayUtils.isEmpty(batchIds)) {
                    continue;
                }
                Collections.sort(batchIds);
                String mapKey = String.join(",", batchIds);
                List<OTFSession> sessions = null;
                if (batchIdPastSessionsMap.containsKey(mapKey)) {
                    sessions = batchIdPastSessionsMap.get(mapKey);
                    logger.info(" Getting past session from map:" + sessions);
                } else {
                    sessions = otfSessionDAO.getPastSessionForNoShow(batchIds, endTime);
                    logger.info(" Getting past session from dao:" + sessions);
                    batchIdPastSessionsMap.put(mapKey, sessions);
                }
                if (ArrayUtils.isEmpty(sessions) || sessions.size() < 3) {
                    continue;
                }
                List<String> sessionIds = new ArrayList<>();
                List<OTFSession> pastSessions = new ArrayList<>(sessions.subList(0, 3));
                sessions.stream().forEach((s) -> {
                    sessionIds.add(s.getId());
                });
                boolean shouldSend = gttAttendeeDetailsManager.shouldSendNoShowEmail(userId, sessionIds, session.getId());
                logger.info("shouldSendNoShowEmail for this entryset:" + entry + " result:" + shouldSend);
                if (shouldSend) {
                    OTFSession upcomingSession = null;
                    if (batchIdUpcomingSessionsMap.containsKey(mapKey)) {
                        upcomingSession = batchIdUpcomingSessionsMap.get(mapKey);
                    } else {
                        upcomingSession = otfSessionDAO.getLatestUpcomingSessionForBatchIds(batchIds);
                        batchIdUpcomingSessionsMap.put(mapKey, upcomingSession);
                    }
                    logger.info("sending now show email userId:" + userId + " upcomingSession:" + upcomingSession + "pastSessions:" + pastSessions);
                    communicationManager.sendOtmNoShowEmail(userId, upcomingSession, pastSessions);

                }
            } catch (Exception ex) {
                logger.error("error in sending no show email to entry:" + entry + "  after session:" + session + " error:" + ex.getMessage());
            }
        }
    }

    public HashMap<String, List<String>> getUserIdEnrollmentBatchMap(String batchId, Set<String> userIds) throws VException {
        if (userIds.size() > 1000) {
            logger.error("UserIds exceed 1000 for getUserIdEnrollmentBatchMap, please optimize the code ");
        }

        GetUserIdEnrollmentBatchMapReq req = new GetUserIdEnrollmentBatchMapReq();
        if (ArrayUtils.isNotEmpty(userIds)) {
            req.setUserIds(new ArrayList<>(userIds));
        }
        if (StringUtils.isNotEmpty(batchId)) {
            ArrayList bIds = new ArrayList<>();
            bIds.add(batchId);
            req.setBatchIds(bIds);
        }

        ClientResponse resp = WebUtils.INSTANCE
                .doCall(subscriptionEndPoint + "/enroll/getUserIdEnrollmentBatchMapViaPOST", HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<HashMap<String, List<String>>>() {
        }.getType();
        HashMap<String, List<String>> batchs = gson.fromJson(jsonString, listType);
        return batchs;
    }

    public void sendPostSessionEmailAsync() {
        logger.info("sendPostSessionEmailAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTF_POST_SESSION_EMAILS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendPostSessionEmails() {
        logger.info("sendPostSessionEmails");
        try {
            Long currentTime = System.currentTimeMillis();
            logger.info("sendPostSessionEmails starting at time : " + currentTime);
            Long dayStartTime = DateTimeUtils.getISTDayStartTime(currentTime);
            Long afterEndTime = dayStartTime - sendPostSessionEmailCronRunTime;
            Long beforeEndTime = afterEndTime + sendPostSessionEmailCronRunTime;
            afterEndTime += 1;
            logger.info("sendPostSessionEmails afterEndTime " + afterEndTime + " beforeEndTime " + beforeEndTime);

            int start = 0;
            final int MAX_FETCH_SIZE = 250;
            final List<String> includeFieldsSesssion = Stream.of(_ID, TITLE, PRESENTER, START_TIME, POLLS_DATA, BATCH_IDS,
                    SESSION_TOOL_TYPE, SESSION_CONTEXT_TYPE, QUIZ_COUNT, NOTICE_COUNT, HOTSPOT_COUNT).collect(Collectors.toList());
            while (true) {
                List<OTFSession> fetchedSessions = otfSessionDAO.getNonWebinarSessionsForDuration(afterEndTime, beforeEndTime,
                        includeFieldsSesssion, start, MAX_FETCH_SIZE);
                logger.info("fetched sessions" + fetchedSessions.size());
                if (CollectionUtils.isEmpty(fetchedSessions)) {
                    break;
                }

                for (OTFSession session : fetchedSessions) {
                    try {
                        logger.info("sendPostSessionEmails got session to send email : " + session.getId());
                        awsSQSManager.sendToSQS(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO, SQSMessageType.SEND_POST_SESSION_SUMMARY_MAILS, gson.toJson(session));
                    } catch (Exception ex) {
                        logger.error("error in sendPostSessionEmails: " + session + " error:" + ex.getMessage());
                    }
                }

                if (fetchedSessions.size() < MAX_FETCH_SIZE) {
                    break;
                }
                start += fetchedSessions.size();
            }
        } catch (Exception ex) {
            logger.error("error in sendPostSessionEmails " + ex.getMessage());
        }
    }

    public void prepareToSendSessionEmails() {
        logger.info("sendTrialSessionEmails");
        try {
            long currentTime = System.currentTimeMillis();
            logger.info("sendTrialSessionEmails starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterStartTime = currentTime + DateTimeUtils.MILLIS_PER_HOUR;
            Long beforeStartTime = afterStartTime + 15 * DateTimeUtils.MILLIS_PER_MINUTE - 1;
            logger.info("sendTrialSessionEmails afterStartTime " + afterStartTime + " beforeStartTime " + beforeStartTime);

            cachedThreadPoolUtility.execute(() -> sendRegularSessionToEmailQueue(afterStartTime, beforeStartTime, SQSMessageType.SEND_SESSION_MAIL));
            cachedThreadPoolUtility.execute(() -> sendTrialSessionToEmailQueue(afterStartTime, beforeStartTime, SQSMessageType.SEND_TRIAL_SESSION_MAIL));

        } catch (Exception ex) {
            logger.error("error in sendTrialSessionEmails " + ex.getMessage());
        }
    }

    public void prepareToSendSessionLiveEmails() {
        logger.info("prepareToSendSessionLiveEmails");
        try {
            long currentTime = System.currentTimeMillis();
            logger.info("prepareToSendSessionLiveEmails starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterStartTime = currentTime;
            Long beforeStartTime = afterStartTime + 5 * DateTimeUtils.MILLIS_PER_MINUTE - 1;
            logger.info("prepareToSendSessionLiveEmails afterStartTime " + afterStartTime + " beforeStartTime " + beforeStartTime);

            cachedThreadPoolUtility.execute(() -> sendRegularSessionToEmailQueue(afterStartTime, beforeStartTime, SQSMessageType.SEND_LIVE_SESSION_MAIL));
            cachedThreadPoolUtility.execute(() -> sendTrialSessionToEmailQueue(afterStartTime, beforeStartTime, SQSMessageType.SEND_LIVE_TRIAL_SESSION_MAIL));

        } catch (Exception ex) {
            logger.error("error in sendTrialSessionEmails " + ex.getMessage());
        }
    }

    private void sendRegularSessionToEmailQueue(Long afterStartTime, Long beforeStartTime, SQSMessageType messageType) {
        List<OTFSession> regularSessions = otfSessionDAO.getNonTrialSessionBetweenTime(afterStartTime, beforeStartTime);
        logger.info("Regular Session Email From " + afterStartTime + " To " + beforeStartTime + " For " + messageType);
        logger.info("Sessions " + regularSessions);
        if (ArrayUtils.isNotEmpty(regularSessions)) {
            for (OTFSession session : regularSessions) {
                try {
                    logger.info("Tregaring regular reminder emails for session Id " + session.getId());
                    awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, messageType, gson.toJson(session));
                } catch (Exception ex) {
                    logger.error("error in sendTrialSessionEmails: " + session + " error:" + ex.getMessage());
                }
            }
        }
    }

    private void sendTrialSessionToEmailQueue(Long afterStartTime, Long beforeStartTime, SQSMessageType messageType) {
        List<OTFSession> sessions = otfSessionDAO.getTrialSessionBetweenTime(afterStartTime, beforeStartTime);
        logger.info("Trial Session Email From " + afterStartTime + " To " + beforeStartTime + " For " + messageType);
        logger.info("Sessions " + sessions);

        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSession session : sessions) {
                try {
                    logger.info("Triggering for trial session " + session.getId());
                    awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, messageType, gson.toJson(session));
//                    communicationManager.sendOtmTrialSessionEmail(session);
                } catch (Exception ex) {
                    logger.error("error in sendTrialSessionEmails: " + session + " error:" + ex.getMessage());
                }
            }
        }
    }

    public void sendTrialSessionEmail(OTFSession session) throws VException {
        communicationManager.sendOtmTrialSessionEmail(session);
    }

    public List<String> getModularBatchIds() throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/getModularBatch", HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        List<String> result = gson.fromJson(jsonString, listType);
        return result;
    }

    public void sentEmailForModularBatchSession() throws VException, BadRequestException {
        logger.info("send email for modular batch sessions");
        List<String> batchIdList = getModularBatchIds();

        // partition the modular batch in group of size 500
        List<List<String>> batchIdsGroup = Lists.partition(batchIdList, 500);

        for (List<String> batchIds : batchIdsGroup) {
            try {
                Long currentTime = System.currentTimeMillis();
                logger.info("sendModularRegularSessionEmails starting at time : " + currentTime);
                currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
                Long afterStartTime = currentTime + DateTimeUtils.MILLIS_PER_HOUR;
                Long beforeStartTime = afterStartTime + 15 * DateTimeUtils.MILLIS_PER_MINUTE - 1;
                logger.info("sendModularRegularSessionEmails afterStartTime " + afterStartTime + " beforeStartTime " + beforeStartTime);

                List<OTFSession> sessions = otfSessionDAO.getRegularSessionForBatchesBetweenTime(batchIds, afterStartTime, beforeStartTime);
                if (ArrayUtils.isNotEmpty(sessions)) {
                    for (OTFSession session : sessions) {
                        try {
                            communicationManager.sendOtmTrialSessionEmail(session);
                        } catch (Exception ex) {
                            logger.error("error in sendModularRegularSessionEmails: " + session + " error:", ex);
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("error in sendEmailForModularBatchSessions: ", ex);
            }
        }

    }

    public void sendPostSessionEmail(OTFSession session) throws VException, UnsupportedEncodingException {
        if (session == null) {
            logger.info("sendPostSessionEmail session is null: " + session);
            return;
        }
        if (session.isWebinarSession()) {
            return;
        }
        logger.info("sending otf sendPostSessionEmail session: " + session);
        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        bodyScopes.put("sessionTitle", session.getTitle());
        subjectScopes.put("sessionTitle", session.getTitle());
        sdfDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfDate.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionDate", timestr);
        subjectScopes.put("sessionDate", timestr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfTime.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionTime", timestr);
        subjectScopes.put("sessionTime", timestr);
        sdfDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfDay.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionDay", timestr);
        subjectScopes.put("sessionDay", timestr);

        sdfDateWithMonth.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateStrWithMonth = sdfDateWithMonth.format(new Date(session.getStartTime()));
        bodyScopes.put("sessionDateWithMonth", dateStrWithMonth);

        if (session.hasVedantuWaveFeatures()) {
            bodyScopes.put("replayUrl", FOS_ENDPOINT + "/session-replay/" + session.getId());
        } else {
            bodyScopes.put("replayUrl", FOS_ENDPOINT + "/v/otfsessionreplay/" + session.getId());
        }
        bodyScopes.put("notesUrl", FOS_ENDPOINT + "/v/otfSessionNotes/" + session.getId());
        Long currentTime = System.currentTimeMillis();
        logger.info("currentTime time is: " + currentTime);
        Pollsdata pollsdata = session.getPollsdata();
        if (pollsdata != null && pollsdata.getTotalPolls() > 0) {
            bodyScopes.put("totalPolls", String.valueOf(pollsdata.getTotalPolls()));
        }
        String teacherId = session.getPresenter();
        if (StringUtils.isEmpty(teacherId)) {
            logger.info("sendPostSessionEmail  teacherId is null: " + session);
            return;
        }
        UserBasicInfo teacherInfo = fosUtils.getUserBasicInfo(teacherId, true);
        if (teacherInfo == null) {
            logger.info("sendPostSessionEmail  teacherInfo is null: " + session);
            return;
        }
        bodyScopes.put("teacherName", teacherInfo.getFullName());
        bodyScopes.put("teacherImg", teacherInfo.getProfilePicUrl());
        ClientResponse resp = WebUtils.INSTANCE.doCall(PLATFORM_ENDPOINT + "remark/getRemarkForSession?sessionId=" + session.getId(),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        RemarkResp remarkResp = gson.fromJson(jsonString, RemarkResp.class);
        if (remarkResp != null) {
            bodyScopes.put("teacherComment", remarkResp.getRemark());
        }

        Set<String> batchIds = session.getBatchIds();

        Type mapType = new TypeToken<HashMap<String, Object>>() {
        }.getType();
        Set<String> sessionIds = new HashSet<>();
        sessionIds.add(session.getId());
        List<GTTAttendeeDetails> gTTAttendeeDetailses = gttAttendeeDetailsManager.getActiveAttendeeDetailsForSessions(sessionIds, null);
        if (ArrayUtils.isNotEmpty(gTTAttendeeDetailses)) {
            for (GTTAttendeeDetails attendee : gTTAttendeeDetailses) {
                HashMap<String, Object> scope = gson.fromJson(gson.toJson(bodyScopes, mapType), mapType);
                HashMap<String, Object> s_scope = gson.fromJson(gson.toJson(subjectScopes, mapType), mapType);
                CommunicationType communicationType;
                if (session.hasVedantuWaveFeatures()) {
                    communicationType = CommunicationType.OTF_POST_SESSION_TO_STUDENT_NEW_WAVE;
                    logger.info("preparing scopes for vedantu wave session" + attendee);
                    String joinTimeMessage;
                    if (ArrayUtils.isNotEmpty(attendee.getJoinTimes()) && session.getStartTime() != null) {
                        //TODO sort in ascending order
                        Long diff = attendee.getJoinTimes().get(0) - session.getStartTime();
                        if (diff <= 300000) {
                            joinTimeMessage = "Right on time";
                            scope.put("onTimeMessage", joinTimeMessage);
                        } else {
                            if (diff > DateTimeUtils.MILLIS_PER_HOUR) {
                                joinTimeMessage = "more than hour";
                            } else {
                                Long mins = diff / 60000;
                                joinTimeMessage = mins + "mins";
                            }
                            scope.put("lateMessage", joinTimeMessage);
                        }
                        scope.put("attendedClass", true);
                    } else {
                        joinTimeMessage = "You missed it!";
                        scope.put("missedMessage", joinTimeMessage);
                    }

                    int asked = 0, attempted = 0, correct = 0, wrong = 0, pollsLiked = 0, pollsDisliked = 0;
                    if (session.getQuizCount() != null) {
                        asked += session.getQuizCount();
                    }
                    if (session.getHotspotCount() != null) {
                        asked += session.getHotspotCount();
                    }

                    if (ArrayUtils.isNotEmpty(attendee.getInteractionDatas())) {
                        for (InteractionData data : attendee.getInteractionDatas()) {
                            if (InteractionType.QUIZ.equals(data.getInteractionType())
                                    || InteractionType.HOTSPOT.equals(data.getInteractionType())) {
                                logger.info("anaylysing data " + data);
                                if (StringUtils.isNotEmpty(data.getSelected())) {
                                    attempted++;
                                    if (data.isIsCorrect()) {
                                        correct++;
                                    } else {
                                        wrong++;
                                    }
                                }
                            } else if (InteractionType.POLL.equals(data.getInteractionType())) {
                                if ("YES".equalsIgnoreCase(data.getSelected())) {
                                    pollsLiked++;
                                } else if ("NO".equalsIgnoreCase(data.getSelected())) {
                                    pollsDisliked++;
                                }
                            }
                        }
                    }
                    scope.put("asked", String.valueOf(asked));
                    scope.put("attempted", String.valueOf(attempted));
                    scope.put("correct", String.valueOf(correct));
                    scope.put("wrong", String.valueOf(wrong));
                    scope.put("pollsLiked", String.valueOf(pollsLiked + ((attendee.getThumbsUpCount() != null) ? attendee.getThumbsUpCount() : 0)));
                    scope.put("pollsDisliked", String.valueOf(pollsDisliked + ((attendee.getThumbsDownCount() != null) ? attendee.getThumbsDownCount() : 0)));

                    if (ArrayUtils.isNotEmpty(attendee.getDoubtDatas())) {
                        scope.put("doubtsAsked", String.valueOf(attendee.getDoubtDatas().size()));
                    } else {
                        scope.put("doubtsAsked", 0);
                    }

                    if (session.getNoticeCount() != null && session.getNoticeCount() >= 0) {
                        scope.put("noticesCount", String.valueOf(session.getNoticeCount()));
                    } else {
                        scope.put("noticesCount", 0);
                    }

                    if (attendee.getLbPercentile() != null) {
                        scope.put("leaderboardPercentile", attendee.getLbPercentile());
                    }
                } else {
                    communicationType = CommunicationType.OTF_POST_SESSION_TO_STUDENT;
                }

                try {
                    communicationManager.sendOtfPostSessionEmail(attendee, scope, s_scope, communicationType);
                } catch (Exception ex) {
                    logger.error("error in sendingPost session email to attendee: " + attendee.getId() + " error:" + ex.getMessage());
                }
            }
        }
    }

    public List<SessionSnapshot> getSessionSnapshot(String batchId, Long startTime, Long endTime) {

        List<OTFSession> oTFSessions = otfSessionDAO.getSessionForBatchIdBetweenTime(batchId, startTime, endTime);
        List<SessionSnapshot> sessionSnapshots = new ArrayList<>();
        if (oTFSessions != null) {
            for (OTFSession oTFSession : oTFSessions) {
                SessionSnapshot sessionSnapshot = new SessionSnapshot();
                sessionSnapshot.setEndTime(oTFSession.getEndTime());
                sessionSnapshot.setStartTime(oTFSession.getStartTime());
                sessionSnapshot.setSessionId(oTFSession.getId());
                sessionSnapshots.add(sessionSnapshot);
            }
        } else {
            return new ArrayList<>();
        }
        return sessionSnapshots;

    }

    public PlatformBasicResponse updateOTFSessionAdminTechSupport(AddAdminTechSupportToSession addAdminTechSupportToSession) throws BadRequestException {

        if (StringUtils.isEmpty(addAdminTechSupportToSession.getAdminJoined()) && StringUtils.isEmpty(addAdminTechSupportToSession.getTechSupport())
                && StringUtils.isEmpty(addAdminTechSupportToSession.getFinalIssueStatus())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No field to update");
        }

        if (StringUtils.isEmpty(addAdminTechSupportToSession.getSessionId())) {
            return new PlatformBasicResponse(false, null, "empty sessionId");
        }
        Update update = new Update();
        if (!StringUtils.isEmpty(addAdminTechSupportToSession.getAdminJoined())) {
            update.set("adminJoined", addAdminTechSupportToSession.getAdminJoined());
        }
        if (!StringUtils.isEmpty(addAdminTechSupportToSession.getTechSupport())) {
            update.set("techSupport", addAdminTechSupportToSession.getTechSupport());
        }
        if (!StringUtils.isEmpty(addAdminTechSupportToSession.getFinalIssueStatus())) {
            update.set("finalIssueStatus", addAdminTechSupportToSession.getFinalIssueStatus());
        }

        // session state not updated via this flow
        otfSessionDAO.updateSession(update, addAdminTechSupportToSession.getSessionId());

        return new PlatformBasicResponse(true, null, null);

    }

    public Map<String, SessionsAttendeeInfoRes> getUpdatedSessionAttendeeInfo(Set<String> sessionIds) {

        List<OTFSession> sessions = otfSessionDAO.getOTFSessionTeacherJoinTime(sessionIds);

        Map<String, SessionsAttendeeInfoRes> resultMap = new HashMap<>();

        for (OTFSession oTFSession : sessions) {

            Integer attendeeCount = oTFSession.getUniqueStudentAttendants();
            if (attendeeCount == null) {
                attendeeCount = 0;
            }

            SessionsAttendeeInfoRes sessionsAttendeeInfoRes = new SessionsAttendeeInfoRes();
            sessionsAttendeeInfoRes.setCountAttendees(attendeeCount);
            if (oTFSession.getTeacherJoinTime() != null) {
                sessionsAttendeeInfoRes.setCountAttendees(attendeeCount - 1);
            }
            sessionsAttendeeInfoRes.setTeacherJoinTime(oTFSession.getTeacherJoinTime());
            sessionsAttendeeInfoRes.setAdminJoined(oTFSession.getAdminJoined());
            sessionsAttendeeInfoRes.setTechSupport(oTFSession.getTechSupport());
            sessionsAttendeeInfoRes.setFinalIssueStatus(oTFSession.getFinalIssueStatus());

            resultMap.put(oTFSession.getId(), sessionsAttendeeInfoRes);

        }

        return resultMap;
    }


    public List<OTFSessionAttendeeInfo> getOTFSessionAttendeeInfos(String sessionId) {
        Set<String> sessionIds = new HashSet<>();
        sessionIds.add(sessionId);
        Map<String, GTTAttendeeDetails> gttMap = new HashMap<>();
        List<GTTAttendeeDetails> gttAttendeeDetailsList = gttAttendeeDetailsManager
                .getAttendeeDetailsForSessions(sessionIds);

        List<String> userIds = new ArrayList<>();

        for (GTTAttendeeDetails gTTAttendeeDetails : gttAttendeeDetailsList) {
            userIds.add(gTTAttendeeDetails.getUserId());
            gttMap.put(gTTAttendeeDetails.getUserId(), gTTAttendeeDetails);
        }

        Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(userIds, true);
        List<OTFSessionAttendeeInfo> results = new ArrayList<>();
        for (String userId : userIds) {
            if (userBasicInfoMap.containsKey(userId)) {
                UserBasicInfo userBasicInfo = userBasicInfoMap.get(userId);
                if (!Role.STUDENT.equals(userBasicInfo.getRole())) {
                    continue;
                }
                GTTAttendeeDetails gTTAttendeeDetails = gttMap.get(userId);
                OTFSessionAttendeeInfo oTFSessionAttendeeInfo = new OTFSessionAttendeeInfo(userId);
                oTFSessionAttendeeInfo.setActiveIntervals(gTTAttendeeDetails.getActiveIntervals());
                oTFSessionAttendeeInfo.setJoinTimes(gTTAttendeeDetails.getJoinTimes());
                oTFSessionAttendeeInfo.setTimeInSession(gTTAttendeeDetails.getTimeInSession());

                oTFSessionAttendeeInfo.updateUserDetails(userBasicInfo);
                results.add(oTFSessionAttendeeInfo);

            }

        }

        return results;
    }

    public PlatformBasicResponse deleteSession(@RequestBody CancelOrDeleteSessionReq req) throws VException {
        if (com.vedantu.util.StringUtils.isEmpty(req.getSessionId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "sessionid not found");
        }
        OTFSession session = otfSessionDAO.getById(req.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found for " + req.getSessionId());
        }

        if (!(session.getStartTime() != null
                && session.getStartTime() > System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY && SessionState.SCHEDULED.equals(session.getState()))) {
            throw new ForbiddenException(ErrorCode.DELETING_NOT_ALLOWED_WITHIN_24_HRS_OF_START_TIME, "DELETING_NOT_ALLOWED_WITHIN_24_HRS_OF_START_TIME");
        }

        session.setState(SessionState.DELETED);
        if (req.getCallingUserId() != null) {
            session.setDeletedBy(req.getCallingUserId().toString());
        }
        if (!StringUtils.isEmpty(session.getOrganizerAccessToken())) {
            descheduleSessionByBitSet(session);
        }
        otfSessionDAO.save(session);

        // async-- unmark calendar for deleted sessions
        Map<String, Object> payloadunmark = new HashMap<>();
        payloadunmark.put("sessions", Arrays.asList(session));
        payloadunmark.put("fifoGrpId", session.getId());
        payloadunmark.put("mark", false);
        awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS_FIFO, SQSMessageType.INITIATE_ADDED_SESSIONS_MARK_CALENDAR, gson.toJson(payloadunmark), session.getId());
        gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedBulk(session.getId(), DeleteContext.SESSION_STATE_CHANGE);

        try {
            Map<String, Object> payload2 = new HashMap<>();
            payload2.put("sessionInfo", mapper.map(session, OTFSessionPojo.class));
            payload2.put("event", SessionEventsOTF.OTF_SESSION_CANCELED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload2);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for OTF_SESSION_CANCELED " + e);
        }

        return new PlatformBasicResponse();
    }

    public List<String> getBatchIds(String userId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "batch/getBatchIdsForUser?userId=" + userId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new com.google.common.reflect.TypeToken<ArrayList<String>>() {
        }.getType();
        List<String> batchIds = gson.fromJson(jsonString, listType);
        return batchIds;
    }

    public List<CommonSessionInfo> getCommonSessionInfo(String userId) throws VException {
        List<CommonSessionInfo> commonSessionInfos = new ArrayList<>();
        List<String> batchIds = getBatchIds(userId);

        if (ArrayUtils.isNotEmpty(batchIds)) {

        }

        return new ArrayList<>();
    }

    public List<GTTAttendeeSessionInfo> getAttendenceForBatches(List<String> batchIds, String userId) {
        // Long currentTime = System.currentTimeMillis();
        logger.info("IN SCHEDULING getAttendenceForBatches");
        List<GTTAttendeeSessionInfo> results = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        logger.info("OTFSession Query {}", query);
        // Fetching all scheduled OTFSessions
        List<OTFSession> oTFSessions = new ArrayList<>();
        int size = 100;
        int start = 0;
        while (true) {
            otfSessionDAO.setFetchParameters(query, start, size);
            List<OTFSession> oTFSessions1 = otfSessionDAO.runQuery(query, OTFSession.class);
            if (ArrayUtils.isEmpty(oTFSessions1)) {
                break;
            }

            oTFSessions.addAll(oTFSessions1);

            if (oTFSessions1.size() < size) {
                break;
            }
            start = start + size;

        }

        List<String> sessionIds = new ArrayList<>();

        if (ArrayUtils.isEmpty(oTFSessions)) {
            return results;
        }
        logger.info("OTFSessions size " + oTFSessions.size());
        Map<String, OTFSession> sessionMap = new HashMap<>();

        for (OTFSession oTFSession : oTFSessions) {
            sessionIds.add(oTFSession.getId());
            sessionMap.put(oTFSession.getId(), oTFSession);
        }

        Query gttquery = new Query();
        gttquery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        gttquery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        // gttquery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).ne(EntityState.DELETED));
        // gttquery.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false));
        // gttquery.addCriteria(Criteria.where("joinTimes.0").exists(true));
        gttquery.addCriteria(new Criteria().orOperator(
                Criteria.where(GTTAttendeeDetails.Constants.DELETED_CONTEXT).ne(DeleteContext.UPCOMING_SESSION),
                Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false),
                Criteria.where(GTTAttendeeDetails.Constants.TIME_IN_SESSION).gt(0)
        ));

        logger.info("GTTQuery {}", gttquery);
        Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();
        BiFunction<GTTAttendeeDetails, GTTAttendeeDetails, GTTAttendeeDetails> mergeFunctions = (e1, e2) -> {
            if (e1.getTimeInSession() != null && e1.getTimeInSession() > 0) {
                return e1;
            }
            if (e2.getTimeInSession() != null && e2.getTimeInSession() > 0) {
                return e2;
            }
            if (e1.getEntityState() == EntityState.ACTIVE) {
                return e1;
            }
            if (e2.getEntityState() == EntityState.ACTIVE) {
                return e2;
            }
            if (e1.getCreationTime() != null && e2.getCreationTime() != null && e1.getCreationTime() > e2.getCreationTime()) {
                return e1;
            } else {
                return e2;
            }
        };

        start = 0;
        size = 100;
        while (true) {
            gttAttendeeDetailsDAO.setFetchParameters(gttquery, start, size);
            List<GTTAttendeeDetails> tempResults = gttAttendeeDetailsDAO.runQuery(gttquery, GTTAttendeeDetails.class);

            if (ArrayUtils.isEmpty(tempResults)) {
                break;
            }

            for (GTTAttendeeDetails tempResult : tempResults) {
                String sessionId = tempResult.getSessionId();
                attendeeMap.merge(sessionId, tempResult, mergeFunctions);
            }
            if (tempResults.size() < size) {
                break;
            }
            start = start + size;

        }
        List<GTTAttendeeDetails> attendeeDetails = new ArrayList<>(attendeeMap.values());
        if (!ArrayUtils.isEmpty(attendeeDetails)) {
            for (GTTAttendeeDetails gttAttendeeDetails : attendeeDetails) {
                GTTAttendeeSessionInfo gTTAttendeeSessionInfo = mapper.map(gttAttendeeDetails, GTTAttendeeSessionInfo.class);
                OTFSession oTFSession = sessionMap.get(gttAttendeeDetails.getSessionId());
                gTTAttendeeSessionInfo.setBatchIds(oTFSession.getBatchIds());
                gTTAttendeeSessionInfo.setStartTime(oTFSession.getStartTime());
                gTTAttendeeSessionInfo.setTitle(oTFSession.getTitle());
                results.add(gTTAttendeeSessionInfo);
            }
        }
        logger.info("getAttendenceForBatches response is " + results);
        return results;

    }

    public Set<String> getTeacherIdsForBatchSessions(String batchId, Long startTime, Long endTime,
            ArrayList<String> _teacherIds) {
        List<OTFSession> sessions = otfSessionDAO.getSessionsForBatchId(batchId, startTime, endTime, _teacherIds);
        Set<String> teacherIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSession oTFSession : sessions) {
                teacherIds.add(oTFSession.getPresenter());
            }
        }
        return teacherIds;
    }

    public List<CommonSessionInfo> convertOTFSessionToCommonSessionInfos(List<OTFSessionPojo> oTFSessionPojos) {

        List<CommonSessionInfo> commonSessionInfos = new ArrayList<>();

        if (ArrayUtils.isEmpty(oTFSessionPojos)) {
            return commonSessionInfos;
        }

        for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
            CommonSessionInfo commonSessionInfo = new CommonSessionInfo();
            commonSessionInfo.setEndTime(oTFSessionPojo.getEndTime());
            commonSessionInfo.setSessionTitle(oTFSessionPojo.getTitle());
            commonSessionInfo.setEntityId(oTFSessionPojo.getId());
            commonSessionInfo.setEntityType(com.vedantu.session.pojo.EntityType.OTF);
            commonSessionInfo.setStartTime(oTFSessionPojo.getStartTime());
            commonSessionInfo.setVimeoId(oTFSessionPojo.getVimeoId());
            commonSessionInfo.setReplayUrls(oTFSessionPojo.getReplayUrls());
            commonSessionInfo.setTeacherId(oTFSessionPojo.getPresenter());
            commonSessionInfo.setDisplayState(oTFSessionPojo.getState().toString());
            commonSessionInfo.setoTFSessionToolType(oTFSessionPojo.getSessionToolType());
            if (ArrayUtils.isNotEmpty(oTFSessionPojo.getAttendeeInfos())) {
                for (OTFSessionAttendeeInfo oTFSessionAttendeeInfo : oTFSessionPojo.getAttendeeInfos()) {
                    if (Role.TEACHER.equals(oTFSessionAttendeeInfo.getRole())) {
                        commonSessionInfo.setTeacherName(oTFSessionAttendeeInfo.getFullName());
                    }
                    if (Role.STUDENT.equals(oTFSessionAttendeeInfo.getRole())) {
                        if (ArrayUtils.isNotEmpty(oTFSessionAttendeeInfo.getJoinTimes())) {
                            commonSessionInfo.setAttended(true);
                        }
                    }

                }
                if (StringUtils.isNotEmpty(commonSessionInfo.getTeacherName())) {
                    commonSessionInfos.add(commonSessionInfo);
                }
            }
        }

        return commonSessionInfos;

    }

    public List<CommonSessionInfo> convertOTFSessionToCommonSessionInfos(List<OTFSessionPojo> oTFSessionPojos, Map<String, EnrollmentPojo> enrollmentMap) throws VException {

        List<CommonSessionInfo> commonSessionInfos = new ArrayList<>();

        if (ArrayUtils.isEmpty(oTFSessionPojos)) {
            return commonSessionInfos;
        }

        for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
            CommonSessionInfo commonSessionInfo = new CommonSessionInfo();
            commonSessionInfo.setEndTime(oTFSessionPojo.getEndTime());
            commonSessionInfo.setSessionTitle(oTFSessionPojo.getTitle());
            commonSessionInfo.setEntityId(oTFSessionPojo.getId());
            commonSessionInfo.setEntityType(com.vedantu.session.pojo.EntityType.OTF);
            commonSessionInfo.setStartTime(oTFSessionPojo.getStartTime());
            commonSessionInfo.setVimeoId(oTFSessionPojo.getVimeoId());
            commonSessionInfo.setReplayUrls(oTFSessionPojo.getReplayUrls());
            commonSessionInfo.setTeacherId(oTFSessionPojo.getPresenter());
            commonSessionInfo.setDisplayState(oTFSessionPojo.getState().toString());
            commonSessionInfo.setoTFSessionToolType(oTFSessionPojo.getSessionToolType());
            Set<String> batchIds = oTFSessionPojo.getBatchIds();
            for (String batchId : batchIds) {
                if (enrollmentMap.containsKey(batchId)) {
                    commonSessionInfo.setBatchId(batchId);
                    commonSessionInfo.setCourseId(enrollmentMap.get(batchId).getCourseId());
                    commonSessionInfo.setCourseName(enrollmentMap.get(batchId).getEntityTitle());
                    break;
                }
            }
            /*
            // To be done if and only if we've to put this extra validation
            if(commonSessionInfo.getBatchId()==null){
                throw new VException(ErrorCode.ENROLLMENT_NOT_FOUND,
                        "No enrollment could be found for any batch id of this session "+oTFSessionPojo.getId(), Level.FATAL);
            }
             */
            if (ArrayUtils.isNotEmpty(oTFSessionPojo.getAttendeeInfos())) {
                for (OTFSessionAttendeeInfo oTFSessionAttendeeInfo : oTFSessionPojo.getAttendeeInfos()) {
                    if (Role.TEACHER.equals(oTFSessionAttendeeInfo.getRole())) {
                        commonSessionInfo.setTeacherName(oTFSessionAttendeeInfo.getFullName());
                    }
                    if (Role.STUDENT.equals(oTFSessionAttendeeInfo.getRole())) {
                        if (ArrayUtils.isNotEmpty(oTFSessionAttendeeInfo.getJoinTimes()) || oTFSessionAttendeeInfo.getStudentSessionJoinTime() != null) {
                            commonSessionInfo.setAttended(true);
                        }
                    }

                }
                if (StringUtils.isNotEmpty(commonSessionInfo.getTeacherName())) {
                    commonSessionInfos.add(commonSessionInfo);
                }
            }
        }

        return commonSessionInfos;

    }

    public List<CommonSessionInfo> convertSessionInfoToCommonSessionInfos(List<SessionInfo> sessionInfos) throws VException {

        if (ArrayUtils.isEmpty(sessionInfos)) {
            return new ArrayList<>();
        }

        List<CommonSessionInfo> commonSessionInfos = new ArrayList<>();

        for (SessionInfo sessionInfo : sessionInfos) {

            CommonSessionInfo commonSessionInfo = new CommonSessionInfo();
            commonSessionInfo.setEndTime(sessionInfo.getEndTime());
            commonSessionInfo.setStartTime(sessionInfo.getStartTime());
            commonSessionInfo.setSessionTitle(sessionInfo.getTitle());
            commonSessionInfo.setEntityId(sessionInfo.getId().toString());
            commonSessionInfo.setVimeoId(sessionInfo.getVimeoId());
            commonSessionInfo.setReplayUrls(Arrays.asList(sessionInfo.getReplayUrl()));
            commonSessionInfo.setEntityType(com.vedantu.session.pojo.EntityType.COURSE_PLAN);
            commonSessionInfo.setTeacherId(sessionInfo.getTeacherId().toString());
            commonSessionInfo.setDisplayState(sessionInfo.getDisplayState().toString());
            if (ArrayUtils.isNotEmpty(sessionInfo.getAttendees())) {
                for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                    if (Role.TEACHER.equals(userSessionInfo.getRole())) {
                        commonSessionInfo.setTeacherName(userSessionInfo.getFullName());
                    }
                    if (Role.STUDENT.equals(userSessionInfo.getRole())) {
                        if (userSessionInfo.getJoinTime() != null) {
                            commonSessionInfo.setAttended(true);
                        }
                    }

                }
                if (StringUtils.isNotEmpty(commonSessionInfo.getTeacherName())) {
                    commonSessionInfos.add(commonSessionInfo);
                }
            }
            if (sessionInfo.getContextId() != null) {
                ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/courseplan/basicInfo/" + sessionInfo.getContextId(),
                        HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                CoursePlanBasicInfo coursePlanBasicInfo = gson.fromJson(jsonString, CoursePlanBasicInfo.class);
                commonSessionInfo.setCourseName(coursePlanBasicInfo.getTitle());

            }

        }
        return commonSessionInfos;

    }

    public String getCachedKeyForUserLatestSessions(String userId) {
        return env + "_USER_3_SESSIONS_" + userId;
    }

    public int getExpiryTimeForLatestSessions(List<CommonSessionInfo> commonSessionInfos) {

        Long currentTime = System.currentTimeMillis();
        Long expiryTime = new Long(DateTimeUtils.MILLIS_PER_MINUTE * LATEST_SESSION_CACHE_EXPIRY_MINUTE);
        expiryTime += currentTime;
        if (ArrayUtils.isNotEmpty(commonSessionInfos)) {
            for (CommonSessionInfo commonSessionInfo : commonSessionInfos) {
                if (commonSessionInfo.getEndTime() != null && commonSessionInfo.getEndTime() < expiryTime && commonSessionInfo.getEndTime() > currentTime) {
                    expiryTime = commonSessionInfo.getEndTime();
                }
            }
        }
        int expiry = (int) ((expiryTime - currentTime) / 1000);
        if (expiry > 0) {
            return expiry;
        }

        return 1;

    }

    private void populateUserBasicInfos(List<SessionInfo> sessionInfos, boolean exposeEmail) {
        if (!CollectionUtils.isEmpty(sessionInfos)) {
            // Collect all the userIds from attendee list
            Set<String> userIds = new HashSet<String>();
            for (SessionInfo sessionInfo : sessionInfos) {
                if (!CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        userIds.add(String.valueOf(userSessionInfo.getUserId()));
                    }
                }
            }

            // Fetch user info and update the session info
            Map<String, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (SessionInfo sessionInfo : sessionInfos) {
                if (!CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                    List<UserSessionInfo> users = new ArrayList<UserSessionInfo>();
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        userSessionInfo.updateUserDetails(userInfos.get(String.valueOf(userSessionInfo.getUserId())));
                        users.add(userSessionInfo);
                    }
                    sessionInfo.setAttendees(users);
                }
            }
        }
    }

    public List<CommonSessionInfo> getCommonSessionInfos(String userId, Role role) throws VException {
        List<CommonSessionInfo> result = new ArrayList<>();
        boolean isNull = false;
        String cachedKey = getCachedKeyForUserLatestSessions(userId);

        String resp = null;

        try {
            resp = redisDAO.get(cachedKey);
        } catch (Exception ex) {
            logger.error("Error in getting from redis for key: " + cachedKey + " : " + ex.getMessage());
        }

        if (StringUtils.isNotEmpty(resp)) {
            Type listType = new TypeToken<List<CommonSessionInfo>>() {
            }.getType();
            result = gson.fromJson(resp, listType);
            if (result != null && result.isEmpty()) {
                isNull = true;
            }
        }
        Map<String, EnrollmentPojo> enrollmentMap = new HashMap<>();
        if (ArrayUtils.isEmpty(result)) {
            List<OTFSessionPojo> upcomingOTMSessions = getUpcomingSessions(userId, role, null, 0, 1, null, true, enrollmentMap, null, null, null);
            GetUserUpcomingSessionReq getUserUpcomingSessionReq = new GetUserUpcomingSessionReq();
            getUserUpcomingSessionReq.setCallingUserId(Long.parseLong(userId));
            getUserUpcomingSessionReq.setCallingUserRole(role);
            getUserUpcomingSessionReq.setSize(1);
            getUserUpcomingSessionReq.setStart(0);
            getUserUpcomingSessionReq.setSessionStates(Arrays.asList(com.vedantu.session.pojo.SessionState.ACTIVE, com.vedantu.session.pojo.SessionState.SCHEDULED, com.vedantu.session.pojo.SessionState.STARTED));
            List<SessionInfo> upcomingOTOSessions = sessionManager.getUserUpcomingSessions(getUserUpcomingSessionReq);
            populateUserBasicInfos(upcomingOTOSessions, false);
            List<CommonSessionInfo> upcomingCommonSessionInfos = convertOTFSessionToCommonSessionInfos(upcomingOTMSessions, enrollmentMap);
            upcomingCommonSessionInfos.addAll(convertSessionInfoToCommonSessionInfos(upcomingOTOSessions));
            logger.info(upcomingCommonSessionInfos);
            upcomingCommonSessionInfos.sort(new Comparator<CommonSessionInfo>() {
                @Override
                public int compare(CommonSessionInfo o1, CommonSessionInfo o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });

            List<OTFSessionPojo> pastOTMSessions = getPastSessions(userId, role, null, 0, 1, null, true, null, null, null);
            GetUserPastSessionReq getUserPastSessionReq = new GetUserPastSessionReq();
            getUserPastSessionReq.setUserId(Long.parseLong(userId));
            getUserPastSessionReq.setCallingUserId(Long.parseLong(userId));
            getUserPastSessionReq.setCallingUserRole(role);
            getUserPastSessionReq.setStart(0);
            getUserPastSessionReq.setSize(1);
            getUserPastSessionReq.setSessionStates(Arrays.asList(com.vedantu.session.pojo.SessionState.ACTIVE, com.vedantu.session.pojo.SessionState.SCHEDULED, com.vedantu.session.pojo.SessionState.STARTED, com.vedantu.session.pojo.SessionState.ENDED));
            List<SessionInfo> pastOTOSessions = sessionManager.getUserPastSessions(getUserPastSessionReq, role);
            populateUserBasicInfos(pastOTOSessions, false);
            List<CommonSessionInfo> pastCommonSessionInfos = convertOTFSessionToCommonSessionInfos(pastOTMSessions, enrollmentMap);
            pastCommonSessionInfos.addAll(convertSessionInfoToCommonSessionInfos(pastOTOSessions));
            logger.info("Upcoming : " + pastCommonSessionInfos);
            pastCommonSessionInfos.sort(new Comparator<CommonSessionInfo>() {
                @Override
                public int compare(CommonSessionInfo o1, CommonSessionInfo o2) {
                    return o2.getStartTime().compareTo(o1.getStartTime());
                }

            });

            //upcomingCommonSessionInfos = upcomingCommonSessionInfos.subList(0, 1);
            //pastCommonSessionInfos = pastCommonSessionInfos.subList(0, 1);
            if (!upcomingCommonSessionInfos.isEmpty()) {

                result.add(upcomingCommonSessionInfos.get(0));
            }
            if (!pastCommonSessionInfos.isEmpty()) {
                CommonSessionInfo pastSession = pastCommonSessionInfos.get(0);
                pastSession.setIsPastSession(true);
                result.add(pastSession);

            }

            int expiry = getExpiryTimeForLatestSessions(result);
            logger.info("Expiry time: " + expiry);
            try {
                redisDAO.setex(cachedKey, gson.toJson(result), expiry);
            } catch (Exception ex) {
                logger.error("Error in caching get 2 sessions for: " + cachedKey + " : " + ex.getMessage());
            }
        }

        return result;
    }

    public List<GTTOrganizerToken> getOrganizerTokens(List<String> seatValues) {
        return gttOrganizerTokenDAO.getTokens(seatValues);
    }

    public void updateOrganizerTokens(UpdateOrganizerTokenReq req) {
        String errors = "";
        if (req != null && !ArrayUtils.isEmpty(req.getOrganizerTokens())) {
            for (GTTOrganizerToken organizerToken : req.getOrganizerTokens()) {
                GTTOrganizerToken tempToken = gttOrganizerTokenDAO.getTokenForSeat(organizerToken.getSeatValue());
                if (tempToken == null) {
                    errors += organizerToken.getSeatValue() + ", ";
                    logger.info("Error in finding token for " + organizerToken.getSeatValue());
                    continue;
                }
                tempToken.setExpiresIn(organizerToken.getExpiresIn());
                tempToken.setOrganizerAccessToken(organizerToken.getOrganizerAccessToken());
                tempToken.setEncodedString(organizerToken.getEncodedString());
                tempToken.setRefreshToken(organizerToken.getRefreshToken());
                gttOrganizerTokenDAO.update(tempToken);
            }

        }
        if (!StringUtils.isEmpty(errors)) {
            logger.error("error in updating Token for seats: " + errors);
        }
    }

    public String checkOrganizerTokenExpiry() throws ConflictException {
//        List<String> seats = new ArrayList<>();
        Long expiryTime = System.currentTimeMillis() + TOKEN_EXPIRY_CHECK_MINUTE;
        logger.info("Checking expiry for token : " + expiryTime);

        List<GTTOrganizerToken> tokens = gttOrganizerTokenDAO.checkExpiryForTokens(expiryTime);
        if (ArrayUtils.isNotEmpty(tokens)) {
            String errors = "";
            for (GTTOrganizerToken organizerToken : tokens) {
                errors += organizerToken.getSeatValue() + ", ";

            }
            throw new ConflictException(ErrorCode.TOKEN_NOT_REFRESHED, "Tokens to expire found : " + errors);
        } else {
            return "DONE";
        }

    }

    public OTFSession addWebinarSession(AddWebinarSessionReq req) throws VException {
        OTFSession otfSession;
        CalendarEntryReq calendarEntryReq;
        String newPresenter = Long.toString(req.getPresenter());
        if (StringUtils.isEmpty(req.getSessionId())) {
            otfSession = OTFSession.createWebinarSession(req);
            checkSlotAvailabilityForUser(new SessionSlot(req.getStartTime(), req.getEndTime()), newPresenter);
            scheduleSessionByBitSet(otfSession);
        } else {
            otfSession = otfSessionDAO.getById(req.getSessionId());

            if (otfSession == null) {
                throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found for " + req.getSessionId());
            }

            // check slot availability for teacher
            if (!newPresenter.equals(otfSession.getPresenter())) {
                // presenter is not same -- check for entire slot
                checkSlotAvailabilityForUser(new SessionSlot(req.getStartTime(), req.getEndTime()), newPresenter);
            } else {
                List<SessionSlot> rangesToCheck = CommonCalendarUtils.getTimeIntervalsToCheckForConflicts(otfSession.getStartTime(),
                        otfSession.getEndTime(), req.getStartTime(), req.getEndTime());
                for (SessionSlot slot : rangesToCheck) {
                    checkSlotAvailabilityForUser(slot, newPresenter);
                }

            }

            // unblock old presenter and session calendar entries TODO handle when unblocking fails
            calendarEntryReq = new CalendarEntryReq(otfSession.getPresenter(), otfSession.getStartTime(),
                    otfSession.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION, otfSession.getId());
            BasicRes response = calendarEntryManager.removeCalendarEntrySlotsUsingUserId(calendarEntryReq);

            // Cannot update session in past
//            if (otfSession.getStartTime() < System.currentTimeMillis()) {
//                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Cannot update session in past");
//            }
            // Update session timings if required
            if (!otfSession.getStartTime().equals(req.getStartTime())
                    || !otfSession.getEndTime().equals(req.getEndTime())) {
                // De-schedule the session
                if (!StringUtils.isEmpty(otfSession.getOrganizerAccessToken())) {
                    descheduleSessionByBitSet(otfSession);
                }

                // Update session timings
                otfSession.setStartTime(req.getStartTime());
                otfSession.setEndTime(req.getEndTime());
                otfSession.setSessionToolType(req.getSessionToolType());

                // Schedule the session
                scheduleSessionByBitSet(otfSession);
            }
            otfSession.setTitle(req.getTitle());
            otfSession.setPresenter(req.getPresenter().toString());
            otfSession.setTaIds(req.getTaIds());
        }

        // block calendar entries for the newPresenter and updated session timings
        calendarEntryReq = new CalendarEntryReq(newPresenter, req.getStartTime(),
                req.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION, otfSession.getId());
        calendarEntryManager.upsertCalendarEntry(calendarEntryReq);

        if (OTFSessionToolType.VEDANTU_WAVE.equals(req.getSessionToolType())) {
            // changing stream type to VEDANTU
            otfSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
        }
        if (Optional.ofNullable(req.getEntityTags()).orElseGet(HashSet::new).contains(EntityTag.BIG_WHITE_BOARD)) {
            // changing stream type to AGORA
            otfSession.setCanvasStreamingType(CanvasStreamingType.AGORA);
        }
        if (req.getIsSimLive() != null && req.getIsSimLive()) {
            String parentSessionId = req.getParentWebinarSessionId();
            OTFSession parentSession = otfSessionDAO.getById(parentSessionId);
            if (parentSession == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "parent session not found");
            }

            if ((parentSession.getEndTime() + BUFFER_TIME_TO_SCHEDULE_SIM_LIVE_SESSIONS) > otfSession.getStartTime()) {
                throw new BadRequestException(ErrorCode.SCHEDULING_NOT_ALLOWED_WITHIN_2_HR_OF_PARENT_SESSION_END_TIME,
                        "SCHEDULING_NOT_ALLOWED_WITHIN_2_HR_OF_PARENT_SESSION_END_TIME");
            }

            otfSession.addEntityTags(EntityTag.SIMULATED_LIVE);
            otfSession.setParentSessionId(parentSessionId);
        }

        otfSession.setWebinarId(req.getWebinarId());
        logger.info("addWebinarSession saving session: " + otfSession);
        otfSessionDAO.save(otfSession);

        if (otfSession.getStartTime() != null
                && otfSession.getStartTime() < System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) {
            if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", oTFSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
                sendToQForGeneratingSessionLinkAndGTT(otfSession);
            }
        }

        return otfSession;
    }

    public void sendRegularSessionMail(OTFSession session, SQSMessageType messageType) throws ConflictException {
        session = otfSessionDAO.getByIdWithExclude(session.getId(), Collections.singletonList(ATTENDEES));
        int limit = 500;
        List<GTTAttendeeDetails> gttAttendeeDetails = new ArrayList<>();
        String lastFetchedId = null;
        do {

            gttAttendeeDetails = gttAttendeeDetailsDAO.getAttendeesForSessionId(session.getId(), lastFetchedId, limit);

            if (ArrayUtils.isNotEmpty(gttAttendeeDetails)) {
                lastFetchedId = gttAttendeeDetails.get(gttAttendeeDetails.size() - 1).getId();
            }

            List<String> userIds = gttAttendeeDetails.stream()
                    .filter(e -> StringUtils.isNotEmpty(e.getEnrollmentId()))
                    .map(GTTAttendeeDetails::getUserId).collect(toList());

            if (!userIds.isEmpty()) {
                logger.info("Tregaring regular reminder emails for user IDs  " + userIds);
                Long boardId = session.getBoardId();
                Board board = null;
                if (boardId != null) {
                    List<Long> boardIds = new ArrayList();
                    boardIds.add(boardId);
                    Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
                    if (boardMap != null && boardMap.containsKey(boardId)) {
                        board = boardMap.get(boardId);
                    }
                }

                HashMap<String, Object> bodyScopes = communicationManager.getBasicOTMSessionInfoMap(session);
                if (board != null) {
                    bodyScopes.put("sessionSubject", board.getName());
                }

                try {
                    GroupOfUserEmail groupOfUserEmail = new GroupOfUserEmail();
                    groupOfUserEmail.setBodyScopeOfEmail(bodyScopes);
                    groupOfUserEmail.setUserIds(userIds);
                    groupOfUserEmail.setSessionId(session.getId());
                    if (messageType == SQSMessageType.SESSION_USER_BATCH_FOR_MAIL) {
                        groupOfUserEmail.setExpectedAt(session.getStartTime() - DateTimeUtils.MILLIS_PER_MINUTE * 60);
                    } else if (messageType == SQSMessageType.SESSION_USER_BATCH_FOR_LIVE_MAIL) {
                        groupOfUserEmail.setExpectedAt(session.getStartTime());
                    }
                    awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, messageType, gson.toJson(groupOfUserEmail));
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }

        } while (ArrayUtils.isNotEmpty(gttAttendeeDetails));

    }

    private Set<String> getFirstThreeSessionUsers(String otfSessionId, List<String> userIds) {
        Set<String> firstThreeSessionUsers = new HashSet<>();
        Map<String, Set<String>> map = new HashMap<>();
        logger.info("Session Id " + otfSessionId);
        return gttAttendeeDetailsDAO.getFirstThreeSessionUsers(userIds, otfSessionId);
    }

    public void processRegularSessionEmailForGroup(GroupOfUserEmail group, CommunicationType communicationType) {
        List<String> userIds = group.getUserIds();
        final HashMap<String, Object> bodyScopeOfEmail = group.getBodyScopeOfEmail();
        long expectedAt = group.getExpectedAt();
        logger.info("Getting First three session user ids for " + group.getSessionId());
        Set<String> users = getFirstThreeSessionUsers(group.getSessionId(), userIds);
        logger.info("First Three Session Users for Session " + group.getSessionId() + " - " + users);

        Consumer<List<String>> listConsumer = (List<String> list) -> {
            for (String userId : list) {

                try {
                    logger.info("Sending mail for OTF session user " + userId);
                    boolean contains = users.contains(userId);
                    logger.info("user is eligible for sms " + contains);
                    communicationManager.sendNewSessionRelatedEmail(Long.valueOf(userId), bodyScopeOfEmail,
                            communicationType, contains, expectedAt);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        };

        RecursiveActionForCollection<String, List<String>> action = new RecursiveActionForCollection<>(100, listConsumer, userIds);
        forkJoinPoolUtility.execute(action);
    }

    public List<OTFSessionPojo> getSessionsForUserWithBatchShuffle(GetOTFSessionsListReq req) throws ForbiddenException, BadRequestException, VException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Forbidden access");
        }

        if (StringUtils.isEmpty(req.getBatchId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchId is missing");
        }

        if (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole())) {
            return getOTFSessions(req);
        }
        List<EntityStatus> entityStatuses = new ArrayList<EntityStatus>();
        entityStatuses.add(EntityStatus.ACTIVE);
        entityStatuses.add(EntityStatus.INACTIVE);
        List<OTFSessionPojo> result = new ArrayList<>();
        GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
        enrolReq.setBatchId(req.getBatchId());
        enrolReq.setUserId(sessionData.getUserId());
        enrolReq.setStatuses(entityStatuses);
        Long latestInactiveTime = null;
        List<EnrollmentPojo> enrollments = getEnrollmentsForUser(enrolReq);
        List<BatchChangeTime> batchChangeTimes = new ArrayList<BatchChangeTime>();
        if (ArrayUtils.isNotEmpty(enrollments)) {
            for (EnrollmentPojo enrollmentPojo : enrollments) {
                if (EntityStatus.INACTIVE.equals(enrollmentPojo.getStatus())) {
                    latestInactiveTime = enrollmentPojo.getLastUpdated();
                }
                batchChangeTimes.addAll(enrollmentPojo.getBatchChangeTime());
            }
            if (latestInactiveTime != null) {
                req.setTillTime(latestInactiveTime);
            }
            BatchChangeTime batchChangeTime = new BatchChangeTime();
            batchChangeTime.setPreviousBatchId(req.getBatchId());
            if (latestInactiveTime != null) {
                batchChangeTime.setChangeTime(latestInactiveTime);
            }
            batchChangeTimes.add(batchChangeTime);
            req.setBatchChangeTime(batchChangeTimes);
            List<OTFSessionPojo> oTFSessionPojos = getOTFSessionsBatchSuffle(req);
            if (Role.STUDENT.equals(sessionData.getRole())) {
                for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                    oTFSessionPojo.setAttendees(new ArrayList<>());
                    oTFSessionPojo.setAttendeeInfos(new ArrayList<>());
                }
            }
            return oTFSessionPojos;
        } else {
            return result;
        }
    }

    public List<OTFSessionPojo> getOTFSessionsBatchSuffle(GetOTFSessionsListReq req) {
        List<OTFSessionPojo> result = new ArrayList<>();
        Query sessionQuery = new Query();
        Criteria criteria = new Criteria();
        List<Criteria> criteriaList = new ArrayList<Criteria>();
        Long startTime = 0L;
        for (BatchChangeTime batchChangeTime : req.getBatchChangeTime()) {
            Criteria criteria1 = new Criteria();
            if (batchChangeTime.getChangeTime() != null) {
                criteria1.andOperator(Criteria.where(OTFSession.Constants.START_TIME).lt(batchChangeTime.getChangeTime()),
                        Criteria.where(OTFSession.Constants.START_TIME).gte(startTime),
                        Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchChangeTime.getPreviousBatchId()));
            } else {
                criteria1.andOperator(Criteria.where(OTFSession.Constants.START_TIME).gte(new Long(startTime)),
                        Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchChangeTime.getPreviousBatchId()));
            }
            startTime = batchChangeTime.getChangeTime();
            criteriaList.add(criteria1);
        }

        criteria.orOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
        sessionQuery.addCriteria(criteria);
        if (req.getTeacherId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(req.getTeacherId().toString()));
        }

        if (req.getBoardId() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(req.getBoardId()));
        }
        if (ArrayUtils.isNotEmpty(req.getStates())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(req.getStates()));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        }
        if (req.getSortOrder() != null) {
            sessionQuery.with(Sort.by(Direction.valueOf(req.getSortOrder().name()), OTFSession.Constants.END_TIME));
        } else {
            sessionQuery.with(Sort.by(Direction.ASC, OTFSession.Constants.END_TIME));
        }
        logger.info("\n Session query :-  " + sessionQuery + " \n");
        otfSessionDAO.setFetchParameters(sessionQuery, req);
        List<OTFSession> sessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        if (ArrayUtils.isNotEmpty(sessions)) {
            result = getSessionInfos(sessions, null);
        }
        return result;
    }

    public List<String> getTopicsByEntity(String entityId, CurriculumEntityName entityName) throws VException {
        String queryStringLms = LMS_ENDPOINT + "curriculum/getTopicsByEntity?entityId=" + entityId + "&entityName=" + entityName;

        ClientResponse resp = WebUtils.INSTANCE.doCall(queryStringLms, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<String>>() {
        }.getType();
        List<String> result = gson.fromJson(jsonString, listType);
        return result;
    }

    public void validateSessionTopics(List<String> sessionTopics, List<String> curricullumTopics) throws VException {
        //  List<String> _curricullumTopics = new ArrayList<String>();

        for (String topics : sessionTopics) {
            if (!curricullumTopics.contains(topics)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, topics + " is not part of batch curriculum");
            }
        }
    }

    public void sendNotificationToAcadOps() throws VException {
        logger.info("Start of debugging");

        long currentTime = System.currentTimeMillis();

        long _1hr_ = DateTimeUtils.MILLIS_PER_HOUR;
        long _15min_ = DateTimeUtils.MILLIS_PER_MINUTE * 15;
        long _4hr_ = 4 * _1hr_;
        long _8hr_ = 2 * _4hr_;

        // Get all the required completed session
        List<OTFSession> before1Hour = otfSessionDAO.getEndedOTFSessionWithin(currentTime - _1hr_ - _15min_, currentTime - _1hr_);
        logger.info("Getting data in between " + getFormattedDate(currentTime - _1hr_ - _15min_) + " to " + getFormattedDate(currentTime - _1hr_));
        logger.info("before1Hour data size is " + before1Hour.size());
        List<OTFSession> before4Hour = otfSessionDAO.getEndedOTFSessionWithin(currentTime - _4hr_ - _15min_, currentTime - _4hr_);
        logger.info("Getting data in between " + getFormattedDate(currentTime - _4hr_ - _15min_) + " to " + getFormattedDate(currentTime - _4hr_));
        logger.info("before4Hour data size is " + before4Hour.size());
        List<OTFSession> before8Hour = otfSessionDAO.getEndedOTFSessionWithin(currentTime - _8hr_ - _15min_, currentTime - _8hr_);
        logger.info("Getting data in between " + getFormattedDate(currentTime - _8hr_ - _15min_) + " to " + getFormattedDate(currentTime - _8hr_));
        logger.info("before8Hour data size is " + before8Hour.size());

        // Check if for each batch of the completed session curriculum is curriculumUpdated or not
        checkForCurriculumUpdateAndSendEmail(before1Hour, currentTime - _1hr_ - _15min_, CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_1_HOUR);
        checkForCurriculumUpdateAndSendEmail(before4Hour, currentTime - _4hr_ - _15min_, CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_4_HOUR);
        checkForCurriculumUpdateAndSendEmail(before8Hour, currentTime - _8hr_ - _15min_, CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_8_HOUR);

    }

    private boolean curriculumUpdated(String batchId, Long boardId, Long lastUpdated) throws VException {
        if (StringUtils.isEmpty(batchId) || Objects.isNull(boardId) || Objects.isNull(lastUpdated)) {
            return false;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "curriculum/getCurriculumForBatchBoardAndLastUpdated/batchId/" + batchId + "/boardId/" + boardId + "/lastUpdated/" + Long.toString(lastUpdated),
                HttpMethod.GET, null);
        logger.info("LMS Endpoint to find updated curriculum is " + LMS_ENDPOINT + "curriculum/getCurriculumForBatchBoardAndLastUpdated/batchId/" + batchId + "/boardId/" + boardId + "/lastUpdated/" + Long.toString(lastUpdated));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<Node>>() {
        }.getType();
        List<Node> curriculums = gson.fromJson(jsonString, listType);
        if (ArrayUtils.isEmpty(curriculums)) {
            logger.info("No updated curriculum found so telling curriculum is not updated so send email.");
            return false;
        }
        logger.info("Curriculum updated, so no need to send email.");
        return true;
    }

    private void checkForCurriculumUpdateAndSendEmail(List<OTFSession> beforeXHour, Long lastUpdated, CommunicationType type) throws VException {
        logger.info("checkForCurriculumUpdateAndSendEmail debugging start");
        String acadOps = "acad-ops@vedantu.com";
        // String acadOps="bibhuti.bhusan@vedantu.com";
        String emailCC[] = {"manish.kumar@vedantu.com", "sridhar.rotnadigikar@vedantu.com", "anand.prakash@vedantu.com", "pulkit.jain@vedantu.com"};
        // String emailCC[]={"kishore.nerilla@vedantu.com"};
        if (!ArrayUtils.isEmpty(beforeXHour)) {
            Map<String, List<String>> sessionBatchMap = new HashMap<>();

            Map<String, Long> sessionTeacherMap = new HashMap<>();
            Map<String, Long> sessionBoardMap = new HashMap<>();
            Map<String, Boolean> batchIdList = new HashMap<>();
            Map<String, Boolean> teacherList = new HashMap<>();
            beforeXHour.forEach(session -> {
                logger.info("Preparing for communication for " + type.toString());
                Set<String> batchIds = session.getBatchIds();
                Long boardId = session.getBoardId();
                sessionBoardMap.put(session.getId(), boardId);
                if (!ArrayUtils.isEmpty(batchIds)) {
                    logger.info("Batch ids present so going for checking for the same");
                    batchIds.forEach(batchId -> {
                        try {
                            if (!curriculumUpdated(batchId, boardId, lastUpdated)) {
                                logger.info("No curriculum is updated, so get ready to send email");
                                List<String> batchIdsOfSession;
                                if (sessionBatchMap.containsKey(session.getId())) {
                                    // batchIdsOfSession=sessionBatchMap.get(batchId);
                                    batchIdsOfSession = sessionBatchMap.get(session.getId());
                                    batchIdsOfSession.add(batchId);
                                } else {
                                    batchIdsOfSession = new ArrayList<>();
                                    batchIdsOfSession.add(batchId);
                                }
                                sessionBatchMap.put(session.getId(), batchIdsOfSession);
                                if (!sessionTeacherMap.containsKey(session.getId())) {
                                    sessionTeacherMap.put(session.getId(), Long.parseLong(session.getPresenter()));
                                    logger.info("The presenter of session with session id " + session.getId() + " is " + session.getPresenter());
                                }
                                batchIdList.put(batchId, Boolean.TRUE);
                                teacherList.put(session.getPresenter(), Boolean.TRUE);
                            }
                        } catch (VException e) {
                            logger.error("The error while retrieving updated curriculum is ", e);
                        }
                    });
                }
            });
            if (!teacherList.isEmpty() && !batchIdList.isEmpty()) {
                List<UserBasicInfo> teachers = fosUtils.getUserBasicInfosSet(teacherList.keySet(), true);
                Map<Long, String> teacherName = new HashMap<>();
                Map<Long, String> teacherEmail = new HashMap<>();
                Map<Long, String> teacherPhoneNo = new HashMap<>();
                Map<Long, String> teacherPhoneCode = new HashMap<>();
                teachers.forEach(teacher -> {
                    logger.info("      The details for teacher with teacher id " + teacher.getUserId() + " with name " + teacher.getFullName() + "     ");
                    if (CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_1_HOUR.equals(type)) {
                        logger.info("Phone code is " + teacher.getPhoneCode());
                        teacherPhoneCode.put(teacher.getUserId(), teacher.getPhoneCode());
                        logger.info("Contact number is " + teacher.getContactNumber());
                        teacherPhoneNo.put(teacher.getUserId(), teacher.getContactNumber());
                    }
                    logger.info("Teacher's name is " + teacher.getFirstName());
                    teacherName.put(teacher.getUserId(), teacher.getFirstName());
                    logger.info("Teacher's email address is " + teacher.getEmail());
                    teacherEmail.put(teacher.getUserId(), teacher.getEmail());
                    logger.info("    End of information about teacher.     ");

                });
                Map<String, String> batchCourseNameMap = new HashMap<>();
                Map<String, String> batchDateMap = new HashMap<>();
                Map<String, String> batchMonthMap = new HashMap<>();
                // List<BatchBasicInfo> batches = getBatchBasicInfosByIds(batchIdList.keySet());

                BatchDetailInfoReq req = new BatchDetailInfoReq(new ArrayList<>(batchIdList.keySet()));
                req.setBatchDataIncludeFields(Arrays.asList(
                        BatchInfo.Constants._ID,
                        BatchInfo.Constants.START_TIME,
                        BatchInfo.Constants.COURSE_ID
                ));
                req.setCourseDataIncludeFields(Arrays.asList(
                        BatchInfo.Constants._ID,
                        BatchInfo.Constants.TITLE
                ));
                List<BatchBasicInfo> batches = getDetailedBatchInfo(req);

                batches.forEach(batch -> {
                    String batchStartTime = getFormattedDate(batch.getStartTime());
                    String[] dmy = batchStartTime.split("-");
                    batchCourseNameMap.put(batch.getBatchId(), batch.getCourseInfo().getTitle());
                    batchDateMap.put(batch.getBatchId(), dmy[0]);
                    batchMonthMap.put(batch.getBatchId(), dmy[1]);
                });
                sessionBatchMap.forEach((session, batchIds) -> batchIds.forEach(batchId -> {
                    logger.info(" For session with session id " + session + " for the batch id " + batchId);
                    EmailRequest emailRequest = new EmailRequest();
                    emailRequest.setClickTrackersEnabled(true);
                    emailRequest.setType(type);

                    Map<String, Object> subjectScopes = new HashMap<>(); //courseName,date,month
                    subjectScopes.put("courseName", batchCourseNameMap.get(batchId));
                    subjectScopes.put("date", batchDateMap.get(batchId));
                    subjectScopes.put("month", batchMonthMap.get(batchId));
                    emailRequest.setSubjectScopes(subjectScopes);

                    Map<String, Object> bodyScopes = new HashMap<>(); // teacherName,courseName,url
                    bodyScopes.put("courseName", batchCourseNameMap.get(batchId));
                    bodyScopes.put("teacherName", teacherName.get(sessionTeacherMap.get(session)));
                    bodyScopes.put("shortenedURL", getDiscussProgressShortenedURL(batchId, sessionBoardMap.get(session)));
                    emailRequest.setBodyScopes(bodyScopes);

                    if (CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_1_HOUR.equals(type)) {
                        List<InternetAddress> tos = new ArrayList<>();
                        InternetAddress to = new InternetAddress();
                        try {
                            to.setPersonal(teacherName.get(sessionTeacherMap.get(session)));
                            to.setAddress(teacherEmail.get(sessionTeacherMap.get(session)));
                        } catch (UnsupportedEncodingException e) {
                            logger.error("Error while setting to address", e);
                        }
                        tos.add(to);
                        emailRequest.setTo(tos);

                        TextSMSRequest textSMSRequest = new TextSMSRequest();
                        textSMSRequest.setPhoneCode(teacherPhoneCode.get(sessionTeacherMap.get(session)));
                        logger.info("The teacher's phone code is " + teacherPhoneCode.get(sessionTeacherMap.get(session)));
                        textSMSRequest.setTo(teacherPhoneNo.get(sessionTeacherMap.get(session)));
                        logger.info("The teacher's phone number is " + teacherPhoneNo.get(sessionTeacherMap.get(session)));
                        textSMSRequest.setType(type);
                        Map<String, Object> scopeParams = new HashMap<>();// teacherName,url
                        scopeParams.put("teacherName", teacherName.get(sessionTeacherMap.get(session)));
                        scopeParams.put("url", getDiscussProgressShortenedURL(batchId, sessionBoardMap.get(session)));
                        textSMSRequest.setScopeParams(scopeParams);
                        textSMSRequest.setRole(Role.TEACHER);
                        try {
                            communicationManager.sendSMS(textSMSRequest);
                        } catch (VException e) {
                            logger.error("Exception in sending sms " + e);
                        }

                    } else if (CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_4_HOUR.equals(type)) {
                        List<InternetAddress> tos = new ArrayList<>();
                        InternetAddress to = new InternetAddress();
                        try {
                            to.setPersonal("acad-ops");
                            to.setAddress(acadOps);
                        } catch (UnsupportedEncodingException e) {
                            logger.error("Error while setting to address", e);
                        }
                        tos.add(to);
                        emailRequest.setTo(tos);
                    } else if (CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_8_HOUR.equals(type)) {
                        List<InternetAddress> cc = new ArrayList<>();
                        List<InternetAddress> tos = new ArrayList<>();
                        InternetAddress to = new InternetAddress();
                        try {
                            to.setPersonal("acad-ops");
                            to.setAddress(acadOps);
                        } catch (UnsupportedEncodingException e) {
                            logger.error("Error while setting to address", e);
                        }
                        tos.add(to);
                        emailRequest.setTo(tos);
                        for (String email : emailCC) {
                            to = new InternetAddress();
                            to.setAddress(email);
                            cc.add(to);
                        }
                        emailRequest.setCc(cc);

                    }
                    try {
                        communicationManager.sendEmail(emailRequest);
                    } catch (VException e) {
                        logger.error("Exception in sending email " + e);
                    }
                }));
            }
        }
        logger.info("checkForCurriculumUpdateAndSendEmail debugging end");
    }

    private String getDiscussProgressShortenedURL(String batchId, Long boardId) {
        return WebUtils.INSTANCE.shortenUrl(FOS_ENDPOINT + "/v/1-to-few/batch/" + batchId + "/progress?boardId=" + boardId);
    }

    public String getFormattedDate(Long epoch) {
        return getFormattedDateForLeadSquared(epoch, false);
    }

    public String getFormattedDateForLeadSquared(Long epoch, Boolean isLeadSquared) {
        Date date = new Date(epoch);
        DateFormat format;
        if (isLeadSquared) {
            format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
        } else {
            format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        }
        String formattedDate = format.format(date);
        return formattedDate;
    }



    @ApiOperation("return only  for 10 sessions for given star and end time  for active enrollments ")
    public List<OTFSessionPojoUtils> getSessionsForActiveUserTools(UserInfo user, Long fromTime, Long toTime, Integer start, Integer size) throws VException {
        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        List<OTFSessionPojo> oTFSessionPojos = new ArrayList<>();
        Set<String> batchIds = new HashSet<>();
        Query sessionQuery = new Query();
        GetEnrollmentsReq enrolReq = new GetEnrollmentsReq();
        enrolReq.setStatus(EntityStatus.ACTIVE);
        enrolReq.setUserId(user.getUserId());
        List<EnrollmentPojo> enrollments = getEnrollmentsDataForSessionStrip(enrolReq);
        if (!CollectionUtils.isEmpty(enrollments)) {
            for (EnrollmentPojo enrollment : enrollments) {
                batchIds.add(enrollment.getBatchId());
            }
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(user.getUserId().toString()));
        }
        if (!ArrayUtils.isEmpty(batchIds)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        }
        start = (start == null) ? 0 : start;
        size = (size == null) ? 10 : size;
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(fromTime));
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(toTime));
        sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.END_TIME));
        sessionQuery.skip(start);
        sessionQuery.limit(size);
        List<OTFSession> oTFSessions = otfSessionDAO.runQuery(sessionQuery, OTFSession.class);
        List<String> sessionIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(oTFSessions)) {
            Map<String, SessionAdditionalInfo> sessionAdditionalInfoMap = createBatchIdCourseMap(oTFSessions);
            oTFSessionPojos = getSessionInfos(oTFSessions, sessionAdditionalInfoMap);
            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                sessionIds.add(oTFSessionPojo.getId());
            }
            List<GTTAttendeeDetails> gttAttendeeDetails = gttAttendeeDetailsDAO.getAttendeesForSessionIdsForUser(user.getUserId().toString(), sessionIds);
            Map<String, GTTAttendeeDetails> attendeeDetailsMap = new HashMap<>();
            for (GTTAttendeeDetails gttAttendeeDetail : gttAttendeeDetails) {
                attendeeDetailsMap.put(gttAttendeeDetail.getSessionId(), gttAttendeeDetail);

            }
            for (OTFSessionPojo oTFSessionPojo : oTFSessionPojos) {
                OTFSessionAttendeeInfo sessionAttendeeInfo = mapper.map(user, OTFSessionAttendeeInfo.class);
                if (attendeeDetailsMap.containsKey(oTFSessionPojo.getId())) {
                    sessionAttendeeInfo.setJoinTimes(attendeeDetailsMap.get(oTFSessionPojo.getId()).getJoinTimes());
                    sessionAttendeeInfo.setTimeInSession(attendeeDetailsMap.get(oTFSessionPojo.getId()).getTimeInSession());
                    sessionAttendeeInfo.setActiveIntervals(attendeeDetailsMap.get(oTFSessionPojo.getId()).getActiveIntervals());
                }
                oTFSessionPojo.getAttendeeInfos().add(sessionAttendeeInfo);
                sessions.add(mapper.map(oTFSessionPojo, OTFSessionPojoUtils.class));
            }
            populatePresenterInfos(sessions, false);
        }
        return sessions;
    }

    private void populatePresenterInfos(List<OTFSessionPojoUtils> sessions, boolean exposeEmail) {
        if (!com.vedantu.util.CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                userIds.add(session.getPresenter());
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojoUtils session : sessions) {
                if (userMap.containsKey(session.getPresenter())) {
                    session.setPresenterInfo(userMap.get(session.getPresenter()));
                }
            }
        }
    }

    private void populatePresenterInfosInOtfPojo(List<OTFSessionPojo> sessions, boolean exposeEmail) {
        if (!com.vedantu.util.CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojo session : sessions) {
                userIds.add(session.getPresenter());
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojo session : sessions) {
                if (userMap.containsKey(session.getPresenter())) {
                    session.setPresenterInfo(userMap.get(session.getPresenter()));
                }
            }
        }
    }

    private void populateAttendeeInfosInOtfPojo(List<OTFSessionPojo> sessions, boolean exposeEmail) {
        if (!com.vedantu.util.CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojo session : sessions) {
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendees())) {
                    userIds.addAll(session.getAttendees());
                }
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendeeInfos())) {
                    Set<String> allAttendees = session.getAttendeeInfos().stream()
                            .map(OTFSessionAttendeeInfo::getUserId).filter(Objects::nonNull).map(String::valueOf)
                            .collect(Collectors.toSet());
                    if (allAttendees != null) {
                        userIds.addAll(allAttendees);
                    }
                }
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojo session : sessions) {
                if (!com.vedantu.util.CollectionUtils.isEmpty(session.getAttendees())) {
                    for (String studentId : session.getAttendees()) {
                        if (userMap.containsKey(studentId)) {
                            session.updateUserBasicInfo(userMap.get(studentId));
                        }
                    }
                }
            }
        }
    }

    private void populateAttendeeInfoInOtfPojoForUser(List<OTFSessionPojo> sessions, String userId, boolean exposeEmail) {
        if (StringUtils.isNotEmpty(userId)) {
            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(Collections.singleton(userId), exposeEmail);
            for (OTFSessionPojo session : sessions) {
                if (userMap.containsKey(userId)) {
                    session.updateUserBasicInfo(userMap.get(userId));
                }
            }
        }
    }

    private void setupSimLiveSession(OTFSession oTFSession, OTFSession result) throws VException {
        String parentSessionId = oTFSession.getParentSessionId();
        if (parentSessionId == null || StringUtils.isEmpty(parentSessionId)) {
            throw new BadRequestException(ErrorCode.MISSING_PARENT_SESSIONID, "MISSING_PARENT_SESSIONID");
        } else {
            OTFSession parentSession = otfSessionDAO.getById(parentSessionId);
            if (parentSession == null) {
                logger.error("Session not found for id : " + parentSessionId);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "parent session not found with id " + parentSessionId);
            }
            if ((parentSession.getEndTime() + BUFFER_TIME_TO_SCHEDULE_SIM_LIVE_SESSIONS) > oTFSession.getStartTime()) {
                throw new BadRequestException(ErrorCode.SCHEDULING_NOT_ALLOWED_WITHIN_2_HR_OF_PARENT_SESSION_END_TIME,
                        "SCHEDULING_NOT_ALLOWED_WITHIN_2_HR_OF_PARENT_SESSION_END_TIME");
            }
        }
        if (result != null) {
            result.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
            result.setLabels(Sets.newHashSet(SessionLabel.WEBINAR));
            result.setParentSessionId(parentSessionId);
        } else {
            oTFSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
        }
    }

    private void setAttendeesForWaveOTO(OTFSession otfSession) throws VException {
        logger.info("setAttendeesForWaveOTO" + otfSession.getId());
        if (otfSession.isWebinarSession()) {
            // swallow
        } else {
            JSONObject reqJsonObj = new JSONObject();
            reqJsonObj.put("batchIds", otfSession.getBatchIds());
            reqJsonObj.put("size", 10);
            reqJsonObj.put("start", 0);

            String enrollUrl = SUBSCRIPTION_ENDPOINT + "enroll/getActiveUserIdsByBatchIds";
            ClientResponse subResponse = WebUtils.INSTANCE.doCall(enrollUrl, HttpMethod.POST, reqJsonObj.toString());

            VExceptionFactory.INSTANCE.parseAndThrowException(subResponse);
            String jsonString = subResponse.getEntity(String.class);

            Type listType = new TypeToken<List<ActiveUserIdsByBatchIdsResp>>() {
            }.getType();
            List<ActiveUserIdsByBatchIdsResp> activeUserIdsResp = gson.fromJson(jsonString, listType);
            logger.info( "activeUserIdsResp"+ activeUserIdsResp.size() );
            Set<String> attendeeIds = new HashSet<>();
            for (ActiveUserIdsByBatchIdsResp enrollObj : activeUserIdsResp) {
                attendeeIds.add(enrollObj.getUserId());
            }
            List<String> uniqueAttendeeIds = new ArrayList<>(attendeeIds);
            if (!ArrayUtils.isEmpty(uniqueAttendeeIds)) {
                otfSession.setAttendees(uniqueAttendeeIds);
            }
            logger.info( "attendees"+ otfSession.getAttendees().size() );
        }

    }
    public PlatformBasicResponse setAttendeesInSession(String sessionId) throws VException {
        PlatformBasicResponse res = new PlatformBasicResponse();
        OTFSession otfSession = otfSessionDAO.getById( sessionId );
        if(Objects.isNull( otfSession )) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found for " + sessionId);
        }
        setAttendeesForWaveOTO(otfSession);
        otfSessionDAO.save( otfSession );
        return res;
    }

    public void checkSlotAvailabilityForUser(SessionSlot slot, String presenter) throws VException {
        List<SessionSlot> availableSlots = calendarEntryManager.getSlotListAvailability(Arrays.asList(slot), presenter, null);
        String errorMessage = "Slot not available for these slots : " + slot.toString();

        if (CollectionUtils.isEmpty(availableSlots) || availableSlots.size() == 0) {
            logger.info(errorMessage);
            throw new ConflictException(ErrorCode.USER_NOT_AVAILABLE, errorMessage);
        }

        for (SessionSlot s : availableSlots) {
            if (Objects.equals(slot.getStartTime(), s.getStartTime())
                    && Objects.equals(slot.getEndTime(), s.getEndTime())) {
                return;
            }
        }

        // if none of the available slots match with the slot required, the user is not available
        throw new ConflictException(ErrorCode.USER_NOT_AVAILABLE, errorMessage);
    }

    public List<GTTAggregatedStudentList> getNewAbsenteeStudentListForSession(String sessionId, String lastSessionId) throws VException {
        OTFSession sessionObj = otfSessionDAO.getById(sessionId);
        if (sessionObj == null) {
            String errorMessage = "Session does not exist for req : " + sessionId;
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        String presenter = sessionObj.getPresenter();
        Set<String> batchIds = sessionObj.getBatchIds();

        JSONObject reqJsonObj = new JSONObject();
        reqJsonObj.put("batchIds", sessionObj.getBatchIds());
        reqJsonObj.put("size", 200);
        reqJsonObj.put("start", 0);

        String enrollUrl = SUBSCRIPTION_ENDPOINT + "enroll/getActiveUserIdsByBatchIds";
        ClientResponse subResponse = WebUtils.INSTANCE.doCall(enrollUrl, HttpMethod.POST, reqJsonObj.toString());

        VExceptionFactory.INSTANCE.parseAndThrowException(subResponse);
        String jsonString = subResponse.getEntity(String.class);

        Type listType = new TypeToken<List<ActiveUserIdsByBatchIdsResp>>() {
        }.getType();
        List<ActiveUserIdsByBatchIdsResp> activeUserIdsResp = gson.fromJson(jsonString, listType);

        List<String> userIds = new ArrayList<>();
        Map<String,ActiveUserIdsByBatchIdsResp> idEnrollObjMap = new HashMap<>();
        for (ActiveUserIdsByBatchIdsResp enrollObj : activeUserIdsResp) {
            idEnrollObjMap.putIfAbsent(enrollObj.getUserId(),enrollObj);
            userIds.add(enrollObj.getUserId());
            // fetching previous batchIds in case of batch shuffling
            Set<String> previousBatchIds = batchIds;
            List<BatchChangeTime> batchChangeTimes = enrollObj.getBatchChangeTime();

            if (!ArrayUtils.isEmpty(batchChangeTimes)) {
                for (BatchChangeTime batchChangeTime : batchChangeTimes) {
                    previousBatchIds.add(batchChangeTime.getPreviousBatchId());
                }

                // appending to the initial batch ids set
                batchIds.addAll(previousBatchIds);
            }
        }

        // quering past session with all batchIds from OTFSession and previousBatchId from user enrollments
        List<String> oldSessionIds = getTeachersPastActiveSession(presenter, batchIds, 200, 0); // in desc order

        if (oldSessionIds == null) {
            logger.info("No past session found for sessionId" + sessionId);
            oldSessionIds = new ArrayList<String>();
        }

        // getting the last session if no last sessionId provided
        if (lastSessionId == null && !ArrayUtils.isEmpty(oldSessionIds)) {
            lastSessionId = oldSessionIds.get(0);
        }

        // gttAttendeeDetailsDAO.getNewAbsenteeStudentListAggregate(userIds, oldSessionIds, lastSessionId);
        AggregationResults<GTTAggregatedStudentList> result = gttAttendeeDetailsDAO.getNewAbsenteeStudentListAggregate(userIds, oldSessionIds, lastSessionId);
        List<GTTAggregatedStudentList> studentList = result.getMappedResults();

        for(GTTAggregatedStudentList _list : studentList) {
            if(_list.getId() != null && idEnrollObjMap.containsKey(_list.getId()) && idEnrollObjMap.get(_list.getId()) != null && idEnrollObjMap.get(_list.getId()).getSectionId() != null)
                _list.setSectionId(idEnrollObjMap.get(_list.getId()).getSectionId());
        }

        JSONObject finalResp = new JSONObject();
        boolean isWebinarSession = sessionObj.getEntityTags().contains(EntityTag.WEBINAR);
        finalResp.put("sessionId", sessionId);
        finalResp.put("isWebinarSession", isWebinarSession);
        finalResp.put("labels", sessionObj.getLabels());
        finalResp.put("entityTags", sessionObj.getEntityTags());
        finalResp.put("list", studentList);
        finalResp.put("type",sessionObj.getType());
        awsSQSManager.sendToSQS(SQSQueue.OTF_PRESESSION_STUDENTLIST_QUEUE, SQSMessageType.SEND_OTF_PRESESSION_STUDENTLIST, gson.toJson(finalResp));
        return studentList;
    }

    public List<String> getTeachersPastActiveSession(String presenter, Set<String> batchIds, Integer size, Integer start) {
        List<String> sessionIds = new ArrayList<>();
        if (size == null) {
            size = 200;
        }
        if (start == null) {
            start = 0;
        }
        List<OTFSession> sessions = otfSessionDAO.getPresentersActiveSessionsByBatchIds(presenter, batchIds, size, start);
        if (ArrayUtils.isEmpty(sessions)) {
            return null;
        }

        for (OTFSession session : sessions) {
            sessionIds.add(session.getId());
        }

        return sessionIds;
    }

    public void setTAsToSession(SetTAsToSessionRequest req) throws BadRequestException {
        String sessionId = req.getSessionId();
        Integer size = req.getSize();
        Set<Long> taIds = Optional.ofNullable(req.getTaIds()).orElseGet(HashSet::new).stream().filter(Objects::nonNull).collect(Collectors.toSet());
        OTFSession session = otfSessionDAO.getById(sessionId);

        // sentry npe fix
        if (Objects.isNull(session)) {
            logger.warn("session not found with Id {}", req.getSessionId());
            return;
        }

        Set<Long> oldTaIds = session.getTaIds();
        if (!ArrayUtils.isEmpty(oldTaIds)) {
            taIds.addAll(oldTaIds);
        }
        Update update = new Update();
        update.set(OTFSession.Constants.TA_IDS, taIds);
        otfSessionDAO.updateSession(update, sessionId);
    }

    private Boolean checkIsIntermittentScreenAllowed(String teacherEmail) throws VException, IOException {
        teacherEmail = teacherEmail.replaceAll("\\+.*@", "@");
        logger.info("setIntermittentScreenUrl entry..{}", teacherEmail);
        ClientResponse resp = WebUtils.INSTANCE.doCall(VEDANTUDATA_ENDPOINT + "leadsquared/data/isIntermittentScreenAllowed?emailId=" + teacherEmail, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> respMap = mapper.readValue(jsonString, Map.class);
        Boolean isAllowed = (Boolean) respMap.get("payload");
        logger.info("intermittent response : {} , value : {}", respMap, isAllowed);
        return isAllowed;
    }

    public Map<String, String> addSalesDemoSession(AddSalesDemoSessionReq req) throws VException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, IOException {

        // validation  checks and user info fetch using email and ph
        UserBasicInfo studentInfo = null;
        UserBasicInfo teacherInfo = null;

        // student
        if (!StringUtils.isEmpty(req.getStudentPhone())) {
            // fetch user by phone
            studentInfo = fosUtils.getUserBasicInfoByContactNumber(req.getStudentPhone());
        } else if (!StringUtils.isEmpty(req.getStudentEmail())) {
            studentInfo = fosUtils.getUserBasicInfoFromEmail(req.getStudentEmail(), true);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Both student phone and email are Empty");
        }

        // teacher
        if (!StringUtils.isEmpty(req.getPresenterPhone())) {
            // fetch user by phone
            logger.info("fetching teacher with phone-- req.getPresetnerPhone()");
            teacherInfo = fosUtils.getUserBasicInfoByContactNumber(req.getPresenterPhone());
        } else if (!StringUtils.isEmpty(req.getPresenterEmail())) {
            teacherInfo = fosUtils.getUserBasicInfoFromEmail(req.getPresenterEmail(), true);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Both presenter phone and email are Empty");
        }

        if (studentInfo == null || studentInfo.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No student has signed up with the Email id/ Phone number which you have entered. Please check the Email id / Phone number and enter a correct one");
        }

        if (teacherInfo == null || teacherInfo.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Teacher number or email ID is not-registered");
        }

        req.setStudentId(studentInfo.getUserId().toString());
        req.setPresenter(teacherInfo.getUserId().toString());

        OTFSession otfSession = OTFSession.createSalesDemoSession(req);
        otfSession.addLabels(SessionLabel.WEBINAR);
        // check slot conflicts for presenter and student
        // bypassed

        // save session
        otfSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
        otfSession.setAttendees(Collections.singletonList(req.getStudentId()));
        otfSession.setTitle("Online Counselling session");
//        Set<OTFSessionFlag> salesFlag =  EnumSet.of(OTFSessionFlag.SALES_DEMO);
//        otfSession.setFlags(salesFlag);
        otfSession.setOtmSessionType(OTMSessionType.SALES_DEMO);
        otfSession.setType(EntityType.ONE_TO_ONE);

        otfSessionDAO.save(otfSession);

        // Post session schedule tasks
        // 1. generate session link and create GTTAttendeeDetails
        // keeping a buffer of 15 minutes.
        Long startTimeBuffer = System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_MINUTE;
        if (otfSession.getStartTime() != null && otfSession.getStartTime() > startTimeBuffer) {
            if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", otfSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
                sendToQForGeneratingSessionLinkAndGTT(otfSession);
            }
        }

        // vedantuWaveScheduleManager.changeServiceProviderToImpartus(otfSession); // using impartus only for sales

        try {
            vedantuWaveSessionManager.sendSessionsToNodeForCaching(Collections.singletonList(otfSession));
            if (otfSession.getStartTime() < (System.currentTimeMillis() + 10 * DateTimeUtils.MILLIS_PER_MINUTE)) {
                vedantuWaveSessionManager.initSessionRecordingFromNode(otfSession);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        if (StringUtils.isEmpty(otfSession.getId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "generated session id not found");
        }
        String sesseionUrl = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT") + "session/" + otfSession.getId();
        logger.info("session url : " + sesseionUrl);
        Map<String, String> respMap = new HashMap<>();
        Boolean isIntermittentScreenAllowed = false;
        try {
            isIntermittentScreenAllowed =  checkIsIntermittentScreenAllowed(teacherInfo.getEmail());
        }catch (Exception e) {
            logger.error("Exception while checking isIntermittentScreenAllowed..." + e);
        }

        String presenterLoginUrl = getFosDeomSessionShortnUrlUsingLoginToken(sesseionUrl, teacherInfo.getUserId());
        String studentLoginUrl;
        if (isIntermittentScreenAllowed) {
            sesseionUrl = FOS_ENDPOINT + "/counselling-session-home/" + otfSession.getId();
            studentLoginUrl = getIntermittentScreenShortnUrlUsingLoginToken(sesseionUrl, studentInfo.getUserId());
        }else{
            studentLoginUrl = getFosDeomSessionShortnUrlUsingLoginToken(sesseionUrl, studentInfo.getUserId());
        }

        String date = DateTimeUtils.getDateInMMMMForamatWithTime(req.getStartTime());
        sendEmailAndSMSToStudentAfterFosDemoCreation(studentInfo, studentLoginUrl, date);
        sendEmailAndSMSToTeacherAfterFosDemoCreation(teacherInfo,studentInfo, presenterLoginUrl,studentLoginUrl, date);

        respMap.put("studentLoginUrl", studentLoginUrl);
        respMap.put("presenterLoginUrl", presenterLoginUrl);

        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
            payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
        }
        
		try {
			LeadSquaredRequest leadSquaredRequest = new LeadSquaredRequest();
			leadSquaredRequest.setAction(LeadSquaredAction.POST_LEAD_ACTIVITY);
			leadSquaredRequest.setDataType(LeadSquaredDataType.LEAD_ACTIVITY_ONLINE_COUNSELLING_SESSION_SCHEDULED);
			Map<String, String> payload = new HashMap<>();
            getleadSquaredOnlineSessionMap(studentInfo,teacherInfo,otfSession,payload);
			leadSquaredRequest.setParams(payload);
			awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.ONLINE_COUNSELLING_SESSION_SCHEDULED,
					gson.toJson(leadSquaredRequest));

		} catch (Exception e) {
			logger.error("Error in making call to sqs queue" ,e);

		}

        return respMap;
    }

    public void getleadSquaredOnlineSessionMap(UserBasicInfo studentInfo,UserBasicInfo teacherInfo,OTFSession otfSession,Map<String,String> payload) {

        payload.put("studentId", String.valueOf(studentInfo.getUserId()));
        payload.put("agentId", String.valueOf(teacherInfo.getUserId()));
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        if (otfSession.getStartTime() != null && otfSession.getEndTime()!=null) {
            String dateTime = df.format(new Date(otfSession.getStartTime()));
            String craetionTime = df.format(new Date(System.currentTimeMillis()));
            payload.put("sessionDuration", String.valueOf((otfSession.getEndTime() - otfSession.getStartTime())/ DateTimeUtils.MILLIS_PER_MINUTE));
            payload.put("sessionCreationTime", craetionTime);
            payload.put("sessionStartTime", dateTime);
        }
        payload.put("sessionId", otfSession.getId());
        payload.put("agentEmailId", teacherInfo.getEmail());


    }


    public String getFosDeomSessionShortnUrlUsingLoginToken(String sesseionUrl, Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId not found for generating login url");
        }
        CreateUserAuthenticationTokenReq createUserPresenterAuthenticationTokenReq = new CreateUserAuthenticationTokenReq(LoginTokenContext.FOS_DEMO_SESSION, null, 180L, userId);
        createUserPresenterAuthenticationTokenReq.setTestLink(sesseionUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/createUserAuthenticationToken", HttpMethod.POST,
                new Gson().toJson(createUserPresenterAuthenticationTokenReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String createTeacherAuthenticationTokenResStr = resp.getEntity(String.class);
        CreateUserAuthenticationTokenRes createTeacherAuthenticationTokenRes = gson.fromJson(createTeacherAuthenticationTokenResStr, CreateUserAuthenticationTokenRes.class);
        String redirectUrl = FOS_ENDPOINT + "/v/joinTestWithLink/" + createTeacherAuthenticationTokenRes.getLoginToken();
        return WebUtils.INSTANCE.shortenUrl(redirectUrl);
    }

    private String getIntermittentScreenShortnUrlUsingLoginToken(String sessionUrl, Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId not found for generating login url");
        }
        CreateUserAuthenticationTokenReq createUserPresenterAuthenticationTokenReq = new CreateUserAuthenticationTokenReq(LoginTokenContext.INTERMITTENT_SCREEN, null, 180L, userId);
        createUserPresenterAuthenticationTokenReq.setTestLink(sessionUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/createUserAuthenticationToken", HttpMethod.POST,
                new Gson().toJson(createUserPresenterAuthenticationTokenReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String createTeacherAuthenticationTokenResStr = resp.getEntity(String.class);
        CreateUserAuthenticationTokenRes createTeacherAuthenticationTokenRes = gson.fromJson(createTeacherAuthenticationTokenResStr, CreateUserAuthenticationTokenRes.class);
        String redirectUrl = FOS_ENDPOINT + "/v/joinTestWithLink/" + createTeacherAuthenticationTokenRes.getLoginToken();
        return WebUtils.INSTANCE.shortenUrl(redirectUrl);
    }

    private void sendEmailAndSMSToStudentAfterFosDemoCreation(UserBasicInfo studentInfo, String sessionLink, String date) throws VException {
        try {
            //sending email
            CommunicationType emailType = CommunicationType.FOS_DEMO_SESSION_CREATION;
            HashMap<String, Object> bodyScopes = new HashMap<>();
            bodyScopes.put("studentName", StringUtils.defaultIfEmpty(studentInfo.getFirstName()) + " " + StringUtils.defaultIfEmpty(studentInfo.getLastName()));
            bodyScopes.put("sessionLink", sessionLink);
            bodyScopes.put("dateAndTime", date);

            HashMap<String, Object> subjectScopes = new HashMap<>();
            ArrayList<InternetAddress> toAddress = new ArrayList<>();

            if (StringUtils.isNotEmpty(studentInfo.getEmail())) {
                toAddress.add(new InternetAddress(studentInfo.getEmail()));
                EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.STUDENT);
                request.setIncludeHeaderFooter(Boolean.FALSE);
                //sending sms
                communicationManager.sendEmail(request);
            }
            if (StringUtils.isNotEmpty(studentInfo.getContactNumber())) {
                TextSMSRequest textSMSRequest;
                textSMSRequest = new TextSMSRequest(studentInfo.getContactNumber(), studentInfo.getPhoneCode(),
                        bodyScopes, CommunicationType.FOS_DEMO_SESSION_CREATION, Role.STUDENT);
                awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(textSMSRequest));
            }
        } catch (AddressException e) {
            logger.error(e);
            throw new VException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

    }
    private void sendEmailAndSMSToTeacherAfterFosDemoCreation(UserBasicInfo teacherInfo,UserBasicInfo studentInfo, String teacherSessionLink, String studentSessionLink, String date) throws VException {
        try {
            //sending email
            CommunicationType emailType = CommunicationType.FOS_DEMO_SESSION_CREATION;
            HashMap<String, Object> bodyScopes = new HashMap<>();
            bodyScopes.put("teacherName", StringUtils.defaultIfEmpty(teacherInfo.getFirstName()) + " " + StringUtils.defaultIfEmpty(teacherInfo.getLastName()));
            bodyScopes.put("sessionLink", teacherSessionLink);
            bodyScopes.put("studentName", StringUtils.defaultIfEmpty(studentInfo.getFirstName()) + " " + StringUtils.defaultIfEmpty(studentInfo.getLastName()));
            bodyScopes.put("studentSessionLink",studentSessionLink);
            bodyScopes.put("studentContactNumber", StringUtils.defaultIfEmpty(studentInfo.getContactNumber()));
            bodyScopes.put("dateAndTime",date);
            HashMap<String, Object> subjectScopes = new HashMap<>();
            subjectScopes.put("studentName", StringUtils.defaultIfEmpty(studentInfo.getFirstName()) + " " + StringUtils.defaultIfEmpty(studentInfo.getLastName()));
            ArrayList<InternetAddress> toAddress = new ArrayList<>();

            if (StringUtils.isNotEmpty(teacherInfo.getEmail())) {
                toAddress.add(new InternetAddress(teacherInfo.getEmail()));
                EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.TEACHER);
                request.setIncludeHeaderFooter(Boolean.FALSE);

                communicationManager.sendEmail(request);
            }

        } catch (AddressException e) {
            logger.error(e);
            throw new VException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

    }

    public void sendEarlyLearningClassSms() {

        //Get the List of all the Sessions which going to start within One Hour
        List<OTFSession> earlyLearningSessions = new ArrayList<>();
        Long normalised_time = System.currentTimeMillis() - (System.currentTimeMillis() % DateTimeUtils.MILLIS_PER_MINUTE);
        Long oneHourStartTime = normalised_time + DateTimeUtils.MILLIS_PER_HOUR;
        Long oneHourEndTime = normalised_time + DateTimeUtils.MILLIS_PER_HOUR + DateTimeUtils.MILLIS_PER_MINUTE * 5;
        int start = 0;
        int size = 100;
        while (true) {
            List<OTFSession> earlyLearningSessionsBatch = otfSessionDAO.getEarlyLearningSessionsForSMS(oneHourStartTime, oneHourEndTime, start, size);
            if (ArrayUtils.isEmpty(earlyLearningSessionsBatch)) {
                break;
            } else {
                earlyLearningSessions.addAll(earlyLearningSessionsBatch);
            }
            start = start + size;
        }
        if (ArrayUtils.isNotEmpty(earlyLearningSessions)) {
            for (OTFSession earlyLearningSession : earlyLearningSessions) {
                List<String> studentIds = Optional.ofNullable(earlyLearningSession.getAttendees())
                        .orElseGet(ArrayList::new)
                        .stream()
                        .collect(Collectors.toList());
                logger.info("earlyLearningSession" + earlyLearningSession.getId());
                logger.info("earlyLearningSession studentIds" + studentIds);
                Map<String, UserBasicInfo> userBasicInfosMap = fosUtils.getUserBasicInfosMap(studentIds, true);

                for (String userId : studentIds) {
                    HashMap<String, Object> bodyScopes = new HashMap<>();
                    sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                    String timeStr = sdfTime.format(new Date(earlyLearningSession.getStartTime()));
                    bodyScopes.put("classStartTime", timeStr);
                    UserBasicInfo userBasicInfo = userBasicInfosMap.get(userId);
                    if (StringUtils.isNotEmpty(userBasicInfo.getContactNumber()) && Role.STUDENT.equals(userBasicInfo.getRole())) {
                        TextSMSRequest textSMSRequest;
                        textSMSRequest = new TextSMSRequest(userBasicInfo.getContactNumber(), userBasicInfo.getPhoneCode(),
                                bodyScopes, CommunicationType.SUPER_KIDS_CLASS_REMINDER, Role.STUDENT);
                        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(textSMSRequest));
                    }
                }
            }
        }
    }

    public List<String> getVSKDemoAttendee(String sessionId) throws BadRequestException {
        List<OTFSession> sessions = otfSessionDAO.getOTFSessionById(sessionId, Arrays.asList(ATTENDEES, SESSION_LABELS));
        logger.info("vsk sessions" + sessions);
        if (ArrayUtils.isEmpty(sessions) || !sessions.get(0).isEarlyLearningSession()) {
            throw new BadRequestException(ErrorCode.SESSION_NOT_FOUND, "Invalid VSK sessionId");
        }
        return sessions.get(0).getAttendees();
    }

    public Map<Long, List<AbsentSessionRes>> getPastWeekAbsentSessionsWithBoard(String userId) throws IllegalArgumentException {

        // validate-- params
        if (StringUtils.isEmpty(userId)) {
            throw new IllegalArgumentException("Invalid userId");
        }

        Map<Long, List<AbsentSessionRes>> absentSessionsInfo = new HashMap<>();
        Map<String, GTTAttendeeDetails> absentSessionIdGTTMap = gttAttendeeDetailsManager.getPastWeekAbsentAttendees(userId);
        if (CollectionUtils.isEmpty(absentSessionIdGTTMap)) {
            return absentSessionsInfo;
        }

        // fetch-- absent sessions
        List<String> includeFieldsOTFSession = Stream.of(PRESENTER, BOARD_ID, STATE, START_TIME, END_TIME, OTMSESSION_TYPE).collect(Collectors.toList());
        List<OTFSession> absentSessions = otfSessionDAO.getByIdsWithProjection(absentSessionIdGTTMap.keySet(),
                includeFieldsOTFSession, 0, absentSessionIdGTTMap.keySet().size());

        BiFunction<OTFSession, GTTAttendeeDetails, AbsentSessionRes> absentSessionResFactory = (session, gtt) -> AbsentSessionRes.builder()
                .boardId(session.getBoardId())
                .presenter(session.getPresenter())
                .gttId(gtt.getId())
                .sessionId(gtt.getSessionId())
                .startTime(session.getStartTime())
                .endTime(session.getEndTime())
                .enrolmentId(gtt.getEnrollmentId()).build();

        absentSessionsInfo = Optional.ofNullable(absentSessions).orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .filter(s -> Objects.nonNull(s.getBoardId())
                        && s.getState().equals(SessionState.SCHEDULED)
                        && s.getStartTime() < System.currentTimeMillis()
                        && !s.getOtmSessionType().equals(OTMSessionType.TEST))
                .map(s -> absentSessionResFactory.apply(s, absentSessionIdGTTMap.get(s.getId())))
                .collect(Collectors.groupingBy(AbsentSessionRes::getBoardId));

        return absentSessionsInfo;
    }


    public List<OTFSession> getSessionsByBatchIds(GetSessionsByBatchIdsReq getSessionsByBatchIdsReq) {
        List<String> customeBatchIds = new ArrayList<>();
        if(getSessionsByBatchIdsReq.getBatchIds().size()>20){
            customeBatchIds = getSessionsByBatchIdsReq.getBatchIds()
                    .stream()
                    .limit(20)
                    .collect(Collectors.toList());
        } else{
            customeBatchIds.addAll(getSessionsByBatchIdsReq.getBatchIds());
        }
        List<OTFSession> otfSessions = otfSessionDAO.getSessionForBatchId(getSessionsByBatchIdsReq,customeBatchIds);
        return otfSessions;
    }

    public List<OTFSessionPojo> getLatestUpcomingSessionsForBatch(String batchId) {
        if(batchId.isEmpty())
            return null;

        List<String> includeFields = Arrays.asList(OTFSession.Constants.ID,OTFSession.Constants.START_TIME, OTFSession.Constants.END_TIME, OTFSession.Constants.BATCH_IDS);
        // only top 3 sessions required
        List<OTFSession> sessions = otfSessionDAO.getUpcomingSessionsForBatches(Collections.singletonList(batchId),0,3,includeFields);
        logger.info("sessions.size {}",sessions.size());

        List<OTFSessionPojo> sessionPojos = new ArrayList<>();
        if(!sessions.isEmpty()){
            for(OTFSession s:sessions) {
                sessionPojos.add(mapper.map(s,OTFSessionPojo.class));
            }
        }
        logger.info("sessionPojos {}",sessionPojos);
        return sessionPojos;

    }

    public OTFSessionPojoUtils getCachedSessionType(String id) {
        if (StringUtils.isEmpty(id) || "null".equalsIgnoreCase(id)) {
            return new OTFSessionPojoUtils();
        }
        String key = "OTF_SESSION_TYPE_FOR_" + id;
        String json = null;
        try {
            json = redisDAO.get(key);
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.error(e.getErrorMessage(), e);
        }

        if (json == null) {
            List<OTFSession> session = otfSessionDAO.getOTFSessionById(id, Arrays.asList(OTMSESSION_TYPE, SESSION_CONTEXT_TYPE));
            if (ArrayUtils.isEmpty(session)) {
                return new OTFSessionPojoUtils();
            } else {
                OTFSession otfSession = session.get(0);
                OTFSessionPojoUtils pojo = new OTFSessionPojoUtils();
                pojo.setOtmSessionType(otfSession.getOtmSessionType());
//                pojo.setSessionContextType(otfSession.getSessionContextType());
                pojo.setLabels(otfSession.getLabels());
                try {
                    redisDAO.setex(key, gson.toJson(pojo), DateTimeUtils.SECONDS_PER_DAY * 5);
                } catch (InternalServerErrorException e) {
                    logger.error(e.getErrorMessage(), e);
                }

                return pojo;
            }
        } else {
            OTFSessionPojoUtils session = gson.fromJson(json, OTFSessionPojoUtils.class);
            return session;
        }
    }

    public List<OTFSessionPojoApp> getUserLiveSessionsForApp(OTFSessionReqApp request) throws VException {
    	logger.debug("getUserLiveSessionsForApp request ->"+request);
    	List<OTFSessionPojoApp> otfSessionPojoApps = new ArrayList<OTFSessionPojoApp>();
    	if (StringUtils.isEmpty(request.getCallingUserId())
    			|| (!Role.STUDENT.equals(request.getCallingUserRole()))) {
    		return otfSessionPojoApps;
    	}

    	List<OTFSession> oTFSessions = new ArrayList<OTFSession>();
    	oTFSessions = otfSessionDAO.getUserLiveOrUpcomingSessionsForApp(request);

    	 if (ArrayUtils.isNotEmpty(oTFSessions)) {
         	Set<String> presenterIds = new HashSet<String>();
         	Set<Long> boardIds = new HashSet<Long>();
         	presenterIds = oTFSessions.stream().map(OTFSession::getPresenter).collect(Collectors.toSet());
         	boardIds = oTFSessions.stream().map(OTFSession::getBoardId).collect(Collectors.toSet());
         	Map<String, TeacherBasicInfo> teachersBasicInfo = fosUtils.getTeacherBasicInfoForApp(presenterIds);
         	logger.debug(" getUserLiveSessionsForApp teachersBasicInfo -> "+teachersBasicInfo);
         	Map<Long, Board> boardMap = fosUtils.getBoardInfoMapForApp(boardIds.stream().collect(Collectors.toList()));
         	logger.debug(" getUserLiveSessionsForApp boardMap -> "+boardMap);
         	for (OTFSession otfSession : oTFSessions) {
         		OTFSessionPojoApp otfSessionPojoApp = new OTFSessionPojoApp();
         		otfSessionPojoApp = convertSessionToSessionAppPojo(otfSession);
         		if(null != otfSession.getId()) {
         			otfSessionPojoApp.setSessionUrl(JOIN_SESSION_URL+otfSession.getId());
         		}
         		if (StringUtils.isNotEmpty(otfSession.getPresenter()) && teachersBasicInfo.containsKey(otfSession.getPresenter())) {
         			otfSessionPojoApp.setTeacherBasicInfo(teachersBasicInfo.get(otfSession.getPresenter()));
         		}
         		if ( null != otfSession.getBoardId() && otfSession.getBoardId() > 0l) {
         			Board board = boardMap.get(otfSession.getBoardId());
         			if (null != board && StringUtils.isNotEmpty(board.getName())) {
         				otfSessionPojoApp.setSubject(board.getName());
         			}
         		}
         		otfSessionPojoApps.add(otfSessionPojoApp);

         	}
         }
    	return otfSessionPojoApps;

    }

	private OTFSessionPojoApp convertSessionToSessionAppPojo(OTFSession otfSession) {

		return OTFSessionPojoApp.builder()
				.sessionId(otfSession.getId())
				.title(otfSession.getTitle())
				.startTime(otfSession.getStartTime())
				.endTime(otfSession.getEndTime())
				.batchIds(new ArrayList<String>(otfSession.getBatchIds()))
				.build();
	}

	public List<OTFSessionPojoApp> getUserUpcomingSessionsForApp(OTFSessionReqApp request) throws VException {

		logger.debug("getUserUpcomingSessionsForApp request ->"+request);
		List<OTFSessionPojoApp> otfSessionPojoApps = new ArrayList<OTFSessionPojoApp>();
    	if (StringUtils.isEmpty(request.getCallingUserId())
    			|| (!Role.STUDENT.equals(request.getCallingUserRole()))) {
    		return otfSessionPojoApps;

    	}

        List<OTFSession> oTFSessions = new ArrayList<OTFSession>();
        oTFSessions = otfSessionDAO.getUserLiveOrUpcomingSessionsForApp(request);
        logger.debug("getUserUpcomingSessionsForApp oTFSessions ->"+oTFSessions);


        if (ArrayUtils.isNotEmpty(oTFSessions)) {
        	Set<String> presenterIds = new HashSet<String>();
        	Set<Long> boardIds = new HashSet<Long>();
        	presenterIds = oTFSessions.stream().map(OTFSession::getPresenter).collect(Collectors.toSet());
        	logger.debug("getUserUpcomingSessionsForApp presenterIds ->"+presenterIds);
        	boardIds = oTFSessions.stream().map(OTFSession::getBoardId).collect(Collectors.toSet());
        	logger.debug("getUserUpcomingSessionsForApp boardIds ->"+boardIds);
        	Map<String, TeacherBasicInfo> teachersBasicInfo = fosUtils.getTeacherBasicInfoForApp(presenterIds);
        	logger.info(" getUserUpcomingSessionsForApp teachersBasicInfo -> "+teachersBasicInfo);
        	Map<Long, Board> boardMap = fosUtils.getBoardInfoMapForApp(boardIds.stream().collect(Collectors.toList()));
        	logger.debug(" getUserUpcomingSessionsForApp boardMap -> "+boardMap);
        	for (OTFSession otfSession : oTFSessions) {
        		OTFSessionPojoApp otfSessionPojoApp = new OTFSessionPojoApp();
        		otfSessionPojoApp = convertSessionToSessionAppPojo(otfSession);

        		if (StringUtils.isNotEmpty(otfSession.getPresenter()) && teachersBasicInfo.containsKey(otfSession.getPresenter())) {
        			otfSessionPojoApp.setTeacherBasicInfo(teachersBasicInfo.get(otfSession.getPresenter()));
        		}
        		if ( null != otfSession.getBoardId() && otfSession.getBoardId() > 0l) {
        			Board board = boardMap.get(otfSession.getBoardId());
        			if (null != board && StringUtils.isNotEmpty(board.getName())) {
        				otfSessionPojoApp.setSubject(board.getName());
        			}
        		}
        		otfSessionPojoApps.add(otfSessionPojoApp);

        	}
        }
    	return otfSessionPojoApps;

	}

	public List<WeeklySessionDetails> getAllSessionsForAppSchedulePage(OTFSessionReq otfSessionReq) throws VException {
		if(null == otfSessionReq.getStartTime() && null != otfSessionReq.getYear() && null != otfSessionReq.getMonth()) {
			otfSessionReq.setStartTime(DateTimeUtils.getStartTimeOfMonth(otfSessionReq.getYear(), otfSessionReq.getMonth()));
			logger.debug("StartTime -> "+otfSessionReq.getStartTime());
		}

		if(null == otfSessionReq.getEndTime() && null != otfSessionReq.getYear() && null != otfSessionReq.getMonth()) {
			otfSessionReq.setEndTime(DateTimeUtils.getEndTimeOfMonth(otfSessionReq.getYear(), otfSessionReq.getMonth()));
			logger.debug("EndTime -> "+otfSessionReq.getEndTime());
		}

		List<WeeklySessionDetails> weeklySessionDetails = new ArrayList<WeeklySessionDetails>();
		List<OTFSession> otfSessions = new ArrayList<OTFSession>();
		List<OTFSessionPojoForApp> otfSessionPojos = new ArrayList<OTFSessionPojoForApp>();

		if (StringUtils.isEmpty(otfSessionReq.getUserId())
				|| (!Role.STUDENT.equals(otfSessionReq.getUserRole()))) {
			return weeklySessionDetails;
		}

		otfSessions = otfSessionDAO.getAllSessionsForAppSchedulePage(otfSessionReq);

		Long currentTime = System.currentTimeMillis();


		Map<String, TeacherBasicInfo> teacherBasicInfoMap = new HashMap<String, TeacherBasicInfo>();
		Set<String> teacherIds = new HashSet<String>();
		Map<Long, Board> boardInfoMap = new HashMap<Long, Board>();
		List<Long> boardIds = new ArrayList<Long>();

		if(ArrayUtils.isNotEmpty(otfSessions)) {
			teacherIds = otfSessions.stream().map(OTFSession::getPresenter).collect(Collectors.toSet());
			boardIds = otfSessions.stream().map(OTFSession::getBoardId).collect(Collectors.toList());

		}

		if(ArrayUtils.isNotEmpty(teacherIds)) {
			teacherBasicInfoMap = fosUtils.getTeacherBasicInfoForApp(teacherIds);
		}

		if(ArrayUtils.isNotEmpty(boardIds)) {
			boardInfoMap = fosUtils.getBoardInfoMap(boardIds);
		}

		for(OTFSession s : otfSessions) {
			otfSessionPojos.add(convertSessionsToSessionPojo(s, currentTime, teacherBasicInfoMap, boardInfoMap));
		}
		if(ArrayUtils.isNotEmpty(otfSessionPojos) && null != otfSessionReq.getYear() && null != otfSessionReq.getMonth()) {
			weeklySessionDetails = segregateSessionsIntoWeeks(otfSessionPojos, otfSessionReq.getYear(), otfSessionReq.getMonth());
		}
		logger.debug("weeklySessionDetails -> "+weeklySessionDetails);
		return weeklySessionDetails;
	}

	private OTFSessionPojoForApp convertSessionsToSessionPojo(OTFSession s, Long currentTime, Map<String, TeacherBasicInfo> teacherBasicInfoMap, Map<Long, Board> boardInfoMap) {

		OTFSessionPojoForApp otfSessionPojo = new OTFSessionPojoForApp();
		otfSessionPojo.setTitle(s.getTitle());
		otfSessionPojo.setStartTime(s.getStartTime());
		otfSessionPojo.setEndTime(s.getEndTime());
		if(null != boardInfoMap && !boardInfoMap.isEmpty() && null != boardInfoMap.get(s.getBoardId()) && StringUtils.isNotEmpty(boardInfoMap.get(s.getBoardId()).getName())) {
			otfSessionPojo.setSubject(boardInfoMap.get(s.getBoardId()).getName());
		}

		if(null != teacherBasicInfoMap && !teacherBasicInfoMap.isEmpty() && null != teacherBasicInfoMap.get(s.getPresenter())
				&&	StringUtils.isNotEmpty(teacherBasicInfoMap.get(s.getPresenter()).getFullName())){
			otfSessionPojo.setTeacherName(teacherBasicInfoMap.get(s.getPresenter()).getFullName());
		}


		if(s.getEndTime()<currentTime && (StringUtils.isNotEmpty(s.getVimeoId()) || StringUtils.isNotEmpty(s.getAgoraReplayUrl()) ||
				(ArrayUtils.isNotEmpty(s.getFlags()) && s.getFlags().contains(OTFSessionFlag.HLS_RENDITIONS_CREATED)))) {
			//Past Session having a replay valid replaySession
			otfSessionPojo.setReplaySessionUrl(REPLAY_SESSION_URL + s.getId());

		}else {

			if(s.getStartTime()<=currentTime && s.getEndTime() > currentTime) {
				//Live Session
				otfSessionPojo.setJoinSessionUrl(JOIN_SESSION_URL+s.getId());
			}else {
				//Upcoming Session no need to set url

			}
		}

		return otfSessionPojo;
	}

	private List<WeeklySessionDetails> segregateSessionsIntoWeeks(List<OTFSessionPojoForApp> otfSessionPojo, Integer year, Integer month) {
		List<WeeklySessionDetails> weeklySessionDetails = new ArrayList<WeeklySessionDetails>();
		List<List<Long>> listOfWeekStartEndTimestamp = DateTimeUtils.getNumberOfWeeks(year, month);
		SimpleDateFormat df = new SimpleDateFormat("MMM-dd");
		df.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Kolkata")));
		logger.debug("listOfWeekStartEndTimestamp -> "+listOfWeekStartEndTimestamp);
		int noOfWeeks = listOfWeekStartEndTimestamp.size();
		logger.debug("noOfWeeks -> "+noOfWeeks+"    otfSessionPojo->"+otfSessionPojo);
		if(ArrayUtils.isNotEmpty(listOfWeekStartEndTimestamp)) {
			for(int i = 0; i<=noOfWeeks-1; i++) {
				List<OTFSessionPojoForApp> week = new ArrayList<OTFSessionPojoForApp>();
				WeeklySessionDetails weeklySessionDetail = new WeeklySessionDetails();
				for(OTFSessionPojoForApp s: otfSessionPojo) {
					if(null != s.getStartTime() && null != listOfWeekStartEndTimestamp.get(i) && null != listOfWeekStartEndTimestamp.get(i).get(0)
							&& null != s.getEndTime() && null != listOfWeekStartEndTimestamp.get(i) && null != listOfWeekStartEndTimestamp.get(i).get(1))
					if(s.getStartTime()>listOfWeekStartEndTimestamp.get(i).get(0) && s.getEndTime()<listOfWeekStartEndTimestamp.get(i).get(1)) {
						week.add(s);
						logger.debug("week -> "+week+ "  s.getStartTime() -> "+s.getStartTime()+"  s.getEndTime()-->"+s.getEndTime()+" "
								+ "  startEndTime -> "+listOfWeekStartEndTimestamp.get(i));
					}
				}
				if(i==0) {

					Long startTitle = listOfWeekStartEndTimestamp.get(i).get(0);
					logger.debug("startTitle -> "+startTitle);
					Long endTitle = listOfWeekStartEndTimestamp.get(i).get(1);
					logger.debug("endTitle -> "+endTitle);
					Long actualEndTitle = endTitle - 1L;
					logger.debug("actualEndTitle -> "+actualEndTitle);
					Long actualStartTitle = DateTimeUtils.getStartTimeOfMonth(year, month);
					logger.debug("actualStartTitle -> "+actualStartTitle);
					List<Long> newSectionTitle = new ArrayList<Long>();
					String startDateStr = df.format(actualStartTitle);
					logger.debug("startDateStr ->"+startDateStr);
					String endDateStr = df.format(actualEndTitle);
					logger.debug("endDateStr ->"+endDateStr);
					newSectionTitle.add(actualStartTitle);
					newSectionTitle.add(actualEndTitle);
					listOfWeekStartEndTimestamp.set(i, newSectionTitle);
					weeklySessionDetail.setStartTime(actualStartTitle);
					weeklySessionDetail.setEndTime(actualEndTitle);
					logger.debug("listOfWeekStartEndTimestamp -> "+" i : "+i+ " "+listOfWeekStartEndTimestamp.get(i));
					weeklySessionDetail.setSectionTitle(startDateStr.replace("-", " ") + " - " + endDateStr.replace("-", " "));

				}else if(i == noOfWeeks-1) {
					Long startTitle = listOfWeekStartEndTimestamp.get(i).get(0);
					logger.debug("startTitle -> "+startTitle);
					Long endTitle = listOfWeekStartEndTimestamp.get(i).get(1);
					Long actualEndTitle = DateTimeUtils.getEndTimeOfMonth(year, month);
					logger.debug("actualEndTitle -> "+actualEndTitle);
					actualEndTitle = actualEndTitle - 1L;
					logger.debug("actualEndTitle -> "+actualEndTitle);
					List<Long> newSectionTitle = new ArrayList<Long>();
					String startDateStr = df.format(startTitle);
					logger.debug("startDateStr ->"+startDateStr);
					String endDateStr = df.format(actualEndTitle);
					logger.debug("endDateStr ->"+endDateStr);
					newSectionTitle.add(startTitle);
					newSectionTitle.add(actualEndTitle);
					listOfWeekStartEndTimestamp.set(i, newSectionTitle);
					logger.debug("listOfWeekStartEndTimestamp -> "+listOfWeekStartEndTimestamp);
					weeklySessionDetail.setStartTime(startTitle);
					weeklySessionDetail.setEndTime(actualEndTitle);
					weeklySessionDetail.setSectionTitle(startDateStr.replace("-", " ") + " - " + endDateStr.replace("-", " "));

				}else {
					Long startTitle = listOfWeekStartEndTimestamp.get(i).get(0);
					Long endTitle = listOfWeekStartEndTimestamp.get(i).get(1);
					Long actualEndTitle = endTitle - 1L;
					List<Long> newSectionTitle = new ArrayList<Long>();
					String startDateStr = df.format(startTitle);
					logger.debug("startDateStr ->"+startDateStr);
					String endDateStr = df.format(actualEndTitle);
					logger.debug("endDateStr ->"+endDateStr);
					newSectionTitle.add(startTitle);
					newSectionTitle.add(actualEndTitle);
					listOfWeekStartEndTimestamp.set(i, newSectionTitle);
					logger.debug("listOfWeekStartEndTimestamp -> "+listOfWeekStartEndTimestamp);
					weeklySessionDetail.setStartTime(startTitle);
					weeklySessionDetail.setEndTime(actualEndTitle);
					weeklySessionDetail.setSectionTitle(startDateStr.replace("-", " ") + " - " + endDateStr.replace("-", " "));
				}

				weeklySessionDetail.setSessions(week);
				logger.debug("weeklySessionDetail -> "+weeklySessionDetail);
				weeklySessionDetails.add(weeklySessionDetail);
			}
		}

		return weeklySessionDetails;
	}

    public PlatformBasicResponse handleEntityChangeForSession(UpdateEntityTypeForSessionReq req) throws VException {
        req.verify();

        String sessionId = req.getSessionId();
        OTFSession otfSession = otfSessionDAO.getById(sessionId);
        if (Objects.isNull(otfSession)) {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "no such session exists");
        }

        setEntityTypeForSession(otfSession,otfSession.getBatchIds(),req.getType());
        otfSessionDAO.save(otfSession);
        return new PlatformBasicResponse(true,"success","");
    }

    public void registerAttendeeWithSession(SessionEnrollmentsPojo payload) throws NotFoundException, InternalServerErrorException {
        OTFSession otfSession = payload.toOtfSession();
        Map<String, List<EnrollmentPojo>> pojosMap = payload.toEnrollmentPojosMap();
        for (Entry<String, List<EnrollmentPojo>> entry : pojosMap.entrySet()) {
            try {
                vedantuWaveScheduleManager.registerAttendee(otfSession, entry.getKey(), entry.getValue());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    public CreateTownhallSessionRes createTownhallSession(CreateTownhallSessionReq req) throws ConflictException {
        OTFSession otfSession = new OTFSession();
        otfSession.setPresenter(String.valueOf(req.getPresenterId()));
        Set<Long> taIdSet = new HashSet<Long>(req.getTaIds());
        otfSession.setTaIds(taIdSet);
        otfSession.setStartTime(req.getStartTime());
        otfSession.setEndTime(req.getEndTime());
        //otfSession.setSessionContextType(OTFSessionContextType.WEBINAR);
        Set<SessionLabel> labelSet = new HashSet<>();
        labelSet.add(SessionLabel.WEBINAR);
        otfSession.setLabels(labelSet);

        Set<EntityTag> entityTagSet = new HashSet<>();
        entityTagSet.add(EntityTag.WEBINAR);
        otfSession.setEntityTags(entityTagSet);

        otfSession.setType(EntityType.INTERNAL);
        otfSession.setOtmSessionType(OTMSessionType.TOWNHALL);

        otfSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);

        Long startTimeBuffer = System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_MINUTE;
        if (otfSession.getStartTime() != null && otfSession.getStartTime() > startTimeBuffer) {
            if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                 /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", otfSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
                sendToQForGeneratingSessionLinkAndGTT(otfSession);
            }
        }
        // vedantuWaveScheduleManager.changeServiceProviderToImpartus(otfSession); // using impartus only for sales

        try {
            vedantuWaveSessionManager.sendSessionsToNodeForCaching(Collections.singletonList(otfSession));
            if (otfSession.getStartTime() < (System.currentTimeMillis() + 10 * DateTimeUtils.MILLIS_PER_MINUTE)) {
                vedantuWaveSessionManager.initSessionRecordingFromNode(otfSession);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        otfSessionDAO.save(otfSession);

        String sesseionUrl = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT") + "session/" + otfSession.getId();
        logger.info("session url : " + sesseionUrl);

        CreateTownhallSessionRes res = new CreateTownhallSessionRes();
        res.setSessionLink(sesseionUrl);

        return res;

    }
}
