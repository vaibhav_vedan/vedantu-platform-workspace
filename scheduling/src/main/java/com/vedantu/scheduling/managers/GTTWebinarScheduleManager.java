package com.vedantu.scheduling.managers;

import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.dao.serializers.GTTOrganizerTokenDAO;
import com.vedantu.util.LogFactory;

@Service("gttWebinarScheduleManager")
public class GTTWebinarScheduleManager extends GTTScheduleManager{
	@Autowired
	private LogFactory logFactory;
        
        @Autowired
	private GTTOrganizerTokenDAO gttOrganizerTokenDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTTWebinarScheduleManager.class);

	private String[] authAccessArray = { "gttWebinarSeat1","gttWebinarSeat2","gttWebinarSeat3","gttWebinarSeat4"
                ,"gttWebinarSeat5"
        };
//, 
	protected static Map<String, String> overwriteAccessKeys = Collections.unmodifiableMap(new HashMap<String, String>() {

		private static final long serialVersionUID = 4675157667161370845L;
		{
			put("gttWebinarSeat1", "wGZzZ4yECuHw0YVuj1FCd2M0Bfy6"); // gttWebinarSeat1 - gtt220@vedantu.com
                        put("gttWebinarSeat2", "G3bYJVNPRcaNGGHIiSqZJN2MJv7c"); // gttnew232@vedantu.com
                        put("gttWebinarSeat3", "fvj1fhZQth1G4tQjknyFWu0e61Wb"); // gttnew233@vedantu.com
                        put("gttWebinarSeat4", "1TSXJW9XbEIuW8az4Z3AGkG2wXnP"); // gttnew234@vedantu.com
                        put("gttWebinarSeat5", "9oNPmDM6sv75D3z0AtBtw10z2fuD"); // gttnew235@vedantu.com
                        
		}
	});

	protected static Map<String, String> organizerKeys = Collections.unmodifiableMap(new HashMap<String, String>() {
		private static final long serialVersionUID = 2357808695344388664L;

		{
			put("gttWebinarSeat1", "3784316003353336325"); // gttWebinarSeat1
                        put("gttWebinarSeat2", "729603323124414220"); // gttWebinarSeat2
                        put("gttWebinarSeat3", "3383336209412945676"); // gttWebinarSeat3
                        put("gttWebinarSeat4", "4642175868146710284"); // gttWebinarSeat4
                        put("gttWebinarSeat5", "4066429798401341196"); // gttWebinarSeat5
                        
		}
	});

	private List<String> authAccessList = Arrays.asList(authAccessArray);

	@Override
	public int getAccessArrayLength() {
		return authAccessArray.length;
	}

	@Override
	public String getAccessToken(int slot) {
		if (slot != -1 && slot < authAccessList.size()) {
			return authAccessList.get(slot);
		}
		return null;
	}

	@Override
	public int getAccessTokenSlot(String accessToken) {
		int returnVal = -1;
		if (!StringUtils.isEmpty(accessToken)) {
			returnVal = authAccessList.indexOf(accessToken);
		}
		return returnVal;
	}

	@Override
	public String getPostOverwriteAccessToken(String accessToken) {
            GTTOrganizerToken token = gttOrganizerTokenDAO.getTokenForSeat(accessToken);
            if (token != null && !StringUtils.isEmpty(token.getOrganizerAccessToken()) && token.getExpiresIn()>System.currentTimeMillis()) {
                return token.getOrganizerAccessToken();
            } else {
                logger.error("Just as reference to look into : "+ token);
                if (overwriteAccessKeys.containsKey(accessToken)) {
                    
                    return overwriteAccessKeys.get(accessToken);
		}
                return accessToken;
            }
    }

	@Override
	public BitSet getScheduleBitSet(Schedule schedule) {
		return schedule.getGttWebinarScheduleBitset();
	}

	@Override
	public String getOrganizerKeyForToken(String accessToken) {
		return organizerKeys.get(accessToken);
	}

}
