package com.vedantu.scheduling.managers;

import com.google.gson.Gson;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.onetofew.request.GetOTFSessionsListReq;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.dao.serializers.GTTAttendeeDetailsDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.OverBookingDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.enums.DeleteContext;
import com.vedantu.scheduling.enums.ELFailureReasonType;
import com.vedantu.scheduling.enums.ELSessionStatus;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.pojo.ELDemoSessionPojo;
import com.vedantu.scheduling.pojo.OverBookingPojo;
import com.vedantu.scheduling.pojo.SlotBookingCount;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.request.*;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.*;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class EarlyLearningSessionManager {

    @Autowired
    private CalendarEntryManager calendarEntryManager;

    @Autowired
    private OTFSessionManager otfSessionManager;

    @Autowired
    private OTFSessionDAO otfSessionDAO;

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private VedantuWaveSessionManager vedantuWaveSessionManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private OverBookingDAO overBookingDAO;

    @Autowired
    private RedisDAO redisDAO;

    private static final Gson GSON = new Gson();

    private final Logger logger = LogFactory.getLogger(EarlyLearningSessionManager.class);


    @Deprecated
    // TODO store session info in redis for this user
    public List<OTFSession> hasEarlyLearningSessions(String studentId, SessionLabel earlyLearningCourse) throws ForbiddenException {
        boolean isSuperCoder = SessionLabel.SUPER_CODER.equals(earlyLearningCourse);
        List<OTFSession> scheduledSessions = otfSessionDAO.getELSessionsForUser(studentId, isSuperCoder);
        logger.info("scheduledSessions : " + scheduledSessions);

        if (!CollectionUtils.isEmpty(scheduledSessions)) {
            scheduledSessions.sort(Comparator.comparing(OTFSession::getStartTime));
            OTFSession lastSession = scheduledSessions.get(scheduledSessions.size() - 1);
            if (ArrayUtils.isNotEmpty(lastSession.getLabels()) &&
                    (lastSession.getLabels().contains(earlyLearningCourse)) && lastSession.getStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
                return Collections.singletonList(lastSession);
            }
        }

        Set<String> pastSessionIds = Optional.ofNullable(scheduledSessions)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .filter(s -> (s.getLabels().contains(earlyLearningCourse)))
                .filter(s -> (s.getStartTime() < System.currentTimeMillis()
                        && s.getStartTime() > System.currentTimeMillis() - 30L * DateTimeUtils.MILLIS_PER_DAY))
                .map(OTFSession::getId)
                .collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(pastSessionIds)) {
            List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager.getJoinedSessionsAttendeeDetailForUser(pastSessionIds, studentId);
            if (!CollectionUtils.isEmpty(joinedGtt)) {
                throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION, "User has successfully attended an EL session");
            }
        }

        // user attended no EL sessions in the past
        return new ArrayList<>();
    }


    @Deprecated
    public OTFSession addEarlyLearningSession(AddEarlyLearningSessionReq req)
            throws VException {
        OTFSession otfSession = OTFSession.createEarlyLearningSession(req);
        SessionSlot sessionSlot = new SessionSlot(req.getStartTime(), req.getEndTime());

        boolean set = redisDAO.setnx(req.getPresenter() + "_" + req.getStartTime(), Long.toString(System.currentTimeMillis()), 10);
        if (!set) {
            throw new ConflictException(ErrorCode.SLOTS_NOT_AVAILABLE,
                    "Looks like this slot is already booked by the time you clicked. Please try with some other slot");
        }
        boolean isSuperCoder = true;

        if (Objects.nonNull(req.getEarlyLearningCourse())) {
            isSuperCoder = SessionLabel.SUPER_CODER.equals(req.getEarlyLearningCourse());
        }
        logger.info("isSuperCoder" + isSuperCoder);

        List<OTFSession> scheduledSessions = otfSessionDAO.getUpcomingELSessionsForUser(req.getStudentId(), req.getEarlyLearningCourse());
        logger.info("scheduled sessions for userid " + scheduledSessions.toString());
        if (!CollectionUtils.isEmpty(scheduledSessions)) {
            OTFSession lastSession = scheduledSessions.get(scheduledSessions.size() - 1);
            if (ArrayUtils.isNotEmpty(lastSession.getLabels()) && (lastSession.getLabels().contains(req.getEarlyLearningCourse()))) {
                throw new NotFoundException(ErrorCode.FORBIDDEN_ERROR, "User has already booked an EL session");
            }
        }

        // check slot conflicts for presenter and student
        otfSessionManager.checkSlotAvailabilityForUser(sessionSlot, req.getPresenter());

        logger.info("slot available from {} to {} for both teacher and student", req.getStartTime(), req.getEndTime());

        // save session
        otfSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
        otfSession.setAttendees(Collections.singletonList(req.getStudentId()));
        otfSession.setTitle("Early learning session");

//        Set<OTFSessionFlag> earlyLearningFlag = new HashSet<>();
//        earlyLearningFlag.add(OTFSessionFlag.EARLY_LEARNING);
//        otfSession.setFlags(earlyLearningFlag);
        Set<SessionLabel> labels = new HashSet<>();
        if (Objects.nonNull(req.getEarlyLearningCourse())) {
            labels.add(req.getEarlyLearningCourse());
        }
        otfSession.setLabels(labels);

        // vedantuWaveScheduleManager.changeServiceProviderToImpartus(otfSession); // using agora as default
        otfSessionDAO.save(otfSession);

        // Post session schedule tasks
        // 1. generate session link and create GTTAttendeeDetails
        if (otfSession.getStartTime() != null) {
            if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", otfSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
                otfSessionManager.sendToQForGeneratingSessionLinkAndGTT(otfSession);
            }
        }

        // 2. blocking teacher calendar for session duration
        CalendarEntryReq calendarEntryReq = new CalendarEntryReq(req.getPresenter(), req.getStartTime(),
                req.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION, otfSession.getId());
        calendarEntryManager.upsertCalendarEntry(calendarEntryReq);

        // 3. send email to presenter and student
        try {
            logger.info("sending email to student and parent");
            sqsManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_SESSION_MAIL, GSON.toJson(otfSession));
        } catch (Exception ex) {
            logger.error("Error while sending Early Learning Demo session mail: " + otfSession + " error:" + ex.getMessage());
        }

        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
            payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
        }

        try {
            Map<String, String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, req.getStudentId());
            userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, otfSessionManager.getFormattedDateForLeadSquared(otfSession.getStartTime(), true));
            userLead.put(LeadSquaredRequest.Constants.DEMO_TEACHER, req.getPresenter());
            userLead.put(LeadSquaredRequest.Constants.SESSION_ID, otfSession.getId());
            userLead.put(LeadSquaredRequest.Constants.CALLING_USER_ID, String.valueOf(req.getCallingUserId()));
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "DEMO BOOKED");

            logger.info("BOOK_DEMO_SESSION_SUPERKIDS userLeads : " + userLead);
            LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_BOOK, null, userLead, null);
            String messageGroupId = req.getStudentId() + "_DEMO";
            sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOK_DEMO_SESSION_SUPERKIDS, GSON.toJson(request), messageGroupId);
        } catch (Exception ex) {
            logger.error("error sending data in Leadsquare" + otfSession + " error:" + ex.getMessage());
        }
        vedantuWaveSessionManager.sendSessionsToNodeForCaching(Collections.singletonList(otfSession));
        if (otfSession.getStartTime() < (System.currentTimeMillis() + 10 * DateTimeUtils.MILLIS_PER_MINUTE)) {
            vedantuWaveSessionManager.initSessionRecordingFromNode(otfSession);
        }
        logger.info("saved addEarlyLearningSession session: " + otfSession.toString());

        return otfSession;
    }

    public PlatformBasicResponse cancelEarlyLearningSession(String sessionId, String cancellationRemark) throws VException {

        OTFSession scheduledSession = otfSessionDAO.getById(sessionId);
        if (scheduledSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Session not found : " + sessionId);
        }
        if (SessionState.CANCELLED.equals(scheduledSession.getState())
                || SessionState.FORFEITED.equals(scheduledSession.getState())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session already cancelled");
        }


        if (System.currentTimeMillis() > (scheduledSession.getStartTime() - DateTimeUtils.MILLIS_PER_MINUTE * 10L)) {
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "Cancellation allowed only until 10 minutes before session start time");
        }

        scheduledSession.setState(SessionState.CANCELLED);
        scheduledSession.setCancelledBy(httpSessionUtils.getCallingUserId().toString());
        scheduledSession.setRemarks(cancellationRemark);
        otfSessionDAO.save(scheduledSession);
        logger.info("session cancelled for " + scheduledSession.getId());

        try {
            Map<String, String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, otfSessionManager.getFormattedDateForLeadSquared(scheduledSession.getStartTime(), true));
            userLead.put(LeadSquaredRequest.Constants.SESSION_ID, scheduledSession.getId());
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Signed Cancelled");
            for (String studentId : scheduledSession.getAttendees()) {
                userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, studentId);
                logger.info("Superkids Cancel Session : " + userLead);
                LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_CANCEL, null, userLead, null);
                String messageGorupId = scheduledSession.getId() + "_CANCEL";
                sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.CANCEL_SESSION_SUPERKIDS, GSON.toJson(request), messageGorupId);
            }
        } catch (Exception ex) {
            logger.error("error sending data in Leadsquare" + scheduledSession.getId() + " error:" + ex.getMessage());
        }

        // unblock teacher calendar entries for session duration
        CalendarEntryReq calendarEntryReq = new CalendarEntryReq(scheduledSession.getPresenter(), scheduledSession.getStartTime(),
                scheduledSession.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION, scheduledSession.getId());
        calendarEntryManager.removeCalendarEntrySlotsUsingUserId(calendarEntryReq);
        logger.info("calendar entry removed");

        gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedBulk(scheduledSession.getId(), DeleteContext.SESSION_STATE_CHANGE);
        vedantuWaveSessionManager.sendSessionsToNodeForCaching(Collections.singletonList(scheduledSession));
        vedantuWaveSessionManager.sendGttAttendeeDetailsToNodeForCaching(Collections.singletonList(scheduledSession));


        return new PlatformBasicResponse();

    }

    public Long getEarlyLearningDemoSessionStartTime(Long studentId, SessionState state) {

        List<OTFSession> otfSessions = otfSessionDAO.getELSessionForLeadSquared(studentId, state);
        Long startTime = 0L;

        if (ArrayUtils.isNotEmpty(otfSessions)) {
            startTime = otfSessions.get(0).getStartTime();
        }

        logger.info("startTime : " + startTime);
        return startTime;
    }


    public List<OTFSessionPojo> getEarlyLearningSessions(GetOTFSessionsListReq req) {

        List<OTFSessionPojo> response = new ArrayList<>();


        List<OTFSession> otfSessions = new ArrayList<>(otfSessionDAO.getELSessions(req));


        Set<String> sessionIds = Optional.of(otfSessions)
                .orElseGet(ArrayList::new)
                .stream()
                .map(OTFSession::getId)
                .collect(Collectors.toSet());

        // Fetch GTT attendees
        List<GTTAttendeeDetails> gttAttendeeDetailsList = gttAttendeeDetailsManager
                .getAttendeeDetailsForSessions(sessionIds);
        Map<String, GTTAttendeeDetails> attendeeMap = OTFSessionManager.createAttendeeInfoMap(gttAttendeeDetailsList);
        // Create session infos
        for (OTFSession otfSession : otfSessions) {
            if (otfSession == null) {
                continue;
            }
            Set<String> allStudentSet = new HashSet<>();


            if (!CollectionUtils.isEmpty(otfSession.getAttendees())) {
                allStudentSet.addAll(otfSession.getAttendees());
            }

            if (!CollectionUtils.isEmpty(allStudentSet)) {
                otfSession.setAttendees(new ArrayList<>(allStudentSet));
            } else {
                otfSession.setAttendees(new ArrayList<>());
            }

            OTFSessionPojo oTFSessionPojo = mapper.map(otfSession, OTFSessionPojo.class);

            // Fill the GTT attendees
            logger.info(oTFSessionPojo);
            if (!CollectionUtils.isEmpty(allStudentSet)) {
                List<OTFSessionAttendeeInfo> attendeeInfos = new ArrayList<>();
                for (String student : allStudentSet) {
                    OTFSessionAttendeeInfo sessionAttendeeInfo = new OTFSessionAttendeeInfo(student);
                    if (!StringUtils.isEmpty(otfSession.getMeetingId())
                            && attendeeMap.containsKey(OTFSessionManager.getAttendeeInfoMapKey(otfSession.getMeetingId(), student))) {
                        GTTAttendeeDetails attendeeDetails = attendeeMap
                                .get(OTFSessionManager.getAttendeeInfoMapKey(otfSession.getMeetingId(), student));
                        if (attendeeDetails != null) {
                            sessionAttendeeInfo.setJoinTimes(attendeeDetails.getJoinTimes());
                            sessionAttendeeInfo.setTimeInSession(attendeeDetails.getTimeInSession());
                            sessionAttendeeInfo.setActiveIntervals(attendeeDetails.getActiveIntervals());
                        }
                    }

                    attendeeInfos.add(sessionAttendeeInfo);
                }
                oTFSessionPojo.setAttendeeInfos(attendeeInfos);
            }

            response.add(oTFSessionPojo);
        }

        return response;
    }

    public PlatformBasicResponse postEarlyLearningFeedbackForm(ELFeedbackFormReq req) throws NotFoundException {

        String sessionId = req.getSessionId();
        OTFSession otfSession = otfSessionDAO.getById(sessionId);

        if (Objects.isNull(otfSession)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Invalid SessionId : " + sessionId);
        }
        String failureReason = null;
        if (Objects.nonNull(req.getFailureReason())) {
            failureReason = req.getFailureReason().stream().map(ELFailureReasonType::getELFailureType).collect(Collectors.joining(", "));
            if (StringUtils.isNotEmpty(req.getOtherComment())) {
                failureReason = failureReason + " Reason Description :  " + req.getOtherComment();
                failureReason = failureReason.substring(0, Math.min(failureReason.length(), 200));
            }
        }
        SessionLabel courseType = otfSession.getLabels().contains(SessionLabel.SUPER_CODER) ?
                SessionLabel.SUPER_CODER : SessionLabel.SUPER_READER;


        if (otfSession.isEarlyLearningSession() && otfSession.isWebinarSession()) {
            try {
                Map<String, String> userLead = new HashMap<>();
                userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, otfSessionManager.getFormattedDateForLeadSquared(otfSession.getStartTime(), true));
                userLead.put(LeadSquaredRequest.Constants.SESSION_ID, otfSession.getId());
                userLead.put(LeadSquaredRequest.Constants.DEMO_TEACHER, otfSession.getPresenter());
                if (Objects.nonNull(req.getSessionSuccessful())) {
                    userLead.put(LeadSquaredRequest.Constants.SESSION_SUCCESSFUL, String.valueOf(req.getSessionSuccessful()));
                }
                userLead.put(LeadSquaredRequest.Constants.SESSION_FAILURE_REASON, failureReason);
                if (Objects.nonNull(req.getParentInterest())) {
                    userLead.put(LeadSquaredRequest.Constants.PARENT_INTEREST, String.valueOf(req.getParentInterest()));
                }
                if(StringUtils.isNotEmpty(req.getNotesForSales())) {
                    String salesNotes = req.getNotesForSales();
                    userLead.put(LeadSquaredRequest.Constants.NOTES, salesNotes);
                }
                userLead.put(LeadSquaredRequest.Constants.EARLY_LEARNING_COURSE, courseType.name());
                userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Demo Complete");
                for (String studentId : otfSession.getAttendees()) {
                    userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, studentId);
                    logger.info("Early Learning Demo Session Finished userLead : " + userLead);
                    LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                            LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_FINISH, null, userLead, null);
                    String messageGroupId = otfSession.getId() + "_UPSERT";
                    sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.DEMO_SESSION_FINISHED_SUPERKIDS, GSON.toJson(request), messageGroupId);
                }
                logger.info("sending email to student");
                if (req.getSessionSuccessful() != null && ELSessionStatus.YES.equals(req.getSessionSuccessful())) {
                    if (ArrayUtils.isNotEmpty(otfSession.getLabels()) && otfSession.getLabels().contains(SessionLabel.SUPER_READER)) {
                        try {
                            logger.info("Super Reader Session Ended");
                            sqsManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_SUPER_READER_POST_SESSION_MAIL, GSON.toJson(otfSession));
                        } catch (Exception ex) {
                            logger.error("Error while sending Early Learning Demo session mail: " + otfSession + " error:" + ex.getMessage());
                        }
                    }
                    if (ArrayUtils.isNotEmpty(otfSession.getLabels()) && otfSession.getLabels().contains(SessionLabel.SUPER_CODER)) {
                        List<GTTAttendeeDetails> joinedGtt = null;
                        for (String studentId : otfSession.getAttendees()) {
                            joinedGtt = gttAttendeeDetailsManager
                                    .getJoinedSessionsAttendeeDetailForStudent(Collections.singleton(otfSession.getId()), studentId);
                        }
                        GTTAttendeeDetails gttAttendeeDetails = Optional.ofNullable(joinedGtt).orElseGet(ArrayList::new)
                                            .stream()
                                            .findFirst()
                                            .orElse(null);
                        if(Objects.nonNull(gttAttendeeDetails)) {
                            logger.info("Updating GTTAttendeeDetails with session status - {}",req.getSessionSuccessful());
                            gttAttendeeDetails.setSessionSuccessful(req.getSessionSuccessful());
                            gttAttendeeDetailsManager.updateGTTAttendeeDetails(gttAttendeeDetails);
                            try {
                                logger.info("Super Coder Demo Session Certificate");
                                sqsManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_DEMO_SESSION_CERTIFICATE, GSON.toJson(otfSession));
                            } catch (Exception ex) {
                                logger.error("Error while sending Early Learning Demo Session Certificate mail: " + otfSession + " error:" + ex.getMessage());
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("error sending data in Leadsquare" + otfSession.getId() + " error:" + ex.getMessage());
            }
        }

        return new PlatformBasicResponse();
    }

    @Deprecated
    public List<OverBooking> hasEarlyLearningBooking(String studentId, SessionLabel earlyLearningCourse) throws ForbiddenException {

        OverBooking scheduledBooking = overBookingDAO.getEarlyLearningBookingByStudentId(studentId, earlyLearningCourse);
        if (Objects.nonNull(scheduledBooking) && SessionLabel.SUPER_READER.equals(earlyLearningCourse)) {
            // Early Learning Booking exists
            if (scheduledBooking.getSlotStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
                return Collections.singletonList(scheduledBooking);
            }
            String pastSessionId = scheduledBooking.getSessionId();
            if (StringUtils.isNotEmpty(pastSessionId)) {
                // Check if the User has already attempted the session
                List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager.getJoinedSessionsAttendeeDetailForUser(Collections.singleton(pastSessionId), studentId);
                if (!ArrayUtils.isEmpty(joinedGtt)) {
                    throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION, "User has already attended the Early Learning Session");
                }
            }
        }

        if (SessionLabel.SUPER_CODER.equals(earlyLearningCourse)) {
            if (Objects.nonNull(scheduledBooking)) {
                if (scheduledBooking.getSlotStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
                    return Collections.singletonList(scheduledBooking);
                }
                String pastSessionId = scheduledBooking.getSessionId();
                if (StringUtils.isNotEmpty(pastSessionId)) {
                    // Check if the User has already attempted the session
                    List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager.getJoinedSessionsAttendeeDetailForUser(Collections.singleton(pastSessionId), studentId);
                    if (!ArrayUtils.isEmpty(joinedGtt)) {
                        throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION, "User has already attended the Early Learning Session");
                    }
                }
            }

            long currentTimeInMillis = System.currentTimeMillis(); // let this variable 18 Jul 12 AM
            // Check if the User has already booked a session
            OTFSession otfSession = otfSessionDAO.getELOldBookedSession(studentId, currentTimeInMillis);
            if (otfSession != null) {
                if (otfSession.getStartTime() > currentTimeInMillis - DateTimeUtils.MILLIS_PER_HOUR) {
                    OverBooking sessionWithOutOverbooking = new OverBooking(otfSession);
                    logger.info("sessionWithOutOverbooking : " + sessionWithOutOverbooking);
                    overBookingDAO.save(sessionWithOutOverbooking);
                    return Collections.singletonList(sessionWithOutOverbooking);
                } else {
                    List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager.getJoinedSessionsAttendeeDetailForUser(Collections.singleton(otfSession.getId()), studentId);
                    if (!ArrayUtils.isEmpty(joinedGtt)) {
                        throw new ForbiddenException(ErrorCode.ALREADY_ATTENDED_EL_SESSION, "User has already attended the Early Learning Session");
                    }
                }
            }
        }
        // Early Learning Booking doesn't exist
        return new ArrayList<>();
    }

    @Deprecated
    public PlatformBasicResponse reScheduleEarlyLearningBooking(ReScheduleEarlyLearningBookingReq request) throws BadRequestException, ForbiddenException, ConflictException, NotFoundException {
        deleteEarlyLearningBooking(mapper.map(request, CancelOrDeleteBookingReq.class));
        OverBooking reScheduledBooking = addEarlyLearningBookingUsingPrevBookingId(mapper.map(request, AddEarlyLearningBookingReq.class), request.getBookingId());
        return new PlatformBasicResponse(true, reScheduledBooking, "");
    }

    public PlatformBasicResponse addAlternateNumberToEarlyLearningBooking(AddAlternateNumberForEarlyLearningBookingReq request, Long callingUserId) throws NotFoundException, BadRequestException {
        OverBooking scheduleBooking = overBookingDAO.getOverBookingById(request.getBookingId());
        if (Objects.isNull(scheduleBooking)) {
            throw new NotFoundException(ErrorCode.EL_BOOKING_NOT_FOUND, "Early Booking Not Found : " + request.getBookingId());
        }
        if (!callingUserId.toString().equals(scheduleBooking.getStudentId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to update alternate phone number");
        }
        scheduleBooking.setAlternatePhoneNumber(request.getPhoneNumber());
        overBookingDAO.save(scheduleBooking);
        //LeadSquared Entry For Overbooking
        try{
            Map<String,String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, scheduleBooking.getStudentId());
            userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME,otfSessionManager.getFormattedDateForLeadSquared(scheduleBooking.getSlotStartTime(), true));
            userLead.put(LeadSquaredRequest.Constants.BOOKING_ID,scheduleBooking.getId());
            userLead.put(LeadSquaredRequest.Constants.CALLING_USER_ID,String.valueOf(httpSessionUtils.getCallingUserId()));
            userLead.put(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER, scheduleBooking.getAlternatePhoneNumber());
            logger.info("BOOK_DEMO_SESSION_SUPERKIDS_ALT_NUMBER userLeads Alternate phone Number update: "+userLead);
            LeadSquaredRequest leadRequest = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_BOOK, null, userLead, null);
            String messageGroupId = scheduleBooking.getStudentId() + "_DEMO";
            sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOK_DEMO_SESSION_SUPERKIDS_ALT_NUMBER, GSON.toJson(leadRequest), messageGroupId);
        } catch (Exception ex) {
            logger.error("error sending data in Leadsquare" + scheduleBooking + " error:" + ex.getMessage());
        }

        return new PlatformBasicResponse(true, scheduleBooking, "");
    }

    @Deprecated
    public OverBooking addEarlyLearningBookingUsingPrevBookingId(AddEarlyLearningBookingReq request, String prevBookingId) throws BadRequestException, ForbiddenException, ConflictException {
        OverBooking overBooking = new OverBooking();
        Long startTime = CommonCalendarUtils.getSlotStartTime(request.getSlotStartTime());
        Long endTime = CommonCalendarUtils.getSlotEndTime(request.getSlotEndTime());
        logger.info("Booking StartTime : " + startTime);
        logger.info("Booking EndTime   : " + endTime);
        if (endTime - startTime > DateTimeUtils.MILLIS_PER_HOUR) {
            throw new BadRequestException(ErrorCode.EL_SESSION_SLOT_DURATION_ERROR, "Slot duration is more than one hour");
        }
        overBooking.setStudentId(request.getStudentId());
        overBooking.setSlotStartTime(startTime);
        overBooking.setSlotEndTime(endTime);
        overBooking.setGrade(request.getGrade());
        overBooking.setState(SessionState.SCHEDULED);
        OverBooking scheduledBooking = overBookingDAO.getEarlyLearningBookingByStudentId(request.getStudentId(), request.getEarlyLearningCourse());
        if (Objects.nonNull(scheduledBooking)) {
            // Early Learning Booking exists
            if (scheduledBooking.getSlotStartTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR) {
                throw new ConflictException(ErrorCode.ALREADY_BOOKED_EL_SESSION, "Booking Already Exists For Early Learning Session");
            }
        }
        double overBookingFactor = OverBooking.getOverBookingFactor(request.getEarlyLearningCourse());
        logger.info("overBookingFactor : {}", overBookingFactor);
        //Checking for fixed Number of booking
        int availableTeachers = calendarEntryManager.getEarlyLearningAvailableTeachers(startTime, endTime, request.getEarlyLearningCourse()).size();
        List<SlotBookingCount> slotBookingCount = overBookingDAO.getOverBookingAggregationWithSlotTime(startTime, endTime, request.getEarlyLearningCourse());
        int numberOfBookingInSlot = 0;
        Map<Long, Long> slotBookingMapping;
        if (!CollectionUtils.isEmpty(slotBookingCount)) {
            slotBookingMapping = slotBookingCount.stream().filter(Objects::nonNull).collect(Collectors.toMap(
                    SlotBookingCount::getSlotStartTime,
                    SlotBookingCount::getCount
            ));
            if (slotBookingMapping.containsKey(startTime)) {
                numberOfBookingInSlot = slotBookingMapping.get(startTime).intValue();
                logger.info("CurrentTime : {} , numberOfBookingInSlot : {} ", System.currentTimeMillis(), numberOfBookingInSlot);
            }
        }
        if (Math.floor(availableTeachers * overBookingFactor) <= numberOfBookingInSlot) {
            throw new ConflictException(ErrorCode.BOOKING_CLOSED, "Booking is closed by the time, you clicked. Please Check other slot");
        }
//        Set<OTFSessionFlag> earlyLearningFlag = new HashSet<>();
//        earlyLearningFlag.add(OTFSessionFlag.EARLY_LEARNING);
//        overBooking.setFlags(earlyLearningFlag);

        Set<SessionLabel> labels = new HashSet<>();
        labels.add(request.getEarlyLearningCourse());
        overBooking.setLabels(labels);

        // updating alternate Number if it exists in previous booking
        String alternatePhoneNumber = null;
        if (StringUtils.isNotEmpty(prevBookingId)) {
            OverBooking oldScheduleBooking = overBookingDAO.getOverBookingById(prevBookingId);
            if (Objects.nonNull(oldScheduleBooking) && StringUtils.isNotEmpty(oldScheduleBooking.getAlternatePhoneNumber())) {
                logger.info("Updating reScheduledBooking alternateNumber");
                alternatePhoneNumber = oldScheduleBooking.getAlternatePhoneNumber();
                overBooking.setAlternatePhoneNumber(alternatePhoneNumber);
            }
        }

        overBookingDAO.save(overBooking);
        // 3. send email to  student
        try {
            logger.info("sending email and sms to student");
            sqsManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_BOOKING_MAIL, GSON.toJson(overBooking));
        } catch (Exception ex) {
            logger.error("Error while sending Early Learning Demo session mail: " + overBooking + " error:" + ex.getMessage());
        }
        //LeadSquared Entry For Overbooking
        try {
            Map<String, String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, overBooking.getStudentId());
            userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, otfSessionManager.getFormattedDateForLeadSquared(overBooking.getSlotStartTime(), true));
            userLead.put(LeadSquaredRequest.Constants.BOOKING_ID, overBooking.getId());
            userLead.put(LeadSquaredRequest.Constants.CALLING_USER_ID, String.valueOf(httpSessionUtils.getCallingUserId()));
            if (StringUtils.isNotEmpty(alternatePhoneNumber)) {
                userLead.put(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER, overBooking.getAlternatePhoneNumber());
            }
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "DEMO BOOKED");
            logger.info("BOOK_DEMO_SESSION_SUPERKIDS userLeads : " + userLead);
            LeadSquaredRequest leadRequest = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_BOOK, null, userLead, null);
            String messageGroupId = overBooking.getStudentId() + "_DEMO";
            sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOK_DEMO_SESSION_SUPERKIDS, GSON.toJson(leadRequest), messageGroupId);
        } catch (Exception ex) {
            logger.error("error sending data in Leadsquare" + overBooking + " error:" + ex.getMessage());
        }
        return overBooking;
    }

    @Deprecated
    public OverBooking addEarlyLearningBooking(AddEarlyLearningBookingReq request) throws BadRequestException , ForbiddenException , ConflictException {
        return addEarlyLearningBookingUsingPrevBookingId(request, null);
    }

    @Deprecated
    public OTFSession joinEarlyLearningSession(AddEarlyLearningSessionReq request) throws  BadRequestException, ConflictException , VException{

        OTFSession otfSession = OTFSession.createEarlyLearningSession(request);
        Long startTime = CommonCalendarUtils.getSlotStartTime(request.getStartTime());
        Long endTime = CommonCalendarUtils.getSlotStartTime(request.getEndTime());
        SessionSlot sessionSlot = new SessionSlot(startTime, endTime);
        logger.info("sessionSlot : {}",sessionSlot);
        if(endTime - startTime > DateTimeUtils.MILLIS_PER_HOUR) {
            throw new BadRequestException(ErrorCode.EL_SESSION_SLOT_DURATION_ERROR, "Slot duration is more than one hour");
        }
        OverBooking earlyLearningBooking = overBookingDAO.getEarlyLearningBookingByStudentId(request.getStudentId(), request.getEarlyLearningCourse());
        if(Objects.isNull(earlyLearningBooking)) {
            throw new ConflictException(ErrorCode.EL_BOOKING_NOT_FOUND, "Early  Learning Booking not found for user");
        }
        if(System.currentTimeMillis() < earlyLearningBooking.getSlotStartTime() - 10 * DateTimeUtils.MILLIS_PER_MINUTE) {
            throw new ForbiddenException(ErrorCode.SESSION_NOT_STARTED, "Early  Learning Session is not  Started yet");
        }
        if(System.currentTimeMillis() > earlyLearningBooking.getSlotEndTime()) {
            throw new ForbiddenException(ErrorCode.SESSION_ALREADY_ENDED, "Early  Learning Session has been ended");
        }

        if(StringUtils.isNotEmpty(earlyLearningBooking.getSessionId())) {
            OTFSession scheduledSession = otfSessionDAO.getById(earlyLearningBooking.getSessionId());
            logger.info("scheduledSession : "+scheduledSession);
            if(Objects.nonNull(scheduledSession)) {
                return scheduledSession;
            }
        } else {
            if(Objects.isNull(earlyLearningBooking.getIsTeacherAssigned()) || !OverBookingStatus.NO.equals(earlyLearningBooking.getIsTeacherAssigned())) {
                if (!redisDAO.setnx(request.getStudentId() + "_" + startTime, Long.toString(System.currentTimeMillis()), 5)) {
                    throw new ConflictException(ErrorCode.REQUEST_PROCESSING, "Your request is processing in our system, Session access link will open automatically on new tab");
                }
            }
        }
        List<String> availableTeachers = calendarEntryManager.getEarlyLearningAvailableTeachers(startTime , endTime, request.getEarlyLearningCourse());
        logger.info("availableTeachers : "+availableTeachers);
        String presenter;
        // Random Teacher allocation
        while(true) {
            Collections.shuffle(availableTeachers);
            if(availableTeachers.size() == 0) {
                earlyLearningBooking.setIsTeacherAssigned(OverBookingStatus.NO);
                overBookingDAO.save(earlyLearningBooking);
                noTeacherAvailableLeadSquaredEvent(earlyLearningBooking);
                throw new ConflictException(ErrorCode.TEACHER_NOT_AVAILABLE,"No Teacher available, Our Team will reach out to you soon");
            }
            Random randomNumber = new Random();
            int randomIndex = randomNumber.nextInt(availableTeachers.size());
            presenter = availableTeachers.get(randomIndex);

            boolean set = redisDAO.setnx(presenter + "_" + startTime, Long.toString(System.currentTimeMillis()), 10);
            if (!set) {
                availableTeachers.remove(randomIndex);
            } else {
                break;
            }
        }
        // i am already checking slot availability in getEarlyLearningAvailableTeachers
        // This part can be removed
        // check slot conflicts for presenter and student
        otfSessionManager.checkSlotAvailabilityForUser(sessionSlot, presenter);
        otfSession.setCanvasStreamingType(CanvasStreamingType.VEDANTU);
        otfSession.setAttendees(Collections.singletonList(request.getStudentId()));
        otfSession.setPresenter(presenter);
        otfSession.setTitle("Early learning session");
//        Set<OTFSessionFlag> earlyLearningFlag = new HashSet<>();
//        earlyLearningFlag.add(OTFSessionFlag.EARLY_LEARNING);
//        otfSession.setFlags(earlyLearningFlag);
        Set<SessionLabel> labels = new HashSet<>();
        labels.add(request.getEarlyLearningCourse());
        otfSession.setLabels(labels);
        otfSessionDAO.save(otfSession);
        // Post session schedule tasks
        // 1. generate session link and create GTTAttendeeDetails
        if (otfSession.getStartTime() != null) {
            if (StringUtils.isEmpty(otfSession.getMeetingId()) || StringUtils.isEmpty(otfSession.getSessionURL())) {
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("session", otfSession);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_SESSION_LINK, payload);
                asyncTaskFactory.executeTask(params);*/
                otfSessionManager.sendToQForGeneratingSessionLinkAndGTT(otfSession);
            }
        }
        // 2. blocking teacher calendar for session duration
        CalendarEntryReq calendarEntryReq = new CalendarEntryReq(presenter,startTime,endTime,CalendarEntrySlotState.SESSION,CalendarReferenceType.WEBINAR_SESSION, otfSession.getId());
        calendarEntryManager.upsertCalendarEntry(calendarEntryReq);
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", mapper.map(otfSession, OTFSessionPojo.class));
            payload.put("event", SessionEventsOTF.OTF_SESSION_SCHEDULED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER_OTF, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for OTF_SESSION_SCHEDULED " + e);
        }
        // 3. send email to presenter
        try {
            logger.info("sending email and sms to Teacher");
            sqsManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.SEND_EARLY_LEARNING_SESSION_MAIL, GSON.toJson(otfSession));
        } catch (Exception ex) {
            logger.error("Error while sending Early Learning Demo session mail: " + otfSession + " error:" + ex.getMessage());
        }
        logger.info("otfSession Id : "+otfSession.getId());
        vedantuWaveSessionManager.sendSessionsToNodeForCaching(Collections.singletonList(otfSession));
        if (otfSession.getStartTime() < (System.currentTimeMillis() + 10 * DateTimeUtils.MILLIS_PER_MINUTE)) {
            vedantuWaveSessionManager.initSessionRecordingFromNode(otfSession);
        }
        logger.info("saved addEarlyLearningSession session: " + otfSession.toString());
        return otfSession;
    }

    @Deprecated
    public void noTeacherAvailableLeadSquaredEvent(OverBooking earlyLearningBooking) {

        try{
            Map<String,String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, earlyLearningBooking.getStudentId());
            userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME,otfSessionManager.getFormattedDateForLeadSquared(earlyLearningBooking.getSlotStartTime(), true));
            userLead.put(LeadSquaredRequest.Constants.BOOKING_ID,earlyLearningBooking.getId());
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Overbooking Cancelled");

            logger.info("noTeacherAvailableLeadSquaredEvent userLeads : "+userLead);
            LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_CANCEL, null, userLead, null);
            String messageGroupId = earlyLearningBooking.getStudentId() + "_NO_TEACHER_AVAILABLE";
            sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.BOOKING_CANCELLED_BY_OVERBOOKING, GSON.toJson(request), messageGroupId);
        } catch (Exception ex) {
            logger.error("error sending data in LeadSquare" + earlyLearningBooking + " error:" + ex.getMessage());
        }
    }

    public List<OverBookingPojo> getEarlyLearningBooking(GetOverBookingListReq req) {

        List<OverBookingPojo> response = new ArrayList<>();


        List<OverBooking> earlyLearningBooking = new ArrayList<>(overBookingDAO.getEarlyLearningBooking(req));


        Set<String> sessionIds = earlyLearningBooking
                .stream()
                .map(OverBooking::getSessionId)
                .filter(StringUtils::isNotEmpty)
                .collect(Collectors.toSet());

        // Fetch GTT attendees
        List<GTTAttendeeDetails> gttAttendeeDetailsList = gttAttendeeDetailsManager.getAttendeeDetailsForSessions(sessionIds);
        Map<String, GTTAttendeeDetails> attendeeMap = OTFSessionManager.createAttendeeInfoMap(gttAttendeeDetailsList);
        // Create session infos
        for (OverBooking earlyBooking : earlyLearningBooking) {
            if (earlyBooking == null) {
                continue;
            }
            OverBookingPojo oTFBookingPojo = mapper.map(earlyBooking, OverBookingPojo.class);
            oTFBookingPojo.setBookingId(earlyBooking.getId());
            Set<String> allStudentSet = new HashSet<>();

            if (StringUtils.isNotEmpty(earlyBooking.getStudentId())) {
                allStudentSet.add(earlyBooking.getStudentId());
            }

            if (!CollectionUtils.isEmpty(allStudentSet)) {
                List<OTFSessionAttendeeInfo> attendeeInfos = new ArrayList<>();
                for (String student : allStudentSet) {
                    OTFSessionAttendeeInfo sessionAttendeeInfo = new OTFSessionAttendeeInfo(student);
                    if (!StringUtils.isEmpty(earlyBooking.getSessionId())
                            && attendeeMap.containsKey(OTFSessionManager.getAttendeeInfoMapKey(earlyBooking.getSessionId(), student))) {
                        GTTAttendeeDetails attendeeDetails = attendeeMap
                                .get(OTFSessionManager.getAttendeeInfoMapKey(earlyBooking.getSessionId(), student));
                        if (attendeeDetails != null){
                            sessionAttendeeInfo.setJoinTimes(attendeeDetails.getJoinTimes());
                            sessionAttendeeInfo.setTimeInSession(attendeeDetails.getTimeInSession());
                            sessionAttendeeInfo.setActiveIntervals(attendeeDetails.getActiveIntervals());
                        }
                    }

                    attendeeInfos.add(sessionAttendeeInfo);
                }
                oTFBookingPojo.setAttendeeInfos(attendeeInfos);
            }

            response.add(oTFBookingPojo);
        }

        return response;

    }


    public PlatformBasicResponse deleteEarlyLearningBooking(CancelOrDeleteBookingReq req) throws NotFoundException, BadRequestException {

        OverBooking scheduledBooking = overBookingDAO.getOverBookingById(req.getBookingId());
        if(Objects.isNull(scheduledBooking)) {
            throw new NotFoundException(ErrorCode.EL_BOOKING_NOT_FOUND, "Early Booking Not Found : "+req.getBookingId());
        }

        if (SessionState.CANCELLED.equals(scheduledBooking.getState())
                || SessionState.FORFEITED.equals(scheduledBooking.getState())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session already cancelled");
        }

        if (System.currentTimeMillis() > (scheduledBooking.getSlotStartTime() - DateTimeUtils.MILLIS_PER_MINUTE * 10L)) {
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "Cancellation allowed only until 10 minutes before booking start time");
        }

        scheduledBooking.setState(SessionState.CANCELLED);
        scheduledBooking.setCancelledBY(httpSessionUtils.getCallingUserId().toString());
        scheduledBooking.setRemarks(req.getRemark());
        overBookingDAO.save(scheduledBooking);

        try{
            Map<String,String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, otfSessionManager.getFormattedDateForLeadSquared(scheduledBooking.getSlotStartTime(), true));
            userLead.put(LeadSquaredRequest.Constants.BOOKING_ID, scheduledBooking.getId());
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Signed Cancelled");
            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, scheduledBooking.getStudentId());
            userLead.put(LeadSquaredRequest.Constants.CALLING_USER_ID, String.valueOf(httpSessionUtils.getCallingUserId()));
            logger.info("Superkids Cancel Session : "+userLead);
            LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_DEMO_CANCEL, null, userLead, null);
            String messageGorupId = scheduledBooking.getId() + "_CANCEL";
            sqsManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.CANCEL_SESSION_SUPERKIDS, GSON.toJson(request), messageGorupId);

        } catch (Exception ex) {
            logger.error("error sending data in Leadsquare" + scheduledBooking.getId() + " error:" + ex.getMessage());
        }
        return new PlatformBasicResponse();
    }

    public List<OTFSession> getEarlyLearningSessionByStudentId(String studentId, SessionLabel earlyLearningCourse, Integer start, Integer size) {

        return otfSessionDAO.getELSessionByStudentId(studentId, earlyLearningCourse, start, size);
    }

    public List<GTTAttendeeDetails> getSessionJoiningDetailsBySessionId(Set<String> sessionId, String studentId, Integer start, Integer size) {

        return gttAttendeeDetailsDAO.getEarlyLearningAttendeeDetailForUser(sessionId, studentId, start, size);
    }

    public List<OverBooking> getEarlyLearningBookingByStudentId(String studentId, SessionLabel earlyLearningCourse, Integer start, Integer size) {

        GetOverBookingListReq overBookingReq = new GetOverBookingListReq();
        overBookingReq.setStudentId(Long.valueOf(studentId));
        overBookingReq.setEarlyLearningCourseType(earlyLearningCourse);
        overBookingReq.setStart(start);
        overBookingReq.setSize(size);

        return overBookingDAO.getEarlyLearningBooking(overBookingReq);
    }

    public ELDemoSessionPojo getEarlyLearningDemoDetails(String studentId, SessionLabel earlyLearningCourseType) throws ForbiddenException {

        List<OTFSession> scheduledSessions = otfSessionDAO.getELSessionDemoDetailsByStudentId(studentId, earlyLearningCourseType, 0, 20);

        if (CollectionUtils.isEmpty(scheduledSessions)) {
            throw new ForbiddenException(ErrorCode.NOT_FOUND_ERROR, "Please complete the demo to access the certificate");
        } else {
            Set<String> pastSessionIds = scheduledSessions
                    .stream()
                    .filter(Objects::nonNull)
                    .filter(e -> Optional.ofNullable(e.getLabels()).orElseGet(HashSet::new).contains(earlyLearningCourseType))
                    .filter(s -> s.getEndTime() < System.currentTimeMillis())
                    .map(OTFSession::getId)
                    .collect(Collectors.toSet());

            if (!CollectionUtils.isEmpty(pastSessionIds)) {
                List<GTTAttendeeDetails> joinedGtt = gttAttendeeDetailsManager.getJoinedSessionsAttendeeDetailForUser(pastSessionIds, studentId);
                if (!CollectionUtils.isEmpty(joinedGtt)) {
                    GTTAttendeeDetails joinedDetails = joinedGtt.get(0);
                    return new ELDemoSessionPojo(joinedDetails.getSessionStartTime(),joinedDetails.getUserId(),joinedDetails.getSessionId(),true);
                }
            }
        }
        return new ELDemoSessionPojo(null,studentId,null,false);
    }

}