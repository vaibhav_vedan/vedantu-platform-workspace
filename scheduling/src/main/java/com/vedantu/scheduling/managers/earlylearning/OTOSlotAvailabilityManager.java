package com.vedantu.scheduling.managers.earlylearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.vedantu.onetofew.enums.SessionLabel;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.scheduling.CommonCalendarUtils;

@Service("otoSlotAvailabilityManager")
public class OTOSlotAvailabilityManager extends AbstractSlotAvailabilityManager {
	

	@Override
	public String getBookingAvailabilityBitString(Long startTime, Long endTime, ELTeachersProficiencies proficiencyIds,
			Integer grade, SessionLabel earlyLearningCourseType) {
		
		
		 List<String> teacherIds = proficiencyIds.getElTeachersByGrade(grade);
			if (CollectionUtils.isEmpty(teacherIds)) {
				return Constants.NOT_AVAILABLE_BIT;
			}
		StringBuilder availabiltyInSLotBooking = new StringBuilder();
        Long dayStartTime =  CommonCalendarUtils.getDayStartTime(startTime);
        Long currentStartTime = startTime;
        while (dayStartTime < endTime) {
            Long dayEndTime = CommonCalendarUtils.getDayEndTime(dayStartTime);
            List<List<String>> allTeachersPartitions = Lists.partition(teacherIds, Constants.PARTITION_SIZE);
            List<CalendarEntry> calendarEntries = new ArrayList<>();
            for (List<String> teachersPartition : allTeachersPartitions) {
                calendarEntries.addAll(calendarEntryDAO.getTeachersAvailabilityInSlot(dayStartTime, dayEndTime, teachersPartition));
            }
            int startIndex = CommonCalendarUtils.getSlotStartIndex(currentStartTime, dayStartTime);
            int endIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
            if (endTime < dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY) {
                endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, dayStartTime);
            }
			//Calculating Available teachers for every slot in a week
			Map<Integer, Long> teacherAvailableInSlot =  calculateTeacherAvailabilityCountForSlots(calendarEntries);
			for(int slotIndex = startIndex; slotIndex <= endIndex; slotIndex++) {
				Long teacherCountForSlot = Optional.ofNullable(teacherAvailableInSlot.get(slotIndex)).orElse(0L);
				if(teacherCountForSlot == 0 || !CalendarUtils.validateTime(slotIndex, dayStartTime)) {
					availabiltyInSLotBooking.append(Constants.NOT_AVAILABLE_BIT);
				}  else {
					availabiltyInSLotBooking.append(Constants.AVAILABLE_BIT);
				}
			}
            dayStartTime = dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY;
            currentStartTime = dayStartTime;
        }
        
        return availabiltyInSLotBooking.toString();
		
	}

	@Override
	public ProficiencyType getAvailableProficiencyCategoryForDemo(Long startTime, Long endTime,
			ELTeachersProficiencies proficiencyIds, Integer grade, SessionLabel earlyLearningCourseType) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
