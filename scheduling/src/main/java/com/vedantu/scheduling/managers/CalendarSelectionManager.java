package com.vedantu.scheduling.managers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.scheduling.response.SlotSelectionPojo;
import com.vedantu.scheduling.response.TagSlots;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;

@Service
public class CalendarSelectionManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarSelectionManager.class);

	public static final String eveningSlug = "evening";
	public static final String morningSlug = "morning";

	public static final int eveningSlotLimit = 4;
	public static final int morningSlotLimit = 2;

	public static final String[] fields = { "sectionName", "tagName", "startTime", "endTime", "maxSlots" };

	public SlotSelectionPojo initSlotSelectionPojo()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		List<TagSlots> tagSlots = new ArrayList<TagSlots>();
		for (int i = 1;; i++) {
			String slotConfigValues = ConfigUtils.INSTANCE.getStringValue("calendar.selectionslot" + i);
			if (StringUtils.isEmpty(slotConfigValues)) {
				break;
			}

			String[] slotValues = slotConfigValues.split(",");
			TagSlots tagSlot = new TagSlots();
			for (int fieldIndex = 0; fieldIndex < slotValues.length; fieldIndex++) {
				Field field = TagSlots.class.getField(fields[fieldIndex]);
				String value = slotValues[fieldIndex];
				if (StringUtils.isEmpty(value)) {
					continue;
				}

				value = value.trim();
				if (Long.class.equals(field.getType())) {
					long longValue;
					if ("maxSlots".equals(field.getName())) {
						longValue = Long.parseLong(value);
					} else {
						longValue = Long.parseLong(value) * CommonCalendarUtils.MILLIS_PER_HOUR;
					}

					field.set(tagSlot, longValue);
				} else {
					field.set(tagSlot, value);
				}
			}

			tagSlots.add(tagSlot);
		}

		return new SlotSelectionPojo(tagSlots, 0);
	}

	// startIndex should belong to day start time based on time zone.
	public SlotSelectionPojo populateSlotSelectionPojo(Integer[] trueAvailability, long startTime, int startIndex)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		SlotSelectionPojo slotSelectionPojo = initSlotSelectionPojo();
		List<TagSlots> tagSlots = slotSelectionPojo.getTagSlots();

		for (TagSlots tagSlot : tagSlots) {
			List<SessionSlot> sessionSlots = getAvailableSlots(trueAvailability, startTime,
					startIndex + getSlotIndex(tagSlot.getStartTime()), startIndex + getSlotIndex(tagSlot.getEndTime()));
			tagSlot.setAvailableSlots(sessionSlots);
		}

		List<SessionSlot> otherSlots = getAvailableSlots(trueAvailability, startTime, startIndex,
				startIndex + CommonCalendarUtils.getCalendarEntrySlotCount());
		slotSelectionPojo.setOthers(TagSlots.createOthers(otherSlots));
		slotSelectionPojo.updateTotalCount();
		return slotSelectionPojo;
	}

	private int getSlotIndex(long offset) {
		return (int) (offset / CommonCalendarUtils.getSlotLength());
	}

	private List<SessionSlot> getAvailableSlots(Integer[] trueAvailability, long startTime, int startIndex,
			int endIndex) {
		// TODO : find out if endIndex should be inclusive or exclusive
		List<SessionSlot> sessionSlots = new ArrayList<SessionSlot>();
		for (int i = startIndex; (i <= (endIndex - 1)) && ((i + 3) < trueAvailability.length); i = i + 2) {
			if ((trueAvailability[i] == 1) && (trueAvailability[i + 1] == 1) && (trueAvailability[i + 2] == 1)
					&& (trueAvailability[i + 3] == 1)) {
				SessionSlot sessionSlot = new SessionSlot(startTime + CommonCalendarUtils.getSlotLength() * i,
						startTime + CommonCalendarUtils.getSlotLength() * (i + 4), null, null, null, null);
				sessionSlots.add(sessionSlot);
				trueAvailability[i] = 0;
				trueAvailability[i + 1] = 0;
				trueAvailability[i + 2] = 0;
				trueAvailability[i + 3] = 0;
				i = i + 2;
			}
		}

		return sessionSlots;
	}

	public void filterSlotByPriority(SlotSelectionPojo slotSelectionPojo) {
		if (slotSelectionPojo.getSlotCount() <= 6) {
			return;
		}

		// Filter by priority
		if (slotSelectionPojo.fetchSectionCount(eveningSlug) + slotSelectionPojo.fetchSectionCount(morningSlug) <= 6) {
			return;
		}

		// Filter additional slots based on the maxlimit
		if (slotSelectionPojo.fetchSectionCount(eveningSlug) < eveningSlotLimit) {
			int slotCountLimit = morningSlotLimit
					+ (eveningSlotLimit - slotSelectionPojo.fetchSectionCount(eveningSlug));
			slotSelectionPojo.filterMorningSlots(slotCountLimit);
		} else if (slotSelectionPojo.fetchSectionCount(morningSlug) < morningSlotLimit) {
			int slotCountLimit = eveningSlotLimit
					+ (morningSlotLimit - slotSelectionPojo.fetchSectionCount(morningSlug));
			slotSelectionPojo.filterEveningSlots(slotCountLimit);
		} else {
			slotSelectionPojo.filterEveningSlots(eveningSlotLimit);
			slotSelectionPojo.filterMorningSlots(morningSlotLimit);
		}

		// Update the slots
		slotSelectionPojo.updateTotalCount();
		logger.info(" slotSelectionPojo : " + slotSelectionPojo.toString());
	}
}
