package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.User.Role;
import com.vedantu.onetofew.request.GetFeedbackReq;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.scheduling.dao.entity.Feedback;
import com.vedantu.scheduling.dao.serializers.FeedbackDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.util.LogFactory;

@Service
public class FeedbackManager {

	@Autowired
	private OTFSessionManager oTFSessionManager;

	@Autowired
	@Qualifier("gttScheduleManager")
	private GTTScheduleManager gttScheduleManager;

	@Autowired
	@Qualifier("gttProScheduleManager")
	private GTTProScheduleManager gttProScheduleManager;

	@Autowired
	@Qualifier("gtt100ScheduleManager")
	private GTT100ScheduleManager gtt100ScheduleManager;

	@Autowired
	@Qualifier("gtwScheduleManager")
	private GTWScheduleManager gtwScheduleManager;

	@Autowired
	@Qualifier("gttWebinarScheduleManager")
	private GTTWebinarScheduleManager gttWebinarScheduleManager;

	@Autowired
	private FeedbackDAO feedbackDAO;

	@Autowired
	private OTFSessionDAO oTFSessionDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FeedbackManager.class);

	public Feedback feedback(Feedback feedback) throws IllegalAccessException {
		// Check if feedback already existing
		Role role = feedback.getRole();
		Query query = new Query();
		// query.addCriteria(Criteria.where(Feedback.Constants.RECEIVER_ID).is(feedback.getReceiverId()));
		query.addCriteria(Criteria.where(Feedback.Constants.SENDER_ID).is(feedback.getSenderId()));
		query.addCriteria(Criteria.where(Feedback.Constants.SESSION_ID).is(feedback.getSessionId()));
		List<Feedback> results = feedbackDAO.runQuery(query, Feedback.class);
		if (results != null && !results.isEmpty()) {
			Feedback result = results.get(0);
			feedback.setId(result.getId());
			if (Role.STUDENT.equals(role)) {
				String resultFeedbackReceived = result.getFeedbackReceived();
				String feedbackReceived = feedback.getFeedbackReceived();
				if (StringUtils.isEmpty(feedbackReceived) && !StringUtils.isEmpty(resultFeedbackReceived)) {
					feedback.setFeedbackReceived(result.getFeedbackReceived());
				}
				if (!StringUtils.isEmpty(feedbackReceived) && StringUtils.isEmpty(resultFeedbackReceived)) {
					result.setFeedbackReceived(feedback.getFeedbackReceived());
					feedback = result;
				}
			}
		}
		// else {
		// if (Role.TEACHER.equals(role)) {
		// // Update the student feedback entry based on feedback provided
		// // by teacher for student
		// String studentFeedbackJSON = feedback.getFeedback();
		// Type listType = new TypeToken<List<StudentFeedback>>() {
		// }.getType();
		// List<StudentFeedback> studentFeedbacks = new
		// Gson().fromJson(studentFeedbackJSON, listType);
		// Feedback feedbackEntry;
		// if (studentFeedbacks != null) {
		// for (StudentFeedback studentFeedback : studentFeedbacks) {
		// // If entry exists, it will save the feedback
		// // received.
		// feedbackEntry = new Feedback(feedback.getSenderId(),
		// studentFeedback.getUserId(),
		// feedback.getSessionId(), Role.STUDENT, null,
		// studentFeedback.getFeedback());
		// feedback(feedbackEntry);
		// }
		// }
		// }
		// }

		feedback = feedbackDAO.create(feedback);
		if (feedback == null) {
			return null;
		}
		return feedback;
	}

	public Feedback getFeedbackById(String id) {
		Feedback feedback = feedbackDAO.getById(id);
		return feedback;
	}

	public List<OTFSessionPojo> getSessionsForFeedback(long afterEndTime, long beforeEndTime) {
		// Query query = new Query();
		// query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(afterEndTime).lt(beforeEndTime));
		// query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
		// query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_ID).exists(true).ne(null));
		// List<OTFSession> results = oTFSessionDAO.runQuery(query,
		// OTFSession.class);
		List<OTFSessionPojo> oTFSessionPojos = new ArrayList<>();
		// if (!CollectionUtils.isEmpty(results)) {
		// List<String> errors = new ArrayList<>();
		// Set<String> batchIds = new HashSet<>();
		// batchIds.add(oTFSession.getBatchIds());
		// }
		//
		// if (CollectionUtils.isEmpty(batchIds)) {
		// return oTFSessionPojos;
		// }
		//
		// Query batchQuery = new Query();
		// batchQuery.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
		// List<Batch> batches = batchDAO.runQuery(batchQuery, Batch.class);
		//
		// Set<String> courseIds = new HashSet<>();
		// Map<String, Batch> batchMap = new HashMap<>();
		// for (Batch batch : batches) {
		// batchMap.put(batch.getId(), batch);
		// courseIds.add(batch.getCourseId());
		// }
		//
		// // Fetch course infos
		// Query courseQuery = new Query();
		// courseQuery.addCriteria(Criteria.where(Course.Constants.ID).in(courseIds));
		// List<Course> courses = courseDAO.runQuery(courseQuery, Course.class);
		// // Exit if no courses found
		// if (CollectionUtils.isEmpty(courses)) {
		// return oTFSessionPojos;
		// }
		// Map<String, Course> courseMap = new HashMap<>();
		// for (Course course : courses) {
		// courseMap.put(course.getId(), course);
		// }
		//
		// for (OTFSession oTFSession : results) {
		// Batch batch = batchMap.get(oTFSession.getBatchIds());
		//
		// if (oTFSession.getSessionToolType() == null
		// ||
		// !OTFSessionManager.SUPPORTED_SESSION_TOOL_TYPES.contains(oTFSession.getSessionToolType()))
		// {
		// if (batch != null &&
		// !CollectionUtils.isEmpty(batch.getEnrolledStudents())) {
		// oTFSession.setAttendees(new
		// ArrayList<>(batch.getEnrolledStudents()));
		// }
		// } else {
		// GTTScheduleManager scheduleManager = null;
		// switch (oTFSession.getSessionToolType()) {
		// case GTT:
		// scheduleManager = gttScheduleManager;
		// break;
		// case GTT_PRO:
		// scheduleManager = gttProScheduleManager;
		// break;
		// case GTT100:
		// scheduleManager = gtt100ScheduleManager;
		// break;
		// case GTW:
		// scheduleManager = gtwScheduleManager;
		// break;
		// case GTT_WEBINAR:
		// scheduleManager = gttWebinarScheduleManager;
		// break;
		// default:
		// break;
		// }
		//
		// try {
		// if (!Boolean.TRUE
		// .equals(oTFSession.containsFlag(com.vedantu.util.Constants.POST_SESSION_DATA_PROCESSED)))
		// {
		// scheduleManager.handleFeedback(oTFSession);
		// }
		//
		// if
		// (!oTFSession.containsFlag(com.vedantu.util.Constants.POST_SESSION_DATA_PROCESSED))
		// {
		// logger.info("Session entries not updated yet from Gototraining");
		// continue;
		// }
		// } catch (Exception ex) {
		// String errorMessage = "Error occured handling feedback from GTT -
		// ex:" + ex.toString()
		// + " message:" + ex.getMessage() + " session:" +
		// oTFSession.toString();
		// logger.info(errorMessage);
		// errors.add(errorMessage);
		// }
		// }
		//
		// if (!StringUtils.isEmpty(batch.getCourseId())) {
		// Course course = courseMap.get(batch.getCourseId());
		// if (course != null) {
		// oTFSessionPojos.add(oTFSessionManager.getSessionInfo(oTFSession,
		// course.toCourseBasicInfo(course, null, null)));
		// }
		// }
		// }
		//
		// if (!CollectionUtils.isEmpty(errors)) {
		// logger.error("Error handling feedback:" +
		// Arrays.toString(errors.toArray()));
		// }
		// }

		return oTFSessionPojos;
	}

	public List<Feedback> getFeedbacks(GetFeedbackReq req) {
		Query query = new Query();
		if (!StringUtils.isEmpty(req.getReceiverid())) {
			query.addCriteria(Criteria.where(Feedback.Constants.RECEIVER_ID).is(req.getReceiverid()));
		}
		if (!StringUtils.isEmpty(req.getSenderId())) {
			query.addCriteria(Criteria.where(Feedback.Constants.SENDER_ID).is(req.getSenderId()));
		}
		if (!StringUtils.isEmpty(req.getSessionId())) {
			query.addCriteria(Criteria.where(Feedback.Constants.SESSION_ID).is(req.getSessionId()));
		}

		Integer start = req.getStart();
		Integer limit = req.getSize();
		if (start == null || start < 0) {
			start = 0;
		}

		if (limit == null || limit <= 0) {
			limit = 20;
		}

		query.skip(start);
		query.limit(limit);
		List<Feedback> results = feedbackDAO.runQuery(query, Feedback.class);
		return results;
	}

}
