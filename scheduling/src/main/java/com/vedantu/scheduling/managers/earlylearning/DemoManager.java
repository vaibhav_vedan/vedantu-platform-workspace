package com.vedantu.scheduling.managers.earlylearning;

import com.vedantu.User.Role;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.enums.earlyLeaning.AnalyticsType;
import com.vedantu.scheduling.pojo.EarlyLearningSessionPojo;
import com.vedantu.scheduling.pojo.earlylearning.CourseConfiguration;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.scheduling.request.earlylearning.AddDemoRequest;
import com.vedantu.scheduling.request.earlylearning.JoinDemoRequest;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.scheduling.request.earlylearning.ReScheduleDemoRequest;
import com.vedantu.scheduling.response.AvailabilitySlotStringRes;
import com.vedantu.scheduling.response.earlylearning.JoinDemoResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

@Service
public class DemoManager {

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	@Qualifier("otoManager")
	private AbstractEarlyLearning otoManager;

	@Autowired
	@Qualifier("overbookingManager")
	private AbstractEarlyLearning overbookingManager;

	@Autowired
	private MigrationManager migrationManager;
	
	private Logger logger = LogFactory.getLogger(DemoManager.class);

	public EarlyLearningSessionPojo addDemo(AddDemoRequest request)
			throws VException {

		request.verify();
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN),Boolean.TRUE);

		return getEarlyLearningManager(request.getEarlyLearningCourse()).addDemo(request);
	}

	public JoinDemoResponse joinDemo(JoinDemoRequest request) throws BadRequestException, ConflictException, VException {

		request.verify();
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.verifyLoggedInAndCallingUser(Role.STUDENT, request.getStudentId(),"User is not authorized to Join a session");
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN),
				Boolean.TRUE);

		return getEarlyLearningManager(request.getEarlyLearningCourse()).joinDemo(request,
				CourseConfiguration.getCourseProperties(request.getEarlyLearningCourse()));
	}

	public EarlyLearningSessionPojo hasEarlyLearningDemo(String studentId, SessionLabel earlyLearningCourse) throws VException {
		
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN),
				Boolean.TRUE);

		if (Role.ADMIN.equals(sessionUtils.getCallingUserRole()) && studentId == null) {
			throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Please use tools section to access booking page");
		}
		
		if (Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && studentId == null) {
			studentId = String.valueOf(sessionUtils.getCallingUserId());
		}
		return getEarlyLearningManager(earlyLearningCourse).hasEarlyLearningDemo(studentId, earlyLearningCourse);
	}

    public AvailabilitySlotStringRes getDemoAvailabilitySlotStrings(AvailabilitySlotStringReq request) throws ForbiddenException, BadRequestException, VException {
    	
    	request.verify();
    	sessionUtils.checkIfUserLoggedIn();
		return getEarlyLearningManager(request.getEarlyLearningCourseType()).getCalendarAvailability(request);
    }
    
	private AbstractEarlyLearning getEarlyLearningManager(SessionLabel courseType) {
		if (CourseConfiguration.getCourseProperties(courseType).getOverBookingFactor() == 1.0) {
			return otoManager;
		}

		return overbookingManager;
	}

    public EarlyLearningSessionPojo reScheduleDemo(ReScheduleDemoRequest request) throws VException {

		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN),
				Boolean.TRUE);
		request.verify();
		return getEarlyLearningManager(request.getEarlyLearningCourse()).reScheduleDemo(request);
    }

    //For Testing only
    public Map<String, Object> getAvailableTeacher(AvailabilitySlotStringReq request) throws VException {
    	logger.info("Inside Demo Manager's Availability teacher's method.");
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN),
				Boolean.TRUE);
		return migrationManager.getAvailableBookingAndTeacher(request);
    }

    @Deprecated
	public Map<String, Object> migrateBookings(Integer limit) throws VException {
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.FALSE);
		return migrationManager.migrateBookings(limit);
	}

	public Map<AnalyticsType, Object> proficiencyAnalytics(ProficiencyAnalyticsRequest request) throws VException, ParseException {
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.FALSE);
		request.verify();
		return getEarlyLearningManager(request.getEarlyLearningCourseType()).proficiencyAnalytics(request);
	}
}
