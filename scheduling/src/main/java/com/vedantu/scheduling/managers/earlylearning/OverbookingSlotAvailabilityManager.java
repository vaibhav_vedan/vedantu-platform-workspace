package com.vedantu.scheduling.managers.earlylearning;

import java.util.*;

import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.util.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.scheduling.dao.serializers.OverBookingDAO;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.SlotBookingCount;
import com.vedantu.scheduling.pojo.earlylearning.CourseConfiguration;
import com.vedantu.scheduling.pojo.earlylearning.CourseProperties;
import com.vedantu.scheduling.pojo.earlylearning.SlotAvaillability;
import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.scheduling.CommonCalendarUtils;

@Service("overBookingSlotAvailabilityManager")
public class OverbookingSlotAvailabilityManager extends AbstractSlotAvailabilityManager {

	@Autowired
	private OverBookingDAO overBookingDAO;

	@Autowired
	@Qualifier("overbookingManager")
	private AbstractEarlyLearning overbookingManager;

	@Autowired
	private CalendarEntryManager calendarEntryManager;

	public static double overBookingFactor;

	// TODO: Check this logic
	@Override
	public String getBookingAvailabilityBitString(Long startTime, Long endTime, ELTeachersProficiencies proficiencyIds,
			Integer grade, SessionLabel earlyLearningCourseType) {

		StringBuilder slotAvailability = new StringBuilder();

		TeacherCategory gradeType = TeacherCategory.getGradeType(grade);
		List<SlotBookingCount> slotBookingCount = overBookingDAO.getOverBookingAggregationWithSlotTime(startTime,
				endTime, earlyLearningCourseType);
		Map<Long, Map<ProficiencyType, Long>> slotToCountMap = getBookingProficiencyCount(slotBookingCount);
		CourseProperties courseProperties = CourseConfiguration.getCourseProperties(earlyLearningCourseType);
		overBookingFactor = courseProperties.getOverBookingFactor();

		Long dayStartTime = CommonCalendarUtils.getDayStartTime(startTime);
		Long currentStartTime = startTime;

		Long testStartTime = System.currentTimeMillis();
		
		while (dayStartTime < endTime) {
			Long dayEndTime = CommonCalendarUtils.getDayEndTime(dayStartTime);
			int startIndex = CommonCalendarUtils.getSlotStartIndex(currentStartTime, dayStartTime);
			int endIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
			if (endTime < dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY) {
				endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, dayStartTime);
			}
			SlotAvaillability overbookingSlotAvailability = prepareTeachersAvailabilityMap(proficiencyIds, gradeType,
					dayStartTime, dayEndTime);
			for (int slotIndex = startIndex; slotIndex <= endIndex; slotIndex++) {
				Long slotTime = CommonCalendarUtils.getAbsoluteSlotTime(slotIndex, dayStartTime);
				int finalSlotIndex = slotIndex;
				ProficiencyType teacherAvailable = gradeType.getProficiencies().stream()
						.filter(proficiencyType -> overbookingSlotAvailability.isTeacherAvailable(finalSlotIndex, proficiencyType))
						.findFirst().orElse(null);
				if ( Objects.isNull(teacherAvailable) || !CalendarUtils.validateTime(slotIndex, dayStartTime)) {
					slotAvailability.append(Constants.NOT_AVAILABLE_BIT);
				} else if (!overbookingSlotAvailability.isSlotAvailable(Optional.ofNullable(slotToCountMap.get(slotTime)).orElseGet(HashMap::new),
						gradeType, slotIndex, overBookingFactor)) {
					slotAvailability.append(Constants.NOT_AVAILABLE_BIT_HOUR);
					slotIndex = slotIndex + 3;
				} else {
					slotAvailability.append(Constants.AVAILABLE_BIT);
				}
			}
			dayStartTime = dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY;
			currentStartTime = dayStartTime;

		}
		Long testEndTime = System.currentTimeMillis();
		logger.debug("testStartTime : " +testStartTime);
		logger.debug("testEndtime : " +testEndTime);
		logger.debug("total time in seconds : " + (testEndTime - testStartTime));
		
		return slotAvailability.toString();
	}

	public ProficiencyType getAvailableProficiencyCategoryForDemo(Long startTime, Long endTime,
			ELTeachersProficiencies proficiencyIds, Integer grade, SessionLabel earlyLearningCourseType) {

		TeacherCategory teacherCategory = TeacherCategory.getGradeType(grade);
		CourseProperties courseProperties = CourseConfiguration.getCourseProperties(earlyLearningCourseType);
		overBookingFactor = courseProperties.getOverBookingFactor();
		List<SlotBookingCount> slotBookingCount = overBookingDAO.getOverBookingAggregationWithSlotTime(startTime,
				endTime, earlyLearningCourseType);
		Map<Long, Map<ProficiencyType, Long>> slotBookingMapping = getBookingProficiencyCount(slotBookingCount);

		Long dayStartTime = CommonCalendarUtils.getDayStartTime(startTime);
		int startIndex = CommonCalendarUtils.getSlotStartIndex(startTime, dayStartTime);
		Long slotTime = CommonCalendarUtils.getAbsoluteSlotTime(startIndex, dayStartTime);
		return teacherCategory.getProficiencies()
				.stream()
				.filter(proficiencyType -> Objects.nonNull(getProficiencyCategory(startTime, endTime,
						proficiencyIds.getTeachersForProficiency(proficiencyType),
						Optional.ofNullable(slotBookingMapping.getOrDefault(slotTime,new HashMap<>()).get(proficiencyType)).orElse(0L), proficiencyType)))
				.findFirst()
				.orElse(null);
	}

	private ProficiencyType getProficiencyCategory(Long startTime, Long endTime, List<String> proficiencyIds,
			Long bookingCount, ProficiencyType proficiencyType) {
			List<List<String>> allTeachersPartitions = Lists.partition(proficiencyIds, 100);
			List<String> availableElTeachers = new ArrayList<>();
			for (List<String> teachersPartition : allTeachersPartitions) {
				availableElTeachers.addAll(calendarEntryManager.getAvailableUsers(startTime, endTime, teachersPartition));
			}
			if (Math.floor(availableElTeachers.size() * overBookingFactor) > bookingCount) {
				return proficiencyType;
			}
		return null;
	}
}
