package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.serializers.CalendarEntryDAO;
import com.vedantu.scheduling.pojo.BitSetEntry;
import com.vedantu.scheduling.pojo.BitSetUtils;
import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;

@Service
public class CalendarUtilsManager {

	@Autowired
	private CalendarEntryDAO calendarEntryDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarUtilsManager.class);

	public List<CalendarEntry> getCalendarEntries(String userId, Long startTime, Long endTime) {
		// TODO : Indexing in mongo
		Long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).gte(startTimeDay).lte(endTimeDay));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(userId));
		query.with(Sort.by(Direction.ASC, CalendarEntry.Constants.DAY_START_TIME));
		List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		return results;
	}

	public List<CalendarEntry> getCalendarEntries(String userId, List<Long> dayStartTimes) {
		logger.info("userId : " + userId + " dayStartTimes : " + Arrays.toString(dayStartTimes.toArray()));
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).in(dayStartTimes));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(userId));
		query.with(Sort.by(Direction.ASC, CalendarEntry.Constants.DAY_START_TIME));
		List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		return results;
	}

	public List<CalendarEntry> getCalendarEntries(List<String> userIds, List<Long> dayStartTimes) {
		logger.info("userId : " + Arrays.toString(userIds.toArray()) + " dayStartTimes : "
				+ Arrays.toString(dayStartTimes.toArray()));
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).in(dayStartTimes));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(userIds));
		query.with(Sort.by(Direction.ASC, CalendarEntry.Constants.DAY_START_TIME));
		List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		return results;
	}

	public Map<Long, CalendarEntry> getCalendarEntryMap(List<CalendarEntry> calendarEntries) {
		Map<Long, CalendarEntry> calendarEntryMap = new HashMap<>();
		for (CalendarEntry calendarEntry : calendarEntries) {
			calendarEntryMap.put(calendarEntry.getDayStartTime(), calendarEntry);
		}
		return calendarEntryMap;
	}

	public BitSet createSlotBitSet(SessionSchedule schedule) {
		BitSet bitSet = BitSetUtils.createSlotBitSet(schedule);
		schedule.setSlotBitSet(bitSet.toLongArray());
		return bitSet;
	}

	public List<CalendarEntry> getLastMarkedEntry(String userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(userId));
		query.addCriteria(
				Criteria.where(CalendarEntry.Constants.AVAILABILITY).in(CalendarUtils.createArrayList(0, 95)));
		query.with(Sort.by(Sort.Direction.DESC, CalendarEntry.Constants.DAY_START_TIME));
		query.limit(1);
		List<CalendarEntry> calendarEntries = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		if (calendarEntries != null && !calendarEntries.isEmpty()) {
			return calendarEntries;
		}

		return null;
	}

	public Long getLastMarkedTime(String userId) {
		List<CalendarEntry> calendarEntries = getLastMarkedEntry(userId);
		Long lastMarkedTime = null;
		if (calendarEntries != null && !calendarEntries.isEmpty()) {
			lastMarkedTime = calendarEntries.get(0).getDayStartTime();
		}

		return lastMarkedTime;
	}

	public Long getLastMarkedWeekStartTime(String userId) {
		Long lastMarkedTime = getLastMarkedTime(userId);
		if (lastMarkedTime != null
				&& lastMarkedTime >= CommonCalendarUtils.getDayStartTime(System.currentTimeMillis())) {
			lastMarkedTime = CommonCalendarUtils.getWeekStartTime(lastMarkedTime);
		} else {
			lastMarkedTime = null;
		}

		return lastMarkedTime;
	}

	public List<Long> getModifiedDayStartTimes(BitSetEntry bitSetEntry) {
		List<Long> dayStartTimes = new ArrayList<Long>();
		Long startTime = CommonCalendarUtils.getDayStartTime(bitSetEntry.getStartTime());
		BitSet bitSet = bitSetEntry.getBitSet();
		for (Long i = startTime; i < bitSetEntry.getEndTime(); i += CommonCalendarUtils.MILLIS_PER_DAY) {
			int index = bitSetEntry.getBitSetIndex(i);
			if ((index < -CalendarEntryManager.SLOT_COUNT) || (index > BitSetUtils.bitSetSize(bitSetEntry))) {
				continue;
			}
			if (index < 0) {
				index = 0;
			}
			int nextModifiedBit = bitSet.nextSetBit(index);
			if ((nextModifiedBit >= 0) && (nextModifiedBit < index + CalendarEntryManager.SLOT_COUNT)) {
				dayStartTimes.add(i);
			}
		}

		return dayStartTimes;
	}
}
