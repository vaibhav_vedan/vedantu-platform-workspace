package com.vedantu.scheduling.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.internal.bulk.WriteRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.entity.*;
import com.vedantu.scheduling.dao.serializers.*;
import com.vedantu.scheduling.pojo.*;
import com.vedantu.scheduling.pojo.calendar.*;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.CreateAvailabilityRangesRequest;
import com.vedantu.scheduling.request.GetCalendarSlotBitReq;
import com.vedantu.scheduling.request.UpdateCalendarSlotBitsReq;
import com.vedantu.scheduling.request.session.UpdateMultipleSlotsReq;
import com.vedantu.scheduling.response.BaseResponse;
import com.vedantu.scheduling.response.*;
import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.subscription.request.BlockSlotScheduleReq;
import com.vedantu.util.*;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.scheduling.SyncAllCalendarReq;
import com.vedantu.util.scheduling.SyncCalendarSessionsReq;
import com.vedantu.util.subscription.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CalendarEntryManager {

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private SessionDAO sessionDAO;
        
	@Autowired
	private OTFSessionDAO otfSessionDAO;

	@Autowired
	private CalendarEntryDAO calendarEntryDAO;

	@Autowired
	private CalendarUtilsManager calendarUtilsManager;

	@Autowired
	private CalendarEventManager calendarEventManager;

	@Autowired
	private CalendarBlockEntryManager calendarBlockEntryManager;

	@Autowired
	private CalendarSelectionManager calendarSelectionManager;

	@Autowired
	private AvailabilityRangeDAO availabilityRangeDAO;

	@Autowired
	private OverBookingDAO overBookingDAO;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEntryManager.class);

	public static final long SLOT_LENGTH = 15 * CommonCalendarUtils.MILLIS_PER_MINUTE;
	public static final int SLOT_COUNT = (int) (CommonCalendarUtils.MILLIS_PER_DAY / SLOT_LENGTH);
	public static final int REQ_MAX_BATCH_SIZE = 100;
	private static final int UPDATE_HISTORY_MAX_LEN = 15;

	private static Gson gson = new Gson();

	private static final Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
	}.getType();

	public List<CalendarEntry> upsertCalendarEntry(CalendarEntryReq calendarEntryReq)
			throws NotFoundException, ConflictException {
		logger.info("upsertCalendarEntry : " + calendarEntryReq.toString());
		Long startTimeDay = CommonCalendarUtils.getDayStartTime(calendarEntryReq.getStartTime());
		Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(calendarEntryReq.getEndTime());

		List<CalendarEntry> response = new ArrayList<CalendarEntry>();
		for (long i = startTimeDay; i <= endTimeDay; i = i + CommonCalendarUtils.MILLIS_PER_DAY) {
			Query query = new Query();
			query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(calendarEntryReq.getUserId()));

			List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);
			CalendarEntry calendarEntry = null;
			if (!CollectionUtils.isEmpty(results)) {
				calendarEntry = results.get(0);
			} else {
				calendarEntry = new CalendarEntry(calendarEntryReq.getUserId(), i);
			}

			// Add slot entries
			int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(calendarEntryReq.getStartTime(), i);
			int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(calendarEntryReq.getEndTime(), i);
			boolean updated = calendarEntry.addSlotEntries(calendarEntryReq.getState(), slotStartIndex, slotEndIndex);

			// Add reference id in calendar entry
			if (updated) {
				CalendarUpdateDetails calendarUpdateDetails = new CalendarUpdateDetails(calendarEntryReq.getState(),
						calendarEntryReq.getStartTime(), calendarEntryReq.getEndTime(), false,
						calendarEntryReq.getReferenceType(), calendarEntryReq.getReferenceId());

				List<CalendarUpdateDetails> sortedUpdatedHistory = calendarEntry.getUpdateHistory();
				if (!CollectionUtils.isEmpty(sortedUpdatedHistory) && sortedUpdatedHistory.size() > UPDATE_HISTORY_MAX_LEN) {
					sortedUpdatedHistory = Optional.ofNullable(calendarEntry.getUpdateHistory())
							.orElseGet(ArrayList::new)
							.stream()
							.sorted(Comparator.comparingLong(CalendarUpdateDetails::getUpdateTime).reversed())
							.limit(UPDATE_HISTORY_MAX_LEN - 1)
							.collect(Collectors.toList());
					calendarEntry.setUpdateHistory(sortedUpdatedHistory);
				}
				calendarEntry.addUpdateHistoryEntry(calendarUpdateDetails);
			}

			// Update the calendar entry in db
			calendarEntryDAO.save(calendarEntry);
			response.add(calendarEntry);
		}

		return response;
	}

	public List<CalendarEntry> upsertCalendarEntry(List<CalendarEntryReq> calendarEntryReqList) {
		List<CalendarEntry> response = new ArrayList<>();
		List<String> errors = new ArrayList<>();
		if (!CollectionUtils.isEmpty(calendarEntryReqList)) {
			logger.info("Request size:" + calendarEntryReqList.size());
			for (CalendarEntryReq calendarEntryReq : calendarEntryReqList) {
				try {
					List<CalendarEntry> results = upsertCalendarEntry(calendarEntryReq);
					if (!CollectionUtils.isEmpty(results)) {
						logger.info("Results:" + results.size());
						response.addAll(results);
					}
				} catch (Exception ex) {
					String errorMessage = "Error occured updating the calendar : " + calendarEntryReq.toString();
					logger.info(errorMessage);
					errors.add(errorMessage);
				}
			}
		}

		if (!CollectionUtils.isEmpty(errors)) {
			logger.error(Arrays.toString(errors.toArray()));
		}

		return response;
	}

	private List<CalendarEntry> fetchCalendarEntriesForDays(List<CalendarEntryReq> reqs) {
		Map<Long, List<String>> dayStartTimeUserSets = new HashMap<>();
		List<CalendarEntry> fetchedCalEntries = new ArrayList<>();
		List<CalendarEntry> fetch;

		for (CalendarEntryReq req : reqs) {
			for (long dst = req.getFirstDayStart(); dst <= req.getFinalDayEnd(); dst = dst + CommonCalendarUtils.MILLIS_PER_DAY) {
				if (!dayStartTimeUserSets.containsKey(dst)) {
					dayStartTimeUserSets.put(dst, new ArrayList<>());
				}
				dayStartTimeUserSets.get(dst).add(req.getUserId());
			}
		}

		for (long dst : dayStartTimeUserSets.keySet()) {
			Query q = new Query();
			q.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(dst));
			q.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(dayStartTimeUserSets.get(dst)));
			fetch = calendarEntryDAO.runQuery(q, CalendarEntry.class);
			if (!CollectionUtils.isEmpty(fetch)) {
				fetchedCalEntries.addAll(fetch);
			}
		}

		return fetchedCalEntries;
	}

	private List<CalendarEntry>  fetchCalendarEntriesForUsers(List<CalendarEntryReq> reqs) {
		Map<String, List<Long>> userDayStartTimeSets = new HashMap<>();
		List<CalendarEntry> fetchedCalEntries = new ArrayList<>();
		List<CalendarEntry> fetch;

		for (CalendarEntryReq req : reqs) {
			for (long dst = req.getFirstDayStart(); dst <= req.getFinalDayEnd(); dst = dst + CommonCalendarUtils.MILLIS_PER_DAY) {
				if (!userDayStartTimeSets.containsKey(req.getUserId())) {
					userDayStartTimeSets.put(req.getUserId(), new ArrayList<>());
				}
				userDayStartTimeSets.get(req.getUserId()).add(dst);
			}
		}

		for (String userId : userDayStartTimeSets.keySet()) {
			Query q = new Query();
			q.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(userId));
			q.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).in(userDayStartTimeSets.get(userId)));
			fetch = calendarEntryDAO.runQuery(q, CalendarEntry.class);
			if (!CollectionUtils.isEmpty(fetch)) {
				fetchedCalEntries.addAll(fetch);
			}
		}

		return fetchedCalEntries;
	}

	public BasicRes upsertCalendarEntriesBulk(List<CalendarEntryReq> reqs, String callingUserId) {
		logger.info("UPSERT CALENDAR ENTRIES BULK STARTED");
		if (reqs.size() > 500) {
			logger.error("Processing upsert calendar entry request of size > 500");
		}

		Set<Long> dayStartTimes = new HashSet<>();
		Set<String> userIds = new HashSet<>();
		// map <userId_dst, <operationType, calendarEntry>>
		Map<String, Pair<WriteRequest.Type, CalendarEntry>> calEntryMap = new HashMap<>();
		CalendarEntry calendarEntry;

		// collect userIds and dayStartTimes
		// prepare ops with slots to mark
		for (CalendarEntryReq req : reqs) {
			userIds.add(req.getUserId());
			req.setFirstDayStart(CommonCalendarUtils.getDayStartTime(req.getStartTime()));
			req.setFinalDayEnd(CommonCalendarUtils.getDayStartTime_end(req.getEndTime()));

			for (long dst = req.getFirstDayStart(); dst <= req.getFinalDayEnd(); dst += CommonCalendarUtils.MILLIS_PER_DAY) {
				String key = req.getUserId() + "_" + dst;
				dayStartTimes.add(dst);

				if (!calEntryMap.containsKey(key)) {
					calendarEntry = new CalendarEntry(req.getUserId(), dst);
				} else {
					calendarEntry = calEntryMap.get(key).getSecond();
				}

				// Add slot entries
				int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(req.getStartTime(), dst);
				int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(req.getEndTime(), dst);
				boolean updated = calendarEntry.addSlotEntries(req.getState(), slotStartIndex, slotEndIndex);

				logger.info("dst " + dst + " slotStartIndex " + slotStartIndex + " endIndex " + slotEndIndex);

				// Add reference id in calendar entry
				if (updated) {
					CalendarUpdateDetails calendarUpdateDetails = new CalendarUpdateDetails(req.getState(),
							req.getStartTime(), req.getEndTime(), false,
							req.getReferenceType(), req.getReferenceId());
					calendarEntry.addUpdateHistoryEntry(calendarUpdateDetails);
				}
				calEntryMap.put(key, Pair.of(WriteRequest.Type.INSERT, calendarEntry));
			}
		}

		// fetch existing cal entries
		List<CalendarEntry> fetchedCalendarEntries;
		if (dayStartTimes.size() <= userIds.size()) {
			fetchedCalendarEntries = fetchCalendarEntriesForDays(reqs);
		} else {
			fetchedCalendarEntries = fetchCalendarEntriesForUsers(reqs);
		}
		for (CalendarEntry fetchedEntry : fetchedCalendarEntries) {
			 String key = fetchedEntry.getUserId() + "_" + fetchedEntry.getDayStartTime();
			 calendarEntry = calEntryMap.get(key).getSecond();
			 calendarEntry.getBooked().addAll(fetchedEntry.getBooked());

			 List<CalendarUpdateDetails> sortedUpdatedHistory = fetchedEntry.getUpdateHistory();
			 if (!CollectionUtils.isEmpty(sortedUpdatedHistory) && sortedUpdatedHistory.size() > UPDATE_HISTORY_MAX_LEN) {
				 sortedUpdatedHistory = Optional.of(sortedUpdatedHistory)
						 .orElseGet(ArrayList::new)
						 .stream()
						 .sorted(Comparator.comparingLong(CalendarUpdateDetails::getUpdateTime).reversed())
						 .limit(UPDATE_HISTORY_MAX_LEN - 1)
						 .collect(Collectors.toList());
			 }

			 calendarEntry.getUpdateHistory().addAll(sortedUpdatedHistory);
			 calendarEntry.getSessionRequest().addAll(fetchedEntry.getSessionRequest());
			 calendarEntry.getAvailability().addAll(fetchedEntry.getAvailability());
			 calEntryMap.put(key, Pair.of(WriteRequest.Type.UPDATE, calendarEntry));
		}

		// for bulk write ops
		List<CalendarEntry> inserts = new ArrayList<>();
		List<Document> queries = new ArrayList<>();
		List<Document> updates = new ArrayList<>();

		for (Pair<WriteRequest.Type, CalendarEntry> pair : calEntryMap.values()) {
			CalendarEntry ce = pair.getSecond();

			// segregate inserts and updates
			if (WriteRequest.Type.INSERT.equals(pair.getFirst())) {
				inserts.add(pair.getSecond());
			} else {
				Document queryDoc = new Document();
				queryDoc.append(CalendarEntry.Constants.USER_ID, ce.getUserId());
				queryDoc.append(CalendarEntry.Constants.DAY_START_TIME, ce.getDayStartTime());
				Document updateDoc = new Document();
				Document setDoc = new Document();
				setDoc.append(CalendarEntry.Constants.USER_ID, ce.getUserId());
				setDoc.append(CalendarEntry.Constants.DAY_START_TIME, ce.getDayStartTime());
				setDoc.append(CalendarEntry.Constants.BOOKED, ce.getBooked());
				setDoc.append(CalendarEntry.Constants.AVAILABILITY, ce.getAvailability());
				setDoc.append(CalendarEntry.Constants.SESSION_REQUEST, ce.getSessionRequest());
				setDoc.append(CalendarEntry.Constants.UPDATE_HISTORY, ce.getUpdateHistory());
				updateDoc.append(CalendarEntry.Constants.UPDATE_SET_PARAM, setDoc);
				queries.add(queryDoc);
				updates.add(updateDoc);
			}
		}

		if (!CollectionUtils.isEmpty(inserts)) {
			logger.info("inserting calendar entries in bulk " + inserts.size());
			long insertedCount = calendarEntryDAO.bulkInsert(inserts, CalendarEntry.class, callingUserId);
			logger.info("inserted calendar entries in bulk " + insertedCount);
		}

		if (!CollectionUtils.isEmpty(updates)) {
			logger.info("updating calendar entries in bulk " + updates.size());
			long modifiedCount = calendarEntryDAO.bulkUpdate(queries, updates,false, CalendarEntry.class, callingUserId);
			logger.info("updating calendar entries in bulk " + modifiedCount);
		}

		logger.info("UPSERT CALENDAR ENTRIES BULK FINISHED");
		return new BasicRes();
	}


	public BasicRes removeCalendarEntrySlotsUsingUserId(CalendarEntryReq calendarEntryReq) throws NotFoundException {
		BasicRes response = new BasicRes();
		List<CalendarEntry> calendarEntries = calendarUtilsManager.getCalendarEntries(calendarEntryReq.getUserId(),
				calendarEntryReq.getStartTime(), calendarEntryReq.getEndTime());
		if (CollectionUtils.isEmpty(calendarEntries)) {
			return response;
		}

		long start;
		long end;
		for (CalendarEntry calendarEntry : calendarEntries) {
			start = calendarEntryReq.getStartTime();
			end = calendarEntryReq.getEndTime();
			if (calendarEntry.getDayStartTime() >= calendarEntryReq.getStartTime()) {
				start = calendarEntry.getDayStartTime();
			}
			if (calendarEntryReq.getEndTime() >= calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY) {
				end = calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY;
			}

			RemoveCalendarEntryReq removeCalendarEntryReq = new RemoveCalendarEntryReq(calendarEntry.getId(), start,
					end, calendarEntryReq.getState(), calendarEntryReq.getReferenceType(),
					calendarEntryReq.getReferenceId());
			BasicRes resp = removeCalendarEntrySlots(removeCalendarEntryReq);
			if (!resp.getSuccess()) {
				resp.setSuccess(Boolean.FALSE);
			}
		}
		return response;
	}

	public BasicRes removeCalendarEntrySlots(RemoveCalendarEntryReq removeCalendarEntryReq) throws NotFoundException {
		logger.info("calendar entry to remove slots:" + removeCalendarEntryReq.toString());
		CalendarEntry calendarEntry = calendarEntryDAO.getById(removeCalendarEntryReq.getCalendarEntryId());
		if (calendarEntry == null) {
			throw new NotFoundException(ErrorCode.CALENDAR_ENTRY_NOT_FOUND, "calendar Entry not present");
		}

		int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(removeCalendarEntryReq.getStartTime(),
				calendarEntry.getDayStartTime());
		int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(removeCalendarEntryReq.getEndTime(),
				calendarEntry.getDayStartTime());
		int SLOT_COUNT = CommonCalendarUtils.getCalendarEntrySlotCount();
		if (slotStartIndex < 0 || slotStartIndex >= SLOT_COUNT || slotEndIndex < 0 || slotEndIndex >= SLOT_COUNT) {
			// throw exception : Invalid request
			logger.error("slot index is out of bounds id:" + removeCalendarEntryReq.toString());
		}

		// Remove the slots
		boolean updated = calendarEntry.removeSlotEntries(removeCalendarEntryReq.getSlotState(), slotStartIndex,
				slotEndIndex);

		// Add reference id in calendar entry
		if (updated) {
			CalendarUpdateDetails calendarUpdateDetails = new CalendarUpdateDetails(
					removeCalendarEntryReq.getSlotState(), removeCalendarEntryReq.getStartTime(),
					removeCalendarEntryReq.getEndTime(), true, removeCalendarEntryReq.getReferenceType(),
					removeCalendarEntryReq.getReferenceId());
			calendarEntry.addUpdateHistoryEntry(calendarUpdateDetails);
		}

		calendarEntryDAO.save(calendarEntry);
		return new BasicRes();
	}

	public PlatformBasicResponse unmarkCalendarEntriesBulk(final List<CalendarEntryReq> reqs, String callingUserId) {
		logger.info("unmark calendar entries in bulk-- start");
		if (reqs.size() > 500) {
			logger.error("Unmarking more than 500 calendar entries in bulk");
		}

		// get sets of users and days to fetch docs in a single query
		Set<String> userIds = new HashSet<>();
		Set<Long> dayStartTimes = new HashSet<>();
		for (CalendarEntryReq req : reqs) {
			userIds.add(req.getUserId());
			req.setFirstDayStart(CommonCalendarUtils.getDayStartTime(req.getStartTime()));
			req.setFinalDayEnd(CommonCalendarUtils.getDayStartTime_end(req.getEndTime()));

			for (long dst = req.getFirstDayStart(); dst <= req.getFinalDayEnd(); dst += CommonCalendarUtils.MILLIS_PER_DAY) {
				dayStartTimes.add(dst);
			}
		}

		List<CalendarEntry> fetchedCalendarEntries;
		if (dayStartTimes.size() <= userIds.size()) {
			fetchedCalendarEntries = fetchCalendarEntriesForDays(reqs);
		} else {
			fetchedCalendarEntries = fetchCalendarEntriesForUsers(reqs);
		}

		Map<String, CalendarEntry> fetchedCalendarEntriesMap = Optional.of(fetchedCalendarEntries)
				.orElseGet(ArrayList::new)
				.stream()
				.filter(Objects::nonNull)
				.collect(Collectors.toMap(ce -> ce.getUserId() + "_" + ce.getDayStartTime(), ce -> ce));

		List<Document> queries = new ArrayList<>();
		List<Document> updates = new ArrayList<>();
		for (CalendarEntryReq req : reqs) {
			for (long dst = req.getFirstDayStart(); dst <= req.getFinalDayEnd(); dst += CommonCalendarUtils.MILLIS_PER_DAY) {
				String key = req.getUserId() + "_" + dst;
				CalendarEntry fetchedCalendarEntry = fetchedCalendarEntriesMap.get(key);
				if (Objects.isNull(fetchedCalendarEntry)) {
					logger.warn("No calendar entry found to unmark userId: {} dst: {}", req.getUserId(), dst);
					continue;
				}
				int slotStartIndex = Math.max(CommonCalendarUtils.getSlotStartIndex(req.getStartTime(), dst),
						CommonCalendarUtils.getCalendarEntryMinIndex());
				int slotEndIndex = Math.min(CommonCalendarUtils.getSlotEndIndex(req.getEndTime(), dst),
						CommonCalendarUtils.getCalendarEntryMaxIndex());
				boolean updated = fetchedCalendarEntry.removeSlotEntries(req.getState(), slotStartIndex, slotEndIndex);

				// Add updateHistory
				if (updated) {
					CalendarUpdateDetails calendarUpdateDetails = new CalendarUpdateDetails(
							req.getState(), req.getStartTime(), req.getEndTime(), true,
							req.getReferenceType(), req.getReferenceId());
					fetchedCalendarEntry.addUpdateHistoryEntry(calendarUpdateDetails);
				}

				Document queryDoc = new Document();
				queryDoc.append(CalendarEntry.Constants.USER_ID, fetchedCalendarEntry.getUserId());
				queryDoc.append(CalendarEntry.Constants.DAY_START_TIME, fetchedCalendarEntry.getDayStartTime());

				Document updateDoc = new Document();
				Document setDoc = new Document();
				setDoc.append(CalendarEntry.Constants.BOOKED, fetchedCalendarEntry.getBooked());
				setDoc.append(CalendarEntry.Constants.SESSION_REQUEST, fetchedCalendarEntry.getSessionRequest());
				setDoc.append(CalendarEntry.Constants.UPDATE_HISTORY, fetchedCalendarEntry.getUpdateHistory());
				updateDoc.append(CalendarEntry.Constants.UPDATE_SET_PARAM, setDoc);
				queries.add(queryDoc);
				updates.add(updateDoc);
			}
		}


		if (!CollectionUtils.isEmpty(updates)) {
			logger.info("updating calendar entries in bulk " + updates.size());
			long modifiedCount = calendarEntryDAO.bulkUpdate(queries, updates, false, CalendarEntry.class, callingUserId);
			logger.info("updating calendar entries in bulk " + modifiedCount);
		}

		return new PlatformBasicResponse();
	}

	public List<String> getAvailableUsers(Long startTime, Long endTime, List<String> ids) {

		List<String> availableUsers = new ArrayList<>();
		if (CollectionUtils.isEmpty(ids)) {
			return availableUsers;
		}
		logger.info("list of ids " + ids.toString());
		long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);


		for (long dst = startTimeDay; dst <= endTimeDay; dst += DateTimeUtils.MILLIS_PER_DAY) {
			if (!availableUsers.isEmpty()) {
				ids = new ArrayList<>();
				ids.addAll(availableUsers);
				availableUsers = new ArrayList<>();
			}


			int slotStartIndex = Math.max(CommonCalendarUtils.getSlotStartIndex(startTime, dst), CommonCalendarUtils.getCalendarEntryMinIndex());
			int slotEndIndex = Math.min(CommonCalendarUtils.getSlotEndIndex(endTime, dst), CommonCalendarUtils.getCalendarEntryMaxIndex());
			List<CalendarEntry> results = calendarEntryDAO.getTeachersAvailabilityInOneHourSlot(dst, slotStartIndex, slotEndIndex, ids);

			logger.info("results : " + results == null ? 0 : results.size());
			if (results != null && results.size() > 0) {
				if (!CollectionUtils.isEmpty(results)) {
					logger.info("list of users available is " + results.toString());
					for (CalendarEntry result : results) {

						if (!availableUsers.contains(result.getUserId())) {
							availableUsers.add(result.getUserId());
						}
					}
				}

				if (availableUsers.isEmpty()) {
					break;
				}

				logger.info("get available users end Respone: " + availableUsers.toString());
			}
		}
		return availableUsers;
	}

	public List<CalendarEntry> getAvilableTeachersForOverBooking(Long startTime, Long endTime, List<String> ids) {

		List<CalendarEntry> availableUsers = new ArrayList<>();
		if (CollectionUtils.isEmpty(ids)) {
			return availableUsers;
		}
		logger.info("list of ids " + ids.toString());
		long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);


		for (long dst = startTimeDay; dst <= endTimeDay; dst += DateTimeUtils.MILLIS_PER_DAY) {

			List<String> avilableIds = new ArrayList<>();
			avilableIds.addAll(availableUsers.stream().filter(Objects::nonNull).map(CalendarEntry::getUserId).collect(Collectors.toList()));
			int slotStartIndex = Math.max(CommonCalendarUtils.getSlotStartIndex(startTime, dst), CommonCalendarUtils.getCalendarEntryMinIndex());
			int slotEndIndex = Math.min(CommonCalendarUtils.getSlotEndIndex(endTime, dst), CommonCalendarUtils.getCalendarEntryMaxIndex());
			List<CalendarEntry> results = calendarEntryDAO.getTeachersAvailabilityInOneHourSlot(dst, slotStartIndex, slotEndIndex, ids);

			logger.info("results : " + results == null ? 0 : results.size());
			if (!CollectionUtils.isEmpty(results)) {
				logger.info("list of users available is " + results.toString());
				for (CalendarEntry result : results) {
					if (!avilableIds.contains(result.getUserId())) {
						availableUsers.add(result);
					}
				}
				if (availableUsers.isEmpty()) {
					break;
				}
				logger.info("get available users end Respone: " + availableUsers.toString());
			}
		}
		return availableUsers;
	}

	public List<String> getFutureAvailableTeachers(Long startTime, Long endTime, List<String> ids,
			Boolean slotIsConflicted) throws ForbiddenException {
		logger.info("get available users start");
		logger.info("list of ids " + ids.toString());
		Long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);

		logger.info("start time and end time in same day");
		List<String> userIds = new ArrayList<String>();

		for (long i = startTimeDay; i <= endTimeDay; i += DateTimeUtils.MILLIS_PER_DAY) {
			if (!userIds.isEmpty()) {
				ids = new ArrayList<String>();
				ids.addAll(userIds);
				userIds = new ArrayList<String>();
			}

			int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(startTime, i);
			int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(endTime, i);
			if (slotStartIndex < 0) {
				slotStartIndex = 0;
			}

			if (slotEndIndex >= CommonCalendarUtils.getCalendarEntrySlotCount()) {
				slotEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
			}
			Query query = new Query();
			if (ids != null && !ids.isEmpty()) {
				query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
			}
			query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.AVAILABILITY)
					.all(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));
			List<CalendarEntry> existingEntries = calendarEntryDAO.runQuery(query, CalendarEntry.class);
			query.addCriteria(Criteria.where(CalendarEntry.Constants.BOOKED)
					.nin(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));
			if (slotIsConflicted.booleanValue() == false)
				query.addCriteria(Criteria.where(CalendarEntry.Constants.SESSION_REQUEST)
						.nin(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));

			logger.info("query is " + query.toString());
			logger.info("day start time for teacher availability is " + i);
			logger.info("created list " + CalendarUtils.createArrayList(slotStartIndex, slotEndIndex).toString());
			List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);
			logger.info("results : " + results == null ? 0 : results.size());

			if (existingEntries.size() == 0) {
				userIds.addAll(ids);

			}
			if (results != null && results.size() > 0) {
				logger.info("list of users available is " + results.toString());
				for (CalendarEntry result : results) {
					if (!userIds.contains(result.getUserId())) {
						userIds.add(result.getUserId());
					}

				}
			}

			if (userIds.isEmpty()) {
				break;
			}
			logger.info("get available users end Respone: " + userIds.toString());
		}
		return userIds;
	}

	public List<String> getAvailableStudents(Long startTime, Long endTime, List<String> ids, Boolean slotConflict)
			throws ForbiddenException {
		logger.info("get available users start");
		logger.info("list of ids " + ids.toString());
		Long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);

		logger.info("start time and end time in same day");
		List<String> userIds = new ArrayList<String>();
		if (ids == null || ids.isEmpty()) {
			return userIds;
		}

		for (long i = startTimeDay; i <= endTimeDay; i += DateTimeUtils.MILLIS_PER_DAY) {
			if (!userIds.isEmpty()) {
				ids = new ArrayList<String>();
				ids.addAll(userIds);
				userIds = new ArrayList<String>();
			}

			int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(startTime, i);
			int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(endTime, i);
			if (slotStartIndex < 0) {
				slotStartIndex = 0;
			}

			if (slotEndIndex >= CommonCalendarUtils.getCalendarEntrySlotCount()) {
				slotEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
			}
			Query query = new Query();
			query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.BOOKED)
					.in(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));
			List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);

			if (slotConflict.booleanValue() == false) {
				Query requestQuery = new Query();
				requestQuery.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
				requestQuery.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(i));
				requestQuery.addCriteria(Criteria.where(CalendarEntry.Constants.SESSION_REQUEST)
						.in(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));
				List<CalendarEntry> resultsForRequest = calendarEntryDAO.runQuery(requestQuery, CalendarEntry.class);
				results.addAll(resultsForRequest);
			}
			logger.info("day start time for student availability is " + i);
			logger.info("query : " + query);
			logger.info("results : " + results == null ? 0 : results.size());
			if (results != null && results.size() > 0) {
				logger.info("list of users available is " + results.toString());
				for (CalendarEntry result : results) {
					if (ids.contains(result.getUserId())) {
						ids.remove(result.getUserId());
					}
				}
			}

			if (ids.isEmpty()) {
				break;
			} else {
				logger.info("available users loop : " + ids);
				userIds.addAll(ids);
			}
			logger.info("get available users end Respone: " + ids.toString());
		}
		return userIds;
	}

	public List<String> getAvailableTeachers(Long startTime, Long endTime, List<String> ids) {
		List<String> results = new ArrayList<String>();
		if (CollectionUtils.isEmpty(ids)) {
			return results;
		}

		Long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).gte(startTimeDay).lte(endTimeDay));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(ids));
		query.addCriteria(
				Criteria.where(CalendarEntry.Constants.AVAILABILITY).in(CalendarUtils.createArrayList(0, SLOT_COUNT)));
		List<CalendarEntry> calendarEntries = calendarEntryDAO.runQuery(query, CalendarEntry.class);

		if (calendarEntries != null) {
			for (CalendarEntry calendarEntry : calendarEntries) {

				// validate if the slots fall between startTime and endTime
				if (calendarEntry.getDayStartTime() < startTime) {
					int startIndex = CommonCalendarUtils.getSlotStartIndex(startTime, calendarEntry.getDayStartTime());
					if (startIndex < 0 || startIndex >= SLOT_COUNT) {
						continue;
					}

					Set<Integer> availability = calendarEntry.getAvailability();
					boolean valid = false;
					for (int i = startIndex; i < SLOT_COUNT; i++) {
						if (availability.contains(Integer.valueOf(i))) {
							valid = true;
							break;
						}
					}

					if (!valid) {
						continue;
					}
				}

				if ((calendarEntry.getDayStartTime() + CommonCalendarUtils.MILLIS_PER_DAY) > endTime) {
					int endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, calendarEntry.getDayStartTime());
					if (endIndex < 0 || endIndex >= SLOT_COUNT) {
						continue;
					}

					Set<Integer> availability = calendarEntry.getAvailability();
					boolean valid = false;
					for (int i = 0; i <= endIndex; i++) {
						if (availability.contains(Integer.valueOf(i))) {
							valid = true;
							break;
						}
					}

					if (!valid) {
						continue;
					}
				}

				if (!results.contains(calendarEntry.getUserId())) {
					results.add(calendarEntry.getUserId());
				}
			}
		}

		return results;
	}

	public List<String> getUsersWithFreeSlots(Long startTime, Long endTime, long duration) throws ForbiddenException {

		int slotlength;
		slotlength = (int) (duration / CommonCalendarUtils.getCalendarEntrySlotCount());
		if (duration % CommonCalendarUtils.getCalendarEntrySlotCount() != 0) {
			slotlength += 1;
		}
		Long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		List<String> userIds = new ArrayList<>();

		int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(startTime, startTimeDay);
		int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(endTime, startTimeDay);
		if (slotEndIndex > CommonCalendarUtils.getCalendarEntrySlotCount()) {
			throw new ForbiddenException(ErrorCode.TIME_EXCEEDED_DAY,
					"Interval exceeds duration for available teachers");
		}

		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(startTimeDay));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.AVAILABILITY)
				.in(CalendarUtils.createArrayList(slotStartIndex, slotStartIndex + slotlength - 1)));
		Set<Integer> slots = CalendarUtils.createArrayList(slotStartIndex, slotStartIndex + slotlength - 1);
		List<CalendarEntry> calendarEntries = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		while ((slotStartIndex + slotlength - 1) <= slotEndIndex && calendarEntries.size() > 0) {

			for (CalendarEntry calendarEntry : calendarEntries) {

				if (calendarEntry.getAvailability().containsAll(slots)) {
					userIds.add(calendarEntry.getUserId());
					calendarEntries.remove(calendarEntry);
				}

				if (calendarEntries.size() == 0)
					break;
				slots.remove(slotStartIndex);
				slots.add(slotStartIndex + slotlength);
				slotStartIndex += 1;
			}
		}

		return userIds;
	}

	public List<SessionSlot> getSlotListAvailability(List<SessionSlot> slots, String teacherId, String studentId)
			throws ForbiddenException {
		logger.info("getSlotListAvailability : " + slots + " teacherId : " + teacherId + " studentId : " + studentId);
		List<SessionSlot> results = new ArrayList<SessionSlot>();
		if (slots == null || slots.isEmpty()) {
			return results;
		}

		List<CalendarEntry> lastMarkedEntryList = calendarUtilsManager.getLastMarkedEntry(teacherId);
		logger.info("lastMarkedCalendarEntry : " + lastMarkedEntryList);
		if (lastMarkedEntryList == null
				|| (lastMarkedEntryList.get(0).getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY) < System
						.currentTimeMillis()) {
			return results;
		}

		CalendarEntry lastMarkedEntry = lastMarkedEntryList.get(0);
		try {
			Set<Long> dayStartTimes = new HashSet<Long>();
			for (SessionSlot slot : slots) {
				dayStartTimes.add(CommonCalendarUtils.getDayStartTime(slot.getStartTime()));
				dayStartTimes.add(CommonCalendarUtils.getDayStartTime(slot.getEndTime()));
			}
			logger.info("fetch day start times : " + dayStartTimes.toString());

			Query query = new Query();
			List<String> userIds = new ArrayList<>();
			if (!StringUtils.isEmpty(teacherId)) {
				userIds.add(teacherId);
			}
			if (!StringUtils.isEmpty(studentId)) {
				userIds.add(studentId);
			}

			query.addCriteria(
					Criteria.where(CalendarEntry.Constants.DAY_START_TIME).in(new ArrayList<>(dayStartTimes)));
			query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(userIds));
			List<CalendarEntry> calendarEntries = calendarEntryDAO.runQuery(query, CalendarEntry.class);
			logger.info("fetched calendar entries : " + calendarEntries);
			Set<Integer> defaultAvailability = CalendarUtils.createArrayList(0, 95);
			Map<Long, Set<Integer>> teacherMap = new HashMap<>();
			Map<Long, Set<Integer>> studentMap = new HashMap<>();
			for (CalendarEntry calendarEntry : calendarEntries) {
				logger.info("processing calendar entry" + calendarEntry.toString());
				if (calendarEntry.getUserId().equals(teacherId)) {
					Set<Integer> calendarEntryAvailability = calendarEntry.getAvailability();
					if (calendarEntry.getDayStartTime() > lastMarkedEntry.getDayStartTime()) {
						calendarEntryAvailability = defaultAvailability;
					}

					logger.info("last marked day after calendar entry start time");
					Set<Integer> availableEntry;
					// for confirm proposal case only
					if (slots.size() == 1 && Boolean.TRUE.equals(slots.get(0).isConflicted))
						availableEntry = excludeUnavailableSlots(defaultAvailability, calendarEntry.getBooked(),
								new HashSet<>());
					else {
						availableEntry = excludeUnavailableSlots(calendarEntryAvailability, calendarEntry.getBooked(),
								calendarEntry.getSessionRequest());
					}
					teacherMap.put(calendarEntry.getDayStartTime(), availableEntry);
				} else if (calendarEntry.getUserId().equals(studentId)) {
					Set<Integer> availableEntry;
					if (slots.size() == 1 && Boolean.TRUE.equals(slots.get(0).isConflicted))
						availableEntry = excludeUnavailableSlots(defaultAvailability, calendarEntry.getBooked(),
								new HashSet<>());
					else
						availableEntry = excludeUnavailableSlots(defaultAvailability, calendarEntry.getBooked(),
								calendarEntry.getSessionRequest());
					studentMap.put(calendarEntry.getDayStartTime(), availableEntry);
				}
			}
			logger.info("teacher map for check slot :" + teacherMap.toString());
			logger.info("student map for check slot :" + studentMap.toString());

			for (SessionSlot slot : slots) {
				boolean match = true;
				Long startTimeDay = CommonCalendarUtils.getDayStartTime(slot.getStartTime());
				Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(slot.getEndTime());

				logger.info("slot : " + slot.toString() + " startTimeDay : " + startTimeDay + " endTimeDay : "
						+ endTimeDay);
				for (long i = startTimeDay; i <= endTimeDay; i += DateTimeUtils.MILLIS_PER_DAY) {
					Set<Integer> teacherAvailabilityEntry = new HashSet<>();
					if (teacherMap.containsKey(i))
						teacherAvailabilityEntry = teacherMap.get(i);
					Set<Integer> studentAvailabilityEntry = new HashSet<>();
					if (studentMap.containsKey(i))
						studentAvailabilityEntry = studentMap.get(i);

					int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(slot.getStartTime(), i);
					int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(slot.getEndTime(), i);
					logger.info("slot start index : " + slotStartIndex);
					logger.info("slot end index : " + slotEndIndex);
					if (slotStartIndex < 0) {
						slotStartIndex = 0;
					}

					if (slotEndIndex >= CommonCalendarUtils.getCalendarEntrySlotCount()) {
						slotEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
					}
					logger.info("teacher availability entry : " + teacherAvailabilityEntry);
					logger.info("student availability entry : " + studentAvailabilityEntry);
					for (int slotIndex = slotStartIndex; slotIndex <= slotEndIndex; slotIndex++) {
						if (teacherAvailabilityEntry != null && teacherAvailabilityEntry.size() == 0
								&& i <= lastMarkedEntry.getDayStartTime())
							match = false;
						if (teacherAvailabilityEntry != null && teacherAvailabilityEntry.size() > 0
								&& !teacherAvailabilityEntry.contains(slotIndex))
							match = false;
						if (studentAvailabilityEntry != null && studentAvailabilityEntry.size() > 0
								&& !studentAvailabilityEntry.contains(slotIndex))
							match = false;
					}

					if (!match) {
						logger.info("No slots available for scheduling for teacher: " + teacherId + " student: "
								+ studentId + " for slots from " + slot.getStartTime() + " to " + slot.getEndTime());
						break;
					}
				}
				if (match) {
					logger.info("getSlotListAvailabilityNoMatch : slot " + slot);
					results.add(slot);
				}
			}

		} catch (Exception e) {
			logger.error("slot list availability error : " + e.getMessage());

		}

		if (results.size() == 0 && slots.size() > 0 && !StringUtils.isEmpty(studentId)) {
			logger.info("No slots available for scheduling for teacher: " + teacherId + " student: " + studentId
					+ " for slots from " + slots.toString());
		}
		if ((results.size() > 0) && (slots.size() > 0) && ((results.size() / (double) slots.size()) < 0.8)
				&& !StringUtils.isEmpty(studentId)) {
			logger.info("80 percent scheduling not happening for teacher: " + teacherId + " student: " + studentId
					+ " scheduled slots size: " + results.size() + "total slots size: " + slots.size()
					+ " for slots from " + slots.toString());
		}
		if ((results.size() > 0) && (slots.size() > 0) && ((results.size() / (double) slots.size()) < 0.8)
				&& !StringUtils.isEmpty(studentId)) {
			logger.info("80 percent scheduling not happening for teacher: " + teacherId + " student: " + studentId
					+ " scheduled slots size: " + results.size() + "total slots size: " + slots.size()
					+ " for slots from " + slots.toString());
		}
		return results;
	}

	public AvailabilityResponse getTrueAvailability(Long startTime, Long endTime, String teacherId, String studentId,
			String neglectReferenceId, boolean useExtrapolation) {

		logger.info("getTrueAvailabilityRequest - startTime:" + startTime + " endTime:" + endTime + " teacherId:"
				+ teacherId + " studentId:" + studentId + " referenceId : " + neglectReferenceId);

		// Round off start time and end time to start of a slot
		startTime = CommonCalendarUtils.getSlotStartTime(startTime);
		endTime = CommonCalendarUtils.getSlotStartTime(endTime);

		List<CalendarEntry> teacherCalendarEntries = calendarUtilsManager.getCalendarEntries(teacherId, startTime,
				endTime);
		List<CalendarEntry> studentCalendarEntries = calendarUtilsManager.getCalendarEntries(studentId, startTime,
				endTime);
		Map<Long, CalendarEntry> teacherMap = CalendarUtils.createCalendarEntryMap(teacherCalendarEntries);
		Map<Long, CalendarEntry> studentMap = CalendarUtils.createCalendarEntryMap(studentCalendarEntries);

		AvailabilityResponse response = new AvailabilityResponse(teacherId, studentId, startTime, endTime);
		int trueAvailabilityIndex = 0;
		int slotCount = CommonCalendarUtils.calculateTotalSlots(startTime, endTime);
		Integer[] trueAvailabilityArray = ArrayUtils.toObject(new int[slotCount]);
		BitSet availabiltiyBitSet = new BitSet();
		long currentStartTime = startTime;

		// Calculate last marked time
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		logger.info("week start day : " + cal.getFirstDayOfWeek());
		Long lastMarkedWeekStartTime = calendarUtilsManager.getLastMarkedWeekStartTime(teacherId);
		logger.info("lastMarkedWeekStartTime - " + lastMarkedWeekStartTime);
		if (lastMarkedWeekStartTime != null
				&& lastMarkedWeekStartTime >= CommonCalendarUtils.getWeekStartTime(System.currentTimeMillis())) {
			List<CalendarEntry> lastMarkedWeekEntries = calendarUtilsManager.getCalendarEntries(teacherId,
					lastMarkedWeekStartTime, lastMarkedWeekStartTime + CommonCalendarUtils.MILLIS_PER_WEEK);
			Map<Long, CalendarEntry> lastMarkedEntriesMap = CalendarUtils.createCalendarEntryMap(lastMarkedWeekEntries);

			while (currentStartTime < endTime) {
				long dayStartTime = CommonCalendarUtils.getDayStartTime(currentStartTime);
				int startIndex = CommonCalendarUtils.getSlotStartIndex(currentStartTime, dayStartTime);
				int endIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
				if (endTime < dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY) {
					endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, dayStartTime);
				}

				CalendarEntry teacherEntry = teacherMap.get(dayStartTime);
				CalendarEntry studentEntry = studentMap.get(dayStartTime);

				// Update availability of teacher by extrapolation
				if (dayStartTime >= (lastMarkedWeekStartTime + CommonCalendarUtils.MILLIS_PER_WEEK)) {
					// Create teacher availability if it is empty.
					if (teacherEntry == null) {
						teacherEntry = new CalendarEntry(teacherId, dayStartTime);
					}

					// Reset availability from last marked week
					if (useExtrapolation) {
						Long availabiltiyStartTime = lastMarkedWeekStartTime
								+ ((dayStartTime - lastMarkedWeekStartTime) % CommonCalendarUtils.MILLIS_PER_WEEK);
						CalendarEntry availabilityEntry = lastMarkedEntriesMap.get(availabiltiyStartTime);
						if (availabilityEntry != null) {
							teacherEntry.setAvailability(availabilityEntry.getAvailability());
						} else {
							teacherEntry.setAvailability(new HashSet<>());
						}
					}
				}

				for (int slotIndex = startIndex; slotIndex <= endIndex; slotIndex++, trueAvailabilityIndex++) {
					if (CalendarUtils.calculateTrueAvailability(slotIndex, teacherEntry, studentEntry)) {
						trueAvailabilityArray[trueAvailabilityIndex] = 1;
					} else {
						trueAvailabilityArray[trueAvailabilityIndex] = 0;
					}

					if (CalendarUtils.isTeacherAvailabilityMarked(slotIndex, teacherEntry)) {
						availabiltiyBitSet.set(trueAvailabilityIndex);
					}
				}

				currentStartTime = CommonCalendarUtils.getDayStartTime(currentStartTime)
						+ CommonCalendarUtils.MILLIS_PER_DAY;
			}
		}

		// Update trueAvailability based on blocked slots
		BitSet trueAvailabilityBitSet = BitSetUtils.createBitSet(trueAvailabilityArray);
		BitSet blockBitSet = calendarBlockEntryManager.getCalendarBlockBitSet(studentId, teacherId, startTime, endTime,
				neglectReferenceId);
		trueAvailabilityBitSet.andNot(blockBitSet);
		trueAvailabilityArray = BitSetUtils.createSlotIntegerArray(startTime, endTime, trueAvailabilityBitSet);

		// True availability response
		response.setTrueAvailability(trueAvailabilityArray);
		response.setAvailabilityBitSet(availabiltiyBitSet.toLongArray());
		response.setTrueAvailabilityBitSet(trueAvailabilityBitSet.toLongArray());
		logger.info("getTrueAvailabilityResponse : " + response);
		return response;
	}

	public AvailabilityResponse getTruerAvailability(Long startTime, Long endTime, String teacherId, String studentId,
			Integer repeat, float match, String neglectReferenceId) throws ForbiddenException {
		logger.info("getTruerAvailabilityRequest - startTime:" + startTime + " endTime:" + endTime + " teacherId:"
				+ teacherId + " studentId:" + studentId + " repeat:" + repeat + " match:" + match);

		// Round off start time and end time to start of a slot
		startTime = CommonCalendarUtils.getSlotStartTime(startTime);
		endTime = CommonCalendarUtils.getSlotStartTime(endTime);
		int totalSlotCount = CommonCalendarUtils.calculateTotalSlots(startTime, endTime);
		int[] resultArray = new int[totalSlotCount];

		// Calculate Truer Availability calendar
		long diff = endTime - startTime;
		AvailabilityResponse r;
		if (match < Float.valueOf(1)) {
			r = getTrueAvailability(startTime, endTime + (repeat * diff), teacherId, studentId, neglectReferenceId,
					true);
		} else {
			r = getTrueAvailability(startTime, endTime + (repeat * diff), teacherId, studentId, neglectReferenceId,
					false);
		}

		for (int count = 0; count <= repeat; count++) {
			int intervalStartIndex = CommonCalendarUtils.getSlotStartIndex(startTime + (count * diff), startTime);
			int intervalEndIndex = CommonCalendarUtils.getSlotStartIndex(endTime + (count * diff), startTime);
			CalendarUtils.updateTruerAvailabilityArray(resultArray, r.getTrueAvailability(), intervalStartIndex,
					intervalEndIndex);
		}

		// Update truerAvailability by match
		logger.info("updateTruerAvailabilityByMatch : " + Arrays.toString(resultArray) + " match : " + match
				+ " repeat : " + repeat);
		CalendarUtils.updateTruerAvailabilityByMatch(resultArray, match, repeat + 1);

		AvailabilityResponse response = new AvailabilityResponse(teacherId, studentId, startTime, endTime);
		response.setTrueAvailability(ArrayUtils.toObject(resultArray));
		response.setStartTime(startTime);
		response.setEndTime(endTime);
		logger.info("Response : " + response.toString());
		return response;
	}

	public Set<Integer> excludeUnavailableSlots(Set<Integer> availability, Set<Integer> booked,
			Set<Integer> sessionRequest) {

		Set<Integer> availableEntry = new HashSet<Integer>();

		if (availability == null) {
			return availableEntry;
		}

		if (booked == null) {
			booked = new HashSet<Integer>();
		}
		if (sessionRequest == null) {
			sessionRequest = new HashSet<Integer>();
		}

		for (Integer slot : availability) {
			if (!booked.contains(slot) && !sessionRequest.contains(slot)) {
				availableEntry.add(slot);
			}
		}
		return availableEntry;

	}

	public List<CalendarEntry> getBulkCalendarEntries(List<String> userIds, Long startTime, Long endTime) {
		Long startTimeDay = CommonCalendarUtils.getDayStartTime(startTime);
		Long endTimeDay = CommonCalendarUtils.getDayStartTime_end(endTime);
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).gte(startTimeDay).lte(endTimeDay));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(userIds));
		query.with(Sort.by(Direction.ASC, CalendarEntry.Constants.DAY_START_TIME));
		List<CalendarEntry> results = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		return results;
	}

	public UpdateCalendarSlotBitsRes updateCalendarSlotBits(UpdateCalendarSlotBitsReq updateCalendarSlotBitsReq)
			throws ConflictException, BadRequestException {
		Long currentTime = System.currentTimeMillis();
		logger.info("updateCalendarSlotBitsReq : " + updateCalendarSlotBitsReq.toString());
		UpdateCalendarSlotBitsRes response = new UpdateCalendarSlotBitsRes();

		// Validate save request
		List<String> errors = updateCalendarSlotBitsReq.collectErrors();
		if (!errors.isEmpty()) {
			String errorMessage = "updateCalendarSlotBits - Bad Request error for request "
					+ updateCalendarSlotBitsReq.toString() + " and errors : " + Arrays.toString(errors.toArray());
			logger.error(errorMessage);
			// String displayMessage =
			// ConfigUtils.INSTANCE.getStringValue("bad.request.display.message");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		}

		// Check if there is already an save operation in progress
		List<CalendarEvent> calendarEvents = calendarEventManager
				.getAvailabilityRemainingEvents(updateCalendarSlotBitsReq.getUserId());
		if (!calendarEvents.isEmpty()) {
			String errorMessage = "updateCalendarSlotBitsPending - " + updateCalendarSlotBitsReq.toString();
			logger.info(errorMessage);
			// String displayMessage =
			// ConfigUtils.INSTANCE.getStringValue("pending.calendar.update.error.message");
			throw new ConflictException(ErrorCode.PENDING_CALENDAR_UPDATE, errorMessage);
		}

		// Create calendar event and save calendar entries
		CalendarEventDetails calendarEventDetails = new CalendarEventDetails(updateCalendarSlotBitsReq);
		CalendarEvent calendarEvent = new CalendarEvent(updateCalendarSlotBitsReq,
				new Gson().toJson(calendarEventDetails), CalendarEventState.PENDING);
		calendarEventManager.updateCalendarEvent(calendarEvent);

		logger.info("updateCalendarSlotBitsSync - updating calendar slot entries synchronously");
		calendarEventManager.updateCalendarEntries(calendarEvent);
		response.setCalendarEvent(calendarEvent);

		// Track time taken for execution
		Long timeTaken = System.currentTimeMillis() - currentTime;
		if (timeTaken > 10000l) {
			logger.info("updateCalendarSlotBitsDelay - request : " + updateCalendarSlotBitsReq.toString()
					+ " timeTaken : " + timeTaken);
		}
		logger.info("updateCalendarSlotBitsDuration : " + timeTaken);
		logger.info("updateCalendarSlotBitsResponse : " + response.toString());
		return response;
	}

	public void updateCalendarEntries(CalendarEvent calendarEvent) throws BadRequestException {
		// Validate calendar event
		Long executionStartTime = System.currentTimeMillis();
		List<String> errors = calendarEvent.validate();
		if (!errors.isEmpty()) {
			String errorMessage = "updateCalendarEntriesInvalid calendarEvent : " + calendarEvent.toString()
					+ " errors : " + Arrays.toString(errors.toArray());
			logger.error(errorMessage);
			// String displayMessage =
			// ConfigUtils.INSTANCE.getStringValue("bad.request.display.message");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		}

		// Set calendar state to PROCESSING
		calendarEvent.incrementRetryCount();
		calendarEvent.setState(CalendarEventState.PROCESSING);
		calendarEventManager.updateCalendarEvent(calendarEvent);
		logger.info("calendarEventProcessing : " + calendarEvent);

		// validate event details
		String details = calendarEvent.getDetails();
		CalendarEventDetails eventDetails = new Gson().fromJson(details, CalendarEventDetails.class);
		errors = eventDetails.collectErrors();
		if (!errors.isEmpty()) {
			String errorMessage = "updateCalendarEntriesInvalid request : " + eventDetails.toString()
					+ " calendarEvent : " + calendarEvent.toString() + " errors : " + Arrays.toString(errors.toArray());
			logger.error(errorMessage);
			// String displayMessage =
			// ConfigUtils.INSTANCE.getStringValue("bad.request.display.message");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		}

		// Identify modified bits
		BitSet modifiedBitSet = BitSetUtils.getModifiedBitSet(eventDetails.getOldBitSetLongArray(),
				eventDetails.getNewBitSetLongArray());
		logger.info("modifedBitSet : " + Arrays.toString(modifiedBitSet.toLongArray()));
		BitSetEntry bitSetEntry = new BitSetEntry(eventDetails, calendarEvent.getSlotState());

		// Capture Day Start Times
		List<Long> dayStartTimes = new ArrayList<>();
		Long startTime = CommonCalendarUtils.getDayStartTime(eventDetails.getStartTime());
		for (Long i = startTime; i < eventDetails.getEndTime(); i += CommonCalendarUtils.MILLIS_PER_DAY) {
			int index = bitSetEntry.getBitSetIndex(i);
			if ((index < -CalendarEntryManager.SLOT_COUNT)
					|| (index + CalendarEntryManager.SLOT_COUNT > BitSetUtils.bitSetSize(bitSetEntry))) {
				continue;
			}
			if (index < 0) {
				index = 0;
			}
			int nextModifiedBit = modifiedBitSet.nextSetBit(index);
			if ((nextModifiedBit >= 0) && (nextModifiedBit < index + CalendarEntryManager.SLOT_COUNT)) {
				dayStartTimes.add(i);
			}
		}

		// Create calendar entry map for the updated entries
		List<CalendarEntry> calendarEntries = calendarUtilsManager.getCalendarEntries(calendarEvent.getUserId(),
				dayStartTimes);
		Map<Long, CalendarEntry> calendarEntriesMap = calendarUtilsManager.getCalendarEntryMap(calendarEntries);
		for (long i : dayStartTimes) {
			CalendarEntry calendarEntry = null;
			if (calendarEntriesMap.containsKey(i)) {
				calendarEntry = calendarEntriesMap.get(i);
			} else {
				calendarEntry = new CalendarEntry(calendarEvent.getUserId(), i, new HashSet<Integer>(),
						new HashSet<Integer>(), new HashSet<Integer>());
			}
			calendarEntry.updateByBitSet(bitSetEntry);
			logger.info("updateCalendarEntriesPostUpdate : " + calendarEntry.toString());
			calendarEntryDAO.save(calendarEntry);
		}

		logger.info("updateCalendarEntriesUpdateCount : update count is " + calendarEntries.size());

		// Update calendar event to DONE state
		eventDetails.setExecutionTime(System.currentTimeMillis() - executionStartTime);
		calendarEvent.setState(CalendarEventState.DONE);
		calendarEventManager.updateCalendarEvent(calendarEvent);
		logger.info("calendarEventDone : " + calendarEvent);
	}

	public GetCalendarSlotBitRes getCalendarSlotBits(GetCalendarSlotBitReq getCalendarSlotBitReq)
			throws BadRequestException, ConflictException {
		// Validate request
		logger.info("getCalendarSlotBitReq : " + getCalendarSlotBitReq.toString());
		List<String> errors = getCalendarSlotBitReq.collectErrors();
		if (!errors.isEmpty()) {
			String errorMessage = "getCalendarSlotBits - Bad Request error for request "
					+ getCalendarSlotBitReq.toString() + " and errors : " + Arrays.toString(errors.toArray());
			logger.error(errorMessage);
			// String displayMessage =
			// ConfigUtils.INSTANCE.getStringValue("bad.request.display.message");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		}

		// Check if there are any save operations pending for this user.
		List<CalendarEvent> calendarEvents = calendarEventManager
				.getAvailabilityRemainingEvents(getCalendarSlotBitReq.getUserId());
		if (!calendarEvents.isEmpty()) {
			String errorMessage = "getCalendarSlotBitsPending - " + getCalendarSlotBitReq.toString();
			logger.info(errorMessage);
			// String displayMessage =
			// ConfigUtils.INSTANCE.getStringValue("pending.calendar.update.error.message");
			throw new ConflictException(ErrorCode.PENDING_CALENDAR_UPDATE, errorMessage);
		}

		// Populate BitSet from calendar entries
		BitSetEntry bitSetEntry = new BitSetEntry(getCalendarSlotBitReq);
		List<CalendarEntry> calendarEntries = calendarUtilsManager.getCalendarEntries(getCalendarSlotBitReq.getUserId(),
				getCalendarSlotBitReq.getStartTime(), getCalendarSlotBitReq.getEndTime());
		logger.info("calendarEntries count : " + (calendarEntries == null ? 0 : calendarEntries.size()));
		if (calendarEntries != null) {
			for (CalendarEntry calendarEntry : calendarEntries) {
				logger.info("CalendarEntry : " + calendarEntry.toString());
				bitSetEntry.updateBitSet(calendarEntry);
			}
		}
		bitSetEntry.updatePadding();
		logger.info("updatedBitSet : " + bitSetEntry.getBitSetLongString());

		// Create response from generated bitSet
		GetCalendarSlotBitRes response = new GetCalendarSlotBitRes(getCalendarSlotBitReq);
		response.setBitSetLongArray(bitSetEntry);

		logger.info("getCalendarSlotBitsResponse : " + response.toString());
		return response;
	}

	public static String pringBinaryString(long[] values) {
		System.out.println();
		String binaryStr = "";
		BitSet bitSet = BitSet.valueOf(values);
		for (int i = 0; i < bitSet.size(); i++) {
			if (bitSet.get(i)) {
				binaryStr += "1";
			} else {
				binaryStr += "0";
			}
			// System.out.print(Long.toBinaryString(value));
		}

		System.out.println(binaryStr);
		return binaryStr;
	}

	public static boolean validateSlotDuration(Long duration) {
		boolean valid = true;
		if (duration == null || duration <= 0l) {
			valid = false;
		} else if (duration % CalendarEntryManager.SLOT_LENGTH != 0l) {
			valid = false;
		}

		return valid;
	}

	public BlockSlotScheduleRes blockSlotsSchedule(BlockSlotScheduleReq blockSlotScheduleReq) {
		logger.info("blockSlotsSchedule - request : " + blockSlotScheduleReq.toString());
		try {
			SessionSchedule schedule = blockSlotScheduleReq.getSchedule();
			if (CollectionUtils.isEmpty(schedule.getSessionSlots())) {
				BlockSlotScheduleRes returnrespone = new BlockSlotScheduleRes(blockSlotScheduleReq.getStudentId(),
						blockSlotScheduleReq.getTeacherId(), schedule);
				return returnrespone;
			}

			// Updating Start time to day start time should only done during
			// blocking.
			Long startTime = CommonCalendarUtils.getDayStartTime(System.currentTimeMillis());
			schedule.setStartTime(startTime);
			Long endTime = startTime + ((schedule.getNoOfWeeks() + 3) * CommonCalendarUtils.MILLIS_PER_WEEK);

			AvailabilityResponse availabilityResponse = getTrueAvailability(startTime, endTime,
					blockSlotScheduleReq.getTeacherId(), blockSlotScheduleReq.getStudentId(),
					blockSlotScheduleReq.getReferenceId(), true);
			int bitSetLength = BitSetUtils.bitSetSize(startTime, endTime, CommonCalendarUtils.getSlotLength());
			BitSet slotBitSet = calendarUtilsManager.createSlotBitSet(schedule);
			logger.info("blockSlotsSchedule slotBitSet - " + Arrays.toString(slotBitSet.toLongArray()));
			BitSet trueAvailabilityBitSet = BitSetUtils.createBitSet((availabilityResponse.getTrueAvailability()));

			BitSet conflictSlotBitSet = new BitSet(bitSetLength);
			conflictSlotBitSet.or(slotBitSet);
			conflictSlotBitSet.andNot(trueAvailabilityBitSet);

			// Update conflict bits by sessions
			CalendarUtils.updateConflictBitsBySessions(conflictSlotBitSet, slotBitSet);
			logger.info("blockSlotsSchedule conflictSlotBitSet - " + Arrays.toString(conflictSlotBitSet.toLongArray()));

			CalendarBlockEntry calendarBlockEntry = new CalendarBlockEntry(blockSlotScheduleReq, startTime, slotBitSet);
			calendarBlockEntryManager.updateCalendarBlockEntry(calendarBlockEntry);

			// Calculate conflict count
			int conflictCount = 0;
			for (int i = 0; i >= 0 && i < bitSetLength; i++) {
				int index = conflictSlotBitSet.nextSetBit(i);
				if (index < 0) {
					break;
				}

				conflictCount++;
				i = conflictSlotBitSet.nextClearBit(index);
			}

			schedule.setSlotBitSet(slotBitSet.toLongArray());
			schedule.setConflictSlotBitSet(conflictSlotBitSet.toLongArray());
			schedule.setConflictCount(conflictCount);
			schedule.setStartTime(startTime);
			schedule.setEndTime(endTime);
			logger.info("new schedule - " + schedule.toString());
			BlockSlotScheduleRes respone = new BlockSlotScheduleRes(blockSlotScheduleReq.getStudentId(),
					blockSlotScheduleReq.getTeacherId(), schedule);
			logger.info("BlockSlotScheduleRes - " + respone.toString() + "referenceId="
					+ blockSlotScheduleReq.getReferenceId());
			return respone;
		} catch (Exception ex) {
			logger.error("blockSlotsSchedule : ex " + ex.getMessage() + " stack trace : " + ex.toString());
			throw ex;
		}
	}

	public GetSlotConflictRes getSlotConflicts(GetSlotConflictReq getSlotConflictReq) throws BadRequestException {
		logger.info("Request : " + getSlotConflictReq.toString());
		List<String> errors = getSlotConflictReq.collectErrors();
		if (!errors.isEmpty()) {
			throw new BadRequestException(com.vedantu.exception.ErrorCode.BAD_REQUEST_ERROR,
					Arrays.toString(errors.toArray()));
		}

		SessionSchedule schedule = getSlotConflictReq.getSchedule();

		// Start time should not be rounded off to day start time. Additional 12
		// weeks is added as a safety net
		// if we send 3 month slots in schedule with noOfWeeks as one. This wont
		// have any affect anywhere.
		Long startTime = schedule.getStartTime();
		Long endTime = startTime + ((schedule.getNoOfWeeks() + 12) * CommonCalendarUtils.MILLIS_PER_WEEK);
		logger.info("startTime : " + startTime + " endTime : " + endTime);
		GetSlotConflictRes response = new GetSlotConflictRes(getSlotConflictReq.getStudentId(),
				getSlotConflictReq.getTeacherId(), new BitSet().toLongArray(), startTime, endTime);

		if (schedule.getConflictSlotBitSet() != null) {
			// No conflicts
			BitSet slotBitSet = BitSet.valueOf(schedule.getSlotBitSet());
			BitSet conflictBitSet = BitSet.valueOf(schedule.getConflictSlotBitSet());
			AvailabilityResponse trueAvailabilityResponse = getTrueAvailability(startTime, endTime,
					String.valueOf(getSlotConflictReq.getTeacherId()),
					String.valueOf(getSlotConflictReq.getStudentId()), getSlotConflictReq.getReferenceId(), false);
			Integer[] availability = trueAvailabilityResponse.getTrueAvailability();

			int bitSetCount = BitSetUtils.bitSetSize(startTime, endTime, CommonCalendarUtils.getSlotLength());
			for (int i = 0; i < bitSetCount; i++) {
				int startIndex = conflictBitSet.nextSetBit(i);
				if (startIndex == -1) {
					break;
				}

				int endIndex = conflictBitSet.nextClearBit(startIndex);

				if (endIndex >= bitSetCount) {
					logger.error("GetConflictEndIndexError - End index is greater than bitSetCount : " + bitSetCount
							+ " endIndex : " + endIndex + " request : " + getSlotConflictReq.toString());
					endIndex = bitSetCount - 1;
				}

				boolean conflictFound = false;
				for (int index = startIndex; index < endIndex; index++) {
					if (availability[index] == 0) {
						response.addConflictSlot(new com.vedantu.session.pojo.SessionSlot(
								CommonCalendarUtils.getAbsoluteSlotTime(startIndex, startTime),
								CommonCalendarUtils.getAbsoluteSlotTime(endIndex, startTime)));
						conflictFound = true;
						break;
					}
				}

				if (!conflictFound) {
					logger.info("startIndex : " + startIndex + " endIndex : " + endIndex + " slotBitSet update");
					slotBitSet.set(startIndex, endIndex, true);
					logger.info("startIndex : " + startIndex + " endIndex : " + endIndex + " conflictBitSet update");
					conflictBitSet.set(startIndex, endIndex, false);
				}

				i = endIndex;
			}

			// Update conflict bits by sessions
			CalendarUtils.updateConflictBitsBySessions(conflictBitSet, slotBitSet);
			logger.info("getSlotConflicts conflictSlotBitSet - " + Arrays.toString(conflictBitSet.toLongArray())
					+ " slotBitSet : " + Arrays.toString(slotBitSet.toLongArray()));
			response.setConflictSlotBits(conflictBitSet.toLongArray());
			response.setSlotBits(slotBitSet.toLongArray());

			// Update Calendar block entry
			// if (StringUtils.isNotEmpty(getSlotConflictReq.getReferenceId())) {
			// calendarBlockEntryManager.updateCalendarBlockEntry(getSlotConflictReq.getReferenceId(),
			// startTime,
			// slotBitSet);
			// }
		}
		return response;
	}

	public ResolveSlotConflictRes resolveSlotConflicts(ResolveSlotConflictReq req) {
		logger.info("resolveSlotConflicts - " + req.toString());
		BitSet slotBitSet = new BitSet();
		if (req.getBitSetLongArray() != null) {
			slotBitSet = BitSet.valueOf(req.getBitSetLongArray());
		}

		BitSet conflictBitSet = new BitSet();
		if (req.getConflictSlotBits() != null) {
			conflictBitSet = BitSet.valueOf(req.getConflictSlotBits());
		}

		try {
			final long startTime = req.getStartTime();
			Long endTime = startTime + (req.getNoOfWeeks() + 3) * CommonCalendarUtils.MILLIS_PER_WEEK;
			ResolveSlotConflictRes resolveSlotConflictRes = new ResolveSlotConflictRes(
					Long.parseLong(req.getStudentId()), Long.parseLong(req.getTeacherId()), startTime, endTime,
					req.getBitSetLongArray(), 0);
			if (req.getSessionConflict() != null && !req.getSessionConflict().isEmpty()) {
				AvailabilityResponse availabilityResponse = getTrueAvailability(startTime, endTime, req.getTeacherId(),
						req.getStudentId(), req.getReferenceId(), false);
				for (SessionConflict sessionConflict : req.getSessionConflict()) {

					// Add previous session in bit set
					if (sessionConflict.getNewSessionSlot() != null) {
						if (sessionConflict.getNewSessionSlot().getStartTime() < req.getStartTime()) {
							logger.error(
									"resolveSlotConflictsError - session conflict - " + sessionConflict.toString());
							continue;
						}

						logger.info(
								"Checking availability for slot : " + sessionConflict.getNewSessionSlot().toString());
						Boolean available = checkSlotAvailabiltiy(availabilityResponse,
								sessionConflict.getNewSessionSlot().getStartTime(),
								sessionConflict.getNewSessionSlot().getEndTime());
						if (available) {
							logger.info("Slot available");
							updateBitSetSession(startTime, sessionConflict.getNewSessionSlot(), slotBitSet, true);
							// Remove previous session in bit set
							if (sessionConflict.getPreviousSessionSlot() != null) {
								updateBitSetSession(startTime, sessionConflict.getPreviousSessionSlot(), slotBitSet,
										false);
								resolveSlotConflictRes.incrementResolvedConflictCount();
								updateBitSetSession(startTime, sessionConflict.getPreviousSessionSlot(), conflictBitSet,
										false);
							}
						}
					}
				}

				// Update Calendar block entry
				calendarBlockEntryManager.updateCalendarBlockEntry(req.getReferenceId(), startTime, slotBitSet);

				// Update conflict bits by sessions
				logger.info("resolveSlotConflicts conflictSlotBitSet pre update of session slots- "
						+ Arrays.toString(conflictBitSet.toLongArray()));
				CalendarUtils.updateConflictBitsBySessions(conflictBitSet, slotBitSet);
				logger.info(
						"resolveSlotConflicts conflictSlotBitSet - " + Arrays.toString(conflictBitSet.toLongArray()));

				// Call get conflict and send the response
				resolveSlotConflictRes.setSlotBits(slotBitSet.toLongArray());
				resolveSlotConflictRes.setConflictSlotBits(conflictBitSet.toLongArray());
			}
			logger.info("Respone - " + resolveSlotConflictRes.toString());
			return resolveSlotConflictRes;
		} catch (Exception ex) {
			logger.error("resolveSlotConflicts - ex : " + ex.getMessage() + " ex : " + ex.toString());
			throw ex;
		}
	}

	private boolean checkSlotAvailabiltiy(AvailabilityResponse availabilityResponse, Long startTime, Long endTime) {
		Long availabilityStartTime = availabilityResponse.getStartTime();
		int startIndex = CommonCalendarUtils.getSlotStartIndex(startTime, availabilityStartTime);
		int endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, availabilityStartTime);

		Integer[] trueAvailability = availabilityResponse.getTrueAvailability();
		for (int i = startIndex; i < endIndex; i++) {
			if (trueAvailability[i] == 0) {
				return false;
			}
		}

		return true;
	}

	private void updateBitSetSession(Long startTime, com.vedantu.session.pojo.SessionSlot sessionSlot, BitSet bitSet,
			boolean value) {
		int slotStartIndex = CommonCalendarUtils.getSlotStartIndex(sessionSlot.getStartTime(), startTime);
		int slotEndIndex = CommonCalendarUtils.getSlotEndIndex(sessionSlot.getEndTime(), startTime);

		logger.info("slotStartIndex : " + slotStartIndex + " slotEndIndex : " + slotEndIndex);
		for (int i = slotStartIndex; i <= slotEndIndex; i++) {
			bitSet.set(i, value);
		}
	}

	public BasicResponse updateCalendarSessionSlots(UpdateCalendarSessionSlotsReq req) {
		logger.info("ENTRY: " + req);
		BasicResponse response = new BasicResponse();
		if (req.getScheduledSlots() == null || req.getScheduledSlots().length == 0) {
			return response;
		}
		BitSetEntry bitSetEntry = new BitSetEntry(req.getStartTime(), req.getEndTime(), CalendarEntrySlotState.SESSION,
				Constants.SLOT_LENGTH, BitSet.valueOf(req.getScheduledSlots()));
		List<Long> modifiedDayStartTimes = calendarUtilsManager.getModifiedDayStartTimes(bitSetEntry);

		if (modifiedDayStartTimes != null && !modifiedDayStartTimes.isEmpty()) {
			List<CalendarEntry> teacherCalendarEntries = calendarUtilsManager.getCalendarEntries(req.getTeacherId(),
					modifiedDayStartTimes);
			List<CalendarEntry> studentCalendarEntries = calendarUtilsManager.getCalendarEntries(req.getStudentId(),
					modifiedDayStartTimes);
			Map<Long, CalendarEntry> teacherMap = CalendarUtils.createCalendarEntryMap(teacherCalendarEntries);
			Map<Long, CalendarEntry> studentMap = CalendarUtils.createCalendarEntryMap(studentCalendarEntries);

			for (long i : modifiedDayStartTimes) {
				// Update teacher calendar
				CalendarEntry teacherEntry = teacherMap.get(i);
				if (teacherEntry == null) {
					teacherEntry = new CalendarEntry(req.getTeacherId(), i);
				}
				logger.info("Calendar entry pre update - " + teacherEntry.toString());
				teacherEntry.mergeSessionByBitSet(bitSetEntry);
				calendarEntryDAO.save(teacherEntry);
				logger.info("Calendar entry post update - " + teacherEntry.toString());

				// Update student calendar
				CalendarEntry studentEntry = studentMap.get(i);
				if (studentEntry == null) {
					studentEntry = new CalendarEntry(req.getStudentId(), i);
				}
				logger.info("Calendar entry pre update - " + studentEntry.toString());
				studentEntry.mergeSessionByBitSet(bitSetEntry);
				calendarEntryDAO.save(studentEntry);
				logger.info("Calendar entry post update - " + studentEntry.toString());
			}
		}
		logger.info("EXIT " + response);
		return response;
	}

	public BaseResponse createAvailabilityRanges(CreateAvailabilityRangesRequest createAvailabilityRangesRequest) {
		int i;
		BaseResponse jsonResp = new BaseResponse();
		List<AvailabilityRange> newAvailabilityRanges = createAvailabilityRangesRequest.getAvailabilityRanges();
		logger.info("got availability ranges in request");
		Integer min, max;
		Query query;

		// TODO remove saving inside loop

		// Disable slabs
		for (i = 0; i < newAvailabilityRanges.size(); i++) {

			min = newAvailabilityRanges.get(i).getMin();
			max = newAvailabilityRanges.get(i).getMax();
			query = new Query();
			query.addCriteria(Criteria.where("min").lte(min).andOperator(Criteria.where("max").gt(min.intValue())
					.andOperator(Criteria.where("max").ne(-1).andOperator(Criteria.where("active").is(true)))));
			findAndDisableSlab(query, min, max);

			query = new Query();
			query.addCriteria(Criteria.where("min").lt(max).andOperator(Criteria.where("max").gte(max.intValue())
					.andOperator(Criteria.where("max").ne(-1).andOperator(Criteria.where("active").is(true)))));
			findAndDisableSlab(query, min, max);

			query = new Query();
			query.addCriteria(Criteria.where("min").is(min)
					.andOperator(Criteria.where("max").is(max).andOperator(Criteria.where("active").is(true))));
			findAndDisableSlab(query, min, max);

			AvailabilityRange availabilityRange = new AvailabilityRange(min, max,
					newAvailabilityRanges.get(i).getWeight(), true);
			availabilityRangeDAO.save(availabilityRange);

		}
		jsonResp.setErrorCode(com.vedantu.exception.ErrorCode.SUCCESS);
		jsonResp.setErrorMessage("Slab Ranges created successfully");
		logger.info("Exiting Response:" + jsonResp.toString());
		return jsonResp;
	}

	private void findAndDisableSlab(Query query, Integer min, Integer max) {

		List<AvailabilityRange> foundSlabRanges;
		AvailabilityRange foundSlabRange;
		foundSlabRanges = availabilityRangeDAO.runQuery(query, AvailabilityRange.class);
		for (int i = 0; i < foundSlabRanges.size(); i++) {
			// logger.info("FoundSlabaRange min:" +
			// foundSlabRanges.get(i).getMin() + " max:" +
			// foundSlabRanges.get(i).getMax());
			foundSlabRange = foundSlabRanges.get(i);
			foundSlabRange.setActive(false);
			logger.info("Disabling slabRange:" + foundSlabRange.getId());
			availabilityRangeDAO.save(foundSlabRange);
		}
	}

	public List<AvailabilityRange> getAvailabilityRanges() {

		Query query = new Query();
		query.addCriteria(Criteria.where("active").is(true));
		List<AvailabilityRange> foundSlabRanges = availabilityRangeDAO.runQuery(query, AvailabilityRange.class);
		return foundSlabRanges;
	}

	public List<SessionSlot> getSuggestedSlot(String studentId, String teacherId, Long startTime, Long endTime)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		List<SessionSlot> suggestedSlots = new ArrayList<>();

		AvailabilityResponse availabilityResponse = getTrueAvailability(startTime, endTime, teacherId, studentId, null,
				false);

		// Remove availability for next 24 hours
		availabilityResponse.removeAvailabilityTillTime(System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY);

		Integer[] trueAvailability = availabilityResponse.getTrueAvailability();
		SessionSlot otherSlot = null;
		for (int startIndex = 0; startIndex < trueAvailability.length; startIndex += SLOT_COUNT) {
			SlotSelectionPojo slotSelectionPojo = calendarSelectionManager.populateSlotSelectionPojo(trueAvailability,
					startTime, startIndex);
			calendarSelectionManager.filterSlotByPriority(slotSelectionPojo);
			SessionSlot suggestSlot = pickSuggestedSlot(slotSelectionPojo);
			if (suggestSlot != null) {
				suggestedSlots.add(suggestSlot);
				return suggestedSlots;
			}

			// Pick others if no prime slot is selected
			if (otherSlot == null) {
				otherSlot = pickTagSlotByTagName(slotSelectionPojo.others, "others");
			}
		}

		if (otherSlot != null) {
			suggestedSlots.add(otherSlot);
		}
		return suggestedSlots;
	}

	public List<SelectedSlotResponse> getSelectedSlots(String studentId, String teacherId, Long startTime, Long endTime)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		logger.info("Request - startTime : " + startTime + " endTime : " + endTime);

		AvailabilityResponse availabilityResponse = getTrueAvailability(startTime, endTime, teacherId, studentId, null,
				false);

		// Remove availability for next 24 hours
		availabilityResponse.removeAvailabilityTillTime(System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY);

		Integer[] trueAvailability = availabilityResponse.getTrueAvailability();
		List<SelectedSlotResponse> response = new ArrayList<SelectedSlotResponse>();
		for (int startIndex = 0; startIndex < trueAvailability.length; startIndex += SLOT_COUNT) {
			SlotSelectionPojo slotSelectionPojo = calendarSelectionManager.populateSlotSelectionPojo(trueAvailability,
					startTime, startIndex);
			calendarSelectionManager.filterSlotByPriority(slotSelectionPojo);
			SelectedSlotResponse slotResponse = new SelectedSlotResponse(
					startTime + CommonCalendarUtils.getSlotLength() * startIndex, slotSelectionPojo);
			response.add(slotResponse);
		}

		logger.info("response - " + response.toString());
		return response;
	}

	public List<AvailabilityResponse> getTeacherAvailabilityBitSet(List<String> teacherIds, Long startTime,
			Long endTime) {
		logger.info("Request : " + Arrays.toString(teacherIds.toArray()) + " startTime:" + startTime + " endTime:"
				+ endTime);
		List<AvailabilityResponse> response = new ArrayList<AvailabilityResponse>();
		if (!CollectionUtils.isEmpty(teacherIds)) {
			for (String teacherId : teacherIds) {
				response.add(getTrueAvailability(startTime, endTime, String.valueOf(teacherId), "0", null, false));
			}
		}

		return response;
	}

	public BasicRes syncNextMonthCalendar() {
		SyncAllCalendarReq req = new SyncAllCalendarReq();
		long startTime = System.currentTimeMillis();
		startTime = startTime - (startTime % DateTimeUtils.MILLIS_PER_HOUR) + DateTimeUtils.MILLIS_PER_HOUR;
		req.setStartTime(startTime);
		req.setEndTime(startTime + 4 * DateTimeUtils.MILLIS_PER_WEEK);
		logger.info("Req:" + req.toString());
		return syncAllCalendar(req);
	}

	public BasicRes syncNextWeekCalendar() {
		SyncAllCalendarReq req = new SyncAllCalendarReq();
		long startTime = System.currentTimeMillis();
		startTime = startTime - (startTime % DateTimeUtils.MILLIS_PER_HOUR) + DateTimeUtils.MILLIS_PER_HOUR;
		req.setStartTime(startTime);
		req.setEndTime(startTime + DateTimeUtils.MILLIS_PER_WEEK);
		logger.info("Req:" + req.toString());
		return syncAllCalendar(req);
	}

	public BasicRes syncNextYearCalendar() {
		SyncAllCalendarReq req = new SyncAllCalendarReq();
		long startTime = System.currentTimeMillis();
		startTime = startTime - (startTime % DateTimeUtils.MILLIS_PER_HOUR) + DateTimeUtils.MILLIS_PER_HOUR;
		req.setStartTime(startTime);
		req.setEndTime(startTime + 52 * DateTimeUtils.MILLIS_PER_WEEK);
		logger.info("Req:" + req.toString());
		return syncAllCalendar(req);
	}

	// Only applicable for teachers
	public BasicRes syncAllCalendar(SyncAllCalendarReq req) {

		// Update for all the teachers if no user ids are passed
		List<String> userIds = req.getUserIds();
		if (CollectionUtils.isEmpty(userIds)) {
			userIds = new ArrayList<String>();
			List<UserInfo> users = fosUtils.getAllTeacherInfos();
			userIds.addAll(getUserIds(users));
		}
		if (CollectionUtils.isEmpty(userIds)) {
			return new BasicRes();
		}

		// Sync the calendar for each user
		int mismatchedCount = 0;
		int mismatchedUsers = 0;
		for (String userId : userIds) {
			try {
				List<String> mismatchedEntries = syncUserCalendar(Long.parseLong(userId), req.getRole(),
						req.getStartTime(), req.getEndTime());
				if (!CollectionUtils.isEmpty(mismatchedEntries)) {
					mismatchedCount += mismatchedEntries.size();
					mismatchedUsers++;
				}
			} catch (Exception ex) {
				logger.error("Error syncing the user calendar: " + req.toString() + " userId:" + userId + " ex:"
						+ ex.toString() + " message:" + ex.getMessage());
			}
		}

		if (mismatchedCount > 0) {
			logger.error("MismatchedEntries calendar entries : " + mismatchedCount + " users:" + mismatchedUsers);
		}

		return new BasicRes();
	}

	public List<String> syncUserCalendar(Long userId, Role role, long startTime, long endTime) throws VException {
		logger.info("Start syncing calendar for user id : " + userId);

		// Start time and end time must be GTM start time of the day and in future.
		if (startTime < System.currentTimeMillis()) {
			startTime = System.currentTimeMillis();
		}
		startTime = CommonCalendarUtils.getDayStartTime(startTime) + DateTimeUtils.MILLIS_PER_DAY;
		endTime = CommonCalendarUtils.getDayStartTime(endTime) + DateTimeUtils.MILLIS_PER_DAY;
		if (startTime >= endTime) {
			return new ArrayList<>();
		}

		// Fetch all OTO sessions
		Query sessionQuery = new Query();
		if (Role.STUDENT.equals(role)) {
			sessionQuery.addCriteria(Criteria.where(Session.Constants.STUDENT_IDS).in(userId));
		} else {
			sessionQuery.addCriteria(Criteria.where(Session.Constants.TEACHER_ID).is(userId));
		}
		sessionQuery.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.SCHEDULED));
		sessionQuery.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(startTime));
		sessionQuery.addCriteria(Criteria.where(Session.Constants.START_TIME).lte(endTime));
		List<Session> sessions = sessionDAO.runQuery(sessionQuery, Session.class);
		BitSet otoBitSet = new BitSet();
		if (!CollectionUtils.isEmpty(sessions)) {
			otoBitSet = CalendarUtils.getBitSetFromSessions(sessions, startTime);
		}

		// Fetch all OTF sessions
		BitSet otfBitSet = new BitSet();
		List<OTFSessionPojoUtils> otfSessions = getUserOTFSessions(startTime, endTime, role, userId);
		if (!CollectionUtils.isEmpty(otfSessions)) {
			otfBitSet = CalendarUtils.getBitSetOTFFromSessions(otfSessions, startTime);
		}

		// Call sync calendar
		SyncCalendarSessionsReq syncCalendarSessionsReq = new SyncCalendarSessionsReq();
		syncCalendarSessionsReq.setStartTime(startTime);
		syncCalendarSessionsReq.setEndTime(endTime);
		syncCalendarSessionsReq.setUserId(String.valueOf(userId));
		syncCalendarSessionsReq.setSessionBitSet(otoBitSet.toLongArray());
		syncCalendarSessionsReq.setOtfSessionBitSet(otfBitSet.toLongArray());
		List<String> mismatchedEntries = syncCalendarSessions(syncCalendarSessionsReq);
		if (!CollectionUtils.isEmpty(mismatchedEntries)) {
			logger.info("FoundMismatchEntries : " + Arrays.toString(mismatchedEntries.toArray()));
		} else {
			logger.info("NoMismatchResEntries : " + Arrays.toString(mismatchedEntries.toArray()));
		}

		logger.info("End syncing calendar for user id : " + userId);
		return mismatchedEntries;
	}

	private List<String> getUserIds(List<UserInfo> users) {
		List<String> userIds = new ArrayList<>();
		if (!CollectionUtils.isEmpty(users)) {
			for (UserInfo user : users) {
				userIds.add(String.valueOf(user.getUserId()));
			}
		}

		return userIds;
	}

	public List<String> syncCalendarSessions(SyncCalendarSessionsReq req) {
		logger.info("Request : " + req.toString());
		BitSet sessionBitSet = req.fetchSessionBitSet();
		BitSet otfSessionBitSet = req.fetchOTFSessionBitSet();

		sessionBitSet.or(otfSessionBitSet);
		logger.info("Merged session bit set : " + Arrays.toString(sessionBitSet.toLongArray()));

		if (req.getEndTime() == null || req.getEndTime() <= 0l) {
			int size = sessionBitSet.length();
			req.setEndTime(req.getStartTime() + size * Constants.SLOT_LENGTH);
		}

		BitSetEntry bitSetEntry = new BitSetEntry(req.getStartTime(), req.getEndTime(), CalendarEntrySlotState.SESSION,
				Constants.SLOT_LENGTH, sessionBitSet);

		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(req.getUserId()));
		query.addCriteria(
				Criteria.where(CalendarEntry.Constants.DAY_START_TIME).gte(req.getStartTime()).lt(req.getEndTime()));

		List<CalendarEntry> calendarEntries = calendarEntryDAO.runQuery(query, CalendarEntry.class);
		List<String> mismatchEntries = new ArrayList<String>();
		if (!CollectionUtils.isEmpty(calendarEntries)) {
			for (CalendarEntry entry : calendarEntries) {
				logger.info("CalendarEntry : " + entry.toString());
				List<Integer> preUpdateBooked = new ArrayList<>();
				if (!CollectionUtils.isEmpty(entry.getBooked())) {
					preUpdateBooked.addAll(entry.getBooked());
				}

				entry.updateByBitSet(bitSetEntry);
				Set<Integer> postUpdateBooked = entry.getBooked();
				calendarEntryDAO.save(entry);

				// Check if any save is required
				if (!(preUpdateBooked.containsAll(postUpdateBooked) && postUpdateBooked.containsAll(preUpdateBooked))) {
					mismatchEntries.add("Mismatch found for entry : " + entry.toString() + " preUpdateBooked : "
							+ Arrays.toString(preUpdateBooked.toArray()) + " postUpdateBooked : "
							+ Arrays.toString(postUpdateBooked.toArray()));
				}
			}

			logger.info("Mismatch entries : " + Arrays.toString(mismatchEntries.toArray()));
		}
		return mismatchEntries;
	}

	public SessionSlot pickSuggestedSlot(SlotSelectionPojo slotSelectionPojo) {
		List<TagSlots> tagSlots = slotSelectionPojo.tagSlots;
		if (!com.vedantu.util.CollectionUtils.isEmpty(tagSlots)) {
			SessionSlot slot = pickTagSlotByTagName(tagSlots, "evening1");
			if (slot != null) {
				return slot;
			}

			slot = pickTagSlotByTagName(tagSlots, "evening2");
			if (slot != null) {
				return slot;
			}

			slot = pickTagSlotByTagName(tagSlots, "morning1");
			if (slot != null) {
				return slot;
			}

			slot = pickTagSlotByTagName(tagSlots, "morning2");
			if (slot != null) {
				return slot;
			}
		}

		return null;
	}

	public SessionSlot pickTagSlotByTagName(List<TagSlots> tagSlots, String tagName) {
		if (!CollectionUtils.isEmpty(tagSlots)) {
			for (TagSlots slot : tagSlots) {
				SessionSlot sessionSlot = pickTagSlotByTagName(slot, tagName);
				if (sessionSlot != null) {
					return sessionSlot;
				}
			}
		}

		return null;
	}

	public SessionSlot pickTagSlotByTagName(TagSlots tagSlot, String tagName) {
		if (tagSlot != null && tagName.equals(tagSlot.getTagName())
				&& !CollectionUtils.isEmpty(tagSlot.getAvailableSlots())) {
			return tagSlot.getAvailableSlots().get(0);
		}
		return null;
	}

	public SessionSlot pickLatestSlot(List<SessionSlot> slots) {
		SessionSlot result = slots.get(0);
		for (SessionSlot slot : slots) {
			if (slot.getStartTime() < result.getStartTime()) {
				result = slot;
			}
		}

		return result;
	}

	public List<OTFSessionPojoUtils> getUserOTFSessions(long startTime, long endTime, Role role, long userId)
			throws VException {
		logger.info("Request:" + startTime + " endTime:" + endTime + " userId:" + userId);
		String getOTFSessionsUrl = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT")
				+ "/onetofew/session/getUserUpcomingScheduledSessions?startTime=" + startTime + "&endTime=" + endTime;
		if (Role.STUDENT.equals(role)) {
			getOTFSessionsUrl += "&studentId=" + userId;
		} else {
			getOTFSessionsUrl += "&teacherId=" + userId;
		}

		ClientResponse resp = WebUtils.INSTANCE.doCall(getOTFSessionsUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		List<OTFSessionPojoUtils> otfSessions = gson.fromJson(jsonString, otfSessionListType);
		return otfSessions;
	}

	public List<AvailabilitySlotStringRes> getTeacherAvailabilitySlotStrings(Long startTime, Long endTime, SessionLabel type)
			throws VException {
		// generate a list of teacher availability response slot strings
		List<AvailabilitySlotStringRes> availableSlotStrings = new ArrayList<>();

		if (endTime - startTime > CommonCalendarUtils.MILLIS_PER_WEEK) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Availability fetch for duration more than 1 week");
		}

		// TODO load balancing --allocation of teacher slots based on availability using max heap Priority Queue pool
		List<String> teacherIds = fosUtils.getEarlyLearningTeachers(type.toString());
		if (CollectionUtils.isEmpty(teacherIds)) {
			return availableSlotStrings;
		}
		Collections.shuffle(teacherIds);

		List<AvailabilityResponse> availabilityResponses = getTeacherAvailabilityBitSet(teacherIds, startTime, endTime);
		if (!CollectionUtils.isEmpty(availabilityResponses)) {

			availabilityResponses.forEach((ar) -> {

				long[] trueAvailability = Arrays.stream(ar.getTrueAvailability()).mapToLong(i -> i).toArray();
				AvailabilitySlotStringRes res = new AvailabilitySlotStringRes(ar.getTeacherId(), ar.getStartTime(),
						ar.getEndTime(), CalendarEntrySlotState.AVAILABLE, Arrays.stream(trueAvailability).mapToObj(String::valueOf).collect(Collectors.joining("")));

				availableSlotStrings.add(res);
			});
		}

		return availableSlotStrings;
	}
    
    public PlatformBasicResponse updateBatchSessionsCalendar(String batchId, boolean mark) {
		// fetching only upcoming sessions to unmark calendar-- ignored past sessions' unmarking
        List<OTFSession> sessions = otfSessionDAO.getUpcomingSessionsForBatches(Arrays.asList(batchId), 0, 500,
                Arrays.asList(OTFSession.Constants.BATCH_IDS, OTFSession.Constants.PRESENTER,
                        OTFSession.Constants.START_TIME, OTFSession.Constants.END_TIME,
                        OTFSession.Constants.PRIMARY_BATCH_ID));
        if (sessions != null && !sessions.isEmpty()) {
            if (sessions.size() >= 500) {
                logger.error("more than 500 sessions are in batch, please rewrite the api batchId " + batchId);
            }
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessions", sessions);
            AsyncTaskName asyncTaskName = AsyncTaskName.BATCH_SESSIONS_MARK_CALENDAR;
            if (!mark) {
                asyncTaskName = AsyncTaskName.BATCH_SESSIONS_UNMARK_CALENDAR;
            }
            AsyncTaskParams params = new AsyncTaskParams(asyncTaskName, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    public Set<String> getAvailableELTeachersForSlot(Long startTime, Long endTime, SessionLabel earlyLearningCourseType) throws BadRequestException {

		if (endTime - startTime > CommonCalendarUtils.MILLIS_PER_DAY) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Availability fetch for duration more than 1 day");
		}

		Set<String> availableELTeachers = new HashSet<>();
		List<String> allELTeachers = fosUtils.getEarlyLearningTeachers(earlyLearningCourseType.name());

		// TODO use fork join pool to divide&conquer when the size of teachers > 500 -- only 38 at present
		//  -- overload execute/compute with Function
		if (CollectionUtils.isEmpty(allELTeachers)) {
			return availableELTeachers;
		} else if (allELTeachers.size() > 1000) {
			logger.error("Size of el teachers is larger than 1000, change impl");
		}

		List<List<String>> allTeachersPartitions = Lists.partition(allELTeachers, 100);
		logger.info("deblog-- allteachers" + allELTeachers);
		for (List<String> teachersPartition : allTeachersPartitions) {
			logger.info("deblog-- partition " + teachersPartition);
			availableELTeachers.addAll(getAvailableUsers(startTime, endTime, teachersPartition));
		}

		return availableELTeachers;
	}

	// TODO Process in Queue
	public void unblockCalenderEntries(OTFSessionPojoUtils session, List<Long> studentIds) throws VException {
		List<SessionSlot> sessionSlots = new ArrayList<>();
		sessionSlots.add(session.createSessionSlot());
		// Unblock the previous times for teacher
		if (!com.vedantu.util.StringUtils.isEmpty(session.getPresenter())) {
			UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(Long.parseLong(session.getPresenter()),
					sessionSlots, CalendarEntrySlotState.SESSION, false, CalendarReferenceType.OTF_SESSION,
					session.getId());
			unmarkCalendarEntries(updateMultipleSlotsReq);
		}

		// Unblock the previous times for students
		sessionSlots = new ArrayList<>();
		sessionSlots.add(session.createSessionSlot());
		UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(studentIds, sessionSlots,
				CalendarEntrySlotState.SESSION, true, CalendarReferenceType.OTF_SESSION, session.getId());
		unmarkCalendarEntries(updateMultipleSlotsReq);
	}

	public void unmarkCalendarEntries(UpdateMultipleSlotsReq updateMultipleSlotsReq) throws VException {
		logger.info("Request : " + updateMultipleSlotsReq.toString());
		if (org.springframework.util.CollectionUtils.isEmpty(updateMultipleSlotsReq.collectErrors())) {
			logger.info("Invalid request : " + updateMultipleSlotsReq.toString());
			return;
		}

		// TODO : Needs optimization. Will pick it up later
		for (Long userId : updateMultipleSlotsReq.getUserIds()) {
			for (SessionSlot sessionSlot : updateMultipleSlotsReq.getSessionSlots()) {
				AddCalendarEntryReq calendarEntryReq = new AddCalendarEntryReq(String.valueOf(userId),
						sessionSlot.getStartTime(), sessionSlot.getEndTime(),
						com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION,
						updateMultipleSlotsReq.getReferenceType(), updateMultipleSlotsReq.getReferenceId());
				unmarkCalendarEntries(calendarEntryReq);
			}
		}
	}

	public BasicRes unmarkCalendarEntries(AddCalendarEntryReq req) throws NotFoundException {
		logger.info("Request : " + req.toString());

		List<CalendarEntry> calendarEntries = calendarUtilsManager.getCalendarEntries(req.getUserId(), req.getStartTime(), req.getEndTime());
		logger.info("output from get calendar entry in platform with basics {}", gson.toJson(calendarEntries));
		BasicRes basicRes = new BasicRes();
		basicRes.setSuccess(true);

		long start;
		long end;
		for (CalendarEntry calendarEntry : calendarEntries) {
			start = req.getStartTime();
			end = req.getEndTime();

			if (calendarEntry.getDayStartTime() >= req.getStartTime()) {
				start = calendarEntry.getDayStartTime();
			}
			if (req.getEndTime() >= calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY) {
				end = calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY;
			}

			RemoveCalendarEntryReq removeCalendarEntryReq = new RemoveCalendarEntryReq();
			removeCalendarEntryReq.setStartTime(start);
			removeCalendarEntryReq.setEndTime(end);
			removeCalendarEntryReq.setSlotState(req.getState());
			removeCalendarEntryReq.setCalendarEntryId(calendarEntry.getId());
			BasicRes removeCalendarEntrySlotsRes = removeCalendarEntrySlots(removeCalendarEntryReq);
			logger.info("output from remove calendar entry slots in platform with basics {}", removeCalendarEntrySlotsRes);
		}
		return basicRes;
	}
	public AvailabilitySlotStringRes getBookingAvailabilitySlotStrings(Long startTime, Long endTime, SessionLabel earlyLearningCourse) throws VException {
		// generate a list of teacher availability response slot strings
		AvailabilitySlotStringRes availableSlotString = new AvailabilitySlotStringRes(startTime,endTime,CalendarEntrySlotState.AVAILABLE,"0");
		if (endTime - startTime > CommonCalendarUtils.MILLIS_PER_WEEK) {
			logger.error("availability fetch for duration more than 1 week");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Availability fetch for duration more than 1 week");
		}
		List<String> teacherIds = fosUtils.getEarlyLearningTeachers(earlyLearningCourse.toString());
		Long overBookingStartTime = CommonCalendarUtils.getDayStartTime(startTime);
		Long overBookingEndTime = CommonCalendarUtils.getDayEndTime(endTime);
		logger.info("overBookingStartTime : {}   overBookingEndTime : {}",overBookingStartTime,overBookingEndTime);
		List<SlotBookingCount> slotBookingCount = overBookingDAO.getOverBookingAggregationWithSlotTime(overBookingStartTime, overBookingEndTime, earlyLearningCourse );
		logger.info("slotBookingCount : "+slotBookingCount.size());
		Map<Long,Long> slotBookingMapping = new HashMap<>();
		if(!CollectionUtils.isEmpty(slotBookingCount)) {
			slotBookingMapping = slotBookingCount.stream().filter(Objects::nonNull).collect(Collectors.toMap(
					SlotBookingCount::getSlotStartTime,
					SlotBookingCount::getCount
			));
		}
		logger.info("slotBookingMapping : {}",slotBookingMapping);
		if (CollectionUtils.isEmpty(teacherIds)) {
			return availableSlotString;
		}
		OTFSessionManager otfSessionManager = new OTFSessionManager();
		Long dayStartTime =  CommonCalendarUtils.getDayStartTime(startTime);
		double overBookingFactor = OverBooking.getOverBookingFactor(earlyLearningCourse);
		logger.info("overBookingFactor : {}",overBookingFactor);
		String availabiltyInSLotBooking = new String();
		Long currentStartTime = startTime;
		while(dayStartTime < endTime) {

			Long dayEndTime = CommonCalendarUtils.getDayEndTime(dayStartTime);
			List<List<String>> allTeachersPartitions = Lists.partition(teacherIds, 100);
			List<CalendarEntry> calendarEntries = new ArrayList<>();
			for (List<String> teachersPartition : allTeachersPartitions) {
				calendarEntries.addAll(calendarEntryDAO.getTeachersAvailabilityInSlot(dayStartTime, dayEndTime, teachersPartition));
			}
			int startIndex = CommonCalendarUtils.getSlotStartIndex(currentStartTime, dayStartTime);
			int endIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
			if (endTime < dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY) {
				endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, dayStartTime);
			}
			//Calculating Availble teachers for every slot in a week
			Map<Integer, Long> teacherAvailableInSlot =  calculateTeacherAvailabilityCountForSlots(calendarEntries);
			logger.info("startIndex : "+startIndex);
			logger.info("endIndex : "+endIndex);
			for(int slotIndex = startIndex; slotIndex <= endIndex; slotIndex++) {

				Long teacherCountForSlot = Optional.ofNullable(teacherAvailableInSlot.get(slotIndex)).orElse(0L);
				Long slotTime = CommonCalendarUtils.getAbsoluteSlotTime(slotIndex, dayStartTime);
				Long numberOfBookingInSlot = Optional.ofNullable(slotBookingMapping.get(slotTime)).orElse(0L);
				logger.info("slotIndex : {} teacherCountForSlot : {}",slotIndex,teacherCountForSlot);
				logger.info("slotTime : "+otfSessionManager.getFormattedDate(slotTime));
				logger.info("numberOfBookingInSlot : "+numberOfBookingInSlot);
				if(teacherCountForSlot == 0 || !CalendarUtils.validateTime(slotIndex, dayStartTime)) {
					availabiltyInSLotBooking = availabiltyInSLotBooking + "0";

				} else if(Math.floor(teacherCountForSlot * overBookingFactor) <= numberOfBookingInSlot){
					availabiltyInSLotBooking = availabiltyInSLotBooking + "0000";
					slotIndex = slotIndex + 3;
				} else {
					availabiltyInSLotBooking = availabiltyInSLotBooking + "1";
				}
			}
			dayStartTime = dayStartTime + CommonCalendarUtils.MILLIS_PER_DAY;
			currentStartTime = dayStartTime;
		}
		availableSlotString = new AvailabilitySlotStringRes(startTime, endTime, CalendarEntrySlotState.AVAILABLE, availabiltyInSLotBooking);
		return availableSlotString;
	}

	public Map<Integer, Long> calculateTeacherAvailabilityCountForSlots(List<CalendarEntry> calendarEntries) {

		int dayStartIndex = 0;
		int dayEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;

		Map<Integer, List<String>> availableTeachersInSlotIndex = new HashMap<>();
		Map<Integer, Long> teacherAvailableInSlot = new HashMap<>();
		logger.info("calendarEntries size : "+calendarEntries.size());

		for (int index=0; index<calendarEntries.size() ;index++) {

			CalendarEntry calendarEntry = calendarEntries.get(index);
			Set<Integer> availablityIndex = getTeacherAvailibilityIndexes(calendarEntry);
			for(int availableIndex = dayStartIndex; availableIndex <= dayEndIndex;  availableIndex++) {

				if(availablityIndex.contains(availableIndex)) {
					List<String> teacherIds = availableTeachersInSlotIndex.get(availableIndex);
					if(CollectionUtils.isEmpty(teacherIds)) {
						teacherIds = new ArrayList<>();
						teacherIds.add(calendarEntry.getUserId());
					} else {
						teacherIds.add(calendarEntry.getUserId());
					}
					availableTeachersInSlotIndex.put(availableIndex,teacherIds);
				}
			}
		}

		//This logic for checking if the teacher is availble for whole 1 hour
		//this logic will break if the session time will not start from Intger number like 9,,10
		//In future if the demo time is flexible, take the frontend demo booking slot start time like 8:30 8:45 and calcualte the index below
		for(int index=dayStartIndex+2 ;index<=dayEndIndex; index=index+4) {

			Set<String> availableTeachersIntersection = new HashSet<>();
			availableTeachersIntersection.addAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index)).orElseGet(ArrayList::new));
			availableTeachersIntersection.retainAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index+1)).orElseGet(ArrayList::new));
			availableTeachersIntersection.retainAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index+2)).orElseGet(ArrayList::new));
			availableTeachersIntersection.retainAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index+3)).orElseGet(ArrayList::new));

			teacherAvailableInSlot.put(index,Long.valueOf(availableTeachersIntersection.size()));
			teacherAvailableInSlot.put(index+1,Long.valueOf(availableTeachersIntersection.size()));
			teacherAvailableInSlot.put(index+2,Long.valueOf(availableTeachersIntersection.size()));
			teacherAvailableInSlot.put(index+3,Long.valueOf(availableTeachersIntersection.size()));
		}
		return teacherAvailableInSlot;
	}

	public Set<Integer> getTeacherAvailibilityIndexes(CalendarEntry calendarEntry){

		Set<Integer> booked = calendarEntry.getBooked();
		if(!CollectionUtils.isEmpty(calendarEntry.getAvailability()) && !CollectionUtils.isEmpty(calendarEntry.getBooked())){
			calendarEntry.getAvailability().removeAll(calendarEntry.getBooked());
		}
		if(!CollectionUtils.isEmpty(calendarEntry.getAvailability()) && !CollectionUtils.isEmpty(calendarEntry.getSessionRequest())) {
			calendarEntry.getAvailability().removeAll(calendarEntry.getSessionRequest());
		}
		return calendarEntry.getAvailability();

	}

	public List<String> getEarlyLearningAvailableTeachers(Long startTime, Long endTime, SessionLabel earlyLearningCourseType) throws BadRequestException {

		if (endTime - startTime > CommonCalendarUtils.MILLIS_PER_DAY) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Availability fetch for duration more than 1 day");
		}

		List<String> availableELTeachers = new ArrayList<>();
		List<String> allELTeachers = fosUtils.getEarlyLearningTeachers(earlyLearningCourseType.name());
		// TODO use fork join pool to divide&conquer when the size of teachers > 500
		//  -- overload execute/compute with Function
		if (CollectionUtils.isEmpty(allELTeachers)) {
			return availableELTeachers;
		} else if (allELTeachers.size() > 1000) {
			logger.error("Size of el teachers is larger than 1000, change impl");
		}
		synchronized (this){
			List<List<String>> allTeachersPartitions = Lists.partition(allELTeachers, 100);
			for (List<String> teachersPartition : allTeachersPartitions) {
				logger.info("deblog-- partition " + teachersPartition);
				availableELTeachers.addAll(getAvailableUsers(startTime, endTime, teachersPartition));
			}

		}

		return availableELTeachers;
	}

}
