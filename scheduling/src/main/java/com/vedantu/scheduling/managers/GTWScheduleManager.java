package com.vedantu.scheduling.managers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.pojo.BatchEnrolmentInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import com.vedantu.scheduling.dao.entity.GTTSessionRecordingInfo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.scheduling.dao.serializers.GTTOrganizerTokenDAO;
import com.vedantu.scheduling.dao.serializers.GTTSessionRecordingInfoDAO;
import com.vedantu.scheduling.pojo.GTTGetRegistrantDetailsRes;
import com.vedantu.scheduling.pojo.GTTRecording;
import com.vedantu.scheduling.pojo.GTTRegisterAttendeeRes;
import com.vedantu.scheduling.pojo.GTWSessionAttendanceInfo;
import com.vedantu.scheduling.pojo.GTWSessionInfoRes;
import com.vedantu.scheduling.request.GTWCreateSessionReq;
import com.vedantu.scheduling.request.GTWRegisterAttendeeReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service("gtwScheduleManager")
public class GTWScheduleManager extends GTTScheduleManager {

	@Autowired
	private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

	@Autowired
	private GTTSessionRecordingInfoDAO gttSessionRecordingInfoDAO;
        
	@Autowired
	private GTTOrganizerTokenDAO gttOrganizerTokenDAO;
	
	@Autowired
	private OTFSessionManager oTFSessionManager;

	@Autowired
	private OTFSessionTaskManager otfSessionTaskManager;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GTTScheduleManager.class);

	private static Gson gson = new Gson();
	public static String citrixURL = ConfigUtils.INSTANCE.getStringValue("otf.citrix.url");

	private static Type GTW_SESSION_LIST_TYPE = new TypeToken<List<GTWSessionInfoRes>>() {
	}.getType();

	private static Type GTW_SESSION_ATTENDEE_LIST_TYPE = new TypeToken<List<GTWSessionAttendanceInfo>>() {
	}.getType();

	private String[] authAccessArray = { "gtwSeat1" };
	// ,
	protected static Map<String, String> overwriteAccessKeys = Collections
			.unmodifiableMap(new HashMap<String, String>() {

				private static final long serialVersionUID = 4675157667161370845L;

				{

					put("gtwSeat1", "iVds7GcTFK1wbCvR3WBmnYEYtdPf"); // academics@vedantu.com

				}
			});

	protected static Map<String, String> organizerKeys = Collections.unmodifiableMap(new HashMap<String, String>() {
		private static final long serialVersionUID = 2357808695344388664L;

		{

			put("gtwSeat1", "7470416618302296069"); // gtwSeat1
		}
	});

	private List<String> authAccessList = Arrays.asList(authAccessArray);

	@Override
	public int getAccessArrayLength() {
		return authAccessArray.length;
	}

	@Override
	public String getAccessToken(int slot) {
		if (slot != -1 && slot < authAccessList.size()) {
			return authAccessList.get(slot);
		}
		return null;
	}

	@Override
	public int getAccessTokenSlot(String accessToken) {
		int returnVal = -1;
		if (!StringUtils.isEmpty(accessToken)) {
			returnVal = authAccessList.indexOf(accessToken);
		}
		return returnVal;
	}

	@Override
	public String getPostOverwriteAccessToken(String accessToken) {
		if (overwriteAccessKeys.containsKey(accessToken)) {
			return overwriteAccessKeys.get(accessToken);
		} else {
                    GTTOrganizerToken token = gttOrganizerTokenDAO.getTokenForSeat(accessToken);
                    if (token != null && !StringUtils.isEmpty(token.getOrganizerAccessToken())) {
                        return token.getOrganizerAccessToken();
                    } else {
                        return accessToken;
                    }
		}
	}

	@Override
	public BitSet getScheduleBitSet(Schedule schedule) {
		return schedule.getGtwScheduleBitSet();
	}

	@Override
	public String getOrganizerKeyForToken(String accessToken) {
		return organizerKeys.get(accessToken);
	}

	@Override
	public void createSessionLink(OTFSession oTFSession) throws InternalServerErrorException, BadRequestException {
		// Validate session for creation
		if (oTFSession.getStartTime() < System.currentTimeMillis()) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session is in past");
		}

		// https://api.getgo.com/G2W/rest/organizers/9017557036820883206/webinars
		String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
		String organizerKey = getOrganizerKeyForToken(oTFSession.getOrganizerAccessToken());
		if (StringUtils.isEmpty(organizerAccessToken) || StringUtils.isEmpty(organizerKey)) {
			String errorMessage = "Organizertoken or key is missing :" + organizerAccessToken + " key : " + organizerKey
					+ " session:" + oTFSession.toString();
			logger.error(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		String createSessionUrl = getCreateSessionUrl(organizerKey);
		GTWCreateSessionReq gtwCreateSessionReq = new GTWCreateSessionReq(oTFSession, organizerAccessToken,
				organizerKey);
		logger.info("Request : " + gtwCreateSessionReq.toString());
		ClientResponse cRes = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
				gson.toJson(gtwCreateSessionReq), organizerAccessToken);
		String resp = cRes.getEntity(String.class);
		switch (cRes.getStatus()) {
		case 200:
		case 201:
			break;
		/*case 429:
			try {
				Thread.sleep(1000);
				createSessionLink(oTFSession);
				return;
			} catch (Exception ex) {
				logger.info("Exception ex:" + ex.toString() + " message:" + ex.getMessage());
			}*/
		case 400:
			String errorMessage = "Invalid response from citrix during creation of training : "
					+ cRes.getEntity(String.class) + " session:" + oTFSession.toString() + " req:"
					+ gson.toJson(gtwCreateSessionReq);
			logger.error(errorMessage);
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
		default:
			String defaultErrorMessage = "Invalid response from citrix during creation of training : " + resp
					+ " session:" + oTFSession.toString() + " req:" + gson.toJson(gtwCreateSessionReq);
			logger.error(defaultErrorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, defaultErrorMessage);
		}

		logger.info("createLinks clientResponse : " + resp);
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = jsonParser.parse(resp).getAsJsonObject();
		String webinarKey = jsonObject.get("webinarKey").getAsString();
		oTFSession.setMeetingId(webinarKey);
		oTFSession.setSessionURL(getTrainingUrl(webinarKey, organizerKey));
	}

	@Override
	public GTTAttendeeDetails registerAttendee(OTFSession oTFSession, UserBasicInfo user, String organizerKey,
											   String meetingId, List<EnrollmentPojo> enrollmentPojos, String organizerAccessToken) throws NotFoundException, InternalServerErrorException {
		GTTAttendeeDetails gttAttendeeDetails = null;

		// Check if meeting id exists
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED, "OTF meeting not scheduled");
		}

		String serverUrl = getRegisterUrl(organizerKey, meetingId);
		GTWRegisterAttendeeReq gtwRegisterAttendeeReq = new GTWRegisterAttendeeReq(user);
		logger.info("GTT register req : " + gtwRegisterAttendeeReq.toString());
                if(StringUtils.isEmpty(organizerAccessToken)){
                    organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
                }
		ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, gson.toJson(gtwRegisterAttendeeReq),
				organizerAccessToken);
		logger.info("Response is: " + cRes.toString());
		String resp = cRes.getEntity(String.class);
		switch (cRes.getStatus()) {
		case 201:
			GTTRegisterAttendeeRes gttRegisterAttendeeRes = gson.fromJson(resp, GTTRegisterAttendeeRes.class);

			// Update the gtt attendee details
			gttAttendeeDetails = gttAttendeeDetailsManager.createAttendeeDetails(gttRegisterAttendeeRes,
					organizerAccessToken, organizerKey, oTFSession, user.getUserId(),enrollmentPojos);

			break;
		case 409:
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(resp).getAsJsonObject();
			String registrantKey = jsonObject.get("registrantKey").getAsString();
			GTTGetRegistrantDetailsRes registrantDetailsRes = getRegistrantDetails(registrantKey, meetingId,
					organizerKey, oTFSession,organizerAccessToken);

			gttAttendeeDetails = gttAttendeeDetailsManager.createAttendeeDetails(registrantDetailsRes,
					organizerAccessToken, organizerKey, oTFSession,enrollmentPojos);

			break;
		/*case 429:
			try {
				Thread.sleep(1000);
				return registerAttendee(oTFSession, user, organizerKey, meetingId,enrollmentId);
			} catch (Exception ex) {
				logger.info("Exception ex:" + ex.toString() + " message:" + ex.getMessage());
			}*/

		default:
			String errorMessage = "Error occured while registering the student. response : " + cRes.toString() + "user:"
					+ user.toString() + " session:" + oTFSession.toString();
			logger.info(errorMessage);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
		}

		return gttAttendeeDetails;
	}

	private GTTGetRegistrantDetailsRes getRegistrantDetails(String registrantKey, String trainigKey,
			String organizerKey, OTFSession oTFSession,String organizerAccessToken) throws NotFoundException {

		String serverUrl = getRegistrantUrl(registrantKey, trainigKey, organizerKey);
		logger.info("url : " + serverUrl);
                if(StringUtils.isEmpty(organizerAccessToken)){
                    organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
                }
		ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
				organizerAccessToken);
		/*if (cRes.getStatus() == 429) {
			try {
				Thread.sleep(1000);
				return getRegistrantDetails(registrantKey, trainigKey, organizerKey, oTFSession);
			} catch (Exception ex) {
				logger.info("Exception ex:" + ex.toString() + " message:" + ex.getMessage());
			}
		}*/
		if (cRes.getStatus() != 200) {
			String errorMessage = "Error fetching the registrant details - status code not 200. response : "
					+ cRes.toString() + " req: " + registrantKey + " trainigKey:" + trainigKey + " organizerKey:"
					+ organizerKey + " session:" + oTFSession.toString();
			logger.info(errorMessage);
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Registrant not found : " + errorMessage);
		}
		logger.info("Response is: " + cRes.toString());
		String resp = cRes.getEntity(String.class);
		GTTGetRegistrantDetailsRes registrantDetailsRes = gson.fromJson(resp, GTTGetRegistrantDetailsRes.class);
		logger.info("Registrant details : " + registrantDetailsRes.toString());
		return registrantDetailsRes;
	}

	@Override
	public List<GTTRecording> getRecordingDownloadUrl(OTFSession oTFSession)
			throws NotFoundException, InternalServerErrorException {
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			throw new NotFoundException(ErrorCode.OTF_MEETING_NOT_SCHEDULED, "Meeting not scheduled for the session");
		}

		// Check if already exists in db
		GTTSessionRecordingInfo existingEntry = gttSessionRecordingInfoDAO
				.getGTTSessionRecordingInfo(oTFSession.getId(), oTFSession.getMeetingId());
		if (existingEntry != null) {
			return existingEntry.getGttRecordings();
		} else {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
					"Recording not found for session " + oTFSession.getId());
		}
	}

	@Override
	public void handleFeedback(OTFSession oTFSession) throws VException {
		logger.info("Request:" + oTFSession.getId());
		if (StringUtils.isEmpty(oTFSession.getMeetingId())) {
			logger.info("No training is scheduled for this session");
			return;
		}

		if (oTFSession.getEndTime() > System.currentTimeMillis()) {
			logger.info("Too early to handle feedback for this session");
			return;
		}
                // Fetch enrolled students
                List<String> enrolledStudents=new ArrayList<>();
                if (ArrayUtils.isNotEmpty(oTFSession.getBatchIds())) {
					List<BatchEnrolmentInfo> batches = otfSessionTaskManager.getBatchEnrolmentsByIds(oTFSession.getBatchIds());
                        if (ArrayUtils.isNotEmpty(batches)) {
                            for (BatchEnrolmentInfo batch : batches) {
                                if (batch != null && !CollectionUtils.isEmpty(batch.getEnrolledStudentIds())) {
                                    enrolledStudents.addAll(batch.getEnrolledStudentIds());
                                }
                            }
                        }
                }
		String organizerAccessToken = getPostOverwriteAccessToken(oTFSession.getOrganizerAccessToken());
		String organizerKey = getOrganizerKeyForToken(oTFSession.getOrganizerAccessToken());
		String sessionUrl = getSessionsUrl(organizerKey, oTFSession.getMeetingId());
		logger.info("SessionUrl:" + sessionUrl);

		ClientResponse cRes = WebUtils.INSTANCE.doCall(sessionUrl, HttpMethod.GET, null,
				organizerAccessToken);
		if (cRes.getStatus() != 200) {
			String errorMessage = "Error fetching the sessions- status code not 200. response : " + cRes.toString()
					+ " req: " + oTFSession.toString();
			logger.info(errorMessage);
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Registrant not found : " + errorMessage);
		}

		logger.info("Response is: " + cRes.toString());
		String resp = cRes.getEntity(String.class);
		if (StringUtils.isEmpty(resp)) {
			logger.info(
					"Emptry response while fetching sessions:" + cRes.toString() + " session:" + oTFSession.toString());
		}
		List<GTWSessionInfoRes> gtwSessions = gson.fromJson(resp, GTW_SESSION_LIST_TYPE);
		List<String> errors = new ArrayList<>();
		List<GTTAttendeeDetails> gttAttendeeDetailsList = gttAttendeeDetailsManager
				.getAttendeeDetailsByTraining(oTFSession.getMeetingId());
		Map<String, GTTAttendeeDetails> attendeeMap = createAttendeeMap(gttAttendeeDetailsList);
		Map<String, GTTAttendeeDetails> updatedAttendeeMap = new HashMap<>();

		if (ArrayUtils.isNotEmpty(gtwSessions)) {
			for (GTWSessionInfoRes gtwSession : gtwSessions) {

				String attendeeUrl = getAttendeesUrl(organizerKey, oTFSession.getMeetingId(),
						gtwSession.getSessionKey());

				ClientResponse cAttendeeRes = WebUtils.INSTANCE.doCall(attendeeUrl, HttpMethod.GET, null,
						organizerAccessToken);
				if (cAttendeeRes.getStatus() != 200) {
					String errorMessage = "Error fetching the attendees details- status code not 200. response : "
							+ cAttendeeRes.toString() + " req: " + oTFSession.toString();
					logger.info(errorMessage);
					throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Registrant not found : " + errorMessage);
				}

				logger.info("Response is: " + cAttendeeRes.toString());
				String attendeeResp = cAttendeeRes.getEntity(String.class);
				logger.info("AttendeeResp:" + attendeeResp);

				if (!StringUtils.isEmpty(attendeeResp)) {
					List<GTWSessionAttendanceInfo> attendeeInfos = gson.fromJson(attendeeResp,
							GTW_SESSION_ATTENDEE_LIST_TYPE);
					if (ArrayUtils.isNotEmpty(attendeeInfos)) {
						for (GTWSessionAttendanceInfo attendeeInfo : attendeeInfos) {
							String userId = GTTAttendeeDetailsManager.extractUserId(attendeeInfo.getEmail());
							if (StringUtils.isEmpty(userId)
									|| !enrolledStudents.contains(userId)) {
								continue;
							}

							GTTAttendeeDetails gttAttendeeDetails = updatedAttendeeMap.get(userId);
							if (gttAttendeeDetails == null) {
								gttAttendeeDetails = attendeeMap.get(userId);
							}

							if (gttAttendeeDetails == null) {
								gttAttendeeDetails = new GTTAttendeeDetails();
								gttAttendeeDetails.setUserId(userId);
								gttAttendeeDetails.setOrganizerAccessToken(oTFSession.getOrganizerAccessToken());
								gttAttendeeDetails.setOrganizerKey(organizerKey);
								gttAttendeeDetails.setTraningId(oTFSession.getMeetingId());
								gttAttendeeDetails.setSessionId(oTFSession.getId());
							}

							try {
								gttAttendeeDetails.updateSessionAttendeeInfo(attendeeInfo);
								// todo enable for gamification
								// gttAttendeeDetailsManager.processGttAttendeeForGameAsync(gttAttendeeDetails);
								attendeeMap.put(userId, gttAttendeeDetails);
								updatedAttendeeMap.put(userId, gttAttendeeDetails);
							} catch (Exception ex) {
								String errorMessage = "Error updating gtt attendee details ex:" + ex.toString()
										+ " message:" + ex.getMessage() + " req:" + attendeeInfo.toString();
								logger.info(errorMessage);
								errors.add(errorMessage);
							}
						}
					}
				}
			}
		}

		if (ArrayUtils.isNotEmpty(errors)) {
			logger.error("Error occured updating gtt attendee report : " + Arrays.toString(errors.toArray()));
		}

		if (!updatedAttendeeMap.isEmpty()) {
			logger.info("Updated entries : " + updatedAttendeeMap.size() + " session:" + oTFSession.toString());
			List<String> attendees = new ArrayList<>();
			for (GTTAttendeeDetails gttAttendeeDetails : updatedAttendeeMap.values()) {
				logger.info("Gtt attendee details:" + gttAttendeeDetails.toString());
				gttAttendeeDetailsManager.updateGTTAttendeeDetails(gttAttendeeDetails);
				if (!CollectionUtils.isEmpty(gttAttendeeDetails.getActiveIntervals())
						&& !attendees.contains(gttAttendeeDetails.getUserId())) {
					attendees.add(gttAttendeeDetails.getUserId());
				}
			}

			oTFSession.setAttendees(attendees);
			oTFSession.addFlag(OTFSessionFlag.POST_SESSION_DATA_PROCESSED);
		} else {
			logger.info("No entries updated for session:" + oTFSession.toString());
		}
	}

	private static Map<String, GTTAttendeeDetails> createAttendeeMap(List<GTTAttendeeDetails> attendees) {
		Map<String, GTTAttendeeDetails> attendeeMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(attendees)) {
			for (GTTAttendeeDetails gttAttendeeDetails : attendees) {
				gttAttendeeDetails.resetFeedbackData();
				attendeeMap.put(gttAttendeeDetails.getUserId(), gttAttendeeDetails);
			}
		}

		return attendeeMap;
	}

	private static String getCreateSessionUrl(String organizerKey) {
		return citrixURL + "/G2W/rest/organizers/" + organizerKey + "/webinars";

	}

	private static String getTrainingUrl(String trainingId, String organizerKey) {
		return citrixURL + "/G2W/rest/organizers/" + organizerKey + "/webinars/" + trainingId;
	}

	private static String getRegisterUrl(String organizerKey, String trainingKey) {
		return citrixURL + "/G2W/rest/organizers/" + organizerKey + "/webinars/" + trainingKey + "/registrants";
	}

	private static String getRegistrantUrl(String registrantKey, String trainigKey, String organizerKey) {
		return citrixURL + "/G2W/rest/organizers/" + organizerKey + "/webinars/" + trainigKey + "/registrants/"
				+ registrantKey;
	}

	private static String getAttendeesUrl(String organizerKey, String trainingKey, String sessionKey) {
		return citrixURL + "/G2W/rest/organizers/" + organizerKey + "/webinars/" + trainingKey + "/sessions/"
				+ sessionKey + "/attendees";
	}

	private static String getSessionsUrl(String organizerKey, String trainingKey) {
		return citrixURL + "/G2W/rest/organizers/" + organizerKey + "/webinars/" + trainingKey + "/sessions";
	}

	// /organizers/{organizerKey}/webinars/{webinarKey}/sessions
}
