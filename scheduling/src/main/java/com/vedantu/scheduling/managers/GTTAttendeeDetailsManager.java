package com.vedantu.scheduling.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.onetofew.request.StudentPollDataReq;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.ReplayAttendee;
import com.vedantu.scheduling.dao.serializers.GTTAttendeeDetailsDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.dao.serializers.ReplayAttendeeDAO;
import com.vedantu.scheduling.enums.AttendanceFeedbackTag;
import com.vedantu.scheduling.pojo.AggregatedGTTAttendeeCountForSession;
import com.vedantu.scheduling.pojo.AttendanceFeedback;
import com.vedantu.scheduling.pojo.GTTAggregatedStudentList;
import com.vedantu.scheduling.pojo.GTTGetRegistrantDetailsRes;
import com.vedantu.scheduling.pojo.GTTRegisterAttendeeRes;
import com.vedantu.scheduling.pojo.ReplayWatchInfo;
import com.vedantu.scheduling.pojo.ReplayWatchedDuration;
import com.vedantu.scheduling.request.AbsentSessionFeedbackReq;
import com.vedantu.scheduling.request.UpdateReplayWatchedDurationReq;
import com.vedantu.subscription.pojo.game.GameSessionActivity;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.pojo.DeviceDetails;
import com.vedantu.util.pojo.DeviceType;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.threadutil.CachedThreadPoolUtility;
import com.vedantu.util.threadutil.ProducerConsumer;
import nl.basjes.parse.useragent.UserAgent;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GTTAttendeeDetailsManager {

    private static final String EMAIL_SUFFIX_SLUG = "@user.com";
    private static final int MAX_FETCH_SIZE = 1000;
    private final int GTT_PARTITION_SIZE = 100;

    @Autowired
    private GTTAttendeeDetailsDAO gttAttendeeDetailsDAO;

    @Autowired
    private ReplayAttendeeDAO replayAttendeeDAO;


    @Autowired
    private OTFSessionDAO oTFSessionDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private CachedThreadPoolUtility cachedThreadPoolUtility;

    @Autowired
    private UserAgentParser userAgentParser;

    private Logger logger = LogFactory.getLogger(GTTAttendeeDetailsManager.class);

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private static final Gson gson = new Gson();

    @PostConstruct
    private void init() {
        List<Bson> pipeline = Collections.singletonList(Aggregates.match(Filters.or(Filters.in("operationType", Arrays.asList("insert", "update", "replace")))));
//        gttAttendeeDetailsDAO.watchDocument(this::processGttAttendee, GTTAttendeeDetails.class, pipeline);
    }

    public void queueAttendeesAsBulk(List<GTTAttendeeDetails> list) {
        final SQSMessageType task = SQSMessageType.GTT_BULK_UPDATE_TASK;
        final List<List<GTTAttendeeDetails>> partition = Lists.partition(list, GTT_PARTITION_SIZE);
        for (List<GTTAttendeeDetails> details : partition) {
            awsSQSManager.sendToSQS(task.getQueue(), task, gson.toJson(details));
        }
    }

    public void queueAttendees(List<GTTAttendeeDetails> attendeeDetails) {
        if (attendeeDetails.size() > GTT_PARTITION_SIZE) {
            queueAttendeesAsBulk(attendeeDetails);
            return;
        }
        for (GTTAttendeeDetails detail : attendeeDetails) {
            queueAttendee(detail);
        }
    }

    public void queueAttendee(GTTAttendeeDetails detail) {
        final SQSMessageType task = SQSMessageType.GTT_UPDATE_TASK;
        final String groupId = detail.getSessionId() + detail.getUserId();
//        awsSQSManager.sendToSQS(task.getQueue(), task, gson.toJson(detail), groupId);
        saveEntityFromQueue(detail);
    }

    public void saveEntityFromQueue(GTTAttendeeDetails details) {
        gttAttendeeDetailsDAO.save(details);
        // todo enable for gamification
        // processGttAttendeeForGameAsync(details);
    }

    public class ItrGameSession implements Iterator<List<GTTAttendeeDetails>> {

        private final int limit = 300;
        private final AtomicLong skip = new AtomicLong(0);
        private final AtomicLong count = new AtomicLong(0);
        private final long fromTime, toTime;

        public ItrGameSession(long fromTime, long toTime) {
            this.fromTime = fromTime;
            this.toTime = toTime;
        }

        @Override
        public boolean hasNext() {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<GTTAttendeeDetails> next() {
            //noinspection SynchronizeOnNonFinalField
            synchronized (gttAttendeeDetailsDAO) {
                List<GTTAttendeeDetails> list = gttAttendeeDetailsDAO.gttActivePaidAttendeeDetailsBetween(fromTime, toTime, skip.get(), limit);
                if (ArrayUtils.isEmpty(list)) {
                    throw new NoSuchElementException();
                }
                skip.addAndGet(limit);
                count.addAndGet(list.size());
                logger.info("Gamification Cron total processed count " + count.get());
                return list;
            }
        }
    }

    public void processGameSessionCronAsync() {
        final long currentTime = System.currentTimeMillis();
        final long normalizedTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        final long fromTime = normalizedTime - DateTimeUtils.MILLIS_PER_MINUTE;
        final long toTime = normalizedTime - 1;

        logger.info("Gamification Cron Current Time " + currentTime);
        logger.info("Gamification Cron Normalised Time " + normalizedTime);
        logger.info("Gamification Query From Time " + fromTime);
        logger.info("Gamification Query To Time " + toTime);


        ArrayBlockingQueue<List<GTTAttendeeDetails>> queue = new ArrayBlockingQueue<>(5);
        ProducerConsumer<List<GTTAttendeeDetails>> producerConsumer = new ProducerConsumer<>(queue,
                () -> new ItrGameSession(fromTime, toTime), this::consumeGttAttendeeBatch);

        producerConsumer.start(2, 4);

        logger.info("Gamification Cron Finished @ " + System.currentTimeMillis());
        logger.info("Gamification Cron took time of " + (System.currentTimeMillis() - currentTime));
    }

    public void consumeGttAttendeeBatch(List<GTTAttendeeDetails> list) {
        List<GameSessionActivity> activities = list.stream()
                .filter(Objects::nonNull)
                .filter(this::isValidForGame)
                .map(this::getGameSessionActivity)
                .collect(Collectors.toList());
        logger.info("Total valid activities " + activities.size());
        List<List<GameSessionActivity>> lists = Lists.partition(activities, 50);
        for (List<GameSessionActivity> activityList : lists) {
            logger.info("Sending activity batch " + activityList);
            awsSQSManager.sendToSQS(SQSQueue.GAME_JOURNEY_SESSION_STANDARD, SQSMessageType.GAME_SESSION_ACTIVITY_BATCH, gson.toJson(activityList));
        }
    }

    public void processGttAttendeeForGameAsync(GTTAttendeeDetails attendeeDetails) {
        cachedThreadPoolUtility.execute(() -> processGttAttendee(attendeeDetails));
    }

    private void processGttAttendee(GTTAttendeeDetails attendeeDetails) {
        try {
            logger.info("WACTHED GTT SESSSION ATTENDEE " + attendeeDetails.getId());
            attendeeDetails = gttAttendeeDetailsDAO.getById(attendeeDetails.getId());
            logger.info("GTT SESSSION ATTENDEE " + attendeeDetails);

            if (attendeeDetails.getEntityState() == EntityState.ACTIVE && StringUtils.isNotEmpty(attendeeDetails.getEnrollmentId())) {
                GameSessionActivity activity = getGameSessionActivity(attendeeDetails);
                logger.info("SESSION ACTIVITY " + activity);
                String message = gson.toJson(activity);
                if (isValidForGame(attendeeDetails)) {
                    logger.info("SESSION ACTIVITY VALID FOR GAME");
                    awsSQSManager.sendToSQS(SQSQueue.GAME_JOURNEY_SESSION_STANDARD, SQSMessageType.GAME_SESSION_ACTIVITY, message,
                            attendeeDetails.getUserId());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private GameSessionActivity getGameSessionActivity(GTTAttendeeDetails attendeeDetails) {
        List<ReplayWatchedDuration> durations = attendeeDetails.getReplayWatchedDurations();
        ReplayWatchedDuration duration = ArrayUtils.isNotEmpty(durations) ? durations.get(durations.size() - 1)
                : new ReplayWatchedDuration();

        if (attendeeDetails.getSessionStartTime() == null || attendeeDetails.getSessionEndTime() == null) {
            List<OTFSession> sessions = oTFSessionDAO.getOTFSessionById(attendeeDetails.getSessionId(),
                    Arrays.asList(OTFSession.Constants.START_TIME, OTFSession.Constants.END_TIME));
            if (ArrayUtils.isNotEmpty(sessions)) {
                OTFSession session = sessions.get(0);
                attendeeDetails.setSessionStartTime(session.getStartTime());
                attendeeDetails.setSessionEndTime(session.getEndTime());
            } else {
                logger.error("no session id found for {}", attendeeDetails.getSessionId());
            }
        }

        GameSessionActivity activity = GameSessionActivity.builder()
                .userId(Long.valueOf(attendeeDetails.getUserId()))
                .sessionId(attendeeDetails.getSessionId())
                .enrollmentId(attendeeDetails.getEnrollmentId())
                .sessionEndTime(attendeeDetails.getSessionEndTime())
                .sessionStartTime(attendeeDetails.getSessionStartTime())
                .timeInSession(attendeeDetails.getTimeInSession())
                .lastReplayWatchedDuration(duration.getDuration())
                .lastReplayWatchedTime(duration.getStartTime())
                .attended(ArrayUtils.isNotEmpty(attendeeDetails.getJoinTimes()))
                .build();
        activity.setCreationTime(attendeeDetails.getCreationTime());
        activity.setLastUpdated(attendeeDetails.getLastUpdated());
        activity.setId(attendeeDetails.getId());
        return activity;
    }

    private boolean isValidForGame(GTTAttendeeDetails attendeeDetails) {
        return attendeeDetails != null && (
                (attendeeDetails.getTimeInSession() != null && attendeeDetails.getTimeInSession() > 0) ||
                        ArrayUtils.isNotEmpty(attendeeDetails.getReplayWatchedDurations())
        );
    }

    public GTTAttendeeDetails createAttendeeDetails(GTTGetRegistrantDetailsRes registrantDetailsRes,
                                                    String organizerAccessToken, String organizerKey, OTFSession oTFSession, List<EnrollmentPojo> enrollmentPojos) {
        String registrantKey = registrantDetailsRes.getRegistrantKey();
        GTTAttendeeDetails gttAttendeeDetails = getAttendeeDetails(registrantKey);
        if (gttAttendeeDetails == null) {
            gttAttendeeDetails = new GTTAttendeeDetails(registrantDetailsRes, organizerAccessToken, organizerKey,
                    oTFSession);
            String userId = gttAttendeeDetails.getUserId();
            if (!ArrayUtils.isEmpty(enrollmentPojos)) {
                gttAttendeeDetails.addEnrollmentInfos(enrollmentPojos);
            }
            gttAttendeeDetails.setSessionStartTime(oTFSession.getStartTime());
            gttAttendeeDetails.setSessionEndTime(oTFSession.getEndTime());

            markOldAttendeeEntriesAsDeleted(oTFSession.getId(), userId);
//            gttAttendeeDetailsDAO.update(gttAttendeeDetails);
            queueAttendee(gttAttendeeDetails);
            logger.info("Post update : " + gttAttendeeDetails.toString());
        }

        logger.info("Entry already exists : " + gttAttendeeDetails.toString());
        return gttAttendeeDetails;
    }

    public GTTAttendeeDetails createAttendeeDetails(GTTRegisterAttendeeRes gttRegisterAttendeeRes,
                                                    String organizerAccessToken, String organizerKey, OTFSession oTFSession, Long userId,
                                                    List<EnrollmentPojo> enrollmentPojos) {
        String registrantKey = gttRegisterAttendeeRes.getRegistrantKey();
        GTTAttendeeDetails gttAttendeeDetails = getAttendeeDetails(registrantKey);
        if (gttAttendeeDetails == null) {
            gttAttendeeDetails = new GTTAttendeeDetails(gttRegisterAttendeeRes, organizerAccessToken, organizerKey,
                    oTFSession, String.valueOf(userId));
            markOldAttendeeEntriesAsDeleted(oTFSession.getId(), String.valueOf(userId));
            if (!ArrayUtils.isEmpty(enrollmentPojos)) {
                gttAttendeeDetails.addEnrollmentInfos(enrollmentPojos);
            }
            gttAttendeeDetails.setSessionEndTime(oTFSession.getEndTime());
            gttAttendeeDetails.setSessionStartTime(oTFSession.getStartTime());
//            gttAttendeeDetailsDAO.update(gttAttendeeDetails);
            queueAttendee(gttAttendeeDetails);
            logger.info("Post update : " + gttAttendeeDetails.toString());
        }

        logger.info("Entry already exists : " + gttAttendeeDetails.toString());
        return gttAttendeeDetails;
    }

    private void markOldAttendeeEntriesAsDeleted(String sessionId, String userId) {
        if (sessionId != null && userId != null) {
            logger.info("marking the old sessionId+userId entries as deleted");
            Query query = new Query();
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).is(sessionId));
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
            List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
            if (ArrayUtils.isNotEmpty(attendeeDetails)) {
                for (GTTAttendeeDetails _attendee : attendeeDetails) {
                    _attendee.setEntityState(EntityState.DELETED);
//                    gttAttendeeDetailsDAO.update(_attendee);
                }
                queueAttendeesAsBulk(attendeeDetails);
            }
        }
    }

    public GTTAttendeeDetails getAttendeeDetails(String registrantKey) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.REGISTRATION_KEY).is(registrantKey));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).ne(EntityState.DELETED));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        if (CollectionUtils.isEmpty(attendeeDetails)) {
            return null;
        }

        if (attendeeDetails.size() > 1) {
            logger.error("Multiple entries found for same registrant key : " + registrantKey);
        }
        return attendeeDetails.get(0);
    }

    public List<GTTAttendeeDetails> getAttendeeDetailsByTraining(String trainingId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).is(trainingId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).ne(EntityState.DELETED));
        return gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
    }

    public GTTAttendeeDetails getAttendeeDetails(String sessionId, String trainingId, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).is(sessionId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).is(trainingId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).ne(EntityState.DELETED));


        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        if (CollectionUtils.isEmpty(attendeeDetails)) {
            return null;
        }

        if (attendeeDetails.size() > 1) {
            logger.error("Multiple entries found for same registrant key : " + sessionId + " userId : " + userId);
        }
        return attendeeDetails.get(0);
    }

    public static String generateEmail(long userId) {
        return userId + EMAIL_SUFFIX_SLUG;
    }

    public static String extractUserId(String emailId) {
        String userId = "";
        if (!StringUtils.isEmpty(emailId) && emailId.contains(EMAIL_SUFFIX_SLUG)) {
            userId = emailId.replace(EMAIL_SUFFIX_SLUG, "");
        }

        return userId;
    }

    public void userJoined(OTFSession otfSession, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).is(otfSession.getId()));
        if ( !otfSession.hasVedantuWaveFeatures() &&
                StringUtils.isNotEmpty(otfSession.getMeetingId())) {
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).is(otfSession.getMeetingId()));
        }
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).ne(EntityState.DELETED));

        List<GTTAttendeeDetails> attendees = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        if (CollectionUtils.isEmpty(attendees)) {
            logger.info("Attendee details not found for user : " + userId + " session: " + otfSession.toString());
            return;
        }

        if (attendees.size() > 1) {
            logger.error("Multiple entries found for same user : " + userId + " session : " + otfSession.toString());
        }

        final GTTAttendeeDetails details = attendees.get(0);
        details.addJoinTime();
//        gttAttendeeDetailsDAO.update(attendees.get(0));
        queueAttendee(details);
        logger.info("attendee : " + details.toString());
    }

    public void updateGTTAttendeeDetails(GTTAttendeeDetails gttAttendeeDetails) {
//        gttAttendeeDetailsDAO.update(gttAttendeeDetails);
        queueAttendee(gttAttendeeDetails);
    }

    public List<GTTAttendeeDetails> getJoinedAttendeeDetailsOfUser(String userId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where("joinTimes.0").exists(true));
        gttAttendeeDetailsDAO.setFetchParameters(query, start, size);

        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public ReplayWatchInfo calculateReplayWatchDuration(List<ReplayWatchedDuration> durations, UpdateReplayWatchedDurationReq req) {
        logger.info("calculateReplayWatchDuration: " + durations);
        if (durations == null) {
            durations = new ArrayList<>();
        }
        boolean found = false;
        long totalDuration = 0;
        for (ReplayWatchedDuration duration : durations) {
            if (duration.getStartTime() == req.getStartTime()) {
                duration.setDuration(req.getDuration());
                found = true;
            }
            totalDuration += duration.getDuration();
        }
        if (!found) {
            String userAgent = req.getUserAgent();
            DeviceDetails deviceDetails = null;
            if (StringUtils.isNotEmpty(userAgent)) {
                deviceDetails = new DeviceDetails();
                UserAgent details = userAgentParser.getUserAgentDetails(userAgent);
                if (StringUtils.isNotEmpty(details.getValue("DeviceClass"))) {
                    deviceDetails.setDeviceType(DeviceType.valueOfKey(details.getValue("DeviceClass")));
                }
                if (StringUtils.isNotEmpty(details.getValue("DeviceName"))) {
                    deviceDetails.setDeviceName(details.getValue("DeviceName"));
                }

                if (StringUtils.isNotEmpty(details.getValue("DeviceBrand"))) {
                    deviceDetails.setDeviceBrand(details.getValue("DeviceBrand"));
                }
                if (StringUtils.isNotEmpty(details.getValue("AgentName"))) {
                    deviceDetails.setBrowserName(details.getValue("AgentName"));
                }
                if (StringUtils.isNotEmpty(details.getValue("AgentVersion"))) {
                    deviceDetails.setBrowserVersion(details.getValue("AgentVersion"));
                }
                if (StringUtils.isNotEmpty(details.getValue("AgentVersionMajor"))) {
                    deviceDetails.setBrowserVersionMajor(details.getValue("AgentVersionMajor"));
                }
            }
            totalDuration += req.getDuration();
            ReplayWatchedDuration duration = new ReplayWatchedDuration(req.getStartTime(), req.getDuration());
            duration.setDeviceDetails(deviceDetails);
            durations.add(duration);
        }
        long avgDuration = 0;
        if (durations.size() > 0) {
            avgDuration = totalDuration / durations.size();
        }
        ReplayWatchInfo replayWatchInfo = new ReplayWatchInfo(durations, totalDuration, avgDuration);
        logger.info("calculateReplayWatchDuration: " + replayWatchInfo);
        return replayWatchInfo;
    }

    public void updateGttReplayWatchedDuration(UpdateReplayWatchedDurationReq req, GTTAttendeeDetails attendeeDetail) {
        ReplayWatchInfo replayWatchInfo;
        List<ReplayWatchedDuration> durations = attendeeDetail.getReplayWatchedDurations();
        replayWatchInfo = calculateReplayWatchDuration(durations, req);
        Update update = new Update();
        update.set(GTTAttendeeDetails.Constants.REPLAY_WATCHED_DURATIONS, replayWatchInfo.getDurations());
        update.set(GTTAttendeeDetails.Constants.AVG_REPLAY_WATCHED_DURATION, replayWatchInfo.getAvgDuration());
        update.set(GTTAttendeeDetails.Constants.TOTAL_REPLAY_WATCHED_DURATION, replayWatchInfo.getTotalDuration());
        String callingUserId = null;
        if (req.getCallingUserId() != null) {
            callingUserId = String.valueOf(req.getCallingUserId());
        }
        if (EntityState.DELETED.equals(attendeeDetail.getEntityState())) {
//            update.set(GTTAttendeeDetails.Constants.RANDOM_UNIQUE_INDEX, UUID.randomUUID().toString());
            update.unset(GTTAttendeeDetails.Constants.SPARSE_SESSION_USER_ID);
        } else {
//            update.set(GTTAttendeeDetails.Constants.RANDOM_UNIQUE_INDEX, EntityState.ACTIVE.name());
            update.set(GTTAttendeeDetails.Constants.SPARSE_SESSION_USER_ID, attendeeDetail.createSparseSessionUserId());
        }
        gttAttendeeDetailsDAO.updateAttendeeDetail(update, attendeeDetail.getId(), callingUserId);
        // todo enable for gamification
        // processGttAttendeeForGameAsync(attendeeDetail);
    }

    public void updateReplayWatchedDuration(UpdateReplayWatchedDurationReq req) throws VException {
        req.verify();
        OTFSession otfSession = oTFSessionDAO.getById(req.getSessionId());
        if (otfSession == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "otfsession not found:" + req);
        }
        boolean isWebinarSession = otfSession.isWebinarSession();
        GTTAttendeeDetails attendeeDetail = gttAttendeeDetailsDAO.getAttendeeDetail(req.getUserId(), req.getSessionId());

        ReplayWatchInfo replayWatchInfo;
        if (isWebinarSession) {
            if (attendeeDetail != null) {
                updateGttReplayWatchedDuration(req, attendeeDetail);
            }
            logger.info("updateReplayWatchedDuration-webinar session");
            ReplayAttendee replayAttendee = replayAttendeeDAO.getAttendeeDetail(req.getUserId(), req.getSessionId());
            if (replayAttendee == null) {
                replayAttendee = new ReplayAttendee(req.getSessionId(), req.getUserId());
                replayAttendeeDAO.save(replayAttendee);
            }
            logger.info("updateReplayWatchedDuration-replayAttendee: " + replayAttendee);
            replayWatchInfo = calculateReplayWatchDuration(replayAttendee.getReplayWatchedDurations(), req);
            Update update = new Update();
            update.set(ReplayAttendee.Constants.REPLAY_WATCHED_DURATIONS, replayWatchInfo.getDurations());
            update.set(ReplayAttendee.Constants.AVG_REPLAY_WATCHED_DURATION, replayWatchInfo.getAvgDuration());
            update.set(ReplayAttendee.Constants.TOTAL_REPLAY_WATCHED_DURATION, replayWatchInfo.getTotalDuration());
            if (req.getIR() != null) {
                update.set(ReplayAttendee.Constants.IS_IR_ATTENDEE, req.getIR());
            }
            String callingUserId = null;
            if (req.getCallingUserId() != null) {
                callingUserId = String.valueOf(req.getCallingUserId());
            }
            replayAttendeeDAO.updateAttendeeDetail(update, replayAttendee.getId(), callingUserId);
        } else {
            if (attendeeDetail == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "matching gtt attendee details not found.req:" + req);
            }
            updateGttReplayWatchedDuration(req, attendeeDetail);
        }
    }

    public List<GTTAttendeeDetails> getAttendeeDetailsForSessions(Set<String> sessionIds, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 2000;
        }
        gttAttendeeDetailsDAO.setFetchParameters(query, start, size);
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getAttendeeDetailsForSessions(Set<String> sessionIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getAttendeeDetailsForSession(String sessionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).is(sessionId));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getActiveAttendeeDetailsForSessions(Set<String> sessionIds, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        if (StringUtils.isNotEmpty(userId)) {
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        }
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getAttendeeDetailsByTrainings(Set<String> trainingIds) {
        return getAttendeeDetailsByTrainings(trainingIds, null);
    }

    public List<GTTAttendeeDetails> getAttendeeDetailsByTrainings(Set<String> trainingIds, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.TRAINING_ID).in(trainingIds));
        if (StringUtils.isNotEmpty(userId)) {
            query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        }
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getJoinedSessionsAttendeeDetailForUser(Set<String> sessionIds, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIMES).ne(Collections.EMPTY_LIST));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public List<GTTAttendeeDetails> getJoinedSessionsAttendeeDetailForStudent(Set<String> sessionIds, String studentId) {
        if(sessionIds.isEmpty() || sessionIds.size() > 1) return new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public GTTAttendeeDetails getOneJoinedSessionAttendeeDetailForUser(String userId, Sort.Direction direction) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIMES).ne(Collections.EMPTY_LIST));
        if (Sort.Direction.ASC.equals(direction)) {
            query.with(Sort.by(Sort.Direction.ASC, GTTAttendeeDetails.Constants.JOIN_TIMES));
        } else {
            query.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.JOIN_TIMES));
        }
        GTTAttendeeDetails attendeeDetail = gttAttendeeDetailsDAO.findOne(query, GTTAttendeeDetails.class);
        return attendeeDetail;
    }

    public static long parseTimeString(String timeStr) throws ParseException {
        return sdf.parse(timeStr).getTime();
    }

    public void updatePollAnswered(StudentPollDataReq req, String sessionId) throws BadRequestException {
        req.verify();
        GTTAttendeeDetails attendeeDetail = gttAttendeeDetailsDAO.getAttendeeDetail(req.getUserId(), sessionId);
        if (attendeeDetail != null) {
            Update update = new Update();
            update.set(GTTAttendeeDetails.Constants.POLL_ANSWERED, req.getPollAnswered());
            if (attendeeDetail.getEntityState().equals(EntityState.DELETED)) {
                attendeeDetail.setRandomUniqueIndex(UUID.randomUUID().toString());
                attendeeDetail.setSparseSessionUserId(null);
            } else {
                attendeeDetail.setRandomUniqueIndex(EntityState.ACTIVE.name());
                attendeeDetail.setSparseSessionUserId(attendeeDetail.createSparseSessionUserId());
            }
            gttAttendeeDetailsDAO.updateAttendeeDetail(update, attendeeDetail.getId(), null);
        } else {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "not gtt attendee is there");
        }
    }

    public Map<String, Integer> getAttendeeCount(Set<String> sessionIds) {
        List<Criteria> criterias = new ArrayList<>();
        criterias.add(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        criterias.add(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIMES).ne(Collections.EMPTY_LIST));

        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                Aggregation.group("sessionId").count().as("count")).withOptions(aggregationOptions);

        logger.info(aggregation);
        AggregationResults<AggregatedGTTAttendeeCountForSession> result = gttAttendeeDetailsDAO.getMongoOperations().aggregate(aggregation, GTTAttendeeDetails.class.getSimpleName(), AggregatedGTTAttendeeCountForSession.class);
        logger.info(result.getRawResults());
        List<AggregatedGTTAttendeeCountForSession> countList = result.getMappedResults();
        logger.info("Aggregation Result: " + countList);
        Map<String, Integer> countMap = new HashMap<>();

        for (AggregatedGTTAttendeeCountForSession aggregatedGTTAttendeeCountForSession : countList) {
            countMap.put(aggregatedGTTAttendeeCountForSession.getId(), aggregatedGTTAttendeeCountForSession.getCount());
        }

        return countMap;

    }


    public boolean shouldSendNoShowEmail(String userId, List<String> sessionIds, String currentSessionId) {
        if (ArrayUtils.isEmpty(sessionIds) || StringUtils.isEmpty(userId)) {
            return false;
        }
        logger.info("checkAndSendNoShowEmail sessionId:" + sessionIds);
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.LAST_UPDATED));
        query.limit(6);
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        logger.info("checkAndSendNoShowEmail got attendeeDetails:" + attendeeDetails);
        if (ArrayUtils.isEmpty(attendeeDetails) || attendeeDetails.size() < 3) {
            return false;
        }
        Collections.sort(attendeeDetails, (GTTAttendeeDetails left, GTTAttendeeDetails right) ->
                Integer.compare(sessionIds.indexOf(left.getSessionId()), sessionIds.indexOf(right.getSessionId())));
        logger.info("checkAndSendNoShowEmail sorted attendeeDetails:" + attendeeDetails);
        int notJoined = 0;
        for (int i = 0; i < attendeeDetails.size(); i++) {
            GTTAttendeeDetails detail = attendeeDetails.get(i);
            if (ArrayUtils.isEmpty(detail.getJoinTimes()) && !detail.isNoshowEmailSent()) {
                notJoined++;
            } else {
                break;
            }
        }
        logger.info("checkAndSendNoShowEmail notJoined count:" + notJoined);
        if (notJoined == 3) {
            GTTAttendeeDetails attendeeDetail = gttAttendeeDetailsDAO.getAttendeeDetail(userId, currentSessionId);
            if (attendeeDetail != null) {
                Update update = new Update();
                update.set(GTTAttendeeDetails.Constants.NO_SHOW_EMAIL_SENT, true);

                gttAttendeeDetailsDAO.updateAttendeeDetail(update, attendeeDetail.getId(), null);
            }
            logger.info("checkAndSendNoShowEmail updated attendee with email sent flag detail:" + attendeeDetail);
            return true;
        }
        return false;
    }


    public List<GTTAttendeeDetails> getAbsentAttendees(OTFSession oTFSession) {
        if (oTFSession == null) {
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).is(oTFSession.getId()));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIMES).is(Collections.EMPTY_LIST));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }


    public List<String> cleanGTTData() {
        return gttAttendeeDetailsDAO.cleanGTTData();
    }

    public void removeGTT(String userId, String batchId) {
        removeGTT(userId, batchId, new EnrollmentPojo());
    }

    public void removeGTT(String userId, String batchId, EnrollmentPojo enrollmentPojo) {
        List<String> batchIds = new ArrayList<>();
        if (StringUtils.isNotEmpty(batchId)) {
            batchIds.add(batchId);
        }
        Query sessionQuery = new Query();
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        sessionQuery.fields().include(OTFSession.Constants._ID)
                .include(OTFSession.Constants.START_TIME)
                .include(OTFSession.Constants.END_TIME);
        logger.info("batch enrolment ended, gtt update query {}", sessionQuery);
        List<OTFSession> otfSessions = oTFSessionDAO.runQuery(sessionQuery, OTFSession.class);
        Map<String, OTFSession> sessionMap = otfSessions.stream()
                .collect(Collectors.toMap(OTFSession::getId, e -> e));
        logger.info("got sesssion id to mark delete {}", sessionMap.keySet());
        if (ArrayUtils.isNotEmpty(sessionMap.keySet())) {
            gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedForDeEnrollmentBulk(sessionMap, userId, enrollmentPojo);
        }
    }

    public void gttEnrollmentStatusUpdate(String userId, String batchId, EnrollmentPojo enrollmentPojo) throws BadRequestException {
        List<String> batchIds = new ArrayList<>();
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(batchId) || enrollmentPojo == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "empty values");
        }
        if (StringUtils.isNotEmpty(batchId)) {
            batchIds.add(batchId);
        }

        if (enrollmentPojo.getStatus() == EntityStatus.ACTIVE) {
            enrollmentPojo.setBatchId(batchId);
            enrollmentPojo.setUserId(userId);
            gttCreateStatusChangeEntriesForEnrollment(enrollmentPojo);
        } else {
            Query sessionQuery = new Query();
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));

            // taking future sessions only for inactive enrolments.
            // but enrollment info in gtt not updated for old data, leaves the data in inconsistent state.
            // if (enrollmentPojo.getStatus() == EntityStatus.INACTIVE) {
            //     sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gt(System.currentTimeMillis()));
            // }
            sessionQuery.fields().include(OTFSession.Constants._ID)
                    .include(OTFSession.Constants.START_TIME)
                    .include(OTFSession.Constants.END_TIME);
            logger.info("batch enrollment status change, gtt status update query {}", sessionQuery);
            Map<String, OTFSession> sessionMap = oTFSessionDAO.runQuery(sessionQuery, OTFSession.class)
                    .stream().collect(Collectors.toMap(OTFSession::getId, e -> e));
            logger.info("got sesssion id to change status {}", sessionMap);
            if (ArrayUtils.isNotEmpty(sessionMap.keySet())) {
                gttAttendeeDetailsDAO.markGTTAttendeeAsDeletedForDeEnrollmentBulk(sessionMap, userId, enrollmentPojo);
            }
        }
    }

    public void gttCreateStatusChangeEntriesForEnrollment(EnrollmentPojo enrollmentPojo) throws BadRequestException {
        List<String> batchIds = new ArrayList<>();
        logger.info("received req {}", gson.toJson(enrollmentPojo));
        if (enrollmentPojo == null || StringUtils.isEmpty(enrollmentPojo.getUserId()) || StringUtils.isEmpty(enrollmentPojo.getBatchId())
        || enrollmentPojo.getCreationTime() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "empty values");
        }

        String userId = enrollmentPojo.getUserId();
        String batchId = enrollmentPojo.getBatchId();
        if (StringUtils.isNotEmpty(batchId)) {
            batchIds.add(batchId);
        }

        Instant now = Instant.now();
        long cuurent = now.toEpochMilli();
        long instant = now.truncatedTo(ChronoUnit.DAYS).plus(1, ChronoUnit.DAYS).toEpochMilli();
        Query sessionQuery = new Query();
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(instant));

        Field fields = sessionQuery.fields();
        Stream.of(OTFSession.Constants._ID,
                OTFSession.Constants.START_TIME,
                OTFSession.Constants.END_TIME,
                OTFSession.Constants.MEETING_ID)
                .forEach(fields::include);
        logger.info("batch enrollment status change, gtt status update query {}", sessionQuery);
        List<OTFSession> otfSessions = oTFSessionDAO.runQuery(sessionQuery, OTFSession.class);
        Set<String> sessionIds = otfSessions.stream().map(OTFSession::getId).collect(Collectors.toSet());
        logger.info("got sesssion id to change status {}", sessionIds);
        if (ArrayUtils.isNotEmpty(otfSessions)) {
            List<List<String>> lists = Lists.partition(new ArrayList<>(sessionIds), 100);
            for (List<String> list : lists) {
                List<GTTAttendeeDetails> activeSessionList = gttAttendeeDetailsDAO.getAttendeeDetailForSessionList(enrollmentPojo.getUserId(), new HashSet<>(list),
                        Collections.singletonList(GTTAttendeeDetails.Constants.SESSION_ID), 0, AbstractMongoDAO.MAX_ALLOWED_FETCH_SIZE);
                Set<String> activeSessionSet = activeSessionList.stream().map(GTTAttendeeDetails::getSessionId).collect(Collectors.toSet());
                List<OTFSession> notCreatedSession = otfSessions.stream()
                        .filter(e -> !activeSessionSet.contains(e.getId()))
                        .collect(Collectors.toList());
                for (OTFSession session : notCreatedSession) {
                    try {
                        GTTAttendeeDetails details = new GTTAttendeeDetails(session.getId(), userId, Collections.singletonList(enrollmentPojo));
                        details.setEnrollmentId(enrollmentPojo.getId());
                        details.setSessionEndTime(session.getEndTime());
                        details.setSessionStartTime(session.getStartTime());
                        details.setTraningId(session.getMeetingId());
                        AttendeeContext attendeeContext = AttendeeContext.LATE_JOIN;
                        if (enrollmentPojo.getCreationTime() != null && session.getStartTime() > enrollmentPojo.getCreationTime()) {
                            if (session.getStartTime() < cuurent) {
                                attendeeContext = AttendeeContext.STATUS_CHANGE;
                            } else {
                                attendeeContext = null;
                            }
                        }
                        details.setAttendeeType(attendeeContext);
                        logger.info("creating gtt for reactivated enrolment with session {}", details.getSessionId());
                        gttAttendeeDetailsDAO.save(details);
                    } catch (org.springframework.dao.DuplicateKeyException |
                            com.mongodb.MongoWriteException e) {
                        logger.warn(e.getMessage(), e);
                        GTTAttendeeDetails detail = gttAttendeeDetailsDAO.getAttendeeDetail(enrollmentPojo.getUserId(), session.getId(), EntityState.ACTIVE);
                        detail.addEnrollmentInfo(enrollmentPojo);
                        AttendeeContext attendeeContext = AttendeeContext.LATE_JOIN;
                        if (enrollmentPojo.getCreationTime() != null && session.getStartTime() > enrollmentPojo.getCreationTime()) {
                            if (session.getStartTime() < cuurent) {
                                attendeeContext = AttendeeContext.STATUS_CHANGE;
                            } else {
                                attendeeContext = null;
                            }
                        }
                        detail.setAttendeeType(attendeeContext);
                        queueAttendee(detail);
                    }
                }
            }
        }
    }

    public void gttUpdateSessionTimings(OTFSession session) throws BadRequestException {
        List<String> batchIds = new ArrayList<>();
        if (session == null || session.getId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "empty obj");
        }
        gttAttendeeDetailsDAO.gttUpdateSessionTimings(session);
    }


    public List<GTTAttendeeDetails> getAttendeeDetails(List<String> userIds, Long fromTime, Long thruTime) {

        Criteria timeFilter = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
        Query query = new Query();
        query.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(userIds));
        query.addCriteria(timeFilter);
        query.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME));
        List<GTTAttendeeDetails> attendeeDetails = gttAttendeeDetailsDAO.runQuery(query, GTTAttendeeDetails.class);
        return attendeeDetails;
    }

    public Map<String, Integer> getAttendanceStatus(List<String> userIds, Long fromTime, Long thruTime) {

        List<Criteria> criterias = new ArrayList<>();

        if (thruTime == null) {
            thruTime = new Date().getTime();
        }
        criterias.add(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).in(userIds));

        if (fromTime != null) {
            Criteria timeFilter = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
            criterias.add(timeFilter);
        }
        criterias.add(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIME_FIRST).exists(true));

        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                Aggregation.group("userId").count().as("count")).withOptions(aggregationOptions);

        logger.info(aggregation);
        AggregationResults<AggregatedGTTAttendeeCountForSession> result = gttAttendeeDetailsDAO.getMongoOperations().aggregate(aggregation, GTTAttendeeDetails.class.getSimpleName(), AggregatedGTTAttendeeCountForSession.class);
        logger.info(result.getRawResults());
        List<AggregatedGTTAttendeeCountForSession> countList = result.getMappedResults();
        logger.info("Aggregation Result: " + countList);
        Map<String, Integer> countMap = new HashMap<>();

        for (AggregatedGTTAttendeeCountForSession aggregatedGTTAttendeeCountForSession : countList) {
            countMap.put(aggregatedGTTAttendeeCountForSession.getId(), aggregatedGTTAttendeeCountForSession.getCount());
        }

        return countMap;
    }

    public AggregationResults<AggregatedGTTAttendeeCountForSession> getAbsenteeInLastClasses(List<String> attendeeIdChunk, int absentCount, Long start, Long end) {
        return gttAttendeeDetailsDAO.getAbsenteeInLastClasses(attendeeIdChunk, absentCount, start, end);
    }

    public PlatformBasicResponse checkActiveGttAttendeeForUserIdAndSessionId(String userId, String sessionId) {
        logger.info("checkActiveGttAttendeeForUserIdAndSessionId " + userId + "," + sessionId);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse(false, null, null);
        if ((userId != null) && (sessionId != null)) {
            GTTAttendeeDetails attendeeDetail = gttAttendeeDetailsDAO.getAttendeeDetail(userId, sessionId);
            if (attendeeDetail != null) {
                platformBasicResponse.setSuccess(true);
            }
        }
        return platformBasicResponse;
    }

    public GTTAttendeeDetailsInfo getPreviousSessionForGttAttendee(String userId, Long startTime, String enrollmentId) throws BadRequestException {
        GTTAttendeeDetails attendee = gttAttendeeDetailsDAO.getPreviousSessionForGttAttendee(userId, startTime, enrollmentId);
        if (attendee != null) {
            return attendee.gttAttendeeDetailsInfo();
        }
        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Previous Session Not Found");
    }

    public List<GTTAggregatedStudentList> getNewAbsenteeStudentListAggregate(List<String> userIds, List<String> sessionIds, String lastSessionId) {
        AggregationResults<GTTAggregatedStudentList> result = gttAttendeeDetailsDAO.getNewAbsenteeStudentListAggregate(userIds, sessionIds, lastSessionId);
        List<GTTAggregatedStudentList> studentList = result.getMappedResults();

        return studentList;
    }

    public PlatformBasicResponse getAttendeesCountBySessionId(String sessionId) throws BadRequestException {
        long count = gttAttendeeDetailsDAO.getAttendeesCountBySessionId(sessionId);
        if (count == 0l) {
            throw new BadRequestException(ErrorCode.SESSION_NOT_FOUND, "no gtt attendee is there");
        }
        return new PlatformBasicResponse(true, count, null);
    }

    public PlatformBasicResponse getPresentAttendeesCountBySessionId(String sessionId) throws BadRequestException {
        long count = gttAttendeeDetailsDAO.getPresentAttendeesCountBySessionId(sessionId);
        if (count == 0l) {
            throw new BadRequestException(ErrorCode.SESSION_NOT_FOUND, "no gtt attendee is there");
        }
        return new PlatformBasicResponse(true, count, null);
    }

    public PlatformBasicResponse postAbsentFeedback(List<AbsentSessionFeedbackReq> feedbackReqs, Long callingUserId) {
        PlatformBasicResponse response = new PlatformBasicResponse();

        if (CollectionUtils.isEmpty(feedbackReqs)) {
            return response;
        }

        final String key = "CONFLICT_SESSIONS_ABSENT_FEEDBACK_" + callingUserId;

        try {
            Type setType = new TypeToken<Set<String>>() {
            }.getType();
            Set<String> conflictSessionGttIds = new Gson().fromJson(redisDAO.get(key), setType);

            logger.info("conflictSessionGttIds {}", conflictSessionGttIds);
            if (CollectionUtils.isNotEmpty(conflictSessionGttIds)) {
                String autoFillFeedbackText = "Autofilled feedback due to session conflicts";
                feedbackReqs.add(new AbsentSessionFeedbackReq(new ArrayList<>(conflictSessionGttIds), autoFillFeedbackText, AttendanceFeedbackTag.SCHEDULING));
                redisDAO.del(key);
            }
        } catch (BadRequestException | InternalServerErrorException e) {
            logger.error("Error while fetching set of gttIds for overlapping sessions");
        }

        for (AbsentSessionFeedbackReq req : feedbackReqs) {
            Update update = new Update();
            AttendanceFeedback attendanceFeedback = AttendanceFeedback.builder()
                    .feedbackText(req.getFeedbackText())
                    .attendanceFeedbackTag(req.getAttendanceFeedbackTag())
                    .submissionTime(System.currentTimeMillis())
                    .build();
            update.push(GTTAttendeeDetails.Constants.FEEDBACK_TEXTS, attendanceFeedback);
            gttAttendeeDetailsDAO.postAbsentFeedbackText(update, req.getGttIds(), callingUserId.toString());
        }

        return response;
    }


    // Time : O(nlogn) -- Space : O(n) - n : number of sessions for user in the past week
    public Map<String, GTTAttendeeDetails> getPastWeekAbsentAttendees(String userId) throws IllegalArgumentException {

        // validate-- params
        if (StringUtils.isEmpty(userId)) {
            throw new IllegalArgumentException("Invalid userId");
        }

        // define-- fetch interval, one hour margin to facilitate the attendance update for sessions that've ended just now
        final long toTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis() - CommonCalendarUtils.MILLIS_PER_HOUR);
        final long fromTime = CommonCalendarUtils.getDayStartTime(toTime - 15 * DateTimeUtils.MILLIS_PER_DAY);
        List<String> includeGTTFields = Stream.of(GTTAttendeeDetails.Constants.SESSION_ID, GTTAttendeeDetails.Constants.ENROLLMENT_ID,
                GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME, GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME,
                GTTAttendeeDetails.Constants.FEEDBACK_TEXTS, GTTAttendeeDetails.Constants.ATTENDEE_TYPE,
                GTTAttendeeDetails.Constants.JOIN_TIMES).collect(Collectors.toList());

        List<GTTAttendeeDetails> pastWeekGTT = gttAttendeeDetailsDAO.getUserAttendeesForDuration(userId, fromTime, toTime,
                includeGTTFields, 0, MAX_FETCH_SIZE);
        if (CollectionUtils.isEmpty(pastWeekGTT)) {
            return new HashMap<>();
        } else if (pastWeekGTT.size() > MAX_FETCH_SIZE / 2) {
            logger.warn("Number of fetched absent gtt entries is of size > {} for userId {}", MAX_FETCH_SIZE / 2, userId);
            return new HashMap<>();
        }

        // remove-- overlapping sessions if any
        pastWeekGTT = Optional.ofNullable(pastWeekGTT)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .filter(a -> CollectionUtils.isEmpty(a.getFeedbackTexts()) &&
                        !AttendeeContext.LATE_JOIN.equals(a.getAttendeeType()))
                .sorted(Comparator.comparing(GTTAttendeeDetails::getSessionStartTime))
                .collect(Collectors.toList());

        // O(n)
        Set<String> conflictAbsentSessionGTTIds = new HashSet<>();
        Set<String> conflictNonAbsentSessionGTTIds = new HashSet<>();
        GTTAttendeeDetails latest = null;
        for (GTTAttendeeDetails next : pastWeekGTT) {
            if (latest != null && next.getSessionStartTime() < latest.getSessionEndTime()) {

                if (CollectionUtils.isEmpty(latest.getJoinTimes())) {
                    conflictAbsentSessionGTTIds.add(latest.getId());
                } else {
                    conflictNonAbsentSessionGTTIds.add(latest.getId());
                }

                if (CollectionUtils.isEmpty(next.getJoinTimes())) {
                    conflictAbsentSessionGTTIds.add(next.getId());
                } else {
                    conflictNonAbsentSessionGTTIds.add(next.getId());
                }

            }

            if (latest == null || next.getSessionEndTime() > latest.getSessionEndTime()) {
                latest = next;
            }
        }

        logger.info("conflictSessionGTTIds with overlap : {}", conflictAbsentSessionGTTIds);
        if (CollectionUtils.isNotEmpty(conflictAbsentSessionGTTIds)) {
            final String key = "CONFLICT_SESSIONS_ABSENT_FEEDBACK_" + userId;
            try {
                redisDAO.setex(key, gson.toJson(conflictAbsentSessionGTTIds), 900);
            } catch (InternalServerErrorException e) {
                logger.error("Error while setting conflict session gttIds for user " + userId);
            }
        }

        return Optional.of(pastWeekGTT).orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .filter(gtt -> !conflictAbsentSessionGTTIds.contains(gtt.getId())
                        && !conflictNonAbsentSessionGTTIds.contains(gtt.getId())
                        && CollectionUtils.isEmpty(gtt.getJoinTimes()))
                .collect(Collectors.toMap(GTTAttendeeDetails::getSessionId, a -> a));
    }

}
