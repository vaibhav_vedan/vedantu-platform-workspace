package com.vedantu.scheduling.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.google.common.collect.Lists;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.pojo.FailedPayoutInfo;
import com.vedantu.onetofew.pojo.GetOTFUpcomingSessionReq;
import com.vedantu.scheduling.dao.serializers.SessionStateDetailsDAO;
import com.vedantu.session.pojo.*;
import com.vedantu.scheduling.response.GetLatestUpComingOrOnGoingSessionResponse;
import com.vedantu.scheduling.response.session.*;
import com.vedantu.session.pojo.*;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.onetofew.request.GetFeedbackReq;
import com.vedantu.review.response.FeedbackInfo;
import com.vedantu.review.response.GetFeedbackResponse;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.scheduling.dao.serializers.SessionAttendeeDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.dao.serializers.SessionDAO;
import com.vedantu.scheduling.dao.sql.entity.SessionStateDetails;
import com.vedantu.scheduling.pojo.SessionInfo;
import com.vedantu.scheduling.pojo.SessionReportAggregation;
import com.vedantu.scheduling.pojo.TotalSessionDuration;
import com.vedantu.scheduling.pojo.UserSessionAttendance;
import com.vedantu.scheduling.pojo.session.GTMGTTSessionDetails;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.scheduling.pojo.session.TeacherSessionDashboard;
import com.vedantu.scheduling.request.session.EndSessionReq;
import com.vedantu.scheduling.request.session.GetSessionBillDetailsReq;
import com.vedantu.scheduling.request.session.GetSessionBillingDurationRes;
import com.vedantu.scheduling.request.session.GetSessionDurationByContextIdReq;
import com.vedantu.scheduling.request.session.GetSessionPartnersReq;
import com.vedantu.scheduling.request.session.GetSessionsByCreationTime;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.scheduling.request.session.GetSubscriptionSessionsReq;
import com.vedantu.scheduling.request.session.GetUserCalendarSessionsReq;
import com.vedantu.scheduling.request.session.GetUserLatestActiveSessionReq;
import com.vedantu.scheduling.request.session.GetUserPastSessionReq;
import com.vedantu.scheduling.request.session.GetUserUpcomingSessionReq;
import com.vedantu.scheduling.request.session.JoinSessionReq;
import com.vedantu.scheduling.request.session.MarkSessionActiveReq;
import com.vedantu.scheduling.request.session.MarkSessionExpiredReq;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.scheduling.request.session.ProcessSessionPayoutReq;
import com.vedantu.scheduling.request.session.RescheduleSessionReq;
import com.vedantu.scheduling.request.session.SessionScheduleReq;
import com.vedantu.scheduling.request.session.SetVimeoVideoIdRequest;
import com.vedantu.scheduling.request.session.SubscriptioncoursePlanCancelSessionsReq;
import com.vedantu.scheduling.request.session.UpdateLiveSessionPlatformDetailsReq;
import com.vedantu.scheduling.response.BasicRes;
import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.scheduling.utils.SessionUtils;
import com.vedantu.subscription.response.CoursePlanDashboardSessionInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.subscription.TrialSessionInfos;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;
import javax.mail.internet.AddressException;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.http.HttpMethod;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

@Service
public class SessionManager {

    @Autowired
    private SessionDAO sessionDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private SessionAttendeeDAO sessionAttendeeDAO;

    @Autowired
    private WizIQClassManager wizIQClassManager;

    @Autowired
    private OTFSessionManager oTFSessionManager;

    @Autowired
    private SessionAttendeeManager sessionAttendeeManager;

    @Autowired
    private SessionStateDetailsManager sessionStateDetailsManager;

    @Autowired
    private SessionCalendarManager sessionCalendarManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionManager.class);

    @Autowired
    private FosUtils userUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private SessionStateDetailsDAO sessionStateDetailsDAO;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    private AwsSNSManager awsSNSManager;


    public static String arn;
    public static String env;
    private static int minRescheduleTime = ConfigUtils.INSTANCE.getIntValue("session.reschedule.time.min");
    public final static Long sendPostSessionEmailCronRunTime = 15 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private AmazonSNSAsync snsClient;
    private static final int LATEST_SESSION_CACHE_EXPIRY_MINUTE = 30;


    private static Gson gson = new Gson();

    public static final List<SessionState> UPCOMING_SESSION_STATES = Collections
            .unmodifiableList(Arrays.asList(SessionState.SCHEDULED));
    public static final List<SessionState> ONGOING_SESSION_STATES = Collections
            .unmodifiableList(Arrays.asList(SessionState.ACTIVE, SessionState.STARTED));
    public static final List<SessionState> COMPLETE_SUCCESSFUL_SESSION_STATES = Collections
            .unmodifiableList(Arrays.asList(SessionState.ENDED));
    public static final List<SessionState> COMPLETE_UNSUCCESSFUL_SESSION_STATES = Collections
            .unmodifiableList(Arrays.asList(SessionState.CANCELED, SessionState.EXPIRED, SessionState.FORFEITED));

    public SessionManager() {
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();

    }

    public SessionInfo createSession(SessionScheduleReq sessionScheduleReq) throws VException {
        logger.info("Session req : " + sessionScheduleReq.toString());

        // Validate request
        sessionScheduleReq.validate();

        Session session = new Session(sessionScheduleReq);
        logger.info("Session : " + session.toString());

        // Block the calendar for the requested slot
        if (!Boolean.TRUE.equals(sessionScheduleReq.getNoCalendarCheck())
                || SessionModel.IL.equals(sessionScheduleReq.getSessionModel())) {
            sessionCalendarManager.checkAndBlockCalendar(session);
        }

        // Create session
        sessionDAO.insert(session,
                sessionScheduleReq.getCallingUserId() != null ? sessionScheduleReq.getCallingUserId().toString()
                : null);

        // Create Session attendees
        List<SessionAttendee> attendees = sessionAttendeeManager.createSessionAttendees(session);

        // Create session state details
        sessionStateDetailsManager.createSessionStateDetails(session.getId());

        logger.info("SessionId:" + session.getId());

        SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session), attendees);

        Map<String, Object> payload = new HashMap<>();
        payload.put("sessionInfo", sessionInfo);
        payload.put("event", SessionEvents.SESSION_SCHEDULED);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
        asyncTaskFactory.executeTask(params);
        if (!LiveSessionPlatformType.DEFAULT.equals(session.getLiveSessionPlatformType())) {
            Map<String, Object> payload1 = new HashMap<>();
            payload1.put("sessionId", session.getId());
            payload1.put("event", SessionEvents.SESSION_SCHEDULED);
            AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.HANDLE_LIVE_SESSION_TOOL_TYPE, payload1);
            asyncTaskFactory.executeTask(params1);
        }
        return sessionInfo;
    }

    public List<SessionInfo> createMultipleSessions(MultipleSessionScheduleReq multipleSessionScheduleReq)
            throws VException {
        logger.info("Session req : " + multipleSessionScheduleReq.toString());

        // Validate request
        multipleSessionScheduleReq.validate();

        List<Session> sessions = Session.createSessions(multipleSessionScheduleReq);
        logger.info("Session : " + sessions == null ? 0 : sessions.size());

        // Block the calendar for the requested slot
        List<Session> availableSessions = null;
        if (!Boolean.TRUE.equals(multipleSessionScheduleReq.getNoCalendarCheck())) {
            availableSessions = sessionCalendarManager.checkAndBlockCalendar(sessions);
            if (CollectionUtils.isEmpty(availableSessions)) {
                String errorMessage = "No sessions are available for req : " + multipleSessionScheduleReq.toString();
                logger.info(errorMessage);
                throw new BadRequestException(ErrorCode.SLOTS_NOT_AVAILABLE, errorMessage);
            }
        } else {
            availableSessions = sessions;
        }

        // Create sessions
        sessionDAO.insertAll(availableSessions,
                multipleSessionScheduleReq.getCallingUserId() != null
                ? multipleSessionScheduleReq.getCallingUserId().toString()
                : null);
        List<Long> sessionIds = SessionUtils.getSessionIds(availableSessions);

        // Create Session Attendees
        List<SessionAttendee> attendees = sessionAttendeeManager.createSessionAttendees(availableSessions);

        // Create session state details
        sessionStateDetailsManager.createSessionStateDetails(sessionIds);

        logger.info("SessionIds:" + sessionIds);
        List<SessionInfo> sessionInfos = getSessionInfos(availableSessions, attendees);

        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("sessionInfo", sessionInfos.get(0));
        payload.put("event", SessionEvents.SESSION_SCHEDULED);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
        asyncTaskFactory.executeTask(params);
        if (ArrayUtils.isNotEmpty(availableSessions)) {
            for (Session info : availableSessions) {
                if (!LiveSessionPlatformType.DEFAULT.equals(info.getLiveSessionPlatformType())) {
                    Map<String, Object> payload1 = new HashMap<>();
                    payload1.put("sessionId", info.getId());
                    payload1.put("event", SessionEvents.SESSION_SCHEDULED);
                    AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.HANDLE_LIVE_SESSION_TOOL_TYPE, payload1);
                    asyncTaskFactory.executeTask(params1);
                }
            }
        }
        return sessionInfos;
    }

    public SessionInfo rescheduleSession(RescheduleSessionReq req) throws VException {
        logger.info("Reschedule req : " + req.toString());
        Session session = sessionDAO.getById(req.getSessionId());
        if (session == null || req.getStartTime() < (System.currentTimeMillis() + (DateTimeUtils.MILLIS_PER_MINUTE * minRescheduleTime))) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Invalid rescheduleSessionReq : " + req.toString());
        }

        logger.info("Session pre update:" + session.toString());

        // Check if the action is allowed for this user
        isSessionUpdateAllowedForUser(session, req.getCallingUserId(), req.getCallingUserRole());

        // validate if the reschedule can be allowed
        if (session.getStartTime() <= (System.currentTimeMillis() + (DateTimeUtils.MILLIS_PER_MINUTE * minRescheduleTime))
                || !SessionState.SCHEDULED.equals(session.getState())) {
            String errorMessage = "Session invalid for reschedule : " + session.toString();
            throw new BadRequestException(ErrorCode.SESSION_INVALID_FOR_RESCHEDULE, errorMessage);
        }

        // Block the new Slot - Session start time should not be updated before
        // this call.
        sessionCalendarManager.rescheduleSlot(session, req.getStartTime());

        // Update session
        Long previousStartTime = session.getStartTime();
        session.setEndTime(req.getStartTime() + (session.getEndTime() - session.getStartTime()));
        session.setStartTime(req.getStartTime());
        // adding reschedule data
        List<RescheduleData> rescheduleDatas = session.getRescheduleData();
        if (rescheduleDatas == null) {
            rescheduleDatas = new ArrayList<>();
        }
        RescheduleData rescheduleData = new RescheduleData(req.getCallingUserId(), req.getCallingUserRole(), null,
                previousStartTime);
        rescheduleDatas.add(rescheduleData);
        session.setRescheduleData(rescheduleDatas);
        sessionDAO.update(session, req.getCallingUserId());

        // Update session attendees
        List<SessionAttendee> attendees = sessionAttendeeManager.updateSessionAttendeesStartTime(session);

        logger.info("Session updated for id : " + session.getId());
        SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session), attendees);
        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("sessionInfo", sessionInfo);
        payload.put("event", SessionEvents.SESSION_RESCHEDULED);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
        asyncTaskFactory.executeTask(params);
        if (!LiveSessionPlatformType.DEFAULT.equals(sessionInfo.getLiveSessionPlatformType())) {
            Map<String, Object> payload1 = new HashMap<>();
            payload1.put("sessionId", session.getId());
            payload1.put("event", SessionEvents.SESSION_RESCHEDULED);
            AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.HANDLE_LIVE_SESSION_TOOL_TYPE, payload1);
            asyncTaskFactory.executeTask(params1);
        }
        return sessionInfo;
    }

    public SessionInfo getSessionById(Long id) throws NotFoundException {
        logger.info("GetSessionById : " + id);
        Session session = sessionDAO.getById(id);
        logger.info("session : " + session);
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "Session not found with id : " + id);
        }
        SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session),
                sessionAttendeeManager.getSessionAttendees(session.getId()));
        logger.info("Response : " + sessionInfo.toString());
        return sessionInfo;
    }


    public List<SessionInfo> getCoursePlanSessionsById(String id) throws Exception {
        logger.info("GetSessionsById : " + id);
        List<SessionInfo> sessionInfos = new ArrayList<>();
        List<Session> sessions = sessionDAO.getSessionsByContextId(id);
            logger.info("sessions : " + sessions);
            for(Session session:sessions){
            SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session),
                    sessionAttendeeManager.getSessionAttendees(session.getId()));
            sessionInfos.add(sessionInfo);
        }
            return sessionInfos;
    }

    public SessionInfo joinSession(JoinSessionReq joinSessionReq, boolean allowPreJoin) throws VException {
        logger.info("Request - joinReq:" + joinSessionReq);
        joinSessionReq.verify();
        Session session = sessionDAO.getById(joinSessionReq.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND,
                    "Session not found with id : " + joinSessionReq.toString());
        }

        Long allowedJoinTime = session.getStartTime();
        if (allowPreJoin) {
            int prejoinMins = ConfigUtils.INSTANCE.getIntValue("session.pre.join.time.min");
            allowedJoinTime = allowedJoinTime - (prejoinMins * DateTimeUtils.MILLIS_PER_MINUTE);
        }

        // Check if user is allowed to join
        isSessionUpdateAllowedForUser(session, joinSessionReq.getUserId());

        // Update the session state details
        SessionStateDetails sessionStateDetails = sessionStateDetailsManager.userJoined(joinSessionReq.getSessionId(),
                session.getStartTime(), session.getEndTime(), allowedJoinTime, getSessionExpiryTime(session));

        // Update the attendee if it is already not marked as joined
        sessionAttendeeManager.markJoined(joinSessionReq.getSessionId(), joinSessionReq.getUserId());

        // Update the session
        session.setState(sessionStateDetails.getSessionState());
        if (session.getStartedAt() == null || session.getStartedAt() <= 0l) {
            Long currentTime = System.currentTimeMillis();
            if (currentTime < session.getStartTime()) {
                session.setStartedAt(session.getStartTime());
                session.setStartedBy(joinSessionReq.getCallingUserId());
            } else {
                session.setStartedAt(currentTime);
                session.setStartedBy(joinSessionReq.getCallingUserId());
            }
        }

        sessionDAO.update(session, joinSessionReq.getUserId());
        SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session),
                sessionAttendeeManager.getSessionAttendees(session.getId()));
        Map<String, Object> payload = new HashMap<>();
        payload.put("sessionInfo", sessionInfo);
        payload.put("event", session.getState().getSessionEvent());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
        asyncTaskFactory.executeTask(params);
        logger.info("Response:" + session.toString());
        return sessionInfo;
    }

    GetSessionBillingDurationRes getBillingDuration(Long sessionId) throws VException {
        logger.info("Request: sessionId:" + sessionId);
        String getBillingDurationUrl = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT")
                + "session/getBillingDuration?sessionId=" + sessionId;

        ClientResponse resp = WebUtils.INSTANCE.doCall(getBillingDurationUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        GetSessionBillingDurationRes getSessionBillDetailsRes = gson.fromJson(jsonString, GetSessionBillingDurationRes.class);
        return getSessionBillDetailsRes;
    }

    public Session endSession(EndSessionReq endSessionReq, boolean checkIfUpdateAllowed) throws VException {
        // checkIfUpdateAllowed--> in case of cancelsubscriptionsessions, system
        // triggers the cancel sessions
        // Long sessionId, Long callingUserId)
        logger.info("Request :" + endSessionReq.toString());
        Session session = sessionDAO.getById(endSessionReq.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND,
                    "Session not found with id : " + endSessionReq.getSessionId());
        }

        // Check if user is allowed to join
        if (checkIfUpdateAllowed) {
            isSessionUpdateAllowedForUser(session, endSessionReq.getCallingUserId(),
                    endSessionReq.getCallingUserRole());
        }

        try {
//            if (SessionState.STARTED.equals(session.getState())) {
//                Boolean active = true;
//                List<SessionAttendee> sessionAttendees = sessionAttendeeManager.getSessionAttendees(session.getId());
//
//                for (SessionAttendee sessionAttendee : sessionAttendees) {
//                    if (!SessionUserState.JOINED.equals(sessionAttendee.getUserState())) {
//                        active = false;
//                    }
//                }
//
//                if (active) {
//                    GetSessionBillingDurationRes duration = getBillingDuration(session.getId());
//                    Long billingDuration = duration.getBillingDuration();
//                    if (billingDuration != null && billingDuration != 0l) {
//                        MarkSessionActiveReq markSessionActiveReq = new MarkSessionActiveReq();
//                        markSessionActiveReq.setSessionId(session.getId());
//                        markSessionActive(markSessionActiveReq);
//                    }
//                }
//            }
            // Update the session state details
            SessionStateDetails sessionStateDetails = sessionStateDetailsManager.endSession(
                    endSessionReq.getSessionId(), session.getStartTime(), session.getEndTime(),
                    getSessionExpiryTime(session));

            // Update the session
            if(null != endSessionReq.getEndType()) {
                session.setEndType(endSessionReq.getEndType());
            }
            if(null != endSessionReq.getEndedBy()) {
                session.setEndedBy(endSessionReq.getEndedBy());
            }
            session.setState(sessionStateDetails.getSessionState());
            if (StringUtils.isNotEmpty(endSessionReq.getRemark())) {
                session.setRemark(endSessionReq.getRemark());
            }
            if (SessionState.ENDED.equals(session.getState())
                    && (session.getEndedAt() == null || session.getEndedAt() <= 0l)) {
                long endTime = System.currentTimeMillis();
                if (endTime > session.getEndTime()) {
                    endTime = session.getEndTime();
                }

                session.setEndedAt(endTime);
            }
            if (SessionState.CANCELED.equals(session.getState())) {
                if (endSessionReq.getCallingUserId() != null) {
                    session.setCancelledBy(endSessionReq.getCallingUserId());
                }
            }
            sessionDAO.update(session, endSessionReq.getCallingUserId());

            // Unblock the calendar - Bypass this if the session is IL or demo
            // session
            if (!SessionModel.IL.equals(session.getModel()) && !isDemoSession(session)) {
                sessionCalendarManager.unblockCalendar(session);
            }

            logger.info("Response:" + session.toString());
            SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session),
                    sessionAttendeeManager.getSessionAttendees(session.getId()));
            if (endSessionReq.getEndCoursePlan() != null && Boolean.TRUE.equals(endSessionReq.getEndCoursePlan())) {
                sessionInfo.setEndCoursePlan(endSessionReq.getEndCoursePlan());
            }
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", sessionInfo);
            payload.put("event", session.getState().getSessionEvent());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
            asyncTaskFactory.executeTask(params);

            return sessionInfo;
        } catch (BadRequestException | NotFoundException ex) {
            logger.info("Session end not successful - " + ex.toString());
            throw ex;
        } catch (VException ex) {
            logger.error("Session end not successful - " + ex.toString());
            throw ex;
        }
    }

    public List<Session> cancelSessions(SubscriptioncoursePlanCancelSessionsReq subscriptionCancelSessionsReq)
            throws VException {
        subscriptionCancelSessionsReq.verify();

        // Check if any session is active
        GetSessionsReq getSessionsReq = new GetSessionsReq();
        getSessionsReq.setSubscriptionId(subscriptionCancelSessionsReq.getSubscriptionId());
        if (subscriptionCancelSessionsReq.getContextId() != null) {
            getSessionsReq.setContextId(subscriptionCancelSessionsReq.getContextId());
        }
        if (subscriptionCancelSessionsReq.getContextType() != null) {
            getSessionsReq.setContextType(subscriptionCancelSessionsReq.getContextType());
        }
        // getSessionsReq.setAfterStartTime(System.currentTimeMillis());
        // List<SessionState> sessionStates = new ArrayList<>();
        // sessionStates.add(SessionState.SCHEDULED);
        // getSessionsReq.setSessionStates(sessionStates);
        getSessionsReq.setStart(0);
        getSessionsReq.setSize(500);
        List<Session> sessions = getSessions(getSessionsReq);

        if (sessions != null && sessions.size() >= 500) {
            logger.error("More than 500 sessions in cancelSessions");
        }
        List<Long> sessionIds = SessionUtils.getSessionIds(sessions);
        sessionStateDetailsManager.checkActiveAndStartedSessions(sessionIds);
        checkPayoutTimeBuffer(sessions);

        List<Session> cancelledSessions = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (Session session : sessions) {
                try {
                    if (SessionState.SCHEDULED.equals(session.getState())) {
                        EndSessionReq endSessionReq = new EndSessionReq(session.getId(),
                                subscriptionCancelSessionsReq.getRemark(),
                                subscriptionCancelSessionsReq.getCallingUserId(),
                                subscriptionCancelSessionsReq.getCallingUserRole());
                        endSessionReq.setEndCoursePlan(Boolean.TRUE);
                        Session endedSession = endSession(endSessionReq, false);
                        cancelledSessions.add(endedSession);
                    }
                    if (!LiveSessionPlatformType.DEFAULT.equals(session.getLiveSessionPlatformType())) {
                        Map<String, Object> payload1 = new HashMap<>();
                        payload1.put("sessionId", session.getId());
                        payload1.put("event", SessionEvents.SESSION_CANCELED);
                        AsyncTaskParams params1 = new AsyncTaskParams(AsyncTaskName.HANDLE_LIVE_SESSION_TOOL_TYPE, payload1);
                        asyncTaskFactory.executeTask(params1);
                    }
                } catch (VException ex) {
                    logger.error("Exception occured while cancelling the sessions : " + session.toString()
                            + " subscriptionCancelReq : " + subscriptionCancelSessionsReq.toString(), ex);
                }
            }
        }
        return cancelledSessions;
    }

    private void checkPayoutTimeBuffer(List<Session> sessions) throws ConflictException {
        Long currentTime = System.currentTimeMillis();
        if (CollectionUtils.isNotEmpty(sessions)) {
            for (Session session : sessions) {
                if (SessionState.ENDED.equals(session.getState())
                        && (session.getEndTime() + 7 * DateTimeUtils.MILLIS_PER_MINUTE) > currentTime) {
                    throw new ConflictException(ErrorCode.SUBSCRIPTION_SESSION_ACTIVE,
                            "Session payout might be pending");
                }
            }
        }
    }

    public Session markSessionActive(MarkSessionActiveReq req) throws VException {
        logger.info("Request :" + req.toString());
        Session session = sessionDAO.getById(req.getSessionId());
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND,
                    "Session not found with id : " + req.getSessionId());
        }

        try {
            // Update the session state details
            SessionStateDetails sessionStateDetails = sessionStateDetailsManager.markActive(req.getSessionId(),
                    getSessionExpiryTime(session));

            // Make sure attendees are joined
            sessionAttendeeManager.markJoined(req.getSessionId());

            // Update the session
            session.setState(sessionStateDetails.getSessionState());
            session.setActivatedAt(System.currentTimeMillis());

            // callingUserId is null as this event is generated by SYSTEM. We
            // will not overwrite the callingUserId field in this event
            sessionDAO.update(session, null);

            SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session), null);
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", sessionInfo);
            payload.put("event", session.getState().getSessionEvent());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
            asyncTaskFactory.executeTask(params);

        } catch (VException ex) {
            logger.info("Session join not successful - " + ex.toString());
        }

        logger.info("Response:" + session.toString());
        return session;
    }

    @Deprecated //getting used for wiziq sessions only, do not use this method
    public Session markSessionActiveForExpiredWiziq(Long sessionId) throws VException {
        Session session = sessionDAO.getById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND,
                    "Session not found with id : " + sessionId);
        }

        try {
            // Update the session state details
            SessionStateDetails sessionStateDetails = sessionStateDetailsManager.markActive(sessionId,
                    getSessionExpiryTime(session));

            // Make sure attendees are joined
            List<SessionAttendee> sessionAttendees = sessionAttendeeManager.getSessionAttendees(sessionId);
            if (ArrayUtils.isNotEmpty(sessionAttendees)) {
                for (SessionAttendee sessionAttendee : sessionAttendees) {
                    if (com.vedantu.session.pojo.SessionAttendee.SessionUserState.NOT_JOINED.equals(sessionAttendee.getUserState())) {
                        sessionAttendee.setJoinTime(session.getStartTime());
                        sessionAttendee.setUserState(com.vedantu.session.pojo.SessionAttendee.SessionUserState.JOINED);
                        sessionAttendeeDAO.update(sessionAttendee);
                    }
                }
            }

            // Update the session
            session.setState(sessionStateDetails.getSessionState());
            session.setActivatedAt(session.getStartTime());

            // callingUserId is null as this event is generated by SYSTEM. We
            // will not overwrite the callingUserId field in this event
            sessionDAO.update(session, null);

        } catch (VException ex) {
            logger.info("Session join not successful - " + ex.toString());
        }

        logger.info("Response:" + session.toString());
        return session;
    }

    public List<SessionAttendee> processSessionPayout(ProcessSessionPayoutReq req) throws VException {
        logger.info("Request :" + req.toString());

        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        org.hibernate.Session sqlSession = sessionFactory.openSession();
        Transaction transaction = sqlSession.getTransaction();

        SessionStateDetails stateDetails = null;
        try {
            transaction.begin();
            stateDetails = sessionStateDetailsDAO.getBySessionId(req.getSessionId(), sqlSession);
            transaction.commit();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            transaction.rollback();
        } finally {
            if (sqlSession.isOpen()) {
                sqlSession.close();
            }
        }
        if (stateDetails == null) {
            logger.info("processSessionPayoutError : session state not found for req - " + req.toString());
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND,
                    "Session not found with id : " + req.getSessionId());
        }

        // TODO : Rely on sessionStateDetails instead of session
        switch (stateDetails.getSessionState()) {
            case ENDED:
                // Update the attendees based on the req details
                List<SessionAttendee> updateAttendees = sessionAttendeeManager.updateAttendeePostPayout(req);

                logger.info("Response:" + stateDetails);
                return updateAttendees;
            case FORFEITED:
                logger.info("Session is in FORFEITED state. Hence payout does not exist");
                return null;
            default:
                logger.error("processSessionPayout called for session which is not ended : " + stateDetails
                        + "Request : " + req.toString());
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "Session state is not applicable for the processing session payout. req:" + req.toString());
        }
    }

    public List<Session> getSessions(GetSessionsReq getSessionsReq) {
        logger.info("Request : " + getSessionsReq.toString());
        List<Session> sessions = sessionDAO.getSessions(getSessionsReq);
        logger.info("Session count : " + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    public List<SessionInfo> getSessionInfos(GetSessionsReq getSessionsReq) {
        logger.info("Request : " + getSessionsReq.toString());
        List<Session> sessions = sessionDAO.getSessions(getSessionsReq);
        logger.info("Session count : " + sessions == null ? 0 : sessions.size());
        return getSessionInfos(sessions, getSessionsReq.isIncludeAttendees());
    }

    public List<SessionInfo> getSessionInfos(GetSessionsReq getSessionsReq, boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        List<SessionInfo> infos = getSessionInfos(getSessionsReq);
        logger.info("getSessions {}", infos);
        populateUserBasicInfos(infos, exposeEmail);
        return infos;
    }

    public List<SessionInfo> getUserUpcomingSessions(GetUserUpcomingSessionReq getUserUpcomingSessionReq)
            throws VException {
        getUserUpcomingSessionReq.verify();
        List<Session> sessions = sessionDAO.getUserUpcomingSessions(getUserUpcomingSessionReq);
        logger.info("Session count : {}", sessions == null ? 0 : sessions.size());
        return getSessionInfos(sessions, getUserUpcomingSessionReq.isPopulateAttendees());
    }

    public List<SessionInfo> getUserUpcomingSessionsInfo(GetUserUpcomingSessionReq req,
                                                                                         boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        List<SessionInfo> sessions = getUserUpcomingSessions(req);
        logger.info("getUserUpcomingSessionsInfo {}", sessions);
        populateUserBasicInfos(sessions, exposeEmail);
        return sessions;
    }

    public List<SessionInfo> getUserPastSessions(GetUserPastSessionReq getUserPastSessionReq, Role role) throws VException {
        List<Session> sessions = sessionDAO.getUserPastSessions(getUserPastSessionReq, role);
        logger.info("Session count : " + sessions == null ? 0 : sessions.size());
        return getSessionInfos(sessions, true);
    }

    public List<SessionInfo> getUserSessionsForDuration(GetUserUpcomingSessionReq req,
                                                                                         boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        List<Session> sessions = sessionDAO.getUserSessionsForDuration(req);
        List<SessionInfo> sessionInfos = getSessionInfos(sessions, true);
        populateUserBasicInfos(sessionInfos, exposeEmail);
        return sessionInfos;
    }

    private void populateUserBasicInfos(List<SessionInfo> sessionInfos, boolean exposeEmail) {
        if (!org.springframework.util.CollectionUtils.isEmpty(sessionInfos)) {
            // Collect all the userIds from attendee list
            Set<String> userIds = new HashSet<>();
            for (SessionInfo sessionInfo : sessionInfos) {
                if (!org.springframework.util.CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        userIds.add(String.valueOf(userSessionInfo.getUserId()));
                    }
                }
            }

            // Fetch user info and update the session info
            Map<String, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (SessionInfo sessionInfo : sessionInfos) {
                if (!org.springframework.util.CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                    List<UserSessionInfo> users = new ArrayList<>();
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        userSessionInfo.updateUserDetails(userInfos.get(String.valueOf(userSessionInfo.getUserId())));
                        users.add(userSessionInfo);
                    }
                    sessionInfo.setAttendees(users);
                }
            }
        }
    }

    public List<SessionInfo> getUserPastSessionsInfo(GetUserPastSessionReq req, boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        List<SessionInfo> sessionInfos = getUserPastSessions(req, req.getCallingUserRole());
        logger.info("getUserPastSessionsInfo {}", sessionInfos);
        populateUserBasicInfos(sessionInfos, exposeEmail);
        return sessionInfos;
    }


    public List<SessionInfo> getUserLatestActiveSession(GetUserLatestActiveSessionReq getUserLatestActiveSessionReq)
            throws VException {
        logger.info("Request : " + getUserLatestActiveSessionReq.toString());
        getUserLatestActiveSessionReq.verify();
        List<SessionInfo> response = new ArrayList<>();
        List<Session> sessions = sessionDAO.getUserLatestActiveSessions(getUserLatestActiveSessionReq);
        long currentTimeMillis = System.currentTimeMillis();
        if (!CollectionUtils.isEmpty(sessions)) {
            for (Session entry : sessions) {
                if (SessionState.ACTIVE.equals(entry.getState()) || getSessionExpiryTime(entry) > currentTimeMillis) {
                    List<Session> sessionList = new ArrayList<>();
                    sessionList.add(entry);
                    response.addAll(getSessionInfos(sessionList, true));
                    break;
                }
            }
        }

        logger.info("Session count : " + response.size());
        return response;
    }

    public GetLatestUpComingOrOnGoingSessionResponse getUserLatestSessionsRedis(GetUserUpcomingSessionReq req)
            throws IllegalArgumentException, IllegalAccessException, VException {
        GetLatestUpComingOrOnGoingSessionResponse res = new GetLatestUpComingOrOnGoingSessionResponse();
        req.verify();
        String cacheKey = redisDAO.getUserLatestSessionCacheKey(req.getCallingUserId());
        if (StringUtils.isNotEmpty(cacheKey)) {

            String response = null;
            try {
                response = redisDAO.get(cacheKey);
            } catch (Exception e) {
                logger.info("redis get throws exception." + cacheKey + " " + e);
            }
            if (StringUtils.isEmpty(response)) {
                res = getUserLatestSessions(req);
                int expiry = getExpiryTimeForLastestSessions(res);
                if (expiry > DateTimeUtils.SECONDS_PER_MINUTE) {
//                    if (res.getOtfSession() == null && res.getSessionRes() == null && res.getWebinarSession() == null) {
//                        expiry = DateTimeUtils.SECONDS_PER_MINUTE * 5;
//                    }
                    String redisValue = gson.toJson(res);
                    try {
                        redisDAO.setex(cacheKey, redisValue, expiry);
                    } catch (Exception e) {
                        logger.info("redis setex throws exception." + cacheKey + " " + e);
                    }
                }
            } else {
                res = gson.fromJson(response, GetLatestUpComingOrOnGoingSessionResponse.class);
            }
        }
        res.setCurrentSystemTime(System.currentTimeMillis());
        return res;
    }

    public int getExpiryTimeForLastestSessions(GetLatestUpComingOrOnGoingSessionResponse res) {
        Long currentTime = System.currentTimeMillis();
        Long expiryTime = new Long(DateTimeUtils.MILLIS_PER_MINUTE * LATEST_SESSION_CACHE_EXPIRY_MINUTE);
        expiryTime += currentTime;
        if (res != null) {
            if (res.getSessionRes() != null) {
                if (res.getSessionRes().getEndTime() != null && res.getSessionRes().getEndTime() < expiryTime) {
                    expiryTime = res.getSessionRes().getEndTime();
                }
                if (res.getSessionRes().getSessionExpireTime() != null && res.getSessionRes().getSessionExpireTime() < expiryTime
                        && res.getSessionRes().getSessionExpireTime() > currentTime) {
                    expiryTime = res.getSessionRes().getSessionExpireTime();
                }
            }
            if (res.getOtfSession() != null && res.getOtfSession().getEndTime() != null) {
                if (res.getOtfSession().getEndTime() < expiryTime) {
                    expiryTime = res.getOtfSession().getEndTime();
                }
            }
            if (res.getWebinarSession() != null && res.getWebinarSession().getEndTime() != null) {
                if (res.getWebinarSession().getEndTime() < expiryTime) {
                    expiryTime = res.getWebinarSession().getEndTime();
                }
            }
        }
        int expiry = (int) ((expiryTime - currentTime) / 1000);
        if (expiry > 0) {
            return expiry;
        } else {
            return 0;
        }
    }

    public GetLatestUpComingOrOnGoingSessionResponse getUserLatestSessions(GetUserUpcomingSessionReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        GetLatestUpComingOrOnGoingSessionResponse res = new GetLatestUpComingOrOnGoingSessionResponse();
        // Fetch OTO session
        try {
            GetUserLatestActiveSessionReq getUserLatestActiveSessionReq = new GetUserLatestActiveSessionReq();
            getUserLatestActiveSessionReq.setCallingUserId(req.getCallingUserId());
            getUserLatestActiveSessionReq.setCallingUserRole(req.getCallingUserRole());
            getUserLatestActiveSessionReq.setStart(0);
            getUserLatestActiveSessionReq.setSize(10);
            List<SessionInfo> sessions = getUserLatestActiveSession(getUserLatestActiveSessionReq);
            SessionInfo sessionInfo = null;
            if (CollectionUtils.isNotEmpty(sessions)) {
                sessionInfo = sessions.get(0);
            }
            res.setSessionRes(sessionInfo);
        } catch (Exception ex) {
            // Error log will provide huge no of sentry errors. Making it info for now
            logger.info("Fetching latest oto session failed:" + ex.getMessage() + " req:" + req.toString());
        }

        // Fetch OTF session
        try {
            GetOTFUpcomingSessionReq getOTFUpcomingSessionReq = new GetOTFUpcomingSessionReq(System.currentTimeMillis(),
                    null, null, 0, 1);
            switch (req.getCallingUserRole()) {
                case TEACHER:
                    getOTFUpcomingSessionReq.setTeacherId(String.valueOf(req.getCallingUserId()));
                    break;
                case STUDENT:
                    getOTFUpcomingSessionReq.setStudentId(String.valueOf(req.getCallingUserId()));
                    break;
                default:
                    logger.info("Role invalid for the above request : " + req.toString());
                    throw new VException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user");
            }

            GetLatestUpComingOrOnGoingSessionResponse otfResponse = getUpcomingSessionsForTicker(getOTFUpcomingSessionReq);
            if (otfResponse.getOtfSession() != null) {
                // Populate users
                populateUserBasicInfos(otfResponse.getOtfSession(), false, false);
            }
            if (otfResponse.getWebinarSession() != null) {
                // Populate users
                populateUserBasicInfos(otfResponse.getWebinarSession(), false, false);
            }

            res.includeResponse(otfResponse);
        } catch (Exception ex) {
            // Error log will provide huge no of sentry errors. Making it info for now
            logger.info("Fetching latest otf session failed:" + ex.getMessage() + " req:" + req.toString());
        }

        // Reset current time
        res.setCurrentSystemTime(System.currentTimeMillis());
        return res;
    }

    public void populateUserBasicInfos(OTFSessionPojoUtils sessionInfo, boolean includeAttendees, boolean exposeEmail) {
        Set<String> userIds = new HashSet<>();
        String presenter = sessionInfo.getPresenter();
        if (!StringUtils.isEmpty(presenter)) {
            userIds.add(presenter);
        }

        if (includeAttendees && !org.springframework.util.CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
            userIds.addAll(sessionInfo.getAttendees());
        }

        Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
        if (userMap != null && !userMap.isEmpty()) {
            // Populate presenter
            if (userMap.containsKey(presenter)) {
                sessionInfo.setPresenterInfo(userMap.get(presenter));
            }

            // Populate attendees
            if (includeAttendees && !org.springframework.util.CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                List<UserBasicInfo> attendees = new ArrayList<>();
                for (String attendee : sessionInfo.getAttendees()) {
                    if (userMap.containsKey(attendee)) {
                        attendees.add(userMap.get(attendee));
                    }
                }
            }
        }
    }

    public GetLatestUpComingOrOnGoingSessionResponse getUpcomingSessionsForTicker(
            GetOTFUpcomingSessionReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {

        GetLatestUpComingOrOnGoingSessionResponse sessions = oTFSessionManager.getUpcomingSessionsForTicker(req.getAfterEndTime(), req.getTeacherId(), req.getStudentId(),
                req.getStart(), req.getLimit());
        logger.info("Response : " + sessions);

        // if(sessions != null && sessions.getOtfSession() != null){
        // SessionPojo otf = sessions.getOtfSession();
        // if(otf != null && otf.getBatchIds() != null && otf.getBoardId() != null){
        // try{
        // GetLastSessionRemarkResp previousSessionResp =
        // remarkManager.getLastSessionRemark(otf.getBatchIds(),
        // otf.getBoardId(),otf.getId());
        // sessions.getOtfSession().setPreviousSessionRemark(previousSessionResp.getLastSessionRemark());
        // }catch(Exception e){
        // logger.info("remark not found");
        // }
        // }
        // }
        return sessions;
    }

    public List<SessionInfo> getUserCalendarSessions(GetUserCalendarSessionsReq getUserCalendarSessionsReq)
            throws VException {
        logger.info("Request : " + getUserCalendarSessionsReq.toString());
        getUserCalendarSessionsReq.verify();
        List<Session> sessions = sessionDAO.getUserCalendarSessions(getUserCalendarSessionsReq);

        List<SessionInfo> response = new ArrayList<>();
        response.addAll(getSessionInfos(sessions, true));
        logger.info("Session count : " + response.size());
        return response;
    }

    public List<SessionInfo> getSubscriptionSessions(GetSubscriptionSessionsReq getSubscriptionSessionsReq)
            throws VException {
        logger.info("Request : " + getSubscriptionSessionsReq.toString());
        getSubscriptionSessionsReq.verify();
        List<Session> sessions = sessionDAO.getSubscriptionSessions(getSubscriptionSessionsReq.getSubscriptionId(),
                getSubscriptionSessionsReq.getStart(), getSubscriptionSessionsReq.getSize());
        logger.info("Session count : " + sessions == null ? 0 : sessions.size());
        return getSessionInfos(sessions, false);
    }

    public Long getSubscriptionScheduledHours(Long subscriptionId) throws VException {
        logger.info("Request : " + subscriptionId);
        List<Session> sessions = sessionDAO.getSubscriptionSessions(subscriptionId, null, null);
        long scheduledHours = 0l;
        for (Session session : sessions) {
            SessionState displayState = SessionUtils.getDisplayState(session.getState(), getSessionExpiryTime(session));
            if (session.getEndTime() != null && session.getStartTime() != null
                    && (SessionState.SCHEDULED.equals(displayState) || SessionState.STARTED.equals(displayState)
                    || SessionState.ACTIVE.equals(displayState))) {
                scheduledHours += (session.getEndTime() - session.getStartTime());
            }
        }
        logger.info("getSubscriptionScheduledHours  " + scheduledHours);
        return scheduledHours;
    }

    public long getSessionExpiryTime(Session session) {
        return session.getEndTime();
    }

    private long getSessionExpiryTime(long startTime) {
        return startTime + getExpirydurationInMillis();
    }

    private long getExpirydurationInMillis() {
        return ConfigUtils.INSTANCE.getLongValue("session.expiry.time.min") * 60 * 1000;
    }

    private static void isSessionUpdateAllowedForUser(Session session, Long userId) throws VException {
        isSessionUpdateAllowedForUser(session, userId, null);
    }

    private static void isSessionUpdateAllowedForUser(Session session, Long userId, Role role) throws VException {
        // Action should be allowed for attendees and ADMIN
        if (!SessionUtils.getAttendeeIds(session).contains(userId) && !Role.ADMIN.equals(role)) {
            throw new ForbiddenException(ErrorCode.SESSION_FORBIDDEN_FOR_USER,
                    "Action not allowed for the user : " + userId + " for sessionId: " + session.getId());
        }
    }

    // no usage found
    @Deprecated
    public List<SessionInfo> getSessionInfos(Session session, boolean populateAttendees) {
        List<Session> sessions = new ArrayList<>();
        if (session != null) {
            sessions.add(session);
        }
        return getSessionInfos(sessions, populateAttendees);
    }

    public List<SessionInfo> getSessionInfos(List<Session> sessions, boolean populateAttendees) {
        if (!CollectionUtils.isEmpty(sessions)) {
            if (populateAttendees) {
                List<SessionAttendee> sessionAttendees = sessionAttendeeManager
                        .getSessionAttendees(SessionUtils.getSessionIds(sessions));
                return getSessionInfos(sessions, sessionAttendees);
            } else {
                return sessions.stream()
                        .map(e -> new SessionInfo(e, getSessionExpiryTime(e), null))
                        .collect(Collectors.toList());
            }
        }
        return new ArrayList<>();
    }

    public List<SessionInfo> getSessionInfos(List<Session> sessions, List<SessionAttendee> attendees) {
        List<SessionInfo> sessionInfos = new ArrayList<>();

        for (Session session : sessions) {
            Map<Long, List<SessionAttendee>> attendeeMap = createSessionAttendeeMap(attendees);
            sessionInfos.add(new SessionInfo(session, getSessionExpiryTime(session), attendeeMap.get(session.getId())));
        }
        return sessionInfos;
    }

    public static Map<Long, List<SessionAttendee>> createSessionAttendeeMap(List<SessionAttendee> attendees) {
        Map<Long, List<SessionAttendee>> attendeeMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(attendees)) {
            for (SessionAttendee attendee : attendees) {
                List<SessionAttendee> existingList = attendeeMap.get(attendee.getSessionId());
                if (CollectionUtils.isEmpty(existingList)) {
                    existingList = new ArrayList<>();
                }
                existingList.add(attendee);
                attendeeMap.put(attendee.getSessionId(), existingList);
            }
        }

        return attendeeMap;
    }

    public long getTotalSessionDuration(Long afterEndTime, Long beforeEndTime) throws BadRequestException {
        if (afterEndTime != null && beforeEndTime != null && beforeEndTime < afterEndTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Invalid request params. beforeEndTime should be greater than afterEndTime");
        }
        long duration = sessionDAO.getTotalSessionDuration(afterEndTime, beforeEndTime);
        logger.info(
                "Duration : " + duration + " lastUpdatedTime : " + afterEndTime + " currentTime : " + beforeEndTime);
        return duration;
    }

    public TotalSessionDuration getTotalSessionDurationForTeacher(Long userId) {
        TotalSessionDuration duration = sessionDAO.getTotalSessionDurationForTeacher(userId);
        logger.info("Duration : " + duration + " userId: " + userId);
        return duration;
    }

    public SessionInfo getLatestTrial(Long studentId, Long teacherId) {
        Long fromTime = System.currentTimeMillis();

        GetSessionsReq req = new GetSessionsReq();
        req.setTeacherId(teacherId);
        req.setStudentId(studentId);
        req.setAfterStartTime(fromTime);
        List<SessionState> states = new ArrayList<>();
        states.add(SessionState.SCHEDULED);
        req.setSessionStates(states);
        req.setModel(SessionModel.TRIAL);
        req.setSortType(SessionSortType.START_TIME_ASC);
        req.setSize(1);
        req.setIncludeAttendees(true);

        List<SessionInfo> sessions = getSessionInfos(req);

        if (sessions != null && !sessions.isEmpty()) {
            return sessions.get(0);
        } else {
            return null;
        }
    }

    public GetSessionBillDetailsRes getSessionBillDetails(GetSessionBillDetailsReq req) throws VException {
        try {
            SessionAttendee sessionAttendee = sessionAttendeeManager.getSessionAttendees(req.getSessionId(),
                    req.getUserId());
            GetSessionBillDetailsRes getSessionBillDetailsRes = new GetSessionBillDetailsRes(
                    sessionAttendee.getBillingPeriod(), sessionAttendee.getChargedAmount());
            return getSessionBillDetailsRes;
        } catch (VException ex) {
            logger.error(ex.getErrorMessage());
            throw ex;
        }
    }

    public GetSessionPartnersRes getSessionPartners(GetSessionPartnersReq req, boolean fetchEndedOnly,
            Long beforeTime) {

        List<UserBasicInfo> partners = new ArrayList<>();
        Set<Long> partnerIds = sessionDAO.getSessionPartners(req.getCallingUserId(), fetchEndedOnly, beforeTime);
        if (ArrayUtils.isNotEmpty(partnerIds)) {
            Map<Long, UserBasicInfo> partnersMap = userUtils.getUserBasicInfosMapFromLongIds(partnerIds, false);
            // to preserve the order
            for (Long partnerId : partnerIds) {
                if (partnersMap.containsKey(partnerId)) {
                    partners.add(partnersMap.get(partnerId));
                }
            }
        }
        GetSessionPartnersRes res = new GetSessionPartnersRes(partners);
        return res;
    }

    public GetScheduledAndAllSessionCountRes getSessionCounts(Long userId, Role userRole) throws VException {
        GetScheduledAndAllSessionCountRes getScheduledAndAllSessionCountRes = new GetScheduledAndAllSessionCountRes();
        GetSessionsReq getSessionsReq = new GetSessionsReq();
        if (Role.STUDENT.equals(userRole)) {
            getSessionsReq.setStudentId(userId);
        } else if (Role.TEACHER.equals(userRole)) {
            getSessionsReq.setTeacherId(userId);
        }
        // ended session count
        List<SessionState> sessionStates = new ArrayList<>();
        sessionStates.add(SessionState.SCHEDULED);
        getSessionsReq.setSessionStates(sessionStates);
        Long scheduledSessionsCount = sessionDAO.getSessionsCount(getSessionsReq);

        // scheduled session count
        sessionStates.clear();
        sessionStates.add(SessionState.ENDED);
        getSessionsReq.setSessionStates(sessionStates);
        Long endedSessionsCount = sessionDAO.getSessionsCount(getSessionsReq);

        // trial session count
        getSessionsReq.setModel(SessionModel.TRIAL);
        Long trailSessionsCount = sessionDAO.getSessionsCount(getSessionsReq);

        getScheduledAndAllSessionCountRes.setAllSessionsCount(endedSessionsCount);
        getScheduledAndAllSessionCountRes.setScheduledSessionCount(scheduledSessionsCount);
        getScheduledAndAllSessionCountRes.setTrialSessionCount(trailSessionsCount);

        return getScheduledAndAllSessionCountRes;
    }

    public List<FailedPayoutInfo> getEndedSessionsToday() {
        List<FailedPayoutInfo> sessions = sessionDAO.getEndedSessionsToday();
        Map<Long, List<SessionAttendee>> attendeeMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(sessions)) {
            List<Long> sessionIds = new ArrayList<>();
            for (FailedPayoutInfo session : sessions) {
                sessionIds.add(session.getSessionId());
            }
            List<SessionAttendee> sessionAttendees = sessionAttendeeManager
                    .getSessionAttendees(sessionIds);
            attendeeMap = createSessionAttendeeMap(sessionAttendees);

            for (FailedPayoutInfo session : sessions) {
                List<SessionAttendee> sessionAttendeeList = attendeeMap.get(session.getSessionId());
                if (CollectionUtils.isNotEmpty(sessionAttendeeList)) {
                    Integer billingDuration = sessionAttendeeList.get(0).getSavedBillingPeriod();
                    session.setBillingDuration(billingDuration);
                }
            }
        }
        return sessions;

    }

    public void triggerSessionEventSNS(SessionEvents event, SessionInfo sessionInfo) {
        try {
            String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":SESSION_EVENTS_" + env.toUpperCase();
            String msg = new Gson().toJson(sessionInfo);
            String snsSubject = event.name();
            PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
            PublishResult publishResult = snsClient.publish(publishRequest);
            logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
        } catch (Exception ex) {
            logger.error("Error creating the sns session event : " + event.toString() + " " + ex.getMessage());
        }
    }

    public SessionInfo setVimeoVideoId(Long sessionId, SetVimeoVideoIdRequest setVimeoVideoIdRequest) throws BadRequestException {
        setVimeoVideoIdRequest.verify();
        Session session = sessionDAO.getById(sessionId);
        logger.info("Session pre update:" + session.toString());
        session.setVimeoId(setVimeoVideoIdRequest.getVimeoId());
        sessionDAO.update(session, setVimeoVideoIdRequest.getCallingUserId());
        logger.info("Session post update:" + session.toString());
        SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session),
                sessionAttendeeManager.getSessionAttendees(session.getId()));
        logger.info("Response : " + sessionInfo.toString());
        return sessionInfo;
    }

    public String getUserLatestSessionCacheKey(Long userId) {
        if (userId == null) {
            return null;
        } else {
            return "user_latest_session_" + userId;
        }
    }

    public void checkAndPublishRedisEvent(SessionEvents event, SessionInfo sessionInfo) {
        try {
            redisDAO.publish(event.name(), String.valueOf(sessionInfo.getId()));
        } catch (InternalServerErrorException ex) {
            logger.error("Error checkAndPublishRedisEvent session event : " + event.name() + " sessionId:" + event.name() + ", error " + ex.getMessage());
        }
    }

    public void triggerSNS(SNSTopicOTF topic, String subject, String message) {
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic + "_" + env.toUpperCase();
        PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
        logger.info("publishRequest " + publishRequest);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }

    public Boolean checkFirstBookedSession(Long studentId) {
        Boolean response = false;

        GetSessionsReq getSessionsReq = new GetSessionsReq();
        getSessionsReq.setStudentId(studentId);
        getSessionsReq.setSortType(SessionSortType.START_TIME_ASC);
        // ended session count
        List<SessionState> sessionStates = new ArrayList<>();
        sessionStates.add(SessionState.SCHEDULED);
        sessionStates.add(SessionState.ACTIVE);
        sessionStates.add(SessionState.ENDED);
        sessionStates.add(SessionState.STARTED);
        getSessionsReq.setSessionStates(sessionStates);
        getSessionsReq.setSize(20);

        List<Session> sessions = sessionDAO.getSessions(getSessionsReq);

        sessionStates.clear();
        sessionStates.add(SessionState.ENDED);
        getSessionsReq.setSessionStates(sessionStates);
        getSessionsReq.setSize(2);
        Long endedSessionsCount = sessionDAO.getSessionsCount(getSessionsReq);

        logger.info("scheduled sessions count : " + (sessions == null ? 0 : sessions.size()));
        logger.info("ended sessions count : " + endedSessionsCount);
        if (!CollectionUtils.isEmpty(sessions) && endedSessionsCount == 0L) {
            Long currentTime = System.currentTimeMillis();
            Long currentDayStart = CalendarUtils.getDayStartTime(currentTime);
            response = true;
            for (Session session : sessions) {
                if (session.getCreationTime() != null && session.getCreationTime() > 0
                        && (currentDayStart > session.getCreationTime())) {
                    response = false;
                    break;
                }
            }
        }
        return response;
    }

    public void markSessionExpired(MarkSessionExpiredReq req) {
        Session session = sessionDAO.getById(req.getSessionId());
        logger.info("Session pre update:" + session.toString());
        session.setState(SessionState.EXPIRED);
        sessionDAO.update(session, req.getCallingUserId());
        logger.info("Session post update:" + session.toString());

        // Update session state details as well
        try {
            sessionStateDetailsManager.markExpired(session.getId());
        } catch (VException ex) {
            String errorMessage = "Error occured marking the session state active:" + session.toString() + " ex:"
                    + ex.getMessage();
            logger.error(errorMessage);
        }

        SessionInfo sessionInfo = new SessionInfo(session, getSessionExpiryTime(session), null);
        Map<String, Object> payload = new HashMap<>();
        payload.put("sessionInfo", sessionInfo);
        payload.put("event", session.getState().getSessionEvent());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_EVENTS_TRIGGER, payload);
        asyncTaskFactory.executeTask(params);
    }

    public List<Session> getSessionsByCreationTime(GetSessionsByCreationTime req) {
        return sessionDAO.getSessionsByCreationTime(req);
    }

    public void updateTOSSession(long sessionId) throws NotFoundException {
        updateTOSSession(getSessionById(sessionId));
    }

    public void updateTOSSession(Session session) {
        switch (SessionUtils.getDisplayState(session.getState(), getSessionExpiryTime(session))) {
            case SCHEDULED:
                // fetchTOSType
                break;
            case CANCELED:
            case EXPIRED:
            case FORFEITED:

                break;
            default:
                break;
        }
    }

    public void fetchTOSType(Session session) {
        // TODO : Implement this
    }

    public PlatformBasicResponse updateLiveSessionPlatformDetails(UpdateLiveSessionPlatformDetailsReq req)
            throws VException {
        Session session = sessionDAO.getById(req.getSessionId());
        if (null == session) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found");
        }
        if (LiveSessionPlatformType.GTM.equals(req.getLiveSessionPlatformType())
                || LiveSessionPlatformType.GTT.equals(req.getLiveSessionPlatformType())) {
            GTMGTTSessionDetails details = new GTMGTTSessionDetails();
            details.setStudentLink(req.getStudentLink());
            details.setTeacherLink(req.getTeacherLink());
            session.setLiveSessionPlatformDetails(details);
            session.setLiveSessionPlatformType(req.getLiveSessionPlatformType());
        }
        sessionDAO.insert(session);
        return new PlatformBasicResponse();
    }

    public List<SessionInfo> getAttendeesNextSessions(Long sessionId, Long userId, Role role) throws NotFoundException, VException {
        Session session = sessionDAO.getById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "Session not found for id:" + sessionId);
        }
        isSessionUpdateAllowedForUser(session, userId, null);

        List<Session> sessions = sessionDAO.getAttendeesNextSessions(userId, role);

        // Remove the existing sessions
        removeSessionFromList(sessions, sessionId);

        return getSessionInfos(sessions, false);
    }

    public SessionStateDetails getSessionStateDetailsById(Long sessionId) throws Exception {
        return sessionStateDetailsManager.getSessionStateDetailsBySessionId(sessionId);
    }

    private void removeSessionFromList(List<Session> sessions, Long sessionId) {
        if (!CollectionUtils.isEmpty(sessions) && sessionId != null) {
            Iterator<Session> iterator = sessions.iterator();
            while (iterator.hasNext()) {
                Session session = iterator.next();
                if (sessionId.equals(session.getId())) {
                    iterator.remove();
                }
            }
        }
    }

    private boolean isDemoSession(Session session) {
        // TODO : Below code is more of a hack and need to have a permanent flag
        // to identify the demo sessions scheduled by ADMIN
        if ("Orientation/Demo Session".equals(session.getTitle()) && SessionPayoutType.FREE.equals(session.getType())
                && "demo".equals(session.getTopic())) {
            return true;
        }
        return false;
    }

    public BasicRes migrateSessionStateDetails(Long startTime, Long endTime, Integer batchCount) {
        logger.info("Request:" + startTime + " endTime:" + endTime);
        Query query = new Query();
        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.SCHEDULED));
        if (startTime != null && startTime > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.START_TIME).gte(startTime));
        } else {
            query.addCriteria(Criteria.where(Session.Constants.START_TIME).gte(System.currentTimeMillis()));
        }

        if (endTime != null && endTime > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).lte(endTime));
        }
        List<Session> sessions = sessionDAO.runQuery(query, Session.class);
        logger.info("Scheduled Session : " + sessions.size());
        if (!CollectionUtils.isEmpty(sessions)) {
            sessionStateDetailsManager.migrateSessionStateDetails(sessions, batchCount);
        }
        return new BasicRes();
    }

    public BasicRes migrateOneSessionStateDetails(long sessionId) throws NotFoundException {
        logger.info("Request : " + sessionId);
        Session sessionEntry = getSessionById(sessionId);
        sessionStateDetailsManager.migrateOneSessionStateDetails(sessionEntry);
        return new BasicRes();
    }

    public Map<String, TrialSessionInfos> getCoursePlanTrialSessionInfo(Set<String> coursePlanIds) {
        List<Session> sessions = sessionDAO.getCoursePlanTrialSessionInfo(coursePlanIds);
        Map<String, TrialSessionInfos> trialSessionInfosMap = new HashMap<>();
        for (Session session : sessions) {
            TrialSessionInfos trialSessionInfos;
            if (trialSessionInfosMap.get(session.getContextId()) == null) {
                trialSessionInfos = new TrialSessionInfos();
                trialSessionInfosMap.put(session.getContextId(), trialSessionInfos);
            } else {
                trialSessionInfos = trialSessionInfosMap.get(session.getContextId());
            }

            trialSessionInfos.setTrialSessionsTotal(trialSessionInfos.getTrialSessionsTotal() + 1);

            switch (session.getState()) {
                case SCHEDULED:
                    trialSessionInfos.setTrialSessionsScheduled(trialSessionInfos.getTrialSessionsScheduled() + 1);
                    break;
                case EXPIRED:
                    trialSessionInfos.setTrialSessionsExpired(trialSessionInfos.getTrialSessionsExpired() + 1);
                    break;
                case FORFEITED:
                    trialSessionInfos.setTrialSessionsForfeited(trialSessionInfos.getTrialSessionsForfeited() + 1);
                    break;
                case CANCELED:
                    trialSessionInfos.setTrialSessionsCancelled(trialSessionInfos.getTrialSessionsCancelled() + 1);
                    break;
                case ENDED:
                    trialSessionInfos.setTrialSessionsEnded(trialSessionInfos.getTrialSessionsEnded() + 1);
                    break;
                default:
                    break;
            }

        }
        return trialSessionInfosMap;
    }

    public List<SessionInfo> getSessionsForEnding() {
        logger.info("Request:" + System.currentTimeMillis());
        List<Session> results = new ArrayList<>();

        // Get sessions that need to be ended
        Query sessionsTobeEndedQuery = new Query();
        sessionsTobeEndedQuery.addCriteria(Criteria.where(Session.Constants.SESSION_STATE)
                .in(Arrays.asList(SessionState.SCHEDULED, SessionState.STARTED, SessionState.ACTIVE)));
        sessionsTobeEndedQuery.addCriteria(Criteria.where(Session.Constants.END_TIME)
                .lte(System.currentTimeMillis() - 2 * DateTimeUtils.MILLIS_PER_MINUTE));
        List<Session> sessionsTobeEnded = sessionDAO.runQuery(sessionsTobeEndedQuery, Session.class);
        if (!CollectionUtils.isEmpty(sessionsTobeEnded)) {
            results.addAll(sessionsTobeEnded);
            logger.info("Sessions to be ended:" + sessionsTobeEnded.size());
        }

//        // Get sessions that need to be expired
//        Query sessionsTobeExpiredQuery = new Query();
//        sessionsTobeExpiredQuery.addCriteria(Criteria.where(Session.Constants.SESSION_STATE)
//                .in(Arrays.asList(SessionState.SCHEDULED, SessionState.STARTED)));
//        sessionsTobeExpiredQuery.addCriteria(Criteria.where(Session.Constants.START_TIME)
//                .lte(System.currentTimeMillis() - getExpirydurationInMillis()));
//        sessionsTobeExpiredQuery.addCriteria(Criteria.where(Session.Constants.END_TIME).gt(System.currentTimeMillis()));
//        List<Session> sessionsTobeExpired = sessionDAO.runQuery(sessionsTobeExpiredQuery, Session.class);
//        if (!CollectionUtils.isEmpty(sessionsTobeExpired)) {
//            results.addAll(sessionsTobeExpired);
//            logger.info("Sessions to be expired:" + sessionsTobeExpired.size());
//        }
        return getSessionInfos(results, true);
    }

    public Map<Long, TeacherSessionDashboard> getTeacherSessionForDashboard(Long startTime, Long endTime) {
        List<Session> sessions = new ArrayList<>();
        Set<Long> lateJoinSessions = new HashSet<>();
        int start = 0, limit = 100;
        Map<Long, TeacherSessionDashboard> teacherSessionCountMap = new HashMap<>();
        while (true) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(startTime).lt(endTime));
            query.fields().include(Session.Constants._ID);
            query.fields().include(Session.Constants.ID);
            query.fields().include(Session.Constants.TEACHER_ID);
            query.fields().include(Session.Constants.STUDENT_IDS);
            query.fields().include(Session.Constants.SESSION_STATE);
            query.fields().include(Session.Constants.SUBJECT);
            query.fields().include(Session.Constants.RESCHEDULE_DATA);
            query.fields().include(Session.Constants.CANCELLED_BY);
            sessionDAO.setFetchParameters(query, start, limit);
            query.with(Sort.by(Sort.Direction.DESC, Session.Constants.LAST_UPDATED));
            List<Session> sessionsTemp = (sessionDAO.runQuery(query, Session.class));
            start = start + limit;
            List<Long> sessionIds = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(sessionsTemp)) {
                for (Session session : sessionsTemp) {
                    sessionIds.add(session.getId());
                }
            } else {
                break;
            }
            lateJoinSessions.addAll(sessionAttendeeManager.checkForTeacherJoinTime(sessionIds));
            sessions.addAll(sessionsTemp);
        }
        if (ArrayUtils.isEmpty(sessions)) {
            return teacherSessionCountMap;
        }
        logger.info("No. of Sessions for Dashboard from getTeacherSessionForDashboard : " + sessions.size());
        for (Session session : sessions) {
            TeacherSessionDashboard teacherSessionDashboard;
            Long teacherId = session.getTeacherId();
            if (teacherSessionCountMap.containsKey(teacherId)) {
                teacherSessionDashboard = teacherSessionCountMap.get(teacherId);
            } else {
                teacherSessionDashboard = new TeacherSessionDashboard();
                teacherSessionDashboard.setTeacherId(teacherId);
                teacherSessionDashboard.setStudentIds(new HashSet<Long>());
            }
            if (lateJoinSessions.contains(session.getId())) {
                teacherSessionDashboard.setLateJoined(teacherSessionDashboard.getLateJoined() + 1);
            }
            incrementTeacherSessionDashboard(teacherSessionDashboard, session);
            teacherSessionCountMap.put(teacherId, teacherSessionDashboard);
        }
        return teacherSessionCountMap;
    }

    public List<ContextSessionDurationResponse> getSessionDurationByContextId(GetSessionDurationByContextIdReq req)
            throws BadRequestException {
        logger.info("Request:" + req.toString());

        // Validate request
        req.verify();

        List<ContextSessionDurationResponse> contextSessionDurationResponseList = new ArrayList<>();

        // Fetch the ended duration
        for (String contextId : req.getContextIds()) {
            ContextSessionDurationResponse contextSessionDurationResponse = new ContextSessionDurationResponse(
                    contextId, req.getEntityType());

            // Fetch ended duration
            Set<Long> sessionIds = sessionDAO.getEndedSessionIdsByContext(req.getEntityType(), contextId);
            long endedSessionDuration = sessionAttendeeManager.getEndedSessionDurationBySessionIds(sessionIds);
            contextSessionDurationResponse.setEndedSessionDuration(Long.valueOf(endedSessionDuration).intValue());
            logger.info("EndedSessionDuration :" + endedSessionDuration + " for contextId:" + contextId);

            // Fetch duration based on state
            List<TotalSessionDuration> sessionStateDurationList = sessionDAO
                    .getSessionStateDurationByContext(req.getEntityType(), contextId);
            logger.info("sessionStateDurationList --> "+sessionStateDurationList);
            if (!CollectionUtils.isEmpty(sessionStateDurationList)) {
                sessionStateDurationList.stream()
                        .filter(s -> !COMPLETE_UNSUCCESSFUL_SESSION_STATES.contains(s.getState())).forEach(s -> {
                    if (ONGOING_SESSION_STATES.contains(s.getState())) {
                        contextSessionDurationResponse.incrementActiveSessionDuration(s.getDuration());
                    }
                    if (UPCOMING_SESSION_STATES.contains(s.getState())) {
                        contextSessionDurationResponse.incrementUpcomingDuration(s.getDuration());
                    }
                });
            }

            contextSessionDurationResponseList.add(contextSessionDurationResponse);
        }

        logger.info("contextSessionDurationResponseList --> "+contextSessionDurationResponseList);
        return contextSessionDurationResponseList;
    }

    public void incrementTeacherSessionDashboard(TeacherSessionDashboard teacherSessionDashboard, Session session) {
        if (ArrayUtils.isNotEmpty(session.getRescheduleData())) {
            teacherSessionDashboard
                    .setRescheduled(teacherSessionDashboard.getRescheduled() + session.getRescheduleData().size());
        }
        switch (session.getState()) {
            case SCHEDULED:
            case EXPIRED:
                teacherSessionDashboard.setExpired(teacherSessionDashboard.getExpired() + 1);
                teacherSessionDashboard.setBooked(teacherSessionDashboard.getBooked() + 1);
                break;
            case FORFEITED:
                teacherSessionDashboard.setForfeited(teacherSessionDashboard.getForfeited() + 1);
                teacherSessionDashboard.setBooked(teacherSessionDashboard.getBooked() + 1);
                break;
            case CANCELED:
                if (session.getCancelledBy() != null) {
                    if (session.getTeacherId().equals(session.getCancelledBy())) {
                        teacherSessionDashboard.setCancelledByTeacher(teacherSessionDashboard.getCancelledByTeacher() + 1);
                    } else if (session.getStudentIds().contains(session.getCancelledBy())) {
                        teacherSessionDashboard.setCancelledByStudent(teacherSessionDashboard.getCancelledByStudent() + 1);
                    } else {
                        teacherSessionDashboard.setCancelledByAdmin(teacherSessionDashboard.getCancelledByAdmin() + 1);
                    }
                }
                teacherSessionDashboard.setCancelled(teacherSessionDashboard.getCancelled() + 1);
                teacherSessionDashboard.setBooked(teacherSessionDashboard.getBooked() + 1);
                break;
            case ENDED:
                teacherSessionDashboard.setEnded(teacherSessionDashboard.getEnded() + 1);
                teacherSessionDashboard.setBooked(teacherSessionDashboard.getBooked() + 1);
                teacherSessionDashboard.getStudentIds().addAll(session.getStudentIds());
                break;
            default:
                break;
        }
    }

    public List<SessionAttendee> getEndedSessionAttendeesOfContext(String contextId, EntityType contextType) {
        Set<Long> sessionIds = sessionDAO.getEndedSessionIdsByContext(contextType, contextId);
        if (sessionIds.size() >= SessionAttendeeDAO.MAX_ALLOWED_FETCH_SIZE) {
            logger.error("Got MAX_ALLOWED_FETCH_SIZE session attendees in getEndedSessionAttendeesOfContext, its time to handle, contextId " + contextId);
        }
        if (ArrayUtils.isNotEmpty(sessionIds)) {
            return sessionAttendeeDAO.getAttendees(new ArrayList<>(sessionIds));
        } else {
            return null;
        }
    }

    public Map<String, CounselorDashboardSessionsInfoRes> getSessionInfosForContextIds(List<String> contextIds) {
        Map<String, CounselorDashboardSessionsInfoRes> response = new HashMap();
        if (ArrayUtils.isEmpty(contextIds)) {
            return response;
        }
        List<CounselorDashboardSessionsInfoRes> coursePlanSessionsInfoReses = sessionDAO.getCouserPlanSessionInfosByContextIds(contextIds);
        if (ArrayUtils.isNotEmpty(coursePlanSessionsInfoReses)) {
            for (CounselorDashboardSessionsInfoRes res : coursePlanSessionsInfoReses) {
                response.put(res.getId(), res);
            }
        }
        return response;
    }

    public void sendPostSessionEmailAsync() {
        logger.info("sendPostSessionEmailAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_OTO_POST_SESSION_EMAILS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendPostSessionEmails() {
        logger.info("sendPostSessionEmail");
        try {
            Long currentTime = System.currentTimeMillis();
            logger.info("sendPostSessionEmail starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterEndTime = currentTime - DateTimeUtils.MILLIS_PER_HOUR - sendPostSessionEmailCronRunTime;
            Long beforeEndTime = afterEndTime + sendPostSessionEmailCronRunTime - 1;
            logger.info("sendPostSessionEmail afterStartTime " + afterEndTime + " beforeStartTime " + beforeEndTime);
            GetSessionsReq req = new GetSessionsReq();
            req.setSessionStates(COMPLETE_SUCCESSFUL_SESSION_STATES);
            req.setAfterEndTime(afterEndTime);
            req.setBeforeEndTime(beforeEndTime);
            req.setSize(1000);
            List<Session> sessions = sessionDAO.getSessions(req);
            if (ArrayUtils.isNotEmpty(sessions)) {
                if (sessions.size() == 1000) {
                    logger.error("Number of session ended more than 1000 not processing between afterStartTime " + afterEndTime + " beforeStartTime " + beforeEndTime);
                    return;
                }
                for (Session session : sessions) {
                    try {
                        sendPostSessionEmail(session);
                    } catch (Exception ex) {
                        logger.error("error in sendPostSessionEmail: " + session + " error:" + ex.getMessage());
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("error in sendPostSessionEmail " + ex.getMessage());
        }
    }

    public void sendPostSessionEmail(Long sessionId) throws VException, AddressException, IOException {
        Session session = sessionDAO.getById(sessionId);
        sendPostSessionEmail(session);
    }

    public void sendPostSessionEmail(Session session) throws VException, AddressException, IOException {
        if (session == null) {
            return;
        }
        String getBillingDurationUrl = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT")
                + "feedback/fetchFeedback";
        GetFeedbackReq req = new GetFeedbackReq();
        req.setSessionId(String.valueOf(session.getId()));
        req.setSenderId(String.valueOf(session.getTeacherId()));
        ClientResponse resp = WebUtils.INSTANCE.doCall(getBillingDurationUrl + "?" + WebUtils.INSTANCE.createQueryStringOfObject(req), HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        GetFeedbackResponse getFeedbackResponse = gson.fromJson(jsonString, GetFeedbackResponse.class);
        List<FeedbackInfo> data = getFeedbackResponse.getData();
        FeedbackInfo feedback = null;
        if (ArrayUtils.isNotEmpty(data)) {
            feedback = data.get(0);
        }

        List<SessionAttendee> sessionAttendees = sessionAttendeeDAO.getAttendees(session.getId());
        if (sessionAttendees == null) {
            return;
        }

        // send email only if both the student and the teacher have joined the session -- not using a set here as the size of the list will be at most 2
        if (SessionModel.OTO.equals(session.getModel()) && !sessionAttendees.isEmpty()) {
            Boolean teacherJoined = false;
            Boolean studentJoined = false;
            Boolean isSessionBilled = false;

            for (SessionAttendee sessionAttendee : sessionAttendees) {
                if (Objects.equals(session.getTeacherId(), sessionAttendee.getUserId()) &&
                        com.vedantu.session.pojo.SessionAttendee.SessionUserState.JOINED.equals(sessionAttendee.getUserState())) {
                    teacherJoined = true;

                    if (sessionAttendee.getBillingPeriod() > 0) {
                        isSessionBilled = true;
                    }
                }

                List<Long> studentIds = session.getStudentIds();
                if (Objects.equals(studentIds.get(0), sessionAttendee.getUserId()) &&
                        com.vedantu.session.pojo.SessionAttendee.SessionUserState.JOINED.equals(sessionAttendee.getUserState())) {
                    studentJoined = true;
                }

                if (!teacherJoined || !studentJoined || !isSessionBilled) {
                    return;
                }
            }
        }

        communicationManager.sendOtoPostSessionEmail(feedback, session);
    }

    public GetUserDashboardSessionInfoRes getUserDashboardSessionInfo(Long userId) {
        GetUserDashboardSessionInfoRes response = new GetUserDashboardSessionInfoRes();
        GetSessionsReq req = new GetSessionsReq();
        req.setSortType(SessionSortType.START_TIME_ASC);
        req.setStudentId(userId);
        req.setSessionStates(COMPLETE_SUCCESSFUL_SESSION_STATES);
        Session session = sessionDAO.getOneSession(req);
        if (session != null) {
            response.setFirstOTOSessionTime(session.getStartTime());
        }

        req.setSortType(SessionSortType.START_TIME_DESC);
        session = sessionDAO.getOneSession(req);
        if (session != null) {
            response.setLastOTOSessionTime(session.getStartTime());
        }

        OTFSession otfSession = oTFSessionManager.getUserJoinedOtfSession(userId, Sort.Direction.ASC);
        if (otfSession != null) {
            response.setFirstOTFSessionTime(otfSession.getStartTime());
        }

        otfSession = oTFSessionManager.getUserJoinedOtfSession(userId, Sort.Direction.DESC);
        if (otfSession != null) {
            response.setLastOTFSessionTime(otfSession.getStartTime());
        }
        return response;
    }

    public void convertToWizIQ(Long sessionId) throws Exception {
        Session session = sessionDAO.getById(sessionId);
        if (session == null) {
            return;
        }
        convertToWizIQ(session);
    }


    private void convertToWizIQ(Session session) throws Exception {
        session.setLiveSessionPlatformType(LiveSessionPlatformType.WIZIQ);
        logger.info("session:" + session);
        wizIQClassManager.scheduleWizIQClass(session);
        Long teacherId = session.getTeacherId();
        Long studentId = session.getStudentIds().get(0);
        String[] keys = new String[]{getUserLatestSessionCacheKey(studentId), getUserLatestSessionCacheKey(teacherId)};
        redisDAO.deleteKeys(keys);
    }

    public void convertToWizIQ(String contextId) throws Exception {
        List<Session> sessions = sessionDAO.getSessionsByContextId(contextId);
        for (Session session : sessions) {
            if (session == null) {
                continue;
            }
            convertToWizIQ(session);
        }
    }


    public void handleLiveSessionToolType(SessionEvents event, Long sessionId) {
        Session session = sessionDAO.getById(sessionId);
        if (session == null) {
            return;
        }
        if (!LiveSessionPlatformType.WIZIQ.equals(session.getLiveSessionPlatformType())) {
            return;
        }
        try {
            switch (event) {
                case SESSION_CANCELED:
                    wizIQClassManager.delete(session);
                    break;
                case SESSION_RESCHEDULED:
                case SESSION_SCHEDULED:
                    wizIQClassManager.scheduleWizIQClass(session);
                    break;
            }
        } catch (Exception e) {
            logger.error("there is some error handling wiziq class event id:" + session.getId() + " event:" + event + "msg:" + e.getMessage() + " error:" + ExceptionUtils.getStackTrace(e));
        }
    }

    public Map<String, CoursePlanDashboardSessionInfo> getCoursePlanDashboardSessionInfos(List<String> coursePlanIds) {
        Map<String, CoursePlanDashboardSessionInfo> response = new HashMap<>();
        if (ArrayUtils.isEmpty(coursePlanIds)) {
            return response;
        }
        Long currentTime = System.currentTimeMillis();
        List<SessionReportAggregation> results = sessionDAO.getSessionReportAggregation(coursePlanIds);
        logger.info("ENTRY: getSessionReportAggregation results= " + results);
        if (ArrayUtils.isNotEmpty(results)) {
            for (SessionReportAggregation aggregation : results) {
                CoursePlanDashboardSessionInfo entity = new CoursePlanDashboardSessionInfo();
                entity.setCoursePlanId(aggregation.getId());
                entity.setTotalSessionCount(aggregation.getTotalSessionCount());
                entity.setFirstSessionTime(aggregation.getFirstSessionStartTime());
                long upcomingSessionTime = 0l;
                long lastSessionTime = 0l;
                long completedSessionCount = 0l;
                long upcomingSessionCount = 0l;
                for (Long startTime : aggregation.getStartTimes()) {
                    if (startTime >= currentTime && (upcomingSessionTime == 0l || startTime < upcomingSessionTime)) {
                        upcomingSessionTime = startTime;
                    }
                    if (startTime < currentTime && (lastSessionTime == 0l || startTime > lastSessionTime)) {
                        lastSessionTime = startTime;
                    }
                    if (startTime >= currentTime) {
                        upcomingSessionCount += 1;
                    }
                }
                for (Long endTime : aggregation.getEndTimes()) {
                    if (endTime <= currentTime) {
                        completedSessionCount += 1;
                    }
                }
                entity.setCompletedSessionCount(completedSessionCount);
                entity.setUpcomingSessionTime(upcomingSessionTime);
                entity.setLastSessionTime(lastSessionTime);
                entity.setUpcomingSessionCount(upcomingSessionCount);
                response.put(aggregation.getId(), entity);
            }
        }
        List<SessionState> expiredState = new ArrayList<>(
                Arrays.asList(SessionState.EXPIRED));
        List<SessionReportAggregation> expiredSessions = sessionDAO.getSessionReportAggregation(coursePlanIds, expiredState);
        logger.info("ENTRY: getSessionReportAggregation expiredSessions= " + expiredSessions);
        if (ArrayUtils.isNotEmpty(expiredSessions)) {
            for (SessionReportAggregation aggregation : expiredSessions) {
                long studentNotJoined = 0l;
                long teacherNotJoined = 0l;
                Set<Long> sessionIds = aggregation.getOtoSessionIds();
                if (ArrayUtils.isNotEmpty(sessionIds)) {
                    studentNotJoined = sessionAttendeeManager.getStudentNotJoinedSessionCount(sessionIds);
                    teacherNotJoined = sessionAttendeeManager.getTeacherNotJoinedSessionCount(sessionIds);
                }
                String key = aggregation.getId();
                CoursePlanDashboardSessionInfo entity = response.get(key);
                if (entity != null) {
                    entity.setExpiredSessionCount(aggregation.getTotalSessionCount());
                    entity.setStudentNotJoined(studentNotJoined);
                    entity.setTeacherNotJoined(teacherNotJoined);
                } else {
                    entity = new CoursePlanDashboardSessionInfo();
                    entity.setCoursePlanId(aggregation.getId());
                    entity.setExpiredSessionCount(aggregation.getTotalSessionCount());
                    entity.setStudentNotJoined(studentNotJoined);
                    entity.setTeacherNotJoined(teacherNotJoined);
                    response.put(aggregation.getId(), entity);
                }

            }
        };
        List<SessionState> canceledState = new ArrayList<>(
                Arrays.asList(SessionState.CANCELED));
        List<SessionReportAggregation> canceledSessions = sessionDAO.getSessionReportAggregation(coursePlanIds, canceledState);
        logger.info("ENTRY: getSessionReportAggregation canceledSessions= " + canceledSessions);
        if (ArrayUtils.isNotEmpty(canceledSessions)) {
            for (SessionReportAggregation aggregation : canceledSessions) {
                String key = aggregation.getId();
                CoursePlanDashboardSessionInfo entity = response.get(key);
                if (entity != null) {
                    entity.setCanceledSessionCount(aggregation.getTotalSessionCount());
                } else {
                    entity = new CoursePlanDashboardSessionInfo();
                    entity.setCoursePlanId(aggregation.getId());
                    entity.setCanceledSessionCount(aggregation.getTotalSessionCount());
                    response.put(aggregation.getId(), entity);
                }

            }
        };
        List<SessionState> forfittedState = new ArrayList<>(
                Arrays.asList(SessionState.FORFEITED));
        List<SessionReportAggregation> forfittedSessions = sessionDAO.getSessionReportAggregation(coursePlanIds, forfittedState);
        logger.info("ENTRY: getSessionReportAggregation forfittedSessions= " + forfittedSessions);
        if (ArrayUtils.isNotEmpty(forfittedSessions)) {
            for (SessionReportAggregation aggregation : forfittedSessions) {
                String key = aggregation.getId();
                CoursePlanDashboardSessionInfo entity = response.get(key);
                if (entity != null) {
                    entity.setForfittedSessionCount(aggregation.getTotalSessionCount());
                } else {
                    entity = new CoursePlanDashboardSessionInfo();
                    entity.setCoursePlanId(aggregation.getId());
                    entity.setForfittedSessionCount(aggregation.getTotalSessionCount());
                    response.put(aggregation.getId(), entity);
                }

            }
        }
        return response;
    }

    public List<UserSessionAttendance> getUserSessionAttendances(List<String> coursePlanIds, String userId) {

        List<Session> sessions = new ArrayList<>();
        int start = 0;
        int size = 100;
        while (true) {
            List<Session> tempSessions = sessionDAO.getSessionsForCoursePlansAndUserId(coursePlanIds, userId, start, size);
            if (ArrayUtils.isEmpty(tempSessions)) {
                break;
            }

            sessions.addAll(tempSessions);

            if (tempSessions.size() < size) {
                break;
            }
            start = start + size;

        }
        logger.info("Sessions: " + sessions);
        List<Long> sessionIds = new ArrayList<>();

        if (ArrayUtils.isEmpty(sessions)) {
            return new ArrayList<>();
        }

        Map<Long, Session> sessionMap = new HashMap<>();
        for (Session session : sessions) {
            sessionIds.add(session.getId());
            sessionMap.put(session.getId(), session);
        }

        start = 0;
        size = 100;
        List<SessionAttendee> sessionAttendees = new ArrayList<>();
        while (true) {
            List<SessionAttendee> tempSessionAttendees = sessionAttendeeDAO.getAttendees(sessionIds);
            if (ArrayUtils.isEmpty(tempSessionAttendees)) {
                break;
            }

            sessionAttendees.addAll(tempSessionAttendees);

            if (tempSessionAttendees.size() < size) {
                break;
            }
            start = start + size;

        }

        logger.info("SessionAttendee: " + sessionAttendees);

        if (ArrayUtils.isEmpty(sessionAttendees)) {
            return new ArrayList<>();
        }
        List<UserSessionAttendance> userSessionAttendances = new ArrayList<>();
        for (SessionAttendee sessionAttendee : sessionAttendees) {

            if (sessionMap.containsKey(sessionAttendee.getSessionId())) {
                UserSessionAttendance userSessionAttendance = new UserSessionAttendance();
                if (!Role.STUDENT.equals(sessionAttendee.getRole())) {
                    continue;
                }
                userSessionAttendance.setAttended(false);
                if (sessionAttendee.getJoinTime() > 0) {
                    userSessionAttendance.setAttended(true);
                }
                userSessionAttendance.setStartTime(sessionAttendee.getStartTime());
                userSessionAttendance.setContextIds(new HashSet<>(Arrays.asList(sessionMap.get(sessionAttendee.getSessionId()).getContextId())));
                userSessionAttendance.setSessionTitle(sessionMap.get(sessionAttendee.getSessionId()).getTitle());
                userSessionAttendances.add(userSessionAttendance);
            }

        }

        //List<Session> sessions = sessionDAO.getSessions(getSessionsReq);
        return userSessionAttendances;
    }

    /*
            if nobody comes before 15mins/expiry times, the session is marked expired by the normal cron,
                but we are not cancelling the wiziq session
            if atleast one comes before the expiry time the session goes directly to active
            when the second person joins, the session is already in active state(above rule), so nothing happens
    
            doing nothing for 'expired' and 'completed', state of wiziq status pings, as they are handled the normal cron
            
     */
    public PlatformBasicResponse wiziqstatusping(String class_id, String class_status, String recording_status,
            String attendance_report_status, HttpServletRequest request) {
        File file = null;
        try {
            DefaultMultipartHttpServletRequest req = (DefaultMultipartHttpServletRequest) request;
            Iterator<String> names = req.getFileNames();
            while (names.hasNext()) {
                logger.info("found a file in the request");
                file = CommonUtils.multipartToFile(req.getFile(names.next()));
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line.trim());
                }

                String xmlString = sb.toString();
                logger.info(xmlString);
                JSONObject response = XML.toJSONObject(xmlString);
                logger.info(response);
                JSONObject jsonObj = wizIQClassManager.extractEntryExitTimes(response);
                logger.info("adding join and exit entries for billing");

                Session session = sessionDAO.getWiziqSession(class_id);
                if (session == null) {
                    throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "Wiziq session not found for classId " + class_id);
                }
                jsonObj.put("sessionId", session.getId());
                jsonObj.put("teacherId", session.getTeacherId());
                jsonObj.put("studentId", session.getStudentIds().get(0));

                String url = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT")
                        + "session/endWiziqSession";
                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, jsonObj.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                logger.info(jsonString);
                break;//consuming only 1 file
            }

            if ("live".equals(class_status)) {
                Session session = sessionDAO.getWiziqSession(class_id);
                if (session == null) {
                    throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "Wiziq session not found for classId " + class_id);
                }
                if (!(SessionState.SCHEDULED.equals(session.getState())
                        || SessionState.STARTED.equals(session.getState()))) {
                    return new PlatformBasicResponse();
                } else {
                    //all other states we will mark it active 
                }

                logger.info("auto joining the teacher to wiziq session");
                JoinSessionReq teaJoinReq = new JoinSessionReq();
                teaJoinReq.setSessionId(session.getId());
                teaJoinReq.setUserId(session.getTeacherId());
                joinSession(teaJoinReq, true);

                logger.info("auto joining the student to wiziq session");
                JoinSessionReq stuJoinReq = new JoinSessionReq();
                stuJoinReq.setSessionId(session.getId());
                stuJoinReq.setUserId(session.getStudentIds().get(0));
                joinSession(stuJoinReq, true);

                logger.info("marking the session active");
                MarkSessionActiveReq markSessionActiveReq = new MarkSessionActiveReq();
                markSessionActiveReq.setSessionId(session.getId());
                markSessionActive(markSessionActiveReq);
            } else if ("expired".equals(class_status)) {
                logger.info("wiziq session classid " + class_id + " nobody joined for the entire duration of the session");
                //ignore
            } else if ("completed".equals(class_status)) {
                logger.info("wiziq session classid " + class_id + " completed");
                //ignore
            }
        } catch (Exception e) {
            logger.error("Error in handling wiziq status ping " + e.getMessage());
        } finally {
            if (file != null) {
                file.delete();
            }
        }
        return new PlatformBasicResponse();
    }

    // migrated from platform to scheduling as a part of exp/segRedis
    public void handleSessionEventsForLatestSession(SessionEvents eventType, String message) throws InternalServerErrorException {
        com.vedantu.scheduling.pojo.session.SessionInfo sessionInfo = null;
        sessionInfo = gson.fromJson(message, com.vedantu.scheduling.pojo.session.SessionInfo.class);
        if (sessionInfo != null) {
            Long currentTime = System.currentTimeMillis();
            Long expiryTime = new Long(DateTimeUtils.MILLIS_PER_MINUTE * (LATEST_SESSION_CACHE_EXPIRY_MINUTE + 15));
            expiryTime += currentTime;
            /*if(sessionInfo.getStartTime() != null && sessionInfo.getStartTime() > expiryTime
					&& !SessionEvents.SESSION_RESCHEDULED.equals(eventType)){
				return;
			}*/
            List<String> cacheKeys = new ArrayList<>();
            String teacher = null;
            String student = null;
            List<String> cachedKeyForSessions = new ArrayList<>();
            if (sessionInfo.getTeacherId() != null) {
                teacher = redisDAO.getUserLatestSessionCacheKey(sessionInfo.getTeacherId());
                if (StringUtils.isNotEmpty(teacher)) {
                    cacheKeys.add(teacher);
                }
                String teacherKey = redisDAO.getCachedKeyForUserLatestSessions(sessionInfo.getTeacherId().toString(), env);
                if (StringUtils.isNotEmpty(teacherKey)) {
                    cachedKeyForSessions.add(teacherKey);
                }
            }
            if (ArrayUtils.isNotEmpty(sessionInfo.getStudentIds()) && sessionInfo.getStudentIds().get(0) != null) {
                student = redisDAO.getUserLatestSessionCacheKey(sessionInfo.getStudentIds().get(0));
                if (StringUtils.isNotEmpty(student)) {
                    cacheKeys.add(student);
                }
                String studentKey = redisDAO.getCachedKeyForUserLatestSessions(sessionInfo.getStudentIds().get(0).toString(), env);
                if (StringUtils.isNotEmpty(studentKey)) {
                    cachedKeyForSessions.add(studentKey);
                }
            }
            switch (eventType) {
                case SESSION_SCHEDULED:
                case SESSION_CANCELED:
                case SESSION_FORFEITED:
                case SESSION_EXPIRED:
                case SESSION_ENDED:
                case SESSION_RESCHEDULED:
                    String[] toDelete = new String[cacheKeys.size()];
                    try {
                        redisDAO.deleteKeys(cacheKeys.toArray(toDelete));
                    } catch (Exception e) {
                        logger.info("Error in deleting Keys." + e);
                    }

                    String[] toDeleteSession = new String[cachedKeyForSessions.size()];
                    try {
                        redisDAO.deleteKeys(cachedKeyForSessions.toArray(toDeleteSession));
                    } catch (Exception ex) {
                        logger.info("Error in deleting keys: " + ex.getMessage());
                    }
                    return;
            }

            Map<String, String> redisMap = null;
            try {
                redisMap = redisDAO.getValuesForKeys(cacheKeys);
            } catch (Exception e) {
                logger.info("redis get throws exception." + e);
            }
            if (redisMap == null || redisMap.isEmpty()) {
                return;
            }
            GetLatestUpComingOrOnGoingSessionResponse resStudent = null;
            GetLatestUpComingOrOnGoingSessionResponse resTeacher = null;
            if (student != null && redisMap.containsKey(student)) {
                resStudent = gson.fromJson(redisMap.get(student), GetLatestUpComingOrOnGoingSessionResponse.class);
            }
            if (teacher != null && redisMap.containsKey(teacher)) {
                resTeacher = gson.fromJson(redisMap.get(teacher), GetLatestUpComingOrOnGoingSessionResponse.class);
            }
            Boolean saveStudent = Boolean.FALSE;
            Boolean saveTeacher = Boolean.FALSE;
            switch (eventType) {
                case SESSION_ACTIVE:
                    if (resStudent != null && resStudent.getSessionRes() != null) {
                        if (resStudent.getSessionRes().getId().equals(sessionInfo.getId())) {
                            resStudent.getSessionRes().setState(SessionState.ACTIVE);
                            saveStudent = Boolean.TRUE;
                        }
                    }
                    if (resTeacher != null && resTeacher.getSessionRes() != null) {
                        if (resTeacher.getSessionRes().getId().equals(sessionInfo.getId())) {
                            resTeacher.getSessionRes().setState(SessionState.ACTIVE);
                            saveTeacher = Boolean.TRUE;
                        }
                    }
                    break;
                case SESSION_STARTED:
                    if (resStudent != null && resStudent.getSessionRes() != null) {
                        if (resStudent.getSessionRes().getId().equals(sessionInfo.getId())
                                && sessionInfo.getStartedAt() != null) {
                            resStudent.getSessionRes().setStartedAt(sessionInfo.getStartedAt());
                            resStudent.getSessionRes().setState(SessionState.STARTED);
                            saveStudent = Boolean.TRUE;

                            if (sessionInfo.getStartedBy() != null) {
                                resStudent.getSessionRes().setStartedBy(sessionInfo.getStartedBy());
                                saveStudent = Boolean.TRUE;
                            }
                        }
                    }
                    if (resTeacher != null && resTeacher.getSessionRes() != null) {
                        if (resTeacher.getSessionRes().getId().equals(sessionInfo.getId())
                                && sessionInfo.getStartedAt() != null) {
                            resTeacher.getSessionRes().setStartedAt(sessionInfo.getStartedAt());
                            resTeacher.getSessionRes().setState(SessionState.STARTED);
                            saveTeacher = Boolean.TRUE;

                            if (sessionInfo.getStartedBy() != null) {
                                resTeacher.getSessionRes().setStartedBy(sessionInfo.getStartedBy());
                                saveTeacher = Boolean.TRUE;
                            }
                        }
                    }
                    break;
                default:
                    String[] toDelete = new String[cacheKeys.size()];
                    try {
                        redisDAO.deleteKeys(cacheKeys.toArray(toDelete));
                    } catch (Exception e) {
                        logger.info("Error in deleting Keys." + e);
                    }

                    String[] toDeleteSession = new String[cachedKeyForSessions.size()];
                    try {
                        // redisDAO.deleteKeys(cachedKeyForSessions.toArray(toDeleteSession));
                        deleteUserSessionCachedKeys(cachedKeyForSessions);
                    } catch (Exception ex) {
                        logger.info("Error in deleting keys: " + ex.getMessage());
                    }

                    return;
            }
            if (saveStudent) {
                String redisValue = gson.toJson(resStudent);
                int expiry = getExpiryTimeForLastestSessions(resStudent);
                if (expiry > DateTimeUtils.SECONDS_PER_MINUTE) {
                    try {
                        redisDAO.setex(student, redisValue, expiry);
                    } catch (Exception e) {
                        logger.info("redis student setex throws exception." + student + " " + e);
                    }
                }
            }
            if (saveTeacher) {
                int expiry = getExpiryTimeForLastestSessions(resTeacher);
                String redisValue = gson.toJson(resTeacher);
                if (expiry > DateTimeUtils.SECONDS_PER_MINUTE) {
                    try {
                        redisDAO.setex(teacher, redisValue, expiry);
                    } catch (Exception e) {
                        logger.info("redis teacher setex throws exception." + teacher + " " + e);
                    }
                }
            }
        }
    }

    private void deleteUserSessionCachedKeys(List<String> cacheKeys) {
        if (ArrayUtils.isNotEmpty(cacheKeys)) {
            try {
                List<List<String>> partition = Lists.partition(cacheKeys, 500);
                for (List<String> list : partition) {
                    redisDAO.deleteKeys(list.toArray(new String[]{}));
                }
            } catch (Exception e) {
                logger.info("Error in deleting Keys." + e);
            }
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing snsclient connection ", e);
        }
    }
}
