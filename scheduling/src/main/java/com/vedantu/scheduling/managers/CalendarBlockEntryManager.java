package com.vedantu.scheduling.managers;

import com.vedantu.exception.BadRequestException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.dao.entity.CalendarBlockEntry;
import com.vedantu.scheduling.dao.serializers.CalendarBlockEntryDAO;
import com.vedantu.scheduling.pojo.BitSetEntry;
import com.vedantu.scheduling.pojo.CalendarBlockEntryState;
import com.vedantu.scheduling.pojo.CalendarBlockType;
import com.vedantu.scheduling.pojo.calendar.CalendarBlockReferenceType;
import com.vedantu.scheduling.pojo.calendar.CalendarBlockSlot;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.util.Constants;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.springframework.data.mongodb.core.query.Update;

@Service
public class CalendarBlockEntryManager {

	@Autowired
	private CalendarBlockEntryDAO calendarBlockEntryDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarBlockEntryManager.class);

	public void updateCalendarBlockEntry(CalendarBlockEntry calendarBlockEntry) {
		logger.info("updateCalendarBlockEntry - " + calendarBlockEntry.toString());
		Query query = new Query();
		query.addCriteria(
				Criteria.where(CalendarBlockEntry.Constants.REFERENCE_ID).is(calendarBlockEntry.getReferenceId()));
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STATE).is(CalendarBlockEntryState.ACTIVE));
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.BLOCK_TYPE).is(CalendarBlockType.STANDARD));
		Update update = new Update();
		update.set(CalendarBlockEntry.Constants.BLOCK_ENTRY, calendarBlockEntry.getBlockEntry());
		update.set(CalendarBlockEntry.Constants.TEACHER_ID, calendarBlockEntry.getTeacherId());
		update.set(CalendarBlockEntry.Constants.STUDENT_ID, calendarBlockEntry.getStudentId());
		update.set(CalendarBlockEntry.Constants.START_TIME, calendarBlockEntry.getStartTime());
		update.set(CalendarBlockEntry.Constants.REPEAT_COUNT, calendarBlockEntry.getRepeatCount());
                update.set(CalendarBlockEntry.Constants.LAST_UPDATED, System.currentTimeMillis());
		calendarBlockEntryDAO.upsertEntity(query, update,CalendarBlockEntry.class);
	}

	public void updateCalendarBlockEntry(String referenceId, Long startTime, BitSet bitSet) {
		logger.info("updateCalendarBlockEntry - referenceId : " + referenceId + "bitSet : "
				+ Arrays.toString(bitSet.toLongArray()));

		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.REFERENCE_ID).is(referenceId));
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STATE).is(CalendarBlockEntryState.ACTIVE));
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.BLOCK_TYPE).is(CalendarBlockType.STANDARD));

		List<CalendarBlockEntry> results = calendarBlockEntryDAO.runQuery(query, CalendarBlockEntry.class);
		if (results != null && !results.isEmpty()) {
			if (results.size() > 1) {
				logger.error("updateCalendarBlockEntryDuplicate - Duplicate entries found for reference id - "
						+ referenceId);
			}
			for (CalendarBlockEntry calendarBlockEntry : results) {
				calendarBlockEntry.setBlockEntry(bitSet);
				calendarBlockEntry.setStartTime(startTime);
				calendarBlockEntryDAO.save(calendarBlockEntry);
			}
		} else {
			logger.info("updateCalendarBlockEntry - No entries found");
		}
	}

	public CalendarBlockEntry getCalendarBlockEntry(String id) {
		return calendarBlockEntryDAO.getById(id);
	}

	public BitSet getCalendarBlockBitSet(String studentId, String teacherId, Long startTime, Long endTime,
			String neglectReferenceId) {
		return getCalendarBlockBitSetEntry(studentId, teacherId, startTime, endTime, neglectReferenceId).getBitSet();
	}

	public BitSetEntry getCalendarBlockBitSetEntry(String studentId, String teacherId, Long startTime, Long endTime,
			String neglectReferenceId) {
		logger.info("getCalendarBlockBitSetEntry - request : studentId " + studentId + " teacherId " + teacherId
				+ " startTime " + startTime + " endTime " + endTime + " referenceId : " + neglectReferenceId);
		Query query = new Query();
		if (!StringUtils.isEmpty(studentId) && !StringUtils.isEmpty(teacherId)) {
			query.addCriteria(
					new Criteria().orOperator(Criteria.where(CalendarBlockEntry.Constants.STUDENT_ID).is(studentId),
							Criteria.where(CalendarBlockEntry.Constants.TEACHER_ID).is(teacherId)));
		} else if (!StringUtils.isEmpty(studentId)) {
			query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STUDENT_ID).is(studentId));
		} else if (!StringUtils.isEmpty(teacherId)) {
			query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.TEACHER_ID).is(teacherId));
		} else {
			logger.error("Invalid get block request with no teacher or student ids - " + startTime + " endTime - "
					+ endTime);
		}

		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STATE).is(CalendarBlockEntryState.ACTIVE));
		List<CalendarBlockEntry> calendarBlockEntries = calendarBlockEntryDAO.runQuery(query, CalendarBlockEntry.class);
		logger.info("getCalendarBlockBitSetEntry - query response : " + calendarBlockEntries);
		BitSetEntry bitSetEntry = new BitSetEntry(startTime, endTime, CalendarEntrySlotState.SESSION);
		if (calendarBlockEntries != null && !calendarBlockEntries.isEmpty()) {
			for (CalendarBlockEntry calendarBlockEntry : calendarBlockEntries) {
				// Neglect the bit sets with the provided referenceId;
				if (!StringUtils.isEmpty(neglectReferenceId)
						&& neglectReferenceId.equals(calendarBlockEntry.getReferenceId())) {
					logger.info("getCalendarBlockBitSetEntry - entry found for neglectReferenceId : "
							+ calendarBlockEntry.toString());
					continue;
				}

				BitSetEntry blockBitSetEntry = new BitSetEntry(calendarBlockEntry.getStartTime(),
						calendarBlockEntry.getEndTime(), CalendarEntrySlotState.SESSION, Constants.SLOT_LENGTH,
						calendarBlockEntry.getBlockEntry());
				bitSetEntry.orBitSet(blockBitSetEntry);
			}
		}

		logger.info("getCalendarBlockBitSetEntry - result bitSetEntry : " + bitSetEntry.toString() + " bitSet : "
				+ (bitSetEntry.getBitSet() == null ? "" : Arrays.toString(bitSetEntry.getBitSet().toLongArray())));
		return bitSetEntry;
	}

	public List<CalendarBlockEntry> getBlockedEntries(String studentId, String teacherId,
			CalendarBlockEntryState state) {
		logger.info("getBlockedEntries - studentId : " + studentId + " teacherId : " + teacherId + " state : " + state);
		Query query = new Query();
		if (!StringUtils.isEmpty(studentId)) {
			query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STUDENT_ID).is(studentId));
		}

		if (!StringUtils.isEmpty(teacherId)) {
			query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.TEACHER_ID).is(teacherId));
		}

		if (state != null) {
			query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STATE).is(state));
		}

		List<CalendarBlockEntry> results = calendarBlockEntryDAO.runQuery(query, CalendarBlockEntry.class);
		logger.info("results - " + Arrays.toString(results.toArray()));
		return results;
	}

	public List<CalendarBlockSlot> getBlockedEntrySlots(String studentId, String teacherId, Long startTime,
			Long endTime) {
		logger.info("getBlockedEntrySlots - studentId : " + studentId + " teacherId :" + " startTime : " + startTime
				+ " endTime : " + endTime);
		startTime = CommonCalendarUtils.getSlotStartTime(startTime);
		endTime = CommonCalendarUtils.getSlotEndTime(endTime);
		logger.info("Rounded off startTime : " + startTime + " endTime : " + endTime);
		List<CalendarBlockEntry> entries = getBlockedEntries(studentId, teacherId, CalendarBlockEntryState.ACTIVE);
		logger.info("block entries fetched {}", entries);
		BitSet slotBitSet = new BitSet(); // Slot bit set to track conflicts
		List<CalendarBlockSlot> slots = new ArrayList<>();
		if (!CollectionUtils.isEmpty(entries)) {
			for (CalendarBlockEntry entry : entries) {
				if (entry.getBlockEntry() == null) {
					logger.error("CalendarBlockEntryNull for  " + entry.toString() + " studentId : " + studentId
							+ " teacherId : " + teacherId + " startTime : " + startTime + " endTime : " + endTime);
					continue;
				}

				BitSetEntry blockBitSetEntry = new BitSetEntry(startTime, endTime, CalendarEntrySlotState.SESSION);
				blockBitSetEntry.orBitSet(new BitSetEntry(entry.getStartTime(), entry.getEndTime(), CalendarEntrySlotState.SESSION,
						Constants.SLOT_LENGTH, entry.getBlockEntry()));
				BitSet blockBitSet = blockBitSetEntry.getBitSet();
				if (blockBitSet.cardinality() > 0) {
					for (int i = 0; i < blockBitSet.length(); i++) {
						int startIndex = blockBitSet.nextSetBit(i);
						if (startIndex < 0) {
							break;
						}

						int endIndex = blockBitSet.nextClearBit(startIndex);
						long slotStartTime = startTime + (startIndex * Constants.SLOT_LENGTH);
						long slotEndTime = startTime + (endIndex * Constants.SLOT_LENGTH);
						logger.info("Slot - slotStartTime : " + slotStartTime + " slotEndTime : " + slotEndTime
								+ " referenceId : " + entry.getReferenceId() + " startIndex : " + startIndex
								+ " endIndex : " + endIndex);

						// startIndex is inclusive and endIndex is exclusive
						slots.add(new CalendarBlockSlot(entry.getReferenceId(),
								CalendarBlockReferenceType.SUBSCRIPTION_REQUEST, entry.getStudentId(),
								entry.getTeacherId(), slotStartTime, slotEndTime));

						if ((slotBitSet.nextSetBit(startIndex) > 0) && (slotBitSet.nextSetBit(startIndex) < endIndex)) {
							logger.warn("Conflict found for session - startTime:" + slotStartTime + " endTime:"
									+ slotEndTime + " studentId:" + studentId + " teacherId:" + teacherId
									+ " blockEntry:" + entry.toString());
						}

						// Update slotBitSet
						slotBitSet.set(startIndex, endIndex);
						i = endIndex;
					}
				}
			}
		}

		return slots;
	}

	public List<CalendarBlockEntry> markProcessed(String referenceId) {
		logger.info("markProcessed - " + referenceId);
		return updateCalendarBlockEntryState(referenceId, CalendarBlockEntryState.PROCESSED);
	}

	public List<CalendarBlockEntry> markCanceled(String referenceId) {
		logger.info("markCanceled - " + referenceId);
		return updateCalendarBlockEntryState(referenceId, CalendarBlockEntryState.CANCELED);
	}

	public List<CalendarBlockEntry> cancelBulkEntries(List<String> referenceIds) throws VException {
		logger.info("markCanceled - " + referenceIds);
		List<CalendarBlockEntry> updatedEntries = new ArrayList<CalendarBlockEntry>();
		if (referenceIds == null || referenceIds.isEmpty()) {
			logger.error("cancelBulkEntries - Invalid request");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					"cancelBulkEntries - no referenceIds in request");
		}

		for (String referenceId : referenceIds) {
			try {
				List<CalendarBlockEntry> entries = updateCalendarBlockEntryState(referenceId,
						CalendarBlockEntryState.CANCELED);
				updatedEntries.addAll(entries);
			} catch (Exception ex) {
				logger.error("cancelBulkEntries - error canceling request - " + referenceId);
			}
		}

		return updatedEntries;
	}

	public List<CalendarBlockEntry> updateCalendarBlockEntryState(String referenceId, CalendarBlockEntryState state) {
		logger.info("updateCalendarBlockEntryState - referenceId : " + referenceId + " state : " + state);
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.REFERENCE_ID).is(referenceId));
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STATE).is(CalendarBlockEntryState.ACTIVE));

		List<CalendarBlockEntry> calendarBlockEntries = calendarBlockEntryDAO.runQuery(query, CalendarBlockEntry.class);
		if (calendarBlockEntries != null) {
			for (CalendarBlockEntry calendarBlockEntry : calendarBlockEntries) {
				logger.info("Calendar entry pre update - " + calendarBlockEntry.toString());
				calendarBlockEntry.setState(state);
				calendarBlockEntryDAO.save(calendarBlockEntry);
			}
		} else {
			logger.error("updateCalendarBlockEntryStateNotFound - No calendar block entry found for referenceId - "
					+ referenceId);
		}

		return calendarBlockEntries;
	}

	public List<CalendarBlockEntry> resetBlockEntry(String referenceId, Long endTime) throws NotFoundException {
		logger.info("resetBlockEntry - Request : " + referenceId + " endTime : " + endTime);
		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.REFERENCE_ID).is(referenceId));
		query.addCriteria(Criteria.where(CalendarBlockEntry.Constants.STATE).is(CalendarBlockEntryState.ACTIVE));

		List<CalendarBlockEntry> calendarBlockEntries = calendarBlockEntryDAO.runQuery(query, CalendarBlockEntry.class);
		if (calendarBlockEntries != null && calendarBlockEntries.size() > 0) {
			if (calendarBlockEntries.size() > 1) {
				logger.error("Multiple calendar block entries found for referenceId : " + referenceId);
			}
			for (CalendarBlockEntry calendarBlockEntry : calendarBlockEntries) {
				logger.info("Calendar block entry pre update - " + calendarBlockEntry.toString());
				resetByEndTime(calendarBlockEntry, endTime);
				calendarBlockEntryDAO.save(calendarBlockEntry);
			}
		} else {
			throw new NotFoundException(ErrorCode.CALENDAR_ENTRY_NOT_FOUND,
					"calendar entry not found for " + referenceId);
		}

		return calendarBlockEntries;
	}

	private void resetByEndTime(CalendarBlockEntry calendarBlockEntry, Long endTime) {
		BitSetEntry bitSetEntry = new BitSetEntry(calendarBlockEntry.getStartTime(), calendarBlockEntry.getEndTime(),
				CalendarEntrySlotState.SESSION, Constants.SLOT_LENGTH, calendarBlockEntry.getBlockEntry());
		logger.info("resetByEndTime - bitSetEntry : " + bitSetEntry.toString());
		BitSetEntry emptyBitSet = new BitSetEntry(calendarBlockEntry.getStartTime(), endTime, CalendarEntrySlotState.SESSION,
				Constants.SLOT_LENGTH, new BitSet());
		logger.info("resetByEndTime - empty bit set : " + emptyBitSet.toString());
		bitSetEntry.updateBitSet(emptyBitSet);
		logger.info("resetByEndTime - post update : " + bitSetEntry.toString());
		calendarBlockEntry.setBlockEntry(bitSetEntry.getBitSet());
	}
}
