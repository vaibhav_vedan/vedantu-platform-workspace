package com.vedantu.scheduling.managers.earlylearning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.vedantu.onetofew.enums.SessionLabel;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.scheduling.Interfaces.ITeacherProficiencyManager;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.serializers.CalendarEntryDAO;
import com.vedantu.scheduling.pojo.SlotBookingCount;
import com.vedantu.scheduling.pojo.earlylearning.SlotAvaillability;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public abstract class AbstractSlotAvailabilityManager {

	protected Logger logger = LogFactory.getLogger(AbstractSlotAvailabilityManager.class);

	@Autowired
	protected ITeacherProficiencyManager teacherProficiencyManager;

	@Autowired
	protected CalendarEntryDAO calendarEntryDAO;

	public abstract String getBookingAvailabilityBitString(Long startTime, Long endTime,
			ELTeachersProficiencies proficiencyIds, Integer grade, SessionLabel earlyLearningCourseType);

	public abstract ProficiencyType getAvailableProficiencyCategoryForDemo(Long startTime, Long endTime,
			ELTeachersProficiencies proficiencyIds, Integer grade, SessionLabel earlyLearningCourseType);

	protected Map<Long, Map<ProficiencyType, Long>> getBookingProficiencyCount(List<SlotBookingCount> slotBookingCount) {

		Map<Long, Map<ProficiencyType, Long>> slotBookingMapping = slotBookingCount.stream().filter(Objects::nonNull)
				.collect(Collectors.groupingBy(SlotBookingCount::getSlotStartTime,
						Collectors.toMap(SlotBookingCount::getProficiencyType, SlotBookingCount::getCount)));
		return slotBookingMapping;
	}

	protected SlotAvaillability prepareTeachersAvailabilityMap(ELTeachersProficiencies proficiencyIds,
			TeacherCategory gradeType, Long dayStartTime, Long dayEndTime) {
		SlotAvaillability slotAvailability = new SlotAvaillability();

		gradeType.getProficiencies().forEach((proficiencyType) -> {
			slotAvailability.addTeacherAvailabilityByProficiency(proficiencyType,
					getAvailableTeachers(proficiencyIds, proficiencyType, dayStartTime, dayEndTime));
		});

		return slotAvailability;
	}

	//TODO: Check this logic once..
	private Map<Integer, Long> getAvailableTeachers(ELTeachersProficiencies proficiencyIds,
			ProficiencyType proficiencyType, Long dayStartTime, Long dayEndTime) {

		List<List<String>> allTeachersPartitions = Lists
				.partition(proficiencyIds.getTeachersForProficiency(proficiencyType), 100);
		List<CalendarEntry> calendarEntries = new ArrayList<>();
		for (List<String> teachersPartition : allTeachersPartitions) {
			calendarEntries.addAll(
					calendarEntryDAO.getTeachersAvailabilityInSlot(dayStartTime, dayEndTime, teachersPartition));
		}
		return calculateTeacherAvailabilityCountForSlots(calendarEntries);
	}
	
   public Map<Integer, Long> calculateTeacherAvailabilityCountForSlots(List<CalendarEntry> calendarEntries) {

        int dayStartIndex = 0;
        int dayEndIndex = CommonCalendarUtils.getCalendarEntrySlotCount() - 1;
        
        Map<Integer, List<String>> availableTeachersInSlotIndex = new HashMap<>();
        Map<Integer, Long> teacherAvailableInSlot = new HashMap<>();
        
        logger.info("calendarEntries size : " + calendarEntries.size());
        
        for (int index = 0; index < calendarEntries.size(); index++) {
            CalendarEntry calendarEntry = calendarEntries.get(index);
            Set<Integer> availablityIndex = getTeacherAvailabilityIndexes(calendarEntry);
            for (int availableIndex = dayStartIndex; availableIndex <= dayEndIndex; availableIndex++) {
                if (availablityIndex.contains(availableIndex)) {
                    List<String> teacherIds = availableTeachersInSlotIndex.get(availableIndex);
                    if (CollectionUtils.isEmpty(teacherIds)) {
                        teacherIds = new ArrayList<>();
                        teacherIds.add(calendarEntry.getUserId());
                    } else {
                        teacherIds.add(calendarEntry.getUserId());
                    }
                    availableTeachersInSlotIndex.put(availableIndex, teacherIds);
                }
            }
        }
        
        //This logic for checking if the teacher is available for whole 1 hour
        //this logic will break if the session time will not start from integer number like 9,10
        //In future if the demo time is flexible, take the front-end demo booking slot start time like 8:30 8:45 and calculate the index below
        for (int index = dayStartIndex + 2; index <= dayEndIndex; index = index + 4) {

            Set<String> availableTeachersIntersection = new HashSet<>();
            availableTeachersIntersection.addAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index)).orElseGet(ArrayList::new));
            availableTeachersIntersection.retainAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index + 1)).orElseGet(ArrayList::new));
            availableTeachersIntersection.retainAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index + 2)).orElseGet(ArrayList::new));
            availableTeachersIntersection.retainAll(Optional.ofNullable(availableTeachersInSlotIndex.get(index + 3)).orElseGet(ArrayList::new));
            teacherAvailableInSlot.put(index, Long.valueOf(availableTeachersIntersection.size()));
            teacherAvailableInSlot.put(index + 1, Long.valueOf(availableTeachersIntersection.size()));
            teacherAvailableInSlot.put(index + 2, Long.valueOf(availableTeachersIntersection.size()));
            teacherAvailableInSlot.put(index + 3, Long.valueOf(availableTeachersIntersection.size()));
        }
        return teacherAvailableInSlot;
    }
    
    
    private Set<Integer> getTeacherAvailabilityIndexes(CalendarEntry calendarEntry) {
        if (!CollectionUtils.isEmpty(calendarEntry.getAvailability()) && !CollectionUtils.isEmpty(calendarEntry.getBooked())) {
            calendarEntry.getAvailability().removeAll(calendarEntry.getBooked());
        }
        if (!CollectionUtils.isEmpty(calendarEntry.getAvailability()) && !CollectionUtils.isEmpty(calendarEntry.getSessionRequest())) {
            calendarEntry.getAvailability().removeAll(calendarEntry.getSessionRequest());
        }
        return calendarEntry.getAvailability();
    }


	public static class Constants {
		public static final String AVAILABLE_BIT = "1";
		public static final String NOT_AVAILABLE_BIT = "0";
		public static final String NOT_AVAILABLE_BIT_HOUR = "0000";
		public static final int PARTITION_SIZE = 100;
	}

}
