package com.vedantu.scheduling.managers;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.scheduling.controllers.OTFSessionController;
import com.vedantu.scheduling.dao.entity.*;
import com.vedantu.scheduling.dao.serializers.LOAMAmbassadorDAO;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class LOAMAmbassadorManager
{
	private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
	private final String LOAM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LOAM_ENDPOINT");
    private final Long FROM_TIME = ConfigUtils.INSTANCE.getLongValue("FROM_TIME");

	@Autowired
	private LOAMAmbassadorDAO lOAMAmbassadorDAO;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private OTFSessionController otfSessionController;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private OTFSessionDAO otfSessionDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LOAMAmbassadorManager.class);

	public Set<OTFSession> loam_getSessionIdsForBoardId(Long studentId, Long fromTime, Long thruTime, Long boardId,Integer sessionChunk) {

		Set<String> gttFilteredFields=new HashSet<>();
		gttFilteredFields.add(GTTAttendeeDetails.Constants.SESSION_ID);

		List<GTTAttendeeDetails> rawGtt = lOAMAmbassadorDAO.loam_getAttendedGTTAttendeeDetailsInSessionRange(studentId, fromTime, thruTime,gttFilteredFields,true);

		Set<String> sids=new HashSet<>();

		for(GTTAttendeeDetails temp:rawGtt){
			sids.add(temp.getSessionId());
		}

		Set<String> sessionFilteredFields =new HashSet<>();
		sessionFilteredFields.add(OTFSession.Constants._ID);
		sessionFilteredFields.add(OTFSession.Constants.BOARD_ID);
		sessionFilteredFields.add(OTFSession.Constants.TITLE);
		sessionFilteredFields.add(OTFSession.Constants.START_TIME);


		List<OTFSession> sessionList;
		if(boardId==-1){
			sessionList= lOAMAmbassadorDAO.loam_getAllSessionsForDoubts(fromTime,thruTime,sessionFilteredFields,true);
		}else{
			sessionList = lOAMAmbassadorDAO.loam_getByBoardIdForDoubt(boardId,fromTime,thruTime,sessionFilteredFields,true);
		}
		Set<OTFSession> sessionSet;
//		for(OTFSession session : sessionList){
//			if(sids.contains(session.getId())){
//				sessionSet.add(session);
//			}
//		}
		sessionSet=sessionList.parallelStream().filter(session -> sids.contains(session.getId())).collect(Collectors.toSet());
		return sessionSet;

	}

	public List<OTFSession> loam_getOTFSessionData(Long fromTime, Long thruTime) {

		List<OTFSession> sessions = new ArrayList<>();
		//Projection on fields to fetch
		Set<String> requiredFields = new HashSet<>();
		loam_addAbstractMongoEntityFields(requiredFields);
		loam_addAbstractOTFSessionEntityFields(requiredFields);
		sessions = lOAMAmbassadorDAO.loam_getOTFSessions(fromTime, thruTime, requiredFields, true);
		return sessions;
	}

	public List<GTTAttendeeDetails> loam_getGTTAttendeeDetails(Long fromTime, Long thruTime, Integer start, Integer size) {
		List<GTTAttendeeDetails> gttAttendeeDetails = new ArrayList<>();
		//Projection on fields to fetch
		Set<String> requiredFields = new HashSet<>();
		loam_addAbstractMongoEntityFields(requiredFields);
		loam_addAbstractGTTAttendeeDetailsEntityFields(requiredFields);
		gttAttendeeDetails = lOAMAmbassadorDAO.loam_getGTTAttendeeDetails(fromTime, thruTime, start, size, requiredFields, true);
		return gttAttendeeDetails;
	}

	public List<OTMSessionEngagementData> loam_getOTMSessionEngagementData(Long fromTime, Long thruTime) {
		List<OTMSessionEngagementData> otmSessionEngagementData = new ArrayList<>();
		//Projection on fields to fetch
		Set<String> requiredFields = new HashSet<>();
		loam_addAbstractMongoEntityFields(requiredFields);
		loam_addAbstractOTMSessionEngagementDataEntityFields(requiredFields);
		otmSessionEngagementData = lOAMAmbassadorDAO.loam_getOTMSessionEngagementData(fromTime, thruTime, requiredFields, true);
		return otmSessionEngagementData;
	}

	private void loam_addAbstractMongoEntityFields(Set<String> requiredFields) {
		requiredFields.add(AbstractMongoEntity.Constants._ID);
		requiredFields.add(AbstractMongoEntity.Constants.CREATION_TIME);
		requiredFields.add(AbstractMongoEntity.Constants.CREATED_BY);
		requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED);
		requiredFields.add(AbstractMongoEntity.Constants.ID);
		requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
		requiredFields.add(AbstractMongoEntity.Constants.ENTITY_STATE);
	}
	private void loam_addAbstractOTFSessionEntityFields(Set<String> requiredFields) {
		requiredFields.add(OTFSession.Constants.BOARD_ID);
		requiredFields.add(OTFSession.Constants.START_TIME);
		requiredFields.add(OTFSession.Constants.END_TIME);
		requiredFields.add(OTFSession.Constants.UNIQUE_STUDENT_ATTENDANCE);
		requiredFields.add(OTFSession.Constants.TITLE);
	}

	private void loam_addAbstractGTTAttendeeDetailsEntityFields(Set<String> requiredFields) {
		requiredFields.add(GTTAttendeeDetails.Constants.USER_ID);
		requiredFields.add(GTTAttendeeDetails.Constants.ATTENDEE_TYPE);
		requiredFields.add(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME);
		requiredFields.add(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME);
		requiredFields.add(GTTAttendeeDetails.Constants.SESSION_ID);
		requiredFields.add(GTTAttendeeDetails.Constants.ENROLLMENT_ID);
		requiredFields.add(GTTAttendeeDetails.Constants.HOTSPOT_NUMBERS);
		requiredFields.add(GTTAttendeeDetails.Constants.QUIZ_NUMBERS);
		requiredFields.add(GTTAttendeeDetails.Constants.JOIN_TIMES);
	}

	private void loam_addAbstractOTMSessionEngagementDataEntityFields(Set<String> requiredFields) {
		requiredFields.add(OTMSessionEngagementData.Constants.SESSIONID);
		requiredFields.add(OTMSessionEngagementData.Constants.CONTEXT);
		requiredFields.add(OTMSessionEngagementData.Constants.TOTAL_INCORRECT_RESPONSE);
		requiredFields.add(OTMSessionEngagementData.Constants.TOTAL_CORRECT_RESPONSE);
		requiredFields.add(OTMSessionEngagementData.Constants.TOTAL_UNATTEMPTS);
		requiredFields.add(OTMSessionEngagementData.Constants.STUDENT_COUNT_AT_START);
	}

}
