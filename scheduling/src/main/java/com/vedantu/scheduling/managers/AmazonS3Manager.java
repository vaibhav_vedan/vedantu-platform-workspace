package com.vedantu.scheduling.managers;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import java.net.URL;
import javax.annotation.PreDestroy;

@Service
public class AmazonS3Manager {

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(AmazonS3Manager.class);

    private AmazonS3Client s3Client;
    private AmazonS3Client s3ClientMumbai;

    private static final String BUCKET_NAME = ConfigUtils.INSTANCE.getStringValue("vedantu.otf.recordings.bucket.name");
    private static final long EXPIRY_WINDOW = 6 * DateTimeUtils.MILLIS_PER_DAY;

    public AmazonS3Manager() {

        s3Client = new AmazonS3Client();
        s3ClientMumbai = new AmazonS3Client();
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        s3ClientMumbai.setRegion(Region.getRegion(Regions.AP_SOUTH_1));
    }

    public boolean keyExists(String sessionId) {
        return s3Client.doesObjectExist(getBucketName(), sessionId + ".mp4");
    }

    public String getPreSignedUrl(String sessionId) {
        String presignedUrl = "";

        try {
            GeneratePresignedUrlRequest grt = new GeneratePresignedUrlRequest(getBucketName(), sessionId + ".mp4");
            grt.setExpiration(new Date(System.currentTimeMillis() + EXPIRY_WINDOW));
            // grt.setContentType("audio/mpeg");
            presignedUrl = s3Client.generatePresignedUrl(grt).toString();
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }
    
    public String getPreSignedSessionNotesUrl(String sessionId) {
        String presignedUrl = "";

        try {
            GeneratePresignedUrlRequest grt = new GeneratePresignedUrlRequest(getNotesBucketName(), sessionId + ".pdf");
            grt.setExpiration(new Date(System.currentTimeMillis() + EXPIRY_WINDOW));
            // grt.setContentType("audio/mpeg");
            presignedUrl = s3ClientMumbai.generatePresignedUrl(grt).toString();
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }
    
    public String getPreSignedSessionNotesUploadUrl(String sessionId) {
        String presignedUrl = "";

        try {
            GeneratePresignedUrlRequest grt = new GeneratePresignedUrlRequest(getNotesBucketName(), sessionId + ".pdf");
            grt.setExpiration(new Date(System.currentTimeMillis() + EXPIRY_WINDOW));
            grt.setMethod(HttpMethod.PUT);
            grt.setContentType("application/pdf");
            presignedUrl = s3Client.generatePresignedUrl(grt).toString();
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }

    public void uploadFile(String sessionId, File file) throws FileNotFoundException {
        logger.info("Request : " + sessionId);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("audio/mpeg");
        PutObjectRequest putObjectRequest = new PutObjectRequest(getBucketName(), sessionId + ".mp4",
                new FileInputStream(file), objectMetadata);
        PutObjectResult result = s3Client.putObject(putObjectRequest);
        logger.info("upload respodnse from s3 : " + result.toString());
    }
    public boolean uploadFile(String bucket, String key, File contentFile) {
        Boolean isUploadContentFileSuccessfull = false;
        try {
            s3Client.putObject(bucket, key, contentFile);
            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            logger.info("exception : "+ex.getMessage());
            isUploadContentFileSuccessfull = false;
        }
        return isUploadContentFileSuccessfull;
    }

    private String getBucketName() {
        return BUCKET_NAME + "/" + ConfigUtils.INSTANCE.getStringValue("environment");
    }
    
    private String getNotesBucketName() {
        return BUCKET_NAME + "/NOTES/" + ConfigUtils.INSTANCE.getStringValue("environment");
    }

    public void downloadFileFromKey(String key, File localFile) {
        GetObjectRequest gor = new GetObjectRequest(getBucketName(), key + ".mp4");
        ObjectMetadata object = s3Client.getObject(gor, localFile);
        logger.info("object : " + object.toString());
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
