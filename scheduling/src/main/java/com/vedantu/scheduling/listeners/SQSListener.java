package com.vedantu.scheduling.listeners;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.onetofew.pojo.OTFUpdateSessionRes;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.request.GetOTFWebinarSessionsReq;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.managers.*;
import com.vedantu.scheduling.pojo.MarkCalendarBatchPojo;
import com.vedantu.scheduling.pojo.SessionEnrollmentsPojo;
import com.vedantu.scheduling.pojo.StudentMarkCalendarPojo;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.wavesession.UpdateSessionAttendeeDataReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author ajith
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private OTFSessionManager otfSessionManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private CalendarEntryManager calendarEntryManager;

    @Autowired
    private AsyncTaskManager asyncTaskManager;

    @Autowired
    GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @Autowired
    OtfSessionManagerV2 otfSessionManagerV2;

    @Autowired
    StatsdClient statsdClient;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    @Autowired
    private VedantuWaveSessionManager vedantuWaveSessionManager;

    private final Gson gson = new Gson();

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            SQSMessageType sqsMessageType = null;
            if (StringUtils.isNotEmpty(textMessage.getStringProperty("messageType"))) {
                sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            }
            logger.info("Received message " + textMessage.getText());
            logger.info("Received type " + sqsMessageType);
            Long startTime = System.currentTimeMillis();

            if (sqsMessageType != null) {
                handleMessage(sqsMessageType, textMessage.getText());
            } else {
                JSONObject jsonObject = new JSONObject(textMessage.getText());
                logger.info("jsonObject:" + jsonObject);
                String subject = null;
                if (jsonObject.has("Subject")) {
                    subject = jsonObject.getString("Subject");
                }
                logger.info("subject:" + subject);
                if(jsonObject.has("Message")){
                    String messageData = jsonObject.getString("Message");
                    logger.info("messageData:" + messageData);
                    handleSNSMessage(subject, messageData);
                }else{
                    handleSNSMessage(subject, jsonObject.toString());
                }
            }
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if (sqsMessageType != null) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }
        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }

    }

    public void handleMessage(SQSMessageType sqsMessageType, String text) throws Exception {

        switch (sqsMessageType) {
            case CREATE_LINK:
                GetOTFWebinarSessionsReq req = gson.fromJson(text, GetOTFWebinarSessionsReq.class);
                otfSessionManager.createLinkSQS(req);
                break;
            case CREATE_SESSION_LINK:
                JsonObject payloadforSessionLink = gson.fromJson(text, JsonObject.class);
                if (payloadforSessionLink.get("sessionId") != null) {
                    otfSessionManager.createSessionLink(payloadforSessionLink.get("sessionId").getAsString());
                } else {
                    logger.error("CREATE_SESSION_LINK msg pushed without sessionId");
                }
                break;
            case CREATE_LINK_AND_REGISTER_GTT: {
                JsonObject payload = gson.fromJson(text, JsonObject.class);
                if (payload.get("sessionId") != null) {
                    otfSessionManager.createSessionLinkAndRegisterGTT(payload.get("sessionId").getAsString());
                } else {
                    logger.error("CREATE_LINK_AND_REGISTER_GTT msg pushed without sessionId");
                }
                break;
            }
            case REGISTER_GTT_FOR_SESSION: {
                JsonObject payload = gson.fromJson(text, JsonObject.class);
                if (payload.get("sessionId") != null) {
                    otfSessionManager.registerAttendees(payload.get("sessionId").getAsString(), null);
                } else {
                    logger.error("REGISTER_GTT_FOR_SESSION msg pushed without sessionId");
                }
                break;
            }
            case REGISTER_GTT_FOR_USER_WITH_WAVE_SESSION: {
                SessionEnrollmentsPojo payload = gson.fromJson(text, SessionEnrollmentsPojo.class);
                otfSessionManager.registerAttendeeWithSession(payload);
                break;
            }

            case SEND_TRIAL_SESSION_MAIL:
            case SEND_SESSION_MAIL: {
                logger.info("Session Houlry mail batch");
                OTFSession session = gson.fromJson(text, OTFSession.class);
                otfSessionManager.sendRegularSessionMail(session, SQSMessageType.SESSION_USER_BATCH_FOR_MAIL);
                break;
            }
            case SEND_LIVE_SESSION_MAIL:
            case SEND_LIVE_TRIAL_SESSION_MAIL: {
                logger.info("Session live mail batch");
                OTFSession session = gson.fromJson(text, OTFSession.class);
                otfSessionManager.sendRegularSessionMail(session, SQSMessageType.SESSION_USER_BATCH_FOR_LIVE_MAIL);
                break;
            }
/*
            case SEND_TRIAL_SESSION_MAIL: {
                OTFSession session = new Gson().fromJson(text, OTFSession.class);
                oTFSessionManager.sendTrialSessionEmail(session);
            }
                break;
*/
            case SESSION_USER_BATCH_FOR_MAIL: {
                logger.info("Session Hourly Mail");
                GroupOfUserEmail group = gson.fromJson(text, GroupOfUserEmail.class);
                otfSessionManager.processRegularSessionEmailForGroup(group, CommunicationType.SESSION_HOUR_REMINDER);
                break;
            }
            case SESSION_USER_BATCH_FOR_LIVE_MAIL: {
                logger.info("Session live Mail");
                GroupOfUserEmail group = gson.fromJson(text, GroupOfUserEmail.class);
                otfSessionManager.processRegularSessionEmailForGroup(group, CommunicationType.SESSION_LIVE_REMINDER);
                break;
            }
//
//            case PREPARE_WAVE_SESSION_DATA_INSIGHTS:

            case INITIATE_ADDED_SESSIONS_MARK_CALENDAR:
                JsonObject payloadJsonForInit = gson.fromJson(text, JsonObject.class);
                Type otfSessionListType = new TypeToken<List<OTFSession>>() {
                }.getType();
                List<OTFSession> sessions = gson.fromJson(payloadJsonForInit.get("sessions"), otfSessionListType);
                Boolean markForCalendar = false;
                if (payloadJsonForInit.get("mark") != null) {
                    markForCalendar = payloadJsonForInit.get("mark").getAsBoolean();
                }
                String fifoGrpId = null;
                if (payloadJsonForInit.get("fifoGrpId") != null) {
                    fifoGrpId = payloadJsonForInit.get("fifoGrpId").getAsString();
                }
                asyncTaskManager.updateCalendarForAddedSessions(sessions, markForCalendar, fifoGrpId);
                break;
            case SEND_EARLY_LEARNING_SESSION_MAIL:
                OTFSession otfSession = gson.fromJson(text, OTFSession.class);
                communicationManager.sendEarlyLearningSessionMailAndSms(otfSession);
                break;
            case SEND_EARLY_LEARNING_BOOKING_MAIL:
                OverBooking overBooking = gson.fromJson(text, OverBooking.class);
                communicationManager.sendEarlyLearningBookingMailAndSms(overBooking);
                break;
            case SEND_SUPER_READER_POST_SESSION_MAIL:
                OTFSession earlyLearningSession = gson.fromJson(text, OTFSession.class);
                communicationManager.sendSuperReaderSessionMail(earlyLearningSession);
            case SEND_EARLY_LEARNING_TEACHER_CHANGE_SESSION_MAIL:
                OTFSession reScheduledSession = gson.fromJson(text, OTFSession.class);
                communicationManager.sendEarlyLearningSessionMailAndSms(reScheduledSession, true);
                break;
            case SEND_EARLY_LEARNING_DEMO_SESSION_CERTIFICATE:
                OTFSession certificateSession = gson.fromJson(text, OTFSession.class);
                communicationManager.sendELDemoSessionCertificate(certificateSession);
                break;
            case SEND_OTF_SESSION_TO_NODE_FOR_CACHING: {
                List<OTFSession> otfSessionsForCaching = gson.fromJson(text, new TypeToken<List<OTFSession>>() {}.getType());
                vedantuWaveSessionManager.sendSessionsToNodeForCaching(otfSessionsForCaching);
                break;
            }
            case MARK_CALENDAR_BATCH_TASK: {
                MarkCalendarBatchPojo pojo = gson.fromJson(text, MarkCalendarBatchPojo.class);
                asyncTaskManager.markStudentCalendarForBatch(pojo);
                break;
            }
            case MARK_CALENDAR_SESSION_TASK: {
                StudentMarkCalendarPojo pojo = gson.fromJson(text, StudentMarkCalendarPojo.class);
                asyncTaskManager.markStudentCalendarForSession(pojo);
                break;
            }
            case MARK_CALENDAR_ENTRIES:
                JsonObject payloadJson = gson.fromJson(text, JsonObject.class);
                Type calendarEntryReqListType = new TypeToken<List<CalendarEntryReq>>(){}.getType();
                List<CalendarEntryReq> reqs = gson.fromJson(payloadJson.get("req"), calendarEntryReqListType);
                Boolean mark = payloadJson.get("mark").getAsBoolean();
                if (mark) {
                    calendarEntryManager.upsertCalendarEntriesBulk(reqs, null);
                } else {
                    calendarEntryManager.unmarkCalendarEntriesBulk(reqs, null);
                }
                break;
            case MARK_DELETED_FOR_DEENROLL: {
                JsonObject gttDeenrollJsonPayload = gson.fromJson(text, JsonObject.class);
                String userId = gttDeenrollJsonPayload.get("userId").getAsString();
                String batchId = gttDeenrollJsonPayload.get("batchId").getAsString();
                EnrollmentPojo enrollmentInfo = gson.fromJson(gttDeenrollJsonPayload.get("enrolmentInfo").getAsString(), EnrollmentPojo.class);
                gttAttendeeDetailsManager.removeGTT(userId, batchId, enrollmentInfo);
                break;
            }
            case ADD_JOIN_DETAILS: {
                JsonObject addJoinDetailsPayload = gson.fromJson(text, JsonObject.class);
                String joinedSessionId = addJoinDetailsPayload.get("sessionId").getAsString();
                String joinedUserId = addJoinDetailsPayload.get("userId").getAsString();
                if (Objects.nonNull(addJoinDetailsPayload.get("ipAddress"))) {
                    String userAgent = null;
                    if (Objects.nonNull(addJoinDetailsPayload.get("userAgent"))) {
                        userAgent = addJoinDetailsPayload.get("userAgent").getAsString();
                    }
                    String ipAddress = (String) addJoinDetailsPayload.get("ipAddress").getAsString();
                    otfSessionManager.addJoinDetails(joinedSessionId, joinedUserId, ipAddress, userAgent);
                } else {
                    logger.error("No ip address found for updating join details for "
                            + joinedSessionId + " " + joinedUserId);
                }
                break;
            }
            case GTT_BULK_UPDATE_TASK: {
                final List<GTTAttendeeDetails> attendeeDetails = gson.fromJson(text, new TypeToken<List<GTTAttendeeDetails>>() {
                }.getType());
                gttAttendeeDetailsManager.queueAttendees(attendeeDetails);
                break;
            }
            case GTT_UPDATE_TASK: {
                final GTTAttendeeDetails attendeeDetail = gson.fromJson(text, GTTAttendeeDetails.class);
                gttAttendeeDetailsManager.saveEntityFromQueue(attendeeDetail);
                break;
            }
            case GTT_UPDATE_SESSION_ATTENDEE_TASK: {
                final UpdateSessionAttendeeDataReq updateSessionAttendeeDataReq = gson.fromJson(text, UpdateSessionAttendeeDataReq.class);
                vedantuWaveSessionManager.updateSessionAttendeeDataFromQueue(updateSessionAttendeeDataReq);
                break;
            }
            case GTT_ENROLMENT_STATUS_CHANGE_TASK: {
                final Map<String, String> payload = gson.fromJson(text, new TypeToken<Map<String, String>>() {
                }.getType());
                String userId = payload.get("userId");
                String batchId = payload.get("batchId");
                String enrolmentInfo = payload.get("enrolmentInfo");
                EnrollmentPojo pojo = gson.fromJson(enrolmentInfo, EnrollmentPojo.class);
                gttAttendeeDetailsManager.gttEnrollmentStatusUpdate(userId, batchId, pojo);
                break;
            }
            case GTT_ENROLMENT_ACTIVATED: {
                final Map<String, String> payload = gson.fromJson(text, new TypeToken<Map<String, String>>() {
                }.getType());
                String enrolmentInfo = payload.get("enrolmentInfo");
                EnrollmentPojo pojo = gson.fromJson(enrolmentInfo, EnrollmentPojo.class);
                gttAttendeeDetailsManager.gttCreateStatusChangeEntriesForEnrollment( pojo);
                break;
            }
            case GTT_SESSION_TIME_CHANGE_TASK: {
                final Map<String, String> payload = gson.fromJson(text, new TypeToken<Map<String, String>>() {
                }.getType());
                String enrolmentInfo = payload.get("otf_session");
                OTFSession pojo = gson.fromJson(enrolmentInfo, OTFSession.class);
                gttAttendeeDetailsManager.gttUpdateSessionTimings(pojo);
                break;
            }
            case SEND_POST_SESSION_SUMMARY_MAILS:
                OTFSession sessionToSendMails = gson.fromJson(text, OTFSession.class);
                otfSessionManager.sendPostSessionEmail(sessionToSendMails);
                break;
            case CANCEL_SESSION_CALENDAR_ENTRY_TASK:
                OTFUpdateSessionRes cancelOrDeleteSessionReq = gson.fromJson(text, OTFUpdateSessionRes.class);
                otfSessionManagerV2.cancelSessionTask(cancelOrDeleteSessionReq);
                break;
            case AUTO_ENROLL_GTT_CREATION:
            case CLICK_TO_ENROLL_GTT_CREATION:
                EnrollmentPojo newEnrollment =  gson.fromJson(text, EnrollmentPojo.class);
                asyncTaskManager.createGTTAttendeeForEnrollment(null, newEnrollment);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);

        }
        logger.info("Message handled");
    }

    public void handleSNSMessage(String subject, String message) throws Exception {
        try {
            //TODO ask wave team to pass subject, so sns messages can be differentiated. Revise india pressure so hacking away now
            UpdateSessionAttendeeDataReq req = gson.fromJson(message, UpdateSessionAttendeeDataReq.class);
            vedantuWaveSessionManager.updateSessionAttendeeData(req);
            logger.info("Message handled");
        } catch (Exception e) {
            logger.error("Exception in handling sns message of otf session queue " + message+" Error: "+e.getMessage());
        }
    }
}
