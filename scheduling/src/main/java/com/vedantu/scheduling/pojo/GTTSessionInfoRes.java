package com.vedantu.scheduling.pojo;

public class GTTSessionInfoRes {
	private String sessionKey;
	private String trainingName;
	private String sessionStartTime;
	private String sessionEndTime;
	private String attendanceCount;
	private String duration;

	/*
	 * [ { "sessionKey":"4621041", "trainingName":"Session1",
	 * "sessionStartTime":"2017-03-02T09:58:52Z",
	 * "sessionEndTime":"2017-03-02T11:12:58Z", "attendanceCount":0,
	 * "duration":75, "organizers":[ { "givenName":"Vedantu",
	 * "surname":"Teacher T20", "email":"gtt20@vedantu.com",
	 * "organizerKey":"8963749134346584069"}]},{ "sessionKey":"4621031",
	 * "trainingName":"Session1", "sessionStartTime":"2017-03-02T09:52:45Z",
	 * "sessionEndTime":"2017-03-02T09:58:21Z", "attendanceCount":1,
	 * "duration":6, "organizers":[ { "givenName":"Vedantu",
	 * "surname":"Teacher T20", "email":"gtt20@vedantu.com",
	 * "organizerKey":"8963749134346584069"}]}]
	 * 
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public String getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(String sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public String getSessionEndTime() {
		return sessionEndTime;
	}

	public void setSessionEndTime(String sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}

	public String getAttendanceCount() {
		return attendanceCount;
	}

	public void setAttendanceCount(String attendanceCount) {
		this.attendanceCount = attendanceCount;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "GTTSessionInfoRes [sessionKey=" + sessionKey + ", trainingName=" + trainingName + ", sessionStartTime="
				+ sessionStartTime + ", sessionEndTime=" + sessionEndTime + ", attendanceCount=" + attendanceCount
				+ ", duration=" + duration + ", toString()=" + super.toString() + "]";
	}
}