package com.vedantu.scheduling.pojo;

public class GTTGetRegistrantDetailsRes
{
	/*
	 * { "email":"123@vedantu.com", "givenName":"shyam", "surname":"krishna",
	 * "status":"APPROVED", "registrationDate":"2017-01-24T08:04:25Z",
	 * "joinUrl":
	 * "https://global.gototraining.com/join/training/893408553371535874/107393998",
	 * "confirmationUrl":
	 * "https://attendee.gototraining.com/registration/confirmation.tmpl?registrant=8437663606892038401&training=893408553371535874",
	 * "registrantKey":"8437663606892038401"}
	 * 
	 */

	private String email;
	private String givenName;
	private String surname;
	private String joinUrl;
	private String confirmationUrl;
	private String registrantKey;
	private String firstName;
	private String lastName;

	public GTTGetRegistrantDetailsRes()
	{
		super();
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getJoinUrl()
	{
		return joinUrl;
	}

	public void setJoinUrl(String joinUrl)
	{
		this.joinUrl = joinUrl;
	}

	public String getConfirmationUrl()
	{
		return confirmationUrl;
	}

	public void setConfirmationUrl(String confirmationUrl)
	{
		this.confirmationUrl = confirmationUrl;
	}

	public String getRegistrantKey()
	{
		return registrantKey;
	}

	public void setRegistrantKey(String registrantKey)
	{
		this.registrantKey = registrantKey;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	@Override
	public String toString()
	{
		return "GTTGetRegistrantDetailsRes [email=" + email + ", givenName=" + givenName + ", surname=" + surname
				+ ", joinUrl=" + joinUrl + ", confirmationUrl=" + confirmationUrl + ", registrantKey=" + registrantKey
				+ ", toString()=" + super.toString() + "]";
	}

}