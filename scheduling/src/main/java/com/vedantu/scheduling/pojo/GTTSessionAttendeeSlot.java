package com.vedantu.scheduling.pojo;

public class GTTSessionAttendeeSlot {
	private String joinTime;
	private String leaveTime;
	private Long timeInPartOfSession;

	/*
	 * { "joinTime": "2017-03-03T14:00:00Z", "leaveTime":
	 * "2017-03-03T14:00:00Z", "timeInPartOfSession": 0 }
	 */
	public GTTSessionAttendeeSlot() {
		super();
	}

	public String getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(String joinTime) {
		this.joinTime = joinTime;
	}

	public String getLeaveTime() {
		return leaveTime;
	}

	public void setLeaveTime(String leaveTime) {
		this.leaveTime = leaveTime;
	}

	public Long getTimeInPartOfSession() {
		return timeInPartOfSession;
	}

	public void setTimeInPartOfSession(Long timeInPartOfSession) {
		this.timeInPartOfSession = timeInPartOfSession;
	}

	@Override
	public String toString() {
		return "GTTSessionAttendeeSlot [joinTime=" + joinTime + ", leaveTime=" + leaveTime + ", timeInPartOfSession="
				+ timeInPartOfSession + ", toString()=" + super.toString() + "]";
	}
}