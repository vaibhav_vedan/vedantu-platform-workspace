package com.vedantu.scheduling.pojo.earlylearning;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CourseProperties {
	private double overBookingFactor;
	private int noOfStudents;
	
	// Time difference allowed between start time and join time in minutes
	private int timeDifference = 10;
}
