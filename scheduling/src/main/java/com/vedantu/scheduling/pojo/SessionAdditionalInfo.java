package com.vedantu.scheduling.pojo;


import com.vedantu.onetofew.pojo.CourseBasicInfo;
import lombok.Data;

@Data
public class SessionAdditionalInfo {
	private CourseBasicInfo course;
	private String groupName;
        
        
	public SessionAdditionalInfo() {
		super();
	}

	public SessionAdditionalInfo(CourseBasicInfo course) {
		super();
		this.course = course;
	}

	public SessionAdditionalInfo(CourseBasicInfo course, String groupName) {
		this.course = course;
		this.groupName = groupName;
	}

}
