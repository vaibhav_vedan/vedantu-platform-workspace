package com.vedantu.scheduling.pojo;

import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OverBookingPojo {

    private String bookingId;
    private String studentId;
    private Long slotStartTime;
    private Long slotEndTime;
    private String sessionId;
    private SessionState state;
    private Set<OTFSessionFlag> flags = new HashSet<>();
    private Set<SessionLabel> labels = new HashSet<>();
    private OTMSessionType otmSessionToolType = OTMSessionType.OTO_NURSERY;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE;
    private List<OTFSessionAttendeeInfo> attendeeInfos;
    private OverBookingStatus isTeacherAssigned;
    private String alternatePhoneNumber;
    private String grade;
}