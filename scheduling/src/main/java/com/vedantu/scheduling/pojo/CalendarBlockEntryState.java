package com.vedantu.scheduling.pojo;

public enum CalendarBlockEntryState {
	ACTIVE, PROCESSED, CANCELED
}
