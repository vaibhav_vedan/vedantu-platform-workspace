package com.vedantu.scheduling.pojo;

import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.response.earlylearning.AbstractEarlyLearningResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Set;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class EarlyLearningSessionPojo extends AbstractEarlyLearningResponse {

    private String demoId;
    private String presenter;
    private Long startTime;
    private Long endTime;
    private SessionState state;
    private String alternatePhoneNumber;

}
