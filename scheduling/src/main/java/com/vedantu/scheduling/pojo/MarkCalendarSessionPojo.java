/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

import java.util.Set;
import lombok.Data;

/**
 *
 * @author ajith
 */
@Data
public class MarkCalendarSessionPojo {
    private String id;
    private Set<String> batchIds;
    private String presenter;
    private Long startTime;
    private Long endTime;  
    private  String primaryBatchId;
}
