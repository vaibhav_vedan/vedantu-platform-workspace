package com.vedantu.scheduling.pojo;

import java.text.ParseException;

import com.vedantu.scheduling.managers.SessionRecordingManager;

public class GTTRecording implements Comparable<GTTRecording> {
	/*
	 * { "recordingId":77820, "name":"Session2.mp4", "downloadUrl":
	 * "https://saas-g2t-cbr-prod-usstandard.s3.amazonaws.com/6848942971880425228/g2t/6879452220571615237/5958566152465130241/1403927195333395205/1485331618634.mp4?AWSAccessKeyId=AKIAJX6KKMO4GGKNZ6UA&Expires=1485945856&Signature=8erg%2FgFTbUpfuMRXacmeps4279E%3D",
	 * "startDate":"2017-01-25T13:36:58+05:30",
	 * "endDate":"2017-01-25T13:46:07+05:30"}
	 */

	private String recordingId;
	private String name;
	private String downloadUrl;
	private String startDate;
	private String endDate;

	public GTTRecording() {
		super();
	}

	public String getRecordingId() {
		return recordingId;
	}

	public void setRecordingId(String recordingId) {
		this.recordingId = recordingId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "GTTRecording [recordingId=" + recordingId + ", name=" + name + ", downloadUrl=" + downloadUrl
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", toString()=" + super.toString() + "]";
	}

	@Override
	public int compareTo(GTTRecording o) {
		try {
			return (SessionRecordingManager.getMillisFromDate(this.getStartDate()) > SessionRecordingManager
					.getMillisFromDate(o.getStartDate())) ? 1 : -1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
