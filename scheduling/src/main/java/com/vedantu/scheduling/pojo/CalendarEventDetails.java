package com.vedantu.scheduling.pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.scheduling.request.UpdateCalendarSlotBitsReq;

public class CalendarEventDetails {
	private Long startTime;
	private Long endTime;
	private long[] oldBitSetLongArray;
	private long[] newBitSetLongArray;
	private Long executionTime;

	public CalendarEventDetails() {
		super();
	}

	public CalendarEventDetails(UpdateCalendarSlotBitsReq req) {
		super();
		this.startTime = req.getStartTime();
		this.endTime = req.getEndTime();
		this.oldBitSetLongArray = req.getOldBitSetLongArray();
		this.newBitSetLongArray = req.getNewBitSetLongArray();
	}

	public long[] getOldBitSetLongArray() {
		return oldBitSetLongArray;
	}

	public void setOldBitSetLongArray(long[] oldBitSetLongArray) {
		this.oldBitSetLongArray = oldBitSetLongArray;
	}

	public long[] getNewBitSetLongArray() {
		return newBitSetLongArray;
	}

	public void setNewBitSetLongArray(long[] newBitSetLongArray) {
		this.newBitSetLongArray = newBitSetLongArray;
	}

	public Long getStartTime() {
		return startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public Long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Long executionTime) {
		this.executionTime = executionTime;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<>();
		if (this.startTime == null || this.startTime <= 0l) {
			errors.add("startTime");
		}
		if (this.endTime == null || this.endTime <= 0l) {
			errors.add("endTime");
		}
		if (this.oldBitSetLongArray == null) {
			errors.add("oldBitSetLongArray");
		}
		if (this.newBitSetLongArray == null) {
			errors.add("newBitSetLongArray");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "CalendarEventDetails [oldBitSetLongArray=" + Arrays.toString(oldBitSetLongArray)
				+ ", newBitSetLongArray=" + Arrays.toString(newBitSetLongArray) + "]";
	}
}
