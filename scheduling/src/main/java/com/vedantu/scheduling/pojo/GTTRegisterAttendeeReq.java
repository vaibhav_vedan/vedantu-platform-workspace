package com.vedantu.scheduling.pojo;

import org.springframework.util.StringUtils;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.scheduling.managers.GTTAttendeeDetailsManager;

public class GTTRegisterAttendeeReq {
	private String email;
	private String givenName;
	private String surname;

	public GTTRegisterAttendeeReq() {
		super();
	}

	public GTTRegisterAttendeeReq(UserBasicInfo user) {
		super();
		this.email = GTTAttendeeDetailsManager.generateEmail(user.getUserId());
		this.givenName = StringUtils.isEmpty(user.getFirstName()) ? String.valueOf(user.getUserId())
				: user.getFirstName();
		this.surname = StringUtils.isEmpty(user.getLastName()) ? "STUDENT" : user.getLastName();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "GTTRegisterAttendeeReq [email=" + email + ", givenName=" + givenName + ", surname=" + surname
				+ ", toString()=" + super.toString() + "]";
	}
}
