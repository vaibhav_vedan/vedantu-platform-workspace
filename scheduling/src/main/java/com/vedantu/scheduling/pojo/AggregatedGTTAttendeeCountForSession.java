/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

/**
 *
 * @author parashar
 */
public class AggregatedGTTAttendeeCountForSession {
    
    private String _id;
    private int count=0;

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "AggregatedGTTAttendeeCountForSession{" + "_id=" + _id + ", count=" + count + '}';
    }
    
    
    
}
