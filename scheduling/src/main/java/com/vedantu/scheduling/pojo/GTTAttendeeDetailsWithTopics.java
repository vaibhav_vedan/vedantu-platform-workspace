package com.vedantu.scheduling.pojo;

import java.util.List;

import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;

public class GTTAttendeeDetailsWithTopics {
	
	private List<String> topics;
	private GTTAttendeeDetails gttAttendeeDetails;
	
	public GTTAttendeeDetailsWithTopics() {
		super();
	}

	public GTTAttendeeDetailsWithTopics(List<String> topics, GTTAttendeeDetails gttAttendeeDetails) {
		super();
		this.topics = topics;
		this.gttAttendeeDetails = gttAttendeeDetails;
	}

	public List<String> getTopics() {
		return topics;
	}	
	
	public void setTopics(List<String> topics) {
		this.topics = topics;
	}
	
	public GTTAttendeeDetails getGttAttendeeDetails() {
		return gttAttendeeDetails;
	}
	
	public void setGttAttendeeDetails(GTTAttendeeDetails gttAttendeeDetails) {
		this.gttAttendeeDetails = gttAttendeeDetails;
	}
	
	@Override
	public String toString() {
		return "GTTAttendeeDetailsWithTopics [topics: " + topics + "gttAttendeeDetails" + gttAttendeeDetails.toString() + "]";
	}
	
}
