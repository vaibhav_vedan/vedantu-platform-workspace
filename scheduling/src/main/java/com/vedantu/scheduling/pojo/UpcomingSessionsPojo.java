package com.vedantu.scheduling.pojo;

import com.google.common.collect.Sets;
import com.vedantu.User.Gender;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.WebinarSessionInfo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.util.ConfigUtils;
import lombok.*;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mano
 */
@Data
public class UpcomingSessionsPojo {

    public enum DisplayState {
        /**
         * upcoming session
         */
        UPCOMING,
        /**
         * ongoing session
         */
        LIVE
    }


    /**
     * order of enums matter for preference sorting
     */
    public enum SessionType {
        /**
         * one to one sessions
         */
        OTO,
        /**
         * one to few batch session
         */
        OTF,
        /**
         * master class session
         */
        WEBINAR
    }

    /**
     * order of enums matter for preference sorting
     */
    public enum SessionSubType {
        /**
         * REGULAR,EXTRA REGULAR
         */
        REGULAR,
        /**
         * DEMO, SALES DEMO
         */
        DEMO,
        /**
         * TRIAL, EXTRA TRIAL
         */
        TRIAL,
        /**
         * REST OF TYPES
         */
        REST;

        private static SessionSubType getSubType(OTMSessionType sessionType) {
            if (sessionType == null) {
                return REST;
            }
            switch (sessionType) {
                case REGULAR:
                case EXTRA_REGULAR:
                case OTO_NURSERY:
                    return SessionSubType.REGULAR;
                case DEMO:
                case SIM_LIVE:
                    return SessionSubType.DEMO;
                case TRIAL:
                case EXTRA_TRIAL:
                case NON_REGULAR:
                    return SessionSubType.TRIAL;
                default:
                    // new type of OTMSessionType will be sorted to last unless added in switch
                    return REST;
            }
        }

        private static SessionSubType getSubType(SessionPayoutType type) {
            if (type == null) {
                return REST;
            }
            switch (type) {

                case FREE:
                case FREE_STUDENT_PAID_TEACHER_NOT_PAID:
                case FREE_STUDENT_NOT_PAID_TEACHER_NOT_PAID:
                case FREE_STUDENT_PAID_TEACHER_PAID:
                case FREE_STUDENT_NOT_PAID_TEACHER_PAID:
                    return TRIAL;
                case PAID:
                    return REGULAR;
                default:
                    // new type of SessionPayoutType will be sorted to last unless added in switch
                    return REST;
            }
        }
    }

    /**
     * order of enums matter for preference sorting
     */
    public enum OtfSearchTerms {
        /**
         * LONG TERM, SUBSCRIPTION
         */
        LONG_TERM,
        /**
         * SHORT TERM
         */
        SHORT_TERM,
        /**
         * TEST
         */
        TEST_SERIES,
        /**
         * MC Sessions
         */
        MICRO_COURSES,
        /**
         * REST OF THEM
         */
        REST;

        private static OtfSearchTerms getSearchTerm(Set<CourseTerm> courseTerms) {
            if (courseTerms == null) {
                return REST;
            }
            if (courseTerms.contains(CourseTerm.LONG_TERM)) {
                return LONG_TERM;
            }
            if (courseTerms.contains(CourseTerm.SHORT_TERM)) {
                return SHORT_TERM;
            }
            if (courseTerms.contains(CourseTerm.TEST_SERIES)) {
                return TEST_SERIES;
            }
            if (courseTerms.contains(CourseTerm.MICRO_COURSES)) {
                return MICRO_COURSES;
            }
            // Rest of the course terms
            return REST;
        }
    }


    @Getter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AttendeeInfo {

        private Long userId;
        private String firstName;
        private String lastName;
        private String fullName;
        private Gender gender;
        private String profilePicUrl;
        private String sliderPicUrl;
    }

    @Getter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class CourseInfo {

        private String id;
        private String title;

    }

    private DisplayState displayState;

    private Long startTime;
    private Long endTime;
    private String sessionId;
    private SessionType sessionType;
    private String sessionJoinUrl;
    private Set<String> subjects;
    private String title;
    private OTFSessionToolType sessionToolType;
    private List<CourseInfo> courseInfos;
    private List<AttendeeInfo> attendeeInfos;
    private transient SessionSubType sessionSubType;
    private transient OtfSearchTerms searchTerms;

    public static List<UpcomingSessionsPojo> convertFromOtfSessions(List<OTFSession> otfSessions,
                                                                    Map<String, BatchBasicInfo> batchDetailedInfo,
                                                                    Map<String, UserBasicInfo> userBasicInfo,
                                                                    Map<Long, Board> boardMap,
                                                                    Map<String, WebinarSessionInfo> webinarInfoMap) {
        List<UpcomingSessionsPojo> pojos = new ArrayList<>();

        for (OTFSession otfSession : otfSessions) {
            UpcomingSessionsPojo pojo = new UpcomingSessionsPojo();
            pojo.setStartTime(otfSession.getStartTime());
            pojo.setEndTime(otfSession.getEndTime());
            pojo.setSessionId(otfSession.getId());
            SessionType sessionType;
            if (webinarInfoMap.containsKey(otfSession.getId())) {
                sessionType = SessionType.WEBINAR;
            } else {
                sessionType = SessionType.OTF;
            }
            pojo.setSessionType(sessionType);
            if (sessionType == SessionType.WEBINAR) {
                WebinarSessionInfo webinarSessionInfo = webinarInfoMap.get(otfSession.getId());
                if (webinarSessionInfo != null) {
                    pojo.setSessionJoinUrl(webinarSessionInfo.getJoinUrl());
                    pojo.setSubjects(webinarSessionInfo.getSubjects());
                }
            } else {
                Board board = boardMap.get(otfSession.getBoardId());
                if (board != null) {
                    // TODO check the course basic info logic to get subject
                    pojo.setSubjects(Collections.singleton(board.getName()));
                }
            }

            pojo.setTitle(otfSession.getTitle());
            pojo.setSessionToolType(otfSession.getSessionToolType());
            pojo.setSessionSubType(SessionSubType.getSubType(otfSession.getOtmSessionType()));
            List<BatchBasicInfo> batchBasicInfos = otfSession.getBatchIds().stream()
                    .map(batchDetailedInfo::get)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            Set<CourseTerm> courseTerms = batchBasicInfos.stream()
                    .map(BatchBasicInfo::getSearchTerms)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());
            pojo.setSearchTerms(OtfSearchTerms.getSearchTerm(courseTerms));
            List<CourseBasicInfo> courseBasicInfos = batchBasicInfos.stream()
                    .map(BatchBasicInfo::getCourseInfo)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());


            pojo.setCourseInfos(courseBasicInfos.stream().map(e -> new CourseInfo(e.getId(), e.getTitle())).collect(Collectors.toList()));
            UserBasicInfo userInfo = userBasicInfo.get(otfSession.getPresenter());
            if (userInfo != null) {
                pojo.setAttendeeInfos(Collections.singletonList(
                        new AttendeeInfo(userInfo.getUserId(), userInfo.getFirstName(), userInfo.getLastName(), userInfo.getFullName(), userInfo.getGender(), userInfo.getProfilePicUrl(), userInfo.getTeacherSliderPicUrl())
                ));
            }
            pojos.add(pojo);
        }
        return pojos;
    }

    public static List<UpcomingSessionsPojo> convertFromOtoSessions(List<SessionInfo> otoSessions, Map<String, CoursePlanBasicInfo> coursePlanMap, Map<String, UserBasicInfo> userBasicInfo) {
        List<UpcomingSessionsPojo> pojos = new ArrayList<>();
        for (SessionInfo otoSession : otoSessions) {
            UpcomingSessionsPojo pojo = new UpcomingSessionsPojo();
            pojo.setStartTime(otoSession.getStartTime());
            pojo.setEndTime(otoSession.getEndTime());
            pojo.setSessionId(String.valueOf(otoSession.getId()));
            pojo.setSessionType(SessionType.OTO);
            pojo.setTitle(otoSession.getTitle());
            pojo.setSubjects(Collections.singleton(otoSession.getSubject()));
            pojo.setSessionSubType(SessionSubType.getSubType(otoSession.getType()));

            UserBasicInfo userInfo = userBasicInfo.get(String.valueOf(otoSession.getTeacherId()));
            if (userInfo != null) {
                pojo.setAttendeeInfos(Collections.singletonList(
                        new AttendeeInfo(userInfo.getUserId(), userInfo.getFirstName(), userInfo.getLastName(), userInfo.getFullName(), userInfo.getGender(), userInfo.getProfilePicUrl(), userInfo.getTeacherSliderPicUrl())
                ));
            }
            CoursePlanBasicInfo info = coursePlanMap.get(otoSession.getContextId());
            if (info != null) {
                pojo.setCourseInfos(Collections.singletonList(new CourseInfo(info.getId(), info.getTitle())));
            }
            pojos.add(pojo);
        }
        return pojos;
    }

}
