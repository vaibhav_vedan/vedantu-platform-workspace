package com.vedantu.scheduling.pojo;

import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.request.GetCalendarSlotBitReq;
import com.vedantu.util.Constants;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public class BitSetEntry {
	private long startTime;
	private long endTime;
	private final CalendarEntrySlotState slotState;
	private long bitDuration = Constants.SLOT_LENGTH;
	private BitSet bitSet;

	public BitSetEntry(long startTime, long endTime, CalendarEntrySlotState slotState, Long bitDuration, BitSet bitSet) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.slotState = slotState;
		this.bitDuration = bitDuration;
		this.bitSet = bitSet;
	}

	public BitSetEntry(Long startTime, Long endTime, CalendarEntrySlotState slotState) {
		this(startTime, endTime, slotState, Constants.SLOT_LENGTH,
				BitSetUtils.createBitSet(startTime, endTime, Constants.SLOT_LENGTH));
	}

	public BitSetEntry(CalendarEventDetails eventDetails, CalendarEntrySlotState state) {
		this(eventDetails.getStartTime(), eventDetails.getEndTime(), state, Constants.SLOT_LENGTH,
				BitSet.valueOf(eventDetails.getNewBitSetLongArray()));
	}

	public BitSetEntry(GetCalendarSlotBitReq getCalendarSlotBitReq) {
		this(getCalendarSlotBitReq.getStartTime(), getCalendarSlotBitReq.getEndTime(),
				getCalendarSlotBitReq.getSlotState(), getCalendarSlotBitReq.getSlotDuration(),
				BitSetUtils.createBitSet(getCalendarSlotBitReq.getStartTime(), getCalendarSlotBitReq.getEndTime(),
						Constants.SLOT_LENGTH));
	}

	public BitSetEntry(CalendarEntry calendarEntry, CalendarEntrySlotState slotState) {
		super();
		this.startTime = calendarEntry.getDayStartTime();
		this.endTime = calendarEntry.getDayStartTime() + CommonCalendarUtils.MILLIS_PER_DAY;
		this.slotState = slotState;

		if (this.slotState == null) {
			return;
		}

		switch (this.slotState) {
		case AVAILABLE:
			this.bitSet = calendarEntry.fetchAvailabilityBitSet();
			break;
		case SESSION:
			this.bitSet = calendarEntry.fetchBookedBitSet();
			break;
		case SESSION_REQUEST:
			this.bitSet = calendarEntry.fetchSessionRequestBitSet();
			break;
		default:
			break;
		}
	}

	public long getStartTime() {
		return startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public BitSet getBitSet() {
		return bitSet;
	}

	public long getBitDuration() {
		return bitDuration;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void updateBitSet(List<CalendarEntry> calendarEntries) {
		if (calendarEntries != null && !calendarEntries.isEmpty()) {
			for (CalendarEntry calendarEntry : calendarEntries) {
				updateBitSet(calendarEntry);
			}
		}
	}

	public void updateBitSet(CalendarEntry calendarEntry) {
		this.updateBitSet(new BitSetEntry(calendarEntry, this.slotState));
	}

	interface BitsetOp {
		/*
		 * Update the bitset 'from' at 'from_index' with the 'to' at 'to_index'
		 */
		void operation(BitSet from, int from_index, BitSet to, int to_index);
	}

	private void update(BitSetEntry entry, BitsetOp op) {
		int startIndex = getBitSetIndex(CommonCalendarUtils.getSlotStartTime(entry.getStartTime()));
		int endIndex = getBitSetIndex(CommonCalendarUtils.getSlotStartTime(entry.getEndTime()));

		assert (startIndex <= endIndex);

		// Validate indexes
		int bitSetSize = BitSetUtils.bitSetSize(this);
		if ((startIndex < 0 && endIndex < 0) || (startIndex > bitSetSize)) {
			return;
		}

		int tempCount = 0;
		if (startIndex < 0) {
			tempCount -= startIndex;
			startIndex = 0;
		}
		if (endIndex >= bitSetSize) {
			endIndex = bitSetSize - 1;
		}

		for (int i = startIndex; i <= endIndex; i++, tempCount++) {
			op.operation(entry.bitSet, tempCount, bitSet, i);
		}
	}

	public void updateBitSet(BitSetEntry entry) {
		BitsetOp update_op = (BitSet from, int from_index, BitSet to, int to_index) -> {
			if (from.get(from_index)) {
				to.set(to_index, true);
			} else {
				to.set(to_index, false);
			}
		};

		update(entry, update_op);
	}

	public void orBitSet(BitSetEntry entry) {
		BitsetOp or_op = (BitSet from, int from_index, BitSet to, int to_index) -> {
			if (from.get(from_index)) {
				to.set(to_index, true);
			}
		};

		update(entry, or_op);
	}

	public int getBitSetIndex(long time) {
		return Long.valueOf((time - this.startTime) / this.bitDuration).intValue();
	}

	public boolean isBitSet(int i) {
		return bitSet.get(i);
	}

	public void updatePadding() {
		int count = BitSetUtils.bitSetSize(this);
		for (int i = 0; i < count; i++) {
			if (!this.bitSet.get(i)) {
				this.bitSet.set(i, false);
			}
		}
	}

	public String getBitSetLongString() {
		return (bitSet == null) ? "null" : Arrays.toString(bitSet.toLongArray());
	}

	@Override
	public String toString() {
		return "BitSetEntry [startTime=" + startTime + ", endTime=" + endTime + ", slotState=" + slotState
				+ ", bitDuration=" + bitDuration + ", bitSet=" + getBitSetLongString() + "]";
	}
}
