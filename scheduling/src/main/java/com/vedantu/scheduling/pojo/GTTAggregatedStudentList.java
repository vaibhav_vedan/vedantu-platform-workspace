/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

/**
 *
 * @author aditya
 */
public class GTTAggregatedStudentList {
    
    private String _id;
    private int previousAttendedCount = 0;
    private int attendedLastClass = 0;
    private int absentCount = 0;
    private String sectionId;

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     * @return the count
     */
    public int getPreviousAttendedCount() {
        return previousAttendedCount;
    }

    /**
     * @param count the count to set
     */
    public void setPreviousAttendedCount(int count) {
        this.previousAttendedCount = count;
    }

    public int getAttendedLastClass() {
        return attendedLastClass;
    }

    /**
     * @param count the count to set
     */
    public void setAttendedLastClass(int count) {
        this.attendedLastClass = count;
    }

    public int getAbsentCount() {
        return absentCount;
    }

    /**
     * @param absentCount the absentCount to set
     */
    public void setAbsentCount(int absentCount) {
        this.absentCount = absentCount;
    }

    public static class Constants {

        public static final String ABSENT_COUNT = "absentCount";
        public static final String PREVIOUS_ATTENDED_COUNT = "previousAttendedCount";
        public static final String ATTENDED_LAST_CLASS = "attendedLastClass";
    
    }

    @Override
    public String toString() {
        return "GTTAggregatedStudentList{" + "_id=" + _id + ", previousAttendedCount=" + previousAttendedCount + '}';
    }
    
    
    
}
