package com.vedantu.scheduling.pojo;

import java.util.List;

public class GTTGetRecordingRes {
	/*
	 * { "trainingKey":5958566152465130241, "recordingList":[ {
	 * "recordingId":77820, "name":"Session2.mp4", "downloadUrl":
	 * "https://saas-g2t-cbr-prod-usstandard.s3.amazonaws.com/6848942971880425228/g2t/6879452220571615237/5958566152465130241/1403927195333395205/1485331618634.mp4?AWSAccessKeyId=AKIAJX6KKMO4GGKNZ6UA&Expires=1485945856&Signature=8erg%2FgFTbUpfuMRXacmeps4279E%3D",
	 * "startDate":"2017-01-25T13:36:58+05:30",
	 * "endDate":"2017-01-25T13:46:07+05:30"}]}
	 */

	private String trainingKey;
	private List<GTTRecording> recordingList;

	public GTTGetRecordingRes() {
		super();
	}

	public String getTrainingKey() {
		return trainingKey;
	}

	public void setTrainingKey(String trainingKey) {
		this.trainingKey = trainingKey;
	}

	public List<GTTRecording> getRecordingList() {
		return recordingList;
	}

	public void setRecordingList(List<GTTRecording> recordingList) {
		this.recordingList = recordingList;
	}

	@Override
	public String toString() {
		return "GTTGetRecordingRes [trainingKey=" + trainingKey + ", recordingList=" + recordingList + ", toString()="
				+ super.toString() + "]";
	}
}
