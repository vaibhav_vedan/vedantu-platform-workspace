package com.vedantu.scheduling.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttemptStat
{
    private Double correct;
    private Double wrong;
    private Double unAttempted;
}
