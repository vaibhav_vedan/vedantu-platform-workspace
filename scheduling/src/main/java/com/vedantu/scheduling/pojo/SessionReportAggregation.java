/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;

import com.vedantu.util.fos.response.AbstractRes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jeet
 */
public class SessionReportAggregation extends AbstractRes{
    private String _id;
    private Long totalSessionCount;
    private Long firstSessionStartTime;
    private List<Long> startTimes = new ArrayList<>();
    private List<Long> endTimes= new ArrayList<>();
    private Set<String> sessionIds = Collections.emptySet();  
    private Set<Long> otoSessionIds = Collections.emptySet();  

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public Long getTotalSessionCount() {
        return totalSessionCount;
    }

    public void setTotalSessionCount(Long totalSessionCount) {
        this.totalSessionCount = totalSessionCount;
    }

    public Long getFirstSessionStartTime() {
        return firstSessionStartTime;
    }

    public void setFirstSessionStartTime(Long firstSessionStartTime) {
        this.firstSessionStartTime = firstSessionStartTime;
    }

    public List<Long> getStartTimes() {
        return startTimes;
    }

    public void setStartTimes(List<Long> startTimes) {
        this.startTimes = startTimes;
    }

    public List<Long> getEndTimes() {
        return endTimes;
    }

    public void setEndTimes(List<Long> endTimes) {
        this.endTimes = endTimes;
    }

    public Set<String> getSessionIds() {
        return sessionIds;
    }

    public void setSessionIds(Set<String> sessionIds) {
        this.sessionIds = sessionIds;
    }

    public Set<Long> getOtoSessionIds() {
        return otoSessionIds;
    }

    public void setOtoSessionIds(Set<Long> otoSessionIds) {
        this.otoSessionIds = otoSessionIds;
    }
    
    public static class Constants {
        public static final String TOTAL_SESSION_COUNT = "totalSessionCount";
        public static final String FIRST_SESSION_START_TIME = "firstSessionStartTime";
        public static final String START_TIMES = "startTimes";
        public static final String END_TIMES = "endTimes";
        public static final String SESSION_IDS = "sessionIds";
        public static final String OTO_SESSION_IDS = "otoSessionIds";
    }
}
