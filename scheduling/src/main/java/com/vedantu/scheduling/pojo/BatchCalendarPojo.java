package com.vedantu.scheduling.pojo;

import java.util.List;

public class BatchCalendarPojo {

    private String batchId;
    private String courseTitle;
    private String groupName;
    private List<SessionCalendarPojo> sessions;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<SessionCalendarPojo> getSessions() {
        return sessions;
    }

    public void setSessions(List<SessionCalendarPojo> sessions) {
        this.sessions = sessions;
    }
}
