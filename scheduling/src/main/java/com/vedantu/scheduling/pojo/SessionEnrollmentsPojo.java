package com.vedantu.scheduling.pojo;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@NoArgsConstructor
public class SessionEnrollmentsPojo {

    private SessionInfo session;
    private List<EnrollmentInfo> enrollments;

    public SessionEnrollmentsPojo(OTFSession session, List<EnrollmentPojo> infos) {
        this.session = new SessionInfo(Objects.requireNonNull(session.getId()), Objects.requireNonNull(session.getEndTime()), Objects.requireNonNull(session.getStartTime()), session.getMeetingId());
        this.enrollments = infos.stream()
                .map(e -> new EnrollmentInfo(Objects.requireNonNull(e.getId()), Objects.requireNonNull(e.getStatus()), Objects.requireNonNull(e.getUserId())))
                .collect(Collectors.toList());
    }

    // to reduce sqs payload size all var name are short
    @AllArgsConstructor
    private static class EnrollmentInfo {
        private final String id;
        private final EntityStatus st;
        private final String uid;
    }


    // to reduce sqs payload size all var name are short
    @AllArgsConstructor
    private static class SessionInfo {
        private final String id;
        private final Long end;
        private final Long st;
        private final String mid;
    }

    public OTFSession toOtfSession() {
        OTFSession otf = new OTFSession();
        otf.setId(session.id);
        otf.setStartTime(session.st);
        otf.setEndTime(session.end);
        otf.setMeetingId(session.mid);
        return otf;
    }

    public Map<String, List<EnrollmentPojo>> toEnrollmentPojosMap() {
        return Optional.ofNullable(enrollments)
                .orElseGet(ArrayList::new)
                .stream()
                .map(e -> {
                    EnrollmentPojo pojo = new EnrollmentPojo();
                    pojo.setId(e.id);
                    pojo.setUserId(e.uid);
                    pojo.setStatus(e.st);
                    return pojo;
                })
                .collect(Collectors.groupingBy(EnrollmentPojo::getUserId));
    }

}
