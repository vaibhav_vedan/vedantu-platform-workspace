package com.vedantu.scheduling.pojo;

public class ScheduledSlot {
	private Long startTime;
	private Long endTime;
	private int count;
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getEndTime() {
		return endTime;
	}
	public ScheduledSlot(Long startTime, Long endTime, int count) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.count = count;
	}
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
