package com.vedantu.scheduling.pojo.earlylearning;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;

public class SlotAvaillability {

	private Map<ProficiencyType, Map<Integer, Long>> proficiencyToTeacherAvailabilityMap = new HashMap<>();

	public Long getTeacherAvailabilityByProficiency(Integer slotIndex, ProficiencyType proficiencyType) {
		if (proficiencyToTeacherAvailabilityMap.containsKey(proficiencyType)) {
			return proficiencyToTeacherAvailabilityMap.get(proficiencyType).getOrDefault(slotIndex, 0L);

		}
		return 0L;
	}

	public void addTeacherAvailabilityByProficiency(ProficiencyType proficiencyType,
			Map<Integer, Long> teachersAvailability) {
		proficiencyToTeacherAvailabilityMap.put(proficiencyType, teachersAvailability);
	}

	public boolean isSlotAvailable(Map<ProficiencyType, Long> proficiencyToCountMap, TeacherCategory teacherCategory,
			int slotIndex, double overBookingFactor) {

		Long availableTeacherSlot;
		Long numberOfBookingInSlot;

		switch (teacherCategory) {
		case PRIMARY_GRADE:
			availableTeacherSlot = getTeacherAvailabilityByProficiency(slotIndex, ProficiencyType.L);
			numberOfBookingInSlot = Optional.ofNullable(proficiencyToCountMap.get(ProficiencyType.L)).orElse(0L);
			if (Math.floor(availableTeacherSlot * overBookingFactor) > numberOfBookingInSlot) {
				return true;
			}
		case SECONDARY_GRADE:
			availableTeacherSlot = getTeacherAvailabilityByProficiency(slotIndex, ProficiencyType.H);
			numberOfBookingInSlot = Optional.ofNullable(proficiencyToCountMap.get(ProficiencyType.H)).orElse(0L);
			if (Math.floor(availableTeacherSlot * overBookingFactor) > numberOfBookingInSlot) {
				return true;
			}
		}
		availableTeacherSlot = getTeacherAvailabilityByProficiency(slotIndex, ProficiencyType.LH);
		numberOfBookingInSlot = Optional.ofNullable(proficiencyToCountMap.get(ProficiencyType.LH)).orElse(0L);
		if (Math.floor(availableTeacherSlot * overBookingFactor) > numberOfBookingInSlot) {
			return true;
		}
		availableTeacherSlot = getTeacherAvailabilityByProficiency(slotIndex, ProficiencyType.HL);
		numberOfBookingInSlot = Optional.ofNullable(proficiencyToCountMap.get(ProficiencyType.HL)).orElse(0L);
		if (Math.floor(availableTeacherSlot * overBookingFactor) > numberOfBookingInSlot) {
			return true;
		}
		return false;
	}

	public boolean isTeacherAvailable(int slotIndex, ProficiencyType proficiencyType) {
		return getTeacherAvailabilityByProficiency(slotIndex, proficiencyType) > 0 ? true : false;
	}
}
