package com.vedantu.scheduling.pojo;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.util.DateTimeUtils;

public class GTTSessionSlot {
	private String startDate;
	private String endDate;

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	public GTTSessionSlot() {
		super();
	}

	public GTTSessionSlot(long startTime, long endTime) {
		super();
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
		this.startDate = sdf.format(new Date(startTime));
		this.endDate = sdf.format(new Date(endTime));
	}

	public GTTSessionSlot(OTFSession oTFSession) {
		this(oTFSession.getStartTime(), oTFSession.getEndTime());
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "GTTSessionSlot [startDate=" + startDate + ", endDate=" + endDate + ", toString()=" + super.toString()
				+ "]";
	}
}
