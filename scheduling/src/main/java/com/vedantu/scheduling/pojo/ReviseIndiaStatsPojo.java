package com.vedantu.scheduling.pojo;

import lombok.Data;

@Data
public class ReviseIndiaStatsPojo {
    private Integer doubts;
    private String userId;
}
