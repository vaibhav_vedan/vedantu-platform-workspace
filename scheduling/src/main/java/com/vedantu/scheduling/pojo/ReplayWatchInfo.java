package com.vedantu.scheduling.pojo;

import java.util.List;

public class ReplayWatchInfo {
    List<ReplayWatchedDuration> durations;
    Long avgDuration;
    Long totalDuration;

    public ReplayWatchInfo() { }

    public List<ReplayWatchedDuration> getDurations() {
        return durations;
    }

    public void setDurations(List<ReplayWatchedDuration> durations) {
        this.durations = durations;
    }

    public Long getAvgDuration() {
        return avgDuration;
    }

    public void setAvgDuration(Long avgDuration) {
        this.avgDuration = avgDuration;
    }

    public Long getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(Long totalDuration) {
        this.totalDuration = totalDuration;
    }

    public ReplayWatchInfo(List<ReplayWatchedDuration> durations, Long avgDuration, Long totalDuration) {
        this.durations = durations;
        this.avgDuration = avgDuration;
        this.totalDuration = totalDuration;
    }


}
