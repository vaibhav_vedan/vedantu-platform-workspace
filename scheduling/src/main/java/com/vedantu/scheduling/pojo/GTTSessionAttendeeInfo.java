package com.vedantu.scheduling.pojo;

import java.util.List;

public class GTTSessionAttendeeInfo {
	/*
	 * [ { "givenName":"Shyam", "surname":"STUDENT",
	 * "email":"5640499012567040@user.com", "timeInSession":0,
	 * "inSessionTimes":[ ]},{ "givenName":"Shyam", "surname":"Teacher1",
	 * "email":"5744125232021504@user.com", "timeInSession":0,
	 * "inSessionTimes":[ ]}]
	 * 
	 */

	private String givenName;
	private String surname;
	private String email;
	private Long timeInSession;
	private List<GTTSessionAttendeeSlot> inSessionTimes;

	public GTTSessionAttendeeInfo() {
		super();
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getTimeInSession() {
		return timeInSession;
	}

	public void setTimeInSession(Long timeInSession) {
		this.timeInSession = timeInSession;
	}

	public List<GTTSessionAttendeeSlot> getInSessionTimes() {
		return inSessionTimes;
	}

	public void setInSessionTimes(List<GTTSessionAttendeeSlot> inSessionTimes) {
		this.inSessionTimes = inSessionTimes;
	}

	@Override
	public String toString() {
		return "GTTSessionAttendeeInfo [givenName=" + givenName + ", surname=" + surname + ", email=" + email
				+ ", timeInSession=" + timeInSession + ", inSessionTimes=" + inSessionTimes + ", toString()="
				+ super.toString() + "]";
	}
}