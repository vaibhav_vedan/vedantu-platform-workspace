package com.vedantu.scheduling.pojo;

public class StudentFeedback {
	private String userId;
	private String feedback;

	public StudentFeedback() {
		super();
	}

	public StudentFeedback(String userId, String feedback) {
		super();
		this.userId = userId;
		this.feedback = feedback;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

}
