package com.vedantu.scheduling.pojo;

import java.util.BitSet;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public class BitSetUtils {

	public static BitSet createBitSet(Long startTime, Long endTime, Long slotLength) {
		int count = bitSetSize(startTime, endTime, slotLength);
		return new BitSet(count);
	}

	public static long[] createBitSetLongArray(Long startTime, Long endTime, Long slotLength) {
		BitSet bitSet = createBitSet(startTime, endTime, slotLength);
		return bitSet.toLongArray();
	}

	public static int bitSetSize(Long startTime, Long endTime, Long slotLength) {
		return Long.valueOf((endTime - startTime) / slotLength).intValue();
	}

	public static int bitSetSize(BitSetEntry entry) {
		return bitSetSize(entry.getStartTime(), entry.getEndTime(), entry.getBitDuration());
	}

	public static BitSet getModifiedBitSet(long[] _old, long[] _new) {
		BitSet oldBitSet = BitSet.valueOf(_old);
		BitSet newBitSet = BitSet.valueOf(_new);
		return getModifiedBitSet(oldBitSet, newBitSet);
	}

	public static BitSet getModifiedBitSet(BitSet _old, BitSet _new) {
		BitSet res = (BitSet) _old.clone();
		res.xor(_new);
		return res;
	}

	public static BitSet createBitSet(String str) {
		BitSet bitSet = null;
		if (StringUtils.isEmpty(str)) {
			bitSet = new BitSet();
		} else {
			int size = str.length();
			bitSet = new BitSet(size);
			for (int i = 0; i < size; i++) {
				if ('1' == str.charAt(i)) {
					bitSet.set(i);
				}
			}
		}

		return bitSet;
	}

	public static String createString(BitSet bitSet, int size) {
		StringBuilder sb = new StringBuilder(size);
		for (int i = 0; i < size; i++) {
			if (bitSet.get(i)) {
				sb.append("1");
			} else {
				sb.append("0");
			}
		}

		return sb.toString();
	}

	public static BitSet createSlotBitSet(SessionSchedule schedule) {
		Long startTime = CommonCalendarUtils.getDayStartTime(schedule.getStartTime());
		BitSet bitSet = new BitSet();
		for (int i = 0; i < schedule.getNoOfWeeks(); i++) {
			if (!CollectionUtils.isEmpty(schedule.getSessionSlots())) {
				for (SessionSlot sessionSlot : schedule.getSessionSlots()) {
					int startIndex = CommonCalendarUtils.getSlotStartIndex(
							sessionSlot.getStartTime() + i * CommonCalendarUtils.MILLIS_PER_WEEK, startTime);
					int endIndex = CommonCalendarUtils
							.getSlotEndIndex(sessionSlot.getEndTime() + i * CommonCalendarUtils.MILLIS_PER_WEEK, startTime);
					bitSet.set(startIndex, endIndex + 1);
				}
			}
		}

		return bitSet;
	}

	public static Integer[] createSlotIntegerArray(Long startTime, Long endTime, BitSet bitSet) {
		int bitSetSize = bitSetSize(startTime, endTime, CommonCalendarUtils.getSlotLength());
		Integer[] bitSetInteger = new Integer[bitSetSize];
		for (int i = 0; i < bitSetSize; i++) {
			if (bitSet.get(i)) {
				bitSetInteger[i] = 1;
			} else {
				bitSetInteger[i] = 0;
			}
		}

		return bitSetInteger;
	}

	public static BitSet createBitSet(Integer[] a) {
		BitSet bitSet = new BitSet(a.length);

		for (int i = 0; i < a.length; i++) {
			if (a[i] == 1) {
				bitSet.set(i);
			}
		}

		return bitSet;
	}
}
