package com.vedantu.scheduling.pojo;

import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GttAttendeeDetailsInfoWithIndex {
    private GTTAttendeeDetailsInfo attendee;
    private String idx;
}
