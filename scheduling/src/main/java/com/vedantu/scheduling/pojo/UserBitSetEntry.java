package com.vedantu.scheduling.pojo;

import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;

public class UserBitSetEntry {
	private String userId;
	private CalendarEntrySlotState slotState;
	private BitSetEntry bitSetEntry;

	public UserBitSetEntry() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	public BitSetEntry getBitSetEntry() {
		return bitSetEntry;
	}

	public void setBitSetEntry(BitSetEntry bitSetEntry) {
		this.bitSetEntry = bitSetEntry;
	}
}
