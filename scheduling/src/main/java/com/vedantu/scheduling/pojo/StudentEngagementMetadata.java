package com.vedantu.scheduling.pojo;

import com.vedantu.scheduling.dao.entity.HotspotNumbers;
import com.vedantu.scheduling.dao.entity.QuizNumbers;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StudentEngagementMetadata {
    private QuizNumbers quizNumbers;
    private HotspotNumbers hotspotNumbers;
    private Double studentPercentile = 0.0;
    private Long studentId;
}
