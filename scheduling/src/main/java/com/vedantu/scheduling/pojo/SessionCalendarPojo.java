package com.vedantu.scheduling.pojo;

import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.pojo.session.OTMSessionType;

public class SessionCalendarPojo {

    private String id;
    private String title;
    private Long start;
    private Long end;
    private Long subject;
    private SessionState state;
    private String tutor;
    private String presenterType;
    private OTMSessionType sessionType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }

    public SessionState getState() {
        return state;
    }

    public void setState(SessionState state) {
        this.state = state;
    }

    public String getTutor() {
        return tutor;
    }

    public void setTutor(String tutor) {
        this.tutor = tutor;
    }

    public String getPresenterType() {
        return presenterType;
    }

    public void setPresenterType(String presenterType) {
        this.presenterType = presenterType;
    }

    public OTMSessionType getSessionType() {
        return sessionType;
    }

    public void setSessionType(OTMSessionType sessionType) {
        this.sessionType = sessionType;
    }
}
