package com.vedantu.scheduling.pojo;

import com.vedantu.User.enums.EventName;
import lombok.Data;

@Data
public class ReviseStatsPojo {
    private String type;
    private String userId;
    private String sessionId;
    private EventName eventName;
}
