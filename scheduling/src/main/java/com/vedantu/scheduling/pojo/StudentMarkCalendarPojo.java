package com.vedantu.scheduling.pojo;

import lombok.Data;

import java.util.List;

@Data
public class StudentMarkCalendarPojo {

    private String batchId;
    private List<MarkCalendarSessionPojo> sessions;
    private List<String> studentIds;
    private boolean mark = true;
    private String fifoGrpId;
}
