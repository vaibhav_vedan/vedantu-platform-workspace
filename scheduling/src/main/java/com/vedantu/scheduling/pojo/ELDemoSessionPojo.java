package com.vedantu.scheduling.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ELDemoSessionPojo {

    private Long demoSessionTime;
    private String StudentId;
    private String SessionId;
    private boolean demoAttended = false;

}
