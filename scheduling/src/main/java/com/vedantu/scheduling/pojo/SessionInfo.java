package com.vedantu.scheduling.pojo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.scheduling.utils.SessionUtils;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.UserSessionInfo;

public class SessionInfo extends Session {

	private String sessionId; // Added so that in email session id is not displayed in exponential form
	private SessionState displayState;
	private Long sessionExpireTime;
	private List<UserSessionInfo> attendees;
	private long currentSystemTime = System.currentTimeMillis();
	private Long activeDuration;
	private Boolean endCoursePlan = Boolean.FALSE;

	private SessionInfo(Session session) {
		super();
		ReflectionUtils.shallowCopyFieldState(session, this);
		updateActiveDuration();
	}

	public SessionInfo(Session session, Long expiryTime, List<SessionAttendee> attendees) {

		this(session);
		this.attendees = createUserSessionInfos(attendees);
		this.displayState = SessionUtils.getDisplayState(session.getState(), expiryTime);
		this.sessionExpireTime = expiryTime;
		updateActiveDuration();
	}

	public List<UserSessionInfo> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<UserSessionInfo> attendees) {
		this.attendees = attendees;
	}

	public SessionState getDisplayState() {
		return displayState;
	}

	public void setDisplayState(SessionState displayState) {
		this.displayState = displayState;
	}

	public Long getSessionExpireTime() {
		return sessionExpireTime;
	}

	public void setSessionExpireTime(Long sessionExpireTime) {
		this.sessionExpireTime = sessionExpireTime;
	}

	public Long getActiveDuration() {
		return activeDuration;
	}

	public void setActiveDuration(Long activeDuration) {
		this.activeDuration = activeDuration;
	}

	private List<UserSessionInfo> createUserSessionInfos(List<SessionAttendee> attendees) {
		List<UserSessionInfo> userSessionInfos = new ArrayList<>();
		if (!CollectionUtils.isEmpty(attendees)) {
			for (SessionAttendee sessionAttendee : attendees) {
				userSessionInfos.add(sessionAttendee.createUserSessionInfo());
			}
		}
		return userSessionInfos;
	}

	public void updateActiveDuration() {
		if (this.getEndedAt() != null && this.getEndedAt() > 0l && this.getStartedAt() != null
				&& this.getStartedAt() > 0l) {
			this.activeDuration = this.getEndedAt() - this.getStartedAt();
		}
	}

	public Boolean getEndCoursePlan() {
		return endCoursePlan;
	}

	public void setEndCoursePlan(Boolean endCoursePlan) {
		this.endCoursePlan = endCoursePlan;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public long getCurrentSystemTime() {
		return currentSystemTime;
	}

	public void setCurrentSystemTime(long currentSystemTime) {
		this.currentSystemTime = currentSystemTime;
	}

	@Override
	public String toString() {
		return "SessionInfo{" +
				"sessionId='" + sessionId + '\'' +
				", displayState=" + displayState +
				", sessionExpireTime=" + sessionExpireTime +
				", attendees=" + attendees +
				", currentSystemTime=" + currentSystemTime +
				", activeDuration=" + activeDuration +
				", endCoursePlan=" + endCoursePlan +
				'}';
	}
}
