package com.vedantu.scheduling.pojo;

public enum CalendarEventState {
	PENDING, PROCESSING, CANCELLED, DONE
}
