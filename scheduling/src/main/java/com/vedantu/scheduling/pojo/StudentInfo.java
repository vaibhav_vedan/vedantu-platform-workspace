package com.vedantu.scheduling.pojo;

import com.vedantu.scheduling.pojo.StudentEngagementMetadata;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StudentInfo {

    Map<String, StudentEngagementMetadata> studentEngagementMetadataMap;
}
