package com.vedantu.scheduling.pojo;

public class GTTRegistrationSettings {
	private boolean disableConfirmationEmail = true;
	private boolean disableWebRegistration = true;

	public GTTRegistrationSettings() {
		super();
	}

	public GTTRegistrationSettings(boolean disableConfirmationEmail, boolean disableWebRegistration) {
		super();
		this.disableConfirmationEmail = disableConfirmationEmail;
		this.disableWebRegistration = disableWebRegistration;
	}

	public boolean isDisableConfirmationEmail() {
		return disableConfirmationEmail;
	}

	public void setDisableConfirmationEmail(boolean disableConfirmationEmail) {
		this.disableConfirmationEmail = disableConfirmationEmail;
	}

	public boolean isDisableWebRegistration() {
		return disableWebRegistration;
	}

	public void setDisableWebRegistration(boolean disableWebRegistration) {
		this.disableWebRegistration = disableWebRegistration;
	}

	@Override
	public String toString() {
		return "GTTRegistrationSettings [disableConfirmationEmail=" + disableConfirmationEmail
				+ ", disableWebRegistration=" + disableWebRegistration + ", toString()=" + super.toString() + "]";
	}
}
