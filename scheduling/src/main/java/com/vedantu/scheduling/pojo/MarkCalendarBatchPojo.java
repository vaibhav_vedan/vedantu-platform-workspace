package com.vedantu.scheduling.pojo;

import lombok.Data;

import java.util.List;

@Data
public class MarkCalendarBatchPojo {

    private String batchId;
    private List<MarkCalendarSessionPojo> sessions;
    private boolean mark = true;
    private String fifoGrpId;
}
