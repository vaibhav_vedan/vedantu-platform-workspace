package com.vedantu.scheduling.pojo;

import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.request.AbstractCreateSessionLinkRequest;

public class GTMCreateSessionReq extends AbstractCreateSessionLinkRequest {
	private String passwordrequired;
	private String conferencecallinfo;
	private String meetingtype;

	public String getPasswordrequired() {
		return passwordrequired;
	}

	public void setPasswordrequired(String passwordrequired) {
		this.passwordrequired = passwordrequired;
	}

	public String getConferencecallinfo() {
		return conferencecallinfo;
	}

	public void setConferencecallinfo(String conferencecallinfo) {
		this.conferencecallinfo = conferencecallinfo;
	}

	public String getMeetingtype() {
		return meetingtype;
	}

	public void setMeetingtype(String meetingtype) {
		this.meetingtype = meetingtype;
	}

	// GotoMeetingRequestPojo(session.getTitle(), sdf.format(sDate),
	// sdf.format(eDate), "false", "VoIP", "", "recurring");

	// String subject, String starttime, String endtime, String
	// passwordrequired,
	// String conferencecallinfo, String timezonekey, String meetingtype
	public GTMCreateSessionReq(OTFSession oTFSession) {
		super(oTFSession.getTitle(), oTFSession.getStartTime(), oTFSession.getEndTime(), "");
		this.passwordrequired = "false";
		this.conferencecallinfo = "VoIP";
		this.meetingtype = "recurring";
	}
}
