package com.vedantu.scheduling.pojo.earlylearning;

import java.util.Map;
import java.util.Objects;

import com.vedantu.onetofew.enums.SessionLabel;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

import lombok.Getter;

@Getter
public class CourseConfiguration {

	private static Logger logger = LogFactory.getLogger(CourseConfiguration.class);

	private CourseConfiguration(){
	}

	@SerializedName("configuration")
	private Map<SessionLabel, CourseProperties> courseConfigurationMap;

	private static CourseConfiguration COURSE_CONFIGURATION;

	public static CourseConfiguration getInstance() {
		try {
			if (Objects.isNull(COURSE_CONFIGURATION)) {
				synchronized (CourseConfiguration.class) {
					if (Objects.isNull(COURSE_CONFIGURATION)) {
						COURSE_CONFIGURATION = new Gson().fromJson(
								ConfigUtils.INSTANCE.getStringValue("vedantu.earlylearning.course.configuration"),
								CourseConfiguration.class);
					}
				}
			}
		} catch (Exception ex) {
			logger.info("Exception" + ex);
		}
		return COURSE_CONFIGURATION;
	}

	public static CourseProperties getCourseProperties(SessionLabel courseType) {
		return getInstance().courseConfigurationMap.get(courseType);
	}

}
