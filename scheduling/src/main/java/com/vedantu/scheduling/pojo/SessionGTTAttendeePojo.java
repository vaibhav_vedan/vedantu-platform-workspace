package com.vedantu.scheduling.pojo;

import java.util.List;

public class SessionGTTAttendeePojo {
	private String sessionId;
	private List<String> attendees;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public List<String> getAttendees() {
		return attendees;
	}
	public void setAttendees(List<String> attendees) {
		this.attendees = attendees;
	}

}
