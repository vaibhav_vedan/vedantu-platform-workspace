package com.vedantu.scheduling.pojo;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.scheduling.dao.entity.OTFSession;

public class GTTCreateSessionReq {
	/*
	 * { "name": "test session1", "description": "session1", "timeZone": "",
	 * "times": [ { "startDate": "2017-01-24T20:00:00Z", "endDate":
	 * "2017-01-24T21:00:00Z" } ], "registrationSettings": {
	 * "disableConfirmationEmail": true, "disableWebRegistration": true },
	 * "organizers": [ ] }
	 */

	private String name;
	private String description;
	private String timeZone = "Asia/Calcutta";
	private List<GTTSessionSlot> times;
	private GTTRegistrationSettings registrationSettings = new GTTRegistrationSettings();
	private List<String> organizers = new ArrayList<String>();

	public GTTCreateSessionReq() {
		super();
	}

	public GTTCreateSessionReq(OTFSession oTFSession, String organizerAccessToken, String organizerKey) {
		super();
		this.name = oTFSession.getTitle();
		this.description = oTFSession.getDescription();

		this.times = new ArrayList<GTTSessionSlot>();
		this.times.add(new GTTSessionSlot(oTFSession));

		this.organizers.add(organizerKey);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public List<GTTSessionSlot> getTimes() {
		return times;
	}

	public void setTimes(List<GTTSessionSlot> times) {
		this.times = times;
	}

	public GTTRegistrationSettings getRegistrationSettings() {
		return registrationSettings;
	}

	public void setRegistrationSettings(GTTRegistrationSettings registrationSettings) {
		this.registrationSettings = registrationSettings;
	}

	public List<String> getOrganizers() {
		return organizers;
	}

	public void setOrganizers(List<String> organizers) {
		this.organizers = organizers;
	}

	@Override
	public String toString() {
		return "GTTCreateSessionReq [name=" + name + ", description=" + description + ", timeZone=" + timeZone
				+ ", times=" + times + ", registrationSettings=" + registrationSettings + ", organizers=" + organizers
				+ ", toString()=" + super.toString() + "]";
	}
}
