package com.vedantu.scheduling.pojo;

public class GTWSessionInfoRes {

	private String sessionKey;
	private String webinarKey;
	private String webinarID;
	private String startTime;
	private String endTime;
	private String registrantsAttended;

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getWebinarKey() {
		return webinarKey;
	}

	public void setWebinarKey(String webinarKey) {
		this.webinarKey = webinarKey;
	}

	public String getWebinarID() {
		return webinarID;
	}

	public void setWebinarID(String webinarID) {
		this.webinarID = webinarID;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getRegistrantsAttended() {
		return registrantsAttended;
	}

	public void setRegistrantsAttended(String registrantsAttended) {
		this.registrantsAttended = registrantsAttended;
	}
}
