package com.vedantu.scheduling.pojo.wavesession;

public enum TeacherTaStudentListTabs {
    PRESENT, NEW, ABSENT, LATE, SPECIAL
}