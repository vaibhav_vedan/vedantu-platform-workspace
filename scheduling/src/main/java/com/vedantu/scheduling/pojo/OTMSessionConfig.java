/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.pojo;
import java.util.*;
import com.vedantu.scheduling.pojo.wavesession.TeacherTaStudentListTabs;
/**
 *
 * @author ajith
 */
public class OTMSessionConfig {

    private boolean chatEnabled = true;
    private String chatMode = "PUBLIC";
    private String handRaiseMode = "IMMEDIATE";
    private boolean welcomeMessage = false;
    private boolean showEmojis = false;
    private boolean upvote = false;
    private boolean doubtStream = false;
    private Boolean showStudentsListTeacher;
    private Boolean showStudentsListTA;
    private List<TeacherTaStudentListTabs> studentListTabsTeacher;
    private List<TeacherTaStudentListTabs> studentListTabsTA;
    private Boolean leaderBoardStreak;
    private Boolean quizStreak;
    private Boolean revise = false;
    private Boolean sectionPhase3 = false;

    public Boolean getRevise() {
        return revise;
    }

    public void setRevise(Boolean revise) {
        this.revise = revise;
    }


    public boolean isChatEnabled() {
        return chatEnabled;
    }

    public void setChatEnabled(boolean chatEnabled) {
        this.chatEnabled = chatEnabled;
    }

    public String getChatMode() {
        return chatMode;
    }

    public void setChatMode(String chatMode) {
        this.chatMode = chatMode;
    }

    public String getHandRaiseMode() {
        return handRaiseMode;
    }

    public void setHandRaiseMode(String handRaiseMode) {
        this.handRaiseMode = handRaiseMode;
    }

    public boolean getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(boolean welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public boolean getShowEmojis() {
        return showEmojis;
    }

    public void setShowEmojis(boolean showEmojis) {
        this.showEmojis = showEmojis;
    }

    public boolean getUpvote() {
        return upvote;
    }

    public void setUpvote(boolean upvote) {
        this.upvote = upvote;
    }

    public boolean getDoubtStream() {
        return doubtStream;
    }

    public void setDoubtStream(boolean doubtStream) {
        this.doubtStream = doubtStream;
    }

    public Boolean getShowStudentsListTeacher() {
        return showStudentsListTeacher;
    }

    public void setShowStudentsListTeacher(Boolean showStudentsListTeacher) {
        this.showStudentsListTeacher = showStudentsListTeacher;
    }

    public Boolean getShowStudentsListTA() {
        return showStudentsListTA;
    }

    public void setShowStudentsListTA(Boolean showStudentsListTA) {
        this.showStudentsListTA = showStudentsListTA;
    }

    public List<TeacherTaStudentListTabs> getStudentListTabsTeacher() {
        return studentListTabsTeacher;
    }

    public void setStudentListTabsTeacher(List<TeacherTaStudentListTabs> studentListTabsTeacher) {
        this.studentListTabsTeacher = studentListTabsTeacher;
    }

    public List<TeacherTaStudentListTabs> getStudentListTabsTA() {
        return studentListTabsTA;
    }

    public void setStudentListTabsTA(List<TeacherTaStudentListTabs> studentListTabsTA) {
        this.studentListTabsTA = studentListTabsTA;
    }
    
    public Boolean getLeaderBoardStreak() {
        return leaderBoardStreak;
    }

    public void setLeaderBoardStreak(Boolean leaderBoardStreak) {
        this.leaderBoardStreak = leaderBoardStreak;
    }

    public Boolean getQuizStreak() {
        return quizStreak;
    }

    public void setQuizStreak(Boolean quizStreak) {
        this.quizStreak = quizStreak;
    }

    public Boolean getSectionPhase3() {
        return sectionPhase3;
    }

    public void setSectionPhase3(Boolean sectionPhase3) {
        this.sectionPhase3 = sectionPhase3;
    }

    @Override
    public String toString() {
        return "OTMSessionConfig{" + "chatEnabled=" + chatEnabled + ", chatMode=" + chatMode + ", handRaiseMode="
                + handRaiseMode + "welcomeMessage=" + welcomeMessage + "showEmojis=" + showEmojis + "upvote=" + upvote
                + "doubtStream=" + doubtStream + "}";
    }

}
