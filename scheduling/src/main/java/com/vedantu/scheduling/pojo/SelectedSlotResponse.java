package com.vedantu.scheduling.pojo;

import java.util.List;

import com.vedantu.scheduling.response.SlotSelectionPojo;
import com.vedantu.session.pojo.SessionSlot;

public class SelectedSlotResponse {
	private Long startTime;
	private List<SessionSlot> threeSessions;
	private List<SessionSlot> sixSessions;

	public SelectedSlotResponse() {
		super();
	}

	public SelectedSlotResponse(Long startTime, List<SessionSlot> threeSessions, List<SessionSlot> sixSessions) {
		super();
		this.startTime = startTime;
		this.threeSessions = threeSessions;
		this.sixSessions = sixSessions;
	}

	public SelectedSlotResponse(Long startTime, SlotSelectionPojo slotSelectionPojo) {
		super();
		this.startTime = startTime;
		this.sixSessions = slotSelectionPojo.getSixSessions();
		this.threeSessions = slotSelectionPojo.getThreeSessions();
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public List<SessionSlot> getThreeSessions() {
		return threeSessions;
	}

	public void setThreeSessions(List<SessionSlot> threeSessions) {
		this.threeSessions = threeSessions;
	}

	public List<SessionSlot> getSixSessions() {
		return sixSessions;
	}

	public void setSixSessions(List<SessionSlot> sixSessions) {
		this.sixSessions = sixSessions;
	}

	@Override
	public String toString() {
		return "SelectedSlotResponse [startTime=" + startTime + ", threeSessions=" + threeSessions + ", sixSessions="
				+ sixSessions + "]";
	}
}
