package com.vedantu.scheduling.pojo;

import com.vedantu.scheduling.enums.AttendanceFeedbackTag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AttendanceFeedback {
    private String feedbackText;
    private AttendanceFeedbackTag attendanceFeedbackTag;
    private Long submissionTime;
}
