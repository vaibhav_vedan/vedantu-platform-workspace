package com.vedantu.scheduling.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GTTAttendanceBasic
{
    private AttemptStat quizNumbers;
    private AttemptStat hotspotNumbers;
}
