package com.vedantu.scheduling.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsightSortBy {

    private String insightName;
    private String sortOrder;
}
