package com.vedantu.scheduling.pojo;

import com.vedantu.User.enums.TeacherCategory.ProficiencyType;

import lombok.Data;

@Data
public class SlotBookingCount {
    private Long slotStartTime;
    private Long count;
    private ProficiencyType proficiencyType;
}