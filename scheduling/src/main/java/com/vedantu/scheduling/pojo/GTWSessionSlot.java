package com.vedantu.scheduling.pojo;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.util.DateTimeUtils;

public class GTWSessionSlot {

	private String startTime;
	private String endTime;

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	public GTWSessionSlot() {
		super();
	}

	public GTWSessionSlot(long startTime, long endTime) {
		super();
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
		this.startTime = sdf.format(new Date(startTime));
		this.endTime = sdf.format(new Date(endTime));
	}

	public GTWSessionSlot(OTFSession oTFSession) {
		this(oTFSession.getStartTime(), oTFSession.getEndTime());
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndDate() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "GTTSessionSlot [startTime=" + startTime + ", endTime=" + endTime + ", toString()=" + super.toString()
				+ "]";
	}
}
