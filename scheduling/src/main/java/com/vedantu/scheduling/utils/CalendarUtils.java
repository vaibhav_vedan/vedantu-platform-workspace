package com.vedantu.scheduling.utils;

import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.Constants;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public class CalendarUtils {

	public static boolean calculateTrueAvailability(Integer index, CalendarEntry teacherEntry,
			CalendarEntry studentEntry) {
		return isTeacherAvailable(index, teacherEntry) && isStudentAvailable(index, studentEntry);
	}

	private static boolean isTeacherAvailable(Integer index, CalendarEntry teacherEntry) {
		boolean trueAvailable = false;
		if (teacherEntry != null && validateTime(index, teacherEntry.getDayStartTime())) {
			trueAvailable = teacherEntry.getAvailability().contains(index) && !teacherEntry.getBooked().contains(index)
					&& !teacherEntry.getSessionRequest().contains(index);
		}

		return trueAvailable;
	}

	public static boolean isTeacherAvailabilityMarked(Integer index, CalendarEntry teacherEntry) {
		boolean available = false;
		if (teacherEntry != null && !validateTime(index, teacherEntry.getDayStartTime())) {
			available = teacherEntry.getAvailability().contains(index);
		}

		return available;
	}

	private static boolean isStudentAvailable(Integer index, CalendarEntry studentEntry) {
		boolean trueAvailable = true;
		if (studentEntry != null) {
			trueAvailable = !studentEntry.getBooked().contains(index)
					&& !studentEntry.getSessionRequest().contains(index);
		}

		return trueAvailable;
	}

	public static Map<Long, CalendarEntry> createCalendarEntryMap(List<CalendarEntry> calendarEntries) {
		Map<Long, CalendarEntry> calendarMap = new HashMap<>();
		if (calendarEntries != null) {
			for (CalendarEntry entry : calendarEntries) {
				calendarMap.put(entry.getDayStartTime(), entry);
			}
		}

		return calendarMap;
	}

	public static void updateTruerAvailabilityArray(int[] truerAvailability, Integer[] result, int resultStartIndex,
			int resultEndIndex) {
		// End time is exclusive
		for (int i = resultStartIndex, tempIndex = 0; i < resultEndIndex; i++, tempIndex++) {
			truerAvailability[tempIndex] += result[i];
		}
	}

	public static void updateTruerAvailabilityByMatch(int[] truerAvailability, float match, int repeat) {
		int matchCount = Double.valueOf(Math.ceil(match * repeat)).intValue();
		for (int i = 0; i < truerAvailability.length; i++) {
			if (truerAvailability[i] >= matchCount) {
				truerAvailability[i] = 1;
			} else {
				truerAvailability[i] = 0;
			}
		}
	}

	public static void updateConflictBitsBySessions(BitSet conflictBitSet, BitSet slotBitSet) {
		// All the bits set in conflictBitSet should always be set in slotBitSet

		for (int i = 0; i < conflictBitSet.length(); i++) {
			// conflictStartIndex index is inclusive
			int conflictStartIndex = conflictBitSet.nextSetBit(i);
			if (conflictStartIndex < 0) {
				break;
			}
			// conflictEndIndex index is exclusive
			int conflictEndIndex = conflictBitSet.nextClearBit(conflictStartIndex);

			// sessionStartIndex index is exclusive here as we are getting
			// previous clear bit.
			int sessionStartIndex = slotBitSet.previousClearBit(conflictStartIndex);
			if (sessionStartIndex < 0) {
				// As BitSet does not take negative index we should start all
				// the operations with start index +1.
				sessionStartIndex = conflictStartIndex - 1;
			}

			// sessionEndIndex index is exclusive
			int sessionEndIndex = slotBitSet.nextClearBit(conflictEndIndex);
			for (int j = (sessionStartIndex + 1); j < sessionEndIndex; j++) {
				conflictBitSet.set(j);
			}

			i = sessionEndIndex;
		}
	}

	public static Set<Integer> createArrayList(int start, int end) {
		return IntStream.rangeClosed(start, end).boxed().collect(Collectors.toSet());
	}

	public static boolean validateTime(int index, long dayStartTime) {
		return CommonCalendarUtils.getAbsoluteSlotTime(index, dayStartTime) > System.currentTimeMillis();
	}

	public static Long getDayStartTime(Long time) {
		Long startTime = time;
		if (0l != (time % DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE)) {
			Long gmtDiff = time % DateTimeUtils.MILLIS_PER_DAY;
			Long gmtTime = time - gmtDiff;
			startTime = gmtTime + DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			if (gmtDiff > DateTimeUtils.IST_TIME_DIFFERENCE) {
				startTime = time - gmtDiff + DateTimeUtils.MILLIS_PER_DAY - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			} else {
				startTime = time - gmtDiff - DateTimeUtils.IST_TIME_DIFFERENCE_NEGATIVE;
			}
		}
		return startTime;
	}

	public static BitSet getBitSetFromSessions(List<Session> sessions, long startTime) {
		BitSet resultBitSet = new BitSet();
		if (sessions != null) {
			for (Session session : sessions) {
				if (session.getStartTime() < System.currentTimeMillis()
						|| !SessionState.SCHEDULED.equals(session.getState())) {
					continue;
				}
				int startIndex = getSlotStartTimeIndex(startTime, session.getStartTime());
				if (startIndex < 0) {
					startIndex = 0;
				}
				int endIndex = getSlotEndTimeIndex(startTime, session.getEndTime());
				for (int i = startIndex; i <= endIndex; i++) {
					if (resultBitSet.get(i)) {
						// TODO :throw sentry error
					}

					resultBitSet.set(i);
				}
			}
		}
		return resultBitSet;
	}

	public static BitSet getBitSetOTFFromSessions(List<OTFSessionPojoUtils> sessions, long startTime) {
		BitSet resultBitSet = new BitSet();
		if (sessions != null) {
			for (OTFSessionPojoUtils session : sessions) {
				if (session.getStartTime() < System.currentTimeMillis()
						|| !com.vedantu.onetofew.enums.SessionState.SCHEDULED.equals(session.getState())) {
					continue;
				}
				int startIndex = getSlotStartTimeIndex(startTime, session.getStartTime());
				if (startIndex < 0) {
					startIndex = 0;
				}
				int endIndex = getSlotEndTimeIndex(startTime, session.getEndTime());
				for (int i = startIndex; i <= endIndex; i++) {
					if (resultBitSet.get(i)) {
						// TODO :throw sentry error
					}

					resultBitSet.set(i);
				}
			}
		}
		return resultBitSet;
	}

	private static int getSlotStartTimeIndex(long startTime, long time) {
		return Long.valueOf((getSlotStartTime(time) - startTime) / Constants.SLOT_LENGTH).intValue();
	}

	private static int getSlotEndTimeIndex(long startTime, long time) {
		return Long.valueOf((getSlotEndTime(time) - startTime) / Constants.SLOT_LENGTH).intValue();
	}

	private static long getSlotStartTime(long time) {
		return (time % Constants.SLOT_LENGTH == 0) ? time : (time - (time % Constants.SLOT_LENGTH));
	}

	private static long getSlotEndTime(long time) {
		return (time % Constants.SLOT_LENGTH == 0) ? (time - Constants.SLOT_LENGTH)
				: (time - (time % Constants.SLOT_LENGTH));
	}
}
