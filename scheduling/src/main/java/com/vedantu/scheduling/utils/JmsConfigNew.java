package com.vedantu.scheduling.utils;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.scheduling.listeners.SQSListener;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/getting-started.html
 */
@Configuration
@EnableJms
public class JmsConfigNew {

    @Autowired
    private SQSListener sqsListener;

    private SQSConnection connection;

    private static final Logger logger = LogManager.getRootLogger();

    final SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
            .withRegion(Region.getRegion(Regions.AP_SOUTHEAST_1))
            .withNumberOfMessagesToPrefetch(10).build();
    final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    final boolean usesqs = (!(StringUtils.isEmpty(env) || env.equals("LOCAL")));

    private List<DefaultMessageListenerContainer> dmlcs = new ArrayList<>();

    private Set<SQSQueue> queueListToCreateAlarm = new HashSet<>();

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;

    @Bean
    public SQSConnection sqsConnection() throws JMSException {
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (usesqs) { //|| env.equals("DEV1"))
            connection = sqsConnectionFactory.createConnection();
            Session session = connection.createSession(false, SQSSession.CLIENT_ACKNOWLEDGE);
            MessageConsumer dmlc = session.createConsumer(session.createQueue(SQSQueue.OTF_SESSION_QUEUE.getQueueName(env)));

            MessageConsumer otfsessionqueuenonfifo = session.createConsumer(session.createQueue(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO.getQueueName(env)));
//
//        DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
//        dmlc.setConnectionFactory(sqsConnectionFactory);
            //dmlc.setDestinationName(queueName);
            //dmlc.setSessionAcknowledgeMode(Session.DUPS_OK_ACKNOWLEDGE);
            //dmlc.setConcurrency("3-10");
            dmlc.setMessageListener(sqsListener);
            otfsessionqueuenonfifo.setMessageListener(sqsListener);

            //Add queues to create alarm
            queueListToCreateAlarm.add(SQSQueue.OTF_SESSION_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.OTF_SESSION_QUEUE_DL);
            queueListToCreateAlarm.add(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO);
            queueListToCreateAlarm.add(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO_DL);
            queueListToCreateAlarm.add(SQSQueue.CALENDAR_OPS_DL);
            queueListToCreateAlarm.add(SQSQueue.CALENDAR_OPS);
            queueListToCreateAlarm.add(SQSQueue.CALENDAR_BATCH_OPS_DL);
            queueListToCreateAlarm.add(SQSQueue.CALENDAR_BATCH_OPS);
            queueListToCreateAlarm.add(SQSQueue.OTF_SESSION_GTT_CREATE_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.OTF_SESSION_GTT_CREATE_DL_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.REGISTER_GTT_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.REGISTER_GTT_DL_QUEUE);



            connection.start();
            awsCloudWatchManager.createAlarms(queueListToCreateAlarm);
            logger.info("JMS Bean created");

            return connection;
        } else {
            return null;
        }
    }

    @Bean
    public DefaultMessageListenerContainer calendarOpsQueueListener() {
        return generateDMLC(SQSQueue.CALENDAR_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer otfSessionQueueListener() {
        return generateDMLC(SQSQueue.OTF_SESSION_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer otfPostSessionNonFifoQueueListener() {
        return generateDMLC(SQSQueue.OTF_POSTSESSION_QUEUE_NON_FIFO, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer calendarBatchOpsQueueListener() {
        return generateDMLC(SQSQueue.CALENDAR_BATCH_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer calendarOpsFifoQueueListener() {
        return generateDMLC(SQSQueue.CALENDAR_OPS_FIFO, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer calendarBatchOpsFifoQueueListener() {
        return generateDMLC(SQSQueue.CALENDAR_BATCH_FIFO_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer gttMarkDeleteOpsFifoQueueListener() {
        return generateDMLC(SQSQueue.GTT_MARK_DELETE_OPS_FIFO, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer gttUpdateOpsFifoQueueListener() {
        return generateDMLC(SQSQueue.GTT_UPDATE_OPS_FIFO, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer gttBulkUpdateOpsFifoQueueListener() {
        return generateDMLC(SQSQueue.GTT_BULK_UPDATE_OPS_FIFO, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer gttEnrolmentOpsFifoQueueListener() {
        return generateDMLC(SQSQueue.GTT_ENROLMENT_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer otfSessionGttCreateOps() {
        return generateDMLC(SQSQueue.OTF_SESSION_GTT_CREATE_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer registerGttAttendee() {
        return generateDMLC(SQSQueue.REGISTER_GTT_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer bundleGTTCreationOpsListener() {
        return generateDMLC(SQSQueue.BUNDLE_GTT_CREATION_OPS, sqsListener);
    }


    public DefaultMessageListenerContainer generateDMLC(SQSQueue queue, MessageListener _sqsListener) {
        if (usesqs) {
            logger.info("creating dmlc for " + queue);
            DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
            dmlc.setConnectionFactory(sqsConnectionFactory);
            dmlc.setDestinationName(queue.getQueueName(env));
            dmlc.setMessageListener(_sqsListener);
            dmlc.setConcurrency(queue.getMaxConcurrency());//dmlc.setConcurrency("3-10");
            dmlc.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
            dmlcs.add(dmlc);

            return dmlc;
        } else {
            return null;
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (connection != null) {
                connection.close();
            }
            if (ArrayUtils.isNotEmpty(dmlcs)) {
                dmlcs.stream().filter((dmlc) -> (dmlc != null)).forEachOrdered((dmlc) -> {
                    logger.info("destroying dmlc for " + dmlc.getDestinationName());
                    dmlc.destroy();
                });
            }
            com.amazonaws.http.IdleConnectionReaper.shutdown();
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (JMSException e) {
            logger.error("Error in closing sqs connection ", e);
        }
    }
}
