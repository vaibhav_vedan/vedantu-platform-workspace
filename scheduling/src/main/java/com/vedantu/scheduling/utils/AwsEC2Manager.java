/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.utils;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.ShutdownBehavior;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AwsEC2Manager {

    private AmazonEC2 ec2Client;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsEC2Manager.class);

    private static final String SESSION_RECORDER_AMI_ID = ConfigUtils.INSTANCE.getStringValue("vedantu.otm.recorder.ami_id");
    private static final String SESSION_RECORDER_EMAIL = ConfigUtils.INSTANCE.getStringValue("vedantu.otm.recorder.email");
    private static final String SESSION_RECORDER_KEY = ConfigUtils.INSTANCE.getStringValue("vedantu.otm.recorder.key");
    private static final String SESSION_RECORDER_SECURITY_GROUP = ConfigUtils.INSTANCE.getStringValue("vedantu.otm.recorder.security_group");
    private static final String SESSION_RECORDER_SUBNET = ConfigUtils.INSTANCE.getStringValue("vedantu.otm.recorder.subnet");
    private static final String SESSION_RECORDER_IAM = ConfigUtils.INSTANCE.getStringValue("vedantu.otm.recorder.iam");
    private static final String SESSION_RECORDER_ENV = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();

    @PostConstruct
    public void init() {
        if (!SESSION_RECORDER_ENV.equalsIgnoreCase("LOCAL")) {
            ec2Client = AmazonEC2ClientBuilder.defaultClient();
        }
    }

    public void createOTMSessionRecorder(String sessionId, Long timeout) {
        if (SESSION_RECORDER_ENV.equalsIgnoreCase("LOCAL")) {
            return;
        }
        if (!SESSION_RECORDER_ENV.equalsIgnoreCase("PROD")) {
            if (SESSION_RECORDER_ENV.equalsIgnoreCase("WAVE") || SESSION_RECORDER_ENV.equalsIgnoreCase("MOBILE")) {
                timeout = 1800l;
            }else{
                timeout = 300l;
            }
        }
        logger.info("got request to start a sessionRecorder sessionId:" + sessionId + " timeout:" + timeout);
        String userData = String.format("#!/bin/bash\n"
                + "cd /home/ubuntu/vedantu-spot-instances/oto-session-recorder \n"
                + "ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts\n"
                + "ssh-agent bash -c 'ssh-add /home/ubuntu/.ssh/id_rsa; git checkout feature/otm_recording'\n"
                + "ssh-agent bash -c 'ssh-add /home/ubuntu/.ssh/id_rsa; git stash'\n"
                + "ssh-agent bash -c 'ssh-add /home/ubuntu/.ssh/id_rsa; git pull'\n"
                    + "sudo -u ubuntu echo \"env='%s'\" > /home/ubuntu/vedantu-spot-instances/oto-session-recorder/env.py\n"
                + "sudo -u ubuntu export LC_ALL=C\n"
                //                + "at now +2 minutes -f /home/ubuntu/vedantu-spot-instances/oto-session-recorder/startRecorder.sh > /home/ubuntu/vedantu-spot-instances/oto-session-recorder/startRecorder.log"
                + "sudo pip install -r /home/ubuntu/vedantu-spot-instances/oto-session-recorder/requirements.txt -t /home/ubuntu/vedantu-spot-instances/oto-session-recorder/.\n"
                + "sudo -u ubuntu python /home/ubuntu/vedantu-spot-instances/oto-session-recorder/otm-session-recorder.py -s %s -t %s -u %s >> /home/ubuntu/vedantu-spot-instances/oto-session-recorder/test.log 2>&1",
                SESSION_RECORDER_ENV,
                sessionId,
                timeout,
                SESSION_RECORDER_EMAIL);

        InstanceNetworkInterfaceSpecification interfaceSpecification = new InstanceNetworkInterfaceSpecification()
                .withSubnetId(SESSION_RECORDER_SUBNET)
                .withAssociatePublicIpAddress(true)
                .withGroups(SESSION_RECORDER_SECURITY_GROUP)
                .withDeviceIndex(0);
        IamInstanceProfileSpecification iamInstanceProfileSpecification = new IamInstanceProfileSpecification()
                .withName(SESSION_RECORDER_IAM);
        RunInstancesRequest run_request = new RunInstancesRequest()
                .withImageId(SESSION_RECORDER_AMI_ID)
                .withInstanceType(InstanceType.C4Large)
                .withMaxCount(1)
                .withMinCount(1)
                .withInstanceInitiatedShutdownBehavior(ShutdownBehavior.Terminate)
                .withUserData(org.apache.commons.codec.binary.Base64.encodeBase64String(userData.getBytes()))
                .withNetworkInterfaces(interfaceSpecification)
                .withIamInstanceProfile(iamInstanceProfileSpecification)
                .withKeyName(SESSION_RECORDER_KEY);
        RunInstancesResult run_response = ec2Client.runInstances(run_request);
        String instanceId = run_response.getReservation().getReservationId();
        logger.info("Start a sessionRecorder sessionId:" + sessionId + " timeout:" + timeout + " instanceId:" + instanceId);
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (ec2Client != null) {
                ec2Client.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in shutting down ec2client ", e);
        }
    }
}
