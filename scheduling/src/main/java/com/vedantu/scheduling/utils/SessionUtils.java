package com.vedantu.scheduling.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public class SessionUtils {
	public static List<Long> getSessionIds(List<Session> sessions) {
		List<Long> ids = new ArrayList<Long>();
		if (!CollectionUtils.isEmpty(sessions)) {
			for (Session session : sessions) {
				if (session.getId() != null) {
					ids.add(session.getId());
				}
			}
		}
		return ids;
	}

	public static SessionState getDisplayState(SessionState sessionState, Long expiryTime) {
		if (expiryTime == null) {
			return sessionState;
		}
		switch (sessionState) {
		case SCHEDULED:
                    if (System.currentTimeMillis() > expiryTime) {
                            return SessionState.EXPIRED;
                    }
                    break;
		case FORFEITED:
			return SessionState.ENDED;
		default:
			break;
		}

		return sessionState;
	}

	public static List<Long> getAttendeeIds(Session session) {
		List<Long> ids = new ArrayList<Long>();
		if (session != null) {
			ids.addAll(session.getStudentIds());
			ids.add(session.getTeacherId());
		}

		return ids;
	}

	public static List<Long> getDayStartTimes(Session session) {
		List<Long> dayStartTimes = new ArrayList<Long>();
		long dayStartTime = CommonCalendarUtils.getDayStartTime(session.getStartTime());
		long dayEndTime = CommonCalendarUtils.getDayStartTime_end(session.getEndTime());
		for (long i = dayStartTime; i <= dayEndTime; i += DateTimeUtils.MILLIS_PER_DAY) {
			dayStartTimes.add(i);
		}

		return dayStartTimes;
	}

	public static List<Long> getDayStartTimes(List<Session> sessions) {
		List<Long> dayStartTimes = new ArrayList<Long>();
		if (!CollectionUtils.isEmpty(sessions)) {
			sortSessionsByStartTime(sessions);
			long dayStartTime = CommonCalendarUtils.getDayStartTime(sessions.get(0).getStartTime());
			long dayEndTime = CommonCalendarUtils.getDayStartTime_end(sessions.get(sessions.size() - 1).getEndTime());
			for (long i = dayStartTime; i <= dayEndTime; i += DateTimeUtils.MILLIS_PER_DAY) {
				dayStartTimes.add(i);
			}

		}
		return dayStartTimes;
	}

	public static void sortSessionsByStartTime(List<Session> sessions) {
		Collections.sort(sessions, new Comparator<Session>() {
			@Override
			public int compare(Session o1, Session o2) {
				return o1.getStartTime().compareTo(o2.getStartTime());
			}
		});
	}

	public static List<String> convertLongListToStringList(List<Long> values) {
		List<String> results = new ArrayList<>();
		if (!CollectionUtils.isEmpty(values)) {
			for (Long value : values) {
				results.add(String.valueOf(value));
			}
		}

		return results;
	}
}
