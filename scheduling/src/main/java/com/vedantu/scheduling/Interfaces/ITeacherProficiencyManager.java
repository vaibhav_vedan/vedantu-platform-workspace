package com.vedantu.scheduling.Interfaces;

import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;

import java.util.List;

public interface ITeacherProficiencyManager {
	public String assignTeacher(Integer grade, Long startTime, Long endTime,
			ELTeachersProficiencies elTeachersProficiencies);

	public ELTeachersProficiencies getTeacherProficiencies(SessionLabel earlyLearningCourseType)
			throws VException;

	public List<String> getAvailableTeacherForSlot(ProficiencyAnalyticsRequest request, ELTeachersProficiencies proficiencyIds) throws VException;

}
