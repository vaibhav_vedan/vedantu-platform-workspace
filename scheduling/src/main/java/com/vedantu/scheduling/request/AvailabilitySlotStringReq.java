package com.vedantu.scheduling.request;

import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class AvailabilitySlotStringReq extends AbstractFrontEndReq {
    private List<String> userIds;
    private Long startTime;
    private Long endTime;
    private Integer grade;
    private CalendarEntrySlotState slotState;
    private Long slotDuration = CalendarEntryManager.SLOT_LENGTH;
    private SessionLabel earlyLearningCourseType = SessionLabel.SUPER_CODER;

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<String>();

        if (this.startTime == null || this.startTime <= 0l) {
            errors.add(GetCalendarSlotBitReq.Constants.START_TIME);
        }
        if (this.endTime == null || this.endTime <= 0l) {
            errors.add(GetCalendarSlotBitReq.Constants.END_TIME);
        } else if (this.startTime != null && this.endTime <= this.startTime) {
            errors.add(GetCalendarSlotBitReq.Constants.END_TIME);
        }
        if (this.grade == null) {
            errors.add("grade");
        }
        if (!CalendarEntryManager.validateSlotDuration(this.slotDuration)) {
            errors.add(GetCalendarSlotBitReq.Constants.SLOT_DURATION);
        }
        return errors;
    }

    public static class Constants {
        public static final String USER_IDS = "userIds";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String SLOT_STATES = "slotState";
        public static final String SLOT_DURATION = "slotDuration";
    }
}
