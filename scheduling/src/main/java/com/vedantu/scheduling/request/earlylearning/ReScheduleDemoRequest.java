package com.vedantu.scheduling.request.earlylearning;


import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReScheduleDemoRequest extends AbstractFrontEndReq {

    private String demoId;
    private Long startTime;
    private Long endTime;
    private SessionLabel earlyLearningCourse;
    private Integer grade;

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(demoId)){
            errors.add("demoId Id is empty");
        }
        if(Objects.isNull(startTime)){
            errors.add("Start Time is null");
        }
        if(Objects.isNull(endTime)){
            errors.add("End Time is null");
        }
        if(Objects.isNull(earlyLearningCourse)) {
            errors.add("earlyLearningCourse is null");
        }
        if(Objects.isNull(grade)) {
            errors.add("grade is null");
        }
        if (earlyLearningCourse != null && earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            errors.add("early learning course type not valid");
        }
        return errors;
    }
}
