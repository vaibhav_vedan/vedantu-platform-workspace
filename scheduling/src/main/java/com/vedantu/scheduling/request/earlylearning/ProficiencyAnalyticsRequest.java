package com.vedantu.scheduling.request.earlylearning;

import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.enums.earlyLeaning.AnalyticsType;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.managers.earlylearning.AbstractEarlyLearning;
import com.vedantu.scheduling.request.GetCalendarSlotBitReq;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProficiencyAnalyticsRequest extends AbstractFrontEndReq {

    private AnalyticsType analyticsType;
    private Long startTime;
    private Long endTime;
    private String selectedTime;
    private ProficiencyType selectedProficiencyType;
    private SessionLabel earlyLearningCourseType;
    private String studentId;
    private String presenter;
    private int start = 0;
    private int limit = 20;

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<String>();

        if(analyticsType == null) {
            errors.add("AnalyticsType");
        }
        if (this.selectedTime == null) {
            errors.add("Selected Time");
        }
        if (this.earlyLearningCourseType == null) {
            errors.add("earlyLearningCourseType");
        }
        return errors;
    }
}
