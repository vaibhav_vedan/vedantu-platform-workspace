package com.vedantu.scheduling.request;

import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import lombok.*;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class ReScheduleEarlyLearningBookingReq extends AbstractFrontEndReq {

    private String studentId;
    private String bookingId;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String remark;
    private Long slotStartTime;
    private Long slotEndTime;
    private SessionState state = SessionState.SCHEDULED;
    private OTMSessionType otmSessionType = OTMSessionType.OTO_NURSERY;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE;
    private SessionLabel earlyLearningCourse = SessionLabel.SUPER_CODER;
    private String grade;

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(studentId)){
            errors.add("Student Id is empty");
        }
        if(StringUtils.isEmpty(bookingId)){
            errors.add("Booking Id is empty");
        }
        if (StringUtils.isEmpty(remark)) {
            errors.add("remark is null");
        }
        if(Objects.isNull(slotStartTime)){
            errors.add("Slot Start Time is null");
        }
        if(Objects.isNull(slotEndTime)){
            errors.add("Slot End Time is null");
        }

        if (earlyLearningCourse != null && earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            errors.add(earlyLearningCourse + " earlyLearningCourseType not supported");
        }

        if (otmSessionType != null && otmSessionType.isUnsupported()) {
            errors.add("deprecated otmSessionType " + otmSessionType);
        }

        if (sessionToolType != null && sessionToolType.isUnsupported()) {
            errors.add("deprecated sessionToolType " + sessionToolType);
        }
        return errors;
    }
}