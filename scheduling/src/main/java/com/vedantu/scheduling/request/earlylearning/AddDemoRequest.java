package com.vedantu.scheduling.request.earlylearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class AddDemoRequest extends AbstractFrontEndReq {

	private Long studentId;
	private Long startTime;
	private Long endTime;
	private Integer grade;
	private SessionLabel earlyLearningCourse;

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = new ArrayList<>();

		if (endTime - startTime > DateTimeUtils.MILLIS_PER_HOUR) {
			errors.add("Slot duration is more than one hour.");
		}

		if (Objects.isNull(earlyLearningCourse)) {
			errors.add("earlyLearningCourse is null");
		}

		if (Objects.isNull(grade)) {
			errors.add("grade is null");
		}
		if (earlyLearningCourse != null && earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
			errors.add("early learning course type not valid");
		}

		return errors;
	}
}
