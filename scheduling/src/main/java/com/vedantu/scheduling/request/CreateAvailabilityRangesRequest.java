package com.vedantu.scheduling.request;

import java.util.List;

import com.vedantu.scheduling.dao.entity.AvailabilityRange;


public class CreateAvailabilityRangesRequest {

	private List<AvailabilityRange> availabilityRanges;

	public CreateAvailabilityRangesRequest(List<AvailabilityRange> availabilityRanges) {
		super();
		this.availabilityRanges = availabilityRanges;
	}
	public CreateAvailabilityRangesRequest() {
		super();
	}

	public List<AvailabilityRange> getAvailabilityRanges() {
		return availabilityRanges;
	}

	public void setAvailabilityRanges(List<AvailabilityRange> availabilityRanges) {
		this.availabilityRanges = availabilityRanges;
	}

	
}
