package com.vedantu.scheduling.request;

import org.springframework.util.StringUtils;

import com.vedantu.scheduling.pojo.BitSetUtils;

public class UpdateCalendarSlotStringReq extends UpdateCalendarSlotBitsReq {
	private String oldBitSetString;
	private String newBitSetString;

	public UpdateCalendarSlotStringReq() {
		super();
	}

	public UpdateCalendarSlotStringReq(UpdateCalendarSlotBitsReq req) {
		super();
		setOldBitSetLongArray();
		setNewBitSetLongArray();
	}

	public String getOldBitSetString() {
		return oldBitSetString;
	}

	public void setOldBitSetString(String oldBitSetString) {
		this.oldBitSetString = oldBitSetString;
	}

	public String getNewBitSetString() {
		return newBitSetString;
	}

	public void setNewBitSetString(String newBitSetString) {
		this.newBitSetString = newBitSetString;
	}

	public void setOldBitSetLongArray() {
		if (StringUtils.isEmpty(this.oldBitSetString)) {
			this.oldBitSetString = "";
		}
		setOldBitSetLongArray(BitSetUtils.createBitSet(oldBitSetString).toLongArray());
	}

	public void setNewBitSetLongArray() {
		if (StringUtils.isEmpty(this.newBitSetString)) {
			this.newBitSetString = "";
		}
		setNewBitSetLongArray(BitSetUtils.createBitSet(newBitSetString).toLongArray());
	}

	public void resetOldBitSetLongArray() {
		setOldBitSetLongArray(new long[0]);
	}

	public void resetNewBitSetLongArray() {
		setNewBitSetLongArray(new long[0]);
	}

	public void resetNewBitSetString() {
		this.newBitSetString = "";
	}

	public void resetOldBitSetString() {
		this.oldBitSetString = "";
	}
}
