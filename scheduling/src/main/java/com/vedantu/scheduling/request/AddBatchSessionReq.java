package com.vedantu.scheduling.request;

import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.onetofew.enums.EntityTag;
import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.util.ArrayUtils;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.OTFSession;

public class AddBatchSessionReq {
	private String callingUserId;
	private OTFSession otfSession;
	private String reason;

	public AddBatchSessionReq() {
		super();
	}

	public String getCallingUserId() {
		return callingUserId;
	}

	public void setCallingUserId(String callingUserId) {
		this.callingUserId = callingUserId;
	}

	public OTFSession getSession() {
		return otfSession;
	}

	public void setSession(OTFSession otfSession) {
		this.otfSession = otfSession;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<String> validate() {
		List<String> errors = new ArrayList<String>();
		if (otfSession == null) {
			errors.add("session");
		} else {
			if (otfSession.getSessionToolType() == null) {
				errors.add("session.sessionToolType");
			}
			if (ArrayUtils.isEmpty(otfSession.getBatchIds())) {
				errors.add("batchids is empty");
			}
		}

		if (StringUtils.isEmpty(this.reason)) {
			errors.add("session");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "AddBatchSessionReq [callingUserId=" + callingUserId + ", session=" + otfSession + ", reason=" + reason
				+ "]";
	}
}
