package com.vedantu.scheduling.request;

import java.util.List;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.scheduling.dao.entity.DemoSession;

public class DemoSessionReq extends DemoSession {
	private Long startTime;
	private Long endTime;
	private String demoSessionId;

	public DemoSessionReq() {
		super();
	}

	public DemoSessionReq(String courseId, String batchId, String teacherId, String sessionId,
			List<String> enrolledStudents, EntityStatus status) {
		super(courseId, batchId, teacherId, sessionId, enrolledStudents, status);
	}

	public DemoSessionReq(Long startTime, Long endTime, String demoSessionId) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.demoSessionId = demoSessionId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getDemoSessionId() {
		return demoSessionId;
	}

	public void setDemoSessionId(String demoSessionId) {
		this.demoSessionId = demoSessionId;
	}

	@Override
	public String toString() {
		return "DemoSessionReq [startTime=" + startTime + ", endTime=" + endTime + ", demoSessionId=" + demoSessionId
				+ "]";
	}

	public DemoSession toDemoSession() {
		DemoSession demoSession = null;
		demoSession = new DemoSession(super.getCourseId(), super.getBatchId(), super.getTeacherId(),
				super.getSessionId(), null, super.getStatus());
		demoSession.setId(demoSessionId);
		return demoSession;
	}
}
