package com.vedantu.scheduling.request;

import com.sun.istack.NotNull;
import com.vedantu.scheduling.enums.AttendanceFeedbackTag;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AbsentSessionFeedbackReq extends AbstractFrontEndReq {

    List<String> gttIds;
    @NotNull String feedbackText;
    AttendanceFeedbackTag attendanceFeedbackTag;

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (CollectionUtils.isEmpty(gttIds)) {
            errors.add("List of attendees is empty");
        } else if (gttIds.size() > 200) {
            errors.add("number of attendees is large");
        }

        if (StringUtils.isEmpty(feedbackText)) {
            errors.add("Feedback text is empty");
        } else if (feedbackText.length() > 160) {
            errors.add("Feedback text should be of length at most 160 characters");
        }

        return errors;
    }
}

