package com.vedantu.scheduling.request;

import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;

public class CalendarEntryReq {
	private String userId;
	private Long startTime;
	private Long endTime;
	private CalendarEntrySlotState state;
	private CalendarReferenceType referenceType;
	private String referenceId;

	private Long firstDayStart;
	private Long finalDayEnd;

	public CalendarEntryReq() {
		super();
	}

	public CalendarEntryReq(String userId, Long startTime, Long endTime, CalendarEntrySlotState state,
			CalendarReferenceType calendarReferenceType, String referenceId) {
		super();
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.state = state;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getState() {
		return state;
	}

	public void setState(CalendarEntrySlotState state) {
		this.state = state;
	}

	public CalendarReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(CalendarReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Long getFirstDayStart() {
		return firstDayStart;
	}

	public void setFirstDayStart(Long firstDayStart) {
		this.firstDayStart = firstDayStart;
	}

	public Long getFinalDayEnd() {
		return finalDayEnd;
	}

	public void setFinalDayEnd(Long finalDayEnd) {
		this.finalDayEnd = finalDayEnd;
	}

	@Override
	public String toString() {
		return "CalendarEntryReq [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime + ", state="
				+ state + ", referenceType=" + referenceType + ", referenceId=" + referenceId + ", toString()="
				+ super.toString() + "]";
	}
}
