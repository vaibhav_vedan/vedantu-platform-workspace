package com.vedantu.scheduling.request.wavesession;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class SessionParametersChangeReq extends AbstractFrontEndReq {
    private Long startTime;
    private Long endTime;
    private String sessionId;
    private String otmSessionType;
    private String progressState;
    private String vimeoId;
    private String agoraVimeoId;

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    
    public Long getStartTime() {
        return this.startTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getEndTime() {
        return this.endTime;
    }

    public void setSessionId(String sessionId){
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return this.sessionId;
    }
    
    public void setOtmSessionType(String otmSessionType) {
        this.otmSessionType = otmSessionType;
    }

    public String getOtmSessionType() {
        return this.otmSessionType;
    }

    public void setProgressState(String progressState) {
        this.progressState = progressState;
    }

    public String getProgressState() {
        return this.progressState;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public String getVimeoId() {
        return this.vimeoId;
    }

    public void setAgoraVimeoId(String agoraVimeoId) {
        this.agoraVimeoId = agoraVimeoId;
    }
    
    public String getAgoraVimeoId() {
        return this.agoraVimeoId;
    }
    @Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (startTime == null) {
			errors.add("No startTime");
		}
		
		if (endTime == null) {
			errors.add("no endTime");
		}
		return errors;
	}
}