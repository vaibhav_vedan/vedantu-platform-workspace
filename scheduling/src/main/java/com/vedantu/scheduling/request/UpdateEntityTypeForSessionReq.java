package com.vedantu.scheduling.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class UpdateEntityTypeForSessionReq extends AbstractFrontEndReq {
    String sessionId;
    com.vedantu.onetofew.enums.EntityType type;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (Objects.isNull(sessionId)) {
            errors.add("sessionId is null/emptuy");
        }
        return errors;
    }
}
