package com.vedantu.scheduling.request;

import com.vedantu.scheduling.dao.entity.OTFSession;
import lombok.Data;

import java.util.List;

@Data
public class MicroCourseSMSReq {
    List<OTFSession> sessionDataList;
}
