package com.vedantu.scheduling.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.enums.OTMSessionInProgressState;
import com.vedantu.scheduling.pojo.OTMSessionConfig;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
public class UpdateOTFSessionReq extends AbstractFrontEndReq  {
    private String id;
    private Set<OTFSessionFlag> flags;
    private Integer pollCount;
    private Integer quizCount;
    private Integer hotspotCount;
    private Integer noticeCount;
    private Integer chatCount;
    private Integer uniqueStudentAttendants;
    private Integer totalDoubtsAsked;
    private Integer totalDoubtsResolved;
    private Integer sessionDuration;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    List<SessionPagesMetadata> pagesMetaData;
    private OTMSessionConfig otmSessionConfig;
    private OTMSessionInProgressState progressState;
    private Long startedAt;
    private Long recordingStartTime;
    private Long recordedTill;
    private String vimeoId;
    private String agoraVimeoId;
    private Long endedAt;
    private Long endedBy;
    private Long agoraStartTime;
    private Integer averageStudentDuration;//millis
    private String primaryBatchId;
}
