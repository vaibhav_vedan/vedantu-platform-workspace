package com.vedantu.scheduling.request;

import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class ChangeServiceProviderReq extends AbstractFrontEndReq {
    private List<String> sessionIds;

    private OTMServiceProvider serviceProvider;

    public List<String> getSessionIds() {
        return sessionIds;
    }

    public void setSessionIds(List<String> sessionIds) {
        this.sessionIds = sessionIds;
    }

    public OTMServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(OTMServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(serviceProvider.name())) {
            errors.add("serviceProvider");
        }
        if(ArrayUtils.isEmpty(sessionIds)){
            errors.add("sessionIds");
        }
        return errors;
    }
}