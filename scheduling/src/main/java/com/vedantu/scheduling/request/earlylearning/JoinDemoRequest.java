package com.vedantu.scheduling.request.earlylearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class JoinDemoRequest extends AbstractFrontEndReq {

	private String demoId;
	private Long studentId;
	private Integer grade;
	private SessionLabel earlyLearningCourse;
	
	
	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = new ArrayList<>();
		
		//TODO: Add demo Id validation logic here
		if(StringUtils.isEmpty(demoId)) {
			errors.add("demoId");
		}
		if(Objects.isNull(grade)) {
			errors.add("grade");
		}
		if(Objects.isNull(earlyLearningCourse)) {
			errors.add("earlyLearningCourse");
		}
		if (earlyLearningCourse != null && earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
			errors.add("early learning course type not valid");
		}
		return errors;
		
	}

}
