/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author parashar
 */
public class AddAdminTechSupportToSession extends AbstractFrontEndReq{
    
    private String adminJoined;
    private String techSupport;
    private String finalIssueStatus;
    private String sessionId;

    /**
     * @return the adminJoined
     */
    public String getAdminJoined() {
        return adminJoined;
    }

    /**
     * @param adminJoined the adminJoined to set
     */
    public void setAdminJoined(String adminJoined) {
        this.adminJoined = adminJoined;
    }

    /**
     * @return the techSupport
     */
    public String getTechSupport() {
        return techSupport;
    }

    /**
     * @param techSupport the techSupport to set
     */
    public void setTechSupport(String techSupport) {
        this.techSupport = techSupport;
    }

    /**
     * @return the finalIssueStatus
     */
    public String getFinalIssueStatus() {
        return finalIssueStatus;
    }

    /**
     * @param finalIssueStatus the finalIssueStatus to set
     */
    public void setFinalIssueStatus(String finalIssueStatus) {
        this.finalIssueStatus = finalIssueStatus;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
    
}
