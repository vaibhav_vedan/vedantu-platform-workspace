package com.vedantu.scheduling.request.wavesession;

import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.pojo.OTMSessionConfig;
import com.vedantu.scheduling.pojo.Pollsdata;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OTFSessionRequest extends AbstractFrontEndReq {
    private String sessionId;
    private OTFSessionContextType sessionContextType;
    private String sessionContextId;
    private Set<String> batchIds;
    private String title;
    private Long boardId;// subject
    private String description;
    private String presenter;
    private Set<Long> taIds;
    private Long startTime;
    private Long startedAt;
    private Long endTime;
    private String sessionURL;
    private String meetingId;
    private List<String> replayUrls;
    private OTMSessionType otmSessionType;
    private SessionState state;
    private OTMSessionInProgressState progressState;
    private String rescheduledFromId;
    private EntityType type;
    private List<String> attendees;
    private String organizerAccessToken;
    private String presenterUrl;
    private String remarks;
    private Boolean teacherJoined;
    private Long teacherJoinTime;
    private OTFSessionToolType sessionToolType;
    private Set<String> flags = new HashSet<>();
    private List<RescheduleData> rescheduleData = new ArrayList<>();
    private String cancelledBy; // needs to be used to cancel sessions
    private String vimeoId;
    private String forfeitedBy;
    private List<ContentInfo> preSessionContents;
    private List<ContentInfo> postSessionContents;
    private String ownerId;
    private String ownerComment;
    private Pollsdata pollsdata;
    private String adminJoined;
    private String techSupport;
    private String finalIssueStatus;
    private String deletedBy;
    private OTMSessionConfig otmSessionConfig;
    private List<SessionPagesMetadata> pagesMetaData;
    private String nodeServerUrl;
    private String nodeServerPort;
    private CanvasStreamingType canvasStreamingType; // data streaming type
    private OTMServiceProvider serviceProvider = OTMServiceProvider.AGORA;
    private String socialVidConfResp;
    private Long recordingStartTime;
    private Long recordedTill;
    private Long endedBy;
    private Long endedAt;
    private List<String> curricullumTopics;
    private List<String> baseTreeTopics;
    private Integer pollCount;
    private Integer quizCount;
    private Integer hotspotCount;
    private Integer noticeCount;
    private Integer chatCount;
    private Integer uniqueStudentAttendants;
    private Integer totalDoubtsAsked;
    private Integer totalDoubtsResolved;
    private Integer averageStudentDuration;//millis
    private Long createdBy;
    private Long agoraStartTime;
    private String parentSessionId;
    private String agoraReplayUrl;

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    private Long creationTime;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public OTFSessionContextType getSessionContextType() {
        return sessionContextType;
    }

    public void setSessionContextType(OTFSessionContextType sessionContextType) {
        this.sessionContextType = sessionContextType;
    }

    public String getSessionContextId() {
        return sessionContextId;
    }

    public void setSessionContextId(String sessionContextId) {
        this.sessionContextId = sessionContextId;
    }

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPresenter() {
        return presenter;
    }

    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = taIds;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Long startedAt) {
        this.startedAt = startedAt;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getSessionURL() {
        return sessionURL;
    }

    public void setSessionURL(String sessionURL) {
        this.sessionURL = sessionURL;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public List<String> getReplayUrls() {
        return replayUrls;
    }

    public void setReplayUrls(List<String> replayUrls) {
        this.replayUrls = replayUrls;
    }

    public OTMSessionType getOtmSessionType() {
        return otmSessionType;
    }

    public void setOtmSessionType(OTMSessionType otmSessionType) {
        this.otmSessionType = otmSessionType;
    }

    public SessionState getState() {
        return state;
    }

    public void setState(SessionState state) {
        this.state = state;
    }

    public OTMSessionInProgressState getProgressState() {
        return progressState;
    }

    public void setProgressState(OTMSessionInProgressState progressState) {
        this.progressState = progressState;
    }

    public String getRescheduledFromId() {
        return rescheduledFromId;
    }

    public void setRescheduledFromId(String rescheduledFromId) {
        this.rescheduledFromId = rescheduledFromId;
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public List<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<String> attendees) {
        this.attendees = attendees;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public String getPresenterUrl() {
        return presenterUrl;
    }

    public void setPresenterUrl(String presenterUrl) {
        this.presenterUrl = presenterUrl;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean getTeacherJoined() {
        return teacherJoined;
    }

    public void setTeacherJoined(Boolean teacherJoined) {
        this.teacherJoined = teacherJoined;
    }

    public Long getTeacherJoinTime() {
        return teacherJoinTime;
    }

    public void setTeacherJoinTime(Long teacherJoinTime) {
        this.teacherJoinTime = teacherJoinTime;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public Set<String> getFlags() {
        return flags;
    }

    public void setFlags(Set<String> flags) {
        this.flags = flags;
    }

    public List<RescheduleData> getRescheduleData() {
        return rescheduleData;
    }

    public void setRescheduleData(List<RescheduleData> rescheduleData) {
        this.rescheduleData = rescheduleData;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public String getForfeitedBy() {
        return forfeitedBy;
    }

    public void setForfeitedBy(String forfeitedBy) {
        this.forfeitedBy = forfeitedBy;
    }

    public List<ContentInfo> getPreSessionContents() {
        return preSessionContents;
    }

    public void setPreSessionContents(List<ContentInfo> preSessionContents) {
        this.preSessionContents = preSessionContents;
    }

    public List<ContentInfo> getPostSessionContents() {
        return postSessionContents;
    }

    public void setPostSessionContents(List<ContentInfo> postSessionContents) {
        this.postSessionContents = postSessionContents;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerComment() {
        return ownerComment;
    }

    public void setOwnerComment(String ownerComment) {
        this.ownerComment = ownerComment;
    }

    public Pollsdata getPollsdata() {
        return pollsdata;
    }

    public void setPollsdata(Pollsdata pollsdata) {
        this.pollsdata = pollsdata;
    }

    public String getAdminJoined() {
        return adminJoined;
    }

    public void setAdminJoined(String adminJoined) {
        this.adminJoined = adminJoined;
    }

    public String getTechSupport() {
        return techSupport;
    }

    public void setTechSupport(String techSupport) {
        this.techSupport = techSupport;
    }

    public String getFinalIssueStatus() {
        return finalIssueStatus;
    }

    public void setFinalIssueStatus(String finalIssueStatus) {
        this.finalIssueStatus = finalIssueStatus;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OTMSessionConfig getOtmSessionConfig() {
        return otmSessionConfig;
    }

    public void setOtmSessionConfig(OTMSessionConfig otmSessionConfig) {
        this.otmSessionConfig = otmSessionConfig;
    }

    public List<SessionPagesMetadata> getPagesMetaData() {
        return pagesMetaData;
    }

    public void setPagesMetaData(List<SessionPagesMetadata> pagesMetaData) {
        this.pagesMetaData = pagesMetaData;
    }

    public String getNodeServerUrl() {
        return nodeServerUrl;
    }

    public void setNodeServerUrl(String nodeServerUrl) {
        this.nodeServerUrl = nodeServerUrl;
    }

    public String getNodeServerPort() {
        return nodeServerPort;
    }

    public void setNodeServerPort(String nodeServerPort) {
        this.nodeServerPort = nodeServerPort;
    }

    public CanvasStreamingType getCanvasStreamingType() {
        return canvasStreamingType;
    }

    public void setCanvasStreamingType(CanvasStreamingType canvasStreamingType) {
        this.canvasStreamingType = canvasStreamingType;
    }

    public Long getAgoraStartTime() {
        return agoraStartTime;
    }

    public void setAgoraStartTime(Long agoraStartTime) {
        this.agoraStartTime = agoraStartTime;
    }

    public String getParentSessionId() {
        return parentSessionId;
    }

    public void setParentSessionId(String parentSessionId) {
        this.parentSessionId = parentSessionId;
    }

    public String getAgoraReplayUrl() {
        return agoraReplayUrl;
    }

    public void setAgoraReplayUrl(String agoraReplayUrl) {
        this.agoraReplayUrl = agoraReplayUrl;
    }

    public OTMServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(OTMServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getSocialVidConfResp() {
        return socialVidConfResp;
    }

    public void setSocialVidConfResp(String socialVidConfResp) {
        this.socialVidConfResp = socialVidConfResp;
    }

    public Long getRecordingStartTime() {
        return recordingStartTime;
    }

    public void setRecordingStartTime(Long recordingStartTime) {
        this.recordingStartTime = recordingStartTime;
    }

    public Long getRecordedTill() {
        return recordedTill;
    }

    public void setRecordedTill(Long recordedTill) {
        this.recordedTill = recordedTill;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public Long getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Long endedAt) {
        this.endedAt = endedAt;
    }

    public List<String> getCurricullumTopics() {
        return curricullumTopics;
    }

    public void setCurricullumTopics(List<String> curricullumTopics) {
        this.curricullumTopics = curricullumTopics;
    }

    public List<String> getBaseTreeTopics() {
        return baseTreeTopics;
    }

    public void setBaseTreeTopics(List<String> baseTreeTopics) {
        this.baseTreeTopics = baseTreeTopics;
    }

    public Integer getPollCount() {
        return pollCount;
    }

    public void setPollCount(Integer pollCount) {
        this.pollCount = pollCount;
    }

    public Integer getQuizCount() {
        return quizCount;
    }

    public void setQuizCount(Integer quizCount) {
        this.quizCount = quizCount;
    }

    public Integer getHotspotCount() {
        return hotspotCount;
    }

    public void setHotspotCount(Integer hotspotCount) {
        this.hotspotCount = hotspotCount;
    }

    public Integer getNoticeCount() {
        return noticeCount;
    }

    public void setNoticeCount(Integer noticeCount) {
        this.noticeCount = noticeCount;
    }

    public Integer getChatCount() {
        return chatCount;
    }

    public void setChatCount(Integer chatCount) {
        this.chatCount = chatCount;
    }

    public Integer getUniqueStudentAttendants() {
        return uniqueStudentAttendants;
    }

    public void setUniqueStudentAttendants(Integer uniqueStudentAttendants) {
        this.uniqueStudentAttendants = uniqueStudentAttendants;
    }

    public Integer getTotalDoubtsAsked() {
        return totalDoubtsAsked;
    }

    public void setTotalDoubtsAsked(Integer totalDoubtsAsked) {
        this.totalDoubtsAsked = totalDoubtsAsked;
    }

    public Integer getTotalDoubtsResolved() {
        return totalDoubtsResolved;
    }

    public void setTotalDoubtsResolved(Integer totalDoubtsResolved) {
        this.totalDoubtsResolved = totalDoubtsResolved;
    }

    public Integer getAverageStudentDuration() {
        return averageStudentDuration;
    }

    public void setAverageStudentDuration(Integer averageStudentDuration) {
        this.averageStudentDuration = averageStudentDuration;
    }

    public Integer getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(Integer sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    private Integer sessionDuration;//millis

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }

        return errors;
    }
}
