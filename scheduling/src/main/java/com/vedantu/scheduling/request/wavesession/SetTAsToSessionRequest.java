/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.wavesession;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import java.util.Set;

/**
 *
 * @author aditya
 */
public class SetTAsToSessionRequest extends AbstractFrontEndReq {

    private Set<Long> taIds;
    private String sessionId;
    private Integer size;

    SetTAsToSessionRequest() {
        super();
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds){
        this.taIds = taIds;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId){
        this.sessionId = sessionId;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size){
        this.size = size;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (sessionId == null) {
            errors.add("No sessionId");
        }
        
        if (taIds == null) {
            errors.add("TaIds can't be null");
        }
        return errors;
    }
}
