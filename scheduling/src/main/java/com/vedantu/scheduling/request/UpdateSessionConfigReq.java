/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request;

import java.util.*;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.scheduling.pojo.wavesession.TeacherTaStudentListTabs;

/**
 *
 * @author ajith
 */
public class UpdateSessionConfigReq extends AbstractFrontEndReq {

    private String sessionId;
    private Boolean chatEnabled;
    private String chatMode;
    private String handRaiseMode;
    private Boolean showEmojis;
    private Boolean welcomeMessage;
    private Boolean doubtStream;
    private Boolean upvote;
    private Boolean showStudentsListTeacher;
    private Boolean showStudentsListTA;
    private List<TeacherTaStudentListTabs> studentListTabsTeacher;
    private List<TeacherTaStudentListTabs> studentListTabsTA;
    private Boolean leaderBoardStreak;
    private Boolean quizStreak;
    private Boolean sectionPhase3;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Boolean getChatEnabled() {
        return chatEnabled;
    }

    public void setChatEnabled(Boolean chatEnabled) {
        this.chatEnabled = chatEnabled;
    }

    public String getChatMode() {
        return chatMode;
    }

    public void setChatMode(String chatMode) {
        this.chatMode = chatMode;
    }

    public String getHandRaiseMode() {
        return handRaiseMode;
    }

    public void setHandRaiseMode(String handRaiseMode) {
        this.handRaiseMode = handRaiseMode;
    }
    
    public Boolean getShowEmojis() {
        return showEmojis;
    }

    public void setShowEmojis(Boolean showEmojis) {
        this.showEmojis = showEmojis;
    }

    public Boolean getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(Boolean welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public Boolean getDoubtStream() {
        return doubtStream;
    }

    public void setDoubtStream(Boolean doubtStream) {
        this.doubtStream = doubtStream;
    }

    public Boolean getUpvote() {
        return upvote;
    }

    public void setUpvote(Boolean upvote) {
        this.upvote = upvote;
    }

    public Boolean getShowStudentsListTeacher() {
        return showStudentsListTeacher;
    }

    public void setShowStudentsListTeacher(Boolean showStudentsListTeacher) {
        this.showStudentsListTeacher = showStudentsListTeacher;
    }

    public Boolean getShowStudentsListTA() {
        return showStudentsListTA;
    }

    public void setShowStudentsListTA(Boolean showStudentsListTA) {
        this.showStudentsListTA = showStudentsListTA;
    }

    public List<TeacherTaStudentListTabs> getStudentListTabsTeacher() {
        return studentListTabsTeacher;
    }

    public void setStudentListTabsTeacher(List<TeacherTaStudentListTabs> studentListTabsTeacher) {
        this.studentListTabsTeacher = studentListTabsTeacher;
    }

    public List<TeacherTaStudentListTabs> getStudentListTabsTA() {
        return studentListTabsTA;
    }

    public void setStudentListTabsTA(List<TeacherTaStudentListTabs> studentListTabsTA) {
        this.studentListTabsTA = studentListTabsTA;
    }
    
    public Boolean getLeaderBoardStreak() {
        return leaderBoardStreak;
    }

    public void setLeaderBoardStreak(Boolean leaderBoardStreak) {
        this.leaderBoardStreak = leaderBoardStreak;
    }

    public Boolean getQuizStreak() {
        return quizStreak;
    }

    public void setQuizStreak(Boolean quizStreak) {
        this.quizStreak = quizStreak;
    }

    public Boolean getSectionPhase3() {
        return sectionPhase3;
    }

    public void setSectionPhase3(Boolean sectionPhase3) {
        this.sectionPhase3 = sectionPhase3;
    }
}
