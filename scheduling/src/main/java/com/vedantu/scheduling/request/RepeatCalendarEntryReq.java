package com.vedantu.scheduling.request;

import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;

public class RepeatCalendarEntryReq {

	private String userId;
	private Long startTime;
	private Long endTime;
	private CalendarEntrySlotState state;
	private Integer count;

	public RepeatCalendarEntryReq() {
		super();
	}

	public RepeatCalendarEntryReq(String userId, Long startTime, Long endTime, CalendarEntrySlotState state, Integer count) {
		super();
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.state = state;
		this.count = count;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getState() {
		return state;
	}

	public void setState(CalendarEntrySlotState state) {
		this.state = state;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "RepeatCalendarEntryReq [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", state=" + state + ", count=" + count + "]";
	}
}
