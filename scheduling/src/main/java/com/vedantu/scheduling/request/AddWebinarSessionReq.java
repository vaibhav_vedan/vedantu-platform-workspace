/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request;

import com.vedantu.onetofew.enums.EntityTag;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
@Getter
@Setter
public class AddWebinarSessionReq extends AbstractFrontEndReq {

    private String sessionId;
    private String webinarId;
    private Long presenter;
    private Set<Long> taIds;
    private String title;
    private Long startTime;
    private Long endTime;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE;
    private Boolean isSimLive;
    private String parentWebinarSessionId;
    private Set<EntityTag> entityTags;
    private Set<SessionLabel> labels;
    private EntityType type;

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public Long getPresenter() {
        return presenter;
    }

    public void setPresenter(Long presenter) {
        this.presenter = presenter;
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = taIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public Boolean getIsSimLive() {
        return isSimLive;
    }

    public void setIsSimLive(Boolean isSimLive) {
        this.isSimLive = isSimLive;
    }

    public String getParentWebinarSessionId() {
        return parentWebinarSessionId;
    }

    public void setParentWebinarSessionId(String parentWebinarSessionId) {
        this.parentWebinarSessionId = parentWebinarSessionId;
    }


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(webinarId)) {
            errors.add("webinarId");
        }
        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (startTime == null) {
            errors.add("startTime");
        }
        if (endTime == null) {
            errors.add("endTime");
        }
        if (presenter == null) {
            errors.add("presenter");
        }
        if (isSimLive != null && isSimLive && StringUtils.isEmpty(parentWebinarSessionId)) {
            errors.add("parentWebinarSessionId");
        }
        if (sessionToolType != null && sessionToolType.isUnsupported()) {
            errors.add("deprecated sessionToolType: " + sessionToolType);
        }
        if (type != null && type.isUnsupported()) {
            errors.add("deprecated type: " + type);
        }

        return errors;
    }

}
