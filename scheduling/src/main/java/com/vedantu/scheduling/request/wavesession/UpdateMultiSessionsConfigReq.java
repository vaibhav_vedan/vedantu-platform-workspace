package com.vedantu.scheduling.request.wavesession;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import com.vedantu.scheduling.request.UpdateSessionConfigReq;

public class UpdateMultiSessionsConfigReq extends AbstractFrontEndReq {
    private List<String> sessionIds;
    private UpdateSessionConfigReq otmSessionConfig;

    public UpdateSessionConfigReq getOtmSessionConfig() {
        return this.otmSessionConfig;
    }

    public void setOtmSessionConfig(UpdateSessionConfigReq otmSessionConfig) {
        this.otmSessionConfig = otmSessionConfig;
    }

    public List<String> getSessionIds() {
        return this.sessionIds;
    }

    public void setSessionIds(List<String> sessionIds) {
        this.sessionIds = sessionIds;
    }
    @Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (ArrayUtils.isEmpty(sessionIds)) {
			errors.add("No sessionIds");
		}
		
		if (otmSessionConfig == null) {
			errors.add("no otmSessionConfig provided");
		}
		return errors;
	}
}