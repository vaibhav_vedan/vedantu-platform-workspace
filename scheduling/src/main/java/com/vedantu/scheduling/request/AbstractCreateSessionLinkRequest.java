package com.vedantu.scheduling.request;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.vedantu.util.DateTimeUtils;

public class AbstractCreateSessionLinkRequest {
	private String subject;
	private String starttime;
	private String endtime;
	private String timezonekey;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public AbstractCreateSessionLinkRequest(String subject, long startTime, long endTime, String timezonekey) {
		super();
		this.subject = subject;
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
		this.starttime = sdf.format(new Date(startTime)) + "Z";
		this.endtime = sdf.format(new Date(endTime)) + "Z";
		this.timezonekey = timezonekey;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public String getTimezonekey() {
		return timezonekey;
	}

	public void setTimezonekey(String timezonekey) {
		this.timezonekey = timezonekey;
	}

	@Override
	public String toString() {
		return "AbstractCreateSessionLinkRequest [subject=" + subject + ", starttime=" + starttime + ", endtime="
				+ endtime + ", timezonekey=" + timezonekey + ", toString()=" + super.toString() + "]";
	}
}
