package com.vedantu.scheduling.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMGetBoardNameReq extends AbstractFrontEndReq {

    List<Long> boardId;

}
