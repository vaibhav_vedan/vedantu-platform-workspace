package com.vedantu.scheduling.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.BitSetUtils;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;

public class UpdateCalendarSlotBitsReq {
	private String userId;
	private Long startTime;
	private Long endTime;
	private CalendarEntrySlotState slotState;
	private Long slotDuration = CalendarEntryManager.SLOT_LENGTH;
	private long[] oldBitSetLongArray;
	private long[] newBitSetLongArray;

	public UpdateCalendarSlotBitsReq() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	public Long getSlotDuration() {
		return slotDuration;
	}

	public void setSlotDuration(Long slotDuration) {
		this.slotDuration = slotDuration;
	}

	public long[] getOldBitSetLongArray() {
		return oldBitSetLongArray;
	}

	public void setOldBitSetLongArray(long[] oldBitSetLongArray) {
		this.oldBitSetLongArray = oldBitSetLongArray;
	}

	public long[] getNewBitSetLongArray() {
		return newBitSetLongArray;
	}

	public void setNewBitSetLongArray(long[] newBitSetLongArray) {
		this.newBitSetLongArray = newBitSetLongArray;
	}

	public int getBitSetSize() {
		return BitSetUtils.bitSetSize(getStartTime(), getEndTime(), getSlotDuration());
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (StringUtils.isEmpty(this.userId)) {
			errors.add(Constants.USER_ID);
		}
		if (this.startTime == null || this.startTime <= 0l) {
			errors.add(Constants.START_TIME);
		}
		if (this.endTime == null || this.endTime <= 0l) {
			errors.add(Constants.END_TIME);
		}
		if (this.oldBitSetLongArray == null) {
			errors.add(Constants.OLD_BITSET);
		}
		if (this.newBitSetLongArray == null) {
			errors.add(Constants.NEW_BITSET);
		}

		return errors;
	}

	public static class Constants {
		public static final String USER_ID = "userId";
		public static final String START_TIME = "startTime";
		public static final String END_TIME = "endTime";
		public static final String SLOT_DURATION = "slotDuration";
		public static final String OLD_BITSET = "oldBitSetLongArray";
		public static final String NEW_BITSET = "newBitSetLongArray";
	}

	@Override
	public String toString() {
		return "UpdateCalendarSlotBitsReq [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", slotDuration=" + slotDuration + ", oldBitSetLongArray=" + Arrays.toString(oldBitSetLongArray)
				+ ", newBitSetLongArray=" + Arrays.toString(newBitSetLongArray) + "]" + ", GrepCode : " + userId + "_"
				+ startTime + "_" + this.getClass().getSimpleName();
	}
}
