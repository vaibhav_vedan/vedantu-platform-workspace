package com.vedantu.scheduling.request.wavesession;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

public class CreateTownhallSessionReq extends AbstractFrontEndReq {
    private Long startTime;
    private Long endTime;
    private List<Long> taIds;
    private Long presenterId;

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public List<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(List<Long> taIds) {
        this.taIds = taIds;
    }

    public Long getPresenterId() {
        return presenterId;
    }

    public void setPresenterId(Long presenterId) {
        this.presenterId = presenterId;
    }
}
