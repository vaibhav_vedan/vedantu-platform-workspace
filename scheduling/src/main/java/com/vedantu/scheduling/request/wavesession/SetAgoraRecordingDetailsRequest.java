/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.wavesession;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class SetAgoraRecordingDetailsRequest extends AbstractFrontEndReq {

    private String agoraVimeoId;
    private Long recordingStartTime;
    private Long recordedTill;

    public SetAgoraRecordingDetailsRequest() {
        super();
    }

    public SetAgoraRecordingDetailsRequest(String agoraVimeoId) {
        super();
        this.agoraVimeoId = agoraVimeoId;
    }

    public String getAgoraVimeoId() {
        return agoraVimeoId;
    }

    public void setAgoraVimeoId(String agoraVimeoId) {
        this.agoraVimeoId = agoraVimeoId;
    }

    public Long getRecordingStartTime() {
        return recordingStartTime;
    }

    public void setRecordingStartTime(Long recordingStartTime) {
        this.recordingStartTime = recordingStartTime;
    }

    public Long getRecordedTill() {
        return recordedTill;
    }

    public void setRecordedTill(Long recordedTill) {
        this.recordedTill = recordedTill;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == agoraVimeoId) {
            errors.add("agoraVimeoId");
        }

        return errors;
    }
}
