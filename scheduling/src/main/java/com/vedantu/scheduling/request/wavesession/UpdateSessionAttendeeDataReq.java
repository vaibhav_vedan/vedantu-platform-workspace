/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.wavesession;

import com.vedantu.scheduling.pojo.wavesession.DoubtData;
import com.vedantu.scheduling.pojo.wavesession.InteractionData;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class UpdateSessionAttendeeDataReq extends AbstractFrontEndReq {

    private String sessionId;
    private String userId;

    private Long studentSessionJoinTime;
    private Long studentLastDisconnectTime;
    private Integer totalDisconnections;
    private Integer studentChatCount;
    private Integer thumbsUpCount;
    private Integer thumbsDownCount;
    private Float averageResponseTime;
    private List<Long> connectTimes;//connectiontimes
    private List<Long> disconnectTimes;//diconnectiontimes
    private List<InteractionData> interactionDatas;
    private List<DoubtData> doubtDatas;
    private Integer lbRank;
    private Float lbPercentile;
    private Integer lbPoints;
    private Integer studentOnlineDuration;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getStudentSessionJoinTime() {
        return studentSessionJoinTime;
    }

    public void setStudentSessionJoinTime(Long studentSessionJoinTime) {
        this.studentSessionJoinTime = studentSessionJoinTime;
    }

    public Long getStudentLastDisconnectTime() {
        return studentLastDisconnectTime;
    }

    public void setStudentLastDisconnectTime(Long studentLastDisconnectTime) {
        this.studentLastDisconnectTime = studentLastDisconnectTime;
    }

    public Integer getTotalDisconnections() {
        return totalDisconnections;
    }

    public void setTotalDisconnections(Integer totalDisconnections) {
        this.totalDisconnections = totalDisconnections;
    }

    public Integer getStudentChatCount() {
        return studentChatCount;
    }

    public void setStudentChatCount(Integer studentChatCount) {
        this.studentChatCount = studentChatCount;
    }

    public Integer getThumbsUpCount() {
        return thumbsUpCount;
    }

    public void setThumbsUpCount(Integer thumbsUpCount) {
        this.thumbsUpCount = thumbsUpCount;
    }

    public Integer getThumbsDownCount() {
        return thumbsDownCount;
    }

    public void setThumbsDownCount(Integer thumbsDownCount) {
        this.thumbsDownCount = thumbsDownCount;
    }

    public Float getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(Float averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public List<Long> getConnectTimes() {
        return connectTimes;
    }

    public void setConnectTimes(List<Long> connectTimes) {
        this.connectTimes = connectTimes;
    }

    public List<Long> getDisconnectTimes() {
        return disconnectTimes;
    }

    public void setDisconnectTimes(List<Long> disconnectTimes) {
        this.disconnectTimes = disconnectTimes;
    }

    public List<InteractionData> getInteractionDatas() {
        return interactionDatas;
    }

    public void setInteractionDatas(List<InteractionData> interactionDatas) {
        this.interactionDatas = interactionDatas;
    }

    public List<DoubtData> getDoubtDatas() {
        return doubtDatas;
    }

    public void setDoubtDatas(List<DoubtData> doubtDatas) {
        this.doubtDatas = doubtDatas;
    }

    public Integer getLbRank() {
        return lbRank;
    }

    public void setLbRank(Integer lbRank) {
        this.lbRank = lbRank;
    }

    public Float getLbPercentile() {
        return lbPercentile;
    }

    public void setLbPercentile(Float lbPercentile) {
        this.lbPercentile = lbPercentile;
    }

    public Integer getLbPoints() {
        return lbPoints;
    }

    public void setLbPoints(Integer lbPoints) {
        this.lbPoints = lbPoints;
    }

    public Integer getStudentOnlineDuration() {
        return studentOnlineDuration;
    }

    public void setStudentOnlineDuration(Integer studentOnlineDuration) {
        this.studentOnlineDuration = studentOnlineDuration;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }
        if (StringUtils.isEmpty(userId)) {
            errors.add("userId");
        }
        return errors;
    }

}
