/**
 * 
 */
package com.vedantu.scheduling.request;

import java.util.List;

import com.vedantu.User.Role;
import com.vedantu.app.enums.TimeFrame;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OTFSessionReqApp {
	
	private Integer start;
	
	private Integer size;
	
	private List<String> batchIds;
	
	private String callingUserId;
	
	private Role callingUserRole;
	
	private TimeFrame timeFrame;

}
