package com.vedantu.scheduling.request;

public class GetCalendarEventReq {
	private Long creationStartTime;
	private Long creationEndTime;
	private String eventStates;
	private String userIds;

	public GetCalendarEventReq() {
		super();
	}

	public Long getCreationStartTime() {
		return creationStartTime;
	}

	public void setCreationStartTime(Long creationStartTime) {
		this.creationStartTime = creationStartTime;
	}

	public Long getCreationEndTime() {
		return creationEndTime;
	}

	public void setCreationEndTime(Long creationEndTime) {
		this.creationEndTime = creationEndTime;
	}

	public String getEventStates() {
		return eventStates;
	}

	public void setEventStates(String eventStates) {
		this.eventStates = eventStates;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	@Override
	public String toString() {
		return "GetCalendarEventReq [creationStartTime=" + creationStartTime + ", creationEndTime=" + creationEndTime
				+ ", eventStates=" + eventStates + ", userIds=" + userIds + "]";
	}
}
