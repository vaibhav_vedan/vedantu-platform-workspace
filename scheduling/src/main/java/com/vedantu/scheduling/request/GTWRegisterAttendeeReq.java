package com.vedantu.scheduling.request;

import org.springframework.util.StringUtils;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.scheduling.managers.GTTAttendeeDetailsManager;

public class GTWRegisterAttendeeReq {
	private String email;
	private String firstName;
	private String lastName;

	public GTWRegisterAttendeeReq() {
		super();
	}

	public GTWRegisterAttendeeReq(UserBasicInfo user) {
		super();
		this.email = GTTAttendeeDetailsManager.generateEmail(user.getUserId());
		this.firstName = StringUtils.isEmpty(user.getFirstName()) ? String.valueOf(user.getUserId())
				: user.getFirstName();
		this.lastName = StringUtils.isEmpty(user.getLastName()) ? "STUDENT" : user.getLastName();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "GTTRegisterAttendeeReq [email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", toString()=" + super.toString() + "]";
	}
}
