package com.vedantu.scheduling.request;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetOverBookingListReq extends AbstractFrontEndListReq {

    private Long studentId;
    private Long fromTime;
    private Long tillTime;
    private SessionLabel earlyLearningCourseType;
    private List<SessionState> states;
    private List <OTMSessionType> otmSessionType;
    private List <OTFSessionToolType> sessionToolType;
    private SortOrder sortOrder;
    private OverBookingStatus isTeacherAssigned;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        Set<OTMSessionType> unsupportedTypes = Optional.ofNullable(otmSessionType).orElseGet(ArrayList::new)
                .stream().filter(OTMSessionType::isUnsupported)
                .collect(Collectors.toSet());
        if (!unsupportedTypes.isEmpty()) {
            errors.add("deprecated otm session types " + unsupportedTypes);
        }
        Set<OTFSessionToolType> unsupportedToolTypes = Optional.ofNullable(sessionToolType).orElseGet(ArrayList::new)
                .stream().filter(OTFSessionToolType::isUnsupported)
                .collect(Collectors.toSet());
        if (!unsupportedToolTypes.isEmpty()) {
            errors.add("deprecated session tool types " + unsupportedToolTypes);
        }

        if (earlyLearningCourseType != null && earlyLearningCourseType != SessionLabel.SUPER_CODER && earlyLearningCourseType != SessionLabel.SUPER_READER) {
            errors.add(earlyLearningCourseType + " earlyLearningCourseType not supported");
        }
        return errors;
    }
}