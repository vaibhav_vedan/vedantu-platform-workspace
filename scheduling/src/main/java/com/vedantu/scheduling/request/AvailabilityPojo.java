package com.vedantu.scheduling.request;

import java.util.List;

public class AvailabilityPojo {

	Long startTime;
	Long endTime;
	List<String> userId;
	public AvailabilityPojo() {
		super();
	}
	public AvailabilityPojo(Long startTime, Long endTime, List<String> userId) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.userId = userId;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getEndTime() {
		return endTime;
	}
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
	public List<String> getUserId() {
		return userId;
	}
	public void setUserId(List<String> userId) {
		this.userId = userId;
	}
}