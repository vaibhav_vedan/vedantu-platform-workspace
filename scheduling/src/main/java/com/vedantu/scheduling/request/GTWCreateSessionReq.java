package com.vedantu.scheduling.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.pojo.GTWSessionSlot;

public class GTWCreateSessionReq {

//	{
//		  "subject": "string",
//		  "description": "string",
//		  "times": [
//		    {
//		      "startTime": "2018-01-17T11:00:00Z",
//		      "endTime": "2018-01-17T12:00:00Z"
//		    }
//		  ],
//		  "timeZone": "string",
//		  "type": "single_session",
//		  "isPasswordProtected": false
//		}
	private String subject;
	private String description;
	private String timeZone = "Asia/Calcutta";
	private List<GTWSessionSlot> times;
	private String type = "single_session";
	private boolean isPasswordProtected =false;
	

	public GTWCreateSessionReq() {
		super();
	}

	public GTWCreateSessionReq(OTFSession oTFSession, String organizerAccessToken, String organizerKey) {
		super();
		this.subject = oTFSession.getTitle();
		this.description = oTFSession.getDescription();

		this.times = new ArrayList<GTWSessionSlot>();
		this.times.add(new GTWSessionSlot(oTFSession));

	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<GTWSessionSlot> getTimes() {
		return times;
	}

	public void setTimes(List<GTWSessionSlot> times) {
		this.times = times;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPasswordProtected() {
		return isPasswordProtected;
	}

	public void setPasswordProtected(boolean isPasswordProtected) {
		this.isPasswordProtected = isPasswordProtected;
	}

}
