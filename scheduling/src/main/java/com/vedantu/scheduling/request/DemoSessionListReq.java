package com.vedantu.scheduling.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class DemoSessionListReq extends AbstractFrontEndReq {
	List<DemoSessionReq> demoSessions;

	public DemoSessionListReq() {
		super();
	}

	public DemoSessionListReq(List<DemoSessionReq> demoSessions) {
		super();
		this.demoSessions = demoSessions;
	}

	public List<DemoSessionReq> getDemoSessions() {
		return demoSessions;
	}

	public void setDemoSessions(List<DemoSessionReq> demoSessions) {
		this.demoSessions = demoSessions;
	}
}
