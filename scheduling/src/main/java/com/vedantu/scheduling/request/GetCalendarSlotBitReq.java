package com.vedantu.scheduling.request;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;

public class GetCalendarSlotBitReq {
	private String userId;
	private Long startTime;
	private Long endTime;
	private CalendarEntrySlotState slotState;
	private Long slotDuration = CalendarEntryManager.SLOT_LENGTH;

	public GetCalendarSlotBitReq() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	public Long getSlotDuration() {
		return slotDuration;
	}

	public void setSlotDuration(Long slotDuration) {
		this.slotDuration = slotDuration;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (StringUtils.isEmpty(this.userId)) {
			errors.add(Constants.USER_ID);
		}

		if (this.startTime == null || this.startTime <= 0l) {
			errors.add(Constants.START_TIME);
		}

		if (this.endTime == null || this.endTime <= 0l) {
			errors.add(Constants.END_TIME);
		} else if (this.startTime != null && this.endTime <= this.startTime) {
			errors.add(Constants.END_TIME);
		}

		if (this.slotState == null) {
			errors.add(Constants.SLOT_STATES);
		}

		if (!CalendarEntryManager.validateSlotDuration(this.slotDuration)) {
			errors.add(Constants.SLOT_DURATION);
		}

		return errors;
	}

	public static class Constants {
		public static final String USER_ID = "userId";
		public static final String START_TIME = "startTime";
		public static final String END_TIME = "endTime";
		public static final String SLOT_STATES = "slotState";
		public static final String SLOT_DURATION = "slotDuration";
	}

	@Override
	public String toString() {
		return "GetCalendarSlotBitReq [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", slotState=" + slotState + ", slotDuration=" + slotDuration + "]" + ", GrepCode : " + userId + "_"
				+ startTime + "_" + this.getClass().getSimpleName();
	}
}
