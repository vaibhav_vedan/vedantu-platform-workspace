package com.vedantu.scheduling.request;

import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.*;

import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class AddAlternateNumberForEarlyLearningBookingReq extends AbstractFrontEndReq {

    private String bookingId;
    private String phoneNumber;

    @Override
    public List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(StringUtils.isEmpty(bookingId)){
            errors.add("Booking Id is empty");
        }
        if(Objects.isNull(phoneNumber)){
            errors.add("phoneNumber is null");
        }
        return errors;
    }
}