package com.vedantu.scheduling.request;

public class CancelCalendarEventReq {
	private String eventId;
	private String userId;

	public CancelCalendarEventReq() {
		super();
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
