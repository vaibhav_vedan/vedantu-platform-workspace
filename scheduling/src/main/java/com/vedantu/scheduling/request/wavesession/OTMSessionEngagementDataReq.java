/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.wavesession;

import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import static org.aspectj.weaver.Iterators.array;

/**
 *
 * @author ajith
 */
public class OTMSessionEngagementDataReq extends AbstractFrontEndReq {

    private String sessionId;
    private List<OTMSessionEngagementData> list;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<OTMSessionEngagementData> getList() {
        return list;
    }

    public void setList(List<OTMSessionEngagementData> list) {
        this.list = list;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }
        return errors;
    }
}
