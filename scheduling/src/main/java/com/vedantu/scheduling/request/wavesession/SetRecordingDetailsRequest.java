/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.wavesession;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class SetRecordingDetailsRequest extends AbstractFrontEndReq {

    private String vimeoId;
    private Long recordingStartTime;
    private Long recordedTill;

    public SetRecordingDetailsRequest() {
        super();
    }

    public SetRecordingDetailsRequest(String vimeoId) {
        super();
        this.vimeoId = vimeoId;
    }

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public Long getRecordingStartTime() {
        return recordingStartTime;
    }

    public void setRecordingStartTime(Long recordingStartTime) {
        this.recordingStartTime = recordingStartTime;
    }

    public Long getRecordedTill() {
        return recordedTill;
    }

    public void setRecordedTill(Long recordedTill) {
        this.recordedTill = recordedTill;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == vimeoId && recordedTill == null && recordingStartTime == null) {
            errors.add("vimeoId/recordedTill/recordingStartTime");
        }

        return errors;
    }
}
