package com.vedantu.scheduling.request;


import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.enums.Entity;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Objects;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class AddEarlyLearningSessionReq extends AbstractFrontEndReq {

    private String studentId;
    private String presenter;
    private Long startTime;
    private Long endTime;
    private boolean isEarlyLearningBooking = false;
    private String presenterEmail;
    private String presenterPhone;
    private String studentEmail;
    private String studentPhone;
    private SessionLabel earlyLearningCourse = SessionLabel.SUPER_CODER;
    private OTMSessionType otmSessionType = OTMSessionType.OTO_NURSERY;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
//        if(!isEarlyLearningBooking) {
//            if (StringUtils.isEmpty(presenter)) {
//                errors.add("presenter");
//            }
//        }

        if (Objects.isNull(startTime)) {
            errors.add("startTime can not be null");
        }
//        } else if (startTime < System.currentTimeMillis()) {
//            errors.add("startTime can not be in the past");
//        }

        if (Objects.isNull(endTime)) {
            errors.add("endTime can not be null");
        }
//        } else if (endTime < System.currentTimeMillis()) {
//            errors.add("endTime can not be in the past");
//        }

        if (endTime - startTime > 5 * CommonCalendarUtils.MILLIS_PER_HOUR) {
            errors.add("session duration can not be more than 5 hours");
        }


        if(Objects.isNull(earlyLearningCourse)) {
            errors.add("earlyLearning Course is null");
        }

        if (earlyLearningCourse != null && earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            errors.add(earlyLearningCourse + " earlyLearningCourseType not supported");
        }

        if (otmSessionType != null && otmSessionType.isUnsupported()) {
            errors.add("deprecated otmSessionType " + otmSessionType);
        }

        if (sessionToolType != null && sessionToolType.isUnsupported()) {
            errors.add("deprecated sessionToolType " + sessionToolType);
        }
        return errors;
    }

}
