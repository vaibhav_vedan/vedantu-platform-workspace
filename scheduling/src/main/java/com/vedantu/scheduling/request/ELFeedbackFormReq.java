package com.vedantu.scheduling.request;

import com.vedantu.scheduling.enums.ELFailureReasonType;
import com.vedantu.scheduling.enums.ELInterestMark;
import com.vedantu.scheduling.enums.ELSessionStatus;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ELFeedbackFormReq extends AbstractFrontEndReq {

    private ELSessionStatus sessionSuccessful;
    private List<ELFailureReasonType> failureReason;
    private ELInterestMark parentInterest;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String notesForSales;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String otherComment;
    private String sessionId;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        switch(sessionSuccessful) {
            case YES :
                if(Objects.isNull(parentInterest)) {
                    errors.add("parentInterest is null");
                }
                break;
            case NO :
                if(ArrayUtils.isEmpty(failureReason)) {
                    errors.add("failureReason is empty");
                }
                break;
            default:
                errors.add("sessionSuccessful is having illegal value.. It should be YES or NO");
        }

        if(StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId is empty");
        }

        return errors;
    }
}
