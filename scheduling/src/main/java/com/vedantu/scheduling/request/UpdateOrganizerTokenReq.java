/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request;

import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class UpdateOrganizerTokenReq extends AbstractFrontEndReq {

    private List<GTTOrganizerToken> organizerTokens;

    public List<GTTOrganizerToken> getOrganizerTokens() {
        return organizerTokens;
    }

    public void setOrganizerTokens(List<GTTOrganizerToken> organizerTokens) {
        this.organizerTokens = organizerTokens;
    }
//
//    @Override
//    public String toString() {
//        return "UpdateOrganizerTokenReq{" + "organizerTokens=" + organizerTokens + '}';
//    }
    
    

}
