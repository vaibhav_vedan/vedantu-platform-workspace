package com.vedantu.scheduling.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import javax.validation.constraints.Size;
import java.util.List;

public class CancelOrDeleteBookingReq extends AbstractFrontEndReq {

    private String bookingId;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String remark;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String sessionId) {
        this.bookingId = sessionId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(bookingId)) {
            errors.add("bookingId");
        }
        if (StringUtils.isEmpty(remark)) {
            errors.add("remark");
        }
        return errors;
    }

}