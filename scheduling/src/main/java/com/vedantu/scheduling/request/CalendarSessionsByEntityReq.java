package com.vedantu.scheduling.request;

import com.vedantu.scheduling.enums.SessionCalendarEntityType;

import java.util.List;

public class CalendarSessionsByEntityReq {

    private Long afterStartTime;
    private Long beforeStartTime;
    private Long beforeEndTime;
    private Long afterEndTime;
    private List<String> subjects;
    private List<Integer> grade;
    private List<String> identifiers;
    private SessionCalendarEntityType type;

    public Long getAfterStartTime() {
        return afterStartTime;
    }

    public void setAfterStartTime(Long afterStartTime) {
        this.afterStartTime = afterStartTime;
    }

    public Long getBeforeStartTime() {
        return beforeStartTime;
    }

    public void setBeforeStartTime(Long beforeStartTime) {
        this.beforeStartTime = beforeStartTime;
    }

    public Long getBeforeEndTime() {
        return beforeEndTime;
    }

    public void setBeforeEndTime(Long beforeEndTime) {
        this.beforeEndTime = beforeEndTime;
    }

    public Long getAfterEndTime() {
        return afterEndTime;
    }

    public void setAfterEndTime(Long afterEndTime) {
        this.afterEndTime = afterEndTime;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public List<Integer> getGrade() {
        return grade;
    }

    public void setGrade(List<Integer> grade) {
        this.grade = grade;
    }

    public List<String> getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(List<String> identifiers) {
        this.identifiers = identifiers;
    }

    public SessionCalendarEntityType getType() {
        return type;
    }

    public void setType(SessionCalendarEntityType type) {
        this.type = type;
    }
}
