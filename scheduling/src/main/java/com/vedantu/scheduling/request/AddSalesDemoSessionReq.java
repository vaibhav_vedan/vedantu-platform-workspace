package com.vedantu.scheduling.request;

import com.vedantu.onetofew.enums.EntityTag;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.enums.Entity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class AddSalesDemoSessionReq extends AbstractFrontEndReq {
    private String studentId;
    private String presenter;
    private Long startTime;
    private Long endTime;
    private String presenterEmail;
    private String presenterPhone;
    private String studentEmail;
    private String studentPhone;
    private OTMSessionType otmSessionType = OTMSessionType.OTO_NURSERY;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE;
    private OTFSessionContextType otfSessionContextType = OTFSessionContextType.WEBINAR;
    private Set<SessionLabel> labels;
    private Set<EntityTag> entityTags;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (Objects.isNull(startTime)) {
            errors.add("startTime can not be null");
        } else if (startTime < System.currentTimeMillis()) {
            errors.add("Session time which you have selected is in the Past. We can not schedule the session of past time. Please check the date & time which you have selected. Select the date & time which is in future from current time.");
        }

        if (Objects.isNull(endTime)) {
            errors.add("endTime can not be null");
        } else if (endTime < System.currentTimeMillis()) {
            errors.add("endTime can not be in the past");
        }

        if (endTime - startTime > 5 * CommonCalendarUtils.MILLIS_PER_HOUR) {
            errors.add("session duration can not be more than 5 hours");
        }

        if (otmSessionType != null && otmSessionType.isUnsupported()) {
            errors.add("deprecated otmSessionType " + otmSessionType);
        }

        if (sessionToolType != null && sessionToolType.isUnsupported()) {
            errors.add("deprecated sessionToolType " + sessionToolType);
        }


            return errors;
    }

}
