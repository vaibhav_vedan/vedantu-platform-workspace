/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request.wavesession;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class UpdateSessionDataInsightsReq extends AbstractFrontEndReq {

    private String sessionId;
    private Integer pollCount;
    private Integer quizCount;
    private Integer hotspotCount;
    private Integer noticeCount;
    private Integer chatCount;
    private Integer uniqueStudentAttendants;
    private Integer totalDoubtsAsked;
    private Integer totalDoubtsResolved;
    private Integer averageStudentDuration;//millis

    private Integer sessionDuration;//millis

    public Integer getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(Integer sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getPollCount() {
        return pollCount;
    }

    public void setPollCount(Integer pollCount) {
        this.pollCount = pollCount;
    }

    public Integer getQuizCount() {
        return quizCount;
    }

    public void setQuizCount(Integer quizCount) {
        this.quizCount = quizCount;
    }

    public Integer getHotspotCount() {
        return hotspotCount;
    }

    public void setHotspotCount(Integer hotspotCount) {
        this.hotspotCount = hotspotCount;
    }

    public Integer getNoticeCount() {
        return noticeCount;
    }

    public void setNoticeCount(Integer noticeCount) {
        this.noticeCount = noticeCount;
    }

    public Integer getChatCount() {
        return chatCount;
    }

    public void setChatCount(Integer chatCount) {
        this.chatCount = chatCount;
    }

    public Integer getUniqueStudentAttendants() {
        return uniqueStudentAttendants;
    }

    public void setUniqueStudentAttendants(Integer uniqueStudentAttendants) {
        this.uniqueStudentAttendants = uniqueStudentAttendants;
    }

    public Integer getTotalDoubtsAsked() {
        return totalDoubtsAsked;
    }

    public void setTotalDoubtsAsked(Integer totalDoubtsAsked) {
        this.totalDoubtsAsked = totalDoubtsAsked;
    }

    public Integer getTotalDoubtsResolved() {
        return totalDoubtsResolved;
    }

    public void setTotalDoubtsResolved(Integer totalDoubtsResolved) {
        this.totalDoubtsResolved = totalDoubtsResolved;
    }

    public Integer getAverageStudentDuration() {
        return averageStudentDuration;
    }

    public void setAverageStudentDuration(Integer averageStudentDuration) {
        this.averageStudentDuration = averageStudentDuration;
    }

    
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }

        return errors;
    }

}
