/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class UpdateReplayWatchedDurationReq extends AbstractFrontEndReq{
    private String userId;
    private String sessionId;
    private Long startTime;
    private Integer duration;
    private Boolean isIR;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Boolean getIR() {
        return isIR;
    }

    public void setIsIR(Boolean isIR) {
        this.isIR = isIR;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(userId)) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(sessionId)) {
            errors.add("sessionId");
        }
        if(startTime == null){
            errors.add("startTime");
        }
        if(duration == null){
            errors.add("duration");
        }
        
        return errors;
    }
    
}
