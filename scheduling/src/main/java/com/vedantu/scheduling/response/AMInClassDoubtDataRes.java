package com.vedantu.scheduling.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AMInClassDoubtDataRes {
    private String errorCode;
    private String errorMessage;
    private List<AMInClassDoubtData> result;
    private Integer remainingChunk;

}
