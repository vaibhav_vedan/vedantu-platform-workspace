package com.vedantu.scheduling.response;

import org.springframework.util.StringUtils;

import com.vedantu.exception.VException;

public class BasicCalendarSlotResponse {
	private Boolean success = Boolean.TRUE;
	private String errorCode = "";
	private String errorMessage = "";
	private String displayMessage = "";
	private Object result;

	public BasicCalendarSlotResponse() {
		super();
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
		if (!StringUtils.isEmpty(errorCode)) {
			this.success = Boolean.FALSE;
		}
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		if (!StringUtils.isEmpty(errorCode)) {
			this.success = Boolean.FALSE;
		}
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public void updateResult(Object response) {
		this.result = response;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}

	public void setErrorFields(VException exception) {
		this.success = Boolean.FALSE;
		this.errorCode = exception.getErrorCode().name();
		this.errorMessage = exception.getErrorMessage();
	}
}
