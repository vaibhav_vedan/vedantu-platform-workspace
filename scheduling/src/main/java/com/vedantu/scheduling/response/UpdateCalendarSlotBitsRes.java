package com.vedantu.scheduling.response;

import com.vedantu.scheduling.dao.entity.CalendarEvent;

public class UpdateCalendarSlotBitsRes {
	private Boolean success = Boolean.TRUE;
	private CalendarEvent calendarEvent;

	public UpdateCalendarSlotBitsRes() {
		super();
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public CalendarEvent getCalendarEvent() {
		return calendarEvent;
	}

	public void setCalendarEvent(CalendarEvent calendarEvent) {
		this.calendarEvent = calendarEvent;
	}

	@Override
	public String toString() {
		return "UpdateCalendarSlotBitsRes [success=" + success + ", calendarEvent=" + calendarEvent + "]";
	}
}
