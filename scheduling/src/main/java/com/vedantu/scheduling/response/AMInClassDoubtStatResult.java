package com.vedantu.scheduling.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMInClassDoubtStatResult {
    private String _id;
    private long count;
}
