package com.vedantu.scheduling.response;

public class Remark {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Remark [value=" + value + "]";
	}

}
