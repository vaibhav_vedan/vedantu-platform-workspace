package com.vedantu.scheduling.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class AbsentSessionRes {
    String presenter;
    String gttId;
    String sessionId;
    String enrolmentId;
    Long boardId;
    Long startTime;
    Long endTime;
}
