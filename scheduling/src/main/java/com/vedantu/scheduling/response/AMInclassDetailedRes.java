package com.vedantu.scheduling.response;

import java.util.List;

import com.vedantu.scheduling.pojo.GTTAttendeeDetailsWithTopics;

public class AMInclassDetailedRes {
	
	private Long boardId;
	private String boardName;
	private List<GTTAttendeeDetailsWithTopics> gttAttendeeDetailsWithTopics;
	
	public AMInclassDetailedRes(Long boardId, String boardName,
			List<GTTAttendeeDetailsWithTopics> gttAttendeeDetailsWithTopics) {
		super();
		this.boardId = boardId;
		this.boardName = boardName;
		this.gttAttendeeDetailsWithTopics = gttAttendeeDetailsWithTopics;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public List<GTTAttendeeDetailsWithTopics> getGttAttendeeDetailsWithTopics() {
		return gttAttendeeDetailsWithTopics;
	}

	public void setGttAttendeeDetailsWithTopics(List<GTTAttendeeDetailsWithTopics> gttAttendeeDetailsWithTopics) {
		this.gttAttendeeDetailsWithTopics = gttAttendeeDetailsWithTopics;
	}	
	
	@Override
	public String toString() {
		return "AMInclassDetailedRes [ + boardId: " + boardId + ", boardName:" +boardName +  "gttAttendeeDetailsWithTopics:" + gttAttendeeDetailsWithTopics +" ]";
	}
}
