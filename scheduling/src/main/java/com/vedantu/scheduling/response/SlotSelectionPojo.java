package com.vedantu.scheduling.response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.managers.CalendarSelectionManager;
import com.vedantu.session.pojo.SessionSlot;

public class SlotSelectionPojo {
	public List<TagSlots> tagSlots;
	public TagSlots others;
	public int morningSlotCount;
	public int eveningSlotCount;
	public int slotCount;

	public SlotSelectionPojo() {
		super();
	}

	public SlotSelectionPojo(List<TagSlots> tagSlots, int slotCount) {
		super();
		this.tagSlots = tagSlots;
		this.slotCount = slotCount;
		updateTotalCount();
	}

	public List<TagSlots> getTagSlots() {
		return tagSlots;
	}

	public void setTagSlots(List<TagSlots> tagSlots) {
		this.tagSlots = tagSlots;
	}

	public int getSlotCount() {
		return slotCount;
	}

	public void setSlotCount(int slotCount) {
		this.slotCount = slotCount;
	}

	public TagSlots getOthers() {
		return others;
	}

	public void setOthers(TagSlots others) {
		this.others = others;
	}

	public int getMorningSlotCount() {
		return morningSlotCount;
	}

	public void setMorningSlotCount(int morningSlotCount) {
		this.morningSlotCount = morningSlotCount;
	}

	public int getEveningSlotCount() {
		return eveningSlotCount;
	}

	public void setEveningSlotCount(int eveningSlotCount) {
		this.eveningSlotCount = eveningSlotCount;
	}

	public void updateTotalCount() {
		this.eveningSlotCount = 0;
		this.morningSlotCount = 0;
		this.slotCount = 0;
		if (tagSlots != null) {
			for (TagSlots tagSlot : tagSlots) {
				this.slotCount += tagSlot.fetchSlotCount();
				if (CalendarSelectionManager.eveningSlug.equals(tagSlot.getSectionName())) {
					this.eveningSlotCount += tagSlot.fetchSlotCount();
				}
				if (CalendarSelectionManager.morningSlug.equals(tagSlot.getSectionName())) {
					this.morningSlotCount += tagSlot.fetchSlotCount();
				}
			}
		}

		if (others != null) {
			this.slotCount += others.fetchSlotCount();
		}
	}

	public int fetchSectionCount(String name) {
		int count = 0;
		if (CollectionUtils.isEmpty(this.tagSlots) || StringUtils.isEmpty(name)) {
			count = 0;
		} else {
			for (TagSlots tagSlot : this.tagSlots) {
				if (name.equals(tagSlot.getSectionName())) {
					count += tagSlot.fetchSlotCount();
				}
			}
		}

		return count;
	}

	public int fetchTagCount(String name) {
		int count = 0;
		if (CollectionUtils.isEmpty(this.tagSlots) || StringUtils.isEmpty(name)) {
			count = 0;
		} else {
			for (TagSlots tagSlot : this.tagSlots) {
				if (name.equals(tagSlot.getTagName())) {
					count += tagSlot.fetchSlotCount();
				}
			}
		}

		return count;
	}

	public void filterMorningSlots(int limit) {
		updateTotalCount();
		if (this.morningSlotCount > limit) {
			if (this.tagSlots != null) {
				Map<String, Integer> morningMap = new HashMap<String, Integer>();
				int morningIndex = 1;
				for (TagSlots tagSlot : tagSlots) {
					if (CalendarSelectionManager.morningSlug.equals(tagSlot.getSectionName())) {
						morningMap.put("morning" + morningIndex, tagSlots.indexOf(tagSlot));
						morningIndex++;
					}
				}

				if (this.tagSlots.get(morningMap.get("morning1")).fetchSlotCount() < this.tagSlots
						.get(morningMap.get("morning2")).fetchSlotCount()) {
					// Filter evening1 slots based on limit
					TagSlots tagSlot = this.tagSlots.get(morningMap.get("morning1"));
					tagSlot.filterSlots(Long.valueOf(tagSlot.maxSlots).intValue());

					// Filter evening2 slots based on remaining slots
					int remainingSlots = limit - tagSlot.fetchSlotCount();
					this.tagSlots.get(morningMap.get("morning2")).filterSlots(remainingSlots);
				} else {
					// Filter evening1 slots based on limit
					TagSlots tagSlot = this.tagSlots.get(morningMap.get("morning2"));
					tagSlot.filterSlots(Long.valueOf(tagSlot.maxSlots).intValue());

					// Filter evening2 slots based on remaining slots
					int remainingSlots = limit - tagSlot.fetchSlotCount();
					this.tagSlots.get(morningMap.get("morning1")).filterSlots(remainingSlots);
				}
			}
		}
	}

	public void filterEveningSlots(int limit) {
		updateTotalCount();
		if (this.eveningSlotCount > limit) {
			if (this.tagSlots != null) {
				Map<String, Integer> eveningMap = new HashMap<String, Integer>();
				int eveningIndex = 1;
				for (TagSlots tagSlot : tagSlots) {
					if (CalendarSelectionManager.eveningSlug.equals(tagSlot.getSectionName())) {
						eveningMap.put("evening" + eveningIndex, tagSlots.indexOf(tagSlot));
						eveningIndex++;
					}
				}

				if (this.tagSlots.get(eveningMap.get("evening1")).fetchSlotCount() < this.tagSlots
						.get(eveningMap.get("evening2")).fetchSlotCount()) {
					// Filter evening1 slots based on limit
					TagSlots tagSlot = this.tagSlots.get(eveningMap.get("evening1"));
					tagSlot.filterSlots(Long.valueOf(tagSlot.maxSlots).intValue());

					// Filter evening2 slots based on remaining slots
					int remainingSlots = limit - tagSlot.fetchSlotCount();
					this.tagSlots.get(eveningMap.get("evening2")).filterSlots(remainingSlots);
				} else {
					// Filter evening1 slots based on limit
					TagSlots tagSlot = this.tagSlots.get(eveningMap.get("evening2"));
					tagSlot.filterSlots(Long.valueOf(tagSlot.maxSlots).intValue());

					// Filter evening2 slots based on remaining slots
					int remainingSlots = limit - tagSlot.fetchSlotCount();
					this.tagSlots.get(eveningMap.get("evening1")).filterSlots(remainingSlots);
				}
			}
		}
	}

	public List<SessionSlot> getSixSessions() {
		int slotLimit = 6;
		int slotCount = 0;
		List<SessionSlot> slots = new ArrayList<SessionSlot>();
		if (this.tagSlots != null) {
			for (TagSlots tagSlot : tagSlots) {
				slots.addAll(tagSlot.getAvailableSlots());
				slotCount += tagSlot.getAvailableSlots().size();
			}
		}

		if (this.others != null && !CollectionUtils.isEmpty(this.others.getAvailableSlots()) && slotCount < slotLimit) {
			int size = (slotLimit - slotCount);
			if (this.others.getAvailableSlots().size() < size) {
				size = this.others.getAvailableSlots().size();
			}

			if (size > 0) {
				this.others.filterSlots(size);
				slots.addAll(this.others.getAvailableSlots());
			}
		}

		this.updateTotalCount();
		if (!slots.isEmpty()) {
			Collections.sort(slots);
		}
		return slots;
	}

	public List<SessionSlot> getThreeSessions() {
		int slotLimit = 3;
		int slotCount = 0;
		List<SessionSlot> slots = new ArrayList<SessionSlot>();

		if (this.slotCount > 3) {
			// Filter slots
			int eveningLimit = 2;
			int eveningSlots = 0;
			int morningSlots = 0;
			if (eveningSlotCount < 2) {
				// Pick evening first
				eveningSlots = eveningSlotCount;
				morningSlots = slotLimit - eveningSlots;
			} else if (morningSlotCount < 1) {
				eveningSlots = slotLimit;
			} else {
				eveningSlots = eveningLimit;
				morningSlots = slotLimit - eveningSlots;
			}

			// Pick eveningSlots
			pickEveningThreeSessions(eveningSlots);
			pickMorningThreeSessions(morningSlots);
		}

		for (TagSlots tagSlot : tagSlots) {
			slots.addAll(tagSlot.getAvailableSlots());
			slotCount += tagSlot.getAvailableSlots().size();
		}

		if (this.others != null && !CollectionUtils.isEmpty(this.others.getAvailableSlots()) && (slotCount < slotLimit)) {
			this.others.filterSlots(slotLimit - slotCount);
			slots.addAll(this.others.getAvailableSlots());
		}

		if (!slots.isEmpty()) {
			Collections.sort(slots);
		}
		return slots;
	}

	public void pickEveningThreeSessions(int remainingLimit) {
		// Pick evening sessions
		Map<String, Integer> eveningMap = new HashMap<String, Integer>();
		int eveningIndex = 1;
		for (TagSlots tagSlot : tagSlots) {
			if (CalendarSelectionManager.eveningSlug.equals(tagSlot.getSectionName())) {
				eveningMap.put("evening" + eveningIndex, tagSlots.indexOf(tagSlot));
				eveningIndex++;
			}
		}

		int evening1Count = 0;
		int evening2Count = 0;
		int evening1Limit = 1;
		int evening2Limit = 1;
		switch (remainingLimit) {
		case 0:
			evening1Count = 0;
			evening2Count = 0;
			break;
		case 1:
			if (this.tagSlots.get(eveningMap.get("evening1")).fetchSlotCount() >= 1) {
				evening1Count = remainingLimit;
				evening2Count = 0;
			} else {
				evening1Count = 0;
				evening2Count = remainingLimit;
			}
			break;
		case 2:
			if (this.tagSlots.get(eveningMap.get("evening1")).fetchSlotCount() < 1) {
				evening1Count = 0;
				evening2Count = remainingLimit;
			} else if (this.tagSlots.get(eveningMap.get("evening2")).fetchSlotCount() < 1) {
				evening1Count = remainingLimit;
				evening2Count = 0;
			} else {
				evening1Count = evening1Limit;
				evening2Count = remainingLimit - evening1Count;
			}
			break;
		case 3:
			if (this.tagSlots.get(eveningMap.get("evening1")).fetchSlotCount() < 1) {
				evening1Count = 0;
				evening2Count = remainingLimit;
			} else if (this.tagSlots.get(eveningMap.get("evening2")).fetchSlotCount() < 1) {
				evening1Count = remainingLimit;
				evening2Count = 0;
			} else if (this.tagSlots.get(eveningMap.get("evening1")).fetchSlotCount() < this.tagSlots
					.get(eveningMap.get("evening2")).fetchSlotCount()) {
				evening1Count = Math.min(this.tagSlots.get(eveningMap.get("evening1")).fetchSlotCount(),
						remainingLimit - evening2Limit);
				evening2Count = remainingLimit - evening1Count;
			} else {
				evening2Count = Math.min(this.tagSlots.get(eveningMap.get("evening2")).fetchSlotCount(), evening2Limit);
				evening1Count = remainingLimit - evening2Count;
			}
			break;
		default:
			break;
		}

		this.tagSlots.get(eveningMap.get("evening1")).filterSlots(evening1Count);
		this.tagSlots.get(eveningMap.get("evening2")).filterSlots(evening2Count);
		updateTotalCount();
	}

	public void pickMorningThreeSessions(int remainingLimit) {
		// Pick evening sessions
		Map<String, Integer> morningMap = new HashMap<String, Integer>();
		int morningIndex = 1;
		for (TagSlots tagSlot : tagSlots) {
			if (CalendarSelectionManager.morningSlug.equals(tagSlot.getSectionName())) {
				morningMap.put("morning" + morningIndex, tagSlots.indexOf(tagSlot));
				morningIndex++;
			}
		}

		int morning1Count = 0;
		int morning2Count = 0;
		int morning1Limit = 1;
		int morning2Limit = 1;
		switch (remainingLimit) {
		case 0:
			break;
		case 1:
			if (this.tagSlots.get(morningMap.get("morning1")).fetchSlotCount() >= 1) {
				morning1Count = remainingLimit;
				morning2Count = 0;
			} else {
				morning1Count = 0;
				morning2Count = remainingLimit;
			}
			break;
		case 2:
			if (this.tagSlots.get(morningMap.get("morning1")).fetchSlotCount() < 1) {
				morning1Count = 0;
				morning2Count = remainingLimit;
			} else if (this.tagSlots.get(morningMap.get("morning2")).fetchSlotCount() < 1) {
				morning1Count = remainingLimit;
				morning2Count = 0;
			} else {
				morning1Count = morning1Limit;
				morning2Count = remainingLimit - morning1Limit;
			}
			break;
		case 3:
			if (this.tagSlots.get(morningMap.get("morning1")).fetchSlotCount() < 1) {
				morning1Count = 0;
				morning2Count = remainingLimit;
			} else if (this.tagSlots.get(morningMap.get("morning2")).fetchSlotCount() < 1) {
				morning2Count = 0;
				morning1Count = remainingLimit;
			} else if (this.tagSlots.get(morningMap.get("morning1")).fetchSlotCount() < this.tagSlots
					.get(morningMap.get("morning2")).fetchSlotCount()) {
				morning1Count = Math.min(this.tagSlots.get(morningMap.get("morning1")).fetchSlotCount(),
						remainingLimit - morning2Limit);
				morning2Count = remainingLimit - morning1Count;
			} else {
				morning2Count = Math.min(this.tagSlots.get(morningMap.get("morning2")).fetchSlotCount(), morning2Limit);
				morning1Count = remainingLimit - morning2Count;
			}
			break;
		default:
			break;
		}

		this.tagSlots.get(morningMap.get("morning1")).filterSlots(morning1Count);
		this.tagSlots.get(morningMap.get("morning2")).filterSlots(morning2Count);
		updateTotalCount();
	}

	@Override
	public String toString() {
		return "SlotSelectionPojo [tagSlots=" + Arrays.toString(tagSlots.toArray()) + ", slotCount=" + slotCount + "]";
	}
}
