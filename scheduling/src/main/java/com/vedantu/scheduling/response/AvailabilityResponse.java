package com.vedantu.scheduling.response;

import java.util.Arrays;
import java.util.BitSet;

import com.vedantu.scheduling.pojo.BitSetUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public class AvailabilityResponse {
	private String teacherId;
	private String studentId;
	private Long startTime;
	private Long endTime;
	private Integer[] trueAvailability;
	private long[] trueAvailabilityBitSet;
	private long[] availabilityBitSet;

	public AvailabilityResponse() {
		super();
	}

	public AvailabilityResponse(String teacherId, String studentId, Long startTime, Long endTime) {
		super();
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Integer[] getTrueAvailability() {
		return trueAvailability;
	}

	public void setTrueAvailability(Integer[] trueAvailability) {
		this.trueAvailability = trueAvailability;
	}

	public void setTrueAvailability(BitSet bitSet, long startTime, long endTime) {
		this.trueAvailability = BitSetUtils.createSlotIntegerArray(startTime, endTime, bitSet);
	}

	public void setTrueAvailabilityIndex(int index) {
		this.trueAvailability[index] = 1;
	}

	public void unsetTrueAvailabilityIndex(int index) {
		this.trueAvailability[index] = 0;
	}

	public long[] getTrueAvailabilityBitSet() {
		return trueAvailabilityBitSet;
	}

	public void setTrueAvailabilityBitSet(long[] trueAvailabilityBitSet) {
		this.trueAvailabilityBitSet = trueAvailabilityBitSet;
	}

	public long[] getAvailabilityBitSet() {
		return availabilityBitSet;
	}

	public void setAvailabilityBitSet(long[] availabilityBitSet) {
		this.availabilityBitSet = availabilityBitSet;
	}

	public void removeAvailabilityTillTime(long time) {
		time = CommonCalendarUtils.getSlotStartTime(time);
		int endIndex = CommonCalendarUtils.getSlotStartIndex(time, this.startTime);
		for (int index = 0; index <= endIndex; index++) {
			this.trueAvailability[index] = 0;
		}
	}

	@Override
	public String toString() {
		return "AvailabilityResponse [teacherId=" + teacherId + ", studentId=" + studentId + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", trueAvailability=" + Arrays.toString(trueAvailability)
				+ ", trueAvailabilityBitSet=" + Arrays.toString(trueAvailabilityBitSet) + "]";
	}
}
