package com.vedantu.scheduling.response;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.OTFSessionAttendeeInfo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OTFSessionPojo extends OTFSession {

    private List<CourseBasicInfo> courseInfos;
    private List<String> groupNames;
    private EntityStatus batchState;
    private List<OTFSessionAttendeeInfo> attendeeInfos = new ArrayList<>();
    private UserBasicInfo presenterInfo;


    public void updateUserBasicInfo(UserBasicInfo userBasicInfo) {
        for (int i = 0; i < this.attendeeInfos.size(); i++) {
            OTFSessionAttendeeInfo sessionAttendeeInfo = this.attendeeInfos.get(i);
            if (sessionAttendeeInfo.getUserId() != null && sessionAttendeeInfo.getUserId() > 0l
                    && sessionAttendeeInfo.getUserId().equals(userBasicInfo.getUserId())) {
                sessionAttendeeInfo.updateUserDetails(userBasicInfo);
                this.attendeeInfos.set(i, sessionAttendeeInfo);
                return;
            }
        }

        // Create a new attendee
        OTFSessionAttendeeInfo otfSessionAttendeeInfo = new OTFSessionAttendeeInfo(userBasicInfo.getUserId());
        otfSessionAttendeeInfo.updateUserDetails(userBasicInfo);
        if (Role.STUDENT.equals(otfSessionAttendeeInfo.getRole())) {
            this.attendeeInfos.add(otfSessionAttendeeInfo);
        }
    }
}
