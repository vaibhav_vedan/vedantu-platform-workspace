package com.vedantu.scheduling.response;

public class InstaActiveMissedResponse {

	private Integer active;
	private Integer missed;

	public InstaActiveMissedResponse() {

	}

	public InstaActiveMissedResponse(Integer active, Integer missed) {
		this.active = active;
		this.missed = missed;
	}

	public Integer getActive() {
		return active;
	}

	public void setId(Integer active) {
		this.active = active;
	}

	public Integer getMissed() {
		return missed;
	}

	public void setMissed(Integer missed) {
		this.missed = missed;
	}
}