/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response;

import java.util.List;

import com.vedantu.scheduling.pojo.SessionInfo;
import org.springframework.util.CollectionUtils;

import com.vedantu.session.pojo.OTFSessionPojoUtils;

/**
 *
 * @author shyam
 */
public class GetLatestUpComingOrOnGoingSessionResponse {
	private SessionInfo sessionRes;
	private OTFSessionPojoUtils otfSession;
	private OTFSessionPojoUtils webinarSession;
	private long currentSystemTime = System.currentTimeMillis();
	
	public GetLatestUpComingOrOnGoingSessionResponse() {
		super();
	}

	public GetLatestUpComingOrOnGoingSessionResponse(List<SessionInfo> sessions, List<OTFSessionPojoUtils> otfSessions) {
		if (!CollectionUtils.isEmpty(sessions)) {
			this.sessionRes = sessions.get(0);
		}
		if (!CollectionUtils.isEmpty(otfSessions)) {
			this.otfSession = otfSessions.get(0);
		}
		this.currentSystemTime = System.currentTimeMillis();
	}

	public GetLatestUpComingOrOnGoingSessionResponse(SessionInfo sessionRes, OTFSessionPojoUtils otfSession) {
		this.sessionRes = sessionRes;
		this.otfSession = otfSession;
		this.currentSystemTime = System.currentTimeMillis();
	}

	public void setSessionRes(SessionInfo sessionRes) {
		this.sessionRes = sessionRes;
	}

	public void setOtfSession(OTFSessionPojoUtils otfSession) {
		this.otfSession = otfSession;
	}

	public SessionInfo getSessionRes() {
		return sessionRes;
	}

	public OTFSessionPojoUtils getOtfSession() {
		return otfSession;
	}

	public OTFSessionPojoUtils getWebinarSession() {
		return webinarSession;
	}

	public void setWebinarSession(OTFSessionPojoUtils webinarSession) {
		this.webinarSession = webinarSession;
	}

	public long getCurrentSystemTime() {
		return currentSystemTime;
	}

	public void setCurrentSystemTime(long currentSystemTime) {
		this.currentSystemTime = currentSystemTime;
	}

	public void includeResponse(GetLatestUpComingOrOnGoingSessionResponse res) {
		if (res.getOtfSession() != null) {
			this.otfSession = res.getOtfSession();
		}
		if (res.getWebinarSession() != null) {
			this.webinarSession = res.getWebinarSession();
		}
		if (res.getSessionRes() != null) {
			this.sessionRes = res.getSessionRes();
		}
	}

	@Override
	public String toString() {
		return "GetLatestUpComingOrOnGoingSessionRes [sessionRes=" + sessionRes + ", otfSession=" + otfSession
				+ ", webinarSession=" + webinarSession + ", currentSystemTime=" + currentSystemTime + ", toString()="
				+ super.toString() + "]";
	}
}
