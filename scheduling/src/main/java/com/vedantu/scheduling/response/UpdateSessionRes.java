package com.vedantu.scheduling.response;

import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.util.fos.response.AbstractRes;

public class UpdateSessionRes extends AbstractRes {

    private OTFSession preUpdateSession;
    private OTFSession postUpdateSession;

    public UpdateSessionRes() {
        super();
    }

    public UpdateSessionRes(OTFSession preUpdateSession, OTFSession postUpdateSession) {
        this.preUpdateSession = preUpdateSession;
        this.postUpdateSession = postUpdateSession;
    }
    
    public OTFSession getPreUpdateSession() {
        return preUpdateSession;
    }

    public void setPreUpdateSession(OTFSession preUpdateSession) {
        this.preUpdateSession = preUpdateSession;
    }

    public OTFSession getPostUpdateSession() {
        return postUpdateSession;
    }

    public void setPostUpdateSession(OTFSession postUpdateSession) {
        this.postUpdateSession = postUpdateSession;
    }

}
