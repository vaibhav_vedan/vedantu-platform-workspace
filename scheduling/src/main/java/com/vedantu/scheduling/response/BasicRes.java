package com.vedantu.scheduling.response;

public class BasicRes {
	private Boolean success = true;

	public BasicRes() {
		super();
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "BasicRes [success=" + success + "]";
	}
}
