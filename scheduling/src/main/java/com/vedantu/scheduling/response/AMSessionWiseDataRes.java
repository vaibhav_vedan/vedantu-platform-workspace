package com.vedantu.scheduling.response;

import com.vedantu.scheduling.pojo.GTTAttendanceBasic;
import lombok.*;

@Data
@Getter
@Setter
public class AMSessionWiseDataRes {
    private GTTAttendanceBasic individual;
    private GTTAttendanceBasic average;
    private Long sessionCreationTime;
    private String sessionName;

    public AMSessionWiseDataRes() {
    }

    public AMSessionWiseDataRes(GTTAttendanceBasic individual, GTTAttendanceBasic average, Long sessionCreationTime) {
        this.individual = individual;
        this.average = average;
        this.sessionCreationTime = sessionCreationTime;
    }

    public AMSessionWiseDataRes(GTTAttendanceBasic individual, GTTAttendanceBasic average, Long sessionCreationTime, String sessionName) {
        this.individual = individual;
        this.average = average;
        this.sessionCreationTime = sessionCreationTime;
        this.sessionName = sessionName;
    }
}
