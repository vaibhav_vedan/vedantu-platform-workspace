package com.vedantu.scheduling.response;

import java.util.BitSet;

import com.vedantu.scheduling.pojo.BitSetUtils;

public class GetCalendarSlotStringRes extends GetCalendarSlotBitRes {
	public String bitSetString;

	public GetCalendarSlotStringRes() {
		super();
	}

	public GetCalendarSlotStringRes(GetCalendarSlotBitRes res) {
		super();
		updateFields(res);
		updateBitSetString();
		resetBitSetLongArray();
	}

	public String getBitSetString() {
		return bitSetString;
	}

	public void setBitSetString(String bitSetString) {
		this.bitSetString = bitSetString;
	}

	public void updateBitSetString() {
		this.bitSetString = BitSetUtils.createString(BitSet.valueOf(getBitSetLongArray()), getBitSetSize());
	}

	public void resetBitSetLongArray() {
		setBitSetLongArray(new long[0]);
	}

	public void updateFields(GetCalendarSlotBitRes res) {
		this.startTime = res.getStartTime();
		this.endTime = res.getEndTime();
		this.slotDuration = res.getSlotDuration();
		this.slotState = res.getSlotState();
		this.userId = res.getUserId();
		this.bitSetLongArray = res.getBitSetLongArray();
		if (this.bitSetLongArray == null) {
			this.bitSetLongArray = new long[0];
		}
	}
}
