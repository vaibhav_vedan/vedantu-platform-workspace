/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response;

import com.vedantu.onetofew.enums.OTMSessionInProgressState;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author ajith
 */
public class SetRecordingDetailsRes extends AbstractRes {

    private SessionState state;
    private OTMSessionInProgressState progressState;

    public SessionState getState() {
        return state;
    }

    public void setState(SessionState state) {
        this.state = state;
    }

    public OTMSessionInProgressState getProgressState() {
        return progressState;
    }

    public void setProgressState(OTMSessionInProgressState progressState) {
        this.progressState = progressState;
    }

}
