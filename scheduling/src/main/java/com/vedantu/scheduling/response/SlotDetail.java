package com.vedantu.scheduling.response;

public class SlotDetail {

	private Long studentId;
	private Long teacherId;
	private Long startTime;
	private Long endTime;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public SlotDetail() {
		super();
	}

	public SlotDetail(Long studentId, Long teacherId, Long startTime, Long endTime) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "Session [studentId=" + studentId + ", teacherId=" + teacherId + ", startTime=" + startTime
				+ ", endTime=" + endTime + "]";
	}


	


	
	
}
