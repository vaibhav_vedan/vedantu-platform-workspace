package com.vedantu.scheduling.response;

import com.vedantu.session.pojo.SessionSlot;
import java.util.List;

import com.vedantu.util.scheduling.CommonCalendarUtils;

public class TagSlots {
	public String tagName;
	public String sectionName;
	public Long maxSlots;
	public Long startTime;
	public Long endTime;
	public List<SessionSlot> availableSlots;

	public TagSlots() {
		super();
	}

	public static TagSlots createOthers(List<SessionSlot> sessionSlots) {
		TagSlots tagSlots = new TagSlots();
		tagSlots.availableSlots = sessionSlots;
		tagSlots.startTime = 0l;
		tagSlots.endTime = CommonCalendarUtils.MILLIS_PER_DAY;
		tagSlots.maxSlots = 96l;
		tagSlots.sectionName = "others";
		tagSlots.tagName = "others";
		return tagSlots;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public Long getMaxSlots() {
		return maxSlots;
	}

	public void setMaxSlots(Long maxSlots) {
		this.maxSlots = maxSlots;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public List<SessionSlot> getAvailableSlots() {
		return availableSlots;
	}

	public void setAvailableSlots(List<SessionSlot> availableSlots) {
		this.availableSlots = availableSlots;
	}

	public int fetchSlotCount() {
		return (this.availableSlots != null) ? availableSlots.size() : 0;
	}

	public void filterSlots(int count) {
		if (this.availableSlots != null && this.availableSlots.size() > count && count >= 0) {
			this.availableSlots = this.availableSlots.subList(0, count);
		}
	}

	@Override
	public String toString() {
		return "TagSlots [tagName=" + tagName + ", sectionName=" + sectionName + ", maxSlots=" + maxSlots
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", availableSlots=" + availableSlots + "]";
	}
}
