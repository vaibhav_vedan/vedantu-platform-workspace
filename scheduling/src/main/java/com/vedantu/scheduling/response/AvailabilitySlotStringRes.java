package com.vedantu.scheduling.response;

import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.BitSetUtils;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.response.earlylearning.AbstractEarlyLearningResponse;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Arrays;
import java.util.stream.Collectors;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@SuperBuilder
@ToString
public class AvailabilitySlotStringRes extends AbstractEarlyLearningResponse {

    private String userId;
    private Long startTime;
    private Long endTime;
    private CalendarEntrySlotState slotState;
    private Long slotDuration = CalendarEntryManager.SLOT_LENGTH;
    private String bitSetString;
    private long[] bitSetArray;

    public AvailabilitySlotStringRes(String userId, Long startTime, Long endTime,
                                     CalendarEntrySlotState slotState, String bitSetString) {
        this.userId = userId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.slotState = slotState;
        this.bitSetString = bitSetString;
    }

    public AvailabilitySlotStringRes(Long startTime, Long endTime,
                                     CalendarEntrySlotState slotState, String bitSetString) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.slotState = slotState;
        this.bitSetString = bitSetString;
    }


//    public int getBitSetSize() {
//        return BitSetUtils.bitSetSize(getStartTime(), getEndTime(), getSlotDuration());
//    }

}
