package com.vedantu.scheduling.response;

import java.util.List;

import com.vedantu.User.User;
import com.vedantu.scheduling.dao.entity.OTFSession;

public class StudentInclassDetailedRes {
	
	private User user;
	private List<AMInclassDetailedRes> amInclassDetailedList;
	private List<OTFSession> sessionList;
	
	public StudentInclassDetailedRes() {
		super();
	}

	public StudentInclassDetailedRes(User user, List<AMInclassDetailedRes> amInclassDetailedList) {
		super();
		this.user = user;
		this.amInclassDetailedList = amInclassDetailedList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<AMInclassDetailedRes> getAmInclassDetailedList() {
		return amInclassDetailedList;
	}

	public void setAmInclassDetailedList(List<AMInclassDetailedRes> amInclassDetailedList) {
		this.amInclassDetailedList = amInclassDetailedList;
	}

	public List<OTFSession> getSessionList() {
		return sessionList;
	}

	public void setSessionList(List<OTFSession> sessionList) {
		this.sessionList = sessionList;
	}

	@Override
	public String toString() {
		return "SlotSelectionPojo [user=" + user + ", amInclassDetailedList=" +amInclassDetailedList;
	}
	
}
