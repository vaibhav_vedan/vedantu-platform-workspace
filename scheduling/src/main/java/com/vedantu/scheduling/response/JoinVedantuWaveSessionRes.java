/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response;

import com.vedantu.onetofew.enums.*;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.enums.OTMSessionRole;
import com.vedantu.scheduling.pojo.OTMSessionConfig;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.fos.response.AbstractRes;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
@Getter
@Setter
public class JoinVedantuWaveSessionRes extends AbstractRes {

    private OTMSessionConfig otmSessionConfig;
    private String sessionId;
    private String title;
    private String teacherId;
    private Set<Long> taIds;
    private String nodeServerUrl;
    private String nodeServerPort;
    private OTMServiceProvider serviceProvider;
    private String meetingId;
    private String sessionUrl;
    private List<SessionPagesMetadata> pagesMetadata;
    private OTMSessionRole sessionRole;
    private Long startTime;
    private Long endTime;
    private String sessionContextType;
    private OTFSessionToolType sessionToolType;
    private CanvasStreamingType canvasStreamingType;
    private OTMSessionInProgressState progressState;
    private Long serverTime;
    private Long agoraStartTime;
    private String parentSessionId;
    private String agoraReplayUrl;
    private OTMSessionType otmSessionType;
    private List<String> attendees;
    private EntityType type;
    private Set<SessionLabel> labels;
    private Set<EntityTag> entityTags = new HashSet<>();
    private String webinarId;
    private Boolean isBigWhiteBoard;

    public String getSessionContextType() {
        return sessionContextType;
    }

    public void setSessionContextType(String sessionContextType) {
        this.sessionContextType = sessionContextType;
    }
    
    public OTFSessionToolType getSessionToolType() {
        return this.sessionToolType;
    }
    
    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public OTMSessionConfig getOtmSessionConfig() {
        return otmSessionConfig;
    }

    public void setOtmSessionConfig(OTMSessionConfig otmSessionConfig) {
        this.otmSessionConfig = otmSessionConfig;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SessionPagesMetadata> getPagesMetadata() {
        return pagesMetadata;
    }

    public void setPagesMetadata(List<SessionPagesMetadata> pagesMetadata) {
        this.pagesMetadata = pagesMetadata;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getNodeServerUrl() {
        return nodeServerUrl;
    }

    public void setNodeServerUrl(String nodeServerUrl) {
        this.nodeServerUrl = nodeServerUrl;
    }

    public String getNodeServerPort() {
        return nodeServerPort;
    }

    public void setNodeServerPort(String nodeServerPort) {
        this.nodeServerPort = nodeServerPort;
    }

    public OTMServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(OTMServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public String getSessionUrl() {
        return sessionUrl;
    }

    public void setSessionUrl(String sessionUrl) {
        this.sessionUrl = sessionUrl;
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = taIds;
    }

    public OTMSessionRole getSessionRole() {
        return sessionRole;
    }

    public void setSessionRole(OTMSessionRole sessionRole) {
        this.sessionRole = sessionRole;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public OTMSessionInProgressState getProgressState() {
        return progressState;
    }

    public void setProgressState(OTMSessionInProgressState progressState) {
        this.progressState = progressState;
    }

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public Long getAgoraStartTime() {
        return agoraStartTime;
    }

    public void setAgoraStartTime(Long agoraStartTime) {
        this.agoraStartTime = agoraStartTime;
    }

    public String getParentSessionId() {
        return parentSessionId;
    }

    public void setParentSessionId(String parentSessionId) {
        this.parentSessionId = parentSessionId;
    }

    public String getAgoraReplayUrl() {
        return agoraReplayUrl;
    }

    public void setAgoraReplayUrl(String agoraReplayUrl) {
        this.agoraReplayUrl = agoraReplayUrl;
    }
    
    public OTMSessionType getOtmSessionType() {
        return otmSessionType;
    }

    public void setOtmSessionType(OTMSessionType otmSessionType) {
        this.otmSessionType = otmSessionType;
    }

    public CanvasStreamingType getCanvasStreamingType() {
        return canvasStreamingType;
    }

    public void setCanvasStreamingType(CanvasStreamingType canvasStreamingType) {
        this.canvasStreamingType = canvasStreamingType;
    }

    public List<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<String> attendees) {
        this.attendees = attendees;
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public String getWebinarId() {
        return this.webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public Boolean getIsBigWhiteBoard() {
        return this.isBigWhiteBoard;
    }

    public void setIsBigWhiteBoard(Boolean isBigWhiteBoard) {
        this.isBigWhiteBoard = isBigWhiteBoard;
    }
}
