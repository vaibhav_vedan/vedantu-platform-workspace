package com.vedantu.scheduling.response;

import java.util.List;

public class ActiveSessionsTAResp {
    List<Long> taIds;
    String id; // OTFSession id

    public List<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(List<Long> taIds) {
        this.taIds = taIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ActiveSessionsTAResp() {
    }

    public ActiveSessionsTAResp(List<Long> taIds, String id) {
        this.taIds = taIds;
        this.id = id;
    }
}