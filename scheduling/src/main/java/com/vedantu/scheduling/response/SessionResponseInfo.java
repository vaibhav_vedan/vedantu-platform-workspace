package com.vedantu.scheduling.response;

import java.util.List;

import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.dao.entity.OTFSession;
import java.util.Set;

public class SessionResponseInfo {

    private Set<String> batchIds;
    private String title;
    private String description;
    private String presenter;
    private Long startTime;
    private Long endTime;
    private String sessionURL;
    private List<String> replayUrls;
    private boolean isRescheduled;
    private SessionState state;
    private List<String> attendees;
    private String organizerAccessToken;
    private String presenterURL;

    public SessionResponseInfo(OTFSession oTFSession) {
        super();
        if (oTFSession != null) {
            this.attendees = oTFSession.getAttendees();
            this.batchIds = oTFSession.getBatchIds();
            this.description = oTFSession.getDescription();
            this.endTime = oTFSession.getEndTime();
            this.organizerAccessToken = oTFSession.getOrganizerAccessToken();
            this.replayUrls = oTFSession.getReplayUrls();
            this.title = oTFSession.getTitle();
            this.state = oTFSession.getState();
            this.presenter = oTFSession.getPresenter();
            this.startTime = oTFSession.getStartTime();
            this.sessionURL = oTFSession.getSessionURL();
            this.presenterURL = oTFSession.getPresenterUrl();
        }
    }

    public Set<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    public boolean isIsRescheduled() {
        return isRescheduled;
    }

    public void setIsRescheduled(boolean isRescheduled) {
        this.isRescheduled = isRescheduled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPresenter() {
        return presenter;
    }

    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getSessionURL() {
        return sessionURL;
    }

    public void setSessionURL(String sessionURL) {
        this.sessionURL = sessionURL;
    }

    public List<String> getReplayUrls() {
        return replayUrls;
    }

    public void setReplayUrls(List<String> replayUrls) {
        this.replayUrls = replayUrls;
    }

    public boolean isRescheduled() {
        return isRescheduled;
    }

    public void setRescheduled(boolean isRescheduled) {
        this.isRescheduled = isRescheduled;
    }

    public SessionState getState() {
        return state;
    }

    public void setState(SessionState state) {
        this.state = state;
    }

    public List<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<String> attendees) {
        this.attendees = attendees;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public String getPresenterURL() {
        return presenterURL;
    }

    public void setPresenterURL(String presenterURL) {
        this.presenterURL = presenterURL;
    }
}
