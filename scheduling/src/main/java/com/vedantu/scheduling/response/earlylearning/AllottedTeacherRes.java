package com.vedantu.scheduling.response.earlylearning;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AllottedTeacherRes {

    private String bookingId;
    private String studentId;
    private String presenter;
    private ProficiencyType proficiencyType;
}
