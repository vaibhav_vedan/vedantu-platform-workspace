/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.response;

/**
 *
 * @author parashar
 */
public class SessionsAttendeeInfoRes {
    
    private Long teacherJoinTime;
    
    private Integer countAttendees=0;
    
    private String adminJoined;
    private String techSupport;
    private String finalIssueStatus;

    /**
     * @return the teacherJoinTime
     */
    public Long getTeacherJoinTime() {
        return teacherJoinTime;
    }

    /**
     * @param teacherJoinTime the teacherJoinTime to set
     */
    public void setTeacherJoinTime(Long teacherJoinTime) {
        this.teacherJoinTime = teacherJoinTime;
    }

    /**
     * @return the countAttendees
     */
    public Integer getCountAttendees() {
        return countAttendees;
    }

    /**
     * @param countAttendees the countAttendees to set
     */
    public void setCountAttendees(Integer countAttendees) {
        this.countAttendees = countAttendees;
    }

    /**
     * @return the adminJoined
     */
    public String getAdminJoined() {
        return adminJoined;
    }

    /**
     * @param adminJoined the adminJoined to set
     */
    public void setAdminJoined(String adminJoined) {
        this.adminJoined = adminJoined;
    }

    /**
     * @return the techSupport
     */
    public String getTechSupport() {
        return techSupport;
    }

    /**
     * @param techSupport the techSupport to set
     */
    public void setTechSupport(String techSupport) {
        this.techSupport = techSupport;
    }

    /**
     * @return the finalIssueStatus
     */
    public String getFinalIssueStatus() {
        return finalIssueStatus;
    }

    /**
     * @param finalIssueStatus the finalIssueStatus to set
     */
    public void setFinalIssueStatus(String finalIssueStatus) {
        this.finalIssueStatus = finalIssueStatus;
    }
    
    
    
}
