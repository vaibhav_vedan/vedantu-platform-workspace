package com.vedantu.scheduling.response;

import com.vedantu.scheduling.dao.entity.DemoSession;
import com.vedantu.scheduling.dao.entity.OTFSession;

public class DemoSessionRes {
	private DemoSession demoSession;
	private Long startTime;
	private Long endTime;
	private String sessionURL;
	private String organizerAccessToken;
	private String presenterUrl;

	public DemoSessionRes() {
		super();
	}

	public DemoSessionRes(DemoSession demoSession) {
		super();
		this.demoSession = demoSession;
	}

	public DemoSessionRes(DemoSession demoSession, OTFSession oTFSession) {
		super();
		this.demoSession = demoSession;
		this.startTime = oTFSession.getStartTime();
		this.endTime = oTFSession.getEndTime();
		this.sessionURL = oTFSession.getSessionURL();
		this.organizerAccessToken = oTFSession.getOrganizerAccessToken();
		this.presenterUrl = oTFSession.getPresenterUrl();
	}

	public DemoSession getDemoSession() {
		return demoSession;
	}

	public void setDemoSession(DemoSession demoSession) {
		this.demoSession = demoSession;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getSessionURL() {
		return sessionURL;
	}

	public void setSessionURL(String sessionURL) {
		this.sessionURL = sessionURL;
	}

	public String getOrganizerAccessToken() {
		return organizerAccessToken;
	}

	public void setOrganizerAccessToken(String organizerAccessToken) {
		this.organizerAccessToken = organizerAccessToken;
	}

	public String getPresenterUrl() {
		return presenterUrl;
	}

	public void setPresenterUrl(String presenterUrl) {
		this.presenterUrl = presenterUrl;
	}

	@Override
	public String toString() {
		return "DemoSessionRes [demoSession=" + demoSession + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", sessionURL=" + sessionURL + ", organizerAccessToken=" + organizerAccessToken + ", presenterUrl="
				+ presenterUrl + "]";
	}
}
