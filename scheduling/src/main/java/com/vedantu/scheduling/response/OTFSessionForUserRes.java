package com.vedantu.scheduling.response;

import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OTFSessionForUserRes {
    private String id;
    private Set<String> batchIds;
    private String title;
    private Long boardId;
    private String description;
    private String presenter;
    private Set<Long> taIds;
    private Long startTime;
    private Long endTime;
    private String sessionURL;
    private List<String> replayUrls;
    private OTMSessionType otmSessionType;
    private SessionState state;
    private String rescheduledFromId;
    private EntityType type;
    private String organizerAccessToken;
    private OTFSessionToolType sessionToolType;
    private Set<OTFSessionFlag> flags;
    private Set<SessionLabel> labels;
    private Set<EntityTag> entityTags;
    private String vimeoId;
    private List<ContentInfo> preSessionContents;
    private List<ContentInfo> postSessionContents;
    private List<String> curricullumTopics;
    private List<String> baseTreeTopics;
    private String parentSessionId;
}
