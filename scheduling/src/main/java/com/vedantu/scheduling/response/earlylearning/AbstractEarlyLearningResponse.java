package com.vedantu.scheduling.response.earlylearning;

import com.vedantu.scheduling.enums.EarlyLearningSessionFlag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public abstract class AbstractEarlyLearningResponse {

    private EarlyLearningSessionFlag earlyLearningSessionFlag;

}
