package com.vedantu.scheduling.response.wavesession;


public class CreateTownhallSessionRes {
    private String sessionLink;

    public String getSessionLink() {
        return sessionLink;
    }

    public void setSessionLink(String sessionLink) {
        this.sessionLink = sessionLink;
    }
}
