package com.vedantu.scheduling.response;

import java.util.Arrays;
import java.util.BitSet;

import com.vedantu.scheduling.pojo.BitSetEntry;
import com.vedantu.scheduling.pojo.BitSetUtils;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.request.GetCalendarSlotBitReq;

public class GetCalendarSlotBitRes {
	public String userId;
	public Long startTime;
	public Long endTime;
	public Long slotDuration;
	public CalendarEntrySlotState slotState;
	public long[] bitSetLongArray;

	public GetCalendarSlotBitRes() {
		super();
	}

	public GetCalendarSlotBitRes(GetCalendarSlotBitReq req) {
		super();
		this.userId = req.getUserId();
		this.startTime = req.getStartTime();
		this.endTime = req.getEndTime();
		this.slotDuration = req.getSlotDuration();
		this.slotState = req.getSlotState();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public long[] getBitSetLongArray() {
		return bitSetLongArray;
	}

	public void setBitSetLongArray(long[] bitSetLongArray) {
		this.bitSetLongArray = bitSetLongArray;
	}

	public void setBitSetLongArray(BitSetEntry bitSetEntry) {
		this.startTime = bitSetEntry.getStartTime();
		this.endTime = bitSetEntry.getEndTime();
		this.slotDuration = bitSetEntry.getBitDuration();
		this.slotState = bitSetEntry.getSlotState();
		this.bitSetLongArray = (bitSetEntry.getBitSet() == null
				? BitSetUtils.createBitSetLongArray(this.startTime, this.endTime, this.slotDuration)
				: bitSetEntry.getBitSet().toLongArray());
	}

	public void updateBitSetString() {
		BitSet bitSet = BitSet.valueOf(this.bitSetLongArray);
		int size = BitSetUtils.bitSetSize(startTime, endTime, this.slotDuration);
		StringBuilder sb = new StringBuilder(size);
		for (int i = 0; i < size; i++) {
			if (bitSet.get(i)) {
				sb.append("1");
			} else {
				sb.append("0");
			}
		}
	}

	public Long getSlotDuration() {
		return slotDuration;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public int getBitSetSize() {
		return BitSetUtils.bitSetSize(getStartTime(), getEndTime(), getSlotDuration());
	}

	@Override
	public String toString() {
		return "GetCalendarSlotBitRes [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", slotDuration=" + slotDuration + ", slotState=" + slotState + ", bitSetLongArray="
				+ Arrays.toString(bitSetLongArray) + "]";
	}
}
