package com.vedantu.scheduling.response.earlylearning;

import com.vedantu.scheduling.response.BaseResponse;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class JoinDemoResponse extends BaseResponse{
	
	private String demoId;

}
