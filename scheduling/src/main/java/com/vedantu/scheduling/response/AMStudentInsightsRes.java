package com.vedantu.scheduling.response;

import com.vedantu.scheduling.pojo.InsightResponseData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMStudentInsightsRes {

    private Map<Long, InsightResponseData> insightResponseDataMap;
}

