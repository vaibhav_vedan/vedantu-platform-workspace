/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.async;

import com.google.gson.Gson;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.managers.*;
import com.vedantu.scheduling.pojo.SessionInfo;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.MicroCourseSMSReq;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author somil
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private OTFSessionManager otfSessionManager;

    @Autowired
    private AsyncTaskManager asyncTaskManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private VedantuWaveSessionManager vedantuWaveSessionManager;

    @Autowired
    private MicroCourseSMSManager microCourseSMSManager;

    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case SESSION_EVENTS_TRIGGER:
                SessionInfo sessionInfo = (SessionInfo) payload.get("sessionInfo");
                SessionEvents event = (SessionEvents) payload.get("event");
                sessionManager.triggerSessionEventSNS(event, sessionInfo);
                break;
            case HANDLE_LIVE_SESSION_TOOL_TYPE:
                Long sessionId = (Long) payload.get("sessionId");
                SessionEvents sessionEvents = (SessionEvents) payload.get("event");
                sessionManager.handleLiveSessionToolType(sessionEvents, sessionId);
                break;
            case CREATE_SESSION_LINK:
                OTFSession otfSession = (OTFSession) payload.get("session");
                otfSessionManager.createSessionLink(otfSession);
                break;
            case SEND_OTM_EXTRA_CLASS_EMAIL:
                OTFSession session = (OTFSession) payload.get("session");
                communicationManager.sendOtmExtraClassEmail(session);
                break;
            case SEND_OTM_SESSION_RELATED_EMAILS:
                OTFSession session1 = (OTFSession) payload.get("session");
                HashMap<String, Object> bodyScopes = (HashMap<String, Object>) payload.get("bodyScopes");
                CommunicationType communicationType = (CommunicationType) payload.get("communicationType");
                boolean teacher = (boolean) payload.get("teacher");
                boolean sms = (boolean) payload.get("sms");
                communicationManager.sendOtmNewSessionRelatedEmailToAllParticipants(session1, bodyScopes, communicationType, teacher, sms);
                break;
            case TRIGGER_SNS:
                SNSTopic topic = (SNSTopic) payload.get("topic");
                String snsSubject = (String) payload.get("subject");
                String snsMessage = (String) payload.get("message");
                awsSNSManager.triggerSNS(topic, snsSubject, snsMessage);
                break;
            case SYNC_OTF_POLLS_DATA:
                otfSessionManager.syncPollsData();
                break;
            case SEND_OTF_POST_SESSION_EMAILS:
                otfSessionManager.sendPostSessionEmails();
                break;
            case SEND_OTM_TRIAL_SESSION_EMAILS:
                otfSessionManager.prepareToSendSessionEmails();
                break;
            case SEND_LIVE_SESSION_EMAILS:
                otfSessionManager.prepareToSendSessionLiveEmails();
                break;
            case SEND_EARLY_LEARNING_CLASS_SMS:
                otfSessionManager.sendEarlyLearningClassSms();
                break;
            case SEND_MODULAR_REGULAR_SESSION_EMAILS:
                otfSessionManager.sentEmailForModularBatchSession();
                break;
            case SEND_OTO_POST_SESSION_EMAILS:
                sessionManager.sendPostSessionEmails();
                break;
            case SEND_OTM_NO_SHOW_EMAIL:
                otfSessionManager.sendOtmNoShowEmails();
                break;
            case UPDATE_ENROLLMENT_EVENTS:
                EnrollmentPojo old = (EnrollmentPojo) payload.get("previousEnrollment");
                EnrollmentPojo newEn = (EnrollmentPojo) payload.get("newEnrollment");
//                asyncTaskManager.markCalendarForEnrollment(old, newEn);
                asyncTaskManager.createGTTAttendeeForEnrollment(old, newEn);
                break;
            case CREATE_GTT_ATTENDEES_NEW_ENROLLMENT:
                EnrollmentPojo newEn1 = (EnrollmentPojo) payload.get("newEnrollment");
                asyncTaskManager.createGTTAttendeeForEnrollment(null, newEn1);
                break;
            case SESSION_EVENTS_TRIGGER_OTF: {
                OTFSessionPojo otfSessionPojo = null;
                OTFSessionPojoUtils otfSessionPojoUtils = null;
                if (payload.get("sessionInfo") instanceof OTFSessionPojo) {
                    otfSessionPojo = (OTFSessionPojo) payload.get("sessionInfo");
                }
                if (payload.get("sessionInfo") instanceof OTFSessionPojoUtils) {
                    otfSessionPojoUtils = (OTFSessionPojoUtils) payload.get("sessionInfo");
                }
                SessionEventsOTF event1 = (SessionEventsOTF) payload.get("event");
                String subject = null;

                OTFSessionPojoUtils snsPojo = new OTFSessionPojoUtils();
                if (otfSessionPojo != null) {
                    snsPojo.setId(otfSessionPojo.getId());
                    snsPojo.setBatchIds(otfSessionPojo.getBatchIds());
                    snsPojo.setPresenter(otfSessionPojo.getPresenter());
                    if (CollectionUtils.size(otfSessionPojo.getAttendees()) <= 500) {
                        snsPojo.setAttendees(otfSessionPojo.getAttendees());
                    }
                }
                if (otfSessionPojoUtils != null) {
                    snsPojo.setId(otfSessionPojoUtils.getId());
                    snsPojo.setBatchIds(otfSessionPojoUtils.getBatchIds());
                    snsPojo.setPresenter(otfSessionPojoUtils.getPresenter());
                    snsPojo.setPreviousPresenter(otfSessionPojoUtils.getPreviousPresenter());
                    if (CollectionUtils.size(otfSessionPojoUtils.getAttendees()) <= 500) {
                        snsPojo.setAttendees(otfSessionPojoUtils.getAttendees());
                    }
                }
                if (event1 != null) {
                    subject = event1.name();
                }
                String message = new Gson().toJson(snsPojo);
                sessionManager.triggerSNS(SNSTopicOTF.SESSION_EVENTS_OTF, subject, message);
                break;
            }
            case MARK_CALENDAR:
                List<CalendarEntryReq> requests = (List<CalendarEntryReq>) payload.get("calendarRequests");
                boolean mark = (boolean) payload.get("mark");
                asyncTaskManager.updateCalendarForRequest(requests, mark);
                break;
            case RESCHEDULE_SESSION_EMAIL:
                OTFSession preUpdateSession = (OTFSession) payload.get("preUpdateSession");
                OTFSession result = (OTFSession) payload.get("result");
                communicationManager.sendSessionRescheduledEmail(preUpdateSession, result);
                break;
            case ADD_JOIN_DETAILS:
                String joinedotfsessionId = (String) payload.get("otfsessionId");
                String joinedUserId = (String) payload.get("userId");
                if (!StringUtils.isEmpty(payload.get("ipAddress"))) {
                    String userAgent = null;
                    if (payload.get("userAgent") != null) {
                        userAgent = (String) payload.get("userAgent");
                    }
                    String ipAddress = (String) payload.get("ipAddress");
                    otfSessionManager.addJoinDetails(joinedotfsessionId, joinedUserId, ipAddress, userAgent);
                } else {
                    logger.error("No ip address found for updating join details for "
                            + joinedotfsessionId + " " + joinedUserId);
                }
                break;
            case CREATE_CURRENT_DAY_SOCIALVID_CONFERENCES:
                vedantuWaveSessionManager.createCurrentDaySocialVidConferencesTask();
                break;
            case LAUNCH_OTM_SESSION_RECORDERS:
                vedantuWaveSessionManager.launchOTMSessionRecorders();
                break;
            case CHECK_OTM_SESSION_RECORDERINGS:
                vedantuWaveSessionManager.checkOTMSessionRecordings();
                break;
            case END_OTM_VEDANTU_WAVE_ACTIVE_SESSIONS:
                vedantuWaveSessionManager.endActiveSessions();
                break;
            case CACHE_SCHEDULING_COLLECTIONS:
                int cronTime = (int) payload.get("cronTime");
                vedantuWaveSessionManager.cacheSchedulingCollections(cronTime);
                break;
            case CACHE_GTT_ATTENDEE_COLLECTIONS:
                vedantuWaveSessionManager.cacheGTTAttendeeCollections();
                break;
            case SEND_VEDANTU_WAVE_SESSION_LATE_JOIN_SMS:
                vedantuWaveSessionManager.sendLateJoinSMS();
                break;
            case SEND_ABSENTEE_SMS:
                vedantuWaveSessionManager.sendAbsenteeSMS();
                break;
            case SEND_HOURLY_MICROCOURSES_REMAINDER:
                MicroCourseSMSReq hourlySMSReq = (MicroCourseSMSReq) payload.get("hourlyRemainderReq");
                microCourseSMSManager.sendHourlySMS(hourlySMSReq);
                break;
            case SEND_FIFTEEN_MINS_MICROCOURSES_REMAINDER:
                MicroCourseSMSReq fifteenMinSMSReq = (MicroCourseSMSReq) payload.get("fifteenMinRemReq");
                microCourseSMSManager.sendFifteenMinSMS(fifteenMinSMSReq);
                break;
            case SEND_LIVE_MICROCOURSES_REMAINDER:
                MicroCourseSMSReq liveSMSReq = (MicroCourseSMSReq) payload.get("liveRemReq");
                microCourseSMSManager.sendLiveSMS(liveSMSReq);
                break;
            case ADDED_SESSIONS_MARK_CALENDAR:
                List<OTFSession> sessions = (List<OTFSession>) payload.get("sessions");
                asyncTaskManager.updateCalendarForAddedSessions(sessions, true, null);
                break;
            case BATCH_SESSIONS_MARK_CALENDAR:
                List<OTFSession> sessionsb = (List<OTFSession>) payload.get("sessions");
                asyncTaskManager.updateCalendarForAddedSessions(sessionsb, true, null);
                break;
            case BATCH_SESSIONS_UNMARK_CALENDAR:
                List<OTFSession> sessionsc = (List<OTFSession>) payload.get("sessions");
                asyncTaskManager.updateCalendarForAddedSessions(sessionsc, false, null);
                break;                
            case GAME_GTT_SESSION_CRON:
                logger.info("GTT SESSION CRON @ " + System.currentTimeMillis());
                gttAttendeeDetailsManager.processGameSessionCronAsync();
                break;
            case ADD_ATTENDEES_TO_SESSION_ON_BATCHIDS_ADDITION:
                List<String> batchIds = (List<String>) payload.get("batchIds");
                String sessionId2 = (String) payload.get("sessionId");
                asyncTaskManager.addAttendeesToSessionFromBatchIds(batchIds, sessionId2);
                break;
            default:
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
