package com.vedantu.scheduling.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.managers.AMInclassManager;
import com.vedantu.scheduling.pojo.AMInclassAggregateResp;
import com.vedantu.scheduling.response.StudentInclassDetailedRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

@RestController
@RequestMapping("am-inclass")
public class AMInclassController
{
    @Autowired
    private AMInclassManager amInclassManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AMInclassController.class);

    @RequestMapping(value = "/getInClassAggregate/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AMInclassAggregateResp getInClassAggregate(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws VException, UnsupportedEncodingException
    {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        return amInclassManager.getAMInClassAggregate(studentId, fromTime, thruTime);
    }

    @RequestMapping(value = "/getInClassDetailed/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public StudentInclassDetailedRes getInClassDetailed(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,@RequestParam(name = "includeSession",required = false) Boolean includeSession) throws VException, UnsupportedEncodingException
    {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        return amInclassManager.getAMInClassDetailed(studentId, fromTime, thruTime,includeSession!=null?includeSession:false);
    }


    //TODO implement app to app check
    @RequestMapping(value = "/hasBadAttendanceInPast3Sessions/{studentId}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String hasBadAttendanceInPast3Sessions(@PathVariable("studentId") Long studentId, @RequestParam(name = "creationTime", required = true) Long creationTime) throws VException, UnsupportedEncodingException
    {
        return "" + amInclassManager.hasBadAttendanceInPast3Sessions(studentId, creationTime);
    }

    //TODO implement app to app check
    @RequestMapping(value = "/hasBadAttendanceInPastMonth/{studentId}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String hasBadAttendanceInPastMonth(@PathVariable("studentId") Long studentId, @RequestParam(name = "creationTime", required = true) Long creationTime) throws VException, UnsupportedEncodingException
    {
        return "" + amInclassManager.hasBadAttendanceInPastMonth(studentId, creationTime);

    }

    //TODO implement app to app check
    @RequestMapping(value = "/isLessThan20PercentEngagement/{studentId}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String isLessThan20PercentEngagement(@PathVariable("studentId") Long studentId, @RequestParam(name = "creationTime", required = true) Long creationTime) throws VException, UnsupportedEncodingException
    {
        return "" + amInclassManager.isLessThan20PercentEngagement(studentId, creationTime);
    }

}
