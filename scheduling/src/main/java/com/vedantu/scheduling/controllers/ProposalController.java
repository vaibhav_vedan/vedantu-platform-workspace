/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.scheduling.managers.ProposalManager;
import com.vedantu.scheduling.request.proposal.AddProposalReq;
import com.vedantu.scheduling.request.proposal.CancelSubscriptionProposalsReq;
import com.vedantu.scheduling.request.proposal.GetProposalReq;
import com.vedantu.scheduling.request.proposal.GetProposalsReq;
import com.vedantu.scheduling.response.proposal.ProposalRes;
import com.vedantu.scheduling.request.proposal.UpdateProposalReq;
import com.vedantu.scheduling.response.proposal.GetProposalsRes;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/proposal")
public class ProposalController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ProposalManager proposalManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionController.class);

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProposalRes addProposal(@RequestBody AddProposalReq addProposalReq) {
        return proposalManager.addProposal(addProposalReq);
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProposalRes confirmProposal(@RequestBody UpdateProposalReq updateProposalReq) throws NotFoundException, ForbiddenException, BadRequestException {
        return proposalManager.confirmProposal(updateProposalReq);
    }

    @RequestMapping(value = "/getProposal", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProposalRes getProposal(GetProposalReq getProposalReq) throws NotFoundException, ForbiddenException {
        return proposalManager.getProposal(getProposalReq);
    }
    
    @Deprecated
    @RequestMapping(value = "/getProposals", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetProposalsRes getProposals(GetProposalsReq getProposalsReq) throws NotFoundException, ForbiddenException {
        return new GetProposalsRes();
//        return proposalManager.getProposals(getProposalsReq);
    }   
    
    @Deprecated
    @RequestMapping(value = "/cancelSubscriptionProposals", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicResponse cancelSubscriptionProposals(@RequestBody CancelSubscriptionProposalsReq req) throws BadRequestException{
        return new BasicResponse();
//        return proposalManager.cancelSubscriptionProposals(req);
    }       
}
