package com.vedantu.scheduling.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.enums.SubRole;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.*;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import com.vedantu.scheduling.dao.entity.SessionAttendanceAvg;
import com.vedantu.scheduling.managers.LOAMAmbassadorManager;
import com.vedantu.scheduling.request.AMStudentInsightsReq;
import com.vedantu.scheduling.response.AMInClassDoubtDataRes;
import com.vedantu.scheduling.response.AMInClassDoubtStatRes;
import com.vedantu.scheduling.response.AMStudentInsightsRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping("loam-ambassador-sched")
public class LOAMAmbassadorController {
    @Autowired
    private LOAMAmbassadorManager lOAMAmbassadorManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LOAMAmbassadorController.class);

    //Used for LOAM Inclass Doubts
    @RequestMapping(value = "/loam_getSessionIdsForBoardId/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Set<OTFSession> loam_getSessionIdsForBoardId(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime, @RequestParam(name = "boardId") Long boardId, @RequestParam(name = "chunk") Integer chunk) throws VException, UnsupportedEncodingException {
        checkAuthorization();
        return lOAMAmbassadorManager.loam_getSessionIdsForBoardId(studentId, fromTime, thruTime, boardId, chunk);
    }

    //---------------------------------To update LOAM corresponding tables/collections-----------------------------------------------------//
    @RequestMapping(value = "/loam_getOTFSessionData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFSession> loam_getOTFSessionData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return lOAMAmbassadorManager.loam_getOTFSessionData(fromTime, thruTime);
    }

    @RequestMapping(value = "/loam_getGTTAttendeeDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GTTAttendeeDetails> loam_getGTTAttendeeDetails(@RequestParam(name = "fromTime", required = true) Long fromTime,
                                                               @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                               @RequestParam(name = "start", required = true) Integer start,
                                                               @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return new ArrayList<>();
        //        return lOAMAmbassadorManager.loam_getGTTAttendeeDetails(fromTime, thruTime,start,size);
    }


    @RequestMapping(value = "/loam_getOTMSessionEngagementData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTMSessionEngagementData> loam_getOTMSessionEngagementData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
        //        return lOAMAmbassadorManager.loam_getOTMSessionEngagementData(fromTime, thruTime);
    }

    /**
     * checkAuthorization
     *
     * @throws VException
     */
    private void checkAuthorization() throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);

        if (sessionUtils.getCurrentSessionData() != null) {
            Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();

            if (subRoles == null || !(subRoles.contains(SubRole.ACADMENTOR))) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User other than Acedemic Mentor are forbidden to use this API");
            }
        } else {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Session is Expired. Please Login again.");
        }
    }

}
