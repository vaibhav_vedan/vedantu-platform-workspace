package com.vedantu.scheduling.controllers;

import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.vedantu.scheduling.managers.InstaRequestManager;
import com.vedantu.scheduling.request.instalearn.AddInstaRequestReq;
import com.vedantu.scheduling.request.instalearn.GetInstaRequestsReq;
import com.vedantu.scheduling.request.instalearn.SetTeacherSubscriptionReq;
import com.vedantu.scheduling.request.instalearn.UpdateInstaRequestReq;
import com.vedantu.scheduling.response.InstaActiveMissedResponse;
import com.vedantu.scheduling.response.instalearn.GetInstaRequestsRes;
import com.vedantu.scheduling.response.instalearn.SubmitInstaRequestRes;
import com.vedantu.scheduling.response.instalearn.TeacherSubscriptionResponse;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("instalearn")
public class InstalearnController {

    @Autowired
    private InstaRequestManager instaRequestManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalearnController.class);

    @RequestMapping(value = "/addRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitInstaRequestRes addInstaRequest(@RequestBody AddInstaRequestReq addInstaRequestReq) {
        return instaRequestManager.addInstaRequest(addInstaRequestReq);
    }    


    @RequestMapping(value = "/submitRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitInstaRequestRes submitInstaRequest(@RequestBody UpdateInstaRequestReq updateInstaRequestReq) throws NotFoundException, ConflictException, ForbiddenException {
        return instaRequestManager.submitInstaRequest(updateInstaRequestReq);
    }


    @RequestMapping(value = {"getInstaRequests"}, method = RequestMethod.GET)
    @ResponseBody
    public GetInstaRequestsRes getInstaRequests(GetInstaRequestsReq getInstaRequestsReq){

        GetInstaRequestsRes getRequestsResponse = instaRequestManager.getInstaRequests(getInstaRequestsReq);
        return getRequestsResponse;
    }

    @RequestMapping(value = "getRequestById/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SubmitInstaRequestRes getInstaRequest(@PathVariable("id") String id) throws Exception {
        SubmitInstaRequestRes instaProposalResponse = instaRequestManager.getInstaRequest(id);
        return instaProposalResponse;
    }

    @RequestMapping(value = {"getTeacherILRequestsCount/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public InstaActiveMissedResponse getTeacherILRequestsCount(@PathVariable("id") Long id) throws Exception {
        InstaActiveMissedResponse instaActiveMissedResponse = instaRequestManager
                .getInstaActiveMissedResponseByUserId(id);
        return instaActiveMissedResponse;
    }

    @RequestMapping(value = "/setTeacherSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TeacherSubscriptionResponse setTeacherSubscription(@RequestBody SetTeacherSubscriptionReq setTeacherSubscriptionReq)
            {

        TeacherSubscriptionResponse teacherSubscriptionResponse = instaRequestManager.setTeacherSubscription(setTeacherSubscriptionReq);
        return teacherSubscriptionResponse;
    }

    @RequestMapping(value = {"getTeacherSubscription/{userId}"}, method = RequestMethod.GET)
    @ResponseBody
    public TeacherSubscriptionResponse getTeacherSubscription(@PathVariable("userId") Long userId,
                                                              @RequestParam(value = "authorize", defaultValue = "true") boolean authorize) throws VException {
        /*if (authorize) {
            httpSessionUtils.checkIfAllowed(userId, null, true);
        }
        TeacherSubscriptionResponse teacherSubscriptionResponse = instaRequestManager.getTeacherSubscription(userId);
        return teacherSubscriptionResponse;*/
        return new TeacherSubscriptionResponse();
    }
}
