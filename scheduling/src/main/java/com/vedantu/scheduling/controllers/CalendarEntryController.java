package com.vedantu.scheduling.controllers;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;


import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.scheduling.response.AvailabilitySlotStringRes;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;
import com.vedantu.exception.ErrorCode;
import lombok.NonNull;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.scheduling.dao.entity.AvailabilityRange;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.managers.CalendarSelectionManager;
import com.vedantu.scheduling.managers.CalendarUtilsManager;
import com.vedantu.scheduling.pojo.SelectedSlotResponse;
import com.vedantu.scheduling.pojo.calendar.GetLastMarkedDayResponse;
import com.vedantu.scheduling.pojo.calendar.RemoveCalendarEntryReq;
import com.vedantu.scheduling.request.CalendarEntryReq;
import com.vedantu.scheduling.request.CreateAvailabilityRangesRequest;
import com.vedantu.scheduling.request.GetCalendarSlotBitReq;
import com.vedantu.scheduling.request.UpdateCalendarSlotBitsReq;
import com.vedantu.scheduling.request.UpdateCalendarSlotStringReq;
import com.vedantu.scheduling.response.AvailabilityResponse;
import com.vedantu.scheduling.response.BasicCalendarSlotResponse;
import com.vedantu.scheduling.response.BasicRes;
import com.vedantu.scheduling.response.GetCalendarSlotBitRes;
import com.vedantu.scheduling.response.GetCalendarSlotStringRes;
import com.vedantu.scheduling.response.SlotSelectionPojo;
import com.vedantu.scheduling.response.UpdateCalendarSlotBitsRes;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SlotCheckPojo;
import com.vedantu.subscription.request.BlockSlotScheduleReq;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.scheduling.SyncAllCalendarReq;
import com.vedantu.util.scheduling.SyncCalendarSessionsReq;
import com.vedantu.util.subscription.BlockSlotScheduleRes;
import com.vedantu.util.subscription.GetSlotConflictReq;
import com.vedantu.util.subscription.GetSlotConflictRes;
import com.vedantu.util.subscription.ResolveSlotConflictReq;
import com.vedantu.util.subscription.ResolveSlotConflictRes;
import com.vedantu.util.subscription.UpdateCalendarSessionSlotsReq;


@RestController
public class CalendarEntryController {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private CalendarEntryManager calendarEntryManager;

	@Autowired
	private CalendarUtilsManager calendarUtilsManager;

	@Autowired
	private CalendarSelectionManager calendarSelectionManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEntryController.class);

	@RequestMapping(value = "/calendarEntry", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<CalendarEntry> postCalendarEntries(@RequestBody CalendarEntryReq calendarEntryReq) throws Exception {
		logger.info("postCalendarEntries request : " + calendarEntryReq);
		List<CalendarEntry> calendarEntries = calendarEntryManager.upsertCalendarEntry(calendarEntryReq);
		logger.info("postCalendarEntries response : " + calendarEntries == null ? 0 : calendarEntries.size());
		return calendarEntries;
	}

	@RequestMapping(value = "/calendarEntry/multiple", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicRes calendarEntryMultiple(@RequestBody List<CalendarEntryReq> calendarEntryReq)
			throws Exception {
		logger.info("postCalendarEntries request : " + calendarEntryReq);
		if (calendarEntryReq.size() > 100) {
			throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "Can not mark more than 100 calendar entries at once");
		}
		String callingUserId = null;
		if (sessionUtils.getCallingUserId() != null) {
			callingUserId = sessionUtils.getCallingUserId().toString();
		}

		BasicRes res = calendarEntryManager.upsertCalendarEntriesBulk(calendarEntryReq, callingUserId);
		return res;
	}

	@RequestMapping(value = "/calendarEntry/{id}/removeSlots", method = RequestMethod.POST)
	public BasicRes removeSlots(@ModelAttribute RemoveCalendarEntryReq removeCalendarEntryReq, @PathVariable(value = "id") String calendarEntryId) throws Exception {
		logger.info("removeSlots request : " + removeCalendarEntryReq.toString());
		removeCalendarEntryReq.setCalendarEntryId(calendarEntryId);
		BasicRes res = calendarEntryManager.removeCalendarEntrySlots(removeCalendarEntryReq);
		logger.info("removeSlots response : " + res);
		return res;
	}

	@RequestMapping(value = "/calendarEntry/availableTeachers", method = RequestMethod.GET)
	public List<String> getAvailableUsers(@RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime, @RequestParam("userIds") String[] userIdsParams) throws Exception {
		List<String> userIds = Arrays.asList(userIdsParams);
		logger.info("getAvailableUsers request : startTime - " + startTime + " , endTime - " + endTime
				+ " userids list " + userIds.toString());
		List<String> availableUsers = calendarEntryManager.getAvailableUsers(startTime, endTime, userIds);
		logger.info("getAvailableUsers response : count " + availableUsers == null ? 0 : availableUsers.size());
		return availableUsers;
	}

	@RequestMapping(value = "/calendarEntry/availableStudents", method = RequestMethod.GET)
	public List<String> getAvailableStudents(@RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime, @RequestParam("userIds") String[] userIdsParams) throws Exception {
		List<String> userIds = Arrays.asList(userIdsParams);
		logger.info("getAvailableUsers request : startTime - " + startTime + " , endTime - " + endTime
				+ " userids list " + userIds.toString());
		List<String> availableUsers = calendarEntryManager.getAvailableStudents(startTime, endTime, userIds, true);
		logger.info("getAvailableUsers response : count " + availableUsers == null ? 0 : availableUsers.size());
		return availableUsers;
	}

	@RequestMapping(value = "/calendarEntry/availableTeachersBulk", method = RequestMethod.GET)
	public List<String> getavailableTeachers(@RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime, @RequestParam("userIds") String[] userIdsParams) throws Exception {
		List<String> userIds = Arrays.asList(userIdsParams);
		logger.info("getAvailableUsers request : startTime - " + startTime + " , endTime - " + endTime
				+ " userids list " + userIds.toString());
		List<String> availableUsers = calendarEntryManager.getAvailableTeachers(startTime, endTime, userIds);
		logger.info("getAvailableUsers response : count " + availableUsers == null ? 0 : availableUsers.size());
		return availableUsers;
	}

	@RequestMapping(value = "/calendarEntry/slotListAvailability", method = RequestMethod.POST)
	public List<SessionSlot> slotListAvailability(@RequestBody SlotCheckPojo slotCheckPojo) throws Exception {
		return calendarEntryManager.getSlotListAvailability(slotCheckPojo.getSlots(),
				String.valueOf(!CollectionUtils.isEmpty(slotCheckPojo.getTeacherIds()) ? slotCheckPojo.getTeacherIds().get(0) : null),
				String.valueOf(!CollectionUtils.isEmpty(slotCheckPojo.getStudentIds()) ? slotCheckPojo.getStudentIds().get(0) : null));
	}

	@RequestMapping(value = "/calendarEntry/durationAvailableTeachers", method = RequestMethod.GET)
	public List<String> getDurationAvailableUsers(@RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime, @RequestParam("duration") Long duration) throws Exception {
		logger.info("getDurationAvailableUsers : startTime - " + startTime + ", endTime - " + endTime + ", duration - "
				+ duration);
		List<String> availableUsers = calendarEntryManager.getUsersWithFreeSlots(startTime, endTime, duration);
		logger.info("getDurationAvailableUsers response : count " + availableUsers == null ? 0 : availableUsers.size());
		return availableUsers;
	}

	@RequestMapping(value = "/calendarEntry/getIndianTimeInterval", method = RequestMethod.GET)
	public String getDurationAvailableUsers(@RequestParam("start") Integer start, @RequestParam("end") Integer end,
			@RequestParam("dayStartTime") Long dayStartTime) throws Exception {

		String s = "";
		Long startTime = dayStartTime + start * DateTimeUtils.MILLIS_PER_HOUR / 4;
		Long endTime = dayStartTime + end * DateTimeUtils.MILLIS_PER_HOUR / 4;

		Date startTimeDate = new Date(startTime);
		Date endTimeDate = new Date(endTime);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		String formattedDate = sdf.format(startTimeDate);
		s = s + formattedDate + "   TO  ";
		formattedDate = sdf.format(endTimeDate);
		s = s + formattedDate;
		return s;
	}

	@RequestMapping(value = "/calendarEntry/getEpoch", method = RequestMethod.GET)
	public String getDurationAvailableUsers(@RequestParam("start") Date start, @RequestParam("end") Date end)
			throws Exception {

		String s = "";
		long epochStart = start.getTime();
		long epochEnd = end.getTime();
		s = epochStart + " TO " + epochEnd;
		return s;
	}

	@RequestMapping(value = "/calendarEntry/getLastAvailableEntry", method = RequestMethod.GET)
	public List<CalendarEntry> getLastMarkedEntry(@RequestParam("userId") String userId) throws Exception {

		List<CalendarEntry> calendarEntries = calendarUtilsManager.getLastMarkedEntry(userId);
		return calendarEntries;
	}

	@RequestMapping(value = "/calendarEntry/getLastMarkedDay", method = RequestMethod.GET)
	public GetLastMarkedDayResponse getLastMarkedDay(@RequestParam("userId") String userId) throws Exception {
		logger.info("Request - " + userId);
		Long lastMarkedTime = calendarUtilsManager.getLastMarkedTime(userId);
		logger.info("Response - " + lastMarkedTime);
		return new GetLastMarkedDayResponse(userId, lastMarkedTime);
	}

	@RequestMapping(value = "/calendarEntries", method = RequestMethod.GET)
	public List<CalendarEntry> getCalendarEntries(@RequestParam("userId") String userId,
			@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws Exception {
		logger.info("getCalendarEntries request : userId - " + userId + ", startTime - " + startTime + ", endTime - "
				+ endTime);
		List<CalendarEntry> calendarEntries = calendarUtilsManager.getCalendarEntries(userId, startTime, endTime);
		logger.info("getCalendarEntries response : count - " + calendarEntries == null ? 0 : calendarEntries.size());
		return calendarEntries;
	}

	@RequestMapping(value = "/bulkFetchCalendarEntries", method = RequestMethod.GET)
	public List<CalendarEntry> getBulkCalendarEntries(@RequestParam("userIds") List<String> userIds,
			@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws Exception {
		logger.info("getCalendarEntries request : userId - " + userIds + ", startTime - " + startTime + ", endTime - "
				+ endTime);
		List<CalendarEntry> calendarEntries = calendarEntryManager.getBulkCalendarEntries(userIds, startTime, endTime);
		logger.info("getCalendarEntries response : count - " + calendarEntries == null ? 0 : calendarEntries.size());
		return calendarEntries;
	}

	@RequestMapping(value = "/calendarEntry/trueAvailability", method = RequestMethod.GET)
	public AvailabilityResponse getTrueAvailability(@RequestParam("studentId") String studentId,
			@RequestParam("teacherId") String teacherId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime,
			@RequestParam(name = "neglectReferenceId", required = false) String neglectReferenceId) throws Exception {
		logger.info("getCalendarEntries request : studentId - " + studentId + ", teacherId - " + teacherId
				+ ", startTime - " + startTime + ", endTime - " + endTime + ", neglectReferenceId - "
				+ neglectReferenceId);
		try {
			AvailabilityResponse response = calendarEntryManager.getTrueAvailability(startTime, endTime, teacherId,
					studentId, neglectReferenceId, false);
			logger.info("getCalendarEntries response : count - " + response);
			return response;
		} catch (Exception ex) {
			logger.error("getTrueAvailability : error occured - " + ex.getMessage() + " for request : studentId - "
					+ studentId + ", teacherId - " + teacherId + ", startTime - " + startTime + ", endTime - "
					+ endTime);
			throw ex;
		}
	}

	@RequestMapping(value = "/calendarEntry/truerAvailability", method = RequestMethod.GET)
	public AvailabilityResponse getTruerAvailability(@RequestParam("studentId") String studentId,
			@RequestParam("teacherId") String teacherId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime, @RequestParam("match") float match,
			@RequestParam("repeat") int repeat,
			@RequestParam(name = "neglectReferenceId", required = false) String neglectReferenceId) throws Exception {
		logger.info("getCalendarEntries request : studentId - " + studentId + ", teacherId - " + teacherId
				+ ", startTime - " + startTime + ", endTime - " + endTime + " match : " + match + " repeat : " + repeat
				+ ", neglectReferenceId - " + neglectReferenceId);
		try {
			AvailabilityResponse response = calendarEntryManager.getTruerAvailability(startTime, endTime, teacherId,
					studentId, repeat, match, neglectReferenceId);
			logger.info("getCalendarEntries response : count - " + response);
			return response;
		} catch (Exception ex) {
			logger.error("getTruerAvailability : error occured - " + ex.getMessage() + " for request : studentId - "
					+ studentId + ", teacherId - " + teacherId + ", startTime - " + startTime + ", endTime - "
					+ endTime);
			throw ex;
		}
	}

	@RequestMapping(value = "/calendarEntry/updateSlotBits", method = RequestMethod.POST)
	public BasicCalendarSlotResponse updateCalendarSlotBits(@RequestBody UpdateCalendarSlotBitsReq request)
			throws Exception {
		logger.info("updateCalendarSlotBitsRequest : " + request.toString());
		BasicCalendarSlotResponse response = new BasicCalendarSlotResponse();
		try {
			UpdateCalendarSlotBitsRes updateCalendarSlotBitsRes = calendarEntryManager.updateCalendarSlotBits(request);
			response.updateResult(updateCalendarSlotBitsRes);
		} catch (ConflictException ex) {
			logger.info("updateCalendarSlotBitsTransactionPending - pending transaction for request : "
					+ request.toString());
			response.setErrorFields(ex);
		} catch (BadRequestException ex) {
			logger.info("updateCalendarSlotBitsBadReq - Bad request error " + request.toString() + " message : "
					+ ex.getErrorMessage());
			response.setErrorFields(ex);
		}

		logger.info("updateCalendarSlotBitsResponse : " + response);
		return response;
	}

	@RequestMapping(value = "/calendarEntry/updateSlotString", method = RequestMethod.POST)
	public BasicCalendarSlotResponse updateCalendarSlotString(@RequestBody UpdateCalendarSlotStringReq request)
			throws Exception {
		logger.info("updateCalendarSlotBitsRequest : " + request.toString());
		BasicCalendarSlotResponse response = new BasicCalendarSlotResponse();
		try {
			request.setOldBitSetLongArray();
			request.resetOldBitSetString();
			request.setNewBitSetLongArray();
			request.resetNewBitSetString();
			UpdateCalendarSlotBitsRes updateCalendarSlotBitsRes = calendarEntryManager.updateCalendarSlotBits(request);
			response.updateResult(updateCalendarSlotBitsRes);
		} catch (ConflictException ex) {
			logger.info("updateCalendarSlotBitsTransactionPending - pending transaction for request : "
					+ request.toString());
			response.setErrorFields(ex);
		} catch (BadRequestException ex) {
			logger.info("updateCalendarSlotBitsBadReq - Bad request error " + request.toString() + " message : "
					+ ex.getErrorMessage());
			response.setErrorFields(ex);
		}

		logger.info("updateCalendarSlotBitsResponse : " + response);
		return response;
	}

	@RequestMapping(value = "/calendarEntry/getSlotBits", method = RequestMethod.GET)
	public BasicCalendarSlotResponse getCalendarSlotBits(@ModelAttribute GetCalendarSlotBitReq request)
			throws Exception {
		logger.info("getCalendarSlotsRequest : " + request.toString());
		BasicCalendarSlotResponse response = new BasicCalendarSlotResponse();
		try {
			GetCalendarSlotBitRes getCalendarSlotBitRes = calendarEntryManager.getCalendarSlotBits(request);
			response.updateResult(getCalendarSlotBitRes);
		} catch (ConflictException ex) {
			logger.info("getCalendarSlotsTransactionPending - pending transaction for request : " + request.toString());
			response.setErrorCode(ex.getErrorCode().name());
			response.setErrorMessage(ex.getErrorMessage());
		} catch (BadRequestException ex) {
			String errorMessage = "getCalendarSlotsError - Exception occured with message : " + ex.getMessage()
					+ " request : " + request.toString();
			logger.info(errorMessage + " stackTrace : " + ex.getStackTrace());
			response.setErrorFields(ex);
		}
		logger.info("getCalendarSlotsResponse : " + response);
		return response;
	}

	@RequestMapping(value = "/calendarEntry/getSlotString", method = RequestMethod.GET)
	public BasicCalendarSlotResponse getCalendarSlotString(@ModelAttribute GetCalendarSlotBitReq request)
			throws Exception {
		BasicCalendarSlotResponse response = new BasicCalendarSlotResponse();
		try {
			GetCalendarSlotBitRes getCalendarSlotBitRes = calendarEntryManager.getCalendarSlotBits(request);
			GetCalendarSlotStringRes getCalendarSlotStringRes = new GetCalendarSlotStringRes(getCalendarSlotBitRes);
			response.updateResult(getCalendarSlotStringRes);
		} catch (ConflictException ex) {
			logger.info("getCalendarSlotsTransactionPending - pending transaction for request : " + request.toString());
			response.setErrorCode(ex.getErrorCode().name());
			response.setErrorMessage(ex.getErrorMessage());
		} catch (BadRequestException ex) {
			String errorMessage = "getCalendarSlotsError - Exception occured with message : " + ex.getMessage()
					+ " request : " + request.toString();
			logger.info(errorMessage + " stackTrace : " + ex.getStackTrace());
			response.setErrorFields(ex);
		}
		return response;
	}

	@RequestMapping(value = "/bitSet/getBinaryString", method = RequestMethod.POST)
	public String getCalendarSlots(@RequestBody long[] values) throws Exception {
		return CalendarEntryManager.pringBinaryString(values);
	}

	@RequestMapping(value = "/calendarEntry/getSlotConflicts", method = RequestMethod.POST)
	public GetSlotConflictRes getSlotConflicts(@RequestBody GetSlotConflictReq req) throws Exception {
		logger.info("checkSlotConflicts : " + req.toString());
		return calendarEntryManager.getSlotConflicts(req);
	}

	@RequestMapping(value = "/calendarEntry/resolveSlotConflicts", method = RequestMethod.POST)
	public ResolveSlotConflictRes resolveSlotConflicts(@RequestBody ResolveSlotConflictReq req) throws Exception {
		logger.info("checkSlotConflicts : " + req.toString());
		return calendarEntryManager.resolveSlotConflicts(req);
	}

	@RequestMapping(value = "/calendarEntry/blockSlotSchedule", method = RequestMethod.POST)
	public BlockSlotScheduleRes blockSlotSchedule(@RequestBody BlockSlotScheduleReq req) throws Exception {
		logger.info("blockSlotSchedule : " + req.toString());
		return calendarEntryManager.blockSlotsSchedule(req);
	}

	@RequestMapping(value = "/calendarEntry/updateCalendarSessionSlots", method = RequestMethod.POST)
	public BasicResponse updateCalendarSessionSlots(@RequestBody UpdateCalendarSessionSlotsReq req) throws Exception {
		logger.info("updateCalendarSessionSlots : " + req.toString());
		return calendarEntryManager.updateCalendarSessionSlots(req);
	}

	@RequestMapping(value = "/calendarEntry/syncNextMonthCalendar", method = RequestMethod.GET)
	public BasicRes syncNextMonthCalendar() throws Exception {
		logger.info("Request:" + System.currentTimeMillis());
		return calendarEntryManager.syncNextMonthCalendar();
	}

	@RequestMapping(value = "/calendarEntry/syncNextWeekCalendar", method = RequestMethod.GET)
	public BasicRes syncNextWeekCalendar() throws Exception {
		logger.info("Request:" + System.currentTimeMillis());
		return calendarEntryManager.syncNextWeekCalendar();
	}

	@RequestMapping(value = "/calendarEntry/syncNextYearCalendar", method = RequestMethod.GET)
	public BasicRes syncNextYearCalendar() throws Exception {
		logger.info("Request:" + System.currentTimeMillis());
		return calendarEntryManager.syncNextYearCalendar();
	}

	@RequestMapping(value = "/calendarEntry/syncAllCalendar", method = RequestMethod.POST)
	public BasicRes syncCalendarSessions(@RequestBody SyncAllCalendarReq req) throws Exception {
		logger.info("updateCalendarSessionSlots : " + req.toString());
		return calendarEntryManager.syncAllCalendar(req);
	}

	@RequestMapping(value = "/calendarEntry/syncCalendarSessions", method = RequestMethod.POST)
	public List<String> syncCalendarSessions(@RequestBody SyncCalendarSessionsReq req) throws Exception {
		logger.info("updateCalendarSessionSlots : " + req.toString());
		return calendarEntryManager.syncCalendarSessions(req);
	}

	@RequestMapping(value = "/setWeightedAvailabilityRanges", method = RequestMethod.POST)
	public com.vedantu.scheduling.response.BaseResponse setWeightedAvailabilitySlabs(@RequestBody String req)
			throws Exception {
		CreateAvailabilityRangesRequest createAvailabilityRangesRequest = new Gson().fromJson(req,
				CreateAvailabilityRangesRequest.class);
		return calendarEntryManager.createAvailabilityRanges(createAvailabilityRangesRequest);
	}

	@RequestMapping(value = "/getWeightedAvailabilityRanges", method = RequestMethod.GET)
	public List<AvailabilityRange> getWeightedAvailabilitySlabs() throws Exception {
		return calendarEntryManager.getAvailabilityRanges();
	}

	@RequestMapping(value = "/initSlotSelection", method = RequestMethod.GET)
	public SlotSelectionPojo initSlotSelection() throws Exception {
		return calendarSelectionManager.initSlotSelectionPojo();
	}

	@RequestMapping(value = "/calendarEntry/getSelectedSlots", method = RequestMethod.GET)
	public List<SelectedSlotResponse> getSelectedSlots(@RequestParam("studentId") String studentId,
			@RequestParam("teacherId") String teacherId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime) throws Exception {
		return calendarEntryManager.getSelectedSlots(studentId, teacherId, startTime, endTime);
	}

	@RequestMapping(value = "/calendarEntry/getSuggestedSlots", method = RequestMethod.GET)
	public List<SessionSlot> getSuggestedSlots(@RequestParam("studentId") String studentId,
			@RequestParam("teacherId") String teacherId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime) throws Exception {
		return calendarEntryManager.getSuggestedSlot(studentId, teacherId, startTime, endTime);
	}

	@RequestMapping(value = "/calendarEntry/getTeacherAvailabilityBitSet", method = RequestMethod.GET)
	public List<AvailabilityResponse> getTeacherAvailabilityBitSet(@RequestParam("teacherIds") List<String> teacherIds,
			@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws Exception {
		return calendarEntryManager.getTeacherAvailabilityBitSet(teacherIds, startTime, endTime);
	}

	@ApiOperation("Fetch availability slot strings for a given set of teachers -- availability across both OTF and OTO")
	@RequestMapping(value = "/calendarEntry/getTeacherAvailabilitySlotStrings", method = RequestMethod.GET)
	public List<AvailabilitySlotStringRes> getTeacherAvailabilitySlotStrings(@ModelAttribute AvailabilitySlotStringReq req)
			throws VException {
		req.verify();
		return calendarEntryManager.getTeacherAvailabilitySlotStrings(req.getStartTime(), req.getEndTime(), req.getEarlyLearningCourseType());
	}

	@ApiOperation("Fetch availability slot strings for a given set of teachers -- availability across both OTF and OTO")
	@RequestMapping(value = "/calendarEntry/getBookingAvailabilitySlotStrings", method = RequestMethod.GET)
	public AvailabilitySlotStringRes getBookingAvailabilitySlotStrings(@ModelAttribute AvailabilitySlotStringReq req)
			throws VException {
		sessionUtils.checkIfUserLoggedIn();
		sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);
		req.verify();
		return calendarEntryManager.getBookingAvailabilitySlotStrings(req.getStartTime(), req.getEndTime(),req.getEarlyLearningCourseType());
	}

     // this api is called usually during the addition of batch   
    @RequestMapping(value = "/calendarEntry/markBatchSessionsCalendar/{batchId}", method = RequestMethod.POST)
    public PlatformBasicResponse markBatchSessionsCalendar(@PathVariable(value = "batchId") String batchId) throws Exception {
        return calendarEntryManager.updateBatchSessionsCalendar(batchId, true);
    }

    // this api is called when a batch is marked inactive
    @RequestMapping(value = "/calendarEntry/unmarkBatchSessionsCalendar/{batchId}", method = RequestMethod.POST)
    public PlatformBasicResponse unmarkBatchSessionsCalendar(@PathVariable(value = "batchId") String batchId) throws Exception {
        return calendarEntryManager.updateBatchSessionsCalendar(batchId, false);
    }

    @RequestMapping(value = "calendarEntry/getAvailableELTeachersForSlot", method = RequestMethod.GET)
	public Set<String> getAvailableELTeachersForGivenSlot(@RequestParam("startTime") Long startTime,
														  @RequestParam("endTime") Long endTime,
														  @RequestParam(value ="earlyLearningCourseType", required = true) SessionLabel earlyLearningCourseType) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		if (earlyLearningCourseType != SessionLabel.SUPER_CODER && earlyLearningCourseType != SessionLabel.SUPER_READER) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, earlyLearningCourseType + " not supported");
		}
		return calendarEntryManager.getAvailableELTeachersForSlot(startTime, endTime, earlyLearningCourseType);
	}

}
