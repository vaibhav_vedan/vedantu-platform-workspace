package com.vedantu.scheduling.controllers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.onetofew.request.GetFeedbackReq;
import com.vedantu.scheduling.dao.entity.Feedback;
import com.vedantu.scheduling.managers.FeedbackManager;
import com.vedantu.scheduling.response.BasicRes;
import com.vedantu.scheduling.response.GetFeedbackRes;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("feedback")
public class FeedbackController {
	@Autowired
	private FeedbackManager feedbackManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings({ "static-access", "unused" })
	private Logger logger = logFactory.getLogger(FeedbackController.class);

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public BasicRes feedback(@RequestBody Feedback feedback)
			throws IllegalAccessException {
		feedback = feedbackManager.feedback(feedback);
		BasicRes res = new BasicRes();
		if (feedback == null) {
			res.setSuccess(false);
		}
		return res;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Feedback getFeedbackById(@PathVariable("id") String id) throws Exception {
		Feedback result = feedbackManager.getFeedbackById(id);
		return result;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public GetFeedbackRes getFeedbacks(GetFeedbackReq req) {
		List<Feedback> feedbacks = feedbackManager.getFeedbacks(req);
		GetFeedbackRes res = new GetFeedbackRes();
		res.setList(feedbacks);
		return res;
	}

	@RequestMapping(value = "/getSessions", method = RequestMethod.GET)
	@ResponseBody
	public List<OTFSessionPojo> getSessionsForFeedback(
			@RequestParam(name = "afterEndTime", required = true) long afterEndTime,
			@RequestParam(name = "beforeEndTime", required = true) long beforeEndTime) {
		return feedbackManager.getSessionsForFeedback(afterEndTime, beforeEndTime);
	}
}
