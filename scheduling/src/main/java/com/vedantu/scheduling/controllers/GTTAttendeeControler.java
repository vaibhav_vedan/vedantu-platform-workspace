package com.vedantu.scheduling.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.managers.GTTAttendeeDetailsManager;
import com.vedantu.scheduling.pojo.GTTAggregatedStudentList;
import com.vedantu.scheduling.request.AbsentSessionFeedbackReq;
import com.vedantu.subscription.pojo.game.GameSessionActivity;
import com.vedantu.subscription.request.GTTAttendeeRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/gttattendee")
public class GTTAttendeeControler {

    private Logger logger = LogFactory.getLogger(GTTAttendeeDetails.class);


    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/getGttAttendeeForUsers", method = RequestMethod.POST)
    public @ResponseBody
    List<GTTAttendeeDetails> getGttAttendeeForUsers(@RequestBody GTTAttendeeRequest gttAttendeeRequest)
            throws VException, InterruptedException, UnsupportedEncodingException {

        return gttAttendeeDetailsManager.getAttendeeDetails(gttAttendeeRequest.getUserIds(), gttAttendeeRequest.getFromTime(), gttAttendeeRequest.getThruTime());

    }

    @RequestMapping(value = "/getAttendanceStatus", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, Integer> getAttendanceStatus(@RequestBody GTTAttendeeRequest gttAttendeeRequest)
            throws VException, InterruptedException, UnsupportedEncodingException {

        return gttAttendeeDetailsManager.getAttendanceStatus(gttAttendeeRequest.getUserIds(), gttAttendeeRequest.getFromTime(), gttAttendeeRequest.getThruTime());

    }

    @RequestMapping(value = "/checkActiveGttAttendeeForUserIdAndSessionId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse checkActiveGttAttendeeForUserIdAndSessionId(@RequestParam(name = "sessionId", required = true) String sessionId,
                                                                             @RequestParam(name = "userId", required = true) String userId) throws VException {
        return gttAttendeeDetailsManager.checkActiveGttAttendeeForUserIdAndSessionId(userId, sessionId);
    }

    @RequestMapping(value = "/previous/session", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GameSessionActivity getPreviousSessionForGttAttendee(@RequestParam(name = "userId") String userId,
                                                                @RequestParam(name = "sessionStartTime") Long sessionStartTime,
                                                                @RequestParam(name = "enrollmentId") String enrollmentId
    ) throws VException {
        if (StringUtils.isBlank(enrollmentId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Enrolment id is empty");
        }
        GTTAttendeeDetailsInfo session = gttAttendeeDetailsManager.getPreviousSessionForGttAttendee(userId, sessionStartTime, enrollmentId);
        GameSessionActivity activity = new GameSessionActivity();
        BeanUtils.copyProperties(session, activity);
        return activity;
    }

    @RequestMapping(value = "/getNewAbsenteeStudentList", method = RequestMethod.GET)
    @ResponseBody
    public List<GTTAggregatedStudentList> getNewAbsenteeStudentListAggregate(@RequestParam(name = "sessionIds", required = true) List<String> sessionIds,
                                                                             @RequestParam(name = "userIds", required = true) List<String> userIds, @RequestParam(name = "lastSessionId") String lastSessionId) throws VException {
        return gttAttendeeDetailsManager.getNewAbsenteeStudentListAggregate(userIds, sessionIds, lastSessionId);
    }

    @RequestMapping(value = "/cron/game/sessionactivity", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void gameSessionActivityBatch(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                          @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification gameSessionActivityBatch received - SNS");
            logger.info(subscriptionRequest.toString());
//            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.GAME_GTT_SESSION_CRON, new HashMap<>());
//            asyncTaskFactory.executeTask(params);
        }
    }
    @RequestMapping(value = "/getAttendeesCountBySessionId", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getAttendeesCountBySessionId(@RequestParam(name = "sessionId", required = true) String sessionId) throws VException {
        return gttAttendeeDetailsManager.getAttendeesCountBySessionId(sessionId);
    }

    @RequestMapping(value = "/getPresentAttendeesCountBySessionId", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getPresentAttendeesCountBySessionId(@RequestParam(name = "sessionId", required = true) String sessionId) throws VException {
        return gttAttendeeDetailsManager.getPresentAttendeesCountBySessionId(sessionId);
    }

    @RequestMapping(value = "/postAbsentFeedback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse postAbsentFeedback(@RequestBody List<AbsentSessionFeedbackReq> absentSessionFeedbackReqs) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }

        if (!CommonCalendarUtils.isValidDateForAbsentFeedback()) {
            new PlatformBasicResponse();
        }

        for (AbsentSessionFeedbackReq req : absentSessionFeedbackReqs) {
            req.verify();
        }

        Long callingUserId = sessionUtils.getCallingUserId();
        sessionUtils.checkIfAllowedList(callingUserId, Collections.singletonList(Role.STUDENT), false);
        return gttAttendeeDetailsManager.postAbsentFeedback(absentSessionFeedbackReqs, callingUserId);
    }

}
