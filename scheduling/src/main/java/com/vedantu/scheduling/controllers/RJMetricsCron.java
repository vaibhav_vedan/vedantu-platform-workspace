package com.vedantu.scheduling.controllers;

import com.mongodb.bulk.BulkWriteError;
import com.mongodb.MongoBulkWriteException;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.dao.entity.DemoSession;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.scheduling.dao.serializers.SessionAttendeeDAO;
import com.vedantu.scheduling.dao.serializers.SessionDAO;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.scheduling.pojo.session.ReferenceType;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.rjmetrics.RJMetricsExportManager;
import com.vedantu.util.rjmetrics.pojos.SessionRjPojo;
import java.net.UnknownHostException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

/**
 * Created by somil on 21/02/17.
 */
@RestController
@RequestMapping("/export/rjMetrics")
public class RJMetricsCron {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RJMetricsExportManager rJMetricsExportManager;

    @Autowired
    SessionAttendeeDAO sessionAttendeeDAO;

    @Autowired
    SessionDAO sessionDAO;

    private Logger logger = logFactory.getLogger(RJMetricsCron.class);

    private String RJMETRICS_BASE_URL;

    public RJMetricsCron() {
        RJMETRICS_BASE_URL = "https://connect.rjmetrics.com/v2/client/{{clientId}}/table/{{table}}/data?apikey={{apiKey}}";
        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{clientId}}",
                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.id"));
        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{apiKey}}",
                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.apiKey"));
    }

    @Autowired
    private DozerBeanMapper mapper;

    @RequestMapping(value = "/updateRJMetrics", method = RequestMethod.GET)
    @ResponseBody
    public void updateRJMetrics(@RequestParam(value = "fromTime", required = false) Long fromTime, @RequestParam(value = "tillTime", required = false) Long tillTime) throws VException {

        try {
            postSessionAttendeeData(fromTime, tillTime);
        } catch (Exception ex) {
            logger.error("Error in updating RJ : postSessionAttendeeData", ex);
        }

        try {
            postSessionData(fromTime, tillTime);
        } catch (Exception ex) {
            logger.error("Error in updating RJ : postSessionData", ex);
        }

    }

    private void postSessionAttendeeData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<SessionAttendee> entities = sessionAttendeeDAO.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, SessionAttendee.class);
        if (entities != null) {
            rJMetricsExportManager.postData(new ArrayList<>(entities), RJMETRICS_BASE_URL.replace("{{table}}", SessionAttendee.class.getSimpleName()));
        }
    }

    private void postSessionData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<Session> entities = sessionDAO.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, Session.class);
        if (entities != null) {
            List<SessionRjPojo> rjEntities = new ArrayList<>();
            for (Session session : entities) {
                try {
                    SessionRjPojo rjPojo = mapper.map(session, SessionRjPojo.class);
                    List<ReferenceTag> referenceTags = session.getSessionTags();
                    if (referenceTags != null) {
                        for (ReferenceTag referenceTag : referenceTags) {
                            if (ReferenceType.OFFERING.equals(referenceTag.getReferenceType()) && referenceTag.getReferenceId() != null) {
                                rjPojo.setOfferingId(Long.parseLong(referenceTag.getReferenceId()));
                            } else if (ReferenceType.PROPOSAL.equals(referenceTag.getReferenceType()) && referenceTag.getReferenceId() != null) {
                                rjPojo.setProposalId(Long.parseLong(referenceTag.getReferenceId()));
                            }
                        }
                    }
                    rjEntities.add(rjPojo);
                } catch (Exception ex) {
                    logger.error("Error in updating RJ entry: ", ex);
                }
            }
            rJMetricsExportManager.postData(new ArrayList<>(rjEntities), RJMETRICS_BASE_URL.replace("{{table}}", Session.class.getSimpleName()));
        }
    }

    @RequestMapping(value = "/updateRJSessionAttendeeAll", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse updateRJSessionAttendeeAll(@RequestParam(value = "fromTime", required = false) Long fromTime, @RequestParam(value = "tillTime", required = false) Long tillTime) throws VException {

        try {
            postSessionAttendeeDataAll();
        } catch (Exception ex) {
            logger.error("Error in updating RJ : postSessionAttendeeData", ex);
        }
        return new PlatformBasicResponse();

    }

    private void postSessionAttendeeDataAll() {
        Long currentTime = System.currentTimeMillis();
        Long startTime = 1489172400000L;

        while (startTime < currentTime) {
            Long tillTime = startTime + DateTimeUtils.MILLIS_PER_HOUR * 6;
            logger.info("====================");
            logger.info("fromTime: " + new Date(startTime) + " tillTime: " + new Date(tillTime));
            List<SessionAttendee> entities = sessionAttendeeDAO.getEntitiesByUpdationTimeAndLimit(startTime, tillTime, null, SessionAttendee.class);
            if (entities != null) {
                rJMetricsExportManager.postData(new ArrayList<>(entities), RJMETRICS_BASE_URL.replace("{{table}}", SessionAttendee.class.getSimpleName()));
            }
            startTime = tillTime;
        }
    }

    /**
     * @see com.vedantu.util.dbutils.AbstractMongoClientFactory for mongo connection reference
     */
    public static void main(String[] args) throws UnknownHostException {

        MongoClientSettings.Builder builder = MongoClientSettings.builder();
        builder.applyToConnectionPoolSettings(conn->conn.maxSize(10));
        builder.applyToSslSettings(ssl->ssl.enabled(true));
        ConnectionString connectionString = new ConnectionString("mongodb+srv://<cred>:<pass>@scheduling-baqwz.mongodb.net/vedantuscheduling?retryWrites=true&w=majority&ssl=true");
        builder.applyConnectionString(connectionString);

        MongoClient mongoClient = MongoClients.create(builder.build());
        MongoOperations mongoOperations = (MongoOperations) new MongoTemplate(mongoClient, "vedantuscheduling");
        System.err.println("MongoClientFactory is initialized");

        int start = 0;

        Query query2 = new Query();
        query2.skip(start);
        query2.limit(2);
        System.out.println("start " + start);
        List<DemoSession> sessions = mongoOperations.find(query2, DemoSession.class, DemoSession.class.getSimpleName());

        List<Document> queries = new ArrayList<>();
        List<Document> updates = new ArrayList<>();
        Document query3 = new Document();
        query3.append("_id", new ObjectId("5e874353c63e0e359fb38f7a"));
        Document updateDocument = new Document();
        Document setDocument = new Document();
        setDocument.append("a", "1009");
        updateDocument.append("$set", setDocument);
        updates.add(updateDocument);
        queries.add(query3);

        final MongoCollection<Document> collection = mongoOperations.getCollection("test");
        final BulkWriteOptions bulkWriteOptions = new BulkWriteOptions()
                .ordered(false).bypassDocumentValidation(true);
        final UpdateOptions updateOptions = new UpdateOptions().upsert(false);

        Iterator<? extends Bson> queryIterator = queries.iterator();
        Iterator<? extends Bson> updateIterator = updates.iterator();
        List<WriteModel<Document>> actions = new ArrayList<>(updates.size());
        long modifiedCount = 0L;
        Bson query = queries.get(0);

        while (updateIterator.hasNext()) {
            if (queries.size() != 1) {
                query = queryIterator.next();
            }
            Bson update = updateIterator.next();
            actions.add(new UpdateOneModel<>(query, update, updateOptions));
        }

        BulkWriteResult bulkWriteResult;

        try {
            bulkWriteResult = collection.bulkWrite(actions, bulkWriteOptions);
            modifiedCount += bulkWriteResult.getModifiedCount();
        } catch (MongoBulkWriteException bwe) {
            List<BulkWriteError> bulkWriteErrors = bwe.getWriteErrors();
            List<WriteModel<Document>> retryDocs = new ArrayList<>();

            for (BulkWriteError bulkWriteError : bulkWriteErrors) {
                retryDocs.add(actions.get(bulkWriteError.getIndex()));
            }

            try {
                bulkWriteResult = collection.bulkWrite(retryDocs, bulkWriteOptions);
                modifiedCount += bulkWriteResult.getModifiedCount();
            } catch (MongoBulkWriteException e) {
                System.out.println(bwe.getWriteErrors());
            } catch (MongoException me) {
                System.out.println("Bulk write operation failed for updating docs " + me.getMessage() + retryDocs);
            }

        } catch (MongoException me) {
            System.out.println("Bulk write operation failed for updating docs " + me.getMessage() + updates);
        }

        System.err.println(sessions);
    }

}
