package com.vedantu.scheduling.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;

import java.util.*;

import com.vedantu.dinero.pojo.FailedPayoutInfo;
import com.vedantu.scheduling.response.session.*;
import com.vedantu.exception.*;

import com.vedantu.scheduling.response.GetLatestUpComingOrOnGoingSessionResponse;
import com.vedantu.scheduling.response.SessionInfoResponse;
import com.vedantu.scheduling.response.session.*;
import com.vedantu.util.*;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.onetofew.pojo.LastUpdatedUsers;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.scheduling.dao.sql.entity.SessionStateDetails;
import com.vedantu.scheduling.managers.SessionAttendeeManager;
import com.vedantu.scheduling.managers.SessionManager;
import com.vedantu.scheduling.pojo.SessionInfo;
import com.vedantu.scheduling.pojo.TotalSessionDuration;
import com.vedantu.scheduling.pojo.UserSessionAttendance;
import com.vedantu.scheduling.pojo.session.TeacherSessionDashboard;
import com.vedantu.scheduling.request.session.EndSessionReq;
import com.vedantu.scheduling.request.session.GetSessionBillDetailsReq;
import com.vedantu.scheduling.request.session.GetSessionDurationByContextIdReq;
import com.vedantu.scheduling.request.session.GetSessionPartnersReq;
import com.vedantu.scheduling.request.session.GetSessionsByCreationTime;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.scheduling.request.session.GetSubscriptionSessionsReq;
import com.vedantu.scheduling.request.session.GetUserCalendarSessionsReq;
import com.vedantu.scheduling.request.session.GetUserLatestActiveSessionReq;
import com.vedantu.scheduling.request.session.GetUserPastSessionReq;
import com.vedantu.scheduling.request.session.GetUserUpcomingSessionReq;
import com.vedantu.scheduling.request.session.JoinSessionReq;
import com.vedantu.scheduling.request.session.MarkSessionActiveReq;
import com.vedantu.scheduling.request.session.MarkSessionExpiredReq;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.scheduling.request.session.ProcessSessionPayoutReq;
import com.vedantu.scheduling.request.session.RescheduleSessionReq;
import com.vedantu.scheduling.request.session.SessionScheduleReq;
import com.vedantu.scheduling.request.session.SetVimeoVideoIdRequest;
import com.vedantu.scheduling.request.session.SubscriptioncoursePlanCancelSessionsReq;
import com.vedantu.scheduling.request.session.UpdateLiveSessionPlatformDetailsReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.subscription.response.CoursePlanDashboardSessionInfo;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.subscription.TrialSessionInfos;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpMethod;

@RestController
@RequestMapping("/session")
public class SessionController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private SessionAttendeeManager sessionAttendeeManager;
    
    @Autowired
    private HttpSessionUtils httpSessionUtils;    

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionController.class);

    private static final Gson GSON = new Gson();

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionInfo createSession(@RequestBody SessionScheduleReq sessionScheduleReq,
            @RequestParam(name = "returnAttendees", required = false) Boolean returnAttendees) throws Exception {
        logger.info("Request : " + sessionScheduleReq.toString());
        return sessionManager.createSession(sessionScheduleReq);
    }

    @RequestMapping(value = "/createMultiple", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SessionInfo> createMultipleSessions(@RequestBody MultipleSessionScheduleReq multipleSessionScheduleReq)
            throws Exception {
        logger.info("Request : " + multipleSessionScheduleReq.toString());
        return sessionManager.createMultipleSessions(multipleSessionScheduleReq);
    }

    @RequestMapping(value = "/reschedule", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionInfo rescheduleSession(@RequestBody RescheduleSessionReq rescheduleSessionReq) throws Exception {
        logger.info("Request : " + rescheduleSessionReq.toString());
        return sessionManager.rescheduleSession(rescheduleSessionReq);
    }

    @RequestMapping(value = "/join", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionInfo joinSession(@RequestBody JoinSessionReq joinSessionReq) throws VException {
        logger.info("Request : " + joinSessionReq.toString());
        return sessionManager.joinSession(joinSessionReq, false);
    }

    @RequestMapping(value = "/joinFromNode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionInfo joinSessionFromNode(@RequestBody JoinSessionReq joinSessionReq) throws VException {
        logger.info("Request : " + joinSessionReq.toString());
        return sessionManager.joinSession(joinSessionReq, true);
    }

    @RequestMapping(value = "/end", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Session endSession(@RequestBody EndSessionReq endSessionReq) throws Exception {
        logger.info("Request : " + endSessionReq.toString());
        Session details = sessionManager.getSessionById(endSessionReq.getSessionId());
        if (details == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found");
        }
        if (SessionState.SCHEDULED.equals(details.getState())) {
            throw new ForbiddenException(ErrorCode.SESSION_NOT_STARTED,
                    "current state " + details.getState());
        }
        if (!Role.ADMIN.equals(endSessionReq.getCallingUserRole())) {
            SessionAttendee sessionAttendee = sessionAttendeeManager.getSessionAttendees(details.getId(), endSessionReq.getCallingUserId());
            if (sessionAttendee != null && !com.vedantu.session.pojo.SessionAttendee.SessionUserState.JOINED.equals(sessionAttendee.getUserState())) {
                throw new ForbiddenException(ErrorCode.SESSION_NOT_STARTED,
                        "user : " + endSessionReq.getCallingUserId() + " have not joined the session yet for sessionId: " + details.getId());
            }
        }        
        return sessionManager.endSession(endSessionReq, true);
    }
        
        @RequestMapping(value = "/getCoursePlanSessionAttendeeInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<UserSessionAttendance> getCoursePlanSessionAttendenceInfos(@RequestParam(name="coursePlanIds", required=true) List<String> ids, @RequestParam(name="userId", required=true) String userId)
			throws InternalServerErrorException, BadRequestException, NotFoundException {
		return sessionManager.getUserSessionAttendances(ids, userId);
	}        
        
        
    @RequestMapping(value = "/endWithoutAuthCheck", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Session endSessionWithoutAuthCheck(@RequestBody EndSessionReq endSessionReq) throws Exception {
        logger.info("Request : " + endSessionReq.toString());
        Session session = sessionManager.endSession(endSessionReq, false);
        return session;
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionInfo cancelSession(@RequestBody EndSessionReq endSessionReq) throws Exception {
        logger.info("Request : " + endSessionReq.toString());
        Session details = sessionManager.getSessionById(endSessionReq.getSessionId());
        if (details == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found");
        }
        if (!SessionState.SCHEDULED.equals(details.getState())) {
            throw new ForbiddenException(ErrorCode.SESSION_NOT_IN_SCHEDULED_STATE,
                    "current state " + details.getState());
        }
        if (details.getStartTime() < (System.currentTimeMillis() + (DateTimeUtils.MILLIS_PER_MINUTE * 6))) {
            throw new ForbiddenException(ErrorCode.SESSION_INVALID_FOR_CANCEL,
                    "Invalid cancelSessionReq : " + endSessionReq.toString());
        }
        Session session = sessionManager.endSession(endSessionReq, true);
        return new SessionInfo(session, sessionManager.getSessionExpiryTime(session), null);
    }

    @RequestMapping(value = "/cancelSubscriptionSessions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SessionInfo> cancelSubscriptionSessions(
            @RequestBody SubscriptioncoursePlanCancelSessionsReq subscriptionCancelSessionsReq) throws Exception {
        logger.info("Request : " + subscriptionCancelSessionsReq);
        List<Session> sessions = sessionManager.cancelSessions(subscriptionCancelSessionsReq);
        return sessionManager.getSessionInfos(sessions, false);
    }

    @RequestMapping(value = "/markActive", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Session markSessionActive(@RequestBody MarkSessionActiveReq markSessionActiveReq) throws Exception {
        logger.info("Request : " + markSessionActiveReq.toString());
        Session session = sessionManager.markSessionActive(markSessionActiveReq);
        return session;
    }

    @RequestMapping(value = "/setVimeoVideoId/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SessionInfo setVimeoVideoId(@PathVariable("id") Long id, @RequestBody SetVimeoVideoIdRequest setVimeoVideoIdRequest) throws VException {
        return sessionManager.setVimeoVideoId(id, setVimeoVideoIdRequest);
    }

    @RequestMapping(value = "/processSessionPayout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SessionAttendee> processSessionPayout(@RequestBody ProcessSessionPayoutReq processSessionPayoutReq)
            throws Exception {
        logger.info("Request : " + processSessionPayoutReq.toString());
        processSessionPayoutReq.validate();
        return sessionManager.processSessionPayout(processSessionPayoutReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public SessionInfo getSessionById(@PathVariable("id") Long id) throws Exception {
        logger.info("Request : " + id);
        return sessionManager.getSessionById(id);
    }

    @RequestMapping(value = "/handleWizIQEvent/{id}", method = RequestMethod.GET)
    public SessionInfo handleWizIQEvent(@PathVariable("id") Long id, @RequestParam(name = "event", required = true) SessionEvents event) throws Exception {
        logger.info("Request : " + id);
        sessionManager.handleLiveSessionToolType(event, id);
        return sessionManager.getSessionById(id);
    }

    @RequestMapping(value = "/convertToWizIQ/{id}", method = RequestMethod.GET)
    public SessionInfo convertToWizIQ(@PathVariable("id") Long id) throws Exception {
        logger.info("Request : " + id);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        sessionManager.convertToWizIQ(id);
        return sessionManager.getSessionById(id);
    }


    @RequestMapping(value = "/coursePlanSessionsConvertToWizIQ/{contextId}", method = RequestMethod.GET)
    public List<SessionInfo> coursePlanSessionsConvertToWizIQ(@PathVariable(value = "contextId") String contextId) throws Exception {
        logger.info("Request : " + contextId);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        sessionManager.convertToWizIQ(contextId);
        return sessionManager.getCoursePlanSessionsById(contextId);
    }


    @RequestMapping(value = "/sendPostSessionEmail", method = RequestMethod.GET)
    public void sendPostSessionEmail(@RequestParam(name = "sessionId", required = false) Long sessionId) throws Exception {
        sessionManager.sendPostSessionEmail(sessionId);
    }

    @RequestMapping(value = "/getSessions", method = RequestMethod.GET)
    public List<SessionInfo> getSessions(@ModelAttribute GetSessionsReq getSessionsReq) {
        return sessionManager.getSessionInfos(getSessionsReq);
    }

    @GetMapping(value = "/getSessionsInfo")
    @ApiOperation(value = "Returns sessions based on parameters", notes = "Allowed only for ADMIN")
    public SessionInfoResponse getSessionsInfo(@ModelAttribute GetSessionsReq getSessionsReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        getSessionsReq.verify();
        List<SessionInfo> sessionInfos = sessionManager.getSessionInfos(getSessionsReq,
                httpSessionUtils.isAdminOrStudentCare(httpSessionUtils.getCurrentSessionData()));
        return new SessionInfoResponse(sessionInfos);
    }

    @RequestMapping(value = "/getUserUpcomingSessions", method = RequestMethod.GET)
    public List<SessionInfo> getUserUpcomingSessions(
            @ModelAttribute GetUserUpcomingSessionReq getUserUpcomingSessionReq) throws Exception {
        return sessionManager.getUserUpcomingSessions(getUserUpcomingSessionReq);
    }

    @GetMapping(value = "/getUserUpcomingSessionsInfo")
    @ApiOperation(value = "Get User upcoming Sessions", notes = "Used for ticker and session listing")
    public SessionInfoResponse getUserUpcomingSessionsInfo(GetUserUpcomingSessionReq getUserUpcomingSessionReq)
            throws Exception {
        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        List<SessionInfo> sessionInfos = sessionManager.getUserUpcomingSessionsInfo(getUserUpcomingSessionReq, exposeEmail);
        return new SessionInfoResponse(sessionInfos);
    }

    @RequestMapping(value = "/getUserPastSessions", method = RequestMethod.GET)
    public List<SessionInfo> getUserPastSessions(@ModelAttribute GetUserPastSessionReq getUserPastSessionReq)
            throws Exception {
        return sessionManager.getUserPastSessions(getUserPastSessionReq, getUserPastSessionReq.getCallingUserRole());
    }

    @RequestMapping(value = "/getUserSessionsForDuration", method = RequestMethod.GET)
    public SessionInfoRes getUserSessionsForDuration(
            @ModelAttribute GetUserUpcomingSessionReq getUserSessionsForDurationReq) throws Exception {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        boolean exposeEmail = false;
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        getUserSessionsForDurationReq.verify();
        List<SessionInfo> sessions = sessionManager.getUserSessionsForDuration(getUserSessionsForDurationReq, exposeEmail);
        List<com.vedantu.scheduling.pojo.session.SessionInfo> sessionsInfoRes = GSON.fromJson(GSON.toJson(sessions), new TypeToken<List<com.vedantu.scheduling.pojo.session.SessionInfo>>() {
        }.getType());
        return new SessionInfoRes(sessionsInfoRes);
    }

    @GetMapping(value = "/getUserPastSessionsInfo")
    @ApiOperation(value = "Get User past Sessions", notes = "Used in session listing")
    public SessionInfoResponse getUserPastSessionsInfo(@ModelAttribute GetUserPastSessionReq getUserPastSessionReq) throws Exception {
        httpSessionUtils.checkIfAllowed(getUserPastSessionReq.getUserId(), null, Boolean.TRUE);

        if (getUserPastSessionReq.getUserId() == null) {
            HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
            if (sessionData != null) {
                getUserPastSessionReq.setUserId(sessionData.getUserId());
                getUserPastSessionReq.setCallingUserRole(sessionData.getRole());
            }
        }
        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        List<SessionInfo> sessionInfos = sessionManager.getUserPastSessionsInfo(getUserPastSessionReq, exposeEmail);
        return new SessionInfoResponse(sessionInfos);
    }

    @RequestMapping(value = "/getUserLatestActiveSession", method = RequestMethod.GET)
    public List<SessionInfo> getUserLatestActiveSession(
            @ModelAttribute GetUserLatestActiveSessionReq getUserLatestActiveSessionReq) throws Exception {
        return sessionManager.getUserLatestActiveSession(getUserLatestActiveSessionReq);
    }

    @GetMapping(value = "/getUserLatestActiveSessionFromCache")
    public GetLatestUpComingOrOnGoingSessionResponse getUserLatestSessionsRedis(
            @ModelAttribute GetUserUpcomingSessionReq getUserLatestActiveSessionReq) throws Exception {
        return sessionManager.getUserLatestSessionsRedis(getUserLatestActiveSessionReq);
    }

    @RequestMapping(value = "/getUserCalendarSessions", method = RequestMethod.GET)
    public List<SessionInfo> getUserCalendarSessions(
            @ModelAttribute GetUserCalendarSessionsReq getUserCalendarSessionsReq) throws Exception {
        return sessionManager.getUserCalendarSessions(getUserCalendarSessionsReq);
    }

    @RequestMapping(value = "/getSubscriptionSessions", method = RequestMethod.GET)
    public List<SessionInfo> getSubscriptionSessions(GetSubscriptionSessionsReq getSubscriptionSessionsReq)
            throws Exception {
        return sessionManager.getSubscriptionSessions(getSubscriptionSessionsReq);
    }

    @RequestMapping(value = "/getSubscriptionScheduledHours", method = RequestMethod.GET)
    public Long getSubscriptionScheduledHours(
            @RequestParam(value = "subscriptionId", required = true) Long subscriptionId) throws Exception {
        return sessionManager.getSubscriptionScheduledHours(subscriptionId);
    }

    @RequestMapping(value = "/getTotalSessionDuration", method = RequestMethod.GET)
    public Long getTotalSessionDuration(@RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime)
            throws Exception {
        Long duration = sessionManager.getTotalSessionDuration(afterEndTime, beforeEndTime);
        return duration;
    }

    @RequestMapping(value = "/getTotalSessionDurationForTeacher", method = RequestMethod.GET)
    public TotalSessionDuration getTotalSessionDurationForTeacher(
            @RequestParam(name = "userId", required = true) Long userId) throws Exception {
        TotalSessionDuration duration = sessionManager.getTotalSessionDurationForTeacher(userId);
        return duration;
    }

    @RequestMapping(value = "/getLatestTrial", method = RequestMethod.GET)
    public SessionInfo getLatestTrial(@RequestParam(name = "studentId") Long studentId,
            @RequestParam(name = "teacherId") Long teacherId) throws Exception {
        return sessionManager.getLatestTrial(studentId, teacherId);
    }

    @RequestMapping(value = "/getSessionBillDetails", method = RequestMethod.GET)
    public GetSessionBillDetailsRes getSessionBillDetails(@ModelAttribute GetSessionBillDetailsReq request)
            throws Exception {
        return sessionManager.getSessionBillDetails(request);
    }

    @RequestMapping(value = "/getSessionPartners", method = RequestMethod.GET)
    @ResponseBody
    public GetSessionPartnersRes getSessionPartners(GetSessionPartnersReq req) throws VException {
        req.verify();
        return sessionManager.getSessionPartners(req, true, null);
    }

    @RequestMapping(value = "/getSessionPartnersChat", method = RequestMethod.GET)
    @ResponseBody
    public GetSessionPartnersRes getSessionPartnersChat(GetSessionPartnersReq req) throws VException {
        req.verify();
        return sessionManager.getSessionPartners(req, false, System.currentTimeMillis());
    }

    @RequestMapping(value = "/getEndedSessionsToday", method = RequestMethod.GET)
    @ResponseBody
    public List<FailedPayoutInfo> getEndedSessionsToday() throws VException {
        return sessionManager.getEndedSessionsToday();
    }

    @RequestMapping(value = "/getSessionCounts", method = RequestMethod.GET)
    @ResponseBody
    public GetScheduledAndAllSessionCountRes getSessionCounts(
            @RequestParam(value = "userId", required = true) Long userId,
            @RequestParam(value = "userRole", required = true) Role userRole) throws VException {
        return sessionManager.getSessionCounts(userId, userRole);
    }

    @RequestMapping(value = "/checkFirstBookedSession", method = RequestMethod.GET)
    @ResponseBody
    public Boolean checkFirstBookedSession(@RequestParam(name = "studentId") Long studentId) throws VException {
        return sessionManager.checkFirstBookedSession(studentId);
    }

    @RequestMapping(value = "/markSessionExpired", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void markSessionExpired(@RequestBody MarkSessionExpiredReq req) throws Exception {
        logger.info("Request : " + req.toString());
        sessionManager.markSessionExpired(req);
    }

    @RequestMapping(value = "/getSessionsByCreationTime", method = RequestMethod.GET)
    @ResponseBody
    public List<Session> getSessionsByCreationTime(GetSessionsByCreationTime req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        return sessionManager.getSessionsByCreationTime(req);
    }

    @RequestMapping(value = "/updateLiveSessionPlatformDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateLiveSessionPlatformDetails(@RequestBody UpdateLiveSessionPlatformDetailsReq req)
            throws VException {
        if (!Role.ADMIN.equals(req.getCallingUserRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "non admins cannot change session platform type");
        }
        return sessionManager.updateLiveSessionPlatformDetails(req);
    }

    @RequestMapping(value = "/getAttendeesNextSessions", method = RequestMethod.GET)
    @ResponseBody
    public List<SessionInfo> getAttendeesNextSessions(@RequestParam(name = "sessionId") Long sessionId,
            @RequestParam(name = "userId", required = true) Long userId)
            throws VException, IllegalArgumentException, IllegalAccessException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Forbidden access");
        }        
        return sessionManager.getAttendeesNextSessions(sessionId, userId, sessionData.getRole());
    }

    @RequestMapping(value = "/getSessionStateDetailsById", method = RequestMethod.GET)
    @ResponseBody
    public SessionStateDetails getSessionStateDetailsById(@RequestParam(name = "sessionId") Long sessionId)
            throws Exception {
        return sessionManager.getSessionStateDetailsById(sessionId);
    }

    @RequestMapping(value = "/getCoursePlanTrialSessionInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, TrialSessionInfos> getCoursePlanTrialSessionInfo(@RequestBody Set<String> coursePlanIds)
            throws VException {
        return sessionManager.getCoursePlanTrialSessionInfo(coursePlanIds);
    }

    @RequestMapping(value = "/getSessionsForEnding", method = RequestMethod.GET)
    @ResponseBody
    public List<SessionInfo> getSessionsForEnding() throws VException {
        // TODO : For now no limit on session count. May need to introduce
        // @RequestParam(name="start") Integer start, @RequestParam(name="limit")
        // Integer limit
        return sessionManager.getSessionsForEnding();
    }

    @RequestMapping(value = "/getTeacherSessionForDashboard", method = RequestMethod.GET)
    @ResponseBody
    public Map<Long, TeacherSessionDashboard> getTeacherSessionForDashboard(@RequestParam("startTime") Long startTime,
            @RequestParam("endTime") Long endTime) throws VException {
        if (startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start or endTime not given");
        }

        return sessionManager.getTeacherSessionForDashboard(startTime, endTime);
    }

    @RequestMapping(value = "/getSessionDurationByContextId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContextSessionDurationResponse> getSessionDurationByContextId(@RequestBody GetSessionDurationByContextIdReq req) throws VException {
        return sessionManager.getSessionDurationByContextId(req);
    }

    @RequestMapping(value = "/getLastUpdatedUserIds", method = RequestMethod.GET)
    @ResponseBody
    public LastUpdatedUsers getLastUpdatedUserIds(@RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime", required = true) long endTime)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return new LastUpdatedUsers();
    }

    @RequestMapping(value = "/getEndedSessionAttendeesOfContext", method = RequestMethod.GET)
    @ResponseBody
    public List<SessionAttendee> getEndedSessionAttendeesOfContext(@RequestParam(name = "contextId", required = true) String contextId,
            @RequestParam(name = "contextType", required = true) EntityType contextType)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return sessionManager.getEndedSessionAttendeesOfContext(contextId, contextType);
    }

    @RequestMapping(value = "/getSessionInfosForContextIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, CounselorDashboardSessionsInfoRes> getSessionInfosForContextIds(@RequestParam(name = "contextIds") List<String> contextIds)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return sessionManager.getSessionInfosForContextIds(contextIds);
    }

    @RequestMapping(value = "/getUserDashboardSessionInfo", method = RequestMethod.GET)
    @ResponseBody
    public GetUserDashboardSessionInfoRes getUserDashboardSessionInfo(@RequestParam(name = "userId", required = true) Long userId)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return sessionManager.getUserDashboardSessionInfo(userId);
    }

    @RequestMapping(value = "/getCoursePlanDashboardSessionInfos", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, CoursePlanDashboardSessionInfo> getCoursePlanDashboardSessionInfos(@RequestParam(name = "ids", required = true) List<String> ids)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return sessionManager.getCoursePlanDashboardSessionInfos(ids);
    }

    @RequestMapping(value = "/sendPostSessionEmails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendPostSessionEmails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            sessionManager.sendPostSessionEmailAsync();
        }
    }

    @RequestMapping(value = "/wiziq/statusping", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse statusping(@RequestParam(name = "class_id", required = false) String class_id,
            @RequestParam(name = "class_status", required = false) String class_status,
            @RequestParam(name = "recording_status", required = false) String recording_status,
            @RequestParam(name = "attendance_report_status", required = false) String attendance_report_status,
            HttpServletRequest request
    )
            //@RequestParam(name = "attendance_report_file", required = false) MultipartFile attendance_report_file
            throws VException, IOException {
//        Enumeration<String> e = request.getParameterNames();
//        while (e.hasMoreElements()) {
//            logger.info(">> " + e.nextElement());
//        }
        if (class_id != null) {
            class_id = class_id.split(",")[0].trim();
        }
        if (class_status != null) {
            class_status = class_status.split(",")[0].trim();
        }
        sessionManager.wiziqstatusping(class_id, class_status, recording_status, attendance_report_status, request);
        return new PlatformBasicResponse();
    }
    
    @RequestMapping(value = "/markSessionActiveForExpiredWiziq", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Session markSessionActiveForExpiredWiziq(@RequestParam(name = "sessionId") Long sessionId) throws Exception {
        Session session = sessionManager.markSessionActiveForExpiredWiziq(sessionId);
        return session;
    }

    // migrated from platform
    @RequestMapping(value = "/handleSessionEventsForLatestSession", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleSessionEventsForLatestSession(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                    @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();

            if (StringUtils.isEmpty(subject)) {
                logger.warn("Event type can not be null for /handleSessionEventsForLatestSession");
                return;
            }

            SessionEvents eventType = SessionEvents.valueOf(subject);
            String message = subscriptionRequest.getMessage();

            sessionManager.handleSessionEventsForLatestSession(eventType, message);
        }
    }

}
