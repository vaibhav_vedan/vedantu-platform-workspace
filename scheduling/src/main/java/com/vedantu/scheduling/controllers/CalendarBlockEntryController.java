package com.vedantu.scheduling.controllers;

import java.util.Arrays;
import java.util.List;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import com.vedantu.scheduling.dao.entity.CalendarBlockEntry;
import com.vedantu.scheduling.managers.CalendarBlockEntryManager;
import com.vedantu.scheduling.pojo.CalendarBlockEntryState;
import com.vedantu.scheduling.pojo.calendar.CalendarBlockSlot;
import com.vedantu.util.LogFactory;


@RestController
@RequestMapping("/calendarBlockEntry")
public class CalendarBlockEntryController {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEntryController.class);

	@Autowired
	private CalendarBlockEntryManager calendarBlockEntryManager;

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@Autowired
	private FosUtils fosUtils;

	@RequestMapping(value = "/markCanceled", method = RequestMethod.GET)
	public List<CalendarBlockEntry> markCanceled(
			@RequestParam(name = "referenceId", required = true) String referenceId) throws Exception {
		logger.info("markCanceled : " + referenceId);
		return calendarBlockEntryManager.markCanceled(referenceId);
	}

	@RequestMapping(value = "/markCanceledBulk", method = RequestMethod.POST)
	public List<CalendarBlockEntry> markBulkCanceled(
			@RequestParam(name = "referenceIds", required = true) List<String> referenceIds) throws Exception {
		logger.info("markCanceled : " + Arrays.toString(referenceIds.toArray()));
		return calendarBlockEntryManager.cancelBulkEntries(referenceIds);
	}

	@RequestMapping(value = "/markProcessed", method = RequestMethod.POST)
	public List<CalendarBlockEntry> markProcessed(
			@RequestParam(name = "referenceId", required = true) String referenceId) throws Exception {
		logger.info("markProcessed : " + referenceId);
		return calendarBlockEntryManager.markProcessed(referenceId);
	}

	@RequestMapping(value = "/getEntries", method = RequestMethod.GET)
	public List<CalendarBlockEntry> getCalendarBlockEntries(
			@RequestParam(name = "studentId", required = false) String studentId,
			@RequestParam(name = "teacherId", required = false) String teacherId,
			@RequestParam(name = "state", required = false) CalendarBlockEntryState state) throws Exception {
		logger.info("getCalendarEntries request : studentId - " + studentId + ", teacherId - " + teacherId
				+ ", state - " + state);
		List<CalendarBlockEntry> entries = calendarBlockEntryManager.getBlockedEntries(studentId, teacherId, state);
		logger.info("getCalendarEntries response : count - " + entries.toString());
		return entries;
	}

	@RequestMapping(value = "/getBlockEntrySlots", method = RequestMethod.GET)
	public List<CalendarBlockSlot> getBlockEntrySlots(
			@RequestParam(name = "studentId", required = false) String studentId,
			@RequestParam(name = "teacherId", required = false) String teacherId,
			@RequestParam(name = "startTime", required = false) Long startTime,
			@RequestParam(name = "endTime", required = false) Long endTime) throws Exception {
		logger.info("getBlockEntrySlots request : studentId - " + studentId + ", teacherId - " + teacherId
				+ ", startTime - " + startTime, ", endTime - " + endTime);
		List<CalendarBlockSlot> entries = calendarBlockEntryManager.getBlockedEntrySlots(studentId, teacherId,
				startTime, endTime);
		logger.info("getBlockEntrySlots response : count - " + entries.toString());
		return entries;
	}

	@GetMapping(value = "/getBlockEntrySlotsInfo")
	@ApiOperation(value = "Get last marked day", notes = "")
	public List<CalendarBlockSlot> getBlockEntrySlots(@RequestParam(name = "userId", required = true) String userId,
													  @RequestParam(name = "startTime", required = true) Long startTime,
													  @RequestParam(name = "endTime", required = true) Long endTime) throws VException {
		logger.info("getBlockEntrySlots : userId - {} startTime - {}  endTime - {}", userId ,startTime, endTime);

		HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
		Role role;
		if (httpSessionData != null && httpSessionData.getUserId().toString().equals(userId)) {
			role = httpSessionData.getRole();
		} else {
			UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
			if (userBasicInfo == null) {
				throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found");
			} else {
				role = userBasicInfo.getRole();
			}
		}
		String studentId = null;
		String teacherId = null;
		if (role == Role.TEACHER) {
			teacherId = userId;
		} else {
			studentId = userId;
		}

		List<CalendarBlockSlot> entries = calendarBlockEntryManager.getBlockedEntrySlots(studentId, teacherId,
				startTime, endTime);

		logger.info("getSlotStringResponse : {}", entries);
		return entries;
	}

	@RequestMapping(value = "/getBitSet", method = RequestMethod.GET)
	public long[] getCalendarBlockEntries(@RequestParam("id") String id) throws Exception {
		CalendarBlockEntry entry = calendarBlockEntryManager.getCalendarBlockEntry(id);
		long[] bitSet = null;
		if (entry != null) {
			bitSet = entry.getBlockEntry().toLongArray();
		}
		logger.info("getCalendarEntries response : count - " + bitSet.toString());
		return bitSet;
	}

	@RequestMapping(value = "/resetBlockEntry", method = RequestMethod.POST)
	public List<CalendarBlockEntry> resetBlockEntry(
			@RequestParam(name = "referenceId", required = true) String referenceId,
			@RequestParam(name = "endTime", required = true) Long endTime) throws Exception {
		logger.info("resetBlockEntry : " + referenceId + " endTime - " + endTime);
		return calendarBlockEntryManager.resetBlockEntry(referenceId, endTime);
	}
}
