package com.vedantu.scheduling.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.request.GetOTFSessionsListReq;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.managers.EarlyLearningSessionManager;
import com.vedantu.scheduling.pojo.ELDemoSessionPojo;
import com.vedantu.scheduling.pojo.OverBookingPojo;
import com.vedantu.scheduling.request.*;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * EarlyLearning Demo Scheduler logic has shifted to EarlyLearningDemoController
 *
 *
 */

@RestController
@RequestMapping("/earlyLearning")
public class EarlyLearningSessionController {

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private EarlyLearningSessionManager earlyLearningSessionManager;

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(EarlyLearningSessionController.class);


    @Deprecated
    @RequestMapping(value = "/hasEarlyLearningSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSession> hasEarlyLearningSessions(@RequestParam(name = "studentId" , required = false) String studentId,
                                                     @RequestParam(name = "earlyLearningCourse" , required = true)SessionLabel earlyLearningCourse) throws VException {
        if (sessionUtils == null) {
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "Please log in / register");
        }
        if(Role.ADMIN.equals(sessionUtils.getCallingUserRole()) && studentId == null) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Please use tools section to access booking page");
        }
        if (Role.TEACHER.equals(sessionUtils.getCallingUserRole())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "You are not authorized to use this page");
        }
        if (Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && studentId == null) {
            studentId = String.valueOf(sessionUtils.getCallingUserId());
        }
        if (earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, earlyLearningCourse + " not supported");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);

        return earlyLearningSessionManager.hasEarlyLearningSessions(studentId,earlyLearningCourse);
    }

    @Deprecated
    @RequestMapping(value = "/addEarlyLearningSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public OTFSession addEarlyLearningSession(@RequestBody AddEarlyLearningSessionReq req)
            throws VException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        req.verify();
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);

        return earlyLearningSessionManager.addEarlyLearningSession(req);
    }

    @RequestMapping(value = "/cancelEarlyLearningSession", method = RequestMethod.POST, consumes = "application/json" ,produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse cancelEarlyLearningSession(@RequestBody CancelOrDeleteSessionReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, Boolean.TRUE);

        return earlyLearningSessionManager.cancelEarlyLearningSession(req.getSessionId(), req.getRemark());
    }

    @RequestMapping(value = "/getEarlyLearningSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFSessionPojo> getEarlyLearningSessions(@ModelAttribute GetOTFSessionsListReq req) throws VException {
        logger.info("requested filters to fetch EL sessoins" + req.toString());
        req.verify();
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, Boolean.TRUE);

        return earlyLearningSessionManager.getEarlyLearningSessions(req);
    }

    @RequestMapping(value = "/getEarlyLearningDemoSessionTime", method = RequestMethod.GET)
    public Long getEarlyLearningDemoSessionStartTime(@RequestParam(name = "studentId") Long studentId,
                                                            @RequestParam(name = "states") com.vedantu.onetofew.enums.SessionState sessionState) throws VException {
        logger.info("studentId : "+studentId);
        logger.info("sessionState :"+sessionState);
        return earlyLearningSessionManager.getEarlyLearningDemoSessionStartTime(studentId,sessionState);
    }

    @RequestMapping(value = "/postEarlyLearningFeedbackForm", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse postEarlyLearningFeedbackForm(@RequestBody ELFeedbackFormReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.TEACHER, Role.ADMIN), Boolean.TRUE);
        return earlyLearningSessionManager.postEarlyLearningFeedbackForm(req);

    }

    @RequestMapping(value = "/getEarlyLearningSessionByStudentId", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSession> getEarlyLearningSessionByStudentId(@RequestParam(name = "studentId" , required = true) String studentId,
                                                               @RequestParam(name = "earlyLearningCourse" , required = true) SessionLabel earlyLearningCourse,
                                                               @RequestParam(name = "start", required = false, defaultValue = "0")Integer start,
                                                               @RequestParam(name = "size", required = false, defaultValue = "20")Integer size) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);
        if (earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, earlyLearningCourse + " not supported");
        }
        return earlyLearningSessionManager.getEarlyLearningSessionByStudentId(studentId,earlyLearningCourse,start,size);

    }

    @RequestMapping(value = "/getEarlyLearningBookingByStudentId", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OverBooking> getEarlyLearningBookingByStudentId(@RequestParam(name = "studentId" , required = true) String studentId,
                                                      @RequestParam(name = "earlyLearningCourse" , required = true)SessionLabel earlyLearningCourse,
                                                      @RequestParam(name = "start", required = false, defaultValue = "0")Integer start,
                                                      @RequestParam(name = "size", required = false, defaultValue = "20")Integer size) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);
        if(Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && !String.valueOf(sessionUtils.getCallingUserId()).equals(studentId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to get booking details");
        }
        if (earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, earlyLearningCourse + " not supported");
        }
        return earlyLearningSessionManager.getEarlyLearningBookingByStudentId(studentId,earlyLearningCourse,start,size);

    }

    @RequestMapping(value = "/getSessionJoiningDetailsBySessionId", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<GTTAttendeeDetails> getSessionJoiningDetailsBySessionId(@RequestParam(name = "sessionId" , required = true) Set<String> sessionId,
                                                                        @RequestParam(name = "studentId" , required = true)String studentId,
                                                                        @RequestParam(name = "start", required = false, defaultValue = "0")Integer start,
                                                                        @RequestParam(name = "size", required = false,  defaultValue = "20")Integer size) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);
        if(Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && !String.valueOf(sessionUtils.getCallingUserId()).equals(studentId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to get demo details");
        }
        return earlyLearningSessionManager.getSessionJoiningDetailsBySessionId(sessionId,studentId,start,size);

    }

    @Deprecated
    @RequestMapping(value = "/hasEarlyLearningBooking" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OverBooking> hasEarlyLearningBooking(@RequestParam(name = "studentId", required = false) String studentId,
                                                     @RequestParam(name = "earlyLearningCourse" , required = true)SessionLabel earlyLearningCourse) throws VException {
        if (sessionUtils == null) {
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "Please log in / register");
        }
        if(Role.ADMIN.equals(sessionUtils.getCallingUserRole()) && studentId == null) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Please use tools section to access booking page");
        }
        if (Role.TEACHER.equals(sessionUtils.getCallingUserRole())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "You are not authorized to use this page");
        }
        if (Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && studentId == null) {
            studentId = String.valueOf(sessionUtils.getCallingUserId());
        }
        if (earlyLearningCourse != SessionLabel.SUPER_CODER && earlyLearningCourse != SessionLabel.SUPER_READER) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, earlyLearningCourse + " not supported");
        }
        return earlyLearningSessionManager.hasEarlyLearningBooking(studentId, earlyLearningCourse);
    }

    @Deprecated
    @RequestMapping(value = "/reScheduleEarlyLearningBooking", method = RequestMethod.POST , produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse reScheduleEarlyLearningBooking (@RequestBody ReScheduleEarlyLearningBookingReq request) throws VException {
        logger.info("Inside reScheduleEarlyLearningBooking method.");
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT), Boolean.FALSE);
        request.verify();
        if(!String.valueOf(sessionUtils.getCallingUserId()).equals(request.getStudentId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to reschedule a session");
        }
        return earlyLearningSessionManager.reScheduleEarlyLearningBooking(request);
    }

    @RequestMapping(value = "/addAlternateNumberToEarlyLearningBooking", method = RequestMethod.POST , produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse addAlternateNumberToEarlyLearningBooking (@RequestBody AddAlternateNumberForEarlyLearningBookingReq request) throws VException {
        logger.info("Inside addAlternateNumberToEarlyLearningBooking method.");
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT), Boolean.FALSE);
        request.verify();
        return earlyLearningSessionManager.addAlternateNumberToEarlyLearningBooking(request,sessionUtils.getCallingUserId());
    }

    @Deprecated
    @RequestMapping(value = "/addEarlyLearningBooking", method = RequestMethod.POST , produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse addEarlyLearningBooking (@RequestBody AddEarlyLearningBookingReq request) throws VException {
        logger.info("Inside addEarlyLearningBooking method.");
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);
        request.verify();
        if(Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && !String.valueOf(sessionUtils.getCallingUserId()).equals(request.getStudentId())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to book a session");
        }
        OverBooking overBooking = earlyLearningSessionManager.addEarlyLearningBooking(request);
        return new PlatformBasicResponse(true, overBooking, "");
    }

    @Deprecated
    @RequestMapping(value = "/joinEarlyLearningSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public OTFSession joinEarlyLearningSession(@RequestBody AddEarlyLearningSessionReq req)
            throws VException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT, Role.ADMIN), Boolean.TRUE);
        req.verify();
        if(Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && !String.valueOf(sessionUtils.getCallingUserId()).equals(req.getStudentId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to join a session");
        }
        return earlyLearningSessionManager.joinEarlyLearningSession(req);
    }

    @RequestMapping(value = "/getEarlyLearningBooking", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OverBookingPojo> getEarlyLearningBooking(@ModelAttribute GetOverBookingListReq req) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        req.verify();
        return earlyLearningSessionManager.getEarlyLearningBooking(req);

    }

    @RequestMapping(value = "/deleteEarlyLearningBooking", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse deleteEarlyLearningBooking(@RequestBody CancelOrDeleteBookingReq req) throws VException {
        logger.info("Inside deleteEarlyLearningBooking method.");
        req.verify();
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN,Role.STUDENT), Boolean.TRUE);
        return earlyLearningSessionManager.deleteEarlyLearningBooking(req);

    }

    @RequestMapping(value = "/getEarlyLearningDemoDetails", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ELDemoSessionPojo getEarlyLearningDemoDetails(@RequestParam(name = "studentId", required = true)String studentId,
                                                         @RequestParam(name = "earlyLearningCourseType", required = true) SessionLabel earlyLearningCourseType) throws VException {

        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.STUDENT,Role.TEACHER,Role.ADMIN), Boolean.TRUE);
        if(Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && !String.valueOf(sessionUtils.getCallingUserId()).equals(studentId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not authorized to get demo details");
        }
        if (earlyLearningCourseType != SessionLabel.SUPER_CODER && earlyLearningCourseType != SessionLabel.SUPER_READER) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, earlyLearningCourseType + " not supported");
        }
        return earlyLearningSessionManager.getEarlyLearningDemoDetails(studentId, earlyLearningCourseType);
    }

}
