package com.vedantu.scheduling.controllers;


import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.*;
import com.vedantu.onetofew.pojo.GetOTFUpcomingSessionReq;
import com.vedantu.onetofew.request.AddOTFSessionsReq;
import com.vedantu.onetofew.request.GetOTFSessionsReq;
import com.vedantu.onetofew.request.OTFJoinSessionReq;
import com.vedantu.onetofew.request.UpdateReplayUrlReq;
import com.vedantu.scheduling.managers.OtfSessionManagerV2;
import com.vedantu.scheduling.pojo.UpcomingSessionsPojo;
import com.vedantu.scheduling.request.CancelOrDeleteSessionReq;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.scheduling.response.OTFSessionPojo;
import com.vedantu.scheduling.response.UpdateSessionRes;
import com.vedantu.session.pojo.OTFCalendarSessionInfo;
import com.vedantu.session.pojo.OTFCalendarSessionInfoRes;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * will have migrated apis from platform
 */
@RestController
@RequestMapping("/onetofew/session/v2")
public class OtfSessionControllerV2 {

    private static final Gson GSON = new Gson();

    private final Logger logger = LogFactory.getLogger(OtfSessionControllerV2.class);
    private final OtfSessionManagerV2 otfSessionManagerV2;
    private final HttpSessionUtils sessionUtils;
    private final FosUtils fosUtils;

    @Autowired
    public OtfSessionControllerV2 (OtfSessionManagerV2 otfSessionManagerV2,
                                   HttpSessionUtils sessionUtils,
                                   FosUtils fosUtils) {
        this.otfSessionManagerV2 = otfSessionManagerV2;
        this.sessionUtils = sessionUtils;
        this.fosUtils = fosUtils;
    }

    @GetMapping
    @ResponseBody
    public List<OTFSessionPojo> getSessions(@RequestParam(name = "afterStartTime", required = false) Long afterStartTime,
                                            @RequestParam(name = "beforeStartTime", required = false) Long beforeStartTime,
                                            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
                                            @RequestParam(name = "teacherId", required = false) String teacherId,
                                            @RequestParam(name = "studentId", required = false) String studentId) throws VException {
        return otfSessionManagerV2.getSessions(afterStartTime, beforeStartTime, afterEndTime, teacherId, studentId);
    }

    @GetMapping(value = "/getUpcomingSessions")
    @ResponseBody
    public List<OTFSessionPojo> getUpcomingSessions(@ModelAttribute GetOTFUpcomingSessionReq getOTFUpcomingSessionReq)
            throws VException {
        logger.info("getUpcomingSessions Request : {}",  getOTFUpcomingSessionReq.toString());
        return otfSessionManagerV2.getUpcomingSessions(getOTFUpcomingSessionReq);
    }

    @GetMapping(value = "/getPastSessions")
    @ResponseBody
    public List<OTFSessionPojo> getPastSessions(@ModelAttribute GetOTFSessionsReq getOTFSessionsReq)
            throws VException {
        // Only allowed for ADMIN
        logger.info("Request : {}", getOTFSessionsReq.toString());
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        getOTFSessionsReq.verify();
        return otfSessionManagerV2.getPastSessions(getOTFSessionsReq, true);
    }

    @GetMapping(value = "/joinSession")
    @ResponseBody
    public String joinSession(@ModelAttribute OTFJoinSessionReq otfJoinSessionReq)
            throws VException {
        return otfSessionManagerV2.getJoinSessionUrl(otfJoinSessionReq, sessionUtils.getCurrentSessionData());
    }

    @GetMapping(value = "/getJoinSessionUrl")
    @ResponseBody
    public PlatformBasicResponse getJoinSessionUrl(@ModelAttribute OTFJoinSessionReq otfJoinSessionReq)
            throws VException {
        PlatformBasicResponse res=new PlatformBasicResponse();
        res.setResponse(otfSessionManagerV2.getJoinSessionUrl(otfJoinSessionReq, sessionUtils.getCurrentSessionData()));
        return res;
    }

//    @PostMapping(value = "/addBatchSession")
//    @ResponseBody
//    public PlatformBasicResponse addBatchSession(@RequestBody AddBatchSessionReq req) throws Exception {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
//        otfSessionManagerV2.addBatchSession(req);
//        return new PlatformBasicResponse();
//    }

    @PostMapping(value = "/cancelSession")
    @ResponseBody
    public UpdateSessionRes cancelSession(@RequestBody CancelOrDeleteSessionReq req) throws Exception {
        return otfSessionManagerV2.cancelSession(req);
    }

//    @GetMapping(value = "/getUserPastSessions")
//    @ResponseBody
//    public List<OTFSessionPojo> getUserPastSessions(@ModelAttribute GetOTFSessionsReq getOTFSessionsReq)
//            throws  VException {
//        // Only allowed for ADMIN
//        logger.info("Request : " + getOTFSessionsReq.toString());
//        List<Role> allowedRoles = new ArrayList<>();
//        allowedRoles.add(Role.STUDENT);
//        allowedRoles.add(Role.TEACHER);
//        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
//        return otfSessionManagerV2.getUserPastSessions(getOTFSessionsReq);
//    }

//    @GetMapping(value = "/otfPresenterLink")
//    @ResponseBody
//    public String getPresenterLink(@RequestParam("sessionId") String sessionId) throws VException {
//        OTFJoinSessionReq req = new OTFJoinSessionReq();
//        req.setSessionId(sessionId);
//        req.setRole(Role.TEACHER);
//        String sessionJson = joinSession(req);
//        logger.info("presenterLink json {}", sessionJson);
//
//        OTFSessionPojoUtils session = new Gson().fromJson(sessionJson, OTFSessionPojoUtils.class);
//        return session.getPresenterURL();
//    }

//    @PostMapping(value = "/updateSessionRecordingsById")
//    @ResponseBody
//    public PlatformBasicResponse updateSessionRecordingsById(
//            @RequestParam(name = "sessionId") String sessionId) {
//        logger.info("Request - sessionId:{}", sessionId);
//        otfSessionManagerV2.handleSessionRecordings(sessionId);
//        PlatformBasicResponse response = new PlatformBasicResponse();
//        response.setResponse(sessionId);
//        return response;
//    }

//    @PostMapping(value = "/invokeSessionRecordingLambda", consumes = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public PlatformBasicResponse invokeSessionRecordingLambda(
//            @RequestBody HandleOTFRecordingLambda recordingLambda) {
//        logger.info("Request - {}", recordingLambda.toString());
//        otfSessionManagerV2.invokeSessionRecordingLambda(recordingLambda);
//        PlatformBasicResponse response = new PlatformBasicResponse();
//        response.setResponse(GSON.toJson(recordingLambda));
//        return response;
//    }

//    @PostMapping(value = "/updateSessionRecordings")
//    @ResponseBody
//    public PlatformBasicResponse updateRecordings(
//            @RequestParam(name = "afterEndTime") Long afterEndTime,
//            @RequestParam(name = "beforeEndTime") Long beforeEndTime) {
//        return otfSessionManagerV2.updateRecordings(afterEndTime, beforeEndTime);
//    }

//    @PostMapping(value = "/updateSessionReplayUrls")
//    @ResponseBody
//    public PlatformBasicResponse updateSessionReplayUrls(
//            @RequestParam(name = "afterEndTime") Long afterEndTime,
//            @RequestParam(name = "beforeEndTime") Long beforeEndTime) {
//        return otfSessionManagerV2.updateSessionReplayUrls(afterEndTime, beforeEndTime);
//    }

    @PostMapping(value = "/updateReplayUrlforGTW")
    @ResponseBody
    public PlatformBasicResponse updateReplayUrlforGTW(@RequestBody UpdateReplayUrlReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        logger.info("Request: {}", req);
        PlatformBasicResponse response = otfSessionManagerV2.updateReplayUrlforGTW(req);
        otfSessionManagerV2.handleSessionRecordings(req.getSessionId());
        return response;
    }

    @PostMapping(value = "/teacherAddSessionToBatch", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Map<String, String> teacherAddSessionToBatch(@RequestBody AddOTFSessionsReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.TEACHER, Boolean.TRUE);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        List<OTFSessionPojo> sessions = otfSessionManagerV2.teacherAddSessionToBatch(req, sessionData);

        if(ArrayUtils.isEmpty(sessions) || sessions.get(0) == null || StringUtils.isEmpty(sessions.get(0).getId())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session not created try again later");
        }
        String sessionUrl = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT") +"/session/"+ sessions.get(0).getId();
        logger.info("session url : {}", sessionUrl);
        Map<String, String> respMap = new HashMap<>();
        respMap.put("sessionUrl",WebUtils.INSTANCE.shortenUrl(sessionUrl));
        return respMap;
    }

    @GetMapping(value = "/getUpcomingSessionsForUser")
    @ResponseBody
    public OTFCalendarSessionInfoRes getUserUpcomingSessions(@Valid GetSessionsReq req) throws VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(userId == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        if(role == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfSessionManagerV2
                .getUserUpcomingSessions(req.getStart(), req.getSize(), req.getBatchId(),
                        userId, role, req.getQuery(), req.getStartTime(), req.getEndTime(), req.getBatchIds());
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
                (oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);
    }

    @RequestMapping(value = "/getPastSessionsForUser", method = RequestMethod.GET)
    @ResponseBody
    public OTFCalendarSessionInfoRes getUserPastSessions(GetSessionsReq req) throws VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(userId == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        if(role == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfSessionManagerV2.getUserPastSessions(req.getStart(),
                req.getSize(), req.getBatchId(),
                userId, role, req.getQuery(), req.getStartTime(), req.getEndTime(), req.getBatchIds());
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
                (oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);

    }

    @GetMapping(value = "/getPastSessionsForUserWithFilter")
    @ResponseBody
    public OTFCalendarSessionInfoRes getPastSessionsForUserWithFilter(GetSessionsReq req) throws VException, UnsupportedEncodingException {
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(userId == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        if(role == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfSessionManagerV2.getPastSessionsForUserWithFilter(req.getStart(),
                req.getSize(), req.getBatchId(), userId, role, req.getQuery(), req.getStartTime(), req.getEndTime(), req.getBatchIds());
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
                (oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);

    }

    @GetMapping(value = "/getOTFSessionsForCalendar", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTFCalendarSessionInfoRes getOTFSessionsForCalendar(
            @RequestParam(name = "afterStartTime", required = false) Long afterStartTime,
            @RequestParam(name = "beforeStartTime", required = false) Long beforeStartTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime,
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole,
            @RequestParam(name = "userId", required = false) String userId,
            @RequestParam(name = "query", required = false) String query
    ) throws Exception {
        logger.info("afterStartTime {}, beforeStartTime {}, beforeEndTime {}, afterEndTime {}, callingUserId {}, callingUserRole {}, query {}",
                afterEndTime, beforeStartTime, beforeEndTime, afterEndTime, callingUserId, callingUserRole, query);

        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        Long requestUserId = sessionData.getUserId();
        Role requestUserRole = sessionData.getRole();
        if (Role.ADMIN.equals(requestUserRole) && !com.vedantu.util.StringUtils.isEmpty(userId)) {
            requestUserId = Long.parseLong(userId);

            // Update role
            UserBasicInfo requestUser = fosUtils.getUserBasicInfo(requestUserId, false);
            requestUserRole = requestUser.getRole();
        }
        String stringUserId = requestUserId == null ? null : String.valueOf(requestUserId);
        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfSessionManagerV2.getOTFSessionsForCalendar(beforeStartTime,
                afterStartTime, beforeEndTime, afterEndTime, stringUserId, requestUserRole, query);
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
                (oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);
    }

    @GetMapping(value = "/upcomingSessionsAndWebinarForUser", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public List<UpcomingSessionsPojo> upcomingSessionsAndWebinarForUser(@RequestParam("start") Integer start, @RequestParam("limit") Integer limit) throws VException {
        logger.info("upComingSessionsAndWebinarForUser {} {}", start, limit);
        sessionUtils.checkIfUserLoggedIn();
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.TEACHER), Boolean.FALSE);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        Long callingUserId = sessionData.getUserId();
        Role role = sessionData.getRole();
        logger.info("user id: {}, role: {}", callingUserId, role);
        return otfSessionManagerV2.upcomingSessionsAndWebinarForUser(String.valueOf(callingUserId), role, start, limit);
    }

}
