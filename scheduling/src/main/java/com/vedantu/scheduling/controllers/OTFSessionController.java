package com.vedantu.scheduling.controllers;

import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.managers.OTFSessionTaskManager;
import com.vedantu.scheduling.request.session.GetOTFSessionByIdsReq;
import com.vedantu.scheduling.request.session.GetUserUpcomingSessionReq;
import com.vedantu.scheduling.request.wavesession.SetAgoraRecordingDetailsRequest;
import com.vedantu.scheduling.request.wavesession.SetRecordingDetailsRequest;
import com.vedantu.scheduling.request.wavesession.SetTAsToSessionRequest;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.scheduling.request.*;
import com.vedantu.scheduling.request.wavesession.CreateTownhallSessionReq;
import com.vedantu.scheduling.response.wavesession.CreateTownhallSessionRes;
import com.vedantu.scheduling.response.*;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.app.requests.OTFSessionReq;
import com.vedantu.app.responses.WeeklySessionDetails;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.request.*;
import com.vedantu.onetofew.response.AddRemoveBatchIdsToSessionRes;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.managers.*;
import com.vedantu.scheduling.pojo.BatchCalendarPojo;
import com.vedantu.scheduling.pojo.CommonSessionInfo;
import com.vedantu.scheduling.pojo.GTTAggregatedStudentList;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.TeacherSessionDashboard;
import com.vedantu.scheduling.request.*;
import com.vedantu.scheduling.request.session.GetOTFSessionByIdsReq;
import com.vedantu.scheduling.request.wavesession.SetAgoraRecordingDetailsRequest;
import com.vedantu.scheduling.request.wavesession.SetRecordingDetailsRequest;
import com.vedantu.scheduling.request.wavesession.SetTAsToSessionRequest;
import com.vedantu.scheduling.response.*;
import com.vedantu.scheduling.response.session.CounselorDashboardSessionsInfoRes;
import com.vedantu.scheduling.response.session.OTFBatchSessionReportRes;
import com.vedantu.scheduling.response.session.OTFSessionPojoApp;
import com.vedantu.scheduling.response.session.OTFSessionStats;
import com.vedantu.session.pojo.OTFCalendarSessionInfo;
import com.vedantu.session.pojo.OTFCalendarSessionInfoRes;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.response.OTMSessionDashboardInfo;
import com.vedantu.subscription.response.StudentAttendanceInfo;
import com.vedantu.util.*;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;

@RestController
@RequestMapping("/onetofew/session")
public class OTFSessionController {

    @Autowired
    private OTFSessionDAO otfSessionDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private OTFSessionManager otfSessionManager;

    @Autowired
    private OTFSessionTaskManager otfSessionTaskManager;

    @Autowired
    private GTTAttendeeDetailsManager gttAttendeeDetailsManager;

    @Autowired
    private SessionRecordingManager sessionRecordingManager;

    @Autowired
    private AsyncTaskManager asyncTaskManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(OTFSessionController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    public static Long MILLIS_PER_MINUTE = 60000l;

    @RequestMapping(value = "/joinSession", method = RequestMethod.GET)
    @ResponseBody
    public String joinSession(@ModelAttribute OTFJoinSessionReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException {
        logger.info("getSession fetch :", req.toString());
        return otfSessionManager.joinSession(req);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getSessions(@RequestParam(name = "afterStartTime", required = false) Long afterStartTime,
            @RequestParam(name = "beforeStartTime", required = false) Long beforeStartTime,
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "teacherId", required = false) String teacherId,
            @RequestParam(name = "studentId", required = false) String studentId) throws VException {
        logger.info("getSessions", afterStartTime, beforeStartTime, teacherId, studentId);
        return otfSessionManager.getOtfSessions(afterStartTime, beforeStartTime, afterEndTime, teacherId, studentId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public OTFSession getSessionById(@PathVariable("id") String id) {
        OTFSession toRet = otfSessionDAO.getById(id);
        return toRet;
    }

    @RequestMapping(value = "/setWhiteboardDataMigrated/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSession addedWhiteboardDatatoS3(@PathVariable("id") String id) throws ConflictException {
        OTFSession toRet = otfSessionDAO.getById(id);
        toRet.addWhiteboardDataMigrated(true);
        otfSessionDAO.save(toRet);
        return toRet;
    }

    @RequestMapping(value = "/getOTFSessionForBatchIds", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<OTFSession> getOTFSessionForBatchIds(@RequestBody List<String> batchIds) throws VException {
        List<OTFSession> otfsessionlist = otfSessionManager.getLatestSession(batchIds);
        return otfsessionlist;
    }

    @RequestMapping(value = "/setVimeoVideoId/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSession setVimeoVideoId(@PathVariable("id") String id, @RequestBody SetRecordingDetailsRequest req)
            throws BadRequestException, NotFoundException, ConflictException {
        req.verify();
        OTFSession toRet = otfSessionDAO.getById(id);
        if (toRet == null) {
            String errorMessage = "Session does not exist for req : " + id;
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        if (StringUtils.isNotEmpty(req.getVimeoId())) {
            toRet.setVimeoId(req.getVimeoId());
        }
        otfSessionDAO.save(toRet);
        return toRet;
    }

    @RequestMapping(value = "/setAgoraVimeoVideoId/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSession setAgoraVimeoVideoId(@PathVariable("id") String id, @RequestBody SetAgoraRecordingDetailsRequest req)
            throws BadRequestException, NotFoundException, ConflictException {
        req.verify();
        OTFSession toRet = otfSessionDAO.getById(id);
        if (toRet == null) {
            String errorMessage = "Session does not exist for req : " + id;
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        if (StringUtils.isNotEmpty(req.getAgoraVimeoId())) {
            toRet.setAgoraVimeoId(req.getAgoraVimeoId());
        }
        otfSessionDAO.save(toRet);
        return toRet;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public OTFSession setSessions(@PathVariable("id") String id, @RequestBody OTFSession otfSession) throws ConflictException {
        OTFSession toRet = null;
        boolean sessionURLAndTokenValid = otfSession.getSessionURL() != null && otfSession.getOrganizerAccessToken() != null;
        boolean sessionURLAndTokenNull = otfSession.getSessionURL() == null && otfSession.getOrganizerAccessToken() == null;
        if (sessionURLAndTokenNull || sessionURLAndTokenValid) {
            OTFSession sessionOnDisk = otfSessionDAO.getById(id);
            sessionOnDisk.setSessionURL(otfSession.getSessionURL());
            sessionOnDisk.setOrganizerAccessToken(otfSession.getOrganizerAccessToken());
            otfSessionDAO.save(sessionOnDisk);
            toRet = sessionOnDisk;
        } else {
            logger.error("validation errors in session url or organizer access key ");
            throw new IllegalArgumentException(
                    "Illegal Arguments with fields both organizer access key and session url should be null or not null");
        }
        return toRet;
    }

    @RequestMapping(value = "/createLink/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSessionPojo createSessionLinks(@PathVariable("id") String id)
            throws VException {
        return otfSessionManager.createSessionLink(id);
    }

    @RequestMapping(value = "/addBatchSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSessionPojo addBatchSession(@RequestBody AddBatchSessionReq req) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.addBatchSession(req);
    }

    @RequestMapping(value = "/endedSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSession> getEndedSessions(@RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "end", required = false) Integer end) {
        return otfSessionManager.getEndedSessions(afterEndTime, beforeEndTime, start, end);
    }

    @RequestMapping(value = "/endedSessionDuration", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public long getEndedSessionDuration(@RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime) throws VException {
        return otfSessionManager.getTotalSessionDuration(afterEndTime, beforeEndTime);
    }

    // test api
    @Deprecated
    @RequestMapping(value = "/testDownloadUrl", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void testDownloadUrl(@RequestParam(name = "sessionId", required = false) String sessionId,
            @RequestParam(name = "url", required = false) String url) throws InternalServerErrorException {
        List<String> downloadRecordingUrls = new ArrayList<>();
        downloadRecordingUrls.add(url);
        // sessionRecordingManager.downloadAndUploadToAWSS3(downloadRecordingUrls,
        // sessionId);
    }

    @RequestMapping(value = "/getRecordingDownloadUrl", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getRecordingDownloadUrl(@RequestParam(name = "sessionId") String sessionId,
            @RequestParam(name = "recordingId") String recordingId) throws NotFoundException {
        String url = sessionRecordingManager.getDownloadUrl(sessionId, recordingId);
        return new PlatformBasicResponse(true, url, null);
    }

    @RequestMapping(value = "/getSessionNotesDownloadUrl", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getSessionNotesDownloadUrl(@RequestParam(name = "sessionId", required = true) String sessionId) throws NotFoundException {
        Long userId = sessionUtils.getCallingUserId();
        String url = sessionRecordingManager.getSessionNotesDownloadUrl(sessionId, userId);
        return new PlatformBasicResponse(true, url, null);
    }

    @RequestMapping(value = "/getSessionNotesUploadUrl", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getSessionNotesUploadUrl(@RequestParam(name = "sessionId", required = true) String sessionId) throws NotFoundException {
        String url = sessionRecordingManager.getSessionNotesUploadUrl(sessionId);
        return new PlatformBasicResponse(true, url, null);
    }

    @RequestMapping(value = "/getCalendarSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getCalendarSessions(
            @RequestParam(name = "afterStartTime", required = false) Long afterStartTime,
            @RequestParam(name = "beforeStartTime", required = false) Long beforeStartTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime,
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole,
            @RequestParam(name = "query", required = false) String query) throws VException {
        return otfSessionManager.getCalendarSessions(afterStartTime, beforeStartTime, beforeEndTime, afterEndTime,
                callingUserId, callingUserRole, query);
    }

    @RequestMapping(value = "/getUserUpcomingSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getUserUpcomingSessions(
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "query", required = false) String query,
            @Deprecated @RequestParam(name = "batchId", required = false) String batchId,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole,
            @RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "batchIds", required = false) Set<String> batchIds
    ) throws VException {

        return otfSessionManager.getUpcomingSessions(callingUserId, callingUserRole, query, start, size, batchId, startTime, endTime, batchIds);
    }

    @RequestMapping(value = "/getUserPastSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getUserPastSessions(
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "query", required = false) String query,
            @Deprecated @RequestParam(name = "batchId", required = false) String batchId,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole,
            @RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "batchIds", required = false) Set<String> batchIds
    ) throws VException {

        return otfSessionManager.getPastSessions(callingUserId, callingUserRole, query, start, size, batchId, startTime, endTime, batchIds);
    }

    @RequestMapping(value = "/getCalendarSessionsByEntity", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<BatchCalendarPojo> getCalendarSessoinByEntity(
            @RequestParam(name = "afterStartTime", required = false) Long afterStartTime,
            @RequestParam(name = "beforeStartTime", required = false) Long beforeStartTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime,
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "grade", required = false) List<String> grade,
            @RequestParam(name = "identifiers", required = false) List<String> identifiers,
            @RequestParam(name = "type", required = false) String type) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.getCalendarSessionByEntity(afterStartTime, beforeStartTime, beforeEndTime, afterEndTime, grade, identifiers, type);
    }

    @RequestMapping(value = "/getUpcomingSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getUpcomingSessions(
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "teacherId", required = false) String teacherId,
            @RequestParam(name = "studentId", required = false) String studentId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "limit", required = false) Integer limit,
            @RequestParam(name = "batchId", required = false) String batchId,
            @RequestParam(name = "query", required = false) String query) throws VException {
        return otfSessionManager.getUpcomingSessions(afterEndTime, teacherId, studentId,
                batchId, start, limit, query, false);
    }

    // using for internal purpose
    @RequestMapping(value ="/getLatestUpcomingSessionsForBatch", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getLatestUpcomingSessionsForBatch(@RequestParam(name = "batchId")String batchId){
        return otfSessionManager.getLatestUpcomingSessionsForBatch(batchId);
    }

    @RequestMapping(value = "/getUpcomingSessionsForTicker", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetLatestUpComingOrOnGoingSessionResponse getUpcomingSessionsForTicker(
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "teacherId", required = false) String teacherId,
            @RequestParam(name = "studentId", required = false) String studentId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "limit", required = false) Integer limit) throws VException {
        return otfSessionManager.getUpcomingSessionsForTicker(afterEndTime, teacherId, studentId, start, limit);
    }

    @RequestMapping(value = "/registerAttendees", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<GTTAttendeeDetails> registerAttendees(
            @RequestParam(name = "sessionId", required = true) String sessionId,
            @RequestParam(name = "userId", required = false) String userId)
            throws VException {
        return otfSessionManager.registerAttendees(sessionId, userId);
    }

    @RequestMapping(value = "/getJoinedAttendeeDetailsOfUser", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<GTTAttendeeDetails> getJoinedAttendeeDetailsOfUser(
            @RequestParam(name = "userId", required = true) String userId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size)
            throws NotFoundException, BadRequestException, InternalServerErrorException {
        return gttAttendeeDetailsManager.getJoinedAttendeeDetailsOfUser(userId, start, size);
    }

    @RequestMapping(value = "/updateReplayWatchedDuration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateReplayWatchedDuration(@RequestBody UpdateReplayWatchedDurationReq req) throws VException {
        gttAttendeeDetailsManager.updateReplayWatchedDuration(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getSessionsBasicInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSession> getSessionsBasicInfos(
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime,
            @RequestParam(name = "afterEndTime", required = false) Long afterEndTime) throws VException {
        return otfSessionManager.getSessionsBasicInfos(afterEndTime, beforeEndTime);
    }

    @RequestMapping(value = "/getUserUpcomingScheduledSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getUserUpcomingScheduledSessions(
            @RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "teacherId", required = false) String teacherId,
            @RequestParam(name = "studentId", required = false) String studentId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "limit", required = false) Integer limit) throws VException {
        logger.info("Request:" + startTime + " endTime:" + endTime + " teacherId:" + teacherId + " studentId:"
                + studentId + " start:" + start + " limit:" + limit);
        return otfSessionManager.getUserUpcomingScheduledSessions(startTime, endTime, teacherId, studentId, start, limit);
    }

    @RequestMapping(value = "/getUpcomingSessionsWithAttendeeInfos", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getUpcomingSessionsWithAttendeeInfos(
            @RequestParam(name = "startTime", required = true) Long startTime,
            @RequestParam(name = "endTime", required = true) Long endTime,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "sessionId", required = false) String sessionId,
            HttpServletRequest servletRequest) throws VException {
        logger.info("Request:" + startTime + " endTime:" + endTime);
        Map<String, Object> requestDetails = new HashMap<>();
        requestDetails.put("requestURI", servletRequest.getRequestURI());
        requestDetails.put(HttpSessionUtils.Constants.CLIENT, servletRequest.getHeader("X-Ved-Client"));
        requestDetails.put(HttpSessionUtils.Constants.CLIENT_ID, servletRequest.getHeader("X-Ved-Client-Id"));
        requestDetails.put(HttpSessionUtils.Constants.CLIENT_SECRET, servletRequest.getHeader("X-Ved-Client-Secret"));
        if (!sessionUtils.isAllowedApp(requestDetails) && sessionUtils.getCurrentSessionData() != null) {
            sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        } else {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User forbidden to use this API");
        }
        return otfSessionManager.getUpcomingSessionsWithAttendeeInfos(startTime, endTime, start, size, sessionId);
    }

    @RequestMapping(value = "/getSessionsForRecordingDeletion", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getSessionsForRecordingDeletion(@ModelAttribute GetOTFSessionsForRecordingDeletionReq req)
            throws BadRequestException {
        return otfSessionManager.getSessionsForRecordingDeletion(req);
    }

    @RequestMapping(value = "/getPastSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFSessionPojo> getPastSessions(@ModelAttribute GetOTFSessionsReq getOTFSessionsReq)
            throws VException {
        logger.info("Request:" + getOTFSessionsReq.toString());
        return otfSessionManager.getPastSessions(getOTFSessionsReq);
    }

    // ---------- OTF recording APIs ------------//
    @RequestMapping(value = "/handleSesssionRecordings", method = RequestMethod.GET)
    @ResponseBody
    public HandleOTFRecordingLambda handleSesssionRecordings(
            @RequestParam(name = "sessionId", required = true) String sessionId)
            throws NotFoundException, BadRequestException, InternalServerErrorException, ConflictException {
        return sessionRecordingManager.handleSessionRecording(sessionId);
    }

    @RequestMapping(value = "/handleSesssionRecordingsBulk", method = RequestMethod.GET)
    @ResponseBody
    public List<HandleOTFRecordingLambda> handleSesssionRecordingsBulk(
            @RequestParam(name = "afterEndTime", required = true) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = true) Long beforeEndTime)
            throws NotFoundException, BadRequestException, InternalServerErrorException {
        // afterEndTime, Long beforeEndTime
        logger.info("ENTRY updateSessionRecordings");
        return sessionRecordingManager.handleSesssionRecordingsBulk(afterEndTime, beforeEndTime);
    }

    @RequestMapping(value = "/updateSessionReplayUrls", method = RequestMethod.POST)
    @ResponseBody
    public List<OTFSession> updateSessionReplayUrls(
            @RequestParam(name = "afterEndTime", required = true) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = true) Long beforeEndTime)
            throws NotFoundException, BadRequestException, InternalServerErrorException {
        // afterEndTime, Long beforeEndTime
        logger.info("ENTRY updateSessionRecordings");
        return sessionRecordingManager.updateSessionReplayUrls(afterEndTime, beforeEndTime);
    }

    @RequestMapping(value = "/updateAttendeeReport", method = RequestMethod.POST)
    @ResponseBody
    public List<OTFSession> updateAttendeeReport(@RequestParam(name = "afterEndTime", required = true) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = true) Long beforeEndTime)
            throws NotFoundException, BadRequestException, InternalServerErrorException {
        // afterEndTime, Long beforeEndTime
        logger.info("ENTRY updateSessionRecordings");
        return otfSessionManager.updateAttendeeReport(afterEndTime, beforeEndTime);
    }

    // ---------- Webinar session APIs ------------//
    @RequestMapping(value = "/webinar/updateBulk", method = RequestMethod.POST)
    @ResponseBody
    public List<SessionPojoWithRefId> addWebinarSessions(@RequestBody OTFWebinarSessionReq req)
            throws NotFoundException, BadRequestException, InternalServerErrorException, ConflictException {
        logger.info("ENTRY addWebinarSessions : " + req.toString());
        return otfSessionManager.addWebinarSessions(req);
    }

    @RequestMapping(value = "/webinar/get", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojo> getWebinarSessions(@ModelAttribute GetOTFWebinarSessionsReq req)
            throws NotFoundException, BadRequestException, InternalServerErrorException {
        logger.info("ENTRY getWebinarSessions : " + req.toString());
        return otfSessionManager.getWebinarSessionInfos(req, false);
    }

    @RequestMapping(value = "/webinar/cancel", method = RequestMethod.POST)
    @ResponseBody
    public List<OTFSessionPojo> cancelWebinarSession(@RequestBody CancelOTFWebinarSessionsReq req) throws Exception {
        logger.info("ENTRY cancelWebinarSessions : " + req.toString());
        return otfSessionManager.cancelWebinarSession(req);
    }

    @RequestMapping(value = "/webinar/attendee/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFSessionPojo> attendWebinarSession(
            @RequestBody UpdateWebinarSessionsAttendenceReq attendWebinarSessionsReq)
            throws InternalServerErrorException, BadRequestException, NotFoundException, ConflictException {
        return otfSessionManager.updateWebinarSessionAttendee(attendWebinarSessionsReq);
    }

    @RequestMapping(value = "/updateflag", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicResponse sessionUpdateflag(@RequestBody UpdateOTFSessionFlag updateOTFSessionFlag)
            throws InternalServerErrorException, BadRequestException, NotFoundException, ConflictException {
        otfSessionManager.updateSessionFlags(updateOTFSessionFlag);
        return new BasicResponse();
    }

    @RequestMapping(value = "/getLastUpdatedUserIds", method = RequestMethod.GET)
    @ResponseBody
    public LastUpdatedUsers getLastUpdatedUserIds(@RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime", required = true) long endTime)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return new LastUpdatedUsers();
    }

    @RequestMapping(value = "/getSessionIdsForCurriculum", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getSessionIdsForCurriculum(@RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "teacherId", required = false) Long teacherId,
            @RequestParam(name = "batchId", required = false) String batchId) {
        return otfSessionManager.getSessionIdsForCurriculum(startTime, endTime, teacherId, batchId);
    }

    @RequestMapping(value = "/getStudentAttendenceCountWithTotal", method = RequestMethod.GET)
    @ResponseBody
    public OTFAttendance getStudentAttendenceCountWithTotal(StudentBatchProgressReq req) throws BadRequestException {
        return otfSessionManager.getStudentAttendenceCountWithTotal(req);
    }

    @RequestMapping(value = "/getTeacherForBatchIdAndBoardId", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionInfo> getTeacherForBatchIdAndBoardId(@RequestParam(name = "boardId", required = false) Long boardId,
            @RequestParam(name = "batchId", required = true) String batchId) throws BadRequestException {
        return otfSessionManager.getTeacherForBatchIdAndBoardId(boardId, batchId);
    }

    // Get start time of shared assignments with session subtype for the given subject(boardId) and batch(contextId)
    @GetMapping(value = "/getNextSessionTimeForBatchIdAndBoardId")
    @ResponseBody
    public Long getNextSessionTimeForBatchIdAndBoardId(@RequestParam(name = "boardId", required = false) String boardIds,
            @RequestParam(name = "batchId", required = true) String batchId, @RequestParam(name = "startTime", required = true) Long startTime) {

    	logger.info("getNextSessionTimeForBatchIdAndBoardId {} , {} , {} ", boardIds, batchId, startTime );
    	List<String> boardIds1 = Arrays.asList(boardIds.split(","));
    	ArrayList<Long> boardIds2 = new ArrayList<>();
    	for(String s : boardIds1)
    		boardIds2.add(Long.valueOf(s));
        return otfSessionManager.getNextSessionTimeForBatchIdAndBoardId(boardIds2, batchId, startTime);
    }

    // TODO Removal
    @Deprecated
    @RequestMapping(value = "/getAttendanceForStudentsAndBoard", method = RequestMethod.POST)
    @ResponseBody
    public List<OTFAttendance> getAttendanceForStudentsAndBoard(@RequestBody TeacherBatchProgressReq req) throws BadRequestException {
//        return oTFSessionManager.getAttendanceForStudentsAndBoard(req);
        return new ArrayList<>();
    }

    // TODO Removal
    @Deprecated
    @RequestMapping(value = "/getPastSessionsForBatchProgress", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionInfo> getPastSessionsForBatchProgress(TeacherBatchProgressReq req) throws NotFoundException, BadRequestException {
//        return oTFSessionManager.getPastSessionsForBatchProgress(req);
        return new ArrayList<>();
    }

    @RequestMapping(value = "/cancelSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public UpdateSessionRes cancelSession(@RequestBody CancelOrDeleteSessionReq req) throws Exception {
        return otfSessionManager.cancelSession(req);
    }

    @RequestMapping(value = "/updateReplayUrlforGTW", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse updateReplayUrlforGTW(@RequestBody UpdateReplayUrlReq req) throws Exception {
        otfSessionManager.updateReplayUrlforGTW(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/updateSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public UpdateSessionRes updateSession(@RequestBody OTFSession otfSession) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("updateSession session : " + otfSession);
        if (otfSession == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "session id not found");
        }

        return otfSessionManager.updateSession(otfSession);
    }

    @Deprecated
    @RequestMapping(value = "/createSessions", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> createSessions(@RequestBody CreateOTFSessionsReq req) throws VException {
        req.verify();
        return otfSessionManager.createSessions(req);
    }

    @RequestMapping(value = "/updateBatchToolType", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void updateBatchToolType(@RequestBody CreateOTFSessionsReq req) throws BadRequestException {
        if (StringUtils.isEmpty(req.getBatchId()) || req.getSessionToolType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchId is null or tooltype is null");
        }
        if (req.getSessionToolType() != null && req.getSessionToolType().isUnsupported()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "using deprecated session tool type");
        }
        otfSessionManager.updateBatchToolType(req.getBatchId(), req.getSessionToolType());
    }

    @RequestMapping(value = "/descheduleSessions/{batchId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void descheduleSessions(@PathVariable("batchId") String batchId) throws VException {
        if (StringUtils.isEmpty(batchId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchId is null");
        }
        otfSessionManager.descheduleSessions(batchId);
    }

    @RequestMapping(value = "/getAttendanceForBatch", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, List<OTFAttendance>> getAttendanceForBatch(@RequestParam("batchId") String batchId,
            @RequestParam("beforeEndTime") Long beforeEndTime) throws BadRequestException, NotFoundException {

        return otfSessionManager.getAttendanceForBatch(batchId, beforeEndTime);
    }

    @RequestMapping(value = "/viewAttendanceForUser", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojo> viewAttendanceForUser(ViewAttendanceForUserBatchReq req)
            throws BadRequestException, NotFoundException {

        return otfSessionManager.viewAttendanceForUser(req);
    }

    @RequestMapping(value = "/getTeacherSessionForDashboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<Long, TeacherSessionDashboard> getTeacherSessionForDashboard(@RequestParam("startTime") Long startTime,
            @RequestParam("endTime") Long endTime) throws BadRequestException, NotFoundException {

        if (startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start or endTime not given");
        }
        return otfSessionManager.getTeacherSessionForDashboard(startTime, endTime);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojo> getOTFSessions(GetOTFSessionsListReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException {
        logger.info("getSession fetch :", req.toString());
        req.verify();
        return otfSessionManager.getOTFSessions(req);
    }

    @RequestMapping(value = "/getWaveSessionsStats", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojo> getWaveDailyStatsSessions(GetOTFSessionsListReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException {
        logger.info("getSession fetch :", req.toString());
        req.verify();
        return otfSessionManager.getOtfSessionsForDailyStats(req);
    }

    //this api is called from web/mobile apps
    @RequestMapping(value = "/getSessionsForUser", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionForUserRes> getSessionsForUser(GetOTFSessionsListReq req)
            throws VException {
        logger.info("getSession fetch :", req.toString());
        req.verify();
        return otfSessionManager.getSessionsForUser(req);
    }

    //this api is called from web/mobile apps
    @RequestMapping(value = "/getOTFSessionsBasicsInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionBasicInfo> getOTFSessionsBasicsInfos(GetOTFSessionsListReq req)
            throws VException {
        logger.info("getSession fetch :", req.toString());
        req.verify();
        return otfSessionManager.getOTFSessionsBasicsInfos(req);
    }

    @RequestMapping(value = "/addSessions", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> addSessions(@RequestBody AddOTFSessionsReq req) throws VException {

        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN,Role.TEACHER), Boolean.TRUE);
        req.verify();
        if (req.getSessionPojo().size() > 50) {
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule more than 50 sessions at once");
        }

        return  otfSessionManager.addOTFSessionsToBatch(req);
    }

    @RequestMapping(value = "/addUpdateSessionContent", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSessionPojo addUpdateSessionContent(@RequestBody AddUpdateSessionContentReq req) throws Exception {
        req.verify();
        return otfSessionManager.addUpdateSessionContent(req);
    }

    @RequestMapping(value = "/updateAsyncEnrollmentTask", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void updateAsyncEnrollmentTask(@RequestBody EnrollmentUpdatePojo req) throws VException {
        asyncTaskManager.updateAsyncEnrollmentTask(req);
    }

    @RequestMapping(value = "/createAsyncGttAttendeeTask", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void createAsyncGttAttendeeTask(@RequestBody EnrollmentUpdatePojo req) throws VException {
        asyncTaskManager.createAsyncGttAttendeeTask(req);
    }

    // create a gtt attendee detail here
    @RequestMapping(value = "/createGttAttendee", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void createGttAttendee(@RequestBody EnrollmentUpdatePojo req) throws VException {
        asyncTaskManager.createGttAttendee(req);
    }

    @RequestMapping(value = "/getOTFSessionSnapshots", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<SessionSnapshot> getSessionSnapshots(@RequestParam(name = "batchId", required = true) String batchId,
            @RequestParam(name = "endTime", required = true) Long endTime,
            @RequestParam(name = "startTime", required = true) Long startTime) {
        List<OTFSession> otfSessionList = otfSessionDAO.getSessionForBatchIdBetweenTime(batchId, startTime, endTime);
        List<SessionSnapshot> sessionSnapshotList = new ArrayList<>();
        for (OTFSession otfSession : otfSessionList) {

            if (otfSession.isAbruptEnded() || OTMSessionType.PTM == otfSession.getOtmSessionType()
                    || OTMSessionType.NON_REGULAR == otfSession.getOtmSessionType()
                    || OTMSessionType.TEST == otfSession.getOtmSessionType()) {
                continue;
            }

            SessionSnapshot sessionSnapshot = new SessionSnapshot();
            sessionSnapshot.setSessionId(otfSession.getId());
            sessionSnapshot.setStartTime(otfSession.getStartTime());
            sessionSnapshot.setEndTime(otfSession.getEndTime());
            sessionSnapshot.setBatchIds(new ArrayList<>(otfSession.getBatchIds()));
            sessionSnapshot.setoTMSessionType(otfSession.getOtmSessionType());
            sessionSnapshotList.add(sessionSnapshot);
        }

        return sessionSnapshotList;

    }

    @RequestMapping(value = "/getSessionStatsForBatches", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, OTFSessionStats> getOTFSessionsStats(@RequestParam(value = "batchIds", required = true) List<String> batchIds,
            @RequestParam(value = "time", required = true) Long time) {
        return otfSessionManager.getOTFSessionStats(batchIds, time);
    }

    @RequestMapping(value = "/addBatchIdsToSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public AddRemoveBatchIdsToSessionRes addBatchIdsToSession(@RequestBody AddRemoveBatchIdsToSessionReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();
        return otfSessionManager.addBatchIdsToSession(req);
    }

    @RequestMapping(value = "/removeBatchIdsFromSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public AddRemoveBatchIdsToSessionRes removeBatchIdsFromSession(@RequestBody AddRemoveBatchIdsToSessionReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();
        return otfSessionManager.removeBatchIdsFromSession(req);
    }

    @RequestMapping(value="/updateEntityTypeForSession",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse updateEntityTypeForSession(@RequestBody UpdateEntityTypeForSessionReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.handleEntityChangeForSession(req);
    }

    @RequestMapping(value = "/isStudentPartOfSession", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse isStudentPartOfSession(@RequestParam(name = "userId", required = true) String userId,
            @RequestParam(name = "sessionId", required = true) String sessionId) throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException {
        return otfSessionManager.isStudentPartOfSession(userId, sessionId);
    }

    @RequestMapping(value = "/getOTMSessionDashboardPartialInfo", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, OTMSessionDashboardInfo> getOTMSessionDashboardPartialInfo(@RequestParam(name = "batchIds", required = true) List<String> batchIds,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size) {

        return otfSessionManager.getOTMDashboardPartialInfo(batchIds, start, size);

    }

    @RequestMapping(value = "/sessionAttendance", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentAttendanceInfo> getOTMSessionAttendanceInfo(@RequestParam(name = "sessionId", required = true) String sessionId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size) throws VException {
            sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.getOTMSessionAttendanceInfo(sessionId, start, size);
    }

    @RequestMapping(value = "/getOtfBatchSessionReport", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFBatchSessionReportRes> getOtfBatchSessionReport(@RequestParam(name = "batchIds", required = true) List<String> batchIds, @RequestParam(name = "userId", required = true) String userId) throws VException {
        return otfSessionManager.getOtfBatchSessionReport(batchIds, userId);
    }

    @RequestMapping(value = "/getFirstRegularSessionForBatch", method = RequestMethod.GET)
    @ResponseBody
    public List<BatchFirstRegularSessionPojo> getFirstRegularSessionForBatch(@RequestParam(name = "batchIds", required = true) List<String> batchIds) throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException {
        return otfSessionManager.getFirstRegularSessionForBatch(batchIds);
    }

    @RequestMapping(value = "/getSessionInfosForBatchIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, CounselorDashboardSessionsInfoRes> getSessionInfosForBatchIds(@RequestParam(name = "batchIds") List<String> batchIds)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return otfSessionManager.getSessionInfosForBatchIds(batchIds);
    }

    @RequestMapping(value = "/resetOrganizerTokens", method = RequestMethod.POST)
    @ResponseBody
    public void resetOrganizerTokens(@RequestParam(name = "startTime", required = true) Long startTime,
            @RequestParam(name = "organizerToken", required = true) String organizerToken)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        otfSessionManager.resetOrganizerTokens(startTime, organizerToken);

    }

    @RequestMapping(value = "/updatePollsMetadataForSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updatePollsMetadataForSession(@RequestBody UpdateSessionPollsDataReq req)
            throws VException {
        return otfSessionManager.updatePollsMetadataForSession(req);
    }

    @RequestMapping(value = "/launchPollsDataLambdaForSession", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse launchPollsDataLambdaForSession(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException {
        OTFSession session = otfSessionDAO.getById(sessionId);
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + sessionId;
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        if (!session.hasVedantuWaveFeatures()) {
            return otfSessionManager.launchPollsDataLambdaForSession(session);
        } else {
            return new PlatformBasicResponse();
        }

    }

    @RequestMapping(value = "/sendOtmNoShowEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendOtmNoShowEmail(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException, UnsupportedEncodingException {
        OTFSession session = otfSessionDAO.getById(sessionId);
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + sessionId;
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        otfSessionManager.sendOtmNoShowEmail(session);
        return;
    }

    @RequestMapping(value = "/sendPostSessionEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendPostSessionEmail(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException, UnsupportedEncodingException {
        OTFSession session = otfSessionDAO.getById(sessionId);
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + sessionId;
            logger.info(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        otfSessionManager.sendPostSessionEmail(session);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/syncPollsData", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void syncPollsData(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            otfSessionManager.syncPollsDataAsync();
        }
    }

    @RequestMapping(value = "/sendPostSessionEmails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendPostSessionEmails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            otfSessionManager.sendPostSessionEmailAsync();
        }
    }

    @RequestMapping(value = "/setAdminTechSupport", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateOTFSessionAdminTechSupport(@RequestBody AddAdminTechSupportToSession addAdminTechSupportToSession) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.updateOTFSessionAdminTechSupport(addAdminTechSupportToSession);
    }

    @RequestMapping(value = "/getUpdatedSessionAttendeeInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, SessionsAttendeeInfoRes> getUpdatedSessionInfoMap(@RequestParam(name = "sessionIds", required = true) Set<String> sessionIds) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.getUpdatedSessionAttendeeInfo(sessionIds);
    }

    @RequestMapping(value = "/getAttendeeInfoForSession", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFSessionAttendeeInfo> getOTFSessionAttendeeInfos(@RequestParam(name = "sessionId", required = true) String sessionId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.getOTFSessionAttendeeInfos(sessionId);
    }

    @RequestMapping(value = "/deleteSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse deleteSession(@RequestBody CancelOrDeleteSessionReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.deleteSession(req);
    }

    @RequestMapping(value = "/getAttendeeInfoForBatchAndUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GTTAttendeeSessionInfo> getOTFSessionAttendeeInfosForBatchIdsAndUser(@RequestParam(name = "batchIds", required = true) List<String> batchIds, @RequestParam(name = "userId", required = true) String userId) {
        return otfSessionManager.getAttendenceForBatches(batchIds, userId);
    }

    @RequestMapping(value = "/getUserSessionPostLogin", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CommonSessionInfo> getUserSessionPostLogin() throws VException {
        Long userId = sessionUtils.getCallingUserId();
        if (userId == null) {
            throw new BadRequestException(ErrorCode.USER_LOGIN_REQUIRED, "");
        }
        Role role = sessionUtils.getCallingUserRole();

        if (!(Role.STUDENT.equals(role) || Role.TEACHER.equals(role))) {
            throw new BadRequestException(ErrorCode.USER_NOT_VALID_STUDENT, "");
        }

        return otfSessionManager.getCommonSessionInfos(userId.toString(), role);
    }

    @RequestMapping(value = "/getTeacherIdsForBatchSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Set<String> getTeacherIdsForBatchSessions(@RequestParam(name = "batchId", required = true) String batchId,
            @RequestParam(name = "startTime", required = true) Long startTime,
            @RequestParam(name = "endTime", required = true) Long endTime,
            @RequestParam(value = "teacherIds", required = false) ArrayList<String> teacherIds) {
        return otfSessionManager.getTeacherIdsForBatchSessions(batchId, startTime, endTime, teacherIds);
    }

    @Deprecated
    @RequestMapping(value = "/testapi", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse testapi() {
        otfSessionManager.addJoinDetails("58d2a50fe4b0da4b56115a68", "24", "45.112.138.98", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/updateOrganizerTokenLambda", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void updateOrganizerTokenLambda(@RequestBody UpdateOrganizerTokenReq req) throws VException {
        otfSessionManager.updateOrganizerTokens(req);
    }

    @RequestMapping(value = "/getOrganizerTokensLambda", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<GTTOrganizerToken> getOrganizerTokensLambda(@RequestParam(name = "secKey", required = true) String secKey,
            @RequestParam(name = "seatValues", required = false) List<String> seatValues) throws VException {
        if (!StringUtils.isEmpty(secKey) && "Bourgeouis24*)".equals(secKey)) {
            return otfSessionManager.getOrganizerTokens(seatValues);
        }
        logger.info("Unauthenticated request");
        return null;
    }

    @RequestMapping(value = "/checkOrganizerTokenExpiry", method = RequestMethod.GET)
    @ResponseBody
    public String checkOrganizerTokenExpiry() throws VException {
        return otfSessionManager.checkOrganizerTokenExpiry();
    }

    @RequestMapping(value = "/addWebinarSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public OTFSession addWebinarSession(@RequestBody AddWebinarSessionReq req) throws Exception {
        req.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return otfSessionManager.addWebinarSession(req);
    }

    @RequestMapping(value = "/sendNotificationToAcadOps",method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendNotificationToAcadOps(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                        @RequestBody String request) throws VException {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {
            otfSessionManager.sendNotificationToAcadOps();
        }
    }

    @RequestMapping(value = "/getSessionsForUserWithBatchShuffle", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojo> getSessionsForUserWithBatchShuffle(@ModelAttribute GetOTFSessionsListReq req)
            throws VException {
        logger.info("getSession fetch :", req.toString());
        req.verify();
        return otfSessionManager.getSessionsForUserWithBatchShuffle(req);
    }

    @RequestMapping(value = "/cleanGTTData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> cleanGTTData(){
        return gttAttendeeDetailsManager.cleanGTTData();
    }

    @RequestMapping(value = "/getSessionsForActiveUserTools", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojoUtils> getSessionsForActiveUserTools(
            @RequestParam(name = "userEmail", required = true) String userEmail,
            @RequestParam(name = "startTime", required = true) Long startTime,
            @RequestParam(name = "endTime", required = true) Long endTime,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size
            ) throws VException {
        if(sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData())) {
        Set<String> emailFromReqSet = new HashSet<>();
        emailFromReqSet.add(userEmail);
        Map<String, UserInfo> userInfoFromMail = fosUtils.getUserInfosFromEmails(emailFromReqSet, false);
            if (userInfoFromMail.get( userEmail ) == null || !Role.STUDENT.equals( userInfoFromMail.get( userEmail ).getRole() )) {
                throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND, "Please enter valid student email");
            }
            return otfSessionManager.getSessionsForActiveUserTools(userInfoFromMail.get(userEmail), startTime, endTime, start, size);
        }
        else{
            return new ArrayList<>();
        }
    }


    @RequestMapping(value = "/getUserPastSessionsWithFilter", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojo> getUserPastSessionsWithFilter(
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "query", required = false) String query,
            @Deprecated @RequestParam(name = "batchId", required = false) String batchId,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole,
            @RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "batchIds", required = false) Set<String> batchIds
            ) throws VException, UnsupportedEncodingException {

        return otfSessionManager.getUserPastSessionsWithFilter(callingUserId, callingUserRole, query, start, size, batchId, startTime, endTime, batchIds);
    }
    @RequestMapping(value = "/removeGTT", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void removeGTT(   @RequestParam(name = "userId", required = true) String userId,@RequestParam(name = "batchId", required = true) String batchId){
        gttAttendeeDetailsManager.removeGTT(userId,batchId);
    }

    @RequestMapping(value = "/getNewAbsenteeStudentListForSession", method = RequestMethod.GET)
    @ResponseBody
    public List<GTTAggregatedStudentList> getNewAbsenteeStudentListForSession( 
        @RequestParam(name = "sessionId", required = true) String sessionId,
        @RequestParam(name = "lastSessionId", required = false) String lastSessionId   
    ) throws VException{
        return otfSessionManager.getNewAbsenteeStudentListForSession(sessionId, lastSessionId);
    }

    @RequestMapping(value = "/getTeachersPastActiveSession", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getTeachersPastActiveSession( 
        @RequestParam(name = "presenter", required = true) String presenter, 
        @RequestParam(name = "batchIds", required = true) Set<String> batchIds,
        @RequestParam(name = "size", required = false) Integer size,
        @RequestParam(name = "start", required = false) Integer start
    ) throws VException {
        return otfSessionManager.getTeachersPastActiveSession(presenter, batchIds, size, start);
    }

    @RequestMapping(value = "/setTAsToSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE ,produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse setTAsToSession(@RequestBody SetTAsToSessionRequest req, HttpServletRequest request) throws VException {
        sessionUtils.isAllowedApp(request);
        otfSessionManager.setTAsToSession(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/addSalesDemoSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Map<String, String> addSalesDemoSession(@RequestBody AddSalesDemoSessionReq req)
            throws VException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.TEACHER,Role.ADMIN), false);

        req.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData != null && sessionData.getRole() != null && Role.TEACHER.equals(sessionData.getRole())) {
            Set<SubRole> subRoles = sessionData.getSubRoles();
            if (subRoles != null && !(subRoles.contains(SubRole.SALES)))
            {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "this only availabe SALES Person and admin only");
            }
            if(StringUtils.isNotEmpty(sessionData.getEmail())){
                req.setPresenterEmail(sessionData.getEmail());
            }else if(StringUtils.isNotEmpty(sessionData.getContactNumber())) {
                req.setPresenterPhone(sessionData.getContactNumber());
            }
        }
        return otfSessionManager.addSalesDemoSession(req);
    }

    @RequestMapping(value = "/getVSKDemoAttendee", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getVSKDemoAttendee(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException{
            return  otfSessionManager.getVSKDemoAttendee(sessionId);
    }

    @ApiOperation("get a map of key- boardId - and value - a list of absent session response objects - ")
    @RequestMapping(value = "/getPastWeekAbsentSessionsWithBoard", method = RequestMethod.GET)
    @ResponseBody
    public Map<Long, List<AbsentSessionRes>> getPastWeekAbsentSessionsWithBoard(@RequestParam(name = "userId") String userId) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.STUDENT), false);
        if (!CommonCalendarUtils.isValidDateForAbsentFeedback()) {
            new PlatformBasicResponse();
        }
        return otfSessionManager.getPastWeekAbsentSessionsWithBoard(userId);
    }


    @Deprecated
    @RequestMapping(value = "/setAttendeesInSession", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse setAttendeesInSession(@RequestParam(name = "sessionId", required = true) String sessionId) throws VException {
        return otfSessionManager.setAttendeesInSession(sessionId);
    }

    @RequestMapping(value = "/getUserSessionsForDuration", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public OTFCalendarSessionInfoRes getUserSessionsForDuration(
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "query", required = false) String query,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole,
            @RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "batchIds", required = false) Set<String> batchIds
    ) throws Exception {
        if (startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start time or end time is null");
        }
        if (startTime > endTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start time is greater than end time");
        }
        long diff = endTime - startTime;
        if ((diff / DateTimeUtils.MILLIS_PER_DAY) > 7) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "querying for more than week");
        }

        List<OTFCalendarSessionInfo> sessions = otfSessionManager.getSessionsForDuration(String.valueOf(sessionUtils.getCallingUserId()),
                sessionUtils.getCallingUserRole(), query, start, size,
                null, true, startTime, endTime, batchIds);

        OTFCalendarSessionInfoRes res = new OTFCalendarSessionInfoRes(sessions, sessions.size());
        return res;
    }

    @RequestMapping(value = "/getSessionsByBatchIds", method = RequestMethod.POST)
    @ResponseBody
    public List<OTFSession> getSessionsByBatchIds(@RequestBody GetSessionsByBatchIdsReq getSessionsByBatchIdsReq) throws VException {
        if(ArrayUtils.isEmpty(getSessionsByBatchIdsReq.getBatchIds())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"batchids can not be null");
        }
        return otfSessionManager.getSessionsByBatchIds(getSessionsByBatchIdsReq);
    }

    @GetMapping(value = "/cached/sessiontype/{id}")
    @ResponseBody
    public OTFSessionPojoUtils getCachedSessionType(@PathVariable(value = "id") String id) {
        return otfSessionManager.getCachedSessionType(id);
    }

    @RequestMapping(value = "/getSessionByIds", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<OTFSession> getSessionByIds(@RequestBody GetOTFSessionByIdsReq getOTFSessionByIdsReq) {
        logger.info("getOTFSessionByIdsReq: {}",getOTFSessionByIdsReq);
        List<OTFSession> toRet = otfSessionDAO.getOTFSessionByIds(getOTFSessionByIdsReq);
        return toRet;

    }

    @RequestMapping(value = "/getUserLiveSessionsForApp", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojoApp> getUserLiveSessionsForApp(@RequestBody OTFSessionReqApp req) throws VException {
    		req.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId().toString());
    		req.setCallingUserRole(sessionUtils.getCurrentSessionData().getRole());

        return otfSessionManager.getUserLiveSessionsForApp(req);
    }


    @RequestMapping(value = "/getUserUpcomingSessionsForApp", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<OTFSessionPojoApp> getUserUpcomingSessionsForApp(@RequestBody OTFSessionReqApp req) throws VException {
    		req.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId().toString());
    		req.setCallingUserRole(sessionUtils.getCurrentSessionData().getRole());

        return otfSessionManager.getUserUpcomingSessionsForApp(req);
    }

    @RequestMapping(value = "/getAllSessionsForAppSchedulePage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<WeeklySessionDetails> getAllSessionsForAppSchedulePage(@RequestBody OTFSessionReq req) throws VException {

    	if(null == req || ArrayUtils.isEmpty(req.getBatchIds())) {
    		throw new VException(ErrorCode.BAD_REQUEST_ERROR, "BatchIds missing in request");
    	}
    		req.setUserId(sessionUtils.getCurrentSessionData().getUserId().toString());
    		req.setUserRole(sessionUtils.getCurrentSessionData().getRole());

        return otfSessionManager.getAllSessionsForAppSchedulePage(req);
    }

    @RequestMapping(value = "/handleSessionEventsForOTF", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleSessionEventsForOTF(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                          @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SessionEventsOTF eventType = SessionEventsOTF.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            otfSessionTaskManager.handleSessionEventsForOTF(eventType, message);
        }
    }

    @RequestMapping(value = "/handleBatchEventsForOTF", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleBatchEventsForOTF(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                        @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            if (StringUtils.isEmpty(subject)) {
                logger.warn("subject can not be empty for handleBatchEventsForOTF");
                return;
            }
            BatchEventsOTF eventType = BatchEventsOTF.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            otfSessionTaskManager.handleBatchEventsForOTF(eventType, message);
        }
    }

    @RequestMapping(value = "/createTownhallSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public CreateTownhallSessionRes createTownhallSession(@RequestBody CreateTownhallSessionReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return otfSessionManager.createTownhallSession(req);
    }

}
