/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.request.EndOTMSessionReq;
import com.vedantu.onetofew.request.OTFJoinSessionReq;
import com.vedantu.scheduling.async.AsyncTaskName;
import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import com.vedantu.scheduling.dao.serializers.OTFSessionDAO;
import com.vedantu.scheduling.dao.serializers.OTMSessionEngagementDataDAO;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.managers.VedantuWaveSessionManager;
import com.vedantu.scheduling.pojo.OTMSessionConfig;
import com.vedantu.scheduling.request.ChangeServiceProviderReq;
import com.vedantu.scheduling.request.UpdateOTFSessionReq;
import com.vedantu.scheduling.request.UpdateSessionConfigReq;
import com.vedantu.scheduling.request.wavesession.OTMSessionEngagementDataReq;
import com.vedantu.scheduling.request.wavesession.SessionParametersChangeReq;
import com.vedantu.scheduling.request.wavesession.SetRecordingDetailsRequest;
import com.vedantu.scheduling.request.wavesession.UpdateMultiSessionsConfigReq;
import com.vedantu.scheduling.request.wavesession.UpdateSessionAttendeeDataReq;
import com.vedantu.scheduling.request.wavesession.UpdateSessionDataInsightsReq;
import com.vedantu.scheduling.response.ActiveSessionsTAResp;
import com.vedantu.scheduling.response.JoinVedantuWaveSessionRes;
import com.vedantu.scheduling.response.SetRecordingDetailsRes;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/onetofew/vwavesession")
public class VedantuWaveSessionController {

    @Autowired
    private OTFSessionDAO oTFSessionDAO;

    @Autowired
    private VedantuWaveSessionManager vedantuWaveSessionManager;

    @Autowired
    private OTMSessionEngagementDataDAO oTMSessionEngagementDataDAO;

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(VedantuWaveSessionController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @RequestMapping(value = "/joinVedantuWaveSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public JoinVedantuWaveSessionRes joinVedantuWaveSession(@RequestBody OTFJoinSessionReq req)
            throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();

        req.verify();
        return vedantuWaveSessionManager.joinVedantuWaveSession(req,
                sessionData.getUserId(), sessionData.getRole());
    }

    @RequestMapping(value = "/endVedantuWaveSession", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endVedantuWaveSession(@RequestBody EndOTMSessionReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException, Exception {
        req.verify();
        //TODO verify that this is a app to app call
        return vedantuWaveSessionManager.endVedantuWaveSession(req);
    }

    @RequestMapping(value = "/updateSessionConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateSessionConfig(@RequestBody UpdateSessionConfigReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException {
        return vedantuWaveSessionManager.updateSessionConfig(req);
    }

    @RequestMapping(value = "/getSessionConfig", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTMSessionConfig getSessionConfig(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException {
        return vedantuWaveSessionManager.getSessionConfig(sessionId);
    }

    @RequestMapping(value = "/triggerSQSForDataInsights", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse triggerSQSForDataInsights(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException {
        return vedantuWaveSessionManager.triggerSQSForDataInsights(sessionId);
    }

    @RequestMapping(value = "/saveSlidesMetaDataNew/{sessionId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse savePagesMetadata(@RequestBody List<SessionPagesMetadata> pages, @PathVariable("sessionId") String sessionId)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, BadRequestException, ConflictException {
        return vedantuWaveSessionManager.savePagesMetadata(pages, sessionId);
    }

    //TODO add automatic sns subscription to 1:00 am cron
    @RequestMapping(value = "/createSocialVidConferences", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void createSocialVidConferences(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            vedantuWaveSessionManager.createCurrentDaySocialVidConferences();
        }
    }

    @RequestMapping(value = "/launchOTMSessionRecorder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse launchOTMSessionRecorder(@RequestParam(name = "sessionId", required = true) String sessionId, @RequestParam(name = "timeout", required = true) Long timeout)
            throws VException, IOException, AddressException {

        return vedantuWaveSessionManager.launchOTMSessionRecorder(sessionId, timeout);
    }

    @RequestMapping(value = "/vedantuwavesessionscron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void launchOTMSessionRecorders(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
//            vedantuWaveSessionManager.launchOTMSessionRecordersAsync();
//            vedantuWaveSessionManager.checkOTMSessionRecordingsAsync();
//            vedantuWaveSessionManager.endActiveSessionsAsync();
//            vedantuWaveSessionManager.sendLateJoinSMSAsync();
        }
    }

    @RequestMapping(value = "/cacheschedulingcollectionscron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void cacheschedulingcollectionscron(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            int cronTime = 0; //minutes
            if (subscriptionRequest.getTopicArn().toUpperCase().contains("CRON_CHIME_15_MINUTES")) {
                cronTime = 15;
            } else {
                cronTime = 1;
            }
            logger.info("cacheschedulingcollectionscron cronTime: " + cronTime);
            vedantuWaveSessionManager.cacheSchedulingCollectionsAsync(cronTime);
        }
    }

    // For manually triggering caching with cron time 1 minute
    @RequestMapping(value = "/cacheschedulingcollections", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse cacheschedulingcollections() throws Exception {
        logger.info("cacheschedulingcollectionscron cronTime: 1 minute");
        vedantuWaveSessionManager.cacheSchedulingCollectionsAsync(1);
        return new PlatformBasicResponse();
    }

    // called from otmnode
    @RequestMapping(value = "/cachegttattendeecollections", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse cachegttattendeecollectionscron(HttpServletRequest request) throws Exception {

        sessionUtils.isAllowedApp(request);
        vedantuWaveSessionManager.cacheGTTAttendeeCollectionsAsync();
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/setRecordingDetails/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SetRecordingDetailsRes setRecordingDetails(@PathVariable("id") String id, @RequestBody SetRecordingDetailsRequest req)
            throws BadRequestException, NotFoundException, ConflictException{
        req.verify();
        OTFSession session = oTFSessionDAO.getById(id);
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + id;
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        if (req.getRecordingStartTime() != null) {
            session.setRecordingStartTime(req.getRecordingStartTime());
        }
        if (req.getRecordedTill() != null) {
            session.setRecordedTill(req.getRecordedTill());
        }
        oTFSessionDAO.save(session);
        SetRecordingDetailsRes res = new SetRecordingDetailsRes();
        res.setProgressState(session.getProgressState());
        res.setState(session.getState());
        return res;
    }

    @RequestMapping(value = "/updateSessionAttendeeData", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateSessionAttendeeData(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("updateSessionAttendeeData notification received - SNS");
            UpdateSessionAttendeeDataReq updateSessionAttendeeDataReq = gson.fromJson(subscriptionRequest.getMessage(), UpdateSessionAttendeeDataReq.class);
            vedantuWaveSessionManager.updateSessionAttendeeData(updateSessionAttendeeDataReq);
        }
        return new PlatformBasicResponse();
    }

//    @RequestMapping(value = "/updateSessionAttendeeData", method = RequestMethod.POST, produces = "application/json")
//    @ResponseBody
//    public PlatformBasicResponse updateSessionAttendeeData(@RequestBody UpdateSessionAttendeeDataReq req) throws VException {
//        //use app to app verification
//        req.verify();
//
//        logger.info("loam_updateSessionAttendeeData ===== : {}" , req);
//        vedantuWaveSessionManager.updateSessionAttendeeData(req);
//        return new PlatformBasicResponse();
//    }

    @RequestMapping(value = "/updateSessionDataInsights", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse updateSessionAttendeeData(@RequestBody UpdateSessionDataInsightsReq req) throws VException {
        //use app to app verification
        req.verify();

        logger.info("loam_updateSessionDataInsights ===== : {}", req);

        OTFSession session = oTFSessionDAO.getById(req.getSessionId());
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + req.getSessionId();
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        session.setPollCount(req.getPollCount());
        session.setQuizCount(req.getQuizCount());
        session.setHotspotCount(req.getHotspotCount());
        session.setNoticeCount(req.getNoticeCount());
        session.setChatCount(req.getChatCount());
        session.setUniqueStudentAttendants(req.getUniqueStudentAttendants());
        session.setTotalDoubtsAsked(req.getTotalDoubtsAsked());
        session.setTotalDoubtsResolved(req.getTotalDoubtsResolved());
        session.setSessionDuration(req.getSessionDuration());
        if (req.getAverageStudentDuration() != null) {//sometimes can be null as there is a different api to set it
            session.setAverageStudentDuration(req.getAverageStudentDuration());
        }
        oTFSessionDAO.save(session);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/setStudentAvgDuration", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse setStudentAvgDuration(@RequestBody UpdateSessionDataInsightsReq req) throws VException {
        //use app to app verification
        req.verify();
        OTFSession session = oTFSessionDAO.getById(req.getSessionId());
        if (session == null) {
            String errorMessage = "Session does not exist for req : " + req.getSessionId();
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }
        if (req.getAverageStudentDuration() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "averageStudentDuration");
        }
        session.setAverageStudentDuration(req.getAverageStudentDuration());
        oTFSessionDAO.save(session);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/setStudentEngagementData", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse setStudentEngagementData(@RequestBody OTMSessionEngagementDataReq req) throws VException {
        //use app to app verification
        vedantuWaveSessionManager.setStudentEngagementData(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getStudentEngagementData", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTMSessionEngagementData> getStudentEngagementData(@RequestParam(name = "sessionId", required = true) String sessionId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size) throws VException {
        //use app to app verification
        return oTMSessionEngagementDataDAO.getBySessionId(sessionId, start, size);
    }

    @RequestMapping(value = "/getSessionDataInsights", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public OTFSession getSessionDataInsights(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException {
        //use app to app verification
        OTFSession otfSession = oTFSessionDAO.getById(sessionId);
        logger.info("getSessionDataInsights " + otfSession);
        return otfSession;
    }

    @RequestMapping(value = "/getActiveSessionsTAs", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<ActiveSessionsTAResp> getActiveSessionsTAs()
            throws VException {
        //use app to app verification
        return vedantuWaveSessionManager.getActiveSessionsTAs();
    }

    @RequestMapping(value = "/saveOtfSession", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse saveOtfSession(@RequestBody UpdateOTFSessionReq req) throws VException {
        //use app to app verification
        return vedantuWaveSessionManager.saveOtfSession(req);
    }

    @RequestMapping(value = "/saveGttAttendeeDetails", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse saveGttAttendeeDetails(@RequestBody List<GTTAttendeeDetails> req) throws VException {
        //use app to app verification
        return vedantuWaveSessionManager.saveGttAttendeeDetails(req);
    }

    @RequestMapping(value = "/updateGttAttendeeFromNodeCache", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse updateGttAttendeeFromNodeCache(@RequestBody List<GTTAttendeeDetails> req, HttpServletRequest request) throws VException {

        sessionUtils.isAllowedApp(request);

        //use app to app verification
        return vedantuWaveSessionManager.updateGttAttendeeFromNodeCache(req);
    }

    @RequestMapping(value = "/sendAbsenteeSMS", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendAbsenteeSMS(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());

            Map<String, Object> payload1 = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_ABSENTEE_SMS, payload1);
            asyncTaskFactory.executeTask(params);            
        }
    }

    @RequestMapping(value = "/changeServiceProvider", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse changeServiceProvider(@RequestBody ChangeServiceProviderReq request)
            throws VException {
        List<String> sessionIds = request.getSessionIds();
        OTMServiceProvider serviceProvider = request.getServiceProvider();
        logger.info("changeServiceProvider-serviceProvider" + serviceProvider + ", sessionIds: " + sessionIds);
        return vedantuWaveSessionManager.changeServiceProvider(sessionIds, serviceProvider);
    }

    @RequestMapping(value = "/updateMultiSessionsConfig", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse changeServiceProvider(@RequestBody UpdateMultiSessionsConfigReq req, HttpServletRequest request)
            throws VException {
        sessionUtils.isAllowedApp(request);
        List<String> sessionIds = req.getSessionIds();
        UpdateSessionConfigReq otmSessionConfig = req.getOtmSessionConfig();
        logger.info("updateMultiSessionsConfig" + otmSessionConfig + ", sessionIds: " + sessionIds);
        return vedantuWaveSessionManager.updateMultiSessionsConfig(sessionIds, otmSessionConfig);
    }

    @RequestMapping(value = "/changeSessionParameters", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse changeSessionParameters(@RequestBody SessionParametersChangeReq req, HttpServletRequest request)
            throws VException {
        sessionUtils.isAllowedApp(request);
        String sessionId = req.getSessionId();
        Long startTime = req.getStartTime();
        Long endtime = req.getEndTime(); 
        String otmSessionType = req.getOtmSessionType();
        String progressState = req.getProgressState();
        String vimeoId = req.getVimeoId();
        String agoraVimeoId = req.getAgoraVimeoId();
        logger.info("changeSessionParameters - sessionId:" + sessionId + ", endtime: " + endtime + ", startTime: " + startTime + ", otmSessionType: " + otmSessionType + ", progressState: " + progressState + ", vimeoId: " + vimeoId + ", agoraVimeoId: " + agoraVimeoId);
        return vedantuWaveSessionManager.changeSessionParameters(sessionId, startTime, endtime, otmSessionType, progressState, vimeoId, agoraVimeoId);
    }

}
