package com.vedantu.scheduling.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.scheduling.dao.serializers.RedisDAO;
import com.vedantu.scheduling.managers.CronManager;
import com.vedantu.scheduling.managers.OTFSessionManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/cron")
public class CronController {
    
    @Autowired
    private LogFactory logfactory;
    
    @Autowired
    private CronManager cronManager;

    @Autowired
    private RedisDAO redisDAO;

    private Logger logger = logfactory.getLogger(CronController.class);
    
    @RequestMapping(value = "/sendOtmNoShowEmails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendOtmNoShowEmails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            cronManager.sendOtmNoShowEmailsAsync();
        }
    }
    
    @RequestMapping(value = "/sendOtmTrialSessionEmails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendOtmTrialSessionEmails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            cronManager.sendOtmTrialSessionEmailsAsync();
        }
    }

    @RequestMapping(value = "/sendLiveSessionEmails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendLiveSessionEmails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                          @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification sendLiveSessionEmails received - SNS");
            logger.info(subscriptionRequest.toString());
            cronManager.sendLiveSessionEmails();
        }
    }
    
    @RequestMapping(value = "/sendModularRegularSessionEmails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendModularRegularSessionEmails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            cronManager.sendModularRegularSessionEmailsAsync();
        }
    }

    @RequestMapping(value = "/sendEarlyLearningClassReminder", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendEarlyLearningClassReminder(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            cronManager.sendEarlyLearningClassSmsAsync();
        }
    }

    @ApiOperation(value = "performRedisOps", notes = "for other services to perform ops on scheduling redis cluster keys")
    @RequestMapping(value = "/performRedisOps", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void performRedisOps(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                      @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest req = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + req.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(req.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification perform redis operations received - SNS");
            SNSSubject subject = SNSSubject.valueOf(req.getSubject());
            String message = req.getMessage();
            try {
                redisDAO.performOps(subject, message);
            } catch (InternalServerErrorException e) {
                logger.warn("error while performing redis ops for {}", req);
            }
        }
    }
}
