package com.vedantu.scheduling.controllers;

import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.scheduling.enums.earlyLeaning.AnalyticsType;
import com.vedantu.scheduling.managers.earlylearning.DemoManager;
import com.vedantu.scheduling.pojo.EarlyLearningSessionPojo;
import com.vedantu.scheduling.request.AvailabilitySlotStringReq;
import com.vedantu.scheduling.request.earlylearning.AddDemoRequest;
import com.vedantu.scheduling.request.earlylearning.JoinDemoRequest;
import com.vedantu.scheduling.request.earlylearning.ProficiencyAnalyticsRequest;
import com.vedantu.scheduling.request.earlylearning.ReScheduleDemoRequest;
import com.vedantu.scheduling.response.AvailabilitySlotStringRes;
import com.vedantu.scheduling.response.earlylearning.JoinDemoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.util.Map;

@RestController
@RequestMapping("/earlylearning/v2")
public class EarlyLearningDemoController {

	@Autowired
	private DemoManager demoManager;

	@RequestMapping(value = "/has-demo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public EarlyLearningSessionPojo hasEarlyLearningBooking(
			@RequestParam(name = "studentId", required = false) String studentId,
			@RequestParam(name = "earlyLearningCourse", required = true) SessionLabel earlyLearningCourse)
			throws VException {
		return demoManager.hasEarlyLearningDemo(studentId, earlyLearningCourse);
	}

	@RequestMapping(value = "/demo-slots", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public AvailabilitySlotStringRes getDemoAvailabilitySlotStrings(@RequestBody AvailabilitySlotStringReq req)
			throws VException {
		return demoManager.getDemoAvailabilitySlotStrings(req);
	}

	@RequestMapping(value = "/add-demo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public EarlyLearningSessionPojo addEarlyLearningSession(@RequestBody AddDemoRequest request)
			throws VException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
		return demoManager.addDemo(request);
	}

	@RequestMapping(value = "/join-demo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public JoinDemoResponse joinEarlyLearningSession(@RequestBody JoinDemoRequest request)
			throws VException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
		return demoManager.joinDemo(request);
	}

	@RequestMapping(value = "/reSchedule-demo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public EarlyLearningSessionPojo reScheduleDemo(@RequestBody ReScheduleDemoRequest request) throws VException {
		return demoManager.reScheduleDemo(request);
	}

	//For Testing Purpose
	@RequestMapping(value = "/getAvailableTeacher", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, Object> getAvailableTeacher(@RequestBody AvailabilitySlotStringReq request) throws VException {
		return demoManager.getAvailableTeacher(request);
	}

	@Deprecated
	//one time migration script
	@RequestMapping(value = "/migrate-bookings-proficiency", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, Object> migrateBookings(@RequestParam(name = "limit", required = true) Integer limit) throws VException {
		return demoManager.migrateBookings(limit);
	}

	@RequestMapping(value = "/proficiency-analytics", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<AnalyticsType, Object> proficiencyAnalytics(@RequestBody ProficiencyAnalyticsRequest request) throws VException, ParseException {
		return demoManager.proficiencyAnalytics(request);
	}
}
