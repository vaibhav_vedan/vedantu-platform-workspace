package com.vedantu.scheduling.controllers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.scheduling.dao.entity.CalendarEvent;
import com.vedantu.scheduling.managers.CalendarEventManager;
import com.vedantu.scheduling.request.CancelCalendarEventReq;
import com.vedantu.scheduling.request.GetCalendarEventReq;
import com.vedantu.scheduling.pojo.calendar.RetryCalendarEventReq;
import com.vedantu.util.LogFactory;


@RestController
@RequestMapping("/calendarEvent")
public class CalendarEventController {
	@Autowired
	private LogFactory logFactory;

	@Autowired
	private CalendarEventManager calendarEventManager;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEventController.class);

        
	@RequestMapping(value = "/getEvents", method = RequestMethod.GET)
	@ResponseBody
	public List<CalendarEvent> getCalendarEvents(@ModelAttribute GetCalendarEventReq req) throws Exception {
		logger.info("getCalendarEvents request : " + req.toString());
		List<CalendarEvent> results = calendarEventManager.getCalendarEvents(req);
		logger.info("getCalendarEvents response : count " + results == null ? 0 : results.size());
		return results;
	}

	@RequestMapping(value = "/cancelEvents", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<CalendarEvent> cancelCalendarEvents(@ModelAttribute CancelCalendarEventReq req) throws Exception {
		logger.info("cancelCalendarEvents request : " + req.toString());
		List<CalendarEvent> results = calendarEventManager.cancelEvents(req);
		logger.info("cancelCalendarEvents response : count " + results == null ? 0 : results.size());
		return results;
	}

	@RequestMapping(value = "/retryEvents", method = RequestMethod.GET)
	@ResponseBody
	public List<CalendarEvent> retryCalendarEvents(@ModelAttribute RetryCalendarEventReq req) throws Exception {
		logger.info("cancelCalendarEvents request : " + req.toString());
		List<CalendarEvent> results = calendarEventManager.retryEvents(req);
		logger.info("cancelCalendarEvents response : count " + results == null ? 0 : results.size());
		return results;
	}
}
