package com.vedantu.scheduling.dao.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vedantu.User.Role;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Feedback extends AbstractMongoStringIdEntity {
	private String senderId;
	private String receiverId;
	private String sessionId;
	private Role role;
	private int interactionRating;
	private int contentProvidedRating;
	private int technologyRating;
	private int contentCoverageRating;
	private Boolean assignmentProvided;
	private int attendeeCount;
	private String topicString;
	private String subTopics;
	private String feedback;
	private String feedbackReceived;
	private String comments;

	private String teacherRating;
	@JsonProperty
	private List<String> teacherImprovement;
	@JsonProperty
	private List<String> technologyImprovement;
	private Boolean satisfied;

	public Feedback() {
		super();
	}

	public Feedback(String receiverId, String senderId, String sessionId, Role role, String feedback,
			String feedbackReceived) {
		super();
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.sessionId = sessionId;
		this.role = role;
		this.feedback = feedback;
		this.feedbackReceived = feedbackReceived;
	}

	public Feedback(String senderId, String receiverId, String sessionId, Role role, int interactionRating,
			int contentProvidedRating, int technologyRating, int contentCoverageRating, Boolean assignmentProvided,
			int attendeeCount, String topicString, String subTopics, String feedback, String feedbackReceived,
			String comments, String teacherRating, List<String> teacherImprovement, List<String> technologyImprovement,
			Boolean satisfied) {
		super();
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.sessionId = sessionId;
		this.role = role;
		this.interactionRating = interactionRating;
		this.contentProvidedRating = contentProvidedRating;
		this.technologyRating = technologyRating;
		this.contentCoverageRating = contentCoverageRating;
		this.assignmentProvided = assignmentProvided;
		this.attendeeCount = attendeeCount;
		this.topicString = topicString;
		this.subTopics = subTopics;
		this.feedback = feedback;
		this.feedbackReceived = feedbackReceived;
		this.comments = comments;
		this.teacherRating = teacherRating;
		this.teacherImprovement = teacherImprovement;
		this.technologyImprovement = technologyImprovement;
		this.satisfied = satisfied;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getInteractionRating() {
		return interactionRating;
	}

	public void setInteractionRating(int interactionRating) {
		this.interactionRating = interactionRating;
	}

	public int getContentProvidedRating() {
		return contentProvidedRating;
	}

	public void setContentProvidedRating(int contentProvidedRating) {
		this.contentProvidedRating = contentProvidedRating;
	}

	public int getTechnologyRating() {
		return technologyRating;
	}

	public void setTechnologyRating(int technologyRating) {
		this.technologyRating = technologyRating;
	}

	public int getContentCoverageRating() {
		return contentCoverageRating;
	}

	public void setContentCoverageRating(int contentCoverageRating) {
		this.contentCoverageRating = contentCoverageRating;
	}

	public Boolean getAssignmentProvided() {
		return assignmentProvided;
	}

	public void setAssignmentProvided(Boolean assignmentProvided) {
		this.assignmentProvided = assignmentProvided;
	}

	public int getAttendeeCount() {
		return attendeeCount;
	}

	public void setAttendeeCount(int attendeeCount) {
		this.attendeeCount = attendeeCount;
	}

	public String getTopicString() {
		return topicString;
	}

	public void setTopicString(String topicString) {
		this.topicString = topicString;
	}

	public String getSubTopics() {
		return subTopics;
	}

	public void setSubTopics(String subTopics) {
		this.subTopics = subTopics;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getFeedbackReceived() {
		return feedbackReceived;
	}

	public void setFeedbackReceived(String feedbackReceived) {
		this.feedbackReceived = feedbackReceived;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTeacherRating() {
		return teacherRating;
	}

	public void setTeacherRating(String teacherRating) {
		this.teacherRating = teacherRating;
	}

	public List<String> getTeacherImprovement() {
		return teacherImprovement;
	}

	public void setTeacherImprovement(List<String> teacherImprovement) {
		this.teacherImprovement = teacherImprovement;
	}

	public List<String> getTechnologyImprovement() {
		return technologyImprovement;
	}

	public void setTechnologyImprovement(List<String> technologyImprovement) {
		this.technologyImprovement = technologyImprovement;
	}

	public Boolean getSatisfied() {
		return satisfied;
	}

	public void setSatisfied(Boolean satisfied) {
		this.satisfied = satisfied;
	}

	public static class Constants {
		public static final String SENDER_ID = "senderId";
		public static final String RECEIVER_ID = "receiverId";
		public static final String SESSION_ID = "sessionId";
	}
}
