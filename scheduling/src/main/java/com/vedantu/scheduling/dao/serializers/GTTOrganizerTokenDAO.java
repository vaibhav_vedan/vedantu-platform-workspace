/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.dao.serializers;

/**
 *
 * @author pranavm
 */
import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.GTTOrganizerToken;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class GTTOrganizerTokenDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GTTOrganizerTokenDAO.class);

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public GTTOrganizerTokenDAO() {
        super();
    }

    public GTTOrganizerToken getById(String id) {
        GTTOrganizerToken gttAttendeeDetails = null;
        if (!StringUtils.isEmpty(id)) {
            gttAttendeeDetails = (GTTOrganizerToken) getEntityById(id, GTTOrganizerToken.class);
        }
        return gttAttendeeDetails;
    }

    public void update(GTTOrganizerToken p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public List<GTTOrganizerToken> getTokens(List<String> seatValues) {
        Query query = new Query();
//        query.addCriteria(Criteria.where(GTTOrganizerToken.Constants.SEAT_VALUE).is(seatValue));
//        query.with(Sort.by(Sort.Direction.DESC, GTTOrganizerToken.Constants.LAST_UPDATED));

        if(ArrayUtils.isNotEmpty(seatValues)){
            query.addCriteria(Criteria.where(GTTOrganizerToken.Constants.SEAT_VALUE).in(seatValues));
        }
        query.addCriteria(Criteria.where(GTTOrganizerToken.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        List<GTTOrganizerToken> results = runQuery(query, GTTOrganizerToken.class);
        return results;
    }

    public GTTOrganizerToken getTokenForSeat(String seatValue) {
        Query query = new Query();
        logger.info("getting token for seat:"+ seatValue);
        query.addCriteria(Criteria.where(GTTOrganizerToken.Constants.SEAT_VALUE).is(seatValue));
        query.with(Sort.by(Sort.Direction.DESC, GTTOrganizerToken.Constants.LAST_UPDATED));
        List<GTTOrganizerToken> results = runQuery(query, GTTOrganizerToken.class);
        if (ArrayUtils.isNotEmpty(results)) {
            return results.get(0);
        }
        return null;
    }
    
    public List<GTTOrganizerToken> checkExpiryForTokens(Long expiryTime) {
        Query query = new Query();

        query.addCriteria(Criteria.where(GTTOrganizerToken.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(GTTOrganizerToken.Constants.EXPIRES_IN).lte(expiryTime));
        List<GTTOrganizerToken> results = runQuery(query, GTTOrganizerToken.class);
        return results;
    }

}
