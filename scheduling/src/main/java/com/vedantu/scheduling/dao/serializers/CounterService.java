package com.vedantu.scheduling.dao.serializers;

import com.vedantu.util.dbutils.AbstractMongoCounterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class CounterService extends AbstractMongoCounterService {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
}

