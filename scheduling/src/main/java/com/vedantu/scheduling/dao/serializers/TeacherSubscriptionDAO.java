package com.vedantu.scheduling.dao.serializers;

import com.vedantu.scheduling.dao.entity.TeacherSubscription;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class TeacherSubscriptionDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public TeacherSubscriptionDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(TeacherSubscription p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public TeacherSubscription getById(String id) {
        TeacherSubscription TeacherSubscription = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                TeacherSubscription = (TeacherSubscription) getEntityById(id, TeacherSubscription.class);
            }
        } catch (Exception ex) {
            TeacherSubscription = null;
        }
        return TeacherSubscription;
    }

    public void update(Query q, Update u) {
        try {
            updateFirst(q, u, TeacherSubscription.class);
        } catch (Exception ex) {
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, TeacherSubscription.class);
        } catch (Exception ex) {
        }

        return result;
    }
}
