/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.dao.entity;

/**
 *
 * @author ajith
 */
public class PollNumbers {

    private Integer numberAsked;
    private Integer numberYES;
    private Integer numberNO;

    public PollNumbers() {
    }

    public PollNumbers(Integer numberAsked, Integer numberYES, Integer numberNO) {
        this.numberAsked = numberAsked;
        this.numberYES = numberYES;
        this.numberNO = numberNO;
    }

    public Integer getNumberAsked() {
        return numberAsked;
    }

    public void setNumberAsked(Integer numberAsked) {
        this.numberAsked = numberAsked;
    }

    public Integer getNumberYES() {
        return numberYES;
    }

    public void setNumberYES(Integer numberYES) {
        this.numberYES = numberYES;
    }

    public Integer getNumberNO() {
        return numberNO;
    }

    public void setNumberNO(Integer numberNO) {
        this.numberNO = numberNO;
    }

    @Override
    public String toString() {
        return "PollNumber{" + "numberAsked=" + numberAsked + ", numberYES=" + numberYES + ", numberNO=" + numberNO + '}';
    }

}
