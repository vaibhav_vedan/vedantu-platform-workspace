package com.vedantu.scheduling.dao.entity;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.BitSetEntry;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarUpdateDetails;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.scheduling.CommonCalendarUtils;

public class CalendarEntry extends AbstractMongoStringIdEntity {

	private String userId;
	private Long dayStartTime;
	private Set<Integer> availability;
	private Set<Integer> booked;
	private Set<Integer> sessionRequest;
	private List<CalendarUpdateDetails> updateHistory = new ArrayList<>();

	public CalendarEntry() {
		super();
	}

	public CalendarEntry(String userId, Long dayStartTime, Set<Integer> availability, Set<Integer> booked,
			Set<Integer> sessionRequest) {
		super();
		this.userId = userId;
		this.dayStartTime = dayStartTime;
		this.availability = (availability == null) ? new HashSet<Integer>() : availability;
		this.booked = (booked == null) ? new HashSet<Integer>() : booked;
		this.sessionRequest = (sessionRequest == null) ? new HashSet<Integer>() : sessionRequest;
	}

	public CalendarEntry(String userId, Long dayStartTime) {
		this(userId, dayStartTime, null, null, null);
	}

	public boolean addSlotEntries(CalendarEntrySlotState slotState, int startIndex, int endIndex) {
		return addSlotEntries(getListBySlotState(slotState), startIndex, endIndex);
	}

	private boolean addSlotEntries(Set<Integer> entries, int startIndex, int endIndex) {
		if (entries == null) {
			entries = new HashSet<>();
		}

		startIndex = Math.max(startIndex, CommonCalendarUtils.getCalendarEntryMinIndex());
		endIndex = Math.min(endIndex, CommonCalendarUtils.getCalendarEntryMaxIndex());
		return entries.addAll(IntStream.rangeClosed(startIndex, endIndex).boxed().collect(Collectors.toSet()));
	}

	public boolean removeSlotEntries(CalendarEntrySlotState slotState, int startIndex, int endIndex) {
		return removeSlotEntries(getListBySlotState(slotState), startIndex, endIndex);
	}

	private boolean removeSlotEntries(Set<Integer> entries, int startIndex, int endIndex) {
		if (entries != null) {
			return entries.removeAll(IntStream.rangeClosed(startIndex, endIndex).boxed().collect(Collectors.toSet()));
		} else {
			return false;
		}
	}

	public String getUserId() {
		return userId;
	}

	public Long getDayStartTime() {
		return dayStartTime;
	}

	public Set<Integer> getAvailability() {
		return availability;
	}

	public void setAvailability(Set<Integer> availability) {
		this.availability = availability;
	}

	public Set<Integer> getBooked() {
		return booked;
	}

	public Set<Integer> getSessionRequest() {
		return sessionRequest;
	}

	public void setSessionRequest(Set<Integer> sessionRequest) {
		this.sessionRequest = sessionRequest;
	}

	public List<CalendarUpdateDetails> getUpdateHistory() {
		return updateHistory;
	}

	public void setUpdateHistory(List<CalendarUpdateDetails> updateHistory) {
		this.updateHistory = updateHistory;
	}

	public void setBooked(Set<Integer> booked) {
		this.booked = booked;
	}

	private static BitSet fetchBitset(Set<Integer> list) {
		BitSet bitSet = new BitSet(CalendarEntryManager.SLOT_COUNT);
		for (Integer i = 0; i < CalendarEntryManager.SLOT_COUNT; i++) {
			if (list.contains(i)) {
				bitSet.set(i);
			}
		}

		return bitSet;
	}

	public BitSet fetchAvailabilityBitSet() {
		return fetchBitset(availability);
	}

	public BitSet fetchBookedBitSet() {
		return fetchBitset(booked);
	}

	public BitSet fetchSessionRequestBitSet() {
		return fetchBitset(sessionRequest);
	}

	public void updateByBitSet(BitSetEntry entry) {
		CalendarEntrySlotState slotState = entry.getSlotState();
		assert (slotState != null);

		switch (slotState) {
		case AVAILABLE:
			availability = getList(entry, availability);
			break;
		case SESSION:
			booked = getList(entry, booked);
			break;
		case SESSION_REQUEST:
			sessionRequest = getList(entry, sessionRequest);
			break;
		default:
			break;
		}
	}

	public void mergeSessionByBitSet(BitSetEntry entry) {
		CalendarEntrySlotState slotState = entry.getSlotState();
		assert (slotState != null);
		this.booked = mergeList(entry, this.booked);
	}

	private Set<Integer> mergeList(BitSetEntry bs, Set<Integer> list) {
		int count = CalendarEntryManager.SLOT_COUNT;
		int index = bs.getBitSetIndex(dayStartTime);
		Set<Integer> results = new HashSet<Integer>();

		for (Integer i = index, tempCount = 0; tempCount < count; i++, tempCount++) {
			if (i < 0) {
				// Retain the existing entries
				if (list.contains(tempCount)) {
					results.add(tempCount);
				}
			} else if (bs.isBitSet(i) || list.contains(tempCount)) {
				// Update entries from Bit Set
				results.add(tempCount);
			}
		}

		return results;
	}

	private Set<Integer> getList(BitSetEntry bs, Set<Integer> list) {
		int count = CalendarEntryManager.SLOT_COUNT;
		int index = bs.getBitSetIndex(dayStartTime);
		Set<Integer> results = new HashSet<Integer>();

		for (Integer i = index, tempCount = 0; tempCount < count; i++, tempCount++) {
			if (i < 0) {
				// Retain the existing entries
				if (list.contains(tempCount)) {
					results.add(tempCount);
				}
			} else if (bs.isBitSet(i)) {
				// Update entries from Bit Set
				results.add(tempCount);
			}
		}

		return results;
	}

	private Set<Integer> getListBySlotState(CalendarEntrySlotState slotState) {
		switch (slotState) {
		case AVAILABLE:
			return this.availability;
		case SESSION:
			return this.booked;
		case SESSION_REQUEST:
			return this.sessionRequest;
		default :
			return null;
		}
	}

	public void addUpdateHistoryEntry(CalendarUpdateDetails calendarUpdateDetails) {
		switch (calendarUpdateDetails.getSlotState()) {
		case SESSION:
			this.updateHistory.add(calendarUpdateDetails);
			break;
		default:
			break;
		}
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String DAY_START_TIME = "dayStartTime";
		public static final String AVAILABILITY = "availability";
		public static final String BOOKED = "booked";
		public static final String SESSION_REQUEST = "sessionRequest";
		public static final String UPDATE_HISTORY = "updateHistory";

	}

	@Override
	public String toString() {
		return "CalendarEntry [userId=" + userId + ", dayStartTime=" + dayStartTime + ", availability=" + availability
				+ ", booked=" + booked + ", sessionRequest=" + sessionRequest + ", updateHistory=" + updateHistory
				+ ", toString()=" + super.toString() + "]";
	}
}
