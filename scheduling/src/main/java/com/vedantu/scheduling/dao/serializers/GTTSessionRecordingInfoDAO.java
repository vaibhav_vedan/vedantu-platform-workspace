package com.vedantu.scheduling.dao.serializers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.GTTSessionRecordingInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class GTTSessionRecordingInfoDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public GTTSessionRecordingInfoDAO() {
		super();
	}

	public void create(GTTSessionRecordingInfo p) {
		if (p != null) {
			update(p);
		}
	}

	public GTTSessionRecordingInfo getById(String id) {
		GTTSessionRecordingInfo gttAttendeeDetails = null;
		if (!StringUtils.isEmpty(id)) {
			gttAttendeeDetails = (GTTSessionRecordingInfo) getEntityById(id, GTTSessionRecordingInfo.class);
		}
		return gttAttendeeDetails;
	}

	public void update(GTTSessionRecordingInfo p) {
		if (p != null) {
			saveEntity(p);
		}
	}

	public GTTSessionRecordingInfo getGTTSessionRecordingInfo(String sessionId, String trainingId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(GTTSessionRecordingInfo.Constants.SESSION_ID).is(sessionId));
		query.addCriteria(Criteria.where(GTTSessionRecordingInfo.Constants.TRAINING_ID).is(trainingId));
		List<GTTSessionRecordingInfo> results = runQuery(query, GTTSessionRecordingInfo.class);

		if (!CollectionUtils.isEmpty(results)) {
			return results.get(0);
		}
		return null;
	}
}
