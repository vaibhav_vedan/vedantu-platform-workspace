package com.vedantu.scheduling.dao.serializers;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.Feedback;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class FeedbackDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public FeedbackDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
        
	public Feedback create(Feedback p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			return null;
		}

		return p;
	}

	public Feedback getById(String id) {
		Feedback feedback = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				feedback = (Feedback) getEntityById(id, Feedback.class);
			}
		} catch (Exception ex) {
			// log Exception
			feedback = null;
		}
		return feedback;
	}

	public void save(Feedback p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			// throw Exception;
		}
	}

	public void updateAll(List<Feedback> p) {
		try {
			if (p != null) {
				insertAllEntities(p, Feedback.class.getSimpleName());
			}
		} catch (Exception ex) {
			// throw Exception;
		}
	}
}
