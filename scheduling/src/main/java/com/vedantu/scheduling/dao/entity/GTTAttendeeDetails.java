package com.vedantu.scheduling.dao.entity;

import com.vedantu.User.LocationInfo;
import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.scheduling.enums.DeleteContext;
import com.vedantu.scheduling.enums.ELSessionStatus;
import com.vedantu.scheduling.managers.GTTAttendeeDetailsManager;
import com.vedantu.scheduling.pojo.*;
import com.vedantu.scheduling.pojo.wavesession.DoubtData;
import com.vedantu.scheduling.pojo.wavesession.InteractionData;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.TimeInterval;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.pojo.DeviceDetails;
import lombok.*;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.*;

@Document(collection = "GTTAttendeeDetails")
@CompoundIndexes({
        @CompoundIndex(name = "sessionId_1_entityState_1__id_1", def = "{'sessionId':1, 'entityState': 1, '_id':1}", background = true),
        @CompoundIndex(name = "sessionId_1_userId_1", def = "{'sessionId':1, 'userId':1}", background = true),
        @CompoundIndex(name = "sessionStartTime_1", def = "{'sessionStartTime': 1}", background = true)
})

public class GTTAttendeeDetails extends AbstractMongoStringIdEntity {
    private String userId;
    private String organizerAccessToken;
    private String organizerKey;
    private String traningId;
    private String sessionId;
    private String registrantKey;
    private String joinUrl;
    private ELSessionStatus sessionSuccessful;
    private List<Long> joinTimes = new ArrayList<>();
    private long timeInSession = 0L;
    private List<TimeInterval> activeIntervals = new ArrayList<>();
    private AttendeeContext attendeeType;
    private DeleteContext deletedContext;
    private String enrollmentId;
    private List<PollQuestionAnswer> pollAnswered;
    private List<ReplayWatchedDuration> replayWatchedDurations = new ArrayList<>();
    private long totalReplayWatchedDuration;
    private long avgReplayWatchedDuration;
    private boolean noshowEmailSent = false;
    private LocationInfo locationInfo;
    private DeviceDetails deviceDetails;
    private Long sessionStartTime;
    private Long sessionEndTime;

    //from post session data insights, info will be for Student, Teacher, TA , some of the fields won't apply to all
    private Long studentSessionJoinTime;
    private Long studentLastDisconnectTime;
    private Integer totalDisconnections;
    private Integer studentChatCount;
    private Integer thumbsUpCount;
    private Integer thumbsDownCount;
    private Float averageResponseTime;
    private List<Long> connectTimes;//connectiontimes
    private List<Long> disconnectTimes;//diconnectiontimes
    private List<InteractionData> interactionDatas;
    private QuizNumbers quizNumbers;
    private HotspotNumbers hotspotNumbers;
    private List<DoubtData> doubtDatas;
    private DoubtNumbers doubtNumbers;
    private PollNumbers pollNumbers;
    private Integer lbRank;
    private Float lbPercentile;
    private Integer lbPoints;
    private Integer studentOnlineDuration;
    private boolean isdeletedWithDeenrolled;
    private List<AttendanceFeedback> feedbackTexts;

    // used to keep track of multiple enrollments for the same userId and sessionId
    private LinkedHashSet<EnrollmentInfo> enrollmentInfos = new LinkedHashSet<>();


    // userId_sessionId_radomIndex field when gtt is created
    private String randomUniqueIndex = "ACTIVE";

    @Indexed(unique = true, sparse = true, background = true)
    private String sparseSessionUserId;


    @Data
    @Builder
    @Setter(AccessLevel.PRIVATE)
    @EqualsAndHashCode(onlyExplicitlyIncluded = true)
    public static class EnrollmentInfo {
        @EqualsAndHashCode.Include
        private String refId;
        private EntityStatus status;

        private EnrollmentInfo() {

        }

        private EnrollmentInfo(String refId, EntityStatus status) {
            this.refId = Objects.requireNonNull(refId);
            this.status = Objects.requireNonNull(status);
        }
    }

    public Set<EnrollmentInfo> getEnrollmentInfos() {
        return Collections.unmodifiableSet(enrollmentInfos);
    }

    private void setEnrollmentInfos(LinkedHashSet<EnrollmentInfo> enrollmentInfos) {
        this.enrollmentInfos = enrollmentInfos;
    }

    public void addEnrollmentInfo(EnrollmentPojo enrollment) {
        final EnrollmentInfo info = EnrollmentInfo.builder()
                .refId(enrollment.getId())
                .status(enrollment.getStatus())
                .build();
        if (this.enrollmentInfos == null) {
            setEnrollmentInfos(new LinkedHashSet<>());
        }
        // remove old entry
        this.enrollmentInfos.remove(info);
        // add updated entry
        this.enrollmentInfos.add(info);

        if (info != null && this.enrollmentId == null) {
            this.enrollmentId = info.refId;
        }
    }

    public void addEnrollmentInfos(List<EnrollmentPojo> enrollments) {
        Optional.ofNullable(enrollments)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(e -> StringUtils.hasText(e.getId()))
                .forEach(this::addEnrollmentInfo);
    }

    public String getRandomUniqueIndex() {
        return randomUniqueIndex;
    }

    public void setRandomUniqueIndex(String randomUniqueIndex) {
        this.randomUniqueIndex = randomUniqueIndex;
    }

    public String getSparseSessionUserId() {
        return sparseSessionUserId;
    }

    public void setSparseSessionUserId(String sparseSessionUserId) {
        this.sparseSessionUserId = sparseSessionUserId;
    }

    public String createSparseSessionUserId() {
        Objects.requireNonNull(this.sessionId);
        Objects.requireNonNull(this.userId);
        return this.sessionId + "." + this.userId;
    }

    public boolean getIsIsdeletedWithDeenrolled() {
        return isdeletedWithDeenrolled;
    }

    public void setIsdeletedWithDeenrolled(boolean isdeletedWithDeenrolled) {
        this.isdeletedWithDeenrolled = isdeletedWithDeenrolled;
    }

    public GTTAttendeeDetails() {
        super();
    }

    public GTTAttendeeDetails(String sessionId, String userId, List<EnrollmentPojo> enrollmentPojos) {
        super();
        this.userId = userId;
        this.traningId = sessionId;//using the sessionId as the traning Id
        this.sessionId = sessionId;
        this.addEnrollmentInfos(enrollmentPojos);
    }

    public GTTAttendeeDetails(GTTGetRegistrantDetailsRes registrantDetailsRes, String organizerAccessToken,
            String organizerKey, OTFSession oTFSession)
    {
        super();
        this.userId = GTTAttendeeDetailsManager.extractUserId(registrantDetailsRes.getEmail());
        this.organizerAccessToken = organizerAccessToken;
        this.organizerKey = organizerKey;
        this.traningId = oTFSession.getMeetingId();
        this.sessionId = oTFSession.getId();
        this.registrantKey = registrantDetailsRes.getRegistrantKey();
        this.joinUrl = registrantDetailsRes.getJoinUrl();
    }

    public GTTAttendeeDetails(GTTRegisterAttendeeRes gttRegisterAttendeeRes, String organizerAccessToken,
            String organizerKey, OTFSession oTFSession, String userId)
    {
        super();
        this.userId = userId;
        this.organizerAccessToken = organizerAccessToken;
        this.organizerKey = organizerKey;
        this.traningId = oTFSession.getMeetingId();
        this.sessionId = oTFSession.getId();
        this.registrantKey = gttRegisterAttendeeRes.getRegistrantKey();
        this.joinUrl = gttRegisterAttendeeRes.getJoinUrl();
    }

    public GTTAttendeeDetailsInfo gttAttendeeDetailsInfo() {
        GTTAttendeeDetailsInfo info = new GTTAttendeeDetailsInfo();
        BeanUtils.copyProperties(this, info);
        return info;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public String getOrganizerKey() {
        return organizerKey;
    }

    public void setOrganizerKey(String organizerKey) {
        this.organizerKey = organizerKey;
    }

    public String getTraningId() {
        return traningId;
    }

    public void setTraningId(String traningId) {
        this.traningId = traningId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getJoinUrl() {
        return joinUrl;
    }

    public void setJoinUrl(String joinUrl) {
        this.joinUrl = joinUrl;
    }

    public ELSessionStatus getSessionSuccessful() {
        return sessionSuccessful;
    }

    public void setSessionSuccessful(ELSessionStatus sessionSuccessful) {
        this.sessionSuccessful = sessionSuccessful;
    }

    public List<Long> getJoinTimes() {
        return joinTimes;
    }

    public void setJoinTimes(List<Long> joinTimes) {
        this.joinTimes = joinTimes;
    }

    public String getRegistrantKey() {
        return registrantKey;
    }

    public void setRegistrantKey(String registrantKey) {
        this.registrantKey = registrantKey;
    }

    public Long getTimeInSession() {
        return timeInSession;
    }

    public void setTimeInSession(Long timeInSession) {
        this.timeInSession = timeInSession;
    }

    public List<TimeInterval> getActiveIntervals() {
        return activeIntervals;
    }

    public void setActiveIntervals(List<TimeInterval> activeIntervals) {
        this.activeIntervals = activeIntervals;
    }

    public AttendeeContext getAttendeeType() {
        return attendeeType;
    }

    public void setAttendeeType(AttendeeContext attendeeType) {
        this.attendeeType = attendeeType;
    }

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public void resetFeedbackData() {
        this.activeIntervals = new ArrayList<>();
        this.timeInSession = 0;
    }

    public void addJoinTime() {
        if (this.joinTimes == null) {
            this.joinTimes = new ArrayList<>();
        }

        this.joinTimes.add(System.currentTimeMillis());
    }

    public void addTimeInterval(long startTime, long endTime) {
        if (this.activeIntervals == null) {
            this.activeIntervals = new ArrayList<>();
        }

        this.activeIntervals.add(new TimeInterval(startTime, endTime));
    }

    public void addTimeInsession(Long time) {
        if (time != null) {
            this.timeInSession += time;
        }
    }

    public List<PollQuestionAnswer> getPollAnswered() {
        return pollAnswered;
    }

    public void setPollAnswered(List<PollQuestionAnswer> pollAnswered) {
        this.pollAnswered = pollAnswered;
    }

    public void updateSessionAttendeeInfo(GTTSessionAttendeeInfo gttSessionAttendeeInfo) throws ParseException {
        addTimeInsession(gttSessionAttendeeInfo.getTimeInSession());
        List<GTTSessionAttendeeSlot> slots = gttSessionAttendeeInfo.getInSessionTimes();
        if (!CollectionUtils.isEmpty(slots)) {
            for (GTTSessionAttendeeSlot slot : slots) {
                if (!StringUtils.isEmpty(slot.getJoinTime()) && !StringUtils.isEmpty(slot.getLeaveTime())) {
                    addTimeInterval(GTTAttendeeDetailsManager.parseTimeString(slot.getJoinTime()),
                            GTTAttendeeDetailsManager.parseTimeString(slot.getLeaveTime()));
                }
            }
        }
    }

    public List<ReplayWatchedDuration> getReplayWatchedDurations() {
        return replayWatchedDurations;
    }

    public void setReplayWatchedDurations(List<ReplayWatchedDuration> replayWatchedDurations) {
        this.replayWatchedDurations = replayWatchedDurations;
    }

    public long getTotalReplayWatchedDuration() {
        return totalReplayWatchedDuration;
    }

    public void setTotalReplayWatchedDuration(long totalReplayWatchedDuration) {
        this.totalReplayWatchedDuration = totalReplayWatchedDuration;
    }

    public long getAvgReplayWatchedDuration() {
        return avgReplayWatchedDuration;
    }

    public void setAvgReplayWatchedDuration(long avgReplayWatchedDuration) {
        this.avgReplayWatchedDuration = avgReplayWatchedDuration;
    }

    public boolean isNoshowEmailSent() {
        return noshowEmailSent;
    }

    public void setNoshowEmailSent(boolean noshowEmailSent) {
        this.noshowEmailSent = noshowEmailSent;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    public DeviceDetails getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(DeviceDetails deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    /**
     * @return the sessionStartTime
     */
    public Long getSessionStartTime() {
        return sessionStartTime;
    }

    /**
     * @param sessionStartTime the sessionStartTime to set
     */
    public void setSessionStartTime(Long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    /**
     * @return the sessionEndTime
     */
    public Long getSessionEndTime() {
        return sessionEndTime;
    }

    /**
     * @param sessionEndTime the sessionEndTime to set
     */
    public void setSessionEndTime(Long sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public Float getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(Float averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public List<Long> getConnectTimes() {
        return connectTimes;
    }

    public void setConnectTimes(List<Long> connectTimes) {
        this.connectTimes = connectTimes;
    }

    public List<Long> getDisconnectTimes() {
        return disconnectTimes;
    }

    public void setDisconnectTimes(List<Long> disconnectTimes) {
        this.disconnectTimes = disconnectTimes;
    }

    public List<InteractionData> getInteractionDatas() {
        return interactionDatas;
    }

    public void setInteractionDatas(List<InteractionData> interactionDatas) {
        this.interactionDatas = interactionDatas;
    }

    public List<DoubtData> getDoubtDatas() {
        return doubtDatas;
    }

    public void setDoubtDatas(List<DoubtData> doubtDatas) {
        this.doubtDatas = doubtDatas;
    }

    public Long getStudentSessionJoinTime() {
        return studentSessionJoinTime;
    }

    public void setStudentSessionJoinTime(Long studentSessionJoinTime) {
        this.studentSessionJoinTime = studentSessionJoinTime;
    }

    public Long getStudentLastDisconnectTime() {
        return studentLastDisconnectTime;
    }

    public void setStudentLastDisconnectTime(Long studentLastDisconnectTime) {
        this.studentLastDisconnectTime = studentLastDisconnectTime;
    }

    public Integer getStudentChatCount() {
        return studentChatCount;
    }

    public void setStudentChatCount(Integer studentChatCount) {
        this.studentChatCount = studentChatCount;
    }

    public Integer getThumbsUpCount() {
        return thumbsUpCount;
    }

    public void setThumbsUpCount(Integer thumbsUpCount) {
        this.thumbsUpCount = thumbsUpCount;
    }

    public Integer getThumbsDownCount() {
        return thumbsDownCount;
    }

    public void setThumbsDownCount(Integer thumbsDownCount) {
        this.thumbsDownCount = thumbsDownCount;
    }

    public Integer getTotalDisconnections() {
        return totalDisconnections;
    }

    public void setTotalDisconnections(Integer totalDisconnections) {
        this.totalDisconnections = totalDisconnections;
    }

    public Integer getLbRank() {
        return lbRank;
    }

    public void setLbRank(Integer lbRank) {
        this.lbRank = lbRank;
    }

    public Float getLbPercentile() {
        return lbPercentile;
    }

    public void setLbPercentile(Float lbPercentile) {
        this.lbPercentile = lbPercentile;
    }

    public Integer getLbPoints() {
        return lbPoints;
    }

    public void setLbPoints(Integer lbPoints) {
        this.lbPoints = lbPoints;
    }

    public Integer getStudentOnlineDuration() {
        return studentOnlineDuration;
    }

    public void setStudentOnlineDuration(Integer studentOnlineDuration) {
        this.studentOnlineDuration = studentOnlineDuration;
    }

    public QuizNumbers getQuizNumbers() {
        return quizNumbers;
    }

    public void setQuizNumbers(QuizNumbers quizNumbers) {
        this.quizNumbers = quizNumbers;
    }

    public HotspotNumbers getHotspotNumbers() {
        return hotspotNumbers;
    }

    public void setHotspotNumbers(HotspotNumbers hotspotNumbers) {
        this.hotspotNumbers = hotspotNumbers;
    }

    public DoubtNumbers getDoubtNumbers() {
        return doubtNumbers;
    }

    public void setDoubtNumbers(DoubtNumbers doubtNumbers) {
        this.doubtNumbers = doubtNumbers;
    }

    public PollNumbers getPollNumbers() {
        return pollNumbers;
    }

    public void setPollNumbers(PollNumbers pollNumbers) {
        this.pollNumbers = pollNumbers;
    }

    public List<AttendanceFeedback> getFeedbackTexts() {
        return feedbackTexts;
    }

    public void setFeedbackTexts(List<AttendanceFeedback> feedbackTexts) {
        this.feedbackTexts = feedbackTexts;
    }

    public DeleteContext getDeletedContext() {
        return deletedContext;
    }

    public void setDeletedContext(DeleteContext deletedContext) {
        this.deletedContext = deletedContext;
    }

    public void updateSessionAttendeeInfo(GTWSessionAttendanceInfo gtwSessionAttendeeInfo) throws ParseException {
        addTimeInsession(gtwSessionAttendeeInfo.getAttendanceTimeInSeconds());
        List<GTTSessionAttendeeSlot> slots = gtwSessionAttendeeInfo.getAttendance();
        if (!CollectionUtils.isEmpty(slots)) {
            for (GTTSessionAttendeeSlot slot : slots) {
                if (!StringUtils.isEmpty(slot.getJoinTime()) && !StringUtils.isEmpty(slot.getLeaveTime())) {
                    addTimeInterval(GTTAttendeeDetailsManager.parseTimeString(slot.getJoinTime()),
                            GTTAttendeeDetailsManager.parseTimeString(slot.getLeaveTime()));
                }
            }
        }
    }


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String ORGANIZER_ACCESS_TOKEN = "organizerAccessToken";
        public static final String ORGANIZER_KEY = "organizerKey";
        public static final String TRAINING_ID = "traningId";
        public static final String REGISTRATION_KEY = "registrantKey";
        public static final String SESSION_ID = "sessionId";
        public static final String TIME_IN_SESSION = "timeInSession";
        public static final String JOIN_TIMES = "joinTimes";
        public static final String CONNECT_TIMES = "connectTimes";
        public static final String ENROLLMENT_ID = "enrollmentId";
        public static final String ATTENDEE_TYPE = "attendeeType";
        public static final String POLL_ANSWERED = "pollAnswered";
        public static final String NO_SHOW_EMAIL_SENT = "noshowEmailSent";
        public static final String REPLAY_WATCHED_DURATIONS = "replayWatchedDurations";
        public static final String TOTAL_REPLAY_WATCHED_DURATION = "totalReplayWatchedDuration";
        public static final String AVG_REPLAY_WATCHED_DURATION = "avgReplayWatchedDuration";
        public static final String OTF_SESSION_END_TIME = "sessionEndTime";
        public static final String OTF_SESSION_START_TIME = "sessionStartTime";
        public static final String STUDENT_SESSION_JOIN_TIME = "studentSessionJoinTime";
        public static final String JOIN_TIME_FIRST = "joinTimes.0";
        public static final String HOTSPOT_NUMBERS = "hotspotNumbers";
        public static final String QUIZ_NUMBERS = "quizNumbers";
        public static final String LOCATION_INFO = "locationInfo";
        public static final String DEVICE_DETAILS = "deviceDetails";
        public static final String IsdeletedWithDeenrolled = "isdeletedWithDeenrolled";
        public static final String FEEDBACK_TEXTS = "feedbackTexts";
        public static final String ENROLLMENT_INFOS = "enrollmentInfos";
        public static final String ENROLLMENT_INFOS_STATUS = "enrollmentInfos.status";
        public static final String RANDOM_UNIQUE_INDEX = "randomUniqueIndex";
        public static final String SPARSE_SESSION_USER_ID = "sparseSessionUserId";
        public static final String DELETED_CONTEXT = "deletedContext";
        public static final String ACTIVE_INTERVALS = "activeIntervals";
    }

}
