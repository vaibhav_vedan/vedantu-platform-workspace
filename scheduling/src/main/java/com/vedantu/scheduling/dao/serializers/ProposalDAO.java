package com.vedantu.scheduling.dao.serializers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.User.User;
import com.vedantu.scheduling.dao.entity.Proposal;
import com.vedantu.scheduling.request.proposal.GetProposalsReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.ProposalStatus;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;

@Service
public class ProposalDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(ProposalDAO.class);    
    
    @Autowired
    private CounterService counterService;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public ProposalDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void insert(Proposal proposal, Long callingUserId) {
        // Create session
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        proposal.setScheduledBy(proposal.getFromUserId());
        if (proposal.getId() == null || proposal.getId() <= 0l) {
            proposal.setId(counterService.getNextSequence(Proposal.class.getSimpleName()));
        }
        saveEntity(proposal, callingUserIdString);
    }

    public void updateProposal(Proposal p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
    }

    public List<Proposal> fetchProposals(GetProposalsReq getProposalsReq) {
        Query query = new Query();

        if (getProposalsReq.getUserId() != null) {
            query.addCriteria(new Criteria().orOperator(
                    Criteria.where(Proposal.Constants.FROM_USERID).is(getProposalsReq.getUserId()),
                    Criteria.where(Proposal.Constants.TO_USERID).is(getProposalsReq.getUserId())));
        }
        
        if (!CollectionUtils.isEmpty(getProposalsReq.getStatusList())) {
            query.addCriteria(Criteria.where(Proposal.Constants.PROPOSAL_STATUS).in(getProposalsReq.getStatusList()));
        }        

        if (getProposalsReq.getSubscriptionId() != null && getProposalsReq.getSubscriptionId() > 0l) {
            query.addCriteria(
                    Criteria.where(Proposal.Constants.SUBSCRIPTION_ID).is(getProposalsReq.getSubscriptionId()));
        }

        if ((getProposalsReq.getRegisteredFromTime() != null && getProposalsReq.getRegisteredFromTime() > 0)) {
            if (getProposalsReq.getRegisteredTillTime() != null && getProposalsReq.getRegisteredTillTime() > 0) {
                query.addCriteria(Criteria.where(User.Constants.CREATION_TIME)
                        .gte(getProposalsReq.getRegisteredFromTime()).lt(getProposalsReq.getRegisteredTillTime()));
            } else {
                query.addCriteria(
                        Criteria.where(User.Constants.CREATION_TIME).gte(getProposalsReq.getRegisteredFromTime()));
            }
        } else if (getProposalsReq.getRegisteredTillTime() != null && getProposalsReq.getRegisteredTillTime() > 0) {
            query.addCriteria(Criteria.where(User.Constants.CREATION_TIME).lt(getProposalsReq.getRegisteredTillTime()));
        }

        query.with(Sort.by(Sort.Direction.DESC, Proposal.Constants.LAST_UPDATED));
        setFetchParameters(query, getProposalsReq);
        logger.info("query "+query);
        return runQuery(query, Proposal.class);
    }

    public Proposal getProposalById(Long proposalId) {
        Proposal proposal = null;
        try {
            // TODO : Need to debug why getEntityById is not working.
            Query query = new Query();
            query.addCriteria(Criteria.where(Proposal.Constants.ID).is(proposalId));
            List<Proposal> results = runQuery(query, Proposal.class);
            proposal = results.get(0);
        } catch (Exception ex) {
            throw new RuntimeException("getById : Error fetch the proposal with id " + proposalId, ex);
        }
        return proposal;
    }

    public void cancelSubscriptionProposals(Long subscriptionId) {
        Query query = new Query();
        List<ProposalStatus> states = new ArrayList<>();
        states.add(ProposalStatus.PENDING);
        states.add(ProposalStatus.UNSENT);
        query.addCriteria(Criteria.where(Proposal.Constants.SUBSCRIPTION_ID).is(subscriptionId));
        query.addCriteria(Criteria.where(Proposal.Constants.PROPOSAL_STATUS).in(states));

        Update update = new Update();
        update.set(Proposal.Constants.PROPOSAL_STATUS, ProposalStatus.CANCELED);
        logger.info("query "+query);
        logger.info("update query  "+update);
        updateMulti(query, update, Proposal.class);
    }

}
