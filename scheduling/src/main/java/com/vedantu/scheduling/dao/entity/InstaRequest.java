package com.vedantu.scheduling.dao.entity;

import com.vedantu.scheduling.pojo.instalearn.InstaPayload;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "InstaRequest")
public class InstaRequest extends AbstractMongoStringIdEntity {
        @Indexed(background=true)
	private Long to;
        @Indexed(background=true)
	private Long from;
	private InstaPayload data;

	public InstaRequest() {
		super();
	}

	public InstaRequest(Long to, Long from, InstaPayload data) {

		super();
		this.to = to;
		this.from = from;
		this.data = data;
	}
        
	public Long getTo() {
		return to;
	}

	public void setTo(Long to) {
		this.to = to;
	}

	public Long getFrom() {
		return from;
	}

	public void setFrom(Long from) {
		this.from = from;
	}

	public InstaPayload getData() {
		return data;
	}

	public void setData(InstaPayload data) {
		this.data = data;
	}

	public boolean checkIfCurrentRequest() {
		Long curTime = System.currentTimeMillis();
		if (curTime - this.getCreationTime() <= 5 * 3600 * 1000){
                    return true;
                }
		return false;
	}


	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String TO = "to";
		public static final String FROM = "from";
		public static final String DATA = "data";
	}
}
