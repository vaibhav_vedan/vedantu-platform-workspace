package com.vedantu.scheduling.dao.serializers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.vedantu.User.Role;
import com.vedantu.scheduling.dao.entity.CalendarEvent;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.scheduling.pojo.TotalSessionDuration;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.bson.Document;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

@Service
public class SessionAttendeeDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionAttendeeDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public SessionAttendeeDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void update(SessionAttendee sessionAttendee) {
		try {
			Assert.notNull(sessionAttendee);
			saveEntity(sessionAttendee);
		} catch (Exception ex) {
			throw new RuntimeException("update : Error updating the SessionAttendee " + sessionAttendee.toString(), ex);
		}
	}

	public void insertAll(Collection<SessionAttendee> sessionAttendees) {
		try {
			insertAllEntities(sessionAttendees, SessionAttendee.class.getSimpleName());
		} catch (Exception ex) {
			throw new RuntimeException(
					"update : Error updating the session attendees" + Arrays.toString(sessionAttendees.toArray()), ex);
		}
	}

	public SessionAttendee getById(String id) {
		SessionAttendee sessionAttendee = null;
		try {
			Assert.hasText(id);
			sessionAttendee = (SessionAttendee) getEntityById(id, SessionAttendee.class);
		} catch (Exception ex) {
			throw new RuntimeException("getById : Error fetch the SessionAttendee with id " + id, ex);
		}
		return sessionAttendee;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			Assert.hasText(id);
			result = deleteEntityById(id, CalendarEvent.class);
		} catch (Exception ex) {
			throw new RuntimeException("deleteById : Error deleting the session attendee");
		}

		return result;
	}

	public List<SessionAttendee> getAttendees(Long sessionId) {
		Assert.notNull(sessionId, "Session id is null");
		List<SessionAttendee> results = new ArrayList<>();
		Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).is(sessionId));
		results = runQuery(query, SessionAttendee.class);
		return results;
	}

	public List<SessionAttendee> getAttendees(List<Long> sessionIds) {
		Assert.notEmpty(sessionIds, "Session ids is empty");
		List<SessionAttendee> results = new ArrayList<>();
		Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).in(sessionIds));
		results = runQuery(query, SessionAttendee.class);
		return results;
	}

        public List<SessionAttendee> getAttendees(List<Long> sessionIds, int start, int size) {
		Assert.notEmpty(sessionIds, "Session ids is empty");
		List<SessionAttendee> results = new ArrayList<>();
		Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).in(sessionIds));
                setFetchParameters(query, start, size);
		results = runQuery(query, SessionAttendee.class);
		return results;
	}        

	public List<SessionAttendee> getAttendees(Long sessionId, Long userId) {
		Assert.notNull(sessionId, "Session id is null");
		Assert.notNull(userId, "User id is null");
		List<SessionAttendee> results = new ArrayList<>();
		Query query = new Query();
		query.addCriteria(Criteria.where(SessionAttendee.Constants.SESSION_ID).is(sessionId));
		query.addCriteria(Criteria.where(SessionAttendee.Constants.USER_ID).is(userId));
		results = runQuery(query, SessionAttendee.class);
		return results;
	}

	public long getEndedSessionDurationBySessionIds(Set<Long> sessionIds) {
		Criteria criteria = new Criteria().andOperator(
				Criteria.where(SessionAttendee.Constants.SESSION_ID).in(sessionIds),
				Criteria.where(SessionAttendee.Constants.USER_ROLE).is(Role.TEACHER));

                AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria),
				Aggregation.project(SessionAttendee.Constants.USER_ROLE, SessionAttendee.Constants.BILLING_DURATION),
				Aggregation.group(SessionAttendee.Constants.USER_ROLE).sum(SessionAttendee.Constants.BILLING_DURATION)
						.as("duration"),
				Aggregation.project("duration").and("_id").as(SessionAttendee.Constants.USER_ROLE)).withOptions(aggregationOptions);

		AggregationResults<TotalSessionDuration> results = getMongoOperations().aggregate(aggregation,
				SessionAttendee.class, TotalSessionDuration.class);
		logger.info("aggregation query of getEndedSessionDurationBySessionIds --> "+aggregation);
		logger.info("results is -->"+results);

		if (results != null && !CollectionUtils.isEmpty(results.getMappedResults())) {
			return results.getMappedResults().get(0).getDuration();
		} else {
			return 0;
		}
	}

}
