package com.vedantu.scheduling.dao.serializers;

import com.vedantu.scheduling.utils.CalendarUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class CalendarEntryDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEntryDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public CalendarEntryDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public static final long SLOT_LENGTH = 15 * CommonCalendarUtils.MILLIS_PER_MINUTE;
    public static final int SLOT_COUNT = (int) (CommonCalendarUtils.MILLIS_PER_DAY / SLOT_LENGTH);

	public void save(CalendarEntry calendarEntry) {
		try {
			Assert.notNull(calendarEntry);
			saveEntity(calendarEntry);
		} catch (Exception ex) {
			throw new RuntimeException(
					"CalendarEntryUpdateError : Error updating the calendarEntry " + calendarEntry.toString(), ex);
		}
	}

	// public void upsert(CalendarEntry calendarEntry) {
	// try {
	// Assert.hasText(calendarEntry.getUserId());
	// Assert.notNull(calendarEntry.getDayStartTime());
	// Query query = new Query();
	// query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).is(calendarEntry.getUserId()));
	// query.addCriteria(
	// Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(calendarEntry.getDayStartTime()));
	//
	// Update save = new Update();
	// save.set(CalendarEntry.Constants.AVAILABILITY,
	// calendarEntry.getAvailabilty());
	// save.set(CalendarEntry.Constants.BOOKED, calendarEntry.getBooked());
	// save.set(CalendarEntry.Constants.SESSION_REQUEST,
	// calendarEntry.getSessionRequest());
	//
	// String id = upsertEntity(query, save, CalendarEntry.class);
	// if (!StringUtils.isEmpty(id)) {
	// calendarEntry.setId(id);
	// }
	// } catch (Exception ex) {
	// throw new RuntimeException(
	// "CalendarEntryUpdateError : Error updating the calendarEntry " +
	// calendarEntry.toString(), ex);
	// }
	// }

	public CalendarEntry getById(String id) {
		CalendarEntry CalendarEntry = null;
		try {
			Assert.hasText(id);
			CalendarEntry = (CalendarEntry) getEntityById(id, CalendarEntry.class);
		} catch (Exception ex) {
			throw new RuntimeException("CalendarEntryUpdateError : Error fetch the calendarEntry with id " + id, ex);
		}
		return CalendarEntry;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			Assert.hasText(id);
			result = deleteEntityById(id, CalendarEntry.class);
		} catch (Exception ex) {
		}

		return result;
	}

	public List<CalendarEntry> getTeachersAvailabilityInSlot(Long dayStartTime, Long dayEndTime, List<String> teacherIds) {


		Query query = new Query();
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).gte(dayStartTime).lt(dayEndTime));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(teacherIds));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.AVAILABILITY).in(CalendarUtils.createArrayList(0, SLOT_COUNT)));

		return runQuery(query, CalendarEntry.class);
	}

	public List<CalendarEntry> getTeachersAvailabilityInOneHourSlot(long dayStartTime,int slotStartIndex,int slotEndIndex, List<String> teacherIds) {

		Query query = new Query();

		query.addCriteria(Criteria.where(CalendarEntry.Constants.USER_ID).in(teacherIds));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.DAY_START_TIME).is(dayStartTime));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.AVAILABILITY).all(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));
		query.addCriteria(Criteria.where(CalendarEntry.Constants.BOOKED).nin(CalendarUtils.createArrayList(slotStartIndex, slotEndIndex)));
		logger.info("query is " + query.toString());
		logger.info("day start time for teacher availability is " + dayStartTime);
		query.fields().include(CalendarEntry.Constants.USER_ID);
		query.fields().include(CalendarEntry.Constants.BOOKED);
		query.fields().include(CalendarEntry.Constants.AVAILABILITY);
		return runQuery(query, CalendarEntry.class);
	}
}
