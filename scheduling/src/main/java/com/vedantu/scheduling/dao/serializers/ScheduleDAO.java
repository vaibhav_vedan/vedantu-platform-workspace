package com.vedantu.scheduling.dao.serializers;


import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.Schedule;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class ScheduleDAO extends AbstractMongoDAO {
        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public ScheduleDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void create(Schedule p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public Schedule getById(String id) {
		Schedule schedule = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				schedule = (Schedule) getEntityById(id, Schedule.class);
			}
		} catch (Exception ex) {
			// log Exception
			schedule = null;
		}
		return schedule;
	}

	public void save(Schedule p) throws Exception {
		if (p != null) {
			saveEntity(p);
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Schedule.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
