package com.vedantu.scheduling.dao.entity;

import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.subscription.enums.ContentPriceType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by somil on 10/05/17.
 */
@Document(collection = "Webinar")
public class Webinar extends AbstractMongoStringIdEntity {
    private String presenter;
    private Long endTime;
    private String sessionURL;
    private String meetingId;
    private List<String> replayUrls;
    private boolean isRescheduled;
    //private SessionState state;
    private String organizerAccessToken;
    private String presenterUrl;
    //private String remarks;
    private Boolean teacherJoined;
    private Boolean postSessionDataProcessed;
    private OTFSessionToolType sessionToolType; // If null, Consider session
    // tool type of the batch


    private String title;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;
    private Long startTime;


    public Boolean getTeacherJoined() {
        return teacherJoined;
    }

    public void setTeacherJoined(Boolean teacherJoined) {
        this.teacherJoined = teacherJoined;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPresenter() {
        return presenter;
    }

    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getSessionURL() {
        return sessionURL;
    }

    public void setSessionURL(String sessionURL) {
        this.sessionURL = sessionURL;
        if (StringUtils.isEmpty(sessionURL)) {
            this.meetingId = null;
        }
    }

    public List<String> getReplayUrls() {
        return replayUrls;
    }

    public void setReplayUrls(List<String> replayUrls) {
        this.replayUrls = replayUrls;
    }

    public boolean isRescheduled() {
        return isRescheduled;
    }

    public void setRescheduled(boolean isRescheduled) {
        this.isRescheduled = isRescheduled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public String getPresenterUrl() {
        return presenterUrl;
    }

    public void setPresenterUrl(String presenterUrl) {
        this.presenterUrl = presenterUrl;
    }

    public OTFSessionToolType getSessionToolType() {
        return sessionToolType;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        this.sessionToolType = sessionToolType;
    }

    public Boolean getPostSessionDataProcessed() {
        return postSessionDataProcessed;
    }

    public void setPostSessionDataProcessed(Boolean postSessionDataProcessed) {
        this.postSessionDataProcessed = postSessionDataProcessed;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String BATCH_ID = "batchId";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String PRESENTER = "presenter";
        public static final String ATTENDEES = "attendees";
        public static final String STATE = "state";
        public static final String TYPE = "type";
        public static final String SESSION_TOOL_TYPE = "sessionToolType";
        public static final String ORGANIZER_ACCESS_TOKEN = "organizerAccessToken";
    }

}

