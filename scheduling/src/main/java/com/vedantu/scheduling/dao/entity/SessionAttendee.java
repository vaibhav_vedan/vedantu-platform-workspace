package com.vedantu.scheduling.dao.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.session.pojo.SessionAttendee.SessionUserState;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Document(collection = "SessionAttendee")
public class SessionAttendee extends AbstractMongoStringIdEntity {

	@Indexed
	private Long sessionId;
	private Long userId;
	private Long startTime;
	private Long endTime;
	private Role role;
	private Integer billingPeriod;
	private Integer chargedAmount;
	private Integer paidAmount;
	private SessionUserState userState = SessionUserState.NOT_JOINED;
	private Long joinTime;

	public SessionAttendee() {
		super();
	}

	public SessionAttendee(Session session, Long userId, Role role) {
		super();
		this.sessionId = session.getId();
		this.userId = userId;
		this.startTime = session.getStartTime();
		this.endTime = session.getEndTime();
		this.role = role;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getBillingPeriod() {
		return billingPeriod == null ? 0 : billingPeriod;
	}

	public Integer getSavedBillingPeriod() {
		return billingPeriod;
	}

	public void setBillingPeriod(Integer billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	public Integer getChargedAmount() {
		return chargedAmount == null ? 0: chargedAmount;
	}

	public void setChargedAmount(Integer chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	public Integer getPaidAmount() {
		return paidAmount == null ? 0 : paidAmount;
	}

	public void setPaidAmount(Integer paidAmount) {
		this.paidAmount = paidAmount;
	}

	public SessionUserState getUserState() {
		return userState;
	}

	public void setUserState(SessionUserState userState) {
		this.userState = userState;
	}

	public Long getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(Long joinTime) {
		this.joinTime = joinTime;
	}

	public static List<SessionAttendee> createSessionAttendees(Session session) throws VException {
		List<SessionAttendee> sessionAttendees = new ArrayList<>();
		if (session != null) {

			// Create teacher attendee
			sessionAttendees.add(new SessionAttendee(session, session.getTeacherId(), Role.TEACHER));

			// Create student attendees
			for (Long studentId : session.getStudentIds()) {
				sessionAttendees.add(new SessionAttendee(session, studentId, Role.STUDENT));
			}
		}
		return sessionAttendees;
	}

	public static List<SessionAttendee> createSessionAttendees(Collection<Session> sessions) throws VException {
		List<SessionAttendee> sessionAttendees = new ArrayList<>();
		if (!CollectionUtils.isEmpty(sessions)) {
			for (Session session : sessions) {
				// Create teacher attendee
				sessionAttendees.add(new SessionAttendee(session, session.getTeacherId(), Role.TEACHER));

				// Create student attendees
				for (Long studentId : session.getStudentIds()) {
					sessionAttendees.add(new SessionAttendee(session, studentId, Role.STUDENT));
				}
			}
		}
		return sessionAttendees;
	}

	public UserSessionInfo createUserSessionInfo() {
		UserSessionInfo userSessionInfo = new UserSessionInfo();
		userSessionInfo.setUserId(this.userId);
		userSessionInfo.setRole(this.role);
		userSessionInfo.setBillingPeriod(getSavedBillingPeriod());
		userSessionInfo.setChargedAmount(getChargedAmount());
		userSessionInfo.setPaidAmount(getPaidAmount());
		userSessionInfo.setUserState(this.userState);
		userSessionInfo.setJoinTime(this.joinTime);
		return userSessionInfo;
	}

	@Override
	public String toString() {
		return "SessionAttendee [sessionId=" + sessionId + ", userId=" + userId + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", role=" + role + ", billingPeriod=" + billingPeriod + ", chargedAmount="
				+ chargedAmount + ", paidAmount=" + paidAmount + ", userState=" + userState + ", joinTime=" + joinTime
				+ "]";
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String SESSION_ID = "sessionId";
		public static final String USER_ID = "userId";
		public static final String USER_ROLE = "role";
		public static final String BILLING_DURATION = "billingPeriod";
		public static final String START_TIME = "startTime";
		public static final String JOIN_TIME = "joinTime";
		public static final String USER_STATE = "userState";

	}
}
