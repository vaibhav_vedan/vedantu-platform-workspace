package com.vedantu.scheduling.dao.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author mano
 */
@Setter
@Getter
@Document
@CompoundIndexes({
        @CompoundIndex(name = "userId_1_sessionId_1", unique = true, background = true, def = "{'userId': 1, 'sessionId': 1}")
})
public class SessionActivity extends AbstractMongoStringIdEntity {

    @Indexed
    private String sessionId;
    @Indexed
    private Long userId;
    private List<Download> downloads;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String SESSION_ID = "sessionId";
        public static final String USER_ID = "userId";
    }

    @Getter
    @Setter
    public static class Download {
        private String url;
        private Long downloadedAt;
    }
}
