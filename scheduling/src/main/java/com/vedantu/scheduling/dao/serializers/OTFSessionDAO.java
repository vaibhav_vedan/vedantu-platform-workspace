package com.vedantu.scheduling.dao.serializers;

import com.google.common.collect.Lists;
import com.mongodb.client.AggregateIterable;
import com.vedantu.User.Role;
import com.vedantu.app.enums.TimeFrame;
import com.vedantu.app.requests.OTFSessionReq;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.pojo.BatchFirstRegularSessionPojo;
import com.vedantu.onetofew.pojo.LastUpdatedSessionResults;
import com.vedantu.onetofew.request.GetOTFSessionsForRecordingDeletionReq;
import com.vedantu.onetofew.request.GetOTFSessionsListReq;
import com.vedantu.onetofew.request.GetOTFSessionsReq;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.managers.OTFSessionManager;
import com.vedantu.scheduling.pojo.SessionReportAggregation;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.request.GetSessionsByBatchIdsReq;
import com.vedantu.scheduling.request.OTFSessionReqApp;
import com.vedantu.scheduling.request.UpdateSessionConfigReq;
import com.vedantu.scheduling.request.session.GetOTFSessionByIdsReq;
import com.vedantu.scheduling.response.session.CounselorDashboardSessionsInfoRes;
import com.vedantu.util.*;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.vedantu.scheduling.dao.entity.OTFSession.Constants.START_TIME;
import static com.vedantu.scheduling.dao.entity.OTFSession.Constants.STATE;
import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

@Service
public class OTFSessionDAO extends AbstractMongoDAO {

    private final Logger logger = LogFactory.getLogger(OTFSessionDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment");

    private static final int MAX_COUNT = 500;

    public OTFSessionDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(OTFSession p, String callingUserId) throws ConflictException{
        if (p != null) {
            if (SessionState.SCHEDULED.equals(p.getState())){
                p.setRandomUniqueIndex(SessionState.SCHEDULED.name());
            } else {
                p.setRandomUniqueIndex(UUID.randomUUID().toString());
            }

            if (p.getEndTime() - p.getStartTime() > 5 * CommonCalendarUtils.MILLIS_PER_HOUR) {
                logger.error("saving a session with duration > 5 hours");
            }

            List<String> errors = p.validateDeprecated();
            if (ArrayUtils.isNotEmpty(errors)) {
                throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, String.join(",", errors));
            }

            if (p.getEntityTags() != null && p.getEntityTags().contains(EntityTag.SIMULATED_LIVE)) {
                if (p.getType() == EntityType.ONE_TO_ONE) {
                    logger.error("IGNORE ERROR: {}",
                            ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.IGNORABLE_ERROR, "SIM LIVE SESSION - type is oto")));
                    p.setType(EntityType.STANDARD);
                }
            }

            if (p.isWebinarSession() && p.getSessionToolType() == OTFSessionToolType.VEDANTU_WAVE) {
                if (p.getType() == null) {
                    logger.error("IGNORE ERROR: {}",
                            ExceptionUtils.getStackTrace(new VRuntimeException(ErrorCode.IGNORABLE_ERROR, "WAVE WEBINAR SESSION - type is null")));
                    p.setType(EntityType.STANDARD);
                }
            }


            try {
                saveEntity(p, callingUserId);
            } catch (org.springframework.dao.DuplicateKeyException | com.mongodb.DuplicateKeyException e){
                logger.warn("Session with presenter : " + p.getPresenter() + " and startTime " + p.getStartTime() +
                        " already exists with the state " + p.getState().name());
                throw new ConflictException(ErrorCode.OTF_SESSION_ALREADY_EXISTS, "Session with presenter : " + p.getPresenter() +
                        " and startTime " + p.getStartTime() + " already exists with the state " + p.getState().name());
            }
        }
    }

    public void save(OTFSession p) throws ConflictException {
        save(p, null);
    }

    /** no usages found */
    @Deprecated
    public void updateAll(List<OTFSession> p) {
        if (p != null && !p.isEmpty()) {
            Set<String> errors = p.stream()
                    .map(OTFSession::validateDeprecated)
                    .flatMap(List::stream)
                    .collect(Collectors.toSet());
            if (ArrayUtils.isNotEmpty(errors)) {
                throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, String.join(",", errors));
            }
            insertAllEntities(p, OTFSession.class.getSimpleName());
        }
    }

    public OTFSession getById(String id) {
        OTFSession otfSession = null;
        if (!StringUtils.isEmpty(id)) {
            otfSession = getEntityById(id, OTFSession.class);
        }

        return otfSession;
    }

    public List<OTFSession> getOTFSessionById (String id, List<String> includeFields) {
        Query query = new Query(Criteria.where(OTFSession.Constants._ID).is(id));
        return runQuery(query, OTFSession.class, includeFields);
    }

    public OTFSession getByIdWithExclude(String id, List<String> excludeFields) {
        Query query = new Query(Criteria.where(OTFSession.Constants._ID).is(id));
        Field fields = query.fields();
        excludeFields.forEach(fields::exclude);
        List<OTFSession> sessions = Optional.ofNullable(runQuery(query, OTFSession.class)).orElseGet(ArrayList::new);
        return sessions.isEmpty() ? null : sessions.get(0);
    }

    public List<OTFSession> getOTFSessionByIds(GetOTFSessionByIdsReq getOTFSessionByIdsReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants._ID).in(getOTFSessionByIdsReq.getSessionIds()));
        query.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.END_TIME));
        setFetchParameters(query, getOTFSessionByIdsReq.getStart(), getOTFSessionByIdsReq.getSize());
        logger.info("query: {}", query);
        return runQuery(query, OTFSession.class, getOTFSessionByIdsReq.getIncludeSet());
    }

    public List<OTFSession> getByIdsWithProjection(Set<String> ids, List<String> includeFields, int start, int size) {
        Query q = new Query();
        q.addCriteria(Criteria.where(OTFSession.Constants._ID).in(ids));
        setFetchParameters(q, start, size);
        return runQuery(q, OTFSession.class, includeFields);
    }

    //For no fromTime and thruTime send -1,-1 as fromTime and thruTime
    public List<OTFSession> getByBoardId(Long id,Long fromTime,Long thruTime) {
        List<OTFSession> oTFSessions = new ArrayList<>();
        if (!StringUtils.isEmpty(id)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(id));
            if(fromTime>-1 && thruTime>-1){
                query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
            }
            query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
            List<OTFSession> results = runQuery(query, OTFSession.class);
            if (!CollectionUtils.isEmpty(results)) {
                oTFSessions.addAll(results);
            }
        }

        return oTFSessions;
    }

    public List<OTFSession> getByBoardIdInSessions(Long id,List<String> sids) {
        List<OTFSession> oTFSessions = new ArrayList<>();
        if (!StringUtils.isEmpty(id)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(id));
            query.addCriteria(Criteria.where(OTFSession.Constants._ID).in(sids));
            query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
            List<OTFSession> results = runQuery(query, OTFSession.class);
            if (!CollectionUtils.isEmpty(results)) {
                oTFSessions.addAll(results);
            }
        }

        return oTFSessions;
    }


    //TODO start and size is missing
    public List<OTFSession> getAllSessions(Long fromTime,Long thruTime) {
        List<OTFSession> oTFSessions = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
        query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
        List<OTFSession> results = runQuery(query, OTFSession.class);
        if (!CollectionUtils.isEmpty(results)) {
            oTFSessions.addAll(results);
        }

        return oTFSessions;
    }

    public List<OTFSession> getByIds(List<String> ids) {
        List<OTFSession> oTFSessions = new ArrayList<>();
        if (!CollectionUtils.isEmpty(ids)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(OTFSession.Constants.ID).in(ids));
            query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
            List<OTFSession> results = runQuery(query, OTFSession.class);
            if (!CollectionUtils.isEmpty(results)) {
                oTFSessions.addAll(results);
            }
        }

        return oTFSessions;
    }
    public List<OTFSession> getBy_Ids(List<String> ids) {
        List<OTFSession> oTFSessions = new ArrayList<>();
        if (!CollectionUtils.isEmpty(ids)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(OTFSession.Constants._ID).in(ids));
            query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
            List<OTFSession> results = runQuery(query, OTFSession.class);
            if (!CollectionUtils.isEmpty(results)) {
                oTFSessions.addAll(results);
            }
        }

        return oTFSessions;
    }

    public List<OTFSession> getOTFSessionsForBatches(List<String> batchIds, Integer start, Integer size) {
        return getOTFSessionsForBatches(batchIds, start, size, null);
    }

    public List<OTFSession> getOTFSessionsForBatches(List<String> batchIds, Integer start, Integer size, List<String> includeFields) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        if (start != null && size != null) {
            setFetchParameters(query, start, size);
        }
        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        return runQuery(query, OTFSession.class, includeFields);
    }

    //// Get next OTFSession of shared assignments with session subtype for the given subject(boardId) and batch(contextId)
    public List<OTFSession> getNextSessionTimeForBatchIdAndBoardId(List<Long> boardIds, String batchId, Long startTime, int size) {

    	Set<String> batchIds = new HashSet<>();
    	batchIds.add(batchId);
    	Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).in(boardIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gt(startTime));
        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED.toString()));
        query.fields().include(OTFSession.Constants.START_TIME);
        query.limit(size);
        logger.info("Query for next Session Time : {} ",query.toString());
        return runQuery(query, OTFSession.class);
    }

    // Only fetches past sessions

    public List<OTFSession> getPastSessions(GetOTFSessionsReq sessionsReq) {

        // Set the end time as current time if end time is missing
        if (sessionsReq.getEndTime() == null || sessionsReq.getEndTime() > System.currentTimeMillis()) {
            sessionsReq.setEndTime(System.currentTimeMillis());
        }

        Query sessionQuery = new Query();
        if (sessionsReq.getStartTime() != null && sessionsReq.getStartTime() > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(sessionsReq.getStartTime())
                    .lte(sessionsReq.getEndTime()));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(sessionsReq.getEndTime()));
        }

        if (!CollectionUtils.isEmpty(sessionsReq.getBatchIds())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(sessionsReq.getBatchIds()));
        } else {
            if (sessionsReq.getStudentId() != null && sessionsReq.getStudentId() > 0L) {
                // If student id is provided fetch the batch ids from enrollment. Ignoring the
                // batch enrollment check in case of single batch id.;
            }
        }

        if (!StringUtils.isEmpty(sessionsReq.getQuery())) {
            int length = sessionsReq.getQuery().length();
            int index = Math.min(length, 30);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(sessionsReq.getQuery().substring(0, index), "i"));
        }

        if (sessionsReq.getTeacherId() != null && sessionsReq.getTeacherId() > 0L) {
            sessionQuery.addCriteria(
                    Criteria.where(OTFSession.Constants.PRESENTER).is(String.valueOf(sessionsReq.getTeacherId())));
        }

        if (sessionsReq.getSessionContextType() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE)
                    .is(sessionsReq.getSessionContextType()));
        }

        if (sessionsReq.getTeacherJoined() != null) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TEACHER_JOINED)
                    .is(sessionsReq.getTeacherJoined()));
        }

        if (!StringUtils.isEmpty(sessionsReq.getQuery())) {
            int length = sessionsReq.getQuery().length();
            int index = Math.min(length, 30);
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.TITLE).regex(sessionsReq.getQuery().substring(0, index), "i"));
        }

        if (!StringUtils.isEmpty(sessionsReq.getSessionContextId())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_ID)
                    .is(sessionsReq.getSessionContextId()));
        }

        if (ArrayUtils.isNotEmpty(sessionsReq.getLabels())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).in(sessionsReq.getLabels()));
        }

        // TODO : Need to modify this criteria when we introduce ended state
        if (ArrayUtils.isNotEmpty(sessionsReq.getState())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(sessionsReq.getState()));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        }

        if (ArrayUtils.isNotEmpty(sessionsReq.getFlags())) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.FLAGS).in(sessionsReq.getFlags()));
        }

        sessionQuery.with(Sort.by(Direction.DESC, OTFSession.Constants.END_TIME));
        setFetchParameters(sessionQuery, sessionsReq);
        return runQuery(sessionQuery, OTFSession.class);
    }

    public List<OTFSession> getLatestSessionForBatchIds(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(System.currentTimeMillis()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
        setFetchParameters(query, 0, 1);
        return runQuery(query, OTFSession.class);
    }

    public OTFSession getLatestUpcomingSessionForBatchIds(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(System.currentTimeMillis()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
        return findOne(query, OTFSession.class);
    }

    public List<OTFSession> getSessionForBatchIdBetweenTime(String batchId, Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime).lte(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getTrialSessionBetweenTime(Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime).lte(endTime));
        List<String> type = new ArrayList<>();
        type.add(OTMSessionType.TRIAL.name());
        type.add(OTMSessionType.EXTRA_TRIAL.name());
        query.addCriteria(Criteria.where(OTFSession.Constants.OTMSESSION_TYPE).in(type));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.fields().include(OTFSession.Constants._ID);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getRegularSessionForBatchesBetweenTime(List<String> batchIds, Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime).lte(endTime));
        List<String> type = new ArrayList<>();
        type.add(OTMSessionType.REGULAR.name());
        type.add(OTMSessionType.EXTRA_REGULAR.name());
        query.addCriteria(Criteria.where(OTFSession.Constants.OTMSESSION_TYPE).in(type));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        return runQuery(query, OTFSession.class);
    }

    // Not used anywhere
    @Deprecated
    public List<OTFSession> getSessionsBetweenTime(Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(endTime).gte(startTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getSessionsBetweenTimeForSMS(Long startTime1, Long startTime2) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(startTime2).gte(startTime1));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.fields().include(OTFSession.Constants.START_TIME);
        query.fields().include(OTFSession.Constants.END_TIME);
        query.fields().include(OTFSession.Constants.BATCH_IDS);
        query.fields().include(OTFSession.Constants.TITLE);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getSessionsForRecordingDeletion(GetOTFSessionsForRecordingDeletionReq req) {
        // Construct query
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getStartTime()).lte(req.getEndTime()));
        query.addCriteria(Criteria.where(OTFSession.Constants.REPLAY_URLS + ".0").exists(true));
        query.addCriteria(
                Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).in(OTFSessionManager.SUPPORTED_SESSION_TOOL_TYPES));

        // Note : Do not filter by scheduled state. recording deletion should happen for
        // canceled sessions as well
        query.fields().include(OTFSession.Constants.ID);
        query.fields().include(OTFSession.Constants.REPLAY_URLS);
        query.fields().include(OTFSession.Constants.ORGANIZER_ACCESS_TOKEN);
        query.fields().include(OTFSession.Constants.END_TIME); // Required to make sure recording is not deleted before        
        // processing
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));

        // Set the limits
        setFetchParameters(query, req);
        return runQuery(query, OTFSession.class);
    }

    public LastUpdatedSessionResults getLastUpdatedTeacherIds(long startTime, long endTime) {
//        Query query = new Query();
//        query.addCriteria(Criteria.where(OTFSession.Constants.LAST_UPDATED).gte(startTime).lte(endTime));
//        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
//        query.fields().include(OTFSession.Constants.PRESENTER);

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where(OTFSession.Constants.LAST_UPDATED).gte(startTime).lte(endTime).and(OTFSession.Constants.STATE).ne(SessionState.DELETED.name())),
                Aggregation.project(OTFSession.Constants.PRESENTER, OTFSession.Constants.BATCH_IDS),
                Aggregation.group().addToSet(OTFSession.Constants.PRESENTER).as("teacherIds").addToSet(OTFSession.Constants.BATCH_IDS).as("batchIds")).withOptions(aggregationOptions);

        AggregationResults<LastUpdatedSessionResults> results = getMongoOperations().aggregate(aggregation, OTFSession.class,
                LastUpdatedSessionResults.class);
        return results.getUniqueMappedResult();
    }
    ////////

    public List<BatchFirstRegularSessionPojo> getFirstRegularSessionForBatch(List<String> batchIds) {

        Criteria cr = new Criteria();
        Criteria sessionTypeCr = Criteria.where(OTFSession.Constants.OTMSESSION_TYPE).is(OTMSessionType.REGULAR.name());
        Criteria stateCr = Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED.name());

        Criteria batchIdCr = Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds);
        cr.andOperator(sessionTypeCr, batchIdCr, stateCr);
        MatchOperation match = Aggregation.match(cr);
        /*"$batchIds"*/
        GroupOperation group = Aggregation.group(OTFSession.Constants.BATCH_IDS).first(OTFSession.Constants.BATCH_IDS).as("batchId")
                .first(OTFSession.Constants.END_TIME).as("endTime");
        SortOperation sortBy = Aggregation.sort(Direction.ASC, OTFSession.Constants.END_TIME);
        Aggregation.unwind(OTFSession.Constants.BATCH_IDS);

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(match, Aggregation.unwind(OTFSession.Constants.BATCH_IDS),
                sortBy, group).withOptions(aggregationOptions);

        AggregationResults<BatchFirstRegularSessionPojo> result = getMongoOperations()
                .aggregate(aggregation, OTFSession.class.getSimpleName(), BatchFirstRegularSessionPojo.class);
        List<BatchFirstRegularSessionPojo> results = result.getMappedResults();
        return results;
    }

    public List<CounselorDashboardSessionsInfoRes> getDashboardSessionInfosByBatchIds(List<String> batchIds) {
        logger.info("ENTRY: " + batchIds);
        Criteria sessionCriteria = new Criteria();
        sessionCriteria.andOperator(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds),
                Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED.name()));
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(
                Aggregation.match(sessionCriteria),
                Aggregation.unwind(OTFSession.Constants.BATCH_IDS),
                Aggregation.match(sessionCriteria),
                Aggregation.sort(Sort.Direction.ASC, OTFSession.Constants.START_TIME),
                Aggregation.group(OTFSession.Constants.BATCH_IDS)
                        .first(OTFSession.Constants.START_TIME).as(CounselorDashboardSessionsInfoRes.Constants.FIRST_SESSION_START_TIME)
                        .last(OTFSession.Constants.END_TIME).as(CounselorDashboardSessionsInfoRes.Constants.LAST_SESSION_END_TIME)
                        .count().as(CounselorDashboardSessionsInfoRes.Constants.TOTAL_NO_OF_SESSIONS)
        ).withOptions(aggregationOptions);
        logger.info("Agrregation :" + agg);
        AggregationResults<CounselorDashboardSessionsInfoRes> groupResults
                = getMongoOperations().aggregate(agg, OTFSession.class.getSimpleName(), CounselorDashboardSessionsInfoRes.class);
        List<CounselorDashboardSessionsInfoRes> result = groupResults.getMappedResults();
        logger.info("EXIT:" + result);
        return result;
    }

    public List<SessionReportAggregation> getOTFBatchSessionReportAggregation(List<String> batchIds) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(Aggregation.match(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds).and(OTFSession.Constants.STATE).is(SessionState.SCHEDULED)),
                Aggregation.unwind(OTFSession.Constants.BATCH_IDS),
                Aggregation.match(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds)),
                Aggregation.sort(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME)),
                Aggregation.group(OTFSession.Constants.BATCH_IDS)
                        .count().as(SessionReportAggregation.Constants.TOTAL_SESSION_COUNT)
                        .first(OTFSession.Constants.START_TIME).as(SessionReportAggregation.Constants.FIRST_SESSION_START_TIME)
                        .push(OTFSession.Constants.START_TIME).as(SessionReportAggregation.Constants.START_TIMES)
                        .push(OTFSession.Constants.END_TIME).as(SessionReportAggregation.Constants.END_TIMES)
                        .push(OTFSession.Constants.ID).as(SessionReportAggregation.Constants.SESSION_IDS)
        ).withOptions(aggregationOptions);
        AggregationResults<SessionReportAggregation> results = getMongoOperations().aggregate(agr, OTFSession.class,
                SessionReportAggregation.class);
        return results.getMappedResults();
    }

    public void updateSession(Update update, String sessionId) {
        logger.info("updateSession - " + sessionId + "update:" + update);
        if (update == null || StringUtils.isEmpty(sessionId)) {
            return;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants._ID).is(sessionId));
        upsertEntity(query, update, OTFSession.class);
    }

    public List<OTFSession> getOTFSessionTeacherJoinTime(Set<String> sessionIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.ID).in(sessionIds));

        query.fields().include(OTFSession.Constants.ID);
        query.fields().include(OTFSession.Constants.TEACHER_JOIN_TIME);
        query.fields().include(OTFSession.Constants.UNIQUE_STUDENT_ATTENDANCE);
        query.fields().include("adminJoined");
        query.fields().include("techSupport");
        query.fields().include("finalIssueStatus");
        return runQuery(query, OTFSession.class);

    }

    public List<OTFSession> getPastSessionForNoShow(List<String> batchIds, Long endTime) {
        if (endTime == null || ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(endTime));
        // query.addCriteria(Criteria.where(OTFSession.Constants.TEACHER_JOINED).is(true));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.DESC, OTFSession.Constants.END_TIME));
        query.limit(4);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getSessionsForBatchId(String batchId, Long startTime, Long endTime,
            List<String> teacherIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).is(batchId));
        query.addCriteria(Criteria.where(OTFSession.Constants.TEACHER_JOINED).is(true));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(endTime).gt(startTime));
        if (ArrayUtils.isNotEmpty(teacherIds)) {
            query.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).in(teacherIds));
        }

        return runQuery(query, OTFSession.class);
    }

    public String getOTMSessionDBObjectKey(String id) {
        return ENV.toUpperCase() + "_OTM_SESSION_DB_OBJECT_" + id;
    }

    public List<OTFSession> getNonTrialSessionBetweenTime(Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime).lte(endTime));
        List<String> type = new ArrayList<>();
        type.add(OTMSessionType.TRIAL.name());
        type.add(OTMSessionType.EXTRA_TRIAL.name());
        query.addCriteria(Criteria.where(OTFSession.Constants.OTMSESSION_TYPE).nin(type));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.fields().include(OTFSession.Constants._ID);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getOTFSessions(Long afterStartTime, Long beforeStartTime,
            SessionState state, OTMSessionInProgressState progressState, int start, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(state));
        if (progressState != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(progressState));
        }
        if (afterStartTime != null && afterStartTime > 0L) {
            if (beforeStartTime == null || beforeStartTime <= 0L) {
                beforeStartTime = afterStartTime + 300000;
            }
            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime).lt(beforeStartTime));
        }
        query.with(Sort.by(Sort.Direction.ASC, _ID));
        setFetchParameters(query, start, size);
        logger.info("session query : " + query);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getOTFSessions(Long afterStartTime, Long beforeStartTime,
                                           SessionState state, OTMSessionInProgressState progressState, String lastFetchedId, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(state));
        if (progressState != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(progressState));
        }
        if (afterStartTime != null && afterStartTime > 0L) {
            if (beforeStartTime == null || beforeStartTime <= 0L) {
                beforeStartTime = afterStartTime + 300000;
            }
            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(afterStartTime).lt(beforeStartTime));
        }
        if (lastFetchedId != null) {
            query.addCriteria(Criteria.where(_ID).gt(new ObjectId(lastFetchedId)));
        }
        query.with(Sort.by(Sort.Direction.ASC, _ID));
        query.limit(size);
        logger.info("session query : {}", query);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getEndedOTFSessionWithin(long startTime, long endTime) {
        Query query=new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.ENDED_AT).gte(startTime).lt(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(OTMSessionInProgressState.ENDED));

        query.fields().include(OTFSession.Constants.BATCH_IDS);
        query.fields().include(OTFSession.Constants.BOARD_ID);
        query.fields().include(OTFSession.Constants.PRESENTER);

        logger.info("The query for getting OTFSessions within a range is "+query);
        return runQuery(query,OTFSession.class);
    }

    public List<OTFSession> getActiveSessionsTAs(SessionState state, OTMSessionInProgressState progressState, OTFSessionToolType sessionToolType, int start, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(state));
        query.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(progressState));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).is(sessionToolType));
        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        query.fields().include(OTFSession.Constants.TA_IDS);
        setFetchParameters(query, start, size);
        logger.info("session query : " + query);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getNonDeletedSessionByInTimeRange(Long startTime, Long endTime, Integer start, Integer size) {
        Query sessionQuery = new Query();
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(startTime));
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(endTime));
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).exists(true).ne(null));
        setFetchParameters(sessionQuery, start, size);
        return  runQuery(sessionQuery, OTFSession.class);
    }

	public List<OTFSession> getOtfSessionsForDailyStats(Long fromTime, Long tillTime, List<SessionState> states,
			List<OTFSessionToolType> sessionToolTypes, List<OTMSessionType> otmSessionTypes,
			List<OTMSessionInProgressState> progressStates, SortOrder sortOrder, Integer start, Integer size) {

        Query sessionQuery = new Query();

        if (fromTime != null && fromTime > 0L) {
            if (tillTime != null && tillTime > 0L) {
                sessionQuery.addCriteria(
                        Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lt(tillTime));
            } else {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime));
            }
        } else if (tillTime != null && tillTime > 0L) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(tillTime));
        }
        else { // default getting all previous day sessions
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME)
                .gte(System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY)
                .lt(System.currentTimeMillis()));
        }

        if (ArrayUtils.isNotEmpty(states)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(states));
        } else {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        }

        if (ArrayUtils.isNotEmpty(sessionToolTypes)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).in(sessionToolTypes));
        }

        if (ArrayUtils.isNotEmpty(otmSessionTypes)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.OTMSESSION_TYPE).nin(otmSessionTypes));
        }

        if (ArrayUtils.isNotEmpty(progressStates)) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).in(progressStates));
        }

        if (sortOrder != null) {
            sessionQuery.with(Sort.by(Direction.valueOf(sortOrder.name()), OTFSession.Constants.END_TIME));
        } else {
            sessionQuery.with(Sort.by(Direction.ASC, OTFSession.Constants.END_TIME));
        }
        sessionQuery.fields().include(OTFSession.Constants.TA_IDS);
        sessionQuery.fields().include(OTFSession.Constants.PROGRESS_STATE);
        sessionQuery.fields().include(OTFSession.Constants.OTMSESSION_TYPE);
        sessionQuery.fields().include(OTFSession.Constants.FLAGS);
        sessionQuery.fields().include(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE);
        sessionQuery.fields().include(OTFSession.Constants.SESSION_TOOL_TYPE);
        sessionQuery.fields().include(OTFSession.Constants.TYPE);
        sessionQuery.fields().include(OTFSession.Constants.STATE);
        sessionQuery.fields().include(OTFSession.Constants.PRESENTER);
        sessionQuery.fields().include(OTFSession.Constants.END_TIME);
        sessionQuery.fields().include(OTFSession.Constants.START_TIME);
        sessionQuery.fields().include(OTFSession.Constants.RECORDING_START_TIME);
        sessionQuery.fields().include(OTFSession.Constants.PAGES_META_DATA);
        sessionQuery.fields().include(OTFSession.Constants.ENTITY_TAGS);

        setFetchParameters(sessionQuery, start, size);
        return  runQuery(sessionQuery, OTFSession.class);
    }

    public List<OTFSession> getPresentersActiveSessionsByBatchIds(String presenter, Set<String> batchIds, Integer size, Integer start) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(presenter));
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(OTMSessionInProgressState.ENDED));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(System.currentTimeMillis()));

        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, OTFSession.class);
    }

     public void setUpdateOtmSessionConfig(UpdateSessionConfigReq updateConfig, List<String> sessionIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants._ID).in(sessionIds));
        Update update = new Update();
        update.set(OTFSession.Constants.OTM_SESSION_CONFIG, updateConfig);
        updateMulti(query, update, OTFSession.class);
    }

    public Query prepareELQuery(SessionLabel courseType) {
        Query query = new Query();

        // fields to project
        query.fields().include(OTFSession.Constants._ID);
        query.fields().include(OTFSession.Constants.ATTENDEES);
        query.fields().include(OTFSession.Constants.START_TIME);
        query.fields().include(OTFSession.Constants.END_TIME);
        query.fields().include(OTFSession.Constants.PRESENTER);
        query.fields().include(OTFSession.Constants.STATE);
        query.fields().include(OTFSession.Constants.REMARKS);
        query.fields().include(OTFSession.Constants.TITLE);
        query.fields().include(OTFSession.Constants.SESSION_LABELS);

        // common criteria for all EL sessions
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).in(courseType));
//        query.addCriteria(Criteria.where(OTFSession.Constants.OTMSESSION_TYPE).is(OTMSessionType.OTO_NURSERY));
//        query.addCriteria(Criteria.where(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE).is(OTFSessionContextType.WEBINAR));
//        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_TOOL_TYPE).is(OTFSessionToolType.VEDANTU_WAVE_OTO));

        return query;
    }


    public List<OTFSession> getUpcomingELSessionsForUser(String userId, SessionLabel courseType) {
    	Query query = prepareELQuery(courseType);
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(userId));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(System.currentTimeMillis()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));

        // session context type state start time end time attendees
        return runQuery(query, OTFSession.class);

    }
    
    public List<OTFSession> getUpcomingELSessionsForUser(Long userId, SessionLabel courseType) {
       return getUpcomingELSessionsForUser(String.valueOf(userId), courseType);
    }

    // Not used anywhere
    @Deprecated
    public OTFSession getELCurrentSessionForUser(String userId, boolean isSuperCoder, Long fromTime, Long endTime) {
        Query query = prepareELQuery(isSuperCoder ? SessionLabel.SUPER_CODER : SessionLabel.SUPER_READER);

        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(userId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        logger.info("Query getELCurrentSessionForUser "+query);
        return findOne(query, OTFSession.class);
    }

    public OTFSession getELDemoSessionForStudent(Long userId, SessionLabel earlyLearningCourse) {
    	return getELDemoSessionForStudent(String.valueOf(userId),earlyLearningCourse);
    }
    
    public OTFSession getELDemoSessionForStudent(String userId, SessionLabel earlyLearningCourse) {

        Query query = new Query();
//        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_CONTEXT_TYPE).is(OTFSessionContextType.WEBINAR));
        query.addCriteria(Criteria.where((OTFSession.Constants.SESSION_LABELS)).in(SessionLabel.WEBINAR, earlyLearningCourse));
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(userId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        return findOne(query, OTFSession.class);
    }

    public List<OTFSession> getELSessionsForUser(String userId, boolean isSuperCoder) {
        Query query = prepareELQuery(isSuperCoder ? SessionLabel.SUPER_CODER : SessionLabel.SUPER_READER);

        // user can only have an upcoming session within a week from the present day of the calendar
        Long endTime = System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_WEEK;
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(userId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        return runQuery(query, OTFSession.class);
    }

    public OTFSession getELOldBookedSession(String userId, Long deploymentDateInMillis) {
        Query query = new Query();
        // user can only have an upcoming session within a week from the present day of deployment
        Long endTime = deploymentDateInMillis + DateTimeUtils.MILLIS_PER_WEEK;
        query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).lte(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(userId));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).all(SessionLabel.WEBINAR, SessionLabel.SUPER_CODER));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        return findOne(query, OTFSession.class);
    }

    public List<OTFSession> getELSessionForLeadSquared(Long studentId,SessionState state) {

        int start = 0;
        int limit = 10;
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(studentId.toString()));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(state));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).in(SessionLabel.SUPER_CODER, SessionLabel.SUPER_READER));

        query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
        setFetchParameters(query, start, limit);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getELSessionByStudentId(String studentId,SessionLabel earlyLearningCourse, Integer start, Integer size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(studentId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).all(earlyLearningCourse));

        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        setFetchParameters(query,start,size);
        logger.info("getELSessionByStudentId query: "+query);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getELSessionDemoDetailsByStudentId(String studentId,SessionLabel earlyLearningCourse, Integer start, Integer size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(studentId));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).all(SessionLabel.WEBINAR, earlyLearningCourse));

        query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        setFetchParameters(query,start,size);
        logger.info("getELSessionByStudentId query: "+query);
        return runQuery(query, OTFSession.class);
    }

    public List<OTFSession> getELSessions(GetOTFSessionsListReq req) {

        Query query = new Query();
        //have taken the default value SUPER_CODER
        if (req.getEarlyLearningCourseType() == null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).in(SessionLabel.SUPER_CODER, SessionLabel.SUPER_READER));
        } else {
            boolean isSuperCoder = SessionLabel.SUPER_CODER.equals(req.getEarlyLearningCourseType());
            // common criteria for all EL sessions
            if (isSuperCoder) {
                query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).all(SessionLabel.SUPER_CODER));
            } else {
                query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).all(SessionLabel.SUPER_READER));
            }
        }

        if (req.getTeacherId() != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(req.getTeacherId().toString()));
        }
        if (req.getStudentId() != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.ATTENDEES).in(req.getStudentId().toString()));
        }
        if (ArrayUtils.isNotEmpty(req.getStates())) {
            query.addCriteria(Criteria.where(OTFSession.Constants.STATE).in(req.getStates()));
        } else {
            query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        }

        if (req.getFromTime() != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(req.getFromTime()));
        }
        if (req.getTillTime() != null) {
            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(req.getTillTime()));
        }

//        if (req.getFromTime() != null) {
//            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(req.getFromTime()).lte(req.getFromTime()));
//        }

        if (req.getSortOrder() != null) {
            query.with(Sort.by(Direction.valueOf(req.getSortOrder().name()), OTFSession.Constants.START_TIME));
        } else {
            query.with(Sort.by(Direction.DESC, OTFSession.Constants.START_TIME));
        }
        logger.info("getELSessions query-- " + query);

        // fields to project
        query.fields().include(OTFSession.Constants._ID);
        query.fields().include(OTFSession.Constants.ATTENDEES);
        query.fields().include(OTFSession.Constants.START_TIME);
        query.fields().include(OTFSession.Constants.END_TIME);
        query.fields().include(OTFSession.Constants.PRESENTER);
        query.fields().include(OTFSession.Constants.STATE);
        query.fields().include(OTFSession.Constants.REMARKS);
        query.fields().include(OTFSession.Constants.TITLE);
        query.fields().include(OTFSession.Constants.SESSION_LABELS);

        setFetchParameters(query, req);
        return runQuery(query, OTFSession.class);
    }

    public OTFSession getActiveELSessionByPresenter(Long startTime, Long endTime, String presenter,SessionLabel earlyLearningCourse) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(startTime).lt(endTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(presenter));
//        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_CONTEXT_TYPE).is(OTFSessionContextType.WEBINAR));
        query.addCriteria(Criteria.where((OTFSession.Constants.SESSION_LABELS)).all(SessionLabel.WEBINAR, earlyLearningCourse));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.fields().include(OTFSession.Constants.ATTENDEES);
        return findOne(query, OTFSession.class);
    }

    public List<OTFSession> getEarlyLearningSessionsForSMS(Long startTime, Long endTime, int start, int size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lte(endTime).gte(startTime));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).in(SessionLabel.SUPER_CODER, SessionLabel.SUPER_READER));
        setFetchParameters(query, start, size);
        query.fields().include(OTFSession.Constants.START_TIME);
        query.fields().include(OTFSession.Constants.END_TIME);
        query.fields().include(OTFSession.Constants.ATTENDEES);
        query.fields().include(OTFSession.Constants.TITLE);
        return runQuery(query, OTFSession.class);
    }

    // examined winning plan-- using index batchIds_startTime
    public List<OTFSession> getUpcomingSessionsForBatches(List<String> batchIds, Integer start, Integer size, List<String> includeFields) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(System.currentTimeMillis()));
        if (start != null && size != null) {
            setFetchParameters(query, start, size);
        }
        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        return runQuery(query, OTFSession.class, includeFields);
    }

    // TODO-- add relevant index on collection
    public List<OTFSession> getNonWebinarSessionsForDuration(Long fromTime, Long toTime, List<String> includeFields, int start, int size) {
        Query q = new Query();
        q.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(fromTime).lt(toTime));
        q.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));
        q.addCriteria(Criteria.where(OTFSession.Constants.SESSION_LABELS).nin(SessionLabel.WEBINAR));
        q.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.END_TIME));

        setFetchParameters(q, start, size);
        return runQuery(q, OTFSession.class, includeFields);
    }

    public List<OTFSession> getSessionForBatchId(GetSessionsByBatchIdsReq getSessionsByBatchIdsReq, List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
        if(getSessionsByBatchIdsReq.getEndTime()!=null){
            Criteria andCriteria = new Criteria();
            andCriteria.andOperator(Criteria.where(OTFSession.Constants.START_TIME).gte(getSessionsByBatchIdsReq.getStartTime()),
                    Criteria.where(OTFSession.Constants.END_TIME).lt(getSessionsByBatchIdsReq.getEndTime()));
            query.addCriteria(andCriteria);
        } else{
            query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(getSessionsByBatchIdsReq.getStartTime()));
        }
        query.addCriteria(Criteria.where(OTFSession.Constants.STATE).ne(SessionState.DELETED));
        query.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        setFetchParameters(query, getSessionsByBatchIdsReq.getStart(), getSessionsByBatchIdsReq.getSize());
        List<OTFSession> otfSessions = runQuery(query, OTFSession.class);
        return otfSessions;
    }

    public List<OTFSession> getSessionDurtaionAggregation(List<Bson> bsons) {
        AggregateIterable<Document> documents = getMongoOperations()
                .getCollection(OTFSession.class.getSimpleName())
                .aggregate(bsons)
                .allowDiskUse(true)
                .maxTime(15, TimeUnit.SECONDS);
        ArrayList<OTFSession> objects = Lists.newArrayList();
        for (Document document : documents) {
            logger.info("aggregated doc " + document);
            objects.add(convert(document, OTFSession.class));
        }
        return objects;
    }

    public List<OTFSession> getAllSessionsForAppSchedulePage(OTFSessionReq otfSessionReq){
    	Integer start = null != otfSessionReq.getStart() ? otfSessionReq.getStart() : 0;
    	Integer size = null != otfSessionReq.getSize() ? otfSessionReq.getSize() : MAX_COUNT;

		Query sessionQuery = new Query();

		if (null != otfSessionReq && ArrayUtils.isNotEmpty(otfSessionReq.getBatchIds())) {
			sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(otfSessionReq.getBatchIds()));
		}

		if(null != otfSessionReq && null != otfSessionReq.getStartTime() && null != otfSessionReq.getEndTime()) {
			sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(otfSessionReq.getStartTime()).and(OTFSession.Constants.END_TIME).lte(otfSessionReq.getEndTime()));
		}

		sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

//      this is wrong statement. which is always false
//		if (OTFSession.Constants.FLAGS.contains("SALES_DEMO")) {
//			sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(OTMSessionInProgressState.ACTIVE));
//		}

		sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));

		if (null != start && null != size) {
			setFetchParameters(sessionQuery, start, size);
		}

		return runQuery(sessionQuery, OTFSession.class, Arrays.asList(OTFSession.Constants.ID, OTFSession.Constants.BATCH_IDS,
				OTFSession.Constants.BOARD_ID, OTFSession.Constants.AGORA_VIMEO_ID, OTFSession.Constants.END_TIME, OTFSession.Constants.PRESENTER,
				OTFSession.Constants.FLAGS, OTFSession.Constants.START_TIME, OTFSession.Constants.TITLE, OTFSession.Constants.VIMEO_ID));
    }

    public List<OTFSession> getUserLiveOrUpcomingSessionsForApp(OTFSessionReqApp request){

    	Integer start = null != request.getStart() ? request.getStart() : 0;
    	Integer size = null != request.getSize() ? request.getSize() : MAX_COUNT;


    	Query sessionQuery = new Query();
    	Long currentTime = System.currentTimeMillis();


    	if (ArrayUtils.isNotEmpty(request.getBatchIds())) {
    		sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(request.getBatchIds()));
    	}

        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.STATE).is(SessionState.SCHEDULED));

//        this is wrong statement. which is always false
//        if (OTFSession.Constants.FLAGS.contains("SALES_DEMO")) {
//            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PROGRESS_STATE).is(OTMSessionInProgressState.ACTIVE));
//        }

        if(null != request.getTimeFrame())
        if(TimeFrame.UPCOMING.equals(request.getTimeFrame())) {
        	sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gt(currentTime).lt(DateTimeUtils.getTimestampForTomorrowMidnight()));
        	sessionQuery.with(Sort.by(Sort.Direction.ASC, OTFSession.Constants.START_TIME));
        }else if(TimeFrame.LIVE.equals(request.getTimeFrame())){
        	sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).lt(currentTime).and(OTFSession.Constants.END_TIME).gt(currentTime));
        	sessionQuery.with(Sort.by(Sort.Direction.DESC, OTFSession.Constants.START_TIME));
        }



        if (null != start && null != size) {
            setFetchParameters(sessionQuery, start, size);
        }
        sessionQuery.fields().include(OTFSession.Constants.ID);
    	sessionQuery.fields().include(OTFSession.Constants.BATCH_IDS);
    	sessionQuery.fields().include(OTFSession.Constants.BOARD_ID);
    	sessionQuery.fields().include(OTFSession.Constants.PRESENTER);
    	sessionQuery.fields().include(OTFSession.Constants.START_TIME);
    	sessionQuery.fields().include(OTFSession.Constants.END_TIME);
    	sessionQuery.fields().include(OTFSession.Constants.OTF_SESSION_CONTEXT_ID);
    	sessionQuery.fields().include(OTFSession.Constants.OTF_SESSION_CONTEXT_TYPE);
    	sessionQuery.fields().include(OTFSession.Constants.TITLE);
    	sessionQuery.fields().include(OTFSession.Constants.SESSION_TOOL_TYPE);
    	sessionQuery.fields().include(OTFSession.Constants.PROGRESS_STATE);
    	sessionQuery.fields().include(OTFSession.Constants.STATE);


    	logger.info("getUserLiveOrUpcomingSessionsForApp sessionQuery -> "+sessionQuery );
       return runQuery(sessionQuery, OTFSession.class);
    }

    public List<OTFSession> getUpcomingOtfSessions(String userId, Role role, Integer start, Integer limit, Set<String> webinarSessionIds, Set<String> batchIds) {
        Query sessionQuery = new Query();
        Long currentTime = System.currentTimeMillis();
        if (Role.STUDENT == role) {
            if (ArrayUtils.isNotEmpty(batchIds) && ArrayUtils.isNotEmpty(webinarSessionIds)) {
                sessionQuery.addCriteria(new Criteria().orOperator(
                        Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds),
                        Criteria.where(OTFSession.Constants._ID).in(webinarSessionIds)
                ));
            } else if (ArrayUtils.isNotEmpty(batchIds)) {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.BATCH_IDS).in(batchIds));
            } else if (ArrayUtils.isNotEmpty(webinarSessionIds)) {
                sessionQuery.addCriteria(Criteria.where(OTFSession.Constants._ID).in(webinarSessionIds));
            }
        }
        if (Role.TEACHER == role) {
            sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.PRESENTER).is(userId));
        }

        sessionQuery.addCriteria(Criteria.where(OTFSession.Constants.END_TIME).gte(currentTime));
        sessionQuery.addCriteria(Criteria.where(STATE).is(SessionState.SCHEDULED));
        sessionQuery.with(Sort.by(Sort.Direction.ASC, START_TIME));
        setFetchParameters(sessionQuery, start, limit);

        sessionQuery.fields().exclude(OTFSession.Constants.ATTENDEES);
        logger.info("get upcoming session query {}", sessionQuery);
        return runQuery(sessionQuery, OTFSession.class);
    }
}
