package com.vedantu.scheduling.dao.entity;

import java.util.List;

import com.vedantu.scheduling.pojo.GTTRecording;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class GTTSessionRecordingInfo extends AbstractMongoStringIdEntity {
	private String sessionId;
	private String trainingId;
	private List<GTTRecording> gttRecordings;
	private Boolean recordingDeleted;

	public GTTSessionRecordingInfo() {
		super();
	}

	public GTTSessionRecordingInfo(String sessionId, String trainingId, List<GTTRecording> recordings) {
		super();
		this.sessionId = sessionId;
		this.trainingId = trainingId;
		this.gttRecordings = recordings;
	}

	public GTTSessionRecordingInfo(OTFSession oTFSession, List<GTTRecording> recordings) {
		this(oTFSession.getId(), oTFSession.getMeetingId(), recordings);
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(String trainingId) {
		this.trainingId = trainingId;
	}

	public List<GTTRecording> getGttRecordings() {
		return gttRecordings;
	}

	public void setGttRecordings(List<GTTRecording> gttRecordings) {
		this.gttRecordings = gttRecordings;
	}

	public Boolean getRecordingDeleted() {
		return recordingDeleted;
	}

	public void setRecordingDeleted(Boolean recordingDeleted) {
		this.recordingDeleted = recordingDeleted;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String TRAINING_ID = "trainingId";
		public static final String SESSION_ID = "sessionId";
	}

	@Override
	public String toString() {
		return "GTTSessionRecordingInfo [sessionId=" + sessionId + ", trainingId=" + trainingId + ", gttRecordings="
				+ gttRecordings + ", toString()=" + super.toString() + "]";
	}
}
