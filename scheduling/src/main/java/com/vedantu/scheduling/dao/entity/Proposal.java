package com.vedantu.scheduling.dao.entity;



import com.vedantu.scheduling.request.proposal.AddProposalReq;
import com.vedantu.scheduling.response.proposal.ProposalRes;
import com.vedantu.session.pojo.ProposalRepeatOrder;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.session.pojo.proposal.ProposalSlot;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.enums.ProposalPaymentMode;
import com.vedantu.util.enums.ProposalStatus;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


//TODO revamp this data model with the suggestions given
@Document(collection = "Proposal")
@CompoundIndexes({ 
    @CompoundIndex(name = "fromUserId_proposalStatus", def = "{'fromUserId' : 1, 'proposalStatus': 1}"),
    @CompoundIndex(name = "toUserId_proposalStatus", def = "{'toUserId' : 1, 'proposalStatus': 1}")
})
public class Proposal extends AbstractMongoLongIdEntity {

	private Long fromUserId;
	private Long toUserId;
	private Long startTime;//this won't be needed once proposalSlots is a pojo
	private Long endTime;//this won't be needed once proposalSlots is a pojo
	private Long boardId;
	private String topicName;// remove this and use List<Long> boardIds instead if topic is needed
	private String title;
	private ProposalRepeatOrder repeatOrder;
	private Integer repeatFrequency;
        @Indexed(background = true)
	private Long subscriptionId; //if this is a payment context helper, use separate pojo for it
	private Long offeringId;//remove this, if a context is needed create separate pojos for this 
	private ProposalStatus proposalStatus;
	private SessionPayoutType paymentType;
	private List<ProposalSlot> proposalSlots;
	private Long scheduledBy;
	private String reason;
	private String modifiedFieldData;//not the right way, use versioning if old data is required
	private ProposalPaymentMode paymentMode;
	private Integer flowCount;

	public Proposal(Long fromUserId, Long toUserId, Long startTime, Long endTime, Long boardId, String title,
			ProposalRepeatOrder repeatOrder, Integer repeatFrequency, Long offeringId, Long subscriptionId,
			ProposalStatus proposalStatus, SessionPayoutType paymentType, List<ProposalSlot> proposalSlots, String reason,
			String modifiedFieldData, ProposalPaymentMode paymentMode, String topicName) {
		super();
		this.fromUserId = fromUserId;
		this.toUserId = toUserId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.boardId = boardId;
		this.title = title;
		this.repeatOrder = repeatOrder;
		this.repeatFrequency = repeatFrequency;
		this.offeringId = offeringId;
		this.subscriptionId = subscriptionId;
		this.proposalStatus = proposalStatus;
		this.paymentType = paymentType;
		this.reason = reason;
		this.modifiedFieldData = modifiedFieldData;
		this.paymentMode = paymentMode;
		this.topicName = topicName;
		this.proposalSlots = proposalSlots;
	}

	public Proposal() {
		super();
	}

	public Long getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public ProposalRepeatOrder getRepeatOrder() {
		return repeatOrder;
	}

	public void setRepeatOrder(ProposalRepeatOrder repeatOrder) {
		this.repeatOrder = repeatOrder;
	}

	public Integer getRepeatFrequency() {
		if (repeatFrequency == null || repeatFrequency < 1) {
			repeatFrequency = 1;
		}

		return repeatFrequency;
	}

	public void setRepeatFrequency(Integer repeatFrequency) {
		this.repeatFrequency = repeatFrequency;
	}

	public Long getSubscriptionId() {
		if (subscriptionId == null) {
			subscriptionId = Long.valueOf(0);
		}
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public ProposalStatus getProposalStatus() {
		return proposalStatus;
	}

	public void setProposalStatus(ProposalStatus proposalStatus) {
		this.proposalStatus = proposalStatus;
	}

	public SessionPayoutType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(SessionPayoutType paymentType) {
		this.paymentType = paymentType;
	}

	public Long getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(Long scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getModifiedFieldData() {
		return modifiedFieldData;
	}

	public void setModifiedFieldData(String modifiedFieldData) {
		this.modifiedFieldData = modifiedFieldData;
	}

	public ProposalPaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(ProposalPaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ProposalSlot> getProposalSlots() {
		return proposalSlots;
	}

	public void setProposalSlots(List<ProposalSlot> proposalSlots) {
		this.proposalSlots = proposalSlots;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Integer getFlowCount() {
		if (flowCount == null || flowCount < 0) {
			flowCount = 0;
		}
		return flowCount;
	}

	public void setFlowCount(Integer flowCount) {
		this.flowCount = flowCount;
	}

	public void incrementFlowCount() {
		if (this.flowCount == null || this.flowCount <= 0) {
			this.flowCount = 1;
		} else {
			this.flowCount++;
		}
	}

	public static Proposal toProposal(AddProposalReq req){
		return new Proposal(req.getFromUserId(), req.getToUserId(), req.getStartTime(), req.getEndTime(), req.getBoardId(), req.getTitle(),
				req.getRepeatOrder(), req.getRepeatFrequency(), req.getOfferingId(), req.getSubscriptionId(),
				req.getProposalStatus(), req.getPaymentType(), req.getProposalSlotList(), req.getReason(), null,
				req.getPaymentMode(), req.getTopicName());
	}
        
        public ProposalRes toProposalRes(Mapper mapper){
            ProposalRes res = mapper.map(this, ProposalRes.class);
            res.setProposalSlotList(proposalSlots);
            return res;
        }

        @Override
        public String toString() {
            return super.toString()+" Proposal{" + "fromUserId=" + fromUserId + ", toUserId=" + toUserId + ", startTime=" + startTime + ", endTime=" + endTime + ", boardId=" + boardId + ", topicName=" + topicName + ", title=" + title + ", repeatOrder=" + repeatOrder + ", repeatFrequency=" + repeatFrequency + ", subscriptionId=" + subscriptionId + ", offeringId=" + offeringId + ", proposalStatus=" + proposalStatus + ", paymentType=" + paymentType + ", proposalSlots=" + proposalSlots + ", scheduledBy=" + scheduledBy + ", reason=" + reason + ", modifiedFieldData=" + modifiedFieldData + ", paymentMode=" + paymentMode + ", flowCount=" + flowCount + '}';
        }


	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String FROM_USERID = "fromUserId";
		public static final String TO_USERID = "toUserId";
		public static final String START_TIME = "startTime";
		public static final String END_TIME = "endTime";
		public static final String BOARD_ID = "boardId";
		public static final String TOPIC_NAME = "topicName";
		public static final String TITLE = "title";
		public static final String REPEAT_ORDER = "repeatOrder";
		public static final String REPEAT_FREQUENCY = "repeatFrequency";
		public static final String OFFERING_ID = "offeringId";
		public static final String SUBSCRIPTION_ID = "subscriptionId";
		public static final String PROPOSAL_STATUS = "proposalStatus";
		public static final String STATUS = "status";
		public static final String PAYMENT_TYPE = "paymentType";
		public static final String PROPOSAL_SLOTS = "proposalSlots";
		public static final String SCHEDULED_BY = "scheduledBy";
		public static final String REASON = "reason";
		public static final String MODIFIED_FIELD_DATA = "modifiedFieldData";
		public static final String PAYMENT_MODE = "paymentMode";
	}

}
