package com.vedantu.scheduling.dao.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "TeacherSubscription")
public class TeacherSubscription extends AbstractMongoStringIdEntity {
        @Indexed(background=true)
	private Long userId;
	private boolean subscribed;

	public TeacherSubscription() {
		super();
	}

	public TeacherSubscription(Long userId, boolean subscribed) {

		super();
		this.userId = userId;
		this.subscribed = subscribed;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public boolean getSubscribed() {
		return subscribed;
	}

	public void setSubscribed(boolean subscribed) {
		this.subscribed = subscribed;
	}

	public static class Constants extends AbstractMongoEntity.Constants {
		public static final String USERID = "userId";
		public static final String SUBSCRIBED = "subscribed";
	}
}