package com.vedantu.scheduling.dao.serializers;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vedantu.scheduling.dao.entity.CalendarEvent;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class CalendarEventDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public CalendarEventDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void save(CalendarEvent calendarEvent) {
		try {
			Assert.notNull(calendarEvent);
			saveEntity(calendarEvent);
		} catch (Exception ex) {
			throw new RuntimeException(
					"CalendarEventUpdateError : Error updating the calendarEvent " + calendarEvent.toString(), ex);
		}
	}

	public CalendarEvent getById(String id) {
		CalendarEvent calendarEvent = null;
		try {
			Assert.hasText(id);
			calendarEvent = (CalendarEvent) getEntityById(id, CalendarEvent.class);
		} catch (Exception ex) {
			throw new RuntimeException("CalendarEventUpdateError : Error fetch the calendarEvent with id " + id, ex);
		}
		return calendarEvent;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			Assert.hasText(id);
			result = deleteEntityById(id, CalendarEvent.class);
		} catch (Exception ex) {
		}

		return result;
	}

}
