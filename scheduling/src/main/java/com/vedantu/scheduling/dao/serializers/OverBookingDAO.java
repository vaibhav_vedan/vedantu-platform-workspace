package com.vedantu.scheduling.dao.serializers;


import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.dao.entity.CalendarEntry;
import com.vedantu.scheduling.dao.entity.OTFSession;
import com.vedantu.scheduling.dao.entity.OverBooking;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.pojo.SlotBookingCount;
import com.vedantu.scheduling.request.GetOverBookingListReq;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import lombok.extern.flogger.Flogger;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Service
public class OverBookingDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OverBookingDAO.class);

    public OverBookingDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(OverBooking overBooking) {
        try {
            Assert.notNull(overBooking);
            saveEntity(overBooking);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "OverBookingUpdateError : Error updating the OverBooking " + overBooking.toString(), ex);
        }
    }

    public OverBooking getOverBookingById(String bookingId) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OverBooking.Constants._ID).is(bookingId));
        logger.info("OverBookingById Query : "+query);
        return findOne(query, OverBooking.class);
    }

    public OverBooking getEarlyLearningBookingByStudentId(Long studentId, SessionLabel earlyLearningCourse) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OverBooking.Constants.Student_Id).is(String.valueOf(studentId)));
        query.addCriteria(Criteria.where(OverBooking.Constants.LABELS).in(earlyLearningCourse));
        query.addCriteria(Criteria.where(OverBooking.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.DESC ,OverBooking.Constants.SLOT_START_TIME));
        logger.info("query : "+query);
        return findOne(query,OverBooking.class);
    }

    
    public OverBooking getEarlyLearningBookingByStudentId(String studentId, SessionLabel earlyLearningCourse) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OverBooking.Constants.Student_Id).is(studentId));
        query.addCriteria(Criteria.where(OverBooking.Constants.LABELS).in(earlyLearningCourse));
        query.addCriteria(Criteria.where(OverBooking.Constants.STATE).is(SessionState.SCHEDULED));
        query.with(Sort.by(Direction.DESC ,OverBooking.Constants.SLOT_START_TIME));
        logger.info("query : "+query);
        return findOne(query,OverBooking.class);
    }

    //Don't Use this query...
    public List<OverBooking> getBookingsForMigration(Long migrationDayStartTime, Integer limit) {

        Query query = new Query();
        query.addCriteria(Criteria.where(OverBooking.Constants.SLOT_START_TIME).gte(migrationDayStartTime));
        query.addCriteria(Criteria.where(OverBooking.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OverBooking.Constants.PROFICIENCY_TYPE).exists(Boolean.FALSE));
        query.with(Sort.by(Direction.ASC ,OverBooking.Constants.SLOT_START_TIME));
        setFetchParameters(query, 0, limit);
        logger.info("query : "+query);
        return runQuery(query,OverBooking.class);
    }

    public List<OverBooking> getEarlyLearningBooking(GetOverBookingListReq req) {

        Query query = new Query();

        if (req.getStudentId() != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.Student_Id).in(req.getStudentId().toString()));
        }
        if (req.getEarlyLearningCourseType() != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.LABELS).in(req.getEarlyLearningCourseType()));
        } else {
            query.addCriteria(Criteria.where(OverBooking.Constants.LABELS).in(SessionLabel.SUPER_CODER, SessionLabel.SUPER_READER));
        }
        if (req.getStates() != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.STATE).in(req.getStates()));
        } else {
            query.addCriteria(Criteria.where(OverBooking.Constants.STATE).ne(SessionState.DELETED));
        }
        if (req.getFromTime() != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.SLOT_START_TIME).gte(req.getFromTime()));
        }
        if (req.getTillTime() != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.SLOT_END_TIME).lte(req.getTillTime()));
        }
        if(req.getIsTeacherAssigned() != null) {
            if (OverBookingStatus.STUDENT_NOT_JOINED.equals(req.getIsTeacherAssigned())) {
                query.addCriteria(Criteria.where(OverBooking.Constants.SESSION_ID).exists(false));
                query.addCriteria(Criteria.where(OverBooking.Constants.IS_TEACHER_ASSIGNED).exists(false));
            } else if(OverBookingStatus.TEACHER_ASSIGNED.equals(req.getIsTeacherAssigned())) {
                query.addCriteria(Criteria.where(OverBooking.Constants.IS_TEACHER_ASSIGNED).is(OverBookingStatus.YES));
            } else {
                query.addCriteria(Criteria.where(OverBooking.Constants.IS_TEACHER_ASSIGNED).is(OverBookingStatus.NO));
            }
        }
        if (req.getSortOrder() != null) {
            query.with(Sort.by(Direction.valueOf(req.getSortOrder().name()), OverBooking.Constants.SLOT_START_TIME));
        } else {
            query.with(Sort.by(Direction.DESC, OverBooking.Constants.SLOT_START_TIME));
        }
        logger.info("getOverBooking query-- " + query);

        setFetchParameters(query, req);
        return runQuery(query, OverBooking.class);
    }

    public List<SlotBookingCount> getOverBookingAggregationWithSlotTime(Long startTime, Long endTime, SessionLabel earlyLearningCourseType) {

        Criteria andOperator = new Criteria().andOperator(Criteria.where(OverBooking.Constants.SLOT_START_TIME).gte(startTime).lte(endTime),
                                                          Criteria.where(OverBooking.Constants.LABELS).is(earlyLearningCourseType),
                                                            Criteria.where(OverBooking.Constants.STATE).is(SessionState.SCHEDULED));
        List<AggregationOperation> aggregationOperation = new ArrayList<>();
        aggregationOperation.add(Aggregation.match(andOperator));
        aggregationOperation.add(Aggregation.match(Criteria.where(OverBooking.Constants.PROFICIENCY_TYPE).ne(null)));
        aggregationOperation.add(Aggregation.group(OverBooking.Constants.SLOT_START_TIME, OverBooking.Constants.PROFICIENCY_TYPE).count().as("count"));
        aggregationOperation.add(Aggregation.project("count").and(OverBooking.Constants._ID + "." + OverBooking.Constants.SLOT_START_TIME).as(OverBooking.Constants.SLOT_START_TIME)
                                                           .and(OverBooking.Constants._ID + "."  + OverBooking.Constants.PROFICIENCY_TYPE).as(OverBooking.Constants.PROFICIENCY_TYPE));
        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);
        logger.info("Aggregation Query Booking "+aggregation);
        AggregationResults<SlotBookingCount> groupResults = getMongoOperations().aggregate(aggregation,
                OverBooking.class.getSimpleName(), SlotBookingCount.class);
        List<SlotBookingCount> results = groupResults.getMappedResults();
        logger.info("Size of SlotBookingCount : "+results.size());
        return results;
    }

    public List<OverBooking> assignedTeacherForBooking(Long startTime, Long endTime, SessionLabel courseType, int start, int limit, String studentId, ProficiencyType proficiencyType) {

        Query query = new Query();
        if(StringUtils.isNotEmpty(studentId)) {
            query.addCriteria(Criteria.where(OverBooking.Constants.Student_Id).is(studentId));
        }
        if(courseType != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.LABELS).in(courseType));
        }
        if(proficiencyType != null) {
            query.addCriteria(Criteria.where(OverBooking.Constants.TEACHER_PROFICIENCY).is(proficiencyType));
        }
        query.addCriteria(Criteria.where(OverBooking.Constants.SLOT_START_TIME).gte(startTime).lt(endTime));
        query.addCriteria(Criteria.where(OverBooking.Constants.STATE).is(SessionState.SCHEDULED));
        query.addCriteria(Criteria.where(OverBooking.Constants.SESSION_ID).exists(Boolean.TRUE));
        query.fields().include(OverBooking.Constants.SESSION_ID);
        query.fields().include(OverBooking.Constants.Student_Id);
        setFetchParameters(query,start,limit);
        return runQuery(query, OverBooking.class);
    }
}