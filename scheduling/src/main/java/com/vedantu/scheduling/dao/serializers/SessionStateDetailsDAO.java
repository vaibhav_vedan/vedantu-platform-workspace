package com.vedantu.scheduling.dao.serializers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.scheduling.dao.sql.entity.SessionStateDetails;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.LogFactory;
import org.hibernate.LockMode;

@Service
public class SessionStateDetailsDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionStateDetailsDAO.class);

	public SessionStateDetailsDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	public void create(SessionStateDetails sessionStateDetails, Session session) {
		create(sessionStateDetails, session, null);
	}

	public void create(SessionStateDetails sessionStateDetails, Session session, String callingUserId) {
		try {
			Assert.notNull(sessionStateDetails);
			logger.info("create SessionStateDetails: " + sessionStateDetails.toString());
			saveEntity(sessionStateDetails, session, callingUserId);
		} catch (Exception ex) {
			logger.error("Create sessionStateDetails: " + ex.getMessage());
			throw ex;
		}
	}

	public SessionStateDetails getById(Long id, Session session) {
		SessionStateDetails sessionStateDetails = null;
		try {
			if (id != null) {
				sessionStateDetails = (SessionStateDetails) getEntityById(id, null, SessionStateDetails.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById sessionStateDetails: " + ex.getMessage());
			sessionStateDetails = null;
		}
		return sessionStateDetails;
	}

	public SessionStateDetails getBySessionId(Long sessionId, Session session) {
		return getBySessionId(sessionId, session, null);
	}        
        
	public SessionStateDetails getBySessionId(Long sessionId, Session session,
                LockMode lockMode) {
		SessionStateDetails sessionStateDetails = null;
		try {
			Assert.notNull(sessionId);
			Criteria cr = session.createCriteria(SessionStateDetails.class);
			cr.add(Restrictions.eq(SessionStateDetails.Constants.SESSION_ID, sessionId));
			List<SessionStateDetails> results = runQuery(session, cr, SessionStateDetails.class, lockMode);
			if (!CollectionUtils.isEmpty(results)) {
				if (results.size() > 1) {
					logger.error("Duplicate SessionStateDetails entries found for id : " + sessionId);
				}
				sessionStateDetails = results.get(0);
			} else {
				logger.info("getById SessionStateDetails : no session state details found" + sessionId);
			}
		} catch (Exception ex) {
			logger.error("getById SessionStateDetails : " + ex.getMessage());
			sessionStateDetails = null;
		}
		return sessionStateDetails;
	}

	public List<SessionStateDetails> getActiveAndStartedSessions(List<Long> sessionIds, Session session) {
		List<SessionStateDetails> sessionStateDetails = new ArrayList<SessionStateDetails>();
		try {
			Assert.notEmpty(sessionIds);
			Criteria cr = session.createCriteria(SessionStateDetails.class);
			cr.add(Restrictions.in(SessionStateDetails.Constants.SESSION_ID, sessionIds));
			cr.add(Restrictions.in(SessionStateDetails.Constants.SESSION_STATE,
					Arrays.asList(SessionState.ACTIVE, SessionState.STARTED)));

			sessionStateDetails = runQuery(session, cr, SessionStateDetails.class);
		} catch (Exception ex) {
			logger.info("getById SessionStateDetails : " + ex.getMessage());
			sessionStateDetails = null;
		}
		return sessionStateDetails;
	}

	public List<SessionStateDetails> getBySessionId(List<Long> sessionIds, Session session) {
		List<SessionStateDetails> results = new ArrayList<>();
		try {
			Assert.notEmpty(sessionIds);
			Criteria cr = session.createCriteria(SessionStateDetails.class);
			// TODO : testing required for the type matching
			cr.add(Restrictions.in(SessionStateDetails.Constants.SESSION_ID, sessionIds));
			results = runQuery(session, cr, SessionStateDetails.class);
			if (CollectionUtils.isEmpty(results)) {
				logger.info("getById SessionStateDetails : No session details found : "
						+ Arrays.toString(sessionIds.toArray()));
			}
		} catch (Exception ex) {
			logger.error("getById SessionStateDetails : " + ex.getMessage());
		}
		return results;
	}

	public void update(SessionStateDetails p, Session session) {
		update(p, session, null);
	}

	public void update(SessionStateDetails p, Session session, String callingUserId) {
		try {
			if (p != null) {
				logger.info("update SessionStateDetails: " + p.toString());
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Update SessionStateDetails: " + ex.getMessage());
		}
	}

	public void updateAll(List<SessionStateDetails> p, Session session) {
		updateAll(p, session, null);
	}

	public void updateAll(List<SessionStateDetails> p, Session session, String callingUserId) {
		try {
			if (p != null) {
				logger.info("update SessionStateDetails: " + p.toString());
				updateAllEntities(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Update SessionStateDetails: " + ex.getMessage());
		}
	}

	public List<SessionStateDetails> getStartedSessionStateDetails(long startTime, long endTime, Session session) {
		List<SessionStateDetails> results = new ArrayList<>();
		try {
			Criteria cr = session.createCriteria(SessionStateDetails.class);
			// TODO : testing required for the type matching
			cr.add(Restrictions.gt(SessionStateDetails.Constants.LAST_UPDATED_TIME, startTime));
			cr.add(Restrictions.lt(SessionStateDetails.Constants.LAST_UPDATED_TIME, endTime));
			cr.add(Restrictions.eq(SessionStateDetails.Constants.SESSION_STATE, SessionState.STARTED));
			results = runQuery(session, cr, SessionStateDetails.class);
		} catch (Exception ex) {
			logger.error("getById SessionStateDetails : " + ex.getMessage());
		}
		return results;
	}
}
