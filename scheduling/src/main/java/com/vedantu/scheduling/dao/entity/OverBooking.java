package com.vedantu.scheduling.dao.entity;


import com.google.common.collect.Sets;
import com.vedantu.onetofew.enums.*;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.User.enums.TeacherCategory.ProficiencyType;
import com.vedantu.onetofew.enums.OTFSessionContextType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.enums.OverBookingStatus;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.request.AddEarlyLearningBookingReq;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "OverBooking")
@CompoundIndexes({
        @CompoundIndex(name = "slotStartTime", def = "{'slotStartTime': 1}", background = true),
        @CompoundIndex(name = "state_slotStartTime_ProficiencyType", def = "{'state':1, 'flags' : 1, 'proficiencyType' : 1, 'slotStartTime':1}", background = true)
})
public class OverBooking extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String studentId;
    private Long slotStartTime;
    private Long slotEndTime;
    private String sessionId;
    private SessionState state;
    private Set<OTFSessionFlag> flags = new HashSet<>();
    private Set<SessionLabel> labels = new HashSet<>();
    private ProficiencyType proficiencyType;
    @Deprecated
    private OTMSessionType otmSessionToolType = OTMSessionType.OTO_NURSERY;
    @Deprecated
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE;
    @Deprecated
    private OTFSessionContextType otfSessionContextType = OTFSessionContextType.WEBINAR;
    private String CancelledBY;
    private String Remarks;
    private OverBookingStatus isTeacherAssigned;
    private String alternatePhoneNumber;
    private String grade;
    private ProficiencyType teacherProficiency;

    public OverBooking(AddEarlyLearningBookingReq earlyLearningBooking) {
        super();
        this.studentId = earlyLearningBooking.getStudentId();
        this.slotStartTime = earlyLearningBooking.getSlotStartTime();
        this.slotEndTime = earlyLearningBooking.getSlotEndTime();
        this.otmSessionToolType = earlyLearningBooking.getOtmSessionType();
        this.sessionToolType = earlyLearningBooking.getSessionToolType();
        this.labels = Sets.newHashSet(SessionLabel.WEBINAR);
        this.state = SessionState.SCHEDULED;
    }

    public OverBooking(OTFSession otfSession) {

     super();
     this.studentId = otfSession.getAttendees().get(0);
     this.slotStartTime = otfSession.getStartTime();
     this.slotEndTime = otfSession.getEndTime();
     this.sessionId = otfSession.getId();
     this.flags = otfSession.getFlags();
     this.labels = otfSession.getLabels();
     this.isTeacherAssigned = OverBookingStatus.YES;
     this.state = SessionState.SCHEDULED;
    }

    public static double getOverBookingFactor(SessionLabel earlyLearningCourse) {

        if(SessionLabel.SUPER_CODER.equals(earlyLearningCourse)) {
            return ConfigUtils.INSTANCE.getFloatValue("vedantu.overBookingfactor.supercoder");
        }
        return ConfigUtils.INSTANCE.getFloatValue("vedantu.overBookingfactor.superreader");
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String FLAGS = "flags";
        public static final String LABELS = "labels";
        public static final String STATE = "state";
        public static final String Student_Id = "studentId";
        public static final String SLOT_END_TIME = "slotEndTime";
        public static final String SLOT_START_TIME = "slotStartTime";
        public static final String IS_TEACHER_ASSIGNED = "isTeacherAssigned";
        public static final String SESSION_ID = "sessionId";
        public static final String PROFICIENCY_TYPE = "proficiencyType";
        public static final String TEACHER_PROFICIENCY = "teacherProficiency";
    }

    @Override
    public String toString() {
        return "OverBooking [studentId=" + studentId + ", slotStartTime=" + slotStartTime + ", slotEndTime=" + slotEndTime
                + ", state=" + state + ", flags=" + flags + ", alternatePhoneNumber=" + alternatePhoneNumber
                + ", toString()=" + super.toString() + "]";
    }
}
