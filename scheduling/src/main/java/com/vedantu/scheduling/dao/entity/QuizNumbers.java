package com.vedantu.scheduling.dao.entity;

public class QuizNumbers {

    private Integer numberAsked;
    private Integer numberAttempted;
    private Integer numberCorrect;

    public QuizNumbers() {
    }

    public QuizNumbers(Integer numberAsked, Integer numberAttempted, Integer numberCorrect) {
        this.numberAsked = numberAsked;
        this.numberAttempted = numberAttempted;
        this.numberCorrect = numberCorrect;
    }

    public Integer getNumberAttempted() {
        return numberAttempted;
    }

    public void setNumberAttempted(Integer numberAttempted) {
        this.numberAttempted = numberAttempted;
    }

    public Integer getNumberCorrect() {
        return numberCorrect;
    }

    public void setNumberCorrect(Integer numberCorrect) {
        this.numberCorrect = numberCorrect;
    }

    public Integer getNumberAsked() {
        return numberAsked;
    }

    public void setNumberAsked(Integer numberAsked) {
        this.numberAsked = numberAsked;
    }

    @Override
    public String toString() {
        return "QuizNumbers{" + "numberAsked=" + numberAsked + ", numberAttempted=" + numberAttempted + ", numberCorrect=" + numberCorrect + '}';
    }

}
