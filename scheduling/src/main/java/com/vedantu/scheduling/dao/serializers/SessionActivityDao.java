package com.vedantu.scheduling.dao.serializers;

import com.vedantu.scheduling.dao.entity.SessionActivity;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mano
 */
@Service
public class SessionActivityDao extends AbstractMongoDAO {


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(SessionActivity activity) {
        saveEntity(activity);
    }

    public SessionActivity findAndAddDownloadActivity(String sessionId, Long userId, String url) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SessionActivity.Constants.SESSION_ID).is(sessionId));
        if (userId != null) {
            query.addCriteria(Criteria.where(SessionActivity.Constants.USER_ID).is(userId));
        }
        SessionActivity activity = findOne(query, SessionActivity.class);
        if (activity == null) {
            activity = new SessionActivity();
            activity.setSessionId(sessionId);
            activity.setUserId(userId);
        }
        SessionActivity.Download download = new SessionActivity.Download();
        download.setDownloadedAt(System.currentTimeMillis());
        download.setUrl(url);
        List<SessionActivity.Download> downloads = CollectionUtils.defaultIfNull(activity.getDownloads(), ArrayList::new);
        downloads.add(download);
        activity.setDownloads(downloads);
        save(activity);
        return activity;
    }
}
