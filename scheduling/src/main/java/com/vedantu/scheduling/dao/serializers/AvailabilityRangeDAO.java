package com.vedantu.scheduling.dao.serializers;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.AvailabilityRange;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class AvailabilityRangeDAO extends AbstractMongoDAO {

                @Autowired
                private MongoClientFactory mongoClientFactory;    


                public AvailabilityRangeDAO() {
                    super();
                }

                @Override
                protected MongoOperations getMongoOperations() {
                    return mongoClientFactory.getMongoOperations();
                }

		public void save(AvailabilityRange p) {
			try {
				if (p != null) {
					saveEntity(p);
				}
			} catch (Exception ex) {
			}
		}

		public AvailabilityRange getById(String id) {
			AvailabilityRange SlabRange = null;
			try {
				if (!StringUtils.isEmpty(id)) {
					SlabRange = (AvailabilityRange) getEntityById(id, AvailabilityRange.class);
				}
			} catch (Exception ex) {
				SlabRange = null;
			}
			return SlabRange;
		}

		public int deleteById(String id) {
			int result = 0;
			try {
				result = deleteEntityById(id, AvailabilityRange.class);
			} catch (Exception ex) {
			}

			return result;
		}

	
}
