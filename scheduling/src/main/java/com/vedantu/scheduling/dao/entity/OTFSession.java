package com.vedantu.scheduling.dao.entity;

import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.OTFWebinarSession;
import com.vedantu.onetofew.request.OTFWebinarSessionReq;
import com.vedantu.scheduling.enums.OTMServiceProvider;
import com.vedantu.scheduling.managers.OTFSessionManager;
import com.vedantu.scheduling.pojo.GTMCreateSessionRes;
import com.vedantu.scheduling.pojo.OTMSessionConfig;
import com.vedantu.scheduling.pojo.Pollsdata;
import com.vedantu.scheduling.pojo.session.CanvasStreamingType;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.scheduling.request.AddEarlyLearningSessionReq;
import com.vedantu.scheduling.request.AddSalesDemoSessionReq;
import com.vedantu.scheduling.request.AddWebinarSessionReq;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.DBUtils;
import lombok.*;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Document(collection = "OTFSession")
@CompoundIndexes({
        @CompoundIndex(name = "state_1_startTime_1_batchIds_1_endTime_1", def = "{'state': 1, 'startTime': 1, 'batchIds': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "sessionContextType_1_state_1_startTime_1_attendees_1_endTime_1", def = "{'sessionContextType': 1, 'state': 1, 'startTime': 1, 'attendees': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "sessionContextType_1_presenter_1_state_1_startTime_1_endTime_1", def = "{'sessionContextType': 1, 'presenter': 1, 'state': 1, 'startTime': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "state_1_presenter_1_batchIds_1_startTime_1_endTime_1", def = "{'state': 1, 'presenter': 1, 'batchIds': 1, 'startTime': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "state_1_batchIds_1_startTime_1_endTime_1", def = "{'state': 1, 'batchIds': 1, 'startTime': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "state_1_endTime_1_batchIds_1", def = "{'state': 1, 'endTime': 1, 'batchIds': 1}", background = true)
        ,
        @CompoundIndex(name = "endTime", def = "{'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "startTime", def = "{'startTime': 1}", background = true)
        ,
        @CompoundIndex(name = "batchIds_1_endTime_1", def = "{'batchIds': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "batchIds_1_startTime_1", def = "{'batchIds': 1, 'startTime': 1}", background = true)
        ,
        @CompoundIndex(name = "attendees_1_endTime_1", def = "{'attendees': 1, 'endTime': 1}", background = true)
        ,
        @CompoundIndex(name = "batchIds_1_state_1", def = "{'batchIds': 1, 'state': 1}", background = true)
        ,
        @CompoundIndex(name = "state_1_startTime_1__id_1", def = "{ 'state': 1, 'startTime': 1, '_id': 1 }", background = true)
        ,
        @CompoundIndex(name = "state_1_startTime_1_progressState_1__id_1", def = "{ 'state': 1, 'startTime': 1, 'progressState': 1, '_id': 1 }", background = true)
//        ,//Please check previous index which has same fields but ordering is different
//        @CompoundIndex(name = "batchIds_1_startTime_1_endTime_1_state_1", def = "{'batchIds': 1, 'startTime': 1, 'endTime': 1, 'state': 1}", background = true)
})
@Getter
@Setter
@Builder
@AllArgsConstructor
public class OTFSession extends AbstractMongoStringIdEntity {

    private OTFSessionContextType sessionContextType;
    private String sessionContextId;
    private Set<String> batchIds;
    @Indexed(background = true)
    private String title;
    private Long boardId;// subject
    private String description;
    private String presenter;
    private Set<Long> taIds;
    private Long startTime;
    private Long startedAt;

    @Indexed(background = true)
    private Long endTime;
    private String sessionURL;
    private String meetingId;
    private List<String> replayUrls;

    private EntityType type;
    private Set<EntityTag> entityTags = new HashSet<>();
    private Set<SessionLabel> labels = new HashSet<>();
    private OTMSessionType otmSessionType;
    private OTFSessionToolType sessionToolType;
    private OTMSessionConfig otmSessionConfig;
    private Set<OTFSessionFlag> flags = new HashSet<>();

    private SessionState state;
    private OTMSessionInProgressState progressState;

    private String rescheduledFromId;
    @Indexed(background = true)
    private List<String> attendees;
    private String organizerAccessToken;
    private String presenterUrl;
    private String remarks;
    private Boolean teacherJoined;
    private Long teacherJoinTime;
    private List<RescheduleData> rescheduleData = new ArrayList<>();
    private String cancelledBy; // needs to be used to cancel sessions
    private String vimeoId;
    private String forfeitedBy;
    private List<ContentInfo> preSessionContents;
    private List<ContentInfo> postSessionContents;
    private String ownerId;
    private String ownerComment;
    private Pollsdata pollsdata;
    private String adminJoined;
    private String techSupport;
    private String finalIssueStatus;
    private String deletedBy;

    @Deprecated
    private List<SessionPagesMetadata> pagesMetaData;

    private String nodeServerUrl;
    private String nodeServerPort;
    private Long agoraStartTime;
    private String parentSessionId;
    private String agoraReplayUrl;
    private CanvasStreamingType canvasStreamingType = CanvasStreamingType.VEDANTU; // data streaming type
    private OTMServiceProvider serviceProvider = OTMServiceProvider.AGORA;
    private String socialVidConfResp;
    private Long recordingStartTime;
    private Long recordedTill;
    private Long endedBy;
    @Indexed(background = true, direction = IndexDirection.DESCENDING)
    private Long endedAt;

    private List<String> curricullumTopics;
    private List<String> baseTreeTopics;
    private String webinarId;
    //TODO
//    "isConferenceDeleted" : true,
// "migratedToS3" : true
    //post session insights data
    private Integer pollCount;
    private Integer quizCount;
    private Integer hotspotCount;
    private Integer noticeCount;
    private Integer chatCount;
    private Integer uniqueStudentAttendants;
    private Integer totalDoubtsAsked;
    private Integer totalDoubtsResolved;
    private Integer averageStudentDuration;//millis
    private Integer sessionDuration;//millis
    private String primaryBatchId;
    private String agoraVimeoId;
    private String randomUniqueIndex = SessionState.SCHEDULED.name();
    private Long sessionStartedBy;

    public String getPrimaryBatchId() {
        return primaryBatchId;
    }

    public void setPrimaryBatchId(String primaryBatchId) {
        this.primaryBatchId = primaryBatchId;
    }

    public OTFSession() {
        super();
    }

    public static OTFSession createWebinarSession(OTFWebinarSession webinarSession, OTFWebinarSessionReq req) {
        OTFSession otfSession = new OTFSession();
        otfSession.title = webinarSession.getTitle();
        otfSession.description = webinarSession.getDescription();
        otfSession.presenter = webinarSession.getPresenter();
        otfSession.startTime = webinarSession.getStartTime();
        otfSession.endTime = webinarSession.getEndTime();
        otfSession.sessionToolType = webinarSession.getSessionToolType();
        otfSession.state = SessionState.SCHEDULED;
        otfSession.sessionContextId = req.getBundleId();
        otfSession.type = EntityType.STANDARD;
        otfSession.addLabels(SessionLabel.WEBINAR);
        return otfSession;
    }

    public static OTFSession createWebinarSession(AddWebinarSessionReq webinarSession) {
        OTFSession otfSession = new OTFSession();
        otfSession.title = webinarSession.getTitle();
        otfSession.presenter = webinarSession.getPresenter().toString();
        otfSession.taIds = Optional.ofNullable(webinarSession.getTaIds()).orElseGet(HashSet::new)
                .stream().filter(Objects::nonNull).collect(Collectors.toSet());
        otfSession.startTime = webinarSession.getStartTime();
        otfSession.endTime = webinarSession.getEndTime();
        otfSession.sessionToolType = webinarSession.getSessionToolType();
        otfSession.type = webinarSession.getType() == null ? EntityType.STANDARD : webinarSession.getType();
        otfSession.state = SessionState.SCHEDULED;
        otfSession.addLabels(SessionLabel.WEBINAR);
        if (ArrayUtils.isNotEmpty(webinarSession.getEntityTags())) {
            otfSession.addEntityTags(webinarSession.getEntityTags().toArray(new EntityTag[0]));
        }
        if (ArrayUtils.isNotEmpty(webinarSession.getLabels())) {
            otfSession.addLabels(webinarSession.getLabels().toArray(new SessionLabel[0]));
        }
        return otfSession;
    }

    public static OTFSession createEarlyLearningSession(AddEarlyLearningSessionReq earlyLearningSession) {
        OTFSession otfSession = new OTFSession();
        otfSession.presenter = earlyLearningSession.getPresenter();
        otfSession.startTime = earlyLearningSession.getStartTime();
        otfSession.endTime = earlyLearningSession.getEndTime();
        otfSession.sessionToolType = earlyLearningSession.getSessionToolType();
        otfSession.otmSessionType = earlyLearningSession.getOtmSessionType();
        otfSession.type = EntityType.ONE_TO_ONE;
        otfSession.state = SessionState.SCHEDULED;
        otfSession.addLabels(SessionLabel.WEBINAR);
        return otfSession;
    }

    public static OTFSession createSalesDemoSession(AddSalesDemoSessionReq salesDemoSession) {
        OTFSession otfSession = new OTFSession();
        otfSession.presenter = salesDemoSession.getPresenter();
        otfSession.startTime = salesDemoSession.getStartTime();
        otfSession.endTime = salesDemoSession.getEndTime();
        otfSession.sessionToolType = salesDemoSession.getSessionToolType();
        otfSession.otmSessionType = salesDemoSession.getOtmSessionType();
        otfSession.state = SessionState.SCHEDULED;
        otfSession.addLabels(SessionLabel.WEBINAR);
        return otfSession;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = Optional.ofNullable(taIds).orElseGet(HashSet::new)
                .stream().filter(Objects::nonNull).collect(Collectors.toSet());
    }

    public Set<String> getBatchIds() {
        if (batchIds == null) {
            return new HashSet<>();
        }
        return batchIds;
    }

    public void addBatchId(String batchId) {
        if (!StringUtils.isEmpty(batchId)) {
            if (batchIds == null) {
                batchIds = new HashSet<>();
            }
            batchIds.add(batchId);
        }
    }

    public void setSessionURL(String sessionURL) {
        this.sessionURL = sessionURL;
        if (StringUtils.isEmpty(sessionURL)) {
            this.meetingId = null;
        }
    }

    public List<String> getAttendees() {
        if (attendees == null) {
            return new ArrayList<>();
        }
        return attendees;
    }

    public void addAttendees(Set<String> attendees) {
        if (!CollectionUtils.isEmpty(attendees)) {
            if (this.attendees == null) {
                this.attendees = new ArrayList<>(attendees);
            } else {
                attendees.addAll(this.attendees);
                this.attendees = new ArrayList<>(attendees);
            }
        }
    }

    public void updateAttendees(Set<String> attendees) {
        this.attendees = new ArrayList<>();

        if (!CollectionUtils.isEmpty(attendees)) {
            this.attendees.addAll(attendees);
        }
    }

    public void updateAttendee(String studentId, EntityStatus entityStatus) {
        if (this.attendees == null) {
            this.attendees = new ArrayList<>();
        }

        if (EntityStatus.INACTIVE.equals(entityStatus) && this.attendees.contains(studentId)) {
            this.attendees.remove(studentId);
        }
        if (!EntityStatus.INACTIVE.equals(entityStatus) && !this.attendees.contains(studentId)) {
            this.attendees.add(studentId);
        }
    }

    public boolean containsWhiteboardDataMigrated() {
        return flags.contains(OTFSessionFlag.WHITEBOARD_DATA_MIGRATED);
    }

    public void addWhiteboardDataMigrated(boolean whiteboardDataMigrated) {
        if (whiteboardDataMigrated) {
            flags.add(OTFSessionFlag.WHITEBOARD_DATA_MIGRATED);
        } else {
            flags.remove(OTFSessionFlag.WHITEBOARD_DATA_MIGRATED);
        }
    }

    public boolean containsFlag(OTFSessionFlag flag) {
        return flag != null && this.flags != null && this.flags.contains(flag);
    }

    public void addFlag(OTFSessionFlag flag) {
        if (flag != null) {
            this.flags.add(flag);
        }
    }

    public void addFlags(List<OTFSessionFlag> flags) {
        for (OTFSessionFlag flag : flags) {
            if (flag != null) {
                this.addFlag(flag);
            }
        }
    }

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
//        if (ArrayUtils.isEmpty(batchIds)) {
//            errors.add(Constants.BATCH_IDS);
//        }

        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }

        if (StringUtils.isEmpty(startTime)) {
            errors.add(Constants.START_TIME);
        }

        if (StringUtils.isEmpty(endTime)) {
            errors.add(Constants.END_TIME);
        }

        if (this.state == null) {
            errors.add(Constants.STATE);
        }

        if (!OTFSessionManager.SUPPORTED_SESSION_TOOL_TYPES.contains(this.sessionToolType)) {
            errors.add(Constants.SESSION_TOOL_TYPE);
        }

        if (StringUtils.isEmpty(this.getPresenter())) {
            errors.add(Constants.PRESENTER);
        }

        errors.addAll(validateDeprecated());

        return errors;
    }

    public List<String> validateDeprecated() {
        List<String> errors = new ArrayList<>();

        // TODO remove after checking for few days. (migration done on 1603305000000L)
        if (this.getCreationTime() != null && this.getCreationTime() < 1603305000000L) {
            return errors;
        }

        if (this.sessionToolType != null && this.sessionToolType.isUnsupported()) {
            errors.add("using deprecated session tool type");
        }

        if (this.getType() != null && this.getType().isUnsupported()) {
            errors.add("deprecated entity type " + this.getType());
        }
        if (this.getLabels().contains(SessionLabel.WEBINAR) && !this.getEntityTags().contains(EntityTag.WEBINAR)) {
            errors.add("entityTags should include WEBINAR");
        }
        if (!this.getLabels().contains(SessionLabel.WEBINAR) && this.getEntityTags().contains(EntityTag.WEBINAR)) {
            errors.add("labels should include WEBINAR");
        }
        if (this.getOtmSessionType() != null && this.getOtmSessionType().isUnsupported()) {
            errors.add("deprecated otm session type " + this.getOtmSessionType());
        }
        Set<OTFSessionFlag> unsupportedFlags = Optional.ofNullable(this.getFlags()).orElseGet(HashSet::new)
                .stream()
                .filter(OTFSessionFlag::isUnsupported)
                .collect(Collectors.toSet());
        if (!unsupportedFlags.isEmpty()) {
            errors.add("deprecated flags " + unsupportedFlags);
        }
        return errors;
    }

    public void updateGTMDetails(GTMCreateSessionRes res) {
        this.sessionURL = res.getJoinURL();
        this.meetingId = res.getMeetingid();
    }

    public void updateWebinarSession(OTFWebinarSession webinarSession) {
        // Ignores Start time, end time updates and the session tool type
        // updates
        if (webinarSession != null) {
            if (!StringUtils.isEmpty(webinarSession.getTitle())) {
                this.title = webinarSession.getTitle();
            }
            this.description = webinarSession.getDescription();
            this.presenter = webinarSession.getPresenter();
        }
    }

    public void rescheduleToNewStartTime(long newStartTime, long newEndTime) {
        // Add reschedule data
        this.rescheduleData.add(
                new RescheduleData(Long.parseLong(DBUtils.getCallingUserId()), null, this.remarks, this.startTime));

        // Update the timings
        long duration = this.endTime - this.startTime;
        this.startTime = newStartTime;
        if ((System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_DAY) < newStartTime) {
            this.endTime = newEndTime;
        } else {
            this.endTime = this.startTime + duration;
        }
    }

    public Integer getPollCount() {
        if (pollCount == null) {
            return 0;
        }
        return pollCount;
    }

    public Integer getQuizCount() {
        if (quizCount == null) {
            return 0;
        }
        return quizCount;
    }

    public Integer getHotspotCount() {
        if (hotspotCount == null) {
            return 0;
        }
        return hotspotCount;
    }

    public Set<SessionLabel> getLabels() {
        return Optional.ofNullable(this.labels).orElseGet(HashSet::new);
    }

    public Set<EntityTag> getEntityTags() {
        return Optional.ofNullable(this.entityTags).orElseGet(HashSet::new);
    }

    public void addLabels(SessionLabel... labels) {
        this.labels.addAll(Arrays.asList(labels));

        if (this.labels.contains(SessionLabel.WEBINAR) && !this.getEntityTags().contains(EntityTag.WEBINAR)) {
            addEntityTags(EntityTag.WEBINAR);
        }
    }

    public void addEntityTags(EntityTag... tags) {
        this.entityTags.addAll(Arrays.asList(tags));
        if (!this.labels.contains(SessionLabel.WEBINAR) && this.getEntityTags().contains(EntityTag.WEBINAR)) {
            addLabels(SessionLabel.WEBINAR);
        }
    }

    public void setLabels(Set<SessionLabel> labels) {
        this.labels = labels == null ? new HashSet<>() : labels;
        if (this.labels.contains(SessionLabel.WEBINAR) && !this.getEntityTags().contains(EntityTag.WEBINAR)) {
            addEntityTags(EntityTag.WEBINAR);
        }
    }

    public void setEntityTags(Set<EntityTag> entityTags) {
        this.entityTags = entityTags == null ? new HashSet<>() : entityTags;
        if (!this.labels.contains(SessionLabel.WEBINAR) && this.getEntityTags().contains(EntityTag.WEBINAR)) {
            addLabels(SessionLabel.WEBINAR);
        }
    }

    /**
     * Any chages made here needs to be added in {@link com.vedantu.session.pojo.OTFSessionPojoUtils#hasVedantuWaveFeatures}
     *
     * @return
     */
    public boolean hasVedantuWaveFeatures() {
        return OTFSessionToolType.VEDANTU_WAVE == this.getSessionToolType() ||
                OTFSessionToolType.VEDANTU_WAVE_BIG_WHITEBOARD == this.getSessionToolType() ||
                OTFSessionToolType.VEDANTU_WAVE_OTO == this.getSessionToolType() ||
                EntityType.ONE_TO_ONE == this.getType() ||
                Optional.ofNullable(this.getEntityTags()).orElseGet(HashSet::new).contains(EntityTag.BIG_WHITE_BOARD);
    }

    public boolean hasBigWhiteBoardFeature() {
        return OTFSessionToolType.VEDANTU_WAVE_BIG_WHITEBOARD == this.getSessionToolType() ||
                Optional.ofNullable(this.getEntityTags()).orElseGet(HashSet::new).contains(EntityTag.BIG_WHITE_BOARD);
    }

    public boolean isEarlyLearningSession() {
        return getLabels().contains(SessionLabel.SUPER_CODER) || getLabels().contains(SessionLabel.SUPER_READER);
    }

    public boolean isWebinarSession() {
        return getLabels().contains(SessionLabel.WEBINAR) || getSessionContextType() == OTFSessionContextType.WEBINAR;
    }

    public boolean isOtoSession() {
        return this.getSessionToolType() == OTFSessionToolType.VEDANTU_WAVE_OTO ||
                this.getType() == EntityType.ONE_TO_ONE;
    }

    public boolean isExtraTypeSession() {
        return OTMSessionType.EXTRA_REGULAR == this.getOtmSessionType() || OTMSessionType.EXTRA_TRIAL == this.getOtmSessionType();
    }

    public boolean isSalesDemoSession() {
        return this.containsFlag(OTFSessionFlag.SALES_DEMO) || this.otmSessionType == OTMSessionType.SALES_DEMO;
    }

    public boolean isSimLiveSession() {
        return OTMSessionType.SIM_LIVE == this.getOtmSessionType() || this.getEntityTags().contains(EntityTag.SIMULATED_LIVE);
    }

    public boolean isAbruptEnded() {
        return OTMSessionType.ABRUPT_ENDING == this.getOtmSessionType() ||
                Optional.ofNullable(this.getFlags()).orElseGet(HashSet::new).contains(OTFSessionFlag.ABRUPT_ENDING);
    }

    public boolean isBigWhiteBoard() {
        return this.getEntityTags().contains(EntityTag.BIG_WHITE_BOARD);
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String BATCH_IDS = "batchIds";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String MEETING_ID = "meetingId";
        public static final String PRESENTER = "presenter";
        public static final String ATTENDEES = "attendees";
        public static final String STATE = "state";
        public static final String TYPE = "type";
        public static final String SESSION_TOOL_TYPE = "sessionToolType";
        public static final String ORGANIZER_ACCESS_TOKEN = "organizerAccessToken";
        public static final String OTF_SESSION_CONTEXT_TYPE = "sessionContextType";
        public static final String OTF_SESSION_CONTEXT_ID = "sessionContextId";
        public static final String TITLE = "title";
        public static final String RESCHEDULE_DATA = "rescheduleData";
        public static final String TEACHER_JOIN_TIME = "teacherJoinTime";
        public static final String TEACHER_JOINED = "teacherJoined";
        public static final String CANCELLED_BY = "cancelledBy";
        public static final String BOARD_ID = "boardId";
        public static final String REPLAY_URLS = "replayUrls";
        public static final String FLAGS = "flags";
        public static final String OTMSESSION_TYPE = "otmSessionType";
        public static final String POLLS_DATA = "pollsdata";
        public static final String OTM_SESSION_CONFIG = "otmSessionConfig";
        public static final String PROGRESS_STATE = "progressState";
        public static final String ENDED_AT = "endedAt";
        public static final String TA_IDS = "taIds";
        public static final String UNIQUE_STUDENT_ATTENDANCE = "uniqueStudentAttendants";
        public static final String REMARKS = "remarks";
        public static final String CANVAS_STREAMING_TYPE = "canvasStreamingType";
        public static final String VIMEO_ID = "vimeoId";
        public static final String AGORA_VIMEO_ID = "agoraVimeoId";
        public static final String PRIMARY_BATCH_ID = "primaryBatchId";
        public static final String SESSION_CONTEXT_TYPE = "sessionContextType";
        public static final String SESSION_LABELS = "labels";
        public static final String ENTITY_TAGS = "entityTags";
        public static final String RANDOM_UNIQUE_INDEX = "randomUniqueIndex";
        public static final String NOTICE_COUNT = "noticeCount";
        public static final String HOTSPOT_COUNT = "hotspotCount";
        public static final String QUIZ_COUNT = "quizCount";
        public static final String RECORDING_START_TIME = "recordingStartTime";
        public static final String PAGES_META_DATA = "pagesMetaData";
    }
}
