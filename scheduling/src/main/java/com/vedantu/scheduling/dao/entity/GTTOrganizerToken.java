/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.dao.entity;

import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author pranavm
 */
@Document(collection = "GTTOrganizerToken")
public class GTTOrganizerToken extends AbstractMongoStringIdEntity{
    
    private String organizerKey;
    private String organizerAccessToken;
    private String refreshToken;
    private String consumerKey;
    private String consumerSecret;
    private String encodedString;
    @Indexed(background = true)
    private String seatValue;
    private OTFSessionToolType toolType;
    private Long expiresIn;

    public String getOrganizerKey() {
        return organizerKey;
    }

    public void setOrganizerKey(String organizerKey) {
        this.organizerKey = organizerKey;
    }

    public String getOrganizerAccessToken() {
        return organizerAccessToken;
    }

    public void setOrganizerAccessToken(String organizerAccessToken) {
        this.organizerAccessToken = organizerAccessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getSeatValue() {
        return seatValue;
    }

    public void setSeatValue(String seatValue) {
        this.seatValue = seatValue;
    }

    public OTFSessionToolType getToolType() {
        return toolType;
    }

    public void setToolType(OTFSessionToolType toolType) {
        this.toolType = toolType;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {


        public static final String ORGANIZER_ACCESS_TOKEN = "organizerAccessToken";
        public static final String ORGANIZER_KEY = "organizerKey";
        public static final String SEAT_VALUE = "seatValue";
        public static final String TOOL_TYPE = "toolType";
        public static final String EXPIRES_IN = "expiresIn";
        
    }

    @Override
    public String toString() {
        return "GTTOrganizerToken{" + "organizerKey=" + organizerKey + ", organizerAccessToken=" + organizerAccessToken + ", refreshToken=" + refreshToken + ", consumerKey=" + consumerKey + ", consumerSecret=" + consumerSecret + ", encodedString=" + encodedString + ", seatValue=" + seatValue + ", sessionToolType=" + toolType + '}';
    }
    
    
    
}
