package com.vedantu.scheduling.dao.serializers;

import com.google.common.collect.Lists;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Field;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.vedantu.User.Role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.dinero.pojo.FailedPayoutInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.dao.entity.Session;
import com.vedantu.scheduling.pojo.SessionReportAggregation;
import com.vedantu.scheduling.pojo.TotalSessionDuration;
import com.vedantu.scheduling.request.session.GetSessionsByCreationTime;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.scheduling.request.session.GetUserCalendarSessionsReq;
import com.vedantu.scheduling.request.session.GetUserLatestActiveSessionReq;
import com.vedantu.scheduling.request.session.GetUserPastSessionReq;
import com.vedantu.scheduling.request.session.GetUserUpcomingSessionReq;
import com.vedantu.scheduling.response.session.CounselorDashboardSessionsInfoRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSortType;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SessionModel;
import org.bson.Document;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

@Service
public class SessionDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionDAO.class);

    @Autowired
    private CounterService counterService;

    public static final SessionSortType DEFAULT_SESSION_SORT_TYPE = SessionSortType.START_TIME_DESC;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public SessionDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void insert(Session session) {
        insert(session, null);
    }

    public void insert(Session session, String callingUserId) {
        try {
            // Create session
            if (session.getId() == null || session.getId() <= 0l) {
                session.setId(counterService.getNextSequence(Session.class.getSimpleName()));
            }
            saveEntity(session, callingUserId);
        } catch (Exception ex) {
            throw new RuntimeException("update : Error inserting the session " + session.toString(), ex);
        }
    }

    public void insertAll(Collection<Session> sessions) {
        insertAll(sessions, null);
    }

    public void insertAll(Collection<Session> sessions, String callingUserId) {
        try {
            // Create Sessions
            for (Session session : sessions) {
                if (session.getId() == null || session.getId() <= 0l) {
                    session.setId(counterService.getNextSequence(Session.class.getSimpleName()));
                }
                saveEntity(session, callingUserId);
            }
        } catch (Exception ex) {
            throw new RuntimeException("update : Error inserting the session " + Arrays.toString(sessions.toArray()),
                    ex);
        }
    }

    public void update(Session session, Long callingUserId) {
        try {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            // session.setEntityDefaultProperties();
            saveEntity(session, callingUserIdString);
        } catch (Exception ex) {
            throw new RuntimeException("update : Error updating the session " + session.toString(), ex);
        }
    }

    public Session getById(Long id) {
        Session session = null;
        try {
            // TODO : Need to debug why getEntityById is not working.
            Query query = new Query();
            query.addCriteria(Criteria.where(Session.Constants.ID).is(id));
            List<Session> results = runQuery(query, Session.class);
            if (ArrayUtils.isNotEmpty(results)) {
                session = results.get(0);
            }
        } catch (Exception ex) {
            throw new RuntimeException("getById : Error fetch the session with id " + id, ex);
        }
        return session;
    }


    public List<Session> getSessionsByContextId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Session.Constants.CONTEXT_ID).is(id));
        query.addCriteria(Criteria.where(Session.Constants.START_TIME).gt(System.currentTimeMillis()));
        logger.info("getSessionsByContextId" + query);
        return runQuery(query, Session.class);
    }

    public List<Session> getSessions(GetSessionsReq getSessionsReq) {

        Query query = getSessionsReqQuery(getSessionsReq, true);

        // Set start and limit
        setFetchParameters(query, getSessionsReq);

        // Set sorting
        SessionSortType sessionSortType = getSessionsReq.getSortType();
        if (sessionSortType == null) {
            sessionSortType = DEFAULT_SESSION_SORT_TYPE;
        }
        switch (sessionSortType) {
            case START_TIME_ASC:
                query.with(Sort.by(Sort.Direction.ASC, Session.Constants.START_TIME));
                break;
            case START_TIME_DESC:
                query.with(Sort.by(Sort.Direction.DESC, Session.Constants.START_TIME));
                break;
            default:
                break;
        }

        // RUN query RUN
        return runQuery(query, Session.class);
    }

    public Session getOneSession(GetSessionsReq getSessionsReq) {

        Query query = getSessionsReqQuery(getSessionsReq, false);

        // Set sorting
        SessionSortType sessionSortType = getSessionsReq.getSortType();
        if (sessionSortType == null) {
            sessionSortType = DEFAULT_SESSION_SORT_TYPE;
        }
        switch (sessionSortType) {
            case START_TIME_ASC:
                query.with(Sort.by(Sort.Direction.ASC, Session.Constants.START_TIME));
                break;
            case START_TIME_DESC:
                query.with(Sort.by(Sort.Direction.DESC, Session.Constants.START_TIME));
                break;
            default:
                break;
        }

        // RUN query RUN
        return findOne(query, Session.class);
    }

    public long getSessionsCount(GetSessionsReq getSessionsReq) {

        Query query = getSessionsReqQuery(getSessionsReq, true);

        // RUN query RUN
        return queryCount(query, Session.class);
    }

    public List<Session> getUserSessionsForDuration(GetUserUpcomingSessionReq req)
            throws VException {
        if (req.getEndTime() == null || req.getEndTime() <= 0 || req.getStartTime() == null || req.getStartTime() <= 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start time and end time are mandatory");
        }
        Query query = new Query();
        switch (req.getCallingUserRole()) {
            case TEACHER:
                query.addCriteria(
                        Criteria.where(Session.Constants.TEACHER_ID).is(req.getCallingUserId()));
                break;
            case STUDENT:
                query.addCriteria(
                        Criteria.where(Session.Constants.STUDENT_IDS).in(req.getCallingUserId()));
                break;
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "Action not allowed for this role : " + req.toString());
        }

        if (!StringUtils.isEmpty(req.getQuery())) {
            int length = req.getQuery().length();
            int index = Math.min(length, 30);
            query.addCriteria(Criteria.where(Session.Constants.TITLE).regex(req.getQuery().substring(0, index), "i"));
        }

        Long startTime = req.getStartTime();
        Long endTime = req.getEndTime();
        if (startTime != null && startTime > 0 && endTime != null && endTime > 0 && startTime <= endTime) {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(startTime).lte(endTime));
        }
        if (!CollectionUtils.isEmpty(req.getSessionStates())) {
            query.addCriteria(
                    Criteria.where(Session.Constants.SESSION_STATE).in(req.getSessionStates()));
        }


        long currentTime = System.currentTimeMillis();

        List<Session> sessions = new ArrayList<>();
        if (startTime != null && endTime != null &&
                startTime <= currentTime && endTime >= currentTime) {

            try {
                int start = req.getStart() == null ? 0 : req.getStart();
                int size = req.getSize() == null ? 20 : req.getSize();
                List<Serializable> conditions = Arrays.asList(new Document("$and", Arrays.asList(
                        new Document("$lte", Arrays.asList("$" + Session.Constants.START_TIME, currentTime)),
                        new Document("$gte", Arrays.asList("$" + Session.Constants.END_TIME, currentTime)))), 0, 1);
                List<Bson> bsons = Arrays.asList(
                        new Document("$match", getParsedQueryDocument(query, Session.class)),
                        Aggregates.addFields(new Field<>("live_sessions", new Document("$cond", conditions))),
                        Aggregates.sort(Sorts.ascending("live_sessions", Session.Constants.START_TIME)),
                        Aggregates.skip(start), Aggregates.limit(size)
                );
                sessions = getSessionDurtaionAggregation(bsons);
                logger.info("session aggreagation query for get sesions for duration {}, {}, {} ", size, start, bsons);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            // sorting in asc order on endtime
            List<Sort.Order> orderList = new ArrayList<>();
            orderList.add(new Sort.Order(Sort.Direction.ASC, Session.Constants.END_TIME));
            query.with(Sort.by(orderList));
            setFetchParameters(query, req);
            logger.info("session query for get sesions for duration {}, {}, {} ", req.getStart(), req.getSize(), query);
            sessions = runQuery(query, Session.class);
        }
        return sessions;
    }

    public List<Session> getSessionDurtaionAggregation(List<Bson> bsons) {
        AggregateIterable<Document> documents = getMongoOperations()
                .getCollection(Session.class.getSimpleName())
                .aggregate(bsons)
                .allowDiskUse(true)
                .maxTime(15, TimeUnit.SECONDS);
        ArrayList<Session> objects = Lists.newArrayList();
        for (Document document : documents) {
            logger.info("aggregated doc " + document);
            objects.add(convert(document, Session.class));
        }
        return objects;
    }

    public List<Session> getUserUpcomingSessions(GetUserUpcomingSessionReq req)
            throws VException {
        Query query = new Query();
        switch (req.getCallingUserRole()) {
            case TEACHER:
                query.addCriteria(
                        Criteria.where(Session.Constants.TEACHER_ID).is(req.getCallingUserId()));
                break;
            case STUDENT:
                query.addCriteria(
                        Criteria.where(Session.Constants.STUDENT_IDS).in(req.getCallingUserId()));
                break;
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "Action not allowed for this role : " + req.toString());
        }

        if (!StringUtils.isEmpty(req.getQuery())) {
            int length = req.getQuery().length();
            int index = Math.min(length, 30);
            query.addCriteria(Criteria.where(Session.Constants.TITLE).regex(req.getQuery().substring(0, index), "i"));
        }

        Long startTime = req.getStartTime();
        Long endTime = req.getEndTime();
        long currentTime = System.currentTimeMillis();
        if (startTime != null && startTime > 0 && endTime != null && endTime > 0 && startTime <= endTime) {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(currentTime).gte(startTime).lte(endTime));
        } else {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(currentTime));
        }
        if (!CollectionUtils.isEmpty(req.getSessionStates())) {
            query.addCriteria(
                    Criteria.where(Session.Constants.SESSION_STATE).in(req.getSessionStates()));
        }

        // sorting in asc order on endtime
        query.with(Sort.by(Sort.Direction.ASC, Session.Constants.END_TIME));
        setFetchParameters(query, req);
        logger.info("get oto session query {}", query);
        return runQuery(query, Session.class);
    }

    public List<Session> getUserPastSessions(GetUserPastSessionReq getUserPastSessionReq, Role role) throws VException {
        Query query = new Query();

        if (Role.STUDENT.equals(role)) {
            query.addCriteria(Criteria.where(Session.Constants.STUDENT_IDS).is(getUserPastSessionReq.getUserId()));
        } else if (Role.TEACHER.equals(role)) {
            query.addCriteria(Criteria.where(Session.Constants.TEACHER_ID).is(getUserPastSessionReq.getUserId()));
        }

        if (!StringUtils.isEmpty(getUserPastSessionReq.getQuery())) {
            int length = getUserPastSessionReq.getQuery().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(Session.Constants.TITLE).regex(getUserPastSessionReq.getQuery().substring(0, index), "i"));
        }

        if (ArrayUtils.isNotEmpty(getUserPastSessionReq.getSessionStates())) {
            query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).in(getUserPastSessionReq.getSessionStates()));
        }

        Long startTime = getUserPastSessionReq.getStartTime();
        Long endTime = getUserPastSessionReq.getEndTime();
        long currentTime = System.currentTimeMillis();
        if (startTime != null && startTime > 0 && endTime != null && endTime > 0 && startTime <= endTime) {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(startTime).lte(endTime).lt(currentTime));
        } else {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).lt(currentTime));
        }
        // sorting in asc order on endtime
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, Session.Constants.END_TIME));
        query.with(Sort.by(orderList));
        setFetchParameters(query, getUserPastSessionReq);
        return runQuery(query, Session.class);
    }

    public List<Session> getUserLatestActiveSessions(GetUserLatestActiveSessionReq getUserLatestActiveSessionReq)
            throws VException {
        Query query = new Query();
        switch (getUserLatestActiveSessionReq.getCallingUserRole()) {
            case TEACHER:
                query.addCriteria(
                        Criteria.where(Session.Constants.TEACHER_ID).is(getUserLatestActiveSessionReq.getCallingUserId()));
                break;
            case STUDENT:
                query.addCriteria(
                        Criteria.where(Session.Constants.STUDENT_IDS).in(getUserLatestActiveSessionReq.getCallingUserId()));
                break;
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "Action not allowed for this role : " + getUserLatestActiveSessionReq.toString());
        }

        // Fetch only scheduled, active, started sessions
        List<SessionState> sessionStates = new ArrayList<>();
        sessionStates.add(SessionState.SCHEDULED);
        sessionStates.add(SessionState.ACTIVE);
        sessionStates.add(SessionState.STARTED);
        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).in(sessionStates));
        query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(System.currentTimeMillis()));

        // sorting in asc order on endtime
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.ASC, Session.Constants.START_TIME));
        query.with(Sort.by(orderList));
        setFetchParameters(query, getUserLatestActiveSessionReq);
        return runQuery(query, Session.class);
    }

    public List<Session> getUserCalendarSessions(GetUserCalendarSessionsReq getUserCalendarSessionsReq)
            throws VException {
        Query query = new Query();
        switch (getUserCalendarSessionsReq.getCallingUserRole()) {
            case TEACHER:
                query.addCriteria(
                        Criteria.where(Session.Constants.TEACHER_ID).is(getUserCalendarSessionsReq.getCallingUserId()));
                break;
            case STUDENT:
                query.addCriteria(
                        Criteria.where(Session.Constants.STUDENT_IDS).in(getUserCalendarSessionsReq.getCallingUserId()));
                break;
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "Action not allowed for this role : " + getUserCalendarSessionsReq.toString());
        }

        // Fetch only scheduled, active, started sessions
        List<SessionState> sessionStates = new ArrayList<>();
        sessionStates.add(SessionState.SCHEDULED);
        sessionStates.add(SessionState.ACTIVE);
        sessionStates.add(SessionState.STARTED);
        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).in(sessionStates));
        query.addCriteria(
                Criteria.where(Session.Constants.START_TIME).gte(getUserCalendarSessionsReq.getAfterStartTime())
                        .lt(getUserCalendarSessionsReq.getBeforeStartTime()));

        // Set sort and fetch limits
        query.with(Sort.by(Sort.Direction.ASC, Session.Constants.START_TIME));
        setFetchParameters(query, getUserCalendarSessionsReq);
        return runQuery(query, Session.class);
    }

    public List<Session> getSubscriptionSessions(Long subscriptionId, Integer start, Integer limit) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Session.Constants.SUBSCRIPTION_ID).is(subscriptionId));

        // Return all the sessions under the subsription if the bounderies are
        // not set.
        setFetchParameters(query, start, limit);
        return runQuery(query, Session.class);
    }

    public Set<Long> getSessionPartners(Long userId, boolean fetchOnlyEnded, Long beforeTime) {
        Set<Long> result = new LinkedHashSet<>();
        Query query = new Query();

        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, Session.Constants.START_TIME));
        query.with(Sort.by(orderList));
        if (fetchOnlyEnded) {
            query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.ENDED));
        }
        if (beforeTime != null) {
            query.addCriteria(Criteria.where(Session.Constants.START_TIME).lt(beforeTime));
        }
        query.limit(2000);
        query.fields().include(Session.Constants.STUDENT_IDS);
        query.fields().include(Session.Constants.TEACHER_ID);
        Criteria criteria = new Criteria();
        criteria.orOperator(Criteria.where(Session.Constants.STUDENT_IDS).in(userId),
                Criteria.where(Session.Constants.TEACHER_ID).is(userId));
        query.addCriteria(criteria);
        logger.info("query " + query);
        List<Session> sessions = runQuery(query, Session.class);
        int maxResults = 25;
        if (sessions != null) {
            for (Session session : sessions) {
                if (session != null) {
                    if (session.getStudentIds() != null) {
                        result.addAll(session.getStudentIds());
                    }
                    if (session.getTeacherId() != null) {
                        result.add(session.getTeacherId());
                    }
                }
                if (result.size() > (maxResults + 1)) {// +1 for callinguserid
                    break;
                }
            }
        }
        result.remove(userId);
        logger.info("result " + result);
        return result;
    }

    public List<FailedPayoutInfo> getEndedSessionsToday() {
        List<FailedPayoutInfo> result = new ArrayList<>();
        Long tillTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
        Long fromTime = tillTime - DateTimeUtils.MILLIS_PER_DAY;
        Query query = new Query();
        query.fields().include(Session.Constants.ID);
        query.fields().include(Session.Constants.CONTEXT_TYPE);
        query.fields().include(Session.Constants.SUBSCRIPTION_ID);
        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.ENDED));
        query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(fromTime).lte(tillTime));

        List<Session> sessions = runQuery(query, Session.class);

        if (sessions != null) {
            for (Session session : sessions) {
                if (session.getSubscriptionId() != null || session.getContextType() != null) {
                    FailedPayoutInfo failedPayoutInfo = new FailedPayoutInfo();
                    failedPayoutInfo.setSessionId(session.getId());
                    failedPayoutInfo.setContextType(session.getContextType());
                    result.add(failedPayoutInfo);
                }
            }
        }
        return result;
    }

    private Query getSessionsReqQuery(GetSessionsReq getSessionsReq, boolean setFetchParameters) {
        // Create query from the request parameters
        Query query = new Query();
        if (getSessionsReq.getUserId() != null) {
            Criteria criteria = new Criteria();
            criteria.orOperator(Criteria.where(Session.Constants.STUDENT_IDS).in(getSessionsReq.getUserId()),
                    Criteria.where(Session.Constants.TEACHER_ID).is(getSessionsReq.getUserId()));
            query.addCriteria(criteria);
        } else {
            if (getSessionsReq.getStudentId() != null && getSessionsReq.getStudentId() > 0l) {
                query.addCriteria(Criteria.where(Session.Constants.STUDENT_IDS).in(getSessionsReq.getStudentId()));
            }
            if (getSessionsReq.getTeacherId() != null && getSessionsReq.getTeacherId() > 0l) {
                query.addCriteria(Criteria.where(Session.Constants.TEACHER_ID).is(getSessionsReq.getTeacherId()));
            }
        }

        if (!CollectionUtils.isEmpty(getSessionsReq.getSessionStates())) {
            query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).in(getSessionsReq.getSessionStates()));
        }
        if (getSessionsReq.getSubscriptionId() != null && getSessionsReq.getSubscriptionId() > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.SUBSCRIPTION_ID).is(getSessionsReq.getSubscriptionId()));
        }
        if (getSessionsReq.getContextId() != null) {
            query.addCriteria(Criteria.where(Session.Constants.CONTEXT_ID).is(getSessionsReq.getContextId()));
        }

        if (getSessionsReq.getContextType() != null) {
            query.addCriteria(Criteria.where(Session.Constants.CONTEXT_TYPE).is(getSessionsReq.getContextType()));
        }

        if (getSessionsReq.getAfterStartTime() != null && getSessionsReq.getAfterStartTime() > 0l) {
            if (getSessionsReq.getBeforeStartTime() != null && getSessionsReq.getBeforeStartTime() > 0l) {
                query.addCriteria(Criteria.where(Session.Constants.START_TIME).gte(getSessionsReq.getAfterStartTime())
                        .lte(getSessionsReq.getBeforeStartTime()));
            } else {
                query.addCriteria(Criteria.where(Session.Constants.START_TIME).gte(getSessionsReq.getAfterStartTime()));
            }
        } else if (getSessionsReq.getBeforeStartTime() != null && getSessionsReq.getBeforeStartTime() > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.START_TIME).lte(getSessionsReq.getBeforeStartTime()));
        }
        if (getSessionsReq.getAfterEndTime() != null && getSessionsReq.getAfterEndTime() > 0l) {
            if (getSessionsReq.getBeforeEndTime() != null && getSessionsReq.getBeforeEndTime() > 0l) {
                query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(getSessionsReq.getAfterEndTime())
                        .lte(getSessionsReq.getBeforeEndTime()));
            } else {
                query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(getSessionsReq.getAfterEndTime()));
            }
        } else if (getSessionsReq.getBeforeEndTime() != null && getSessionsReq.getBeforeEndTime() > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.END_TIME).lte(getSessionsReq.getBeforeEndTime()));
        }

        if (getSessionsReq.getModel() != null) {
            query.addCriteria(Criteria.where(Session.Constants.SESSION_MODEL).is(getSessionsReq.getModel()));
        }
        if (setFetchParameters) {
            setFetchParameters(query, getSessionsReq.getStart(), getSessionsReq.getSize());
        }
        logger.info("query " + query);
        return query;
    }

    public List<Session> getSessionsByCreationTime(GetSessionsByCreationTime req) {
        Query query = new Query();
        if (req.getStudentId() != null && req.getStudentId() > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.STUDENT_IDS).in(req.getStudentId()));
        }
        if (req.getTeacherId() != null && req.getTeacherId() > 0l) {
            query.addCriteria(Criteria.where(Session.Constants.TEACHER_ID).is(req.getTeacherId()));
        }
        if (req.getSessionModel() != null) {
            query.addCriteria(Criteria.where(Session.Constants.SESSION_MODEL).is(req.getSessionModel()));
        }

        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.ENDED));

        if (null != req.getFromtime()) {
            if (req.getToTime() == null) {
                req.setToTime(System.currentTimeMillis());
            }
            Criteria c = new Criteria().andOperator(Criteria.where(Session.Constants.CREATION_TIME).lt(req.getToTime()),
                    Criteria.where(Session.Constants.CREATION_TIME).gte(req.getFromtime()));
            query.addCriteria(c);
        }

        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.ASC, Session.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));

        logger.info("query " + query);
        List<Session> sessions = runQuery(query, Session.class);
        return sessions;
    }

    public long getTotalSessionDuration(Long afterEndTime, Long beforeEndTime) {
        // LastUpdated if null, consider all the sessions
        if (afterEndTime == null) {
            afterEndTime = 0l;
        }

        if (beforeEndTime == null) {
            beforeEndTime = System.currentTimeMillis();
        }

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(
                Aggregation.match(new Criteria().andOperator(Criteria.where("startedAt").ne(null),
                        Criteria.where("endedAt").ne(null),
                        Criteria.where(Session.Constants.END_TIME).gte(afterEndTime).lte(beforeEndTime),
                        Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.ENDED))),
                project(Session.Constants.SESSION_STATE).and("endedAt").minus("startedAt").as("duration"),
                Aggregation.group(Session.Constants.SESSION_STATE).sum("duration").as("duration"), project("duration")).withOptions(aggregationOptions);
        AggregationResults<TotalSessionDuration> results = getMongoOperations().aggregate(agr, Session.class,
                TotalSessionDuration.class);
        List<TotalSessionDuration> durationResults = results.getMappedResults();
        if (durationResults.size() > 0) {
            return durationResults.get(0).getDuration();
        } else {
            return 0;
        }
    }

    public TotalSessionDuration getTotalSessionDurationForTeacher(Long userId) {
        // LastUpdated if null, consider all the sessions
        if (userId == null) {
            return null;
        }
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(
                Aggregation.match(new Criteria().andOperator(Criteria.where("startedAt").ne(null),
                        Criteria.where("endedAt").ne(null), Criteria.where(Session.Constants.TEACHER_ID).is(userId),
                        Criteria.where(Session.Constants.SESSION_STATE).is(SessionState.ENDED))),
                project(Session.Constants.SESSION_STATE).and("endedAt").minus("startedAt").as("duration"),
                Aggregation.group(Session.Constants.SESSION_STATE).sum("duration").as("duration").count().as("count"),
                project("duration", "count")).withOptions(aggregationOptions);
        AggregationResults<TotalSessionDuration> results = getMongoOperations().aggregate(agr, Session.class,
                TotalSessionDuration.class);
        List<TotalSessionDuration> durationResults = results.getMappedResults();
        if (durationResults.size() > 0) {
            return durationResults.get(0);
        } else {
            return null;
        }
    }

    public Set<Long> getEndedSessionIdsByContext(EntityType contextType, String contextId) {
        Set<Long> sessionIds = new HashSet<>();

        // Construct query
        Query query = new Query();
        query.addCriteria(Criteria.where(Session.Constants.CONTEXT_ID).is(contextId));
        query.addCriteria(Criteria.where(Session.Constants.CONTEXT_TYPE).is(contextType));
        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).in(SessionState.ENDED));
        query.addCriteria(Criteria.where(Session.Constants.SESSION_MODEL).ne(SessionModel.TRIAL));
        query.with(Sort.by(Sort.Direction.ASC, Session.Constants.START_TIME));
        query.fields().include(Session.Constants.ID);

        int start = 0;
        while (true) {
            query.skip(start);
            query.limit(MAX_ALLOWED_FETCH_SIZE);
            logger.info("query to get ended sessions --> "+query);
            List<Session> sessions = runQuery(query, Session.class);
            if (CollectionUtils.isEmpty(sessions)) {
                break;
            }

            sessions.forEach(s -> sessionIds.add(s.getId()));
            if (sessions.size() < MAX_ALLOWED_FETCH_SIZE) {
                break;
            }

            start += MAX_ALLOWED_FETCH_SIZE;
        }

        return sessionIds;
    }

    public List<TotalSessionDuration> getSessionStateDurationByContext(EntityType contextType, String contextId) {
        Criteria upcomingOngoingSessionCriteria = new Criteria();
        upcomingOngoingSessionCriteria.andOperator(Criteria.where(Session.Constants.CONTEXT_ID).is(contextId),
                Criteria.where(Session.Constants.CONTEXT_TYPE).is(contextType),
                Criteria.where(Session.Constants.SESSION_MODEL).ne(SessionModel.TRIAL));

        // Criteria.where(Session.Constants.SESSION_STATE)
        // .in(Arrays.asList(SessionState.SCHEDULED, SessionState.ACTIVE,
        // SessionState.STARTED)));
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(upcomingOngoingSessionCriteria),
                project(Session.Constants.SESSION_STATE).and(Session.Constants.END_TIME)
                        .minus(Session.Constants.START_TIME).as("duration"),
                Aggregation.group("state").sum("duration").as("duration").count().as("count"),
                project("duration").and("_id").as("state")).withOptions(aggregationOptions);

        AggregationResults<TotalSessionDuration> results = getMongoOperations().aggregate(aggregation, Session.class,
                TotalSessionDuration.class);
        logger.info("aggregation query of getSessionStateDurationByContext --> "+aggregation);
        logger.info("results --> "+results);
        return results.getMappedResults();
    }

    public List<Session> getAttendeesNextSessions(Long userId, Role role) {
        Query query = new Query();

        List<SessionState> activeSessionStates = new ArrayList<>(
                Arrays.asList(SessionState.ACTIVE, SessionState.SCHEDULED, SessionState.STARTED));
        query.addCriteria(Criteria.where(Session.Constants.SESSION_STATE).in(activeSessionStates));
        query.addCriteria(Criteria.where(Session.Constants.END_TIME).gte(System.currentTimeMillis()));
        if (Role.TEACHER.equals(role)) {
            query.addCriteria(Criteria.where(Session.Constants.TEACHER_ID).is(userId));
        } else {
            query.addCriteria(Criteria.where(Session.Constants.STUDENT_IDS).in(Arrays.asList(userId)));
        }
        query.with(Sort.by(Direction.ASC, Session.Constants.START_TIME));
        query.limit(3);
        List<Session> results = runQuery(query, Session.class);
        return results;
    }

    public List<Session> getCoursePlanTrialSessionInfo(Set<String> coursePlanIds) {
        List<Session> sessions = new ArrayList<>();
        if (coursePlanIds != null && !coursePlanIds.isEmpty()) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Session.Constants.CONTEXT_ID).in(coursePlanIds));
            query.addCriteria(Criteria.where(Session.Constants.SESSION_MODEL).is(SessionModel.TRIAL));
            sessions = runQuery(query, Session.class);
        }
        return sessions;
    }

    public List<CounselorDashboardSessionsInfoRes> getCouserPlanSessionInfosByContextIds(List<String> contextIds) {
        logger.info("ENTRY: " + contextIds);
        List<String> sessionStates = new ArrayList<>();
        sessionStates.add(SessionState.SCHEDULED.name());
        sessionStates.add(SessionState.ACTIVE.name());
        sessionStates.add(SessionState.ENDED.name());
        sessionStates.add(SessionState.STARTED.name());
        Criteria sessionCriteria = new Criteria();
        sessionCriteria.andOperator(Criteria.where(Session.Constants.CONTEXT_ID).in(contextIds),
                Criteria.where(Session.Constants.SESSION_STATE).in(sessionStates));
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(Aggregation.match(sessionCriteria),
                Aggregation.sort(Sort.Direction.ASC, Session.Constants.START_TIME),
                Aggregation.group(Session.Constants.CONTEXT_ID)
                        .first(Session.Constants.START_TIME).as(CounselorDashboardSessionsInfoRes.Constants.FIRST_SESSION_START_TIME)
                        .last(Session.Constants.END_TIME).as(CounselorDashboardSessionsInfoRes.Constants.LAST_SESSION_END_TIME)
                        .count().as(CounselorDashboardSessionsInfoRes.Constants.TOTAL_NO_OF_SESSIONS)
        ).withOptions(aggregationOptions);
        logger.info("Agrregation :" + agg);
        AggregationResults<CounselorDashboardSessionsInfoRes> groupResults
                = getMongoOperations().aggregate(agg, Session.class.getSimpleName(), CounselorDashboardSessionsInfoRes.class);
        List<CounselorDashboardSessionsInfoRes> result = groupResults.getMappedResults();
        logger.info("EXIT:" + result);
        return result;
    }

    public List<SessionReportAggregation> getSessionReportAggregation(List<String> coursePlanIds) {
        logger.info("ENTRY: getSessionReportAggregation coursePlanIds= " + coursePlanIds);
        List<SessionState> sessionStates = new ArrayList<>(
                Arrays.asList(SessionState.ACTIVE, SessionState.SCHEDULED, SessionState.STARTED, SessionState.ENDED));
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(Aggregation.match(Criteria.where(Session.Constants.CONTEXT_ID).in(coursePlanIds).and(Session.Constants.SESSION_STATE).in(sessionStates)),
                Aggregation.sort(Sort.by(Direction.ASC, Session.Constants.START_TIME)),
                Aggregation.group(Session.Constants.CONTEXT_ID)
                        .count().as(SessionReportAggregation.Constants.TOTAL_SESSION_COUNT)
                        .first(Session.Constants.START_TIME).as(SessionReportAggregation.Constants.FIRST_SESSION_START_TIME)
                        .push(Session.Constants.START_TIME).as(SessionReportAggregation.Constants.START_TIMES)
                        .push(Session.Constants.END_TIME).as(SessionReportAggregation.Constants.END_TIMES)
                        .push(Session.Constants.ID).as(SessionReportAggregation.Constants.SESSION_IDS)
        ).withOptions(aggregationOptions);
        logger.info("getSessionReportAggregation Aggregation= " + agr);
        AggregationResults<SessionReportAggregation> results = getMongoOperations().aggregate(agr, Session.class,
                SessionReportAggregation.class);
        logger.info("getSessionReportAggregation results= " + results);
        return results.getMappedResults();
    }

    public List<SessionReportAggregation> getSessionReportAggregation(List<String> coursePlanIds, List<SessionState> sessionStates) {
        logger.info("ENTRY: getSessionReportAggregation coursePlanIds= " + coursePlanIds + "states=" + sessionStates);
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(Aggregation.match(Criteria.where(Session.Constants.CONTEXT_ID).in(coursePlanIds).and(Session.Constants.SESSION_STATE).in(sessionStates)),
                Aggregation.sort(Sort.by(Direction.ASC, Session.Constants.START_TIME)),
                Aggregation.group(Session.Constants.CONTEXT_ID)
                        .count().as(SessionReportAggregation.Constants.TOTAL_SESSION_COUNT)
                        .push(Session.Constants.ID).as(SessionReportAggregation.Constants.OTO_SESSION_IDS)
        ).withOptions(aggregationOptions);
        logger.info("ENTRY: getSessionReportAggregation Aggregation= " + agr);
        AggregationResults<SessionReportAggregation> results = getMongoOperations().aggregate(agr, Session.class,
                SessionReportAggregation.class);
        logger.info("ENTRY: getSessionReportAggregation results= " + results);
        return results.getMappedResults();
    }

    ;
        
        public List<Session> getSessionsForCoursePlansAndUserId(List<String> coursePlanIds, String userId, int start, int size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Session.Constants.CONTEXT_ID).in(coursePlanIds));
        query.addCriteria(Criteria.where(Session.Constants.STUDENT_IDS).is(userId));

        return runQuery(query, Session.class);

    }

    public Session getWiziqSession(String wizIQclassId)
            throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Session.Constants.WIZ_IQ_CLASS_ID).is(wizIQclassId));
        return findOne(query, Session.class);
    }
}
