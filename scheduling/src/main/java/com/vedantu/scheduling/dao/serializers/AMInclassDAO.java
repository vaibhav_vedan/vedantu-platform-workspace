package com.vedantu.scheduling.dao.serializers;

import java.util.List;

import com.vedantu.onetofew.enums.AttendeeContext;
import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.scheduling.dao.entity.GTTAttendeeDetails;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class AMInclassDAO  extends AbstractMongoDAO{
	
	@Autowired
    private MongoClientFactory mongoClientFactory;    

    @Autowired
    private LogFactory logFactory;
    
    private final Logger logger = logFactory.getLogger(AMInclassDAO.class);
    
    public AMInclassDAO() {
    	super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public List<GTTAttendeeDetails> getAllGTTAttendeeDetailsInRange(final Long studentId, final Long fromTime, final Long thruTime) {
    	Query q = new Query();
    	Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
    	q.addCriteria(andCriteria);
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
    	q.with(Sort.by( Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));
    	logger.info("Logger info :"+q);
    	return runQuery(q, GTTAttendeeDetails.class);
    }
	public List<GTTAttendeeDetails> getAllGTTAttendeeDetailsInRangeForSessions(final Long studentId,List<String> sessionIds, final Long fromTime, final Long thruTime) {
		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).in(sessionIds));
		q.addCriteria(andCriteria);
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		q.with(Sort.by( Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));
		logger.info("Logger info :"+q);
		return runQuery(q, GTTAttendeeDetails.class);
	}

	public List<GTTAttendeeDetails> getAllGTTAttendeeDetailsInRangeForSession(String sessionId, final Long fromTime, final Long thruTime) {
		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.SESSION_ID).is(sessionId));
		q.addCriteria(andCriteria);
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		q.with(Sort.by( Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));
		logger.info("Logger info :"+q);
		return runQuery(q, GTTAttendeeDetails.class);
	}
    
    public List<GTTAttendeeDetails> getAttendedGTTAttendeeDetailsInRange(final Long studentId, final Long fromTime, final Long thruTime) {
    	Query q = new Query();
    	Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
    	q.addCriteria(andCriteria);
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIME_FIRST).gt(0));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
    	logger.info("Logger info :"+q);
    	return runQuery(q, GTTAttendeeDetails.class);
    }
    
    public List<GTTAttendeeDetails> getLastThreeSessionsOfStudent(Long studentId) {
    	Query q = new Query();
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
    	q.with(Sort.by( Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));
    	q.limit(3);
    	logger.info("Logger info :"+q);
    	return runQuery(q, GTTAttendeeDetails.class);
    }
    
    public long countAllSessions(Long studentId, Long fromTime) {
    	Query q = new Query();
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
    	
    	logger.info("Query - " + q);
    	
    	return queryCount(q, GTTAttendeeDetails.class);
    }
    
    public long countAttendedSessions(Long studentId, Long fromTime) {
    	Query q = new Query();
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIME_FIRST).gt(0));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
    	logger.info("Query - " + q);
    	
    	return queryCount(q, GTTAttendeeDetails.class);
    }
    
    public List<GTTAttendeeDetails> getLastThreeAttendedSessionsOfStudent(Long studentId) {
    	Query q = new Query();
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.JOIN_TIME_FIRST).gt(0));
    	q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
    	q.with(Sort.by( Direction.DESC, GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME));
    	q.limit(3);
    	logger.info("Logger info :"+q);
    	return runQuery(q, GTTAttendeeDetails.class);
    }


	public List<OTMSessionEngagementData> getOTMSessionEngagementData(String sessionId) {
		Query q = new Query();
		q.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.SESSIONID).is(sessionId));
		logger.info("Logger info :" + q);
		return runQuery(q, OTMSessionEngagementData.class);
    }

}
