package com.vedantu.scheduling.dao.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class DemoSession extends AbstractMongoStringIdEntity {
	private String courseId;
	private String batchId;
	private String teacherId;
	private String sessionId;
	@JsonProperty
	private List<String> enrolledStudents;
	private EntityStatus status;

	public DemoSession() {
		super();
	}

	public DemoSession(String courseId, String batchId, String teacherId, String sessionId,
			List<String> enrolledStudents, EntityStatus status) {
		super();
		this.courseId = courseId;
		this.batchId = batchId;
		this.teacherId = teacherId;
		this.sessionId = sessionId;
		this.enrolledStudents = enrolledStudents;
		this.status = status;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<String> getEnrolledStudents() {
		return enrolledStudents;
	}

	public void setEnrolledStudents(List<String> enrolledStudents) {
		this.enrolledStudents = enrolledStudents;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "DemoSession [courseId=" + courseId + ", batchId=" + batchId + ", teacherId=" + teacherId
				+ ", sessionId=" + sessionId + ", enrolledStudents=" + enrolledStudents + ", status=" + status + "]";
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String ENROLLED_STUDENTS = "enrolledStudents";
		public static final String COURSE_ID = "courseId";
		public static final String BATCH_ID = "batchId";
		public static final String TEACHER_ID = "teacherId";
		public static final String SESSION_ID = "sessionId";
		public static final String STATUS = "status";
	}

}
