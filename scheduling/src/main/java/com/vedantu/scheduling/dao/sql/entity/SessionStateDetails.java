package com.vedantu.scheduling.dao.sql.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.vedantu.session.pojo.SessionState;

@Entity
@Table(name = "SessionStateDetails")
@Transactional(isolation = Isolation.READ_COMMITTED)
public class SessionStateDetails extends com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity implements Serializable {
	private Long sessionId;
	@Enumerated(EnumType.STRING)
	private SessionState sessionState;

	public SessionStateDetails() {
		super();
	}

	public SessionStateDetails(Long sessionId) {
		super();
		this.sessionId = sessionId;
		this.sessionState = SessionState.SCHEDULED;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public SessionState getSessionState() {
		return sessionState;
	}

	public void setSessionState(SessionState sessionState) {
		this.sessionState = sessionState;
	}

	@Override
	public String toString() {
		return "SessionStateDetails [sessionId=" + sessionId + ", sessionState=" + sessionState + "]";
	}

	public static class Constants extends com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity.Constants {
		public static final String SESSION_ID = "sessionId";
		public static final String SESSION_STATE = "sessionState";
	}
}
