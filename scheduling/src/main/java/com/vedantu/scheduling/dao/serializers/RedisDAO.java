/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.dao.serializers;

import com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.scheduling.controllers.CronController;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * @author jeet
 */
@Service
public class RedisDAO extends AbstractRedisDAO{

    private Logger logger = LogFactory.getLogger(CronController.class);
    private static Gson gson = new Gson();

    public RedisDAO() {
    }

    public void performOps(SNSSubject subject, String message) throws InternalServerErrorException {

        if (StringUtils.isEmpty(message)) {
            logger.warn("message can not be null/empty");
            return;
        }

        switch(subject) {

            case EVICT_EL_TEACHERS:
            case REMOVE_USER_CACHED_SESSIONS:
                Type listType = new TypeToken<List<String>>(){}.getType();
                List<String> cachedKeys = gson.fromJson(message, listType);
                String[] keys = cachedKeys.toArray(new String[]{});

                deleteKeys(keys);
                break;
            default:
                logger.warn("Received a sub req with an invalid subject");
        }
    }
}
