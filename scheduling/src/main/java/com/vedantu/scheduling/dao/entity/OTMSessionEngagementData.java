/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.scheduling.dao.entity;

import com.vedantu.scheduling.enums.wavesession.InteractionType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "OTMSessionEngagementData")
public class OTMSessionEngagementData extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String sessionId;
    private InteractionType context;
    private String contextId;

    //Arun Dhwaj: What if there a question which size is more than one slid?
    private String slideId;

    private Integer studentCountAtStart;//when the quiz/poll/hotspot started, what was the count
    private Integer totalStudentsConnected;
    private Integer totalUnattempts;
    private Integer totalAttempts;
    private Integer slideNumber;
    private Integer totalYes;//poll only
    private Integer totalNo;//poll only
    private Long serverTime;//time at which it happened
    private Integer totalCorrectResponses;//quiz,hotspot only 
    private Integer totalIncorrectResponses;//quiz,hotspot only 
    private String correctAnswer;
    private Integer averageResponseTime;//quiz,hotspot,poll

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public InteractionType getContext() {
        return context;
    }

    public void setContext(InteractionType context) {
        this.context = context;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getSlideId() {
        return slideId;
    }

    public void setSlideId(String slideId) {
        this.slideId = slideId;
    }

    public Integer getStudentCountAtStart() {
        return studentCountAtStart;
    }

    public void setStudentCountAtStart(Integer studentCountAtStart) {
        this.studentCountAtStart = studentCountAtStart;
    }

    public Integer getTotalStudentsConnected() {
        return totalStudentsConnected;
    }

    public void setTotalStudentsConnected(Integer totalStudentsConnected) {
        this.totalStudentsConnected = totalStudentsConnected;
    }

    public Integer getTotalUnattempts() {
        return totalUnattempts;
    }

    public void setTotalUnattempts(Integer totalUnattempts) {
        this.totalUnattempts = totalUnattempts;
    }

    public Integer getTotalAttempts() {
        return totalAttempts;
    }

    public void setTotalAttempts(Integer totalAttempts) {
        this.totalAttempts = totalAttempts;
    }

    public Integer getSlideNumber() {
        return slideNumber;
    }

    public void setSlideNumber(Integer slideNumber) {
        this.slideNumber = slideNumber;
    }

    public Integer getTotalYes() {
        return totalYes;
    }

    public void setTotalYes(Integer totalYes) {
        this.totalYes = totalYes;
    }

    public Integer getTotalNo() {
        return totalNo;
    }

    public void setTotalNo(Integer totalNo) {
        this.totalNo = totalNo;
    }

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public Integer getTotalCorrectResponses() {
        return totalCorrectResponses;
    }

    public void setTotalCorrectResponses(Integer totalCorrectResponses) {
        this.totalCorrectResponses = totalCorrectResponses;
    }

    public Integer getTotalIncorrectResponses() {
        return totalIncorrectResponses;
    }

    public void setTotalIncorrectResponses(Integer totalIncorrectResponses) {
        this.totalIncorrectResponses = totalIncorrectResponses;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Integer getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(Integer averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String SESSIONID = "sessionId";
        public static final String SLIDEID = "slideId";
        public static final String CONTEXT_ID = "contextId";
        public static final String CONTEXT = "context";
        public static final String TOTAL_INCORRECT_RESPONSE = "totalIncorrectResponses";
        public static final String TOTAL_CORRECT_RESPONSE = "totalCorrectResponses";
        public static final String TOTAL_UNATTEMPTS = "totalUnattempts";
        public static final String STUDENT_COUNT_AT_START = "studentCountAtStart";
    }
}
