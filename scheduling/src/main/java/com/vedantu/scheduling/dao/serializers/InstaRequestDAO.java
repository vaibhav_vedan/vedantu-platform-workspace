package com.vedantu.scheduling.dao.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.InstaRequest;
import com.vedantu.scheduling.dao.entity.SessionAttendee;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.Arrays;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class InstaRequestDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public InstaRequestDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(InstaRequest p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public void saveAll(Collection<InstaRequest> instaRequests) {
        try {
            for (InstaRequest instaRequest : instaRequests) {
                saveEntity(instaRequest);
            }
        } catch (Exception ex) {
            throw new RuntimeException(
                    "update : Error updating the instaRequests" + Arrays.toString(instaRequests.toArray()), ex);
        }
    }

    public InstaRequest getById(String id) {
        InstaRequest InstaRequest = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                InstaRequest = (InstaRequest) getEntityById(id, InstaRequest.class);
            }
        } catch (Exception ex) {
            InstaRequest = null;
        }
        return InstaRequest;
    }

    public void update(Query q, Update u) {
        try {
            updateFirst(q, u, InstaRequest.class);
        } catch (Exception ex) {
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, InstaRequest.class);
        } catch (Exception ex) {
        }

        return result;
    }
}
