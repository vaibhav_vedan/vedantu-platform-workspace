package com.vedantu.scheduling.dao.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.vedantu.scheduling.pojo.CalendarEventState;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.request.UpdateCalendarSlotBitsReq;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class CalendarEvent extends AbstractMongoStringIdEntity {
	private String userId;
	private Long startTime;
	private CalendarEntrySlotState slotState;
	private String details;
	private CalendarEventState state;
	private int retryCount = 0;

	public CalendarEvent() {
		super();
	}

	public CalendarEvent(String userId, Long startTime, CalendarEntrySlotState slotState, String details, CalendarEventState state) {
		super();
		this.userId = userId;
		this.startTime = startTime;
		this.slotState = slotState;
		this.details = details;
		this.state = state;
	}

	public CalendarEvent(UpdateCalendarSlotBitsReq req, String details, CalendarEventState state) {
		super();
		this.userId = req.getUserId();
		this.startTime = req.getStartTime();
		this.slotState = req.getSlotState();
		this.details = details;
		this.state = state;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public CalendarEventState getState() {
		return state;
	}

	public void setState(CalendarEventState state) {
		this.state = state;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	public void incrementRetryCount() {
		this.retryCount++;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String START_TIME = "startTime";
		public static final String SLOT_STATE = "slotState";
		public static final String STATE = "state";
	}

	public List<String> validate() {
		List<String> errors = new ArrayList<String>();
		if (this.startTime == null || this.startTime <= 0l) {
			errors.add(Constants.START_TIME);
		}
		if (StringUtils.isEmpty(this.userId)) {
			errors.add(Constants.USER_ID);
		}
		if (this.slotState == null) {
			errors.add(Constants.SLOT_STATE);
		}
		if (StringUtils.isEmpty(this.details)) {
			errors.add("details");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "CalendarEvent [userId=" + userId + ", startTime=" + startTime + ", slotState=" + slotState
				+ ", details=" + details + ", state=" + state + "]";
	}
}
