package com.vedantu.scheduling.dao.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.BitSet;

public class Schedule extends AbstractMongoStringIdEntity {
	private Long startTime;
	// @JsonProperty
	// private long scheduleNum;
	private BitSet scheduleBitSet;
	private BitSet gttScheduleBitset;
	private BitSet gttProScheduleBitSet;
	private BitSet gtt100ScheduleBitSet;
	private BitSet gtwScheduleBitset;
	private BitSet gttWebinarScheduleBitset;

	public Schedule() {
		super();
	}

	public Schedule(long tmstmp) {
		startTime = tmstmp;
		this.scheduleBitSet = new BitSet();
		this.gttScheduleBitset = new BitSet();
		this.gttProScheduleBitSet = new BitSet();
		this.gtt100ScheduleBitSet = new BitSet();
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public BitSet getScheduleBitSet() {
		return scheduleBitSet;
	}

	public void setScheduleBitSet(BitSet scheduleBitSet) {
		this.scheduleBitSet = scheduleBitSet;
	}

	public BitSet getGttScheduleBitset() {
		if (gttScheduleBitset == null) {
			gttScheduleBitset = new BitSet();
		}
		return gttScheduleBitset;
	}

	public void setGttScheduleBitset(BitSet gttScheduleBitset) {
		this.gttScheduleBitset = gttScheduleBitset;
	}

	public BitSet getGttProScheduleBitSet() {
		if (gttProScheduleBitSet == null) {
			gttProScheduleBitSet = new BitSet();
		}
		return gttProScheduleBitSet;
	}

	public void setGttProScheduleBitSet(BitSet gttProScheduleBitSet) {
		this.gttProScheduleBitSet = gttProScheduleBitSet;
	}

	public BitSet getGtt100ScheduleBitSet() {
		if (gtt100ScheduleBitSet == null) {
			gtt100ScheduleBitSet = new BitSet();
		}
		return gtt100ScheduleBitSet;
	}

	public void setGtt100ScheduleBitSet(BitSet gtt100ScheduleBitSet) {
		this.gtt100ScheduleBitSet = gtt100ScheduleBitSet;
	}
	
	public void setGtwScheduleBitset(BitSet gtwScheduleBitset) {
		this.gtwScheduleBitset = gtwScheduleBitset;
	}

	public BitSet getGtwScheduleBitSet() {
		if (gtwScheduleBitset == null) {
			gtwScheduleBitset = new BitSet();
		}
		return gtwScheduleBitset;
	}
	
	public void setGttWebinarScheduleBitset(BitSet gttWebinarScheduleBitset) {
		this.gttWebinarScheduleBitset = gttWebinarScheduleBitset;
	}

	public BitSet getGttWebinarScheduleBitset() {
		if (gttWebinarScheduleBitset == null) {
			gttWebinarScheduleBitset = new BitSet();
		}
		return gttWebinarScheduleBitset;
	}
	

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String START_TIME = "startTime";
	}

}
