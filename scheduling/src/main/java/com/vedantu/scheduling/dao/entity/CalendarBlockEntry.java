package com.vedantu.scheduling.dao.entity;

import java.util.BitSet;

import com.vedantu.scheduling.managers.CalendarEntryManager;
import com.vedantu.scheduling.pojo.CalendarBlockEntryState;
import com.vedantu.scheduling.pojo.CalendarBlockType;
import com.vedantu.subscription.request.BlockSlotScheduleReq;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "CalendarBlockEntry")
public class CalendarBlockEntry extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String studentId;
    private String teacherId;
    private long repeatCount;
    private long startTime;
    private BitSet blockEntry;
    private String referenceId; // subscription request id
    private CalendarBlockType blockType = CalendarBlockType.STANDARD;
    private CalendarBlockEntryState state;

    public CalendarBlockEntry() {
        super();
    }

    public CalendarBlockEntry(BlockSlotScheduleReq req, Long startTime, BitSet blockEntry) {
        super();
        this.studentId = req.getStudentId();
        this.teacherId = req.getTeacherId();
        this.repeatCount = req.getSchedule().getNoOfWeeks() == null ? 1 : req.getSchedule().getNoOfWeeks();
        this.referenceId = req.getReferenceId();
        this.startTime = startTime;
        this.state = CalendarBlockEntryState.ACTIVE;
        this.blockType = CalendarBlockType.STANDARD;
        this.blockEntry = blockEntry;
    }

    public long getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(long repeatCount) {
        this.repeatCount = repeatCount;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public CalendarBlockEntryState getState() {
        return state;
    }

    public void setState(CalendarBlockEntryState state) {
        this.state = state;
    }

    public BitSet getBlockEntry() {
        return blockEntry;
    }

    public void setBlockEntry(BitSet blockEntry) {
        this.blockEntry = blockEntry;
    }

    public Long getEndTime() {
        return startTime + this.blockEntry.length() * CalendarEntryManager.SLOT_LENGTH;
    }

    public CalendarBlockType getBlockType() {
        return blockType;
    }

    public void setBlockType(CalendarBlockType blockType) {
        this.blockType = blockType;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "CalendarBlockEntry [studentId=" + studentId + ", teacherId=" + teacherId + ", repeatCount="
                + repeatCount + ", startTime=" + startTime + ", blockEntry=" + blockEntry + ", referenceId="
                + referenceId + ", blockType=" + blockType + ", state=" + state + "]";
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String STUDENT_ID = "studentId";
        public static final String TEACHER_ID = "teacherId";
        public static final String START_TIME = "startTime";
        public static final String REFERENCE_ID = "referenceId";
        public static final String STATE = "state";
        public static final String BLOCK_TYPE = "blockType";
        public static final String BLOCK_ENTRY = "blockEntry";
        public static final String REPEAT_COUNT = "repeatCount";
    }
}
