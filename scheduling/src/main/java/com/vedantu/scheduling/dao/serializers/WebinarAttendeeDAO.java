package com.vedantu.scheduling.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.WebinarAttendee;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class WebinarAttendeeDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public WebinarAttendeeDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public WebinarAttendee create(WebinarAttendee webinarAttendee) {
		// Always an insert operation. Update is not supported.
		try {
			if (webinarAttendee != null) {
				saveEntity(webinarAttendee);
			}
		} catch (Exception ex) {
			// throw Exception
			return null;
		}

		return webinarAttendee;
	}

	public WebinarAttendee getById(String id) {
		WebinarAttendee webinarAttendee = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				webinarAttendee = (WebinarAttendee) getEntityById(id, WebinarAttendee.class);
			}
		} catch (Exception ex) {
			// log Exception
			webinarAttendee = null;
		}
		return webinarAttendee;
	}
}
