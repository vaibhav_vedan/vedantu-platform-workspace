package com.vedantu.scheduling.dao.serializers;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vedantu.scheduling.dao.entity.CalendarBlockEntry;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class CalendarBlockEntryDAO extends AbstractMongoDAO {
        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public CalendarBlockEntryDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void save(CalendarBlockEntry calendarBlockEntry) {
		try {
			Assert.notNull(calendarBlockEntry);
			saveEntity(calendarBlockEntry);
		} catch (Exception ex) {
			throw new RuntimeException("calendarBlockEntryUpdateError : Error updating the calendarBlockEntry "
					+ calendarBlockEntry.toString(), ex);
		}
	}

	public CalendarBlockEntry getById(String id) {
		CalendarBlockEntry calendarBlockEntry = null;
		try {
			Assert.hasText(id);
			calendarBlockEntry = (CalendarBlockEntry) getEntityById(id, CalendarBlockEntry.class);
		} catch (Exception ex) {
			throw new RuntimeException("CalendarBlockEntryGetError : Error fetch the calendarEvent with id " + id, ex);
		}
		return calendarBlockEntry;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			Assert.hasText(id);
			result = deleteEntityById(id, CalendarBlockEntry.class);
		} catch (Exception ex) {
		}

		return result;
	}
}
