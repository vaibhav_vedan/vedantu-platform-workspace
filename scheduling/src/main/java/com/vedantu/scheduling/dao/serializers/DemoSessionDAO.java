package com.vedantu.scheduling.dao.serializers;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.scheduling.dao.entity.DemoSession;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class DemoSessionDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public DemoSessionDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
        
	public void create(DemoSession p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			// throw Exception
		}
	}

	public void insertAll(List<DemoSession> p) {
		try {
			if (p != null) {
				insertAllEntities(p, DemoSession.class.getSimpleName());
			}
		} catch (Exception ex) {
			// throw Exception
		}
	}

	public DemoSession getById(String id) {
		DemoSession demoSession = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				demoSession = (DemoSession) getEntityById(id, DemoSession.class);
			}
		} catch (Exception ex) {
			// log Exception
			demoSession = null;
		}
		return demoSession;
	}

	public void save(DemoSession p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			// throw Exception;
		}
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, DemoSession.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}
}
