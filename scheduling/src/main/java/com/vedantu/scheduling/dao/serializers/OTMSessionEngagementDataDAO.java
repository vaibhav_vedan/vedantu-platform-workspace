package com.vedantu.scheduling.dao.serializers;

import com.vedantu.scheduling.dao.entity.OTMSessionEngagementData;
import com.vedantu.scheduling.enums.wavesession.InteractionType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import org.springframework.stereotype.Service;

@Service
public class OTMSessionEngagementDataDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTMSessionEngagementDataDAO.class);

    public OTMSessionEngagementDataDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(OTMSessionEngagementData p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public List<OTMSessionEngagementData> getBySessionId(String sessionId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.SESSIONID).is(sessionId));
        setFetchParameters(query, start, size);
        return runQuery(query, OTMSessionEngagementData.class);
    }

    public OTMSessionEngagementData getPreviousSavedEngagementData(String sessionId, String slideId, String contextId, InteractionType context) {
        Query query = new Query();

        query.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.SESSIONID).is(sessionId));
        if (StringUtils.isNotEmpty(contextId)) {
            query.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.CONTEXT_ID).is(contextId));
        }
        query.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.SLIDEID).is(slideId));
        query.addCriteria(Criteria.where(OTMSessionEngagementData.Constants.CONTEXT).is(context));
        setFetchParameters(query, 0, DEFAULT_FETCH_SIZE);
        List<OTMSessionEngagementData> l = runQuery(query, OTMSessionEngagementData.class);
        if (l != null && l.size() > 1) {
            logger.error("multiple entries found for OTMSessionEngagementData sessionId "
                    + sessionId + ", contextId " + contextId + ", slideId " + slideId + ", context " + context);
        }
        if (l == null || l.isEmpty()) {
            return null;
        } else {
            return l.get(0);
        }
    }

}
