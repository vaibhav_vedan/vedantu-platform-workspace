package com.vedantu.scheduling.dao.serializers;

import com.vedantu.scheduling.dao.entity.*;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Service
public class LOAMAmbassadorDAO extends AbstractMongoDAO
{
	@Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(LOAMAmbassadorDAO.class);

    public LOAMAmbassadorDAO() {
    	super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

	public List<GTTAttendeeDetails>  loam_getAttendedGTTAttendeeDetailsInSessionRange(final Long studentId, final Long fromTime, final Long thruTime,  Set<String> includeKeySet, boolean isInclude)
	{
		Query q = new Query();
		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_START_TIME).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.OTF_SESSION_END_TIME).lte(thruTime));
		q.addCriteria(andCriteria);
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.USER_ID).is(studentId.toString()));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ATTENDEE_TYPE).exists(false));
		q.addCriteria(Criteria.where(GTTAttendeeDetails.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

		loam_manageFieldsInclude(q,includeKeySet,isInclude);
		return runQuery(q, GTTAttendeeDetails.class);
	}

	public CopyOnWriteArrayList<OTFSession> loam_getAllSessionsForDoubts(Long fromTime, Long thruTime, Set<String> includeKeySet, boolean isInclude)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		Query query = new Query();
		query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
		query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));

		loam_manageFieldsInclude(query,includeKeySet,isInclude);

		List<OTFSession> results = runQuery(query, OTFSession.class);

		if (!CollectionUtils.isEmpty(results))
		{
			oTFSessions.addAll(results);
		}

		return oTFSessions;
	}

	public CopyOnWriteArrayList<OTFSession> loam_getByBoardIdForDoubt(Long boardId, Long fromTime, Long thruTime, Set<String> includeKeySet, boolean isInclude)
	{
		CopyOnWriteArrayList<OTFSession> oTFSessions = new CopyOnWriteArrayList<>();

		if (!StringUtils.isEmpty(boardId))
		{
			Query query = new Query();
			query.addCriteria(Criteria.where(OTFSession.Constants.BOARD_ID).is(boardId));

			//For no fromTime and thruTime send -1,-1 as fromTime and thruTime
			if (fromTime > -1 && thruTime > -1)
			{
				query.addCriteria(Criteria.where(OTFSession.Constants.START_TIME).gte(fromTime).lte(thruTime));
			}
			query.with(Sort.by(Direction.ASC, OTFSession.Constants.START_TIME));
			loam_manageFieldsInclude(query,includeKeySet,isInclude);
			List<OTFSession> results = runQuery(query, OTFSession.class);

			if (!CollectionUtils.isEmpty(results))
			{
				oTFSessions.addAll(results);
			}
		}

		return oTFSessions;
	}

	private void loam_manageFieldsInclude(Query q, Set<String> includeKeySet, boolean isInclude){
		if(includeKeySet !=null && !includeKeySet.isEmpty())
		{
			if(isInclude)
				includeKeySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
			else
				includeKeySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
		}
	}

    public List<OTFSession> loam_getOTFSessions(Long fromTime, Long thruTime, Set<String> keySet, boolean isInclude) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(OTFSession.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(OTFSession.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);

		loam_manageFieldsInclude(query, keySet, isInclude);
		return runQuery(query, OTFSession.class);
    }

	public List<GTTAttendeeDetails> loam_getGTTAttendeeDetails(Long fromTime, Long thruTime, Integer start, Integer size, Set<String> keySet, boolean isInclude) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(GTTAttendeeDetails.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(GTTAttendeeDetails.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		query.with(Sort.by(Sort.Direction.DESC, GTTAttendeeDetails.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);
		loam_manageFieldsInclude(query, keySet, isInclude);
		return runQuery(query, GTTAttendeeDetails.class);
	}

	public List<OTMSessionEngagementData> loam_getOTMSessionEngagementData(Long fromTime, Long thruTime, Set<String> keySet, boolean isInclude) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(OTMSessionEngagementData.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(OTMSessionEngagementData.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);

		loam_manageFieldsInclude(query, keySet, isInclude);
		return runQuery(query, OTMSessionEngagementData.class);
	}
}
