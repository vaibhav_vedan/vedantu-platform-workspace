package com.vedantu.scheduling.dao.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vedantu.scheduling.pojo.session.*;
import com.vedantu.session.pojo.EntityType;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.User.FeatureSource;
import com.vedantu.exception.VException;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.scheduling.request.session.SessionScheduleReq;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.session.pojo.SessionEndType;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SessionModel;

@Document(collection = "Session")
@CompoundIndexes({ @CompoundIndex(name = "teacherId_endTime", def = "{'teacherId' : 1, 'endTime': 1}", background = true),
		@CompoundIndex(name = "studentIds_endTime", def = "{'studentIds' : 1, 'endTime': 1}", background = true),
		@CompoundIndex(name = "studentIds_1_startTime_-1", def = "{'studentIds' : 1, 'startTime': -1}", background = true),
		@CompoundIndex(name = "teacherId_1_startTime_-1", def = "{'teacherId' : 1, 'startTime': -1}", background = true),
		})
public class Session extends AbstractMongoLongIdEntity {

	private String subject;
	private String topic;
	private String title;
	private String description;
	private Long startTime;
	private Long endTime;
	@Indexed(background=true)
	private SessionState state = SessionState.SCHEDULED;
	private Long scheduledBy;
	private Long startedAt;
	private Long startedBy;
	private Long endedAt;
	private Long endedBy;
	private Long activatedAt; // Introduced to identify any discrepancies
	private SessionPayoutType type;
	@Indexed(background=true)
	private Long subscriptionId;
	private List<Long> studentIds;
	private Long teacherId;
	private String remark;
	private FeatureSource sessionSource;
	private SessionModel model;
	private RequestSource deviceSource;
	private List<RescheduleData> rescheduleData = new ArrayList<>();
	private List<ReferenceTag> sessionTags = new ArrayList<>();
	private SessionTOSType tosType; // Will be set async
	private LiveSessionPlatformType liveSessionPlatformType = LiveSessionPlatformType.DEFAULT;
	private GTMGTTSessionDetails liveSessionPlatformDetails;
	private Long cancelledBy;

	private EntityType contextType;
        
	@Indexed(background=true) //needed for dashboard
	private String contextId;
        
        private String vimeoId;
        @Indexed(background=true) 
        private String wizIQclassId;
        private String replayUrl;
        private SessionEndType endType;

	public Session() {
		super();
	}

	public Session(SessionScheduleReq req) throws VException {
		super();
		this.subject = req.getSubject();
		this.topic = req.getTopic();
		this.title = req.getTitle();
		this.description = req.getDescription();
		this.startTime = req.getStartTime();
		this.endTime = req.getEndTime();
		this.state = SessionState.SCHEDULED;
		this.scheduledBy = req.getCallingUserId();
		this.type = req.getType();
		this.subscriptionId = req.getSubscriptionId();
		this.studentIds = req.getStudentIds();
		this.teacherId = req.getTeacherId();
		this.sessionSource = req.getSessionSource();
		this.model = req.getSessionModel();
		this.deviceSource = req.getDeviceSource();

		// if (req.getCallingUserId() != null && req.getCallingUserId() > 0l) {
		// this.setLastUpdatedBy(String.valueOf(req.getCallingUserId()));
		// }
		if (req.getOfferingId() != null && req.getOfferingId() > 0l) {
			addReferenceTag(ReferenceType.OFFERING, String.valueOf(req.getOfferingId()));
		}
		if (req.getProposalId() != null && req.getProposalId() > 0l) {
			addReferenceTag(ReferenceType.PROPOSAL, String.valueOf(req.getProposalId()));
		}
	}

	public Session(MultipleSessionScheduleReq req, SessionSlot sessionSlot) throws VException {
		super();
		this.subject = req.getSubject();
		this.topic = StringUtils.isEmpty(sessionSlot.getTopic()) ? req.getTopic() : sessionSlot.getTopic();
		this.title = StringUtils.isEmpty(sessionSlot.getTitle()) ? req.getTitle() : sessionSlot.getTitle();
		this.description = StringUtils.isEmpty(sessionSlot.getDescription()) ? req.getDescription()
				: sessionSlot.getDescription();
		this.startTime = sessionSlot.getStartTime();
		this.endTime = sessionSlot.getEndTime();
		this.state = SessionState.SCHEDULED;
		this.scheduledBy = req.getCallingUserId();
		this.type = req.getType();
		this.subscriptionId = req.getSubscriptionId();
		this.studentIds = req.getStudentIds();
		this.teacherId = req.getTeacherId();
		this.sessionSource = req.getSessionSource();
		this.model = sessionSlot.getModel() != null ? sessionSlot.getModel() : req.getSessionModel();
		if (sessionSlot.getPayoutType() != null) {
			this.type = sessionSlot.getPayoutType();
		}
		this.contextType = req.getContextType();
		this.contextId = req.getContextId();
		this.deviceSource = req.getDeviceSource();

		if (req.getOfferingId() != null && req.getOfferingId() > 0l) {
			addReferenceTag(ReferenceType.OFFERING, String.valueOf(req.getOfferingId()));
		}
		if (req.getProposalId() != null && req.getProposalId() > 0l) {
			addReferenceTag(ReferenceType.PROPOSAL, String.valueOf(req.getProposalId()));
		}
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public SessionState getState() {
		return state;
	}

	public void setState(SessionState state) {
		this.state = state;
	}

	public Long getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(Long scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	public Long getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Long startedAt) {
		this.startedAt = startedAt;
	}

	public Long getStartedBy() {
		return startedBy;
	}

	public void setStartedBy(Long startedBy) {
		this.startedBy = startedBy;
	}

	public Long getEndedAt() {
		return endedAt;
	}

	public void setEndedAt(Long endedAt) {
		this.endedAt = endedAt;
	}

	public Long getEndedBy() {
		return endedBy;
	}

	public void setEndedBy(Long endedBy) {
		this.endedBy = endedBy;
	}

	public SessionPayoutType getType() {
		return type;
	}

	public void setType(SessionPayoutType type) {
		this.type = type;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public void setSessionTags(List<ReferenceTag> sessionTags) {
		this.sessionTags = sessionTags;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public FeatureSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(FeatureSource sessionSource) {
		this.sessionSource = sessionSource;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public RequestSource getDeviceSource() {
		return deviceSource;
	}

	public void setDeviceSource(RequestSource deviceSource) {
		this.deviceSource = deviceSource;
	}

	public List<RescheduleData> getRescheduleData() {
		return rescheduleData;
	}

	public void setRescheduleData(List<RescheduleData> rescheduleData) {
		this.rescheduleData = rescheduleData;
	}

	public List<ReferenceTag> getSessionTags() {
		return sessionTags;
	}

	public Long getActivatedAt() {
		return activatedAt;
	}

	public void setActivatedAt(Long activatedAt) {
		this.activatedAt = activatedAt;
	}

	public SessionTOSType getTosType() {
		return tosType;
	}

	public void setTosType(SessionTOSType tosType) {
		this.tosType = tosType;
	}

	public LiveSessionPlatformType getLiveSessionPlatformType() {
		return liveSessionPlatformType;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public void setLiveSessionPlatformType(LiveSessionPlatformType liveSessionPlatformType) {
		this.liveSessionPlatformType = liveSessionPlatformType;
	}

	public GTMGTTSessionDetails getLiveSessionPlatformDetails() {
		return liveSessionPlatformDetails;
	}

	public void setLiveSessionPlatformDetails(GTMGTTSessionDetails liveSessionPlatformDetails) {
		this.liveSessionPlatformDetails = liveSessionPlatformDetails;
	}

	public Long getCancelledBy() {
		return cancelledBy;
	}

	public void setCancelledBy(Long cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public List<ReferenceTag> addReferenceTag(ReferenceType referenceType, String referenceId)
			throws VException {
		ReferenceTag referenceTag = new ReferenceTag(referenceType, referenceId);
		referenceTag.validate();
		this.sessionTags.add(referenceTag);
		return this.sessionTags;
	}

	public List<ReferenceTag> removeReferenceTag(ReferenceType referenceType) {
		Iterator<ReferenceTag> iterator = this.sessionTags.iterator();
		while (iterator.hasNext()) {
			ReferenceTag referenceTag = iterator.next();
			if (referenceType.equals(referenceTag.getReferenceType())) {
				this.sessionTags.remove(referenceTag);
			}
		}
		return this.sessionTags;
	}

	public static List<Session> createSessions(MultipleSessionScheduleReq req) throws VException {
		List<Session> sessions = new ArrayList<>();
		if (!CollectionUtils.isEmpty(req.getSlots())) {
			for (SessionSlot sessionSlot : req.getSlots()) {
				Session session = new Session(req, sessionSlot);
				sessions.add(session);
			}
		}
		return sessions;
	}

        public String getVimeoId() {
            return vimeoId;
        }

        public void setVimeoId(String vimeoId) {
            this.vimeoId = vimeoId;
        }

        public String getWizIQclassId() {
            return wizIQclassId;
        }

        public void setWizIQclassId(String wizIQclassId) {
            this.wizIQclassId = wizIQclassId;
        }

        public String getReplayUrl() {
            return replayUrl;
        }

        public void setReplayUrl(String replayUrl) {
            this.replayUrl = replayUrl;
        }

        public SessionEndType getEndType() {
            return endType;
        }

        public void setEndType(SessionEndType endType) {
            this.endType = endType;
        }
        
	public static class Constants extends AbstractMongoLongIdEntity.Constants {
		public static final String STUDENT_IDS = "studentIds";
		public static final String TEACHER_ID = "teacherId";
		public static final String SUBSCRIPTION_ID = "subscriptionId";
		public static final String SESSION_STATE = "state";
		public static final String START_TIME = "startTime";
		public static final String END_TIME = "endTime";
		public static final String SESSION_MODEL = "model";
		public static final String CONTEXT_ID = "contextId";
		public static final String CONTEXT_TYPE = "contextType";
		public static final String SUBJECT = "subject";
		public static final String RESCHEDULE_DATA = "rescheduleData";
		public static final String CANCELLED_BY = "cancelledBy";
                public static final String WIZ_IQ_CLASS_ID = "wizIQclassId";
                public static final String REPLAY_URL = "replayUrl";
                public static final String TITLE = "title";
	}
}
