package com.vedantu.scheduling.dao.entity;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class WebinarAttendee extends AbstractMongoStringIdEntity {
	private String sessionId;
	private String userId;
	private EntityStatus entityStatus;

	public WebinarAttendee() {
		super();
	}

	public WebinarAttendee(String sessionId, String userId) {
		super();
		this.sessionId = sessionId;
		this.userId = userId;
		this.entityStatus = EntityStatus.ACTIVE;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public EntityStatus getEntityStatus() {
		return entityStatus;
	}

	public void setEntityStatus(EntityStatus entityStatus) {
		this.entityStatus = entityStatus;
	}
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String SESSION_ID = "sessionId";
		public static final String USER_ID = "userId";
	}

	@Override
	public String toString() {
		return "WebinarAttendee [sessionId=" + sessionId + ", userId=" + userId + ", entityStatus=" + entityStatus
				+ ", toString()=" + super.toString() + "]";
	}
}
