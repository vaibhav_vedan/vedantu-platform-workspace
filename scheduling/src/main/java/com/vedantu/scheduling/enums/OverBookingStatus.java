package com.vedantu.scheduling.enums;

public enum OverBookingStatus {

    STUDENT_NOT_JOINED(""),
    TEACHER_ASSIGNED("YES"),
    TEACHER_NOT_AVAILABLE("NO"),
    YES("YES"),
    NO("NO");

    private String value;

    OverBookingStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
