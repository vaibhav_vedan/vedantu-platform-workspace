package com.vedantu.scheduling.enums;

public enum ELInterestMark {
    LOW, MEDIUM, HIGH
}
