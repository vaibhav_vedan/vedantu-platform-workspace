package com.vedantu.scheduling.enums;

public enum InsightGrades {
    POOR("Poor", -10000, 35),
    AVERAGE("Average", 35, 60),
    GOOD("Good", 60, 10000);
    // DEFINE ALL YOUR GRADES HERE

    String grade;
    int gradeMin;
    int gradeMax;

    InsightGrades(String grade, int gradeMin, int gradeMax) {
        this.grade = grade;
        this.gradeMin = gradeMin;
        this.gradeMax = gradeMax;
    }

    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @return the gradeMin
     */
    public int getGradeMin() {
        return gradeMin;
    }

    /**
     * @return the gradeMax
     */
    public int getGradeMax() {
        return gradeMax;
    }

    public static InsightGrades getGrade(double marks) {
        for (InsightGrades g : values()) {
            if (marks >= g.getGradeMin() && marks <= g.getGradeMax()) {
                return g;
            }
        }
        return null;
    }
}
