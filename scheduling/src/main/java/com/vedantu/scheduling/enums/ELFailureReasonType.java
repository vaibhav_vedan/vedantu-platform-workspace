package com.vedantu.scheduling.enums;

public enum ELFailureReasonType {

    STUDENT_NOT_JOIN("Student Not Join"),
    TECH_AUDIO_ISSUE("Tech Audio Issue"),
    TECH_VIDEO_ISSUE("Tech Video Issue"),
    TECH_NETWORK_ISSUE("Poor Network"),
    OTHER("Other");

    private String value;
    private ELFailureReasonType(String value) {
        this.value = value;
    }
    public String getELFailureType() {
        return value;
    }

}
