package com.vedantu.scheduling.enums.earlyLeaning;

public enum AnalyticsType {
    AVAILABLE_TEACHER  , //Check available teacher for a slot
    ASSIGNED_TEACHER,   // Booked teacher for a EL Demo
    CLOSED_BOOKINGS;    // Total bookings done in a demo
}
