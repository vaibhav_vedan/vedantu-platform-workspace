package com.vedantu.scheduling.enums;

public enum ExecutorRequestType {

    ALL_STUDENT_INSIGHT_ENGAGEMENT,
    ALL_STUDENT_INSIGHT_ATTENDANCE
}
