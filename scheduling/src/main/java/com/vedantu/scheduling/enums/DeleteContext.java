package com.vedantu.scheduling.enums;

/**
 * @author mano
 */

public enum DeleteContext {
    /**
     * user de enroll from batch, future session of the enrolment will be flagged with this context
     */
    UPCOMING_SESSION,
    /**
     * user de enroll from batch, all past session of the enrolment will be flagged with this context
     */
    PAST_SESSION,
    /**
     * session got cancelled, deleted or forfeited
     */
    SESSION_STATE_CHANGE,
    /**
     * session start and end time changed
     */
    SESSION_TIME_CHANGE,
    /**
     * batch removed or added from session
     */
    BATCH_ADDED_OR_REMOVED,

}
