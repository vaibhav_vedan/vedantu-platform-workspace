package com.vedantu.scheduling.enums;

public enum AttendanceFeedbackTag {
    ABSENT, SCHEDULING, EXPERIENCE, TECHNICAL, OTHERS
}
