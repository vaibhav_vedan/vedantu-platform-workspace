package com.vedantu.dbs.sessionfactory;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import javax.annotation.PreDestroy;

import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.scheduling.dao.sql.entity.SessionStateDetails;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.io.File;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.Session.LockRequest;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;

@Service
public class SqlSessionFactory {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SqlSessionFactory.class);

	private SessionFactory sessionFactory = null;

	public SqlSessionFactory() {
		logger.info("initializing Sql Session Factory");
		try {
			Configuration configuration = new Configuration();
			String path = "ENV-" + ConfigUtils.INSTANCE.properties.getProperty("environment") + java.io.File.separator
					+ "hibernate.cfg.xml";
			configuration.configure(path);
			configuration.addAnnotatedClass(SessionStateDetails.class);
                        Properties properties = new Properties();
                        properties.put("hibernate.connection.username",ConfigUtils.INSTANCE.properties.getProperty("hibernate.connection.username"));
                        properties.put("hibernate.connection.password",ConfigUtils.INSTANCE.properties.getProperty("hibernate.connection.password"));
			configuration.configure(path).addProperties(properties);                        
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Exception e) {
			logger.error("error in creating sql connection ", e);
		}
	}

	/**
	 * @return the session
	 */
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


    @PreDestroy
    public void cleanUp() {
        try {
            if (sessionFactory != null) {
                sessionFactory.close();
            }
            Enumeration<Driver> drivers = DriverManager.getDrivers();

            Driver driver = null;

            // clear drivers
            while (drivers.hasMoreElements()) {
                try {
                    driver = drivers.nextElement();
                    logger.info("deregistering driver " + driver.getMajorVersion());
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    logger.error("exceoton in driver deregister " + ex.getMessage());
                }
            }
            // MySQL driver leaves around a thread. This static method cleans it up.
            AbandonedConnectionCleanupThread.shutdown();
            logger.info("cleaning done");
        } catch (Exception e) {
            logger.error("Error in closing sql connection ", e);
        }
    }
    //only for testing locally
    public static void main(String[] args) throws InterruptedException {
        Configuration configuration = new Configuration();
        String path = "/Users/ajith/projects/git/vedantu-platform-workspace/scheduling/src/main/resources/ENV-LOCAL/hibernate.cfg.xml";
        File file =new File(path);
        System.err.println(">> file "+path);
        configuration.configure(file);
        configuration.addAnnotatedClass(SessionStateDetails.class);
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        try {
            
            transaction.begin();    
            
            LockOptions lockOptions = new LockOptions(LockMode.PESSIMISTIC_WRITE);
            LockRequest lockRequest = session.buildLockRequest(lockOptions);
            System.err.println("Lock obtained");
            // Fetch the session state details
            Criteria cr = session.createCriteria(SessionStateDetails.class);
            cr.add(Restrictions.eq(SessionStateDetails.Constants.SESSION_ID, 2l));
            List<SessionStateDetails> results= cr.list();

            System.err.println(" results : " + results);
            SessionStateDetails sessionStateDetails=results.get(0);
            lockRequest.lock(sessionStateDetails);
            sessionStateDetails.setSessionState(SessionState.ENDED);
            session.update(sessionStateDetails);
            System.err.println("donww");
           
            transaction.commit();
        } catch (Exception e) {
            if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                System.err.println("rollback: " + e.getMessage());
                session.getTransaction().rollback();
            }
            throw e;
        } finally {
            session.close();
        }
//        Thread.sleep(100000);
    }
}
