package com.vedantu.platform.enums.cms;

public enum WebinarType {
    LIVECLASS, QUIZ, WEBINAR
}
