package com.vedantu.platform.enums.requestcallback;

public enum TeacherLeadStatus {

	NOT_UPDATED("Not Updated"), INTERESTED("Interested"),FOLLOW_UP("Follow up"),NOT_INTERESTED("Not Interested");
	
	private final String statusString;
	
	TeacherLeadStatus(String teacherLeadStatus){
		this.statusString = teacherLeadStatus;
	}
	
	public static String getStatusString(TeacherLeadStatus teacherLeadStatus){
		return teacherLeadStatus.statusString;
	}
	
}
