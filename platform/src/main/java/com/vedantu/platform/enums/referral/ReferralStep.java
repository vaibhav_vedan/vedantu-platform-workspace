package com.vedantu.platform.enums.referral;

public enum ReferralStep {

	SIGNUP("Sign Up"), VEMAIL("Verification Email"), VMOBILE("Verification Mobile"), FIRST_SESSION("First session completed - Can be IL, OTO, book loose session, OTF or Trial"), FIRST_PURCHASE("Purchase subscription - 3 session challenge, book loose session, OTF, OTO"), FIRST_OTF_PURCHASE("OTF course purchase"), NEW_BUNDLE_PURCHASE("New Bundle purchase with max Tries");

	private String statusString;

	ReferralStep(String statusString) {
		this.statusString = statusString;
	}

	public static String getStatusString(ReferralStep referralStep) {
		return referralStep.statusString;
	}
}
