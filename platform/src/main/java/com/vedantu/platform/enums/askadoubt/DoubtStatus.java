package com.vedantu.platform.enums.askadoubt;

public enum DoubtStatus {
	
	INITIATED, BROADCASTED, ACCEPTED, SOLUTION_SUBMITTED;

}
