package com.vedantu.platform.enums.dinero;



public enum CouponType {

	CREDIT,
	TEACHER_DISCOUNT,
	VEDANTU_DISCOUNT,
	PASS
}
