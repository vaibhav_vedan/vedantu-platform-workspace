package com.vedantu.platform.enums;

/**
 * Created by somil on 25/01/17.
 */
public enum AvailabilityExportType {
    EXPORT_BY_SLOTS, EXPORT_BY_INTERVAL, CONSOLIDATED_AVAILABILITY
}
