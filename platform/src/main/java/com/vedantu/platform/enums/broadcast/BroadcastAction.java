package com.vedantu.platform.enums.broadcast;

public enum BroadcastAction {
	
	ACCEPT, REJECT, EXPIRE;

}
