/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums;

/**
 *
 * @author somil
 */
public enum ElasticSearchTask {

	//BOARD TASKS
	ADD_BOARD,
	UPDATE_BOARD,
	
	//TEACHER TASKS
	ADD_TEACHER,
	UPDATE_TEACHER_DATA,
	UPDATE_SESSION_DATA,
	UPDATE_AVAILABILITY_DATA,
	UPDATE_ONLINE_STATUS, 
	UPDATE_RATE, 
	UPDATE_RATING, 
	UPDATE_TEACHER_BOARD_MAPPING,
	DELETE_TEACHER_BOARD_MAPPING,
	
	UPDATE_AVAILABILITY_PERCENT,
	UPDATE_ELASTIC_SEARCH_DATA_MIGRATION,
	UPDATE_ELASTIC_SEARCH_DATA_MIGRATION_RATE,
	UPDATE_TEACHER_SESSION_INFO_DATA_MIGRATION;
	
}
