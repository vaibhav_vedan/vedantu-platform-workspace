/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums;

/**
 *
 * @author ajith
 */
public enum ShowInterestType {
    OTF_COURSES, OTO_COURSES,
    PUBLIC_TEST, TRACK_COURSE, PDF_SOLUTION, TALK_TO_COUNSELLOR, BOOK_A_TRIAL, PDF_SHOW_INTEREST,
    JEE_CRASH_COURSE
}
