/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums.feedback;

/**
 *
 * @author parashar
 */
public enum FeedbackFormType {
    TEACHER_FEEDBACK, CONTENT_FEEDBACK, PARENTS_FEEDBACK;
}
