/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums.feedback;

/**
 *
 * @author parashar
 */
public enum FeedbackQuestionType {
    SINGLE_CHOICE, MULTIPLE_CHOICE, PARAGRAPH, SHORT_ANSWER, LINEAR_SCALE;
}
