package com.vedantu.platform.enums;

public enum WebinarNotifcation {
    WEBINAR_3_HRS, WEBINAR_1_HR, WEBINAR_15_MIN, WEBINAR_LIVE,WEBINAR_COMPLETED
}
