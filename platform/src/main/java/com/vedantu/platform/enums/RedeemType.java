package com.vedantu.platform.enums;

public enum RedeemType {

	FIXED, PERCENTAGE, DURATION
}
