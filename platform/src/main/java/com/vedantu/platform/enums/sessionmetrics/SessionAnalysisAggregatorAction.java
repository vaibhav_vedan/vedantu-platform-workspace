/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums.sessionmetrics;

/**
 *
 * @author jeet
 */
public enum SessionAnalysisAggregatorAction {
    SOCKET_DISCONNECTIONS,AUDIO_DISCONNECTIONS,SOCKET_DISCONNECTION_TIME,AUDIO_DISCONNECTION_TIME,POOR_AUDIO_QUALITY_TIME,
    CALL_COUNT,CALL_CONNECTION_TIME,JOIN_DELAY,HIGH_PACKET_LOSS_TIME,ICE_FAILURES,PAGE_REFRESHES,AUDIO_CALL_TRIES
}
