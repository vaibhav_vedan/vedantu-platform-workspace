package com.vedantu.platform.enums.engagement;

/*
 * Enum for SMS Type Identifiers
 */
public enum EmailType {
	SESSION_VERIFICATION, USER_MESSAGE;
}
