package com.vedantu.platform.enums;

public enum ESSearchType {
    MOST_FIELDS("most_fields");
    private String value;

    ESSearchType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
