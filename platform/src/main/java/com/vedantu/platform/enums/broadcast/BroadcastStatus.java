package com.vedantu.platform.enums.broadcast;

public enum BroadcastStatus {
	
	INITIATED, ACCEPTED, REJECTED, EXPIRED, ACCEPTED_BY_ANOTHER_USER;

}
