package com.vedantu.platform.enums;

public enum StatusType {

	OFFLINE, READY_TO_TEACH, NONE
}
