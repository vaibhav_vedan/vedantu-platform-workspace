/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums.cms;

/**
 *
 * @author jeet
 */
public enum WebinarUserRegistrationStatus {
    CONFIRMED, OTP_VERIFICATION_PENDING, ALREADY_REGISTERED, EMAIL_REGISTERED, PHONE_REGISTERED
}
