package com.vedantu.platform.enums.dinero;

import java.util.List;

/**
 * Created by somil on 02/06/17.
 */
public enum CouponTargetEntityType {
    BATCH,
    PLAN,
    COURSE_PLAN,
    BUNDLE,
    BUNDLE_PACKAGE,
    TEACHER,
    COURSE,
    SUBJECT,
    CATEGORY,
    GRADE;
}
