package com.vedantu.platform.enums.ChallengerGradeType;

public enum ChallengerGradeType {

	JUNIOR,
	INTERMEDIATE,
	SENIOR
}
