package com.vedantu.platform.enums.requestcallback;

public enum LeadAssignee {

	STUDENT_CARE, TEACHER;
	
}
