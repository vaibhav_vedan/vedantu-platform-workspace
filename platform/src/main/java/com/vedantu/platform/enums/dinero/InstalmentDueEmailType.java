/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.enums.dinero;

/**
 *
 * @author ajith
 */
public enum InstalmentDueEmailType {
    INSTALMENT_PAYMENT_DUE_7DAYS_LEFT,
    INSTALMENT_PAYMENT_DUE_1DAY_LEFT,
    INSTALMENT_PAYMENT_DUE_OTF_BLOCKED,
    INSTALMENT_PAYMENT_DUE_1DAY_PASSED,
    INSTALMENT_PAYMENT_DUE_SUBSCRIPTION_ENDED, INSTALMENT_PAYMENT_DUE_7DAYS_LEFT_OTF, 
    INSTALMENT_PAYMENT_DUE_1DAY_LEFT_OTF, 
    INSTALMENT_PAYMENT_OTF_FINAL_REMINDER, 
    INSTALMENT_PAYMENT_OTF_DUE_2DAYS_PASSED;
}
