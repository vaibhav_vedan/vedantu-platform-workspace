package com.vedantu.platform.enums;

/**
 * Created by somil on 23/01/17.
 */
public enum ExportType {
    USER, SESSION, USER_SESSION,PROPOSAL,SUBSCRIPTION, USER_ES_DATA, BILLING_ERROR_DATA,TOS_TEACHERS_DATA, HOUR_TRANSACTION, CONTENT, CONTENT_SHARE, COURSE_PLANS;
}
