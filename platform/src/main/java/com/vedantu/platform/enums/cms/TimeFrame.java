package com.vedantu.platform.enums.cms;

public enum TimeFrame {
    PAST, UPCOMING
}