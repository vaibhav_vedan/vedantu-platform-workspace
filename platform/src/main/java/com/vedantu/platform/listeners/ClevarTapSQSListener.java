/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.listeners;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.platform.enums.cms.WebinarType;
import com.vedantu.platform.managers.cms.WebinarManager;
import com.vedantu.platform.pojo.cms.Webinar;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.platform.request.CleverTapEventReq;
import com.vedantu.util.*;
import com.vedantu.util.pojo.CleverTapEvent;
import com.vedantu.util.enums.SQSMessageType;
import java.util.HashMap;
import java.util.Map;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

/**
 *
 * @author jeet
 */
@Component
public class ClevarTapSQSListener implements MessageListener {

    private static final String ACCOUNT_ID = ConfigUtils.INSTANCE.getStringValue("clevertap.auth.accountId");
    private static final String PASSCODE = ConfigUtils.INSTANCE.getStringValue("clevertap.auth.passcode");
    private static final String CLEVERTAP_SERVER_URL = ConfigUtils.INSTANCE.getStringValue("clevertap.auth.api");
    private HashMap<String, String> headers = new HashMap();

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private FosUtils fosUtils;

    private static Gson gson = new Gson();

    private enum RegistrationPeriod {
        BEFORE_WEBINAR, DURING_WEBINAR, POST_WEBINAR
    }

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ClevarTapSQSListener.class);

    public ClevarTapSQSListener() {
        headers.put("X-CleverTap-Account-Id", ACCOUNT_ID);
        headers.put("X-CleverTap-Passcode", PASSCODE);
    }

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());

            SQSMessageType sqsMessageType = null;
            if (StringUtils.isNotEmpty(textMessage.getStringProperty("messageType"))) {
                sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            }
            logger.info("Received message " + textMessage.getText());
//            if (sqsMessageType != null) {
//                handleMessage(sqsMessageType, textMessage.getText());
//            } else {
//                JSONObject jsonObject = new JSONObject(textMessage.getText());
//                logger.info("jsonObject:" + jsonObject);
//                String subject = jsonObject.getString("Subject");
//                String messageData = jsonObject.getString("Message");
//                logger.info("subject:" + subject);
//                logger.info("messageData:" + messageData);
//                handleSNSMessage(subject, messageData);
//            }
            message.acknowledge();
        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }

    public void handleMessage(SQSMessageType sqsMessageType, String text) throws Exception {
        switch (sqsMessageType) {
            case USER_DETAILS_REGISTRATION:
                UserDetailsInfo userDetailsInfo = new Gson().fromJson(text, UserDetailsInfo.class);
                CleverTapEvent event = getBasicEvent(System.currentTimeMillis(), userDetailsInfo.getStudentPhoneNo(), sqsMessageType.name());
                Map<String, Object> eventData = new HashMap<>();
                eventData.put("category", userDetailsInfo.getCategory());
                eventData.put("event", userDetailsInfo.getEvent());
                eventData.put("email", userDetailsInfo.getEmail());
                eventData.put("name", userDetailsInfo.getStudentName());
                eventData.put("userId", userDetailsInfo.getUserId());
                event.setEvtData(eventData);
                uploadEvent(event);
                break;
        }
        logger.info("Message handled");
    }

    public void handleSNSMessage(String subject, String message) throws Exception {
        WebinarUserRegistrationInfo info = null;
        CleverTapEvent event = null;
        Webinar webinar = null;
        switch (subject) {
            case "WEBINAR_REGISTRATION_OTP_PENDING":
                info = new Gson().fromJson(message, WebinarUserRegistrationInfo.class);
                if (StringUtils.isNotEmpty(info.getWebinarId())) {
                    webinar = webinarManager.getWebinarById(info.getWebinarId());
                }
                event = getWebinarRegisteredEvent(info, webinar, "WEBINAR_REGISTRATION_OTP_PENDING");
                break;

            case "VBRAINER_REGISTRATION_CONFIRMED":
                info = new Gson().fromJson(message, WebinarUserRegistrationInfo.class);
                if (StringUtils.isNotEmpty(info.getWebinarId())) {
                    webinar = webinarManager.getWebinarById(info.getWebinarId());
                }
                event = getWebinarRegisteredEvent(info, webinar, "VBRAINER_REGISTRATION_CONFIRMED");
                Long vRegistrationTime = info.getLastUpdated();
                String vRegistrationTimeStr = "";
                if (vRegistrationTime != null) {
                    long registrationTime_1 = vRegistrationTime / 1000;
                    vRegistrationTimeStr = "$D_" + registrationTime_1;
                    Map<String, Object> eventData = event.getEvtData();
                    eventData.put("registrationTime", vRegistrationTimeStr);
                    if (webinar != null) {
                        Long startTime = webinar.getStartTime();
                        Long endTime = webinar.getEndTime();
                        if (startTime != null && endTime != null) {
                            if (vRegistrationTime <= startTime) {
                                eventData.put("registrationPeriod", RegistrationPeriod.BEFORE_WEBINAR.name());
                            } else if (vRegistrationTime > startTime && vRegistrationTime <= endTime) {
                                eventData.put("registrationPeriod", RegistrationPeriod.DURING_WEBINAR.name());
                            } else {
                                eventData.put("registrationPeriod", RegistrationPeriod.POST_WEBINAR.name());
                            }
                        }
                    }
                }
                break;
            case "LIVECLASS_REGISTRATION_CONFIRMED":
                info = new Gson().fromJson(message, WebinarUserRegistrationInfo.class);
                if (StringUtils.isNotEmpty(info.getWebinarId())) {
                    webinar = webinarManager.getWebinarById(info.getWebinarId());
                }
                event = getWebinarRegisteredEvent(info, webinar, "LIVECLASS_REGISTRATION_CONFIRMED");
                Long lRegistrationTime = info.getLastUpdated();
                String lRegistrationTimeStr = "";
                if (lRegistrationTime != null) {
                    long registrationTime_1 = lRegistrationTime / 1000;
                    lRegistrationTimeStr = "$D_" + registrationTime_1;
                    Map<String, Object> eventData = event.getEvtData();
                    eventData.put("registrationTime", lRegistrationTimeStr);
                    if (webinar != null) {
                        Long startTime = webinar.getStartTime();
                        Long endTime = webinar.getEndTime();
                        if (startTime != null && endTime != null) {
                            if (lRegistrationTime <= startTime) {
                                eventData.put("registrationPeriod", RegistrationPeriod.BEFORE_WEBINAR.name());
                            } else if (lRegistrationTime > startTime && lRegistrationTime <= endTime) {
                                eventData.put("registrationPeriod", RegistrationPeriod.DURING_WEBINAR.name());
                            } else {
                                eventData.put("registrationPeriod", RegistrationPeriod.POST_WEBINAR.name());
                            }
                        }
                    }
                }
                break;
            case "WEBINAR_REGISTRATION_CONFIRMED":
                info = new Gson().fromJson(message, WebinarUserRegistrationInfo.class);
                if (StringUtils.isNotEmpty(info.getWebinarId())) {
                    webinar = webinarManager.getWebinarById(info.getWebinarId());
                }
                event = getWebinarRegisteredEvent(info, webinar, "WEBINAR_REGISTRATION_CONFIRMED");
                Long registrationTime = info.getLastUpdated();
                String registrationTimeStr = "";
                if (registrationTime != null) {
                    long registrationTime_1 = registrationTime / 1000;
                    registrationTimeStr = "$D_" + registrationTime_1;
                    Map<String, Object> eventData = event.getEvtData();
                    eventData.put("registrationTime", registrationTimeStr);
                    if (webinar != null) {
                        Long startTime = webinar.getStartTime();
                        Long endTime = webinar.getEndTime();
                        if (startTime != null && endTime != null) {
                            if (registrationTime <= startTime) {
                                eventData.put("registrationPeriod", RegistrationPeriod.BEFORE_WEBINAR.name());
                            } else if (registrationTime > startTime && registrationTime <= endTime) {
                                eventData.put("registrationPeriod", RegistrationPeriod.DURING_WEBINAR.name());
                            } else {
                                eventData.put("registrationPeriod", RegistrationPeriod.POST_WEBINAR.name());
                            }
                        }
                    }
                }
                break;
            case "WEBINAR_ATTENDED":
                info = new Gson().fromJson(message, WebinarUserRegistrationInfo.class);
                event = getWebinarAttendedEvent(info, "WEBINAR_ATTENDED");
                break;
        }
        if (event != null) {
            uploadEvent(event);
        }
        logger.info("Message handled");
    }

    public CleverTapEvent getWebinarRegisteredEvent(WebinarUserRegistrationInfo info, Webinar webinar, String eventName) {
        CleverTapEvent event = getBasicEvent(info, eventName);
        Map<String, Object> eventData = new HashMap<>();

        if (WebinarType.QUIZ.equals(webinar.getType())) {
            eventData.put("quizId", info.getWebinarId());
        } else if (WebinarType.LIVECLASS.equals(webinar.getType())) {
            eventData.put("liveClassId", info.getWebinarId());
        } else {
            eventData.put("webinarId", info.getWebinarId());
            eventData.put("trainingId", info.getTrainingId());
        }
        eventData.put("joinLink", info.getRegisterJoinUrl());

        try {
            if (webinar != null) {
                if (WebinarType.QUIZ.equals(webinar.getType())) {
                    eventData.put("quizTitle", webinar.getTitle());
                } else if (WebinarType.LIVECLASS.equals(webinar.getType())) {
                    eventData.put("liveClassTitle", webinar.getTitle());
                } else {
                    eventData.put("webinarUrl", webinar.getWebinarCode());
                    eventData.put("webinarTitle", webinar.getTitle());
                }
                Long startTime = webinar.getStartTime();
                String startTimeStr = "";
                String endTimeStr = "";
                if (startTime != null) {
                    startTime = startTime / 1000;
                    startTimeStr = "$D_" + startTime;
                }
                Long endTime = webinar.getEndTime();
                if (endTime != null) {
                    endTime = endTime / 1000;
                    endTimeStr = "$D_" + endTime;
                }
                if (WebinarType.QUIZ.equals(webinar.getType())) {
                    eventData.put("quizStartTime", startTimeStr);
                    eventData.put("quizEndTime", endTimeStr);
                } else if (WebinarType.LIVECLASS.equals(webinar.getType())) {
                    eventData.put("liveClassStartTime", startTimeStr);
                    eventData.put("liveClassEndTime", endTimeStr);
                } else {
                    eventData.put("webinarStartTime", startTimeStr);
                    eventData.put("webinarEndTime", endTimeStr);
                }
                Map<String, Object> teacherInfo = webinar.getTeacherInfo();
                if (teacherInfo != null && teacherInfo.containsKey("name")) {
                    eventData.put("teacherName", teacherInfo.get("name"));
                }
            }
        } catch (Exception e) {
            logger.warn("error getting webinar Info for webinar registrationinfo:" + info + " error: " + e.getMessage());
        }
        event.setEvtData(eventData);
        return event;
    }

    public CleverTapEvent getWebinarAttendedEvent(WebinarUserRegistrationInfo info, String eventName) {
        CleverTapEvent event = getBasicEvent(info, eventName);
        Map<String, Object> eventData = new HashMap<>();
        eventData.put("webinarId", info.getWebinarId());
        eventData.put("joinLink", info.getRegisterJoinUrl());
        eventData.put("trainingId", info.getTrainingId());
        eventData.put("timeInSession", info.getTimeInSession());
        boolean attended = false;
        if (info.getTimeInSession() != null && info.getTimeInSession() > 0) {
            attended = true;
        }
        eventData.put("attended", attended);
        try {
            if (StringUtils.isNotEmpty(info.getWebinarId())) {
                Webinar webinar = webinarManager.getWebinarById(info.getWebinarId());
                if (webinar != null) {
                    eventData.put("webinarUrl", webinar.getWebinarCode());
                    eventData.put("webinarTitle", webinar.getTitle());
                    Long startTime = webinar.getStartTime();
                    String startTimeStr = "";
                    String endTimeStr = "";
                    if (startTime != null) {
                        startTime = startTime / 1000;
                        startTimeStr = "$D_" + startTime;
                    }
                    Long endTime = webinar.getEndTime();
                    if (endTime != null) {
                        endTime = endTime / 1000;
                        endTimeStr = "$D_" + endTime;
                    }
                    eventData.put("webinarStartTime", startTimeStr);
                    eventData.put("webinarEndTime", endTimeStr);
                    Map<String, Object> courseInfo = webinar.getCourseInfo();
                    if (courseInfo != null && courseInfo.containsKey("price")) {
                        Map<String, Object> price = (Map<String, Object>) (courseInfo.get("price"));
                        if (price != null && price.containsKey("purchaseUrl")) {
                            eventData.put("purchaseUrl", price.get("purchaseUrl"));
                        }
                        if (price != null && price.containsKey("registrationUrl")) {
                            eventData.put("registrationUrl", price.get("registrationUrl"));
                        }
                    }
                    if (courseInfo != null && courseInfo.containsKey("lightningDeal")) {
                        Map<String, Object> price = (Map<String, Object>) (courseInfo.get("lightningDeal"));
                        if (price != null && price.containsKey("purchaseUrl")) {
                            eventData.put("lightningDealPurchaseUrl", price.get("purchaseUrl"));
                        }
                        if (price != null && price.containsKey("registrationUrl")) {
                            eventData.put("lightningDealRegistrationUrl", price.get("registrationUrl"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.warn("error getting webinar Info for webinar registrationinfo:" + info + " error: " + e.getMessage());
        }
        event.setEvtData(eventData);
        return event;
    }

    public CleverTapEvent getBasicEvent(WebinarUserRegistrationInfo info, String eventName) {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(info.getUserId(),true);
        return getBasicEvent(info.getLastUpdated(), userBasicInfo.getContactNumber(), eventName);
    }

    public CleverTapEvent getBasicEvent(Long lastUpdated, String phone, String eventName) {
        Long ts = lastUpdated;
        if (ts == null) {
            ts = System.currentTimeMillis();
        }
        ts = ts / 1000;
        CleverTapEvent event = new CleverTapEvent(phone, eventName);
        event.setTs(ts);
        return event;
    }

    public void uploadEvent(CleverTapEvent event) {
        if (event == null) {
            return;
        }
        CleverTapEventReq req = new CleverTapEventReq(event);
//        if(info != null){
//            req.addEvent(getUserProfileEvent(info));
//        }
        ClientResponse respOTFBatch = WebUtils.INSTANCE.doCall(CLEVERTAP_SERVER_URL, HttpMethod.POST,
                gson.toJson(req), headers);
        String jsonRespOTFBatch = respOTFBatch.getEntity(String.class);
        logger.info(jsonRespOTFBatch);
        logger.info("Status code of response " + respOTFBatch.getStatus());
    }

}
