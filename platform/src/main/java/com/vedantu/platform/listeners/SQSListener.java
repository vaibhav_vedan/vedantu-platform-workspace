package com.vedantu.platform.listeners;

import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.managers.ExportManager;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.cms.WebinarManager;
import com.vedantu.platform.managers.dinero.PayoutManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.pojo.courseplan.BlockCoursePlanSlotsPojo;
import com.vedantu.platform.pojo.courseplan.BookCoursePlanSessionPojo;
import com.vedantu.platform.pojo.courseplan.ExportCoursePlanPojo;
import com.vedantu.platform.pojo.courseplan.ProcessInstallmentForCoursePlanPojo;
import com.vedantu.platform.pojo.courseplan.ResetCoursePlanSlotsPojo;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.pojo.QueueMessagePojo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ajith
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    PayoutManager payoutManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    StatsdClient statsdClient;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private ExportManager exportManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private final static Gson GSON = new Gson();

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            String queueName = ((Queue)textMessage.getJMSDestination()).getQueueName();
            logger.info("queueName "+queueName);
            Long startTime = System.currentTimeMillis();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(queueName, sqsMessageType, textMessage.getText());
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if(null != sqsMessageType) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (BadRequestException e){
            logger.warn("Error processing message ", e);
        }catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }

    public void handleMessage(String queueName, SQSMessageType sqsMessageType, String text) throws Exception {

        if (SQSQueue.OTO_POSTSESSION_OPS.getQueueName(env).equals(queueName)) {
            handleOtoPostSessionOps(sqsMessageType, text);
        } else if (SQSQueue.SEND_GPS_LOCALS_TO_LS.getQueueName(env).equals(queueName)) {
//            handleOtoPostSessionOps(sqsMessageType, text);
            handleGPSLocalsMessages(sqsMessageType, text);
        } else if (SQSQueue.COURSE_PLAN_PLATFORM_OPS.getQueueName(env).equals(queueName)) {
            handleCoursePlanOps(sqsMessageType, text);
        } else {
            QueueMessagePojo pojo = new QueueMessagePojo(sqsMessageType, text);
            handleMessages(queueName, pojo);
        }
        logger.info("Message handled");
    }

    private void handleCoursePlanOps(SQSMessageType sqsMessageType, String text) throws Exception {
        logger.info("got type - {} and message - {}", sqsMessageType, text);
        switch (sqsMessageType) {
            case COURSE_PLAN_BOOK_SESSIONS_TASK: {
                BookCoursePlanSessionPojo pojo = GSON.fromJson(text, BookCoursePlanSessionPojo.class);
                coursePlanManager.bookRegularSessions(pojo.getCoursePlanInfo(), pojo.getUserId(), pojo.getPaymentType());
                break;
            }
            case COURSE_PLAN_EXPORT: {
                ExportCoursePlanPojo pojo = GSON.fromJson(text, ExportCoursePlanPojo.class);
                exportManager.exportCoursePlans(pojo.getExportCoursePlansReq(), pojo.getSessionData());
                break;
            }
            case COURSE_PLAN_BLOCK_SLOTS: {
                BlockCoursePlanSlotsPojo pojo = GSON.fromJson(text, BlockCoursePlanSlotsPojo.class);
                coursePlanManager.blockCoursePlanSlots(pojo.getSessionSchedule(), pojo.getStudentId(),
                        pojo.getTeacherId(), pojo.getCoursePlanId());
                break;
            }
            case COURSE_PLAN_RESET_SLOTS: {
                ResetCoursePlanSlotsPojo pojo = GSON.fromJson(text, ResetCoursePlanSlotsPojo.class);
                coursePlanManager.updateBlockedInstalmentSlots(pojo.getCoursePlanId(), pojo.getEndTime());
                break;
            }
            case COURSE_PLAN_PROCESS_INSTALLMENT_PAYMENT: {
                ProcessInstallmentForCoursePlanPojo pojo = GSON.fromJson(text, ProcessInstallmentForCoursePlanPojo.class);
                coursePlanManager.processInstalmentPayment(pojo.getInstalmentInfo(), pojo.getInstalmentPaidUserId(), Boolean.TRUE);
                break;
            }
            case COURSE_PLAN_INCONSISTENCY_REPORT: {
                coursePlanManager.verifyCoursePlanHoursConsistency();
                break;
            }
            default:
                throw new IllegalStateException("Unknown Message Type " + sqsMessageType);

        }
    }

    private void handleOtoPostSessionOps(SQSMessageType sqsMessageType, String text) throws Exception{
        switch (sqsMessageType){
            case CALCULATE_SESSION_PAYOUT_POST_OTO_SESSION:
                SessionBillingDetails session = new Gson().fromJson(text, SessionBillingDetails.class);
                payoutManager.calculateSessionPayout(session);
                break;
            case UPDATE_LEAD_GPS_LEADSQUARED:
                leadSquaredManager.updateGPSLocalesToLSUsingSQS(text);
                break;
            default:
                throw new IllegalStateException("Unknown Message Type " + sqsMessageType);
        }
    }

    public void handleMessages(String queueName, QueueMessagePojo pojo) throws Exception {

        try {

            Long time = System.currentTimeMillis();
            logger.info("handleMessages is called at time : " + System.currentTimeMillis());
            String queueUrl = awsSQSManager.getQueueURL(queueName);

            List<QueueMessagePojo> pojos = new ArrayList<>();
            pojos.add(pojo);
            logger.info("queueurl is : " + queueUrl);
            ReceiveMessageRequest recieveMessageRequest = new ReceiveMessageRequest();

            recieveMessageRequest.setQueueUrl(queueUrl);
            recieveMessageRequest.setMaxNumberOfMessages(10);
            recieveMessageRequest.setWaitTimeSeconds(5);
            recieveMessageRequest.withMessageAttributeNames(Arrays.asList("messageType"));

            ReceiveMessageResult receiveMessageResponse
                    = awsSQSManager.recieveMessageFromQueueManual(recieveMessageRequest);

            logger.info("message result is  : " + receiveMessageResponse);

            if (receiveMessageResponse != null && ArrayUtils.isNotEmpty(receiveMessageResponse.getMessages())) {
                logger.info("message result is  : " + receiveMessageResponse.getMessages().size());
                for (com.amazonaws.services.sqs.model.Message message : receiveMessageResponse.getMessages()) {
                    logger.info("sqs message is " + message + " gap :" + message.toString());

                    try {
                        String messageType = message.getMessageAttributes().get("messageType").getStringValue();
                        logger.info("messageType is " + messageType);
                        SQSMessageType sqsMessageType = null;
                        if (StringUtils.isNotEmpty(messageType)) {

                            sqsMessageType = SQSMessageType.valueOf(messageType);
                        }
                        pojos.add(new QueueMessagePojo(sqsMessageType,message.getBody()));

                        logger.info("message body" + message.getBody());

                    } catch (Exception e) {
                        logger.error("Error processing message ", e);
                    }
                }

            }

            leadSquaredManager.handleMessagesFromLeadSquare(pojos);
            if (receiveMessageResponse != null && ArrayUtils.isNotEmpty(receiveMessageResponse.getMessages())) {
                for (com.amazonaws.services.sqs.model.Message message : receiveMessageResponse.getMessages()) {

                    try {
                        logger.info("Receipt handle is " + message.getReceiptHandle());
                        awsSQSManager.deleteMessageByHandle(queueUrl, message.getReceiptHandle());
                        logger.info("message deleted");

                    } catch (Exception e) {
                        logger.error("Error processing message " + message + " ..error : " , e);
                    }
                }

            }
            logger.info("totalTime is  : " + time + " to " + System.currentTimeMillis());

        }catch (Exception e){
            throw new BadRequestException(ErrorCode.SERVICE_ERROR,e.getMessage());
        }

    }

    public void handleGPSLocalsMessages(SQSMessageType sqsMessageType, String text) throws Exception {
        Long time = System.currentTimeMillis();
        logger.info("handleGPSLocalsMessages is called at time : " + System.currentTimeMillis());
        ReceiveMessageResult receiveMessageResponse = getBulkSQSQueueMessages(SQSQueue.SEND_GPS_LOCALS_TO_LS,10);
        List<QueueMessagePojo> pojos = createMessageTypeBodyMapping(receiveMessageResponse);
        QueueMessagePojo singlePojo = new QueueMessagePojo(sqsMessageType,text);
        pojos.add(singlePojo);
        leadSquaredManager.handleGPSLocalesMessagesFromLeadSquare(pojos);
        deleteBulkSQSQueueMessage(receiveMessageResponse,SQSQueue.SEND_GPS_LOCALS_TO_LS);
        logger.info("totalTime is  : " + time + " to " + System.currentTimeMillis());
    }

    public ReceiveMessageResult getBulkSQSQueueMessages(SQSQueue queue,Integer noOfMessage){
        if(noOfMessage == null){
            noOfMessage = 10;
        }
        Long time = System.currentTimeMillis();
        logger.info("handleMessages is called at time : " + System.currentTimeMillis());
        String queueUrl = awsSQSManager.getQueueURL(queue);
        logger.info("queueurl is : " + queueUrl);
        ReceiveMessageRequest recieveMessageRequest = new ReceiveMessageRequest();
        recieveMessageRequest.setQueueUrl(queueUrl);
        recieveMessageRequest.setMaxNumberOfMessages(noOfMessage);
        recieveMessageRequest.setWaitTimeSeconds(5);
        recieveMessageRequest.withMessageAttributeNames(Arrays.asList("messageType"));
        return awsSQSManager.recieveMessageFromQueueManual(recieveMessageRequest);
    }

    public List<QueueMessagePojo> createMessageTypeBodyMapping(ReceiveMessageResult receiveMessageResponse){
        List<QueueMessagePojo> pojos = new ArrayList<>();
        if (receiveMessageResponse != null && ArrayUtils.isNotEmpty(receiveMessageResponse.getMessages())) {
            logger.info("message result is  : " + receiveMessageResponse.getMessages().size());
            for (com.amazonaws.services.sqs.model.Message message : receiveMessageResponse.getMessages()) {
                logger.info("sqs message is " + message + " gap :" + message.toString());

                try {
                    String messageType = message.getMessageAttributes().get("messageType").getStringValue();
                    logger.info("messageType is " + messageType);
                    SQSMessageType sqsMessageType = null;
                    if (StringUtils.isNotEmpty(messageType)) {
                        sqsMessageType = SQSMessageType.valueOf(messageType);
                    }
                    pojos.add(new QueueMessagePojo(sqsMessageType,message.getBody()));

                    logger.info("message body" + message.getBody());

                } catch (Exception e) {
                    logger.error("Error processing message ", e);
                }
            }

        }
        logger.info("bulk messages pulled for gps locale "+pojos.size());
        return pojos;
    }

    public void deleteBulkSQSQueueMessage(ReceiveMessageResult receiveMessageResponse,SQSQueue queue){
        if (receiveMessageResponse != null && ArrayUtils.isNotEmpty(receiveMessageResponse.getMessages())) {
            String queueUrl = awsSQSManager.getQueueURL(queue);
            for (com.amazonaws.services.sqs.model.Message message : receiveMessageResponse.getMessages()) {
                try {
                    logger.info("Receipt handle is " + message.getReceiptHandle());
                    awsSQSManager.deleteMessageByHandle(queueUrl, message.getReceiptHandle());
                    logger.info("message deleted");

                } catch (Exception e) {
                    logger.error("Error processing message " + message + " ..error : " , e);
                }
            }
        }
    }


}

