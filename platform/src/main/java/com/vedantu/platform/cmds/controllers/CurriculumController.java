package com.vedantu.platform.cmds.controllers;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.lms.cmds.pojo.StudentsBatchTestInfoWithAttendance;
import com.vedantu.lms.cmds.pojo.TestInfoForBatchProgress;
import com.vedantu.lms.cmds.request.AddContentCurriculumReq;
import com.vedantu.lms.cmds.request.EditFromCourseToBatchRes;
import com.vedantu.lms.cmds.request.AddCurriculumReq;
import com.vedantu.lms.cmds.request.ChangeCurriculumStatusReq;
import com.vedantu.lms.cmds.request.CreateBatchCurriculumReq;
import com.vedantu.lms.cmds.request.AddOrDeleteNodeReq;
import com.vedantu.lms.cmds.request.EditCurriculumReq;
import com.vedantu.lms.cmds.request.GetCurriculumReq;
import com.vedantu.lms.cmds.request.ShareCurriculumContentReq;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.onetofew.pojo.GetBatchesRes;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.platform.cmds.managers.CurriculumManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("/cmds/curriculum")
public class CurriculumController {
	
	@Autowired
	private CurriculumManager curriculumManager;
	
	@Autowired
	private OTFManager otfManager;

	@Autowired
	private LogFactory logFactory;
	
	@Autowired
    HttpSessionUtils sessionUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CurriculumController.class);
	
	@RequestMapping(value = "/createCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	@Deprecated
    public Node createCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException {
		sessionUtils.checkIfAllowedList(addCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.createCurriculum(addCurriculumReq);
    }
	

	@RequestMapping(value = "/getCurriculumByEntity", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Node> getCurriculumByEntity(GetCurriculumReq getCurriculumReq) throws VException {
        return curriculumManager.getCurriculumByEntity(getCurriculumReq);
    }
	
	
	@RequestMapping(value = "/editCourseCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	@Deprecated
    public Node editCourseCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException {
		sessionUtils.checkIfAllowedList(addCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.editCourseCurriculum(addCurriculumReq);
    }
	
	@RequestMapping(value = "/createBatchCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void createBatchCurriculum(@RequestBody CreateBatchCurriculumReq createBatchCurriculumReq) throws VException {
		sessionUtils.checkIfAllowedList(createBatchCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		curriculumManager.createBatchCurriculum(createBatchCurriculumReq);
    }
	
//	@RequestMapping(value = "/editMyCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public Node editMyCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException {
//        return curriculumManager.editMyCurriculum(addCurriculumReq);
//    }

	@RequestMapping(value = "/changeCurriculumStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node changeCurriculumStatus(@RequestBody ChangeCurriculumStatusReq changeCurriculumStatusReq) throws VException {
		sessionUtils.checkIfAllowedList(changeCurriculumStatusReq.getCallingUserId(), Arrays.asList(Role.TEACHER), Boolean.TRUE);
		return curriculumManager.changeCurriculumStatus(changeCurriculumStatusReq);
    }
	
	@RequestMapping(value = "/changeAnyCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node changeAnyCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException {
		sessionUtils.checkIfAllowedList(addCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.changeAnyCurriculum(addCurriculumReq);
    }
	
    @RequestMapping(value = "/shareContentForCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse shareContentForCurriculum(@RequestBody ShareCurriculumContentReq shareCurriculumContentReq) throws VException, MalformedURLException {
		sessionUtils.checkIfAllowedList(shareCurriculumContentReq.getCallingUserId(), Arrays.asList(Role.TEACHER), Boolean.TRUE);
		return curriculumManager.shareContentForCurriculum(shareCurriculumContentReq);
    }
	
	@RequestMapping(value = "/getStudentAttendence", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public String getStudentAttendence(StudentBatchProgressReq req) throws VException{
		if(req.getContextType().equals(CurriculumEntityName.OTF_BATCH)){
			return otfManager.getStudentAttendenceCountWithTotal(req);
		}
		else if(req.getContextType().equals(CurriculumEntityName.COURSE_PLAN)){
			
		}
		return null;
	}
	
	@RequestMapping(value = "/getTestInfoForUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public TestInfoForBatchProgress getTestInfoForUser(StudentBatchProgressReq req) throws VException{

		return curriculumManager.getTestInfoForUser(req);
	}

	// TODO Removal
	@Deprecated
	@RequestMapping(value = "/getBatchTestInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudentsBatchTestInfoWithAttendance> getBatchTestInfo(TeacherBatchProgressReq req) throws VException{
		return new ArrayList<>();
//		return curriculumManager.getBatchTestInfo(req);
	}
	
	@RequestMapping(value = "/getPastSessionAnalysis", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFSessionInfo> getPastSessionAnalysis(TeacherBatchProgressReq req) throws VException{

		return otfManager.getPastSessionsForBatchProgress(req);
	}
	
	@RequestMapping(value = "/addNodeInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public Node addNodeInCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException{
		sessionUtils.checkIfAllowedList(addCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.addNodeInCurriculum(addCurriculumReq);
	}
	
	@RequestMapping(value = "/deleteNodeInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	@Deprecated
	public Node deleteNodeInCurriculum(@RequestBody AddOrDeleteNodeReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.deleteNodeInCurriculum(req);
	}
	
	@RequestMapping(value = "/addContentToNode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node addContentToNode(@RequestBody AddContentCurriculumReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN,Role.TEACHER), Boolean.TRUE);
		return curriculumManager.addContentToNode(req);
	}
	
	@RequestMapping(value = "/addContentToCourseAndBatchNodes", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditFromCourseToBatchRes addContentToCourseAndBatchNodes(@RequestBody AddContentCurriculumReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.addContentToCourseAndBatchNodes(req);
	}
	
	@RequestMapping(value = "/editNodeInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node editNodeInCurriculum(@RequestBody EditCurriculumReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN,Role.TEACHER), Boolean.TRUE);
		return curriculumManager.editNodeInCurriculum(req);
	}

	@RequestMapping(value = "/getListOfBatchesToInheritCourse", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public GetBatchesRes getListOfBatchesToInheritCourse(EditCurriculumReq req) throws VException{
		return curriculumManager.getListOfBatchesToInheritCourse(req);
	}
	
	@RequestMapping(value = "/editNodeInCourseAndBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	@Deprecated
    public EditFromCourseToBatchRes editNodeInCourseAndBatch(@RequestBody EditCurriculumReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.editNodeInCourseAndBatch(req);
	}
	
	@RequestMapping(value = "/deleteNodeInCourseAndBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	@Deprecated
	public PlatformBasicResponse deleteNodeInCourseAndBatch(@RequestBody AddOrDeleteNodeReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		curriculumManager.deleteNodeInCourseAndBatch(req);
		return new PlatformBasicResponse();
	}
	
	@RequestMapping(value = "/addNodeInCourseAndBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	@Deprecated
    public EditFromCourseToBatchRes addNodeInCourseAndBatch(@RequestBody AddOrDeleteNodeReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.addNodeInCourseAndBatch(req);
	}
	
	@RequestMapping(value = "/createCurriculumForAllBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public PlatformBasicResponse createCurriculumForAllBatch(@RequestBody AddCurriculumReq req) throws VException{
		sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		if(StringUtils.isEmpty(req.getEntityId())){
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"entityId is null");
		}
		curriculumManager.createCurriculumForAllBatch(req.getEntityId());
		return new PlatformBasicResponse();
	}
}
