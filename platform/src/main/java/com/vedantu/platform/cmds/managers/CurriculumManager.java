package com.vedantu.platform.cmds.managers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.lms.cmds.pojo.StudentTestBasicInfo;
import com.vedantu.lms.cmds.pojo.StudentsBatchTestInfoWithAttendance;
import com.vedantu.lms.cmds.pojo.TestInfoForBatchProgress;
import com.vedantu.lms.cmds.request.AddContentCurriculumReq;
import com.vedantu.lms.cmds.request.EditFromCourseToBatchRes;
import com.vedantu.lms.cmds.request.AddCurriculumReq;
import com.vedantu.lms.cmds.request.ChangeCurriculumStatusReq;
import com.vedantu.lms.cmds.request.CreateBatchCurriculumReq;
import com.vedantu.lms.cmds.request.AddOrDeleteNodeReq;
import com.vedantu.lms.cmds.request.EditCurriculumReq;
import com.vedantu.lms.cmds.request.GetCurriculumReq;
import com.vedantu.lms.cmds.request.ShareContentAndSendMailRes;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.lms.cmds.request.ShareCurriculumContentReq;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.GetBatchesRes;
import com.vedantu.onetofew.pojo.GetTeacherAndStudentIdsRes;
import com.vedantu.onetofew.pojo.OTFAttendance;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.review.RemarkManager;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.util.Arrays;

@Service
public class CurriculumManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private RemarkManager remarkManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    CommunicationManager communicationManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CurriculumManager.class);

    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");

    private static Gson gson = new Gson();

    @Deprecated
    public Node createCurriculum(AddCurriculumReq addCurriculumReq) throws VException {
        addCurriculumReq.verify();
        List<String> errors = validateBoardIds(addCurriculumReq.getEntityName(), addCurriculumReq.getEntityId(), addCurriculumReq.getNode());
        if (ArrayUtils.isNotEmpty(errors)) {
            throw new BadRequestException(ErrorCode.INVALID_BOARDID, errors.toString());
        }
        logger.info("Adding Curriculum for courseId " + addCurriculumReq.getEntityId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/createCurriculum", HttpMethod.POST,
                gson.toJson(addCurriculumReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;
    }

    public Node getBatchCurriculum(String batchId) {

        if (StringUtils.isEmpty(batchId)) {
            return null;
        }
        GetCurriculumReq getCurriculumReq = new GetCurriculumReq();
        getCurriculumReq.setEntityName(CurriculumEntityName.OTF_BATCH);
        getCurriculumReq.addEntityId(batchId);
        try {
            return getCurriculumByEntity(getCurriculumReq).get(0);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("curriculum does not exist for " + batchId);
            return null;
        }
    }

    public List<Node> getCurriculumByEntity(GetCurriculumReq getCurriculumReq) throws VException {
        if (StringUtils.isNotEmpty(getCurriculumReq.getEntityId())) {
            getCurriculumReq.addEntityId(getCurriculumReq.getEntityId());
        }
        getCurriculumReq.verify();
        // sessionId and sessionType
        if (ArrayUtils.isEmpty(getCurriculumReq.getEntityIds())
                && StringUtils.isNotEmpty(getCurriculumReq.getSessionId())) {
            if (getCurriculumReq.getSessionType().equals(SessionType.OTF)) {
                OTFSessionPojoUtils session = otfManager.getSessionById(getCurriculumReq.getSessionId());
                if (session != null && ArrayUtils.isNotEmpty(session.getBatchIds())) {
                    getCurriculumReq.setEntityIds(new ArrayList<>(session.getBatchIds()));
                    getCurriculumReq.setEntityName(CurriculumEntityName.OTF_BATCH);
                } else {
                    throw new VException(ErrorCode.SESSION_NOT_FOUND,
                            "Could not find batch for seesionId " + getCurriculumReq.getSessionId());
                }
            }
        }

        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getCurriculumReq);
        logger.info("Getting Curriculum for entity " + getCurriculumReq.getEntityName() + " and Id "
                + getCurriculumReq.getEntityIds());
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(LMS_ENDPOINT + "/curriculum/getCurriculumByEntity?" + queryString, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Node>>() {
        }.getType();
        List<Node> nodes = gson.fromJson(json, listType);
        return nodes;
    }

    @Deprecated
    public Node editCourseCurriculum(AddCurriculumReq addCurriculumReq) throws VException {
        addCurriculumReq.verify();
        logger.info("Editing Curriculum for courseId " + addCurriculumReq.getEntityId());
        List<String> errors = validateBoardIds(addCurriculumReq.getEntityName(), addCurriculumReq.getEntityId(), addCurriculumReq.getNode());
        if (ArrayUtils.isNotEmpty(errors)) {
            throw new BadRequestException(ErrorCode.INVALID_BOARDID, errors.toString());
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/editCourseCurriculum",
                HttpMethod.POST, gson.toJson(addCurriculumReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;
    }

    public void createBatchCurriculum(CreateBatchCurriculumReq createBatchCurriculumReq) throws VException {
        createBatchCurriculumReq.verify();
        logger.info("Adding Curriculum for batchId " + createBatchCurriculumReq.getEntityId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/createBatchCurriculum",
                HttpMethod.POST, gson.toJson(createBatchCurriculumReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
    }

    public Node editMyCurriculum(AddCurriculumReq addCurriculumReq) throws VException {
        addCurriculumReq.verify();
        logger.info("Editing My Curriculum for courseId " + addCurriculumReq.getEntityId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/editMyCurriculum", HttpMethod.POST,
                gson.toJson(addCurriculumReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;

    }

    public Node changeCurriculumStatus(ChangeCurriculumStatusReq changeCurriculumStatusReq) throws VException {
        changeCurriculumStatusReq.verify();

        if (changeCurriculumStatusReq.getNewStatus() != null
                && changeCurriculumStatusReq.getNewStatus().equals(CurriculumStatus.DONE)
                && changeCurriculumStatusReq.getStartTime() != null) {
            List<String> sessionIds = null;
            if (changeCurriculumStatusReq.getContextType() != null
                    && changeCurriculumStatusReq.getContextType().equals(CurriculumEntityName.OTF_BATCH)
                    && changeCurriculumStatusReq.getContextId() != null) {
                Long presenter;
                if (changeCurriculumStatusReq.getTeacherId() != null) {
                    presenter = changeCurriculumStatusReq.getTeacherId();
                } else {
                    presenter = changeCurriculumStatusReq.getCallingUserId();
                }

                sessionIds = getOTFSessionIdsForCurriculum(changeCurriculumStatusReq.getStartTime(),
                        System.currentTimeMillis(), presenter, changeCurriculumStatusReq.getContextId());

            }
            // TODO: forOTO
            // else if()
            if (ArrayUtils.isNotEmpty(sessionIds)) {
                if (ArrayUtils.isEmpty(changeCurriculumStatusReq.getSessionIds())) {
                    changeCurriculumStatusReq.setSessionIds(new ArrayList<>());
                }
                changeCurriculumStatusReq.getSessionIds().addAll(sessionIds);
            }
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/changeCurriculumStatus",
                HttpMethod.POST, gson.toJson(changeCurriculumStatusReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node nodeResp = gson.fromJson(json, Node.class);
        return nodeResp;

    }

    public List<String> getOTFSessionIdsForCurriculum(Long startTime, Long endTime, Long teacherId, String batchId) {

        try {
            String ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT") + "/onetofew/";
            ClientResponse resp = WebUtils.INSTANCE
                    .doCall(ontofewEndpoint + "/session/getSessionIdsForCurriculum?startTime=" + startTime + "&endTime="
                            + endTime + "&teacherId=" + teacherId + "&batchId=" + batchId, HttpMethod.GET, null);

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);

            String json = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<String>>() {
            }.getType();
            List<String> sessionIds = gson.fromJson(json, listType);
            return sessionIds;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("sessionIds not found. Error Found " + e);
            return null;
        }
    }

    public PlatformBasicResponse shareContentForCurriculum(ShareCurriculumContentReq req) throws VException {

        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/shareContentForCurriculumForBatch",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return new PlatformBasicResponse();
//        if (response != null && response.getNode() != null) {
//            if (ArrayUtils.isNotEmpty(response.getMoodleContentInfos())) {
//                Map<String, Object> payload2 = new HashMap<String, Object>();
//                payload2.put("shareContentAndSendMailRes", response);
//                AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.CONTENT_SHARED_CURRICULUM, payload2);
//                asyncTaskFactory.executeTask(params2);
//            }
//            return response.getNode();
//        } else {
//            throw new NotFoundException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "Node not found");
//        }

    }

    public GetTeacherAndStudentIdsRes getTeachersAndStudentsInBatch(String id) throws VException {
        String ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
        String url = ontofewEndpoint + "batch/getTeachersAndStudentsInBatch?batchId=" + id;

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        GetTeacherAndStudentIdsRes getTeacherAndStudentIdsRes = gson.fromJson(jsonString,
                GetTeacherAndStudentIdsRes.class);
        return getTeacherAndStudentIdsRes;
    }

    public Node changeAnyCurriculum(AddCurriculumReq addCurriculumReq) throws VException {
        addCurriculumReq.verify();
        logger.info("Editing My Curriculum for courseId " + addCurriculumReq.getEntityId());
        List<String> errors = validateBoardIds(addCurriculumReq.getEntityName(), addCurriculumReq.getEntityId(), addCurriculumReq.getNode());
        if (ArrayUtils.isNotEmpty(errors)) {
            throw new BadRequestException(ErrorCode.INVALID_BOARDID, errors.toString());
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/changeAnyCurriculum",
                HttpMethod.POST, gson.toJson(addCurriculumReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;

    }

    public TestInfoForBatchProgress getTestInfoForUser(StudentBatchProgressReq req) throws VException {
        req.verify();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(LMS_ENDPOINT + "/cmds/contentInfo/getTestInfoForStudent?" + queryString, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        TestInfoForBatchProgress testInfoForBatchProgress = gson.fromJson(json, TestInfoForBatchProgress.class);
        return testInfoForBatchProgress;

    }

    // TODO Removal
    @Deprecated
    public List<StudentsBatchTestInfoWithAttendance> getBatchTestInfo(TeacherBatchProgressReq req) throws VException {
        req.verify();
        List<String> studentIds = new ArrayList<>();

        GetTeacherAndStudentIdsRes getTeacherAndStudentIdsRes = getTeachersAndStudentsInBatch(req.getContextId());
        if (getTeacherAndStudentIdsRes != null) {
            if (ArrayUtils.isNotEmpty(getTeacherAndStudentIdsRes.getStudentIds())) {
                for (Long studentId : getTeacherAndStudentIdsRes.getStudentIds()) {
                    studentIds.add(studentId.toString());
                }

            }
        }
        if (ArrayUtils.isEmpty(studentIds)) {
            throw new NotFoundException(ErrorCode.BATCH_ENROLLMENT_NOT_FOUND,
                    "Could Not find students for  : " + req.getContextId());
        }

        if (studentIds.size() >= 30) {
            studentIds = studentIds.subList(0, 30);
        }

        req.setStudentIds(studentIds);
        List<OTFAttendance> attendances = otfManager.getAttendanceForStudentsAndBoard(req);
        Map<String, List<StudentTestBasicInfo>> contentMap = getBatchTestScoreInfo(req);
        if (ArrayUtils.isNotEmpty(attendances)) {
            if (attendances.get(0) != null && attendances.get(0).getTeacherId() != null) {
                req.setTeacherId(attendances.get(0).getTeacherId());
            }

        }

        Map<Long, RemarkResp> remarkMap = remarkManager.getBatchLevelRemarksByTeacher(req);
        if (contentMap == null) {
            contentMap = new HashMap<>();
        }
        if (remarkMap == null) {
            remarkMap = new HashMap<>();
        }
        List<StudentsBatchTestInfoWithAttendance> resps = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(attendances)) {
            for (OTFAttendance attendance : attendances) {
                StudentsBatchTestInfoWithAttendance resp = new StudentsBatchTestInfoWithAttendance();
                resp.setUserId(attendance.getStudentId());
                resp.setAttended(attendance.getAttended());
                resp.setTotalAttendance(attendance.getTotal());
                resp.setTestScores(contentMap.get(attendance.getStudentId().toString()));
                if (remarkMap.get(attendance.getStudentId()) != null) {
                    resp.setRemark(remarkMap.get(attendance.getStudentId()).getRemark());
                    resp.setRemarkCreatedOn(remarkMap.get(attendance.getStudentId()).getCreationTime());
                }
                resps.add(resp);
            }
        }
        return resps;
    }

    public Map<String, List<StudentTestBasicInfo>> getBatchTestScoreInfo(TeacherBatchProgressReq req)
            throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(LMS_ENDPOINT + "/cmds/contentInfo/getBatchTestScoreInfo?" + queryString, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Type otfMapType = new TypeToken<Map<String, List<StudentTestBasicInfo>>>() {
        }.getType();

        Map<String, List<StudentTestBasicInfo>> contentMap = gson.fromJson(json, otfMapType);
        return contentMap;
    }

    public Node addNodeInCurriculum(AddCurriculumReq addCurriculumReq) throws VException {
        // addCurriculumReq.verify();
        if (addCurriculumReq.getNode() == null) {
            throw new BadRequestException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "Request does not contain Node");
        } else if (StringUtils.isEmpty(addCurriculumReq.getNode().getParentId())) {
            throw new BadRequestException(ErrorCode.CURRICULUM_NODE_NOT_FOUND,
                    "Request does not contain Parent Node Id");
        }
        logger.info("Adding Node in Curriculum for parentId :" + addCurriculumReq.getNode().getParentId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/addNodeInCurriculum",
                HttpMethod.POST, gson.toJson(addCurriculumReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;
    }

    public Node addContentToNode(AddContentCurriculumReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/addContentToNode", HttpMethod.POST,
                gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;
    }

    public EditFromCourseToBatchRes addContentToCourseAndBatchNodes(AddContentCurriculumReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/addContentToCourseAndBatchNodes",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        EditFromCourseToBatchRes response = gson.fromJson(json, EditFromCourseToBatchRes.class);
        return response;
    }

    @Deprecated
    public Node deleteNodeInCurriculum(AddOrDeleteNodeReq req) throws VException {
        req.verify();
        if (StringUtils.isEmpty(req.getNode().getId()) || StringUtils.isEmpty(req.getNode().getContextId())
                || req.getNode().getContextType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "nodeId is Null or contextId is null");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/deleteNodeInCurriculum",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node node = gson.fromJson(json, Node.class);
        return node;
    }

    public Node editNodeInCurriculum(EditCurriculumReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/editNodeInCurriculum",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        Node response = gson.fromJson(json, Node.class);
        return response;
    }

    public void shareContentOnEnrollment(ShareContentEnrollmentReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/shareContentOnEnrollment",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    @Deprecated
    public EditFromCourseToBatchRes editNodeInCourseAndBatch(EditCurriculumReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/editNodeInCourseAndBatch",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        EditFromCourseToBatchRes response = gson.fromJson(json, EditFromCourseToBatchRes.class);
        return response;
    }

    public GetBatchesRes getListOfBatchesToInheritCourse(EditCurriculumReq req) throws VException {
        req.verify();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                LMS_ENDPOINT + "/curriculum/getListOfBatchesToInheritCourse?" + queryString, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        EditFromCourseToBatchRes response = gson.fromJson(json, EditFromCourseToBatchRes.class);
        if (ArrayUtils.isNotEmpty(response.getBatchIds()) && StringUtils.isNotEmpty(response.getBatchIds().get(0))) {

            GetBatchesRes getBatches = otfManager.getBasicInfosForAllBatchFromBatchId(response.getBatchIds().get(0));
            if (ArrayUtils.isNotEmpty(getBatches.getBatches())) {
                for (BatchBasicInfo basicInfo : getBatches.getBatches()) {
                    if (StringUtils.isNotEmpty(basicInfo.getBatchId())
                            && response.getBatchIds().contains(basicInfo.getBatchId())) {
                        basicInfo.setCurriculumMap(Boolean.TRUE);
                    } else {
                        basicInfo.setCurriculumMap(Boolean.FALSE);
                    }
                }
            }
            return getBatches;
        }

        return new GetBatchesRes();
    }

    @Deprecated
    public void deleteNodeInCourseAndBatch(AddOrDeleteNodeReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/deleteNodeInCourseAndBatch",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
    }

    @Deprecated
    public EditFromCourseToBatchRes addNodeInCourseAndBatch(AddOrDeleteNodeReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/addNodeInCourseAndBatch",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        EditFromCourseToBatchRes response = gson.fromJson(json, EditFromCourseToBatchRes.class);
        return response;
    }

    public List<String> checkIfCurriculumExists(List<String> contextIds) {
        List<String> ids = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(contextIds)) {
            String url = LMS_ENDPOINT + "/curriculum/checkIfCurriculumExists?contextIds=";

            url += contextIds.get(0);

            for (int i = 1; i < contextIds.size(); i++) {
                if (StringUtils.isNotEmpty(contextIds.get(i))) {
                    url += "&contextIds=" + contextIds.get(i);
                }
            }
            try {
                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String json = resp.getEntity(String.class);
                Type listType1 = new TypeToken<ArrayList<String>>() {
                }.getType();
                List<String> response = gson.fromJson(json, listType1);
                return response;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.info("error in checking if curriculum exists. Logs " + e);
            }
        }
        return ids;
    }

    public void createCurriculumForAllBatch(String courseId) throws VException {
        GetBatchesRes batches = otfManager.getBatchBasicInfosFromCourse(courseId);
        Long time2 = new Long(DateTimeUtils.MILLIS_PER_MINUTE * 2);
        int counter = 0;
        int threadSize = 15;
        if (ArrayUtils.isNotEmpty(batches.getBatches())) {
            for (BatchBasicInfo basicInfo : batches.getBatches()) {
                if (StringUtils.isNotEmpty(basicInfo.getBatchId())) {
                    CreateBatchCurriculumReq req = new CreateBatchCurriculumReq();
                    req.setEntityId(basicInfo.getBatchId());
                    req.setEntityName(CurriculumEntityName.OTF_BATCH);
                    req.setParentEntityId(courseId);
                    req.setEntityName(CurriculumEntityName.OTF_COURSE);
                    Map<String, Object> payload2 = new HashMap<String, Object>();
                    payload2.put("createBatchCurriculumReq", req);
                    Long timeDelay = time2 * (counter / threadSize);
                    AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.CREATE_BATCH_CURRICULUM, payload2, timeDelay);
                    asyncTaskFactory.executeTask(params2);
                    counter++;
                }
            }
        }
    }

    public void getAllBoardsForTree(Node node, Set<Long> boardIds) {
        if (node.getBoardId() != null) {
            boardIds.add(node.getBoardId());
        }
        if (ArrayUtils.isNotEmpty(node.getNodes())) {
            for (Node childNode : node.getNodes()) {
                getAllBoardsForTree(childNode, boardIds);
            }
        }
    }

    public List<String> validateBoardIds(CurriculumEntityName entityName, String entityId, Node validateNode) {
        List<String> errors = new ArrayList<>();
        Set<Long> boardIds = new HashSet<>();
        if (CurriculumEntityName.OTF_COURSE.equals(entityName) && StringUtils.isNotEmpty(entityId)) {
            try {
                CourseInfo courseInfo = otfManager.getCourse(entityId);
                if (courseInfo != null && ArrayUtils.isNotEmpty(courseInfo.getBoardTeacherPairs())) {
                    for (BoardTeacherPair pair : courseInfo.getBoardTeacherPairs()) {
                        if (pair != null && pair.getBoardId() != null) {
                            boardIds.add(pair.getBoardId());
                        }
                    }
                }
                return validateBoardIds(validateNode, boardIds);
            } catch (VException e) {
                errors.add("Exception occured in getting Course");
                logger.error("Exception occured in getting Course : " + e);

            }
        } else if (CurriculumEntityName.OTF_BATCH.equals(entityName) && StringUtils.isNotEmpty(entityId)) {
            try {
                BatchBasicInfo batchInfo = enrollmentManager.getBatch(entityId);
                if (batchInfo != null && ArrayUtils.isNotEmpty(batchInfo.getBoardTeacherPairs())) {
                    for (BoardTeacherPair pair : batchInfo.getBoardTeacherPairs()) {
                        if (pair != null && pair.getBoardId() != null) {
                            boardIds.add(pair.getBoardId());
                        }
                    }
                }
                return validateBoardIds(validateNode, boardIds);
            } catch (VException e) {
                errors.add("Exception occured in getting Batch");
                logger.error("Exception occured in getting batch : " + e);

            }
        }
        return errors;
    }

    public List<String> validateBoardIds(Node validateNode, Set<Long> courseBoards) throws BadRequestException {
        Set<Long> boardIds = new HashSet<>();
        List<String> errors = new ArrayList<>();
        List<String> invalidBoard = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(validateNode.getNodes())) {
            for (Node node : validateNode.getNodes()) {
                if (node.getBoardId() == null) {
                    errors.add("boardId not Present.");
                } else if (courseBoards == null || !courseBoards.contains(node.getBoardId())) {
                    errors.add("boardId " + node.getBoardId() + " not Present in course.");
                }
                boardIds.add(node.getBoardId());
            }
            if (ArrayUtils.isNotEmpty(errors)) {
                return errors;
            }
            // getAllBoardsForTree(validateNode,boardIds);
            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            for (Long id : boardIds) {
                if (!boardMap.containsKey(id)) {
                    if (id != null) {
                        invalidBoard.add(id.toString());
                    }
                }
            }
        }
        if (ArrayUtils.isNotEmpty(invalidBoard)) {
            errors.add(ErrorCode.INVALID_BOARDID.name());
            for (String id : invalidBoard) {
                errors.add(id);
            }
        }
        return errors;
    }
}
