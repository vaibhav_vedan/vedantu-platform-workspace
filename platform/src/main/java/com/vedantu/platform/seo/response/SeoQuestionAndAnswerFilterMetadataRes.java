package com.vedantu.platform.seo.response;

import lombok.Data;

@Data
public class SeoQuestionAndAnswerFilterMetadataRes {

    private String target;
    private String subject;
    private String grade;
    private String topic;
    private String question;
    private String url;

    public static class Constants{
        public static final String TARGET = "target";
        public static final String SUBJECT = "subject";
        public static final String GRADE = "grade";
    }
}
