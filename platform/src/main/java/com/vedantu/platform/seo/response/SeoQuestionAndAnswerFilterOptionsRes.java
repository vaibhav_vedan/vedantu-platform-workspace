package com.vedantu.platform.seo.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
public class SeoQuestionAndAnswerFilterOptionsRes {

    private List<String> targets;
    private List<SeoQuestionAndAnswerFilterSubjectTopicsPojo> subjects = new ArrayList<>();

    public void addSubject(SeoQuestionAndAnswerFilterSubjectTopicsPojo subject){

        subjects.add(subject);

    }
}
