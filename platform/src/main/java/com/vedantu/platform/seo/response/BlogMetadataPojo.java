package com.vedantu.platform.seo.response;

import lombok.Data;

@Data
public class BlogMetadataPojo {
    private String id;
    private String featureImageUrl;
    private String featureImageTag;
    private String title;
    private String description;
    private String url;
}
