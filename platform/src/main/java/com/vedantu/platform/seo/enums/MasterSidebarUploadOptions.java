package com.vedantu.platform.seo.enums;

public enum MasterSidebarUploadOptions {
    ADD, IGNORE, OVERRIDE
}
