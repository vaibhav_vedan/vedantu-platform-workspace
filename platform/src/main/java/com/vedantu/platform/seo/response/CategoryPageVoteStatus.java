package com.vedantu.platform.seo.response;

import lombok.Data;

@Data
public class CategoryPageVoteStatus {

    private Boolean isUserVoted = false;
    private Long totalVotes;
}
