package com.vedantu.platform.seo.enums;

public enum BlogPageType {

    BLOG_HOME_PAGE, BLOG_CATEGORY_PAGE
}
