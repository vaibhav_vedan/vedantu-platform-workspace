package com.vedantu.platform.seo.entity;

import com.vedantu.platform.seo.enums.SeoDomain;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;

/*
 * Base Entity class for all incoming requests
 */
@Getter
@Setter
public class AbstractSeoEntity extends AbstractMongoStringIdEntity {
	private boolean active = true;

	@Indexed(background = true)
	private SeoDomain domain = SeoDomain.VEDANTU; // Default is Vedantu

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = Boolean.TRUE.equals(active);
	}

	public AbstractSeoEntity() {
		super();
	}

	public AbstractSeoEntity(Long creationTime, String createdBy, Long lastUpdated, String callingUserId,
			String lastUpdatedBy) {
		super(creationTime, createdBy, lastUpdated, lastUpdatedBy);

	}

	public AbstractSeoEntity(Long creationTime, String createdBy, Long lastUpdated, String callingUserId) {
		super(creationTime, createdBy, lastUpdated, callingUserId);
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String Active = "active";
		public static final String DOMAIN = "domain";
	}
}
