package com.vedantu.platform.seo.entity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

public class CategoryPageHeader {
    private String title;
    private String subTitle;
    private String description;
    private ArrayList<Map<String, String>> links;

    public CategoryPageHeader() {}

    public CategoryPageHeader(String title, String subTitle, String description, ArrayList<Map<String, String>> links) {
        this.title = title;
        this.subTitle = subTitle;
        this.description = description;
        this.links = links;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Map<String, String>> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<Map<String, String>> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}