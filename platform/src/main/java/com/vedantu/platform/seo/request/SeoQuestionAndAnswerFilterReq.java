package com.vedantu.platform.seo.request;

import lombok.Data;

import java.util.List;

@Data
public class SeoQuestionAndAnswerFilterReq {

    private List<String> targets;
    private List<String> subjects;
    private List<String> grades;
    private List<String> topics;
    private Integer start = 0;
    private Integer size = 10;
}
