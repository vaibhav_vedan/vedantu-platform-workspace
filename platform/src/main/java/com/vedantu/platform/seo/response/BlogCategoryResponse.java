package com.vedantu.platform.seo.response;

import com.vedantu.platform.seo.pojo.BlogArticlePojo;
import lombok.Data;

import java.util.List;

@Data
public class BlogCategoryResponse {

    private String id;
    private String name;
    private Long totalBlogs;
    private List<BlogMetadataPojo> blogs;
    private BlogArticlePojo article;

    public static class Builder{

        private BlogCategoryResponse blogCategoryResponse;

        public Builder(){

            blogCategoryResponse = new BlogCategoryResponse();
        }

        public Builder id(String id){
            blogCategoryResponse.id = id;
            return this;
        }

        public Builder name(String name){
            blogCategoryResponse.name = name;
            return this;
        }

        public Builder totalBlogs(Long totalBlogs){
            blogCategoryResponse.totalBlogs = totalBlogs;
            return this;
        }

        public Builder blogs(List<BlogMetadataPojo> blogs){
            blogCategoryResponse.blogs = blogs;
            return this;
        }

        public Builder article(BlogArticlePojo article){
            blogCategoryResponse.article = article;
            return this;
        }

        public BlogCategoryResponse build(){
            return blogCategoryResponse;
        }


    }
}
