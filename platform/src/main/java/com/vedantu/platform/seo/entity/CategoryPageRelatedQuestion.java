package com.vedantu.platform.seo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryPageRelatedQuestion {
    private String question;
    private String url;
}
