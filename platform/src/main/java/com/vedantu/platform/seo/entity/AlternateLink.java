package com.vedantu.platform.seo.entity;

import com.google.gson.Gson;

/**
 *
 * @author manishkumarsingh
 */
public class AlternateLink {
    private String hreflang;
    private String href;

    public String getHreflang() {
        return hreflang;
    }

    public void setHreflang(String hreflang) {
        this.hreflang = hreflang;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
    
    @Override
    public String toString() {
            return (new Gson()).toJson(this);
    }
}
