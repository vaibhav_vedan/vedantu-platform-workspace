package com.vedantu.platform.seo.enums;


import com.vedantu.util.ConfigUtils;

public enum SeoDomain {
    VEDANTU(ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket"), ConfigUtils.INSTANCE.getStringValue("seo.default.domain")),
    ACADSTACK(ConfigUtils.INSTANCE.getStringValue("aws.seo.acadstack.bucket"), ConfigUtils.INSTANCE.getStringValue("seo.acadstack.domain")),
    ;


    private String awsBucket;
    private String domainUrl;

    SeoDomain(String awsBucket, String domainUrl) {
        this.awsBucket = awsBucket;
        this.domainUrl = domainUrl;
    }

    public String getAwsBucket() {
        return awsBucket;
    }

    public String getDomainUrl() {
        return domainUrl;
    }

}
