package com.vedantu.platform.seo.entity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

public class CategoryPageListModule {
    private String title;
    private String href;
    private String imageTag;
    private ArrayList<Map<String, String>> links;

    public CategoryPageListModule() {}

    public CategoryPageListModule(String title, String href, String imageTag, ArrayList<Map<String, String>> links) {
        this.title = title;
        this.href = href;
        this.imageTag = imageTag;
        this.links = links;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public ArrayList<Map<String, String>> getLinks() {
        return links;
    }

    public String getImageTag() {
        return this.imageTag;
    }

    public void setImageTag(String imageTag) {
        this.imageTag = imageTag;
    }

    public void setLinks(ArrayList<Map<String, String>> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}