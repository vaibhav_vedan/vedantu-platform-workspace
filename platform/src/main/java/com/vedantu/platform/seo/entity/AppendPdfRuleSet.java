/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.seo.entity;

import com.vedantu.platform.seo.pojo.AppendPdfPageLocationRule;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AppendPdfRuleSet extends AbstractMongoStringIdEntity {

    private String name;
    private String categoryId;
    private String target;
    private String grade;
    private String subject;
    private String pageRelativeUrl;
    private List<AppendPdfPageLocationRule> rules;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<AppendPdfPageLocationRule> getRules() {
        return rules;
    }

    public void setRules(List<AppendPdfPageLocationRule> rules) {
        this.rules = rules;
    }

    public String getPageRelativeUrl() {
        return pageRelativeUrl;
    }

    public void setPageRelativeUrl(String pageRelativeUrl) {
        this.pageRelativeUrl = pageRelativeUrl;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String NAME = "name";
        public static final String CATEGORY_ID = "categoryId";
        public static final String TARGET = "target";
        public static final String GRADE = "grade";
        public static final String SUBJECT = "subject";
        public static final String PAGE_RELATIVE_URL = "pageRelativeUrl";
    }
}
