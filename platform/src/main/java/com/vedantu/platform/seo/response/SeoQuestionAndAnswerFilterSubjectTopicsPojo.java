package com.vedantu.platform.seo.response;

import lombok.Data;

import java.util.List;

@Data
public class SeoQuestionAndAnswerFilterSubjectTopicsPojo {

    private String subjectName;
    private List<String> topics;
}
