/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.seo.pojo;

import com.vedantu.cmds.pojo.CMDSImageDetails;

/**
 *
 * @author ajith
 */
public class AppendPdfPageLocationRule {

    private PageLocationtype pageLocationtype;
    private int value;//application to AFTER_PAGE_NO
    private CMDSImageDetails pdf;

    public PageLocationtype getPageLocationtype() {
        return pageLocationtype;
    }

    public void setPageLocationtype(PageLocationtype pageLocationtype) {
        this.pageLocationtype = pageLocationtype;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public CMDSImageDetails getPdf() {
        return pdf;
    }

    public void setPdf(CMDSImageDetails pdf) {
        this.pdf = pdf;
    }

    @Override
    public String toString() {
        return "AppendPdfPageLocationRule{" + "pageLocationtype=" + pageLocationtype + ", value=" + value + ", pdf=" + pdf + '}';
    }

    public enum PageLocationtype {
        TOP, BOTTOM, AFTER_PAGE_NO
    }
}
