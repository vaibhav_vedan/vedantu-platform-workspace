package com.vedantu.platform.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Document(collection = "HomeDemoRequest")
@CompoundIndexes({
        @CompoundIndex(name = "isActivityPushed_creationTime_-1", def = "{'isActivityPushed': -1, 'creationTime': -1}", background = true),
        @CompoundIndex(name = "isActivityPushed_contactNumber_-1", def = "{'isActivityPushed': 1, 'contactNumber': 1}", background = true)})
public class HomeDemoRequest extends AbstractMongoStringIdEntity {

    private String userId;
    private boolean userVerified;

    @Indexed(background = true)
    private String contactNumber;
    private String fullName;
    private String city;
    private String grade;
    private String preferredDate;
    private String preferredTime;
    private String fullAddress;
    private boolean isActivityPushed = false;
    private String pageURL;

    public static class Constants {

        public static final String USER_ID = "userId";
        public static final String IS_ACTIVITY_PUSHED = "isActivityPushed";
        public static final String CREATION_TIME = "creationTime";
        public static final String CONTACT_NUMBER = "contactNumber";
    }

}
