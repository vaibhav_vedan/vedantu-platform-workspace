package com.vedantu.platform.entity;

import com.vedantu.dinero.enums.Centre;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FOSAgent")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FOSAgent extends AbstractMongoStringIdEntity {
	@Indexed(background = true,unique = true)
	private String agentCode;
	private String agentName; // Can't be null
	@Indexed(background = true)
	private String agentEmailId; // Can't be null
	private long DOJ_EDOJ; // Can't be null
	private Centre location;
	private long dateOfResignation;
	private String reportingTeamLeadId;
	private String reportingManagerId;
	private String status;
	private String designation;
	private String function;
	private String subFunction;
	private String teamLeader;
	private String centreHead;
	private String centreHeadCode;
	private String teacherAccount;
	private String regionalManager;
	private String regionalManagerCode;

	public static class Constants extends AbstractMongoStringIdEntity.Constants {

		public static final String AGENT_EMAIL = "agentEmailId";
		public static final String AGENT_CODE ="agentCode";
	}
}
