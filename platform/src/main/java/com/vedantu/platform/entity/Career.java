package com.vedantu.platform.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/**
 * @author MNPK
 */

public class Career extends AbstractMongoStringIdEntity {

    private String description;
    private String title;
    private String skills;
    private String compensation;
    private String technology;
    private boolean dataRemoveTag;

    public Career() {
        super();
    }

    public Career(String technology, String title, String description) {
        super();
        this.technology = technology;
        this.title = title;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getCompensation() {
        return compensation;
    }

    public void setCompensation(String compensation) {
        this.compensation = compensation;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public boolean isDataRemoveTag() {
        return dataRemoveTag;
    }

    public void setDataRemoveTag(boolean dataRemoveTag) {
        this.dataRemoveTag = dataRemoveTag;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String DATA_REMOVE_TAG = "dataRemoveTag";
        public static final String TITLE = "title";
        public static final String TECHNOLOGY = "technology";
        public static final String DESCRIPTION = "description";


    }
}
