/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.social;

import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.FeatureContext;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "Bookmark")
public class Bookmark extends AbstractMongoStringIdEntity {

    private boolean bookmark;
    private SocialContextType socialContextType;
    @Indexed(background = true)
    private String contextId;
    private FeatureContext featureContext;
    private String featureContextId;
    @Indexed(background = true)
    private Long userId;

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public FeatureContext getFeatureContext() {
        return featureContext;
    }

    public void setFeatureContext(FeatureContext featureContext) {
        this.featureContext = featureContext;
    }

    public String getFeatureContextId() {
        return featureContextId;
    }

    public void setFeatureContextId(String featureContextId) {
        this.featureContextId = featureContextId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String SOCIAL_CONTEXT_TYPE = "socialContextType";
        public static final String CONTEXT_ID = "contextId";
        public static final String USER_ID = "userId";
        public static final String BOOKMARK_VALUE = "bookmark";
        public static final String FEATURE_CONTEXT = "featureContext";
        public static final String FEATURE_CONTEXT_ID = "featureContextId";

    }
}
