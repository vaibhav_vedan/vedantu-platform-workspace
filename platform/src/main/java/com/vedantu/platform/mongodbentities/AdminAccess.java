/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities;

import com.vedantu.platform.pojo.AdminAccessFeatureEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "AdminAccess")
public class AdminAccess extends AbstractMongoStringIdEntity {

    @Indexed(background = true, unique = true)
    private Long adminId;
    private List<AdminAccessFeatureEntity> features;

    public AdminAccess(Long adminId) {
        super();
        this.adminId = adminId;
    }

    public AdminAccess() {
        super();
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public List<AdminAccessFeatureEntity> getFeatures() {
        return features;
    }

    public void setFeatures(List<AdminAccessFeatureEntity> features) {
        this.features = features;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String ADMIN_ID = "adminId";
    }
}
