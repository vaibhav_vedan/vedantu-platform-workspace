package com.vedantu.platform.mongodbentities.dashboard;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.User.TeacherCurrentStatus;
import com.vedantu.dashboard.pojo.TeacherDashboardByEntityInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
@Document(collection = "TeacherDailyDashboard")
public class TeacherDailyDashboard extends AbstractMongoStringIdEntity {

	@Indexed(background=true)
	private Long teacherId;
	private TeacherCurrentStatus teacherStatus;
	private Long startTime;
	private Long primarySubject;
	private TeacherDashboardByEntityInfo coursePlan;
	private TeacherDashboardByEntityInfo otf;

	public TeacherDailyDashboard() {
		super();
	}

	public TeacherDailyDashboard(Long teacherId, Long startTime, TeacherCurrentStatus teacherStatus) {
		super();
		this.teacherId = teacherId;
		this.startTime = startTime;
		this.teacherStatus = teacherStatus;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public TeacherCurrentStatus getTeacherStatus() {
		return teacherStatus;
	}

	public void setTeacherStatus(TeacherCurrentStatus teacherStatus) {
		this.teacherStatus = teacherStatus;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getPrimarySubject() {
		return primarySubject;
	}

	public void setPrimarySubject(Long primarySubject) {
		this.primarySubject = primarySubject;
	}

	public TeacherDashboardByEntityInfo getCoursePlan() {
		return coursePlan;
	}

	public void setCoursePlan(TeacherDashboardByEntityInfo coursePlan) {
		this.coursePlan = coursePlan;
	}

	public TeacherDashboardByEntityInfo getOtf() {
		return otf;
	}

	public void setOtf(TeacherDashboardByEntityInfo otf) {
		this.otf = otf;
	}
	
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		
		public static final String TEACHER_ID = "teacherId";
		public static final String START_TIME = "startTime";
		public static final String TEACHER_STATUS = "teacherStatus";
	}
	

}
