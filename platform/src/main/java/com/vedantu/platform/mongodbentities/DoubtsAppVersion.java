/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities;

import com.vedantu.platform.enums.DoubtAppType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class DoubtsAppVersion extends AbstractMongoStringIdEntity{
    
    private String versionName;
    private String apkUrl;

    private boolean toBeForceUpdated = false;
    private DoubtAppType doubtAppType;

    /**
     * @return the versionName
     */
    public String getVersionName() {
        return versionName;
    }

    /**
     * @param versionName the versionName to set
     */
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    /**
     * @return the apkUrl
     */
    public String getApkUrl() {
        return apkUrl;
    }

    /**
     * @param apkUrl the apkUrl to set
     */
    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }
    
    public static class Constants extends AbstractMongoEntity.Constants {
    
        public static final String VERSION_NAME = "versionName";
        public static final String DOUBT_APP_TYPE = "doubtAppType";
    }

    /**
     * @return the doubtAppType
     */
    public DoubtAppType getDoubtAppType() {
        return doubtAppType;
    }

    /**
     * @param doubtAppType the doubtAppType to set
     */
    public void setDoubtAppType(DoubtAppType doubtAppType) {
        this.doubtAppType = doubtAppType;
    }

    public boolean isToBeForceUpdated() {
        return toBeForceUpdated;
    }

    public void setToBeForceUpdated(boolean toBeForceUpdated) {
        this.toBeForceUpdated = toBeForceUpdated;
    }
    
}
