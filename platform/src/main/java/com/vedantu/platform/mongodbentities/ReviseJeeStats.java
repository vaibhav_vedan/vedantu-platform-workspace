package com.vedantu.platform.mongodbentities;


import com.vedantu.platform.pojo.TestAnalytics;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Getter
@Setter
@ToString
@Document(collection = "ReviseJeeStats")
public class ReviseJeeStats extends AbstractMongoStringIdEntity {
    @Indexed(background = true)
    private Long userId;
    private TestAnalytics testAnalytics;
    private String predictedRankRange;
    private String nextRankToAchieve;
    private List<String> areasToImprove;
    private List<String> testAttempted;
    private Integer tests = 0;
    private Integer sessionsAttended = 0;


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String TESTS = "tests";
        public static final String SESSIONS_ATTENDED = "sessionsAttended";
        public static final String TEST_ATTEMPTED = "testAttempted";
        public static final String TEST_ANALYTICS = "testAnalytics";
        public static final String TEST_ATTEMPT_ID = "testAttemptId";
        public static final String TEST_PERFORMANCE_CODE = "testPerformaneCode";
        public static final String PREDICTED_RANK_RANGE = "predictedRankRange";
        public static final String NEXT_RANK_TO_ACHIEVE = "nextRankToAchieve";
        public static final String AREAS_TO_IMPROVE = "areasToImprove";
    }
}

