/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.board;

import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.IBoardAware;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 *
 * @author somil
 */
public class TeacherBoardMapping extends AbstractMongoLongIdEntity implements
		IBoardAware {

	private Long userId;
	private String category;
	private String grade;
	private Set<Long> boardIds;

	public TeacherBoardMapping() {
		super();
	}

	public TeacherBoardMapping(Long userId, String category, String grade,
			Collection<Long> boardIds) {
		super();
		this.userId = userId;
		this.category = category;
		this.grade = grade;
		this.boardIds = boardIds == null ? null : new HashSet<>(boardIds);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Set<Long> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(Set<Long> boardIds) {
		this.boardIds = boardIds;
	}

	@Override
	public String toString() {
		return super.toString()+String.format("{category=%s, grade=%s, boardIds=%s, userId=%s}",
				category, grade, boardIds, userId);
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String CATEGORY = "category";
		public static final String GRADE = "grade";
		public static final String BOARD_IDS = "boardIds";
	}

	@Override
	public Set<Long> _getBoardIds() {
		return boardIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((boardIds == null) ? 0 : boardIds.hashCode());
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((grade == null) ? 0 : grade.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		TeacherBoardMapping other = (TeacherBoardMapping) obj;
		if (boardIds == null) {
			if (other.boardIds != null)
				return false;
		} else if (!boardIds.equals(other.boardIds))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (grade == null) {
			if (other.grade != null)
				return false;
		} else if (!grade.equals(other.grade))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public Set<String> _getBoardIdsInString() {
		Set<String> boardIdsLong = new HashSet<>();
		for (Long boardId : this.boardIds) {
			boardIdsLong.add(boardId.toString());
		}
		return boardIdsLong;
	}

}
