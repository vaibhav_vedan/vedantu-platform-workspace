package com.vedantu.platform.mongodbentities.engagement;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class TeacherPercentile extends AbstractMongoStringIdEntity {

	private Long startTimeoFMonth;
	private int fortnight;
	private Long teacherId;
	private Double percentile;
	public TeacherPercentile() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getStartTimeoFMonth() {
		return startTimeoFMonth;
	}
	public void setStartTimeoFMonth(Long startTimeoFMonth) {
		this.startTimeoFMonth = startTimeoFMonth;
	}
	public int getFortnight() {
		return fortnight;
	}
	public void setFortnight(int fortnight) {
		this.fortnight = fortnight;
	}
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
}

