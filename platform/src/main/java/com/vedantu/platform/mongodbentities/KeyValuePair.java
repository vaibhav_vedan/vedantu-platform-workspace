/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/**
 *
 * @author somil
 */
public class KeyValuePair extends AbstractMongoStringIdEntity {

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "KeyValuePair{" + "key=" + key + ", value=" + value + '}';
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String KEY = "key";
        public static final String VALUE = "value";
    }
}
