package com.vedantu.platform.mongodbentities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class SessionHourCount extends AbstractMongoStringIdEntity {
	private Long otoHourCount;
	private Long otfHourCount;
	private long endTime;
	private long totalHourCount;

	public SessionHourCount() {
		super();
	}

	public SessionHourCount(long endTime, long otoHourCount, long otfHourCount) {
		super();
		this.endTime = endTime;
		this.otoHourCount = otoHourCount;
		this.otfHourCount = otfHourCount;
		this.totalHourCount = otoHourCount + otfHourCount;
	}

	public Long getOtoHourCount() {
		return otoHourCount;
	}

	public void setOtoHourCount(Long otoHourCount) {
		this.otoHourCount = otoHourCount;
	}

	public Long getOtfHourCount() {
		return otfHourCount;
	}

	public void setOtfHourCount(Long otfHourCount) {
		this.otfHourCount = otfHourCount;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getTotalHourCount() {
		return totalHourCount;
	}

	public void setTotalHourCount(long totalHourCount) {
		this.totalHourCount = totalHourCount;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String END_TIME = "endTime";
	}

	@Override
	public String toString() {
		return "SessionHourCount [otoHourCount=" + otoHourCount + ", otfHourCount=" + otfHourCount + ", endTime="
				+ endTime + ", totalHourCount=" + totalHourCount + ", toString()=" + super.toString() + "]";
	}
}
