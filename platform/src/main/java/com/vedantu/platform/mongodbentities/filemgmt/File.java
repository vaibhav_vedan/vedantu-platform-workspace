/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.filemgmt;

import com.vedantu.platform.request.filemgmt.FileACL;
import com.vedantu.platform.request.filemgmt.FileInitReq;
import com.vedantu.platform.request.filemgmt.PdfToImageReq;
import com.vedantu.platform.request.filemgmt.UploadType;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.UploadState;

/**
 *
 * @author somil
 */
public class File extends AbstractMongoLongIdEntity {

    private String name;
    // type: mime-type
    private String type;
    // size: bytes
    private Long size;
    private UploadType uploadType;
    private FileACL acl;
    private Long userId;
    private Long uploadedStartedAt;
    private Long uploadedEndedAt;
    private UploadState state = UploadState.INIT;
    private String bucket;
    // path: relative to the bucket
    private String path;

    public File() {

        super();
    }

    public File(FileInitReq req) {

        /*
		File fileUpload = new File();
		fileUpload.setName(getName());
		fileUpload.setType(getType());
		fileUpload.setSize(getSize());
		fileUpload.setUserId(getUserId());
		fileUpload.setUploadType(getUploadType());
		fileUpload.setAcl(getAcl());
         */
        name = req.getName();
        type = req.getType();
        size = req.getSize();
        userId = req.getUserId();
        uploadType = req.getUploadType();
        acl = req.getAcl();

    }
    public File(PdfToImageReq req) {

        /*
		File fileUpload = new File();
		fileUpload.setName(getName());
		fileUpload.setType(getType());
		fileUpload.setSize(getSize());
		fileUpload.setUserId(getUserId());
		fileUpload.setUploadType(getUploadType());
		fileUpload.setAcl(getAcl());
         */
        name = req.getName();
        type = req.getType();
        size = req.getSize();
        userId = req.getUserId();
        uploadType = req.getUploadType();
        acl = req.getAcl();

    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Long getSize() {

        return size;
    }

    public void setSize(Long size) {

        this.size = size;
    }

    public UploadType getUploadType() {

        return uploadType;
    }

    public void setUploadType(UploadType uploadType) {

        this.uploadType = uploadType;
    }

    public FileACL getAcl() {
        return acl == null ? FileACL.PRIVATE : acl;
    }

    public void setAcl(FileACL acl) {
        this.acl = acl == null ? FileACL.PRIVATE : acl;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public Long getUploadedStartedAt() {

        return uploadedStartedAt;
    }

    public void setUploadedStartedAt(Long uploadedStartedAt) {

        this.uploadedStartedAt = uploadedStartedAt;
    }

    public Long getUploadedEndedAt() {

        return uploadedEndedAt;
    }

    public void setUploadedEndedAt(Long uploadedEndedAt) {

        this.uploadedEndedAt = uploadedEndedAt;
    }

    public UploadState getState() {

        return state;
    }

    public void setState(UploadState state) {

        this.state = state;
    }

    public String getBucket() {

        return bucket;
    }

    public void setBucket(String bucket) {

        this.bucket = bucket;
    }

    public String getPath() {

        return path;
    }

    public void setPath(String path) {

        this.path = path;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{name=").append(name).append(", type=").append(type)
                .append(", size=").append(size)
                .append(", uploadType=").append(uploadType)
                .append(", acl=").append(acl)
                .append(", userId=").append(userId)
                .append(", uploadedStartedAt=").append(uploadedStartedAt)
                .append(", uploadedEndedAt=").append(uploadedEndedAt)
                .append(", state=").append(state).append(", bucket=")
                .append(bucket).append(", path=").append(path).append("}");
        return builder.toString();
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String PATH = "path";
        public static final String BUCKET = "bucket";
        public static final String NAME = "name";
        public static final String TYPE = "type";
        public static final String STATE = "state";
        public static final String USER_ID = "userId";
        public static final String FILE_ID = "fileId";
        public static final String SIZE = "size";
        public static final String UPLOAD_TYPE = "uploadType";
        public static final String FILE_ACL = "acl";
    }

}
