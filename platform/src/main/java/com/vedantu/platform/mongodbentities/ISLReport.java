package com.vedantu.platform.mongodbentities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;

/**
 *
 * @author ajith
 */
public class ISLReport extends AbstractMongoStringIdEntity {

    private String testCode;
    private Long userId;
    private String category;
    private String studentName;
    private String parentName;
    private String parentEmail;
    private String parentContactNumber;
    private String parentPhoneCode;
    private String school;
    private String grade;
    private String city;
    private Long attemptedOn;
    private Section testMarksData;
    private List<Section> sectionMarksData;
    private String brainMapping;
    private String certificateType;

    public String getTestCode() {
        return testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentContactNumber() {
        return parentContactNumber;
    }

    public void setParentContactNumber(String parentContactNumber) {
        this.parentContactNumber = parentContactNumber;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getAttemptedOn() {
        return attemptedOn;
    }

    public void setAttemptedOn(Long attemptedOn) {
        this.attemptedOn = attemptedOn;
    }

    public Section getTestMarksData() {
        return testMarksData;
    }

    public void setTestMarksData(Section testMarksData) {
        this.testMarksData = testMarksData;
    }

    public List<Section> getSectionMarksData() {
        return sectionMarksData;
    }

    public void setSectionMarksData(List<Section> sectionMarksData) {
        this.sectionMarksData = sectionMarksData;
    }

    public String getBrainMapping() {
        return brainMapping;
    }

    public void setBrainMapping(String brainMapping) {
        this.brainMapping = brainMapping;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getParentPhoneCode() {
        return parentPhoneCode;
    }

    public void setParentPhoneCode(String parentPhoneCode) {
        this.parentPhoneCode = parentPhoneCode;
    }

    public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public class Section {

        private String name;
        private Float totalMarks;
        private Float totalMaxMarks;
        private Integer rank;
        private Integer rankOutOf;
        private Float percentile;
        private Integer timeTaken;
        private List<TopicDistribution> distribution;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Float getTotalMarks() {
            return totalMarks;
        }

        public void setTotalMarks(Float totalMarks) {
            this.totalMarks = totalMarks;
        }

        public Float getTotalMaxMarks() {
            return totalMaxMarks;
        }

        public void setTotalMaxMarks(Float totalMaxMarks) {
            this.totalMaxMarks = totalMaxMarks;
        }

        public Integer getRank() {
            return rank;
        }

        public void setRank(Integer rank) {
            this.rank = rank;
        }

        public Float getPercentile() {
            return percentile;
        }

        public void setPercentile(Float percentile) {
            this.percentile = percentile;
        }

        public Integer getTimeTaken() {
            return timeTaken;
        }

        public void setTimeTaken(Integer timeTaken) {
            this.timeTaken = timeTaken;
        }

        public List<TopicDistribution> getDistribution() {
            return distribution;
        }

        public void setDistribution(List<TopicDistribution> distribution) {
            this.distribution = distribution;
        }

        public Integer getRankOutOf() {
            return rankOutOf;
        }

        public void setRankOutOf(Integer rankOutOf) {
            this.rankOutOf = rankOutOf;
        }
        
        @Override
        public String toString() {
            return "Section{" + "name=" + name + ", totalMarks=" + totalMarks + ", totalMaxMarks=" + totalMaxMarks + ", rank=" + rank + ", percentile=" + percentile + ", timeTaken=" + timeTaken + ", topicDistribution=" + distribution + '}';
        }

        public class TopicDistribution {

            private String name;
            private Float percent;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Float getPercent() {
                return percent;
            }

            public void setPercent(Float percent) {
                this.percent = percent;
            }

            @Override
            public String toString() {
                return "TopicDistribution{" + "name=" + name + ", percent=" + percent + '}';
            }
        }

    }
}
