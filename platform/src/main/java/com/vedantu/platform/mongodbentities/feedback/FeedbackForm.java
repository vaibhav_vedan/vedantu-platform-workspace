/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.feedback;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.enums.feedback.FeedbackFormType;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamicSection;
import com.vedantu.platform.pojo.feedback.FeedbackQuestion;
import com.vedantu.platform.pojo.feedback.FormNamePojo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class FeedbackForm extends AbstractMongoStringIdEntity{
    
    private FeedbackFormType feedbackFormType;
    private String formTitle;
    private FeedbackCommunication feedbackCommunication;
    private String parentFormId;
    private Long expiresIn;
    private List<FeedbackQuestion> staticQuestions;
    private List<FeedbackFormDynamicSection> feedbackFormDynamicSections;
    private EntityType contextType;
    private String contextId;
    private Set<Long> userIds = new HashSet<>();
    private Set<Long> respondedBy = new HashSet<>();
    private EntityStatus status = EntityStatus.ACTIVE;
    private String contextName;
    private List<FormNamePojo> formNames = new ArrayList<>();
    
    /**
     * @return the parentFormId
     */
    public String getParentFormId() {
        return parentFormId;
    }

    /**
     * @param parentFormId the parentFormId to set
     */
    public void setParentFormId(String parentFormId) {
        this.parentFormId = parentFormId;
    }

    /**
     * @return the expiresIn
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * @param expiresIn the expiresIn to set
     */
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * @return the staticQuestions
     */
    public List<FeedbackQuestion> getStaticQuestions() {
        return staticQuestions;
    }

    /**
     * @param staticQuestions the staticQuestions to set
     */
    public void setStaticQuestions(List<FeedbackQuestion> staticQuestions) {
        this.staticQuestions = staticQuestions;
    }

    /**
     * @return the feedbackFormDynamicSections
     */
    public List<FeedbackFormDynamicSection> getFeedbackFormDynamicSections() {
        return feedbackFormDynamicSections;
    }

    /**
     * @param feedbackFormDynamicSections the feedbackFormDynamicSections to set
     */
    public void setFeedbackFormDynamicSections(List<FeedbackFormDynamicSection> feedbackFormDynamicSections) {
        this.feedbackFormDynamicSections = feedbackFormDynamicSections;
    }

    /**
     * @return the contextType
     */
    public EntityType getContextType() {
        return contextType;
    }

    /**
     * @param contextType the contextType to set
     */
    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the userIds
     */
    public Set<Long> getUserIds() {
        return userIds;
    }

    /**
     * @param userIds the userIds to set
     */
    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    /**
     * @return the feedbackFormType
     */
    public FeedbackFormType getFeedbackFormType() {
        return feedbackFormType;
    }

    /**
     * @param feedbackFormType the feedbackFormType to set
     */
    public void setFeedbackFormType(FeedbackFormType feedbackFormType) {
        this.feedbackFormType = feedbackFormType;
    }

    /**
     * @return the respondedBy
     */
    public Set<Long> getRespondedBy() {
        return respondedBy;
    }

    /**
     * @param respondedBy the respondedBy to set
     */
    public void setRespondedBy(Set<Long> respondedBy) {
        this.respondedBy = respondedBy;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_IDS = "userIds";
        public static final String RESPONDED_BY = "respondedBy";
        public static final String STATUS = "status";
        public static final String FEEDBACK_COMMUNICATION = "feedbackCommunication";
        public static final String PARENT_FORM_ID = "parentFormId";
        public static final String CONTEXT_ID = "contextId";
        
    }    

    /**
     * @return the status
     */
    public EntityStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    /**
     * @return the formTitle
     */
    public String getFormTitle() {
        return formTitle;
    }

    /**
     * @param formTitle the formTitle to set
     */
    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    /**
     * @return the feedbackCommunication
     */
    public FeedbackCommunication getFeedbackCommunication() {
        return feedbackCommunication;
    }

    /**
     * @param feedbackCommunication the feedbackCommunication to set
     */
    public void setFeedbackCommunication(FeedbackCommunication feedbackCommunication) {
        this.feedbackCommunication = feedbackCommunication;
    }

    /**
     * @return the contextName
     */
    public String getContextName() {
        return contextName;
    }

    /**
     * @param contextName the contextName to set
     */
    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    /**
     * @return the formNames
     */
    public List<FormNamePojo> getFormNames() {
        return formNames;
    }

    /**
     * @param formNames the formNames to set
     */
    public void setFormNames(List<FormNamePojo> formNames) {
        this.formNames = formNames;
    }
    
}
