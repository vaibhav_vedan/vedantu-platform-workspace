/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.filemgmt;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author somil
 */
@Document(collection = "FileMetadata")
@CompoundIndexes({ @CompoundIndex(name = "userId_fileId", def = "{'userId' : 1, 'fileId': 1}") })
public class FileMetadata extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long sessionId;
    private Long userId;
    private String fileData;
    private Long fileId;
    private String fileType;
    @Indexed(background = true)
    private String localObjectId;

    public FileMetadata() {
        super();
    }

    public FileMetadata(Long sessionId, Long userId, Long fileId, String fileType) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.fileId = fileId;
        this.fileType = fileType;
    }

    public FileMetadata(Long sessionId, Long userId, String fileData, Long fileId, String fileType, String localObjectId) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.fileData = fileData;
        this.fileId = fileId;
        this.fileType = fileType;
        this.localObjectId = localObjectId;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getLocalObjectId() {
        return localObjectId;
    }

    public void setLocalObjectId(String localObjectId) {
        this.localObjectId = localObjectId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String NAME = "name";
        public static final String FILE_TYPE = "fileType";
        public static final String SESSION_ID = "sessionId";
        public static final String USER_ID = "userId";
        public static final String LOCAL_OBJECT_ID = "localObjectId";
        public static final String FILE_ID = "fileId";
        public static final String FILE_METADATA_ID = "fileMetadataId";
    }
}
