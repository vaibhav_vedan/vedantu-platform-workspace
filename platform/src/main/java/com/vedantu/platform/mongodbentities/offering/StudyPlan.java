package com.vedantu.platform.mongodbentities.offering;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class StudyPlan extends AbstractMongoStringIdEntity {

	private String userId;
	private String offeringId;
	private String targetUserId;
	private String note;
	private Long targetDate;
	private String name;
	private Long noOfSessions;
	private String status;

	private VisibilityState state = VisibilityState.VISIBLE;

	public StudyPlan() {
		super();
	}

	public StudyPlan(String userId, String offeringId, String targetUserId,
			String note, Long targetDate) {
		super();
		this.userId = userId;
		this.offeringId = offeringId;
		this.targetUserId = targetUserId;
		this.note = note;
		this.targetDate = targetDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Long targetDate) {
		this.targetDate = targetDate;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNoOfSessions() {
		return noOfSessions;
	}

	public void setNoOfSessions(Long noOfSessions) {
		this.noOfSessions = noOfSessions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "StudyPlan [userId=" + userId + ", offeringId=" + offeringId
				+ ", targetUserId=" + targetUserId + ", note=" + note
				+ ", targetDate=" + targetDate + ", name=" + name
				+ ", noOfSessions=" + noOfSessions + ", status=" + status
				+ ", state=" + state + "]";
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {

		public static final String USER_ID = "userId";
		public static final String NAME = "name";
		public static final String STATUS = "status";
		public static final String OFFERING_ID = "offeringId";
		public static final String TARGET_USER_ID = "targetUserId";
		public static final String TARGET_DATE = "targetDate";
		public static final String NOTE = "note";

	}

}
