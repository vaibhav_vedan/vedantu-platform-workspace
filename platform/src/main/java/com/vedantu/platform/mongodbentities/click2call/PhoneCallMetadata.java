/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.click2call;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/**
 *
 * @author somil
 */
public class PhoneCallMetadata extends AbstractMongoStringIdEntity {

    private String callSid;
    private String contextId;
    private String contextType;
    private Long startTime;
    private Long endTime;
    private String status;
    private String fromNumber;
    private String toNumber;
    private String recordingUrl;
    private Integer duration;

    public PhoneCallMetadata() {
        super();
    }

    public PhoneCallMetadata(String callSid, String contextId,
            String contextType, String fromNumber, String toNumber) {
        super();
        this.callSid = callSid;
        this.contextId = contextId;
        this.contextType = contextType;
        this.fromNumber = fromNumber;
        this.toNumber = toNumber;
    }

    public String getCallSid() {
        return callSid;
    }

    public void setCallSid(String callSid) {
        this.callSid = callSid;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getContextType() {
        return contextType;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public String getRecordingUrl() {
        return recordingUrl;
    }

    public void setRecordingUrl(String recordingUrl) {
        this.recordingUrl = recordingUrl;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return String
                .format("{callSid=%s, contextId=%s, contextType=%s, startTime=%s, endTime=%s, status=%s, fromNumber=%s, toNumber=%s, recordingUrl=%s, duration=%s}",
                        callSid, contextId, contextType, startTime, endTime,
                        status, fromNumber, toNumber, recordingUrl, duration);
    }

    public final static class Constants {

        public static final String CALL_SID = "callSid";
        public static final String CONTEXT_ID = "contextId";
        public static final String CONTEXT_TYPE = "contextType";
    }
}
