/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.feedback;

import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.enums.feedback.FeedbackFormType;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamic;
import com.vedantu.platform.pojo.feedback.FeedbackQuestion;
import com.vedantu.platform.pojo.feedback.FeedbackShareMetadata;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;

/**
 *
 * @author parashar
 */
public class FeedbackParentForm extends AbstractMongoStringIdEntity{
    
    private String formTitle; 
    private List<FeedbackQuestion> staticFeedbackQuestions;
    private FeedbackFormType feedbackFormType;
    private EntityType contextType;
    private FeedbackCommunication communication;
    private FeedbackFormDynamic dynamicSection;
    private List<FeedbackShareMetadata> feedbackShareMetadatas;

    /**
     * @return the formTitle
     */
    public String getFormTitle() {
        return formTitle;
    }

    /**
     * @param formTitle the formTitle to set
     */
    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    /**
     * @return the staticFeedbackQuestions
     */
    public List<FeedbackQuestion> getStaticFeedbackQuestions() {
        return staticFeedbackQuestions;
    }

    /**
     * @param staticFeedbackQuestions the staticFeedbackQuestions to set
     */
    public void setStaticFeedbackQuestions(List<FeedbackQuestion> staticFeedbackQuestions) {
        this.staticFeedbackQuestions = staticFeedbackQuestions;
    }

    /**
     * @return the feedbackFormType
     */
    public FeedbackFormType getFeedbackFormType() {
        return feedbackFormType;
    }

    /**
     * @param feedbackFormType the feedbackFormType to set
     */
    public void setFeedbackFormType(FeedbackFormType feedbackFormType) {
        this.feedbackFormType = feedbackFormType;
    }

    /**
     * @return the contextType
     */
    public EntityType getContextType() {
        return contextType;
    }

    /**
     * @param contextType the contextType to set
     */
    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    /**
     * @return the communication
     */
    public FeedbackCommunication getCommunication() {
        return communication;
    }

    /**
     * @param communication the communication to set
     */
    public void setCommunication(FeedbackCommunication communication) {
        this.communication = communication;
    }

    /**
     * @return the dynamicQuestion
     */
    public FeedbackFormDynamic getDynamicSection() {
        return dynamicSection;
    }

    /**
     * @param dynamicSection the dynamicQuestion to set
     */
    public void setDynamicSection(FeedbackFormDynamic dynamicSection) {
        this.dynamicSection = dynamicSection;
    }

    /**
     * @return the feedbackShareMetadatas
     */
    public List<FeedbackShareMetadata> getFeedbackShareMetadatas() {
        return feedbackShareMetadatas;
    }

    /**
     * @param feedbackShareMetadatas the feedbackShareMetadatas to set
     */
    public void setFeedbackShareMetadatas(List<FeedbackShareMetadata> feedbackShareMetadatas) {
        this.feedbackShareMetadatas = feedbackShareMetadatas;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String CONTEXT_ID = "feedbackShareMetadatas.contextId";
        public static final String CONTEXT_TYPE = "contextType";
        public static final String VALID_TILL = "feedbackShareMetadatas.validTill";

    }

    @Override
    public String toString() {
        return "FeedbackParentForm{" + "formTitle=" + formTitle + ", staticFeedbackQuestions=" + staticFeedbackQuestions + ", feedbackFormType=" + feedbackFormType + ", contextType=" + contextType + ", communication=" + communication + ", dynamicSection=" + dynamicSection + ", feedbackShareMetadatas=" + feedbackShareMetadatas + '}';
    }
    
    
    
}
