package com.vedantu.platform.mongodbentities.review;

import com.vedantu.review.pojo.SessionReviewInfo;
import java.util.List;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Review")
@CompoundIndexes({
    @CompoundIndex(name = "creationTime", def = "{'creationTime' : -1}")
})
public class Review extends AbstractMongoStringIdEntity {

        @Indexed(background = true)
	private String userId;
        @Indexed(background = true)
	private String entityId;
	private EntityType entityType;
	private Integer rating;
	private String review;
	// this is just to make sure that we dont use != query when we want to
	// fetch users who has only given rating, instead we will use
	// isReviewed value (true, false)
	private Boolean isReviewed;
	private VisibilityState state = VisibilityState.VISIBLE;

	private ContextType contextType;
        @Indexed(background = true)
	private String contextId;
	private List<String> reason;
	private Integer priority;
	private SessionReviewInfo reviewExtraInfo;

        public Review() {
            super();
        }

	public Review(String userId, String entityId, EntityType entityType,
			Integer rating, String review, ContextType contextType,
			String contextId, Boolean isReviewed, List<String> reason,
			Integer priority) {
		super();
		this.userId = userId;
		this.entityId = entityId;
		this.entityType = entityType;
		this.rating = rating;
		this.review = review;
		this.contextType = contextType;
		this.contextId = contextId;
		this.isReviewed = isReviewed;
		this.reason = reason;
		this.priority = priority;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getReview() {
		return review;
	}

	// public String getReviewString() {
	// if (review != null) {
	// return review.getValue();
	// } else {
	// return null;
	// }
	// }

	public void setReview(String review) {
		this.review = review;
	}

	public Boolean getIsReviewed() {
		return isReviewed;
	}

	public void setIsReviewed(Boolean isReviewed) {
		this.isReviewed = isReviewed;
	}

	public VisibilityState getState() {
		return state;
	}

	public void setState(VisibilityState state) {
		this.state = state;
	}

	public ContextType getContextType() {
		return contextType;
	}

	public void setContextType(ContextType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public List<String> getReason() {
		return reason;
	}

	public String getReasonString() {
		if (reason != null && !reason.isEmpty()) {
			return StringUtils.join(reason.toArray(), ",");
		} else {
			return null;
		}
	}

	public void setReason(List<String> reason) {
		this.reason = reason;
	}

	public SessionReviewInfo getReviewExtraInfo() {
		return reviewExtraInfo;
	}

	public void setReviewExtraInfo(SessionReviewInfo reviewExtraInfo) {
		this.reviewExtraInfo = reviewExtraInfo;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}
        
    
        public com.vedantu.review.pojo.Review getReviewPojo(){
            com.vedantu.review.pojo.Review reviewPojo=new com.vedantu.review.pojo.Review(userId,
                    entityId, entityType, rating, review, contextType, 
                    contextId, isReviewed, reason, priority);
            reviewPojo.setReviewExtraInfo(reviewExtraInfo);
            reviewPojo.setState(state);
            reviewPojo.setId(super.getId());
            return reviewPojo;
        }        

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String REVIEW = "review";
		public static final String RATING = "rating";
		public static final String IS_REVIEWED = "isReviewed";
		public static final String REASON = "reason";
		public static final String CONTEXT_TYPE = "contextType";
		public static final String CONTEXT_ID = "contextId";
		public static final String ENTITY_TYPE = "entityType";
		public static final String ENTITY_ID = "entityId";
		public static final String PRIORITY = "priority";
		public static final String state = "state";
		
	}

}
