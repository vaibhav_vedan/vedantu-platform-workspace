/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "SchoolData")
public class SchoolData extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String pinCode;
    @Indexed(background = true)
    private String board;
    private String affiliationNo;
    @Indexed(background = true)
    private String state;
    @Indexed(background = true)
    private String country;
    private String email;
    @Indexed(background = true)
    private String address;
    @Indexed(background = true)
    private String searchString;
    @Indexed(background = true)
    private String schoolName;
    private String principalName;
    private String contactNo;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getAffiliationNo() {
        return affiliationNo;
    }

    public void setAffiliationNo(String affiliationNo) {
        this.affiliationNo = affiliationNo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @Override
    public String toString() {
        return "SchoolData{" + "pinCode=" + pinCode + ", board=" + board + ", affiliationNo=" + affiliationNo + ", state=" + state + ", country=" + country + ", email=" + email + ", address=" + address + ", schoolName=" + schoolName + ", principalName=" + principalName + ", contactNo=" + contactNo + '}';
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String PIN_CODE = "pinCode";
        public static final String BOARD = "board";
        public static final String AFFILIATION_NO = "affiliationNo";
        public static final String STATE = "state";
        public static final String COUNTRY = "country";
        public static final String EMAIL = "email";
        public static final String ADDRESS = "address";
        public static final String SCHOOL_NAME = "schoolName";
        public static final String PRINCIPLE_NAME = "principalName";
        public static final String CONTACT_NO = "contactNo";
        public static final String SEARCH_STRING = "searchString";
    }
}
