package com.vedantu.platform.mongodbentities.listing;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class ListingDailyCount extends AbstractMongoStringIdEntity{

	private Long date;
	private Long userId;
	private Long viewCount;
	
	public ListingDailyCount(Long date, Long userId, Long viewCount) {
		super();
		this.date = date;
		this.userId = userId;
		this.viewCount = viewCount;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getViewCount() {
		return viewCount;
	}

	public void setViewCount(Long viewCount) {
		this.viewCount = viewCount;
	}

	
}
