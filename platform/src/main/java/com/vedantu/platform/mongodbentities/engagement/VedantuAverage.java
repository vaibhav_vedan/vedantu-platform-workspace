package com.vedantu.platform.mongodbentities.engagement;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class VedantuAverage extends AbstractMongoStringIdEntity{

	private String engagementEntityType;
	private Long startTime;
	private String subject;
	private Double scoreInteraction;
	private Double scoreContentUsage;
	private Double scoreWhiteBoardUsage;
	private Double scoreTotal;
	public VedantuAverage() {
		super();
		scoreInteraction = 0.0;
		scoreContentUsage = 0.0;
		scoreWhiteBoardUsage = 0.0;
		scoreTotal = 0.0;
		// TODO Auto-generated constructor stub
	}
	public String getEngagementEntityType() {
		return engagementEntityType;
	}
	public void setEngagementEntityType(String engagementEntityType) {
		this.engagementEntityType = engagementEntityType;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Double getScoreInteraction() {
		return scoreInteraction;
	}
	public void setScoreInteraction(Double scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}
	public Double getScoreContentUsage() {
		return scoreContentUsage;
	}
	public void setScoreContentUsage(Double scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}
	public Double getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}
	public void setScoreWhiteBoardUsage(Double scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}
	public Double getScoreTotal() {
		return scoreTotal;
	}
	public void setScoreTotal(Double scoreTotal) {
		this.scoreTotal = scoreTotal;
	}
}
