package com.vedantu.platform.mongodbentities.review;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.review.pojo.RemarkContext;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
@Document(collection = "Remark")
public class Remark extends AbstractMongoStringIdEntity{

	private Long reviewer;
	private Long toUser;
	private RemarkContext contextType;
	@Indexed(background=true)
	private String contextId;
	private String remark;
	private String nextSessionRemark;
	private Long boardId;
	
	public Long getReviewer() {
		return reviewer;
	}
	public void setReviewer(Long reviewer) {
		this.reviewer = reviewer;
	}
	public Long getToUser() {
		return toUser;
	}
	public void setToUser(Long toUser) {
		this.toUser = toUser;
	}
	public RemarkContext getContextType() {
		return contextType;
	}
	public void setContextType(RemarkContext contextType) {
		this.contextType = contextType;
	}
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNextSessionRemark() {
		return nextSessionRemark;
	}
	public void setNextSessionRemark(String nextSessionRemark) {
		this.nextSessionRemark = nextSessionRemark;
	}
	
	public Long getBoardId() {
		return boardId;
	}
	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String CONTEXT_ID = "contextId";
		public static final String CONTEXT_TYPE = "contextType";
		public static final String TO_USER = "toUser";
		public static final String REVIEWER = "reviewer";
		public static final String BOARD_ID = "boardId";
	}

	@Override
	public String toString() {
		return "Remark [reviewer=" + reviewer + ", toUser=" + toUser + ", contextType=" + contextType + ", contextId="
				+ contextId + ", remark=" + remark + ", nextSessionRemark=" + nextSessionRemark + "]";
	}
}
