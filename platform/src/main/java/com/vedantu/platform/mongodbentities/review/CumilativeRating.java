package com.vedantu.platform.mongodbentities.review;

import com.vedantu.util.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class CumilativeRating extends AbstractMongoStringIdEntity {

	private String entityId;
	private EntityType entityType;
	private Long totalRatingCount = 0L;
	private Long totalRating = 0L;
	private Float avgRating = new Float(0);
	private Boolean baseRatingAssigned;

	public CumilativeRating() {
		super();
	}

	public CumilativeRating(String entityId, EntityType entityType) {
		super();
		this.entityId = entityId;
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public Long getTotalRatingCount() {
		return totalRatingCount;
	}

	public void setTotalRatingCount(Long totalRatingCount) {
		this.totalRatingCount = totalRatingCount;
	}

	public Long getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(Long totalRating) {
		this.totalRating = totalRating;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Float avgRating) {
		this.avgRating = avgRating;
	}

	public Boolean getBaseRatingAssigned() {
		return baseRatingAssigned == null ? false : baseRatingAssigned;
	}

	public void setBaseRatingAssigned(Boolean baseRatingAssigned) {
		this.baseRatingAssigned = baseRatingAssigned;
	}

	public void calculateAvgRating() {
		if (totalRatingCount != null && totalRatingCount > 0) {
			avgRating = ((float) totalRating / totalRatingCount);
		}
	}

	public void incRating(int rating) {
		this.totalRating += rating;
	}

	public void decRating(int rating) {

		if (this.totalRating > 0 && this.totalRating > rating) {
			this.totalRating -= rating;
		}
	}

	public void incTotalCount(int count) {
		this.totalRatingCount += count;
	}

	public void decTotalCount(int count) {

		if (this.totalRatingCount > 0 && this.totalRatingCount > count) {
			this.totalRatingCount -= count;
		}
	}

	@Override
	public String toString() {
		return String
				.format("{entityId=%s, entityType=%s, totalCount=%s, totalRating=%s, avgRating=%s}",
						entityId, entityType, totalRatingCount, totalRating,
						avgRating);
	}
	
	public static class Constants extends AbstractMongoStringIdEntity.Constants{
		public static final String ENTITY_ID = "entityId";
		public static final String ENTITY_TYPE = "entityType";
		public static final String TOTAL_RATING_COUNT = "totalRatingCount";
		public static final String TOTAL_RATING = "totalRating";
		public static final String AVG_RATING = "avgRating";
		public static final String BASE_RATING_ASSIGNED = "baseRatingAssigned";
	}
        
        
//    public com.vedantu.session.pojo.CumilativeRating toRatingPojo() {
//        com.vedantu.session.pojo.CumilativeRating ratingPojo = new com.vedantu.session.pojo.CumilativeRating();
//        try {
//            BeanUtils.copyProperties(ratingPojo, this);
//        } catch (IllegalAccessException | InvocationTargetException ex) {
//            Logger.getLogger(CumilativeRating.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return ratingPojo;
//    }

}
