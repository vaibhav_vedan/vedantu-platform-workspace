package com.vedantu.platform.mongodbentities;

import com.vedantu.platform.pojo.TestAnalytics;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "TeachersDay2020")
public class TeachersDay2020 extends AbstractMongoLongIdEntity {
    @Indexed(background = true)
    private String firstName;
    private String lastName;
    private String fullName;
    private String profilePicUrl;
    private Integer postedWishesCount;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String FIRST_NAME = "firstName";
        public static final String POSTED_WISHES_COUNT = "postedWishesCount";
        public static final String FULL_NAME = "fullName";
        public static final String LAST_NAME = "lastName";
        public static final String PROFILE_PIC_URL = "profilePicUrl";

    }
}