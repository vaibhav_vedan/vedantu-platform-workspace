package com.vedantu.platform.mongodbentities.mobile;

import com.vedantu.platform.enums.mobile.MobilePlatform;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;



public class MobileConfig extends AbstractMongoStringIdEntity {

	private Long userId;
	private String key;
	private String value;
	private String deviceId;
	private String versionCode;
	private MobilePlatform platform;
	private String platformVersion;

	public MobileConfig() {
		super();
	}

	public MobileConfig(Long userId, String key, String value, String deviceId, String versionCode,
			MobilePlatform platform, String platformVersion) {
		super();
		this.userId = userId;
		this.key = key;
		this.value = value;
		this.deviceId = deviceId;
		this.versionCode = versionCode;
		this.platform = platform;
		this.platformVersion = platformVersion;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public MobilePlatform getPlatform() {
		return platform;
	}

	public void setPlatform(MobilePlatform platform) {
		this.platform = platform;
	}

	public String getPlatformVersion() {
		return platformVersion;
	}

	public void setPlatformVersion(String platformVersion) {
		this.platformVersion = platformVersion;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String KEY = "key";
		public static final String VALUE = "value";
		public static final String DEVICE_ID = "deviceId";
		public static final String VERSION_CODE = "versionCode";
		public static final String MOBILE_PLATFORM = "platform";
		public static final String PLATFORM_VERSION = "platformVersion";
	}

}
