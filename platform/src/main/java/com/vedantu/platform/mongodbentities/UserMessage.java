package com.vedantu.platform.mongodbentities;

import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.MessageType;

public class UserMessage extends AbstractMongoStringIdEntity {

	private Long userId;
	
	private MessageType messageType;
	
	private String message;
	
	private Long referenceTime;

	// below fields are specific to ASK_DOUBT or where ever else it needed
	
	private String subjectName;
	
	private String categoryName;
	
	private String grade;
	
	private List<String> fileUrls;

	public UserMessage() {

		super();
	}

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public MessageType getMessageType() {

		return messageType;
	}

	public void setMessageType(MessageType messageType) {

		this.messageType = messageType;
	}

	public String getMessage() {

		return message;
	}

	public String getMessageString() {
		return message;
	}
	public void setMessage(String message) {

		this.message = message;
	}

	public Long getReferenceTime() {
		return referenceTime;
	}

	public void setReferenceTime(Long referenceTime) {
		this.referenceTime = referenceTime;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public List<String> getFileUrls() {
		return fileUrls;
	}

	public void setFileUrls(List<String> fileUrls) {
		this.fileUrls = fileUrls;
	}
}
