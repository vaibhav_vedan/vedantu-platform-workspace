/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/**
 *
 * @author ajith
 */
public class LeadData extends AbstractMongoStringIdEntity {

    public String mobileno;
    public String campaign;
    public String source;

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
}
