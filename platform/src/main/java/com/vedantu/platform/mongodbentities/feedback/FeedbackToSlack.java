package com.vedantu.platform.mongodbentities.feedback;

import com.vedantu.User.Role;
import com.vedantu.lms.cmds.enums.SessionType;

import java.util.List;

public class FeedbackToSlack {

    //OTF
    private Role role;
    private String sessionId;
    private String userId;
    private String comment;
    private List<String> posTags;
    private List<String> negTags;
    private int rating;
    //OTO
    private String senderId;
    private int interactionRating;
    private int contentProvidedRating;
    private int technologyRating;
    private int contentCoverageRating;
    private int teacherRating;
    private int attendeeCount;
    private List<String> teacherImprovement;
    private List<String> technologyImprovement;
    private Boolean satisfied;


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getPosTags() {
        return posTags;
    }

    public void setPosTags(List<String> posTags) {
        this.posTags = posTags;
    }

    public List<String> getNegTags() {
        return negTags;
    }

    public void setNegTags(List<String> negTags) {
        this.negTags = negTags;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public int getInteractionRating() {
        return interactionRating;
    }

    public void setInteractionRating(int interactionRating) {
        this.interactionRating = interactionRating;
    }

    public int getContentProvidedRating() {
        return contentProvidedRating;
    }

    public void setContentProvidedRating(int contentProvidedRating) {
        this.contentProvidedRating = contentProvidedRating;
    }

    public int getTechnologyRating() {
        return technologyRating;
    }

    public void setTechnologyRating(int technologyRating) {
        this.technologyRating = technologyRating;
    }

    public int getContentCoverageRating() {
        return contentCoverageRating;
    }

    public void setContentCoverageRating(int contentCoverageRating) {
        this.contentCoverageRating = contentCoverageRating;
    }

    public int getTeacherRating() {
        return teacherRating;
    }

    public void setTeacherRating(int teacherRating) {
        this.teacherRating = teacherRating;
    }

    public int getAttendeeCount() {
        return attendeeCount;
    }

    public void setAttendeeCount(int attendeeCount) {
        this.attendeeCount = attendeeCount;
    }

    public List<String> getTeacherImprovement() {
        return teacherImprovement;
    }

    public void setTeacherImprovement(List<String> teacherImprovement) {
        this.teacherImprovement = teacherImprovement;
    }

    public List<String> getTechnologyImprovement() {
        return technologyImprovement;
    }

    public void setTechnologyImprovement(List<String> technologyImprovement) {
        this.technologyImprovement = technologyImprovement;
    }

    public Boolean getSatisfied() {
        return satisfied;
    }

    public void setSatisfied(Boolean satisfied) {
        this.satisfied = satisfied;
    }
}
