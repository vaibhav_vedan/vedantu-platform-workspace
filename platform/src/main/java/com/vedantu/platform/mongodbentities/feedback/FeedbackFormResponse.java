/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.feedback;

import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamicSection;
import com.vedantu.platform.pojo.feedback.FeedbackQuestion;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;

/**
 *
 * @author parashar
 */
public class FeedbackFormResponse extends AbstractMongoStringIdEntity {
    
    private String parentFormId;
    private String formId;
    private List<FeedbackQuestion> staticQuestions;
    private List<FeedbackFormDynamicSection> feedbackFormDynamicSections;
    private EntityType contextType;
    private String contextId;
    private FeedbackCommunication feedbackCommunication;
    
    private Long userId;

    /**
     * @return the parentFormId
     */
    public String getParentFormId() {
        return parentFormId;
    }

    /**
     * @param parentFormId the parentFormId to set
     */
    public void setParentFormId(String parentFormId) {
        this.parentFormId = parentFormId;
    }

    /**
     * @return the staticQuestions
     */
    public List<FeedbackQuestion> getStaticQuestions() {
        return staticQuestions;
    }

    /**
     * @param staticQuestions the staticQuestions to set
     */
    public void setStaticQuestions(List<FeedbackQuestion> staticQuestions) {
        this.staticQuestions = staticQuestions;
    }

    /**
     * @return the feedbackFormDynamicSections
     */
    public List<FeedbackFormDynamicSection> getFeedbackFormDynamicSections() {
        return feedbackFormDynamicSections;
    }

    /**
     * @param feedbackFormDynamicSections the feedbackFormDynamicSections to set
     */
    public void setFeedbackFormDynamicSections(List<FeedbackFormDynamicSection> feedbackFormDynamicSections) {
        this.feedbackFormDynamicSections = feedbackFormDynamicSections;
    }

    /**
     * @return the contextType
     */
    public EntityType getContextType() {
        return contextType;
    }

    /**
     * @param contextType the contextType to set
     */
    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the formId
     */
    public String getFormId() {
        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String FORM_ID = "formId";

        
    }     

    /**
     * @return the feedbackCommunication
     */
    public FeedbackCommunication getFeedbackCommunication() {
        return feedbackCommunication;
    }

    /**
     * @param feedbackCommunication the feedbackCommunication to set
     */
    public void setFeedbackCommunication(FeedbackCommunication feedbackCommunication) {
        this.feedbackCommunication = feedbackCommunication;
    }
    
}
