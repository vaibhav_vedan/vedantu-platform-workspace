/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.social;

import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.pojo.ViewSession;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 *
 * @author parashar
 */
public class View extends AbstractMongoStringIdEntity{
  
    private SocialContextType socialContextType;
    @Indexed(background = true)
    private String contextId;
    @Indexed(background = true)
    private Long userId;
    private List<ViewSession> viewSessions = new ArrayList<>();
    /**
     * @return the socialContextType
     */
    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    /**
     * @param socialContextType the socialContextType to set
     */
    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the viewSessions
     */
    public List<ViewSession> getViewSessions() {
        return viewSessions;
    }

    /**
     * @param viewSessions the viewSessions to set
     */
    public void setViewSessions(List<ViewSession> viewSessions) {
        this.viewSessions = viewSessions;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String SOCIAL_CONTEXT_TYPE = "socialContextType";
        public static final String CONTEXT_ID = "contextId";
        public static final String USER_ID = "userId";

    }    
    
}
