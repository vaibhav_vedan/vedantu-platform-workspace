package com.vedantu.platform.mongodbentities;

import com.vedantu.User.Role;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "NonStudentRoleCreator")
public class NonStudentRoleCreator extends AbstractMongoStringIdEntity {

    @Indexed(background = true, unique = true)
    private Long creatorId;
    private List<Role> allowedRoles;

    public NonStudentRoleCreator(){
        super();
    }

    public NonStudentRoleCreator(Long creatorId, List<Role> allowedRoles){
        this.creatorId = creatorId;
        this.allowedRoles = allowedRoles;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public List<Role> getAllowedRoles() {
        return allowedRoles;
    }

    public void setAllowedRoles(List<Role> allowedRoles) {
        this.allowedRoles = allowedRoles;
    }

    public static class CONSTANTS extends AbstractMongoStringIdEntity.Constants{
        public static final String CREATOR_ID = "creatorId";
        public static final String ALLOWED_ROLES = "allowedRoles";
    }
}
