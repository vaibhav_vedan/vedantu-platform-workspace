/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.mongodbentities.social;

import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "Comment")
public class Comment extends AbstractMongoStringIdEntity {

    private SocialContextType socialContextType;
    @Indexed(background = true)
    private String contextId;
    private String parentId; //parentId of Comment for nlevel commenting
    private String rootCommentId;
    @Indexed(background = true)
    private Long userId;
    private RichTextFormat comment;

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public RichTextFormat getComment() {
        return comment;
    }

    public void setComment(RichTextFormat comment) {
        this.comment = comment;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getRootCommentId() {
        return rootCommentId;
    }

    public void setRootCommentId(String rootCommentId) {
        this.rootCommentId = rootCommentId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String SOCIAL_CONTEXT_TYPE = "socialContextType";
        public static final String CONTEXT_ID = "contextId";
        public static final String PARENT_ID = "parentId";
        public static final String REPLIES = "replies";
        
    }

}
