package com.vedantu.platform.cron;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.listing.pojo.GetTeacherResponse;
import com.vedantu.platform.controllers.listing.ListingController;
import com.vedantu.platform.managers.listing.ListingManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@RestController
@RequestMapping("/listing/updateFaceCount")
public class LisitingCountUpdateCron {

	@Autowired
	private LogFactory logFactory;

	private Logger logger = logFactory.getLogger(LisitingCountUpdateCron.class);

	@Autowired
	ListingController listingController;

	@Autowired
	ListingManager listingManager;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public void updateFaceCount(
			@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {

		logger.info(request);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request,
				AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = "
					+ subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE
					.doCall(subscriptionRequest.getSubscribeURL(),
							HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			String GTR = "{\"functions\":{\"name\":\"none\",\"value\":[{\"type\":\"script\"}]},\"filter\":{\"type\":\"bool\",\"params\":[{\"name\":\"must\",\"value\":[{\"type\":\"term\",\"params\":[{\"name\":\"active\",\"value\":[true]}]}]}]},\"sort\":{\"params\":[]},\"from\":0,\"size\":250}";

			String getTeachersResponseString = listingManager
					.getTeachers(GTR);

			GetTeacherResponse getTeacherResponse = new Gson().fromJson(
					getTeachersResponseString, GetTeacherResponse.class);

			List<EsTeacherData> teachers = getTeacherResponse.getTeachers();

			for (int i = 0; i < getTeacherResponse.getHits(); i += 30) {
				if (i + 30 > getTeacherResponse.getHits()) {
					listingManager.updateFaceCount(teachers.subList(i,
							(int) getTeacherResponse.getHits()));
				} else {
					listingManager.updateFaceCount(teachers.subList(i, i + 30));
				}
			}

		}
	}

}
