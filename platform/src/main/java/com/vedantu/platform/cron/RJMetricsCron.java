package com.vedantu.platform.cron;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.dao.board.BoardDAO;
import com.vedantu.platform.dao.board.TeacherBoardMappingDAO;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.dao.offering.OfferingDao;
import com.vedantu.platform.mongodbentities.offering.Offering;
import com.vedantu.platform.dao.review.FeedbackDao;
import com.vedantu.platform.dao.review.ReviewDao;
import com.vedantu.platform.mongodbentities.review.Feedback;
import com.vedantu.platform.mongodbentities.review.Review;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.rjmetrics.RJMetricsExportManager;
import com.vedantu.util.rjmetrics.pojos.FeedbackRjPojo;
import com.vedantu.util.rjmetrics.pojos.OfferingRjPojo;
import com.vedantu.util.rjmetrics.pojos.ReviewRjPojo;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/export/rjMetrics")
public class RJMetricsCron {

    

    @Autowired
    private ReviewDao reviewDao;

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private OfferingDao offeringDao;

    @Autowired
    private TeacherBoardMappingDAO tbmdao;

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RJMetricsExportManager rJMetricsExportManager;

    @Autowired
    private DozerBeanMapper mapper;

    private Logger logger = logFactory.getLogger(RJMetricsCron.class);

    private String RJMETRICS_BASE_URL;


    public RJMetricsCron() {
        RJMETRICS_BASE_URL = "https://connect.rjmetrics.com/v2/client/{{clientId}}/table/{{table}}/data?apikey={{apiKey}}";
        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{clientId}}",
                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.id"));
        RJMETRICS_BASE_URL = RJMETRICS_BASE_URL.replace("{{apiKey}}",
                ConfigUtils.INSTANCE.getStringValue("rjmetrics.client.apiKey"));
    }

    @RequestMapping(value = "/updateRJMetrics", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void updateRJMetrics(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws IllegalAccessException, VException {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long fromTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY;
            Long tillTime = null;

            try {
                postReviewData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postReviewData", ex);
            }
            try {
                postFeedbackData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postFeedbackData", ex);
            }
            try {
                postOfferingData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postOfferingData", ex);
            }
            try {
                postTeacherBoardMappingData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postTeacherBoardMappingData", ex);
            }

            try {
                postBoardData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postBoardData", ex);
            }

            try {
                postUserSubsystemData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postUserSubsystemData", ex);
            }

            try {
                postSchedulingSubsystemData(fromTime, tillTime);
            } catch(Exception ex) {
                logger.error("Error in updating RJ : postSchedulingSubsystemData", ex);
            }

        }
    }

    private void postReviewData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<Review> entities = reviewDao.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, Review.class);
        if (entities != null) {
            List<ReviewRjPojo> rjEntities = new ArrayList<>();
            for (Review review : entities) {
//                ReviewRjPojo rjPojo = new ReviewRjPojo(review.getUserId(), review.getEntityId(), review.getEntityType(),
//                        review.getRating(), new Text(review.getReview()), review.getIsReviewed(), review.getContextType(),
//                        review.getContextId(), review.getReason(), review.getPriority(), review.getReviewExtraInfo(),
//                        review.getId(), review.getCreationTime(), review.getCreatedBy(), review.getLastUpdated(),
//                        review.getLastUpdatedBy());
                try {
                    ReviewRjPojo rjPojo = mapper.map(review, ReviewRjPojo.class);

                    rjEntities.add(rjPojo);
                } catch(Exception ex) {
                    logger.error("Error in updating RJ entry: ", ex);
                }
            }
            rJMetricsExportManager.postData(new ArrayList<>(rjEntities), RJMETRICS_BASE_URL.replace("{{table}}", Review.class.getSimpleName()));
        }
    }

    private void postFeedbackData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<Feedback> entities = feedbackDao.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, Feedback.class);
        if (entities != null) {
            List<FeedbackRjPojo> rjEntities = new ArrayList<>();
            for (Feedback feedback : entities) {
//                FeedbackRjPojo rjPojo = new FeedbackRjPojo(feedback.getReceiverId(), feedback.getSenderId(),
//                        feedback.getSessionId(), feedback.getRating(), new Text(feedback.getSessionMessage()),
//                        new Text(feedback.getPartnerMessage()), feedback.getReason(), feedback.getOptionalMessage(),
//                        feedback.getSessionCoverage(), feedback.getNextSessionPlan(), feedback.getHomeWork(),
//                        feedback.getStudentPerformance(), feedback.getId(), feedback.getCreationTime(), feedback.getCreatedBy(),
//                        feedback.getLastUpdated(), feedback.getLastUpdatedBy());
                try {
                    FeedbackRjPojo rjPojo = mapper.map(feedback, FeedbackRjPojo.class);
                    rjEntities.add(rjPojo);
                } catch(Exception ex) {
                    logger.error("Error in updating RJ entry: ", ex);
                }

            }
            rJMetricsExportManager.postData(new ArrayList<>(rjEntities), RJMETRICS_BASE_URL.replace("{{table}}", Feedback.class.getSimpleName()));
        }
    }

    private void postOfferingData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<Offering> entities = offeringDao.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, Offering.class);
        if (entities != null) {
            List<OfferingRjPojo> rjEntities = new ArrayList<>();
            for (Offering offering : entities) {
//                OfferingRjPojo rjPojo = new OfferingRjPojo(offering.getTitle(),
//                        offering.getGrades(), offering.getBoardIds(),
//                        new Text(offering.getDescription()), new Text(offering.getTargetAudienceText()),
//                        offering.getPerHrPrice(), offering.getPrice(), offering.getDisplayPrice(),
//                        offering.getCurrencyCode(), new Text(offering.getRecommendedSchedule()),
//                        offering.getTags(), offering.getExamTags(), offering.getTotalDays(),
//                        offering.getTotalHours(), offering.getType(), offering.getImageUrls(),
//                        offering.getVideoUrls(), offering.getContents(), offering.getTeacherIds(),
//                        offering.getCategories(), offering.getParentTopics(), offering.getTakeAways(),
//                        offering.getCreatedByUserId(), offering.getIsFeatured(), offering.getSubtypes(),
//                        offering.getTeacherPlans(), offering.getOfferingLength(), offering.getId(),
//                        offering.getCreationTime(), offering.getLastUpdated());
                try {
                    OfferingRjPojo rjPojo = mapper.map(offering, OfferingRjPojo.class);
                    rjEntities.add(rjPojo);
                } catch(Exception ex) {
                    logger.error("Error in updating RJ entry: ", ex);
                }
            }

            rJMetricsExportManager.postData(new ArrayList<>(rjEntities), RJMETRICS_BASE_URL.replace("{{table}}", Offering.class.getSimpleName()));
        }
    }

    private void postTeacherBoardMappingData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<TeacherBoardMapping> entities = tbmdao.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, TeacherBoardMapping.class);
        if (entities != null) {
            rJMetricsExportManager.postData(new ArrayList<>(entities), RJMETRICS_BASE_URL.replace("{{table}}", TeacherBoardMapping.class.getSimpleName()));
        }
    }

    private void postBoardData(Long fromTime, Long tillTime) {
        Integer limit = 1000;
        List<Board> entities = boardDAO.getEntitiesByUpdationTimeAndLimit(fromTime, tillTime, limit, Board.class);
        if (entities != null) {
            rJMetricsExportManager.postData(new ArrayList<>(entities), RJMETRICS_BASE_URL.replace("{{table}}", Board.class.getSimpleName()));
        }
    }

    private void postUserSubsystemData(Long fromTime, Long tillTime) throws VException {
        String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        String tillTimeQuery = "";
        if(tillTime!=null) {
            tillTimeQuery = "&tillTime="+tillTime;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/export/rjMetrics/updateRJMetrics?fromTime="+fromTime+tillTimeQuery, HttpMethod.GET, null,true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String respString = resp.getEntity(String.class);
        logger.info("Response from postUserSubsystemData: " + respString);
    }

    private void postSchedulingSubsystemData(Long fromTime, Long tillTime) throws VException {
        String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
        String tillTimeQuery = "";
        if(tillTime!=null) {
            tillTimeQuery = "&tillTime="+tillTime;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/export/rjMetrics/updateRJMetrics?fromTime="+fromTime+tillTimeQuery, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String respString = resp.getEntity(String.class);
        logger.info("Response from postSchedulingSubsystemData: " + respString);
    }



}
