package com.vedantu.platform.request.misc;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class WebinarUserRegistrationInfoReq extends AbstractFrontEndReq {

    private String firstName;
    private String lastName;
    private String emailId;
    private String phone;
    private String userId;
    private String utm_source;
    private String utm_campaign;
    private String utm_medium;

    private String trainingId;
    private String timeZone = "Asia/Kolkata";
    private String registerJoinUrl;
    private String registerRegistrantKey;
    private String registerStatus;
    private List<String> responses;
    
    private String clevertapId;

    public WebinarUserRegistrationInfoReq() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(String trainingId) {
        this.trainingId = trainingId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getRegisterJoinUrl() {
        return registerJoinUrl;
    }

    public void setRegisterJoinUrl(String registerJoinUrl) {
        this.registerJoinUrl = registerJoinUrl;
    }

    public String getRegisterRegistrantKey() {
        return registerRegistrantKey;
    }

    public void setRegisterRegistrantKey(String registerRegistrantKey) {
        this.registerRegistrantKey = registerRegistrantKey;
    }

    public String getRegisterStatus() {
        return registerStatus;
    }

    public void setRegisterStatus(String registerStatus) {
        this.registerStatus = registerStatus;
    }

    public List<String> getResponses() {
        return responses;
    }

    public void setResponses(List<String> responses) {
        this.responses = responses;
    }

    public String getClevertapId() {
        return clevertapId;
    }

    public void setClevertapId(String clevertapId) {
        this.clevertapId = clevertapId;
    }

    public void updateUserBasicInfo(UserBasicInfo userBasicInfo) {
        if (userBasicInfo != null) {
            this.firstName = userBasicInfo.getFirstName();
            this.lastName = userBasicInfo.getLastName();
            this.emailId = userBasicInfo.getEmail();
            this.phone = userBasicInfo.getContactNumber();
            this.userId = String.valueOf(userBasicInfo.getUserId());
        }
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(emailId)) {
            errors.add("emailId");
        }
        if (StringUtils.isEmpty(trainingId)) {
            errors.add("trainingId");
        }
        return errors;
    }

}
