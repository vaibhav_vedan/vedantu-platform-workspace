package com.vedantu.platform.request;

import com.vedantu.util.fos.request.AbstractFrontEndUserReq;

/**
 * Created by somil on 24/01/17.
 */
public class ExportSubscriptionReq extends AbstractFrontEndUserReq {

    private Long tillTime;
    private Long fromTime;


    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

}
