package com.vedantu.platform.request;

import com.vedantu.platform.pojo.TestAnalytics;
import com.vedantu.util.fos.request.AbstractReq;

import java.util.List;

public class UpdateStatsPostTestReq extends AbstractReq {

    private String type;
    private String secretKey;
    private String userId;
    private String testId;
    private String testAttemptId;
    private String performanceCode;
    private List<String> areasToWorkOn;
    private TestAnalytics analytics;
    private String s3Link;
    private String sessionId;

    public String getS3Link() {
        return s3Link;
    }

    public void setS3Link(String s3Link) {
        this.s3Link = s3Link;
    }

    public TestAnalytics getAnalytics() {
        return analytics;
    }

    public void setAnalytics(TestAnalytics analytics) {
        this.analytics = analytics;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTestAttemptId() {
        return testAttemptId;
    }

    public void setTestAttemptId(String testAttemptId) {
        this.testAttemptId = testAttemptId;
    }

    public String getPerformanceCode() {
        return performanceCode;
    }

    public void setPerformanceCode(String performanceCode) {
        this.performanceCode = performanceCode;
    }

    public List<String> getAreasToWorkOn() {
        return areasToWorkOn;
    }

    public void setAreasToWorkOn(List<String> areasToWorkOn) {
        this.areasToWorkOn = areasToWorkOn;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
