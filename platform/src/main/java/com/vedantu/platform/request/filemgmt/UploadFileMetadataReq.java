/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class UploadFileMetadataReq extends AbstractFrontEndReq {

    private Long sessionId;
    private Long userId;
    private Long localDBId;
    private String fileData;
    private Long fileId;
    private String fileType;

    public Long getLocalDBId() {
        return localDBId;
    }

    public void setLocalDBId(Long localId) {
        this.localDBId = localId;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public Long getSessionId() {

        return sessionId;
    }

    public void setSessionId(Long sessionId) {

        this.sessionId = sessionId;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public Long getFileId() {

        return fileId;
    }

    public void setFileId(Long fileId) {

        this.fileId = fileId;
    }

    public String getFileType() {

        return fileType;
    }

    public void setFileType(String fileType) {

        this.fileType = fileType;
    }

    /*
	public FileMetadata toFileUploadMetadata() {

		FileMetadata fileUploadMetadata = new FileMetadata();
		fileUploadMetadata.setFileId(getFileId());
		fileUploadMetadata.setFileType(getFileType());
		fileUploadMetadata.setCreationTime(Calendar.getInstance().getTimeInMillis());
		fileUploadMetadata.setSessionId(getSessionId());
		fileUploadMetadata.setUserId(getUserId());
		return fileUploadMetadata;
	}
     */
    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == sessionId) {
            errors.add("sessionId");
        }

        if (null == userId) {
            errors.add("userId");
        }
        return errors;
    }

}
