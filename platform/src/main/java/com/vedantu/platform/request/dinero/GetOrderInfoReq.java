package com.vedantu.platform.request.dinero;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetOrderInfoReq extends AbstractFrontEndReq {

	public GetOrderInfoReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (orderId == null) {
			errors.add("orderId");
		}
		return errors;
	}

	@Override
	public String toString() {
		return "GetOrderInfoReq [orderId=" + orderId + "]";
	}

}
