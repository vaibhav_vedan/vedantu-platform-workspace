/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 *
 * @author parashar
 */
@Data
public class GetSessionsReq extends AbstractFrontEndListReq {
    
    private Integer start;
    private Integer size = 20;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String query;
    @Deprecated private String batchId;
    private Set<String> batchIds;
    private Long startTime;
    private Long endTime;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        Predicate<Long> function = (e) -> e != null && e > 0;
        if (function.test(startTime) && !function.test(endTime)) {
            errors.add("endTime is mandatory when start time is specified");
        }

        if (!function.test(startTime) && function.test(endTime)) {
            errors.add("startTime is mandatory when end time is specified");
        }

        if (function.test(startTime) && function.test(endTime) && startTime > endTime) {
            errors.add("startTime is greater than endTime");
        }

        if (StringUtils.isNotBlank(batchId) && ArrayUtils.isNotEmpty(batchIds)) {
            errors.add("both batchid and array of batchids specified. specify either onr");
        }
        return errors;
    }
}
