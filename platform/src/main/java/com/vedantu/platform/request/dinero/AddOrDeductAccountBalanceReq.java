package com.vedantu.platform.request.dinero;

import java.util.List;

import com.vedantu.platform.enums.HolderType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddOrDeductAccountBalanceReq extends AbstractFrontEndReq {

	private String userId;
	private HolderType holderType;
	private int verificationCode;
	private String verificationId;
	private int amountToAdd;
	private String reasonNote;
	private Boolean credit;

	public AddOrDeductAccountBalanceReq(String userId, HolderType holderType, int verificationCode, String verificationId,
			int amountToAdd, String reasonNote, Boolean credit) {
		super();
		this.userId = userId;
		this.holderType = holderType;
		this.verificationCode = verificationCode;
		this.verificationId = verificationId;
		this.amountToAdd = amountToAdd;
		this.reasonNote = reasonNote;
		this.credit = credit;
	}

	public Boolean getCredit() {
		return credit;
	}

	public void setCredit(Boolean credit) {
		this.credit = credit;
	}

	public String getReasonNote() {
		return reasonNote;
	}

	public void setReasonNote(String reasonNote) {
		this.reasonNote = reasonNote;
	}


	public int getAmountToAdd() {
		return amountToAdd;
	}

	public void setAmountToAdd(int amountToAdd) {
		this.amountToAdd = amountToAdd;
	}

	public String getVerificationId() {
		return verificationId;
	}

	public void setVerificationId(String verificationId) {
		this.verificationId = verificationId;
	}

	@Override
	public String toString() {
		return "AddAccountBalanceReq [userId=" + userId + ", holderType=" + holderType + ", verificationCode="
				+ verificationCode + ", verificationId=" + verificationId + ", amountToAdd=" + amountToAdd
				+ ", reasonNote=" + reasonNote + ", credit=" + credit + "]";
	}

	public AddOrDeductAccountBalanceReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HolderType getHolderType() {
		return holderType;
	}

	public void setHolderType(HolderType holderType) {
		this.holderType = holderType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(int verificationCode) {
		this.verificationCode = verificationCode;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(reasonNote)) {
			errors.add("reasonNote");
		}
		return errors;
	}

}
