package com.vedantu.platform.request;

import com.vedantu.platform.enums.AvailabilityExportType;
import com.vedantu.platform.pojo.AvailabilitySlotPojo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.commons.lang3.StringUtils;

import java.util.List;


/**
 * Created by somil on 25/01/17.
 */
public class AvailabilityTeacherExportReq extends AbstractFrontEndReq {

    private String boardId;
    private String grade;
    private String subject;
    private String target;
    private String teacherId;
    private String teacherEmailId;
    private AvailabilityExportType exportType;
    private List<AvailabilitySlotPojo> slots;

    public AvailabilityTeacherExportReq() {
        super();
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<AvailabilitySlotPojo> getSlots() {
        return slots;
    }

    public void setSlots(List<AvailabilitySlotPojo> slots) {
        this.slots = slots;
    }

    public String getTeacherEmailId() {
        return teacherEmailId;
    }

    public void setTeacherEmailId(String teacherEmailId) {
        this.teacherEmailId = teacherEmailId;
    }

    public AvailabilityExportType getExportType() {
        return exportType;
    }

    public void setExportType(AvailabilityExportType exportType) {
        this.exportType = exportType;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String constructQueryString() {
        if (!StringUtils.isEmpty(boardId) && boardId.equals("null")) {
            this.boardId = "";
        }
        if (!StringUtils.isEmpty(grade) && grade.equals("null")) {
            this.grade = "";
        }
        if (!StringUtils.isEmpty(subject) && subject.equals("null")) {
            this.subject = "";
        }
        if (!StringUtils.isEmpty(teacherId) && teacherId.equals("null")) {
            this.teacherId = "";
        }

        return "boardId=" + boardId + "&grade=" + grade + "&subject=" + subject + "&target=" + target + "&teacherId="
                + teacherId;
    }

    @Override
    public String toString() {
        return "AvailabilityTeacherExportReq [boardId=" + boardId + ", grade=" + grade + ", subject=" + subject
                + ", target=" + target + ", teacherId=" + teacherId + ", teacherEmailId=" + teacherEmailId
                + ", exportType=" + exportType + ", slots=" + slots + ", toString()=" + super.toString() + "]";
    }
}
