/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.usermessage;

import com.vedantu.platform.mongodbentities.UserMessage;
import com.vedantu.util.enums.MessageType;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.fos.request.ValidStringList;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author jeet
 */
public class SendUserMessageReq extends AbstractFrontEndUserReq {

    private MessageType messageType;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String message;
    private Long referenceTime;

    // below fields are specific to ASK_DOUBT or where ever else it needed
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String subjectName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String categoryName;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String grade;
    @ValidStringList(message = ReqLimitsErMsgs.STRING_LIST_MAX)
    private List<String> fileUrls;

    public MessageType getMessageType() {

        return messageType;
    }

    public void setMessageType(MessageType messageType) {

        this.messageType = messageType;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public Long getReferenceTime() {
        return referenceTime;
    }

    public void setReferenceTime(Long referenceTime) {
        this.referenceTime = referenceTime;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<String> getFileUrls() {
        return fileUrls;
    }

    public void setFileUrls(List<String> fileUrls) {
        this.fileUrls = fileUrls;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == getUserId()) {
            errors.add(Constants.USER_ID);
        }
        if (null == messageType) {
            errors.add(Constants.MESSAGE_TYPE);
        }
        if (null == message) {
            errors.add(Constants.MESSAGE);
        }
        return errors;
    }

    public static class Constants {

        public static final String USER_ID = "userId";
        public static final String MESSAGE_TYPE = "messageType";
        public static final String MESSAGE = "message";
        public static final String CREATION_TIME = "creationTime";
    }

    public UserMessage toMessage() {

        UserMessage userMessage = new UserMessage();
        userMessage.setMessageType(messageType);
        userMessage.setUserId(getUserId());
        userMessage.setMessage(message);
        userMessage.setReferenceTime(referenceTime);
        userMessage.setCategoryName(categoryName);
        userMessage.setGrade(grade);
        userMessage.setSubjectName(subjectName);
        userMessage.setFileUrls(fileUrls);
        return userMessage;
    }
}
