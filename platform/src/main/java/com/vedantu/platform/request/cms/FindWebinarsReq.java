/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.cms;

import com.vedantu.platform.enums.cms.TimeFrame;
import com.vedantu.platform.enums.cms.WebinarStatus;
import com.vedantu.platform.enums.cms.WebinarToolType;
import com.vedantu.platform.enums.cms.WebinarType;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author jeet
 */
public class FindWebinarsReq extends AbstractFrontEndListReq {

    private String webinarCode;
    private String grade;
    private String target;
    private String tag;
    private String board;
    private String title;
    private String teacherEmail;
    private Long afterStartTime;
    private Long beforeStartTime;
    private Long afterEndTime;
    private Long beforeEndTime;
    private WebinarStatus webinarStatus;
    private Boolean showInPastWebinars;
    private SortOrder sortOrder;
    private WebinarType type;
    private WebinarToolType toolType;
    private Boolean isSimLive;
    private Boolean showSimLiveWebinars;
    private TimeFrame timeFrame;
    private Integer webinarToSessionLimit; 
    private String studentEmail;


    private Boolean restrict;

    public Boolean isRestricted() {
        return restrict;
    }

    public void setRestrict(Boolean restrict) {
        this.restrict = restrict;
    }

    public String getWebinarCode() {
        return webinarCode;
    }

    public void setWebinarCode(String webinarCode) {
        this.webinarCode = webinarCode;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public Long getAfterStartTime() {
        return afterStartTime;
    }

    public void setAfterStartTime(Long afterStartTime) {
        this.afterStartTime = afterStartTime;
    }

    public Long getBeforeStartTime() {
        return beforeStartTime;
    }

    public void setBeforeStartTime(Long beforeStartTime) {
        this.beforeStartTime = beforeStartTime;
    }

    public WebinarStatus getWebinarStatus() {
        return webinarStatus;
    }

    public void setWebinarStatus(WebinarStatus webinarStatus) {
        this.webinarStatus = webinarStatus;
    }

    public Long getAfterEndTime() {
        return afterEndTime;
    }

    public void setAfterEndTime(Long afterEndTime) {
        this.afterEndTime = afterEndTime;
    }

    public Long getBeforeEndTime() {
        return beforeEndTime;
    }

    public void setBeforeEndTime(Long beforeEndTime) {
        this.beforeEndTime = beforeEndTime;
    }

    public Boolean getShowInPastWebinars() {
        return showInPastWebinars;
    }

    public void setShowInPastWebinars(Boolean showInPastWebinars) {
        this.showInPastWebinars = showInPastWebinars;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

	public WebinarType getType() {
		return type;
	}

	public void setType(WebinarType type) {
		this.type = type;
	}

    public WebinarToolType getToolType() {
        return toolType;
    }

    public void setToolType(WebinarToolType toolType) {
        this.toolType = toolType;
    }

    public Boolean getShowSimLiveWebinars() {
        return showSimLiveWebinars;
    }

    public void setShowSimLiveWebinars(Boolean showSimLiveWebinars) {
        this.showSimLiveWebinars = showSimLiveWebinars;
    }

    public TimeFrame getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(TimeFrame timeFrame) {
        this.timeFrame = timeFrame;
    }

	public Integer getWebinarToSessionLimit() {
		return webinarToSessionLimit;
	}

	public void setWebinarToSessionLimit(Integer webinarToSessionLimit) {
		this.webinarToSessionLimit = webinarToSessionLimit;
	}
	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

    public Boolean getSimLive() {
        return isSimLive;
    }

    public void setSimLive(Boolean simLive) {
        isSimLive = simLive;
    }
}
