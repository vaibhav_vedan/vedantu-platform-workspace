/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.User.User;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class CreateTeacherBoardMappingReq extends AbstractFrontEndReq {

	private Long userId;// teacherId
	private String category;
	private String grade;
	private List<Long> boardIds;

        /*
	public CreateTeacherBoardMappingReq(HttpServletRequest req) {
		super(req);
	}


	@Override
	protected void populate(HttpServletRequest req) {
		userId = getLongFieldValue(Constants.USER_ID, req);
		category = getStringFieldValue(Constants.CATEGORY, req);
		grade = getStringFieldValue(Constants.GRADE, req);
		boardIds = getListFieldLongValue(Constants.BOARD_IDS, req);

	}
        */

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (userId == null || userId == 0) {
			errors.add(User.Constants.USER_ID);
		}

		if (StringUtils.isEmpty(category)) {
			errors.add(TeacherBoardMapping.Constants.CATEGORY);
		}

		if (StringUtils.isEmpty(grade)) {
			errors.add(TeacherBoardMapping.Constants.GRADE);
		}
		return errors;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public List<Long> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(List<Long> boardIds) {
		this.boardIds = boardIds;
	}

	public TeacherBoardMapping toTeacherMapping() {
		TeacherBoardMapping teacherBoardMapping = new TeacherBoardMapping(
				userId, category, grade, boardIds);
		return teacherBoardMapping;
	}

	@Override
	public String toString() {
		return String.format("{userId=%s, category=%s, grade=%s, boardIds=%s}",
				userId, category, grade, boardIds);
	}

}

