package com.vedantu.platform.request.cms;


import com.vedantu.platform.pojo.cms.Testimonial;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CreateTestimonialReq extends AbstractFrontEndReq {
	private Testimonial testimonial;

	public Testimonial getTestimonial() {
		return testimonial;
	}

	public void setTestimonial(Testimonial testimonial) {
		this.testimonial = testimonial;
	}
	
}
