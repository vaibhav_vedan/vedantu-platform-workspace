/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.platform.pojo.ISLStudentData;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class CreateUserDetailsFromReq extends AbstractFrontEndReq {

    private List<ISLStudentData> studentList;

    public CreateUserDetailsFromReq() {
        super();
    }

    public List<ISLStudentData> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<ISLStudentData> studentList) {
        this.studentList = studentList;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (studentList.isEmpty()) {
            errors.add("empty student list");
        } else {
            for (ISLStudentData studentData : studentList) {
                if (StringUtils.isEmpty(studentData.getEmail())) {
                    errors.add("Email empty for student");
                    break;
                }
                if (StringUtils.isEmpty(studentData.getGrade())) {
                    errors.add("grade empty for student");
                    break;
                }
                if (StringUtils.isEmpty(studentData.getParentPhoneCode())) {
                    errors.add("phonecode empty for student");
                    break;
                }
                if (StringUtils.isEmpty(studentData.getParentPhoneNo())) {
                    errors.add("phoneno empty for student");
                    break;
                }
                if (StringUtils.isEmpty(studentData.getStudentName())) {
                    errors.add("studentname empty for student");
                    break;
                }                
            }
        }
        if (studentList != null && studentList.size() > 100) {
            errors.add("student list > 100");
        }
        return errors;
    }

}
