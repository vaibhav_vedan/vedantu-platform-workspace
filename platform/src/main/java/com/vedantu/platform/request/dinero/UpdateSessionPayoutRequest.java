package com.vedantu.platform.request.dinero;

import com.vedantu.platform.enums.DurationConflictReason;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class UpdateSessionPayoutRequest extends AbstractFrontEndReq {

    private Long sessionId;
    private Long newDuration;
    private Long subscriptionId;
    private SessionModel model;
    private Long teacherId;
    private DurationConflictReason reason;
    private String reasonDescription;
    private Long hourlyRate;
    private Long studentId;
    private Long raisedByUserId;
    private boolean refundToStudentWallet;
    private Long remainingHours;
    private Long consumedHours;
    private Long totalHours;

    public UpdateSessionPayoutRequest(Long sessionId, Long newDuration, Long subscriptionId, SessionModel model,
            Long teacherId, DurationConflictReason reason, String reasonDescription, Long hourlyRate, Long studentId,
            Long raisedByUserId, boolean refundToStudentWallet, Long remainingHours, Long consumedHours,
            Long totalHours) {
        super();
        this.sessionId = sessionId;
        this.newDuration = newDuration;
        this.subscriptionId = subscriptionId;
        this.model = model;
        this.teacherId = teacherId;
        this.reason = reason;
        this.reasonDescription = reasonDescription;
        this.hourlyRate = hourlyRate;
        this.studentId = studentId;
        this.raisedByUserId = raisedByUserId;
        this.refundToStudentWallet = refundToStudentWallet;
        this.remainingHours = remainingHours;
        this.consumedHours = consumedHours;
        this.totalHours = totalHours;
    }

    public Long getConsumedHours() {
        return consumedHours;
    }

    public void setConsumedHours(Long consumedHours) {
        this.consumedHours = consumedHours;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public UpdateSessionPayoutRequest(Long sessionId, Long newDuration, Long subscriptionId, SessionModel model,
            Long teacherId, DurationConflictReason reason, String reasonDescription, Long hourlyRate, Long studentId,
            Long raisedByUserId, boolean refundToStudentWallet, Long remainingHours) {
        super();
        this.sessionId = sessionId;
        this.newDuration = newDuration;
        this.subscriptionId = subscriptionId;
        this.model = model;
        this.teacherId = teacherId;
        this.reason = reason;
        this.reasonDescription = reasonDescription;
        this.hourlyRate = hourlyRate;
        this.studentId = studentId;
        this.raisedByUserId = raisedByUserId;
        this.refundToStudentWallet = refundToStudentWallet;
        this.remainingHours = remainingHours;
    }

    public Long getRaisedByUserId() {
        return raisedByUserId;
    }

    public void setRaisedByUserId(Long raisedByUserId) {
        this.raisedByUserId = raisedByUserId;
    }

    public boolean isRefundToStudentWallet() {
        return refundToStudentWallet;
    }

    public void setRefundToStudentWallet(boolean refundToStudentWallet) {
        this.refundToStudentWallet = refundToStudentWallet;
    }

    public Long getRemainingHours() {
        return remainingHours;
    }

    public void setRemainingHours(Long remainingHours) {
        this.remainingHours = remainingHours;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getNewDuration() {
        return newDuration;
    }

    public void setNewDuration(Long newDuration) {
        this.newDuration = newDuration;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public DurationConflictReason getReason() {
        return reason;
    }

    public void setReason(DurationConflictReason reason) {
        this.reason = reason;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public Long getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public UpdateSessionPayoutRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (sessionId == null) {
            errors.add("sessionId");
        }
        if (newDuration == null) {
            errors.add("newDuration");
        }
        return errors;
    }

}
