package com.vedantu.platform.request.wave;

import com.vedantu.platform.enums.wave.WavebookScope;
import com.vedantu.platform.pojo.wave.WavebookType;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import java.util.Set;

public class EditWavebookReq extends AbstractFrontEndReq {

    private String wavebookId;

    private String title;

    private String pagesMetaData;

    private WavebookScope wavebookScope;

    private List<String> grades;

    private List<String> subjects;

    private WavebookType wavebookType;

    private List<SessionPagesMetadata> pagesMetaDataNew;

    private Set<String> targetGrades;
    private Set<String> topics;

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == wavebookId) {
            errors.add("wavebookId");
        }

        return errors;
    }

    public String getWavebookId() {
        return wavebookId;
    }

    public void setWavebookId(String wavebookId) {
        this.wavebookId = wavebookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPagesMetaData() {
        return pagesMetaData;
    }

    public void setPagesMetaData(String pagesMetaData) {
        this.pagesMetaData = pagesMetaData;
    }

    public WavebookScope getWavebookScope() {
        return wavebookScope;
    }

    public void setWavebookScope(WavebookScope wavebookScope) {
        this.wavebookScope = wavebookScope;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public WavebookType getWavebookType() {
        return wavebookType;
    }

    public void setWavebookType(WavebookType wavebookType) {
        this.wavebookType = wavebookType;
    }

    public List<SessionPagesMetadata> getPagesMetaDataNew() {
        return pagesMetaDataNew;
    }

    public void setPagesMetaDataNew(List<SessionPagesMetadata> pagesMetaDataNew) {
        this.pagesMetaDataNew = pagesMetaDataNew;
    }

    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

}
