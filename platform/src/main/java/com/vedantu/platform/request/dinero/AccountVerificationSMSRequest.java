package com.vedantu.platform.request.dinero;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AccountVerificationSMSRequest extends AbstractFrontEndReq{

	private Boolean resend = false;

	public Boolean getResend() {
		return resend;
	}

	public void setResend(Boolean resend) {
		this.resend = resend;
	}

	public AccountVerificationSMSRequest(Boolean resend) {
		super();
		this.resend = resend;
	}

	public AccountVerificationSMSRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

}
