package com.vedantu.platform.request.dinero;

import javax.servlet.http.HttpServletRequest;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetTransactionsReq extends AbstractFrontEndReq {

	private Long userId;
	private String reason;
	private Long start;
	private Long limit;
	private Long fromTime;
	private Long tillTime;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getLimit() {
		return limit;
	}

	public void setLimit(Long limit) {
		this.limit = limit;
	}

	public Long getFromTime() {
		return fromTime;
	}

	public void setFromTime(Long fromTime) {
		this.fromTime = fromTime;
	}

	public Long getTillTime() {
		return tillTime;
	}

	public void setTillTime(Long tillTime) {
		this.tillTime = tillTime;
	}

	public GetTransactionsReq(Long userId, String reason, Long start, Long limit, Long fromTime, Long tillTime) {
		super();
		this.userId = userId;
		this.reason = reason;
		this.start = start;
		this.limit = limit;
		this.fromTime = fromTime;
		this.tillTime = tillTime;
	}

	public GetTransactionsReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetTransactionsReq [userId=" + userId + ", reason=" + reason + ", start=" + start + ", limit=" + limit
				+ ", fromTime=" + fromTime + ", tillTime=" + tillTime + "]";
	}

}
