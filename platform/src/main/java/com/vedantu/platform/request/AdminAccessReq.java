/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.platform.mongodbentities.AdminAccess;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.platform.pojo.AdminAccessFeatureEntity;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AdminAccessReq extends AbstractFrontEndReq {

    private Long adminId;
    private List<AdminAccessFeatureEntity> features;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public List<AdminAccessFeatureEntity> getFeatures() {
        return features;
    }

    public void setFeatures(List<AdminAccessFeatureEntity> features) {
        this.features = features;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (adminId == null) {
            errors.add("adminId");
        }
        return errors;
    }

}
