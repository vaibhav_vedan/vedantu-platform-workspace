package com.vedantu.platform.request;

import com.vedantu.platform.enums.referral.ReferralStep;

public class ProcessReferralBonusReq {

    private ReferralStep referralStep;
    private Long userId;

    public ReferralStep getReferralStep() {
        return referralStep;
    }

    public void setReferralStep(ReferralStep referralStep) {
        this.referralStep = referralStep;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
