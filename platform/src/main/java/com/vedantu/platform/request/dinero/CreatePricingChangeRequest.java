package com.vedantu.platform.request.dinero;

import java.util.List;

import com.vedantu.platform.pojo.dinero.Price;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CreatePricingChangeRequest extends AbstractFrontEndReq {

	private Long teacherId;
	private List<Price> prices;

	public CreatePricingChangeRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CreatePricingChangeRequest(Long teacherId, List<Price> prices) {
		super();
		this.teacherId = teacherId;
		this.prices = prices;
	}

	@Override
	public String toString() {
		return "CreatePricingChangeRequest [teacherId=" + teacherId + ", prices=" + prices + "]";
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public List<Price> getPrices() {
		return prices;
	}

	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == teacherId) {
			errors.add("teacherId");
		}

		if (null == prices || prices.size() == 0) {
			errors.add("prices");
		}

		return errors;
	}

}
