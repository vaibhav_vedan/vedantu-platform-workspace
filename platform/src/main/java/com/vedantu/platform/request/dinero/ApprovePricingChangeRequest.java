package com.vedantu.platform.request.dinero;

import java.util.List;

import com.vedantu.platform.pojo.dinero.ApprovePrice;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ApprovePricingChangeRequest extends AbstractFrontEndReq {

	private Long teacherId;
	private List<ApprovePrice> approvePrices;

	public ApprovePricingChangeRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ApprovePricingChangeRequest(Long teacherId, List<ApprovePrice> approvePrices) {
		super();
		this.teacherId = teacherId;
		this.approvePrices = approvePrices;
	}

	@Override
	public String toString() {
		return "CreatePricingChangeRequest [teacherId=" + teacherId + ", approvePrices=" + approvePrices + "]";
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public List<ApprovePrice> getApprovePrices() {
		return approvePrices;
	}

	public void setApprovePrices(List<ApprovePrice> approvePrices) {
		this.approvePrices = approvePrices;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == teacherId) {
			errors.add("teacherId");
		}
		
		if (null == approvePrices || approvePrices.size() == 0) {
			errors.add("approvePrices");
		}
		return errors;
	}

}
