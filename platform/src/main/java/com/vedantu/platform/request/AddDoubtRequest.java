package com.vedantu.platform.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddDoubtRequest extends AbstractFrontEndReq{

	public Long userId;
	public Integer grade;
	public String boardId;
	public String doubtText;
	public List<String> attachments;
	
	public AddDoubtRequest() {
		super();
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getDoubtText() {
		return doubtText;
	}
	public void setDoubtText(String doubtText) {
		this.doubtText = doubtText;
	}
	public List<String> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

}
