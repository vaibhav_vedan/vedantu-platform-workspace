/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.fos.request.AbstractGetListReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetFileMetadataReq extends AbstractGetListReq {

	private Long sessionId;
	private Long userId;
	private String fileType;
	private Long from;
	private Long till;
	private Boolean orderDesc = Boolean.FALSE;

	public Long getFrom() {

		return from;
	}

	public void setFrom(Long from) {

		this.from = from;
	}

	public Long getTill() {

		return till;
	}

	public void setTill(Long till) {

		this.till = till;
	}

	public Long getSessionId() {

		return sessionId;
	}

	public void setSessionId(Long sessionId) {

		this.sessionId = sessionId;
	}

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public String getFileType() {

		return fileType;
	}

	public void setFileType(String fileType) {

		this.fileType = fileType;
	}

	public Boolean getOrderDesc() {

		return orderDesc;
	}

	public void setOrderDesc(Boolean orderDesc) {

		this.orderDesc = orderDesc;
	}
	
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == sessionId) {
			errors.add("sessionId");
		}
		return errors;
	}
}

