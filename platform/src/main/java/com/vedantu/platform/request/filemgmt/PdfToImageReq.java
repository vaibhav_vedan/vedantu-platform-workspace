/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class PdfToImageReq extends AbstractFrontEndReq {

    private String name;
    // type: mime-type
    private String type;
    // size: bytes
    private Long size;
    private Long userId;
    private UploadType uploadType;
    private FileACL acl;
    private String folder;
    private MultipartFile file;
    private float scale;

    public PdfToImageReq() {
        super();
    }

    public PdfToImageReq(String name, String type, Long size, Long userId, UploadType uploadType, FileACL acl, String folder, MultipartFile file, Float scale) {
        super();
        this.name = name;
        this.type = type;
        this.size = size;
        this.userId = userId;
        this.uploadType = uploadType;
        this.acl = acl;
        this.folder = folder;
        this.file = file;
        if(scale==null){
            this.scale = 3;
        }else{
            this.scale=scale;
        }
    }

    
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public Long getSize() {

        return size;
    }

    public void setSize(Long size) {

        this.size = size;
    }

    public UploadType getUploadType() {

        return uploadType;
    }

    public void setUploadType(UploadType uploadType) {

        this.uploadType = uploadType;
    }

    public FileACL getAcl() {
        return acl;
    }

    public void setAcl(FileACL acl) {
        this.acl = acl;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
    
    /*
	public File toFileUpload() {

		File fileUpload = new File();
		fileUpload.setName(getName());
		fileUpload.setType(getType());
		fileUpload.setSize(getSize());
		fileUpload.setUserId(getUserId());
		fileUpload.setUploadType(getUploadType());
		fileUpload.setAcl(getAcl());

		return fileUpload;
	}
     */
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(name)) {
            errors.add(Constants.NAME);
        }
        if (StringUtils.isEmpty(type)) {
            errors.add(Constants.TYPE);
        }
        if (null == size) {
            errors.add(Constants.SIZE);
        }
        if (null == userId) {
            errors.add(Constants.USER_ID);
        }
        if (null == uploadType) {
            errors.add(Constants.UPLOAD_TYPE);
        }
        
        if (null == file) {
            errors.add(Constants.FILE);
        }
        return errors;
    }

    public static class Constants {

        public static final String NAME = "name";
        public static final String TYPE = "type";
        public static final String STATE = "state";
        public static final String USER_ID = "userId";
        public static final String SIZE = "size";
        public static final String UPLOAD_TYPE = "uploadType";
        public static final String FILE_ACL = "acl";
        public static final String FILE = "file";
        public static final String FOLDER = "folder";
    }

    @Override
    public String toString() {
        return "PdfToImageReq{" + "name=" + name + ", type=" + type + ", size=" + size + ", userId=" + userId + ", uploadType=" + uploadType + ", acl=" + acl + ", folder=" + folder + ", file=" + file + ", scale=" + scale + '}';
    }

    

}
