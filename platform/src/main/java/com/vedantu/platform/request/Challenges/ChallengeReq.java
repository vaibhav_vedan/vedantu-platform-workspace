package com.vedantu.platform.request.Challenges;

import java.util.List;

import com.vedantu.platform.pojo.Challenges.ChallengeLeaderboard;
import com.vedantu.platform.pojo.Challenges.ChallengePOJO;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ChallengeReq extends AbstractFrontEndReq{

	private List<ChallengePOJO> dayChallenges;
	private List<ChallengeLeaderboard> leaderboards;

	public List<ChallengeLeaderboard> getLeaderboards() {
		return leaderboards;
	}

	public void setLeaderboards(List<ChallengeLeaderboard> leaderboards) {
		this.leaderboards = leaderboards;
	}

	public List<ChallengePOJO> getDayChallenges() {
		return dayChallenges;
	}

	public void setDayChallenges(List<ChallengePOJO> dayChallenges) {
		this.dayChallenges = dayChallenges;
	}
	
}
