/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class RemoveTeacherMappingReq extends AbstractFrontEndReq {

	private Long id;

        /*
	public RemoveTeacherMappingReq(HttpServletRequest req) {
		super(req);
	}
        */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (id == null || id == 0) {
			errors.add(TeacherBoardMapping.Constants.ID);
		}
		return errors;
	}

}
