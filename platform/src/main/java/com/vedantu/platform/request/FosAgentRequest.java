package com.vedantu.platform.request;
import com.vedantu.platform.pojo.FOSAgentPOJO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FosAgentRequest extends AbstractFrontEndReq {

    private List<String> agentEmailIds;

    public List<String> getAgentEmailIds() {
        return agentEmailIds;
    }

    public void setAgentEmailIds(List<String> agentEmailIds) {
        this.agentEmailIds = agentEmailIds;
    }


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();

        if(agentEmailIds==null){
            errors.add("There should be at least one fos agent");
        }

        if(ArrayUtils.isNotEmpty(agentEmailIds)){
            agentEmailIds.forEach(agentEmailId->{
                if(StringUtils.isEmpty(agentEmailId)){
                    errors.add("Some of the entries have missing agent email id or agent code");
                }
            });
        }

        return errors;
    }
}
