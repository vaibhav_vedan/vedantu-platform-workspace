package com.vedantu.platform.request.dinero;

import java.util.List;

import com.vedantu.platform.pojo.dinero.Slab;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;

public class CreateSlabRangesRequest extends AbstractMongoEntityBean{
	
	private List<Slab> slabs;

	public CreateSlabRangesRequest(List<Slab> slabs) {
		super();
		this.slabs = slabs;
	}

	public List<Slab> getSlabs() {
		return slabs;
	}

	public void setSlabs(List<Slab> slabs) {
		this.slabs = slabs;
	}

	public CreateSlabRangesRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "CreateSlabRangesRequest [slabs=" + slabs + "]";
	}
	
}
