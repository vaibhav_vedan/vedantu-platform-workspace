/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class FileDownloadUrlReq extends AbstractFrontEndReq {

	private Long id;

	private Integer expirationMinutes;

        public FileDownloadUrlReq(){
            super();
        }
	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public Integer getExpirationMinutes() {

		return expirationMinutes;
	}

	public void setExpirationMinutes(Integer expirationMinutes) {

		this.expirationMinutes = expirationMinutes;
	}
	
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == id) {
			errors.add("id");
		}
		return errors;
	}

	@Override
	public String toString() {

		return "{id=" + id + "}";
	}

}
