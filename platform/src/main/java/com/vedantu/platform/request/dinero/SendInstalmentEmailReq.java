/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.dinero;

import com.vedantu.platform.enums.dinero.InstalmentDueEmailType;
import com.vedantu.session.pojo.EntityType;

/**
 *
 * @author ajith
 */
public class SendInstalmentEmailReq {

    private InstalmentDueEmailType communicationType;
    private Long dueTime;
    private Integer amount;
    private Integer refundAmount;
    private Long userId; //For otf
    private String subscriptionLink; //For otf
    private String subscriptionTitle; //For otf
    private String subscriptionId;
    private Long studentId;
    private Long teacherId;
    private EntityType entityType;

    public SendInstalmentEmailReq() {
    }

    public void setCommunicationType(InstalmentDueEmailType communicationType) {
        this.communicationType = communicationType;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Integer refundAmount) {
        this.refundAmount = refundAmount;
    }

    public InstalmentDueEmailType getCommunicationType() {
        return communicationType;
    }

    public String getSubscriptionLink() {
        return subscriptionLink;
    }

    public void setSubscriptionLink(String subscriptionLink) {
        this.subscriptionLink = subscriptionLink;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    @Override
    public String toString() {
        return "SendInstalmentEmailReq{" + "communicationType=" + communicationType + ", dueTime=" + dueTime + ", amount=" + amount + ", refundAmount=" + refundAmount + ", userId=" + userId + ", subscriptionLink=" + subscriptionLink + ", subscriptionTitle=" + subscriptionTitle + ", subscriptionId=" + subscriptionId + ", studentId=" + studentId + ", teacherId=" + teacherId + ", entityType=" + entityType + '}';
    }

}
