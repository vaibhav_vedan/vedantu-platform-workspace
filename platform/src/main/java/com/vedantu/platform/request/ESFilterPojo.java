package com.vedantu.platform.request;

import lombok.Data;

@Data
public class ESFilterPojo {

    private String key;
    private String value;
}
