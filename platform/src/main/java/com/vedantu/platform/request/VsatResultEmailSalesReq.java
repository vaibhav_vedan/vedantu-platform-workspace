package com.vedantu.platform.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class VsatResultEmailSalesReq extends AbstractFrontEndReq {
    private Integer score;
    private Integer rank;
    private Long duration;
    private String email;
    private String name;

    private String discount;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "VsatResultEmailSalesReq{" +
                "score=" + score +
                ", rank=" + rank +
                ", duration=" + duration +
                ", discount='" + discount + '\'' +
                '}';
    }
}
