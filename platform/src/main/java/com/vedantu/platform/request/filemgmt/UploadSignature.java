/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

/**
 *
 * @author somil
 */
public class UploadSignature {

    public String actionUrl;
    public int success_action_status = 201;

    public String signature;
    public String policy;
    public String key;// file path
    public String contentType;
    public String GoogleAccessId;
    public String acl;

    
    public UploadSignature() {
        
    }
    
    
    public UploadSignature(String actionUrl, String signature, String policy, String GoogleAccessId, String acl) {
        this.actionUrl = actionUrl;
        this.signature = signature;
        this.policy = policy;
        this.GoogleAccessId = GoogleAccessId;
        this.acl = acl;
    }
    
    
    

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public int getSuccess_action_status() {
        return success_action_status;
    }

    public void setSuccess_action_status(int success_action_status) {
        this.success_action_status = success_action_status;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getGoogleAccessId() {
        return GoogleAccessId;
    }

    public void setGoogleAccessId(String GoogleAccessId) {
        this.GoogleAccessId = GoogleAccessId;
    }

    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public String toString() {
        return "UploadSignature{" + "actionUrl=" + actionUrl + ", success_action_status=" + success_action_status + ", signature=" + signature + ", policy=" + policy + ", key=" + key + ", contentType=" + contentType + ", GoogleAccessId=" + GoogleAccessId + ", acl=" + acl + '}';
    }

}
