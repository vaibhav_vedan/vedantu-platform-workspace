package com.vedantu.platform.request;

import com.vedantu.User.request.GetUsersReq;
import com.vedantu.platform.enums.ExportType;

/**
 * Created by somil on 27/01/17.
 */
public class ExportUsersReq extends GetUsersReq {
    ExportType exportType;
}
