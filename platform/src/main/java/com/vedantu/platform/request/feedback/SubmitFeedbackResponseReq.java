/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.feedback;

import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamicSection;
import com.vedantu.platform.pojo.feedback.FeedbackQuestion;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author parashar
 * 
 */
public class SubmitFeedbackResponseReq extends AbstractFrontEndReq{
    
    private Long userId;
    private String feedbackFormId;
    private List<FeedbackQuestion> staticQuestions;
    private List<FeedbackFormDynamicSection> feedbackFormDynamicSections;
    private FeedbackCommunication feedbackCommunication;

    /**
     * @return the feedbackFormId
     */
    public String getFeedbackFormId() {
        return feedbackFormId;
    }

    /**
     * @param feedbackFormId the feedbackFormId to set
     */
    public void setFeedbackFormId(String feedbackFormId) {
        this.feedbackFormId = feedbackFormId;
    }

    /**
     * @return the staticQuestions
     */
    public List<FeedbackQuestion> getStaticQuestions() {
        return staticQuestions;
    }

    /**
     * @param staticQuestions the staticQuestions to set
     */
    public void setStaticQuestions(List<FeedbackQuestion> staticQuestions) {
        this.staticQuestions = staticQuestions;
    }

    /**
     * @return the feedbackFormDynamicSections
     */
    public List<FeedbackFormDynamicSection> getFeedbackFormDynamicSections() {
        return feedbackFormDynamicSections;
    }

    /**
     * @param feedbackFormDynamicSections the feedbackFormDynamicSections to set
     */
    public void setFeedbackFormDynamicSections(List<FeedbackFormDynamicSection> feedbackFormDynamicSections) {
        this.feedbackFormDynamicSections = feedbackFormDynamicSections;
    }
    
    @Override
    protected List<String> collectVerificationErrors(){
        List<String> errors = super.collectVerificationErrors();
                
        if(StringUtils.isEmpty(feedbackFormId)){
            errors.add("feedbackFormId");
        }
        
        if(ArrayUtils.isEmpty(staticQuestions) && ArrayUtils.isEmpty(feedbackFormDynamicSections)){
            errors.add("questions");
        }
        
        if(ArrayUtils.isNotEmpty(staticQuestions)){
            if(!checkQuestions(staticQuestions)){
                errors.add("Mandatory Question not answered");
            }
        }
        
        if(ArrayUtils.isNotEmpty(feedbackFormDynamicSections)){
            boolean noQuestions = true;
            for(FeedbackFormDynamicSection feedbackFormDynamicSection : feedbackFormDynamicSections){
                if(ArrayUtils.isNotEmpty(feedbackFormDynamicSection.getQuestions())){
                    noQuestions = false;
                    if(!checkQuestions(feedbackFormDynamicSection.getQuestions())){
                        errors.add("Mandatory Question not answered");
                    }
                }
            }
            if(noQuestions){
                errors.add("questions");
            }
        }
        
        return errors;
        
    }
    
    public boolean checkQuestions(List<FeedbackQuestion> questions){
        for(FeedbackQuestion feedbackQuestion : questions){
            
            if(feedbackQuestion.isMandatory() && ArrayUtils.isEmpty(feedbackQuestion.getResponses())){
                return false;
            }
            
        }
        return true;
    }

    /**
     * @return the feedbackCommunication
     */
    public FeedbackCommunication getFeedbackCommunication() {
        return feedbackCommunication;
    }

    /**
     * @param feedbackCommunication the feedbackCommunication to set
     */
    public void setFeedbackCommunication(FeedbackCommunication feedbackCommunication) {
        this.feedbackCommunication = feedbackCommunication;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
}
