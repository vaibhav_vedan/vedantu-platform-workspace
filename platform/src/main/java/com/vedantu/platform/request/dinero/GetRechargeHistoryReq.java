package com.vedantu.platform.request.dinero;

import com.vedantu.util.fos.request.AbstractGetListReq;
import java.util.List;

public class GetRechargeHistoryReq extends AbstractGetListReq {

    @Override
    public String toString() {
        return "GetRechargeHistoryReq [userId=" + userId + "]";
    }

    private Long userId;

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public GetRechargeHistoryReq(Long userId) {
        super();
        this.userId = userId;
    }

    public GetRechargeHistoryReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }

}
