package com.vedantu.platform.request.dinero;

public class ReleaseLockedBalanceReq extends GetAccountInfoReq {

	@Override
	public String toString() {
		return "ReleaseLockedBalanceReq [amount=" + amount + "]";
	}

	public ReleaseLockedBalanceReq(Long userId, int amount) {
		super(userId);
		this.amount = amount;
	}

	public ReleaseLockedBalanceReq(Long userId) {
		super(userId);
		// TODO Auto-generated constructor stub
	}

	private int amount;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public ReleaseLockedBalanceReq() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
