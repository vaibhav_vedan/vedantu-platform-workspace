package com.vedantu.platform.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;

import java.util.List;

public class SendSeoPdfBySMSReq  extends AbstractReq {
    private Long userId;
    private String contentId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(contentId)) {
            errors.add("contentId");
        }
        return errors;
    }
}