/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class SendSeoPdfByEmailReq extends AbstractReq{
    private Long userId;
    private String contentId;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(contentId) || "null".equalsIgnoreCase(contentId)) {
            errors.add("contentId");
        }
        return errors;
    }
}
