/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;


/**
 *
 * @author jeet
 */
public class WhiteboardPdfInitReq extends AbstractFrontEndReq{
    private String name;
    private String type;
    private Long size;
    private UploadType uploadType;
    private FileACL acl;
    private String folder;
    private float scale;
    
    public WhiteboardPdfInitReq() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public UploadType getUploadType() {
        return uploadType;
    }

    public void setUploadType(UploadType uploadType) {
        this.uploadType = uploadType;
    }

    public FileACL getAcl() {
        return acl;
    }

    public void setAcl(FileACL acl) {
        this.acl = acl;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(name)) {
            errors.add(Constants.NAME);
        }
        if (StringUtils.isEmpty(type)) {
            errors.add(Constants.TYPE);
        }
        if (null == size) {
            errors.add(Constants.SIZE);
        }
        if (null == uploadType) {
            errors.add(Constants.UPLOAD_TYPE);
        }
        return errors;
    }
    
    public static class Constants {

        public static final String NAME = "name";
        public static final String TYPE = "type";
        public static final String STATE = "state";
        public static final String USER_ID = "userId";
        public static final String SIZE = "size";
        public static final String UPLOAD_TYPE = "uploadType";
        public static final String FILE_ACL = "acl";
        public static final String FOLDER = "folder";
    }
    
    @Override
    public String toString() {
        return "WhiteboardPdfInitReq{" + "name=" + name + ", type=" + type + ", size=" + size + ", uploadType=" + uploadType + ", acl=" + acl + ", folder=" + folder +", scale=" + scale + '}';
    }
    
    
}
