/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.social;

import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import javax.validation.Valid;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class AddCommentReq extends AbstractFrontEndReq {

    private SocialContextType socialContextType;
    private String contextId;
    private String parentId; //parentId of Socializer for nlevel commenting
    private String rootCommentId;
    @Valid
    private RichTextFormat comment;//will be more applicable for COMMENT,REVIEW,RATING    

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public RichTextFormat getComment() {
        return comment;
    }

    public void setComment(RichTextFormat comment) {
        this.comment = comment;
    }

    public String getRootCommentId() {
        return rootCommentId;
    }

    public void setRootCommentId(String rootCommentId) {
        this.rootCommentId = rootCommentId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (parentId == null) {
            if (socialContextType == null) {
                errors.add("socialContextType");
            }
            if (StringUtils.isEmpty(contextId)) {
                errors.add("contextId");
            }
        }

        if (comment==null||StringUtils.isEmpty(comment.getNewText())) {
            errors.add("comment");
        }
        return errors;
    }

}
