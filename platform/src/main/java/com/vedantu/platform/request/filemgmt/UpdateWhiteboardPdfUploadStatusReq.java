/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.platform.response.filemgmt.PdfToImageRes;
import com.vedantu.util.enums.UploadState;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class UpdateWhiteboardPdfUploadStatusReq extends AbstractReq{
    
    private String id;
    private UploadState state;
    private List<PdfToImageRes> images = new ArrayList<>();
    private Long totalPageCount;
    private Long convertedPageCount;

    public UpdateWhiteboardPdfUploadStatusReq() {
        super();
    }
    
    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == id) {
            errors.add(UpdateWhiteboardPdfUploadStatusReq.Constants.FILE_ID);
        }

        if (null == state) {
            errors.add(UpdateWhiteboardPdfUploadStatusReq.Constants.STATE);
        }
        return errors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UploadState getState() {
        return state;
    }

    public void setState(UploadState state) {
        this.state = state;
    }

    public List<PdfToImageRes> getImages() {
        return images;
    }

    public void setImages(List<PdfToImageRes> images) {
        this.images = images;
    }

    public Long getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(Long totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public Long getConvertedPageCount() {
        return convertedPageCount;
    }

    public void setConvertedPageCount(Long convertedPageCount) {
        this.convertedPageCount = convertedPageCount;
    }
    
    public static class Constants {
        public static final String STATE = "state";
        public static final String FILE_ID = "fileId";
    }

    @Override
    public String toString() {
        return "UpdateWhiteboardPdfUploadStatusReq{" + "id=" + id + ", state=" + state + ", images=" + images + ", totalPageCount=" + totalPageCount + ", convertedPageCount=" + convertedPageCount + '}';
    }
    
    
    
    
}
