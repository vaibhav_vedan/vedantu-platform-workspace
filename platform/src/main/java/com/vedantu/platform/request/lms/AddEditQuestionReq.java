package com.vedantu.platform.request.lms;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Set;

public class AddEditQuestionReq extends AbstractAddCMDSEntityReq {

    private String questionId;
    private RichTextFormat questionBody;
    private QuestionType questionType;
    private List<RichTextFormat> options;
    private List<SolutionFormat> solutions;
    private List<String> answers;

    //book related
    private String book;
    private String edition;
    private String chapter;
    private String chapterNo;
    private String exercise;
    private Set<String> pageNos;
    private int slNoInBook;
    private String questionNo;
    private Difficulty difficulty;

    public AddEditQuestionReq() {
        super();
    }

    public RichTextFormat getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(RichTextFormat questionBody) {
        this.questionBody = questionBody;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<RichTextFormat> getOptions() {
        return options;
    }

    public void setOptions(List<RichTextFormat> options) {
        this.options = options;
    }

    public List<SolutionFormat> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<SolutionFormat> solutions) {
        this.solutions = solutions;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public Set<String> getPageNos() {
        return pageNos;
    }

    public void setPageNos(Set<String> pageNos) {
        this.pageNos = pageNos;
    }

    public int getSlNoInBook() {
        return slNoInBook;
    }

    public void setSlNoInBook(int slNoInBook) {
        this.slNoInBook = slNoInBook;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public String getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(String chapterNo) {
        this.chapterNo = chapterNo;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (questionBody == null) {
            errors.add("questionBody");
        } else if (StringUtils.isEmpty(questionBody.getNewText())) {
            errors.add("questionBody newText");
        }

        if (ArrayUtils.isNotEmpty(solutions)) {
            for (RichTextFormat solution : solutions) {
                if (StringUtils.isEmpty(solution.getNewText())) {
                    errors.add("invalid solution");
                }
            }
        }
        if (ArrayUtils.isNotEmpty(options)) {
            for (RichTextFormat option : options) {
                if (StringUtils.isEmpty(option.getNewText())) {
                    errors.add("invalid option");
                }
            }
        }
        return errors;
    }
}
