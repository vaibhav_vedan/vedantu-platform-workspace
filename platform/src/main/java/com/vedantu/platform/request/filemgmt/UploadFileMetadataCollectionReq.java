/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.User.AbstractEntityRes;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class UploadFileMetadataCollectionReq extends AbstractFrontEndReq {
	private List<FileMetadataSetter> collection;

	public List<FileMetadataSetter> getCollection() {
		return collection;
	}

	public void setCollection(
			List<FileMetadataSetter> collection) {
		this.collection = collection;
	}

        @Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (CollectionUtils.isEmpty(collection)) {
			errors.add(Constants.COLLECTION);
		}
		return errors;
	}
	
	public static class Constants extends AbstractEntityRes.Constants {

		public static final String COLLECTION = "collection";
	}

}

