/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.User.User;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetTeacherBoardMappingsReq extends AbstractFrontEndReq {

	private Long userId;

	//public GetTeacherBoardMappingsReq(HttpServletRequest req) {
	//	super(req);
	//}
        
        public GetTeacherBoardMappingsReq(Long userId) {
            this.userId = userId;
        }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if (userId == null || userId == 0) {
			errors.add(User.Constants.USER_ID);
		}
		return errors;
	}

}

