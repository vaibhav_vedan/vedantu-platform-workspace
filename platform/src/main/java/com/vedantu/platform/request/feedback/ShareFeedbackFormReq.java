/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.feedback;

import com.vedantu.platform.enums.feedback.RuleSetType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author parashar
 */
public class ShareFeedbackFormReq extends AbstractFrontEndReq{
    
    private String parentFormId;
    private RuleSetType ruleSetType;
    private Integer ruleSetValue;
    private Long expiresIn; //timestamp
    private Long validTill;
    private String contextId;

    /**
     * @return the parentFormId
     */
    public String getParentFormId() {
        return parentFormId;
    }

    /**
     * @param parentFormId the parentFormId to set
     */
    public void setParentFormId(String parentFormId) {
        this.parentFormId = parentFormId;
    }

    /**
     * @return the ruleSetType
     */
    public RuleSetType getRuleSetType() {
        return ruleSetType;
    }

    /**
     * @param ruleSetType the ruleSetType to set
     */
    public void setRuleSetType(RuleSetType ruleSetType) {
        this.ruleSetType = ruleSetType;
    }

    /**
     * @return the ruleSetValue
     */
    public Integer getRuleSetValue() {
        return ruleSetValue;
    }

    /**
     * @param ruleSetValue the ruleSetValue to set
     */
    public void setRuleSetValue(Integer ruleSetValue) {
        this.ruleSetValue = ruleSetValue;
    }

    /**
     * @return the expiresIn
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * @param expiresIn the expiresIn to set
     */
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * @return the validTill
     */
    public Long getValidTill() {
        return validTill;
    }

    /**
     * @param validTill the validTill to set
     */
    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }
   
    @Override
    protected List<String> collectVerificationErrors(){
        List<String> errors = super.collectVerificationErrors();
        
        if(StringUtils.isEmpty(contextId)){
            errors.add("contextId");
        }
        
        if(StringUtils.isEmpty(parentFormId)){
            errors.add("parentFormId");
        }
        
        if(ruleSetType == null){
            errors.add("ruleSetType");
        }
        
        if(ruleSetValue == null){
            errors.add("ruleSetValue");
        }
        
        if(validTill == null){
            errors.add("validTill");
        }
        
        if(expiresIn == null){
            errors.add("expiresIn");
        }
        
        return errors;
        
    }   
    
}
