package com.vedantu.platform.request.dinero;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetAccountInfoReq extends AbstractFrontEndReq {

	private Long userId;

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public GetAccountInfoReq(Long userId) {
		super();
		this.userId = userId;
	}

	public GetAccountInfoReq() {
		super();
		// TODO Auto-generated constructor stub
	}

}
