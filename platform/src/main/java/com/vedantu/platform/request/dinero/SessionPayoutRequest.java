package com.vedantu.platform.request.dinero;

import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SessionPayoutRequest extends AbstractFrontEndReq {

    private Long sessionId;
    private Long sessionDuration;
    private Long billingDuration;
    private Long subscriptionId;
    private Long hourlyRate;
    private SessionModel model;
    private Long date;
    private Long teacherId;
    private Long studentId;
    private Long consumedHours;
    private Long totalHours;
    private LiveSessionPlatformType liveSessionPlatformType;

    public void setBillingDuration(Long billingDuration) {
        this.billingDuration = billingDuration;
    }
    
    public Long getConsumedHours() {
        return consumedHours;
    }

    public void setConsumedHours(Long consumedHours) {
        this.consumedHours = consumedHours;
    }

    public Long getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Long totalHours) {
        this.totalHours = totalHours;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(Long sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public Long getBillingDuration() {
        return billingDuration;
    }

    public LiveSessionPlatformType getLiveSessionPlatformType() {
        return liveSessionPlatformType;
    }

    public void setLiveSessionPlatformType(LiveSessionPlatformType liveSessionPlatformType) {
        this.liveSessionPlatformType = liveSessionPlatformType;
    }
    
    

}
