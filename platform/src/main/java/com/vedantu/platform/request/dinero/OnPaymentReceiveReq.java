package com.vedantu.platform.request.dinero;

import java.util.Map;


import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class OnPaymentReceiveReq extends AbstractFrontEndReq {

	private Map<String, Object> transactionInfo;
	private String action;

	public Map<String, Object> getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(Map<String, Object> transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public OnPaymentReceiveReq() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public OnPaymentReceiveReq(Map<String, Object> transactionInfo, String action) {
		super();
		this.transactionInfo = transactionInfo;
		this.action = action;
	}

}
