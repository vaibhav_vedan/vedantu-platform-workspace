/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.cms;

import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.fos.request.AbstractReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class AddQuestionToRegistrationReq extends AbstractReq{
    private String webinarId;
    private String question;
    private String email;
    private RequestSource source = RequestSource.WEB;

    public AddQuestionToRegistrationReq() {
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(webinarId)) {
            errors.add("webinarId");
        }
        
        if (StringUtils.isEmpty(question)) {
            errors.add("question");
        }

        if (StringUtils.isEmpty(email)) {
            errors.add("email");
        }

        return errors;
    }

	public RequestSource getSource() {
		return source;
	}

	public void setSource(RequestSource source) {
		this.source = source;
	}
}
