/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.feedback;

import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.enums.feedback.FeedbackFormType;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamic;
import com.vedantu.platform.pojo.feedback.FeedbackQuestion;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author parashar
 */
public class CreateFeedbackFormReq extends AbstractFrontEndReq {

    
    private String formTitle; 
    private List<FeedbackQuestion> staticFeedbackQuestions;
    private FeedbackFormType feedbackFormType;
    private EntityType contextType;
    private FeedbackCommunication communication = FeedbackCommunication.BOTH;
    private FeedbackFormDynamic dynamicSection;

    /**
     * @return the formTitle
     */
    public String getFormTitle() {
        return formTitle;
    }

    /**
     * @param formTitle the formTitle to set
     */
    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    /**
     * @return the staticFeedbackQuestions
     */
    public List<FeedbackQuestion> getStaticFeedbackQuestions() {
        return staticFeedbackQuestions;
    }

    /**
     * @param staticFeedbackQuestions the staticFeedbackQuestions to set
     */
    public void setStaticFeedbackQuestions(List<FeedbackQuestion> staticFeedbackQuestions) {
        this.staticFeedbackQuestions = staticFeedbackQuestions;
    }

    /**
     * @return the feedbackFormType
     */
    public FeedbackFormType getFeedbackFormType() {
        return feedbackFormType;
    }

    /**
     * @param feedbackFormType the feedbackFormType to set
     */
    public void setFeedbackFormType(FeedbackFormType feedbackFormType) {
        this.feedbackFormType = feedbackFormType;
    }

    /**
     * @return the contextType
     */
    public EntityType getContextType() {
        return contextType;
    }

    /**
     * @param contextType the contextType to set
     */
    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    /**
     * @return the communication
     */
    public FeedbackCommunication getCommunication() {
        return communication;
    }

    /**
     * @param communication the communication to set
     */
    public void setCommunication(FeedbackCommunication communication) {
        this.communication = communication;
    }

    /**
     * @return the dynamicSection
     */
    public FeedbackFormDynamic getDynamicSection() {
        return dynamicSection;
    }

    /**
     * @param dynamicSection the dynamicSection to set
     */
    public void setDynamicSection(FeedbackFormDynamic dynamicSection) {
        this.dynamicSection = dynamicSection;
    }
    
    @Override
    protected List<String> collectVerificationErrors(){
        List<String> errors = super.collectVerificationErrors();
        
        /*if(super.getCallingUserId() == null){
            errors.add("calling user id");
        }*/
        
        if(ArrayUtils.isEmpty(staticFeedbackQuestions) && ((dynamicSection == null) || (dynamicSection!=null && ArrayUtils.isEmpty(dynamicSection.getQuestions())))){
           errors.add("no questions");
        }
        
        if(ArrayUtils.isNotEmpty(staticFeedbackQuestions)){
            for(FeedbackQuestion feedbackQuestion : staticFeedbackQuestions){
                if(StringUtils.isEmpty(feedbackQuestion.getQuestionText())){
                    errors.add("Empty Question not allowed");
                    break;
                }
            }
        }
        
        if(dynamicSection!=null && ArrayUtils.isNotEmpty(dynamicSection.getQuestions())){
            for(FeedbackQuestion feedbackQuestion : dynamicSection.getQuestions()){
                if(StringUtils.isEmpty(feedbackQuestion.getQuestionText())){
                    errors.add("Empty Question not allowed");
                    break;
                }                
            }
        }
        
        if(StringUtils.isEmpty(formTitle)){
            errors.add("Empty form title");
        }
        
        if(feedbackFormType == null){
            errors.add("feedbackFormType");
        }
        
        if(contextType == null){
            errors.add("contextType");
        }
        
       return errors; 
    }
    
}
