package com.vedantu.platform.request;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.vedantu.platform.enums.MasterTalkType;
import lombok.Data;

@Data
public class MasterTalkQuestionReq {

    private MasterTalkType masterTalkType;
    private Boolean isTestAttempted ;
    private String question;

}
