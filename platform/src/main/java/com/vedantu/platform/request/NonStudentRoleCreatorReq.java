package com.vedantu.platform.request;

import com.vedantu.User.Role;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NonStudentRoleCreatorReq extends AbstractFrontEndReq {
    private Long creatorId;
    private List<Role> allowedRoles = new ArrayList<>();

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(creatorId == null){
            errors.add("creatorId is mandatory");
        }
        return errors;
    }
}
