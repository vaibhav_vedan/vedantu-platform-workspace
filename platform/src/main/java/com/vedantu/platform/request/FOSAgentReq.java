package com.vedantu.platform.request;

import com.vedantu.platform.pojo.FOSAgentPOJO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FOSAgentReq extends AbstractFrontEndReq {

    private List<FOSAgentPOJO> fosAgents;

    public List<FOSAgentPOJO> getFosAgents() {
        return fosAgents;
    }

    public void setFosAgents(List<FOSAgentPOJO> fosAgents) {
        this.fosAgents = fosAgents;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();

        if(fosAgents==null){
            errors.add("There should be at least one fos agent");
        }

        if(ArrayUtils.isNotEmpty(fosAgents)){
            Set<String> ids=new HashSet<>();
            fosAgents.forEach(fosAgent->{
                if(fosAgent.getAgentCode()==null || fosAgent.getAgentEmailId()==null){
                    errors.add("Some of the entries have missing agent email id or agent code");
                }
                ids.add(fosAgent.getAgentCode());
            });
            if(ids.size()!=fosAgents.size()){
                errors.add("Duplicate ids present in the csv");
            }
        }

        return errors;
    }
}
