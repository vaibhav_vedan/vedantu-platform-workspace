/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class UpdateFileMetadataReq extends AbstractFrontEndReq {
        //TODO: Front end needs to handle String instead of Long
	private String fileMetadataId;
	private Long fileId;

	public String getFileMetadataId() {
		return fileMetadataId;
	}

	public void setFileMetadataId(String fileMetadataId) {
		this.fileMetadataId = fileMetadataId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	
        @Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == fileMetadataId) {
			errors.add("fileMetadataId");
		}
		
		if (null == fileId) {
			errors.add("fileId");
		}
		return errors;
	}

	
}

