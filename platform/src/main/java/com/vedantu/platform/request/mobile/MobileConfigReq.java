package com.vedantu.platform.request.mobile;

import com.vedantu.platform.enums.mobile.MobilePlatform;
import com.vedantu.util.fos.request.AbstractFrontEndReq;



public class MobileConfigReq extends AbstractFrontEndReq {

	private String userId;
	private String key;
	private String value;
	private String deviceId;
	private String versionCode;
	private MobilePlatform platform;
	private String platformVersion;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getPlatformVersion() {
		return platformVersion;
	}

	public void setPlatformVersion(String platformVersion) {
		this.platformVersion = platformVersion;
	}

	public MobileConfigReq() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public MobilePlatform getPlatform() {
		return platform;
	}

	public void setPlatform(MobilePlatform platform) {
		this.platform = platform;
	}

	public MobileConfigReq(String userId, String deviceId, String key, String value, String versionCode,
			MobilePlatform platform, String platformVersion) {
		super();
		this.userId = userId;
		this.key = key;
		this.value = value;
		this.deviceId = deviceId;
		this.versionCode = versionCode;
		this.platform = platform;
		this.platformVersion = platformVersion;
	}

}
