/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.social;

import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class GetCommentsReq extends AbstractFrontEndListReq {

    private String parentId;
    private SocialContextType socialContextType;
    private String contextId;
    private Long userId;
    private String sortOrder;
    private String orderBy;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
    
    

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (parentId == null) {
            if (socialContextType == null) {
                errors.add("socialContextType");
            }
            if (StringUtils.isEmpty(contextId)) {
                errors.add("contextId");
            }
        }
        return errors;
    }
}
