/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.util.fos.request.AbstractReq;
import com.vedantu.util.pojo.CleverTapEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class CleverTapEventReq extends AbstractReq{
    private List<CleverTapEvent> d = new ArrayList<>();

    public CleverTapEventReq(CleverTapEvent event){
        d.add(event);
    }
    
    public void addEvent(CleverTapEvent event) {
        d.add(event);
    }
    
}
