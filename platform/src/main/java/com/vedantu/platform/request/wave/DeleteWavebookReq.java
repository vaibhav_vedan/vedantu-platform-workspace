package com.vedantu.platform.request.wave;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class DeleteWavebookReq extends AbstractFrontEndReq {
    
    private String wavebookId;

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == wavebookId) {
            errors.add("wavebookId");
        }
        
        return errors;
    }

    public String getWavebookId() {
        return wavebookId;
    }

    public void setWavebookId(String wavebookId) {
        this.wavebookId = wavebookId;
    }
    
}