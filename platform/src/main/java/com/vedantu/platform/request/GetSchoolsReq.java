/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author jeet
 */
public class GetSchoolsReq extends AbstractFrontEndListReq {
    
    private String pinCode;
    private String board;
    private String state;
    private String country;
    private String nameQuery;
    private String addressQuery;

    public GetSchoolsReq() {
        super();
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNameQuery() {
        return nameQuery;
    }

    public void setNameQuery(String nameQuery) {
        this.nameQuery = nameQuery;
    }

    public String getAddressQuery() {
        return addressQuery;
    }

    public void setAddressQuery(String addressQuery) {
        this.addressQuery = addressQuery;
    }

    @Override
    public String toString() {
        return "GetSchoolsReq{" + "pinCode=" + pinCode + ", board=" + board + ", state=" + state + ", country=" + country + ", nameQuery=" + nameQuery + ", addressQuery=" + addressQuery + '}';
    }
    
}
