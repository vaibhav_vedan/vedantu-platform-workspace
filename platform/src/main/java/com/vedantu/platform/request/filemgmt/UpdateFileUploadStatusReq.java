/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;

import com.vedantu.util.enums.UploadState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class UpdateFileUploadStatusReq extends AbstractFrontEndReq {

    private Long id;
    private UploadState state;

    public UpdateFileUploadStatusReq() {
        super();
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public UploadState getState() {

        return state;
    }

    public void setState(UploadState state) {

        this.state = state;
    }

    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == id) {
            errors.add(Constants.FILE_ID);
        }

        if (null == state) {
            errors.add(Constants.STATE);
        }
        return errors;
    }

    public static class Constants {

//        public static final String PATH = "path";
//		public static final String BUCKET = "bucket";
//		public static final String NAME = "name";
//		public static final String TYPE = "type";
        public static final String STATE = "state";
        //public static final String USER_ID = "userId";
        public static final String FILE_ID = "fileId";
        //public static final String SIZE = "size";
        //public static final String UPLOAD_TYPE = "uploadType";
        //public static final String FILE_ACL = "acl";
    }

}
