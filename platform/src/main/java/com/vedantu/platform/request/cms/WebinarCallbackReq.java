package com.vedantu.platform.request.cms;

import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.fos.request.AbstractReq;

import java.util.List;

public class WebinarCallbackReq extends AbstractReq {

    private String webinarId;
    private Long time;
    private RequestSource source = RequestSource.WEB;

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public RequestSource getSource() {
        return source;
    }

    public void setSource(RequestSource source) {
        this.source = source;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(webinarId)) {
            errors.add("webinarId");
        }

        if (time == null) {
            errors.add("time");
        }
        return errors;
    }

}
