package com.vedantu.platform.request.usermessage;

import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author jeet
 */
public class SendQueryReq extends AbstractFrontEndReq {

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String name;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String emailId;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String mobileNumber;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String subject;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String message;
    private CommunicationType emailType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public CommunicationType getEmailType() {
        return emailType;
    }

    public void setEmailType(CommunicationType emailType) {
        this.emailType = emailType;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == name) {
            errors.add(Constants.NAME);
        }
        if (null == emailId) {
            errors.add(Constants.EMAIL);
        }
        if (null == mobileNumber) {
            errors.add(Constants.MOBILE_NUMBER);
        }
        if (null == subject) {
            errors.add(Constants.SUBJECT);
        }
        if (null == emailType) {
            errors.add(Constants.EMAIL_TYPE);
        }
        return errors;
    }

    public static class Constants {

        public static final String NAME = "name";
        public static final String EMAIL = "emailId";
        public static final String MOBILE_NUMBER = "mobileNumber";
        public static final String SUBJECT = "subject";
        public static final String MESSAGE = "message";
        public static final String EMAIL_TYPE = "emailType";
    }

}
