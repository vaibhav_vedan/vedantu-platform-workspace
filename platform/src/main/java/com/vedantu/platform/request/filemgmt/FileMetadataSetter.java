/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.request.filemgmt;


/**
 *
 * @author somil
 */
public class FileMetadataSetter {
	private Long sessionId;
	private String localObjectId;
	private String fileData;
	private Long fileId;//starttime of file in case of aws s3
	private String fileType;
	private Long userId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public void setLocalObjectId(String localObjectId) {
		this.localObjectId = localObjectId;
	}
	
	public String getLocalObjectId() {
		return localObjectId;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
        /*
	public FileMetadata toFileUploadMetadata() {

		FileMetadata fileMetadata = new FileMetadata();
		fileMetadata.setFileId(getFileId());
		fileMetadata.setFileType(getFileType());
		fileMetadata.setCreationTime(Calendar.getInstance().getTimeInMillis());
		fileMetadata.setSessionId(getSessionId());
		fileMetadata.setFileData(getFileData());
		fileMetadata.setUserId(getUserId());
		fileMetadata.setLocalObjectId(getLocalObjectId());
		return fileMetadata;
	}
        */

	@Override
	public String toString() {
		return "FileMetaDataSetter {sessionId:" + sessionId + ", localObjectId:"
				+ localObjectId + ", fileData:" + fileData + ", fileId:" + fileId
				+ ", fileType:" + fileType + "}";
	}

}

