/**
 * 
 */
package com.vedantu.platform.background.events.pojo;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WebinarDetails {
	
	private Boolean isAttended;
	
	private String webinarId;
	
	private String firstImpressionTimestamp;
	
	private String campaignSource;
	
	private String deviceId;

}
