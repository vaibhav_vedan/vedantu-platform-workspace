/**
 * 
 */
package com.vedantu.platform.background.events.request;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BackgroundEventsReq {
	
	private String parentEvent;
	
	//clientId: 'cid'
	@JsonProperty("cid")
	private String clientId;
	
	//sessionId: 'sid'
	@JsonProperty("sid")
	private String sessionId;
	
	//userId: 'uid'
	@JsonProperty("uid")
	private String userId;
	
	//email: 'em'
	@JsonProperty("em")
	private String email;
	
	//role: 'rl'
	@JsonProperty("rl")
	private String role;
	
	//grade: 'gd'
	@JsonProperty("gd")
	private String grade;
	
	//target: 'tr'
	@JsonProperty("tr")
	private String target;
	
	//board: 'bd',
	@JsonProperty("bd")
	private String board;
	
	//deviceType: 'dvct'
	@JsonProperty("dvct")
	private String deviceType;
	
	//referrer: 'dr',
	@JsonProperty("dr")
	private String referrer;
	
	//utm_source: 'usrc',
	@JsonProperty("usrc")
	private String utmSource;
	
	//utm_medium: 'umed',
	@JsonProperty("umed")
	private String utmMedium;
	
	//utm_campaign: 'ucam',
	@JsonProperty("ucam")
	private String utmCampaign;
	
	//utm_term: 'utrm',
	@JsonProperty("utrm")
	private String utmTerm;
	
	//utm_content: 'ucnt',
	@JsonProperty("ucnt")
	private String utmContent;
	
	//channel: 'chl',
	@JsonProperty("chl")
	private String channel;
	
	//type: 't',
	@JsonProperty("t")
	private String type;
	
	//ipAddress: 'uip'
	@JsonProperty("uip")
	private String ipAddress;
	
	//appName: 'an',
	@JsonProperty("an")
	private String appName;
	
	//appTrackingId: 'aid',
	@JsonProperty("aid")
	private String appTrackingId;
	
	//timestamp: 'utt',
	@JsonProperty("utt")
	private String timestamp;
	
	//screenResolution: 'sr',
	@JsonProperty("sr")
	private String screenResolution;
	
	//viewportSize: 'vp',
	@JsonProperty("vp")
	private String viewportSize;
	
	//userAgent: 'ua',
	@JsonProperty("ua")
	private String userAgent;
	
	//eventCategory: 'ec',
	@JsonProperty("ec")
	private String eventCategory;
	
	//eventAction: 'ea'
	@JsonProperty("ea")
	private String eventAction;
	
	//eventLabel: 'el'
	@JsonProperty("el")
	private String eventLabel;
	
	//eventValue: 'ev'
	@JsonProperty("ev")
	private String eventValue;
	
	private Map<String, String> keyNvaluePairs;
	
	//loggedIn: 'li',
	@JsonProperty("li")
	private String loggedIn;
	
	//osname: 'on'
	@JsonProperty("on")
	private String osName;
	
	//osversion: 'ov'
	@JsonProperty("ov")
	private String osVersion;
	
	//deviceuniqueid: 'duid',
	@JsonProperty("duid")
	private String deviceUniqueId;
	
	//devicebrand: 'db',
	@JsonProperty("db")
	private String deviceBrand;
	
	//devicemanufacturer: 'dmr',
	@JsonProperty("dmr")
	private String deviceManufacturer;
	
	//devicemodel: 'dm',
	@JsonProperty("dm")
	private String deviceModel;
	
	//serialnumber: 'dsn',
	@JsonProperty("dsn")
	private String serialNumber;
	
	//buildnumber: 'bn',
	@JsonProperty("bn")
	private String buildNumber;
	
	//carrier: 'dc',
	@JsonProperty("dc")
	private String carrier;
	
	//batterylevel: 'bl',
	@JsonProperty("bl")
	private String batteryLevel;
	
	//firstinstalltime: 'fit',
	@JsonProperty("fit")
	private String firstInstallTime;
	
	//isemulator: 'dem',
	@JsonProperty("dem")
	private String isEmulator;
	
	//istablet: 'dta',
	@JsonProperty("dta")
	private String isTablet;
	
	//islandscape: 'dla',
	@JsonProperty("dla")
	private String isLandscape;
	
	//latitude: 'lat',
	@JsonProperty("lat")
	private String latitude;
	
	//longitude: 'long'
	@JsonProperty("long")
	private String longitude;
	
	//gaid: 'gaid'
	@JsonProperty("gaid")
	private String gaid;
	
	//gpscity: 'gpscity'
	@JsonProperty("gpscity")
	private String gpscity;
	
	//gpslat: 'gpslat'
	@JsonProperty("gpslat")
	private String gpslat;
	
	//gpslong: 'gpslong'
	@JsonProperty("gpslong")
	private String gpslong;
	
	
	

}
