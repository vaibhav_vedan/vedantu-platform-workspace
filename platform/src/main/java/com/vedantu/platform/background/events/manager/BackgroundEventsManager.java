/**
 * 
 */
package com.vedantu.platform.background.events.manager;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.background.events.entity.UserCoreEventsMapping;
import com.vedantu.platform.background.events.enums.EventsEnum;
import com.vedantu.platform.background.events.pojo.BranchCustomDataPojo;
import com.vedantu.platform.background.events.pojo.BranchEventDataPojo;
import com.vedantu.platform.background.events.pojo.BranchUserDataPojo;
import com.vedantu.platform.background.events.pojo.WebinarDetails;
import com.vedantu.platform.background.events.repository.BackgroundEventsDao;
import com.vedantu.platform.background.events.request.BackgroundEventsReq;
import com.vedantu.platform.background.events.request.BranchEventRequest;
import com.vedantu.platform.background.events.response.BackgroundEventsResponse;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.aws.AwslambdaManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * @author subarna
 *
 */

@Service
public class BackgroundEventsManager {

	private static final String BRANCH_KEY = ConfigUtils.INSTANCE.getStringValue("branch.io.auth.key");
	
	private static final Long TIMESTAMP_OF_DEPLOYMENT = 1600194600000L; // date of code deployment  Wednesday, 16 September 2020 00:00:00 GMT+05:30
	
	private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
	
	private static final String IS_USER_ACTIVE = "ISUSERACTIVE";

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(BackgroundEventsManager.class);

	@Autowired
	private BackgroundEventsDao backgroundEventsDao;

	@Autowired
	HttpSessionUtils sessionUtils;
	
	@Autowired
	private FosUtils fosUtils;
	
	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	private AwslambdaManager awsLambdaManager;


	public PlatformBasicResponse sendCoreBranchEvent(BackgroundEventsReq backgroundEventsReq) throws VException{
		
		logger.debug("backgroundEventsReq - > "+backgroundEventsReq);
		PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
		BranchEventRequest branchEventReq = new BranchEventRequest();

		logger.debug("sendCoreBranchEvent api has been called");
		if(null != backgroundEventsReq) {
			branchEventReq = createBranchRequest(backgroundEventsReq);
			logger.debug("sendCoreBranchEvent api has been called -> invoking lambda next------>");
			awsLambdaManager.invokeLambdaToTriggerBranchEvents(branchEventReq);

		}
		if(null != backgroundEventsReq.getParentEvent() && StringUtils.isNotEmpty(backgroundEventsReq.getParentEvent())) {

			UserCoreEventsMapping userCoreEventsMapping = new UserCoreEventsMapping();
			userCoreEventsMapping.setGaid(backgroundEventsReq.getGaid());
			EventsEnum type = EventsEnum.valueOf(backgroundEventsReq.getParentEvent().toUpperCase().trim());
			
			switch(type) {
			case WEBINAR:{
				WebinarDetails webinarDetails = new WebinarDetails(true, backgroundEventsReq.getKeyNvaluePairs().get("webinarId"), backgroundEventsReq.getTimestamp(),backgroundEventsReq.getUtmSource(), backgroundEventsReq.getAppTrackingId());
				userCoreEventsMapping.setWebinarDetails(webinarDetails);
				platformBasicResponse = saveUserCoreEvent(userCoreEventsMapping);
				break;
			}
			case TRIAL:{
				break;
			}
			}
		}

		return platformBasicResponse;
	}

	private BranchEventRequest createBranchRequest(BackgroundEventsReq backgroundEventsReq) {

		BranchEventRequest branchEventReq = new BranchEventRequest();
		branchEventReq.setBranch_key(BRANCH_KEY);
		branchEventReq.setName(backgroundEventsReq.getEventLabel());
		branchEventReq.setTimestamp(backgroundEventsReq.getTimestamp());
		branchEventReq.setOrigin("BRANCH");
		branchEventReq.setEvent_timestamp(backgroundEventsReq.getTimestamp());
		BranchUserDataPojo branchUserDataPojo = new BranchUserDataPojo();

		branchUserDataPojo.setAaid(backgroundEventsReq.getGaid()!=null?backgroundEventsReq.getGaid():"");
		branchUserDataPojo.setApp_version(backgroundEventsReq.getAppTrackingId()!=null?backgroundEventsReq.getAppTrackingId():"");
		branchUserDataPojo.setDeveloper_identity(backgroundEventsReq.getEmail()!=null?backgroundEventsReq.getEmail():"");
		branchUserDataPojo.setIp(backgroundEventsReq.getIpAddress());
		branchUserDataPojo.setModel(backgroundEventsReq.getDeviceModel());
		branchUserDataPojo.setOs(backgroundEventsReq.getOsName());
		branchUserDataPojo.setOs_version(backgroundEventsReq.getOsVersion());
		String[] screenResolution = backgroundEventsReq.getScreenResolution().split("x");
		Long screen_height = null;
		Long screen_width = null;
		if(null !=screenResolution && screenResolution.length == 2) {
			screen_height = Long.valueOf(screenResolution[1]);
			screen_width = Long.valueOf(screenResolution[0]);
		}
		branchUserDataPojo.setScreen_height(screen_height);
		branchUserDataPojo.setScreen_width(screen_width);
		branchUserDataPojo.setUser_agent(backgroundEventsReq.getUserAgent());

		BranchCustomDataPojo branchCustomDataPojo = new BranchCustomDataPojo();
		branchCustomDataPojo.setKey1("webinarId");
		branchCustomDataPojo.setValue1(backgroundEventsReq.getKeyNvaluePairs().get("webinarId"));
		branchCustomDataPojo.setUtm_campaign(backgroundEventsReq.getUtmCampaign());
		branchCustomDataPojo.setUtm_content(backgroundEventsReq.getUtmContent());
		branchCustomDataPojo.setUtm_medium(backgroundEventsReq.getUtmMedium());
		branchCustomDataPojo.setUtm_source(backgroundEventsReq.getUtmSource());
		branchCustomDataPojo.setUtm_term(backgroundEventsReq.getUtmTerm());
		branchCustomDataPojo.setVID(sessionUtils.getCallingUserId().toString());

		BranchEventDataPojo branchEventDataPojo = new BranchEventDataPojo();
		//branchEventDataPojo.setTransaction_id(transaction_id);

		branchEventReq.setCustom_data(branchCustomDataPojo);
		branchEventReq.setEvent_data(branchEventDataPojo);
		branchEventReq.setUser_data(branchUserDataPojo);
		return branchEventReq;
	}


	public PlatformBasicResponse saveUserCoreEvent(UserCoreEventsMapping userCoreEventsMapping) throws VException{

		if(null != userCoreEventsMapping) {
			Long userId = sessionUtils.getCallingUserId();
			userCoreEventsMapping.setUserId(userId.toString());
			logger.debug("sendCoreBranchEvent api has been called -> saving core event to DB->");
			backgroundEventsDao.save(userCoreEventsMapping);	
		}
		return new PlatformBasicResponse(true,"success","");
	}

	public BackgroundEventsResponse isCoreEventForUser(String parentEvent, String gaid) throws VException {

		Long userId = sessionUtils.getCallingUserId();
		logger.debug("userId = "+userId);
		logger.debug("gaid - "+gaid);
		logger.debug("parentEvent ="+parentEvent);
		BackgroundEventsResponse backgroundEventsResponse = new BackgroundEventsResponse();
		UserCoreEventsMapping userCoreEventsMapping = new UserCoreEventsMapping();
		userCoreEventsMapping = backgroundEventsDao.getUserMapping(userId.toString(), gaid);
		logger.debug("userCoreEventsMapping="+userCoreEventsMapping);
		EventsEnum type = EventsEnum.valueOf(parentEvent.toUpperCase().trim());
		logger.debug("type="+type);
		
		if(null != userCoreEventsMapping) {
			switch(type) {
			case WEBINAR:
			{
				if(null != userCoreEventsMapping.getUserId() && null != userCoreEventsMapping.getGaid() 
						&& null != userCoreEventsMapping.getWebinarDetails() && null != userCoreEventsMapping.getWebinarDetails().getIsAttended()) {
					backgroundEventsResponse.setIsFirstTimeWebinarAttendee(!userCoreEventsMapping.getWebinarDetails().getIsAttended());
					logger.debug("insdie if-> if-> backgroundEventsResponse ="+backgroundEventsResponse);
				}else {
					backgroundEventsResponse.setIsFirstTimeWebinarAttendee(isFirstTimeWebinarAttendee(userId, gaid));
					logger.debug("insdie if -> else ->backgroundEventsResponse ="+backgroundEventsResponse);
				}
				break;
			}
			case TRIAL:
			{
				//backgroundEventsResponse.setIsFirstTimeTrailAttendee());
				break;
			}
			}
		}else {
			backgroundEventsResponse.setIsFirstTimeWebinarAttendee(isFirstTimeWebinarAttendee(userId, gaid));
			logger.debug("inside else ->backgroundEventsResponse ="+backgroundEventsResponse);
		}
		logger.debug("Final backgroundEventsResponse - "+backgroundEventsResponse);
		return backgroundEventsResponse;
	}

	private boolean isFirstTimeWebinarAttendee(Long userId, String gaid) throws VException {
		String userStatus = "";
		String respString = "";

		try {
			userStatus = redisDAO.get(IS_USER_ACTIVE);
			logger.debug("userStatus -> "+userStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("User Module Health Status retrieval from Redis Failed");
		}
		if(null!=userStatus && !userStatus.isEmpty() && userStatus.equalsIgnoreCase("true")) {
			String url = USER_ENDPOINT + "/GA_Id/appRegistrationCreationTime?userId=" + userId + "&gaid=" +gaid;
			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(response);
			respString = response.getEntity(String.class);
			logger.debug("respString -> "+respString);
		}
	
		if(StringUtils.isNotEmpty(respString) && Long.valueOf(respString)>TIMESTAMP_OF_DEPLOYMENT) {
			return true;
		}
		return false;
	}

}
