/**
 * 
 */
package com.vedantu.platform.background.events.pojo;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BranchCustomDataPojo {
	
	private String VID;
	
	private String  key1;
	
	private String value1;
	
	private String utm_source;
	
	private String  utm_medium;
	
	private String  utm_campaign;
	
	private String  utm_term;
	
	private String  utm_content;

}
