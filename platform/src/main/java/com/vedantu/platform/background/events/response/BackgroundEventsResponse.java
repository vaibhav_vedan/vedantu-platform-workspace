/**
 * 
 */
package com.vedantu.platform.background.events.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BackgroundEventsResponse {

	@Builder.Default
	private Boolean isFirstTimeWebinarAttendee = false;
	@Builder.Default
	private Boolean isFirstTimeTrailAttendee = false;
}
