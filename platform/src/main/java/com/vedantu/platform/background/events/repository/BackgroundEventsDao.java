/**
 * 
 */
package com.vedantu.platform.background.events.repository;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.vedantu.platform.background.events.entity.UserCoreEventsMapping;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

/**
 * @author subarna
 *
 */

@Repository
public class BackgroundEventsDao extends AbstractMongoDAO {
	
	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(BackgroundEventsDao.class);
	
	@Autowired
    private MongoClientFactory mongoClientFactory;

	@Override
	protected MongoOperations getMongoOperations() {
		
		return mongoClientFactory.getMongoOperations();
	}
	
	public UserCoreEventsMapping getUserMapping(String userId, String gaid) {
        Query query = new Query();
        UserCoreEventsMapping userMappings = new UserCoreEventsMapping();
        logger.debug("gaid = "+gaid);
        //query.addCriteria(Criteria.where(UserCoreEventsMapping.Constants.USER_ID).is(userId));
        if(null == gaid) {
        	return userMappings;
        }else {
        	query.addCriteria(Criteria.where(UserCoreEventsMapping.Constants.GAID).is(gaid));
        	userMappings = findOne(query, UserCoreEventsMapping.class);
        }
        logger.debug("query usercoremapping = "+query);
        logger.debug( "userMappings = "+userMappings);
       
        return userMappings;
    }
	
	 public void save(UserCoreEventsMapping userMapping) {
		 logger.debug("Request to save userMapping : " + userMapping.toString());
		 UserCoreEventsMapping entity = new UserCoreEventsMapping(); 
			try {
				if(null != userMapping) {
					if(null != userMapping.getUserId()) {
						entity = getUserMapping(userMapping.getUserId(), userMapping.getGaid());
						if(null != entity) {
							if(null == entity.getWebinarDetails())
							entity.setWebinarDetails(userMapping.getWebinarDetails());
							
							//if(null == entity.getSomeOtherDetails)
							//entity.setSomeotherDetaiks();
							
							saveEntity(entity);
						}else {
							saveEntity(userMapping);
						}
					}

				}
			} catch (Exception ex) {
				throw new RuntimeException("UserCoreEventsMapping DAO : Error updating UserCoreEventsMapping " + userMapping.toString(), ex);
			}
	       
	    }

	public void update(UserCoreEventsMapping userCoreEventsMapping) {
		// TODO Auto-generated method stub
		
	}

	    

}
