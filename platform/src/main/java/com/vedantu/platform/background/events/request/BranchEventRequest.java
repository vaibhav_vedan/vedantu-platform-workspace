/**
 * 
 */
package com.vedantu.platform.background.events.request;

import com.vedantu.platform.background.events.pojo.BranchCustomDataPojo;
import com.vedantu.platform.background.events.pojo.BranchEventDataPojo;
import com.vedantu.platform.background.events.pojo.BranchUserDataPojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BranchEventRequest {
	
	private String branch_key;

	private String name;
	
	private String timestamp;
	
	private String origin;
	
	private String event_timestamp;
	
	private BranchUserDataPojo user_data;
	
	private BranchCustomDataPojo custom_data;
	
	private BranchEventDataPojo event_data;
	
	
}
