/**
 * 
 */
package com.vedantu.platform.background.events.entity;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.platform.background.events.pojo.WebinarDetails;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "UserCoreEventsMapping")
@CompoundIndexes({
    @CompoundIndex(def = "{'gaid': 1, 'userId': 1}", unique = true, background = true, useGeneratedName = true)
})
public class UserCoreEventsMapping extends AbstractMongoStringIdEntity{
	
	@Indexed(background = true)
	private String userId;
	
	@Indexed(background = true)
	private String gaid;
	
	private WebinarDetails webinarDetails;
	
	
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String GAID = "gaid";
        public static final String WEBINAR_DETAILS = "webinarDetails";
        public static final String IS_ATTENDED = "webinarDetails.isAttended";
    }

}
