/**
 * 
 */
package com.vedantu.platform.background.events.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BranchUserDataPojo {

	private String app_version;
	
	private String os;
	
	private String os_version;
	
	private String model;
	
	private String brand;
	
	private String idfa;
	
	private String aaid;
	
	private String browser_fingerprint_id;
	
	private String developer_identity;
	
	private String ip;
	
	private String geo_long;
	
	private String geo_lat;
	
	private Long screen_width;
	
	private Long screen_height;
	
	private String user_agent;
	
}
