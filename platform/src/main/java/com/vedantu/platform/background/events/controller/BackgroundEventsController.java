/**
 * 
 */
package com.vedantu.platform.background.events.controller;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.VException;
import com.vedantu.platform.background.events.manager.BackgroundEventsManager;
import com.vedantu.platform.background.events.request.BackgroundEventsReq;
import com.vedantu.platform.background.events.response.BackgroundEventsResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * @author subarna
 *
 */

@RestController
@RequestMapping("/events")
public class BackgroundEventsController {

	@Autowired
	private BackgroundEventsManager backgroundEventsManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(BackgroundEventsController.class);
	
    @Autowired
    HttpSessionUtils sessionUtils;
    
    @PostMapping(value = "/send/BranchEvent", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse sendCoreBranchEvent(@RequestBody BackgroundEventsReq backgroundEventsReq) throws VException{
    	
		return backgroundEventsManager.sendCoreBranchEvent(backgroundEventsReq);
    	
    }
    
    @RequestMapping(value = "/isCoreEvent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BackgroundEventsResponse isCoreEventForUser(@RequestParam(value = "parentEvent", required = true) String parentEvent, @RequestParam(value = "gaid", required = false) String gaid) throws VException{
    	
    	BackgroundEventsResponse backgroundEventsResponse = new BackgroundEventsResponse();
    	backgroundEventsResponse = backgroundEventsManager.isCoreEventForUser(parentEvent, gaid);
    	logger.debug("In controller final backgroundEventsResponse = "+backgroundEventsResponse);
    	return backgroundEventsResponse;
    	
    }
}
