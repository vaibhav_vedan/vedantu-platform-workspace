package com.vedantu.platform.listing.dao.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Feature extends AbstractMongoStringIdEntity {
	private String name;

	public Feature() {
		super();
	}

	public Feature(String id, String name) {
		super();
		super.setId(id);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String NAME = "name";
	}

}
