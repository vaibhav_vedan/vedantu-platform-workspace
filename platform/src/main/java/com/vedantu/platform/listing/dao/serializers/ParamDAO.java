package com.vedantu.platform.listing.dao.serializers;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.listing.dao.entity.Param;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ParamDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;
        

    public ParamDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

	public void create(Param p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}


	public Param getById(String id) {
		Param p = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				p = (Param) getEntityById(id, Param.class);
			}
		} catch (Exception ex) {
			// log Exception
			p = null;
		}
		return p;
	}

        public void update(Query q, Update u) {
            try {
                updateFirst(q, u, Param.class);
            } catch (Exception ex) {
            }
        }      

	public int deleteById(Long id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Param.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
