package com.vedantu.platform.listing.pojo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.RangeFilterBuilder;

import java.util.ArrayList;
import java.util.List;

public class Filter {

	private String type;
	private List<Tuple> params;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Tuple> getParams() {
		return params;
	}

	public void setParams(List<Tuple> params) {
		this.params = params;
	}

	public FilterBuilder getFilter() throws Exception {
		List<String> errors = validate();
//		LogFactory logfactory = null;
//		@SuppressWarnings("static-access")
//		Logger logger = logFactory.getLogger(Filter.class);
		if (errors != null && !errors.isEmpty()) {
//			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		}

		FilterBuilder filter = null;
		switch (type) {
		case "bool":
			filter = getBoolFilter();
			break;
		case "term":
			filter = getTermFilter();
			break;
		case "range":
			filter = getRangeFilter();
			break;
		case "hasChild":
			filter = getHasChildFilter();
			break;
		case "terms":
			filter = getTermsFilter();
		default:
			break;
		}

		if (filter != null) {
//			logger.info(filter.toString());
		}
		return filter;
	}

	FilterBuilder getBoolFilter() throws Exception {
		FilterBuilder q = FilterBuilders.boolFilter();

		for (Tuple i : params) {
			String key = i.getName();
			List<Object> value = i.getValue();

			switch (key) {
			case "must":
				for (Object o : value) {
					GsonBuilder gsonBuilder = new GsonBuilder();
					gsonBuilder.setLongSerializationPolicy( LongSerializationPolicy.STRING );
					Gson gson = gsonBuilder.create();
					String json = gson.toJson(o);
					Filter f = new Gson().fromJson(json, Filter.class);
					FilterBuilder f1 = f.getFilter();
					if (f1 != null) {
						q = ((BoolFilterBuilder) q).must(f1);
					}
				}
				break;
			case "mustnot":
				for (Object o : value) {
					String json = new Gson().toJson(o);
					Filter f = new Gson().fromJson(json, Filter.class);
					FilterBuilder f1 = f.getFilter();
					if (f1 != null) {
						q = ((BoolFilterBuilder) q).mustNot(f1);
					}
				}
				break;
			case "should":
				for (Object o : value) {
					String json = new Gson().toJson(o);
					Filter f = new Gson().fromJson(json, Filter.class);
					FilterBuilder f1 = f.getFilter();
					if (f1 != null) {
						q = ((BoolFilterBuilder) q).should(f1);
					}
				}
				break;
			default:
				break;
			}
		}
		return q;
	}

	FilterBuilder getTermFilter() {
		Tuple i = params.get(0);
		String key = i.getName();
		if (key != null && i.getValue() != null && !i.getValue().isEmpty()) {
			if(key.equals("grade")){
				Double ii = (Double)i.getValue().get(0);
				return FilterBuilders.termFilter(key, ii.intValue());
			}
			return FilterBuilders.termFilter(key, i.getValue().get(0));
		}
		return null;
	}
	
	FilterBuilder getTermsFilter() {
		Tuple i = params.get(0);
		String key = i.getName();
		if (key != null && i.getValue() != null && !i.getValue().isEmpty()) {
			return FilterBuilders.termsFilter(key, i.getValue());
		}
		return null;
	}

	FilterBuilder getHasChildFilter() throws Exception {
		String type;
		Object o;
		Filter f;
		Tuple t1 = params.get(0);
		Tuple t2 = params.get(1);
		if (t1.getName().equals("type")) {
			type = (String) t1.getValue().get(0);
			o = t2.getValue().get(0);
			String json = new Gson().toJson(o);
			f = new Gson().fromJson(json, Filter.class);
		} else {
			type = (String) t2.getValue().get(0);
			o = t1.getValue().get(0);
			String json = new Gson().toJson(o);
			f = new Gson().fromJson(json, Filter.class);
		}

		FilterBuilder f1 = f.getFilter();
		if (f1 != null) {
			return FilterBuilders.hasChildFilter(type, f1);
		}

		return null;
	}

	FilterBuilder getRangeFilter() {
		FilterBuilder q = null;
		for (Tuple i : params) {
			String key = (String) i.getName();
			List<Object> value = i.getValue();

			if (key != null) {
				switch (key) {
				case "name":
					if (value != null && !value.isEmpty()) {
						q = FilterBuilders.rangeFilter((String) value.get(0));
					}
					break;
				case "gte":
					if (value != null && !value.isEmpty()) {
						q = ((RangeFilterBuilder) q).gte(value.get(0));
					}
					break;
				case "lte":
					if (value != null && !value.isEmpty()) {
						q = ((RangeFilterBuilder) q).lte(value.get(0));
					}
					break;
				case "gt":
					if (value != null && !value.isEmpty()) {
						q = ((RangeFilterBuilder) q).gt(value.get(0));
					}
					break;
				case "lt":
					if (value != null && !value.isEmpty()) {
						q = ((RangeFilterBuilder) q).lt(value.get(0));
					}
					break;
				case "from":
					if (value != null && !value.isEmpty()) {
						q = ((RangeFilterBuilder) q).from(value.get(0));
					}
					break;
				case "to":
					if (value != null && !value.isEmpty()) {
						q = ((RangeFilterBuilder) q).to(value.get(0));
					}
					break;
				case "lower":
					q = ((RangeFilterBuilder) q).includeLower(true);
					break;
				case "upper":
					q = ((RangeFilterBuilder) q).includeUpper(true);
					break;
				default:
					break;
				}
			}
		}
		return q;
	}

	public List<String> validate() {
		List<String> errors = new ArrayList<String>();

		if (type == null) {
			errors.add(Constants.TYPE);
		}
		if (params == null || params.isEmpty()) {
			errors.add(Constants.PARAMS);
		}
		return errors;
	}

	public static class Constants {
		public static final String TYPE = "type";
		public static final String PARAMS = "params";
	}

	@Override
	public String toString() {
		String s = null;
		if (params != null) {
			s = "Filter [ type=" + type + ", params=" + params.toString() + "]";
		} else {
			s = "Filter [ type=" + type + ", params=" + params + "]";
		}
		return s;
	}
}