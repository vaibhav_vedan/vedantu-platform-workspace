package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.platform.listing.dao.entity.Operator;

import java.util.List;

public class GetOperatorRes extends AbstractListRes<Operator> {
	private int totalCount;

	public GetOperatorRes() {
		super();
	}

	public GetOperatorRes(List<Operator> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
