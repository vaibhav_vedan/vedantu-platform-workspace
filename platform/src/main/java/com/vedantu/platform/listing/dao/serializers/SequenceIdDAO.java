package com.vedantu.platform.listing.dao.serializers;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.listing.dao.entity.SequenceId;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class SequenceIdDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;
        

    public SequenceIdDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

	public void create(SequenceId p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public SequenceId getById(String id) {
		SequenceId sequence = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				sequence = (SequenceId) getEntityById(id, SequenceId.class);
			}
		} catch (Exception ex) {
			// log Exception
			sequence = null;
		}
		return sequence;
	}

	public SequenceId findAndModify(Query q, Update u, FindAndModifyOptions o) {
		SequenceId sequence = null;
		try {
			if (q != null) {
				sequence = (SequenceId) findAndModifyEntity(q, u, o, SequenceId.class);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sequence;
	}

        public void update(Query q, Update u) {
            try {
                updateFirst(q, u, SequenceId.class);
            } catch (Exception ex) {
            }
        }        
        
	public int deleteById(Long id) {
		int result = 0;
		try {
			result = deleteEntityById(id, SequenceId.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

	public long getNextSequenceId(String key) throws RuntimeException {

		Query query = new Query(Criteria.where("name").is(key));
		Update update = new Update();
		update.inc("seq", 1);
		FindAndModifyOptions options = new FindAndModifyOptions();
		options.returnNew(true);

		SequenceId seqId = findAndModify(query, update, options);

		if (seqId == null) {
			throw new RuntimeException("Unable to get sequence id for key : " + key);
			
		}

		return seqId.getSeq();
	}
}
