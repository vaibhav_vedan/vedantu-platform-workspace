package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.platform.listing.dao.entity.FeatureMapping;

import java.util.List;

public class GetFeatureMappingRes extends AbstractListRes<FeatureMapping> {
	private int totalCount;

	public GetFeatureMappingRes() {
		super();
	}

	public GetFeatureMappingRes(List<FeatureMapping> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
