package com.vedantu.platform.listing.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetOperatorReq extends AbstractFrontEndListReq {

	public GetOperatorReq() {
		super();
	}

	public GetOperatorReq(Integer start, Integer limit) {
		super(start, limit);
	}

}
