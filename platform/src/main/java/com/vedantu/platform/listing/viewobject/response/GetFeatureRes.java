package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.platform.listing.dao.entity.Feature;

import java.util.List;

public class GetFeatureRes extends AbstractListRes<Feature> {
	private int totalCount;

	public GetFeatureRes() {
		super();
	}

	public GetFeatureRes(List<Feature> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
