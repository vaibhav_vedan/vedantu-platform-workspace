package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.platform.listing.pojo.Teacher;

import java.util.List;

public class GetTeacherResponse {

	long hits;
	List<Teacher> teachers;
	
	public long getHits() {
		return hits;
	}
	public void setHits(long hits) {
		this.hits = hits;
	}
	public List<Teacher> getTeachers() {
		return teachers;
	}
	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}
	
}
