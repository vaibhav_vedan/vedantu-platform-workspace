package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.exception.ErrorCode;

public class BaseResponse {


		public ErrorCode errorCode;
		public String errorMessage;

		public BaseResponse() {
			super();
			// TODO Auto-generated constructor stub
		}

		public BaseResponse(ErrorCode errorCode, String errorMessage) {
			super();
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public ErrorCode getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(ErrorCode errorCode) {
			this.errorCode = errorCode;
		}


		@Override
		public String toString() {
			return "BaseResponse errorCode:" + errorCode.toString() + ", errorMessage:" + errorMessage;
		}

}
