package com.vedantu.platform.listing.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize
public class Tuple {
	private String name;
	private List<Object> value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Object> getValue() {
		return value;
	}

	public void setValue(List<Object> value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Tuple [ name=" + name + ", value=" + value.toString() + "]";
	}
}