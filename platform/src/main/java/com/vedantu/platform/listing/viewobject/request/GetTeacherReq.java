package com.vedantu.platform.listing.viewobject.request;

import com.vedantu.platform.listing.pojo.Filter;
import com.vedantu.platform.listing.pojo.Sort;
import com.vedantu.platform.listing.pojo.Tuple;

import java.util.List;

public class GetTeacherReq {
	
	private Tuple functions;
	private Filter filter;
	private Sort sort;
	private String offeringId;
	private Integer from;
	private Integer size;
        private List<Long> teacherIds;

	public Tuple getFunctions() {
		return functions;
	}

	public void setFunctions(Tuple functions) {
		this.functions = functions;
	}
	
	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}

	public Sort getSort() {
		return sort;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public Integer getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

    public List<Long> getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(List<Long> teacherIds) {
        this.teacherIds = teacherIds;
    }
        
        

	public static class Constants {
		public static final String FILTER = "filter";
		public static final String SORT = "sort";
		public static final String FROM = "from";
		public static final String SIZE = "size";
	}

	@Override
	public String toString() {
		String s = null;
		if(filter != null && sort != null){
			s = "GetTeacherReq [ filter=" + filter.toString() + ", sort=" + sort.toString() + ", from=" + from
					+ ", size=" + size + "]";
		} else if(filter != null) {
			s = "GetTeacherReq [ filter=" + filter.toString() + ", sort=" + sort + ", from=" + from
					+ ", size=" + size + "]";
		} else{
			s = "GetTeacherReq [ filter=" + filter + ", sort=" + sort + ", from=" + from
					+ ", size=" + size + "]";
		}
		return s;
	}
}