package com.vedantu.platform.listing.dao.entity;

import com.vedantu.platform.listing.enums.OperatorType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Operator extends AbstractMongoStringIdEntity {
	private String symbol;
	private OperatorType type;

	public Operator() {
		super();
	}

	public Operator(String id, String symbol, OperatorType type) {
		super();
		super.setId(id);
		this.symbol = symbol;
		this.type = type;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public OperatorType getType() {
		return type;
	}

	public void setType(OperatorType type) {
		this.type = type;
	}

	public Double operatorResult(Double param) {
		Double result = 0.0;
		if (type.equals(OperatorType.UNARY)) {
			switch (symbol) {
			case "+":
				result = param;
				break;
			case "-":
				result = -1 * param;
				break;
			default:
				throw new IllegalArgumentException("Invalid operator: " + symbol);
			}
		} else {
			result = null;
		}
		return result;
	}

	public Double operatorResult(Double param1, Double param2) {
		Double result = 0.0;
		if (type.equals(OperatorType.BINARY)) {
			switch (symbol) {
			case "+":
				result = param1 + param2;
				break;
			case "-":
				result = param1 - param2;
				break;
			case "*":
				result = param1 * param2;
				break;
			case "/":
				if (param2.equals(0)) {
					throw new IllegalArgumentException("Invalid param2: " + param2);
				}
				result = param1 / param2;
				break;
			default:
				throw new IllegalArgumentException("Invalid operator: " + symbol);
			}
		} else {
			result = null;
		}
		return result;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String SYMBOL = "symbol";
		public static final String TYPE = "type";
	}
}
