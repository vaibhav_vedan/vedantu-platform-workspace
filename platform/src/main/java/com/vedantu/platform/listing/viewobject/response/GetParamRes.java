package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.platform.listing.dao.entity.Param;

import java.util.List;

public class GetParamRes extends AbstractListRes<Param> {
	private int totalCount;

	public GetParamRes() {
		super();
	}

	public GetParamRes(List<Param> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
