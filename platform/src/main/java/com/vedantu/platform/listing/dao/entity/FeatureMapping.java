package com.vedantu.platform.listing.dao.entity;

import com.vedantu.platform.listing.enums.OperatorType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class FeatureMapping extends AbstractMongoStringIdEntity {
	private String featureId;
	private String paramId;
	private Double weight;
	private String weightOp;
	private String operatorId;
	private OperatorType operatorType;
	private Boolean bOpOrder;
	private String secOpSymbol;
	private Integer opSequence;

	public FeatureMapping() {
		super();
	}

	public FeatureMapping(String id, String featureId, String paramId, Double weight, String weightOp,
			String operatorId, OperatorType operatorType, Boolean bOpOrder, String secOpSymbol, Integer opSequence) {
		super();
		super.setId(id);
		this.featureId = featureId;
		this.paramId = paramId;
		this.weight = weight;
		this.weightOp = weightOp;
		this.operatorId = operatorId;
		this.operatorType = operatorType;
		this.bOpOrder = bOpOrder;
		this.secOpSymbol = secOpSymbol;
		this.opSequence = opSequence;
	}

	public String getFeatureId() {
		return featureId;
	}

	public void setFeatureId(String featureId) {
		this.featureId = featureId;
	}

	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getWeightOp() {
		return weightOp;
	}

	public void setWeightOp(String weightOp) {
		this.weightOp = weightOp;
	}

	public OperatorType getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(OperatorType operatorType) {
		this.operatorType = operatorType;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	// public boolean getBOpOrder() {
	// return bOpOrder;
	// }
	//
	// public void setBOpOrder(boolean bOpOrder) {
	// this.bOpOrder = bOpOrder;
	// }

	public String getSecOpSymbol() {
		return secOpSymbol;
	}

	public Boolean getbOpOrder() {
		return bOpOrder;
	}

	public void setbOpOrder(Boolean bOpOrder) {
		this.bOpOrder = bOpOrder;
	}

	public void setSecOpSymbol(String secOpSymbol) {
		this.secOpSymbol = secOpSymbol;
	}

	public Integer getOpSequence() {
		return opSequence;
	}

	public void setOpSequence(Integer opSequence) {
		this.opSequence = opSequence;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String FEATUREID = "featureId";
		public static final String PARAMID = "paramId";
		public static final String WEIGHT = "weight";
		public static final String WEIGHTOP = "weightOp";
		public static final String OPERATORID = "operatorId";
		public static final String OPERATORTYPE = "operatorType";
		public static final String BOPORDER = "bOpOrder";
		public static final String SECOPSYMBOL = "secOpSymbol";
		public static final String OPSEQUENCE = "opSequence";
	}
}
