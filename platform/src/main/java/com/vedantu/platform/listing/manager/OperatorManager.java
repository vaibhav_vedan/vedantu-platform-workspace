package com.vedantu.platform.listing.manager;

import com.vedantu.platform.listing.dao.entity.Operator;
import com.vedantu.platform.listing.dao.serializers.OperatorDAO;
import com.vedantu.platform.listing.dao.serializers.SequenceIdDAO;
import com.vedantu.platform.listing.viewobject.request.GetOperatorReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperatorManager {

	@Autowired
	public OperatorDAO operatorDAO;

	@Autowired
	public static SequenceIdDAO sequenceIdDAO = new SequenceIdDAO();

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(OperatorManager.class);

	public Operator operator(Operator operator) {

		OperatorDAO operatorDAO = new OperatorDAO();
		String id = operator.getId();
		logger.info("DT: ");

		if (id == null) {
			Long seq_id = sequenceIdDAO.getNextSequenceId("operator");
			operator.setId(seq_id.toString());
			operatorDAO.create(operator);
		} else {
			Update update = createUpdateObject(operator);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(id));
			operatorDAO.update(query, update);
			operator = (Operator) operatorDAO.getEntityById(id, Operator.class);
		}

		logger.info("ID:");
		return operator;
	}

	public Operator getOperatorById(String id) {
		Operator operator = operatorDAO.getById(id);
		return operator;
	}

	public List<Operator> getOperators(GetOperatorReq req) {
		Query query = new Query();
		query.addCriteria(Criteria.where("enabled").is(true));
		query.with(Sort.by(Sort.Direction.DESC, Operator.Constants.CREATION_TIME));
		Long totalCount = operatorDAO.queryCount(query, Operator.class);
		logger.info("count query :" + query + totalCount);
		Integer start = req.getStart();
		Integer limit = req.getSize();
		if (start == null || start < 0) {
			start = 0;
		}

		if (limit == null || limit <= 0) {
			limit = 20;
		}

		query.skip(start);
		query.limit(limit);
		List<Operator> results = operatorDAO.runQuery(query, Operator.class);
		return results;
	}

	public Update createUpdateObject(Operator operator) {

		if (operator == null) {
			return null;
		}
		Update update = new Update();
		String symbol = operator.getSymbol();
		EntityState entityState = operator.getEntityState();

		if (symbol != null)
			update.set("symbol", symbol);

		update.set("entityState", entityState);

		return update;
	}
}
