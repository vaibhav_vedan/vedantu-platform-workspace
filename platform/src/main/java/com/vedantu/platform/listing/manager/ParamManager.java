package com.vedantu.platform.listing.manager;

import com.vedantu.platform.listing.dao.entity.Feature;
import com.vedantu.platform.listing.dao.entity.FeatureMapping;
import com.vedantu.platform.listing.dao.entity.Param;
import com.vedantu.platform.listing.dao.serializers.FeatureDAO;
import com.vedantu.platform.listing.dao.serializers.FeatureMappingDAO;
import com.vedantu.platform.listing.dao.serializers.ParamDAO;
import com.vedantu.platform.listing.dao.serializers.SequenceIdDAO;
import com.vedantu.platform.listing.enums.ParamType;
import com.vedantu.platform.listing.pojo.WeightedParam;
import com.vedantu.platform.listing.viewobject.request.GetParamReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParamManager {

	@Autowired
	public ParamDAO paramDAO;

	@Autowired
	public FeatureDAO featureDAO;

	@Autowired
	public FeatureMappingDAO featureMappingDAO;

	@Autowired
	public static SequenceIdDAO sequenceIdDAO = new SequenceIdDAO();

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ParamManager.class);

	public Param param(Param param) {

		ParamDAO paramDAO = new ParamDAO();
		String id = param.getId();
		logger.info("param start: ");

		if (id == null) {
			Long seq_id = sequenceIdDAO.getNextSequenceId("param");
			param.setId(seq_id.toString());
			paramDAO.create(param);
		} else {
			Update update = createUpdateObject(param);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(id));
			paramDAO.update(query, update);
			param = (Param) paramDAO.getEntityById(id, Param.class);
		}

		logger.info("param end:");
		return param;
	}

	public Param getParamById(String id) {
		Param param = paramDAO.getById(id);
		return param;
	}

	public List<Param> getParams(GetParamReq req) {
		Query query = new Query();
		query.addCriteria(Criteria.where("enabled").is(true));
		query.with(Sort.by(Sort.Direction.DESC, Param.Constants.CREATION_TIME));
		long totalCount = paramDAO.queryCount(query, Param.class);
		logger.info("count query :" + query + totalCount);
		Integer start = req.getStart();
		Integer limit = req.getSize();
		if (start == null || start < 0) {
			start = 0;
		}

		if (limit == null || limit <= 0) {
			limit = 20;
		}

		query.skip(start);
		query.limit(limit);
		List<Param> results = paramDAO.runQuery(query, Param.class);
		return results;
	}

	public Update createUpdateObject(Param param) {

		if (param == null) {
			return null;
		}
		Update update = new Update();
		String name = param.getName();
		Double value = param.getValue();
		ParamType type = param.getType();
		EntityState entityState = param.getEntityState();

		if (name != null)
			update.set("name", name);
		if (value != null)
			update.set("value", value);
		if (type != null)
			update.set("type", type);
		if (entityState != null)
			update.set("entityState", entityState);

		return update;
	}

	public String getParamByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		List<Param> results = paramDAO.runQuery(query, Param.class);
		return results.get(0).getId();

	}

	public List<WeightedParam> getlistingParamsforFeature(String featureName) {
		Query query = new Query();
		List<WeightedParam> weightedParamsForFeature = new ArrayList<WeightedParam>();
		query.addCriteria(Criteria.where("name").is(featureName));
		List<Feature> features = featureDAO.runQuery(query, Feature.class);
		if (features != null && features.size() > 0) {
			Feature feature = features.get(0);
			query = new Query();
			logger.info("got feature");
			query.addCriteria(Criteria.where("featureId").is(feature.getId()));
		//	query.addCriteria(Criteria.where("featureId").is("1"));
			List<FeatureMapping> featureMappings = featureMappingDAO.runQuery(query, FeatureMapping.class);
			
			for (FeatureMapping featureMapping : featureMappings) {
				logger.info("got featuremapping");
				if (featureMapping.getParamId() != null) {
					query = new Query();
					String paramId = featureMapping.getParamId();
					logger.info("got param"+paramId);
					Param param = paramDAO.getById(paramId);
					///Param param = paramDAO.getById("11");
					if (param!=null && param.getType().equals(ParamType.DYNAMIC)) {
						WeightedParam weightedParam = new WeightedParam(param.getName(), featureMapping.getWeight());
						weightedParamsForFeature.add(weightedParam);
					}

				}
			}
		}
		return weightedParamsForFeature;
	}

}
