package com.vedantu.platform.listing.controllers;

import com.vedantu.platform.listing.dao.entity.Param;
import com.vedantu.platform.listing.manager.FeatureMappingManager;
import com.vedantu.platform.listing.manager.ParamManager;
import com.vedantu.platform.listing.pojo.WeightedParam;
import com.vedantu.platform.listing.viewobject.request.GetParamReq;
import com.vedantu.platform.listing.viewobject.response.GetParamRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("param")
public class ParamController {

	@Autowired
	private ParamManager paramManager;

	@Autowired
	private FeatureMappingManager featureMappingManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ParamController.class);

	@RequestMapping(value = "/addParam", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Param addParam(@RequestBody Param param) throws Exception {

		param.setLastUpdated(System.currentTimeMillis());
                param.setEntityState(EntityState.ACTIVE);
		logger.info("Request:" + param.toString());
		List<String> errors = validate(param);
		Param paramRes;

		if (errors != null && !errors.isEmpty()) {
			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		} else {
			paramRes = paramManager.param(param);
		}

		logger.info("Response:" + paramRes.toString());
		return paramRes;
	}

	@RequestMapping(value = { "getAllParams" }, method = RequestMethod.GET)
	@ResponseBody
	public GetParamRes getAllParams(@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer limit) throws Exception {

		logger.info("Request received for getting All Param");
		GetParamReq req = new GetParamReq(start, limit);
		GetParamRes res = (GetParamRes) paramManager.getParams(req);
		logger.info("Response:" + res.toString());
		return res;
	}

	@RequestMapping(value = "getParam/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Param getParamById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for getting Param for db_id: " + id);
		Param paramRes = paramManager.getParamById(id);
		logger.info("Response:" + paramRes.toString());
		return paramRes;
	}

	@RequestMapping(value = "getParamIdByName", method = RequestMethod.GET)
	@ResponseBody
	public String getParamIdByName(@RequestParam(value = "name", required = true) String name) throws Exception {
		logger.info("Request received for getting Param for name: " + name);
		String id = paramManager.getParamByName(name);
		logger.info("Response id:" + id);
		return id;
	}

	@RequestMapping(value = "getFeatureParams", method = RequestMethod.GET)
	@ResponseBody
	public List<WeightedParam> getFeatureParams(
			@RequestParam(value = "featureName", required = true) String featureName) throws Exception {
		logger.info("Request received for getting listing params: " + featureName);
		List<WeightedParam> weightedParams = paramManager.getlistingParamsforFeature(featureName);
		return weightedParams;
	}

	@RequestMapping(value = "/updateParam/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Param updateParam(@RequestBody Param param, @PathVariable("id") String id) throws Exception {
		param.setLastUpdated(System.currentTimeMillis());
		param.setId(id);
		logger.info("Request:" + param.toString());
		Param paramRes = paramManager.param(param);
		logger.info("Response:" + paramRes.toString());
		return paramRes;
	}

	@RequestMapping(value = "deleteParam/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Param deleteParamById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for disabling Param for id: " + id);
		Param paramRes = paramManager.getParamById(id);
		if (paramRes != null) {
                        paramRes.setEntityState(EntityState.INACTIVE);
			paramRes = paramManager.param(paramRes);
			featureMappingManager.deleteFeatureMappingsByParamId(id);
			logger.info("Response:" + paramRes.toString());
		}
		return paramRes;
	}

	public List<String> validate(Param param) {
		List<String> errors = new ArrayList<String>();

		if (param == null || param.getName() == null) {
			errors.add(Param.Constants.NAME);
		}

		String id = param.getId();
		String name = param.getName();

		logger.info("ID: " + id + " ,name: " + name);
		return errors;
	}
}
