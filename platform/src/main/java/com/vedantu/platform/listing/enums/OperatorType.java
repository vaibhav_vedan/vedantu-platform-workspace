package com.vedantu.platform.listing.enums;

public enum OperatorType {
	UNARY, BINARY
}