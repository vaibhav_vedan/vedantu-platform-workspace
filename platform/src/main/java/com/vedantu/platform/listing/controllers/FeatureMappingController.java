package com.vedantu.platform.listing.controllers;

import com.vedantu.platform.listing.dao.entity.FeatureMapping;
import com.vedantu.platform.listing.manager.FeatureMappingManager;
import com.vedantu.platform.listing.pojo.WeightedParam;
import com.vedantu.platform.listing.viewobject.request.GetFeatureMappingReq;
import com.vedantu.platform.listing.viewobject.response.BaseResponse;
import com.vedantu.platform.listing.viewobject.response.GetFeatureMappingRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("featureMapping")
public class FeatureMappingController {

	@Autowired
	private FeatureMappingManager featureMappingManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FeatureMappingController.class);

	@RequestMapping(value = "/addFeatureMapping", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FeatureMapping addFeatureMapping(@RequestBody FeatureMapping featureMapping) throws Exception {

		featureMapping.setLastUpdated(System.currentTimeMillis());
		featureMapping.setEntityState(EntityState.ACTIVE);
		logger.info("Request:" + featureMapping.toString());
		List<String> errors = validate(featureMapping);
		FeatureMapping featureMappingRes;

		if (errors != null && !errors.isEmpty()) {
			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		} else {
			featureMappingRes = featureMappingManager.featureMapping(featureMapping);
		}

		logger.info("Response:" + featureMappingRes.toString());
		return featureMappingRes;
	}

	@RequestMapping(value = { "getAllFeatureMappings" }, method = RequestMethod.GET)
	@ResponseBody
	public GetFeatureMappingRes getAllFeatureMappings(@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer limit) throws Exception {

		logger.info("Request received for getting All FeatureMapping");
		GetFeatureMappingReq req = new GetFeatureMappingReq(start, limit);
		GetFeatureMappingRes res = new GetFeatureMappingRes();
		res.setList(featureMappingManager.getFeatureMappings(req));
		logger.info("Response:" + res.toString());
		return res;
	}

	@RequestMapping(value = "getFeatureMapping/{id}", method = RequestMethod.GET)
	@ResponseBody
	public FeatureMapping getFeatureMappingById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for getting FeatureMapping for db_id: " + id);
		FeatureMapping featureMappingRes = featureMappingManager.getFeatureMappingById(id);
		logger.info("Response:" + featureMappingRes.toString());
		return featureMappingRes;
	}

	@RequestMapping(value = "/updateFeatureMapping/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FeatureMapping updateFeatureMapping(@RequestBody FeatureMapping featureMapping,
			@PathVariable("id") String id) throws Exception {
		featureMapping.setLastUpdated(System.currentTimeMillis());
		featureMapping.setId(id);
		logger.info("Request:" + featureMapping.toString());
		FeatureMapping featureMappingRes = featureMappingManager.featureMapping(featureMapping);
		logger.info("Response:" + featureMappingRes.toString());
		return featureMappingRes;
	}

	@RequestMapping(value = "/updateFeatureMappingWeight", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse updateFeatureMappingWeight(@RequestBody WeightedParam weightedParam
			) throws Exception {
		 BaseResponse basicRes = featureMappingManager.updateFeatureMappingWeight(weightedParam);
		return basicRes;
	}
	@RequestMapping(value = "deleteFeatureMapping/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public FeatureMapping deleteFeatureMappingById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for disabling FeatureMapping for id: " + id);
		FeatureMapping featureMappingRes = featureMappingManager.getFeatureMappingById(id);
		if (featureMappingRes != null) {
                        featureMappingRes.setEntityState(EntityState.INACTIVE);
			featureMappingRes = featureMappingManager.featureMapping(featureMappingRes);
			logger.info("Response:" + featureMappingRes.toString());
		}
		return featureMappingRes;
	}

	public List<String> validate(FeatureMapping featureMapping) {
		List<String> errors = new ArrayList<String>();

		if (featureMapping == null || featureMapping.getFeatureId() == null) {
			errors.add(FeatureMapping.Constants.FEATUREID);
		}

		String id = featureMapping.getId();
		String featureId = featureMapping.getFeatureId();

		logger.info("ID: " + id + " ,feature: " + featureId);
		return errors;
	}
}
