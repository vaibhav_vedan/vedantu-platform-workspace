package com.vedantu.platform.listing.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetFeatureMappingReq extends AbstractFrontEndListReq {

	public GetFeatureMappingReq() {
		super();
	}

	public GetFeatureMappingReq(Integer start, Integer limit) {
		super(start, limit);
	}

}
