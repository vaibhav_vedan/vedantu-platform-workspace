package com.vedantu.platform.listing.viewobject.response;

import com.vedantu.platform.listing.dao.entity.SequenceId;

import java.util.List;

public class GetSequenceRes extends AbstractListRes<SequenceId> {
	private int totalCount;

	public GetSequenceRes() {
		super();
	}

	public GetSequenceRes(List<SequenceId> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
