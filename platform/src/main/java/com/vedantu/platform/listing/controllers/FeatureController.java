package com.vedantu.platform.listing.controllers;

import com.vedantu.platform.listing.dao.entity.Feature;
import com.vedantu.platform.listing.manager.FeatureManager;
import com.vedantu.platform.listing.manager.FeatureMappingManager;
import com.vedantu.platform.listing.viewobject.request.GetFeatureReq;
import com.vedantu.platform.listing.viewobject.response.GetFeatureRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("feature")
public class FeatureController {

    @Autowired
    private FeatureManager featureManager;

    @Autowired
    private FeatureMappingManager featureMappingManager;

    @Autowired(required = true)
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FeatureController.class);

    @RequestMapping(value = "/addFeature", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Feature addFeature(@RequestBody Feature feature) throws Exception {

        feature.setLastUpdated(System.currentTimeMillis());
        feature.setEntityState(EntityState.ACTIVE);
        logger.info("Request:" + feature.toString());
        List<String> errors = validate(feature);
        Feature featureRes;

        if (errors != null && !errors.isEmpty()) {
            logger.error("Illegal Arguments with fields " + errors);
            throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
        } else {
            featureRes = featureManager.feature(feature);
        }

        logger.info("Response:" + featureRes.toString());
        return featureRes;
    }

    @RequestMapping(value = "getFeature/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Feature getFeatureById(@PathVariable("id") String id) throws Exception {
        logger.info("Request received for getting Feature for db_id: " + id);
        Feature featureRes = featureManager.getFeatureById(id);
        logger.info("Response:" + featureRes.toString());
        return featureRes;
    }

    @RequestMapping(value = {"getAllFeatures"}, method = RequestMethod.GET)
    @ResponseBody
    public GetFeatureRes getAllFeatures(@RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer limit) throws Exception {

        logger.info("Request received for getting All Feature");
        GetFeatureReq req = new GetFeatureReq(start, limit);
        List<Feature> features = featureManager.getFeatures(req);
        GetFeatureRes res = new GetFeatureRes(features, features.size());
        logger.info("Response:" + res.toString());
        return res;
    }

    @RequestMapping(value = "/updateFeature/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Feature updateFeature(@RequestBody Feature feature, @PathVariable("id") String id) throws Exception {
        feature.setLastUpdated(System.currentTimeMillis());
        feature.setId(id);
        logger.info("Request:" + feature.toString());
        Feature featureRes = featureManager.feature(feature);
        logger.info("Response:" + featureRes.toString());
        return featureRes;
    }

    @RequestMapping(value = "deleteFeature/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Feature deleteFeatureById(@PathVariable("id") String id) throws Exception {
        logger.info("Request received for disabling Feature for id: " + id);
        Feature featureRes = featureManager.getFeatureById(id);
        if (featureRes != null) {
            featureRes.setEntityState(EntityState.INACTIVE);
            featureMappingManager.deleteFeatureMappingsByFeatureId(id);
            featureRes = featureManager.feature(featureRes);
            logger.info("Response:" + featureRes.toString());
        }
        return featureRes;
    }

    public List<String> validate(Feature feature) {
        List<String> errors = new ArrayList<>();

        if (feature == null || feature.getName() == null) {
            errors.add(Feature.Constants.NAME);
        }

        String id = feature.getId();
        String name = feature.getName();

        logger.info("ID: " + id + " ,name: " + name);
        return errors;
    }
}
