package com.vedantu.platform.vToolsSecurity.request;

import com.vedantu.platform.vToolsSecurity.entity.VToolsSecurityPageAccess;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityGroupReq extends AbstractFrontEndReq {
    private String groupName;
    private List<String> pageNames;
    private String pageName;
    private Boolean pageAccess;
    private Boolean allPagesAccessible = false;
    private Boolean resetAllPagesAccess = false;
    private List<VToolsSecurityPageAccess> pages;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getPageNames() {
        return pageNames;
    }

    public void setPageNames(List<String> pageNames) {
        this.pageNames = pageNames;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public Boolean getPageAccess() {
        return pageAccess;
    }

    public void setPageAccess(Boolean pageAccess) {
        this.pageAccess = pageAccess;
    }

    public Boolean getAllPagesAccessible() {
        return allPagesAccessible;
    }

    public void setAllPagesAccessible(Boolean allPagesAccessible) {
        this.allPagesAccessible = allPagesAccessible;
    }

    public Boolean getResetAllPagesAccess() {
        return resetAllPagesAccess;
    }

    public void setResetAllPagesAccess(Boolean resetAllPagesAccess) {
        this.resetAllPagesAccess = resetAllPagesAccess;
    }

    public List<VToolsSecurityPageAccess> getPages() {
        return pages;
    }

    public void setPages(List<VToolsSecurityPageAccess> pages) {
        this.pages = pages;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(groupName)) {
            errors.add("groupName");
        }
        return errors;
    }

}
