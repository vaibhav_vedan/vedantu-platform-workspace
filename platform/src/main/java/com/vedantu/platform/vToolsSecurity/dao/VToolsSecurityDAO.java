package com.vedantu.platform.vToolsSecurity.dao;


import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.vToolsSecurity.entity.VTooslSecurity;
import com.vedantu.platform.vToolsSecurity.enums.VToolsSecuritycategory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 *
 * @author mnpk
 */

@Service
public class VToolsSecurityDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VToolsSecurityDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public VTooslSecurity getByAdminId(Long adminId) throws VException {
        if(adminId == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "admin Id not found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.ADMINID).in(adminId));
        VTooslSecurity vTooslSecurity = findOne(query,VTooslSecurity.class);
        if(vTooslSecurity != null){
            return vTooslSecurity;
        }
        return null;
    }

    public VTooslSecurity getByGroupName(String groupName) throws VException {
        if(StringUtils.isEmpty(groupName)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "groupName not found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.GROUPNAME).in(groupName));
        VTooslSecurity vTooslSecurity = findOne(query,VTooslSecurity.class);
        if(vTooslSecurity != null){
            return vTooslSecurity;
        }
        return null;
    }

    public void update(VTooslSecurity p){
        logger.info("Request : " + p);
        try {
            Assert.notNull(p);
            saveEntity(p);
            logger.info("updated id : " + p.getId());
        }catch(Exception ex){
            throw new RuntimeException("update : Error updating the VTooslSecurity - " + p.toString(),ex);
        }
    }

    public void create(VTooslSecurity p){
        try {
            if(p != null){
                saveEntity(p);
            }
        }
        catch (Exception ex){
            logger.error("Create : Error Creating VTooslSecurity : "+ex.getMessage());
        }

    }

    public boolean checkGroupExist(String groupName) throws VException {
        if(StringUtils.isEmpty(groupName)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "groupName not found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.GROUPNAME).in(groupName));
        query.fields().include(VTooslSecurity.Constants.GROUPNAME);
        VTooslSecurity vTooslSecurity = findOne(query,VTooslSecurity.class);
        if(vTooslSecurity != null){
            return true;
        }
        return false;
    }

    public VTooslSecurity getProjectionDataByAdminId(Long adminId) throws VException {
        VTooslSecurity vTooslSecurity = null;
        try {
            if(adminId == null){
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "admin Id not found");
            }
            Query query = new Query();
            query.addCriteria(Criteria.where(VTooslSecurity.Constants.ADMINID).in(adminId));
            query.fields().include(VTooslSecurity.Constants.ADMINID);
            query.fields().include(VTooslSecurity.Constants.PAGES);
            query.fields().include(VTooslSecurity.Constants.GROUPNAMES);
            query.fields().include(VTooslSecurity.Constants.ALLPAGESACCESSIBLE);
            vTooslSecurity = findOne(query,VTooslSecurity.class);
        }
        catch (Exception ex) {
            vTooslSecurity = null;
        }
        return vTooslSecurity;
    }

    public List<VTooslSecurity> getAllGroupsWithProjectionData() throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.CATEGORY).in(VToolsSecuritycategory.GROUP));
        query.fields().include(VTooslSecurity.Constants.GROUPNAME);
        query.fields().include(VTooslSecurity.Constants.PAGES);
        query.fields().include(VTooslSecurity.Constants.ALLPAGESACCESSIBLE);
        List<VTooslSecurity> vTooslSecurityList = runQuery(query,VTooslSecurity.class);
        return vTooslSecurityList;
    }

    public List<VTooslSecurity> getGroupsByGroupNames(List<String> groupNames) throws VException {
        if(ArrayUtils.isEmpty(groupNames)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "groupName not found");
        }
        logger.info("groupNames for fetching : " + groupNames);
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.CATEGORY).in(VToolsSecuritycategory.GROUP));
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.GROUPNAME).in(groupNames));
        List<VTooslSecurity> vTooslSecurityList = runQuery(query,VTooslSecurity.class);
        return vTooslSecurityList;
    }

    public List<VTooslSecurity> getAllGroupNames() throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.CATEGORY).in(VToolsSecuritycategory.GROUP));
        query.fields().include(VTooslSecurity.Constants.GROUPNAME);
        List<VTooslSecurity> vTooslSecurityList = runQuery(query,VTooslSecurity.class);
        return vTooslSecurityList;
    }

    public VTooslSecurity getAdminListByGroupName(String groupName) throws VException {
        if(StringUtils.isEmpty(groupName)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "groupName not found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.GROUPNAME).is(groupName));
        query.fields().include(VTooslSecurity.Constants.GROUPADMINIDS);
        VTooslSecurity vTooslSecurity = findOne(query,VTooslSecurity.class);
        return vTooslSecurity;
    }

    public List<VTooslSecurity> getByAdminIds(List<Long> adminIds) throws VException {
        if(ArrayUtils.isEmpty(adminIds)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "groupName not found");
        }
        logger.info("adminIds for fetching : " + adminIds);
        Query query = new Query();
        query.addCriteria(Criteria.where(VTooslSecurity.Constants.ADMINID).in(adminIds));
        List<VTooslSecurity> vTooslSecurityList = runQuery(query,VTooslSecurity.class);
        return vTooslSecurityList;
    }

}
