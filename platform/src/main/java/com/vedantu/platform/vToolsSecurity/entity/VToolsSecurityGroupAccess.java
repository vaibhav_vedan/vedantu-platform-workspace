package com.vedantu.platform.vToolsSecurity.entity;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityGroupAccess {
    private String groupName;
    private Boolean accessible =false;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getAccessible() {
        return accessible;
    }

    public void setAccessible(Boolean accessible) {
        this.accessible = accessible;
    }
}
