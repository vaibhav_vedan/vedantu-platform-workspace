package com.vedantu.platform.vToolsSecurity.entity;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityPageAccess {

    private String pageName;
    private List<String> components;
    private Boolean allComponentsAccessible = false;

    public VToolsSecurityPageAccess(){}

    public VToolsSecurityPageAccess(String pageName){
        this.pageName = pageName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<String> getComponents() {
        return components;
    }

    public void setComponents(List<String> components) {
        this.components = components;
    }

    public Boolean getAllComponentsAccessible() {
        return allComponentsAccessible;
    }

    public void setAllComponentsAccessible(Boolean allComponentsAccessible) {
        this.allComponentsAccessible = allComponentsAccessible;
    }

    @Override
    public String toString() {
        return "VToolsSecurityPageAccess { "
                + "pageName = " + pageName
                + ", components= "+ components
                + ", allComponentsAccessible= "+ allComponentsAccessible
                + " }";
    }

}
