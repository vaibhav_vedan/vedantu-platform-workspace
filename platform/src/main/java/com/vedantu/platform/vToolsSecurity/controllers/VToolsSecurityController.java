package com.vedantu.platform.vToolsSecurity.controllers;


import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.vToolsSecurity.managers.VToolsSecurityManager;
import com.vedantu.platform.vToolsSecurity.request.*;
import com.vedantu.platform.vToolsSecurity.response.VToolsSecurityAdminRes;
import com.vedantu.platform.vToolsSecurity.response.VToolsSecurityGroupRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mnpk
 */

@RestController
@RequestMapping("/vTools")
public class VToolsSecurityController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VToolsSecurityController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private VToolsSecurityManager vToolsSecurityManager;


    @RequestMapping(value = "/addEditAdminPageAccess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public VToolsSecurityAdminRes addEditAdminPageAccess(@RequestBody VToolsSecurityAdminReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.addEditAdminPageAccess(req);
    }

    @RequestMapping(value = "/getAccessPagesByAdminId", method = RequestMethod.GET)
    @ResponseBody
    public VToolsSecurityAdminRes getAccessPagesByAdminId(@RequestParam(value = "adminId", required = true) Long adminId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.getAccessPagesByAdminId(adminId);
    }

    @RequestMapping(value = "/createNewGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse createNewGroup(@RequestBody VToolsSecurityGroupReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.createGroup(req);
    }

    @RequestMapping(value = "/addRemoveAdminToGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse addRemoveAdminToGroup(@RequestParam(value = "groupName", required = true) String groupName,
                                                       @RequestParam(value = "adminId", required = true) Long adminId,
                                                       @RequestParam(value = "access", required = true) Boolean access) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.addRemoveAdminToGroup(groupName, adminId, access);
    }


    @RequestMapping(value = "/getAllGroupNames", method = RequestMethod.GET)
    public List<String> getAllGroupNames() throws VException{
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.getAllGroupNames();
    }

    @RequestMapping(value = "/getAdminIdssByGroupName", method = RequestMethod.GET)
    public List<Long> getAdminIdssByGroupName(@RequestParam(value = "groupName", required = true) String groupName) throws VException{
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.getAdminIdssByGroupName(groupName);
    }

    @RequestMapping(value = "/getAccessPagesByGroupName", method = RequestMethod.GET)
    public VToolsSecurityGroupRes getAccessPagesByGroupName(@RequestParam(value = "groupName", required = true) String groupName) throws VException{
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.getAccessPagesByGroupName(groupName);
    }

    @RequestMapping(value = "/addEditGroupPageAccess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public VToolsSecurityGroupRes addEditGroupPageAccess(@RequestBody VToolsSecurityGroupReq req) throws VException{
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.addEditGroupPageAccess(req);
    }

    @RequestMapping(value = "/addEditAdminComponentAccess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public VToolsSecurityAdminRes addEditAdminComponentAccess(@RequestBody VToolsSecurityAdminComponentReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.addEditAdminComponentAccess(req);
    }

    @RequestMapping(value = "/addEditGroupComponentAccess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public VToolsSecurityGroupRes addEditGroupComponentAccess(@RequestBody VToolsSecurityGroupComponentReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.addEditGroupComponentAccess(req);
    }

    @RequestMapping(value = "/addRemoveAdminToGroupForBulkUpdate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse addRemoveAdminToGroupForBulkUpdate(@RequestBody VToolsSecurityGroupPermissionToAdmin req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER, Role.STUDENT_CARE, Role.ADMIN_VOLT), Boolean.TRUE);
        return vToolsSecurityManager.addRemoveAdminToGroup(req.getGroupName(), req.getAdminId(), req.getAccess());
    }

}
