package com.vedantu.platform.vToolsSecurity.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityAdminComponentReq extends AbstractFrontEndReq {
    private Long adminId;
    private String pageName;
    private List<String> componentNames;
    private Boolean allComponentsAccessible;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<String> getComponentNames() {
        return componentNames;
    }

    public void setComponentNames(List<String> componentNames) {
        this.componentNames = componentNames;
    }

    public Boolean getAllComponentsAccessible() {
        return allComponentsAccessible;
    }

    public void setAllComponentsAccessible(Boolean allComponentsAccessible) {
        this.allComponentsAccessible = allComponentsAccessible;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if ( adminId == null ) {
            errors.add("adminId or groupName required");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "VToolsSecurityReq { "
                + "adminId = " + adminId
                + ", pageName = " + pageName
                + ", componentNames = "+ componentNames
                + ", allComponentsAccessible = "+ allComponentsAccessible
                + " }";
    }
}
