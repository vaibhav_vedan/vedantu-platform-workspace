package com.vedantu.platform.vToolsSecurity.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityAdminReq extends AbstractFrontEndReq {

    private Long adminId;
    private String pageName;
    private String groupName;
    private List<String> pageNames;
    private List<String> groupNames;
    private Boolean groupNamesChanged = false;
    private Boolean pageAccess;
    private Boolean groupAccess;
    private Boolean allPagesAccessible;
    private Boolean resetAllPagesAccess = false;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getPageNames() {
        return pageNames;
    }

    public void setPageNames(List<String> pageNames) {
        this.pageNames = pageNames;
    }

    public List<String> getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(List<String> groupNames) {
        this.groupNames = groupNames;
    }

    public Boolean getPageAccess() {
        return pageAccess;
    }

    public void setPageAccess(Boolean pageAccess) {
        this.pageAccess = pageAccess;
    }

    public Boolean getGroupAccess() {
        return groupAccess;
    }

    public void setGroupAccess(Boolean groupAccess) {
        this.groupAccess = groupAccess;
    }

    public Boolean getAllPagesAccessible() {
        return allPagesAccessible;
    }

    public void setAllPagesAccessible(Boolean allPagesAccessible) {
        this.allPagesAccessible = allPagesAccessible;
    }

    public Boolean getResetAllPagesAccess() {
        return resetAllPagesAccess;
    }

    public void setResetAllPagesAccess(Boolean resetAllPagesAccess) {
        this.resetAllPagesAccess = resetAllPagesAccess;
    }

    public Boolean getGroupNamesChanged() {
        return groupNamesChanged;
    }

    public void setGroupNamesChanged(Boolean groupNamesChanged) {
        this.groupNamesChanged = groupNamesChanged;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if ( adminId == null ) {
            errors.add("adminId required");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "VToolsSecurityReq { "
                + "adminId = " + adminId
                + ", pageName = " + pageName
                + ", groupName = "+ groupName
                + ", pageNames = "+ pageNames
                + ", groupNames = "+ groupNames
                + ", groupNamesChanged = "+ groupNamesChanged
                + ", pageAccess = "+ pageAccess
                + ", groupAccess = "+ groupAccess
                + ", allPagesAccessible = "+ allPagesAccessible
                + ", resetAllPagesAccess = "+ resetAllPagesAccess
                + " }";
    }

}
