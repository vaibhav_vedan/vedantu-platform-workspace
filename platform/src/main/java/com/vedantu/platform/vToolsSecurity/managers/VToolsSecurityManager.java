package com.vedantu.platform.vToolsSecurity.managers;



import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.vToolsSecurity.dao.VToolsSecurityDAO;
import com.vedantu.platform.vToolsSecurity.entity.VToolsSecurityGroupAccess;
import com.vedantu.platform.vToolsSecurity.entity.VToolsSecurityPageAccess;
import com.vedantu.platform.vToolsSecurity.entity.VTooslSecurity;
import com.vedantu.platform.vToolsSecurity.enums.VToolsSecuritycategory;
import com.vedantu.platform.vToolsSecurity.request.VToolsSecurityAdminComponentReq;
import com.vedantu.platform.vToolsSecurity.request.VToolsSecurityGroupComponentReq;
import com.vedantu.platform.vToolsSecurity.request.VToolsSecurityGroupReq;
import com.vedantu.platform.vToolsSecurity.request.VToolsSecurityAdminReq;
import com.vedantu.platform.vToolsSecurity.response.VToolsSecurityAdminRes;
import com.vedantu.platform.vToolsSecurity.response.VToolsSecurityGroupRes;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;

/**
 *
 * @author mnpk
 */

@Service
public class VToolsSecurityManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VToolsSecurityManager.class);

    @Autowired
    private UserManager userManager;

    @Autowired
    private VToolsSecurityDAO vToolsSecurityDAO;

    @Autowired
    private FosUtils fosUtils;



    public VToolsSecurityAdminRes addEditAdminPageAccess(VToolsSecurityAdminReq req) throws VException{
        req.verify();
        logger.info("VToolsSecurityManager VToolsSecurityReq : " + req);

        UserBasicInfo user =fosUtils.getUserBasicInfo(req.getAdminId(),true);
        if(user == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "admin not found");
        }
        logger.info("Admin Details : " + user);

        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getByAdminId(req.getAdminId());
        logger.info("VTooslSecurity getting from vToolsSecurityDAO : " + vTooslSecurity);

        VTooslSecurity vTooslSecurityForGroup =null;
        if(StringUtils.isNotEmpty(req.getGroupName())){
            vTooslSecurityForGroup = vToolsSecurityDAO.getByGroupName(req.getGroupName());
            if(vTooslSecurityForGroup == null){
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "group not exits with given groupName");
            }
        }

        if(vTooslSecurity == null){
            logger.info("if admin not exists in vTooslSecurity");
            VTooslSecurity vTooslSecurity1 = new VTooslSecurity();
            vTooslSecurity1.setAdminId(req.getAdminId());
            if(req.getAllPagesAccessible() != null && req.getAllPagesAccessible()){
                logger.info("all pages Accessible request");
                vTooslSecurity1.setAllPagesAccessible(req.getAllPagesAccessible());
            }
            if(StringUtils.isNotEmpty(req.getPageName()) && req.getPageAccess()){
                logger.info(req.getPageName() + " page access given to " + req.getAdminId());
                List<VToolsSecurityPageAccess> pageAccessList = new ArrayList<>();
                VToolsSecurityPageAccess vToolsSecurityPageAccess = new VToolsSecurityPageAccess(req.getPageName());
                pageAccessList.add(vToolsSecurityPageAccess);
                vTooslSecurity1.setPages(pageAccessList);
            }
            if( ArrayUtils.isNotEmpty(req.getPageNames()) && vTooslSecurity1.getAllPagesAccessible()){
                logger.info("bulk pages access given to " + req.getAdminId());
                List<VToolsSecurityPageAccess> pageAccessList2 = new ArrayList<>();
                for (String pName : req.getPageNames()){
                    VToolsSecurityPageAccess page1 = new VToolsSecurityPageAccess();
                    page1.setPageName(pName);
                    pageAccessList2.add(page1);
                }
                vTooslSecurity1.setPages(pageAccessList2);
            }
            if(StringUtils.isNotEmpty(req.getGroupName()) && req.getGroupAccess()){
                logger.info(req.getGroupName() + " group access given to " + req.getAdminId());
                List<Long> groupAdminIds = new ArrayList<>();
                if ( vTooslSecurityForGroup != null && ArrayUtils.isNotEmpty(vTooslSecurityForGroup.getGroupAdminIds())) {
                    groupAdminIds = vTooslSecurityForGroup.getGroupAdminIds();
                }
                groupAdminIds.add(req.getAdminId());
                vTooslSecurityForGroup.setGroupAdminIds(groupAdminIds);
                vToolsSecurityDAO.update(vTooslSecurityForGroup);
                List<String> groupNames = Arrays.asList(req.getGroupName());
                vTooslSecurity1.setGroupNames(groupNames);
            }
            if(ArrayUtils.isNotEmpty(req.getGroupNames())){
                List<VTooslSecurity> groupListByGroupNames = vToolsSecurityDAO.getGroupsByGroupNames(req.getGroupNames());
                if(groupListByGroupNames.size() != req.getGroupNames().size()){
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "one of the group not exits with given groupNames");
                }
                List<String> groupNamesListFromGroups = new ArrayList<>();
                for(VTooslSecurity vt : groupListByGroupNames){
                    groupNamesListFromGroups.add(vt.getGroupName());
                    List<Long> gaids = new ArrayList<>();
                    if(ArrayUtils.isNotEmpty(vt.getGroupAdminIds())){
                        gaids = vt.getGroupAdminIds();
                    }
                    if(gaids.indexOf(req.getAdminId()) == -1){
                        gaids.add(req.getAdminId());
                        vt.setGroupAdminIds(gaids);
                        vToolsSecurityDAO.update(vt);
                    }
                }
                vTooslSecurity1.setGroupNames(groupNamesListFromGroups);
            }
            vToolsSecurityDAO.create(vTooslSecurity1);
            return adminResconverter(vTooslSecurity1);
        }
        else {
            boolean prevAllPagesAccessible = vTooslSecurity.getAllPagesAccessible();

            if(req.getResetAllPagesAccess() != null && req.getResetAllPagesAccess()){
                logger.info(req.getAdminId() + " adminId VTool reset all page access");
                if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupNames())){
                    logger.info("removing admin Id from groups : " + vTooslSecurity.getGroupNames());
                    List<VTooslSecurity> groupsList = vToolsSecurityDAO.getGroupsByGroupNames(vTooslSecurity.getGroupNames());
                    if(ArrayUtils.isNotEmpty(groupsList)){
                        for(VTooslSecurity eachGroup : groupsList){
                            if(ArrayUtils.isNotEmpty(eachGroup.getGroupAdminIds())){
                                List<Long> adminIdsList = new ArrayList<>();
                                for(Long aid : eachGroup.getGroupAdminIds()){
                                    if(!aid.equals(vTooslSecurity.getAdminId())){
                                        adminIdsList.add(aid);
                                    }
                                }
                                eachGroup.setGroupAdminIds(adminIdsList);
                                vToolsSecurityDAO.update(eachGroup);
                                logger.info(vTooslSecurity.getAdminId() + " adminId removed from groupName : " + eachGroup.getGroupName());
                            }
                        }
                    }
                }
                vTooslSecurity.setPages(new ArrayList<>());
                logger.info(vTooslSecurity.getAdminId() + " admin all page access removed ");
                vTooslSecurity.setGroupNames(new ArrayList<>());
                vTooslSecurity.setAllPagesAccessible(false);
            }
            if(req.getAllPagesAccessible() != null && req.getAllPagesAccessible() != vTooslSecurity.getAllPagesAccessible()){
                logger.info("isAllPagesAccessible changed to : "+ req.getAllPagesAccessible() + ", from : " + vTooslSecurity.getAllPagesAccessible());
                vTooslSecurity.setAllPagesAccessible(req.getAllPagesAccessible());
            }
            if(StringUtils.isNotEmpty(req.getPageName())){
                logger.info("request PageName : " + req.getPageName());
                List<VToolsSecurityPageAccess> pages = new ArrayList<>();
                if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
                    pages = vTooslSecurity.getPages();
                }
                logger.info("vTooslSecurity pages : " + pages);
                if(req.getPageAccess() != null && req.getPageAccess()){
                    List<String> pageN = new ArrayList<>();
                    for(VToolsSecurityPageAccess pp : pages){
                        pageN.add(pp.getPageName());
                    }
                    if(pageN.indexOf(req.getPageName()) == -1){
                        logger.info(req.getPageName() + " pageAccess given to " + req.getAdminId());
                        VToolsSecurityPageAccess vToolsSecurityPageAccess = new VToolsSecurityPageAccess(req.getPageName());
                        pages.add(vToolsSecurityPageAccess);
                    }
                }
                if(req.getPageAccess() != null && !req.getPageAccess()){
                    List<VToolsSecurityPageAccess> afterRemovingAccessPages = new ArrayList<>();
                    logger.info(req.getPageName() + " pageAccess removing from " + req.getAdminId());
                    for (VToolsSecurityPageAccess pageAccess : pages){
                        logger.info("page : " + pageAccess);
                        if(!req.getPageName().equals(pageAccess.getPageName())){
                            afterRemovingAccessPages.add(pageAccess);
                        }
                    }
                    pages = afterRemovingAccessPages;
                }
                vTooslSecurity.setPages(pages);
            }
            if(ArrayUtils.isNotEmpty(req.getPageNames()) && prevAllPagesAccessible){
                logger.info("this condition used when fist given all pages access then remove one page access");
                List<VToolsSecurityPageAccess> pageAccessList = new ArrayList<>();
                for (String pName : req.getPageNames()){
                    VToolsSecurityPageAccess page1 = new VToolsSecurityPageAccess(pName);
                    pageAccessList.add(page1);
                }
                vTooslSecurity.setPages(pageAccessList);
            }
            if(StringUtils.isNotEmpty(req.getGroupName())){
                logger.info("request GroupName : " + req.getGroupName());
                //admin document
                List<String> groupNames = new ArrayList<>();
                if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupNames())){
                    groupNames = vTooslSecurity.getGroupNames();
                }
                //group document
                List<Long> groupAdminIds = new ArrayList<>();
                if ( vTooslSecurityForGroup != null && ArrayUtils.isNotEmpty(vTooslSecurityForGroup.getGroupAdminIds())) {
                    groupAdminIds = vTooslSecurityForGroup.getGroupAdminIds();
                }
                //group access given
                if(req.getGroupAccess() != null && req.getGroupAccess()){
                    logger.info(req.getGroupName() + " groupAccess given to " + req.getAdminId());
                    if(groupNames.indexOf(req.getGroupName()) ==-1){
                        groupNames.add(req.getGroupName());
                        logger.info(req.getGroupName() + " groupName added into admin groups List : " + groupNames.toString());
                    }
                    if(groupAdminIds.indexOf(req.getAdminId()) == -1){
                        groupAdminIds.add(req.getAdminId());
                        logger.info(req.getAdminId() + " adminId added into " + req.getGroupName() + " GroupAdminIds list : " + groupAdminIds.toString());
                    }
                }
                //group access removed
                if(req.getGroupAccess() != null && !req.getGroupAccess()){
                    List<String> afterRemovingGroupNameList = new ArrayList<>();
                    logger.info(req.getGroupName() + " groupAccess remove from " + req.getAdminId());
                    for(String groupName : groupNames){
                        if(!req.getGroupName().equals(groupName)){
                            afterRemovingGroupNameList.add(groupName);
                        }
                        groupNames = afterRemovingGroupNameList;
                    }
                    List<Long> afterRemovingGroupAdminIdsLis = new ArrayList<>();
                    for(Long adminId : groupAdminIds){
                        if(!adminId.equals(req.getAdminId())){
                            afterRemovingGroupAdminIdsLis.add(adminId);
                        }
                    }
                    groupAdminIds = afterRemovingGroupAdminIdsLis;
                }
                vTooslSecurity.setGroupNames(groupNames);
                if ( vTooslSecurityForGroup != null) {
                    vTooslSecurityForGroup.setGroupAdminIds(groupAdminIds);
                 }
                vToolsSecurityDAO.update(vTooslSecurityForGroup);
            }
            //bulk groups updated
            if(req.getGroupNamesChanged()){
                List<String> newGroups = new ArrayList<>();
                List<String> removedGroups = new ArrayList<>();

                if(ArrayUtils.isNotEmpty(req.getGroupNames())){
                    List<String> updatedGroups = new ArrayList<>();

                    List<String> existedGroups = new ArrayList<>();
                    if(vTooslSecurity.getGroupNames() != null && ArrayUtils.isNotEmpty(vTooslSecurity.getGroupNames())){
                        existedGroups = vTooslSecurity.getGroupNames();
                    }
                    List<String> modifyingGroups = req.getGroupNames();

                    if(ArrayUtils.isNotEmpty(existedGroups)){
                        for(String rg : existedGroups){
                            if(modifyingGroups.indexOf(rg) == -1){
                                removedGroups.add(rg);
                            }else {
                                updatedGroups.add(rg);
                            }
                        }
                    }

                    if(ArrayUtils.isNotEmpty(modifyingGroups)){
                        for(String ng : modifyingGroups){
                            if(existedGroups.indexOf(ng) == -1){
                                newGroups.add(ng);
                                updatedGroups.add(ng);
                            }
                        }
                    }
                    logger.info("existedGroups : " + existedGroups + "modifyingGroups : " + modifyingGroups + "removedGroups : " + removedGroups + "newGroups : " + newGroups);
                    vTooslSecurity.setGroupNames(updatedGroups);
                }
                else if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupNames())) {
                    removedGroups =  vTooslSecurity.getGroupNames();
                    vTooslSecurity.setGroupNames(new ArrayList<>());
                }

                if(ArrayUtils.isNotEmpty(newGroups)){
                    List<VTooslSecurity> vTooslSecurityListByNewGroups = vToolsSecurityDAO.getGroupsByGroupNames(newGroups);
                    if(newGroups.size() != vTooslSecurityListByNewGroups.size()){
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "one of the group not exits with given groupNames");
                    }
                    for (VTooslSecurity eachGrp : vTooslSecurityListByNewGroups){
                        List<Long> eachGroupaidsList = new ArrayList<>();
                        if(ArrayUtils.isNotEmpty(eachGrp.getGroupAdminIds())){
                            eachGroupaidsList = eachGrp.getGroupAdminIds();
                        }
                        if(eachGroupaidsList.indexOf(req.getAdminId()) == -1){
                            eachGroupaidsList.add(req.getAdminId());
                            eachGrp.setGroupAdminIds(eachGroupaidsList);
                            vToolsSecurityDAO.update(eachGrp);
                        }
                    }
                }

                if(ArrayUtils.isNotEmpty(removedGroups)){
                    List<VTooslSecurity> vTooslSecurityListByremovedGroups = vToolsSecurityDAO.getGroupsByGroupNames(removedGroups);
                    logger.info("vTooslSecurityListByremovedGroups : " + vTooslSecurityListByremovedGroups);
                    if(ArrayUtils.isNotEmpty(vTooslSecurityListByremovedGroups)){
                        for (VTooslSecurity v : vTooslSecurityListByremovedGroups){
                            List<Long> adminIdsListFromremovedGroups = new ArrayList<>();
                            if(ArrayUtils.isNotEmpty(v.getGroupAdminIds())){
                                adminIdsListFromremovedGroups = v.getGroupAdminIds();
                            }
                            logger.info("before removing id from list : "+ adminIdsListFromremovedGroups);
                            List<Long> afterAdminIdRemovedList = new ArrayList<>();
                            for (Long id : adminIdsListFromremovedGroups){
                                if(!id.equals(req.getAdminId())){
                                    afterAdminIdRemovedList.add(id);
                                }
                            }
                            logger.info("after removing id from list : "+ afterAdminIdRemovedList);
                            v.setGroupAdminIds(afterAdminIdRemovedList);
                            logger.info(v);
                            vToolsSecurityDAO.update(v);
                        }
                    }
                }

            }

            vToolsSecurityDAO.update(vTooslSecurity);
            return adminResconverter(vTooslSecurity);
        }
    }

    public VToolsSecurityAdminRes getAccessPagesByAdminId(Long adminId) throws VException{
        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getProjectionDataByAdminId(adminId);
        return adminResconverter(vTooslSecurity);
    }

    public VToolsSecurityAdminRes converter(VTooslSecurity vTooslSecurity) throws VException{
        if(vTooslSecurity == null){
           vTooslSecurity = new VTooslSecurity();
        }

        Boolean allPagesAccessible = vTooslSecurity.getAllPagesAccessible();
        logger.info("vTooslSecurity allPagesAccessible : " + allPagesAccessible);

        List<VToolsSecurityPageAccess> pages = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
            pages = vTooslSecurity.getPages();
        }

        logger.info("vTooslSecurity pages : " + pages);

        List<String> pageNamesForChecking = new ArrayList<>();
        for(VToolsSecurityPageAccess pageAccess : pages){
            if(StringUtils.isNotEmpty(pageAccess.getPageName())){
                pageNamesForChecking.add(pageAccess.getPageName());
            }
        }
        logger.info("vTooslSecurity pageNamesForChecking : " + pageNamesForChecking);

        List<String> groupNames = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupNames())){
            groupNames = vTooslSecurity.getGroupNames();
        }
        logger.info("vTooslSecurity groupNames : " + groupNames);

        List<VToolsSecurityGroupAccess> groupNamesRes = new ArrayList<>();
        List<VTooslSecurity> vTooslSecurityGroupList = vToolsSecurityDAO.getAllGroupsWithProjectionData();
        logger.info("all groups in VTooslSecurity : " + vTooslSecurityGroupList);

        for( VTooslSecurity vToolSecurity : vTooslSecurityGroupList){
            if( vToolSecurity.getAllPagesAccessible() != null && groupNames.indexOf(vToolSecurity.getGroupName()) > -1 && vToolSecurity.getAllPagesAccessible()){
                 allPagesAccessible = vToolSecurity.getAllPagesAccessible();
            }
            if( StringUtils.isNotEmpty(vToolSecurity.getGroupName())){
                VToolsSecurityGroupAccess access = new VToolsSecurityGroupAccess();
                if( groupNames.indexOf(vToolSecurity.getGroupName()) > -1){
                    access.setAccessible(true);
                }
                else {
                    access.setAccessible(false);
                }
                access.setGroupName( vToolSecurity.getGroupName() );
                groupNamesRes.add(access);
            }

            if(ArrayUtils.isNotEmpty(vToolSecurity.getPages()) && groupNames.indexOf(vToolSecurity.getGroupName()) >= 0){
                for ( VToolsSecurityPageAccess page : vToolSecurity.getPages()){
                    if(pageNamesForChecking.indexOf(page.getPageName()) == -1){
                        pages.add(page);
                    }
                }
            }
        }

        return new VToolsSecurityAdminRes(vTooslSecurity.getAdminId(),pages,groupNamesRes, allPagesAccessible);
    }

    public PlatformBasicResponse createGroup(VToolsSecurityGroupReq req) throws  VException{
        req.verify();
        logger.info("Create a new Group for VTool page access  VToolsSecurityGroupReq : " + req);
        if(StringUtils.isNotEmpty(req.getGroupName())){
            if(vToolsSecurityDAO.checkGroupExist(req.getGroupName())){
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Group name already existed.");
            }
            VTooslSecurity vTooslSecurity = new VTooslSecurity();
            if(ArrayUtils.isNotEmpty(req.getPages())){
                vTooslSecurity.setPages(req.getPages());
            }
            vTooslSecurity.setGroupName(req.getGroupName());
//            if(req.getAllPagesAccessible() != null && req.getAllPagesAccessible()){
//                vTooslSecurity.setAllPagesAccessible(true);
//            }
//            if(ArrayUtils.isNotEmpty(req.getPageNames())){
//                List<VToolsSecurityPageAccess> pages = new ArrayList<>();
//                for(String page : req.getPageNames()){
//                    VToolsSecurityPageAccess vToolsSecurityPageAccess = new VToolsSecurityPageAccess();
//                    vToolsSecurityPageAccess.setPageName(page);
//                    pages.add(vToolsSecurityPageAccess);
//                }
//                vTooslSecurity.setPages(pages);
//            }
            vTooslSecurity.setCategory(VToolsSecuritycategory.GROUP);
            vToolsSecurityDAO.create(vTooslSecurity);
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse addRemoveAdminToGroup(String groupName, Long adminId, Boolean access ) throws VException {

        UserBasicInfo user =fosUtils.getUserBasicInfo(adminId,true);
        if(user == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "admin not found");
        }
        logger.info("Admin Details : " + user);

        VTooslSecurity vTooslSecurityFromGroupName = vToolsSecurityDAO.getByGroupName(groupName);
        if(vTooslSecurityFromGroupName == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "groupName Not Found");
        }
        logger.info("Group Details : " + vTooslSecurityFromGroupName);

        VTooslSecurity vTooslSecurityFromadminId = vToolsSecurityDAO.getByAdminId(adminId);
        logger.info("admin side VTooslSecurity : " + vTooslSecurityFromadminId);

        if(vTooslSecurityFromadminId == null && access){
            VTooslSecurity vTooslSecurity = new VTooslSecurity();
            vTooslSecurity.setAdminId(adminId);
            List<String> groupsList = Arrays.asList(groupName);
            vTooslSecurity.setGroupNames(groupsList);
            vToolsSecurityDAO.create(vTooslSecurity);
        }else {
            List<String> groupsList = new ArrayList<>();
            if (vTooslSecurityFromadminId != null  && ArrayUtils.isNotEmpty(vTooslSecurityFromadminId.getGroupNames())) {
                groupsList = vTooslSecurityFromadminId.getGroupNames();
            }
            logger.info("admin group Names List : " +groupsList );

            if(access){
                logger.info(adminId + " added into the "+ groupName);
                if(groupsList.indexOf(groupName) == -1){
                    groupsList.add(groupName);
                }
            }
            if (!access){
                logger.info(adminId + " removed from the "+ groupName);
                List<String> afterRemoveGroupNameList = new ArrayList<>();
                for(String gName : groupsList){
                    if(!gName.equals(groupName)){
                        afterRemoveGroupNameList.add(gName);
                    }
                }
                groupsList = afterRemoveGroupNameList;
            }
            if (vTooslSecurityFromadminId != null) {
                vTooslSecurityFromadminId.setGroupNames(groupsList);
            }
            logger.info("updating group document : " + vTooslSecurityFromadminId);
            vToolsSecurityDAO.update(vTooslSecurityFromadminId);
        }

        //this applicable when the new adminId added into group
        List<Long> adminIdsList = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurityFromGroupName.getGroupAdminIds())){
            adminIdsList = vTooslSecurityFromGroupName.getGroupAdminIds();
        }
        logger.info("group adminIds list : " + adminIdsList);

        if(access){
            logger.info(adminId + " added into the "+ groupName);
            if(adminIdsList.indexOf(adminId) == -1){
                adminIdsList.add(adminId);
            }
        }
        if (!access){
            logger.info(adminId + " removed from the "+ groupName);
            List<Long> tempList = new ArrayList<>();
            for(Long id : adminIdsList){
                if(!id.equals(adminId)){
                    tempList.add(id);
                }
            }
            adminIdsList = tempList;
        }
        vTooslSecurityFromGroupName.setGroupAdminIds(adminIdsList);
        logger.info("updating admin collection : "+vTooslSecurityFromGroupName);
        vToolsSecurityDAO.update(vTooslSecurityFromGroupName);

        return new PlatformBasicResponse();
    }

    public VToolsSecurityGroupRes addEditGroupPageAccess(VToolsSecurityGroupReq req) throws VException {
        logger.info(req);
        req.verify();
        logger.info("getting the page Access details By group Name : "+ req.getGroupName());
        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getByGroupName(req.getGroupName());
        if(vTooslSecurity == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "group not exists with groupName : " + req.getGroupName());
        }
        logger.info("vTooslSecurity : "+ vTooslSecurity);
        if(req.getPageAccess() != null && StringUtils.isNotEmpty(req.getPageName())){
            List<VToolsSecurityPageAccess> pages = new ArrayList<>();
            if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
                pages = vTooslSecurity.getPages();
            }
            logger.info("pages : " + pages);
            List<String> pageNames = new ArrayList<>();
            for(VToolsSecurityPageAccess page : pages){
                pageNames.add(page.getPageName());
            }
            logger.info("pageNames : " + pageNames);
            if(req.getPageAccess()  && pageNames.indexOf(req.getPageName()) == -1){
                logger.info(req.getPageName() + " adding into group : " + vTooslSecurity.getGroupName());
                VToolsSecurityPageAccess pName = new VToolsSecurityPageAccess(req.getPageName());
                pages.add(pName);
            }
            if(!req.getPageAccess()  && pageNames.indexOf(req.getPageName()) > -1){
                logger.info(req.getPageName() + " removing from group : " + vTooslSecurity.getGroupName());
                List<VToolsSecurityPageAccess> tPages = new ArrayList<>();
                for(VToolsSecurityPageAccess rpage : pages){
                    if(!req.getPageName().equals(rpage.getPageName())){
                        tPages.add(rpage);
                    }
                }
                pages = tPages;
            }
            vTooslSecurity.setPages(pages);
        }
        if(req.getAllPagesAccessible() != null){
            vTooslSecurity.setAllPagesAccessible(req.getAllPagesAccessible());
        }
        if(req.getResetAllPagesAccess()){
            if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
                vTooslSecurity.setPages(new ArrayList<>());
            }
        }
        //for bulk page insert
        if(ArrayUtils.isNotEmpty(req.getPageNames())){
            List<VToolsSecurityPageAccess> prevPages = new ArrayList<>();
            if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
                prevPages = vTooslSecurity.getPages();
            }
            List<String> pNames = new ArrayList<>();
            for(VToolsSecurityPageAccess page : prevPages){
                pNames.add(page.getPageName());
            }
            for(String pageName : req.getPageNames()){
                if(pNames.indexOf(pageName) == -1){
                    VToolsSecurityPageAccess pageAccess = new VToolsSecurityPageAccess(pageName);
                    prevPages.add(pageAccess);
                }
            }
            vTooslSecurity.setPages(prevPages);
        }
        vToolsSecurityDAO.update(vTooslSecurity);
        return groupResConverter(vTooslSecurity);
    }

    public VToolsSecurityGroupRes getAccessPagesByGroupName(String groupName) throws VException{
        logger.info("getting the page Access details By group Name : "+groupName);
        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getByGroupName(groupName);
        logger.info("vTooslSecurity : "+ vTooslSecurity);
        return groupResConverter(vTooslSecurity);
    }

    public VToolsSecurityGroupRes groupResConverter(VTooslSecurity vTooslSecurity) throws VException{
        logger.info("vTooslSecurity : " + vTooslSecurity);
        if(vTooslSecurity == null){
            return new VToolsSecurityGroupRes();
        }
        String groupName = vTooslSecurity.getGroupName();
        logger.info("groupName : " + groupName);
        Boolean allPagesAccessible = false;
        if(vTooslSecurity.getAllPagesAccessible() != null){
            allPagesAccessible = vTooslSecurity.getAllPagesAccessible();
        }
        logger.info("allPagesAccessible : " + allPagesAccessible);
        List<VToolsSecurityPageAccess> pages = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
            pages = vTooslSecurity.getPages();
        }
        logger.info("pages : " + pages);
        List<Long> adminIds = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupAdminIds())){
            adminIds = vTooslSecurity.getGroupAdminIds();
        }
        logger.info("adminIds : " + adminIds);
        List<Map<Long, String>> groupAdminList = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupAdminIds())){
            Map<Long, User> getUserInfosMap = fosUtils.getUserInfosMap(vTooslSecurity.getGroupAdminIds(),true);
            for(Long id : vTooslSecurity.getGroupAdminIds()){
                Map<Long, String> idEmailMap = new HashMap<>();
                idEmailMap.put(id, getUserInfosMap.get(id).getEmail());
                groupAdminList.add(idEmailMap);
            }
        }
        return new VToolsSecurityGroupRes( groupName, pages, adminIds, allPagesAccessible,groupAdminList);
    }

    public List<String> getAllGroupNames() throws VException{
        List<String>  groupNames = new ArrayList<>();
        List<VTooslSecurity> vTooslSecurityList = vToolsSecurityDAO.getAllGroupNames();
        if( ArrayUtils.isNotEmpty(vTooslSecurityList)){
            for(VTooslSecurity vTooslSecurity : vTooslSecurityList){
                groupNames.add(vTooslSecurity.getGroupName());
            }
        }
        return groupNames;
    }

    public List<Long> getAdminIdssByGroupName(String groupName) throws VException{
        List<Long>  adminIds = new ArrayList<>();
        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getAdminListByGroupName(groupName);
        if(vTooslSecurity != null && ArrayUtils.isNotEmpty(vTooslSecurity.getGroupAdminIds())){
            adminIds = vTooslSecurity.getGroupAdminIds();
        }
        return adminIds;
    }

    public VToolsSecurityAdminRes addEditAdminComponentAccess(VToolsSecurityAdminComponentReq req) throws VException{
        req.verify();
        logger.info("VToolsSecurityManager VToolsSecurityComponentReq : " + req);
        UserBasicInfo user =fosUtils.getUserBasicInfo(req.getAdminId(),true);
        if(user == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "admin not found");
        }
        logger.info("Admin Details : " + user);
        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getByAdminId(req.getAdminId());
        logger.info("VTooslSecurity getting from vToolsSecurityDAO : " + vTooslSecurity);

        if(vTooslSecurity != null) {
            List<VToolsSecurityPageAccess> pages = new ArrayList<>();
            if(vTooslSecurity.getPages() != null && ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
                pages = vTooslSecurity.getPages();
            }
            List<VToolsSecurityPageAccess> newPages = new ArrayList<>();
            boolean isPageExists = false;
            for (VToolsSecurityPageAccess page : pages) {
                if (page.getPageName().equals(req.getPageName())) {
                    if (req.getAllComponentsAccessible() != null && req.getAllComponentsAccessible() != page.getAllComponentsAccessible()) {
                        page.setAllComponentsAccessible(req.getAllComponentsAccessible());
                    }
                    if (ArrayUtils.isNotEmpty(req.getComponentNames())) {
                        page.setComponents(req.getComponentNames());
                    } else {
                        page.setComponents(new ArrayList<>());
                    }
                    isPageExists = true;
                }
                newPages.add(page);
            }
            if (!isPageExists) {
                VToolsSecurityPageAccess pageCreateIfGroupAccess = new VToolsSecurityPageAccess();
                pageCreateIfGroupAccess.setPageName(req.getPageName());
                if (req.getAllComponentsAccessible() != null) {
                    pageCreateIfGroupAccess.setAllComponentsAccessible(req.getAllComponentsAccessible());
                }
                if (ArrayUtils.isNotEmpty(req.getComponentNames())) {
                    pageCreateIfGroupAccess.setComponents(req.getComponentNames());
                } else {
                    pageCreateIfGroupAccess.setComponents(new ArrayList<>());
                }
                newPages.add(pageCreateIfGroupAccess);
            }
            vTooslSecurity.setPages(newPages);
            vToolsSecurityDAO.update(vTooslSecurity);
        }

        return adminResconverter(vTooslSecurity);
    }

    public VToolsSecurityAdminRes adminResconverter(VTooslSecurity vTooslSecurity) throws VException{
        if(vTooslSecurity == null){
            vTooslSecurity = new VTooslSecurity();
        }

        Boolean allPagesAccessible = vTooslSecurity.getAllPagesAccessible();
        logger.info("vTooslSecurity allPagesAccessible : " + allPagesAccessible);

        Map<String, VToolsSecurityPageAccess> pageNameAndListMap = new HashMap<>();

        List<VToolsSecurityPageAccess> pages = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getPages())){
            pages = vTooslSecurity.getPages();
        }

        for(VToolsSecurityPageAccess p : pages){
            if(StringUtils.isNotEmpty(p.getPageName())){
                pageNameAndListMap.put(p.getPageName(),p);
            }
        }

        List<String> groupNames = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(vTooslSecurity.getGroupNames())){
            groupNames = vTooslSecurity.getGroupNames();
        }
        logger.info("vTooslSecurity groupNames : " + groupNames);

        List<VToolsSecurityGroupAccess> groupNamesRes = new ArrayList<>();
        List<VTooslSecurity> vTooslSecurityGroupList = vToolsSecurityDAO.getAllGroupsWithProjectionData();
        logger.info("all groups in VTooslSecurity : " + vTooslSecurityGroupList);

        for( VTooslSecurity vToolSecurity : vTooslSecurityGroupList){
            if( vToolSecurity.getAllPagesAccessible() != null && groupNames.indexOf(vToolSecurity.getGroupName()) > -1 && vToolSecurity.getAllPagesAccessible()){
                allPagesAccessible = true;
            }
            if( StringUtils.isNotEmpty(vToolSecurity.getGroupName())){
                VToolsSecurityGroupAccess access = new VToolsSecurityGroupAccess();
                if( groupNames.indexOf(vToolSecurity.getGroupName()) > -1){
                    access.setAccessible(true);
                }
                else {
                    access.setAccessible(false);
                }
                access.setGroupName( vToolSecurity.getGroupName() );
                groupNamesRes.add(access);
            }
            if(ArrayUtils.isNotEmpty(vToolSecurity.getPages()) && groupNames.indexOf(vToolSecurity.getGroupName()) >= 0){
                for ( VToolsSecurityPageAccess page : vToolSecurity.getPages()){
                    VToolsSecurityPageAccess userPage = pageNameAndListMap.get(page.getPageName());
                    if(userPage !=  null){
                        if(userPage.getAllComponentsAccessible() != null){
                            if(!userPage.getAllComponentsAccessible() && page.getAllComponentsAccessible() != null && page.getAllComponentsAccessible() == true ){
                                userPage.setAllComponentsAccessible(true);
                            }
                        }
                        else if (page.getAllComponentsAccessible() != null){
                            userPage.setAllComponentsAccessible(page.getAllComponentsAccessible());
                        }

                        List<String> userPageComponents = new ArrayList<>();
                        if(ArrayUtils.isNotEmpty(userPage.getComponents())){
                            userPageComponents = userPage.getComponents();
                        }
                        if(ArrayUtils.isNotEmpty(page.getComponents())){
                            for(String cN : page.getComponents()){
                                if(!userPageComponents.contains(cN)){
                                    userPageComponents.add(cN);
                                }
                            }
                        }
                        userPage.setComponents(userPageComponents);
                        pageNameAndListMap.put(userPage.getPageName(),userPage);
                    }
                    else {
                        pageNameAndListMap.put(page.getPageName(),page);
                    }
                }
            }

        }

        List<VToolsSecurityPageAccess> respPages = new ArrayList<>();
        pageNameAndListMap.forEach((key,value) ->{
            respPages.add(value);
        });

        return new VToolsSecurityAdminRes(vTooslSecurity.getAdminId(),respPages,groupNamesRes, allPagesAccessible);
    }

    public VToolsSecurityGroupRes addEditGroupComponentAccess(VToolsSecurityGroupComponentReq req) throws VException {
        req.verify();
        logger.info("getting the page Access details By group Name : "+ req.getGroupName());
        VTooslSecurity vTooslSecurity = vToolsSecurityDAO.getByGroupName(req.getGroupName());
        if(vTooslSecurity == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "group not exists with groupName : " + req.getGroupName());
        }
        logger.info("vTooslSecurity : "+ vTooslSecurity);

        if(vTooslSecurity != null) {
            List<VToolsSecurityPageAccess> pages = vTooslSecurity.getPages();
            List<VToolsSecurityPageAccess> newPages = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(pages)) {
                for (VToolsSecurityPageAccess page : pages) {
                    if (page.getPageName().equals(req.getPageName())) {
                        if (req.getAllComponentsAccessible() != null && req.getAllComponentsAccessible() != page.getAllComponentsAccessible()) {
                            page.setAllComponentsAccessible(req.getAllComponentsAccessible());
                        }
                        if(ArrayUtils.isNotEmpty(req.getComponentNames())){
                            page.setComponents(req.getComponentNames());
                        }else {
                            page.setComponents(new ArrayList<>());
                        }
                    }
                    newPages.add(page);
                }
                vTooslSecurity.setPages(newPages);
                vToolsSecurityDAO.update(vTooslSecurity);
            }
        }

        return groupResConverter(vTooslSecurity);
    }



}
