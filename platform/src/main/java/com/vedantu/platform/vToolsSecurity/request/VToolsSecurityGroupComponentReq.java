package com.vedantu.platform.vToolsSecurity.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityGroupComponentReq extends AbstractFrontEndReq {
    private String groupName;
    private String pageName;
    private List<String> componentNames;
    private Boolean allComponentsAccessible;


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<String> getComponentNames() {
        return componentNames;
    }

    public void setComponentNames(List<String> componentNames) {
        this.componentNames = componentNames;
    }

    public Boolean getAllComponentsAccessible() {
        return allComponentsAccessible;
    }

    public void setAllComponentsAccessible(Boolean allComponentsAccessible) {
        this.allComponentsAccessible = allComponentsAccessible;
    }

    @Override
    public String toString() {
        return "VToolsSecurityReq { "
                + "groupName = " + groupName
                + ", pageName = " + pageName
                + ", componentNames = "+ componentNames
                + ", allComponentsAccessible = "+ allComponentsAccessible
                + " }";
    }
}
