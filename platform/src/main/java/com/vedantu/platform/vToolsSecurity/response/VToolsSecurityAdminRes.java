package com.vedantu.platform.vToolsSecurity.response;

import com.vedantu.platform.vToolsSecurity.entity.VToolsSecurityGroupAccess;
import com.vedantu.platform.vToolsSecurity.entity.VToolsSecurityPageAccess;
import com.vedantu.platform.vToolsSecurity.entity.VTooslSecurity;
import com.vedantu.util.fos.response.AbstractRes;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityAdminRes extends AbstractRes {

    private Long adminId;
    private List<VToolsSecurityPageAccess> pages;
    private List<VToolsSecurityGroupAccess> groupNames;
    private Boolean allPagesAccessible = false;


    public VToolsSecurityAdminRes(){
        super();
    }

    public VToolsSecurityAdminRes(VTooslSecurity vTooslSecurity){
        super();
        this.adminId = vTooslSecurity.getAdminId();
        this.pages = vTooslSecurity.getPages();
        this.allPagesAccessible = vTooslSecurity.getAllPagesAccessible();
    }

    public VToolsSecurityAdminRes(Long adminId, List<VToolsSecurityPageAccess> pages, List<VToolsSecurityGroupAccess> groupNames, Boolean allPagesAccessible){
        super();
        this.adminId = adminId;
        this.pages = pages;
        this.groupNames = groupNames;
        this.allPagesAccessible = allPagesAccessible;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public List<VToolsSecurityPageAccess> getPages() {
        return pages;
    }

    public void setPages(List<VToolsSecurityPageAccess> pages) {
        this.pages = pages;
    }

    public List<VToolsSecurityGroupAccess> getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(List<VToolsSecurityGroupAccess> groupNames) {
        this.groupNames = groupNames;
    }

    public Boolean getAllPagesAccessible() {
        return allPagesAccessible;
    }

    public void setAllPagesAccessible(Boolean allPagesAccessible) {
        this.allPagesAccessible = allPagesAccessible;
    }

    @Override
    public String toString() {
        return "VToolsSecurityRes { "
                + "adminId= " + adminId
                + ", pages= "+ pages
                + ", groupNames= "+ groupNames
                + ", allPagesAccessible= "+ allPagesAccessible
                + " }";
    }


}
