package com.vedantu.platform.vToolsSecurity.response;

import com.vedantu.platform.vToolsSecurity.entity.VToolsSecurityPageAccess;
import com.vedantu.util.fos.response.AbstractRes;

import java.util.List;
import java.util.Map;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityGroupRes extends AbstractRes {

    private String groupName;
    private List<VToolsSecurityPageAccess> pages;
    private List<Long> adminIds;
    private Boolean allPagesAccessible = false;
    private List<Map<Long, String>> groupAdminList;

    public VToolsSecurityGroupRes(){
        super();
    }

    public VToolsSecurityGroupRes(String groupName, List<VToolsSecurityPageAccess> pages, List<Long> adminIds, Boolean allPagesAccessible, List<Map<Long, String>> groupAdminList){
        super();
        this.groupName = groupName;
        this.pages = pages;
        this.adminIds = adminIds;
        this.allPagesAccessible = allPagesAccessible;
        this.groupAdminList = groupAdminList;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<VToolsSecurityPageAccess> getPages() {
        return pages;
    }

    public void setPages(List<VToolsSecurityPageAccess> pages) {
        this.pages = pages;
    }

    public Boolean getAllPagesAccessible() {
        return allPagesAccessible;
    }

    public void setAllPagesAccessible(Boolean allPagesAccessible) {
        this.allPagesAccessible = allPagesAccessible;
    }

    public List<Long> getAdminIds() {
        return adminIds;
    }

    public void setAdminIds(List<Long> adminIds) {
        this.adminIds = adminIds;
    }

    public List<Map<Long, String>> getGroupAdminList() {
        return groupAdminList;
    }

    public void setGroupAdminList(List<Map<Long, String>> groupAdminList) {
        this.groupAdminList = groupAdminList;
    }
}
