package com.vedantu.platform.vToolsSecurity.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class VToolsSecurityGroupPermissionToAdmin extends AbstractFrontEndReq {
    private String groupName;
    private Long adminId;
    private Boolean access;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Boolean getAccess() {
        return access;
    }

    public void setAccess(Boolean access) {
        this.access = access;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if ( adminId == null ) {
            errors.add("adminId required");
        }

        if(StringUtils.isEmpty(groupName)){
            errors.add("groupName Required");
        }

        if(access == null){
            errors.add("access required either true or false");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "VToolsSecurityGroupPermissionToAdmin { "
                + "groupName = " + groupName
                + ", adminId = " + adminId
                + ", access = "+ access
                + " }";
    }

}
