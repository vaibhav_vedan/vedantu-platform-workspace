package com.vedantu.platform.vToolsSecurity.entity;

import com.vedantu.platform.vToolsSecurity.enums.VToolsSecuritycategory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 *
 * @author mnpk
 */

@Document(collection = "VTooslSecurity")
public class VTooslSecurity extends AbstractMongoStringIdEntity {

    @Indexed(background=true)
    private Long adminId;
    @Indexed(background=true)
    private String groupName;
    private VToolsSecuritycategory category;
    private List<VToolsSecurityPageAccess> pages;
    private List<Long> groupAdminIds;
    private List<String> groupNames;
    private Boolean allPagesAccessible = false;


    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public VToolsSecuritycategory getCategory() {
        return category;
    }

    public void setCategory(VToolsSecuritycategory category) {
        this.category = category;
    }

    public List<VToolsSecurityPageAccess> getPages() {
        return pages;
    }

    public void setPages(List<VToolsSecurityPageAccess> pages) {
        this.pages = pages;
    }

    public List<Long> getGroupAdminIds() {
        return groupAdminIds;
    }

    public void setGroupAdminIds(List<Long> groupAdminIds) {
        this.groupAdminIds = groupAdminIds;
    }

    public List<String> getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(List<String> groupNames) {
        this.groupNames = groupNames;
    }

    public Boolean getAllPagesAccessible() {
        return allPagesAccessible;
    }

    public void setAllPagesAccessible(Boolean allPagesAccessible) {
        this.allPagesAccessible = allPagesAccessible;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String ADMINID = "adminId";
        public static final String GROUPNAME = "groupName";
        public static final String CATEGORY = "category";
        public static final String PAGES = "pages";
        public static final String GROUPADMINIDS = "groupAdminIds";
        public static final String GROUPNAMES = "groupNames";
        public static final String ALLPAGESACCESSIBLE = "allPagesAccessible";
    }

    @Override
    public String toString() {
        return "VToolsSecurity { "
                + "adminId= " + adminId
                + ", groupName= "+ groupName
                + ", category= "+ category
                + ", pages= "+ pages
                + ", groupAdminIds= "+ groupAdminIds
                + ", groupNames= "+ groupNames
                + ", allPagesAccessible= "+ allPagesAccessible
                + " }";
    }
}
