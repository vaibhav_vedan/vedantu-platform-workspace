/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.async;

import com.google.gson.Gson;
import com.vedantu.User.User;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.request.UpdateDeliverableDataReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.lms.cmds.request.CreateBatchCurriculumReq;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.onetofew.pojo.BatchDashboardInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFUpdateSessionRes;
import com.vedantu.platform.cmds.managers.CurriculumManager;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.Dashboard.DashboardManager;
import com.vedantu.platform.managers.DropboxManager;
import com.vedantu.platform.managers.ExportManager;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.MiscManager;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.cms.WebinarManager;
import com.vedantu.platform.managers.dinero.CouponManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.dinero.PayoutManager;
import com.vedantu.platform.managers.feedback.FeedbackFormManager;
import com.vedantu.platform.managers.feedback.FeedbackParentFormManager;
import com.vedantu.platform.managers.listing.ListingManager;
import com.vedantu.platform.managers.onetofew.OTFAsyncTaskManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.review.RatingReviewManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.managers.social.SocializerManager;
import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.mongodbentities.feedback.FeedbackForm;
import com.vedantu.platform.mongodbentities.feedback.FeedbackParentForm;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.request.AvailabilityTeacherExportReq;
import com.vedantu.platform.request.CreateUserDetailsFromReq;
import com.vedantu.platform.request.ExportSubscriptionReq;
import com.vedantu.platform.request.ExportUsersReq;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.subscription.pojo.ContentInfo;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 * @author somil
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    UserManager userManager;

    @Autowired
    ReferralManager referralManager;

    @Autowired
    AwsSNSManager awsSNSManager;

    @Autowired
    PayoutManager payoutManager;

    @Autowired
    PaymentManager paymentManager;

    @Autowired
    ExportManager exportManager;

    @Autowired
    private MiscManager miscManager;

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private SocializerManager socializerManager;

    @Autowired
    private OTFAsyncTaskManager otfAsyncTaskManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    ListingManager listingManager;

    @Autowired
    private RatingReviewManager ratingReviewManager;

    @Autowired
    DashboardManager dashboardManager;

    @Autowired
    CurriculumManager curriculumManager;

    @Autowired
    private CouponManager couponManager;
    
    @Autowired
    private FeedbackParentFormManager feedbackParentFormManager;
    
    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    BoardManager boardManager;
    
    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private DropboxManager dropboxManager;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private FeedbackFormManager feedbackFormManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case SIGNUP_TASKS:
                // EmailRequest request = new Gson().fromJson(jsonString,
                // EmailRequest.class);
                User user = (User) payload.get("user");
                String mobileTokenCode = (String) payload.get("mobileTokenCode");
                String emailTokenCode = (String) payload.get("emailTokenCode");
                Long callingUserId = (Long) payload.get("callingUserId");
                userManager.performSignUpTasks(user, mobileTokenCode, emailTokenCode, callingUserId);
                break;
            case POST_OTF_UPDATE_DELIVERABLE_ID:
                String orderId = (String) payload.get("orderId");
                String enrollmentId = (String) payload.get("enrollmentId");
                paymentManager.updateDeliverableEntityIdInOrder(orderId, enrollmentId,
                        DeliverableEntityType.OTF_BATCH_ENROLLMENT);
                break;
            case PROCESS_REFERRAL_BONUS:
                Long userId = (Long) payload.get("userId");
                ReferralStep referralStep = (ReferralStep) payload.get("referralStep");
                referralManager.processReferralBonus(referralStep, userId);
                break;
            case TRIGGER_SNS:
                SNSTopic topic = (SNSTopic) payload.get("topic");
                String subject = (String) payload.get("subject");
                String message = (String) payload.get("message");
                awsSNSManager.triggerSNS(topic, subject, message);
                break;
            case CALCULATE_SESSION_PAYOUT:
                SessionBillingDetails session = (SessionBillingDetails) payload.get("session");
                payoutManager.calculateSessionPayout(session);
                break;
            case OTF_SESSION_CANCEL_TASK:
                OTFUpdateSessionRes cancelSessionRes = (OTFUpdateSessionRes) payload.get("updateSessionRes");

                // Update the calendar
                otfAsyncTaskManager.cancelSessionTask(cancelSessionRes);
                break;
            case OTF_DAILY_SCHEDULER_TASK:
                // Update the calendar
                otfManager.otfScheduler();
                break;
            case OTF_RECORDING_SCHEDULER_TASK:
                // Update the calendar
                otfManager.otfRecordingScheduler();
                break;
            case LAUNCH_OTO_SESSION_RECORDERS:
                sessionManager.launchOTOSessionRecorders();
                break;
            case CMS_SYNC_WEBINAR_ATTENDEE_INFO:
                webinarManager.syncGtwAttendence();
                break;
            case CMS_SEND_WEBINAR_REMINDERS:
                webinarManager.sendWebinarsReminders();
                break;
            case CMS_SEND_WEBINAR_REGISTRATION_QUESTIONS:
                webinarManager.sendWebinarRegistrationQuestions();
                break;
            case OTF_SANITY_CHECK_TASK:
                otfManager.performSanityCheckForFutureSessions();
                break;
            case OTO_SESSION_END_HANDLER:
                sessionManager.handleSessionEnding();
                break;
            // Not deleting recording in platform now
//		case OTF_DELETE_RECORDING_CRON:
//			otfManager.deleteRecordingsDaily();
//			break;
            case FILL_SESSION_REVIEW_INFO:
                // fills
                String reviewId = (String) payload.get("reviewId");
                Long sessionId = (Long) payload.get("sessionId");
                ratingReviewManager.fillSessionReviewInfo(reviewId, sessionId);
                break;
            case EXPORT_USERS:
                ExportUsersReq req = (ExportUsersReq) payload.get("request");
                HttpSessionData currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportUsers(req, currentSessionData);
                break;
            case EXPORT_SUBSCRIPTIONS:
                ExportSubscriptionReq exportSubscriptionReq = (ExportSubscriptionReq) payload.get("request");
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportSubscriptions(exportSubscriptionReq, currentSessionData);
                break;
            case EXPORT_COURSE_PLANS:
                ExportCoursePlansReq exportCoursePlansReq = (ExportCoursePlansReq) payload.get("request");
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportCoursePlans(exportCoursePlansReq, currentSessionData);
                break;
            case EXPORT_AVAILABILITY:
                AvailabilityTeacherExportReq availabilityTeacherExportReq = (AvailabilityTeacherExportReq) payload
                        .get("request");
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportAvailability(availabilityTeacherExportReq, currentSessionData);
                break;
            case EXPORT_SESSIONS:
                GetSessionsReq getSessionsReq = (GetSessionsReq) payload.get("request");
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportSessions(getSessionsReq, currentSessionData);
                break;
            case EXPORT_TOS:
                getSessionsReq = (GetSessionsReq) payload.get("request");
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportScheduledTOSSessions(getSessionsReq, currentSessionData);
                break;
            case CHECK_TRIAL_ENROLLMENT_DUES:
                 Integer start = (Integer) payload.get("start");
                 Integer size = (Integer) payload.get("size");
                 otfAsyncTaskManager.checkTrialEnrollments(start, size);
                break;
            case ALERT_ABOUT_FIRST_REGULAR_SESSION_DATE:
                Integer st = (Integer) payload.get("start");
                Integer si = (Integer) payload.get("size");
                coursePlanManager.alertAboutFirstRegularSessionDate(st, si);
                break;
            case ALERT_ABOUT_NON_TRIAL_PAYMENT:
                Integer s1 = (Integer) payload.get("start");
                Integer s2 = (Integer) payload.get("size");
                coursePlanManager.alertNonTrialPayment(s1, s2);
                break;
            case OTF_UPDATE_CALENDAR:
                EnrollmentPojo enrollmentInfo = (EnrollmentPojo) payload.get("enrollmentInfo");
                otfAsyncTaskManager.updateCalendar(enrollmentInfo);
                break;
            case SHARE_CONTENT_INFOS:
                List<ContentInfo> contentInfos = (List<ContentInfo>) payload.get("contentInfos");
                String title = (String) payload.get("title");
                Long studentId = (Long) payload.get("studentId");
                String coursePlanId = (String) payload.get("coursePlanId");
                coursePlanManager.shareCoursePlanContents(contentInfos, studentId, title, coursePlanId);
                break;
            case BLOCK_COURSE_PLAN_SLOTS:
                SessionSchedule sessionSchedule = (SessionSchedule) payload.get("sessionSchedule");
                coursePlanId = (String) payload.get("coursePlanId");
                Long sId = (Long) payload.get("studentId");
                Long tId = (Long) payload.get("teacherId");
                coursePlanManager.blockCoursePlanSlots(sessionSchedule, sId, tId, coursePlanId);
                break;

            case RESET_COURSE_PLAN_SLOTS:
                String cId = (String) payload.get("coursePlanId");
                Long endTime = (Long) payload.get("endTime");
                coursePlanManager.updateBlockedInstalmentSlots(cId, endTime);
                break;
            case BOOK_COURSE_PLAN_REGULAR_SESSIONS:
                String coursePlanInfoString = (String) payload.get("coursePlanInfo");
                CoursePlanInfo coursePlanInfo = new Gson().fromJson(coursePlanInfoString, CoursePlanInfo.class);
                Long uid = (Long) payload.get("userId");
                PaymentType paymentType = (PaymentType) payload.get("paymentType");
                coursePlanManager.bookRegularSessions(coursePlanInfo, uid, paymentType);
                break;
            case PROCESS_COURSE_PLAN_INSTALMENT_PAYMENT:
                InstalmentInfo instalmentInfo = (InstalmentInfo) payload.get("instalmentInfo");
                Long instalmentPaidUserId = (Long) payload.get("callingUserId");
                coursePlanManager.processInstalmentPayment(instalmentInfo, instalmentPaidUserId, Boolean.TRUE);
                break;
            case WEBINAR_CALENDAR_UPDATE_TASK:
                // String sessions = (String) payload.get("sessions");
                // calendarManager.markCalendarEntries(calendarEntryReq)
                break;
            case CREATE_OTF_ENROLLMENT_DASHBOARD:
                BatchDashboardInfo batchDashboardInfo = (BatchDashboardInfo) payload.get("batchInfo");
                dashboardManager.updateDashboardInfo(batchDashboardInfo);
                break;
            case PASS_EXPIRY_CRON_TASK:
                int startValue = (int) payload.get("start");
                int sizeValue = (int) payload.get("size");
                couponManager.checkPassExpiration(startValue, sizeValue);
                break;
            case EXPORT_OTF_DASHBOARD:
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportOTFDashboard(currentSessionData);
                break;
            case EXPORT_TEACHER_DASHBOARD:
                currentSessionData = (HttpSessionData) payload.get("sessionData");
                exportManager.exportTeacherDashboard(currentSessionData);
                break;
            case CREATE_TEACHER_DASHBOARD:
                Long startTime = (Long) payload.get("startTime");
                Long endingTime = (Long) payload.get("endTime");
                dashboardManager.createTeacherDashboard(startTime, endingTime);
                break;
            case CREATE_DASHBOARD_CRON:
                dashboardManager.createDashboard();
                break;
            case VERIFY_COURSE_PLAN_HOURS:
                coursePlanManager.verifyCoursePlanHoursConsistency();
                break;
            case POST_ENROLLMENT_CONTENT_SHARE_CURRICULUM:
                // Share contents of the otf batch
                ShareContentEnrollmentReq shareContentCurriculumReq = (ShareContentEnrollmentReq) payload
                        .get("shareContentCurriculumReq");
                curriculumManager.shareContentOnEnrollment(shareContentCurriculumReq);
                break;
            case CREATE_BATCH_CURRICULUM:
                CreateBatchCurriculumReq createBatchCurriculumReq = (CreateBatchCurriculumReq) payload
                        .get("createBatchCurriculumReq");
                curriculumManager.createBatchCurriculum(createBatchCurriculumReq);
                break;
            case RECALCULATE_VOTES:
            	String questionId = (String) payload.get("questionId");
            	socializerManager.setQuestionVotes(questionId);
                break;
            case RECALCULATE_VIDEO_LIKES:
            	socializerManager.setVideoVotes(payload);
            	break;
            case CREATE_ISL_USER_DETAILS_FROM_LIST:
                CreateUserDetailsFromReq iSLSendCommunicationReq = (CreateUserDetailsFromReq) payload.get("req");
                userManager.createISLUserDetailsFromList(iSLSendCommunicationReq);
                break;
            case EXPORT_PASSWORD_EMPTY_USERS_FOR_ISL:
                exportManager.exportPasswordEmptyUsersForISL((HttpSessionData) payload.get("sessionData"));
                break;
            case MISSED_CALL_TRIGGER:
                String mobileno = (String) payload.get("mobileno");
                String target = (String) payload.get("target");
                String smstext = null;
                if (payload.get("smstext") != null) {
                    smstext = (String) payload.get("smstext");
                }
                miscManager.missedcalltrigger(mobileno, target, smstext);
                break;
            case UPDATE_ORDER_DELIVERABLE_DATA:
                UpdateDeliverableDataReq updateDeliverableDataReq = (UpdateDeliverableDataReq) payload.get("updateDeliverableDataReq");
                paymentManager.updateDeliverableData(updateDeliverableDataReq);
                break;
            case SEND_BULK_MESSAGE_TO_LEADS:
                miscManager.sendMessageToRegisteredLeads();
                break;
            case SEND_MAIL_FOR_FEEDBACK_FORM:
                FeedbackForm feedbackForm = (FeedbackForm) payload.get("feedbackForm");
                Long userIdForFeedbackForm = (Long) payload.get("userId");
                feedbackParentFormManager.sendMail(userIdForFeedbackForm, feedbackForm);
                break;
            case CREATE_FEEDBACK_FORM_INSTANCE:
                FeedbackParentForm feedbackParentForm = (FeedbackParentForm) payload.get("feedbackParentForm");
                Long time = (Long) payload.get("time");
                feedbackParentFormManager.processFeedbackForm(feedbackParentForm, time);
                break;
            case BUNDLE_AUTO_ENROLL:
                Long bundleUserId = (Long) payload.get("userId");
                String bundleId = (String) payload.get("entityId");
                String bundleEnrollmentId = (String) payload.get("enrollmentId");
                bundleManager.autoEnrollInBundleCourses(bundleId, bundleUserId,bundleEnrollmentId);
                break;
            case DROPBOX_LS_ASYNC_TASK: {
                String requestBody = (String) payload.get("requestBody");
                String dropboxSignature = (String) payload.get("dropboxSignature");
                dropboxManager.performWebHookTask(requestBody, dropboxSignature);
                break;
            }
            case WEBINAR_NOTIFICATION_TASK:
                webinarManager.prepareAlertForWebinarRegisteredUsers();
                break;
            case GET_DEMO_SESSION_SUPERKIDS:
                Map<String,String> userLeads= (Map<String, String>) payload.get("userLeads");
                paymentManager.makeLeadSquaredELBundlePayment(userLeads);
                break;
            case BULK_LS_TASK:
                List<LeadSquaredRequest> requests=(List<LeadSquaredRequest>)payload.get("requests");
                requests.forEach(request -> {
                    try {
                        leadSquaredManager.executeTask(request);
                    } catch (Exception e) {
                        logger.error("Error in executing async task in bulk ",e);
                    }
                });
                break;
            case SEND_EMAIL_AFTER_WEBINAR_COMPLETED:
                webinarManager.prepareEmailForWebinarCompleted();
                break;
            case SEND_FEEDBACK_TO_SLACK_ASYNC:
                String feedbackMessage = (String) payload.get("message");
                String snsSubject = (String) payload.get("subject");
                feedbackFormManager.sendFeedbackToSlack(feedbackMessage,snsSubject);
                break;
            default:
                logger.error("Logic not defined for task:" + taskName);
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
