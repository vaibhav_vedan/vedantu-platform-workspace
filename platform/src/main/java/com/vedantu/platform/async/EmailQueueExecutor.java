/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.async;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.dinero.request.SendInstalmentPaidEmailReq;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.RedeemCouponInfo;
import com.vedantu.lms.cmds.request.ShareContentAndSendMailRes;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager.CoursePlanHourInconsistentDetails;
import com.vedantu.platform.mongodbentities.ISLReport;
import com.vedantu.platform.mongodbentities.UserMessage;
import com.vedantu.platform.mongodbentities.offering.Offering;
import com.vedantu.platform.mongodbentities.review.Feedback;
import com.vedantu.platform.mongodbentities.review.Review;
import com.vedantu.platform.pojo.referral.model.ReferralStepBonus;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestNotificationInfo;
import com.vedantu.platform.pojo.subscription.SubscriptionSessionRequestInfo;
import com.vedantu.platform.request.dinero.SendInstalmentEmailReq;
import com.vedantu.platform.userleads.SendISLEmailReq;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.response.instalearn.SubmitInstaRequestRes;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.request.CoursePlanAdminAlertReq;
import com.vedantu.subscription.request.CoursePlanReqFormReq;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author somil
 */
@Service
public class EmailQueueExecutor implements IAsyncQueueExecutor {

    

    @Autowired
    public LogFactory logFactory;

    @Autowired
    CommunicationManager emailManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(EmailQueueExecutor.class);

    //TODO: Add executor here
    @Async("emailExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        logger.info("ENTRY " + params);
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case EMAIL_GENERAL_TASK:
                //EmailRequest request = new Gson().fromJson(jsonString, EmailRequest.class);
                EmailRequest request = (EmailRequest) payload.get("emailRequest");
                emailManager.sendEmail(request);
                break;
            case WELCOME_EMAIL:
                User user = (User) payload.get("user");
                emailManager.sendWelcomeEmail(user);
                break;
            case VERIFICATION_EMAIL:
                UserBasicInfo userVerification = (UserBasicInfo) payload.get("user");
                CommunicationType type = (CommunicationType) payload.get("type");
                String tokenCode = (String) payload.get("tokenCode");
                emailManager.sendVerificationTokenEmail(userVerification, type, tokenCode);
                break;
            case TRANSFER_FROM_FREEBIES_EMAIL:
                Integer amount = (Integer) payload.get("amount");
                Long toUserId = (Long) payload.get("toUserId");
                Boolean noSMS = (Boolean) payload.get("noSMS");
                emailManager.sendTransferFromFreebiesCommunication(amount, toUserId, noSMS);
                break;
            case OTF_ENROLLMENT_EMAIL:
                EnrollmentPojo info = (EnrollmentPojo) payload.get("enrollmentInfo");
                OrderInfo orderInfo = (OrderInfo) payload.get("orderInfo");
                emailManager.sendOTFEnrollmentEmail(info,orderInfo);
                break;
            case OTF_MANUAL_ENROLLMENT_EMAIL:
                info = (EnrollmentPojo) payload.get("enrollmentInfo");
                emailManager.sendOTFManualEnrollmentEmail(info);
                break;
            case MESSAGE_USER_EMAIL:
                User fromUser = (User) payload.get("fromUser");
                User toUser = (User) payload.get("toUser");
                HashMap<String, Object> bodyScopesMessageUser = (HashMap<String, Object>) payload.get("bodyScopes");
                CommunicationType typeMessageUser = (CommunicationType) payload.get("type");
                emailManager.sendMessageUserEmail(fromUser, toUser, bodyScopesMessageUser, typeMessageUser);
                break;
            case SUBSCRIPTION_SESSION_EMAIL:

                try {

                    SessionInfo sessionInfo = null;
                    if (payload.get("sessionInfo") != null) {
                        sessionInfo = (SessionInfo) payload.get("sessionInfo");
                    }
                    Long subscriptionId = null;
                    if (payload.get("subscriptionId") != null) {
                        subscriptionId = (Long) payload.get("subscriptionId");
                    }
                    Role role = (Role) payload.get("role");
                    SessionModel model = (SessionModel) payload.get("model");
                    SubModel subModel = (SubModel) payload.get("subModel");
                    Boolean scheduled = (Boolean) payload.get("scheduled");
                    Integer sessionGrade = null;
                    if (payload.get("grade") != null) {
                        sessionGrade = (Integer) payload.get("grade");
                    }
                    String target = null;
                    if (payload.get("target") != null) {
                        target = (String) payload.get("target");
                    }
                    String note = null;
                    if (payload.get("note") != null) {
                        note = (String) payload.get("note");
                    }
                    emailManager.sendSubscriptionSessionRelatedEmail(sessionInfo, role, model, subModel, scheduled, sessionGrade, target, note, subscriptionId);

                } catch (Exception e) {
                    logger.error("Error ", e);
                }
                break;
            case SUBSCRIPTION_ENDED_EMAIL:
                CancelSubscriptionResponse cancelSubscriptionResponse = (CancelSubscriptionResponse) payload.get("cancelSubscriptionResponse");
                HttpSessionData httpSessionData = (HttpSessionData) payload.get("httpSessionData");
                emailManager.sendSubscriptionEndedMail(cancelSubscriptionResponse, httpSessionData);
                break;
            case SUBSCRIPTION_REQUEST_REMINDER:
                SubscriptionRequestNotificationInfo subscriptionRequestNotificationInfo = (SubscriptionRequestNotificationInfo) payload.get("subscriptionRequestNotificationInfo");
                emailManager.sendSubscriptionRequestReminder(subscriptionRequestNotificationInfo);
                break;
            case SESSION_RELATED_EMAIL:
                CommunicationType scommunicationType = (CommunicationType) payload.get("communicationType");
                UserBasicInfo semailRecipientUser = (UserBasicInfo) payload.get("emailRecipientUser");
                SessionInfo sessionInfo = (SessionInfo) payload.get("sessionInfo");
                emailManager.sendSessionRelatedEmail(semailRecipientUser, sessionInfo, scommunicationType);
                break;
            case SUBSCRIPTION_SESSION_REQUEST_EMAIL:
                CommunicationType subsReqcommunicationType = (CommunicationType) payload.get("communicationType");
                SubscriptionSessionRequestInfo subscriptionSessionRequestInfo = (SubscriptionSessionRequestInfo) payload.get("subscriptionSessionRequestInfo");
                emailManager.sendSubscriptionSessionRequestRelatedEmail(subscriptionSessionRequestInfo, subsReqcommunicationType);
                break;
            case INSTALEARN_COMMUNICATION:
                SubmitInstaRequestRes submitInstaRequestRes = (SubmitInstaRequestRes) payload.get("submitInstaRequestRes");
                UserBasicInfo emailRecipientUser = (UserBasicInfo) payload.get("emailRecipientUser");
                String ilsubject = (String) payload.get("subject");
                Integer ilgrade = (Integer) payload.get("grade");
                CommunicationType communicationType = (CommunicationType) payload.get("communicationType");
                SessionSlot sessionSlot = null;
                if (payload.get("sessionSlot") != null) {
                    sessionSlot = (SessionSlot) payload.get("sessionSlot");
                }
                emailManager.sendInstalLearnCommunication(emailRecipientUser, submitInstaRequestRes, sessionSlot, ilsubject, ilgrade, communicationType);
                break;
            case REQUEST_CALLBACK_COMMUNICATION:
                RequestCallBackDetails requestCallBackDetails = (RequestCallBackDetails) payload.get("requestCallBackDetails");
                CommunicationType rcommunicationType = (CommunicationType) payload.get("communicationType");
                emailManager.sendRequestCallbackCommunication(requestCallBackDetails, rcommunicationType);
                break;
            case REFERRAL_EMAIL:
                UserInfo userInfo = (UserInfo) payload.get("userInfo");
                UserInfo referrerUserInfo = (UserInfo) payload.get("referrerUserInfo");
                ReferralStep referralStep = (ReferralStep) payload.get("referralStep");
                ReferralStepBonus referralStepBonus = (ReferralStepBonus) payload.get("referralStepBonus");
                emailManager.sendReferralEmail(userInfo, referrerUserInfo, referralStep, referralStepBonus);
                break;
            case REVIEW_EMAIL:
                User toUserReview = (User) payload.get("toUser");
                User fromUserReview = (User) payload.get("fromUser");
                Review review = (Review) payload.get("review");
                emailManager.sendReviewEmail(toUserReview, fromUserReview, review);
                break;
            case ADD_TO_OFFERING_EMAIL:
                List<String> userIds = (List<String>) payload.get("teacherIds");
                Offering offering = (Offering) payload.get("offering");
                emailManager.sendAddToOfferingEmail(userIds, offering);
                break;
            case SESSION_FEEDBACK_EMAIL:
                Feedback feedback = (Feedback) payload.get("feedback");
                emailManager.sendSessionFeedbackEmail(feedback);
                break;
            case INSTALMENT_DUE_EMAIL:
                SendInstalmentEmailReq req = (SendInstalmentEmailReq) payload.get("request");
                emailManager.sendInstalmentDueEmail(req);
                break;
            case INSTALMENT_PAID_EMAIL:
                SendInstalmentPaidEmailReq requestEmail = (SendInstalmentPaidEmailReq) payload.get("request");
                emailManager.sendInstalmentPaidEmail(requestEmail);
                break;
            case TEACHER_EDIT_PROFILE_EMAIL:
                EditProfileRes editProfileRes = (EditProfileRes) payload.get("editProfileRes");
                HashMap<String, HashMap<Object, Object>> diffData = (HashMap<String, HashMap<Object, Object>>) payload.get("diffUserData");
                HttpSessionData callerUser = (HttpSessionData) payload.get("callerUser");
                emailManager.sendTeacherEditProfileToAdmin(editProfileRes, diffData, callerUser);
                break;
            case MAPPING_UPDATE_EMAIL:
                Long userId = (Long) payload.get("userId");
                String subjectName = (String) payload.get("subjectName");
                String grade = (String) payload.get("grade");
                String category = (String) payload.get("category");
                String changeType = (String) payload.get("changeType");
                HttpSessionData callerUserMapping = (HttpSessionData) payload.get("callerUser");
                emailManager.sendTeacherBoardMappingUpdateToAdmin(userId, subjectName, grade, category, changeType, callerUserMapping);
                break;
            case BLOCK_USER_EMAIL:
                Long callingUserId = (Long) payload.get("callingUserId");
                Long userIdBlock = (Long) payload.get("userId");
                String reason = (String) payload.get("reason");
                HttpSessionData callerUserBlock = (HttpSessionData) payload.get("callerUser");
                emailManager.sendBlockUserToAdmin(callingUserId, userIdBlock, reason, callerUserBlock);
                break;
            case USER_MESSAGE_EMAIL:
                User userPojo = (User) payload.get("userPojo");
                UserMessage message = (UserMessage) payload.get("message");
                emailManager.sendUserMessageEmail(userPojo, message);
                break;
            case USER_LEAD_REGISTRATION_EMAIL:
                SendISLEmailReq sendISLEmailReq = (SendISLEmailReq) payload.get("sendISLEmailReq");
                emailManager.sendISLEmail(sendISLEmailReq);
                break;
            case OTF_REGISTRATION_EMAIL:
                Long otfRegisteredUserId = (Long) payload.get("userId");
                String courseId = (String) payload.get("courseId");
                Integer otfRegistrationFee = (Integer) payload.get("amount");
                emailManager.sendCourseRegistrationEmail(otfRegisteredUserId,courseId,
                        otfRegistrationFee,CommunicationType.OTF_REGISTRATION);
                break;
            case OTO_REGISTRATION_EMAIL:
                Long otoRegisteredUserId = (Long) payload.get("userId");
                String otocourseId = (String) payload.get("courseId");
                Integer otoRegistrationFee = (Integer) payload.get("amount");
                emailManager.sendCourseRegistrationEmail(otoRegisteredUserId,otocourseId,
                        otoRegistrationFee,CommunicationType.OTO_REGISTRATION);
                break;
            case BUNDLE_ENROLLMENT_EMAIL:
                Long enrolledUserId = (Long) payload.get("userId");
                String entityId = (String) payload.get("entityId");
                boolean isCampaign = false;
                if(payload.containsKey("campaign")) {
                    String campaign = (String) payload.get("campaign");
                    if(StringUtils.isNotEmpty(campaign)){
                        isCampaign=  emailManager.sendCampainRelatedEnrolmentEmail(enrolledUserId,entityId,campaign);
                    }
                }
                com.vedantu.session.pojo.EntityType entityType = (com.vedantu.session.pojo.EntityType) payload.get("entityType");
                if(!isCampaign) {
                    emailManager.sendBundleRelatedEnrolmentEmail(enrolledUserId, entityId,
                            entityType);
                }
                break;
            case ISL_REPORT_EMAIL:
                ISLReport report = (ISLReport) payload.get("report");
                emailManager.sendISLReportEmail(report);
                break;
            case COURSE_PLAN_EMAILS:
                CommunicationType cPlanEmailType = (CommunicationType) payload.get("type");
                CoursePlanInfo coursePlanInfo=(CoursePlanInfo)payload.get("coursePlanInfo");
                emailManager.sendCoursePlanEmails(coursePlanInfo, cPlanEmailType);
                break;
            case COURSE_PLAN_SESSION_EMAILS:
        		SessionInfo coursePlanSessionInfo = (SessionInfo) payload.get("sessionInfo");
        		SessionState sessionState = (SessionState) payload.get("sessionEvent");
        		callingUserId = (Long) payload.get("cancelSessionUserId");
        		Role callingUserRole = (Role) payload.get("cancelSessionUserRole");
        		coursePlanInfo = (CoursePlanInfo) payload.get("coursePlanInfo");
        		emailManager.sendCoursePlanSessionEmails(coursePlanSessionInfo, coursePlanInfo, sessionState, callingUserId, callingUserRole);
                break;
            case ADD_COURSE_PLAN_REQ_EMAIL:
                CoursePlanReqFormReq coursePlanReqFormReq=(CoursePlanReqFormReq)payload.get("coursePlanReqFormReq");
                emailManager.sendCoursePlanReqEmails(coursePlanReqFormReq);
                break;
            case COURSE_PLAN_ADMIN_ALERTS:
                CoursePlanAdminAlertReq coursePlanAdminAlertReq = (CoursePlanAdminAlertReq) payload.get("coursePlanAdminAlertReq");
                CommunicationType alertType = (CommunicationType) payload.get("type");
                emailManager.sendCoursePlanAdminAlerts(coursePlanAdminAlertReq,alertType);
                break;
            case BUNDLE_ENROLLMENT_PASS_EMAIL:
                enrolledUserId = (Long) payload.get("userId");
                entityId = (String) payload.get("entityId");
                entityType = (com.vedantu.session.pojo.EntityType) payload.get("entityType");
                RedeemCouponInfo redeemCouponInfo = (RedeemCouponInfo) payload.get("redeemCouponInfo");
                emailManager.sendBundleRelatedEnrolmentPassEmail(enrolledUserId,entityId,
                        entityType, redeemCouponInfo);
                break;
            case EXPORT_ARM_FORM_EMAIL:
            	Long startTime = (Long) payload.get("startTime");
            	Long endTime = (Long) payload.get("endTime");
            	Long exportEmailId= (Long) payload.get("requestEmailId");
                emailManager.sendArmFormExportEmail(startTime, endTime, exportEmailId);
                break;
            case COURSE_PLAN_HOURS_INCONSISTENCY_REPORT:
            	List<CoursePlanHourInconsistentDetails> errorCoursePlanEntries = (List<CoursePlanHourInconsistentDetails>) payload.get("errorCoursePlanEntries");
            	long calculatedStartTime = (long) payload.get("calculatedStartTime");
            	long calculatedEndTime = (long) payload.get("calculatedEndTime");
                emailManager.sendCoursePlanInconsistentHours(errorCoursePlanEntries, calculatedStartTime, calculatedEndTime);
                break;
            case CONTENT_SHARED_CURRICULUM:
            	ShareContentAndSendMailRes shareContentAndSendMailReq = (ShareContentAndSendMailRes) payload.get("shareContentAndSendMailRes");
            	emailManager.sendMoodleNotificationsOnSharing(shareContentAndSendMailReq);
            	break;
            case REGISTRATION_EMAIL_ISL:
            	User userData = (User) payload.get("user");
                emailManager.sendRegistrationEmailForISL(userData);
                break;
            case SIGNUP_VIA_ISL_REGISTRATION_TOOLS:
            	User userData2 = (User) payload.get("user");
                emailManager.sendEmailRegistrationISLViaTools(userData2);
                break;
            case USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS:
            	User userData3 = (User) payload.get("user");
                emailManager.sendEmailUserDetailsCreatedViaTools(userData3);
                break;
            case TRACK_EMAIL_LINK_CLICK:
                String emailId = (String) payload.get("emailId");
                String linkRandomId = (String) payload.get("linkRandomId");
                emailManager.trackEmailLinkClicked(emailId, linkRandomId);
                break;
            case REFERRAL_EMAIL_BUNDLE:
                UserInfo userInfo1 = (UserInfo) payload.get("userInfo");
                UserInfo referrerUserInfo1 = (UserInfo) payload.get("referrerUserInfo");
                ReferralStep referralStep1 = (ReferralStep) payload.get("referralStep");
                ReferralStepBonus referralStepBonus1 = (ReferralStepBonus) payload.get("referralStepBonus");
                String bundleId = (String) payload.get("bundleId");
                emailManager.sendReferralBundleEmail(userInfo1, referrerUserInfo1, referralStep1, referralStepBonus1, bundleId);
                break;
            case POST_BUNDLE_PURCHASE_EMAIL:
            	Long userId1 = (Long) payload.get("userId");
            	BundlePackageInfo bundlePackageInfo = (BundlePackageInfo) payload.get("bundlePackageInfo");
            	emailManager.sendPostBundlePurchaseEmail(userId1, bundlePackageInfo);
                break;
            case POST_OTM_BUNDLE_REGISTRATION_EMAIL:
            	Long userId2 = (Long) payload.get("userId");
            	OTFBundleInfo otfBundleInfo = (OTFBundleInfo) payload.get("otfBundleInfo");
            	Integer paidAmount = (Integer) payload.get("paidAmount");
            	emailManager.sendOTMBundlePurchaseEmail(userId2, otfBundleInfo,paidAmount,CommunicationType.POST_OTM_BUNDLE_REGISTRATION);
                break;
            case POST_OTM_BUNDLE_PURCHASE_EMAIL:
            	Long userId3 = (Long) payload.get("userId");
            	OTFBundleInfo otfBundleInfo1 = (OTFBundleInfo) payload.get("otfBundleInfo");
            	Integer paidAmount1 = (Integer) payload.get("paidAmount");
            	emailManager.sendOTMBundlePurchaseEmail(userId3, otfBundleInfo1,paidAmount1,CommunicationType.POST_OTM_BUNDLE_PURCHASE);
                break;
            case REGISTRATION_EMAIL_VSAT:
                User userDataNew = (User) payload.get("user");
                emailManager.sendRegistrationEmailForVSAT(userDataNew);
                break;
            case REGISTRATION_EMAIL_VOLT:
                User user1 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForVolt(user1);
                break;
            case REGISTERED_EMAIL_JRP:
                User userDataNew2 = (User) payload.get("user");
                UserDetailsInfo userDetails = (UserDetailsInfo) payload.get("userDetails");
                emailManager.sendRegistrationEmailForJRP(userDataNew2,userDetails);
                break;
            case REGISTRATION_EMAIL_REVISEINDIA:
                User u1 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForReviseIndia(u1);
                break;
            case REGISTRATION_EMAIL_REVISEJEE:
                User u2 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForReviseJee(u2);
                break;
            case TEST_RESULT_EMAIL_REVISEJEE:
                emailManager.sendResultEmailForReviseJee(payload);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
