package com.vedantu.platform.async;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;

import com.vedantu.async.Task;

public class FillSessionReviewInfoTask implements Task{
	
	    private String reviewId;
	    private String sessionId;

	    public FillSessionReviewInfoTask() {
	        super();
	    }

	    public FillSessionReviewInfoTask(String reviewId, String sessionId) {
			super();
			this.reviewId = reviewId;
			this.sessionId = sessionId;
		}

            @Override
	    @Async
	    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
	    public void execute() {
//	        Session session = DAOFactory.INSTANCE.getSessionDAO().getSessionById(Long.parseLong(sessionId));
//	        if (session != null) {
//	            SessionReviewInfo reviewInfo = new SessionReviewInfo();
//	            reviewInfo.setSubject(session.getSubject());
//	            if (session.getSubscriptionId() != null) {
//	                GetSubscriptionRequest req = new GetSubscriptionRequest();
//	                req.setSubscriptionId(session.getSubscriptionId());
//	                GetSubscriptionsResponse resPojo = NewSubscriptionManager.getSubscription(req);
//	                if (resPojo != null
//	                        && resPojo.getSubscriptionsList() != null
//	                        && !resPojo.getSubscriptionsList().isEmpty()) {
//	                    SubscriptionResponseVO subscriptionResponseVO = resPojo.getSubscriptionsList().get(0);
//	                    reviewInfo.setSubject(subscriptionResponseVO.getSubject());
//	                    if (subscriptionResponseVO.getGrade() != null) {
//	                        reviewInfo.setGrade(subscriptionResponseVO.getGrade().toString());
//	                    }
//	                    reviewInfo.setTarget(subscriptionResponseVO.getTarget());
//	                }
//	            }
//	            IReviewDAO reviewDAO = DAOFactory.INSTANCE.getReviewDAO();
//	            Review review = reviewDAO.getReviewById(Long.parseLong(reviewId));
//	            if (review != null) {
//	                review.setReviewExtraInfo(reviewInfo);
//	                review.setPriority(0);//TODO remove this later
//	                reviewDAO.upsert(review);
//	            }
//	        }
	    }
}
