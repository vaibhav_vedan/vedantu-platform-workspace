/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.async;

import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.listing.pojo.GetTeacherResponse;
import com.vedantu.platform.managers.ESManager;
import com.vedantu.platform.managers.listing.ListingManager;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.platform.mongodbentities.review.CumilativeRating;
import com.vedantu.platform.pojo.requestcallback.request.ESPopulatorRequest;
import com.vedantu.scheduling.pojo.session.Session;
import com.vedantu.util.LogFactory;

/**
 *
 * @author somil
 */
@Service
public class ElasticSearchQueueExecutor implements IAsyncQueueExecutor{

    @Autowired
    private LogFactory logFactory;
    

    @Autowired
    private ListingManager listingManager;    
    
    
    @Autowired
    private ESManager eSManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ElasticSearchQueueExecutor.class);

    @Async("esExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if(!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case ES_ADD_TEACHER:
                User teacher = (User) payload.get("user");
                eSManager.setTeacherData(teacher);
                break;
            case ES_UPDATE_TEACHER_DATA:
                User user = (User) payload.get("user");
                eSManager.updateTeacherData(user);
                break;
            case ES_ADD_BOARD: 
                Board board = (Board) payload.get("board");
                eSManager.setBoardData(board);
                break;
            case ES_UPDATE_TEACHER_BOARD_MAPPING: 
                TeacherBoardMapping tbm = (TeacherBoardMapping) payload.get("teacherBoardMapping");
                eSManager.updateTeacherBoardMapping(tbm);
                break;
            case ES_DELETE_TEACHER_BOARD_MAPPING: 
                Long id = (Long) payload.get("id");
                eSManager.deleteTeacherBoardMapping(id);
                break;
            case ES_UPDATE_STARTS_FROM:
                Long teacherId = (Long) payload.get("teacherId");
                Double teacherMinPrice = (Double) payload.get("teacherMinPrice");
                Double teacherOneHourRate = (Double) payload.get("teacherOneHourRate");
                eSManager.updateStartsFromInES(teacherId, teacherMinPrice, teacherOneHourRate);
                break;
            case ES_POPULATE:
                ESPopulatorRequest esPopulatorRequest = (ESPopulatorRequest) payload.get("esPopulatorRequest");
                eSManager.populateESParam(esPopulatorRequest);
                break;
            case ES_UPDATE_RATING:
                CumilativeRating rating = (CumilativeRating) payload.get("rating");
                eSManager.updateRating(rating);
                break;
            case ES_LISTING_FACE_COUNT_ADDITION:
                GetTeacherResponse response = (GetTeacherResponse) payload.get("getTeacherResponse");
                eSManager.listingFaceCountAddition(response);
                break;
            case ES_UPDATE_SESSION_DATA:
                Session session = (Session) payload.get("session");
                eSManager.updateSessionData(session);
                break;
            case UPDATE_TOS_DATA:
                Integer start = (Integer) payload.get("start");
                Integer limit = (Integer) payload.get("limit");
                listingManager.updateTOSData(start, limit);
                break;                
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }
    
}
