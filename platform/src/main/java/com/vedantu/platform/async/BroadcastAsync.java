package com.vedantu.platform.async;

import com.vedantu.platform.managers.BroadcastManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.platform.dao.askadoubt.DoubtDao;
import com.vedantu.platform.pojo.askadoubt.Doubt;
import com.vedantu.platform.pojo.askadoubt.DoubtInfo;
import com.vedantu.platform.managers.board.TeacherBoardMappingManager;
import com.vedantu.platform.dao.broadcast.BroadcastDao;
import com.vedantu.platform.enums.broadcast.BroadcastStatus;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.pojo.broadcast.Broadcast;
import com.vedantu.platform.pojo.broadcast.BroadcastInfo;
import com.vedantu.platform.pojo.broadcast.SendAskADoubtNotificationInfo;
import com.vedantu.platform.pojo.broadcast.SendBroadcastNotificationReq;
import com.vedantu.platform.pojo.broadcast.SendBroadcastRequest;
import com.vedantu.platform.managers.notification.NotificationManager;
import com.vedantu.teacherBoardMappings.pojo.GetTeacherBoardMappingHierarchialRes;
import com.vedantu.util.LogFactory;

@Service
public class BroadcastAsync {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(BroadcastAsync.class);

    @Autowired
    BroadcastDao broadcastDao;

    @Autowired
    DoubtDao doubtDao;

    @Autowired
    BroadcastManager broadcastManager;

    @Autowired
    TeacherBoardMappingManager tbmManager;

    @Autowired
    NotificationManager notificationManager;

    //final String GET_MAPPING_ENDPOINT = configUtils
    //		.getStringValue("FOS_ENDPOINT")
    //		+ ConfigUtils.INSTANCE.getStringValue("fos.teachermappings.get");
    //final String SEND_NOTIFICATION_ENDPOINT = configUtils
    //		.getStringValue("FOS_ENDPOINT")
    //		+ ConfigUtils.INSTANCE.getStringValue("fos.notification");
    public BroadcastAsync() {
        super();
    }

//    @Async
    public Future<Boolean> sendAskADoubtBroadcast(
            SendBroadcastRequest sendBroadcastRequest) throws Exception {
        try {

            List<Broadcast> listToBeBroadcasted = new ArrayList<Broadcast>();
            Doubt doubt = doubtDao.getById(sendBroadcastRequest
                    .getReferenceId());

            Boolean toBeAdded = false;
            for (Long userId : sendBroadcastRequest.getUserIds()) {

                logger.info("Entered user loop: id: " + userId);

                GetTeacherBoardMappingHierarchialRes getTeacherBoardMappingHierarchialRes = tbmManager.getHierarchialTeacherMappings(userId);
                //getMappings(userId);

                toBeAdded = false;

                if (getTeacherBoardMappingHierarchialRes == null) {
                    continue;
                } else if (getTeacherBoardMappingHierarchialRes.getGrades()
                        .contains(doubt.getGrade().toString())) {
                    for (BoardTreeInfo boardTreeInfo : getTeacherBoardMappingHierarchialRes
                            .getBoards()) {
                        logger.info(boardTreeInfo);
                        if (boardTreeInfo.getId().equals(
                                Long.parseLong(doubt.getBoardId()))) {
                            logger.info("entered board loop : "
                                    + boardTreeInfo.getId());
                            toBeAdded = true;
                            break;
                        }
                    }

                }
                if (toBeAdded) {
                    Broadcast broadcast = new Broadcast(
                            sendBroadcastRequest.getBroadcastType(), 1,
                            System.currentTimeMillis(),
                            BroadcastStatus.INITIATED, userId,
                            sendBroadcastRequest.getReferenceId());
                    listToBeBroadcasted.add(broadcast);
                }
            }
            logger.info(listToBeBroadcasted);

            if (listToBeBroadcasted.isEmpty()) {
                throw new VException(ErrorCode.NO_TEACHER_AVAILABLE,
                        "No teachers are available for grade : "
                        + doubt.getGrade() + ". Subject Id: "
                        + doubt.getBoardId());
            }

            broadcastDao.updateAll(listToBeBroadcasted);

            sendNotifications(listToBeBroadcasted,
                    sendBroadcastRequest.getBroadcastType(),
                    "ASK_A_DOUBT_BROADCAST");

            return new AsyncResult<>(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }

    }

//	private GetTeacherBoardMappingHierarchialRes getMappings(Long userId) {
//
//		String mappingUrl = GET_MAPPING_ENDPOINT + "?callingUserId=" + userId
//				+ "&userId=" + userId;
//
//		ClientResponse fosResp = WebUtils.INSTANCE.doCall(mappingUrl,
//				HttpMethod.GET, null);
//
//		try {
//			String res = fosResp.getEntity(String.class);
//			logger.debug(res);
//			if (fosResp.getStatus() != 200) {
//				// TODO throw custom exception
//				throw new Exception("getUser: getting Users failed");
//			}
//			GetTeacherBoardMappingHierarchialRes getTeacherBoardMappingHierarchialRes = new Gson()
//					.fromJson(res, GetTeacherBoardMappingHierarchialRes.class);
//
//			return getTeacherBoardMappingHierarchialRes;
//		} catch (Exception e) {
//			logger.error("unable to get mappings: " + e.getMessage());
//		}
//		return null;
//	}
//    @Async
    public Future<Boolean> updateExpiredBroadcast(
            SendBroadcastRequest sendBroadcastRequest) throws Exception {
        try {

            List<Broadcast> listToBeBroadcasted = new ArrayList<>();

            for (Long userId : sendBroadcastRequest.getUserIds()) {
                Broadcast broadcast = new Broadcast(
                        sendBroadcastRequest.getBroadcastType(), 1,
                        System.currentTimeMillis(), BroadcastStatus.INITIATED,
                        userId, sendBroadcastRequest.getReferenceId());
                listToBeBroadcasted.add(broadcast);
            }

            broadcastDao.updateAll(listToBeBroadcasted);

            sendNotifications(listToBeBroadcasted,
                    sendBroadcastRequest.getBroadcastType(), "");

        } catch (Exception e) {

        }

        return new AsyncResult<>(true);
    }

    public void sendNotifications(List<Broadcast> broadcastList,
            BroadcastType broadcastType, String notificationType) throws VException {

        List<SendBroadcastNotificationReq> sendBroadcastNotificationReqs = new ArrayList<SendBroadcastNotificationReq>();
        //String notificationUrl = SEND_NOTIFICATION_ENDPOINT;
        //notificationUrl = notificationUrl
        //		+ configUtils
        //				.getStringValue("fos.notification.askadoubt");

        for (Broadcast broadcast : broadcastList) {

            BroadcastInfo broadcastInfo = broadcastManager.getBroadcast(
                    broadcast.getId(), null, null, null, null, null, null,
                    null, null, null).get(0);

            SendAskADoubtNotificationInfo notificationInfo = null;

            switch (broadcastType) {
                case ASKADOUBT:
                    DoubtInfo doubtInfo = (DoubtInfo) broadcastInfo.getEntity();

                    SendAskADoubtNotificationInfo sendAskADoubtNotificationInfo = new SendAskADoubtNotificationInfo(
                            doubtInfo.getUser().getUserId(), null, null, null,
                            null, doubtInfo.getUser().getFirstName(),
                            broadcast.getId(), doubtInfo.getId(),
                            doubtInfo.getGrade(), doubtInfo.getSubject().getName());

                    if (doubtInfo.getAssignedToUser() != null) {
                        sendAskADoubtNotificationInfo.setTeacherId(doubtInfo
                                .getAssignedToUser().getUserId());
                        sendAskADoubtNotificationInfo.setTeacherFirstName(doubtInfo
                                .getAssignedToUser().getFirstName());
                        sendAskADoubtNotificationInfo.setTeacherLastName(doubtInfo
                                .getAssignedToUser().getLastName());
                        sendAskADoubtNotificationInfo
                                .setTeacherProfilePic(doubtInfo.getAssignedToUser()
                                        .getProfilePicUrl());
                    }

                    notificationInfo = sendAskADoubtNotificationInfo;

                    SendBroadcastNotificationReq sendBroadcastNotificationReq = new SendBroadcastNotificationReq(
                            broadcast.getToUserId(), NotificationType.valueOf(notificationType),
                            notificationInfo);
                    sendBroadcastNotificationReqs.add(sendBroadcastNotificationReq);
                    break;
                default:
            }

        }
        notificationManager.sendAskADoubtNotifications(sendBroadcastNotificationReqs);
//		ClientResponse fosResp = WebUtils.INSTANCE.doCall(notificationUrl,
//				HttpMethod.POST,
//				new Gson().toJson(sendBroadcastNotificationReqs));
//
//		try {
//			String res = fosResp.getEntity(String.class);
//			logger.debug(res);
//			if (fosResp.getStatus() != 200) {
//				// TODO throw custom exception
//				throw new Exception(res);
//			}
//			// GetTeacherBoardMappingHierarchialRes
//			// getTeacherBoardMappingHierarchialRes = new Gson()
//			// .fromJson(res, GetTeacherBoardMappingHierarchialRes.class);
//
//		} catch (Exception e) {
//			logger.error("unable to get mappings: " + e.getMessage());
//		}
    }

//    @Async
    public void markDoubtAccepted(Broadcast broadcast) {
        try {
            List<Broadcast> broadcastList = broadcastDao.get(null, null,
                    broadcast.getBroadcastType(), null, null, null,
                    BroadcastStatus.INITIATED, broadcast.getReferenceId(),
                    null, null);

            for (Broadcast tempBroadcast : broadcastList) {
                tempBroadcast
                        .setStatus(BroadcastStatus.ACCEPTED_BY_ANOTHER_USER);
                tempBroadcast.setLastUpdated(System.currentTimeMillis());
                broadcastDao.upsert(tempBroadcast);
            }

            sendNotifications(broadcastList, BroadcastType.ASKADOUBT,
                    "ASK_A_DOUBT_ACCEPTANCE_TEACHER");

        } catch (Exception e) {

        }
    }

}
