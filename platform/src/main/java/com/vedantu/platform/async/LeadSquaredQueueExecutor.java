/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.async;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.util.LogFactory;

/**
 *
 * @author somil
 */
@Service
public class LeadSquaredQueueExecutor implements IAsyncQueueExecutor{

    @Autowired
    public LogFactory logFactory;
    
    @Autowired
    LeadSquaredManager leadSquaredManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(LeadSquaredQueueExecutor.class);

    @Async("lsExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if(!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case LS_TASK:
                LeadSquaredRequest req = (LeadSquaredRequest) payload.get("request");
                leadSquaredManager.executeTask(req);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }
    
}
