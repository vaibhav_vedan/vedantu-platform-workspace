package com.vedantu.platform.controllers.dinero;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.dinero.request.ApplyCouponReq;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.managers.dinero.CouponManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/dinero/coupons")
public class CouponController {

    

    @Autowired
    private LogFactory logFactory;
    
    @Autowired
    CouponManager couponManager;
    
    @Autowired
    HttpSessionUtils sessionUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CouponController.class);

    @Deprecated
    @RequestMapping(value = "/applyCreditCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String applyCreditCoupon(@RequestBody ApplyCouponReq applyCouponReq)
            throws VException, IOException {
        logger.info("ENTRY: " + applyCouponReq);
        sessionUtils.checkIfAllowed(applyCouponReq.getCallingUserId(), null, Boolean.FALSE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/applyCreditCoupon",
                HttpMethod.POST, new Gson().toJson(applyCouponReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/isCouponCodeAvailable", method = RequestMethod.GET)
    @ResponseBody
    public String isCouponCodeAvailable(HttpServletRequest req)
            throws VException, IOException {
        logger.info("ENTRY: " + req.getQueryString());
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/isCouponCodeAvailable?"+req.getQueryString(),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/createCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String createCoupon(@RequestBody Object req)
            throws VException, IOException {
        logger.info("ENTRY: " + req);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/createCoupon", HttpMethod.POST,
                new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/editCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String editCoupon(@RequestBody Object req)
            throws VException, IOException {
        logger.info("ENTRY: " + req);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/editCoupon", HttpMethod.POST,
                new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/getCoupons", method = RequestMethod.GET)
    @ResponseBody
    public String getCoupons(HttpServletRequest request)
            throws VException {

        logger.info("ENTRY: " + request.getQueryString());
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint
                + "/coupons/getCoupons?" + request.getQueryString(),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/checkValidity", method = RequestMethod.POST)
    @ResponseBody
    public String checkValidity(@RequestBody Object req)
            throws VException, IOException {
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/checkValidity",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }


    
    //TODO: Check for usage
//    @RequestMapping(value = "/processCoupon", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public String processCoupon(@RequestBody ProcessCouponReq req) throws VException {
//        req.verify();
//        return couponManager.processCoupon(req);
//    }
}
