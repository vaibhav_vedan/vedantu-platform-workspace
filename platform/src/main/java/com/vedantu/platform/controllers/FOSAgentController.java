package com.vedantu.platform.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.platform.entity.FOSAgent;
import com.vedantu.platform.managers.FOSAgentManager;
import com.vedantu.platform.pojo.FOSAgentPOJO;
import com.vedantu.platform.request.FOSAgentReq;
import com.vedantu.platform.request.FosAgentRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.response.FOSAgentRes;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fosAgent")
public class FOSAgentController {
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FOSAgentController.class);

	@Autowired
	private FOSAgentManager fosAgentManager;

	@RequestMapping(value = "/getFOSAgentById",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FOSAgent getFOSEmployeeById(@RequestBody String  employeeCode){
		return fosAgentManager.getFOSAgentById(employeeCode);
	}

	@RequestMapping(value = "/getFOSAgentByIds",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<FOSAgent> getFOSEmployeeById(@RequestBody List<String> employeeCodes){
		return fosAgentManager.getFOSAgentByIds(employeeCodes);
	}

	@RequestMapping(value = "/createFOSAgent",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FOSAgent createFOSEmployee(@RequestBody FOSAgentReq fosAgent) throws BadRequestException {
		return fosAgentManager.createFOSAgent(fosAgent);
	}

	@RequestMapping(value="/getAgentForMatchingEmailString/{email}",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<FOSAgent> getEmployeesMatchingEmailId(@PathVariable("email") String email){
		return fosAgentManager.getAgentForMatchingEmailString(email);
	}

	@RequestMapping(value="/getEmployeesMatchingEmailStringOrAgentCode/{pattern}",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<FOSAgent> getEmployeesMatchingEmailStringOrAgentCode(@PathVariable("pattern") String pattern){
		return fosAgentManager.getEmployeesMatchingEmailStringOrAgentCode(pattern);
	}

	@RequestMapping(value = "/createFOSAgentInBulk",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<FOSAgentPOJO> createFOSEmployeeInBulk(@RequestBody FOSAgentReq fosAgentsReq) throws BadRequestException {
		fosAgentsReq.verify();
		return fosAgentManager.createFOSAgentInBulk(fosAgentsReq);
	}

	@RequestMapping(value = "/getFOSAgentsByEmails" , method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<FOSAgentRes> getFOSEmployeeByEmailOrEmployeeCode(@RequestBody FosAgentRequest req){
		logger.info("request " + req.toString());
		return fosAgentManager.getFOSEmployeeByEmailOrEmployeeCode(req);
	}
}
