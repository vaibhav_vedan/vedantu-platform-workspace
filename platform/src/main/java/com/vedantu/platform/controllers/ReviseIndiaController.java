package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.pojo.ReviseIndiaReq;
import com.vedantu.platform.pojo.leadsquared.LeadActivityReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping("/reviseindia")
public class ReviseIndiaController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ReviseIndiaController.class);

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/reviseIndiaActivityPush", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public String reviseIndiaActivityPush(@RequestBody List<ReviseIndiaReq> request) throws VException, InterruptedException {

        sessionUtils.checkIfAllowed(null, Role.ADMIN,true);
        if (CollectionUtils.isNotEmpty(request)) {
            logger.info("Request : reviseIndiaActivityPush {}", request);
            leadSquaredManager.reviseIndiaActivityPush(request);
        }
        return "SUCCESS";
    }
}
