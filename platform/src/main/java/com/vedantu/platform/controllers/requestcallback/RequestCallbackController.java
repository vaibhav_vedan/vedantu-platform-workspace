package com.vedantu.platform.controllers.requestcallback;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.enums.requestcallback.LeadAssignee;
import com.vedantu.platform.enums.requestcallback.TeacherLeadStatus;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.requestcallback.request.AddRequestCallbackDetailsReq;
import com.vedantu.platform.pojo.requestcallback.request.GetRequestCallBacksDetailsReq;
import com.vedantu.platform.pojo.requestcallback.request.UpdateTeacherStatusRequestCallbackReq;
import com.vedantu.platform.pojo.requestcallback.response.RequestCallbackDetailsRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping("/requestcallback")
public class RequestCallbackController {

	@Autowired
	RequestCallbackManager requestCallbackManager;

	@Autowired
	HttpSessionUtils sessionUtils;

        @Autowired
        LogFactory logFactory;
        
        Logger logger = logFactory.getLogger(RequestCallbackController.class);

        
        

	@ApiOperation(value = "get Request Callback", notes = "Returns get Request Callback results")
	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<RequestCallbackDetailsRes> getRequestCallback(GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq) 
                throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		try {
			getRequestCallBacksDetailsReq.verify();
			List<RequestCallbackDetailsRes> res = requestCallbackManager
					.getRequestCallBacksDetails(getRequestCallBacksDetailsReq);
			if (res != null) {
				return res;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR, "Get Request CallBack failed");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "Create Request Callback", notes = "Returns created Request Callback status")
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public RequestCallBackDetails createRequestCallback(
			@RequestBody AddRequestCallbackDetailsReq addRequestCallbackDetailsReq) throws Exception {		
		try {
			addRequestCallbackDetailsReq.verify();
			RequestCallBackDetails result = requestCallbackManager.addRequestCallback(addRequestCallbackDetailsReq);
			if (result != null) {
				return result;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR, "Request CallBack Addition failed");
			}

		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "Update Teacher lead status", notes = "Returns Update Teacher lead details updated")
	@RequestMapping(value = "/updateStatus", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public RequestCallbackDetailsRes updateTeacherLeadStatus(
			@RequestBody UpdateTeacherStatusRequestCallbackReq updateTeacherStatusRequestCallbackReq) throws Exception {
		try {
			updateTeacherStatusRequestCallbackReq.verify();
			RequestCallbackDetailsRes res = requestCallbackManager
					.updateTeacherStatus(updateTeacherStatusRequestCallbackReq);
			if (res != null) {
				return res;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR, "Request CallBack updation failed");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "get Teacher lead statuses", notes = "Returns Teacher lead statuses")
	@RequestMapping(value = "/getAllStatuses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public Map<String, TeacherLeadStatus> getAllTeacherLeadStatuses() throws Exception {
		try {
			Map<String, TeacherLeadStatus> res = requestCallbackManager.getAllStatusMessages();
			if (res != null) {
				return res;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR, "get Teacher lead statuses failed");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "update call status", notes = "Returns Teacher lead statuses")
	@RequestMapping(value = "/updateCallStatus", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public RequestCallbackDetailsRes updateCallStatus(
			@RequestParam(value = "callingUserId", required = false) Long callingUserId,
			@RequestParam(value = "id", required = false) String id) throws Exception {
		try {
			RequestCallbackDetailsRes requestCallBackDetailsRes = requestCallbackManager.updateCallingStatus(id);
			return requestCallBackDetailsRes;
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "index rcb details to es before a time", notes = "Returns void")
	@RequestMapping(value = "/updateRCBsToES", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public void updateRCBsToES(@RequestParam(value = "start", required = true) int start,
			@RequestParam(value = "size", required = true) int size,
			@RequestParam(value = "time", required = true) Long time) throws Exception {
		try {
			requestCallbackManager.migrateRCBDetailsToES(start, size, time);
		} catch (Exception e) {
			throw e;
		}
	}
        
        
    @RequestMapping(value = "/updateLeadsSessionSubscription", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateLeadsSessionSubscription(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            //String subject = subscriptionRequest.getSubject();
            //String message = subscriptionRequest.getMessage();
            requestCallbackManager.updateLeadsSessionSubscription();
        }
    }

}
