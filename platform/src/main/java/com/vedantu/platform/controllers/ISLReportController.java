/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.google.common.util.concurrent.RateLimiter;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactoryISL;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.mongodbentities.ISLReport;
import com.vedantu.platform.mongodbentities.ISLReport.Section;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/isl")
public class ISLReportController extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    @Autowired
    private MongoClientFactoryISL mongoClientFactory;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public static int RATE_LIMIT = 3;
    private static RateLimiter rateLimiter = RateLimiter.create(RATE_LIMIT);

    public String[] hyderabadVariations =
            {"Hyd", "Hyderabad", "Hderabad", "Hyd", "Hyderbad", "Hyderebad", "hydreabad", "hydrabad" , "hyedrabad", "hyderabad`", "hyderabah"};


    private Map<String, Integer> rankMap = new HashMap<>();

    public ISLReportController() {
        rankMap.put("category_6_7_day1", 4218);
        rankMap.put("category_6_7_day2", 4218);
        rankMap.put("category_8_9_day1", 3615);
        rankMap.put("category_8_9_day2", 3615);
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    Logger logger = logFactory.getLogger(ISLReportController.class);

    @RequestMapping(value = "/getReport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public List<ISLReport> getReport(@RequestParam(name = "id", required = true) Long id,
            @RequestParam(name = "studentName", required = false) String studentName,
            @RequestParam(name = "parentEmail", required = false) String parentEmail) throws VException {
        ISLReport report;
        if (id != null) {
        	Query query = new Query();
        	query.addCriteria(Criteria.where("userId").is(id));
        	query.with(Sort.by(Sort.Direction.DESC,
                    "attemptedOn"));
            List<ISLReport> reports = runQuery(query, ISLReport.class);
            if (ArrayUtils.isEmpty(reports)) {
            	throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "report not found");
            }
            return reports;
//            report = getEntityById(id, ISLReport.class);
        }/* else if (StringUtils.isNotEmpty(parentEmail) && StringUtils.isNotEmpty(studentName)) {
            Query query = new Query();
            parentEmail = parentEmail.toLowerCase();
            studentName = studentName.toLowerCase();
            query.addCriteria(Criteria.where("parentEmail").is(parentEmail));
            query.addCriteria(Criteria.where("studentName").is(studentName));
            List<ISLReport> reports = runQuery(query, ISLReport.class);
            if (ArrayUtils.isNotEmpty(reports)) {
                report = reports.get(0);
            } else {
                query = new Query();
                query.addCriteria(Criteria.where("parentEmail").is(parentEmail));
                List<ISLReport> reports2 = runQuery(query, ISLReport.class);
                if (ArrayUtils.isNotEmpty(reports2)) {
                    report = reports2.get(0);
                } else {
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "report not found");
                }
            }
        }*/ else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "req params not correct ");
        }
//        Section sec=report.getTestMarksData();
//        if(rankMap.containsKey(report.getTestCode())){
//            sec.setRankOutOf(rankMap.get(report.getTestCode()));
//        }
//       report.setTestMarksData(sec);
//        List<ISLReport> reports = new ArrayList<>();
//        reports.add(report);
//        return reports;
    }

    @RequestMapping(value = "/sendEmails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes sendEmails(@RequestHeader(value = "key") String key,
            @RequestParam(name = "start", required = true) Integer start,
            @RequestParam(name = "size", required = true) Integer size) throws VException {
        if (key.equals("xgA9me8EvwCccX5D")) {
            Query query = new Query();
            query.with(Sort.by(Sort.Direction.ASC, AbstractMongoEntity.Constants.CREATION_TIME));
            setFetchParameters(query, start, size);
            List<ISLReport> reports = runQuery(query, ISLReport.class);
            if (ArrayUtils.isNotEmpty(reports)) {
                for (ISLReport report : reports) {
                    String city = report.getCity();
                    if(isHyderabad(city)) {
                        Map<String, Object> payload = new HashMap<>();
                        payload.put("report", report);
                        double sleptSeconds = rateLimiter.acquire();
                        logger.info(" sleptSeconds: " + sleptSeconds);
                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ISL_REPORT_EMAIL, payload);
                        asyncTaskFactory.executeTask(params);
                    }
                }
            } else {
                logger.info("no reports to send emails");
            }
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "req params not correct ");
        }
        return new BasicRes();
    }


    private boolean isHyderabad(String city) {
        for(String variation: hyderabadVariations) {
            if(variation.equalsIgnoreCase(city)) {
                logger.info("City is hyderabad: "+city);
                return true;
            }
        }
        return false;
    }
    
    @RequestMapping(value = "/getTests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public List getTests() {        
       return new ArrayList<>();

    }

}
