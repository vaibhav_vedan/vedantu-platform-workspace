package com.vedantu.platform.controllers.listing;

import com.google.gson.Gson;
import com.vedantu.platform.listing.manager.FeatureMappingManager;
import com.vedantu.platform.listing.manager.ParamManager;
import com.vedantu.platform.listing.pojo.WeightedParam;
import com.vedantu.platform.listing.viewobject.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.managers.listing.ListingManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@RestController
@RequestMapping("/listing/oto")
public class ListingController {

    

    @Autowired
    private ListingManager listingManager;
    
    @Autowired
    private HttpSessionUtils httpSessionUtils;    

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FeatureMappingManager featureMappingManager;

    @Autowired
    private ParamManager paramManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ListingController.class);

//	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String getTeachers(@RequestBody String GTR) throws IllegalAccessException {
//		String listingEndpoint = ConfigUtils.INSTANCE.getStringValue("LISTING_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(listingEndpoint + "teachers/getTeachers/3", HttpMethod.POST,
//				GTR);
//		return resp.getEntity(String.class);
//	}
//    @RequestMapping(value = "/updateFeatureMappingWeight", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public String updateFeatureMappingWeight(@RequestBody String weightedParam
//    ) throws Exception {
//        String listingEndpoint = ConfigUtils.INSTANCE.getStringValue("LISTING_ENDPOINT");
//        ClientResponse resp = WebUtils.INSTANCE.doCall(listingEndpoint + "featureMapping/updateFeatureMappingWeight", HttpMethod.POST,
//                weightedParam);
//        return resp.getEntity(String.class);
//    }

    @RequestMapping(value = "/updateFeatureMappingWeight", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse updateFeatureMappingWeight(@RequestBody WeightedParam weightedParam
    ) throws Exception {
        BaseResponse basicRes = featureMappingManager.updateFeatureMappingWeight(weightedParam);
        return basicRes;
    }

//    @RequestMapping(value = "/getFeatureParams", method = RequestMethod.GET)
//    @ResponseBody
//    public String getFeatureParams(@RequestParam(value = "featureName", required = true) String featureName) throws Exception {
//        String listingEndpoint = ConfigUtils.INSTANCE.getStringValue("LISTING_ENDPOINT");
//        ClientResponse resp = WebUtils.INSTANCE.doCall(listingEndpoint + "param/getFeatureParams?featureName=" + featureName, HttpMethod.GET,
//                "");
//        return resp.getEntity(String.class);
//    }

    @RequestMapping(value = "/getFeatureParams", method = RequestMethod.GET)
    @ResponseBody
    public List<WeightedParam> getFeatureParams(
            @RequestParam(value = "featureName", required = true) String featureName) throws Exception {
        logger.info("Request received for getting listing params: " + featureName);
        List<WeightedParam> weightedParams = paramManager.getlistingParamsforFeature(featureName);
        return weightedParams;
    }

    @RequestMapping(value = "/getTeachers", method = RequestMethod.GET)
    @ResponseBody
    public String getTeachers(
            @RequestParam(value = "boardId", required = false) String boardId,
            @RequestParam(value = "grade", required = false) String grade,
            @RequestParam(value = "target", required = false) String target,
            @RequestParam(value = "subject", required = false) String subject,
            @RequestParam(value = "filters") String filters,
            @RequestParam(value = "sortingParams", required = false) String sortingParams,
            @RequestParam(value = "start") String start,
            @RequestParam(value = "size") String size,
            @RequestParam(value = "offeringId", required = false) Long offeringId) throws Exception {

        return listingManager.getTeachers(boardId, grade, target, subject, filters, sortingParams, start, size, offeringId);
    }

    @RequestMapping(value = "/filtertosteachers", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void filtertosteachers(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            //String subject = subscriptionRequest.getSubject();
            //String message = subscriptionRequest.getMessage();
            listingManager.filtertosteachers();
        }
    }

    @RequestMapping(value = "/filtertosteachersmanually", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void filtertosteachers() throws VException {
        httpSessionUtils.checkIfAllowed(null, null, true);
        listingManager.filtertosteachers();
    }

    @RequestMapping(value = "/updateDailyCount", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateDailyCount(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            //String subject = subscriptionRequest.getSubject();
            //String message = subscriptionRequest.getMessage();
            listingManager.updateDailyCount();
        }
    }

}
