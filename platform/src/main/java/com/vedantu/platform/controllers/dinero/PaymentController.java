package com.vedantu.platform.controllers.dinero;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.response.*;
import com.vedantu.exception.BadRequestException;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.platform.managers.notification.CommunicationManager;
import org.apache.commons.lang3.EnumUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.ExtTransactionPojo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.subscription.SubscriptionRequestManager;
import com.vedantu.platform.pojo.dinero.PaymentGateway;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestPaymentReq;
import com.vedantu.platform.request.dinero.AddPaymentGatewayReq;
import com.vedantu.platform.request.dinero.GetMyOrdersReq;
import com.vedantu.platform.request.dinero.GetOrderInfoReq;
import com.vedantu.platform.request.dinero.ModifyPaymentGatewayReq;
import com.vedantu.platform.request.dinero.OnPaymentReceiveReq;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.response.dinero.GetMyOrdersRes;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.subscription.pojo.SubscriptionMetric;
import com.vedantu.subscription.request.GetRegisteredCoursesReq;
import com.vedantu.subscription.request.ResetInstallmentStateReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

import io.swagger.annotations.ApiOperation;
import org.json.JSONException;

@EnableAsync
@RestController
@RequestMapping("/dinero/payment")
public class PaymentController {

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @Autowired
    SubscriptionRequestManager subscriptionRequestManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentController.class);

    private Gson gson = new Gson();

    @RequestMapping(value = "/buyItems", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo buyItems(@RequestBody BuyItemsReq buyItemsReq) throws VException, IOException, AddressException {
        sessionUtils.checkIfAllowed(buyItemsReq.getCallingUserId(), null, Boolean.FALSE);
        logger.info("Entering " + buyItemsReq.toString());
        OrderInfo response = paymentManager.buyItems(buyItemsReq);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    @Deprecated
    @RequestMapping(value = "/getMyOrders", method = RequestMethod.GET)
    @ResponseBody
    public GetMyOrdersRes getMyOrders(GetMyOrdersReq getMyOrdersReq) throws VException {
        sessionUtils.checkIfAllowed(getMyOrdersReq.getCallingUserId(), null, Boolean.FALSE);
        logger.info("Entering " + getMyOrdersReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getMyOrders", HttpMethod.POST,
                new Gson().toJson(getMyOrdersReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetMyOrdersRes response = new Gson().fromJson(jsonString, GetMyOrdersRes.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @RequestMapping(value = "/getOrderInfo", method = RequestMethod.GET)
    @ResponseBody
    public OrderInfo getOrderInfo(GetOrderInfoReq getOrderInfoReq) throws VException {

        logger.info("Entering " + getOrderInfoReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrderInfo", HttpMethod.POST,
                new Gson().toJson(getOrderInfoReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        OrderInfo response = new Gson().fromJson(jsonString, OrderInfo.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    // @CrossOrigin(origins = "*")
    @RequestMapping(value = "/onPaymentReceived/**", method = {RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
    public void onPaymentReceived(HttpServletRequest request, HttpServletResponse httpResponse)
            throws VException, IOException {

        // TODO: Discuss auth
        try {
            String action = request.getRequestURI();
            action = action.substring(action.lastIndexOf("/") + 1);
            OnPaymentReceiveReq onPaymentReceiveReq = new OnPaymentReceiveReq(request.getParameterMap(), action);
            logger.info("Entering " + onPaymentReceiveReq.toString());
            PaymentReceiveResponse response = paymentManager.onPaymentReceived(onPaymentReceiveReq);
            ExtTransactionPojo transaction = response.getTransaction();
            String redirectUrl = transaction.getRedirectUrl();
            if (transaction.getVedantuOrderId() != null && transaction.getVedantuOrderId() != null
                    && StringUtils.isNotEmpty(redirectUrl)) {
                redirectUrl += (redirectUrl.contains("?") ? "&" : "?");
                if (transaction.getPaymentType() != null
                        && transaction.getPaymentType().equals(PaymentType.INSTALMENT)) {
                    redirectUrl += "instalmentId=" + transaction.getVedantuOrderId() + "&st=" + transaction.getStatus();
                } else {
                    redirectUrl += "orderId=" + transaction.getVedantuOrderId() + "&st=" + transaction.getStatus();
                }
            }
            if (StringUtils.isEmpty(redirectUrl)) {
                redirectUrl = "/";
            }

            if (!redirectUrl.startsWith("http")  ) {

                redirectUrl = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT") + redirectUrl;
            }

            if (TransactionRefType.SUBSCRIPTION_REQUEST.equals(transaction.getRefType())) {
                SubscriptionRequestPaymentReq req = new SubscriptionRequestPaymentReq();
                req.setSubscriptionRequestId(Long.parseLong(transaction.getRefId()));
                req.setUserId(transaction.getUserId());
                req.setCallingUserId(transaction.getUserId());
                subscriptionRequestManager.handlePayments(req);
            }
            if( transaction.getGatewayName() == null || !transaction.getGatewayName().equals(PaymentGatewayName.PAYTM_MINI)) {
                httpResponse.sendRedirect(httpResponse.encodeRedirectURL(redirectUrl));
            }

            // logger.info("Exiting: " + response.toString());
        } catch (Exception e) {
            logger.warn("onPaymentReceived", e);
            ErrorCode errorCode = ErrorCode.SERVICE_ERROR;
            String displayMessage = "Something went wrong.";
            try {
                if (e instanceof VException) {
                    VException vex = (VException) e;
                    errorCode = vex.getErrorCode();
                }
            } catch (Exception ex) {
                logger.warn("onPaymentReceived", ex);
            }

            if (ErrorCode.TRANSACTION_ALREADY_PROCESSED.equals(errorCode)) {
                displayMessage = "Transaction is already processed.";
            }
            // request.setAttribute("errorCode", errorCode);
            // request.setAttribute("message", message);
            request.setAttribute("displayMessage", displayMessage);
            httpResponse.sendRedirect(httpResponse.encodeRedirectURL(ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT")
                    + "/500?displayMessage=" + URLEncoder.encode(displayMessage, "UTF-8")));
            // request.getRequestDispatcher("/WEB-INF/ui/payment/500.jsp").forward(request,
            // response);
        }
    }

    @Deprecated
    @RequestMapping(value = "/rechargeAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RechargeUrlInfo rechargeAccount(@RequestBody RechargeReq rechargeReq) throws VException {
        rechargeReq.verify();
        logger.info("Entering " + rechargeReq.toString());
        rechargeReq.setUserId(rechargeReq.getCallingUserId());
        sessionUtils.checkIfAllowed(rechargeReq.getUserId(), null, Boolean.FALSE);
        /*
		 * String dineroEndpoint =
		 * ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		 * ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
		 * "/payment/rechargeAccount", HttpMethod.POST, new
		 * Gson().toJson(rechargeReq));
		 * 
		 * VExceptionFactory.INSTANCE.parseAndThrowException(resp); String
		 * jsonString = resp.getEntity(String.class);
		 * 
		 * RechargeUrlInfo response = new Gson().fromJson(jsonString,
		 * RechargeUrlInfo.class);
         */
        RechargeUrlInfo response = paymentManager.rechargeAccount(rechargeReq);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @Deprecated
    @RequestMapping(value = "/addPaymentGateways", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentGateway addPaymentGateways(@RequestBody AddPaymentGatewayReq addPaymentGatewayReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + addPaymentGatewayReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/addPaymentGateways", HttpMethod.POST,
                new Gson().toJson(addPaymentGatewayReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        PaymentGateway response = new Gson().fromJson(jsonString, PaymentGateway.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @Deprecated
    @RequestMapping(value = "/getPaymentGateways", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PaymentGateway> getPaymentGateways() throws VException {
        logger.info("Entering ");
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getPaymentGateways", HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        List<PaymentGateway> response = new Gson().fromJson(jsonString, List.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @Deprecated
    @RequestMapping(value = "/modifyPaymentGateway", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentGateway modifyPaymentGateway(@RequestBody ModifyPaymentGatewayReq modifyPaymentGatewayReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + modifyPaymentGatewayReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/modifyPaymentGateway",
                HttpMethod.POST, new Gson().toJson(modifyPaymentGatewayReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        PaymentGateway response = new Gson().fromJson(jsonString, PaymentGateway.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @RequestMapping(value = "/deletePaymentGateway", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean deletePaymentGateway(@RequestBody String paymentGatewayId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + paymentGatewayId.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/deletePaymentGateway",
                HttpMethod.POST, new Gson().toJson(paymentGatewayId));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Boolean response = new Gson().fromJson(jsonString, Boolean.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    // @RequestMapping(value = "/getTransactions", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public List<Transaction> getTransactions(@RequestBody GetTransactionsReq
    // getTransactionsReq) throws VException {
    //
    // logger.info("Entering " + getTransactionsReq.toString());
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/payment/getTransactions", HttpMethod.POST,
    // new Gson().toJson(getTransactionsReq));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // List<Transaction> response = new Gson().fromJson(jsonString, List.class);
    // logger.info("Exiting: " + response.toString());
    // return response;
    //
    // }
    // @RequestMapping(value = "/getOrders", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public List<Orders> getOrders(@RequestBody GetOrdersReq getOrdersReq)
    // throws VException {
    //
    // logger.info("Entering " + getOrdersReq.toString());
    // return paymentManager.getOrders(getOrdersReq);
    //
    // }
    // @CrossOrigin
    @RequestMapping(value = "/getOrdersBySubscriptionIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<Long, List<Orders>> getOrdersBySubscriptionIds(@RequestParam(value = "ids") List<Long> ids)
            throws VException {

        logger.info("ENTRY");
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        Map<Long, List<Orders>> response = paymentManager.getOrdersBySubscriptionIds(ids);
        logger.info("EXIT");
        return response;

    }

    @RequestMapping(value = "/subscriptionMetrices", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void subscriptionMetrices(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getSubscriptionMetrices",
                    HttpMethod.GET, null);

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);

            Type SubscriptionMetricType = new TypeToken<List<SubscriptionMetric>>() {
            }.getType();
            List<SubscriptionMetric> subscriptionMetrics = new Gson().fromJson(jsonString, SubscriptionMetricType);
            StringWriter stringWriter = new StringWriter();
            String fileName = "SubscriptionMetrices-"
                    + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
            ICsvBeanWriter csvWriter = new CsvBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
            String[] header = {"Status", "Date", "SubscriptionId", "Model", "PurchasedHours", "Amount", "StudentId",
                "TeacherId"};
            csvWriter.writeHeader(header);
            for (SubscriptionMetric subscriptionMetric : subscriptionMetrics) {
                csvWriter.write(subscriptionMetric, header);
            }
            csvWriter.close();
            /*
			 * BufferedReader reader = new BufferedReader(new
			 * FileReader(fileName)); String line = null; StringBuilder
			 * stringBuilder = new StringBuilder(); String ls =
			 * System.getProperty("line.separator"); while ((line =
			 * reader.readLine()) != null) { stringBuilder.append(line);
			 * stringBuilder.append(ls); } // delete the last ls
			 * stringBuilder.deleteCharAt(stringBuilder.length() - 1);
             */

            // Send email via notification centre
            logger.info("Sending email via notification-centre");
            ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
            String emailTos = ConfigUtils.INSTANCE.getStringValue("payment.getSubscriptionMetrices.toEmailIds");
            String[] emails = emailTos.split(",");
            for (String email : emails) {
                to.add(new InternetAddress(email));
            }
            String body = "Please find the attached Daily Subscription Metirces";
            EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "Subscription");
            attachment.setApplication("application/csv");
            attachment.setAttachmentData(stringWriter.toString());
            CommunicationType type = CommunicationType.USER_MESSAGE;
            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setTo(to);
            emailRequest.setBody(body);
            String env = "";
            if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD")) {
                env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
            }
            emailRequest.setSubject(env + "Subscription Metrices Daily Report");
            emailRequest.setType(type);
            emailRequest.setAttachment(attachment);

            String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
            resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                    gson.toJson(emailRequest));
            logger.info(resp.getEntity(String.class));
        }
    }

    @ApiOperation(value = "getInstalmentConfigProps")
    @RequestMapping(value = "getInstalmentConfigProps", method = RequestMethod.GET)
    @ResponseBody
    public String getInstalmentConfigProps(HttpServletRequest request) throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("ENTRY: getInstalmentConfigProps: " + request.getQueryString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/getInstalmentConfigProps?" + request.getQueryString(), HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        return jsonString;
    }

    @ApiOperation(value = "getSuitableInstalmentConfigProps")
    @RequestMapping(value = "getSuitableInstalmentConfigProps", method = RequestMethod.GET)
    @ResponseBody
    public String getSuitableInstalmentConfigProps(HttpServletRequest request) throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("ENTRY: getSuitableInstalmentConfigProps: " + request.getQueryString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/getSuitableInstalmentConfigProps?" + request.getQueryString(),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        return jsonString;
    }

    @ApiOperation(value = "setInstalmentConfigProps")
    @RequestMapping(value = "setInstalmentConfigProps", method = RequestMethod.POST)
    @ResponseBody
    public String setInstalmentConfigProps(@RequestBody Object instalmentConfigPropsReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("ENTRY: " + instalmentConfigPropsReq);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/setInstalmentConfigProps",
                HttpMethod.POST, new Gson().toJson(instalmentConfigPropsReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    @ApiOperation(value = "getInstalments")
    @RequestMapping(value = "getInstalments", method = RequestMethod.GET)
    @ResponseBody
    public String getInstalments(HttpServletRequest request) throws VException, IOException {
        logger.info("ENTRY: " + request.getQueryString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(dineroEndpoint + "/payment/getInstalments?" + request.getQueryString(), HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        return jsonString;
    }

    @ApiOperation(value = "getInstalment")
    @RequestMapping(value = "getInstalment/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getInstalment(@PathVariable("id") String id) throws VException, IOException {
        logger.info("ENTRY: " + id);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getInstalment/" + id, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        return jsonString;
    }

    @ApiOperation(value = "prepareInstalments")
    @RequestMapping(value = "prepareInstalments", method = RequestMethod.POST)
    @ResponseBody
    public String prepareInstalments(@RequestBody Object req) throws VException, IOException {
        logger.info("ENTRY: " + req);

        JsonObject jsonObject = gson.fromJson(gson.toJson(req), JsonObject.class);

        if(jsonObject == null || !jsonObject.has("instalmentPurchaseEntity"))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Instalment Purchase Entity reqest");

        String purchaseEntity = jsonObject.get("instalmentPurchaseEntity").getAsString();
        if(!EnumUtils.isValidEnum(InstalmentPurchaseEntity.class, purchaseEntity))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Instalment Purchase Entity reqest");

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/prepareInstalments", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }


    @ApiOperation(value = "payInstalment")
    @RequestMapping(value = "payInstalment", method = RequestMethod.POST)
    @ResponseBody
    public PayInstalmentRes payInstalment(@RequestBody PayInstalmentReq req) throws VException, IOException {
        sessionUtils.checkIfAllowedList(req.getUserId(), null, Boolean.FALSE);
        return paymentManager.payInstalment(req);
    }

    @RequestMapping(value = "/checkInstalmentDues", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void performDineroTasks(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            paymentManager.performDineroTasks(0, 1000);
        }
    }

//    @RequestMapping(value = "/checkInstalmentDuesManually", method
//            = RequestMethod.GET)
//    @ResponseBody
//    public boolean checkInstalmentDuesManually() throws Throwable {
//        paymentManager.checkInstalmentDues(0, 10);
//        return true;
//    }
    // TODO: Dinero is calling it! Need to remove the call because
    // Authentication will fail the call
    @RequestMapping(value = "sendInstalmentPaidEmail", method = RequestMethod.POST)
    @ResponseBody
    public void sendInstalmentPaidEmail(@RequestBody SendInstalmentPaidEmailReq req)
            throws VException, UnsupportedEncodingException {
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.INSTALMENT_PAID_EMAIL, payload);
        asyncTaskFactory.executeTask(params);
    }

    @RequestMapping(value = "/getOrderDetails", method = RequestMethod.GET)
    @ResponseBody
    public OrderDetails getOrderDetails(GetOrderDetailsReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.TRUE);
        return paymentManager.getOrderDetails(req);
    }

    @RequestMapping(value = "/getRegisteredCourses", method = RequestMethod.GET)
    @ResponseBody
    public List<RegisteredCourse> getRegisteredCourses(GetRegisteredCoursesReq req) throws
            VException, IllegalArgumentException, IllegalAccessException, JSONException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), null, Boolean.TRUE);
        return paymentManager.getRegisteredCourses(req);
    }

    @RequestMapping(value = "/getInstalmentDetailsOTF", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetInstalmentDetailsOTFRes getInstalmentDetailsOTF(@RequestParam(value = "courseId") String courseId,
            @RequestParam(value = "batchId") String batchId,
            @RequestParam(value = "userId") String userId) throws VException {
        sessionUtils.checkIfAllowed(Long.parseLong(userId), null, Boolean.TRUE);
        return paymentManager.getInstalmentDetailsOTF(courseId, batchId, userId);

    }

    @RequestMapping(value = "/resetInstallmentState", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse resetInstallmentState(@RequestBody ResetInstallmentStateReq req) throws
            VException, CloneNotSupportedException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.resetInstallmentState(req);
    }

    //this api is only used for OTF
    @RequestMapping(value = "/changeInstalmentDueDate", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse changeInstalmentDueDate(@RequestBody ChangeInstalmentDueDateReq req) throws
            VException, CloneNotSupportedException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.changeInstalmentDueDate(req);
    }

    @RequestMapping(value = "/getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GetOrderUserInfoRes> getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded(GetOrdersWithoutDeliverableEntityReq req) throws VException {

        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return paymentManager.getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded(req);
    }

    @RequestMapping(value = "/getDueInstalmentsForContextTypeAndTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GetDueInstalmentsForTimeRes> getDueInstalmentsForContextTypeAndTime(GetDueInstalmentsForTimeReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return paymentManager.getDueInstalmentsForContextTypeAndTime(req);
    }

    @RequestMapping(value = "/getAgentClosedSalesForTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AgentClosedSalesRes> getAgentClosedSalesForTime(GetDueInstalmentsForTimeReq req) throws VException, InterruptedException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return paymentManager.getAgentClosedSalesForTime(req);
    }
    @RequestMapping(value = "/sendDescripancyEmail", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendDescripancyEmail(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                     @RequestBody String request) throws VException, InterruptedException, UnsupportedEncodingException, AddressException {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {
            communicationManager.sendDescripancyEmail();
        }
    }

}
