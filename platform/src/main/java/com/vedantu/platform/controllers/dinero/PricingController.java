package com.vedantu.platform.controllers.dinero;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.TeacherCurrentStatus;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.dinero.request.GetAllPricingChangeRequestsReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.managers.dinero.PricingManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.pojo.dinero.TeacherRequest;
import com.vedantu.platform.request.dinero.ApprovePricingChangeRequest;
import com.vedantu.platform.request.dinero.CreatePricingChangeRequest;
import com.vedantu.platform.response.dinero.GetAllRequestsResponse;
import com.vedantu.platform.response.dinero.GetPricingChangeRequestsResponse;
import com.vedantu.platform.response.dinero.GetTeacherPlansResponse;
import com.vedantu.platform.response.dinero.GetTeacherSlabsResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("/dinero/pricing")
public class PricingController {

    @Autowired
    FosUtils fosUtils;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    UserManager userManager;

    @Autowired
    PricingManager pricingManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PricingController.class);

    //TODO: Check usage
	// @RequestMapping(value = "/setSlabRanges", method = RequestMethod.POST,
	// consumes = MediaType.APPLICATION_JSON_VALUE, produces =
	// MediaType.APPLICATION_JSON_VALUE)
	// @ResponseBody
	// public BaseResponse setSlabRanges(@RequestBody CreateSlabRangesRequest
	// request) throws VException {
	//
	// String dineroEndpoint =
	// ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
	// ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
	// "/pricing/setSlabRanges", HttpMethod.POST,
	// new Gson().toJson(request));
	//
	// VExceptionFactory.INSTANCE.parseAndThrowException(resp);
	// String jsonString = resp.getEntity(String.class);
	//
	// BaseResponse response = new Gson().fromJson(jsonString,
	// BaseResponse.class);
	// logger.info("Exiting: " + response.toString());
	// return response;
	// }

    @RequestMapping(value = "/createPricingChangeRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse createPricingChangeRequest(@RequestBody CreatePricingChangeRequest request) throws VException {

        sessionUtils.checkIfAllowed(null, Role.TEACHER, Boolean.FALSE);
        String url = "/pricing/createPricingChangeRequest";
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        Long userId = sessionData.getUserId();
        if (userId != null) {
            UserInfo userInfo = userManager.getUserInfo(userId);
            if (userInfo != null && Role.TEACHER.equals(userInfo.getRole()) && userInfo.getInfo() != null) {
                TeacherInfo teacherInfo = (TeacherInfo) userInfo.getInfo();
                if (TeacherCurrentStatus.UNLISTED.equals(teacherInfo.getCurrentStatus())) {
                    url = "/pricing/createTeacherPlans";
                }
            }
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url,
                HttpMethod.POST, new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        BaseResponse response = new Gson().fromJson(jsonString, BaseResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    //Should be moved to manager, API not needed, used in OfferingManager
//	@RequestMapping(value = "/updateCustomPlan", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String updateCustomPlan(@RequestBody UpdateCustomPlanRequest request) throws VException {
//		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/updateCustomPlan", HttpMethod.POST,
//				new Gson().toJson(request));
//		String jsonString = resp.getEntity(String.class);
//		logger.info("Exiting: " + jsonString);
//		return jsonString;
//	}
    @RequestMapping(value = "/approvePricingChangeRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse approvePricingChangeRequest(@RequestBody ApprovePricingChangeRequest request)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return pricingManager.approvePricingChangeRequest(request);
    }

    //TODO: Check usage
//	@RequestMapping(value = "/createTeacherPlans", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public BaseResponse createTeacherPlans(@RequestBody CreatePricingChangeRequest request) throws VException {
//		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/createTeacherPlans", HttpMethod.POST,
//				new Gson().toJson(request));
//
//		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//		String jsonString = resp.getEntity(String.class);
//
//		BaseResponse response = new Gson().fromJson(jsonString, BaseResponse.class);
//		logger.info("Exiting: " + response.toString());
//		return response;
//	}
    @RequestMapping(value = "/getPricingChangeRequests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetPricingChangeRequestsResponse getPricingChangeRequests(@RequestParam("teacherId") Long teacherId)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/pricing/getPricingChangeRequests?teacherId=" + teacherId, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetPricingChangeRequestsResponse response = new Gson().fromJson(jsonString,
                GetPricingChangeRequestsResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    @RequestMapping(value = "/getTeacherSlabs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetTeacherSlabsResponse getTeacherSlabs(@RequestParam("teacherId") Long teacherId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(dineroEndpoint + "/pricing/getTeacherSlabs?teacherId=" + teacherId, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetTeacherSlabsResponse response = new Gson().fromJson(jsonString, GetTeacherSlabsResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    @RequestMapping(value = "/getTeacherPlans", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetTeacherPlansResponse getTeacherPlans(@RequestParam("teacherId") Long teacherId) throws VException {
        if(teacherId==null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacherId is null");
        }
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(dineroEndpoint + "/pricing/getTeacherPlans?teacherId=" + teacherId, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetTeacherPlansResponse response = new Gson().fromJson(jsonString, GetTeacherPlansResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    //TODO: Check usage
//	@RequestMapping(value = "/getPlanById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public GetPlanByIdResponse getPlanById(@RequestParam("id") String id) throws VException {
//		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/getPlanById?id=" + id, HttpMethod.GET,
//				null);
//
//		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//		String jsonString = resp.getEntity(String.class);
//
//		GetPlanByIdResponse response = new Gson().fromJson(jsonString, GetPlanByIdResponse.class);
//		logger.info("Exiting: " + response.toString());
//		return response;
//	}
//
//	@RequestMapping(value = "/getPlansByIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public GetTeacherPlansResponse getPlansByIds(@RequestParam("ids") List<String> ids) throws VException {
//		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
//		String url = "/pricing/getPlansByIds?ids=";
//		if (ids.size() >= 1) {
//			url += ids.get(0);
//			for (int i = 1; i < ids.size(); i++)
//				url += "&ids=" + ids.get(i);
//		}
//		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
//
//		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//		String jsonString = resp.getEntity(String.class);
//
//		GetTeacherPlansResponse response = new Gson().fromJson(jsonString, GetTeacherPlansResponse.class);
//		logger.info("Exiting: " + response.toString());
//		return response;
//	}
    @RequestMapping(value = "/getAllRequests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAllRequestsResponse getAllRequests(GetAllPricingChangeRequestsReq req) throws VException, IllegalArgumentException, IllegalAccessException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        if (req.getStart() == null || req.getStart() < 0) {
            req.setStart(0);
        }
        if (req.getSize() == null || req.getSize() <= 0) {
            req.setSize(20);
        }
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/pricing/getAllRequests?" + queryString, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetAllRequestsResponse response = new Gson().fromJson(jsonString, GetAllRequestsResponse.class);
        Set<Long> userIds = new HashSet<>();
        List<TeacherRequest> teacherRequests = response.getTeacherRequests();
        if (teacherRequests != null) {
            for (TeacherRequest teacherRequest : teacherRequests) {
                userIds.add(teacherRequest.getUserId());
            }
        }

        Map<Long, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);

        for (TeacherRequest teacherRequest : teacherRequests) {
            UserBasicInfo userBasicInfo = userInfos.get(teacherRequest.getUserId());
            if (userBasicInfo != null) {
                teacherRequest.setEmail(userBasicInfo.getEmail());
                teacherRequest.setFirstName(userBasicInfo.getFirstName());
                teacherRequest.setLastName(userBasicInfo.getLastName());
                teacherRequest.setFullName(userBasicInfo.getFullName());
                teacherRequest.setContactNumber(userBasicInfo
                        .getContactNumber());
                teacherRequest.setProfilePicUrl(userBasicInfo
                        .getProfilePicUrl());
                teacherRequest.setRole(userBasicInfo.getRole());
                //teacherRequests.set(i, teacherRequest);
            }
        }
        logger.info("Exiting: " + response.toString());
        return response;
    }
    //Below two APIs were getting called directly from front-end
    //@CrossOrigin

    @RequestMapping(value = "/uploadTeacherIncentiveTable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse uploadTeacherIncentiveTable(@RequestParam("file") MultipartFile file)
            throws JSONException, UnsupportedEncodingException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        File uploadedFile = handleFileUpload(file);

        if (uploadedFile == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No file uploaded");
        }

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

        map.add("file", new FileSystemResource(uploadedFile.getAbsolutePath()));

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

        RestTemplate restTemplate = new RestTemplate();
        Boolean dineroResponse = restTemplate.postForObject(dineroEndpoint + "/pricing/uploadTeacherIncentiveTable", map, Boolean.class);
        if(Boolean.TRUE.equals(dineroResponse)) {
            return new PlatformBasicResponse(true, null, null);
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in dinero");
        }

    }

    //@CrossOrigin
    @RequestMapping(value = "/uploadTeacherPayoutRateTable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse uploadTeacherPayoutRateTable(@RequestParam("file") MultipartFile file)
            throws JSONException, UnsupportedEncodingException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        File uploadedFile = handleFileUpload(file);

        if (uploadedFile == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No file uploaded");
        }

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

        map.add("file", new FileSystemResource(uploadedFile.getAbsolutePath()));

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

        RestTemplate restTemplate = new RestTemplate();

        Boolean dineroResponse = restTemplate.postForObject(dineroEndpoint + "/pricing/uploadTeacherPayoutRateTable", map, Boolean.class);
        if(Boolean.TRUE.equals(dineroResponse)) {
            return new PlatformBasicResponse(true, null, null);
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in dinero");
        }

    }

    private File handleFileUpload(MultipartFile file) {
        if (!file.isEmpty()) {
            BufferedOutputStream stream = null;
            try {
                File uploadedFile = File.createTempFile(UUID.randomUUID().toString(), ".csv");
                byte[] bytes = file.getBytes();
                stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                return uploadedFile;
            } catch (Exception e) {
            } finally {
                IOUtils.closeQuietly(stream);
            }
        }
        return null;

    }

}
