package com.vedantu.platform.controllers.dashboard;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.dashboard.response.GetTeacherDashboardReq;
import com.vedantu.dashboard.response.GetTeacherDashboardResp;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.pojo.GetOtfDashboardRes;
import com.vedantu.onetofew.request.GetOTFDashboardReq;
import com.vedantu.onetofew.request.ViewAttendanceForUserBatchReq;
import com.vedantu.platform.managers.Dashboard.DashboardManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import javax.mail.internet.AddressException;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(DashboardController.class);

	@Autowired
	private DashboardManager dashboardManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@RequestMapping(value = "/createDashboard", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse createDashboard(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			dashboardManager.createDashboardAsync();
		}
		return new PlatformBasicResponse();
	}

	@RequestMapping(value = "/createDashboardForBatch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse createDashboardForBatch(@RequestParam(value = "batchId") String batchId)
			throws VException, IOException {
		dashboardManager.createDashboardForBatch(batchId);
		return new PlatformBasicResponse();
	}

	@RequestMapping(value = "/getOTFDashboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<GetOtfDashboardRes> getOTFDashboard(GetOTFDashboardReq req) throws VException {
		sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
		return dashboardManager.getOTFDashboard(req);
	}

	@RequestMapping(value = "/viewAttendanceForUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String viewAttendanceForUser(ViewAttendanceForUserBatchReq req) throws VException {
		sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
		return dashboardManager.viewAttendanceForUser(req);
	}

	@RequestMapping(value = "/exportOTFDashboard", method = RequestMethod.POST, consumes = javax.ws.rs.core.MediaType.APPLICATION_JSON, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public PlatformBasicResponse exportOTFDashboard()
			throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return dashboardManager.exportOTFDashboard(sessionUtils.getCurrentSessionData());
	}
//Daily Cron : CRON_CHIME_DAILY
	@RequestMapping(value = "/createTeacherDashboard", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse createTeacherDashboard(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws VException, IOException {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			dashboardManager.createTeacherDashboard();
		}
		return new PlatformBasicResponse();
	}
	
	@RequestMapping(value = "/updatePastTeacherDashboard", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse updatePastTeacherDashboard(@RequestParam(value = "startTime") Long startTime,@RequestParam(value = "days") Integer days) throws VException, IOException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		dashboardManager.updatePastTeacherDashboard(startTime,days);
		return new PlatformBasicResponse();
	}
	
	@RequestMapping(value = "/getTeacherDashboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<GetTeacherDashboardResp> getTeacherDashboard(GetTeacherDashboardReq req) throws VException{
		sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
		return dashboardManager.getTeacherDashboard(req);
	}
	
	@RequestMapping(value = "/exportTeacherDashboard", method = RequestMethod.POST, consumes = javax.ws.rs.core.MediaType.APPLICATION_JSON, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public PlatformBasicResponse exportTeacherDashboard()
			throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return dashboardManager.exportTeacherDashboard(sessionUtils.getCurrentSessionData());
	}
}
