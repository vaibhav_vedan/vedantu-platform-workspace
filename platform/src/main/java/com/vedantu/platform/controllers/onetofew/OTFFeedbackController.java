package com.vedantu.platform.controllers.onetofew;

import com.google.gson.Gson;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.request.GetFeedbackReq;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import javax.annotation.PostConstruct;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping("onetofew/feedback")
public class OTFFeedbackController {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(OTFFeedbackController.class);

	// one to few end point is configured to contain / at the end
	private String ontofewEndpoint;

	@PostConstruct
	public void init() {
		ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT")+"/";
	}

	@Autowired
	OTFManager oTFManager;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public String feedback(HttpServletRequest request) throws IOException, VException {
		String requestBody = PlatformTools.getBody(request);
		logger.info("requestBody - " + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "feedback", HttpMethod.POST, requestBody);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getFeedbackById(@PathVariable("id") String id) throws Exception {
		logger.info("Request:" + id);
		ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "feedback/" + id, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String getFeedbacks(GetFeedbackReq getFeedbackReq)
			throws IOException, VException, IllegalArgumentException, IllegalAccessException {
		String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getFeedbackReq);
		logger.info("queryString - " + queryString);
		String getFeedbacksUrl = ontofewEndpoint + "feedback";
		if (!StringUtils.isEmpty(queryString)) {
			getFeedbacksUrl += "?" + queryString;
		}

		ClientResponse resp = WebUtils.INSTANCE.doCall(getFeedbacksUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	@RequestMapping(value = "/otfFeedbackscheduler", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void otfFeedbackScheduler(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			oTFManager.otfFeedbackScheduler();
		}
	}

	@RequestMapping(value = "/sendFeedbackCommunications", method = RequestMethod.POST)
	@ResponseBody
	public PlatformBasicResponse sendFeedbackCommunications(
			@RequestParam(name = "afterEndTime", required = true) Long afterEndTime,
			@RequestParam(name = "beforeEndTime", required = true) Long beforeEndTime) throws VException {
		oTFManager.handleFeedbacks(afterEndTime, beforeEndTime);
		return new PlatformBasicResponse();
	}

	@RequestMapping(value = "/triggerSessionEnd", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse otfTriggerSessionEnd(
			@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
			throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			oTFManager.otfTriggerSessionEnd();
		}
		return new PlatformBasicResponse();
	}
}
