package com.vedantu.platform.controllers.notification;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.request.RegistrationEmailReq;
import com.vedantu.notification.requests.*;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.VException;
import com.vedantu.notification.responses.GetUnsubscribedListRes;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.Constants;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/notification-centre/email")
public class EmailController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EmailController.class);

    @Autowired
    HttpSessionUtils httpSessionUtils;

    @Autowired
    CommunicationManager emailManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @RequestMapping(value = "/emailStatusNotification", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void emailStatusNotification(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                        @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            String message = subscriptionRequest.getMessage();
            String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/emailStatusNotification",
                    HttpMethod.POST, message);
        }
    }

    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces
            = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String sendEmail(@RequestBody EmailRequest request) throws
            VException {
        return emailManager.sendEmail(request);
    }
    //
    // @RequestMapping(value = "/sendBulkEmail", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public String sendBulkEmail(@RequestBody EmailRequest request) throws
    // VException {
    // return emailManager.sendBulkEmail(request);
    // }

    // @RequestMapping(value = "/getSendQuota", method = RequestMethod.GET,
    // produces = MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public String getSendQuota() {
    // String notificationEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint +
    // "/email/getSendQuota", HttpMethod.GET,
    // null);
    // return resp.getEntity(String.class);
    // }
    //
    // @RequestMapping(value = "/getSendStatistics", method = RequestMethod.GET,
    // produces = MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public String getSendStatistics(@RequestParam(value = "lastHours",
    // required = false) Integer lastHours) {
    // String notificationEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
    // String url = notificationEndpoint + "/email/getSendStatistics";
    // if (lastHours != null)
    // url += "?lastHours=" + lastHours;
    // ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
    // null);
    // return resp.getEntity(String.class);
    // }
    @Deprecated
    @RequestMapping(value = "/emailOpened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String emailOpened(@RequestParam(value = "id", required = true) String id) {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        String url = notificationEndpoint + "/email/emailOpened";
        if (id != null) {
            url += "?id=" + id;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        return resp.getEntity(String.class);
    }

    @RequestMapping(value = "/track", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse track(@RequestParam(value = "emailId", required = true) String emailId,
                                       @RequestParam(value = "linkRandomId", required = false) String linkRandomId,
                                       @RequestParam(value = "redirectUrl", required = true) String redirectUrl,
                                       HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        Map<String, Object> payload = new HashMap<>();
        payload.put("emailId", emailId);
        payload.put("linkRandomId", linkRandomId);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRACK_EMAIL_LINK_CLICK, payload);
        asyncTaskFactory.executeTask(params);
        String decodedUrl = URLDecoder.decode(redirectUrl, Constants.DEFAULT_CHARSET);
        logger.info("redirecting to " + decodedUrl);
        response.sendRedirect(decodedUrl);
        logger.info(" track in platform  ");
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/sendMoodleNotifications", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendMoodleNotifications(@RequestBody SendMoodleNotificationsReq request) throws Exception {
        emailManager.sendMoodleNotifications(request);
    }

    @RequestMapping(value = "/communicationReminder", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void communicationReminder(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                      @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
            emailManager.communicationReminder();
        }
    }

    @RequestMapping(value = "/sendBundleReminder", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse sendBundleReminder(@RequestParam(value = "userId", required = true) long userId,
                                                    @RequestParam(value = "entityId", required = true) String entityId,
                                                    @RequestParam(value = "entityType", required = true) com.vedantu.session.pojo.EntityType entityType) throws Exception {
        emailManager.sendBundleRelatedEnrolmentEmail(userId, entityId, entityType);
        return new PlatformBasicResponse();
    }

    @Deprecated
    @RequestMapping(value = "/markUnsubscribeStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markUnsubscribeStatus(@RequestBody UnsubscribeEmailReq req) throws VException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return emailManager.markUnsubscribeStatus(req);
    }

    @Deprecated
    @RequestMapping(value = "/getUnsubscribedList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUnsubscribedListRes getUnsubscribedList(GetUnsubscribedListReq req) throws VException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return emailManager.getUnsubscribedList(req);
    }

    @RequestMapping(value = "/sendVSATRegistrationEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendVSATRegistrationEmail(@RequestBody RegistrationEmailReq req) throws VException, IOException {
        if(!req.getKey().equals("C1X?KF#jW9xl+f{("))
            return;

        emailManager.sendRegistrationEmailForVSAT(req.getUser());
    }

    @RequestMapping(value = "/sendVoltRegistrationEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendVoltRegistrationEmail(@RequestBody RegistrationEmailReq req) throws VException, IOException {
        if (!req.getKey().equals("C1X?KF#jW9xl+f{(")) {
            return;
        }

        emailManager.sendRegistrationEmailForVolt(req.getUser());
    }

    @RequestMapping(value = "/sendRegistrationEmailForReviseIndia", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendRegistrationEmailForReviseIndia(@RequestBody RegistrationEmailReq req) throws VException, IOException {
        if (!req.getKey().equals("C1X?KF#jW9xl+f{(")) {
            return;
        }

        emailManager.sendRegistrationEmailForReviseIndia(req.getUser());
    }


    @RequestMapping(value = "/sendRegistrationEmailForReviseJee", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendRegistrationEmailForReviseJee(@RequestBody RegistrationEmailReq req) throws VException, IOException {
        if (!req.getKey().equals("C1X?KF#jW9xl+f{(")) {
            return;
        }

        emailManager.sendRegistrationEmailForReviseJee(req.getUser());
    }

    @RequestMapping(value = "/sendEmailRegistrationISLViaTools", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendEmailRegistrationISLViaTools(RegistrationEmailReq req) throws VException, IOException {
        if(!req.getKey().equals("C1X?KF#jW9xl+f{("))
            return;
        emailManager.sendEmailRegistrationISLViaTools(req.getUser());
    }

    @RequestMapping(value = "/sendRegistrationEmailForISL", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendRegistrationEmailForISL(RegistrationEmailReq req) throws VException, IOException {
        if(!req.getKey().equals("C1X?KF#jW9xl+f{("))
            return;
        emailManager.sendRegistrationEmailForISL(req.getUser());
    }


}
