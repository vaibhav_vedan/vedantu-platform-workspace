package com.vedantu.platform.controllers.review;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.platform.managers.review.RemarkManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.review.requests.AddRemarkReq;
import com.vedantu.review.response.GetLastSessionRemarkResp;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("/remark")
public class RemarkController {
	@Autowired
	LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(RemarkController.class);

	@Autowired
	RemarkManager remarkManager;

	@Autowired
	HttpSessionUtils sessionUtils;

	@RequestMapping(value = "/addRemark", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public RemarkResp addRemark(@RequestBody AddRemarkReq addRemarkReq) throws Exception {

		logger.info("Add review request: " + addRemarkReq.toString());
		sessionUtils.checkIfAllowedList(addRemarkReq.getCallingUserId(), Arrays.asList(Role.TEACHER), false);
		RemarkResp remarkResp = remarkManager.addRemark(addRemarkReq);

		return remarkResp;
	}

	@RequestMapping(value = "/getLastSessionRemark", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public GetLastSessionRemarkResp getLastSessionRemark(@RequestParam(name = "batchId", required = true) String batchId,
			@RequestParam(name = "boardId", required = false) Long boardId, @RequestParam(name = "sessionId", required = false) String sessionId) throws Exception {

		GetLastSessionRemarkResp remarkResp = remarkManager.getLastSessionRemark(batchId,boardId,sessionId);

		return remarkResp;
	}
        
        @RequestMapping(value = "/getRemarkForSession", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public RemarkResp getRemarkForSession(@RequestParam(name = "sessionId", required = true) String sessionId) throws Exception {

		RemarkResp remarkResp = remarkManager.getRemarkForSession(sessionId);
		return remarkResp;
	}
	
	@RequestMapping(value = "/getBatchRemarkForStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<RemarkResp> getBatchRemarkForStudent(StudentBatchProgressReq req) throws VException{
		return remarkManager.getBatchRemarkForStudent(req);
	}

}
