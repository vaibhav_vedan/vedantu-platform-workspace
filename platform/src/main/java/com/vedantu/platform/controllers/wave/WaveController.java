/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this wavebook file, choose Tools | Wavebooks
 * and open the wavebook in the editor.
 */
package com.vedantu.platform.controllers.wave;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.platform.dao.wave.SessionIdVsServer;
import com.vedantu.platform.dao.wave.WaveDAO;
import com.vedantu.platform.enums.wave.WavebookScope;
import com.vedantu.platform.managers.wave.NodeServerManager;
import com.vedantu.platform.managers.wave.WavebookManager;
import com.vedantu.platform.pojo.wave.NodeAddress;
import com.vedantu.platform.pojo.wave.NodeServerResponse;
import com.vedantu.platform.pojo.wave.Wavebook;
import com.vedantu.platform.pojo.wave.WavebookVersion;
import com.vedantu.platform.request.wave.CreateWavebookReq;
import com.vedantu.platform.request.wave.EditWavebookReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/wave")
public class WaveController {

    @Autowired
    NodeServerManager nodeServerManager;

    @Autowired
    WavebookManager wavebookManager;

    @Autowired
    private WaveDAO waveDAO;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(WaveController.class);

    //@CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getNodeServer")
    @ResponseBody
    public NodeServerResponse getNodeServer(@RequestParam("sessionId") String sessionId,
                                            @RequestParam(value = "sessionType", required = false) SessionType sessionType) throws VException {
        logger.log(Level.INFO, "Got request for assignNodeserver for sessionId " + sessionId);
//        NodeAddress selectedServer = nodeServerManager.getDefaultServer();
        NodeAddress selectedServer = nodeServerManager.assignServer(sessionId, sessionType);
        NodeServerResponse nodeServerResponse = null;
        if (selectedServer != null) {
            nodeServerResponse = new NodeServerResponse(selectedServer.getIp(), selectedServer.getPort(), selectedServer.getIp(), selectedServer.getPort());
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Some error selecting best server");
        }
        return nodeServerResponse;
    }

    //@CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getSelectedNodeServer")
    @ResponseBody
    public List<SessionIdVsServer> getSelectedNodeServer(@RequestParam("sessionId") String sessionId) {
        logger.log(Level.INFO, "Got request for getSelectedNodeServer for sessionId " + sessionId);
        List<SessionIdVsServer> servers = waveDAO.getServerOfSessionId(sessionId);
        return servers;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/createWavebook")
    @ResponseBody
    public Wavebook createWavebook(@RequestBody CreateWavebookReq createWavebookReq) throws InternalServerErrorException, BadRequestException, Exception {
        logger.log(Level.INFO, "Got request for createWavebook for  " + createWavebookReq.toString());
        return wavebookManager.createWavebook(createWavebookReq);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/deleteWavebook")
    @ResponseBody
    public Wavebook deleteWavebook(@RequestParam("wavebookId") String wavebookId, @RequestParam(value = "callingUserId", required = false) Long callingUserId) throws BadRequestException, NotFoundException, ForbiddenException {
        logger.log(Level.INFO, "Got request for deleteWavebook for  " + wavebookId);
        return wavebookManager.deleteWavebook(wavebookId, callingUserId);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getWavebooks")
    @ResponseBody
    public List<Wavebook> getWavebooks(@RequestParam("userId") Long userId,
            @RequestParam(value = "wavebookScope", required = false) WavebookScope wavebookScope,
            @RequestParam(value = "start", required = true) Integer start,
            @RequestParam(value = "size", required = true) Integer size,
            @RequestParam(value = "wavebookVersion", required = false) WavebookVersion wavebookVersion) throws InternalServerErrorException, BadRequestException {
        logger.log(Level.INFO, "Got request for getWavebooks for userId " + userId + " ,scope " + wavebookScope + ",  start:" + start + ",  size:" + size);
        return wavebookManager.getWavebooksByUserId(userId, wavebookScope, start, size, wavebookVersion);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getAllWavebooks")
    @ResponseBody
    public List<Wavebook> getAllWavebooks(
            @RequestParam(value = "wavebookScope", required = false) WavebookScope wavebookScope,
            @RequestParam(value = "start", required = true) Integer start,
            @RequestParam(value = "size", required = true) Integer size,
            @RequestParam(value = "wavebookVersion", required = false) WavebookVersion wavebookVersion) throws InternalServerErrorException, BadRequestException {
        logger.log(Level.INFO, "Got request for getAllWavebooks scope " + wavebookScope + ",  start:" + start + ",  size:" + size);
        return wavebookManager.getAllWavebooks(wavebookScope, start, size, wavebookVersion);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getWavebookDetails")
    @ResponseBody
    public Wavebook getWavebookDetails(@RequestParam("wavebookId") String wavebookId) throws BadRequestException, NotFoundException {
        logger.log(Level.INFO, "Got request for getWavebookDetails for wavebookID " + wavebookId);
        return wavebookManager.getWavebookDetails(wavebookId);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/editWavebook")
    @ResponseBody
    public Wavebook editWavebook(@RequestBody EditWavebookReq editWavebookReq) throws InternalServerErrorException, BadRequestException, NotFoundException, Exception {
        logger.log(Level.INFO, "Got request for editWavebook for " + editWavebookReq.toString());
        return wavebookManager.editWavebook(editWavebookReq);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getPublicWavebooks")
    @ResponseBody
    public List<Wavebook> getPublicWavebooks(@RequestParam(value = "start", required = true) Integer start, @RequestParam(value = "size", required = true) Integer size) throws InternalServerErrorException, BadRequestException, NotFoundException {
        logger.log(Level.INFO, "Got request for getPublicWavebooks  start:" + start + ",  size:" + size);
        return wavebookManager.getPublicWavebooks(start, size);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/refreshCookies")
    @ResponseBody
    public PlatformBasicResponse refreshCookies() throws InternalServerErrorException, BadRequestException, NotFoundException {
        logger.log(Level.INFO, "Got request for refreshCookies");
        PlatformBasicResponse resp = new PlatformBasicResponse();
        resp.setSuccess(true);
        return resp;
    }

}
