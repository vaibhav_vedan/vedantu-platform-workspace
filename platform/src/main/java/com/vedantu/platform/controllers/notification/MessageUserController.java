/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.notification;

import com.vedantu.exception.VException;
import com.vedantu.notification.requests.MessageUserReplyReq;
import com.vedantu.notification.responses.GetMessageLeadersRes;
import com.vedantu.platform.managers.notification.MessageUserManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.pojo.user.GetMessageUsersReq;
import com.vedantu.platform.pojo.user.GetMessageUsersRes;
import com.vedantu.platform.pojo.user.MessageUserReq;
import com.vedantu.platform.pojo.user.MessageUserServletRes;
import com.vedantu.util.PlatformBasicResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/notification-centre/message")
public class MessageUserController {

    @Autowired
    MessageUserManager messageUserManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    UserManager userManager;

    //@CrossOrigin
    @RequestMapping(value = "/messageUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse messageUser(@Valid @RequestBody MessageUserReq request) throws VException {
        //TODO this request is also fired when user replies to an offline message sent on email, 
        //so think of a proper authentication here
//        sessionUtils.checkIfAllowed(request.getFromUserId(), null , Boolean.FALSE);        
        return userManager.messageUser(request);
    }

    @Deprecated
    @RequestMapping(value = "/messageUserReply", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse messageUserReply(@Valid @RequestBody MessageUserReplyReq request) throws VException {
        return userManager.messageUserReply(request);
    }

    @Deprecated
    @RequestMapping(value = "/getMessageUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetMessageUsersRes messageUser(GetMessageUsersReq request) throws VException {
        return userManager.getMessageUsers(request);
    }

    @Deprecated
    @RequestMapping(value = "/getMessageById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MessageUserServletRes messageUser(@RequestParam(value = "messageId") Long messageId) throws VException {
        return userManager.getMessageById(messageId);
    }

    @Deprecated
    @RequestMapping(value = "/getMessageLeaders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetMessageLeadersRes getMessageLeaders(GetMessageUsersReq request) throws VException {
        request.verify();
        return userManager.getMessageLeaders(request);
    }

}
