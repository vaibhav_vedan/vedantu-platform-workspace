package com.vedantu.platform.controllers.dinero;

import com.vedantu.dinero.pojo.FailedPayoutInfo;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.platform.managers.dinero.ReportManager;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.internet.InternetAddress;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.dinero.pojo.ExtTransactionPojo;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.pojo.dinero.WalletStatusInfo;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

import java.io.StringWriter;

@RestController
@RequestMapping("/dinero/report")
public class ReportController {


    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ReportController.class);


    @Autowired
    private ReportManager reportManager;

    @Deprecated
    @RequestMapping(value = "/getFailedPayoutsForEndedSessions", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getFailedPayoutsForEndedSessions(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                 @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getFailedPayoutsForEndedSessions",
                    HttpMethod.GET, null);

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);

            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<FailedPayoutInfo>>() {
            }.getType();

            List<FailedPayoutInfo> responseObject = new Gson().fromJson(jsonString, listType);
            logger.info("responseObject size " + responseObject.size());

            if (responseObject.size() > 0) {

                StringWriter stringWriter = new StringWriter();
                String fileName = "FailedPayoutsDaily-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ICsvBeanWriter csvWriter = new CsvBeanWriter(stringWriter,
                        CsvPreference.STANDARD_PREFERENCE);
                String[] header = {"SessionId", "PayoutFound", "BillingDuration", "ContextType"};
                csvWriter.writeHeader(header);
                for (FailedPayoutInfo failedPayoutInfo : responseObject) {
                    csvWriter.write(failedPayoutInfo, header);
                }
                csvWriter.close();

                                /*
				BufferedReader reader = new BufferedReader(new FileReader(fileName));
				String line = null;
				StringBuilder stringBuilder = new StringBuilder();
				String ls = System.getProperty("line.separator");
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append(ls);
				}
				// delete the last ls
				stringBuilder.deleteCharAt(stringBuilder.length() - 1);

                                */
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportFailedPayouts.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Failed Payouts List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "Payout");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(stringWriter.toString());
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Failed Payouts Daily Report");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }

    @Deprecated
    @RequestMapping(value = "/getVedantuWalletsStatus", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getVedantuWalletsStatus(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                        @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getVedantuWalletsStatus",
                    HttpMethod.GET, null);

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);

            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<WalletStatusInfo>>() {
            }.getType();

            List<WalletStatusInfo> responseObject = new Gson().fromJson(jsonString, listType);
            logger.info("responseObject size " + responseObject.size());
            if (responseObject.size() > 0) {
                StringWriter stringWriter = new StringWriter();
                String fileName = "VedantuWalletsStatusDaily-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ICsvBeanWriter csvWriter = new CsvBeanWriter(stringWriter,
                        CsvPreference.STANDARD_PREFERENCE);
                String[] header = {"HolderId", "CurrentTimeMilli", "PreviousTimeMilli",
                        "AccountBalanceCurrent", "AccountBalancePrevious",
                        "AccountPromotionalBalanceCurrent", "AccountPromotionalBalancePrevious",
                        "AccountNonPromotionalBalanceCurrent", "AccountNonPromotionalBalancePrevious"};
                csvWriter.writeHeader(header);
                for (WalletStatusInfo walletStatusInfo : responseObject) {
                    csvWriter.write(walletStatusInfo, header);
                }
                csvWriter.close();
                                /*
				BufferedReader reader = new BufferedReader(new FileReader(fileName));
				String line = null;
				StringBuilder stringBuilder = new StringBuilder();
				String ls = System.getProperty("line.separator");
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append(ls);
				}
				// delete the last ls
				stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                                */
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportVedantuWalletsStatus.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Vedantu Wallet Status List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "WalletStatus");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(stringWriter.toString());
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Vedantu Wallet Status Daily Report");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }

    @Deprecated
    @RequestMapping(value = "/pendingExtTransactions", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void pendingExtTransactions(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                       @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/pendingExtTransactions",
                    HttpMethod.GET, null);

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);

            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<ExtTransactionPojo>>() {
            }.getType();
            logger.info("jsonString " + jsonString);

            List<ExtTransactionPojo> responseObject = new Gson().fromJson(jsonString, listType);
            logger.info("responseObject size " + responseObject.size());
            if (responseObject.size() > 0) {
                StringWriter stringWriter = new StringWriter();
                String fileName = "PendingExtTransactionsDaily-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ICsvBeanWriter csvWriter = new CsvBeanWriter(stringWriter,
                        CsvPreference.STANDARD_PREFERENCE);
                String[] header = {"Id", "Type", "Status", "Amount", "VedantuOrderId"};
                csvWriter.writeHeader(header);
                for (ExtTransactionPojo extTransaction : responseObject) {
                    csvWriter.write(extTransaction, header);
                }
                csvWriter.close();
                                /*
				BufferedReader reader = new BufferedReader(new FileReader(fileName));
				String line = null;
				StringBuilder stringBuilder = new StringBuilder();
				String ls = System.getProperty("line.separator");
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append(ls);
				}
				// delete the last ls
				stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                                */
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportPendingExtTransactions.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Pending EXT Transactions List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "PendingExtTransactions");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(stringWriter.toString());
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Pending EXT Transactions Daily Report");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }


    @Deprecated
    @RequestMapping(value = "/getTransactionsToVedantuWallets", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getTransactionsToVedantuWallets(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            Long fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
            String csv = reportManager.getTransactionsToVedantuWalletsCSV(fromTime, toTime);
            if (StringUtils.isNotEmpty(csv)) {
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                String fileName = "TransactionsToVedantuWallets-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportTransactionsToVedantuWallets.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Transactions To Vedantu Wallets List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "TransactionsToVedantuWallets");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(csv);
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Transactions To Vedantu Wallets Daily Report");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }


    @Deprecated
    @RequestMapping(value = "/getTransactionsFromVedantuWallets", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getTransactionsFromVedantuWallets(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                  @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            Long fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
            String csv = reportManager.getTransactionsFromVedantuWalletsCSV(fromTime, toTime);
            if (StringUtils.isNotEmpty(csv)) {
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                String fileName = "TransactionsFromVedantuWallets-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportTransactionsFromVedantuWallets.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Transactions From Vedantu Wallets List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "TransactionsFromVedantuWallets");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(csv);
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Transactions From Vedantu Wallets Daily Report");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }


    @Deprecated
    @RequestMapping(value = "/getExternalTransactionsDetails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getExternalTransactionsDetails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                               @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            Long fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
            String csv = reportManager.getExternalTransactionsDetailsCSV(fromTime, toTime);
            if (StringUtils.isNotEmpty(csv)) {
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                String fileName = "ExternalTransactionsDetails-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportExternalTransactionsDetails.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily External Transactions Details List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "ExternalTransactionsDetails");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(csv);
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "External Transactions Details");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }


    @Deprecated
    @RequestMapping(value = "/getSubscriptionTransactionsDetails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getSubscriptionTransactionsDetails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                   @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            Long fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
            String csvData = reportManager.getSubscriptionTransactionsDetailsCSV(fromTime, toTime);
            if (StringUtils.isNotEmpty(csvData)) {
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                String fileName = "SubscriptionTransactionsDetails-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportSubscriptionTransactionsDetails.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Subscription Transactions Details List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "SubscriptionTransactionsDetails");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(csvData);
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Subscription Transactions Details");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }


    @Deprecated
    @RequestMapping(value = "/getTeacherPayoutTransactionsDetails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getTeacherPayoutTransactionsDetails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                    @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            Long fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
            String csv = reportManager.getTeacherPayoutTransactionsDetailsCSV(fromTime, toTime);
            if (StringUtils.isNotEmpty(csv)) {
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                String fileName = "TeacherPayoutTransactionsDetails-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportTeacherPayoutTransactionsDetails.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Teacher Payout Transactions Details List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "TeacherPayoutTransactionsDetails");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(csv);
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Teacher Payout Transactions Details");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }


    @Deprecated
    @RequestMapping(value = "/getVedantuCutTransactionsDetails", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getVedantuCutTransactionsDetails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                 @RequestBody String request) throws Throwable {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            Long toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            Long fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
            String csv = reportManager.getVedantuCutTransactionsDetailsCSV(fromTime, toTime);
            if (StringUtils.isNotEmpty(csv)) {
                // Send email via notification centre
                logger.info("Sending email via notification-centre");
                String fileName = "VedantuCutTransactionsDetails-"
                        + new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
                ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
                String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportVedantuCutTransactionsDetails.toEmailIds");
                String[] emails = emailTos.split(",");
                for (String email : emails) {
                    to.add(new InternetAddress(email));
                }
                String body = "Please find the attached Daily Vedantu Cut Transactions Details List";
                EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "VedantuCutTransactionsDetails");
                attachment.setApplication("application/csv");
                attachment.setAttachmentData(csv);
                CommunicationType type = CommunicationType.USER_MESSAGE;
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setTo(to);
                emailRequest.setBody(body);
                String env = "";
                if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
                    env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
                emailRequest.setSubject(env + "Vedantu Cut Transactions Details");
                emailRequest.setType(type);
                emailRequest.setAttachment(attachment);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                        gson.toJson(emailRequest));
                logger.info(resp.getEntity(String.class));
            }
        }

    }

}
