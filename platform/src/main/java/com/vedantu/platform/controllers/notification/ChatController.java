package com.vedantu.platform.controllers.notification;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.platform.managers.moodle.ChatManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestHeader;

//@CrossOrigin
@RestController
@RequestMapping("/notification-centre/chat")
public class ChatController {

    @Autowired
    ChatManager chatManager;
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChatManager.class);

    @Deprecated
    //@CrossOrigin
    @RequestMapping(value = "/fetchUsersLastChattedWith", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String fetchUsersLastChattedWith(@RequestParam("userId") Long userId,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size) throws VException {
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 20;
        }
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/fetchUsersLastChattedWith?userId=" + userId + "&start=" + start + "&size=" + size, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @Deprecated
    //@CrossOrigin
    @RequestMapping(value = "/initiateChatWithUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String initiateChatWithUser(@RequestParam("fromUserId") Long fromUserId,
            @RequestParam("toUserId") Long toUserId) throws VException {
        return chatManager.initiateChatWithUser(fromUserId, toUserId);
    }

    @Deprecated
    //@CrossOrigin
    @RequestMapping(value = "/updateLastChattedTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateLastChattedTime(@RequestParam("userId") Long userId,
            @RequestParam("channelName") String channelName) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/updateLastChattedTime?userId=" + userId + "&channelName=" + channelName, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @Deprecated
    //@CrossOrigin
    @RequestMapping(value = "/updateUnreadMessageCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateUnreadMessageCount(@RequestParam("userId") Long userId,
            @RequestParam("channelName") String channelName, @RequestParam("count") int count) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/updateUnreadMessageCount?userId=" + userId + "&channelName=" + channelName + "&count=" + count, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    //@CrossOrigin
    @RequestMapping(value = "/replyToOfflineMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String replyToOfflineMessage(@RequestBody String request) throws VException {

        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/replyToOfflineMessage", HttpMethod.POST, request);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @Deprecated
    //@CrossOrigin
    @RequestMapping(value = "/replyToOfflineMessageFile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String replyToOfflineMessageFile(@RequestBody String request) throws VException {

        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/replyToOfflineMessageFile", HttpMethod.POST, request);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @Deprecated
    @CrossOrigin
    @RequestMapping(value = "/setAblyChannelActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String setAblyChannelActive(@RequestParam(value = "channel", required = true) String channel) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/setAblyChannelActive?channel=" + channel, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @Deprecated
    @CrossOrigin
    @RequestMapping(value = "/getChatChannelName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getChatChannelName(@RequestParam(value = "fromUserId", required = true) Long fromUserId,@RequestParam(value = "toUserId", required = true) Long toUserId) {
      String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/getChatChannelName?fromUserId="+fromUserId+"&toUserId="+toUserId, HttpMethod.GET,null);
        return resp.getEntity(String.class);
    }
    
    @Deprecated
    @CrossOrigin
    @RequestMapping(value = "/getChatMessageHistory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getChatMessageHistory(@RequestParam(value = "channel", required = true) String channel, @RequestParam(value = "size", required = true) Integer size, @RequestParam(value = "before", required = false) Long before) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp;
        if (before != null) {
            resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/getChatMessageHistory?channel=" + channel + "&size=" + size + "&before=" + before, HttpMethod.GET, null);
        } else {
            resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/getChatMessageHistory?channel=" + channel + "&size=" + size, HttpMethod.GET, null);
        }
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @RequestMapping(value = "/handleOTOSessionEvents", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleOTOSessionEvents(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SessionEvents eventType = SessionEvents.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            chatManager.handleSessionEvents(eventType, message);
        }
    }

    @RequestMapping(value = "/handleOTFBatchEvents", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleOTFBatchEvents(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            BatchEventsOTF eventType = BatchEventsOTF.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            chatManager.handleOtfBatchEvents(eventType, message);
        }
    }
//	@RequestMapping(value = "/setStatusType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String setStatusType(@RequestParam("userId") Long userId, 
//   			@RequestParam("statusType") StatusType statusType, @RequestParam("role") Role role){
//		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/setStatusType?userId="+ userId + "&statusType="+ statusType + "&role="+ role, HttpMethod.GET, null);
//		return resp.getEntity(String.class);
//	}
//	
//	@RequestMapping(value = "/getAllStatusTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String getAllStatusTypes(@RequestParam("userIds") List<Long> userIds){
//		
//		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//		String url = notificationEndpoint + "/chat/getAllStatusTypes?";
//		for(Long userId: userIds){
//			url += "&userIds="+userId;
//		}
//		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
//		return resp.getEntity(String.class);
//	}
//	
//	@RequestMapping(value = "/getMyStatusType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String getMyStatusType(@RequestParam("userId") Long userId){
//		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/chat/getMyStatusType?userId="+ userId, HttpMethod.GET, null);
//		return resp.getEntity(String.class);
//	}

}
