package com.vedantu.platform.controllers.scheduling;


import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.VException;
import com.vedantu.platform.managers.scheduling.InstalearnManager;
import com.vedantu.scheduling.request.instalearn.AddInstaRequestReq;
import com.vedantu.scheduling.request.instalearn.GetInstaRequestsReq;
import com.vedantu.scheduling.request.instalearn.SetTeacherSubscriptionReq;
import com.vedantu.scheduling.request.instalearn.UpdateInstaRequestReq;
import com.vedantu.scheduling.response.instalearn.GetInstaRequestsRes;
import com.vedantu.scheduling.response.instalearn.SubmitInstaRequestRes;
import com.vedantu.scheduling.response.instalearn.TeacherSubscriptionResponse;
import com.vedantu.util.LogFactory;
import java.io.IOException;
import org.springframework.web.bind.annotation.RequestBody;

import javax.mail.internet.AddressException;

@RestController
@RequestMapping("/instalearn")
public class InstalearnController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalearnController.class);

    @Autowired
    private InstalearnManager instalearnManager;


    @RequestMapping(value = "/addRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitInstaRequestRes addInstaRequest(@RequestBody AddInstaRequestReq addInstaRequestReq) throws VException {
        return instalearnManager.addInstaRequest(addInstaRequestReq);
    }

    @RequestMapping(value = "/submitRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitInstaRequestRes submitInstaRequest(@RequestBody UpdateInstaRequestReq updateInstaRequestReq) throws VException, IOException, AddressException {
        return instalearnManager.submitInstaRequest(updateInstaRequestReq);
    }

    @RequestMapping(value = "/retryBooking/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitInstaRequestRes retryBooking(@PathVariable("id") String id) throws VException, IOException, AddressException {
        return instalearnManager.retryBooking(id);
    }

    @RequestMapping(value = {"getInstaRequests"}, method = RequestMethod.GET)
    @ResponseBody
    public GetInstaRequestsRes getInstaRequests(GetInstaRequestsReq getInstaRequestsReq) throws VException, IllegalArgumentException, IllegalAccessException {
        return instalearnManager.getInstaRequests(getInstaRequestsReq);
    }

    @RequestMapping(value = "getRequestById/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SubmitInstaRequestRes getRequestById(@PathVariable("id") String id) throws VException {
        return instalearnManager.getRequestById(id, true);
    }

    @RequestMapping(value = {"getTeacherILRequestsCount/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getTeacherILRequestsCount(@PathVariable("id") Long id) throws VException {
        return instalearnManager.getTeacherILRequestsCount(id);
    }

    @RequestMapping(value = "/setTeacherSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TeacherSubscriptionResponse addTeacherSubscription(@RequestBody SetTeacherSubscriptionReq setTeacherSubscriptionReq) throws VException {
        return instalearnManager.addTeacherSubscription(setTeacherSubscriptionReq);
    }

    @Deprecated
    @RequestMapping(value = {"getTeacherSubscription/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public TeacherSubscriptionResponse getTeacherSubscription(@PathVariable("id") Long id) throws VException {
        return instalearnManager.getTeacherSubscription(id, true);
    }
}
