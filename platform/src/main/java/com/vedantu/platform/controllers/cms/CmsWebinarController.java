package com.vedantu.platform.controllers.cms;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.cms.WebinarManager;
import com.vedantu.platform.pojo.cms.Webinar;
import com.vedantu.platform.pojo.cms.WebinarCallbackData;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.platform.request.cms.AddQuestionToRegistrationReq;
import com.vedantu.platform.request.cms.CreateWebinarReq;
import com.vedantu.platform.request.cms.EditWebinarReq;
import com.vedantu.platform.request.cms.FindWebinarsReq;
import com.vedantu.platform.request.cms.GetWebinarRegistrationInfoReq;
import com.vedantu.platform.request.cms.WebinarCallbackReq;
import com.vedantu.platform.request.cms.WebinarRegistrationReq;
import com.vedantu.platform.response.WebinarResponse;
import com.vedantu.platform.response.cms.FindQuizzesRes;
import com.vedantu.platform.response.cms.WebinarRegisterUserRes;
import com.vedantu.subscription.pojo.CourseMobileRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("cms/webinar")
public class CmsWebinarController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CmsWebinarController.class);

    @Autowired
    private RedisDAO redisDAO;

    private static Gson gson = new Gson();

    private final static String growthCmsWebinarBasePath = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT") + "/cms/webinar";

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WebinarRegisterUserRes webinarRegister(@RequestBody WebinarRegistrationReq req) throws VException {
        logger.info("webinar register request :" + req.toString());

        String url = growthCmsWebinarBasePath + "/register";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarRegisterUserRes.class);

    }

    @RequestMapping(value = "/updateWebinarAttended", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateWebinarAttended(@RequestBody GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("webinar update attendence request :" + req.toString());
        if (StringUtils.isEmpty(req.getUserId()) && httpSessionUtils.getCurrentSessionData() != null) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setUserId(data.getUserId().toString());
        }

        String url = growthCmsWebinarBasePath + "/updateWebinarAttended";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, PlatformBasicResponse.class);
    }

    @RequestMapping(value = "/updateOtmWebinarAttended", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateOtmWebinarAttended(@RequestBody GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("otm webinar update attendence request :" + req.toString());
        if (StringUtils.isEmpty(req.getUserId()) && httpSessionUtils.getCurrentSessionData() != null) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setUserId(data.getUserId().toString());
        }
        String url = growthCmsWebinarBasePath + "/updateOtmWebinarAttended";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, PlatformBasicResponse.class);
    }

    @RequestMapping(value = "/addQuestionToRegistration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WebinarRegisterUserRes addQuestionToRegistration(@RequestBody AddQuestionToRegistrationReq req) throws VException {
        logger.info("webinar addQuestionToRegistration request :" + req.toString());
        if (StringUtils.isEmpty(req.getEmail())) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setEmail(data.getEmail());
        }
        String url = growthCmsWebinarBasePath + "/addQuestionToRegistration";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarRegisterUserRes.class);
    }

    @RequestMapping(value = "/webinarRequestCallback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WebinarCallbackData webinarRequestCallback(@RequestBody WebinarCallbackReq req) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);

        String url = growthCmsWebinarBasePath + "/webinarRequestCallback";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarCallbackData.class);
    }

    @RequestMapping(value = "/getWebinarCallbackInfo", method = RequestMethod.GET)
    @ResponseBody
    public WebinarCallbackData getWebinarCallbackInfo() throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);

        String url = growthCmsWebinarBasePath + "/getWebinarCallbackInfo";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarCallbackData.class);
    }

    @RequestMapping(value = "/getWebinarUserRegistrationInfo", method = RequestMethod.GET)
    @ResponseBody
    public WebinarUserRegistrationInfo getWebinarUserRegistrationInfo(GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("webinar register request :" + req.toString());
        if (StringUtils.isEmpty(req.getEmail())) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setEmail(data.getEmail());
        }
        logger.info("after the email check : " + req.toString());

        String url = growthCmsWebinarBasePath + "/getWebinarUserRegistrationInfo";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarUserRegistrationInfo.class);
    }

    @RequestMapping(value = "/getWebinarUserRegistrationInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<WebinarRegisterUserRes> getWebinarUserRegistrationInfos(@RequestParam(value = "email", required = false) String email, @RequestParam(value = "webinarIds", required = true) List<String> webinarIds) throws VException {
        if (StringUtils.isEmpty(email)) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            email = data.getEmail();
        }

        String url = growthCmsWebinarBasePath + "/getWebinarUserRegistrationInfos?email=" + email ;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(webinarIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<WebinarRegisterUserRes>>() {
        }.getType();

        return gson.fromJson(res, _type);
    }

    @RequestMapping(value = "/getWebinarsForUser", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseMobileRes> getWebinarForUser(@RequestParam(value = "userId", required = true) String userId,
                                                   @RequestParam(value = "start", required = false) Integer start,
                                                   @RequestParam(value = "size", required = false) Integer size) throws VException {


        String url = growthCmsWebinarBasePath + "/getWebinarsForUser?userId=" + userId;

        if( start!= null)
            url += "&start=" + start;

        if(size != null)
            url += "&size=" + size;

        logger.info("calling {}", url);

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<CourseMobileRes>>() {
        }.getType();

        return gson.fromJson(res, _type);
    }


    @RequestMapping(value = "/registerForGTWWebinar", method = RequestMethod.GET)
    @ResponseBody
    public WebinarUserRegistrationInfo registerForGTWWebinar(@RequestParam(value = "id", required = true) String id) throws VException {
        logger.info("webinar register request :" + id);
        String url = growthCmsWebinarBasePath + "/registerForGTWWebinar?id=" + id;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarUserRegistrationInfo.class);
    }

    @RequestMapping(value = "/createWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Webinar createWebinar(@RequestBody CreateWebinarReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        String url = growthCmsWebinarBasePath + "/createWebinar";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, Webinar.class);
    }

    @RequestMapping(value = "/bulkCreateWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> createWebinar(@RequestBody List<CreateWebinarReq> createWebinarReqs) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);

        String url = growthCmsWebinarBasePath + "/bulkCreateWebinar";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(createWebinarReqs));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<HashMap<String, String>>() {
        }.getType();

        return gson.fromJson(res, _type);
    }

    @RequestMapping(value = "/editWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Webinar editWebinar(@RequestBody EditWebinarReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        String url = growthCmsWebinarBasePath + "/editWebinar";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, Webinar.class);
    }

    @RequestMapping(value = "/changeWebinarStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Webinar changeWebinarStatus(@RequestBody EditWebinarReq req) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);

        String url = growthCmsWebinarBasePath + "/changeWebinarStatus";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, Webinar.class);
    }

    @RequestMapping(value = "/getWebinarByCode", method = RequestMethod.GET)
    @ResponseBody
    public WebinarResponse getWebinarByCode(FindWebinarsReq req) throws VException {
        String url = growthCmsWebinarBasePath + "/getWebinarByCode";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarResponse.class);
    }

    @RequestMapping(value = "/getWebinarByCodeMobile", method = RequestMethod.GET)
    @ResponseBody
    public WebinarResponse getWebinarByCodeMobile(FindWebinarsReq req) throws VException {
//        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);
        String url = growthCmsWebinarBasePath + "/getWebinarByCodeMobile";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, WebinarResponse.class);
    }

    @RequestMapping(value = "/getWebinarById", method = RequestMethod.GET)
    @ResponseBody
    public Webinar getWebinarById(@RequestParam(value = "webinarId", required = true) String webinarId) throws VException {
        String url = growthCmsWebinarBasePath + "/getWebinarById?webinarId=" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, Webinar.class);
    }

    @RequestMapping(value = "/isWebinarCodeAvailable", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse isWebinarCodeAvailable(FindWebinarsReq req) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        String url = growthCmsWebinarBasePath + "/isWebinarCodeAvailable";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, PlatformBasicResponse.class);
    }

    @RequestMapping(value = "/findWebinars", method = RequestMethod.GET)
    @ResponseBody
    public List<Webinar> findWebinars(FindWebinarsReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        HttpSessionData data = httpSessionUtils.getCurrentSessionData();
        if(data != null) {
            if(data.getRole() != null && Role.ADMIN.equals(data.getRole())) {
                req.setShowSimLiveWebinars(true);
            }
        }
        String url = growthCmsWebinarBasePath + "/findWebinars";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<Webinar>>() {
        }.getType();

        return gson.fromJson(res, _type);
    }

    @RequestMapping(value = "/findWebinarsMobile", method = RequestMethod.GET)
    @ResponseBody
    public List<WebinarResponse> findWebinarsMobile(FindWebinarsReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        String url = growthCmsWebinarBasePath + "/findWebinarsMobile";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<WebinarResponse>>() {
        }.getType();

        return gson.fromJson(res, _type);
    }

    @RequestMapping(value = "/getUpcomingWebinar", method = RequestMethod.GET)
    @ResponseBody
    public Webinar getUpcomingWebinar(FindWebinarsReq req) throws VException {
        String url = growthCmsWebinarBasePath + "/getUpcomingWebinar";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, Webinar.class);
    }

    @RequestMapping(value = "/findQuizzes", method = RequestMethod.GET)
    @ResponseBody
    public FindQuizzesRes findQuizzes() throws VException {
        String url = growthCmsWebinarBasePath + "/findQuizzes";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, FindQuizzesRes.class);
    }

    @RequestMapping(value = "/findLiveSessions", method = RequestMethod.GET)
    @ResponseBody
    public FindQuizzesRes findLiveSessions() throws VException {
        String url = growthCmsWebinarBasePath + "/findLiveSessions";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, FindQuizzesRes.class);

    }

    @RequestMapping(value = "/getWebinarsForGradeBoard", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseMobileRes> getWebinarForGradeBoard(@RequestParam(name = "board", required = false) String board,
                                                         @RequestParam(name = "grade", required = true) String grade,
                                                         @RequestParam(value = "start", required = false) Integer start,
                                                         @RequestParam(value = "size", required = false) Integer size) throws VException {

        String url = growthCmsWebinarBasePath + "/getWebinarsForGradeBoard?board=" + board
                + "&grade=" + grade;

        if(start!=null)
            url += "&start=" + start;

        if(size!=null)
            url += "&size=" + size;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<CourseMobileRes>>() {
        }.getType();
        return gson.fromJson(res, _type);

    }

    @RequestMapping(value = "/getPastWebinarsForGradeBoard", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseMobileRes> getPastWebinarForGradeBoard(@RequestParam(name = "board", required = false) String board,
                                                             @RequestParam(name = "grade", required = true) String grade,
                                                             @RequestParam(value = "start", required = false) Integer start,
                                                             @RequestParam(value = "size", required = false) Integer size) throws VException {

        String url = growthCmsWebinarBasePath + "/getWebinarsForGradeBoard?board=" + board
                + "&grade=" + grade;

        if(start!=null)
            url += "&start=" + start;

        if(size!=null)
            url += "&size=" + size;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<CourseMobileRes>>() {
        }.getType();
        return gson.fromJson(res, _type);

    }

    @RequestMapping(value = "/findUpcomingWebinars", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<WebinarResponse> findWebinarsForHomeFeed(@RequestBody FindWebinarsReq findWebinarsReq) throws VException {
        String url = growthCmsWebinarBasePath + "/findUpcomingWebinars";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(findWebinarsReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        Type _type = new TypeToken<List<WebinarResponse>>() {
        }.getType();
        return gson.fromJson(res, _type);
    }

    @RequestMapping(value = "/webinarHomeDemoDetailsSendToLeadSquaredThroughSqs", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void webinarHomeDemoDetailsSendToLeadSquaredThroughSqs(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                                  @RequestBody String request) throws Exception {
        logger.info("WEBINAR HOMEDEMO LEADSQUARED sns triggered with : "+request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            String message = subscriptionRequest.getMessage();
            logger.info("webinar home demo update to leadsquared subject : "+subject);
            logger.info("webinar home demo update to leadsquared message : " + message);
            webinarManager.webinarHomeDemoDetailsSendToLeadSquaredThroughSqs(message);
        }
    }

    @RequestMapping(value = "/salesDemoSendToLeadSquaredThroughSqs", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void salesDemoSendToLeadSquaredThroughSqs(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                     @RequestBody String request) throws Exception {
        logger.info("SALES DEMO LEADSQUARED sns triggered with : "+request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            String message = subscriptionRequest.getMessage();
            logger.info("sales demo update to leadsquared subject : "+subject);
            logger.info("sales demo update to leadsquared message : " + message);
            webinarManager.salesDemoSendToLeadSquaredThroughSqs(message);
        }
    }

}
