/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.lms;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.request.GetContentInfosRes;
import com.vedantu.lms.request.GetAllContentsReq;
import com.vedantu.lms.request.GetContentsReq;
import com.vedantu.platform.managers.lms.LMSManager;
import com.vedantu.platform.managers.moodle.MoodleManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

/**
 *
 * @author shyam
 */
@RestController
@RequestMapping("lms")
public class LMSController {

	@Autowired
	private LMSManager lmsManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private MoodleManager moodleManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LMSController.class);

	@RequestMapping(value = "/getContents", method = RequestMethod.GET)
	public GetContentInfosRes getUserContents(@ModelAttribute GetContentsReq getContentsReq)
			throws VException, IllegalArgumentException, IllegalAccessException {
		// TODO : Authentication
		return lmsManager.getContents(getContentsReq);
	}

	@RequestMapping(value = "/getTeacherDashboardInfo", method = RequestMethod.GET)
	public String getTeacherDashBoardInfo() throws VException {
		HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
		if (!Role.TEACHER.equals(sessionData.getRole()) || sessionData.getUserId() == null) {
			String errorMessage = "getTeacherDashBoardInfo - Action not allowed for this user "
					+ sessionData.toString();
			logger.error(errorMessage);
			throw new VException(ErrorCode.ACTION_NOT_ALLOWED, errorMessage);
		}
		return lmsManager.getTeacherDashBoardInfo(sessionData.getUserId());
	}


	@RequestMapping(value = "/getAllContents", method = RequestMethod.GET)
	public String getAllContents(GetAllContentsReq getAllContentsReq)
			throws IllegalArgumentException, IllegalAccessException, VException {
		return lmsManager.getAllContents(getAllContentsReq);
	}

	@RequestMapping(value = "/getContentById", method = RequestMethod.GET)
	public String getContentById(@RequestParam(name = "contentInfoId", required = false) String contentId)
			throws VException {
		return lmsManager.getContentById(contentId);
	}

	@RequestMapping(value = "/expireContents", method = RequestMethod.GET)
	public void expireContents() throws VException {
		lmsManager.expireContents();
	}

	@RequestMapping(value = "/expireContentsBeforeTime", method = RequestMethod.GET)
	public void expireContentsBeforeTime(@RequestParam(name = "beforeTime", required = false) Long beforeTime)
			throws VException {
		lmsManager.expireContentsBeforeTime(beforeTime);
	}

	@RequestMapping(value = "/sendPendingEvaluationNotifications", method = RequestMethod.GET)
	public void sendPendingEvaluationNotifications(
			@RequestParam(name = "allPending", required = true) boolean allPending) throws VException {
		lmsManager.sendPendingContentNotifications(allPending);
	}


	@RequestMapping(value = "/moodleContainerPage", method = RequestMethod.GET)
	public PlatformBasicResponse moodleContainerPage(@RequestParam(name = "sessionId", required = true) String sessionId)
			throws VException {
                String url=moodleManager.moodleContainerUrl(sessionId);
                PlatformBasicResponse response=new PlatformBasicResponse();
                if(StringUtils.isNotEmpty(url)){
                    response.setResponse(url);
                }else{
                    response.setSuccess(false);
                }
		return response;
	}

	@RequestMapping(value = "/viewContent", method = RequestMethod.GET)
	public void moodleContainerPage(HttpServletRequest request, HttpServletResponse response)
			throws VException, IOException {
		moodleManager.enrollToPublicGroup(request, response);
	}

	@RequestMapping(value = "/expireContents", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void expireContents(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			moodleManager.expireContents();
		}
	}


	@RequestMapping(value = "/moodleReminders", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void moodleReminders(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			moodleManager.moodleReminders();
		}
	}

	@RequestMapping(value = "getPendingEvaluationCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public void sendPendingContentsEmail(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
			@RequestBody String request) throws UnknownHostException, AddressException, NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {
		Gson gson = new Gson();
		gson.toJson(request);
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messgaetype.equals("SubscriptionConfirmation")) {
			String json = null;
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info("ClientResponse : " + resp.toString());
		} else if (messgaetype.equals("Notification")) {
			lmsManager.getPendingEvaluations(false);
		}
	}

	@RequestMapping(value = "/getPendingEvaluationContents", method = RequestMethod.GET)
	public void getPendingEvaluationContents(@RequestParam(name = "allPending", required = true) boolean allPending) {
		lmsManager.getPendingEvaluations(allPending);
	}
	
}
