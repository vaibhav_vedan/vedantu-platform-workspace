package com.vedantu.platform.controllers.onetofew;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;

@RestController
@RequestMapping("onetofew/webinar")
public class WebinarController {

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(WebinarController.class);

	private static Gson gson = new Gson();

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse webinarRegister(@RequestBody WebinarUserRegistrationInfo req) throws VException {
		logger.info("webinar request :" + req.toString());
		// Handle user data
		if (!StringUtils.isEmpty(req.getUserId())) {
			UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(req.getUserId(), true);
			req.updateUserBasicInfo(userBasicInfo);
		}

		ClientResponse resp = WebUtils.INSTANCE.doCall(
				ConfigUtils.INSTANCE.getStringValue("VEDANTUDATA_ENDPOINT") + "/vedantu/webinar/register",
				HttpMethod.POST, gson.toJson(req));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return new PlatformBasicResponse();
	}
}