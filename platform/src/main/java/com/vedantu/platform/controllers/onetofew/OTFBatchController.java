package com.vedantu.platform.controllers.onetofew;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.request.CreateBatchCurriculumReq;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.AgendaPojo;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.BatchReq;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFUpdateSessionRes;
import com.vedantu.onetofew.request.GetBatchRes;
import com.vedantu.onetofew.request.GetBatchesReq;
import com.vedantu.onetofew.request.MarkBatchInactiveReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.cmds.managers.CurriculumManager;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/onetofew/batch")
public class OTFBatchController {

    private static Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFBatchController.class);

    // one to few end point is configured to contain / at the end
    private String subscriptionEndPoint;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    PaymentManager paymentManager;

    @Autowired
    CurriculumManager curriculumManager;

    @Autowired
    public RedisDAO redisDAO;

    private String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT") + "/";

    @PostConstruct
    public void init() {
        subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public BatchInfo addBatch(@RequestBody BatchReq request) throws IOException, VException {
        // String requestBody = PlatformTools.getBody(request);
        List<BaseInstalmentInfo> baseInstalmentInfos = request.getInstalmentDetails();
        if (StringUtils.isEmpty(request.getId())) {
            paymentManager.validateBaseInstalmentInfos(request.getCourseId(), baseInstalmentInfos);
        }
        if(baseInstalmentInfos != null  ) {
            int totalInstallment = 0;
            for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
                totalInstallment += baseInstalmentInfo.getAmount();
                if(baseInstalmentInfo.getDueTime()==null || baseInstalmentInfo.getDueTime()<=0){
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Base installment time couldn't be null or 0");
                }
            }
            if (totalInstallment < request.getPurchasePrice() && !baseInstalmentInfos.isEmpty()) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Total sum of installment should be equal or greater then purchase price");
            }
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "batch", HttpMethod.POST,
                new Gson().toJson(request));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);


     //   if (StringUtils.isEmpty(request.getId())  ||  request.getStartTime() > System.currentTimeMillis()) {
            BaseInstalment baseInstalment = new BaseInstalment(InstalmentPurchaseEntity.BATCH, batchInfo.getBatchId(),
                    baseInstalmentInfos);
            paymentManager.updateBaseInstalment(baseInstalment);
            batchInfo.setInstalmentDetails(baseInstalmentInfos);
       // }

       
        // TODO later when this api is moved to scheduled, the below update should be triggered using queue
        ClientResponse markCalendarResp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "calendarEntry/markBatchSessionsCalendar/"+batchInfo.getId(), HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(markCalendarResp);
        String markCalendarRespJson = markCalendarResp.getEntity(String.class);
        logger.info("Response : " + markCalendarRespJson);
        
        
        
        CreateBatchCurriculumReq createBatchCurriculumReq = new CreateBatchCurriculumReq();
        createBatchCurriculumReq.setCallingUserId(request.getCallingUserId());
        createBatchCurriculumReq.setEntityName(CurriculumEntityName.OTF_BATCH);
        createBatchCurriculumReq.setEntityId(batchInfo.getBatchId());
        createBatchCurriculumReq.setParentEntityName(CurriculumEntityName.OTF_COURSE);
        createBatchCurriculumReq.setParentEntityId(request.getCourseId());
        Map<String, Object> payload2 = new HashMap<String, Object>();
        payload2.put("createBatchCurriculumReq", createBatchCurriculumReq);
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.CREATE_BATCH_CURRICULUM, payload2);
        asyncTaskFactory.executeTask(params2);
        return batchInfo;
    }

    @RequestMapping(value = "/markBatchInactive", method = RequestMethod.POST)
    @ResponseBody
    public BatchInfo markBatchInactive(@RequestBody MarkBatchInactiveReq markBatchInactiveReq)
            throws IOException, VException {
        markBatchInactiveReq.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "batch/markBatchInactive", HttpMethod.POST,
                gson.toJson(markBatchInactiveReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);
        BatchInfo batchInfo = gson.fromJson(jsonString, BatchInfo.class);

        // OTF task to handle calendar
        //TODO trigger this from queue when moving to scheduling/subscription
        ClientResponse unmarkCalendarResp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "calendarEntry/unmarkBatchSessionsCalendar/"+batchInfo.getId(), HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(unmarkCalendarResp);
        String unmarkCalendarRespJson = unmarkCalendarResp.getEntity(String.class);
        logger.info("Response : " + unmarkCalendarRespJson);
        
        return batchInfo;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchInfo getBatchTemp(@PathVariable("id") String id,
            @RequestParam(name = "returnEnrollments", required = false) Boolean returnEnrollments) throws Exception {
        return getBatch(id, returnEnrollments);
    }

    @RequestMapping(value = "/getBatch/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchInfo getBatch(@PathVariable("id") String id,
            @RequestParam(name = "returnEnrollments", required = false) Boolean returnEnrollments) throws Exception {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        String url = subscriptionEndPoint + "batch/" + id;
        String connector = "";
        if (returnEnrollments != null) {
            url += "?returnEnrollments=" + returnEnrollments;
            connector += "&";
        }
        if (StringUtils.isEmpty(connector)) {
            connector += "?";
        }
        if (sessionData == null || sessionData.getRole() == null) {
            url += connector + "returnStudentInfo=" + Boolean.FALSE;
        } else if (Role.STUDENT.equals(sessionData.getRole())) {
            url += connector + "returnStudentInfo=" + Boolean.FALSE;
            url += "&callingUserId=" + sessionData.getUserId();
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        BatchInfo batchInfo = gson.fromJson(jsonString, BatchInfo.class);

        if (sessionData != null && Role.STUDENT.equals(sessionData.getRole())) {
            Long studentId = sessionData.getUserId();
            List<EnrollmentPojo> enrollments = batchInfo.getEnrollments();
            Map<String, EnrollmentPojo> finalEnrollmentPojos = new HashMap<>();
            Long latestInactiveTime = null;
            if (ArrayUtils.isNotEmpty(enrollments)) {
                //TODO fetch enrollments and filter them here
                //this won't do anything as enrollments are coming as empty
                for (EnrollmentPojo enrollmentPojo : enrollments) {
                    if (enrollmentPojo.getUserId().equals(studentId.toString())) {
                        if (finalEnrollmentPojos.containsKey(studentId.toString())) {
                            EnrollmentPojo _en = finalEnrollmentPojos.get(studentId.toString());
                            if (_en.getLastUpdated() < enrollmentPojo.getLastUpdated()) {
                                finalEnrollmentPojos.put(studentId.toString(), enrollmentPojo);
                            }
                        } else {
                            finalEnrollmentPojos.put(studentId.toString(), enrollmentPojo);
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(finalEnrollmentPojos.values())) {
                    for (EnrollmentPojo enrollmentPojo : finalEnrollmentPojos.values()) {
                        if (!EntityStatus.ACTIVE.equals(enrollmentPojo.getStatus())) {
                            latestInactiveTime = enrollmentPojo.getLastUpdated();
                        }
                    }
                    batchInfo.setEnrollments(new ArrayList<>(finalEnrollmentPojos.values()));
                }
            }
            if (latestInactiveTime != null && ArrayUtils.isNotEmpty(batchInfo.getAgenda())) {
                List<AgendaPojo> finalAgendaPojos = new ArrayList<>();
                for (AgendaPojo agendaPojo : batchInfo.getAgenda()) {
                    if (agendaPojo.getSessionPOJO() != null
                            && agendaPojo.getSessionPOJO().getStartTime() != null
                            && agendaPojo.getSessionPOJO().getStartTime() > latestInactiveTime) {
                        continue;
                    }
                    finalAgendaPojos.add(agendaPojo);
                }
                batchInfo.setAgenda(finalAgendaPojos);
            }
        }

        BaseInstalment baseInstalment = paymentManager.getBaseInstalment(InstalmentPurchaseEntity.BATCH, id);
        if (baseInstalment != null) {
            batchInfo.setInstalmentDetails(baseInstalment.getInfo());
        }
        try {
            List<String> currIds = curriculumManager.checkIfCurriculumExists(Arrays.asList(id));
            if (ArrayUtils.isNotEmpty(currIds)) {
                if (currIds.contains(batchInfo.getBatchId()) || currIds.contains(batchInfo.getId())) {
                    batchInfo.setCurriculumExists(Boolean.TRUE);
                }
            }
        } catch (Exception e) {
            logger.info("curriculum does not exist for " + batchInfo.getBatchId());
        }
        return batchInfo;
    }

    @Deprecated
    @RequestMapping(value = "/batchBasicInfo/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchInfo getBatchBasicInfo(@PathVariable("id") String id) throws Exception {
        String url = subscriptionEndPoint + "batch/" + id;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BatchInfo batchInfo = gson.fromJson(jsonString, BatchInfo.class);
        return batchInfo;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetBatchRes getBatchesTemp(GetBatchesReq getBatchesReq)
            throws VException, IllegalArgumentException, IllegalAccessException, UnsupportedEncodingException {
        return getBatches(getBatchesReq);
    }

    @RequestMapping(value = "/getBatch", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetBatchRes getBatches(GetBatchesReq getBatchesReq)
            throws VException, IllegalArgumentException, IllegalAccessException, UnsupportedEncodingException {

        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null) {
            getBatchesReq.setReturnEnrollments(Boolean.FALSE);
        } else if ((sessionData.getRole() == null || Role.STUDENT.equals(sessionData.getRole()))) {
            getBatchesReq.setReturnEnrollments(Boolean.FALSE);
        }
        String getBatchesUrl = subscriptionEndPoint + "batch" + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getBatchesReq);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getBatchesUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GetBatchRes getBatchRes = gson.fromJson(jsonString, GetBatchRes.class);
        if (getBatchRes != null && ArrayUtils.isNotEmpty(getBatchRes.getList())) {
            List<String> batchIds = new ArrayList<>();
            for (BatchInfo batch : getBatchRes.getList()) {
                batchIds.add(batch.getBatchId());
            }
            try {
                List<String> currIds = curriculumManager.checkIfCurriculumExists(batchIds);
                if (ArrayUtils.isNotEmpty(currIds)) {
                    for (BatchInfo batch : getBatchRes.getList()) {
                        if (currIds.contains(batch.getBatchId())) {
                            batch.setCurriculumExists(Boolean.TRUE);
                        }
                    }
                }
            } catch (Exception e) {
                logger.info("Logging in currciculum Manager");
            }
        }
        return getBatchRes;
    }

    @Deprecated
    @RequestMapping(value = "/updateSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public OTFUpdateSessionRes updateSession(HttpServletRequest request) throws IOException, VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String requestBody = PlatformTools.getBody(request);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "onetofew/session/updateSession", HttpMethod.POST,
                requestBody);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        OTFUpdateSessionRes updateSessionRes = gson.fromJson(jsonString, OTFUpdateSessionRes.class);
        return updateSessionRes;
    }     
}
