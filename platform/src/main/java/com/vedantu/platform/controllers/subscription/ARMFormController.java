package com.vedantu.platform.controllers.subscription;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.subscription.ARMFormManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.subscription.request.AddARMFormReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.response.ARMFormInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import javax.validation.Valid;

@RestController
@RequestMapping("/armform")
public class ARMFormController {

	@Autowired
	private ARMFormManager aRMFormManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(ARMFormController.class);

	@Autowired
	HttpSessionUtils httpSessionUtils;

	@RequestMapping(value = "/addARMForm", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ARMFormInfo addARMForm(@Valid @RequestBody AddARMFormReq addARMFormReq)
			throws VException, AddressException, UnsupportedEncodingException {
                boolean exposeEmail = false;
                HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
                if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                        || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
                    exposeEmail = true;
                }            
		return aRMFormManager.addARMForm(addARMFormReq, exposeEmail);
	}

        //TODO concern: anybody can call this api
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ARMFormInfo getARMForm(@PathVariable("id") String id) throws VException {
		List<Role> allowedRoles = new ArrayList<>();
		allowedRoles.add(Role.ADMIN);
		allowedRoles.add(Role.STUDENT_CARE);
		sessionUtils.checkIfAllowedList(null, allowedRoles, true);
		return aRMFormManager.getARMForm(id);
	}

	@RequestMapping(value = "/getARMForms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ARMFormInfo> getARMForms(ExportCoursePlansReq req) throws VException {
		List<Role> allowedRoles = new ArrayList<>();
		allowedRoles.add(Role.ADMIN);
		allowedRoles.add(Role.STUDENT_CARE);
		sessionUtils.checkIfAllowedList(null, allowedRoles, true);
		return aRMFormManager.getARMForms(req);
	}

        //TODO concern: anybody can call this api
	@RequestMapping(value = "/getARMFormByAdmin/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ARMFormInfo> getARMFormByAdmin(@PathVariable("id") Long id) throws VException {
		List<Role> allowedRoles = new ArrayList<>();
		allowedRoles.add(Role.ADMIN);
		allowedRoles.add(Role.STUDENT_CARE);
		sessionUtils.checkIfAllowedList(null, allowedRoles, true);
		return aRMFormManager.getARMFormByAdmin(id);
	}

        //TODO concern: anybody can call this api
	@RequestMapping(value = "/getARMFormByStartDate/{startDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ARMFormInfo> getARMFormByStartDate(@PathVariable("startDate") Long startDate) throws VException {
		List<Role> allowedRoles = new ArrayList<>();
		allowedRoles.add(Role.ADMIN);
		allowedRoles.add(Role.STUDENT_CARE);
		sessionUtils.checkIfAllowedList(null, allowedRoles, true);
		return aRMFormManager.getARMFormByStartDate(startDate);
	}

        //TODO concern: anybody can call this api
	@RequestMapping(value = "/getARMFormByStudentId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ARMFormInfo> getARMFormByStudentId(@PathVariable("id") Long id) throws VException {
		List<Role> allowedRoles = new ArrayList<>();
		allowedRoles.add(Role.ADMIN);
		allowedRoles.add(Role.STUDENT_CARE);
		sessionUtils.checkIfAllowedList(null, allowedRoles, true);
		return aRMFormManager.getARMFormByStudentId(id);
	}

	@RequestMapping(value = "/exportARMForm", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse exportARMForm(@RequestBody ExportCoursePlansReq exportArmFormReq)
			throws VException, AddressException, UnsupportedEncodingException {
		List<Role> allowedRoles = new ArrayList<>();
		allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
		sessionUtils.checkIfAllowedList(null, allowedRoles, true);

		aRMFormManager.exportARMForm(exportArmFormReq.getFromTime(), exportArmFormReq.getTillTime(), sessionUtils.getCallingUserId());
		return new PlatformBasicResponse();
	}
}
