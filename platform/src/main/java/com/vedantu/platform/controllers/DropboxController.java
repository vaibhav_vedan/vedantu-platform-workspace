package com.vedantu.platform.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.enums.SnsOrders;
import com.vedantu.platform.managers.DropboxManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.util.Objects;

@RestController
@Api(value = "DropboxManager")
@RequestMapping("/dropbox")
public class DropboxController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DropboxController.class);

    @Autowired
    private DropboxManager dropboxManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    private Gson gson = new Gson();

    @RequestMapping(value = "/file-changes/webhook", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.TEXT_PLAIN)
    @ResponseBody
    public String dbxFileChangeWebHook(@RequestHeader(value="X-Dropbox-Signature", required = false) String dropboxSignature,
                                       @RequestBody(required = false) String requestBody,
                                       @RequestParam(name = "challenge", required = false) String challenge,
                                       HttpServletResponse response) throws Exception {
        logger.info("Dbx Signature: " + dropboxSignature);
        logger.info("Dbx Request Body: " + requestBody);
        logger.info("Dbx Request Challenge: " + challenge);
        dropboxManager.executeAsyncTask(requestBody, dropboxSignature);

        logger.info("Dbx Web Hook Success");
        response.setHeader("X-Content-Type-Options", "nosniff");
        return Objects.isNull(challenge) ? "" : challenge;
    }

    @RequestMapping(value = "/file-change/sns", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.TEXT_PLAIN)
    @ResponseBody
    public void dbxSnsRequest(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                @RequestBody String request) throws Exception {

        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            dropboxManager.executeAsyncTask("", "");
        }

        logger.info("Dbx Web Hook Success");
    }


}
