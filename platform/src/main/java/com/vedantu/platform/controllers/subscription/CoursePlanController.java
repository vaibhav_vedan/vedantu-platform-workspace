/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.subscription;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.request.session.EndSessionReq;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.response.*;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/courseplan")
public class CoursePlanController {

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(CoursePlanController.class);

    @Autowired
    HttpSessionUtils httpSessionUtils;

    @Autowired
    HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/addEditCoursePlan", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo addEditCoursePlan(@RequestBody AddEditCoursePlanReq addEditCoursePlanReq) throws Throwable {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        roles.add(Role.STUDENT_CARE);
        httpSessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return coursePlanManager.addEditCoursePlan(addEditCoursePlanReq);
    }

    /**
     * migrated to subscription
     */
    @Deprecated
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo getCoursePlan(@PathVariable("id") String id) throws VException {
        boolean exposeEmail = httpSessionUtils.isAdminOrStudentCare(httpSessionUtils.getCurrentSessionData());
        CoursePlanInfo coursePlanInfo = coursePlanManager.getCoursePlan(id, true, exposeEmail);
        validateCoursePlanAccess(coursePlanInfo);
        return coursePlanInfo;
    }

    @RequestMapping(value = "/basicInfo/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanBasicInfo getCoursePlanBasicInfo(@PathVariable("id") String id) throws VException {
        logger.info("request for basicinfo");
        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        CoursePlanBasicInfo coursePlanBasicInfo = coursePlanManager.getCoursePlanBasicInfo(id, true, exposeEmail);
        validateCoursePlanAccess(coursePlanBasicInfo);
        return coursePlanBasicInfo;
    }

    //TODO concern: anybody can call this api
    /**
     * migrated to subscription
     */
    @Deprecated
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CoursePlanInfo> getCoursePlanInfos(GetCoursePlansReq req) throws VException {

        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        List<CoursePlanInfo> cinfos = coursePlanManager.getCoursePlanInfos(req, true, exposeEmail);
        return cinfos;
    }

    @Deprecated
    @RequestMapping(value = "/addEditStructuredCourse", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String addEditStructuredCourse(@RequestBody AddEditStructuredCourseReq addEditStructuredCourseReq) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return coursePlanManager.addEditStructuredCourse(addEditStructuredCourseReq);
    }

    //TODO concern: anybody can call this api
    @Deprecated
    @RequestMapping(value = "/structuredCourse/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StructuredCourseInfo getStructuredCourse(@PathVariable("id") String id) throws VException {
        return coursePlanManager.getStructuredCourse(id);
    }

    /**
     * migrated to subscription
     */
    @Deprecated
    @RequestMapping(value = "/structuredCourses", method = RequestMethod.GET)
    @ResponseBody
    public List<StructuredCourseInfo> getStructuredCourse(GetStructuredCoursesReq req) throws
            VException, IllegalArgumentException, IllegalAccessException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return coursePlanManager.getStructuredCourses(req);
    }

    @RequestMapping(value = "/payTrialAmount", method = RequestMethod.POST)
    @ResponseBody
    public PayTrialAmountRes payTrialAmount(@RequestBody PayTrialAmountReq req) throws
            VException {
        httpSessionUtils.checkIfAllowedList(req.getUserId(), null, false);
        return coursePlanManager.payTrialAmount(req);
    }

    @RequestMapping(value = "/bookSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SessionInfo> bookSession(@RequestBody CoursePlanSessionRequest sessionRequest) throws VException {
        //httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return coursePlanManager.bookSession(sessionRequest);
    }

    @RequestMapping(value = "/markTrialPaymentDone", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markTrialPaymentDone(@RequestBody MarkTrialPaymentDoneReq req) throws VException {
        httpSessionUtils.checkIfAllowedList(req.getUserId(), null, false);
        return coursePlanManager.markTrialPaymentDone(req);
    }

    @RequestMapping(value = "/checkIfPaidTrialRegFeeAvailable", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CheckIfRegFeePaidRes checkIfRegFeePaid(@RequestBody MarkTrialPaymentDoneReq req) throws VException {
        httpSessionUtils.checkIfAllowedList(req.getCallingUserId(), null, Boolean.TRUE);
        return coursePlanManager.checkIfRegFeePaid(req);
    }

    @RequestMapping(value = "/endCoursePlan", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RefundRes endCoursePlan(@RequestBody EndCoursePlanReq req) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return coursePlanManager.endCoursePlan(req);
    }

    @RequestMapping(value = "/cancelSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionInfo cancelSession(@Valid @RequestBody EndSessionReq endSessionReq) throws VException {
        httpSessionUtils.checkIfAllowedList(null, null, Boolean.TRUE);
        return coursePlanManager.cancelSession(endSessionReq);
    }

    @RequestMapping(value = "/enroll", method = RequestMethod.POST)
    @ResponseBody
    public EnrollCoursePlanRes enroll(@RequestBody EnrollCoursePlanReq req) throws
            VException, CloneNotSupportedException {
        httpSessionUtils.checkIfAllowedList(req.getUserId(), null, false);
        return coursePlanManager.enroll(req);
    }

    @RequestMapping(value = "/prepareDiscountedInstalments", method = RequestMethod.GET)
    @ResponseBody
    public List<InstalmentInfo> prepareDiscountedInstalments(EnrollCoursePlanReq req) throws VException, CloneNotSupportedException {
        httpSessionUtils.checkIfAllowedList(req.getUserId(), null, false);
        return coursePlanManager.prepareDiscountedInstalments(req);
    }

    @RequestMapping(value = "/addCoursePlanReqForm", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addCoursePlanReqForm(@RequestBody CoursePlanReqFormReq req) throws
            VException, NotFoundException, Exception {
        httpSessionUtils.checkIfAllowedList(req.getStudentId(), null, false);
        return coursePlanManager.addCoursePlanReqForm(req);
    }

    @RequestMapping(value = "/alertAboutFirstRegularSessionDate", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse alertAboutFirstRegularSessionDate(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            payload.put("start", 0);
            payload.put("size", 100);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ALERT_ABOUT_FIRST_REGULAR_SESSION_DATE, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/alertNonTrialPayment", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse alertNonTrialPayment(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            payload.put("start", 0);
            payload.put("size", 100);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ALERT_ABOUT_NON_TRIAL_PAYMENT, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/handleSessionEvents", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleSessionEvents(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SessionEvents eventType = SessionEvents.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            coursePlanManager.handleSessionEvents(eventType, message);
        }
    }

    @RequestMapping(value = "/editInstalmentSchedule", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse editInstalmentSchedule(@RequestBody EditInstalmentAmtAndSchedule req) throws
            VException, NotFoundException, Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return coursePlanManager.editInstalmentSchedule(req);
    }

    @RequestMapping(value = "/verifyCoursePlanHoursConsistency", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void verifyCoursePlanHoursConsistency(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            coursePlanManager.verifyCoursePlanHoursConsistencyAsync();
        }
    }

    @RequestMapping(value = "/getCoursePlanTrialSessionList", method = RequestMethod.GET)
    @ResponseBody
    public List<GetCoursePlanReminderListRes> getCoursePlanTrialSessionList(GetCoursePlanReminderListReq req) throws IllegalArgumentException, IllegalAccessException, VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return coursePlanManager.getCoursePlanTrialSessionList(req);
    }

    private void validateCoursePlanAccess(CoursePlanBasicInfo coursePlanInfo) throws ForbiddenException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && coursePlanInfo != null) {
            Long userId = sessionData.getUserId();
            if ((Role.STUDENT.equals(sessionData.getRole()) && !coursePlanInfo.getStudentId().equals(userId))
                    || (Role.TEACHER.equals(sessionData.getRole()) && !coursePlanInfo.getTeacherId().equals(userId))) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not allowed to access " + coursePlanInfo.getId());
            }
        }
    }

    /**
     * Do expose this API for public, this is for internal use only.
     *
     * @param coursePlanId
     * @param secretKey
     * @return
     * @throws com.vedantu.exception.VException
     */
    @Deprecated
    @RequestMapping(value = "/processCoursePlanHrs", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getUserDashboardCoursePlans(@RequestParam(name = "coursePlanId", required = true) String coursePlanId, @RequestParam(name = "secretKey", required = true) String secretKey) throws VException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || !("uuZByTsaZHdJLwE5W9SxyEu4UWPQ3rhvt8H".equals(secretKey))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not LoggedIn or invalid secret key");
        }
        return coursePlanManager.processCoursePlanHrs(coursePlanId, sessionData.getUserId());
    }

    /**
     * Do expose this API for public, this is for internal use only.
     *
     * @param coursePlanId
     * @param secretKey
     * @param paymentType
     * @param userId
     * @return
     * @throws com.vedantu.exception.VException
     */
    @Deprecated
    @RequestMapping(value = "/processCoursePlanFromUpdateCoursePlanState", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse processCoursePlanFromUpdateCoursePlanState(@RequestParam(name = "coursePlanId", required = true) String coursePlanId,
            @RequestParam(name = "secretKey", required = true) String secretKey,
            @RequestParam(name = "paymentType", required = true) PaymentType paymentType,
            @RequestParam(name = "userId", required = true) Long userId
    ) throws VException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || !("uuZByTsaZHdJLwE5W9SxyEu4UWPQ3rhvt8H".equals(secretKey))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not LoggedIn or invalid secret key");
        }
        coursePlanManager.processCoursePlanFromUpdateCoursePlanState(userId, coursePlanId, paymentType);
        return new PlatformBasicResponse();
    }

}
