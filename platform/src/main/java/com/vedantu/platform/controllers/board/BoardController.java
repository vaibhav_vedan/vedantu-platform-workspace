/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.board;

import com.vedantu.User.Role;
import com.vedantu.board.pojo.BoardInfoRes;
import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.board.pojo.GetBoardsRes;
import com.vedantu.board.requests.CreateBoardReq;
import com.vedantu.board.requests.GetBoardsReq;
import com.vedantu.board.requests.UpdateBoardReq;
import com.vedantu.exception.VException;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/board")
public class BoardController {
        @Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(BoardController.class);
        
	@Autowired
	BoardManager boardManager;

	@Autowired
	HttpSessionUtils sessionUtils;

	@RequestMapping(value = "/createBoard", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BoardInfoRes createBoard(@RequestBody CreateBoardReq req) throws VException {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                return boardManager.createBoard(req);
	}
        
	@RequestMapping(value = "/updateBoard", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BoardInfoRes updateBoard(@RequestBody UpdateBoardReq req) throws VException {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                return boardManager.updateBoard(req);
	}
        
        
	@RequestMapping(value = "/getBoards", method = RequestMethod.GET)
	@ResponseBody
	public GetBoardsRes getBoards(GetBoardsReq req) throws VException {
                return boardManager.getBoards(req);
	}
        
        //TODO: Check if needed
//        @RequestMapping(value = "/getBoardById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//        @ResponseBody
//        public Board getBoardById(@RequestParam(value = "boardId", required = true) Long boardId) throws VException {
//            return boardManager.getBoardById(boardId);
//        }
//                        
//        @RequestMapping(value = "/getBoardByField", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//        @ResponseBody
//        public Board getBoardByField(@RequestParam(value = "fieldName", required = true) String fieldName,
//                @RequestParam(value = "fieldValue", required = true) String fieldValue) throws VException {
//            return boardManager.getBoardByField(fieldName, fieldValue);
//        }
//        
	@RequestMapping(value = "/getBoardsMap", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<Long, Board> getBoardsMap(@RequestParam(value = "ids", required = true) ArrayList<Long> ids) throws VException {
                return boardManager.getBoardsMap(ids);
	}
  
        //TODO this is a duplicate, ask somil why it is and change the request url whereever it is used
//        @RequestMapping(value = "/getBoards", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public List< Board> getBoards(@RequestBody Collection<Long> ids) throws VException {
//                return boardManager.getBoards(ids);
//	}
        
        
	@RequestMapping(value = "/getBoardTreeInfoForContent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BoardTreeInfo> getBoardTreeInfoForContent(@RequestBody Set<Long> ids) throws VException {
                return boardManager.getBoardTreeInfoForContent(ids);
	}
        
        
}

