package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.NonStudentRoleCreatorManager;
import com.vedantu.platform.mongodbentities.NonStudentRoleCreator;
import com.vedantu.platform.request.NonStudentRoleCreatorReq;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;


@RestController
public class NonStudentRoleCreatorController {

    @Autowired
    private NonStudentRoleCreatorManager nonStudentRoleCreatorManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/addEditNonStudentRoleCreator", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public NonStudentRoleCreator addEditNonStudentRoleCreator(@RequestBody NonStudentRoleCreatorReq request) throws VException {
        sessionUtils.checkIfAllowedList(request.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.FALSE);
        return nonStudentRoleCreatorManager.addEditNonStudentRoleCreator(request);
    }

    @RequestMapping(value = "/getExistedRoles", method = RequestMethod.GET)
    public List<Role> getExistedRoles(@RequestParam("userId") Long userId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), Boolean.FALSE);
        return nonStudentRoleCreatorManager.getExistedRoles(userId);
    }
}
