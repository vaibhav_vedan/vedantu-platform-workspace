/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserEvents;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.User.request.AcceptTncReq;
import com.vedantu.User.request.AddOrUpdatePhoneNumberReq;
import com.vedantu.User.request.AddUserSystemIntroReq;
import com.vedantu.User.request.ChangePasswordReq;
import com.vedantu.User.request.EditProfileReq;
import com.vedantu.User.request.EditUserDetailsReq;
import com.vedantu.User.request.ExistingUserISLRegReq;
import com.vedantu.User.request.ExtensionNumberReq;
import com.vedantu.User.request.ForgotPasswordReq;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.request.GetActiveTutorsReq;
import com.vedantu.User.request.GetUserInfosByEmailReq;
import com.vedantu.User.request.GetUserSystemIntroReq;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.request.ReSendContactNumberVerificationCodeReq;
import com.vedantu.User.request.SetBlockStatusForUserReq;
import com.vedantu.User.request.SetProfilePicReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.User.request.SignUpForISLReq;
import com.vedantu.User.request.SignUpReq;
import com.vedantu.User.request.SocialSource;
import com.vedantu.User.request.StudentTeacherExotelCallConnectReq;
import com.vedantu.User.request.VerifyContactNumberReq;
import com.vedantu.User.response.ConnectSessionCallRes;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.GetUserProfileRes;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.User.response.IsRegisteredForISLRes;
import com.vedantu.User.response.PhoneNumberRes;
import com.vedantu.User.response.ReSendContactNumberVerificationCodeRes;
import com.vedantu.User.response.SetProfilePicRes;
import com.vedantu.User.response.SignUpForISLRes;
import com.vedantu.User.response.SignUpRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.User.response.UserSystemIntroRes;
import com.vedantu.User.response.VerifyContactNumberRes;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.pojo.scheduling.EsTeacherInfo;
import com.vedantu.platform.pojo.user.AppSocialLoginReq;
import com.vedantu.platform.pojo.user.MessageUserReq;
import com.vedantu.platform.pojo.user.SendUserEmailReq;
import com.vedantu.platform.pojo.user.ShowInterestCourseListingReq;
import com.vedantu.platform.pojo.user.ShowInterestReq;
import com.vedantu.platform.response.AppSocialLoginRes;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/user")
// @CrossOrigin
public class UserController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserController.class);

    @Autowired
    UserManager userManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    // "https://clients.rediff.com"
    // @CrossOrigin(origins = "https://clients.rediff.com")
    @RequestMapping(value = "/signUp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SignUpRes signUpUser(@Valid @RequestBody SignUpReq req, HttpServletRequest request, HttpServletResponse response)
            throws VException, IOException, URISyntaxException {
        return userManager.signUpUser(req, request, response, null);
    }

//    @RequestMapping(value = "/signUpOrLoginApp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public SignUpRes signUpOrLoginApp(@Valid @RequestBody SignUpReq req, HttpServletRequest request, HttpServletResponse response)
//            throws VException, IOException, URISyntaxException {
//        return userManager.signUpOrLoginApp(req, request, response, null);
//    }


    // temp just for the event - will be removed later
    @RequestMapping(value = "/signUpForDemo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SignUpRes signUpUserForDemo(@RequestBody SignUpReq req, HttpServletRequest request, HttpServletResponse response)
            throws VException, IOException, URISyntaxException {
        if (StringUtils.isNotEmpty(req.getFirstName())) {
            req.setEmail(req.getFirstName().replaceAll("\\s", "") + Long.toString(System.currentTimeMillis()) + "@vedantu.com");
            req.setPassword("22dl0983lkl1871kjawwffwpjnc");
        }
        logger.info("SIGNUPDEMO: " + req);
        return userManager.signUpUser(req, request, response, null);
    }

    @Deprecated
    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse forgotPassword(@RequestBody ForgotPasswordReq req)
            throws VException, BadRequestException, UnsupportedEncodingException {
        return userManager.forgotPassword(req);
    }

    @Deprecated
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String changePassword(@Valid @RequestBody ChangePasswordReq request) throws VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (httpSessionData != null) {
            request.setUserId(httpSessionData.getUserId());
        }
        sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
        return userManager.changePassword(request);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HttpSessionData login(@RequestBody SignInReq req, HttpServletRequest request,
            HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userManager.authenticateUser(req, request, response);
    }

//    @RequestMapping(value = "/loginByOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public HttpSessionData loginByOTP(@RequestBody String requestBody, HttpServletRequest request,
//                                 HttpServletResponse response) throws VException, IOException, URISyntaxException {
//        return userManager.authenticateUserByOTP(requestBody, request, response);
//    }

    @Deprecated
    @RequestMapping(value = "/loginByToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HttpSessionData validateOneTimeLogin(@RequestBody String requestBody, HttpServletRequest request,
            HttpServletResponse response) throws VException, IOException, URISyntaxException {
        //return userManager.authenticateUserByToken(requestBody, request, response);
        return null;
    }

    @RequestMapping(value = "/getUserProfile", method = RequestMethod.GET)
    @ResponseBody
    public GetUserProfileRes getUserProfile(@RequestParam(value = "userId") Long userId) throws VException {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        if (!exposeEmail && sessionUtils.getCallingUserId() != null && sessionUtils.getCallingUserId().equals(userId)) {
            exposeEmail = true;
        }
        GetUserProfileRes res = userManager.getUserProfile(userId, exposeEmail);
        if(null != res.getRole() && res.getRole().equals(Role.STUDENT)){
            if(null != sessionUtils.getCurrentSessionData()
                    && null != sessionUtils.getCurrentSessionData().getRole()
                    && sessionUtils.getCurrentSessionData().getRole().equals(Role.STUDENT)
                    && !sessionUtils.getCallingUserId().equals(userId)){
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User is forbidden to use this API");
            }
        }
        return res;
    }

    @RequestMapping(value = "/acceptTnc", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse acceptTnc(@RequestBody AcceptTncReq request, HttpServletRequest httpRequest,
            HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
        return userManager.acceptTnc(request, httpRequest, httpResponse);
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditProfileRes editProfile(@Valid @RequestBody EditProfileReq request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.TRUE);
        return userManager.editProfile(request, httpRequest, httpResponse);
    }

    @RequestMapping(value = "/editStudentInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditProfileRes editStudentInfo(@Valid @RequestBody EditProfileReq request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        sessionUtils.checkIfAllowed(request.getUserId(), Role.STUDENT, Boolean.TRUE);
        return userManager.editStudentInfo(request, httpRequest, httpResponse);
    }

    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    @ResponseBody
    public GetUsersRes getUsers(GetUsersReq request) throws VException {
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.TEACHER);
        allowedRoles.add(Role.STUDENT_CARE);
        // TODO: check for case if a teacher misuses this api
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return userManager.getUsers(request);
    }

    //TODO concern: anybody can send a message to anybody of a different role
    @RequestMapping(value = "/messageUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse messageUser(@Valid @RequestBody MessageUserReq request) throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User is not logged in");
        }
        sessionUtils.checkIfAllowed(request.getFromUserId(), null, Boolean.FALSE);
        if (request.getFromUserId() == null) {
            request.setFromUserId(sessionData.getUserId());
        }
        return userManager.messageUser(request);
    }

//    @Deprecated
//    @RequestMapping(value = "/editContactNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public EditContactNumberRes editContactNumber(@Valid @RequestBody EditContactNumberReq request,
//            HttpServletRequest httpRequest, HttpServletResponse httpResponse)
//            throws VException, IOException, URISyntaxException {
//        sessionUtils.checkIfUserLoggedIn();
//        return userManager.editContactNumber(request, httpRequest, httpResponse);
//    }

    @Deprecated
    @RequestMapping(value = "/reSendContactNumberVerificationCode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCode(
            @RequestBody ReSendContactNumberVerificationCodeReq request) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userManager.reSendContactNumberVerificationCode(request);
    }

    @Deprecated
    @RequestMapping(value = "/updatePhoneNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneNumberRes updatePhoneNumber(@Valid @RequestBody AddOrUpdatePhoneNumberReq req) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userManager.updatePhoneNumber(req);
    }

    @Deprecated
    @RequestMapping(value = "/updateAndMarkActivePhoneNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneNumberRes updateAndMarkActivePhoneNumber(@Valid @RequestBody AddOrUpdatePhoneNumberReq req) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        return userManager.updateAndMarkActivePhoneNumber(req);
    }

    @RequestMapping(value = "/verifyContactNumber", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VerifyContactNumberRes verifyContactNumber(@RequestBody VerifyContactNumberReq req,
            HttpServletRequest request, HttpServletResponse response)
            throws VException, IOException, URISyntaxException {
        sessionUtils.checkIfUserLoggedIn();
        return userManager.verifyContactNumber(req, request, response);
    }

    @Deprecated
    @RequestMapping(value = "/getUserPhoneNumbers", method = RequestMethod.GET)
    @ResponseBody
    public String getUserPhoneNumbers(@RequestParam(value = "userId") Long userId) throws VException {
        sessionUtils.checkIfAllowed(userId, null, Boolean.TRUE);
        return userManager.getUserPhoneNumbers(userId);
    }

    // Commenting out these apis as they are not being accessed from front-end
    // @RequestMapping(value = "/updateUser", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public String updateUser(@RequestBody String request) throws VException {
    // return userManager.updateUser(request);
    // }
    //
    @RequestMapping(value = "/getUserById", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@RequestParam(value = "userId") Long userId) throws VException {
//		sessionUtils.checkIfAllowed(userId, null, true);
        return userManager.getUserById(userId);
    }

    @RequestMapping(value = "/loam_getUserById", method = RequestMethod.GET)
    @ResponseBody
    public User getloamUserById(@RequestParam(value = "userId") Long userId) throws VException {
//		sessionUtils.checkIfAllowed(userId, null, true);
        Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();

        if (subRoles == null || !(subRoles.contains(SubRole.ACADMENTOR))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User other than Acedemic Mentor are forbidden to use this API");
        }
        return userManager.getUserById(userId);
    }

    @RequestMapping(value = "/setProfilePic", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SetProfilePicRes setProfilePic(@Valid @RequestBody SetProfilePicReq request) throws VException {
        sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
        return userManager.setProfilePic(request);
    }

    //TODO concern: any teacher/admin/student_care can mark anybody else as blocked
    @Deprecated
    @RequestMapping(value = "/markBlockStatus", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse markBlockStatus(@Valid @RequestBody SetBlockStatusForUserReq setBlockStatusForUserReq)
            throws VException, UnsupportedEncodingException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        roles.add(Role.TEACHER);
        roles.add(Role.STUDENT_CARE);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.FALSE);
        return userManager.markBlockStatus(setBlockStatusForUserReq);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse logout(HttpServletRequest request, HttpServletResponse response) {
        return userManager.logout(request, response);
    }

    @RequestMapping(value = "/getCounter", method = RequestMethod.GET)
    public void getCounter(HttpServletRequest request, HttpServletResponse response) throws IOException, VException {
        userManager.getCounter(request, response);
    }

    @RequestMapping(value = "/checkUsername", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse checkUsername(@RequestParam(value = "username") String username)
            throws VException, UnsupportedEncodingException {
        return userManager.checkUsername(username);
    }

    @RequestMapping(value = "/getUserSystemIntroData", method = RequestMethod.GET)
    @ResponseBody
    public String getUserSystemIntroData(GetUserSystemIntroReq req, HttpServletRequest request) throws VException {
        sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.TRUE);
        return userManager.getUserSystemIntroData(req, request);
    }

    @RequestMapping(value = "/addUserSystemIntroData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserSystemIntroRes addUserSystemIntroData(@Valid @RequestBody AddUserSystemIntroReq addUserSystemIntroReq)
            throws VException {
        sessionUtils.checkIfAllowed(addUserSystemIntroReq.getUserId(), null, Boolean.FALSE);
        return userManager.addUserSystemIntroData(addUserSystemIntroReq);
    }

    @RequestMapping(value = "/auth.social", method = RequestMethod.GET)
    public void authSocial(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException, VException, UnsupportedEncodingException, URISyntaxException {

        SocialSource source = SocialSource.valueOfKey(request.getParameter("source"));
        if (source == SocialSource.UNKNOWN) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST,
                    "unknown source : " + request.getParameter("source"));
            return;
        }
        JsonObject jsonObject = new JsonObject();
        request.setAttribute("data", jsonObject);
        if (StringUtils.isEmpty(request.getParameter("code"))) {
            String error = request.getParameter("error");
            if (StringUtils.isNotEmpty(error)) {
                jsonObject.addProperty("error", error);
                request.getRequestDispatcher(UserManager.STATUS_SOCIAL_LOGIN).forward(request, response);
                return;
            }

            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing paramater code");
            return;
        }

        //try {
        userManager.authenticate(request, response, source);
        //} catch (Exception e) {
        //    jsonObject.addProperty("error", e.getMessage());
        //    logger.warn(e.getMessage(), e);
        //    request.getRequestDispatcher(UserManager.STATUS_SOCIAL_LOGIN).forward(request, response);
        //}
    }

    @RequestMapping(value = "/appsociallogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HttpSessionData appsociallogin(HttpServletRequest request, HttpServletResponse response,
            @RequestBody AppSocialLoginReq req) throws ServletException, IOException, VException {

        SocialSource source = req.getSource();
        String code = req.getCode();
        if (source == SocialSource.UNKNOWN) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Social Source is unknown : " + source);
        }
        if (StringUtils.isEmpty(req.getCode())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "missing paramater code");
        }

        try {
            return userManager.authenticateAppLogin(request, response, source, code);
        } catch (VException e) {
            // sendErrorJsonResponse(response, e);
            throw e;
        } catch (ServletException | IOException | URISyntaxException e) {
            // sendErrorJsonResponse(response, e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    @RequestMapping(value = "/appsocialloginnew", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AppSocialLoginRes appsocialloginnew(HttpServletRequest request, HttpServletResponse response,
            @RequestBody AppSocialLoginReq req) throws ServletException, IOException, VException {

        SocialSource source = req.getSource();
        String code = req.getCode();
        if (source == SocialSource.UNKNOWN) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Social Source is unknown : " + source);
        }
        if (StringUtils.isEmpty(req.getCode())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "missing paramater code");
        }

        try {
            return userManager.authenticateAppLoginNew(request, response, req);
        } catch (VException e) {
            // sendErrorJsonResponse(response, e);
            throw e;
        } catch (ServletException | IOException | URISyntaxException e) {
            // sendErrorJsonResponse(response, e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.GET)
    public void signUp(HttpServletRequest req, HttpServletResponse resp) throws IOException, InternalServerErrorException {
        SocialSource source = SocialSource.valueOfKey(req.getParameter("source"));
        String utmParams = req.getParameter("utmParams");
        String signUpURL = req.getParameter("signUpURL");
        String referrer = req.getParameter("referrer");
        String optIn = req.getParameter("optIn");
        if (source == SocialSource.UNKNOWN) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "unknown source");
            return;
        }
        userManager.authorize(resp, source, utmParams, signUpURL, referrer, optIn);
    }

    //TODO concern:  anybody can initiate a call with anybody
    @RequestMapping(value = "/connectcall", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConnectSessionCallRes connectCRMCall(@RequestBody StudentTeacherExotelCallConnectReq req) throws VException {
        req.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData == null || !(req.getStudentId().equals(sessionData.getUserId())
                || req.getTeacherId().equals(sessionData.getUserId()))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not allowed");
        }
        return userManager.connectCRMCall(req);
    }

    @RequestMapping(value = "/userEventsOperation", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void userEventsOperation(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            UserEvents eventType = UserEvents.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            if (eventType != null) {
                userManager.userEventsOperation(eventType, message);
            }
        }
    }

    @Deprecated
    @RequestMapping(value = "/getActiveTutorsList", method = RequestMethod.GET)
    public GetActiveTutorsRes getActiveTutorsList(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
            GetActiveTutorsReq req) throws VException {
        try {
            req.verify();
            GetActiveTutorsRes res = userManager.getActiveTutorsList(httpRequest, req);
            httpResponse.setHeader("Cache-Control", "private, max-age=604800, must-revalidate"); // HTTP
            // 1.1
            httpResponse.setHeader("Pragma", "private"); // HTTP 1.0
            httpResponse.setDateHeader("Expires", new Date().getTime() + 604800000); // Proxies.
            //return res;
            return null;
        } catch (Exception exp) {
            // Expiration header cache cleared
            httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
            httpResponse.setDateHeader("Expires", 0); // Proxies.
            httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
            // 1.1
            throw exp;
        }

    }

    //api called by exotel
    @Deprecated
    //TODO concern: anybody can call it
    @RequestMapping(value = "/getTeacherContactFromExtension", method = RequestMethod.GET)
    public String getTeacherContactFromExtension(ExtensionNumberReq req) throws Exception {
        req.verify();
        //return userManager.getTeacherContactFromExtension(req);
        return null;
    }

    @Deprecated
    @RequestMapping(value = "/sendUserRelatedEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendUserRelatedEmail(@RequestBody SendUserEmailReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.TRUE);
        return userManager.sendUserRelatedEmail(req);
    }

    @RequestMapping(value = "/getTeacherInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<EsTeacherInfo> getTeacherInfos(@RequestParam(value = "userIdsList") String userIdsList)
            throws Exception {
        return userManager.getTeacherInfos(userIdsList);
    }

    // Only for ADMIN use
    @Deprecated
    @RequestMapping(value = "/getUserInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<UserBasicInfo> getUserInfos(@RequestParam(value = "userIdsList") String userIdsList) throws Exception {
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return userManager.getUserInfos(userIdsList, true);
    }

    @RequestMapping(value = "/showInterest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse showInterest(@Valid @RequestBody ShowInterestReq req) throws
            VException, NotFoundException, Exception {
        return userManager.showInterest(req);
    }

    @RequestMapping(value = "/showInterestForCourseListing", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse showInterestFromCourseListing(@Valid @RequestBody ShowInterestCourseListingReq req) throws
            VException, NotFoundException, Exception {
        return userManager.showInterestFromCourseListing(req);
    }

    @RequestMapping(value = "/indexUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse indexUser(@RequestParam(value = "userId") Long userId,
            @RequestParam(value = "key") String key) throws
            VException, NotFoundException, Exception {
        return userManager.indexUser(userId, key);
    }

    @RequestMapping(value = "/createUserDetailsForExistingUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo createUserDetailsForExistingUser(@Valid @RequestBody ExistingUserISLRegReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getUserId(), null, true);
        return userManager.createUserDetailsForExistingUser(req);
    }

    @RequestMapping(value = "/getUserDetailsForISLInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo getUserDetailsForISLInfo(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        return userManager.getUserDetailsForISLInfo(httpSessionData.getUserId(), event);
    }

    @Deprecated
    @RequestMapping(value = "/isRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IsRegisteredForISLRes isRegisteredForISL(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        return userManager.isRegisteredForISL(httpSessionData.getUserId(), event);
    }

    @Deprecated
    @RequestMapping(value = "/isRegisteredForVolt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public IsRegisteredForISLRes isRegisteredForVolt(@RequestParam(value = "event", required = false) String event) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        return userManager.isRegisteredForVolt(httpSessionData.getUserId(), event);
    }

    @RequestMapping(value = "/signUpForISL", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SignUpForISLRes signUpForISL(@Valid @RequestBody SignUpForISLReq req, HttpServletRequest request, HttpServletResponse response)
            throws VException, IOException, URISyntaxException {
        return userManager.signUpForISL(req, request, response);
    }

    @Deprecated
    @RequestMapping(value = "/getUsersRegisteredForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getUsersRegisteredForISL(GetUsersReq req) throws VException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        roles.add(Role.STUDENT_CARE);
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), roles, Boolean.TRUE);
        return userManager.getUsersRegisteredForISL(req);
    }

    @RequestMapping(value = "/editUserDetailsForEvent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDetailsInfo editUserDetailsForEvent(@Valid @RequestBody EditUserDetailsReq req)
            throws VException {
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.STUDENT), Boolean.TRUE);
        return userManager.editUserDetailsForEvent(req);
    }

    @Deprecated
    @RequestMapping(value = "/generatePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse generatePassword(@RequestBody GeneratePasswordReq req) throws VException, IOException {
        return userManager.generatePassword(req);
    }

    @Deprecated
    @RequestMapping(value = "/getUserISLStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getUserISLStatus(@RequestParam(value = "email") String email,
            @RequestParam(value = "event", required = false) String event) throws VException, UnsupportedEncodingException {
        return userManager.getUserISLStatus(email, event);
    }

    @RequestMapping(value = "/getUserInfosByEmail", method = RequestMethod.GET)
    @ResponseBody
    public List<UserInfo> getUserInfosByEmail(GetUserInfosByEmailReq req) throws VException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        roles.add(Role.STUDENT_CARE);
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), roles, Boolean.TRUE);
        return userManager.getUserInfosByEmail(req);
    }

    @RequestMapping(value = "/getLoginData", method = RequestMethod.GET)
    @ResponseBody
    public HttpSessionData getLoginData() {
        return sessionUtils.getCurrentSessionData();
    }

    @RequestMapping(value = "/update/lead", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateToLeadSquared(@RequestBody User user) {
        userManager.updateLead(user);
        return new PlatformBasicResponse();
    }


    /**
     * @Intent For Sales puropose. Send VSAT result email before actual result declaration, To be used by Counsellor role only.
     * @return
     */
    @RequestMapping(value = "/uploadResultPdf", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse vsatSalesSendResultEmail(
            @RequestParam(name = "file") MultipartFile multipart,
            @RequestParam(name = "id") Long userId,
            @RequestParam(name = "category") String category)
            throws IOException, MessagingException, JSONException, VException, InterruptedException {

        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        File file = CommonUtils.multipartToFile(multipart);
        logger.info("After conversion from multipart to file the size of the file is " + file.length());
        userManager.uploadUserResultS3(file, userId, category);
        return new PlatformBasicResponse(true, "File Uploaded", "");
    }

    /**
     * @Intent For Sales puropose. Send VSAT result email before actual result declaration, To be used by Counsellor role only.
     * @return
     */
    @RequestMapping(value = "/sendResultEmail", method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse vsatSalesSendResultEmail(
            @RequestParam(name = "userId") Long userId,
            @RequestParam(name = "score") Integer score,
            @RequestParam(name = "rank") Integer rank,
            @RequestParam(name = "duration") Long duration,
            @RequestParam(name = "category") String category)
            throws IOException, MessagingException, JSONException, VException, InterruptedException {

        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        userManager.vsatSalesSendResultEmail(userId ,score, rank, duration, category);
        return new PlatformBasicResponse(true, "Email Sent", "");
    }
}
