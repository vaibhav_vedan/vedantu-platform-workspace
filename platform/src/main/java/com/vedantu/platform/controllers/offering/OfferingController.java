package com.vedantu.platform.controllers.offering;

import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.offering.request.AddStudyPlanRequest;
import com.vedantu.offering.request.CreateOfferingRequest;
import com.vedantu.offering.request.EditOfferingRequest;
import com.vedantu.offering.request.EditStudyPlanInfoRequest;
import com.vedantu.offering.request.GetOfferingRequest;
import com.vedantu.offering.request.GetOfferingsRequest;
import com.vedantu.offering.request.GetStudyPlansRequest;
import com.vedantu.offering.response.CreateOfferingResponse;
import com.vedantu.offering.response.EditOfferingResponse;
import com.vedantu.offering.response.GetOfferingResponse;
import com.vedantu.offering.response.GetOfferingsResponse;
import com.vedantu.offering.response.GetStudyPlansResponse;
import com.vedantu.offering.response.StudyPlanInfo;
import com.vedantu.platform.managers.offering.OfferingManager;
import com.vedantu.platform.managers.offering.StudyPlanManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.OfferingType;

@RestController
@RequestMapping("/offering")
public class OfferingController {

	@Autowired
	LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(OfferingController.class);

	@Autowired
	OfferingManager offeringManager;
	
	@Autowired
	HttpSessionUtils sessionUtils;
        
        @Autowired
        private StudyPlanManager studyPlanManager;

	@ApiOperation(value = "Create Offering", notes = "Returns created Offering")
	@RequestMapping(value = "/createOffering", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public CreateOfferingResponse createOffering(
			@RequestBody CreateOfferingRequest createOfferingReq)
			throws VException {

		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		createOfferingReq.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId());
		createOfferingReq.verify();
		CreateOfferingResponse createOfferingResponse = offeringManager
				.createOffering(createOfferingReq);
		return createOfferingResponse;
	}

	@ApiOperation(value = "edit Offering", notes = "Returns edited offering")
	@RequestMapping(value = "/editOffering", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public EditOfferingResponse editOffering(
			@RequestBody EditOfferingRequest editOfferingRequest)
			throws Throwable {
		try {
			sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
			editOfferingRequest.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId());
			editOfferingRequest.verify();
			EditOfferingResponse resPojo = offeringManager
					.editOffering(editOfferingRequest);
			return resPojo;
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "get Offering", notes = "Returns offering")
	@RequestMapping(value = "/getOffering", method = RequestMethod.GET)
	@ResponseBody
	public GetOfferingResponse getOffering(
			@RequestParam(value = "offeringId", required = false) Long offeringId,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "callingUserId", required = false) Long callingUserId)
			throws Throwable {

		GetOfferingRequest getOfferingRequest = new GetOfferingRequest(
				offeringId, userId, callingUserId);

		try {
			getOfferingRequest.verify();
			GetOfferingResponse resPojo = offeringManager
					.getOffering(getOfferingRequest);
			return resPojo;
		} catch (Exception e) {
			throw e;
		}

	}

	@ApiOperation(value = "get Offerings", notes = "Returns offerings")
	@RequestMapping(value = "/getOfferings", method = RequestMethod.GET)
	@ResponseBody
	public GetOfferingsResponse getOfferings(GetOfferingsRequest reqPojo)
			throws Throwable {
		try {
			reqPojo.verify();
			GetOfferingsResponse resPojo = offeringManager
					.getOfferings(reqPojo);
			return resPojo;
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "get OfferingTeacherIds", notes = "Returns teacher ids")
	@RequestMapping(value = "/getOfferingTeacherIds", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<Long> getOfferingTeacherIds(
			@RequestParam(value = "offeringId", required = true) Long offeringId,
			@RequestParam(value = "callingUserId", required = true) Long callingUserId)
			throws Throwable {
		try {
			logger.info("getOfferingTeacherIds request :  OfferingId: "
					+ offeringId + ", callingUserId: " + callingUserId);
			List<Long> teacherIds = offeringManager
					.getOfferingTeacherIds(offeringId);
			logger.info("getOfferingTeacherIds response :"
					+ teacherIds.toString());
			return teacherIds;
		} catch (Exception e) {
			throw e;
		}

	}
        
	@RequestMapping(value = "/getStudyPlans", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public GetStudyPlansResponse getStudyPlans(GetStudyPlansRequest getStudyPlansRequest)
			throws Throwable {
                getStudyPlansRequest.verify();
		return studyPlanManager.getStudyPlans(getStudyPlansRequest);
	}        
        
	@RequestMapping(value = "/getStudyPlansByUserId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public GetStudyPlansResponse getStudyPlansByUserId(GetStudyPlansRequest getStudyPlansRequest)
			throws Throwable {
                getStudyPlansRequest.verify();
		return studyPlanManager.getStudyPlansByUserId(getStudyPlansRequest);
	}                
        
	@RequestMapping(value = "/addStudyPlan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public StudyPlanInfo addStudyPlan(@RequestBody AddStudyPlanRequest addStudyPlanReq)
			throws Throwable {
                addStudyPlanReq.verify();
		return studyPlanManager.addStudyPlan(addStudyPlanReq);
	}  
        
	@RequestMapping(value = "/editStudyPlan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public StudyPlanInfo editStudyPlan(@RequestBody EditStudyPlanInfoRequest editStudyPlanInfoRequest)
			throws Throwable {
                editStudyPlanInfoRequest.verify();
		return studyPlanManager.editStudyPlan(editStudyPlanInfoRequest);
	}     
        
	@RequestMapping(value = "/deleteStudyPlan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public StudyPlanInfo deleteStudyPlan(@RequestBody EditStudyPlanInfoRequest deleteStudyPlanInfoRequest)
			throws Throwable {
                deleteStudyPlanInfoRequest.verify();
		return studyPlanManager.deleteStudyPlan(deleteStudyPlanInfoRequest);
	}             
}
