package com.vedantu.platform.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.seo.enums.SeoDomain;
import com.vedantu.platform.seo.response.CategoryPageResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.text.ParseException;

@RestController
@Api(value = "SeoManager")
public class PageBuilderController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PageBuilderController.class);

    private static Gson gson = new Gson();

    private final static String growthCmsWebinarBasePath = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT");

    @RequestMapping(value = "/sitemaps/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ApiOperation(value = "Get all sitemaps", notes = "Sitemaps XML")
    public String getSitemaps(@PathVariable(value = "name") String name,
                              @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws MalformedURLException, ParseException, VException {
        logger.info("SEO-SITEMAPS: " + name);
        String url = growthCmsWebinarBasePath + "/sitemaps/" + name + "?domain=" + domain ;

        ClientResponse responseObj = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, "");
        VExceptionFactory.INSTANCE.parseAndThrowException(responseObj);
        String response = responseObj.getEntity(String.class);

        return response;
    }

    @RequestMapping(value = "/seo/category-page", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get categoryPages by categoryId", notes = "CategoryPages (categoryPages entries that are added/updated.)")
    @ResponseBody
    public CategoryPageResponse getCategoryPages(
            @RequestParam(value = "categoryId", required = false) String categoryId,
            @RequestParam(value = "categoryPageUrl", required = false) String categoryPageUrl,
            @RequestParam(value = "categoryPageType", required = false) String categoryPageType,
            @RequestParam(value = "categoryTarget", required = false) String categoryTarget,
            @RequestParam(value = "categoryGrade", required = false) String categoryGrade,
            @RequestParam(value = "disabled", required = false) Boolean disabled,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws Exception {

        String url = growthCmsWebinarBasePath + "/seo/category-page"
                + "?categoryId=" + categoryId
                + "&categoryPageUrl=" + categoryPageUrl
                + "&categoryPageType=" + categoryPageType
                + "&categoryTarget=" + categoryTarget
                + "&categoryGrade=" + categoryGrade
                + "&disabled=" + disabled
                + "&start=" + start
                + "&size=" + size
                + "&domain=" + domain;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, "");
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, CategoryPageResponse.class);

    }

    @RequestMapping(value = "/seo/category-page/get-id-from-url", method = RequestMethod.GET, produces = "text/plain")
    @ApiOperation(value = "Get categoryPage URL by Id", notes = "CategoryPage.url (categoryPage URL entry that are added/updated.)")
    public String getCategoryPageIdByUrl(@RequestParam(value = "categoryPageUrl") String categoryPageUrl,
                                         @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain)
            throws Exception {
        String url = growthCmsWebinarBasePath + "/seo/category-page/get-id-from-url?domain=" + domain
                + "&categoryPageUrl=" + categoryPageUrl;

        ClientResponse responseObj = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, "");
        VExceptionFactory.INSTANCE.parseAndThrowException(responseObj);
        String response = responseObj.getEntity(String.class);

        return response;
    }


}
