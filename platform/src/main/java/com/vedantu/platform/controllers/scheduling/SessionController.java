package com.vedantu.platform.controllers.scheduling;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import com.vedantu.scheduling.request.session.*;
import com.vedantu.scheduling.response.session.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.request.ConnectSessionCallReq;
import com.vedantu.User.response.ConnectSessionCallRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.SessionHourCountManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.pojo.scheduling.SendTrialReminderReq;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.dao.wave.WiziqBillingDataReq;
import com.vedantu.platform.managers.wave.BillingManager;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.session.GetSessionRescheduleRes;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;

import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;

import org.json.JSONObject;

@RestController
@RequestMapping("/session")
// @CrossOrigin
public class SessionController {

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private SessionHourCountManager sessionHourCountManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private BillingManager billingManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionController.class);

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Create session from request", notes = "Session (Created entity)")
    public SessionInfo createSession(@RequestBody SessionScheduleReq req) throws VException, Exception {
        return sessionManager.createSession(req);
    }

    @RequestMapping(value = "/createDemoSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Create demo session from request", notes = "Session (Created entity)")
    public CreateDemoSessionRes createDemoSession(@RequestBody CreateDemoSessionReq sessionScheduleReq) throws Exception {
        httpSessionUtils.checkIfAllowed(null, null, true);
        return sessionManager.createDemoSession(sessionScheduleReq);
    }

    @RequestMapping(value = "/reschedule", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Reschedule session from request", notes = "Session (Updated entity)")
    public PlatformBasicResponse rescheduleSession(@RequestBody RescheduleSessionReq rescheduleSessionReq) throws Exception {
        return sessionManager.rescheduleSession(rescheduleSessionReq);
    }

    @RequestMapping(value = "/join", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Join session for a user", notes = "Session (Updated entity)")
    public SessionInfo joinSession(@RequestBody JoinSessionReq joinSessionReq) throws VException {
        httpSessionUtils.checkIfAllowed(joinSessionReq.getUserId(), null, false);
        return sessionManager.joinSession(joinSessionReq);
    }

    //TODO concern: check if the app credentials and the app is allowed to access this api
    @RequestMapping(value = "/joinFromNode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Join session for a user from node server", notes = "Session (Updated entity)")
    public SessionInfo joinSessionFromNode(@RequestBody JoinSessionReq joinSessionReq) throws VException {
        return sessionManager.joinSessionFromNode(joinSessionReq);
    }

    @RequestMapping(value = "/end", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "End session for a user", notes = "Session (Updated entity)")
    public PlatformBasicResponse endSession(@Valid @RequestBody EndSessionReq endSessionReq) throws Exception {
        return sessionManager.endSession(endSessionReq, true);
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Cancel session for a user. Cancel and end session are same from front end", notes = "Session (Updated entity)")
    public SessionInfo cancelSession(@Valid @RequestBody EndSessionReq endSessionReq) throws Exception {
        return sessionManager.cancelSession(endSessionReq);
    }

    //TODO concern: check if the app credentials and the app is allowed to access this api
    @RequestMapping(value = "/markActive", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Mark the session as active", notes = "Session (Updated entity)")
    public SessionInfo markSessionActive(@RequestBody MarkSessionActiveReq markSessionActiveReq) throws VException {
        return sessionManager.markSessionActive(markSessionActiveReq);
    }

    @RequestMapping(value = "/setVimeoVideoId/{id}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SessionInfo setVimeoVideoId(@PathVariable("id") Long id, @RequestBody SetVimeoVideoIdRequest setVimeoVideoIdRequest) throws VException {
        return sessionManager.setVimeoVideoId(id, setVimeoVideoIdRequest);
    }

    //TODO concern: anybody can call this api, this api is exposing email
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Get Session by id", notes = "Session info")
    public SessionInfo getSessionById(@PathVariable("id") Long id,
                                      @RequestParam(value = "includeAttendeeRatings", required = false) boolean includeAttendeeRatings)
            throws VException {
        return sessionManager.getSessionById(id, includeAttendeeRatings);
    }

    @RequestMapping(value = "/rescheduleinfo/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Get Session by id", notes = "Session info")
    public GetSessionRescheduleRes getSessionRescheduleDetails(@PathVariable("id") Long id) throws VException {
        logger.info("Request : " + id);
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
        httpSessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return sessionManager.getSessionRescheduleDetails(id);
    }

    //TODO concern: anybody can call this api
    // migrated to /getSessionsInfo in scheduling
    @Deprecated
    @RequestMapping(value = "/getSessions", method = RequestMethod.GET)
    @ApiOperation(value = "Returns sessions based on parameters", notes = "Allowed only for ADMIN")
    public SessionInfoRes getSessions(GetSessionsReq getSessionsReq)
            throws VException, IllegalArgumentException, IllegalAccessException {

        // TODO : validate ADMIN
        List<SessionInfo> sessionInfos = sessionManager.getSessions(getSessionsReq,
                httpSessionUtils.isAdminOrStudentCare(httpSessionUtils.getCurrentSessionData()));
        return new SessionInfoRes(sessionInfos);
    }

    @RequestMapping(value = "/getBillingDuration", method = RequestMethod.GET)
    @ApiOperation(value = "Returns sessions based on parameters", notes = "Allowed only for ADMIN")
    public GetSessionBillingDurationRes getSessionBillingDuration(@RequestParam(value = "sessionId", required = true) Long sessionId)
            throws VException, IllegalArgumentException, IllegalAccessException {
        // TODO : validate ADMIN
        return sessionManager.getSessionBillingDuration(sessionId);
    }

    // migrated to /getUserUpcomingSessionsInfo in scheduling
    @Deprecated
    @RequestMapping(value = "/getUserUpcomingSessions", method = RequestMethod.GET)
    @ApiOperation(value = "Get User upcoming Sessions", notes = "Used for ticker and session listing")
    public SessionInfoRes getUserUpcomingSessions(GetUserUpcomingSessionReq getUserUpcomingSessionReq)
            throws Exception {
        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        getUserUpcomingSessionReq.verify();
        List<SessionInfo> sessionInfos = sessionManager.getUserUpcomingSessions(getUserUpcomingSessionReq, exposeEmail);
        return new SessionInfoRes(sessionInfos);
    }

    @RequestMapping(value = "/getUserScheduledUpcomingSessions", method = RequestMethod.GET)
    @ApiOperation(value = "Get User scheduled upcoming Sessions", notes = "Only active and scheduled")
    public SessionInfoRes getUserScheduledUpcomingSessions(GetUserUpcomingSessionReq getUserUpcomingSessionReq)
            throws Exception {

        // Pre fill the session states
        List<SessionState> sessionStates = new ArrayList<>();
        sessionStates.add(SessionState.ACTIVE);
        sessionStates.add(SessionState.SCHEDULED);
        sessionStates.add(SessionState.STARTED);
        getUserUpcomingSessionReq.setSessionStates(sessionStates);

        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        List<SessionInfo> sessionInfos = sessionManager.getUserUpcomingSessions(getUserUpcomingSessionReq, exposeEmail);
        return new SessionInfoRes(sessionInfos);
    }

    // migrated to /getUserPastSessionsInfo in scheduling
    @Deprecated
    @RequestMapping(value = "/getUserPastSessions", method = RequestMethod.GET)
    @ApiOperation(value = "Get User past Sessions", notes = "Used in session listing")
    public SessionInfoRes getUserPastSessions(GetUserPastSessionReq getUserPastSessionReq) throws Exception {
        httpSessionUtils.checkIfAllowed(getUserPastSessionReq.getUserId(), null, Boolean.TRUE);

        if (getUserPastSessionReq.getUserId() == null) {
            HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
            if (sessionData != null) {
                getUserPastSessionReq.setUserId(sessionData.getUserId());
                getUserPastSessionReq.setCallingUserRole(sessionData.getRole());
            }
        }
        boolean exposeEmail = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        List<SessionInfo> sessionInfos = sessionManager.getUserPastSessions(getUserPastSessionReq, exposeEmail);
        return new SessionInfoRes(sessionInfos);
    }

    // migrated to api /getUserLatestActiveSessionFromCache in scheduling
    @Deprecated
    @RequestMapping(value = "/getUserLatestActiveSession", method = RequestMethod.GET)
    @ApiOperation(value = "Get User latest Sessions", notes = "Used in session listing")
    public GetLatestUpComingOrOnGoingSessionRes getUserLatestSession(
            GetUserUpcomingSessionReq getUserUpcomingSessionReq) throws Exception {
        return sessionManager.getUserLatestSessionsRedis(getUserUpcomingSessionReq);
    }

    @RequestMapping(value = "/getUserCalendarSessions", method = RequestMethod.GET)
    @ApiOperation(value = "Get User calendar Sessions", notes = "Used in My calendar")
    public SessionInfoRes getUserCalendarSessions(@ModelAttribute GetUserCalendarSessionsReq getUserCalendarSessionsReq)
            throws Exception {
        List<SessionInfo> sessionInfos = sessionManager.getUserCalendarSessions(getUserCalendarSessionsReq);
        return new SessionInfoRes(sessionInfos);
    }

    @RequestMapping(value = "/getSessionBillDetails", method = RequestMethod.GET)
    @ApiOperation(value = "Get Session bill details of a user", notes = "Used in session listing")
    public GetSessionBillDetailsRes getSessionBillDetails(GetSessionBillDetailsReq request) throws Exception {
        // Check the request user id
        if (!Role.ADMIN.equals(request.getCallingUserRole())) {
            request.setUserId(request.getCallingUserId());
        }
        return sessionManager.getSessionBillDetails(request);
    }

    @RequestMapping(value = "/connectSessionCall", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConnectSessionCallRes connectSessionCall(@RequestBody ConnectSessionCallReq req) throws VException {
        req.verify();
        return sessionManager.connectSessionCall(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/sendTrialReminder", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendTrialReminder(HttpServletRequest req) throws VException {
        SendTrialReminderReq request = new SendTrialReminderReq(req);
        request.verify();
        return sessionManager.sendTrialReminder(request);
    }

    @RequestMapping(value = "/getSessionPartners", method = RequestMethod.GET)
    @ResponseBody
    public GetSessionPartnersRes getSessionPartners(GetSessionPartnersReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        req.verify();
        HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
        if (httpSessionData != null) {
            req.setCallingUserRole(httpSessionData.getRole());
        }
        return sessionManager.getSessionPartners(req);
    }

    @RequestMapping(value = "/getSessionCounts", method = RequestMethod.GET)
    @ResponseBody
    public GetScheduledAndAllSessionCountRes getSessionCounts(
            @RequestParam(value = "userId", required = true) Long userId) throws VException {
        return sessionManager.getSessionCounts(userId);
    }

    @RequestMapping(value = "/sessionEventsOperation", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sessionEventsOperation(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                       @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SessionEvents eventType = SessionEvents.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            if (eventType != null) {
                sessionManager.sessionEventsOperation(eventType, message);
            }
        }
    }

    @RequestMapping(value = "/markSessionExpired", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void markSessionExpired(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                   @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
            sessionManager.handleSessionEndingAsync();
        }
    }

    @RequestMapping(value = "/updateTotalSessionDuration", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateTotalSessionDuration(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                           @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
            sessionHourCountManager.getTotalSessionDuration(true);
        }
    }

    @RequestMapping(value = "/launchOTOSessionRecorders", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void launchOTOSessionRecorders(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                          @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
            sessionManager.launchOTOSessionRecordersAsync();
        }
    }

    @RequestMapping(value = "/getTotalSessionDuration", method = RequestMethod.GET)
    @ResponseBody
    public long getTotalSessionDuration(@RequestParam(name = "reload", required = false) Boolean reload)
            throws VException, IOException, IllegalArgumentException, IllegalAccessException {
        return sessionHourCountManager.getTotalSessionDuration(Boolean.TRUE.equals(reload));
    }

    @Deprecated
    @RequestMapping(value = "/updateLiveSessionPlatformDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateLiveSessionPlatformDetails(@RequestBody UpdateLiveSessionPlatformDetailsReq req)
            throws VException {
        if (!Role.ADMIN.equals(req.getCallingUserRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "non admins cannot chage session platform type");
        }
        return sessionManager.updateLiveSessionPlatformDetails(req);
    }

    @RequestMapping(value = "/joinOTONoNDefaultSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public JoinOTONoNDefaultSessionRes joinOTONoNDefaultSession(@RequestBody JoinSessionReq req)
            throws VException, IOException {
        httpSessionUtils.checkIfAllowed(req.getCallingUserId(), null, Boolean.FALSE);
        return sessionManager.joinOTONoNDefaultSession(req);
    }

    @RequestMapping(value = "/adminSessionUpdate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse adminSessionUpdate(@Valid @RequestBody AdminSessionUpdateReq req)
            throws VException, IOException, AddressException {
        return sessionManager.adminSessionUpdate(req);
    }

    @RequestMapping(value = "/launchOTOSessionRecorder", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse launchOTOSessionRecorder(@RequestParam(name = "sessionId", required = true) String sessionId, @RequestParam(name = "timeout", required = true) Long timeout)
            throws VException, IOException, AddressException {
        return sessionManager.launchOTOSessionRecorder(sessionId, timeout);
    }

    @RequestMapping(value = "/isTOS", method = RequestMethod.GET)
    @ResponseBody
    public CheckSessionTOSRes isTOS(CheckSessionTOSReq req)
            throws VException, IOException, IllegalArgumentException, IllegalAccessException {
        return sessionManager.isTOS(req);
    }

    @RequestMapping(value = "/getAttendeesNextSessions", method = RequestMethod.GET)
    @ResponseBody
    public SessionInfoRes getAttendeesNextSessions(@RequestParam(name = "sessionId", required = true) String sessionId)
            throws VException {
        httpSessionUtils.checkIfUserLoggedIn();
        Long userId = httpSessionUtils.getCurrentSessionData().getUserId();
        List<SessionInfo> sessionInfos = sessionManager.getAttendeesNextSessions(sessionId, userId);
        return new SessionInfoRes(sessionInfos);
    }

    // moved to scheduling
    @Deprecated
    @RequestMapping(value = "/handleSessionEventsForLatestSession", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleSessionEventsForLatestSession(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                    @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SessionEvents eventType = SessionEvents.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            if (eventType != null) {
                sessionManager.handleSessionEventsForLatestSession(eventType, message);
            }
        }
    }

    @RequestMapping(value = "/endWiziqSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endWiziqSession(@RequestBody WiziqBillingDataReq req)
            throws VException, IOException, AddressException {
        billingManager.insertBillingDataForWiziqSessions(req);
        EndSessionReq endSessionReq = new EndSessionReq(req.getSessionId(), null);
        sessionManager.endSession(endSessionReq, false);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/createTestLinkWithLoginToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse createTestLinkWithLoginToken(@RequestBody CreateTestLinkWithLoginTokenReq request) throws VException, IOException, URISyntaxException {
        request.verify();
        return sessionManager.createTestLinkWithLoginToken(request);
    }
    
    //only used for migration purposes
    @RequestMapping(value = "/triggerPayoutQueueEvent/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse triggerPayoutQueueEvent(@PathVariable("id") Long id) throws VException, IOException, URISyntaxException {
        return sessionManager.triggerPayoutQueueEvent(id);
    }

    @RequestMapping(value = "/getSessionShortUrl", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getSessionShortUrl(@RequestParam(name = "sessionUrl", required = true) String sessionUrl,
                                     @RequestParam(name = "userId", required = true) Long userId)
            throws VException, NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        httpSessionUtils.checkIfAllowed(userId, null, false);
        return sessionManager.getSessionShortUrl(sessionUrl);
    }
}
