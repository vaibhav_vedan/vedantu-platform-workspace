package com.vedantu.platform.controllers.review;

import io.swagger.annotations.ApiOperation;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.review.FeedbackManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.review.requests.GetFeedbackRequest;
import com.vedantu.review.requests.GiveFeedbackRequest;
import com.vedantu.review.requests.UpdateFeedbackRequest;
import com.vedantu.review.response.GetFeedbackResponse;
import com.vedantu.review.response.GiveFeedbackResponse;
import com.vedantu.review.response.UpdateFeedbackResponse;
import com.vedantu.util.LogFactory;
import javax.validation.Valid;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(FeedbackController.class);

    @Autowired
    FeedbackManager feedbackManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @ApiOperation(value = "add Feedback", notes = "Returns created feedback")
    @RequestMapping(value = "/giveFeedback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public GiveFeedbackResponse giveFeedback(
           @Valid @RequestBody GiveFeedbackRequest giveFeedbackRequest)
            throws Exception {
        giveFeedbackRequest.verify();
        sessionUtils.checkIfAllowed(Long.parseLong(giveFeedbackRequest.getSenderId()), null, false);
        giveFeedbackRequest.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId());
        if (giveFeedbackRequest.getRating() < 1
                || giveFeedbackRequest.getRating() > 5) {
            throw new BadRequestException(ErrorCode.INVALID_RATING,
                    "Invalid rating provided");
        }
        GiveFeedbackResponse giveFeedbackResponse = feedbackManager
                .giveFeedback(giveFeedbackRequest);

        return giveFeedbackResponse;
    }

    @ApiOperation(value = "fetch Feedback", notes = "Returns feedback")
    @RequestMapping(value = "/fetchFeedback", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public GetFeedbackResponse fetchFeedback(GetFeedbackRequest getFeedbackRequest)
            throws Exception {
        getFeedbackRequest.verify();
        GetFeedbackResponse getFeedbackResponse = feedbackManager.fetchFeedback(getFeedbackRequest);
        return getFeedbackResponse;
    }

    @ApiOperation(value = "update Feedback", notes = "Returns updated feedback")
    @RequestMapping(value = "/updateFeedback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public UpdateFeedbackResponse updateFeedback(
            @Valid @RequestBody UpdateFeedbackRequest updateFeedbackRequest)
            throws Exception {
        //TODO for non admin user, allow if he has not added feedback already, if updating
        //an existing feedback, do not allow him
        updateFeedbackRequest.verify();
        sessionUtils.checkIfAllowed(updateFeedbackRequest.getCallingUserId(), null, true);
        updateFeedbackRequest.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId());
        UpdateFeedbackResponse updateFeedbackResponse = feedbackManager
                .updateFeedback(updateFeedbackRequest);
        return updateFeedbackResponse;
    }

    @ApiOperation(value = "giveTechRating", notes = "Returns feedback response")
    @RequestMapping(value = {"/giveTechRating",
        "/giveTechRatingAndStudentReport"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public GiveFeedbackResponse giveTechRating(
            @Valid @RequestBody GiveFeedbackRequest giveFeedbackRequest)
            throws Exception {

        logger.info("giveTechRatingReq :" + giveFeedbackRequest.toString());

        giveFeedbackRequest.verify();
        sessionUtils.checkIfAllowed(Long.parseLong(giveFeedbackRequest.getSenderId()), null, false);
        giveFeedbackRequest.setCallingUserId(sessionUtils.getCurrentSessionData().getUserId());

        if (giveFeedbackRequest.getRating() < 1
                || giveFeedbackRequest.getRating() > 5) {
            throw new VException(ErrorCode.INVALID_RATING,
                    "Invalid rating provided");
        }
        GiveFeedbackResponse giveFeedbackResponse = feedbackManager
                .giveTechRating(giveFeedbackRequest);
        logger.info("give Tech rating response: "
                + giveFeedbackResponse.toString());
        return giveFeedbackResponse;
    }

}
