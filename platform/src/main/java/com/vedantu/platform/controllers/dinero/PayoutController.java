package com.vedantu.platform.controllers.dinero;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletResponse;

import com.vedantu.exception.*;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.platform.enums.DurationConflictReason;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.response.dinero.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.enums.SessionModel;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.managers.dinero.PayoutManager;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.pojo.dinero.Transaction;
import com.vedantu.platform.pojo.dinero.TransactionWithDate;
import com.vedantu.platform.request.dinero.UpdateSessionPayoutRequest;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.platform.pojo.subscription.UpdateConflictDurationRequest;
import com.vedantu.platform.managers.wave.BillingManager;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@RestController
@RequestMapping("/dinero/payout")
public class PayoutController {

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PayoutController.class);

	@Autowired
	public BillingManager billingManager;

	@Autowired
	private PayoutManager payoutManager;

	@Autowired
	private SessionManager sessionManager;
        
        @Autowired
	private SubscriptionManager subscriptionManager;

        @Autowired
		private CoursePlanManager coursePlanManager;
        
	//@CrossOrigin
	@RequestMapping(value = "/getTotalTeacherPayout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetTotalPayoutResponse getTotalTeacherPayout(
			@RequestParam(value = "teacherId", required = true) Long teacherId) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		GetTotalPayoutResponse response = new GetTotalPayoutResponse();
		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE
				.doCall(dineroEndpoint + "/payout/getTotalTeacherPayout?teacherId=" + teacherId, HttpMethod.GET, null);

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		response = new Gson().fromJson(jsonString, GetTotalPayoutResponse.class);

		return response;

	}

	//@CrossOrigin
	@RequestMapping(value = "/getMonthlyTeacherPayout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetMonthlyPayoutResponse getMonthlyTeacherPayout(
			@RequestParam(value = "teacherId", required = true) Long teacherId,
			@RequestParam(value = "month", required = true) int month) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		GetMonthlyPayoutResponse response = new GetMonthlyPayoutResponse();
		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(
				dineroEndpoint + "/payout/getMonthlyTeacherPayout?teacherId=" + teacherId + "&month=" + month,
				HttpMethod.GET, null);

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		response = new Gson().fromJson(jsonString, GetMonthlyPayoutResponse.class);

		return response;

	}

	//@CrossOrigin
	@RequestMapping(value = "/exportTransactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void exportDailyTransactions(HttpServletResponse response,
			@RequestParam(value = "startDate", required = false) Long startDate,
			@RequestParam(value = "endDate", required = false) Long endDate) throws IOException, VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		String serverUrl;
		if (startDate != null && endDate != null) {
			serverUrl = dineroEndpoint + "/payout/exportTransactions" + "?startDate=" + startDate + "&endDate="
					+ endDate;
		} else {
			serverUrl = dineroEndpoint + "/payout/exportTransactions";
		}
		ClientResponse resp = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		ExportDailyTransactionsResponse responseObject = new Gson().fromJson(jsonString,
				ExportDailyTransactionsResponse.class);

		List<Transaction> dineroTransactions = responseObject.getTransactions();

		List<TransactionWithDate> transactions = new ArrayList<TransactionWithDate>();
		TransactionWithDate tempTransaction;
		for (Transaction dineroTransaction : dineroTransactions) {
			tempTransaction = new TransactionWithDate(dineroTransaction);
			transactions.add(tempTransaction);
		}

		// String csvFileName = "DailyTransactions-" + new
		// SimpleDateFormat("dd/MM/yyyy").format(new
		// Date(System.currentTimeMillis())) + ".csv";
		String csvFileName = "Transactions.csv";

		response.setContentType("text/csv");
		String headerKey = "Content-Disposition";

		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		String[] header = { "Id", "CreationTime", "Amount", "DebitFromAccount", "CreditToAccount", "ReasonType",
				"ReasonRefNo", "ReasonRefType", "ReasonNote", "TriggredBy" };
		csvWriter.writeHeader(header);

		for (TransactionWithDate transaction : transactions) {
			csvWriter.write(transaction, header);
		}
		csvWriter.close();
	}

        @Deprecated
	@RequestMapping(value = "/exportDailyTransactions", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void exportDailyTransactions(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Throwable {
		logger.warn("Ignore this warning...");
		logger.info(request);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
			ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payout/exportTransactions",
					HttpMethod.GET, null);

			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String jsonString = resp.getEntity(String.class);

			ExportDailyTransactionsResponse responseObject = new Gson().fromJson(jsonString,
					ExportDailyTransactionsResponse.class);
			List<Transaction> dineroTransactions = responseObject.getTransactions();

			List<TransactionWithDate> transactions = new ArrayList<TransactionWithDate>();
			TransactionWithDate tempTransaction;
			for (Transaction dineroTransaction : dineroTransactions) {
				tempTransaction = new TransactionWithDate(dineroTransaction);
				transactions.add(tempTransaction);
			}
			StringWriter stringWriter = new StringWriter();
			String fileName = "DailyTransactions-"
					+ new SimpleDateFormat("dd-MM-yyyy").format(new Date(System.currentTimeMillis())) + ".csv";
			ICsvBeanWriter csvWriter = new CsvBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
			String[] header = { "Id", "CreationTime", "Amount", "DebitFromAccount", "CreditToAccount", "ReasonType",
					"ReasonRefNo", "ReasonRefType", "ReasonNote", "TriggredBy" };
			csvWriter.writeHeader(header);
			for (TransactionWithDate transaction : transactions) {
				csvWriter.write(transaction, header);
			}
			csvWriter.close();
			/*
			 * BufferedReader reader = new BufferedReader(new
			 * FileReader(fileName)); String line = null; StringBuilder
			 * stringBuilder = new StringBuilder(); String ls =
			 * System.getProperty("line.separator"); while ((line =
			 * reader.readLine()) != null) { stringBuilder.append(line);
			 * stringBuilder.append(ls); } // delete the last ls
			 * stringBuilder.deleteCharAt(stringBuilder.length() - 1);
			 */
			// Send email via notification centre
			logger.info("Sending email via notification-centre");
			ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
			String emailTos = ConfigUtils.INSTANCE.getStringValue("payout.exportDailyTransaction.toEmailIds");
			String[] emails = emailTos.split(",");
			for (String email : emails) {
				to.add(new InternetAddress(email));
			}
			String body = "Please find the attached Daily Transactions List";
			EmailAttachment attachment = new EmailAttachment(fileName, new Object(), "Payout");
			attachment.setApplication("application/csv");
			attachment.setAttachmentData(stringWriter.toString());
			CommunicationType type = CommunicationType.USER_MESSAGE;
			EmailRequest emailRequest = new EmailRequest();
			emailRequest.setTo(to);
			emailRequest.setBody(body);
			String env = "";
			if (!ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD"))
				env = ConfigUtils.INSTANCE.properties.getProperty("environment") + ": ";
			emailRequest.setSubject(env + "Payout Transactions Daily Report");
			emailRequest.setType(type);
			emailRequest.setAttachment(attachment);

			String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
			resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
					gson.toJson(emailRequest));
			logger.info(resp.getEntity(String.class));
		}

	}


	@RequestMapping(value = "/getTeacherSessionInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetTeacherSessionInfoResponse getTeacherSessionInfos(
			@RequestParam(value = "startDate", required = true) Long startDate,
			@RequestParam(value = "endDate", required = true) Long endDate) throws VException {

		GetTeacherSessionInfoResponse response = new GetTeacherSessionInfoResponse();
		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(
				dineroEndpoint + "/payout/getTeacherSessionInfos?startDate=" + startDate + "&endDate=" + endDate,
				HttpMethod.GET, null);

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		response = new Gson().fromJson(jsonString, GetTeacherSessionInfoResponse.class);

		return response;

	}
        
	@RequestMapping(value = "/calculateSessionPayout", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SessionPayoutResponse calculateSessionPayout(
			@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
			throws Throwable {
		logger.info(request);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification") && subscriptionRequest.getSubject().equals("SESSION_ENDED")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			SessionBillingDetails session = gson.fromJson(subscriptionRequest.getMessage(),
					SessionBillingDetails.class);
			logger.info(session.toString());
			if("wegfiuwegfigxbhsajv".equals(session.getSnsKey())) {
				return payoutManager.calculateSessionPayout(session);
			}
		}
		return new SessionPayoutResponse();
	}

	@RequestMapping(value = "/updateSessionPayout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UpdateSessionPayoutResponse updateSessionPayout(@RequestBody UpdateSessionPayoutRequest request)
			throws Throwable {
                request.verify();
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		String jsonString;
		SessionInfo sessionInfo = sessionManager.getSessionById(request.getSessionId());
		SessionBillingDetails session = new SessionBillingDetails(sessionInfo);
		request.setStudentId(session.getStudentId());
		request.setTeacherId(session.getTeacherId());
		request.setSubscriptionId(session.getSubscriptionId());
		request.setModel(session.getModel());

		Long subscriptionId = request.getSubscriptionId();
		UpdateSessionPayoutResponse updateSessionPayoutResponse = new UpdateSessionPayoutResponse();

		if (session.getSubscriptionId() == null && !EntityType.COURSE_PLAN.equals(session.getContextType())) {
			logger.warn("Null SubscriptionId received");
			return updateSessionPayoutResponse;
		}


		if(EntityType.COURSE_PLAN.equals(session.getContextType())) {

			payoutManager.processSessionPayout(Long.parseLong(session.getSessionId()),
					session.getTeacherId(), request.getNewDuration(),
					0,
					0, true);

			if(!SessionModel.TRIAL.equals(session.getModel()) && !DurationConflictReason.TECH_ISSUES.equals(request.getReason())) {
				UpdateConflictDurationRequest updateConflictDurationRequest = new UpdateConflictDurationRequest(
						null, session.getBillingDuration(),
						request.getNewDuration(), request.getSessionId());
				updateConflictDurationRequest.setContextType(session.getContextType());
				updateConflictDurationRequest.setContextId(session.getContextId());
				try {
					coursePlanManager.updateDurationConflict(updateConflictDurationRequest);
				} catch (Exception ex) {
					logger.info("Error in updateSurationConflict: ",ex);
					payoutManager.processSessionPayout(Long.parseLong(session.getSessionId()),
							session.getTeacherId(), session.getBillingDuration(),
							0,
							0, true);
					throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, ex.getMessage());
				}
			}

			return updateSessionPayoutResponse;
		}

		try {
			GetHourlyRateResponse getHourlyRateResponse;
			String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
			ClientResponse resp = WebUtils.INSTANCE.doCall(
					subscriptionEndpoint + "subscription/getHourlyRate/" + subscriptionId, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			jsonString = resp.getEntity(String.class);

			getHourlyRateResponse = new Gson().fromJson(jsonString, GetHourlyRateResponse.class);

			request.setHourlyRate(getHourlyRateResponse.getHourlyRate());
			if (getHourlyRateResponse.getIsActive().equals(false)) {
				request.setRefundToStudentWallet(true);
			} else
				request.setRemainingHours(getHourlyRateResponse.getRemainingHours());

			request.setTotalHours(getHourlyRateResponse.getTotalHours());
			request.setConsumedHours(getHourlyRateResponse.getConsumedHours());

			// Update Session Payout
			String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
			resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payout/updateSessionPayout", HttpMethod.POST,
					new Gson().toJson(request));

			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			jsonString = resp.getEntity(String.class);

			updateSessionPayoutResponse = new Gson().fromJson(jsonString, UpdateSessionPayoutResponse.class);

			payoutManager.processSessionPayout(updateSessionPayoutResponse.getSessionId(),
					updateSessionPayoutResponse.getTeacherId(), updateSessionPayoutResponse.getDuration(),
					updateSessionPayoutResponse.getTeacherPayout(),
					updateSessionPayoutResponse.getTeacherPayout() + updateSessionPayoutResponse.getCut(), true);

			// Update Subscription Details call
			if (updateSessionPayoutResponse.getIsSubscriptionUpdated()) {
				UpdateConflictDurationRequest updateConflictDurationRequest = new UpdateConflictDurationRequest(
						subscriptionId, updateSessionPayoutResponse.getPreviousDuration(),
						updateSessionPayoutResponse.getDuration(), request.getSessionId());
				resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/updateDurationConflict",
						HttpMethod.POST, new Gson().toJson(updateConflictDurationRequest));
				logger.info(resp);
				VExceptionFactory.INSTANCE.parseAndThrowException(resp);
				jsonString = resp.getEntity(String.class);
				logger.info(jsonString);
			}

			return updateSessionPayoutResponse;
		} catch (Throwable e) {
			logger.error(e.getMessage());
			throw e;
		}

	}
	
	//@CrossOrigin
	@RequestMapping(value = "/uploadTeacherPayout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse uploadTeacherPayout(@RequestParam("file") MultipartFile file)
			throws JSONException, UnsupportedEncodingException, VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		File uploadedFile = handleFileUpload(file);

		if (uploadedFile == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No file uploaded");
		}

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();

		map.add("file", new FileSystemResource(uploadedFile.getAbsolutePath()));

		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

		RestTemplate restTemplate = new RestTemplate();

		Boolean dineroResponse = restTemplate.postForObject(dineroEndpoint + "/payout/uploadTeacherPayout", map, Boolean.class);
		if(Boolean.TRUE.equals(dineroResponse)) {
			return new PlatformBasicResponse(true, null, null);
		} else {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in dinero");
		}

	}

	private File handleFileUpload(MultipartFile file) {
		if (!file.isEmpty()) {
			BufferedOutputStream stream = null;
			try {
				File uploadedFile = File.createTempFile(UUID.randomUUID().toString(), ".csv");
				byte[] bytes = file.getBytes();
				stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
				stream.write(bytes);
				return uploadedFile;
			} catch (Exception e) {
			} finally {
				IOUtils.closeQuietly(stream);
			}
		}
		return null;
	}
}
