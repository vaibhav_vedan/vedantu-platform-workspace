package com.vedantu.platform.controllers;


import java.util.List;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.platform.enums.mobile.MobilePlatform;
import com.vedantu.platform.enums.mobile.MobileRole;
import com.vedantu.platform.managers.MobileConfigManager;
import com.vedantu.platform.mongodbentities.mobile.MobileConfig;
import com.vedantu.platform.mongodbentities.mobile.MobileGlobalConfiguration;
import com.vedantu.platform.request.mobile.MobileConfigReq;
import com.vedantu.platform.request.mobile.UpdateGlobalMobileParamsReq;
import com.vedantu.platform.response.mobile.GlobalMobileConfigRes;
import com.vedantu.platform.response.mobile.MobileConfigRes;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;



@RestController
public class MobileConfigController {

	@Autowired
	private LogFactory logFactory;

	// check this
	@Autowired
	private MobileConfigManager mobileConfigManager;
        
        @Autowired
        FosUtils fosUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MobileConfigController.class);

	@RequestMapping(value = "/UpdateMobileConfig",method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public MobileConfigRes mobileConfig(@RequestBody MobileConfig mobileConfig) throws Exception {

		UserBasicInfo user = fosUtils.getUserBasicInfo(mobileConfig.getUserId(), false);

		JSONObject val = new JSONObject();
		JSONObject result = new JSONObject();
		JSONObject rtcData = new JSONObject();

		MobileRole mobileRole;
		logger.info("role is :" + user.getRole());
		if (user.getRole().equals(Role.STUDENT))
			mobileRole = MobileRole.STUDENT;
		else
			mobileRole = MobileRole.TEACHER;
		
		MobileGlobalConfiguration global= mobileConfigManager.getMobileGlobalConfiguration("-1","-1",
				mobileConfig.getPlatform(), mobileRole);

		result.put("message",global.getGlobal_message());
		if (Double.parseDouble(mobileConfig.getVersionCode()) < Double.parseDouble(global.getForce_update_version()))
			result.put("should_force_update", "true");
		else
			result.put("should_force_update", "false");
		if (Double.parseDouble(mobileConfig.getVersionCode()) < Double.parseDouble(global.getOptional_update_version()))
			result.put("update_available", "true");
		else
			result.put("update_available", "false");
		
		rtcData.put("rtc_client_type", global.getRtc_client_type());
		rtcData.put("rtc_ice_server", global.getRtc_ice_server());
		rtcData.put("rtc_turn_server", global.getRtc_turn_server());
		rtcData.put("rtc_turn_server_user_name", global.getRtc_turn_server_user_name());
		rtcData.put("rtc_turn_server_password", global.getRtc_turn_server_password());
		rtcData.put("socialvid_tenant_name", global.getSocialvid_tenant_name());
		
		val.put("app_update", result);
		val.put("feedback_pending", "false");
		JSONArray features = new JSONArray();
		features.put(new JSONObject().put("session", global.isSession()));
		JSONObject cmdsData = new JSONObject();
		cmdsData.put("cmds", global.isCmds());
		cmdsData.put("teacherCmdsHelpVideo", global.getTeacherCmdsHelpVideo());
		cmdsData.put("studentCmdsHelpVideo", global.getStudentCmdsHelpVideo());
		features.put(cmdsData);
		val.put("feature_config", features);
		val.put("rtc_data", rtcData);
		
		MobileConfigReq mobileConfigReq = new MobileConfigReq(mobileConfig.getUserId().toString(), mobileConfig.getDeviceId(), mobileConfig.getKey(), val.toString(), mobileConfig.getVersionCode(),
				mobileConfig.getPlatform(), mobileConfig.getPlatformVersion());
		List<MobileConfig> mobileConfigs = mobileConfigManager.upsertMobileConfig(mobileConfigReq);
		MobileConfigRes mobileConfigRes=new MobileConfigRes(mobileConfigs.get(0));
		return mobileConfigRes;
	}

	@RequestMapping(value = "/UpdateGlobalMobileParamsPost",method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public GlobalMobileConfigRes GlobalMobileConfiguration(@RequestBody UpdateGlobalMobileParamsReq updateGlobalMobileParamsReq) {
		MobileGlobalConfiguration mobileConfig = mobileConfigManager
				.upsertGlobalMobileConfiguration(updateGlobalMobileParamsReq);
		
		GlobalMobileConfigRes mobileConfigRes=new GlobalMobileConfigRes(mobileConfig);
		return mobileConfigRes;
		
	}

	@RequestMapping(value = "/globalMobileParams", method = RequestMethod.GET)
	public MobileGlobalConfiguration GlobalMobileConfiguration(@RequestParam("userId") String userId,
			@RequestParam("deviceId") String deviceId, @RequestParam("platform") MobilePlatform platform,
			@RequestParam("role") MobileRole role) {
		MobileGlobalConfiguration mobileConfig = mobileConfigManager.getMobileGlobalConfiguration(userId, deviceId,
				platform, role);
		return mobileConfig;
	}

	@RequestMapping(value = "/mobileConfig", method = RequestMethod.GET)
	public String mobileConfig(@RequestParam( value="userId", required = false) String userId, @RequestParam( value="role", required = true) Role role,@RequestParam(value = "deviceId", required = false) String deviceId,
			@RequestParam("key") String key, @RequestParam(value ="platform", required = true) MobilePlatform platform,
			@RequestParam(value = "platformVersion", required = false) String platformVersion, @RequestParam(value = "versionCode", required = true) String versionCode)
					throws Exception {

		//@RequestParam(value = "boardId", required = false)
		
		
//		Set<String> userIds = new HashSet<String>();
//		userIds.add(userId);
//		Set<UserBasicInfo> users = DataStoreUtils.getUserBasicInfosMap(userIds);
//		UserBasicInfo user = users.iterator().next();
		

		JSONObject val = new JSONObject();
		JSONObject result = new JSONObject();
		JSONObject rtcData = new JSONObject();

		MobileRole mobileRole;
		
		if (role.equals(Role.STUDENT))
			mobileRole = MobileRole.STUDENT;
		else
			mobileRole = MobileRole.TEACHER;
		MobileGlobalConfiguration mobileConfig = mobileConfigManager.getMobileGlobalConfiguration("-1", "-1", platform,
				mobileRole);

		if(mobileConfig==null) {
//			logger.error("Global Mobile config is null for " + mobileRole.toString());
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Mobile config not found");
		}
		
		result.put("message", mobileConfig.getGlobal_message());
		if (Double.parseDouble(versionCode) < Double.parseDouble(mobileConfig.getForce_update_version()))
			result.put("should_force_update", "true");
		else
			result.put("should_force_update", "false");
		if (Double.parseDouble(versionCode) < Double.parseDouble(mobileConfig.getOptional_update_version()))
			result.put("update_available", "true");
		else
			result.put("update_available", "false");
		
		rtcData.put("rtc_client_type", mobileConfig.getRtc_client_type());
		rtcData.put("rtc_ice_server", mobileConfig.getRtc_ice_server());
		rtcData.put("rtc_turn_server", mobileConfig.getRtc_turn_server());
		rtcData.put("rtc_turn_server_user_name", mobileConfig.getRtc_turn_server_user_name());
		rtcData.put("rtc_turn_server_password", mobileConfig.getRtc_turn_server_password());
		rtcData.put("socialvid_tenant_name", mobileConfig.getSocialvid_tenant_name());

		val.put("app_update", result);
		val.put("feedback_pending", "false");
		JSONArray features = new JSONArray();
		features.put(new JSONObject().put("session", mobileConfig.isSession()));
		JSONObject cmdsData = new JSONObject();
		cmdsData.put("cmds", mobileConfig.isCmds());
		cmdsData.put("teacherCmdsHelpVideo", mobileConfig.getTeacherCmdsHelpVideo());
		cmdsData.put("studentCmdsHelpVideo", mobileConfig.getStudentCmdsHelpVideo());
		features.put(cmdsData);
		val.put("feature_config", features);
		val.put("rtc_data", rtcData);
		

		MobileConfigReq mobileConfigReq = new MobileConfigReq(userId, deviceId, key, val.toString(), versionCode,
				platform, platformVersion);
		List<MobileConfig> mobileConfigs = mobileConfigManager.upsertMobileConfig(mobileConfigReq);
		return mobileConfigs.get(0).getValue();
	}

}
