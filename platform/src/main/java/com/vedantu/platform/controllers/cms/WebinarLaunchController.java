package com.vedantu.platform.controllers.cms;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("cms/webinar/launch")
public class WebinarLaunchController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CmsWebinarController.class);

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    private final static String growthCmsWebinarBasePath = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT") + "/cms/webinar/launch";

    private static Gson gson = new Gson();

    @RequestMapping(value = "/launchWebinarsById/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void launchWebinarsById(@PathVariable(value = "webinarId") String webinarId,
                                   HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/launchWebinarsById/" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);

    }

    @RequestMapping(value = "/instanceShutdown/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void instanceShutdown(@PathVariable(value = "webinarId") String webinarId,
                                 HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/instanceShutdown/" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);

    }

    @RequestMapping(value = "/instanceTerminated/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void instanceTerminated(@PathVariable(value = "webinarId") String webinarId,
                                   @RequestParam(value = "message", required = false) String message,
                                   HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/instanceTerminated/" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);

    }

    @RequestMapping(value = "/webinarLoaded/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void webinarLoaded(@PathVariable(value = "webinarId") String webinarId,
                              HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/webinarLoaded/" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);

    }

    @RequestMapping(value = "/teacherLoggedIn/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void teacherLoggedIn(@PathVariable(value = "webinarId") String webinarId,
                                @RequestParam(value = "isSuccess", defaultValue = "TRUE") String isSuccess,
                                HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/teacherLoggedIn/" + webinarId + "?isSuccess=" + isSuccess;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
    }

    @RequestMapping(value = "/getTodaysWebinars", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getTodaysWebinar(HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/getTodaysWebinars";

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);

        return gson.fromJson(res, PlatformBasicResponse.class);

    }

    @RequestMapping(value = "/getWebinarStatus/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getWebinarStatus(@PathVariable(value = "webinarId") String webinarId,
                                                  HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/getWebinarStatus/" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);
        return gson.fromJson(res, PlatformBasicResponse.class);

    }

    @RequestMapping(value = "/getActionButton/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getActionButton(@PathVariable(value = "webinarId") String webinarId,
                                                 HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        String url = growthCmsWebinarBasePath + "/getActionButton/" + webinarId;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String res = response.getEntity(String.class);
        return gson.fromJson(res, PlatformBasicResponse.class);

    }

}
