package com.vedantu.platform.controllers.subscription;

import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;

import com.vedantu.scheduling.request.session.EndSessionReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.pojo.subscription.GetSubscriptionsResponse;
import com.vedantu.platform.pojo.subscription.GetUnlockHoursResponse;
import com.vedantu.platform.pojo.subscription.SessionVO;
import com.vedantu.platform.pojo.subscription.SubscriptionVO;
import com.vedantu.platform.pojo.subscription.UnlockHoursRequest;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.SessionResponse;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SessionModel;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/subscription")
public class SubscriptionController {

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SubscriptionController.class);

    private String subscriptionEndpoint;
    private String dineroEndpoint;

    public SubscriptionController() {
        super();
    }

    @PostConstruct
    public void init() {
        subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    }

    @ApiOperation(value = "Create Subscription", notes = "Returns created subscription")
    @RequestMapping(value = "/createSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionResponse createSubscription(@RequestBody SubscriptionVO subscriptionVO) throws VException {
        return subscriptionManager.createSubscription(subscriptionVO);
    }

    @ApiOperation(value = "Get RefundInfo for Subscription", notes = "Returns subscription")
    @RequestMapping(value = "getRefundInfo/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CancelSubscriptionResponse getRefundInfo(@PathVariable("id") Long id) throws VException {
        logger.info("Request received for getting Refund info for subscriptionId: " + id);
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "getRefundInfo/" + id, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        CancelSubscriptionResponse cancelSubscriptionResponse = new Gson().fromJson(jsonString,
                CancelSubscriptionResponse.class);

        Float remainingPercentage = (float) (cancelSubscriptionResponse.getLockedHours()
                + cancelSubscriptionResponse.getRemainingHours()) / cancelSubscriptionResponse.getTotalHours();
        String queryStr = "studentId=" + cancelSubscriptionResponse.getStudentId() + "&subscriptionId=" + id
                + "&refundAmount=" + cancelSubscriptionResponse.getRefundAmount() + "&remainingPercentage="
                + remainingPercentage;
        resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/getRefundInfo?" + queryStr, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String refundResStr = resp.getEntity(String.class);
        logger.info("getRefundInfo Response:" + refundResStr);
        RefundRes refundRes = new Gson().fromJson(refundResStr, RefundRes.class);
        cancelSubscriptionResponse.fillCancelSubscriptionResponse(refundRes);

        logger.info("EXIT " + cancelSubscriptionResponse);
        return cancelSubscriptionResponse;
    }

    @ApiOperation(value = "Get a Subscription", notes = "Returns subscription")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SubscriptionResponse getSubscription(@PathVariable("id") Long id) throws VException {
        logger.info("Request received for getting Subscription for db_id: " + id);
        SubscriptionResponse subscriptionResponse = subscriptionManager.getSubscription(id);
        logger.info("getSubscription Response:" + subscriptionResponse.toString());
        return subscriptionResponse;
    }

    @Deprecated
    @ApiOperation(value = "Get a Subscription basic Info", notes = "Returns subscription basic details")
    @RequestMapping(value = "/getSubscriptionBasicInfo/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SubscriptionResponse getSubscriptionBasicInfo(@PathVariable("id") Long id) throws VException {
        return subscriptionManager.getSubscriptionBasicInfo(id);
    }

    @RequestMapping(value = "/getSubscriptionBasicInfoList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getSubscriptionBasicInfoList(@RequestBody String request) {
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(subscriptionEndpoint + "subscription/getSubscriptionBasicInfoList", HttpMethod.POST, request);
        return resp.getEntity(String.class);
    }

    @ApiOperation(value = "Get Subscriptions by TeacherId and StudentId or all", notes = "Returns list of subscriptions")
    @RequestMapping(value = "/getSubscriptions", method = RequestMethod.GET)
    @ResponseBody
    public GetSubscriptionsResponse getSubscriptions(GetSubscriptionsReq req) throws VException, IllegalArgumentException, IllegalAccessException {

        HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
        if (httpSessionData != null) {
            Long teacherId = req.getTeacherId();
            Long studentId = req.getStudentId();
            if (Role.TEACHER.equals(httpSessionData.getRole()) && (teacherId == null || teacherId <= 0l)) {
                req.setTeacherId(httpSessionData.getUserId());
            }
            if (Role.STUDENT.equals(httpSessionData.getRole()) && (studentId == null || studentId <= 0l)) {
                req.setStudentId(httpSessionData.getUserId());
            }
        }

        return subscriptionManager.getSubscriptions(req);
    }

    @ApiOperation(value = "Get Subscriptions basic info by TeacherId and StudentId or all", notes = "Returns list of subscriptions info")
    @RequestMapping(value = "getSubscriptionsBasicInfo", method = RequestMethod.GET)
    @ResponseBody
    public String getSubscriptionsBasicInfo(GetSubscriptionsReq req) throws VException, IllegalArgumentException, IllegalAccessException {

        HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
        if (httpSessionData != null) {
            Long teacherId = req.getTeacherId();
            Long studentId = req.getStudentId();
            if (Role.TEACHER.equals(httpSessionData.getRole()) && (teacherId == null || teacherId <= 0l)) {
                req.setTeacherId(httpSessionData.getUserId());
            }
            if (Role.STUDENT.equals(httpSessionData.getRole()) && (studentId == null || studentId <= 0l)) {
                req.setStudentId(httpSessionData.getUserId());
            }
        }
        String getString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                subscriptionEndpoint + "subscription/getSubscriptionsBasicInfo?" + getString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String subResp = resp.getEntity(String.class);
        return subResp;
    }

    @Transactional
    @ApiOperation(value = "Cancel a subscription", notes = "Returns subscription")
    @RequestMapping(value = "cancelSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CancelSubscriptionResponse cancelSubscription(
            @RequestBody CancelSubscriptionRequest cancelSubscriptionRequest)
            throws VException, NotFoundException, UnsupportedEncodingException, AddressException {
        logger.info("ENTRY " + cancelSubscriptionRequest);
        CancelSubscriptionResponse cancelSubscriptionResponse = subscriptionManager
                .cancelSubscription(cancelSubscriptionRequest);
        logger.info("EXIT: " + cancelSubscriptionResponse.toString());
        return cancelSubscriptionResponse;
    }

    @ApiOperation(value = "Get HourlyRate for Subscription", notes = "Returns subscription")
    @RequestMapping(value = "unlockDuration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUnlockHoursResponse unlockHours(@RequestBody List<UnlockHoursRequest> unlockHoursRequests)
            throws VException {
        return subscriptionManager.unlockHours(unlockHoursRequests);
    }

    @RequestMapping(value = "/cancelSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionResponse cancelSession(@RequestBody CancelSubscriptionSessionRequest cancelSessionRequest)
            throws VException, UnsupportedEncodingException, AddressException {
        cancelSessionRequest.verify();

        //TODO remove this hardcoding in a few days
        if (cancelSessionRequest.getCallingUserId().equals(6177681383096320l)
                || cancelSessionRequest.getCallingUserId().equals(4102376728415431l)) {
            logger.error("Ignore Error : User tried to cancel the session, reason: " + cancelSessionRequest.getReason());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    "Sorry, you are not allowed to cancel this session. Please contact Vedantu Student Care or ask your teacher to cancel the session.");
        }

        SessionInfo sessionInfo = sessionManager.getSessionById(cancelSessionRequest.getSessionId(), false);
        if (sessionInfo == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND,
                    "there is no session with the id " + cancelSessionRequest.getSessionId());
        }

        if (EntityType.COURSE_PLAN.equals(sessionInfo.getContextType())) {
            EndSessionReq req = new EndSessionReq();
            req.setSessionId(cancelSessionRequest.getSessionId());
            req.setRemark(cancelSessionRequest.getReason());
            req.setCallingUserId(cancelSessionRequest.getCallingUserId());
            req.setCallingUserRole(cancelSessionRequest.getCallingUserRole());
            coursePlanManager.cancelSession(req);
            return new SessionResponse();
        }

        SessionResponse resp;
        if (sessionInfo.getSubscriptionId() != null) {
            SubscriptionResponse subscriptionResponse = getSubscription(sessionInfo.getSubscriptionId());
            cancelSessionRequest.setSubscriptionId(sessionInfo.getSubscriptionId());
            if (subscriptionResponse == null) {
                throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
                        "there is no subscription with the id " + sessionInfo.getSubscriptionId());
            }

            HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();

            CancelSubscriptionRequest cancelSubscriptionRequest = new CancelSubscriptionRequest(false,
                    sessionInfo.getSubscriptionId(), cancelSessionRequest.getReason(), httpSessionData.getUserId());
            cancelSubscriptionRequest.setCallingUserId(httpSessionData.getUserId());
            cancelSubscriptionRequest.setCallingUserRole(httpSessionData.getRole());
            if (sessionInfo.getModel() != null && (sessionInfo.getModel().equals(SessionModel.TRIAL)
                    || sessionInfo.getModel().equals(SessionModel.IL))) {
                resp = subscriptionManager.cancelTrialSession(cancelSubscriptionRequest);
            } else if ((SessionModel.OTO.equals(sessionInfo.getModel()) && (subscriptionResponse.getSubModel() != null
                    && subscriptionResponse.getSubModel().equals(SubModel.LOOSE)))) {
                resp = subscriptionManager.cancelTrialSession(cancelSubscriptionRequest);
            } else {
                resp = subscriptionManager.cancelSession(cancelSessionRequest);
            }
        } else {
            EndSessionReq endSessionReq = new EndSessionReq(cancelSessionRequest);
            sessionManager.cancelSession(endSessionReq);
            resp = new SessionResponse(sessionInfo, null);
        }
        logger.info("cancelSession Response:" + resp.toString());
        return resp;
    }

    @RequestMapping(value = "/bookSessionFromSubscriptionHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionResponse bookSessionFromSubscriptionHours(@RequestBody SessionVO sessionVO) throws VException {
        return subscriptionManager.bookSessionFromSubscriptionHours(sessionVO);
    }

    @ApiOperation(value = "Get Subscriptions by TeacherId and StudentId or all", notes = "Returns list of subscriptions")
    @RequestMapping(value = "/getSubscriptionCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getSubscriptionsCount(GetSubscriptionsReq req) throws VException, IllegalArgumentException, IllegalAccessException {
        HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
        if (httpSessionData != null) {
            Long teacherId = req.getTeacherId();
            Long studentId = req.getStudentId();
            if (Role.TEACHER.equals(httpSessionData.getRole()) && (teacherId == null || teacherId <= 0l)) {
                req.setTeacherId(httpSessionData.getUserId());
            }
            if (Role.STUDENT.equals(httpSessionData.getRole()) && (studentId == null || studentId <= 0l)) {
                req.setStudentId(httpSessionData.getUserId());
            }
        }
        String getString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(subscriptionEndpoint + "subscription/getSubscriptionCount?" + getString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String subResp = resp.getEntity(String.class);
        Long response = new Gson().fromJson(subResp, Long.class);
        return response;
    }
}
