package com.vedantu.platform.controllers;

import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.enums.SQSQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cloudwatch")
public class AwsCloudWatchController {

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;

    @RequestMapping(value = "/createAlarmForServiceLevelQueues", method = RequestMethod.GET)
    public @ResponseBody PlatformBasicResponse createAlarmForServiceLevelQueues(@RequestBody List<SQSQueue> queues, @RequestParam(name = "serviceName", required = true) String serviceName){

        for(SQSQueue sqsQueue : queues){
            awsCloudWatchManager.creatAlarmServiceSpecific(sqsQueue, serviceName);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/createAlarmForGenerallQueues", method = RequestMethod.GET)
    public @ResponseBody PlatformBasicResponse createAlarmForGenerallQueues(@RequestBody Map<String,Object> data,
                                                                            @RequestParam(name = "serviceName", required = true) String serviceName){
        List<String> queues = (List<String>) data.get("queues");
        Double visibleThreshold = (Double) data.get("visibleThreshold");
        Double ageThreshold = (Double) data.get("ageThreshold");
        Regions regions = Regions.valueOf(data.get("regions").toString());

        AwsCloudWatchManager cloudWatchManager = new AwsCloudWatchManager(regions);
        for(String sqsQueue : queues){
            cloudWatchManager.creatAlarm(sqsQueue, visibleThreshold, ageThreshold, serviceName, regions);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/createAlarmForManuallyCreatedQueues", method = RequestMethod.GET)
    public @ResponseBody PlatformBasicResponse createAlarmForManuallyCreatedQueues(@RequestParam(name = "region", required = true) Regions region){

        AwsCloudWatchManager cloudWatchManager = new AwsCloudWatchManager(region);
        cloudWatchManager.createAlarmsForAllManuallyCreatedQueues();

        return new PlatformBasicResponse();
    }

}
