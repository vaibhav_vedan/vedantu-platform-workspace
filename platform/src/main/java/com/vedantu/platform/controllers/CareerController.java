package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.entity.Career;
import com.vedantu.platform.managers.CareerManager;
import com.vedantu.platform.request.CareerReq;
import com.vedantu.platform.response.CareersWithFilterRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;

/**
 * @author MNPK
 */
@RestController

public class CareerController {
    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(CareerController.class);

    @Autowired
    private CareerManager careerManager;

    @Autowired
    private HttpSessionUtils sessionUtils;


    @RequestMapping(value = "/getCareers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public List<Career> getAllCareerJobs() throws VException {
        return careerManager.getAllCareerJobs();
    }

    @RequestMapping(value = "/setCareers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse setCareers(@RequestBody List<CareerReq> req) {
        return careerManager.setCareers(req);
    }

    @RequestMapping(value = "/careers/createJD", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse createJD(@RequestBody CareerReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return careerManager.createJd(req);
    }

    @RequestMapping(value = "/careers/updateJD", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse updateJD(@RequestBody CareerReq req) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return careerManager.updateJd(req);
    }

    @RequestMapping(value = "/careers/updateEntityStateById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse updateEntityStateById(@RequestParam(value = "id", required = true) String id, @RequestParam(value = "state", required = true) EntityState state) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return careerManager.updateEntityStateById(id, state);
    }

    @RequestMapping(value = "/getCareersWithFilter", produces = MediaType.APPLICATION_JSON)
    public List<CareersWithFilterRes> getCareersWithFilter(@RequestParam(value = "start", required = true) Integer start, @RequestParam(value = "limit", required = true) Integer limit, @RequestParam(value = "state", required = false) EntityState state) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return careerManager.getCareersWithFilter(start, limit, state);
    }


}
