/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.response.UploadFileUrlRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.platform.request.CreateUserDetailsFromReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import java.util.HashMap;
import java.util.Map;
import com.vedantu.util.StringUtils;
import javax.ws.rs.core.MediaType;

import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/islData")
public class ISLDataController {

    @Autowired
    LogFactory logFactory;

    @Autowired
    AwsS3Manager awsS3Manager;

    @Autowired
    HttpSessionUtils sessionUtils;

    private final String UNBOUNCE_BUCKET_NAME = "vedantu-temp-bucket";

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    Logger logger = logFactory.getLogger(ISLDataController.class);

    @RequestMapping(value = "/sendCommunication", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void sendCommunication(@RequestBody CreateUserDetailsFromReq req) throws VException {
        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "This api is not supported yet");

//        if(req.getStudentList()==null || req.getStudentList().size()>100 || req.getStudentList().isEmpty()){
//                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "req params not correct ");
//        }
//        String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//        String smsBody="Your child has filled form at school for International Student League (ISL)-The Biggest International Online Aptitude Competition with trips to Oxford University, UK and CERN and UN in Europe as top prizes. ISL is organized by Times of India and Vedantu. See detailed info and complete free registration to generate your ISL Password here: www.vedantu.com/isl.";
//        String whatsapp=" has filled the form at school for *International Student League (ISL)* 2017/18 - the Biggest International Online Aptitude Competition. ISL is organized by *Times of India* and *Vedantu* with educational trips to *Oxford University, UK* and *CERN, UN and UNESCO in Europe* as top awards along with a host of exciting prizes such as 3D Printers, Drones, iPad Pro etc. Each student will also receive a *Certificate of Participation* with *International Rank*. See detailed info and complete free registration to generate your ISL Password here: *www.vedantu.com/isl*.";
//        for(ISLStudentData student:req.getStudentList()){
//                TextSMSRequest request=new TextSMSRequest(student.getParentPhoneNo(), student.getParentPhoneCode(), smsBody, CommunicationType.PHONE_VERIFICATION);
//		WebUtils.INSTANCE.doCall(notificationEndpoint + "/SMS/sendSMS", HttpMethod.POST, new Gson().toJson(request));
//                String whatsappMessage="Greetings! Your child "+student.getStudentName()+whatsapp;
//                WebUtils.INSTANCE.doCall("http://dashboard.boatman.ai:3000/api/send_message", HttpMethod.POST, new Gson().toJson(new ISLWhatsappMessage(student.getParentPhoneCode()+student.getParentPhoneNo(), whatsappMessage)),false);
//                super.saveEntity(student);
//        }
//        return new BasicRes();
    }

    @RequestMapping(value = "/getUnbounceUploadSignedUrl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public UploadFileUrlRes getUploadSignedUrl(@RequestParam("mimeType") String mimeType,@RequestParam("extension") String extension) throws VException {
        
        throw new UnsupportedOperationException("this operation is not supported");
//        if(StringUtils.isEmpty(extension)){
//            extension=".csv";
//        }
//        if(StringUtils.isEmpty(mimeType)){
//            mimeType="text/csv";
//        }
//        String key=new StringBuilder()
//                .append(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())
//                .append(java.io.File.separator)
//                .append("MISC")
//                .append(java.io.File.separator)
//                .append("unbounce_")
//                .append(System.currentTimeMillis())
//                .append(extension)
//                .toString();
//        String uploadUrl=awsS3Manager.getPreSignedUrl(UNBOUNCE_BUCKET_NAME, key,mimeType);
//        UploadFileUrlRes res=new UploadFileUrlRes();
//        res.setUploadUrl(uploadUrl);
//        return res;
    }

    @RequestMapping(value = "/createUserDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse createUserDetails(@RequestBody CreateUserDetailsFromReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        Map<String, Object> payload = new HashMap<>();
        payload.put("req", req);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_ISL_USER_DETAILS_FROM_LIST, payload);
        asyncTaskFactory.executeTask(params);
        return new PlatformBasicResponse();
    }

}
