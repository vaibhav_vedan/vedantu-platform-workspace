package com.vedantu.platform.controllers.subscription;

import java.util.List;

import com.vedantu.subscription.request.EnrolViaPassReq;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.subscription.BundlePackageManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.request.AddEditBundlePackageReq;
import com.vedantu.subscription.request.GetBundlePackagesReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.request.PurchaseBundlePackageReq;
import com.vedantu.subscription.response.PurchaseBundleRes;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;

/**
 * Created by somil on 09/05/17.
 */
@RestController
@RequestMapping("/bundlePackage")
public class BundlePackageController {

    @Autowired
    private BundlePackageManager bundlePackageManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundlePackageController.class);

    @Deprecated
    @RequestMapping(value = "/addEditBundlePackage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageInfo addEditBundlePackage(@RequestBody AddEditBundlePackageReq addEditBundleReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.addEditBundlePackage(addEditBundleReq);
    }

    @Deprecated
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageInfo getBundlePackage(@PathVariable("id") String id) throws VException {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.getBundlePackage(id);
    }

    @Deprecated
    @RequestMapping(value = "/public/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageInfo getPublicBundlePackage(@PathVariable("id") String id) throws VException {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.getBundlePackage(id);
    }

    @Deprecated
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageInfo> getBundlePackageInfos(GetBundlePackagesReq req) throws VException {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.getBundlePackageInfos(req);
    }

    @RequestMapping(value = "/get/public", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageInfo> getPublicBundlePackageInfos(GetBundlePackagesReq req) throws VException {
        return bundlePackageManager.getBundlePackageInfos(req);
    }

    @RequestMapping(value = "/getBundlePackageDetails/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageDetailsInfo getBundlePackageDetails(@PathVariable("id") String id) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.getBundlePackageDetails(id);
    }

    @Deprecated
    @RequestMapping(value = "/getBundlePackageDetailsList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageDetailsInfo> getBundlePackageDetailsList(GetBundlesReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.getBundlePackageDetailsList(req);
    }

    @RequestMapping(value = "/enrolViaPass", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolmentInfo> enrolViaPass(@RequestBody EnrolViaPassReq req) throws VException {
        //sessionUtils.checkIfAllowed(addVideoAnalyticsDataReq.getUserId(), null, false);
        return bundlePackageManager.enrolViaPass(req);
    }

    @RequestMapping(value = "/purchaseBundlePackage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PurchaseBundleRes purchaseBundlePackage(@RequestBody PurchaseBundlePackageReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return bundlePackageManager.purchaseBundlePackage(req);
    }

    @Deprecated
    @RequestMapping(value = "/getBundlesForEntity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageInfo> getBundlesForEntity(GetBundlePackagesReq req) throws VException {
        return bundlePackageManager.getBundlesForEntity(req);
    }
}
