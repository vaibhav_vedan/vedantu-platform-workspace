package com.vedantu.platform.controllers.onetofew;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import com.vedantu.onetofew.pojo.SessionBluePrintEntry;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.util.InstalmentUtils;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.GetCourseRes;
import com.vedantu.onetofew.pojo.StudentSlotPreferencePojo;
import com.vedantu.onetofew.request.GetBatchesReq;
import com.vedantu.onetofew.request.GetCoursesReq;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/onetofew/course")
public class OTFCourseController {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	PaymentManager paymentManager;

	@Autowired
    private HttpSessionUtils sessionUtils;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(OTFCourseController.class);

	// one to few end point is configured to contain / at the end
	private String subscriptionEndPoint;

	@PostConstruct
	public void init() {
		subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT")+"/";
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CourseInfo addCourse(HttpServletRequest request) throws Exception {
		String requestBody = PlatformTools.getBody(request);
		List<BaseInstalmentInfo> baseInstalmentInfos = InstalmentUtils.getBaseInstalmentsFromRequest(requestBody);
                                
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "course", HttpMethod.POST, requestBody);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		CourseInfo courseInfo = new Gson().fromJson(jsonString, CourseInfo.class);

		BaseInstalment baseInstalment = new BaseInstalment(InstalmentPurchaseEntity.OTF_COURSE, courseInfo.getId(),
				baseInstalmentInfos);
		paymentManager.updateBaseInstalment(baseInstalment);
		courseInfo.setInstalmentDetails(baseInstalmentInfos);
		return courseInfo;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public CourseInfo getCourseTemp(@PathVariable("id") String id) throws VException, JSONException {
		return getCourse(id);
	}

	@RequestMapping(value = "/getCourse/{id}", method = RequestMethod.GET)
	@ResponseBody
	public CourseInfo getCourse(@PathVariable("id") String id) throws VException, JSONException {
            
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "course/" + id, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		CourseInfo courseInfo = new Gson().fromJson(jsonString, CourseInfo.class);
		BaseInstalment baseInstalment = paymentManager.getBaseInstalment(InstalmentPurchaseEntity.OTF_COURSE, id);
		if (baseInstalment != null) {
			courseInfo.setInstalmentDetails(baseInstalment.getInfo());
		}

		HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
		if (sessionData == null || !sessionData.hasAdminRole()) {

			List<SessionBluePrintEntry> bluePrintEntries = courseInfo.getSessionBluePrint();

			if(CollectionUtils.isNotEmpty(bluePrintEntries)){

				for(SessionBluePrintEntry bluePrint :bluePrintEntries){
					bluePrint.setTeacherEmail(null);
				}
			}
		}

		return courseInfo;
	}

	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public GetCourseRes getCourseTemp(GetCoursesReq getCoursesReq) 
			throws VException, JSONException, IllegalArgumentException, IllegalAccessException{
		return getCourse(getCoursesReq);
	}
	
	@RequestMapping(value = "/getCourse",method = RequestMethod.GET)
	@ResponseBody
	public GetCourseRes getCourse(GetCoursesReq getCoursesReq)
			throws VException, JSONException, IllegalArgumentException, IllegalAccessException {
		String getCoursesUrl = subscriptionEndPoint + "course" + "?"
				+ WebUtils.INSTANCE.createQueryStringOfObject(getCoursesReq);

		ClientResponse resp = WebUtils.INSTANCE.doCall(getCoursesUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);

		GetCourseRes response = new Gson().fromJson(jsonString, GetCourseRes.class);
		List<CourseInfo> courseInfos = response.getList();
		if (courseInfos != null && !courseInfos.isEmpty()) {
			List<String> courseIds = new ArrayList<>();
			for (CourseInfo courseInfo : courseInfos) {
				courseIds.add(courseInfo.getId());
			}
			List<BaseInstalment> baseInstalments = paymentManager
					.getBaseInstalments(InstalmentPurchaseEntity.OTF_COURSE, courseIds);
			if (baseInstalments != null && !baseInstalments.isEmpty()) {
				Map<String, BaseInstalment> baseInstalmentMap = new HashMap<>();
				for (BaseInstalment baseInstalment : baseInstalments) {
					baseInstalmentMap.put(baseInstalment.getPurchaseEntityId(), baseInstalment);
				}
				for (CourseInfo courseInfo : courseInfos) {
					BaseInstalment baseInstalment = baseInstalmentMap.get(courseInfo.getId());
					if (baseInstalment != null) {
						courseInfo.setInstalmentDetails(baseInstalment.getInfo());
					}
				}

			}
		}
		return response;
	}

	@Deprecated
	@RequestMapping(value = "/fetchStudentSlotPreference", method = RequestMethod.GET)
	@ResponseBody
	public String fetchStudentSlotPreference(GetBatchesReq req) throws VException{
		sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
		String fetchStudentSlotPreferenceUrl = subscriptionEndPoint + "course/fetchStudentSlotPreference" + "?"
				+ WebUtils.INSTANCE.createQueryStringOfObject(req);

		ClientResponse resp = WebUtils.INSTANCE.doCall(fetchStudentSlotPreferenceUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);

		return jsonString;		
	}

	@Deprecated
	@RequestMapping(value="/updateStudentPreferenceSessionSlot",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse updateStudentPreferenceSessionSlot(@RequestBody StudentSlotPreferencePojo req) throws Exception {
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "course/updateStudentPreferenceSessionSlot", HttpMethod.POST, new Gson().toJson(req));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		return new PlatformBasicResponse();
	}
}
