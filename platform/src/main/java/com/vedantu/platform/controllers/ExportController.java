package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.ExportManager;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.request.AvailabilityTeacherExportReq;
import com.vedantu.platform.request.ExportSubscriptionReq;
import com.vedantu.platform.request.ExportUsersReq;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.AddressException;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;

/**
 * Created by somil on 27/01/17.
 */
@RestController
@RequestMapping("/export")
public class ExportController {

    @Autowired
    LogFactory logFactory;

    @Autowired
    HttpSessionUtils httpSessionUtils;

    @Autowired
    ExportManager exportManager;

    Logger logger = logFactory.getLogger(ExportController.class);

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @RequestMapping(value = "/exportUsers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes exportUsers(@RequestBody ExportUsersReq req) throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        if (req.getFromTime() == null || req.getTillTime() == null || req.getTillTime() < req.getFromTime()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid from date and/or to Date");
        }
        Long maxValid = (long) 60l * DateTimeUtils.MILLIS_PER_DAY;
        if ((req.getTillTime() - req.getFromTime()) > maxValid) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Time Period exceeds 60 Days. Invalid Request");
        }
        return exportManager.exportUsersAsync(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/exportSubscriptions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes exportSubscriptions(@RequestBody ExportSubscriptionReq req) throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, null, Boolean.TRUE);
        return exportManager.exportSubscriptionsAsync(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/exportScheduledTOSSessions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes exportScheduledTOSSessions(@RequestBody GetSessionsReq req) throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, null, Boolean.TRUE);
        return exportManager.exportScheduledTOSSessionsAsync(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/exportSessions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes exportSessions(@RequestBody GetSessionsReq req) throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return exportManager.exportSessionsAsync(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/exportAvailability", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes exportAvailability(@RequestBody AvailabilityTeacherExportReq req) throws Exception {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, null, Boolean.TRUE);
        return exportManager.exportAvailabilityAsync(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/exportCoursePlans", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicRes exportCoursePlans(@RequestBody ExportCoursePlansReq req) throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
//        if (req.getFromTime() == null || req.getTillTime() == null || req.getTillTime() < req.getFromTime()) {
//            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid from date and/or to Date");
//        }
//        Long maxValid = (long) 60 * DateTimeUtils.MILLIS_PER_DAY;
//        if ((req.getTillTime() - req.getFromTime()) > maxValid) {
//            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Time Period exceeds 60 Days. Invalid Request");
//        }
        return exportManager.exportCoursePlansAsync(req, httpSessionUtils.getCurrentSessionData());
    }

    @RequestMapping(value = "/exportPasswordEmptyUsersForISL", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse exportPasswordEmptyUsersForISL() throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        Map<String, Object> payload = new HashMap<>();
        payload.put("sessionData", httpSessionUtils.getCurrentSessionData());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_PASSWORD_EMPTY_USERS_FOR_ISL,
                payload);
        asyncTaskFactory.executeTask(params);
        String email = httpSessionUtils.getCurrentSessionData().getEmail();
        return new PlatformBasicResponse(true, "Please check your email "
                + (StringUtils.isNotEmpty(email) ? email : httpSessionUtils.getCurrentSessionData().getContactNumber()), "");
    }

}
