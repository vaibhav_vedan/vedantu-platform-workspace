/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.sessionmetrics;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.platform.enums.sessionmetrics.SessionAnalysisAggregatorAction;
import com.vedantu.platform.managers.sessionmetrics.SessionMetricsManager;
import com.vedantu.platform.pojo.wave.NodeAddress;
import com.vedantu.platform.pojo.wave.NodeServerResponse;
import com.vedantu.platform.response.sessionmetrics.SessionAnalysisAggregateResponse;
import com.vedantu.platform.response.sessionmetrics.SessionAnalysisDataResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/session-metrics")
public class SessionMetricsController {
    @Autowired
    SessionMetricsManager sessionMetricsManager;
    
    @Autowired
    private LogFactory logFactory;    
    
    private final Logger logger = logFactory.getLogger(SessionMetricsController.class);
    
     //@CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/runqueries")
    @ResponseBody
    public SessionAnalysisAggregateResponse runqueries(@RequestParam(value = "reqList", required = false) List<SessionAnalysisAggregatorAction> reqList,@RequestParam(value = "startTime", required = true) Long startTime,@RequestParam(value = "endTime", required = true) Long endTime) throws VException {
        logger.log(Level.INFO, "Got request for runqueries for startTime " + startTime+"  endTime:"+endTime+"  reqList:"+reqList);
        return sessionMetricsManager.runqueries(startTime,endTime,reqList);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/sessiondata")
    @ResponseBody
    public SessionAnalysisDataResponse sessiondata(@RequestParam(value = "sessionId", required = true) String sessionId) throws VException {
        logger.log(Level.INFO, "Got request for sessiondata for sessionId " + sessionId);
        return sessionMetricsManager.getBySessionId(sessionId);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/usersessiondata")
    @ResponseBody
    public SessionAnalysisDataResponse usersessiondata(@RequestParam(value = "userId", required = false) String userId,@RequestParam(value = "email", required = false) String email,@RequestParam(value = "fromTime", required = false) Long fromTime,@RequestParam(value = "tillTime", required = false) Long tillTime,@RequestParam(value = "order", required = false) String order) throws VException {
        logger.log(Level.INFO, "Got request for usersessiondata for userId " + userId);
        return sessionMetricsManager.getByUserId(userId,email,fromTime,tillTime,order);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/analysesession")
    @ResponseBody
    public PlatformBasicResponse analysesession(@RequestParam(value = "sessionId", required = true) String sessionId) throws VException {
        logger.log(Level.INFO, "Got request for analysesession for sessionId " + sessionId);
        return sessionMetricsManager.analyseSession(sessionId);
    }

}
