package com.vedantu.platform.controllers;

import inti.ws.spring.exception.client.BadRequestException;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.BroadcastAsync;
import com.vedantu.platform.managers.BroadcastManager;
import com.vedantu.platform.enums.broadcast.BroadcastAction;
import com.vedantu.platform.enums.broadcast.BroadcastStatus;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.pojo.broadcast.Broadcast;
import com.vedantu.platform.pojo.broadcast.BroadcastInfo;
import com.vedantu.platform.pojo.broadcast.BroadcastLevel;
import com.vedantu.platform.pojo.broadcast.SendBroadcastRequest;
import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("/broadcast")
public class BroadcastController {

	@Autowired
	LogFactory logFactory;

	Logger logger = logFactory.getLogger(RequestCallbackDao.class);

	@Autowired
	BroadcastManager broadcastManager;

	@ApiOperation(value = "send Broadcast", notes = "Returns created broadcast request status")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<Broadcast> sendBroadcast(
			@RequestBody SendBroadcastRequest sendBroadcastRequest)
			throws Exception {
		sendBroadcastRequest.verify();
		try {

			switch (sendBroadcastRequest.getBroadcastType()) {
			case ASKADOUBT:
				
				List<BroadcastLevel> broadcastLevels = broadcastManager.getBroadcastLevel(null, sendBroadcastRequest.getBroadcastType(), sendBroadcastRequest.getLevel(), null, null);
				
				if(broadcastLevels ==null || broadcastLevels.isEmpty()){
					throw new VException(ErrorCode.BROADCAST_LEVEL_NOT_PRESENT, "No level present for this braodcast :" + sendBroadcastRequest.toString());
				}
				
				sendBroadcastRequest.setUserIds(broadcastLevels.get(0).getUserIds());
				logger.info(sendBroadcastRequest.toString());
//				broadcastAsync.sendAskADoubtBroadcast(sendBroadcastRequest);
				break;
			default:
				throw new VException(ErrorCode.ACTION_NOT_ALLOWED, "Broadcast type not supported");
			}
			
		} catch (Exception e) {

		}
		return null;

	}

	@ApiOperation(value = "get Broadcasts", notes = "Returns broadcasts")
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<BroadcastInfo> getBroadcast(
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "broadcastType", required = false) BroadcastType broadcastType,
			@RequestParam(value = "broadcastStatus", required = false) BroadcastStatus broadcastStatus,
			@RequestParam(value = "level", required = false) Integer level,
			@RequestParam(value = "referenceId", required = false) String referenceId,
			@RequestParam(value = "fromTime", required = false) Long fromTime,
			@RequestParam(value = "toTime", required = false) Long toTime,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer size)
			throws Exception {
		try {
			return broadcastManager.getBroadcast(id, userId, broadcastType,
					broadcastStatus, level, referenceId, fromTime, toTime,
					start, size);
		} catch (Exception e) {
			throw e;
		}

	}

	@ApiOperation(value = "update Broadcast", notes = "update broadcasts")
	@RequestMapping(value = "/updateStatus/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public BroadcastInfo updateBroadcastStatus(
			@PathVariable("id") String id,
			@RequestParam(value = "action", required = true) BroadcastAction broadcastAction
			) throws Exception {
		try {
			BroadcastInfo broadcastInfo = new BroadcastInfo();
			
			switch (broadcastAction) {
			case ACCEPT:
				broadcastManager.acceptBroadcast(id);
				break;
			case REJECT:
				broadcastInfo = broadcastManager.rejectBroadcast(id);
				break;
			case EXPIRE:
				broadcastInfo = broadcastManager.expireBroadcast(id);
				break;
			default:
				throw new BadRequestException(
						"Broadcast Action type not supported : "
								+ broadcastAction);
			}
			return broadcastInfo;
		} catch (Exception e) {
			throw e;
		}

	}
	
	@ApiOperation(value = "add Broadcast level", notes = "add broadcast level")
	@RequestMapping(value = "/broadcastlevel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public BroadcastLevel addBroadcastLevel(
			@RequestParam(value = "broadcastType", required = true) BroadcastType broadcastType,
			@RequestParam(value = "level", required = true) Integer level,
			@RequestParam(value = "expiryTime", required = true) Long expiryTime,
			@RequestBody List<Long> userIds) throws Exception {
		try {
			return broadcastManager.addUpdateBroadcastLevel(null, broadcastType, level, expiryTime, userIds);
		}catch(Exception e){
			throw e;
		}
		}
	
	@ApiOperation(value = "update Broadcast level", notes = "update broadcast level")
	@RequestMapping(value = "/broadcastlevel/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public BroadcastLevel updateBroadcastLevel(
			@PathVariable("id") String id,
			@RequestParam(value = "broadcastType", required = true) BroadcastType broadcastType,
			@RequestParam(value = "level", required = true) Integer level,
			@RequestParam(value = "expiryTime", required = true) Long expiryTime,
			@RequestBody List<Long> userIds) throws Exception {
		try {
			return broadcastManager.addUpdateBroadcastLevel(id, broadcastType, level, expiryTime, userIds);
		}catch(Exception e){
			throw e;
		}
		}
	
	@ApiOperation(value = "get Broadcast levels", notes = "get Broadcast levels")
	@RequestMapping(value = "/broadcastlevel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<BroadcastLevel> getBroadcastLevel(
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "broadcastType", required = true) BroadcastType broadcastType,
			@RequestParam(value = "level", required = false) Integer level,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer size) throws Exception {
		try {
			return broadcastManager.getBroadcastLevel(id, broadcastType, level, start, size);
		}catch(Exception e){
		throw e;
		}
	}
	
}
