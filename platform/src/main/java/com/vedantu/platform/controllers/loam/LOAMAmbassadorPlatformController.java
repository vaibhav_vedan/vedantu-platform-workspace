package com.vedantu.platform.controllers.loam;


import com.vedantu.exception.*;
import com.vedantu.platform.managers.loam.LOAMAmbassadorPlatformManager;
import com.vedantu.platform.mongodbentities.board.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/loam")
public class LOAMAmbassadorPlatformController {

    @Autowired
    LOAMAmbassadorPlatformManager loamAmbassadorPlatformManager;

    @RequestMapping(value = "/loam_getBoardData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Board> loam_getBoardData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                               @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return loamAmbassadorPlatformManager.loam_getBoardData(fromTime, thruTime, start, size);
    }
}
