/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.social;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.response.GetUserVoteRes;
import com.vedantu.lms.cmds.response.GetVotesRes;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.managers.social.SocializerManager;
import com.vedantu.platform.request.social.AddCommentReq;
import com.vedantu.platform.request.social.GetCommentsReq;
import com.vedantu.platform.request.social.GetVotesReq;
import com.vedantu.platform.response.social.AddViewReq;
import com.vedantu.util.request.VoteReq;
import com.vedantu.platform.response.social.GetCommentsRes;
import com.vedantu.platform.response.social.LastViewTime;
import com.vedantu.platform.response.social.UserActivity;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.request.BookmarkReq;
import com.vedantu.util.request.VoteReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ajith
 */
@RestController
@RequestMapping("/social")
public class SocializerController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SocializerController.class);

    @Autowired
    private SocializerManager socializerManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/addVote", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse addVote(@RequestBody VoteReq req) throws BadRequestException, VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return socializerManager.addVote(req, httpSessionData.getUserId());
    }

    @RequestMapping(value = "/addBookmark", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse addBookmark(@RequestBody BookmarkReq req) throws BadRequestException, VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return socializerManager.addBookmark(req, httpSessionData.getUserId());
    }

    @RequestMapping(value = "/addComment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse addComment(@Valid @RequestBody AddCommentReq req) throws BadRequestException, VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return socializerManager.addComment(req, httpSessionData.getUserId());
    }

    @RequestMapping(value = "/getComments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetCommentsRes getComments(@ModelAttribute GetCommentsReq req) throws VException {
        return socializerManager.getComments(req);
    }

    @RequestMapping(value = "/getVotes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetVotesRes getVotes(@ModelAttribute GetVotesReq req) throws VException {
        return socializerManager.getVotes(req);
    }

    @RequestMapping(value = "/getUserVotes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUserVoteRes getUserVotes(@ModelAttribute GetVotesReq req) throws VException {
        return socializerManager.getUserVoteRes(req);
    }

    @RequestMapping(value = "/isBookmarked", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse isBookmarked(@RequestParam(value = "userId", required = true) Long userId,
                                              @RequestParam(value = "contextId", required = true) String contextId) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        Map<String, Boolean> map = socializerManager.getBookmarkedMap(userId, Arrays.asList(contextId));
        if (map != null && Boolean.TRUE.equals(map.get(contextId))) {
            res.setSuccess(true);
        } else {
            res.setSuccess(false);
        }
        return res;
    }

    @RequestMapping(value = "/getBookmarkedMap", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Boolean> getBookmarkedMap(@RequestParam(value = "userId", required = true) Long userId,
            @RequestParam(value = "entityIds", required = true) List<String> entityIds) {
        return socializerManager.getBookmarkedMap(userId, entityIds);
    }

    @RequestMapping(value = "/getBookmarkedContextIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getBookmarkedContextIds(@RequestParam(value = "userId", required = true) Long userId,
            @RequestParam(value = "socialContextType", required = true) SocialContextType socialContextType,
            @RequestParam(value = "inputContextIds", required = false) List<String> inputContextIds,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size) {
        return socializerManager.getBookmarkedContextIds(userId, socialContextType, inputContextIds, start, size);
    }

    @RequestMapping(value = "/logViews", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse addBookmark(@RequestBody AddViewReq req) throws BadRequestException, VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return socializerManager.addView(req, httpSessionData.getUserId());
    }

    @RequestMapping(value = "/lastView", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public LastViewTime getLastView(@RequestParam(value = "userId", required = true) Long userId, @RequestParam(value = "socialContextType", required = true) String socialContextType) throws BadRequestException, VException {
        return socializerManager.getLastView(userId, socialContextType);
    }

    @RequestMapping(value = "/getRecentActivities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserActivity> getRecentActivities(@RequestParam(value = "userId") String userId,
                                                  @RequestParam(value = "limit") int limit) {
        logger.info("method=getRecentActivities, class=SocializerController");
        return socializerManager.getRecentActivities(userId, limit);
    }

    @RequestMapping(value = "/getRecentlyWatchedVideos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getRecentlyWatchedVideos(@RequestParam(value = "userId") String userId,
                                          @RequestParam(value = "limit") int limit) {
        logger.info("method=getRecentlyWatchedVideos, class=SocializerController");
        return socializerManager.getRecentlyWatchedVideos(userId, limit);
    }
}
