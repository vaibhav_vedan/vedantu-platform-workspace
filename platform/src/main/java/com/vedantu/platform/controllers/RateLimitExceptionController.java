package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.RateLimitExceptionManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/ratelimit")
public class RateLimitExceptionController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RateLimitExceptionController.class);

    @Autowired
    private RateLimitExceptionManager rateLimitExceptionManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/getRateLimitRecords", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getRateLimitRecords(@RequestParam(value = "keyPattern")String keyPattern) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), Boolean.FALSE);
        return rateLimitExceptionManager.getRateLimitRecords(keyPattern);
    }

    @RequestMapping(value = "/deleteBlockedUserRecord", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse deleteUserRecord(@RequestParam(value = "keys") List<String> keys) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), Boolean.FALSE);
        return rateLimitExceptionManager.deleteUserRecord(keys);
    }
}
