package com.vedantu.platform.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.platform.managers.ReviseManager;
import com.vedantu.platform.mongodbentities.ReviseIndiaStats;
import com.vedantu.platform.mongodbentities.ReviseJeeStats;
import com.vedantu.platform.pojo.ReviseJeeUsageStats;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/revise")
public class ReviseController {
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReviseController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    ReviseManager reviseManager;

    @RequestMapping(value = "/stats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ReviseIndiaStats getStats(@RequestParam(value = "secretKey", required = false) Long secretKey,
                                     @RequestParam(value = "userId", required = false) Long userId) throws Exception {
        Long id = 0L;
        Long secret = 8939112868L;
        if(secret.equals(secretKey)){
            id = userId;
        }
        else{
            id = sessionUtils.getCallingUserId();
        }
        return reviseManager.getStats(id);
    }

    @RequestMapping(value = "/jeeStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ReviseJeeStats jeeStats() throws Exception {

        if(sessionUtils.getCurrentSessionData() == null){
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "Not Logged in");
        }

        return reviseManager.jeeStats(sessionUtils.getCallingUserId());
    }

    @RequestMapping(value = "/jeeUsageStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ReviseJeeUsageStats jeeUsageStats() throws Exception {

        if(sessionUtils.getCurrentSessionData() == null){
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "Not Logged in");
        }

        return reviseManager.jeeUsageStats(sessionUtils.getCallingUserId());
    }

    @RequestMapping(value = "/s3Links", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Map<String, String> getStats() throws Exception {
        return reviseManager.s3Links();
    }

    @RequestMapping(value = "/jeeS3Links", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Map<String, String> getJeeS3Links() throws Exception {
        return reviseManager.getJeeS3Links();
    }

    @RequestMapping(value = "/updateAfterTest", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE ,produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void updateAfterTest(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                 @RequestBody String req) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(req, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            String message = subscriptionRequest.getMessage();
            reviseManager.updateAfterTest(message);
        }
    }

    @RequestMapping(value = "/updateAfterReviseJeeTest", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE ,produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void updateAfterReviseJeeTest(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                @RequestBody String req) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(req, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            String message = subscriptionRequest.getMessage();
            reviseManager.updateReviseJeeAfterTest(message);
        }
    }

    @RequestMapping(value = "/updatePostClass", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void updatePostClass(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                 @RequestBody String req) throws Exception {

        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(req, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            String message = subscriptionRequest.getMessage();
            reviseManager.updatePostClass(message);
        }
    }

    @RequestMapping(value = "/reloadReviseJeeTestIds", method = RequestMethod.POST)
    @ResponseBody
    public void reloadReviseJeeTestIds(@RequestBody List<String> testIds) throws BadRequestException, InternalServerErrorException {
        reviseManager.reloadReviseJeeTestIds(testIds);
    }

    @RequestMapping(value = "/reloadReviseJeeSessionIds", method = RequestMethod.POST)
    @ResponseBody
    public void reloadReviseJeeSessionIds(@RequestBody List<String> sessionIds) throws BadRequestException, InternalServerErrorException {
        reviseManager.reloadReviseJeeSessionIds(sessionIds);
    }

    @RequestMapping(value = "/getReviseJeeTestIds", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getReviseJeeTestIds() throws BadRequestException {
        return reviseManager.getReviseJeeTestIds();
    }

    @RequestMapping(value = "/getReviseJeeSessionIds", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getReviseJeeSessionIds() throws BadRequestException {
        return reviseManager.getReviseJeeSessionIds();
    }


}
