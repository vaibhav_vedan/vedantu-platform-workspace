package com.vedantu.platform.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VException;
import com.vedantu.leadsquared.pojo.TeacherBlueBookFormPojo;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.TeacherLeadSquaredManager;
import com.vedantu.platform.pojo.leadsquared.LeadActivityReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/leadsquared")
public class LeadSquaredController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadSquaredController.class);
    
    @Autowired
    private LeadSquaredManager leadSquaredManager;
    
    @Autowired
    private TeacherLeadSquaredManager teacherLeadSquaredManager;
    
    @Autowired
    private HttpSessionUtils sessionUtils;
    
    private final Gson gson = new Gson();
    
    private static String PATH_KEY = "crL34D51382lh834937464dmQsW";
	//
	// @RequestMapping(value = "/executeLeadSquaredTask", method =
	// RequestMethod.POST)
	// @ResponseBody
	// public void executeLeadSquaredTask(@RequestBody LeadSquaredRequest req)
	// throws VException {
	// logger.info("ENTRY: " + req);
	// leadSquaredManager.executeAsyncTask(req);
	// }

    @RequestMapping(value = "/createTeacherLeads/{id}", method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void createTeacherLeads(@RequestParam(name = "rawRequest") String req, @PathVariable("id") String id) throws VException, AddressException, IOException, IllegalAccessException, NoSuchFieldException {
    	logger.info("Controller : " + req);
    	if(!id.equals(PATH_KEY)){
    		logger.info("Unauthorized id : "+ id);
    		return;
    	}
    	TeacherBlueBookFormPojo reqPojo = gson.fromJson(req, TeacherBlueBookFormPojo.class);
    	logger.info("LS_TeacherBlueBook : "+ reqPojo.toString());
    	teacherLeadSquaredManager.processJotFormAndCreateLead(reqPojo);
    	logger.info("createTeacherLeads_SUCCESS");
    }

//    @RequestMapping(value = "/checkRequests", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON)
//    @ResponseBody
//    public void checkRequests(MultipartHttpServletRequest req) throws IOException, IllegalArgumentException, IllegalAccessException, VException
//    {
//    	logger.info("LeadSquared_checkRequests_MultipartHttpServletRequest");
//    	if(req != null){
//    		logger.info("LS_ContentType: "+req.getContentType());
//    		logger.info("LS_ContentLength: "+req.getContentLength());
//    		logger.info("LS_ContentType2: "+req.getHeader("Content-Type"));
//    		Enumeration<String> params = req.getParameterNames();
//    		while (params.hasMoreElements()) {
//    	         logger.info(params.nextElement()); 
//    	      }
//    		logger.info("LS_ParameterNames: "+req.getParameterNames());
//    		
//    		logger.info("LS_HeadersName: "+req.getHeaderNames());
//    		Map<String,Object> parameterMaps = req.getParameterMap();
//    		String value = null;
//    		if(!parameterMaps.isEmpty()){
//    			for(String map :parameterMaps.keySet()){
////    				logger.info("temp_LS "+ map.getClass());
////    				logger.info("temp_LS_obj "+ map + " value " + parameterMaps.get(map));
//    				if(map.equalsIgnoreCase("rawRequest")){
//    					Object val = parameterMaps.get(map);
//    					if(val instanceof String){
//    						value = (String) val;
//    					}
//    				}
//    			}
//    		}
//    		if(value!= null){
//    			logger.info("value for teacher : " + value);
//    			TeacherBlueBookFormPojo reqPojo = gson.fromJson(value, TeacherBlueBookFormPojo.class);
//    			logger.info("LS_TeacherBlueBook : "+ reqPojo.toString());
//    			teacherLeadSquaredManager.processJotFormAndCreateLead(reqPojo);
//    			logger.info("checkRequests_SUCCESS");
//    		}
//    		Enumeration<String> attributes = req.getAttributeNames();
//    		while (attributes.hasMoreElements()) {
//    	         logger.info(attributes.nextElement()); 
//    	      }
//    		
//    		logger.info("LS_ContentType: "+req.getContentType());
//    	}
//    	logger.info("Exiting_LS_checkRequest");
//    }    
    
//    @RequestMapping(value = "/pushLeadSquaredUsers", method = RequestMethod.GET)
//    @ResponseBody
//    public void pushLeadSquaredUsers(GetUsersReq req) throws VException,InterruptedException{
//        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), Boolean.TRUE);
//        leadSquaredManager.pushLeadSquaredUsers(req.getFromTime(),req.getTillTime(),req.getSize());
//    }
    
    @RequestMapping(value = "/pushLeadSquaredActivity", method = RequestMethod.POST,consumes =MediaType.APPLICATION_JSON)
    @ResponseBody
    public void pushLeadSquaredActivity(@RequestBody LeadSquaredRequest request) throws VException,InterruptedException{
        
        if(request != null){
            logger.info("Request : leadsquare" + request);
            leadSquaredManager.executeAsyncTask(request);
        }
        
    }
    
    /*@RequestMapping(value = "/pushLeadReqForCoursePlanLess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void tempFunction(@RequestBody QueueMessagePojo queueMessagePojo) throws IllegalArgumentException, IllegalAccessException, IOException, VException, Exception{
        
        leadSquaredManager.handleMessagesFromLeadSquare(Arrays.asList(queueMessagePojo));
        
    }*/

    @RequestMapping(value = "/postLeadSquaredData", method = RequestMethod.POST,consumes =MediaType.APPLICATION_JSON)
    @ResponseBody
    public void postLeadSquaredData(@RequestBody LeadSquaredRequest request) throws VException,InterruptedException{

        if(request != null){
            leadSquaredManager.executeAsyncTask(request);
        }

    }

    @RequestMapping(value = "/createLeadSync", method = RequestMethod.POST,consumes =MediaType.APPLICATION_JSON)
    @ResponseBody
    public void createLeadAsync(@RequestBody LeadSquaredRequest request, HttpServletRequest httpRequest) throws Exception {

            sessionUtils.isAllowedApp(httpRequest);

            leadSquaredManager.leadCreate(request);
    }

    @RequestMapping(value = "/updateGPSLocalesToLSUsingSNS", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateGPSLocalesToLSUsingSNS(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                             @RequestBody String request) throws Exception {
        logger.info("sns controller for updateGPSLocalesToLSUsingSNS with messegeType : "+messageType);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            logger.info("sns payload message : "+subscriptionRequest.getMessage());
            leadSquaredManager.updateGPSLocalesToLSUsingSNS(subscriptionRequest.getMessage());
        }
    }

    @RequestMapping(value = "/requestHomeDemo", method = RequestMethod.POST,consumes =MediaType.APPLICATION_JSON)
    @ResponseBody
    public void requestHomeDemo(@RequestBody LeadSquaredRequest request) throws Exception {

        if(request != null){

            leadSquaredManager.requestHomeDemo(request);
        }

    }

    @RequestMapping(value = "/pushToLsRequestHomeDemo", method = RequestMethod.POST,consumes =MediaType.APPLICATION_JSON)
    @ResponseBody
    public void pushToLsRequestHomeDemo(@RequestBody LeadSquaredRequest request) throws Exception {

        leadSquaredManager.pushToLsRequestHomeDemo(request);

    }


    @RequestMapping(value = "/pushLSActivities", method = RequestMethod.POST,consumes =MediaType.APPLICATION_JSON)
    @ResponseBody
    public void pushToLsRequestHomeDemo(@RequestBody  LeadActivityReq leadActivityReq) throws Exception {

        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        leadSquaredManager.postBulkLeadActivity(Collections.singletonList(leadActivityReq),HttpMethod.POST);

    }

}
