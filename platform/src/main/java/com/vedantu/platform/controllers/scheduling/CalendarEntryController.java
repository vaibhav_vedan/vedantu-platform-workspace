package com.vedantu.platform.controllers.scheduling;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.scheduling.CalendarManager;
import com.vedantu.platform.pojo.scheduling.AvailabilityPojo;
import com.vedantu.platform.pojo.scheduling.AvailabilityResponse;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.pojo.scheduling.CalendarEntry;
import com.vedantu.platform.pojo.scheduling.ESSearchResponse;
import com.vedantu.platform.pojo.scheduling.ElasticSearchResponseParser;
import com.vedantu.platform.pojo.scheduling.EsTeacherInfo;
import com.vedantu.platform.pojo.scheduling.IntervalSlot;
import com.vedantu.platform.pojo.scheduling.RouteScheduling;
import com.vedantu.platform.pojo.scheduling.TeacherAvailabilityData;
import com.vedantu.platform.pojo.scheduling.TrueAvailability;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.RetryCalendarEventReq;
import com.vedantu.session.pojo.OTFCalendarSessionInfo;
import com.vedantu.session.pojo.OTFCalendarSessionInfoRes;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SlotCheckPojo;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Platform Scheduling Controller")
public class CalendarEntryController {

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private CalendarManager calendarManager;

	@Autowired
	private RouteScheduling routeScheduling;

	@Autowired
	private OTFManager otfManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarEntryController.class);

	private static final long SLOT_LENGTH = 900000l;

	@RequestMapping(value = "/calendar/availableTeachersWithFilters", method = RequestMethod.GET)
	@ApiOperation(value = "get Teachers with specific grade,class,subject with availability", notes = "List<EsTeacherData> (es teachers who are available)")
	public List<EsTeacherData> getAvailableTeachersWithFilters(
			@RequestParam(value = "boardId", required = false) String boardId, @RequestParam("endTime") Long endTime,
			@RequestParam("startTime") Long startTime, @RequestParam(value = "grade", required = false) String grade,
			@RequestParam(value = "target", required = false) String target) throws VException,Exception {
		httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		List<EsTeacherData> response = new ArrayList<EsTeacherData>();
		// start and limit??
		String teacherHits = routeScheduling.getTeachersWithFilters(boardId, grade, target, "0", "1000", null);
		ESSearchResponse esSearchResponse = ElasticSearchResponseParser.INSTANCE.parse(teacherHits);

		List<EsTeacherInfo> esTeacherDatas = esSearchResponse.getTeacherData();
		if (esTeacherDatas == null || esTeacherDatas.isEmpty()) {
			// logEntering("getAvailableTeachers", "getAvailableTeachersEmpty no
			// teachers found for req", req);
			return response;
		}

		Map<String, EsTeacherData> teachers = new HashMap<String, EsTeacherData>();
		for (EsTeacherData esTeacherData : esTeacherDatas) {
			teachers.put(String.valueOf(esTeacherData.getTeacherId()), esTeacherData);
		}
		List<String> teacherslist = new ArrayList<String>(teachers.keySet());

		AvailabilityPojo availabilityPojo = new AvailabilityPojo();
		availabilityPojo.setStartTime(startTime);
		availabilityPojo.setEndTime(endTime);
		availabilityPojo.setUserId(teacherslist);
		logger.info("availability pojo is " + availabilityPojo.toString());

		String availabiltyUrl = "/calendarEntry/availableTeachers";

		String params = "?startTime=" + availabilityPojo.getStartTime() + "&endTime=" + availabilityPojo.getEndTime();

		for (String userId : availabilityPojo.getUserId())
			params = params + "&userIds=" + userId;
		ClientResponse availableUsers = WebUtils.INSTANCE.doCall(schedulingEndpoint + availabiltyUrl + params,
				HttpMethod.GET, null);
		String output = availableUsers.getEntity(String.class);
		logger.info("output from available in platform with basics " + output);
		JSONArray jsonArray = new JSONArray(output);
		ObjectMapper objectMapper = new ObjectMapper();
		List<String> userIdsAvailable = objectMapper.readValue(jsonArray.toString(),
				objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));
		// List<EsTeacherData> esTeacherData = availableUsers.getEntity(new
		// GenericType<List<EsTeacherData>>() {
		// });
		List<EsTeacherData> esTeacherData = new ArrayList<EsTeacherData>();
		if (userIdsAvailable.size() > 0)
			for (String userId : userIdsAvailable) {
				esTeacherData.add(teachers.get(userId));
			}
		return esTeacherData;

	}

	@RequestMapping(value = "/calendar/mark", method = RequestMethod.POST)
	@ApiOperation(value = "mark slot states for user between start and end times", notes = "basic res if calendar entries were created")
	public BasicRes markStateForUser(@RequestBody AddCalendarEntryReq markCalendarEntryReq) throws Exception {
		return calendarManager.markCalendarEntries(markCalendarEntryReq);
	}

	@RequestMapping(value = "/calendar/get", method = RequestMethod.GET)
	@ApiOperation(value = "get calendar entries for user between start and end times", notes = "List<CalendarEntry> between start and endtime for user")
	public List<IntervalSlot> getCalendarEntriesForUser(@RequestParam("userId") String userId,
			@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws Exception {

		logger.info("entering CALENDARENTRYGET: userId-" + userId + " startTime-" + startTime + "endTime-" + endTime);
		String getCalendarEntriesUrl = "/calendarEntries";
		String params = "?startTime=" + startTime + "&endTime=" + endTime + "&userId=" + userId;

		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		ClientResponse getCalendarEntries = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEntriesUrl + params, HttpMethod.GET, null);

		String output = getCalendarEntries.getEntity(String.class);
		logger.info("output from get calendar entry in platform  " + output);
		JSONArray jsonArray = new JSONArray(output);
		ObjectMapper objectMapper = new ObjectMapper();
		List<CalendarEntry> calendarEntriesList = objectMapper.readValue(jsonArray.toString(),
				objectMapper.getTypeFactory().constructCollectionType(List.class, CalendarEntry.class));
		List<IntervalSlot> intervalSlots = new ArrayList<IntervalSlot>();

		if (calendarEntriesList == null || calendarEntriesList.isEmpty()) {
			return intervalSlots;
		}

		for (CalendarEntrySlotState slotState : CalendarEntrySlotState.values()) {
			IntervalSlot intervalSlot = null;
			for (CalendarEntry calendarEntry : calendarEntriesList) {
				// logger.info("calendar Entry is " + calendarEntry.toString());
				List<Integer> state = new ArrayList<Integer>();
				switch (slotState) {
				case AVAILABLE:
					state = calendarEntry.getAvailability();
					break;
				case SESSION:
					state = calendarEntry.getBooked();
					break;
				case SESSION_REQUEST:
					state = calendarEntry.getSessionRequest();
					break;
				}

				if (state == null || state.isEmpty()) {
					continue;
				}

				// logger.info("state for get calendaer " + state.toString());
				for (int i = 0; i < 96; i++) {
					if (state.contains(i)) {
						if (intervalSlot != null) {
							intervalSlot.setEndTime(
									calendarEntry.getDayStartTime() + (i + 1) * DateTimeUtils.MILLIS_PER_MINUTE * 15);
							if ((endTime - intervalSlot.getEndTime()) <= DateTimeUtils.MILLIS_PER_MINUTE * 14 || i == 95
									|| i == 73) {
								intervalSlots.add(intervalSlot);
								intervalSlot = null;
							}
						} else {
							intervalSlot = new IntervalSlot(
									calendarEntry.getDayStartTime() + i * DateTimeUtils.MILLIS_PER_MINUTE * 15,
									calendarEntry.getDayStartTime() + (i + 1) * DateTimeUtils.MILLIS_PER_MINUTE * 15,
									slotState);
						}
					} else {
						if (intervalSlot != null) {
							intervalSlots.add(intervalSlot);
							intervalSlot = null;
						}
					}
				}

			}

		}

		logger.info("response of interval slots GET CALENDAR: " + intervalSlots.toString());
		logger.info("CALENDARENTRYGETEXIT");
		return intervalSlots;

	}

	@RequestMapping(value = "/calendar/getUsersTrueAvailabilty", method = RequestMethod.GET)
	@ApiOperation(value = "get calendar entries for user between start and end times", notes = "List<CalendarEntry> between start and endtime for user")
	public List<TrueAvailability> getCalendarEntriesForUser(@RequestParam("userIds") List<String> userIds,
			@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws Exception {

		logger.info("entering get true availability state for teachers");
		String getCalendarEntriesUrl = "/bulkFetchCalendarEntries";
		List<TrueAvailability> trueAvailabilityList = new ArrayList<TrueAvailability>();
		String params = "?startTime=" + startTime + "&endTime=" + endTime;
		for (String userId : userIds) {
			params = params + "&userIds=" + userId;
		}

		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		ClientResponse getCalendarEntries = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEntriesUrl + params, HttpMethod.GET, null);

		String output = getCalendarEntries.getEntity(String.class);
		logger.info("output from get for user in true availability " + output);
		JSONArray jsonArray = new JSONArray(output);
		ObjectMapper objectMapper = new ObjectMapper();
		List<CalendarEntry> calendarEntriesList = objectMapper.readValue(jsonArray.toString(),
				objectMapper.getTypeFactory().constructCollectionType(List.class, CalendarEntry.class));

		Map<String, List<CalendarEntry>> usersAvailability = new HashMap<String, List<CalendarEntry>>();
		List<CalendarEntry> calendarEntries;
		for (CalendarEntry calendarEntry : calendarEntriesList) {
			if (usersAvailability.containsKey(calendarEntry.getUserId())) {
				calendarEntries = usersAvailability.get(calendarEntry.getUserId());
			} else {
				calendarEntries = new ArrayList<CalendarEntry>();

			}
			calendarEntries.add(calendarEntry);
			usersAvailability.put(calendarEntry.getUserId(), calendarEntries);
		}

		for (String userId : usersAvailability.keySet()) {
			calendarEntriesList = usersAvailability.get(userId);
			List<IntervalSlot> intervalSlots = new ArrayList<IntervalSlot>();
			IntervalSlot intervalSlot = null;
			if (calendarEntriesList == null || calendarEntriesList.size() == 0) {
				TrueAvailability trueAvailability = new TrueAvailability(userId, intervalSlots);
				trueAvailabilityList.add(trueAvailability);
				continue;
			}
			for (CalendarEntry calendarEntry : calendarEntriesList) {
				// logger.info("calendar Entry is " + calendarEntry.toString());
				List<Integer> availability = calendarEntry.getAvailability();
				List<Integer> booked = calendarEntry.getBooked();
				List<Integer> sessionRequest = calendarEntry.getSessionRequest();

				for (int i = 0; i < 96; i++) {

					long start = calendarEntry.getDayStartTime() + i * DateTimeUtils.MILLIS_PER_MINUTE * 15;
					long end = calendarEntry.getDayStartTime() + (i + 1) * DateTimeUtils.MILLIS_PER_MINUTE * 15;
					if (availability.contains(i) && booked != null && !booked.contains(i)
							&& !sessionRequest.contains(i)) {
						if (intervalSlot != null) {
							intervalSlot.setEndTime(end);
						} else {
							intervalSlot = new IntervalSlot(start, end, CalendarEntrySlotState.AVAILABLE);
						}
					} else {
						if (intervalSlot != null) {
							if (!(intervalSlot.getStartTime() > endTime || intervalSlot.getEndTime() < startTime)) {
								intervalSlots.add(intervalSlot);
							}
							intervalSlot = null;
						}
					}
				}
			}

			if (intervalSlot != null) {
				if (!(intervalSlot.getStartTime() > endTime || intervalSlot.getEndTime() < startTime)) {
					intervalSlots.add(intervalSlot);
				}
				intervalSlot = null;
			}

			TrueAvailability trueAvailability = new TrueAvailability(userId, intervalSlots);
			trueAvailabilityList.add(trueAvailability);

		}
		logger.info("response of interval slots for tos allocation : " + trueAvailabilityList.toString());
		logger.info("exiting calendar entry available GET");
		return trueAvailabilityList;

	}

	@RequestMapping(value = "/calendar/removeSlots", method = RequestMethod.POST)
	@ApiOperation(value = "remove slot states for user between start and end times", notes = "basic res if slots were removed")
	public BasicRes removeSlotsForUser(@RequestBody AddCalendarEntryReq calendarEntryReq) throws Exception {
		logger.info("entering remove state for slots user");
		return calendarManager.unmarkCalendarEntries(calendarEntryReq);
	}

	@RequestMapping(value = "/calendar/prolongedTrueAvailability", method = RequestMethod.GET)
	@ApiOperation(value = "get prolonged true Availability of teachers", notes = "List<slots> (that are prolonged available to teachers and students)")
	public List<IntervalSlot> getProlongedAvailability(@RequestParam(value = "teacherId") String teacherId,
			@RequestParam("studentId") String studentId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endtime, @RequestParam(value = "repeat") int repeat,
			@RequestParam(value = "match") float match,
			@RequestParam(name = "neglectReferenceId", required = false) String neglectReferenceId,
			HttpServletRequest request) throws Exception {
		logger.info("entering CALENDARENTRYTRUER: teacherId-" + teacherId + "studentId" + studentId + " startTime-"
				+ startTime + "endTime-" + endtime + " repeat-" + repeat);
		return getTruerAvailability(teacherId, studentId, startTime, endtime, neglectReferenceId, request);
	}

	@RequestMapping(value = "/calendar/checkSlotAvailability", method = RequestMethod.GET)
	@ApiOperation(value = "get calendar entries for user between start and end times", notes = "List<CalendarEntry> between start and endtime for user")
	public Boolean checkSlotAvailablilty(@RequestParam("studentIds") List<String> studentIds,
			@RequestParam("teacherIds") List<String> teacherIds, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime) throws Exception {
		if (teacherIds != null && !teacherIds.isEmpty()) {
			AvailabilityPojo availabilityPojo = new AvailabilityPojo();
			availabilityPojo.setStartTime(startTime);
			availabilityPojo.setEndTime(endTime);
			availabilityPojo.setUserId(teacherIds);
			logger.info("availability pojo is " + availabilityPojo.toString());

			String availabiltyUrl = "/calendarEntry/availableTeachers";

			String params = "?startTime=" + availabilityPojo.getStartTime() + "&endTime="
					+ availabilityPojo.getEndTime();

			for (String userId : availabilityPojo.getUserId())
				params = params + "&userIds=" + userId;
			String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
			ClientResponse availableUsers = WebUtils.INSTANCE.doCall(schedulingEndpoint + availabiltyUrl + params,
					HttpMethod.GET, null);
			String output = availableUsers.getEntity(String.class);
			logger.info("output from available in platform with basics " + output);
			JSONArray jsonArray = new JSONArray(output);
			ObjectMapper objectMapper = new ObjectMapper();
			List<String> userIdsAvailable = objectMapper.readValue(jsonArray.toString(),
					objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));
			if (userIdsAvailable == null || userIdsAvailable.size() != teacherIds.size()) {
				return false;
			}
		}

		if (studentIds != null) {
			AvailabilityPojo availabilityPojo = new AvailabilityPojo();
			availabilityPojo.setStartTime(startTime);
			availabilityPojo.setEndTime(endTime);
			availabilityPojo.setUserId(studentIds);
			logger.info("availability pojo is " + availabilityPojo.toString());

			String availabiltyUrl = "/calendarEntry/availableStudents";

			String params = "?startTime=" + availabilityPojo.getStartTime() + "&endTime="
					+ availabilityPojo.getEndTime();

			for (String userId : availabilityPojo.getUserId())
				params = params + "&userIds=" + userId;
			String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
			ClientResponse availableUsers = WebUtils.INSTANCE.doCall(schedulingEndpoint + availabiltyUrl + params,
					HttpMethod.GET, null);
			String output = availableUsers.getEntity(String.class);
			logger.info(" output from available in platform with basics " + output);
			JSONArray jsonArray = new JSONArray(output);
			ObjectMapper objectMapper = new ObjectMapper();
			List<String> userIdsAvailable = objectMapper.readValue(jsonArray.toString(),
					objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));
			if (userIdsAvailable == null || userIdsAvailable.size() != studentIds.size()) {
				return false;
			}
		}

		return true;
	}

	@RequestMapping(value = "/calendar/checkSlotListAvailability", method = RequestMethod.POST)
	@ApiOperation(value = "get calendar entries for user between start and end times", notes = "List<CalendarEntry> between start and endtime for user")
	public List<SessionSlot> checkSlotAvailablilty(@RequestBody SlotCheckPojo slotCheckList) throws Exception {
		httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN,Role.TEACHER) , Boolean.TRUE);
		return calendarManager.checkSlotAvailablilty(slotCheckList);
	}

	@RequestMapping(value = "/calendar/getTrueAvailability", method = RequestMethod.GET)
	@ApiOperation(value = "get prolonged true Availability of teachers", notes = "List<slots> (that are prolonged available to teachers and students)")
	public List<IntervalSlot> getTrueAvailability(@RequestParam(value = "teacherId") String teacherId,
			@RequestParam("studentId") String studentId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endtime,
			@RequestParam(name = "neglectReferenceId", required = false) String neglectReferenceId,
			HttpServletRequest request) throws Exception {

		String queryString = request.getQueryString();
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String slotListAvailabiltyUrl = "/calendarEntry/trueAvailability?" + queryString;
		ClientResponse clientResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + slotListAvailabiltyUrl,
				HttpMethod.GET, null);
		String output = clientResponse.getEntity(String.class);
		logger.info(" output from  checkslotavailability in platform with basics " + output);
		try {
			AvailabilityResponse availabilityResponse = new Gson().fromJson(output, AvailabilityResponse.class);
			List<IntervalSlot> resultSlots = new ArrayList<IntervalSlot>();
			Integer[] trueAvailability = availabilityResponse.getTrueAvailability();
			startTime = availabilityResponse.getStartTime();

			for (int i = 0; i < trueAvailability.length; i++) {
				int startIndex = CommonUtils.getNextSetIndex(trueAvailability, i);
				if (startIndex < 0) {
					break;
				}

				int endIndex = CommonUtils.getNextClearIndex(trueAvailability, startIndex);
				resultSlots.add(new IntervalSlot(startTime + (startIndex * SLOT_LENGTH),
						startTime + (endIndex * SLOT_LENGTH), CalendarEntrySlotState.AVAILABLE));
				i = endIndex;
			}
			return resultSlots;
		} catch (Exception ex) {
			logger.error("getTrueAvailabilityError parsing the json : " + output);
			throw ex;
		}
	}

	@RequestMapping(value = "/calendar/getTruerAvailability", method = RequestMethod.GET)
	@ApiOperation(value = "get prolonged true Availability of teachers", notes = "List<slots> (that are prolonged available to teachers and students)")
	public List<IntervalSlot> getTruerAvailability(@RequestParam(value = "teacherId") String teacherId,
			@RequestParam("studentId") String studentId, @RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endtime,
			@RequestParam(name = "neglectReferenceId", required = false) String neglectReferenceId,
			HttpServletRequest request) throws Exception {

		String queryString = request.getQueryString();
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String slotListAvailabiltyUrl = "/calendarEntry/truerAvailability?" + queryString;
		ClientResponse clientResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + slotListAvailabiltyUrl,
				HttpMethod.GET, null);
		String output = clientResponse.getEntity(String.class);
		logger.info(" output from  checkslotavailability in platform with basics " + output);
		try {
			AvailabilityResponse availabilityResponse = new Gson().fromJson(output, AvailabilityResponse.class);
			List<IntervalSlot> resultSlots = new ArrayList<IntervalSlot>();
			Integer[] trueAvailability = availabilityResponse.getTrueAvailability();
			startTime = availabilityResponse.getStartTime();

			for (int i = 0; i < trueAvailability.length; i++) {
				int startIndex = CommonUtils.getNextSetIndex(trueAvailability, i);
				if (startIndex < 0) {
					break;
				}

				int endIndex = CommonUtils.getNextClearIndex(trueAvailability, startIndex);
				resultSlots.add(new IntervalSlot(startTime + (startIndex * SLOT_LENGTH),
						startTime + (endIndex * SLOT_LENGTH), CalendarEntrySlotState.AVAILABLE));
				i = endIndex;
			}

			return resultSlots;
		} catch (Exception ex) {
			logger.error("getTrueAvailabilityError parsing the json : " + output);
			throw ex;
		}
	}

	@Deprecated
	@RequestMapping(value = "/calendar/freeBulkSlots", method = RequestMethod.POST)
	@ApiOperation(value = "free list of slots together", notes = " true if freed all")
	public String freeBulkSlots(@RequestBody String freeSlotsBody) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String freeBulkSlotsUrl = "/calendarEntry/freeBulkSlots";
		ClientResponse freeSlotsResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + freeBulkSlotsUrl,
				HttpMethod.POST, freeSlotsBody);
		String output = freeSlotsResponse.getEntity(String.class);
		logger.info(" output from  freeing bulk slots " + output);
		return output;

	}

	@RequestMapping(value = "/calendar/weightedTruerAvailability", method = RequestMethod.GET)
	@ApiOperation(value = "get truer Availability of teacher", notes = "weighted truer availability (that are prolonged available to teachers and students)")
	public Double getWeightedTruerAvailability(@RequestParam(value = "userId") String userId,
			@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endtime,
			@RequestParam(value = "repeat") int repeat, @RequestParam(value = "match") String matchString)
			throws Exception {

		return calendarManager.getWeightedTruerAvailability(userId, startTime, endtime, repeat, matchString);
	}

	@RequestMapping(value = "/calendar/ThrowSentryError", method = RequestMethod.POST)
	@ApiOperation(value = "throw sentry error from fos", notes = " throw sentry error from fos")
	public void throwSentryError(@RequestBody String request) throws Exception {
		logger.error("fos error recorded in platform : " + request);
	}

	// calendar/setWeightedAvailabilityRanges
	@Deprecated
	@RequestMapping(value = "/calendar/getWeightedAvailabilityRanges", method = RequestMethod.GET)
	@ApiOperation(value = "get availability ranges with weights", notes = " whtever on the left")
	public String getAvailabilityRanges() throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getWeightedAvailabilityRangesUrl = "/getWeightedAvailabilityRanges";
		ClientResponse response = WebUtils.INSTANCE.doCall(schedulingEndpoint + getWeightedAvailabilityRangesUrl,
				HttpMethod.GET, null);
		String output = response.getEntity(String.class);
		return output;
	}

	@RequestMapping(value = "/calendar/updateSlotBits", method = RequestMethod.POST)
	@ApiOperation(value = "Update calendar slots via bitset", notes = "")
	public String updateSlotBits(@RequestBody String updateSlotBitsReq) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String updateSlotBitsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.updateslotbits");
		logger.info("updateSlotBitsRequest : " + updateSlotBitsReq);
		ClientResponse updateSlotBitsResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + updateSlotBitsUrl,
				HttpMethod.POST, updateSlotBitsReq);
		String output = updateSlotBitsResponse.getEntity(String.class);
		logger.info("updateSlotBitsResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getSlotBits", method = RequestMethod.GET)
	@ApiOperation(value = "Get slots in bitset", notes = "")
	public String getSlotBits(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getSlotBitsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.getslotbits");
		String queryString = request.getQueryString();
		logger.info("getSlotBitsRequest : " + queryString);
		ClientResponse getSlotBitsResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getSlotBitsUrl + "?" + queryString, HttpMethod.GET, getSlotBitsUrl);
		String output = getSlotBitsResponse.getEntity(String.class);
		logger.info("getSlotBitsResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/updateSlotString", method = RequestMethod.POST)
	@ApiOperation(value = "Update calendar slots via bitset", notes = "")
	public String updateSlotString(@RequestBody String updateSlotStringReq) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String updateSlotStringUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.updateslotstring");
		logger.info("updateSlotStringRequest : " + updateSlotStringReq);
		ClientResponse updateSlotStringResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + updateSlotStringUrl,
				HttpMethod.POST, updateSlotStringReq);
		String output = updateSlotStringResponse.getEntity(String.class);
		logger.info("updateSlotStringResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getSlotString", method = RequestMethod.GET)
	@ApiOperation(value = "Get slots in bitset", notes = "")
	public String getSlotString(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getSlotStringUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.getslotstring");
		String queryString = request.getQueryString();
		logger.info("getSlotStringRequest : " + queryString);
		ClientResponse getSlotStringResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getSlotStringUrl + "?" + queryString, HttpMethod.GET, getSlotStringUrl);
		String output = getSlotStringResponse.getEntity(String.class);
		logger.info("getSlotStringResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/cancelEvents", method = RequestMethod.POST)
	@ApiOperation(value = "Cancel calendar events", notes = "")
	public String cancelCalendarEvents(@RequestBody String cancelCalendarEventsReq) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String cancelCalendarEventUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.cancelevents");
		logger.info("cancelCalendarEvents : " + cancelCalendarEventsReq);
		ClientResponse cancelCalendarEventResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + cancelCalendarEventUrl, HttpMethod.POST, cancelCalendarEventsReq);
		String output = cancelCalendarEventResponse.getEntity(String.class);
		logger.info("cancelCalendarEvents : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getEvents", method = RequestMethod.GET)
	@ApiOperation(value = "Get calendar events", notes = "")
	public String getCalendarEvents(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarEventsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.getevents");
		String queryString = request.getQueryString();
		logger.info("getSlotStringRequest : " + queryString);
		ClientResponse getSlotStringResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEventsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSlotStringResponse.getEntity(String.class);
		logger.info("getSlotStringResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/retryEvents", method = RequestMethod.GET)
	@ApiOperation(value = "retry calendar events", notes = "")
	public String retryCalendarEvents(RetryCalendarEventReq request) throws Exception {
		return calendarManager.retryCalendarEvents(request);
	}

	@RequestMapping(value = "/calendar/setWeightedAvailabilityRanges", method = RequestMethod.POST)
	@ApiOperation(value = "set availability ranges with weights", notes = " whtever on the left")
	public String setAvailabilityRanges(@RequestBody String request) throws Exception {

		logger.error("set availabiltiy ranges : " + request);
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String setWeightedAvailabilityRangesUrl = "/setWeightedAvailabilityRanges";
		ClientResponse response = WebUtils.INSTANCE.doCall(schedulingEndpoint + setWeightedAvailabilityRangesUrl,
				HttpMethod.POST, request);
		String output = response.getEntity(String.class);
		return output;
	}

	public static List<Integer> createArrayList(int start, int end) {
		List<Integer> list = new ArrayList<>();
		for (int i = start; i <= end; i++) {
			list.add(i);
		}

		return list;
	}

	@RequestMapping(value = "/calendar/blockSlotSchedule", method = RequestMethod.POST)
	@ApiOperation(value = "Resolve conflict for the provided request", notes = "")
	public String blockSlotSchedule(@RequestBody String req) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String updateSlotStringUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.blockSlotSchedule");
		logger.info("updateSlotStringRequest : " + req);
		ClientResponse updateSlotStringResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + updateSlotStringUrl,
				HttpMethod.POST, req);
		String output = updateSlotStringResponse.getEntity(String.class);
		logger.info("updateSlotStringResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/syncCalendarSessions", method = RequestMethod.POST)
	@ApiOperation(value = "Resolve conflict for the provided request", notes = "")
	public String syncCalendarSessions(@RequestBody String req) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String updateSlotStringUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.syncCalendarSessions");
		logger.info("syncCalendarSessions : " + req);
		ClientResponse syncCalendarSessionsResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + updateSlotStringUrl,
				HttpMethod.POST, req);
		String output = syncCalendarSessionsResponse.getEntity(String.class);
		logger.info("syncCalendarSessions : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getLastMarkedDay", method = RequestMethod.GET)
	@ApiOperation(value = "Get last marked day", notes = "")
	public String getLastMarkedDay(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarEventsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.get.last.marked.day");
		String queryString = request.getQueryString();
		logger.info("getSlotStringRequest : " + queryString);
		ClientResponse getSlotStringResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEventsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSlotStringResponse.getEntity(String.class);
		logger.info("getSlotStringResponse : " + output);
		return output;
	}

	// migrated to /getBlockEntrySlotsInfo in scheduling
	@Deprecated
	@RequestMapping(value = "/calendar/getBlockEntrySlots", method = RequestMethod.GET)
	@ApiOperation(value = "Get last marked day", notes = "")
	public String getBlockEntrySlots(@RequestParam(name = "userId", required = true) String userId,
			@RequestParam(name = "startTime", required = true) Long startTime,
			@RequestParam(name = "endTime", required = true) Long endTime) throws VException {
		logger.info("getBlockEntrySlots : userId - " + userId + " startTime - " + startTime + " endTime - " + endTime);
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarEventsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.get.block.entry.slots");

		HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
		Role role;
		if (httpSessionData != null && httpSessionData.getUserId().toString().equals(userId)) {
			role = httpSessionData.getRole();
		} else {
			UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
			if (userBasicInfo == null) {
				throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found");
			} else {
				role = userBasicInfo.getRole();
			}
		}
		String queryString = "";
		switch (role) {
		case TEACHER:
			queryString += "teacherId=" + userId;
			break;
		default:
			queryString += "studentId=" + userId;
			break;
		}

		queryString += "&startTime=" + startTime + "&endTime=" + endTime;
		ClientResponse getSlotStringResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEventsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSlotStringResponse.getEntity(String.class);
		logger.info("getSlotStringResponse : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getSelectedSlots", method = RequestMethod.GET)
	@ApiOperation(value = "Get selected slots", notes = "")
	public String getSelectedSlots(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarSelectedSlotsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.get.selected.slots");
		String queryString = request.getQueryString();
		logger.info("getSelectedSlots : " + queryString);
		ClientResponse getSelecetedSlotResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarSelectedSlotsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSelecetedSlotResponse.getEntity(String.class);
		logger.info("getSelectedSlots : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getSuggestedSlots", method = RequestMethod.GET)
	@ApiOperation(value = "Get selected slots", notes = "")
	public String getSuggestedSlots(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarSuggestedSlotsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.get.suggested.slots");
		String queryString = request.getQueryString();
		logger.info("getSelectedSlots : " + queryString);
		ClientResponse getSuggestedSlotResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarSuggestedSlotsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSuggestedSlotResponse.getEntity(String.class);
		logger.info("getSelectedSlots : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getAvailableTeachers", method = RequestMethod.GET)
	@ApiOperation(value = "Get available teachers", notes = "")
	public String getAvailableTeachers(HttpServletRequest request) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarSelectedSlotsUrl = ConfigUtils.INSTANCE
				.getStringValue("scheduling.api.get.available.teachers");
		String queryString = request.getQueryString();
		logger.info("getAvailableTeachers : " + queryString);
		ClientResponse getSelecetedSlotResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarSelectedSlotsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSelecetedSlotResponse.getEntity(String.class);
		logger.info("getAvailableTeachers : " + output);
		return output;
	}

	@RequestMapping(value = "/calendar/getTeacherAvailabilityBitSet", method = RequestMethod.GET)
	@ApiOperation(value = "get Teachers with specific grade,class,subject with availability", notes = "List<EsTeacherData> (es teachers who are available)")
	public List<TeacherAvailabilityData> getTeacherAvailabilityExport(
			@RequestParam(value = "boardId", required = false) String boardId, @RequestParam("endTime") Long endTime,
			@RequestParam("startTime") Long startTime, @RequestParam(value = "grade", required = false) String grade,
			@RequestParam(value = "target", required = false) String target,
			@RequestParam(value = "teacherId", required = false) String reqTeacherId) throws Exception {

		return calendarManager.getTeacherAvailabilityExport(boardId, endTime, startTime, grade, target, reqTeacherId);
	}

	@RequestMapping(value = "/calendar/retryCalendarEvents", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void retryCalendarEvents(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			calendarManager.retryCalendarEvents();
		}
	}

	@RequestMapping(value = "/calendarEntry/syncNextWeekCalendar", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void syncNextWeekCalendar(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			calendarManager.syncNextWeekCalendar();
		}
	}

	@RequestMapping(value = "/calendarEntry/syncNextMonthCalendar", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void syncNextMonthCalendar(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			calendarManager.syncNextMonthCalendar();
		}
	}

	@RequestMapping(value = "/calendarEntry/syncNextOneYearCalendar", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void syncNextOneYearCalendar(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			calendarManager.syncNextOneYearCalendar();
		}
	}

	@RequestMapping(value = "/calendarEntry/syncCalendarLastUpdated", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void syncCalendarLastUpdated(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			// String subject = subscriptionRequest.getSubject();
			// String message = subscriptionRequest.getMessage();
			calendarManager.syncCalendarLastUpdated();
		}
	}

	// migrated to onetofew/session/v2/getOTFSessionsForCalendar
	@Deprecated
	@RequestMapping(value = "/calendar/getOTFSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public OTFCalendarSessionInfoRes getOTFSessions(HttpServletRequest request) throws Exception {
		String queryString = request.getQueryString();
		logger.info("queryString - " + queryString);

		String beforeStartTime = request.getParameter("beforeStartTime");
		String afterStartTime = request.getParameter("afterStartTime");
		String afterEndTime = request.getParameter("afterEndTime");
		String beforeEndTime = request.getParameter("beforeEndTime");
		String userId = request.getParameter("userId");
                String query = request.getParameter("query");

		HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
		Long requestUserId = sessionData.getUserId();
		Role requestUserRole = sessionData.getRole();
		if (Role.ADMIN.equals(requestUserRole) && !StringUtils.isEmpty(userId)) {
			requestUserId = Long.parseLong(userId);

			// Update role
			UserBasicInfo requestUser = fosUtils.getUserBasicInfo(requestUserId, false);
			requestUserRole = requestUser.getRole();
		}
		List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfManager.getCalendarSessions(beforeStartTime,
				afterStartTime, beforeEndTime, afterEndTime, requestUserId, requestUserRole, query);
		return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
				(oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);
	}

}
