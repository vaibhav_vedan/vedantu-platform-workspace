package com.vedantu.platform.controllers.askadoubt;

import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.enums.askadoubt.DoubtStatus;
import com.vedantu.platform.managers.askadoubt.AskADoubtManager;
import com.vedantu.platform.request.AddDoubtRequest;
import com.vedantu.platform.pojo.askadoubt.DoubtInfo;
import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("/askadoubt")
public class AskADoubtController {

	@Autowired
	private AskADoubtManager askADoubtManager;

        @Autowired
        private LogFactory logFactory;
        
	Logger logger = logFactory.getLogger(RequestCallbackDao.class);

	@ApiOperation(value = "get Doubts", notes = "Returns doubts")
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<DoubtInfo> getDoubts(
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "status", required = false) DoubtStatus status,
			@RequestParam(value = "assignedTo", required = false) Long assignedTo,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer size)
			throws Exception {
		logger.info("request received for getting doubts for : UserId : "
				+ userId + ",status:" + status + ", assigned to : "
				+ assignedTo);
		try {
			List<DoubtInfo> doubtList = askADoubtManager.getDoubts(id, userId,
					status, assignedTo, start, size);
			logger.info("Result:" + doubtList.toString());
			return doubtList;
		} catch (Exception e) {
			logger.error("Failed to get doubts for : UserId : " + userId
					+ ",status:" + status + ", assigned to : " + assignedTo
					+ ". Reason: " + e.getMessage());
			throw new VException(ErrorCode.SERVICE_ERROR,
					"Failed to get doubts for : UserId : " + userId
							+ ",status:" + status + ", assigned to : "
							+ assignedTo + ". Reason: " + e.getMessage());
		}
	}

	@ApiOperation(value = "add Doubt", notes = "Returns created doubts")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public DoubtInfo addDoubts(@RequestBody AddDoubtRequest addDoubtRequest)
			throws Exception {
		logger.info("request received for adding doubt for : "
				+ addDoubtRequest.toString());
		try {
			DoubtInfo doubtInfo = askADoubtManager.addDoubts(addDoubtRequest);
			logger.info("Result:" + doubtInfo.toString());
			return doubtInfo;
		} catch (Exception e) {
			logger.error("Failed to add doubt for :"
					+ addDoubtRequest.toString() + ". Reason: "
					+ e.getMessage());
			throw new VException(ErrorCode.SERVICE_ERROR,
					"Failed to add doubt for :" + addDoubtRequest.toString()
							+ ". Reason: " + e.getMessage());
		}
	}

	@ApiOperation(value = "mark Doubt solution Submission", notes = "mark Doubt solution Submission")
	@RequestMapping(value = "/markDoubtSolutionSubmitted/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public Boolean markDoubtSubmission(@PathVariable("id") String doubtId,
			@RequestParam("callingUserId") Long callingUserId) throws Exception {
		logger.info("request received for marking Doubt solution Submission  Doubt Id : "
				+ doubtId);

		try {
			Boolean result = askADoubtManager.markDoubtSolutionSubmission(
					doubtId, callingUserId);
			logger.info("Result:" + result);
			return result;
		} catch (Exception e) {
			logger.error("Failed to mark solution submission doubt for :"
					+ doubtId + ". Reason: " + e.getMessage());
			throw new VException(ErrorCode.SERVICE_ERROR,
					"Failed to mark solution submission for :" + doubtId
							+ ". Reason: " + e.getMessage());
		}
	}

	@ApiOperation(value = "Get allowed Boards", notes = "Get allowed Boards")
	@RequestMapping(value = "/boards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public String getAllowedBoards() throws Exception {
		logger.info("request received for getting allowed boards ");
		JsonElement jsonElement = new Gson().fromJson(
				                    ConfigUtils.INSTANCE.getStringValue("askadoubt.boards"),
				JsonObject.class);
		return jsonElement.toString();
	}

}
