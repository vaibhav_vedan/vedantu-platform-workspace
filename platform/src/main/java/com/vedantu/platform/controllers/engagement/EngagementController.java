package com.vedantu.platform.controllers.engagement;

import java.net.UnknownHostException;

import javax.mail.internet.AddressException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.engagement.cron.AsynchronousEnagement;
import com.vedantu.platform.engagement.cron.EngagementCron;
import com.vedantu.platform.engagement.cron.PopulateFields;
import com.vedantu.platform.dao.engagement.serializers.EngagementDAO;
import com.vedantu.platform.engagement.graph.manager.GraphManager;
import com.vedantu.platform.engagement.graph.response.GraphResponse;
import com.vedantu.platform.engagement.migration.TeacherPercentileCron;
import com.vedantu.platform.engagement.migration.VedantuAverageManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

//
@RestController
@EnableWebMvc
public class EngagementController {

    @Autowired
    private EngagementDAO engagementDAO;
    //
    @Autowired
    private PopulateFields populateFields;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private AsynchronousEnagement asynchronousEnagement;

    @Autowired
    private EngagementCron engagementCron;

    @Autowired
    private GraphManager graphManager;

    @Autowired
    private VedantuAverageManager migration;

    @Autowired
    private TeacherPercentileCron teacherPercentileCron;


    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EngagementController.class);

    @RequestMapping(value = "getEngagementAnalysis", method = RequestMethod.GET)
    @ResponseBody
    public String getEngagementRequest(@RequestParam("sessionId") Long id) throws Exception {
        logger.info("Request received for getting Engagement for sessionId: " + id);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        EngagementAnalysis engagementAnalysis = engagementDAO.getById(Long.toString(id));
        if (engagementAnalysis == null) {
            logger.info("Session Engagement value not yet populated in the DB");
            asynchronousEnagement.calculateEngagementAsync(id);
            return null;
        } else {
            logger.info("value present in the db");
            populateFields.roundoffValues(engagementAnalysis);
            Gson gson = new Gson();
            String json = gson.toJson(engagementAnalysis);
            return json;
        }

    }

    @RequestMapping(value = "getEngagementReport", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void getDailyReport(@RequestParam("time") Long time, @RequestParam(value = "userId", required = false) Boolean userId, @RequestParam(value = "average", required = false) Boolean average, @RequestHeader(value = "x-amz-sns-message-type") String messgaetype, @RequestBody String request) throws UnknownHostException, AddressException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        logger.info("Request received for getting EngagementDailyReport");
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);

        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info(messgaetype);

            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));

        } else if (messgaetype.equals("Notification")) {
            logger.info("it is a " + messgaetype + ", calling cron for a " + time + " day report ....");
            Boolean id = null;
            Boolean avg = null;
            if (userId != null) {
                id = userId;
            }
            if (average != null) {
                avg = average;
            }
            engagementCron.getEngagementValues(time, id, avg);
        }
    }

    @RequestMapping(value = "getTeacherReportFromTo", method = RequestMethod.GET)
    @ResponseBody
    public void getTeacherReportFromTo(@RequestParam(value = "teacherId", required = false) Long teacherId, @RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws AddressException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        logger.info("Request Received for getting Teacher Average Report");
        engagementCron.getTeacherAverageEngagementValue(teacherId, startTime, endTime);
    }

    @RequestMapping(value = "getEngagementReportFromTo", method = RequestMethod.GET)
    @ResponseBody
    public void getEngagementReportFromTo(@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) throws AddressException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        logger.info("Request Received for getting Engagement Total Report");
        engagementCron.getEngagementValueTotal(startTime, endTime);
    }

    @RequestMapping(value = "getTeacherFacingGraph", method = RequestMethod.GET)
    @ResponseBody
    public GraphResponse getTeacherFacingGraph(@RequestParam("userId") Long userId, @RequestParam("subject") String subject, @RequestParam(value = "currentTime", required = false) Long currentTime) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, VException {
        sessionUtils.checkIfAllowed(userId, null, Boolean.TRUE);
        Long systemCurrentTime;
        if(currentTime==null){
            systemCurrentTime=System.currentTimeMillis();
        }else{
            systemCurrentTime=currentTime;
        }
        GraphResponse graphResponse = graphManager.computeTeacherVsVedantuGraph(userId, subject, systemCurrentTime);
        return graphResponse;
    }

    @RequestMapping(value = "getVedantuDailyMigration", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void getVedantuDailyMigration(@RequestBody String request, @RequestHeader(value = "x-amz-sns-message-type") String messgaetype) throws AddressException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            logger.info("SubscriptionConfirmation received for getVedantuDailyMigration");
            String json = null;
            logger.info(messgaetype);

            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));

        } else if (messgaetype.equals("Notification")) {
            logger.info("Request received for getting Vedantu Daily Average");
            migration.callVedantuDailyMigration();
        }
    }

    @RequestMapping(value = "getTeacherPercentile", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void getTeacherPercentile(@RequestBody String request, @RequestHeader(value = "x-amz-sns-message-type") String messgaetype) throws AddressException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            logger.info("SubscriptionConfirmation received for getTeacherPercentile");
            String json = null;
            logger.info(messgaetype);

            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));

        } else if (messgaetype.equals("Notification")) {
            logger.info("Request received for getting teacher percentile");
            teacherPercentileCron.callTeacherPercentile();
        }
    }
//	@RequestMapping(value = "getSubjectMigration", method = RequestMethod.GET)
//	@ResponseBody
//	public void getSubjectMigration(@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime)
//	{
//		logger.info("Request Received for migration");
//		//migration.callMigration(startTime,endTime);
//		subjectMigration.convertSubjectToLowerCase(startTime, endTime);
//	}
}
