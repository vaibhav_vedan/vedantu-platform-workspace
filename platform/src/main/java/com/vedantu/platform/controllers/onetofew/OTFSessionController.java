package com.vedantu.platform.controllers.onetofew;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.*;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.GetOTFUpcomingSessionReq;
import com.vedantu.onetofew.pojo.HandleOTFRecordingLambda;
import com.vedantu.onetofew.request.AddOTFSessionsReq;
import com.vedantu.onetofew.request.AddUpdateSessionContentReq;
import com.vedantu.onetofew.request.GetOTFSessionsReq;
import com.vedantu.onetofew.request.OTFJoinSessionReq;
import com.vedantu.onetofew.request.UpdateReplayUrlReq;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.onetofew.pojo.OTFUpdateSessionRes;
import com.vedantu.onetofew.request.AddRemoveBatchIdsToSessionReq;
import com.vedantu.onetofew.response.AddRemoveBatchIdsToSessionRes;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.onetofew.OTFAsyncTaskManager;
import com.vedantu.platform.request.GetSessionsReq;
import com.vedantu.session.pojo.OTFCalendarSessionInfo;
import com.vedantu.session.pojo.OTFCalendarSessionInfoRes;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.session.pojo.OTFSessionPojoUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("onetofew/session")
public class OTFSessionController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private OTFAsyncTaskManager oTFAsyncTaskManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFSessionController.class);

    private String schedulingEndPoint;

    private static Gson gson = new Gson();

    private static final Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
    }.getType();

    private String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    @PostConstruct
    public void init() {
        schedulingEndPoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT") + "/onetofew/";
    }

    @Deprecated
    @RequestMapping(value = "/joinSession", method = RequestMethod.GET)
    @ResponseBody
    public String joinSession(@ModelAttribute OTFJoinSessionReq otfJoinSessionReq)
            throws VException, IOException, IllegalArgumentException, IllegalAccessException {
        return _getJoinSessionUrl(otfJoinSessionReq);
    }

    @Deprecated
    @RequestMapping(value = "/getJoinSessionUrl", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getJoinSessionUrl(@ModelAttribute OTFJoinSessionReq otfJoinSessionReq)
            throws VException, IOException, IllegalArgumentException, IllegalAccessException {
        PlatformBasicResponse res=new PlatformBasicResponse();
        res.setResponse(_getJoinSessionUrl(otfJoinSessionReq));
        return res;
    }
    
    @Deprecated
    private String _getJoinSessionUrl(@ModelAttribute OTFJoinSessionReq otfJoinSessionReq)
            throws VException, IOException, IllegalArgumentException, IllegalAccessException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        Long callingUserId = sessionData.getUserId();
        Role callingUserRole = sessionData.getRole();

        if (!Role.ADMIN.equals(callingUserRole)) {
            otfJoinSessionReq.setUserId(String.valueOf(callingUserId));
            otfJoinSessionReq.setRole(callingUserRole);
            otfJoinSessionReq.setAdmin(false);
        } else {
            if (StringUtils.isEmpty(otfJoinSessionReq.getUserId())) {
                otfJoinSessionReq.setUserId(String.valueOf(callingUserId));
            }

            User user = userManager.getUserById(Long.parseLong(otfJoinSessionReq.getUserId()));
            otfJoinSessionReq.setRole(user.getRole());
            otfJoinSessionReq.setAdmin(true);
        }

        String joinSessionsUrl = schedulingEndPoint + "session/joinSession?"
                + WebUtils.INSTANCE.createQueryStringOfObject(otfJoinSessionReq);
        ClientResponse joinSessionResp = WebUtils.INSTANCE.doCall(joinSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(joinSessionResp);
        String jsonString = joinSessionResp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojoUtils> getSessions(HttpServletRequest request) throws IOException, VException {
        return otfManager.getSessions(request);
    }

    @Deprecated
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public OTFSessionPojoUtils getSessions(@PathVariable("id") String id) throws VException {
        return otfManager.getSessionById(id);
    }

    @Deprecated
    @RequestMapping(value = "/createLink/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String createSessionLinks(@PathVariable("id") String id) throws VException {
        return otfManager.createSessionLinks(id);
    }

    @Deprecated
    @RequestMapping(value = "/setWhiteboardDataMigrated/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String setWhiteboardDataMigrated(@PathVariable("id") String id) throws VException {
        logger.info("Request:" + id);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/setWhiteboardDataMigrated/" + id,
                HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/setVimeoVideoId/{id}", method = RequestMethod.POST)
    @ResponseBody
    public String setVimeoVideoId(@PathVariable("id") String id, HttpServletRequest request) throws VException, IOException {
        String requestBody = PlatformTools.getBody(request);
        logger.info("Request body : " + requestBody);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/setVimeoVideoId/" + id,
                HttpMethod.POST, requestBody);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/addBatchSession", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse addBatchSession(HttpServletRequest request) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String requestBody = PlatformTools.getBody(request);
        logger.info("Request body : " + requestBody);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/addBatchSession", HttpMethod.POST,
                requestBody);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return new PlatformBasicResponse();
    }

    @Deprecated
    @RequestMapping(value = "/cancelSession", method = RequestMethod.POST)
    @ResponseBody
    public String cancelSession(HttpServletRequest request) throws Exception {
        String requestBody = PlatformTools.getBody(request);
        logger.info("Request body : " + requestBody);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/cancelSession", HttpMethod.POST,
                requestBody);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        // Update calendar
        OTFUpdateSessionRes updateSessionRes = gson.fromJson(jsonString, OTFUpdateSessionRes.class);
        Map<String, Object> payload = new HashMap<>();
        payload.put("updateSessionRes", updateSessionRes);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_SESSION_CANCEL_TASK, payload);
        asyncTaskFactory.executeTask(params);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/endedSessions", method = RequestMethod.GET)
    @ResponseBody
    public String getEndedSessions(HttpServletRequest request) throws IOException, VException {
        String queryString = request.getQueryString();
        logger.info("queryString - " + queryString);
        String getEndedSessionsUrl = schedulingEndPoint + "session/endedSessions";
        if (!StringUtils.isEmpty(queryString)) {
            getEndedSessionsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEndedSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/endedSessionDuration", method = RequestMethod.GET)
    @ResponseBody
    public Long getEndedSessionDuration(@RequestParam(name = "afterEndTime", required = false) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = false) Long beforeEndTime)
            throws VException {
        return otfManager.getEndedSessionDuration(afterEndTime, beforeEndTime);
    }

    @Deprecated
    @RequestMapping(value = "/getUpcomingSessions", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojoUtils> getUpcomingSessions(@ModelAttribute GetOTFUpcomingSessionReq getOTFUpcomingSessionReq)
            throws IOException, VException, IllegalArgumentException, IllegalAccessException {
        logger.info("Request : " + getOTFUpcomingSessionReq.toString());
        return otfManager.getUpcomingSessions(getOTFUpcomingSessionReq);
    }

    @Deprecated
    @RequestMapping(value = "/getPastSessions", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojoUtils> getPastSessions(@ModelAttribute GetOTFSessionsReq getOTFSessionsReq)
            throws IOException, VException, IllegalArgumentException, IllegalAccessException {
        // Only allowed for ADMIN
        logger.info("Request : " + getOTFSessionsReq.toString());
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.ADMIN);
        allowedRoles.add(Role.STUDENT_CARE);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);

        return otfManager.getPastSessions(getOTFSessionsReq, true);
    }

    @Deprecated
    @RequestMapping(value = "/getUserPastSessions", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojoUtils> getUserPastSessions(@ModelAttribute GetOTFSessionsReq getOTFSessionsReq)
            throws IOException, VException, IllegalArgumentException, IllegalAccessException {
        // Only allowed for ADMIN
        logger.info("Request : " + getOTFSessionsReq.toString());
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.STUDENT);
        allowedRoles.add(Role.TEACHER);
        sessionUtils.checkIfAllowedList(null, allowedRoles, true);
        return otfManager.getUserPastSessions(getOTFSessionsReq);
    }

    @Deprecated
    @RequestMapping(value = "/otfPresenterLink", method = RequestMethod.GET)
    @ResponseBody
    public String getPresenterLink(HttpServletRequest request) throws IOException, VException {
        String getSessionsUrl = schedulingEndPoint + "session/joinSession?sessionId=" + request.getParameter("sessionId")
                + "&role=" + com.vedantu.User.Role.TEACHER.name();

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String sessionString = resp.getEntity(String.class);
        logger.info(sessionString);

        OTFSessionPojoUtils session = new Gson().fromJson(sessionString, OTFSessionPojoUtils.class);
        return session.getPresenterURL();
    }

    @RequestMapping(value = "/otfDailySessions", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void otfDailySessions(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
//			awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.CREATE_LINK, request);
            otfManager.sendDailySessionEmail();
        }
    }

    @RequestMapping(value = "/otfScheduler", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void otfScheduler(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            otfManager.otfSchedulerAsync();
        }
    }

    @RequestMapping(value = "/otfRecordingScheduler", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void otfRecordingScheduler(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
            otfManager.otfRecordingSchedulerAsync();
        }
    }

// Not deleting recording in platform now
//	@RequestMapping(value = "/deleteRecordingsCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public void deleteRecordings(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
//			@RequestBody String request) throws Exception {
//		Gson gson = new Gson();
//		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
//		if (messageType.equals("SubscriptionConfirmation")) {
//			String json = null;
//			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
//			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//			logger.info(resp.getEntity(String.class));
//		} else if (messageType.equals("Notification")) {
//			logger.info("Notification received - SNS");
//			logger.info(subscriptionRequest.toString());
//			// String subject = subscriptionRequest.getSubject();
//			// String message = subscriptionRequest.getMessage();
//			otfManager.deleteRecordingsDailyAsync();
//		}
//	}
    @RequestMapping(value = "/performSessionSanityCheck", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void performSessionSanityCheck(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            // String subject = subscriptionRequest.getSubject();
            // String message = subscriptionRequest.getMessage();
            otfManager.performSanityCheckForFutureSessionsAsync();
        }
    }

    @Deprecated
    @RequestMapping(value = "/getCalendarSessions", method = RequestMethod.GET)
    @ResponseBody
    public String getCalendarSessions(HttpServletRequest request) throws IOException, VException {
        String queryString = request.getQueryString();
        logger.info("queryString - " + queryString);
        String getUpcomingSessionsUrl = schedulingEndPoint + "session/getCalendarSessions";
        if (!StringUtils.isEmpty(queryString)) {
            getUpcomingSessionsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/getRecordingDownloadUrl", method = RequestMethod.GET)
    @ResponseBody
    public String getRecordingDownloadUrl(@RequestParam(name = "sessionId") String sessionId,
            @RequestParam(name = "recordingId") String recordingId) throws VException {
        String url = schedulingEndPoint + "session/getRecordingDownloadUrl?sessionId=" + sessionId + "&recordingId="
                + recordingId;

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/updateSessionRecordingsById", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse updateSessionRecordingsById(
            @RequestParam(name = "sessionId", required = true) String sessionId) throws VException {
        logger.info("Request - sessionId:" + sessionId);
        otfManager.handleSessionRecordings(sessionId);
        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setResponse(sessionId);
        return response;
    }

    @Deprecated
    @RequestMapping(value = "/invokeSessionRecordingLambda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse invokeSessionRecordingLambda(
            @RequestBody HandleOTFRecordingLambda handleOTFRecordingLambda) throws VException {
        logger.info("Request - " + handleOTFRecordingLambda.toString());
        otfManager.invokeSessionRecordingLambda(handleOTFRecordingLambda);
        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setResponse(gson.toJson(handleOTFRecordingLambda));
        return response;
    }

    @Deprecated
    @RequestMapping(value = "/updateSessionRecordings", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse updateRecordings(
            @RequestParam(name = "afterEndTime", required = true) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = true) Long beforeEndTime) throws VException {
        return otfManager.updateRecordings(afterEndTime, beforeEndTime);
    }

    @Deprecated
    @RequestMapping(value = "/updateSessionReplayUrls", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse updateSessionReplayUrls(
            @RequestParam(name = "afterEndTime", required = true) Long afterEndTime,
            @RequestParam(name = "beforeEndTime", required = true) Long beforeEndTime) throws VException {
        return otfManager.updateSessionReplayUrls(afterEndTime, beforeEndTime);
    }

    // Not deleting recording in platform now
//	@RequestMapping(value = "/delelteOTFRecordings", method = RequestMethod.POST)
//	@ResponseBody
//	public PlatformBasicResponse delelteOTFRecordings(@RequestParam(name = "startTime", required = true) Long startTime,
//			@RequestParam(name = "endTime", required = true) Long endTime) throws VException {
//		otfManager.deleteRecordings(startTime, endTime);
//		return new PlatformBasicResponse();
//	}

    @Deprecated
    @RequestMapping(value = "/getUpcomingSessionsWithAttendeeInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<OTFSessionPojoUtils> getUpcomingSessionsWithAttendeeInfos(
            @RequestParam(name = "startTime", required = true) long startTime,
            @RequestParam(name = "endTime") long endTime,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "sessionId", required = false) String sessionId) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return otfManager.getUpcomingSessionsWithAttendeeInfos(startTime, endTime, start, size, sessionId);
    }

    // Moved to scheduling
    @Deprecated
    @RequestMapping(value = "/handleSessionEventsForOTF", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleSessionEventsForOTF(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            SessionEventsOTF eventType = SessionEventsOTF.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            if (eventType != null) {
                otfManager.handleSessionEventsForOTF(eventType, message);
            }
        }
    }

    // migrated to scheduling
    @Deprecated
    @RequestMapping(value = "/handleBatchEventsForOTF", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleBatchEventsForOTF(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            BatchEventsOTF eventType = BatchEventsOTF.valueOf(subject);
            String message = subscriptionRequest.getMessage();
            if (eventType != null) {
                otfManager.handleBatchEventsForOTF(eventType, message);
            }
        }
    }

    @Deprecated
    @RequestMapping(value = "/updateReplayUrlforGTW", method = RequestMethod.POST)
    @ResponseBody
    public String updateReplayUrlforGTW(@RequestBody UpdateReplayUrlReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        logger.info("Request:" + req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/updateReplayUrlforGTW",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        otfManager.handleSessionRecordings(req.getSessionId());
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/addUpdateSessionContent", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String addUpdateSessionContent(@RequestBody AddUpdateSessionContentReq req) throws Exception {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/addUpdateSessionContent",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/addSessions", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String addSessions(@RequestBody AddOTFSessionsReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();

        if (req.getSessionPojo().size() > 50) {
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule more than 50 sessions at once");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/addSessions",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/teacherAddSessionToBatch", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Map<String, String> teacherAddSessionToBatch(@RequestBody AddOTFSessionsReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.TEACHER, Boolean.TRUE);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if(sessionData != null && sessionData.getUserId() != null){
            UserBasicInfo teacher = fosUtils.getUserBasicInfo(sessionData.getUserId(),true);
            if(teacher != null && StringUtils.isEmpty(teacher.getOrgId())){
                throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule the sessions");
            }
            //TODO techer orgId check with batch orgId
        }
        else{
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule the sessions");
        }

        if (req.getSessionPojo().size() > 5) {
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "You are not allowed to schedule more than 5 sessions at once");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/addSessions",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);

        if(ArrayUtils.isEmpty(sessions) || sessions.get(0) == null || StringUtils.isEmpty(sessions.get(0).getId())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session not created try again later");
        }
        String sesseionUrl = ConfigUtils.INSTANCE.getStringValue("FOS_SESSION_ENDPOINT") +"/session/"+ sessions.get(0).getId();
        logger.info("session url : "+ sesseionUrl);
        Map<String, String> respMap = new HashMap<>();
        respMap.put("sessionUrl",WebUtils.INSTANCE.shortenUrl(sesseionUrl));
        return respMap;
    }

    @RequestMapping(value = "/addBatchIdsToSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String addBatchIdsToSession(@RequestBody AddRemoveBatchIdsToSessionReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/addBatchIdsToSession",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    @RequestMapping(value = "/removeBatchIdsFromSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String removeBatchIdsFromSession(@RequestBody AddRemoveBatchIdsToSessionReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();

        OTFSessionPojoUtils sessionPojo = otfManager.getSessionById(req.getSessionId());
        if (sessionPojo == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found for " + req.getSessionId());
        }

        List<String> cacheKeys = otfManager.getSessionAttedeeCacheKeys(sessionPojo);

//        if (ArrayUtils.isNotEmpty(cacheKeys)) {
//            String[] deleteKeys = new String[cacheKeys.size()];
//            redisDAO.deleteKeys(cacheKeys.toArray(deleteKeys));
//        }

        otfManager.removeCachedSessionsForAttendees(cacheKeys);

        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "session/removeBatchIdsFromSession",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @Deprecated
    @RequestMapping(value = "/getUpcomingSessionsForUser", method = RequestMethod.GET)
    @ResponseBody
    public OTFCalendarSessionInfoRes getUserUpcomingSessions(GetSessionsReq getSessionsReq) throws VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(userId == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }
        
        if(role == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");            
        }
        getSessionsReq.verify();
        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfManager.getUserUpcomingSessions(getSessionsReq.getStart(),
                getSessionsReq.getSize(), getSessionsReq.getBatchId(), userId, role, getSessionsReq.getQuery(),
                getSessionsReq.getStartTime(), getSessionsReq.getEndTime(), getSessionsReq.getBatchIds());
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
				(oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);
        
    } 

    @Deprecated
    @RequestMapping(value = "/getPastSessionsForUser", method = RequestMethod.GET)
    @ResponseBody
    public OTFCalendarSessionInfoRes getUserPastSessions(GetSessionsReq getSessionsReq) throws VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(userId == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }
        
        if(role == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");            
        }        
        getSessionsReq.verify();
        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfManager.getUserPastSessions(getSessionsReq.getStart(),
                getSessionsReq.getSize(), getSessionsReq.getBatchId(), userId, role, getSessionsReq.getQuery(),
                getSessionsReq.getStartTime(), getSessionsReq.getEndTime(), getSessionsReq.getBatchIds());
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
				(oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);        
        
    }


    @Deprecated
    @RequestMapping(value = "/getPastSessionsForUserWithFilter", method = RequestMethod.GET)
    @ResponseBody
    public OTFCalendarSessionInfoRes getPastSessionsForUserWithFilter(GetSessionsReq getSessionsReq) throws VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(userId == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        if(role == null){
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "");
        }

        getSessionsReq.verify();
        List<OTFCalendarSessionInfo> oTFCalendarSessionInfos = otfManager.getPastSessionsForUserWithFilter(getSessionsReq.getStart(),
                getSessionsReq.getSize(), getSessionsReq.getBatchId(), userId, role, getSessionsReq.getQuery(), getSessionsReq.getBatchIds());
        return new OTFCalendarSessionInfoRes(oTFCalendarSessionInfos,
                (oTFCalendarSessionInfos != null) ? oTFCalendarSessionInfos.size() : 0);

    }

}
