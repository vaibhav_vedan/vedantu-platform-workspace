/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.filemgmt;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.request.filemgmt.FileDownloadUrlReq;
import com.vedantu.platform.request.filemgmt.FileInitReq;
import com.vedantu.platform.request.filemgmt.GetFileMetadataReq;
import com.vedantu.platform.request.filemgmt.UpdateFileMetadataReq;
import com.vedantu.platform.request.filemgmt.UpdateFileUploadStatusReq;
import com.vedantu.platform.request.filemgmt.UploadFileMetadataCollectionReq;
import com.vedantu.platform.request.filemgmt.UploadFileMetadataReq;
import com.vedantu.platform.response.filemgmt.FileDownloadUrlRes;
import com.vedantu.platform.response.filemgmt.FileInitRes;
import com.vedantu.platform.response.filemgmt.UpdateFileUploadStatusRes;
import com.vedantu.platform.response.filemgmt.FileMetadataCollectionRes;
import com.vedantu.platform.managers.filemgmt.FileMgmtManager;
import com.vedantu.platform.response.filemgmt.GetFileMetadatRes;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.pojo.wave.WhiteboardPDF;
import com.vedantu.platform.request.filemgmt.FileACL;
import com.vedantu.platform.request.filemgmt.PdfToImageReq;
import com.vedantu.platform.request.filemgmt.UpdateWhiteboardPdfUploadStatusReq;
import com.vedantu.platform.request.filemgmt.UploadType;
import com.vedantu.platform.request.filemgmt.WhiteboardPdfInitReq;
import com.vedantu.platform.response.filemgmt.GetWhiteboardPdfInfoRes;
import com.vedantu.platform.response.filemgmt.PdfToImageRes;
import com.vedantu.platform.response.filemgmt.WhiteboardPdfInitRes;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.LogFactory;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/filemgmt")
public class FileMgmtController {

    @Autowired
    FileMgmtManager fileMgmtManager;

    @Autowired
    LogFactory logFactory;

    
    
    @Autowired
    HttpSessionUtils sessionUtils;

    Logger logger = logFactory.getLogger(FileMgmtController.class);
    
    @RequestMapping(value = "/init", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FileInitRes init(@RequestBody FileInitReq req) throws Exception {
        logger.info("Entering " + req.toString());
        sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
        return fileMgmtManager.init(req);

    }
    
    @RequestMapping(value = "/changeState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UpdateFileUploadStatusRes changeState(@RequestBody UpdateFileUploadStatusReq req) throws VException {
        req.verify();
        //Auth added inside manager
        UpdateFileUploadStatusRes res = fileMgmtManager.changeState(req,false);
        return res;
    }
    
    @RequestMapping(value = "/initFromApp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FileInitRes initFromApp(@RequestBody FileInitReq req,HttpServletRequest request) throws Exception {
        logger.info("Entering " + req.toString());
        sessionUtils.isAllowedApp(request);
        return fileMgmtManager.init(req);

    }
    
    @RequestMapping(value = "/changeStateFromApp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UpdateFileUploadStatusRes changeStateFromApp(@RequestBody UpdateFileUploadStatusReq req,HttpServletRequest request) throws VException {
        req.verify();
        sessionUtils.isAllowedApp(request);
        UpdateFileUploadStatusRes res = fileMgmtManager.changeState(req,true);
        return res;
    }

    @RequestMapping(value = "/fetchDownloadUrl", method = RequestMethod.GET)
    @ResponseBody
    public FileDownloadUrlRes fetchDownloadUrl(FileDownloadUrlReq req) throws VException {
        req.verify();
        FileDownloadUrlRes res = fileMgmtManager.fetchDownloadUrl(req);
        return res;
    }

    @RequestMapping(value = "/insertFileMetadata", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicRes insertFileMetadata(@RequestBody UploadFileMetadataReq fileMetaDataReq) throws VException {
        fileMetaDataReq.verify();
        sessionUtils.checkIfAllowed(fileMetaDataReq.getUserId(), null, Boolean.FALSE);
        return fileMgmtManager.insertFileMetadata(fileMetaDataReq);
    }

    @RequestMapping(value = "/insertFileMetadataCollection", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FileMetadataCollectionRes insertFileMetadataCollection(@RequestBody UploadFileMetadataCollectionReq fileMetadataCollectionReq)
            throws VException {
        fileMetadataCollectionReq.verify();
        return fileMgmtManager.insertFileMetadataCollection(fileMetadataCollectionReq);
    }
    
    @RequestMapping(value = "/insertFileMetadataForAWS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FileMetadataCollectionRes insertFileMetadataForAWS(@RequestBody UploadFileMetadataCollectionReq fileMetadataCollectionReq)
            throws VException {
        fileMetadataCollectionReq.verify();
        FileMetadataCollectionRes fileMetadataCollectionRes = fileMgmtManager.insertFileMetadataForAWS(fileMetadataCollectionReq);
        return fileMetadataCollectionRes;
    }
    
    
    @RequestMapping(value = "/fetchFileMetadata", method = RequestMethod.GET)
    @ResponseBody
    public GetFileMetadatRes fetchFileMetadata(GetFileMetadataReq getReq)
			throws VException {
		getReq.verify();
                return fileMgmtManager.fetchFileMetadata(getReq);
    }
    
    @RequestMapping(value = "/updateFileMetadata", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicRes updateFileMetadata(@RequestBody UpdateFileMetadataReq updateReq) throws VException {
		updateReq.verify();
                //Auth added inside manager
                return fileMgmtManager.updateFileMetadata(updateReq);
    }
    
    @RequestMapping(value = "/initWhiteboardPdf", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WhiteboardPdfInitRes initWhiteboardPdf(@RequestBody WhiteboardPdfInitReq updateReq) throws VException {
		updateReq.verify();
                //Auth added inside manager
                return fileMgmtManager.initWhiteboardPdf(updateReq);
    }
    
    @RequestMapping(value = "/changeWhiteboardPdfState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WhiteboardPDF changeWhiteboardPdfState(@RequestBody UpdateWhiteboardPdfUploadStatusReq updateReq) throws VException {
		updateReq.verify();
                //Auth added inside manager
                return fileMgmtManager.changeWhiteboardPdfState(updateReq);
    }
    
    @RequestMapping(value = "/getWhiteboardPdfInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetWhiteboardPdfInfoRes getWhiteboardPdfInfo(@RequestParam(name = "id", required = true) String id) throws VException, Exception {
		//Auth added inside manager
                return fileMgmtManager.getWhiteboardPdfInfo(id);
    }

    @RequestMapping(value = "/convertPdftoImages", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PdfToImageRes> convertPdftoImages(
            @RequestParam(name = "file", required = true) MultipartFile file,
            @RequestParam(name = "name", required = true) String name,
            @RequestParam(name = "type", required = true) String type,
            @RequestParam(name = "size", required = true) Long size,
            @RequestParam(name = "userId", required = true) Long userId,
            @RequestParam(name = "uploadType", required = true) UploadType uploadType,
            @RequestParam(name = "acl", required = false) FileACL acl,
            @RequestParam(name = "folder", required = false) String folder,
            @RequestParam(name = "scale", required = false) Float scale
            ) throws VException, InternalServerErrorException, NotFoundException, BadRequestException, IOException {
		PdfToImageReq req=new PdfToImageReq(name,type,size,userId,uploadType,acl,folder,file,scale);
                req.verify();
                //Auth added inside manager
                
                return fileMgmtManager.convertPdftoImages(req);
    }
}
