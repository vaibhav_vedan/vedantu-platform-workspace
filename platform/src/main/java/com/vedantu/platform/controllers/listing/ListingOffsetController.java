package com.vedantu.platform.controllers.listing;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

import com.vedantu.User.Role;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.listing.ListingOffsetManager;
import com.vedantu.platform.pojo.listing.ListingOffsetUploadResponse;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("/listing/offset")
public class ListingOffsetController {

	@Autowired
	LogFactory logFactory;

	@Autowired
	HttpSessionUtils sessionUtils;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(ListingOffsetController.class);

	@Autowired
	ListingOffsetManager listingOffsetManager;

	//@CrossOrigin
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody ListingOffsetUploadResponse uploadFileHandler(
			@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file) {

		ListingOffsetUploadResponse result =  new ListingOffsetUploadResponse();
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				String[] nameSplit = name.split("\\.");

				File serverFile = File.createTempFile(nameSplit[0], "."
						+ nameSplit[1]);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				listingOffsetManager.uploadToElasticSearch(serverFile);

				logger.info("You successfully uploaded file=" + name);

				result.setErrorCode(ErrorCode.SUCCESS);
				result.setErrorMessage( "You successfully uploaded file=" + name);
				return result;
			} catch (Exception e) {
				result.setErrorMessage("You failed to upload " + name + " => " + e.getMessage());
				result.setErrorCode(ErrorCode.SERVICE_ERROR);
				return result;
			}
		} else {
			result.setErrorMessage("You failed to upload " + name
					+ " because the file was empty.");
			result.setErrorCode(ErrorCode.SERVICE_ERROR);
			return result;
		}
	}

	//@CrossOrigin
	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public @ResponseBody void resetOffset() throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		listingOffsetManager.resetOffset();
	}
}
