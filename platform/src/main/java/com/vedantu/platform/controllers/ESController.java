/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.vedantu.platform.managers.ESManager;
import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.request.AdjustScoreOffsetReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/es")
public class ESController {
    
    @Autowired
    ESManager eSManager;

    @Autowired
    LogFactory logFactory;

    
    
    @Autowired
    HttpSessionUtils httpSessionUtils;

    Logger logger = logFactory.getLogger(ESController.class);
    
//    @RequestMapping(value = "/updateStartsFromInES", method = RequestMethod.GET)
//    @ResponseBody
//    public void updateStartsFromInES(@RequestParam(value = "teacherId") Long teacherId, @RequestParam(value = "teacherMinPrice") Integer teacherMinPrice
//            , @RequestParam(value = "teacherOneHourRate") Integer teacherOneHourRate) throws VException {
//        eSManager.updateStartsFromInES(teacherId, teacherMinPrice, teacherOneHourRate);
//    }
    
    @RequestMapping(value = "/adjustOffset", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse adjustOffset(AdjustScoreOffsetReq req) throws VException {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        eSManager.updateScoreOffset(req.getUserId(), req.getOffset());
        return new PlatformBasicResponse();
    }
    
    
}
