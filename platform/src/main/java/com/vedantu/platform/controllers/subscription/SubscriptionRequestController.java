package com.vedantu.platform.controllers.subscription;

import javax.servlet.http.HttpServletRequest;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.subscription.SubscriptionRequestManager;
import com.vedantu.platform.managers.subscription.SubscriptionRequestTaskManager;
import com.vedantu.platform.pojo.subscription.GetCompactSubscriptionRequestsResponse;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestInfo;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestPaymentReq;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestPaymentRes;
import com.vedantu.platform.pojo.subscription.SubscriptionVO;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.subscription.request.GetSubscriptionRequestReq;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.subscription.ResolveSlotConflictReq;
import com.vedantu.util.subscription.UpdateSubscriptionRequest;
import com.vedantu.util.subscription.UpdateSubscriptionRequestSubState;

import io.swagger.annotations.ApiOperation;

@EnableAsync
@RestController
@RequestMapping("/subscriptionRequest")
public class SubscriptionRequestController {

	@Autowired
	private SubscriptionRequestManager subscriptionRequestManager;

	@Autowired
	private SubscriptionRequestTaskManager taskManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionRequestController.class);

	@ApiOperation(value = "Create Subscription Request", notes = "Returns created subscription")
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequestInfo createSubscriptionRequest(@RequestBody SubscriptionVO subscriptionVO)
			throws VException {
		logger.info("Request:" + subscriptionVO.toString());
		SubscriptionRequestInfo subscriptionRequestInfo = subscriptionRequestManager
				.createSubscriptionRequest(subscriptionVO);
		logger.info("Response:" + subscriptionRequestInfo.toString());
		return subscriptionRequestInfo;
	}

	@ApiOperation(value = "Get a Subscription Request", notes = "Returns SubscriptionRequest")
	@RequestMapping(value = "get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public SubscriptionRequestInfo getSubscriptionRequest(@PathVariable("id") Long id,
			@RequestParam(value = "callingUserId", required = false) Long callingUserId,
			@RequestParam(value = "requestSource", required = false) RequestSource requestSource) throws VException {
		logger.info("SubscriptionRequestId: " + id);
		SubscriptionRequestInfo subscriptionRequestInfo = subscriptionRequestManager.getSubscriptionRequest(id,
				callingUserId,requestSource);
		subscriptionRequestInfo.setPlan(subscriptionRequestManager.getTeacherPlan(subscriptionRequestInfo.getPlanId(),
				subscriptionRequestInfo.getTeacherId()));
		//subscriptionRequestInfo = subscriptionRequestManager.fillSubscriptionRequestInfo(subscriptionRequestInfo,
		//		requestSource);
		logger.info("Response:" + subscriptionRequestInfo.toString());
		return subscriptionRequestInfo;
	}

	@ApiOperation(value = "Get SubscriptionRequests", notes = "Returns list of subscriptions")
	@RequestMapping(value = "gets", method = RequestMethod.GET)
	@ResponseBody
	public GetCompactSubscriptionRequestsResponse getSubscriptionRequests(GetSubscriptionRequestReq req) throws Exception {
		GetCompactSubscriptionRequestsResponse response = subscriptionRequestManager
				.getSubscriptionRequests(req);
		return response;
	}

        //TODO who is calling this
	@ApiOperation(value = "Activate Subscription Request", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/activate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicResponse activateSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws VException {
		logger.info("Request:" + req.toString());
		SubscriptionRequestInfo subscriptionRequestInfo = subscriptionRequestManager.activateSubscriptionRequest(req);
		subscriptionRequestInfo.setPlan(subscriptionRequestManager.getTeacherPlan(subscriptionRequestInfo.getPlanId(),
				subscriptionRequestInfo.getTeacherId()));
		if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.MOBILE)) {
			BasicResponse abstractResponse = new BasicResponse();
			logger.info("Response:" + abstractResponse.toString());
			return abstractResponse;
		}
		logger.info("Response:" + subscriptionRequestInfo.toString());
		return subscriptionRequestInfo;
	}

	@ApiOperation(value = "Cancel Subscription Request", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicResponse cancelSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws VException {
		logger.info("Request:" + req.toString());
		BasicResponse subscriptionRequestInfo = subscriptionRequestManager.cancelSubscriptionRequest(req);
		logger.info("Response:" + subscriptionRequestInfo.toString());
		return subscriptionRequestInfo;
	}

	@ApiOperation(value = "Reject Subscription Request", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/reject", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicResponse rejectSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws VException {
		logger.info("Request:" + req.toString());
		BasicResponse basicResponse = subscriptionRequestManager.rejectSubscriptionRequest(req);
		logger.info("Response:" + basicResponse.toString());
		return basicResponse;
	}

	@ApiOperation(value = "Expire Subscription Request", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/expire", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void expireSubscriptionRequests(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws VException {
		logger.info(request);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request,
				AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Expire Subscription Requests");
			subscriptionRequestManager.expireSubscriptionRequests(AbstractFrontEndReq.Constants.CALLING_USER_ID_SYSTEM);
		}
	}

	@ApiOperation(value = "Expiry Reminder for Subscription Request", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/expiryReminder", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void sendExpiryReminderSubscriptionRequests(
			@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
					throws VException {
		logger.info(request);
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request,
				AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			subscriptionRequestManager.sendExpiryReminderSubscriptionRequests();
		}
	}

	@ApiOperation(value = "Accept Subscription Request", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/accept", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicResponse acceptSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws VException {
		logger.info("Request:" + req.toString());
		BasicResponse basicResponse = subscriptionRequestManager.acceptSubscriptionRequest(req);
		logger.info("Response:" + basicResponse.toString());
		return basicResponse;
	}

	@RequestMapping(value = "/handlePayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequestPaymentRes handlePayment(@RequestBody SubscriptionRequestPaymentReq subscriptionRequestPaymentReq,
			HttpServletRequest req) throws Throwable {
		// String sessionUserId = PlatformTools.getSessionUserId(req);
		// Role role = PlatformTools.getSessionUserRole(req);
		//
		// logger.info("handlePayments - callingUserId " + sessionUserId + ",
		// role " + role);
		// if (StringUtils.isEmpty(sessionUserId) || role == null) {
		// new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Action not
		// allowed for this user");
		// }
		//
		// if (!Role.ADMIN.equals(role)) {
		// subscriptionRequestPaymentReq.setUserId(Long.parseLong(sessionUserId));
		// }

		SubscriptionRequestPaymentRes response = subscriptionRequestManager.handlePayments(subscriptionRequestPaymentReq);
		logger.info("Response: " + response.toString());
		return response;
	}

	@ApiOperation(value = "Update Subscription Request sub state", notes = "Returns subscriptionRequest")
	@RequestMapping(value = "/updateSubState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicResponse updateSubscriptionRequestSubState(@RequestBody UpdateSubscriptionRequestSubState req)
			throws VException {
		logger.info("Request:" + req.toString());
		BasicResponse basicResponse = taskManager.updateSubscriptionRequestSubState(req.getId(), req.getSubState());
		logger.info("Response:" + basicResponse.toString());
		return basicResponse;
	}

	@Deprecated
	@ApiOperation(value = "Fetches slot conflicts of a Subscription Request", notes = "Returns conflicts")
	@RequestMapping(value = "/getSlotConflicts", method = RequestMethod.GET)
	@ResponseBody
	public String getSubscriptionRequestConflicts(@RequestParam(name = "id", required = true) Long id)
			throws Throwable {
		logger.info("Request:" + id);
		String response = subscriptionRequestManager.getSubscriptionRequestConflicts(id);
		logger.info("Response:" + response.toString());
		return response;
	}

	@Deprecated
	@RequestMapping(value = "/resolveSlotConflicts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Resolve conflict for the provided request", notes = "")
	public String resolveSubscriptionRequestConflicts(@RequestBody ResolveSlotConflictReq req) throws Exception {
		logger.info("Request : " + req.toString());
		String response = subscriptionRequestManager.resolveSlotConflicts(req);
		logger.info("Response : " + response.toString());
		return response;
	}

	@RequestMapping(value = "/markScheduled", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Mark the subscription request to session scheduled", notes = "")
	public String markScheduledSubState(@RequestParam(name = "subscriptionRequestId", required = true) Long id)
			throws Exception {
		logger.info("Request : " + id);
		String response = subscriptionRequestManager.markScheduledSubState(id);
		logger.info("Response : " + response.toString());
		return response;
	}

	@RequestMapping(value = "/retryProcessing", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Retry subscription request processing.", notes = "")
	public void retrySubscriptionRequestProcessing(
			@RequestParam(name = "subscriptionRequestId", required = true) Long subscriptionRequestId)
					throws VException {
		logger.info("Request : " + subscriptionRequestId);
		subscriptionRequestManager.retrySubscriptionRequestProcessing(subscriptionRequestId);
	}

	@Deprecated
	@ApiOperation(value = "Get pending Subscription Request", notes = "Returns conflicts")
	@RequestMapping(value = "/getPendingRequests", method = RequestMethod.GET)
	@ResponseBody
	public String getPendingSubscriptionRequests(@RequestParam(name = "startTime", required = true) Long startTime,
			@RequestParam(name = "endTime", required = true) Long endTime) throws Throwable {
		logger.info("Request: startTime - " + startTime + " endTime - " + endTime);
		String response = subscriptionRequestManager.getPendingSubscriptionRequests(startTime, endTime);
		logger.info("Response:" + response.toString());
		return response;
	}
        
        
	@ApiOperation(value = "Retry Subscription Request processing")
	@RequestMapping(value = "/retrySubscriptionRequestProcessing", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void retrySubscriptionRequestProcessingCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws VException {
        Gson gson = new Gson();
            AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            //String subject = subscriptionRequest.getSubject();
            //String message = subscriptionRequest.getMessage();
            subscriptionRequestManager.retrySubscriptionRequestProcessingCron();
        }      
	}        
}
