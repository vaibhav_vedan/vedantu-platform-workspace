package com.vedantu.platform.controllers.notification;


import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.exception.VException;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.platform.managers.notification.SMSManager;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/notification-centre/SMS")
public class SMSController {
	
	
        
        @Autowired
        SMSManager smsManager;
        @Autowired
        private AsyncTaskFactory asyncTaskFactory;
        @Autowired
        private LogFactory logFactory;

        @SuppressWarnings("static-access")
        private Logger logger = logFactory.getLogger(EmailController.class);
        //TODO: Check if anyone using it
//	@RequestMapping(value = "/sendSMS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String sendSMS(@RequestBody TextSMSRequest request) throws VException{
//		return smsManager.sendSMS(request);
//	}
//	
//	@RequestMapping(value = "/sendBulkSMS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String sendBulkSMS(@RequestBody BulkTextSMSRequest request) throws VException {
//                return smsManager.sendBulkSMS(request);
//	}


        @RequestMapping(value = "/track", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public PlatformBasicResponse track(@RequestParam(value = "id", required = true) String id,
                                           @RequestParam(value = "linkRandomId", required = false) String linkRandomId,
                                           @RequestParam(value = "redirectUrl", required = true) String redirectUrl,
                                           HttpServletResponse response) throws UnsupportedEncodingException, IOException {
                Map<String, Object> payload = new HashMap<>();
                payload.put("id", id);
                payload.put("linkRandomId", linkRandomId);
                String decodedUrl = URLDecoder.decode(redirectUrl, Constants.DEFAULT_CHARSET);
                response.sendRedirect(decodedUrl);

                String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
                String url = notificationEndpoint + "/SMS/smsLinkClicked";
                if (id != null) {
                        url += "?id=" + id;
                        url += "&linkRandomId=" + linkRandomId;
                }
                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);

                return new PlatformBasicResponse();
        }

}
