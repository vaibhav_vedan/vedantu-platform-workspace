package com.vedantu.platform.controllers.Challenges;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonSyntaxException;
import com.vedantu.exception.VException;
import com.vedantu.platform.enums.ChallengerGradeType.ChallengerGradeType;
import com.vedantu.platform.managers.Challenges.ChallengeManager;
import com.vedantu.platform.pojo.Challenges.Challenges;
import com.vedantu.platform.request.Challenges.ChallengeReq;
import com.vedantu.platform.response.Challenges.ChallengesRes;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("challenges")
public class ChallengeController {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private ChallengeManager challengeManager;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ChallengeController.class);

	@RequestMapping(value = "/setChallenges", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ChallengeReq setChallenges(@RequestBody ChallengeReq req) throws VException, JsonProcessingException {
		logger.info("Set Challenge Request :" + req.toString());

		return challengeManager.setChallenges(req);
	}

	@RequestMapping(value = "/fetchChallenges", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ChallengesRes fetchTestimonial(@RequestParam(value = "challengerGradeType") ChallengerGradeType challengerGradeType) throws VException, JsonSyntaxException {
		return challengeManager.fetchChallenges(challengerGradeType);
	}
}
