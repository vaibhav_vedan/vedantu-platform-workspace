package com.vedantu.platform.controllers.review;

import com.vedantu.exception.BadRequestException;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.session.pojo.CumilativeRating;
import io.swagger.annotations.ApiOperation;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.review.RatingReviewManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.review.requests.AddPrioritiesRequest;
import com.vedantu.review.requests.AddReviewRequest;
import com.vedantu.review.requests.EditReviewRequest;
import com.vedantu.review.requests.GetBestReviewsForSuggestionRequest;
import com.vedantu.review.requests.GetReviewRequest;
import com.vedantu.review.requests.GetReviewsRequest;
import com.vedantu.review.response.AddReviewResponse;
import com.vedantu.review.response.EditReviewResponse;
import com.vedantu.review.response.GetReviewsResponse;
import com.vedantu.review.response.ReviewInfo;
import com.vedantu.util.LogFactory;
import javax.validation.Valid;

@RestController
@RequestMapping("/reviews")
public class RatingReviewController {


    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(RatingReviewController.class);

    @Autowired
    RatingReviewManager ratingReviewManager;

    @Autowired
    CumilativeRatingDao cumilativeRatingDao;

    @Autowired
    HttpSessionUtils sessionUtils;

    @ApiOperation(value = "assignBaseRating", notes = "Returns add review response")
    @RequestMapping(value = "/assignBaseRating", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public AddReviewResponse assignBaseRating(
            @Valid @RequestBody AddReviewRequest addReviewRequest) throws Exception {
        sessionUtils.checkIfAllowed(addReviewRequest.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        logger.info("assignBaseRating request: " + addReviewRequest.toString());
        addReviewRequest.verify();
        AddReviewResponse addReviewResponse = ratingReviewManager
                .assignBaseRating(addReviewRequest);
        logger.info("assignBaseRating response: " + addReviewResponse.toString());
        return addReviewResponse;
    }

    @ApiOperation(value = "add new review", notes = "Returns created review")
    @RequestMapping(value = "/addReview", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public AddReviewResponse addReview(
            @Valid @RequestBody AddReviewRequest addReviewRequest) throws Exception {

        logger.info("Add review request: " + addReviewRequest.toString());
        sessionUtils.checkIfAllowedList(addReviewRequest.getCallingUserId(), null, false);
        addReviewRequest.verify();
        AddReviewResponse addReviewResponse = ratingReviewManager.addReview(addReviewRequest);
        logger.info("Add review response: " + addReviewResponse.toString());
        return addReviewResponse;
    }

    @ApiOperation(value = "edit review", notes = "Returns edited review")
    @RequestMapping(value = "/editReview", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public EditReviewResponse editReview(
            @RequestBody EditReviewRequest editReviewRequest) throws VException {
        logger.info("Edit review request: " + editReviewRequest.toString());
        sessionUtils.checkIfAllowed(editReviewRequest.getCallingUserId(), Role.ADMIN, true);
        editReviewRequest.verify();
        EditReviewResponse editReviewResponse = ratingReviewManager
                .editReview(editReviewRequest);
        logger.info("Edit review response: " + editReviewRequest.toString());
        return editReviewResponse;
    }

    @ApiOperation(value = "get review", notes = "Returns review")
    @RequestMapping(value = "/getReview", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ReviewInfo getReview(GetReviewRequest getReviewRequest)
            throws VException {
        logger.info("get review request: " + getReviewRequest.toString());
        getReviewRequest.verify();
        ReviewInfo reviewInfo = ratingReviewManager
                .getReview(getReviewRequest);
        logger.info("get review response: " + reviewInfo.toString());
        return reviewInfo;
    }

    @ApiOperation(value = "get reviews", notes = "Returns reviews")
    @RequestMapping(value = "/getReviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public GetReviewsResponse getReviews(GetReviewsRequest getReviewsRequest)
            throws VException {
        logger.info("get reviews request: " + getReviewsRequest.toString());
        getReviewsRequest.verify();
        GetReviewsResponse getReviewsResponse = ratingReviewManager
                .getReviews(getReviewsRequest);
        logger.info("get reviews response: "
                + getReviewsResponse.toString());
        return getReviewsResponse;
    }

    @ApiOperation(value = "add Priorities", notes = "Returns Base response")
    @RequestMapping(value = "/addPriority", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BaseResponse addPriorities(
            @RequestBody AddPrioritiesRequest addPrioritiesRequest)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("add Priorities request: "
                + addPrioritiesRequest.toString());
        addPrioritiesRequest.verify();
        Boolean result = ratingReviewManager
                .addPriorities(addPrioritiesRequest);
        logger.info("add Priorities response: " + result);
        return new BaseResponse(ErrorCode.SUCCESS, "");
    }

    @ApiOperation(value = "get Best reviews", notes = "Returns best review Infos")
    @RequestMapping(value = "/getBestReviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public List<ReviewInfo> getBestReviews(
            @RequestParam(value = "callingUserId", required = false) Long callingUserId,
            @RequestParam(value = "userId", required = false) Long userId,
            @RequestParam(value = "lengthPreferred", required = false) Long lengthPreferred,
            @RequestParam(value = "noOfReviews", required = false) Integer noOfReviews,
            @RequestParam(value = "isPositive", required = false) Boolean isPositive)
            throws VException {

        GetBestReviewsForSuggestionRequest getBestReviewsForSuggestionRequest = new GetBestReviewsForSuggestionRequest(
                userId, lengthPreferred, noOfReviews, isPositive);
        getBestReviewsForSuggestionRequest.setCallingUserId(callingUserId);

        logger.info("get best reviews request: "
                + getBestReviewsForSuggestionRequest.toString());
        getBestReviewsForSuggestionRequest.verify();
        List<ReviewInfo> getBestReviewsResponse = ratingReviewManager
                .getBestReviewsForSuggestion(getBestReviewsForSuggestionRequest);
        return getBestReviewsResponse;
    }

    // using for internal purpose only from subscription
    @RequestMapping(value="/getCumulativeRatingsMap", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Map<String, CumilativeRating> getCumulativeRatingsMap(@RequestParam (value="userIdsCommaSeparated") String userIdsCommaSeparated,
                                                                 @RequestParam(value="entityType") com.vedantu.util.EntityType entityType)
                                                                 throws BadRequestException {

        if(Objects.isNull(entityType) || (userIdsCommaSeparated.isEmpty()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"User Ids empty or Entity Type is null");

        String [] userIds = userIdsCommaSeparated.split(",");
        List<String> userIdsList = Arrays.asList(userIds);
        Map<String,CumilativeRating> res = cumilativeRatingDao.getCumulativeRatingsMap(userIdsList,entityType);
        logger.info("res {}",res);
        return res;
    }

}
