package com.vedantu.platform.controllers.notification;


import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.notification.requests.GetNotificationsReq;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.notification.requests.RegisterUserDeviceRequest;
import com.vedantu.notification.responses.GetNotificationsRes;
import com.vedantu.notification.responses.MarkSeenRes;
import com.vedantu.platform.managers.notification.NotificationManager;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.WebUtils;

@RestController
@RequestMapping("/notification-centre/notification")
public class NotificationController {
	
	
        
        @Autowired
        NotificationManager notificationManager;
        
        @Autowired
        HttpSessionUtils sessionUtils;
	
        
        //TODO: Check if needed
//	@RequestMapping(value = "/sendNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String sendNotification(@RequestBody String request){
//		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/sendNotification", HttpMethod.POST, request);
//		return resp.getEntity(String.class);
//	}
//        
//        @RequestMapping(value = "/createNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public BaseResponse createNotification(@RequestBody CreateNotificationRequest request) throws VException {
//                return notificationManager.createNotification(request);
//	}
        
        
	@Deprecated
	@RequestMapping(value = "/saveMobileNotificationData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String saveMobileNotificationData(@RequestBody String request) throws VException{
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/saveMobileNotificationData", HttpMethod.POST, request);
		return resp.getEntity(String.class);
	}
//	
//	@RequestMapping(value = "/getMobileNotificationData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String getMobileNotificationData(@RequestBody String request){
//		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/getMobileNotificationData", HttpMethod.POST, request);
//		return resp.getEntity(String.class);
//	}

	@Deprecated
	@RequestMapping(value = "/addMobileRegistrationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String addMobileRegistrationId(@RequestBody RegisterUserDeviceRequest request) throws VException{
                sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/addMobileRegistrationId", HttpMethod.POST, new Gson().toJson(request));
		return resp.getEntity(String.class);
	}

	@Deprecated
	@RequestMapping(value = "/removeMobileRegistrationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String removeMobileRegistrationId(@RequestBody RegisterUserDeviceRequest request) throws VException{
                sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
		String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/removeMobileRegistrationId", HttpMethod.POST, new Gson().toJson(request));
		return resp.getEntity(String.class);
	}
        
        
        
    @Deprecated
	@RequestMapping(value = "/getNotifications", method = RequestMethod.GET)
	@ResponseBody
	public GetNotificationsRes getNotifications(GetNotificationsReq req) throws VException {
                req.verify();
                sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
                return notificationManager.getNotifications(req);
	}
        
    @Deprecated
	@RequestMapping(value = "/markSeen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicRes markSeen(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
                return notificationManager.markSeen(req);
	}
        
    @Deprecated
	@RequestMapping(value = "/markSeenByEntity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes markSeenByEntity(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
                return notificationManager.markSeenByEntity(req);
	}
        
    @Deprecated
	@RequestMapping(value = "/setMarkSeen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes setMarkSeen(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
                return notificationManager.setMarkSeen(req);
	}
}
