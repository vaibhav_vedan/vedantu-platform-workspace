package com.vedantu.platform.controllers.vedantudata;

import com.vedantu.User.request.DemoTriggerCallToAgentReq;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.platform.managers.click2call.ExotelPhoneCallManager;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vedantudata")
public class DemoActivityPostCreateController {

    @Autowired
    private ExotelPhoneCallManager exotelPhoneCallManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DemoActivityPostCreateController.class);

    @RequestMapping(value = "/trigger-call-to-agent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerCallTOAgent(@RequestBody DemoTriggerCallToAgentReq req) throws ForbiddenException {

        exotelPhoneCallManager.connectCall(req.getFromNumber(),req.getToNumber(),req.getContextId(),ExotelPhoneCallManager.CALL_CONTEXT_HOME_DEMO,req.getCallingUserId());
    }
}
