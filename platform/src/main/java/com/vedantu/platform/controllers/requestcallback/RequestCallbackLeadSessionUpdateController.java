package com.vedantu.platform.controllers.requestcallback;

import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.requestcallback.request.GetRequestCallBacksDetailsReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("/requestcallback/sessionUpdation")
public class RequestCallbackLeadSessionUpdateController {

	@Autowired
	RequestCallbackDao requestCallbackDao;
	
	@Autowired
	RequestCallbackManager requestCallbackManager;
	
	@Autowired
	LogFactory logFactory;
	
	
	
	Logger logger = logFactory.getLogger(RequestCallbackLeadSessionUpdateController.class);
	
		
	@ApiOperation(value = "get to be checked requests", notes = "get to be checked requests")
	@RequestMapping(value = "/toBeChecked", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<RequestCallBackDetails> toBeChecked(){
		
		return requestCallbackManager.toBeChecked();
	}
	
	@ApiOperation(value = "get to be checked requests all", notes = "get to be checked requests")
	@RequestMapping(value = "/toBeCheckedAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<RequestCallBackDetails> toBeCheckedAll(@RequestParam("teacherId") Long teacherId){
		
		//Integer daysToUpdateAssignee = ConfigUtils.INSTANCE.getIntValue("vedantu.platform.requestcallback.daysToUpdateAssignee");

		try {
			Long currentTime = System.currentTimeMillis();
			GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq = new GetRequestCallBacksDetailsReq(null, null, teacherId, null, null, null, null, null, null, null, null, currentTime, null, null);
			
			List<RequestCallBackDetails> requestCallBackDetailsList =  requestCallbackDao.get(getRequestCallBacksDetailsReq);
			
			List<RequestCallBackDetails> toBeChecked = new ArrayList<RequestCallBackDetails>();
			
			for(RequestCallBackDetails requestCallBackDetails: requestCallBackDetailsList){
				//if(requestCallBackDetails.getSessionId()==null || requestCallBackDetails.getSubscriptionId() == null){
					toBeChecked.add(requestCallBackDetails);
				//}
			}
			
			return toBeChecked;
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "updateCallbackListWithSessionData", notes = "updateCallbackListWithSessionData")
	@RequestMapping(value = "/updateCallbackListWithSessionData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public Boolean updateCallbackListWithSessionData(@RequestBody List<RequestCallBackDetails> requestCallBackDetailsList){
		
		try {
			logger.debug("Request Received for updating request callbacks :" + requestCallBackDetailsList.toString());
			
			requestCallbackDao.updateAll(requestCallBackDetailsList);
			return true;
		} catch (Exception e) {
			logger.error("request Callback Updation failed:" + e.getMessage());
			throw e;
		}
	}

}
