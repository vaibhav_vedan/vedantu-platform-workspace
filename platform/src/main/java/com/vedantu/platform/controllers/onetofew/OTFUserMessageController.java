package com.vedantu.platform.controllers.onetofew;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.request.GetUserMessageReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import javax.annotation.PostConstruct;

@RestController
@RequestMapping("onetofew/userMessage")
public class OTFUserMessageController {

	

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(OTFUserMessageController.class);

	// one to few end point is configured to contain / at the end
	private String ontofewEndpoint;
        
        @PostConstruct
        public void init() {
            ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT")+"/";
        }        
                

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public String userMessage(HttpServletRequest request) throws IOException, VException {
		String requestBody = PlatformTools.getBody(request);
		logger.info("Request body : " + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "userMessage", HttpMethod.POST, requestBody);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);

		// TODO : Need to send email - OTF_USER_MESSAGE

		return jsonString;
	}

	@Deprecated
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getUserMessageById(@PathVariable("id") String id) throws Exception {
		logger.info("Request:" + id);
		ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "userMessage/" + id, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String getUserMessages(GetUserMessageReq getUserMessageReq) throws VException, IllegalArgumentException, IllegalAccessException {
		String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getUserMessageReq);
		String getUserMessageUrl = ontofewEndpoint + "userMessage";
		if (!StringUtils.isEmpty(queryString)) {
			getUserMessageUrl += "?" + queryString;
		}

		ClientResponse resp = WebUtils.INSTANCE.doCall(getUserMessageUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		return jsonString;
	}
}
