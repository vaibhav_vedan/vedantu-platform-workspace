/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.vedantu.exception.VException;
import com.vedantu.platform.managers.SchoolManager;
import com.vedantu.platform.request.GetSchoolsReq;
import com.vedantu.platform.response.GetSchoolsRes;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/school")
public class SchoolController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SchoolController.class);

    @Autowired
    SchoolManager schoolManager;

    @RequestMapping(value = "/getSchools", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetSchoolsRes getSchools(GetSchoolsReq req) throws VException {
//    	return new GetSchoolsRes();
//    	TODO : uncomment post isl
        return schoolManager.getSchools(req);
    }
}
