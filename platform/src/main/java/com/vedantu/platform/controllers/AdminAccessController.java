/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.AdminAccessManager;
import com.vedantu.platform.mongodbentities.AdminAccess;
import com.vedantu.platform.request.AdminAccessReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.Arrays;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/adminaccess/adminFeatures")
public class AdminAccessController {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(MiscController.class);

    @Autowired
    private AdminAccessManager adminAccessManager;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public AdminAccess getFeatures(@RequestParam("adminId") Long adminId) throws VException {
        return adminAccessManager.getAdminAccess(adminId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public AdminAccess addAdminAccess(@RequestBody AdminAccessReq req) throws Exception {        
        return adminAccessManager.createUpdate(req);
    }
}
