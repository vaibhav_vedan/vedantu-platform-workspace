package com.vedantu.platform.controllers.onetofew;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.request.BuyItemsReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GetOTFInstalmentReminderRes;
import com.vedantu.onetofew.request.CheckOrJoinBatchAfterRegReq;
import com.vedantu.onetofew.request.CreateTrialEnrollmentReq;
import com.vedantu.onetofew.request.EndOTFEnrollmentReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.subscription.response.CheckIfRegFeePaidRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;

@RestController
@RequestMapping("onetofew/enroll")
public class OTFEnrollmentController {

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private UserManager userManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFEnrollmentController.class);

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public EnrollmentPojo enrollment(@RequestBody OTFEnrollmentReq enrollmentReq) throws IOException, VException {
        logger.error(" enrollment req directly " + enrollmentReq.toString());
        // Handle authentication
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (!Role.ADMIN.equals(sessionData.getRole())) {
            enrollmentReq.setUserId(String.valueOf(sessionData.getUserId()));
            enrollmentReq.setRole(sessionData.getRole());
        } else if (StringUtils.isEmpty(enrollmentReq.getUserId()) && StringUtils.isNotEmpty(enrollmentReq.getEmail())) {
            User user = userManager.getUserByEmail(enrollmentReq.getEmail());
            if (user != null) {
                enrollmentReq.setUserId(user.getId().toString());
                enrollmentReq.setRole(user.getRole());
            }
        }

        return enrollmentManager.enrollment(enrollmentReq);
    }

    @RequestMapping(value = "/markStatus", method = RequestMethod.POST)
    @ResponseBody
    public EnrollmentPojo markStatus(@RequestBody OTFEnrollmentReq enrollmentReq) throws IOException, VException {
        logger.info(" enrollment req " + enrollmentReq.toString());
        // Handle authentication
        sessionUtils.checkIfAllowed(Long.parseLong(enrollmentReq.getUserId()), null, true);
//        if("ACTIVE".equalsIgnoreCase(enrollmentReq.getStatus())){
//            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED,"Marking status active not allowed in this flow");
//        }

        return enrollmentManager.markStatus(enrollmentReq);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String getEnrollment(HttpServletRequest request) throws VException {
        String queryString = request.getQueryString();
        logger.info("queryString - " + queryString);
        String getEnrollmentsUrl = subscriptionEndPoint + "enroll";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }


    @CrossOrigin
    @RequestMapping(value = "/enrollments", method = RequestMethod.GET)
    @ResponseBody
    public String getEnrollmentsList(HttpServletRequest request) throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData == null) {
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "user not logged in");
        }
        String addQuery = "";
        Role role = sessionData.getRole();
        if (((Role.STUDENT.equals(role)
                || Role.TEACHER.equals(role)))) {
            if (StringUtils.isEmpty(request.getParameter("userId"))
                    && StringUtils.isEmpty(request.getParameter("callingUserId"))) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId not sent");
            } else if (StringUtils.isEmpty(request.getParameter("userId"))
                    && StringUtils.isNotEmpty(request.getParameter("callingUserId"))) {
                addQuery += "&userId=" + Long.parseLong(request.getParameter("callingUserId"));
            }

        }
        if (!(Role.ADMIN.equals(role) || Role.STUDENT_CARE.equals(role))) {
            String userId = request.getParameter("userId");
            if (StringUtils.isEmpty(userId)) {
                userId = request.getParameter("callingUserId");
            }
            sessionUtils.checkIfAllowed(Long.parseLong(userId), null, Boolean.TRUE);
        }
        String queryString = request.getQueryString() + addQuery;
        logger.info("queryString - " + queryString);
        String getEnrollmentsUrl = subscriptionEndPoint + "enroll/enrollments";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @RequestMapping(value = "/createTrialEnrollment", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse createTrialEnrollment(@RequestBody CreateTrialEnrollmentReq req)
            throws VException {
        sessionUtils.checkIfAllowed(req.getUserId(), Role.STUDENT, Boolean.TRUE);
        return enrollmentManager.createTrialEnrollment(req);
    }

    @RequestMapping(value = "/checkTrialEnrollments", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse checkTrialEnrollments(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            payload.put("start", 0);
            payload.put("size", 100);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CHECK_TRIAL_ENROLLMENT_DUES, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/updateCalendar", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse updateCalendar(@RequestBody EnrollmentPojo oTFEnrollmentInfo)
            throws VException {
        // Update calendar
        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentInfo", oTFEnrollmentInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_UPDATE_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/checkIfRegFeePaidForCourse", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CheckIfRegFeePaidRes checkIfRegFeePaidForCourse(@RequestBody CheckOrJoinBatchAfterRegReq req)
            throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.STUDENT, Boolean.TRUE);
        return enrollmentManager.checkIfRegFeePaidForCourse(req);
    }

    @RequestMapping(value = "/enrollToRegularState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo enrollToRegularState(@RequestBody BuyItemsReq buyItemsReq) throws VException, IOException, AddressException {
        sessionUtils.checkIfAllowed(buyItemsReq.getCallingUserId(), null, Boolean.FALSE);
        return enrollmentManager.enrollToRegularState(buyItemsReq);
    }

    @RequestMapping(value = "/getEnrollmentById/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getEnrollmentById(@PathVariable("id") String id) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("ENTRY : id is " + id);
        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id is null : " + id);
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "enroll/getEnrollmentById?id=" + id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    @RequestMapping(value = "/getOTFFirstInstalmentReminderList", method = RequestMethod.GET)
    @ResponseBody
    public List<GetOTFInstalmentReminderRes> getOTFFirstInstalmentReminderList(@RequestParam(value = "startTime") Long startTime,
            @RequestParam(value = "endTime") Long endTime) throws VException, InterruptedException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        List<GetOTFInstalmentReminderRes> enrollmentInfos = enrollmentManager.getOTFFirstInstalmentReminderList(startTime, endTime);
        return enrollmentInfos;
    }

    @RequestMapping(value = "/getEnrollments", method = RequestMethod.GET)
    @ResponseBody
    public List<EnrollmentPojo> getEnrollments(GetEnrollmentsReq req)
            throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.STUDENT, Boolean.TRUE);
        if (Role.STUDENT.equals(sessionUtils.getCallingUserRole()) && req.getUserId() == null && req.getCallingUserId() != null) {
            req.setUserId(req.getCallingUserId());
        }
        return enrollmentManager.getEnrollments(req);
    }

}
