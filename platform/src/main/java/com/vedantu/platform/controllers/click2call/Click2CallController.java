/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.click2call;

import com.google.common.io.CharStreams;
import com.vedantu.User.response.GetUserProfileRes;
import com.vedantu.exception.NotFoundException;
import com.vedantu.platform.mongodbentities.click2call.PhoneCallMetadata;
import com.vedantu.platform.managers.click2call.ExotelPhoneCallManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/click2call")
public class Click2CallController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(Click2CallController.class);

    @Autowired
    ExotelPhoneCallManager exotelPhoneCallManager;

    @RequestMapping(value = "/updateExotelCallStatus", method = RequestMethod.POST)
    public void updateExotelCallStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //TODO: Right now this is a public API, check this later
        Map<String, Object> paramValues = getMultipartData(req);
        if (paramValues.get("CallSid") == null) {
            return;
        }
        PhoneCallMetadata phoneMetadata = exotelPhoneCallManager.updatePhoneCallMetadata((String) paramValues.get("CallSid"),
                paramValues, 0L);
        resp.getWriter().print(phoneMetadata != null);
    }

    private Map<String, Object> getMultipartData(HttpServletRequest req)
            throws IOException {

        BufferedReader reader = req.getReader();
        Map<String, Object> values = new HashMap<String, Object>();
        List<String> lines = CharStreams.readLines(reader);

        logger.info("lines : " + lines + ", line size:" + lines.size());
        String cd = lines.get(0);
        String pName = "";
        String pLine = "";
        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i).trim();
            if (line.startsWith("Content-Disposition")) {
                pName = line.substring(line.indexOf("=") + 1).replace("\"", "");
            }
            if (line.equals(cd)) {
                values.put(pName, pLine);
            }
            pLine = line;
        }
        logger.info("getMultipartData valueMap : " + values);
        return values;
    }
    
    @RequestMapping(value = "/getPhonecallMetadata", method = RequestMethod.GET)
    @ResponseBody
    public List<PhoneCallMetadata> getPhonecallMetadata(@RequestParam(value = "contextId") String contextId, @RequestParam(value = "contextType") String contextType) throws NotFoundException {
        return exotelPhoneCallManager.getPhonecallMetadata(contextId, contextType);
    }

}
