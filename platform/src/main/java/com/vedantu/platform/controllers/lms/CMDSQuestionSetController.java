package com.vedantu.platform.controllers.lms;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.lms.CMDSTestManager;
import com.vedantu.platform.managers.lms.LMSManager;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionUtils;
import inti.ws.spring.exception.client.BadRequestException;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@CrossOrigin
@RestController
@RequestMapping(value = "/cmds/questionSet")
public class CMDSQuestionSetController {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSQuestionSetController.class);

	private static String LMS_ENDPOINT;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private CMDSTestManager cmdsTestManager;

	@Autowired
	private LMSManager lmsManager;

	@Autowired
	RedisDAO redisDAO;

	public static final Map<String, String> inMemoryMap = new HashMap<>();

	@PostConstruct
	private void init()
	{
		LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	}

	//Reviewer: Arun Dhwaj: In next release, try to send request params in post body as much as possible.
	@RequestMapping(value = "/uploadQuestionSet", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
	public void uploadQuestionSet(@RequestParam(name = "file", required = true) MultipartFile multipart,
								  @RequestParam(name = "questionSetFileName", required = true) String questionSetFileName,
								  @RequestParam(name = "userId", required = true) String userId,
								  @RequestParam(name = "type", required = false)VedantuRecordState type,
								  HttpServletRequest request,
								  HttpServletResponse response) throws VException, IllegalStateException, IOException
	{
		logger.info("Request : " + questionSetFileName);
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);

		File file = CommonUtils.multipartToFile(multipart);
		String reqQueryString = request.getQueryString();


		try {
			CompletableFuture<ClientResponse> clientResponseCompletableFuture = CompletableFuture.supplyAsync(() -> {

				ClientResponse resp;

				FileDataBodyPart filePart = new FileDataBodyPart("file", file);
				FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
				formDataMultiPart.bodyPart(filePart);

				resp = WebUtils.INSTANCE.doPost(
						LMS_ENDPOINT + "cmds/questionSet/uploadQuestionSet?" + reqQueryString, formDataMultiPart, true);



				return resp;
			});

			ClientResponse resp = clientResponseCompletableFuture.get();
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			InputStream is = resp.getEntityInputStream();
			ServletOutputStream sos = response.getOutputStream();
			IOUtils.copy(is, sos);
		}
		catch(Exception ex)
		{
			logger.warn("Error in upload question set" + ex);
		}

		finally
		{
			file.delete();
		}

		/*lmsManager.uploadQuestionSetAsync(multipart,request);*/

	}

	@RequestMapping(value = "/confirmQuestionSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void confirmQuestionSet(HttpServletRequest request, HttpServletResponse response)
			throws VException, IllegalStateException, IOException, ClassNotFoundException
	{
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
		String requestBody = PlatformTools.getBody(request);
		logger.info("Request body : " + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/questionSet/confirmQuestionSet",
				HttpMethod.POST, requestBody, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		InputStream is = resp.getEntityInputStream();
		IOUtils.copy(is, response.getOutputStream());
	}

	@RequestMapping(value = "/getQuestionSetById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getQuestionSetById(HttpServletRequest request, HttpServletResponse response)
			throws VException, IOException {
		logger.info("Request : " + request.getQueryString());
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
		ClientResponse resp = WebUtils.INSTANCE.doCall(
				LMS_ENDPOINT + "cmds/questionSet/getQuestionSetById?" + request.getQueryString(), HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		InputStream is = resp.getEntityInputStream();
		IOUtils.copy(is, response.getOutputStream());
	}

	@RequestMapping(value = "/getQuestionSets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getQuestionSets(HttpServletRequest request, HttpServletResponse response)
			throws VException, IOException {
		logger.info("Request : " + request.getQueryString());
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
		ClientResponse resp = WebUtils.INSTANCE.doCall(
				LMS_ENDPOINT + "cmds/questionSet/getQuestionSets?" + request.getQueryString(), HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		InputStream is = resp.getEntityInputStream();
		IOUtils.copy(is, response.getOutputStream());
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteQuestionSet(HttpServletRequest request) throws VException, BadRequestException, IOException {
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);

		String requestBody = PlatformTools.getBody(request);
		logger.info("Request body : " + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/questionSet/delete", HttpMethod.POST,
				requestBody, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	public static byte[] convertToString(MultipartFile file) throws IOException {
		return file.getBytes();
	}
}
