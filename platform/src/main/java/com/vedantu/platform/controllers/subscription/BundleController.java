package com.vedantu.platform.controllers.subscription;

import java.util.*;

import com.vedantu.User.TeacherInfo;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.pojo.BundleEntityInfo;
import com.vedantu.platform.pojo.subscription.TrialReq;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.subscription.pojo.bundle.Classes;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.response.BundleEnrollmentResp;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.pojo.TestAndAttemptDetails;
import com.vedantu.onetofew.request.UpdateWebinarSessionsAttendenceReq;
import com.vedantu.onetofew.request.CancelOTFWebinarSessionsReq;
import com.vedantu.onetofew.request.GetOTFWebinarSessionsReq;
import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.VideoDetails;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import io.swagger.annotations.ApiOperation;

/**
 * Created by somil on 09/05/17.
 */
@RestController
@RequestMapping("/bundle")
public class BundleController {

	@Autowired
	private BundleManager bundleManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(BundleController.class);

	@Deprecated
	@RequestMapping(value = "/addEditBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BundleInfo addEditBundle(@RequestBody AddEditBundleReq addEditBundleReq) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.addEditBundle(addEditBundleReq);
	}

	@Deprecated
	@RequestMapping(value = "/addEditBundleWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BundleInfo addEditBundleWebinar(@RequestBody AddEditBundleWebinarReq addEditBundleWebinarReq)
			throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.addEditBundleWebinar(addEditBundleWebinarReq);
	}

	@RequestMapping(value = "/addEditBundleSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public OTFSessionPojoUtils addEditBundleSession(@RequestBody AddEditBundleSessionReq addEditBundleSessionReq)
			throws Exception {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.addEditBundleSession(addEditBundleSessionReq);
	}

	@RequestMapping(value = "/attendBundleSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<OTFSessionPojoUtils> attendBundleSession(@RequestBody UpdateWebinarSessionsAttendenceReq attendWebinarSessionsReq)
			throws VException {
		return bundleManager.attendWebinarSession(attendWebinarSessionsReq);
	}

	@RequestMapping(value = "/cancelWebinarSession", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<OTFSessionPojoUtils> cancelWebinarSession(@RequestBody CancelOTFWebinarSessionsReq req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return bundleManager.cancelWebinarSession(req);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BundleInfo getBundle(@PathVariable("id") String id,
			@ModelAttribute GetBundlesReq req) throws VException {
//		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.getBundle(id, req);
	}

	@RequestMapping(value = "/public/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BundleInfo getPublicBundle(@PathVariable("id") String id,
			@ModelAttribute GetBundlesReq req) throws VException {
//		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
		if(httpSessionData!=null) {
			req.setCallingUserId(httpSessionData.getUserId());
			req.setCallingUserRole(httpSessionData.getRole());
		}
		req.setCheckAccessControl(true);
		return bundleManager.getBundle(id, req);
	}

	@Deprecated
	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BundleInfo> getBundleInfos(@ModelAttribute GetBundlesReq req) throws VException {
//		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.getBundleInfos(req);
	}

	@Deprecated
	@RequestMapping(value = "/get/public", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BundleInfo> getPublicBundleInfos(@ModelAttribute GetBundlesReq req) throws VException {
		return bundleManager.getPublicBundleInfos(req);
	}

	@RequestMapping(value = "/getWebinarSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<OTFSessionPojoUtils> getWebinarSessions(@ModelAttribute GetOTFWebinarSessionsReq req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return bundleManager.getBundleSessions(req);
	}

	@Deprecated
	@RequestMapping(value = "/getVideoDetails/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VideoDetails getVideoDetails(@PathVariable("id") Long id) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.getVideoDetails(id);
	}

	@RequestMapping(value = "/pre-signed-url", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ApiOperation(value = "Generate a PreSigned url for AWS operation", notes = "Boolean uploadToAWS()")
	public PlatformBasicResponse getPreSignedUrl(@RequestParam(value = "contentType") String contentType,
			@RequestParam(value = "extension") String extension) throws Exception {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.getPreSignedUrl(contentType, extension);
	}


	@Deprecated
	@RequestMapping(value = "/getEnrolledBundles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BundleEnrollmentResp> getEnrolledBundles(@RequestParam(value = "userId") String userId) throws VException {
		sessionUtils.checkIfAllowed(Long.parseLong(userId), null, true);
		return bundleManager.getEnrolledBundles(userId);
	}

	@RequestMapping(value = "/getEnrolments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BundleEnrolmentInfo> getEnrolments(@ModelAttribute GetBundleEnrolmentsReq req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return bundleManager.getEnrolments(req);
	}

	@RequestMapping(value = "/checkEnrolment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BundleEnrolmentInfo> checkEnrolment(@ModelAttribute CheckBundleEnrollmentReq checkBundleEnrollmentReq) throws VException {
//		sessionUtils.checkIfAllowed(null, null, true);
		return bundleManager.checkEnrolment(checkBundleEnrollmentReq);
	}



	@RequestMapping(value = "/getBundleTestDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, TestAndAttemptDetails> getBundleTestDetails(@ModelAttribute GetBundleTestDetailsReq req) throws VException {
		//sessionUtils.checkIfAllowed(req.getUserId(), null, true);
		HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
		if(httpSessionData!=null) {
			req.setCallingUserId(httpSessionData.getUserId());
			req.setCallingUserRole(httpSessionData.getRole());
		}
		return bundleManager.getBundleTestDetails(req);
	}

	@RequestMapping(value = "/addVideoAnalyticsData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse addVideoAnalyticsData(@RequestBody AddVideoAnalyticsDataReq addVideoAnalyticsDataReq) throws VException {
		//sessionUtils.checkIfAllowed(addVideoAnalyticsDataReq.getUserId(), null, false);
		return bundleManager.addVideoAnalyticsData(addVideoAnalyticsDataReq);
	}


	@RequestMapping(value = "/enrolViaPass", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BundleEnrolmentInfo enrolViaPass(@RequestBody EnrolViaPassReq req) throws VException {
		//sessionUtils.checkIfAllowed(addVideoAnalyticsDataReq.getUserId(), null, false);
		return bundleManager.enrolViaPass(req);
	}

	@RequestMapping(value = "/requestTrail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public void requestTrail(@RequestBody TrialReq trialReq) throws VException {
		//sessionUtils.checkIfAllowed(addVideoAnalyticsDataReq.getUserId(), null, false);
		if(StringUtils.isEmpty(trialReq.getUserId())){
			trialReq.setUserId(trialReq.getCallingUserId().toString());
		}
		bundleManager.requestTrail(trialReq);
	}


	//
	// @RequestMapping(value = "/basicInfo/{id}", method = RequestMethod.GET,
	// produces = MediaType.APPLICATION_JSON_VALUE)
	// @ResponseBody
	// public CoursePlanBasicInfo getCoursePlanBasicInfo(@PathVariable("id")
	// String id) throws NotFoundException {
	// return coursePlanManager.getCoursePlanBasicInfo(id);
	// }

}
