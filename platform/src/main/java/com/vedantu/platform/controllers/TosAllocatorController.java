package com.vedantu.platform.controllers;


import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.response.ExportTosTeacherSession;
import com.vedantu.platform.dao.TosAllocatorDAO;
import com.vedantu.platform.mongodbentities.TosEntry;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "tos allocator Controller")
public class TosAllocatorController {

	@Autowired
	private TosAllocatorDAO tosAllocatorDAO;

	@Autowired
	private LogFactory logFactory;

	

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(TosAllocatorController.class);

	@RequestMapping(value = "/tos/uploadData", method = RequestMethod.POST)
	@ApiOperation(value = "upload tos data to mongo", notes = "upload tos data to mongo")
	public BasicRes uploadTosData(@RequestBody String tosUploadData) throws Exception {
		BasicRes basicRes = new BasicRes();
		Gson gson = new Gson();
		ExportTosTeacherSession exportTosTeacherSession = gson.fromJson(tosUploadData, ExportTosTeacherSession.class);
		TosEntry tosEntry = new TosEntry(exportTosTeacherSession.getTeacherName(),
				exportTosTeacherSession.getTeacherId(), exportTosTeacherSession.getToscount(),
				exportTosTeacherSession.getFeedback(), exportTosTeacherSession.getNpsLast2weeks(),
				exportTosTeacherSession.getDate());
		tosAllocatorDAO.create(tosEntry);
		basicRes.setSuccess(true);
		return basicRes;
	}
}
