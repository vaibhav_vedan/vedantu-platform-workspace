/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.user;

import com.vedantu.User.response.ProcessVerificationResponse;
import com.vedantu.exception.VException;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.platform.managers.user.VerificationTokenManager;
import com.vedantu.util.LogFactory;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/user/token")
public class VerificationTokenController {
        @Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(VerificationTokenController.class);
        
        @Autowired
        VerificationTokenManager tokenManager;
        
        @Autowired
        HttpSessionUtils sessionUtils;

	@RequestMapping(value = "/processToken", method = RequestMethod.GET)
	@ResponseBody
	public ProcessVerificationResponse processToken(@RequestParam(value = "tokenCode") String tokenCode,
			@RequestParam(value = "password", required = false) String password,
                        @RequestParam(value = "reloginRequired", required = false) boolean reloginRequired,
                        HttpServletRequest request) throws VException, IOException {
                sessionUtils.isAllowedApp(request);
                return tokenManager.processToken(tokenCode, password,reloginRequired);
	}
        
}
