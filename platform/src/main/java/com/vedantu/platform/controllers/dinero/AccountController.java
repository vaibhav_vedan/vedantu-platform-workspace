package com.vedantu.platform.controllers.dinero;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.request.SalesTransactionReq;
import com.vedantu.dinero.request.SubmitCashChequeReq;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.dinero.request.TransferToVedantuDefaultAccountReq;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.managers.dinero.AccountManager;
import com.vedantu.platform.request.dinero.AddOrDeductAccountBalanceReq;
import com.vedantu.platform.request.dinero.GetAccountInfoReq;
import com.vedantu.platform.request.dinero.GetFreebiesAccountInfoReq;
import com.vedantu.platform.request.dinero.GetRechargeHistoryReq;
import com.vedantu.platform.request.dinero.ReleaseLockedBalanceReq;
import com.vedantu.platform.response.dinero.GetAccountBalanceResponse;
import com.vedantu.platform.response.dinero.GetAccountInfoRes;
import com.vedantu.platform.response.dinero.GetFreebiesAccountInfoRes;
import com.vedantu.platform.response.dinero.GetRechargeHistoryRes;
import com.vedantu.platform.response.dinero.SalesTransactionInfo;
import com.vedantu.platform.response.dinero.SubmitCashChequeRes;
import com.vedantu.platform.response.dinero.TransferFromFreebiesAccountRes;
import com.vedantu.platform.response.dinero.TransferToVedantuDefaultAccountRes;
import com.vedantu.platform.enums.HolderType;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import java.lang.reflect.InvocationTargetException;

@RestController
@RequestMapping("/dinero/account")
public class AccountController {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AccountController.class);

    @Autowired
    AccountManager accountManager;

    @Deprecated
    @RequestMapping(value = "/getRechargeHistory", method = RequestMethod.GET)
    @ResponseBody
    public GetRechargeHistoryRes getRechargeHistory(GetRechargeHistoryReq getRechargeHistoryReq) throws VException {
        getRechargeHistoryReq.verify();
        sessionUtils.checkIfAllowed(getRechargeHistoryReq.getUserId(), null, Boolean.FALSE);
        logger.info("Entering " + getRechargeHistoryReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/getRechargeHistory", HttpMethod.POST,
                new Gson().toJson(getRechargeHistoryReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetRechargeHistoryRes response = new Gson().fromJson(jsonString, GetRechargeHistoryRes.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    // @RequestMapping(value = "/rechargeFreebies", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public RechargeFreebiesAccountRes rechargeFreebies(
    // @RequestBody RechargeFreebiesAccountReq rechargeFreebiesAccountReq) throws
    // VException {
    //
    // logger.info("Entering " + rechargeFreebiesAccountReq.toString());
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/rechargeFreebies", HttpMethod.POST,
    // new Gson().toJson(rechargeFreebiesAccountReq));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // RechargeFreebiesAccountRes response = new Gson().fromJson(jsonString,
    // RechargeFreebiesAccountRes.class);
    // logger.info("Exiting: " + response.toString());
    // return response;
    //
    // }
    @RequestMapping(value = "/transferFromFreebiesAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TransferFromFreebiesAccountRes transferFromFreebiesAccount(
            @RequestBody TransferFromFreebiesAccountReq transferFromFreebiesAccountReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        transferFromFreebiesAccountReq.setTransactionRefType(TransactionRefType.ACCOUNT_CREDIT);
        transferFromFreebiesAccountReq.setBillingReasonType(BillingReasonType.FREEBIES);
        transferFromFreebiesAccountReq.setAllowNegativeBalance(true);
        transferFromFreebiesAccountReq.setNoAlert(false);
        return accountManager.transferFromFreebiesAccount(transferFromFreebiesAccountReq);

    }

    @Deprecated
    @RequestMapping(value = "/submitCashCheque", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitCashChequeRes submitCashCheque(@RequestBody SubmitCashChequeReq submitCashChequeReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + submitCashChequeReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/submitCashCheque", HttpMethod.POST,
                new Gson().toJson(submitCashChequeReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        SubmitCashChequeRes response = new Gson().fromJson(jsonString, SubmitCashChequeRes.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    // @RequestMapping(value = "/transferFromVedantuDefaultAccount", method =
    // RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public TransferFromVedantuDefaultAccountRes
    // transferFromVedantuDefaultAccount(
    // @RequestBody TransferFromVedantuDefaultAccountReq
    // transferFromVedantuDefaultAccountReq) throws VException {
    //
    // logger.info("Entering " + transferFromVedantuDefaultAccountReq.toString());
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/transferFromVedantuDefaultAccount",
    // HttpMethod.POST, new Gson().toJson(transferFromVedantuDefaultAccountReq));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // TransferFromVedantuDefaultAccountRes response = new
    // Gson().fromJson(jsonString,
    // TransferFromVedantuDefaultAccountRes.class);
    // logger.info("Exiting: " + response.toString());
    // return response;
    //
    // }
//    int[] npBalances = {80061, 80925, 67365, 71990, 92057, 110079, 88906, 58785, 102571, 50100, 2263, 69106, 57073, 37925, 57278, 305879, 100046, 92266, 113395, 78908, 100946, 60148, 101695, 61973, 60837, 59128, 83047, 50434, 61846, 72246, 35097, 57075, 50062, 88949, 48696, 52591, 430617, 43084, 45000, 38901, 67674, 51437, 64366, 20488, 49341, 105213, 309556, 30014, 20081, 94187, 72541, 46633, 89207, 32689, 62744, 45820, 55443, 48421, 85683, 61437, 82434, 20320, 90476, 21000, 78790, 82830, 70289, 77517, 101287, 103843, 85391, 52272, 34604, 300603, 77439, 33406, 67145, 80091, 32098, -29885, 86007, 47132, 71440, 64470, 72138, 47622, 98844, 43041, 55976, 45000, 42670, 33325, 48497, 20260, 60513, 108257, 46573, 40761, 71177, 284075, 50100, 274508, 90139, 53650, 20763, 104323, 47483, 283360, 306037, 111119, 20759, 45507, 41545, 46055, 34212, 45000, 55608, 71265, 44469, 65412, 42580, 52633, 40100, 45000, 102449, 297100, 66409, 83421, 57467, 42800, 102122, 53054, 44043, 59629, 31083, 41690, 69040, 20507, 69445, 95278, 232593, 56586, 56602, 93439, 58389, 75487, 75385, 108861, 63503, 56172, 107752, 53231, 36034, 107408, 69364, 20350, 1402358, 52515, 91460, 108513, 59882, 57287, 89715, 80828, 308389, 83665, 32023, 105826, 45967, 44517, 89896, 76678, 88200, 66214, 130530, 78161, 20094, 65563, 80058, 39504, 41002, 20056, 77676, 106268, 50054, 58314, 57824, 46091, 83525, 76284, 63700, 70274, 106391, 265492, 106350, 81900, 64972, 57354, 86646, 108842, 33344, 265114, 43100, 56880, 50136, 59565, 101246, 85888, 46500, 78433, 62500, 100000, 65000, 59136, 40000, 71931, 56089, 45124, 50000, 42531, 95719, 41343, 62825, 279900, 268972, 45595, 40100, 81409, 42708, 75255, 37370, 45000, 103596, 53811, 71701, 80000, 43997, 56768, 104340, 88102, 18007, 54173, 47561, 21013, 86926, 292564, 74123, 73961, 317123, 62595, 87222, 46373, 20340, 1965893, 82799, 103280, 315280, 73904, 65133, 60000, 63617, 102000, 11021, 90000, 81606, 80884, 83506, 47062, 42219, 66370, 50100, 45200, 49900, 49900, 48228, 50100};

//    static List<Integer> accountIds = Arrays.asList(74024, 75150, 75200, 75266, 76601, 76752, 77354, 77686, 77737, 78764, 79370, 80068, 80781, 80799, 82060, 82226, 82551, 82974, 83166, 84286, 84660, 85047, 85127, 85824, 87146, 88164, 88386, 88807, 88894, 89202, 89770, 89962, 90587, 91125, 91404, 91516, 92042, 92896, 94515, 94742, 96350, 96450, 96744, 97535, 97722, 98010, 98681, 98940, 99706, 99818, 100669, 100900, 101088, 101893, 102092, 102636, 102711, 103460, 104910, 106992, 107246, 107457, 107669, 109730, 110234, 110650, 111804, 111916, 112368, 113543, 113796, 114350, 115081, 117030, 117498, 117715, 117848, 117890, 118277, 118368, 120687, 121656, 122322, 122990, 123145, 123593, 124045, 124798, 125089, 125119, 125360, 125780, 126008, 126694, 126776, 126889, 127620, 127623, 128242, 128685, 128954, 129651, 129865, 129930, 129989, 130096, 130339, 130633, 131237, 131624, 131718, 132639, 132730, 132882, 132991, 133207, 133536, 133724, 133858, 134351, 134808, 135579, 137172, 138228, 141086, 141470, 141979, 142165, 142665, 142759, 142893, 143352, 144654, 145148, 145782, 147081, 147107, 147960, 148151, 148692, 149026, 149190, 149529, 150104, 151817, 152035, 152581, 152684, 156018, 156835, 156858, 157535, 157751, 159940, 160151, 160585, 162015, 162056, 162520, 162739, 163250, 163306, 163314, 163433, 163808, 163913, 164265, 164287, 165897, 166494, 166830, 167245, 168543, 168775, 169096, 169361, 169649, 170595, 170752, 170799, 170939, 171129, 171311, 171331, 171390, 171519, 171554, 172133, 172145, 172189, 172191, 172473, 172480, 172615, 172763, 172866, 173219, 173241, 173456, 174324, 174423, 174682, 174712, 174814, 174898, 174928, 174959, 175121, 175469, 175503, 175554, 176070, 176197, 176982, 177545, 177907, 178320, 178717, 178919, 179208, 179259, 179596, 179685, 179697, 179921, 180107, 180108, 180147, 180349, 180671, 181066, 181890, 181977, 182195, 182644, 182818, 182832, 182845, 186940, 197682, 199393, 199415, 220802, 225463, 227648, 241141, 244325, 245097, 256428, 277108, 279830, 291704, 295917, 312041, 319514, 343415, 347219, 361644, 384092, 385460, 398243, 430136, 438464, 438950, 458618, 460760, 531321, 548657, 550107, 557894, 562987, 567766, 598774, 652262, 671944, 702333);

    @Deprecated
    @RequestMapping(value = "/getAccountInfo", method = RequestMethod.GET)
    @ResponseBody
    public GetAccountInfoRes getAccountInfo(GetAccountInfoReq getAccountInfoReq) throws VException {
        sessionUtils.checkIfAllowed(getAccountInfoReq.getUserId(), null, Boolean.TRUE);
        GetAccountInfoRes res = accountManager.getAccountInfo(getAccountInfoReq);
        return res;
    }

    @Deprecated
    @RequestMapping(value = "/releaseLockedBalance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountInfoRes releaseLockedBalance(@RequestBody ReleaseLockedBalanceReq releaseLockedBalanceReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + releaseLockedBalanceReq.toString());
        throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED, "Not allowed this anymore. Please talk to tech team");

    }

    @Deprecated
    @RequestMapping(value = "/getFreebiesAccountInfo", method = RequestMethod.GET)
    @ResponseBody
    public GetFreebiesAccountInfoRes getFreebiesAccountInfo(GetFreebiesAccountInfoReq getFreebiesAccountInfoReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + getFreebiesAccountInfoReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/getFreebiesAccountInfo",
                HttpMethod.POST, new Gson().toJson(getFreebiesAccountInfoReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetFreebiesAccountInfoRes response = new Gson().fromJson(jsonString, GetFreebiesAccountInfoRes.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @Deprecated
    @RequestMapping(value = "/transferToVedantuDefaultAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TransferToVedantuDefaultAccountRes transferToVedantuDefaultAccount(
            @RequestBody TransferToVedantuDefaultAccountReq transferToVedantuDefaultAccountReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + transferToVedantuDefaultAccountReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/transferToVedantuDefaultAccount",
                HttpMethod.POST, new Gson().toJson(transferToVedantuDefaultAccountReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        TransferToVedantuDefaultAccountRes response = new Gson().fromJson(jsonString,
                TransferToVedantuDefaultAccountRes.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    @Deprecated
    @RequestMapping(value = "/transferToVedantuAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String transferToVedantuAccount(@RequestBody String request) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + request);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/transferToVedantuAccount",
                HttpMethod.POST, request);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);

    }

    // TODO: Check if needed
    // @RequestMapping(value = "/createUserAccount", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public Account createUserAccount(@RequestBody User user) throws VException {
    //
    // return accountManager.createUserAccount(user);
    //
    // }
    //
    // @RequestMapping(value = "/createAccounts", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public List<Account> createAccounts(@RequestBody List<Account> accounts)
    // throws VException {
    //
    // logger.info("Request received to create accounts");
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/createAccounts",
    // HttpMethod.POST, new Gson().toJson(accounts));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // Type listAccountType = new TypeToken<List<Account>>() {
    // }.getType();
    //
    // List<Account> response = new Gson().fromJson(jsonString,listAccountType);
    // logger.info("Exiting: " + response.toString());
    // return response;
    //
    // }
    //
    // @RequestMapping(value = "/createVedantuAccounts", method = RequestMethod.GET,
    // produces = MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public Boolean createVedantuAccounts() throws VException {
    // logger.info("Request received to create vedantu accounts");
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/createVedantuAccounts",
    // HttpMethod.GET, null);
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // Boolean response = new Gson().fromJson(jsonString, Boolean.class);
    // logger.info("EXIT " + response);
    // return response;
    // }
    @Deprecated
    @RequestMapping(value = "/getAccountBalance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountBalanceResponse getAccountBalance(@RequestParam(value = "userId", required = true) String userId,
            @RequestParam(value = "holderType", required = true) HolderType holderType) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        GetAccountBalanceResponse response = new GetAccountBalanceResponse();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/account/getAccountBalance?userId=" + userId + "&holderType=" + holderType,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        response = new Gson().fromJson(jsonString, GetAccountBalanceResponse.class);
        logger.info("Exiting " + response.toString());
        return response;

    }

    // TODO: Check if needed
    // @RequestMapping(value = "/getAccountById", method = RequestMethod.GET,
    // produces = MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public Account getAccountById(@RequestParam(value = "id", required = true)
    // String id) throws VException {
    //
    // Account response = new Account();
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(
    // dineroEndpoint + "/account/getAccountById?id=" + id ,
    // HttpMethod.GET, null);
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // response = new Gson().fromJson(jsonString, Account.class);
    // logger.info("Exiting " + response.toString());
    // return response;
    //
    // }
    @Deprecated
    @RequestMapping(value = "/addOrDeductAccountBalance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AddOrDeductAccountBalanceReq addOrDeductAccountBalance(
            @RequestBody AddOrDeductAccountBalanceReq addOrDeductAccountBalanceReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/addOrDeductAccountBalance",
                HttpMethod.POST, new Gson().toJson(addOrDeductAccountBalanceReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        AddOrDeductAccountBalanceReq baseResponse = new Gson().fromJson(jsonString, AddOrDeductAccountBalanceReq.class);

        return baseResponse;

    }

    // TODO: Check if needed
    // //@CrossOrigin
    // @RequestMapping(value = "/accountVerificationSMS", method =
    // RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public Boolean accountVerificationSMS(@RequestBody
    // AccountVerificationSMSRequest accountVerificationSMSRequest) throws
    // VException {
    //
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/accountVerificationSMS", HttpMethod.POST,
    // new Gson().toJson(accountVerificationSMSRequest));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // Boolean baseResponse = new Gson().fromJson(jsonString, Boolean.class);
    //
    // return baseResponse;
    //
    // }
    //
    // @RequestMapping(value = "/deductAccountBalance", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public Boolean deductAccountBalance(@RequestBody DeductAccountBalanceRequest
    // deductAccountBalanceRequest) throws VException {
    //
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/deductAccountBalance", HttpMethod.POST,
    // new Gson().toJson(deductAccountBalanceRequest));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // Boolean baseResponse = new Gson().fromJson(jsonString, Boolean.class);
    //
    // return baseResponse;
    //
    // }
    //
    // @RequestMapping(value = "/createAccount", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public Account createAccount(@RequestBody Account createAccountRequest)
    // throws VException {
    //
    // String dineroEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint +
    // "/account/createAccount", HttpMethod.POST,
    // new Gson().toJson(createAccountRequest));
    //
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    //
    // Account baseResponse = new Gson().fromJson(jsonString, Account.class);
    //
    // return baseResponse;
    //
    // }
    @Deprecated
    @RequestMapping(value = "/getTransactions", method = RequestMethod.GET)
    @ResponseBody
    public List<SalesTransactionInfo> getTransactions(SalesTransactionReq salesTransactionReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        salesTransactionReq.verify();
        return accountManager.getTransactions(salesTransactionReq);
    }

    @RequestMapping(value = "/getOrders", method = RequestMethod.GET)
    @ResponseBody
    public String getOrders(SalesTransactionReq salesOrderReq)
            throws VException, IllegalAccessException, InvocationTargetException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        salesOrderReq.verify();
        return new Gson().toJson(accountManager.getOrders(salesOrderReq));
    }

}
