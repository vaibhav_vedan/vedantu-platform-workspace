package com.vedantu.platform.controllers.dinero;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.dinero.pojo.PaymentInvoiceInfo;
import com.vedantu.dinero.request.GetPaymentInvoiceReq;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.dinero.PaymentInvoiceManager;

@RestController
@RequestMapping("/dinero/invoice")
public class PaymentInvoiceController {

	@Autowired
	private PaymentInvoiceManager paymentInvoiceManager;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public PaymentInvoiceInfo getInvoiceById(@PathVariable("id") String invoiceId) throws VException {
		return paymentInvoiceManager.getPaymentInvoiceById(invoiceId);
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseBody
	public List<PaymentInvoiceInfo> getPaymentInvoices(@ModelAttribute GetPaymentInvoiceReq getPaymentInvoiceReq)
			throws VException {
		return paymentInvoiceManager.getPaymentInvoices(getPaymentInvoiceReq);
	}
}
