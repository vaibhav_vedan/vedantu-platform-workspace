package com.vedantu.platform.controllers.message;

import com.vedantu.exception.VException;
import com.vedantu.platform.managers.UserMessageManager;
import com.vedantu.platform.request.usermessage.SendQueryReq;
import com.vedantu.platform.response.usermessage.SendQueryRes;
import com.vedantu.platform.request.usermessage.SendUserMessageReq;
import com.vedantu.platform.response.usermessage.SendUserMessageRes;
import java.io.UnsupportedEncodingException;
import javax.mail.internet.AddressException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/usermessage")
public class UserMessageController {

    @Autowired
    UserMessageManager userMessageManager;

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SendUserMessageRes sendMessage(@Valid @RequestBody SendUserMessageReq sendUserMessageReq
    ) throws VException, AddressException, UnsupportedEncodingException {
        return userMessageManager.sendMessage(sendUserMessageReq);
    }

    @RequestMapping(value = "/sendQuery", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SendQueryRes sendQuery(@Valid @RequestBody SendQueryReq sendQueryReq) throws Exception {
        return userMessageManager.sendQuery(sendQueryReq);
    }
}
