package com.vedantu.platform.controllers.lms;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.lms.cmds.request.*;
import com.vedantu.lms.request.AddOrEditTestUserReq;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.lms.CMDSTestManager;
import com.vedantu.util.security.HttpSessionUtils;

import inti.ws.spring.exception.client.BadRequestException;
import inti.ws.spring.exception.client.ForbiddenException;
import inti.ws.spring.exception.server.InternalServerError;

@CrossOrigin
@RestController
@RequestMapping("cmds/test")
public class CMDSTestController {

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private CMDSTestManager testManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSTestController.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String createTest(HttpServletRequest request) throws VException, BadRequestException, IOException {
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
		return testManager.createTest(request);
	}

	@Deprecated
	@RequestMapping(value = "/getTests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getTests(HttpServletRequest request) throws VException, BadRequestException {
		sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
		return testManager.getTests(request);
	}

	@RequestMapping(value = "/getTestDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getTestDetails(HttpServletRequest request, HttpServletResponse response)
			throws VException, BadRequestException, IOException {
		//sessionUtils.checkIfAllowed(null, Role.TEACHER, true);
		testManager.getTestDetails(request, response);
	}

	@RequestMapping(value = "/shareOTFBatch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse shareOTFBatch(@RequestParam(name = "batchId", required = true) String batchId)
			throws VException, BadRequestException {
            throw new com.vedantu.exception.ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED,"Not Supported anymore, contact tech team");
            /*
		logger.info("Request : " + batchId);
		testManager.shareOTFBatch(batchId);
		return new BasicResponse();
*/
	}

	@Deprecated
	@RequestMapping(value = "/startAttempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void startAttempt(@RequestBody CMDSStartTestAttemptReq req, HttpServletResponse response)
			throws VException, BadRequestException, ForbiddenException, InternalServerError, IOException {
		logger.info("Request : " + req.toString());
		testManager.startCMDSTest(req, response);
	}


	@RequestMapping(value = "/addOrEditTestUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String addOrEditTestUser(@RequestBody AddOrEditTestUserReq req)
			throws VException, BadRequestException, MalformedURLException, ForbiddenException, InternalServerError {
		logger.info("Request : " + req.toString());
		return testManager.addOrEditTestUser(req);
	}

	@Deprecated
	@RequestMapping(value = "/submitQuestionAttempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String submitQuestionAttempt(@RequestBody BulkSubmitQuestionAttemptRequest req) throws VException {
		logger.info("Request : " + req.toString());
		return testManager.submitQuestionAttempt(req);
	}

	@Deprecated
	@RequestMapping(value = "/submitAttempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)

	public String submitAttempt(@RequestBody CMDSSubmitTestAttemptReq request)
			throws InternalServerErrorException, VException, IOException {
		return testManager.submitCMDSTest(request);
	}

	@RequestMapping(value = "/evaluateTest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Deprecated
	public String evaluateTest(@RequestBody CMDSEvaluateTestAttemptReq request)
			throws VException, IOException {
		sessionUtils.checkIfAllowed(null, Role.TEACHER, false);
		return testManager.evaluateCMDSTest(request);
	}

	@Deprecated
	@RequestMapping(value = "/getAttemptDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getAttemptDetails(@RequestParam(name = "attemptId", required = true) String attemptId, HttpServletResponse response)
			throws VException, BadRequestException, ForbiddenException, IOException {
		testManager.getTestAttemptById(attemptId, sessionUtils.getCallingUserId(),
				sessionUtils.getCallingUserRole(), response);
	}

	@RequestMapping(value = "/attemptViewed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String attemptViewed(@RequestParam(name = "attemptId", required = true) String attemptId,
			@RequestParam(name = "callingUserId", required = false) String callingUserId,
			@RequestParam(name = "callingUserRole", required = false) Role callingUserRole)
			throws VException, BadRequestException, ForbiddenException {
		return testManager.attemptViewed(attemptId, sessionUtils.getCallingUserId(), sessionUtils.getCallingUserRole());
	}


	@RequestMapping(value = "endTests", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public void endTests(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
						 @RequestBody String request) throws UnknownHostException, AddressException, NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException, VException {
		Gson gson = new Gson();
		gson.toJson(request);
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messgaetype.equals("SubscriptionConfirmation")) {
			String json = null;
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info("ClientResponse : " + resp.toString());
		} else if (messgaetype.equals("Notification")) {
			testManager.endTests();
		}
	}


	@RequestMapping(value = "/shareTest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public PlatformBasicResponse shareTest(@RequestBody ShareTestReq req) throws VException {
		logger.info("Request : " + req.toString());
		sessionUtils.checkIfAllowed(req.getTeacherId(), Role.TEACHER, true);
		return testManager.shareTest(req);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteTest(@RequestBody DeleteTestReq request) throws VException, BadRequestException, IOException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
		return testManager.deleteTest(request);
	}
}
