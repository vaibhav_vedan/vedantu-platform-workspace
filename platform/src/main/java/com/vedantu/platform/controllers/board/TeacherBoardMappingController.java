/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.board;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.platform.request.CreateTeacherBoardMappingReq;
import com.vedantu.platform.response.board.GetTeacherBoardMappingsRes;
import com.vedantu.platform.request.RemoveTeacherMappingReq;
import com.vedantu.platform.response.board.RemoveTeacherMappingRes;
import com.vedantu.platform.managers.board.TeacherBoardMappingManager;
import com.vedantu.platform.response.board.TeacherBoardMappingRes;
import com.vedantu.teacherBoardMappings.pojo.GetTeacherBoardMappingHierarchialRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/tbm")
public class TeacherBoardMappingController {
        @Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(TeacherBoardMappingController.class);
        
        @Autowired
        TeacherBoardMappingManager tbmManager;
        
        @Autowired
        HttpSessionUtils sessionUtils;

	@RequestMapping(value = "/createTeacherBoardMapping", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public TeacherBoardMappingRes createTeacherBoardMapping(@RequestBody CreateTeacherBoardMappingReq req) throws VException {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                return tbmManager.createTeacherBoardMapping(req);
	}
        
        
        @RequestMapping(value = "/removeTeacherMapping", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RemoveTeacherMappingRes removeTeacherMapping(@RequestBody RemoveTeacherMappingReq req) throws VException {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                return tbmManager.removeTeacherMapping(req);
	}
        
        
        @RequestMapping(value = "/getTeacherBoardMappingTree", method = RequestMethod.GET)
	@ResponseBody
	public GetTeacherBoardMappingsRes getTeacherBoardMappingTree(@RequestParam(value = "userId") Long userId) throws VException {
                return tbmManager.getTeacherBoardMappingTree(userId);
	}
        
        
        @RequestMapping(value = "/getHierarchialTeacherMappings", method = RequestMethod.GET)
	@ResponseBody
	public GetTeacherBoardMappingHierarchialRes getHierarchialTeacherMappings(@RequestParam(value = "userId") Long userId) throws VException {
                return tbmManager.getHierarchialTeacherMappings(userId);
	}
        
}

