package com.vedantu.platform.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;

import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.pojo.referral.model.ReferralStepBonus;
import com.vedantu.platform.pojo.referral.request.AddUpdateReferralStepsReq;
import com.vedantu.platform.pojo.referral.request.ProcessReferralBonusReq;
import com.vedantu.platform.pojo.referral.response.ReferralStepResponse;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@RestController
@RequestMapping("/referral")
public class ReferralController {

	@Autowired
	LogFactory logFactory;
	
	Logger logger = logFactory.getLogger(ReferralController.class);
	
	@Autowired
	ReferralManager referralManager;
        
        @Autowired
        UserManager userManager;
        
        @Autowired
        HttpSessionUtils sessionUtils;
        
        private final Gson gson = new Gson();

	@ApiOperation(value = "get all referral Steps", notes = "get all referral Steps")
	@RequestMapping(value = "/getAllReferralSteps", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<ReferralStepResponse> getAllReferralSteps(@RequestParam(value="callingUserId") Long callingUserId) throws Exception {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                try {
			List<ReferralStepResponse> res = referralManager
					.getAllReferralSteps();
			if (res != null) {
				return res;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR,
						"get Teacher lead statuses failed");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "get all referral Step Bonus", notes = "get all referral Step Bonus")
	@RequestMapping(value = "/getAllReferralStepBonuses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<ReferralStepBonus> getAllReferralStepBonuses() throws Exception {
		try {
			List<ReferralStepBonus> res = referralManager
					.getReferralStepBonuses();
			if (res != null) {
				return res;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR,
						"get all referral bonus steps failed");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "process referral Step Bonus", notes = "process referral Step Bonus")
	@RequestMapping(value = "/processReferralBonus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public void processReferralBonus(
			@RequestBody ProcessReferralBonusReq processReferralBonusReq)
			throws Exception {
		try {
			logger.info(System.currentTimeMillis());
                        //TODO: Discuss this with Parag
			Thread.sleep(15000);
			logger.info(System.currentTimeMillis());
			referralManager.processReferralBonus(
					processReferralBonusReq.getReferralStep(),
					processReferralBonusReq.getUserId(),
					processReferralBonusReq.getEntityId()
					//processReferralBonusReq.getReferrerUserId(),
					//processReferralBonusReq.getReferralCode()
                                        );
		} catch (Exception e) {
			throw e;
		}
	}

	@ApiOperation(value = "Add or update referral Step Bonuses", notes = "Add or update referral Step Bonuses")
	@RequestMapping(value = "/addUpdateReferralSteps", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<ReferralStepBonus> processReferralBonus(
			@RequestBody AddUpdateReferralStepsReq addUpdateReferralStepsReq)
			throws Exception {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		try {
			List<ReferralStepBonus> result = referralManager
					.addOrUpdateReferralStepBonus(
							addUpdateReferralStepsReq.getReferralStepBonus(),
							addUpdateReferralStepsReq.getCallingUserId());
			return result;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@ApiOperation(value = "get referral Step Bonus by Referral Policy", notes = "get referral Step Bonus by Referral Policy")
	@RequestMapping(value = "/getReferralStepBonusByReferralPolicy", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ReferralStepBonus getAllReferralStepBonuses(@RequestParam("referreeId") Long referreeId, @RequestParam("referrerId") Long referrerId, @RequestParam("referralStep") ReferralStep referralStep) throws VException {
		try {
			ReferralStepBonus res = referralManager
					.getReferralStepBonusByReferralsId(referreeId, referrerId, referralStep);
			if (res != null) {
				return res;
			} else {
				throw new VException(ErrorCode.SERVICE_ERROR,
						"get referral Step Bonus by Referral Policy");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	@RequestMapping(value = "/handleBundleReferral", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public void handleBundleReferral(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
			@RequestBody String request) throws Exception {
		Gson gson = new Gson();
		AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
		if (messageType.equals("SubscriptionConfirmation")) {
			String json = null;
			logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
			ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
			logger.info(resp.getEntity(String.class));
		} else if (messageType.equals("Notification")) {
			logger.info("Notification received - SNS");
			logger.info(subscriptionRequest.toString());
			String subject = subscriptionRequest.getSubject();			
			String message = subscriptionRequest.getMessage();
			ProcessReferralBonusReq req = gson.fromJson(message, ProcessReferralBonusReq.class);
			try {
				logger.info(System.currentTimeMillis());
				if(StringUtils.isEmpty(req.getEntityId()) || req.getReferralStep() == null || req.getUserId() == null){
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Bad Request for Entity Id "+req.getEntityId() 
					+" userId " + req.getUserId() + " referralstep "+ req.getReferralStep());
				}
	                        //TODO: Discuss this with Ajith
				Thread.sleep(15000);
				logger.info(System.currentTimeMillis());
				referralManager.processReferralBonus(
						req.getReferralStep(),
						req.getUserId(),
						req.getEntityId()
                        );
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	@RequestMapping(value = "/getReferralStepBonusesForEntityId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ReferralStepBonus getReferralStepBonusesForEntityId(@RequestParam(name="entityId") String entityId, @RequestParam(name="referralStep", required = true) ReferralStep referralStep) throws Exception {
		
			return referralManager
					.getReferralStepBonusesForEntityId(entityId,referralStep);

	}

	@RequestMapping(value = "/processReferralBonusForSignUp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public void processReferralBonusForSignUp(@RequestBody ProcessReferralBonusReq req) throws Exception {
		referralManager
				.processReferralBonus(req.getReferralStep(), req.getUserId());
	}


}
