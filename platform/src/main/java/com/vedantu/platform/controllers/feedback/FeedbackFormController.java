/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers.feedback;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.feedback.FeedbackFormManager;
import com.vedantu.platform.managers.feedback.FeedbackParentFormManager;
import com.vedantu.platform.mongodbentities.feedback.FeedbackForm;
import com.vedantu.platform.mongodbentities.feedback.FeedbackFormResponse;
import com.vedantu.platform.mongodbentities.feedback.FeedbackToSlack;
import com.vedantu.platform.request.feedback.CreateFeedbackFormReq;
import com.vedantu.platform.request.feedback.ShareFeedbackFormReq;
import com.vedantu.platform.request.feedback.SubmitFeedbackResponseReq;
import com.vedantu.platform.response.feedback.GetContextFormSharedInfoResponse;
import com.vedantu.platform.response.feedback.GetFormsSharedInContextResponse;
import com.vedantu.platform.response.feedback.GetFormsSharedWithStudentResponse;
import com.vedantu.platform.response.feedback.GetParentFormSharedInfoResponse;
import com.vedantu.platform.response.feedback.GetParentFormsResponse;
import com.vedantu.platform.response.feedback.UserFeedbackFormResponseInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.security.HttpSessionUtils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author parashar
 */
@RestController
@RequestMapping("/feedbackForm")
public class FeedbackFormController {
    
    @Autowired
    private FeedbackParentFormManager feedbackParentFormManager;
    
    @Autowired
    private FeedbackFormManager feedbackFormManager;
    
    @Autowired
    private HttpSessionUtils sessionUtils;
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FeedbackFormController.class);

    @Autowired
    AsyncTaskFactory asyncTaskFactory;
    
    // create Form
    
    @RequestMapping(value = "/createParentForm", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse createParentForm(@RequestBody CreateFeedbackFormReq req) throws VException,BadRequestException, ForbiddenException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        req.verify();
        return feedbackParentFormManager.createParentForm(req);
    }
    
    
    // share Form
    
    @RequestMapping(value = "/shareForm", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse shareFeedbackForm(@RequestBody ShareFeedbackFormReq req) throws NotFoundException, ConflictException, BadRequestException, VException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        req.verify();
        return feedbackParentFormManager.shareParentFeedbackForm(req);
    }   
    
    
    // get parent forms

    @RequestMapping(value = "/getParentForms", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetParentFormsResponse getParentForms(@RequestParam(name="start", required = true) int start, @RequestParam(name="size", required=true) int size) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return feedbackParentFormManager.getParentForms(start, size);
    } 
    
    
    // get parent forms's share metadata

    @RequestMapping(value = "/getParentFormSharedInfo", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetParentFormSharedInfoResponse getParentFormSharedInfo(@RequestParam(name="parentFormId", required=true)String parentFormId) throws VException,NotFoundException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return feedbackParentFormManager.getParenFormSharedInfo(parentFormId);
    } 
    
    
    // get form for parent form shared in batch b1 with response count
 
    @RequestMapping(value = "/getContextFormSharedInfo", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetContextFormSharedInfoResponse getContextFormSharedInfo(@RequestParam(name="parentFormId", required=true)String parentFormId,
                                                                    @RequestParam(name="batchId", required = true)String contextId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return feedbackParentFormManager.getContextFormSharedInfo(parentFormId, contextId);
    }
    
    
    // get shared forms in batch b1
    
    @RequestMapping(value = "/getFormsSharedInContext", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetFormsSharedInContextResponse getFormsSharedInContext(@RequestParam(name="batchId", required = true)String contextId, 
                                                                    @RequestParam(name="start", required = true) int start, 
                                                                    @RequestParam(name="size", required=true) int size) throws VException {
               sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return feedbackParentFormManager.getFormsSharedInContextResponse(contextId, start, size);
    }  
    
    
    // get forms shared with student s1
    
    @RequestMapping(value = "/getFormsSharedWithUser", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetFormsSharedWithStudentResponse getFormsSharedWithUser(@RequestParam(name="userId", required=true)Long userId,
                                                                    @RequestParam(name="start", required = true) int start, 
                                                                    @RequestParam(name="size", required=true) int size) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return feedbackParentFormManager.getFormsSharedWithStudent(userId, start, size);
    }
    
    // get form responses

    @RequestMapping(value = "/getFormsResponses", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<UserFeedbackFormResponseInfo> getFormResponses(@RequestParam(name="formId", required = true) String formId,
                                                        @RequestParam(name="userId", required = false) Long userId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return feedbackParentFormManager.getFeedbackFormResponses(formId, userId);
    }
    

    // cron endpoint for form creation
    @RequestMapping(value = "/createFormInstance", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody  
    public void createFormInstance(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request){
        
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            feedbackParentFormManager.processFeedbackFormForInstanceCreation();
        }        
        
    }
    
    @RequestMapping(value = "/expireForms", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody  
    public void expireForms(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request){
        
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            feedbackFormManager.expireFeedbackForms();
        }        
        
    }
    
    @RequestMapping(value = "/submitFeedbackFormResponse", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse submitFeedbackFormResponse(@RequestBody SubmitFeedbackResponseReq req) throws NotFoundException, ConflictException, ForbiddenException, BadRequestException{
        req.verify();   
        Long userId = sessionUtils.getCallingUserId();
        if(userId == null){
            throw new ForbiddenException(ErrorCode.USER_LOGIN_REQUIRED, "user login required");
        }        
        return feedbackFormManager.submitFeedbackFormResponse(req, userId);  
    } 
    
    @RequestMapping(value = "/getNotAnsweredFormForStudent", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetFormsSharedWithStudentResponse getNotAnsweredFormForStudent() throws NotFoundException, ConflictException, ForbiddenException{
        Long userId = sessionUtils.getCallingUserId();
        if(userId == null){
            throw new ForbiddenException(ErrorCode.USER_LOGIN_REQUIRED, "user login required");
        }     
        
        return feedbackFormManager.getNotAnsweredFormForStudent(userId);
    }  
    
    @RequestMapping(value = "/getFeedbackFormToRespond", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public FeedbackForm getFeedbackFormToRespond(@RequestParam(name="feedbackFormId", required = true) String feedbackFormId) throws NotFoundException, ConflictException, ForbiddenException{
        Long userId = sessionUtils.getCallingUserId();
        if(userId == null){
            throw new ForbiddenException(ErrorCode.USER_LOGIN_REQUIRED, "user login required");
        }
        return feedbackFormManager.getFeedbackFormToSubmit(feedbackFormId, userId);
    }    
        
    @RequestMapping(value = "/markFormInactive", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void markFormInactiveForContext(@RequestParam(name="parentFormId", required=true) String parentFormId, @RequestParam(name="contextId", required = true) String contextId, @RequestParam(name="entityState", required=true) EntityState entityState) throws VException,NotFoundException, ConflictException, BadRequestException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        feedbackParentFormManager.markFormShareStatus(contextId, parentFormId, entityState);
    }
    
    @RequestMapping(value = "/getFormsToRespondForBatch", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetFormsSharedWithStudentResponse getFormsToRespondForBatch(@RequestParam(name="batchId", required = true) String batchId) throws NotFoundException, ConflictException, BadRequestException, ForbiddenException, VException{
        Long userId = sessionUtils.getCallingUserId();
        if(userId == null){
            throw new ForbiddenException(ErrorCode.USER_LOGIN_REQUIRED, "user login required");
        }        
        return feedbackFormManager.getFeedbackFormsForBatchId(batchId, userId);
    }

        @RequestMapping(value = "/sendFeedbackToSlack", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendFeedbackToSlack(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                 @RequestBody String request) throws Exception {
        logger.info("OTF_SESSION_FEEDBACK_GENERATED sns triggered with : "+request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String subject = subscriptionRequest.getSubject();
            String message = subscriptionRequest.getMessage();
            logger.info("sendFeedbackToSlack subject : "+subject);
            logger.info("sendFeedbackToSlack : " + message);
            Map<String, Object> payload = new HashMap<>();
            payload.put("message", message);
            payload.put("subject", subject);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_FEEDBACK_TO_SLACK_ASYNC, payload);
            asyncTaskFactory.executeTask(params);
//            feedbackFormManager.sendFeedbackToSlack(message,subject);
        }
    }

    @RequestMapping(value = "/sendFeedbackToSlackLocalTesting", method = RequestMethod.POST, consumes = "application/json" ,produces = "application/json")
    @ResponseBody
    public void testingSlack(@RequestBody FeedbackToSlack req) throws VException, UnsupportedEncodingException {
        feedbackFormManager.sendFeedbackToSlack( new Gson().toJson(req),null);
    }
}
