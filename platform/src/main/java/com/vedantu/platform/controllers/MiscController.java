/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.enums.DoubtAppType;
import com.vedantu.platform.managers.MiscManager;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.mongodbentities.DoubtsAppVersion;
import com.vedantu.platform.request.misc.WebinarUserRegistrationInfoReq;
import com.vedantu.platform.response.ActiveTabsRes;
import com.vedantu.platform.response.AppRatingConfigParamsRes;
import com.vedantu.platform.response.DexConfigRes;
import com.vedantu.platform.response.DoubtsAppVersionRes;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/misc")
public class MiscController {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(MiscController.class);

    @Autowired
    private MiscManager miscManager;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/addmobileno", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse addmobileno(@RequestParam("mobileno") String mobileno,
            @RequestParam("campaign") String campaign,
            @RequestParam("secret") String secret) throws VException {
        if (StringUtils.isEmpty(secret) || !secret.equals("029T4971866c")) {
            throw new ForbiddenException(ErrorCode.INVALID_SECRET, "invalid secret key");
        }
        return miscManager.addmobileno(mobileno, campaign);
    }
    
    @RequestMapping(value = "/getipinformation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Map<String, Object> getipinformation(AbstractFrontEndUserReq req) throws VException {
        return miscManager.getipinformation(req);
    }

    @RequestMapping(value = "/missedcalltrigger", method = RequestMethod.GET)
    public PlatformBasicResponse registerviamissedcall(@RequestParam("From") String mobileno,
            @RequestParam("target") String target,
            @RequestParam(name = "smstext", required = false) String smstext) throws Exception {
        if (StringUtils.isNotEmpty(mobileno) && mobileno.length() == 11
                && mobileno.charAt(0) == '0') {
            mobileno = mobileno.substring(1);
        }
        Map<String, Object> payload = new HashMap<>();
        payload.put("mobileno", mobileno);
        payload.put("target", target);
        payload.put("smstext", smstext);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.MISSED_CALL_TRIGGER,
                payload);
        asyncTaskFactory.executeTask(params);        
        return new PlatformBasicResponse();
    }
    
    @RequestMapping(value = "/webinar/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public PlatformBasicResponse registerWebinarUser(@RequestBody WebinarUserRegistrationInfoReq req) throws Exception {
        miscManager.registerWebinar(req);
        return new PlatformBasicResponse();
    }
    
    @RequestMapping(value = "/userLeads/sendMessage", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON)
    public PlatformBasicResponse sendMessageToLeads(@RequestParam(name = "checkedNumber", required = false) String checkedNumber) throws Exception {
        miscManager.sendMessageToLeads(checkedNumber);
        return new PlatformBasicResponse();
    }
    

    @RequestMapping(value = "/getDoubtsAppVersion", method = RequestMethod.GET)
    @ResponseBody
    public DoubtsAppVersionRes getLatestDoubtsAppVersion(@RequestParam(name = "currentVersion", required = true) String currentVersion, @RequestParam(name= "appType", required = true) DoubtAppType doubtAppType) throws NotFoundException{
        return miscManager.getLatestDoubtAppVersion(currentVersion, doubtAppType);
    }
    
    @RequestMapping(value = "/addDoubtsAppVersion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void addDoubtsAppVersion(@RequestBody DoubtsAppVersion doubtsAppVersion){
        miscManager.addDoubtsAppVersion(doubtsAppVersion);
    }

    @RequestMapping(value = "/getAppRatingConfigParams", method = RequestMethod.GET)
    @ResponseBody
    public AppRatingConfigParamsRes getAppRatingConfigParams(@RequestParam(name = "appVersionCode", required = false) String appVersionCode, @RequestParam(name= "requestSource", required = false) RequestSource requestSource, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws NotFoundException, ForbiddenException, VException, URISyntaxException, IOException {
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
        return miscManager.getAppRatingConfigParams(sessionData.getUserId().toString(), appVersionCode, requestSource);
    }

    @RequestMapping(value = "/getActiveTabsOrder", method = RequestMethod.GET)
    @ResponseBody
    public ActiveTabsRes getActiveTabsOrder(@RequestParam(name = "appVersionCode", required = false) String appVersionCode, @RequestParam(name= "requestSource", required = false) RequestSource requestSource, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws NotFoundException, ForbiddenException, VException, URISyntaxException, IOException {
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
        ActiveTabsRes activeTabs = miscManager.getActiveTabs(sessionData.getUserId().toString(), appVersionCode, requestSource);
        return activeTabs;
    }

    @RequestMapping(value = "/getDexConfigs", method = RequestMethod.GET)
    @ResponseBody
    public DexConfigRes getDexConfigs(@RequestParam(name = "currentVersion", required = true) String currentVersion, @RequestParam(name= "appType", required = true) DoubtAppType doubtAppType) throws NotFoundException{
        return miscManager.getDexConfigs();
    }

    @RequestMapping(value = "/showPopup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Boolean> showPopup(@RequestParam("userId") String userId, @RequestParam("releaseTime") Long releaseTime, @RequestParam("appVersionCode") String appVersionCode)
    {
        return ResponseEntity.ok(miscManager.showPopup(userId, releaseTime, appVersionCode));
    }
}
