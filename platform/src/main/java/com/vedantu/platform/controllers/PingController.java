/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.controllers;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.response.TimeDifferenceClientRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.WebUtils;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/ping")
public class PingController {

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public String user() throws VException {
        return pingSubSystem("USER_ENDPOINT");
    }

    @RequestMapping(value = "/scheduling", method = RequestMethod.GET)
    @ResponseBody
    public String scheduling() throws VException {
        return pingSubSystem("SCHEDULING_ENDPOINT");
    }

    @RequestMapping(value = "/dinero", method = RequestMethod.GET)
    @ResponseBody
    public String dinero() throws VException {
        return pingSubSystem("DINERO_ENDPOINT");
    }

    @RequestMapping(value = "/subscription", method = RequestMethod.GET)
    @ResponseBody
    public String subscription() throws VException {
        return pingSubSystem("SUBSCRIPTION_ENDPOINT");
    }

    @RequestMapping(value = "/lms", method = RequestMethod.GET)
    @ResponseBody
    public String lms() throws VException {
        return pingSubSystem("LMS_ENDPOINT");
    }

    @RequestMapping(value = "/listing", method = RequestMethod.GET)
    @ResponseBody
    public String listing() throws VException {
        return pingSubSystem("PLATFORM_ENDPOINT");
    }

    @RequestMapping(value = "/notification", method = RequestMethod.GET)
    @ResponseBody
    public String notification() throws VException {
        return pingSubSystem("NOTIFICATION_ENDPOINT");
    }
    
    @RequestMapping(value = "/getTimeDifference", method = RequestMethod.GET)
    @ResponseBody
    public TimeDifferenceClientRes getTimeDifference(@RequestParam(value ="clientTime", required = true) Long clientTime) throws VException {
        return new TimeDifferenceClientRes((System.currentTimeMillis()-clientTime),System.currentTimeMillis(),clientTime);
    }

    @RequestMapping(value = "/plat_user", method = RequestMethod.GET)
    @ResponseBody
    public String plat_user() throws VException {
        String userEndpoint = "http://localhost/user";
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/index.jsp", HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    private String pingSubSystem(String subsystem) throws VException {
        String userEndpoint = ConfigUtils.INSTANCE.getStringValue(subsystem);
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/index.jsp", HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }
}
