package com.vedantu.platform.controllers.cms;


import com.vedantu.User.Role;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonSyntaxException;
import com.vedantu.exception.VException;
import com.vedantu.platform.managers.cms.TestimonialManager;
import com.vedantu.platform.request.cms.CreateTestimonialReq;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("cms/testimonial")
public class CmsTestimonialController {

	@Autowired
    private LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

	@Autowired
    private TestimonialManager testimonialManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CmsWebinarController.class);
    
    @RequestMapping(value = "/setTestimonial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CreateTestimonialReq setTestimonial(@RequestBody CreateTestimonialReq req) throws VException, JsonProcessingException {
        logger.info("Set Testimonial Request :" + req.toString());
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testimonialManager.setTestimonial(req);
    }
    
    @RequestMapping(value = "/fetchTestimonial", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CreateTestimonialReq fetchTestimonial() throws VException, JsonSyntaxException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testimonialManager.fetchTestimonial();
    }
    
}
