package com.vedantu.platform.controllers.youscore;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.youscore.pojo.YouScoreCookie;
import com.vedantu.lms.youscore.pojo.YouScoreTokenRes;
import com.vedantu.platform.managers.lms.YouScoreManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.WebUtils;

@RestController
@RequestMapping("/youscore")
public class YouScoreController {

	@Autowired
	private YouScoreManager youScoreManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(YouScoreController.class);

	private String lmsEndpoint;

	public YouScoreController() {
		super();
	}

	@PostConstruct
	public void init() {
		lmsEndpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	}

	// @CrossOrigin
	@Deprecated
	@RequestMapping(value = "/content/share", method = RequestMethod.POST)
	@ResponseBody
	public String contentShared(HttpServletRequest request) throws IOException, VException {
		String requestBody = PlatformTools.getBody(request);
		logger.info("contentShared Batch request :" + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(lmsEndpoint + "youscore/content/share", HttpMethod.POST,
				requestBody, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		return jsonString;
	}

	// @CrossOrigin
	@Deprecated
	@RequestMapping(value = "/content/evaluate", method = RequestMethod.POST)
	@ResponseBody
	public String contentEvaluate(HttpServletRequest request) throws IOException, VException {
		String requestBody = PlatformTools.getBody(request);
		logger.info("contentEvaluate Batch request :" + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(lmsEndpoint + "youscore/content/evaluate", HttpMethod.POST,
				requestBody, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		return jsonString;
	}

	@RequestMapping(value = "/loginValidation", method = RequestMethod.GET)
	@ResponseBody
	public YouScoreTokenRes validateVedantuLogin(HttpServletRequest request, HttpServletResponse response)
			throws IOException, VException {
		return youScoreManager.validateVedantuLogin(request, response);
	}

	@RequestMapping(value = "/createCookie", method = RequestMethod.GET)
	@ResponseBody
	public YouScoreCookie createCookie(HttpServletRequest request, HttpServletResponse response)
			throws IOException, VException, IllegalArgumentException, IllegalAccessException {
		return youScoreManager.createCookie(request, response);
	}

	@RequestMapping(value = "/teacherportal", method = RequestMethod.GET)
	@ResponseBody
	public void teacherportal(HttpServletRequest request, HttpServletResponse response)
			throws IOException, VException, IllegalArgumentException, IllegalAccessException {
		youScoreManager.redirectToTeacherPortal(request, response);
	}

	@RequestMapping(value = "/createStudentTeacherMapping", method = RequestMethod.POST)
	@ResponseBody
	public void createStudentTeacherMapping(HttpServletRequest request)
			throws IOException, VException, IllegalArgumentException, IllegalAccessException {
		youScoreManager.createStudentTeacherMapping(request);
	}
}
