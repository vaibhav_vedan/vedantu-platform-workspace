/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.utils;

import com.vedantu.User.User;
import com.vedantu.User.UserUtils;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.review.CumilativeRating;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class PojoUtils {

    @Autowired
    private DozerBeanMapper mapper;

    public com.vedantu.board.pojo.Board convertToBoardPojo(Board board) {
        com.vedantu.board.pojo.Board _board = mapper.map(board, com.vedantu.board.pojo.Board.class);
        return _board;
    }
    
    public com.vedantu.session.pojo.CumilativeRating convertToRatingPojo(CumilativeRating rating) {
        com.vedantu.session.pojo.CumilativeRating _rating = mapper.map(rating, com.vedantu.session.pojo.CumilativeRating.class);
        return _rating;
    }
    
    public HttpSessionData convertToSessionData(User user) {
        HttpSessionData sessionData = mapper.map(user, HttpSessionData.class);
        if(user!=null) {
            sessionData.setUserId(user.getId());
            sessionData.setProfilePicUrl(StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
                    : UserUtils.INSTANCE.toProfilePicUrl(user.getProfilePicPath()));
        }
        return sessionData;
    }

}
