/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.utils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vdurmont.emoji.EmojiParser;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.pojo.leadsquared.SchemaValuePair;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author somil
 */
public class LeadSquaredUtils {

	public static String getLeadSquaredDate(Long date) {
		// Lead squared date format : "yyyy-MM-dd HH:mm:ss" (This is the only
		// date form Lead squared accepts)
		if (date == null || date <= 0) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
		return sdf.format(new Date(date));
	}

	public static String createJSONObjectLeadDetails(Map<String, String> params)
			throws IllegalArgumentException, IllegalAccessException, BadRequestException {
		if (params == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "params map is null");
		}

		if (!params.containsKey("SearchBy")) {
			if (params.containsKey("Phone")) {
				params.put("SearchBy", "Phone");
			} else {
				params.put("SearchBy", "EmailAddress");
			}
		}

		List<String> attributes = new ArrayList<String>();
		String attribute;
		for (String field : params.keySet()) {
			attribute = "{\"Attribute\":\"" + field + "\",\"Value\":\"" + params.get(field) + "\"}";
			attributes.add(attribute);
		}

		return "[" + join(attributes, ",") + "]";
	}

	public static String createJSONObjectLeadActivity(Map<String, String> params)
			throws IllegalArgumentException, IllegalAccessException, BadRequestException {
		if (params == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "params map is null");
		}

		List<String> attributes = new ArrayList<String>();
		for (String key : params.keySet()) {
			String keyString = Objects.equals("mobileNumber", key) ? "Phone" : key;
			if (params.get(key) == null || Objects.equals("null", params.get(key))) {
				continue;
			}
			attributes.add("\"" + keyString + "\":\"" + params.get(key) + "\"");
		}
		return "{" + join(attributes, ",") + "}";
	}
        
        public static String createLeadActivityCustomFields(Map<String, String> params)
			throws IllegalArgumentException, IllegalAccessException, BadRequestException {
		if (params == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "params map is null");
		}

		List<String> attributes = new ArrayList<String>();
                String attribute;
		for (String field : params.keySet()) {
			attribute = "{\"SchemaName\":\"" + field + "\",\"Value\":\"" + params.get(field) + "\"}";
			attributes.add(attribute);
		}
		return "[" + join(attributes, ",") + "]";
	}

	public static String join(List<String> inputs, String separator) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < inputs.size(); i++) {

			builder.append(inputs.get(i));
			if (inputs.size() - 1 != i) {
				builder.append(separator);
			}

		}
		return builder.toString();
	}
        
                
        public static List<SchemaValuePair> getSchemaValuePair(List<String> values) {
            List<SchemaValuePair> fields = new ArrayList<>();
            if(ArrayUtils.isNotEmpty(values)){
                int i =1;
                for(String value : values){
                    if(value != null){
                        fields.add(new SchemaValuePair("mx_Custom_"+i,value));
                    }
                    i++;
                }
            }
            return fields;
        }

	public static Boolean validateLSEmails(String validLSEmail) {
		try {
			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
					"[a-zA-Z0-9_+&*-]+)*@" +
					"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
					"A-Z]{2,7}$";

			Pattern pattern = Pattern.compile(emailRegex);
			Matcher matcher = pattern.matcher(validLSEmail);
			return matcher.matches();
		} catch (Exception e) {
			return true;

		}

	}

	public static String removeEmojisFromLSRequest(String json) {
		String originalRequest = json;
		try {
			if (StringUtils.isNotEmpty(json)) {
				json = EmojiParser.removeAllEmojis(json);
			}
		} catch (Exception e) {
			return originalRequest;
		}
		return json;
	}
}
