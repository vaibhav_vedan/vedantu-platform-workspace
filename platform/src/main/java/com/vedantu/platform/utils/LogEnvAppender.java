/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.utils;

import com.vedantu.util.ConfigUtils;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;

/**
 *
 * @author ajith includes the env in every log
 */
@Plugin(name = "LogEnvAppender", category = "Converter")
@ConverterKeys({"env"})
public class LogEnvAppender extends LogEventPatternConverter {

    protected LogEnvAppender(String name, String style) {
        super(name, style);
    }

    public static LogEnvAppender newInstance(String[] options) {
        return new LogEnvAppender("env", "env");
    }

    @Override
    public void format(LogEvent event, StringBuilder toAppendTo) {
        String env = getEnv();
        toAppendTo.append(env);
    }

    protected String getEnv() {
        return ConfigUtils.INSTANCE.properties.getProperty("environment");
    }
}
