package com.vedantu.platform.utils;

import com.vedantu.util.EntityType;
import org.springframework.stereotype.Service;

import com.vedantu.util.FosUtils;
import com.vedantu.util.fos.interfaces.IEntity;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class EntityTypeUtils {

    @Autowired
    private FosUtils fosUtils;

    @SuppressWarnings("unchecked")
    public IEntity getEntity(String id, EntityType entityType) {
        if (entityType == null) {
            return null;
        }

        IEntity iEntity = null;
        switch (entityType) {
            case USER:
                iEntity = fosUtils.getUserBasicInfo(id, false);
                break;
            default:
                break;
        }
        return iEntity;
    }

}
