package com.vedantu.platform.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Service;
/*

The start times of the month and day are calculated as per Indian Time.

*/

@Service
public class TimeDateMonth {

	public Long getLastMonthStartTime(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));

		calendar.setTimeInMillis(currentTime);
		int month = calendar.get(Calendar.MONTH);
		if (month == 0) {
			month = 11;
			calendar.set(Calendar.YEAR,(calendar.get(Calendar.YEAR))-1);
		} else {
			month = month - 1;
		}
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0); // set hours to zero
		calendar.set(Calendar.MINUTE, 0); // set minutes to zero
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();

	}	

	public Long getCurrentMonthStartTime(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));

		calendar.setTimeInMillis(currentTime);

		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0); // set hours to zero
		calendar.set(Calendar.MINUTE, 0); // set minutes to zero
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	public int getDayOfTheMonth(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		calendar.setTimeInMillis(currentTime);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		return day;
	}

	public Long getStartTimeOfDay(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));

		calendar.setTimeInMillis(currentTime);
		calendar.set(Calendar.HOUR_OF_DAY, 0); // set hours to zero
		calendar.set(Calendar.MINUTE, 0); // set minutes to zero
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	public String getDate(Long time) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		return dateFormat.format(time);
	}

	public String getDateString(Long time) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));

		calendar.setTimeInMillis(time);
		return sdf.format(calendar.getTime());
	}
}
