/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.utils;

import com.vedantu.util.LogFactory;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ajith
 * https://stackoverflow.com/questions/19189312/jdbc-driver-has-been-forcibly-unregistered-by-tomcat-7-why
 *https://techblog.ralph-schuster.eu/2014/07/09/solution-to-tomcat-cant-stop-an-abandoned-connection-cleanup-thread/
 * http://bugs.mysql.com/bug.php?id=36565
 */
@WebListener
public class ContainerContextClosedHandler implements ServletContextListener {
    
    @Autowired
    private LogFactory logFactory;
    
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ContainerContextClosedHandler.class);
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // nothing to do
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
//        Enumeration<Driver> drivers = DriverManager.getDrivers();
//        
//        Driver driver = null;
//
//        // clear drivers
//        while (drivers.hasMoreElements()) {
//            try {
//                driver = drivers.nextElement();
//                DriverManager.deregisterDriver(driver);
//                
//            } catch (SQLException ex) {
//                // deregistration failed, might want to do something, log at the very least
//            }
//        }
//        
//        logger.info("closing PooledDataSource");
//        for (Object o : C3P0Registry.getPooledDataSources()) {
//            try {
//                ((PooledDataSource) o).close();
//            } catch (Exception e) {
//                logger.info("No thread was open...");
//            }
//        }
//
//        // MySQL driver leaves around a thread. This static method cleans it up.
//        try {
//            AbandonedConnectionCleanupThread.shutdown();
//        } catch (InterruptedException e) {
//            // again failure, not much you can do
//        }
//
        //https://stackoverflow.com/questions/18069042/spring-mvc-webapp-schedule-java-sdk-http-connection-reaper-failed-to-stop
        try {
            com.amazonaws.http.IdleConnectionReaper.shutdown();
        } catch (Throwable t) {
            // log the error
        }
    }
    
}
