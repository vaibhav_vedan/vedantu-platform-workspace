package com.vedantu.platform.utils;

import java.util.HashMap;
import java.util.Map;

public class ReviseJeeFactory {

    public static Map<String, String> getPredictedRank(Integer mark){

        Map<String, String> map = new HashMap<>();

        if(mark > 280){
            map.put("predictedRank", "1 - 50");
            map.put("nextRankToAchieve", "1");
            return map;
        }
        if(mark >= 270){
            map.put("predictedRank", "51 - 100");
            map.put("nextRankToAchieve", "25");
            return map;
        }
        if(mark >= 265){
            map.put("predictedRank", "101 - 200");
            map.put("nextRankToAchieve", "50");
            return map;
        }
        if(mark >= 260){
            map.put("predictedRank", "201 - 400");
            map.put("nextRankToAchieve", "100");
            return map;
        }
        if(mark >= 255){
            map.put("predictedRank", "401 - 600");
            map.put("nextRankToAchieve", "200");
            return map;
        }
        if(mark >= 250){
            map.put("predictedRank", "601 - 900");
            map.put("nextRankToAchieve", "250");
            return map;
        }
        if(mark >= 245){
            map.put("predictedRank", "901 - 1300");
            map.put("nextRankToAchieve", "500");
            return map;
        }
        if(mark >= 240){
            map.put("predictedRank", "1301 - 1800");
            map.put("nextRankToAchieve", "800");
            return map;
        }
        if(mark >= 235){
            map.put("predictedRank", "1801 - 2300");
            map.put("nextRankToAchieve", "1000");
            return map;
        }
        if(mark >= 230){
            map.put("predictedRank", "2301 - 2900");
            map.put("nextRankToAchieve", "1500");
            return map;
        }
        if(mark >= 225){
            map.put("predictedRank", "2901 - 3500");
            map.put("nextRankToAchieve", "1800");
            return map;
        }
        if(mark >= 220){
            map.put("predictedRank", "3501 - 4100");
            map.put("nextRankToAchieve", "2000");
            return map;
        }
        if(mark >= 215){
            map.put("predictedRank", "4101 - 4700");
            map.put("nextRankToAchieve", "2500");
            return map;
        }
        if(mark >= 210){
            map.put("predictedRank", "4701 - 5300");
            map.put("nextRankToAchieve", "25000");
            return map;
        }
        if(mark >= 205){
            map.put("predictedRank", "5301 - 5900");
            map.put("nextRankToAchieve", "3000");
            return map;
        }
        if(mark >= 200){
            map.put("predictedRank", "5901 - 6500");
            map.put("nextRankToAchieve", "30000");
            return map;
        }
        if(mark >= 195){
            map.put("predictedRank", "6501 - 7200");
            map.put("nextRankToAchieve", "3500");
            return map;
        }
        if(mark >= 190){
            map.put("predictedRank", "7201 - 7900");
            map.put("nextRankToAchieve", "4500");
            return map;
        }
        if(mark >= 185){
            map.put("predictedRank", "7901 - 8600");
            map.put("nextRankToAchieve", "5000");
            return map;
        }
        if(mark >= 180){
            map.put("predictedRank", "8601 - 9300");
            map.put("nextRankToAchieve", "5500");
            return map;
        }
        if(mark >= 175){
            map.put("predictedRank", "9301 - 10500");
            map.put("nextRankToAchieve", "6000");
            return map;
        }
        if(mark >= 170){
            map.put("predictedRank", "10501 - 12000");
            map.put("nextRankToAchieve", "6500");
            return map;
        }
        if(mark >= 165){
            map.put("predictedRank", "12001 - 14000");
            map.put("nextRankToAchieve", "6500");
            return map;
        }
        if(mark >= 160){
            map.put("predictedRank", "14001 - 16500");
            map.put("nextRankToAchieve", "7000");
            return map;
        }
        if(mark >= 155){
            map.put("predictedRank", "16501 - 18000");
            map.put("nextRankToAchieve", "8000");
            return map;
        }
        if(mark >= 150){
            map.put("predictedRank", "18001 - 20000");
            map.put("nextRankToAchieve", "10000");
            return map;
        }
        if(mark >= 145){
            map.put("predictedRank", "20001 - 22000");
            map.put("nextRankToAchieve", "12000");
            return map;
        }
        if(mark >= 140){
            map.put("predictedRank", "22001 - 25000");
            map.put("nextRankToAchieve", "12000");
            return map;
        }
        if(mark >= 135){
            map.put("predictedRank", "25001 - 26000");
            map.put("nextRankToAchieve", "12000");
            return map;
        }
        if(mark >= 130){
            map.put("predictedRank", "26001 - 28500");
            map.put("nextRankToAchieve", "15000");
            return map;
        }
        if(mark >= 125){
            map.put("predictedRank", "28501 - 31000");
            map.put("nextRankToAchieve", "15000");
            return map;
        }
        if(mark >= 120){
            map.put("predictedRank", "31001 - 33500");
            map.put("nextRankToAchieve", "15000");
            return map;
        }
        if(mark >= 115){
            map.put("predictedRank", "33501 - 36500");
            map.put("nextRankToAchieve", "20000");
            return map;
        }
        if(mark >= 110){
            map.put("predictedRank", "36501 - 40000");
            map.put("nextRankToAchieve", "25000");
            return map;
        }
        if(mark >= 105){
            map.put("predictedRank", "40001 - 50000");
            map.put("nextRankToAchieve", "25000");
            return map;
        }
        if(mark >= 95){
            map.put("predictedRank", "50001 - 65000");
            map.put("nextRankToAchieve", "30000");
            return map;
        }
        if(mark >= 90){
            map.put("predictedRank", "65001 - 85000");
            map.put("nextRankToAchieve", "35000");
            return map;
        }
        if(mark >= 85){
            map.put("predictedRank", "85001 - 1450000");
            map.put("nextRankToAchieve", "35000");
            return map;
        }
        if(mark >= 75){
            map.put("predictedRank", "145001 - 190000");
            map.put("nextRankToAchieve", "40000");
            return map;
        }
        if(mark >= 70){
            map.put("predictedRank", "190000 - 245000");
            map.put("nextRankToAchieve", "65000");
            return map;
        }
        if(mark >= 65){
            map.put("predictedRank", "245001 - 320000");
            map.put("nextRankToAchieve", "100000");
            return map;
        }
        if(mark >= 55){
            map.put("predictedRank", "320001 - 400000");
            map.put("nextRankToAchieve", "150000");
            return map;
        }
        if(mark >= 45){
            map.put("predictedRank", "400001 - 480000");
            map.put("nextRankToAchieve", "200000");
            return map;
        }
        if(mark >= 35){
            map.put("predictedRank", "480001 - 580000");
            map.put("nextRankToAchieve", "200000");
            return map;
        }
        if(mark >= 25){
            map.put("predictedRank", "580001 - 680000");
            map.put("nextRankToAchieve", "350000");
            return map;
        }
        if(mark >= 10){
            map.put("predictedRank", "680001 - 800000");
            map.put("nextRankToAchieve", "350000");
            return map;
        }
        if(mark >= 1){
            map.put("predictedRank", "800001 - 900000");
            map.put("nextRankToAchieve", "400000");
            return map;
        }

        map.put("predictedRank", "-ve marks - 900000");
        map.put("nextRankToAchieve", "400000");
        return map;

    }
}
