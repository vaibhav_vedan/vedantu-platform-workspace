/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.util.enums.UploadState;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class UpdateFileUploadStatusRes extends AbstractRes {

	private Long id;
	private UploadState oldState;
	private UploadState newState;

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public UploadState getOldState() {

		return oldState;
	}

	public void setOldState(UploadState oldState) {

		this.oldState = oldState;
	}

	public UploadState getNewState() {

		return newState;
	}

	public void setNewState(UploadState newState) {

		this.newState = newState;
	}

}

