package com.vedantu.platform.response.mobile;

import com.vedantu.platform.mongodbentities.mobile.MobileGlobalConfiguration;
import com.vedantu.platform.enums.mobile.RtcClientType;
import com.vedantu.platform.enums.mobile.MobileRole;
import com.vedantu.platform.enums.mobile.MobilePlatform;



public class GlobalMobileConfigRes {

	String userId;
	String deviceId;
	MobilePlatform platform;
	MobileRole role;
	String force_update_version;
	String optional_update_version;
	String mobile_support_email_address;
	String global_message;
	RtcClientType rtc_client_type;
	String rtc_ice_server;
	String rtc_turn_server;
	String rtc_turn_server_user_name;
	String rtc_turn_server_password;
	String socialvid_tenant_name;
	boolean session = true;
	boolean cmds;
	String teacherCmdsHelpVideo;
	String studentCmdsHelpVideo;
	
	public GlobalMobileConfigRes(MobileGlobalConfiguration mobileConfig) {
		super();
		this.userId = mobileConfig.getUserId();
		this.deviceId = mobileConfig.getDeviceId();
		this.platform =mobileConfig.getPlatform() ;
		this.role = mobileConfig.getRole();
		this.force_update_version = mobileConfig.getForce_update_version();
		this.optional_update_version = mobileConfig.getOptional_update_version();
		this.mobile_support_email_address = mobileConfig.getMobile_support_email_address();
		this.global_message = mobileConfig.getGlobal_message();
		this.rtc_client_type=mobileConfig.getRtc_client_type();
		this.rtc_ice_server=mobileConfig.getRtc_ice_server();
		this.rtc_turn_server=mobileConfig.getRtc_turn_server();
		this.rtc_turn_server_user_name=mobileConfig.getRtc_turn_server_user_name();
		this.rtc_turn_server_password=mobileConfig.getRtc_turn_server_password();
		this.socialvid_tenant_name=mobileConfig.getSocialvid_tenant_name();
		this.session = mobileConfig.isSession();
		this.cmds = mobileConfig.isCmds();
		this.teacherCmdsHelpVideo = mobileConfig.getTeacherCmdsHelpVideo();
		this.studentCmdsHelpVideo = mobileConfig.getStudentCmdsHelpVideo();
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public MobilePlatform getPlatform() {
		return platform;
	}
	public void setPlatform(MobilePlatform platform) {
		this.platform = platform;
	}
	public MobileRole getRole() {
		return role;
	}
	public void setRole(MobileRole role) {
		this.role = role;
	}
	public String getForce_update_version() {
		return force_update_version;
	}
	public void setForce_update_version(String force_update_version) {
		this.force_update_version = force_update_version;
	}
	public String getOptional_update_version() {
		return optional_update_version;
	}
	public void setOptional_update_version(String optional_update_version) {
		this.optional_update_version = optional_update_version;
	}
	public String getMobile_support_email_address() {
		return mobile_support_email_address;
	}
	public void setMobile_support_email_address(String mobile_support_email_address) {
		this.mobile_support_email_address = mobile_support_email_address;
	}
	public String getGlobal_message() {
		return global_message;
	}
	public void setGlobal_message(String global_message) {
		this.global_message = global_message;
	}
	public RtcClientType getRtc_client_type() {
		return rtc_client_type;
	}
	public void setRtc_client_type(RtcClientType rtc_client_type) {
		this.rtc_client_type = rtc_client_type;
	}
	public String getRtc_ice_server() {
		return rtc_ice_server;
	}
	public void setRtc_ice_server(String rtc_ice_server) {
		this.rtc_ice_server = rtc_ice_server;
	}
	public String getRtc_turn_server() {
		return rtc_turn_server;
	}
	public void setRtc_turn_server(String rtc_turn_server) {
		this.rtc_turn_server = rtc_turn_server;
	}
	public String getRtc_turn_server_user_name() {
		return rtc_turn_server_user_name;
	}
	public void setRtc_turn_server_user_name(String rtc_turn_server_user_name) {
		this.rtc_turn_server_user_name = rtc_turn_server_user_name;
	}
	public String getRtc_turn_server_password() {
		return rtc_turn_server_password;
	}
	public void setRtc_turn_server_password(String rtc_turn_server_password) {
		this.rtc_turn_server_password = rtc_turn_server_password;
	}
	public String getSocialvid_tenant_name() {
		return socialvid_tenant_name;
	}
	public void setSocialvid_tenant_name(String socialvid_tenant_name) {
		this.socialvid_tenant_name = socialvid_tenant_name;
	}
	public boolean isSession() {
		return session;
	}
	public void setSession(boolean session) {
		this.session = session;
	}
	public boolean isCmds() {
		return cmds;
	}
	public void setCmds(boolean cmds) {
		this.cmds = cmds;
	}
	
}
