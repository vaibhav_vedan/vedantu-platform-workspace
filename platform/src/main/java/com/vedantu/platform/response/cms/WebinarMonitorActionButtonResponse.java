package com.vedantu.platform.response.cms;

import lombok.Data;

@Data
public class WebinarMonitorActionButtonResponse {

    private Boolean canRefresh = Boolean.FALSE;
    private Boolean canEnd = Boolean.FALSE;
    private Boolean canView = Boolean.TRUE;

}
