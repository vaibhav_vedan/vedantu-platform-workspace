package com.vedantu.platform.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActiveTabsRes {
    private List<String> activeTabs;
    private  List<String> moreMenu;
}
