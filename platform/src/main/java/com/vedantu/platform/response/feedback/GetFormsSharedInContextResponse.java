/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.feedback;

import com.vedantu.platform.pojo.feedback.FormSharedInContextTypeInfo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetFormsSharedInContextResponse {

    private List<FormSharedInContextTypeInfo> formSharedInfos = new ArrayList<>();

    /**
     * @return the formSharedInBatchInfos
     */
    public List<FormSharedInContextTypeInfo> getFormSharedInfos() {
        return formSharedInfos;
    }

    /**
     * @param formSharedInfos the formSharedInBatchInfos to set
     */
    public void setFormSharedInfos(List<FormSharedInContextTypeInfo> formSharedInfos) {
        this.formSharedInfos = formSharedInfos;
    }
    
}
