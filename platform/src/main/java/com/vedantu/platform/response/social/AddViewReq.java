/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.social;

import com.vedantu.util.request.AbstractSocialActionReq;
import java.util.List;

/**
 *
 * @author parashar
 */
public class AddViewReq extends AbstractSocialActionReq{
    private Long startTime;

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(startTime == null){
            errors.add("startTime");
        }
        
        return errors;
    }    
    
}
