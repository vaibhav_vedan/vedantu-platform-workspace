package com.vedantu.platform.response.dinero;


public class GetHourlyRateResponse {

	private Long hourlyRate;
	private Long remainingHours;
	private Long lockedHours;
	private Long consumedHours;
	private Long totalHours;
	private Boolean isActive;
	
	public GetHourlyRateResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public GetHourlyRateResponse(Long hourlyRate, Long remainingHours, Long lockedHours, Long consumedHours,
			Long totalHours, Boolean isActive) {
		super();
		this.hourlyRate = hourlyRate;
		this.remainingHours = remainingHours;
		this.lockedHours = lockedHours;
		this.consumedHours = consumedHours;
		this.totalHours = totalHours;
		this.isActive = isActive;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Long getRemainingHours() {
		return remainingHours;
	}

	public void setRemainingHours(Long remainingHours) {
		this.remainingHours = remainingHours;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "GetHourlyRateResponse [hourlyRate=" + hourlyRate + ", remainingHours=" + remainingHours
				+ ", lockedHours=" + lockedHours + ", consumedHours=" + consumedHours + ", totalHours=" + totalHours
				+ ", isActive=" + isActive + "]";
	}

	public Long getLockedHours() {
		return lockedHours;
	}

	public void setLockedHours(Long lockedHours) {
		this.lockedHours = lockedHours;
	}

	public Long getConsumedHours() {
		return consumedHours;
	}

	public void setConsumedHours(Long consumedHours) {
		this.consumedHours = consumedHours;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}


	
}
