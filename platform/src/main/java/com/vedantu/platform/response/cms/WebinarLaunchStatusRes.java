package com.vedantu.platform.response.cms;

import com.vedantu.platform.entity.WebinarSpotInstanceMetadata;
import lombok.Data;

@Data
public class WebinarLaunchStatusRes {

    private WebinarSpotInstanceMetadata instanceMetadata;
    private String latestScreenshotUrl;
}
