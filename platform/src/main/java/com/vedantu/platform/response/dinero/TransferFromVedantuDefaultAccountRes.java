package com.vedantu.platform.response.dinero;

import com.vedantu.platform.pojo.dinero.Account;
import com.vedantu.util.fos.response.AbstractRes;

public class TransferFromVedantuDefaultAccountRes extends AbstractRes {

	private Integer balance;

	public TransferFromVedantuDefaultAccountRes(Account account) {

		super();
		this.balance = account.getBalance();
	}

	public TransferFromVedantuDefaultAccountRes(Integer balance) {
		super();
		this.balance = balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public TransferFromVedantuDefaultAccountRes() {
		// TODO Auto-generated constructor stub
	}

	public Integer getBalance() {

		return balance;
	}

	@Override
	public String toString() {
		return "TransferFromVedantuDefaultAccountRes [balance=" + balance + "]";
	}

}
