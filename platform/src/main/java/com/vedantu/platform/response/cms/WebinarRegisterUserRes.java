/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.cms;

import com.vedantu.platform.enums.cms.WebinarUserRegistrationStatus;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class WebinarRegisterUserRes extends AbstractRes{
    
    private String emailId;
    private String userId;
    private String registerJoinUrl;
    private WebinarUserRegistrationStatus status;
    private String statusMessage;
    private String webinarId;
    private String clevarTapId;
    private RequestSource source;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getRegisterJoinUrl() {
        return registerJoinUrl;
    }

    public void setRegisterJoinUrl(String registerJoinUrl) {
        this.registerJoinUrl = registerJoinUrl;
    }

    public WebinarUserRegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(WebinarUserRegistrationStatus status) {
        this.status = status;
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public String getClevarTapId() {
        return clevarTapId;
    }

    public void setClevarTapId(String clevarTapId) {
        this.clevarTapId = clevarTapId;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

	public RequestSource getSource() {
		return source;
	}

	public void setSource(RequestSource source) {
		this.source = source;
	}
    
}
