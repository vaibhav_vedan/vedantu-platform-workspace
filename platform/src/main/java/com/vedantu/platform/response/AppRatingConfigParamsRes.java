package com.vedantu.platform.response;

import java.util.List;

public class AppRatingConfigParamsRes {
    private Integer doubtYesFeedback;
    private Integer videoLike;
    private Integer pdfSave;
    private List<String> activeTabs;

    public Integer getDoubtYesFeedback() {
        return doubtYesFeedback;
    }

    public void setDoubtYesFeedback(Integer doubtYesFeedback) {
        this.doubtYesFeedback = doubtYesFeedback;
    }

    public Integer getVideoLike() {
        return videoLike;
    }

    public void setVideoLike(Integer videoLike) {
        this.videoLike = videoLike;
    }

    public Integer getPdfSave() {
        return pdfSave;
    }

    public void setPdfSave(Integer pdfSave) {
        this.pdfSave = pdfSave;
    }

    public List<String> getActiveTabs() {
        return activeTabs;
    }

    public void setActiveTabs(List<String> activeTabs) {
        this.activeTabs = activeTabs;
    }

    @Override
    public String toString() {
        return "AppRatingConfigParamsRes{" +
                "doubtYesFeedback=" + doubtYesFeedback +
                ", videoLike=" + videoLike +
                ", pdfSave=" + pdfSave +
                '}';
    }
}
