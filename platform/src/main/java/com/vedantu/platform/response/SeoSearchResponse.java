package com.vedantu.platform.response;

import lombok.Data;

@Data
public class SeoSearchResponse {

    private String categoryName;
    private String name;
    private String url;
    private String title;
    private String description;
    private String target;
    private String grade;
    private String subject;
}
