/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.feedback;

import com.vedantu.platform.pojo.feedback.ParentFormContextSharedInfo;
import com.vedantu.session.pojo.EntityType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetParentFormSharedInfoResponse {
    
    private EntityType contextType;
    
    private List<ParentFormContextSharedInfo> sharedInfos = new ArrayList<>();

    /**
     * @return the contextType
     */
    public EntityType getContextType() {
        return contextType;
    }

    /**
     * @param contextType the contextType to set
     */
    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    /**
     * @return the sharedInfos
     */
    public List<ParentFormContextSharedInfo> getSharedInfos() {
        return sharedInfos;
    }

    /**
     * @param sharedInfos the sharedInfos to set
     */
    public void setSharedInfos(List<ParentFormContextSharedInfo> sharedInfos) {
        this.sharedInfos = sharedInfos;
    }
    
    
}
