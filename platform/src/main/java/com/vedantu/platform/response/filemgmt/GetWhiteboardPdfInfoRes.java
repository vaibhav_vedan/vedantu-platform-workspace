/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.platform.pojo.wave.WhiteboardPDF;
import com.vedantu.platform.request.filemgmt.FileACL;
import com.vedantu.platform.request.filemgmt.UploadType;
import com.vedantu.util.enums.UploadState;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class GetWhiteboardPdfInfoRes extends AbstractRes{
    private String name;
    private String type;
    private Long size;
    private Long userId;
    private UploadType uploadType;
    private FileACL acl;
    private String folder;
    private String s3BucketName;
    private String s3BucketPath;
    private Long totalPageCount;
    private Long convertedPageCount;
    private float scale;
    private List<PdfToImageRes> images = new ArrayList<>();
    private UploadState state = UploadState.INIT;
   
    public GetWhiteboardPdfInfoRes(WhiteboardPDF whiteboardPDF){
        super();
        this.name=whiteboardPDF.getName();
        this.type=whiteboardPDF.getType();
        this.size=whiteboardPDF.getSize();
        this.userId=whiteboardPDF.getUserId();
        this.uploadType=whiteboardPDF.getUploadType();
        this.acl=whiteboardPDF.getAcl();
        this.folder=whiteboardPDF.getFolder();
        this.s3BucketName=whiteboardPDF.getS3BucketName();
        this.s3BucketPath=whiteboardPDF.getS3BucketPath();
        this.scale=whiteboardPDF.getScale();
        this.images=whiteboardPDF.getImages();
        this.state=whiteboardPDF.getState();
        this.convertedPageCount=whiteboardPDF.getConvertedPageCount();
        this.totalPageCount=whiteboardPDF.getTotalPageCount();
    }
    
    public List<PdfToImageRes> getImages() {
        return images;
    }

    public void setImages(List<PdfToImageRes> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public UploadType getUploadType() {
        return uploadType;
    }

    public void setUploadType(UploadType uploadType) {
        this.uploadType = uploadType;
    }

    public FileACL getAcl() {
        return acl;
    }

    public void setAcl(FileACL acl) {
        this.acl = acl;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getS3BucketName() {
        return s3BucketName;
    }

    public void setS3BucketName(String s3BucketName) {
        this.s3BucketName = s3BucketName;
    }

    public String getS3BucketPath() {
        return s3BucketPath;
    }

    public void setS3BucketPath(String s3BucketPath) {
        this.s3BucketPath = s3BucketPath;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public UploadState getState() {
        return state;
    }

    public void setState(UploadState state) {
        this.state = state;
    }
    
    public Long getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(Long totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public Long getConvertedPageCount() {
        return convertedPageCount;
    }

    public void setConvertedPageCount(Long convertedPageCount) {
        this.convertedPageCount = convertedPageCount;
    }

    @Override
    public String toString() {
        return "GetWhiteboardPdfInfoRes{" + "name=" + name + ", type=" + type + ", size=" + size + ", userId=" + userId + ", uploadType=" + uploadType + ", acl=" + acl + ", folder=" + folder + ", s3BucketName=" + s3BucketName + ", s3BucketPath=" + s3BucketPath + ", totalPageCount=" + totalPageCount + ", convertedPageCount=" + convertedPageCount + ", scale=" + scale + ", images=" + images + ", state=" + state + '}';
    }
    
}
