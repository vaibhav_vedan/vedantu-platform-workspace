package com.vedantu.platform.response.mobile;

import com.vedantu.platform.mongodbentities.mobile.MobileConfig;
import com.vedantu.platform.enums.mobile.MobilePlatform;



public class MobileConfigRes {

	private String userId;
	private String key;
	private String value;
	private String deviceId;
	private String versionCode;
	private MobilePlatform platform;
	private String platformVersion;
	public MobileConfigRes(MobileConfig mobileConfig) {
		super();
		this.userId = mobileConfig.getUserId().toString();
		this.key = mobileConfig.getKey();
		this.value = mobileConfig.getValue();
		this.deviceId = mobileConfig.getDeviceId();
		this.versionCode = mobileConfig.getVersionCode();
		this.platform = mobileConfig.getPlatform();
		this.platformVersion = mobileConfig.getPlatformVersion();
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	public MobilePlatform getPlatform() {
		return platform;
	}
	public void setPlatform(MobilePlatform platform) {
		this.platform = platform;
	}
	public String getPlatformVersion() {
		return platformVersion;
	}
	public void setPlatformVersion(String platformVersion) {
		this.platformVersion = platformVersion;
	}
	
	
}
