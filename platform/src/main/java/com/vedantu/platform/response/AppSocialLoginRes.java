/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response;

import com.vedantu.User.Role;
import com.vedantu.User.response.SignUpRes;
import com.vedantu.util.fos.response.AbstractRes;
import com.vedantu.util.security.HttpSessionData;

/**
 *
 * @author ajith
 */
public class AppSocialLoginRes extends AbstractRes{

    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String contactNumber;
    private String phoneCode;
    private Role role;
    private String state;

    public AppSocialLoginRes() {
    }

    public AppSocialLoginRes(HttpSessionData sessionData) {
        this.userId = sessionData.getUserId();
        this.firstName = sessionData.getFirstName();
        this.lastName = sessionData.getLastName();
        this.email = sessionData.getEmail();
        this.contactNumber = sessionData.getContactNumber();
        this.phoneCode = sessionData.getPhoneCode();
        this.role = sessionData.getRole();
        this.state = "LOGIN";
    }

    public AppSocialLoginRes(SignUpRes signUpRes) {
        this.userId = signUpRes.getUserId();
        this.firstName = signUpRes.getFirstName();
        this.lastName = signUpRes.getLastName();
        this.email = signUpRes.getEmail();
        this.contactNumber = signUpRes.getContactNumber();
        this.phoneCode = signUpRes.getPhoneCode();
        this.role = signUpRes.getRole();
        this.state = "SIGNUP";
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
