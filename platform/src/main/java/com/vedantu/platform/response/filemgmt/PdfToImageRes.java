/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class PdfToImageRes extends AbstractRes {

	private String url;
        private Long serverId;
        private int height;
        private int width;

	public String getUrl() {

		return url;
	}

	public void setUrl(String url) {

		this.url = url;
	}

        public Long getServerId() {
            return serverId;
        }

        public void setServerId(Long serverId) {
            this.serverId = serverId;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }
        
        public PdfToImageRes(){
            super();
        }
        
        public PdfToImageRes(String url, Long serverId, int height, int width) {
            super();
            this.url = url;
            this.serverId = serverId;
            this.height = height;
            this.width = width;
        }

        @Override
        public String toString() {
            return "PdfToImageRes{" + "url=" + url + ", serverId=" + serverId + ", height=" + height + ", width=" + width + '}';
        }
        
}
