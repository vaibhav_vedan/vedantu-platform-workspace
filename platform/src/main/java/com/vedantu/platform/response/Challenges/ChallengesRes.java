package com.vedantu.platform.response.Challenges;

import java.util.List;

import com.vedantu.platform.pojo.Challenges.ChallengeLeaderboard;
import com.vedantu.platform.pojo.Challenges.ChallengePOJO;

public class ChallengesRes {

	private List<ChallengePOJO> currentChallenge;
	private List<ChallengePOJO> yesterdaysChallenge;
	private List<ChallengePOJO> tomorrowsChallenge;
	private List<ChallengeLeaderboard> leaderboards;
	
	
	public ChallengesRes(List<ChallengePOJO> currentChallenge, List<ChallengePOJO> yesterdaysChallenge,
			List<ChallengePOJO> tomorrowsChallenge, List<ChallengeLeaderboard> leaderboards) {
		super();
		this.currentChallenge = currentChallenge;
		this.yesterdaysChallenge = yesterdaysChallenge;
		this.tomorrowsChallenge = tomorrowsChallenge;
		this.leaderboards = leaderboards;
	}

	public List<ChallengePOJO> getCurrentChallenge() {
		return currentChallenge;
	}

	public void setCurrentChallenge(List<ChallengePOJO> currentChallenge) {
		this.currentChallenge = currentChallenge;
	}

	public List<ChallengePOJO> getYesterdaysChallenge() {
		return yesterdaysChallenge;
	}

	public void setYesterdaysChallenge(List<ChallengePOJO> yesterdaysChallenge) {
		this.yesterdaysChallenge = yesterdaysChallenge;
	}

	public List<ChallengePOJO> getTomorrowsChallenge() {
		return tomorrowsChallenge;
	}

	public void setTomorrowsChallenge(List<ChallengePOJO> tomorrowsChallenge) {
		this.tomorrowsChallenge = tomorrowsChallenge;
	}

	public List<ChallengeLeaderboard> getLeaderboards() {
		return leaderboards;
	}
	
	public void setLeaderboards(List<ChallengeLeaderboard> leaderboards) {
		this.leaderboards = leaderboards;
	}
	
}
