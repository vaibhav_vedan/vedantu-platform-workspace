/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.platform.mongodbentities.filemgmt.FileMetadata;
import java.util.List;

/**
 *
 * @author somil
 */
public class GetFileMetadatRes {

	private List<FileMetadata> data;

	public List<FileMetadata> getData() {

		return data;
	}

	public void setData(List<FileMetadata> data) {

		this.data = data;
	}

}
