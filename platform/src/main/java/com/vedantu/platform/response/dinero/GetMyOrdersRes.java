package com.vedantu.platform.response.dinero;

import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.util.fos.response.AbstractListRes;

public class GetMyOrdersRes extends AbstractListRes<OrderInfo> {

    public GetMyOrdersRes() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
        return super.toString() + "GetMyOrdersRes []";
    }

}
