package com.vedantu.platform.response.dinero;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.fos.response.AbstractRes;

public class GetRechargeHistoryRes extends AbstractRes {

	private List<TransactionInfoRes> list = new ArrayList<TransactionInfoRes>();

	public void addItem(TransactionInfoRes item) {

		this.list.add(item);
	}

	public List<TransactionInfoRes> getList() {

		return list;
	}

	public GetRechargeHistoryRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetRechargeHistoryRes(List<TransactionInfoRes> list) {
		super();
		this.list = list;
	}

	public void setList(List<TransactionInfoRes> list) {

		this.list = list;
	}

	@Override
	public String toString() {
		return "GetRechargeHistoryRes [list=" + list + "]";
	}
}
