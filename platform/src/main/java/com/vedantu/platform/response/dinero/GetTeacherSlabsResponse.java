package com.vedantu.platform.response.dinero;

import java.util.List;

import com.vedantu.platform.pojo.dinero.Slab;

public class GetTeacherSlabsResponse {

	private List<Slab> slabs;

	public List<Slab> getSlabs() {
		return slabs;
	}

	public void setSlabs(List<Slab> slabs) {
		this.slabs = slabs;
	}

	@Override
	public String toString() {
		return "GetTeacherSlabsResponse [slabs=" + slabs + "]";
	}

	public GetTeacherSlabsResponse(List<Slab> slabs) {
		super();
		this.slabs = slabs;
	}

	public GetTeacherSlabsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
