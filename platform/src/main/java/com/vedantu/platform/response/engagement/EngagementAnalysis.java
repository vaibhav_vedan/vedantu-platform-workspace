package com.vedantu.platform.response.engagement;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class EngagementAnalysis extends AbstractMongoStringIdEntity{

	public String _id;
	public String subject;
	public String shapesandImages;
	public String noImages;
	public String colourChanges;
	public String teacherWriteLength;
	public String studentWriteLength;
	public String whiteboardSpaceUsage;
	public String teacherWriteTime;
	public String studentWriteTime;
	public String whiteboardInactiveTime;
	public String whiteBoards;
	public String hWAveragedensity;
	public String hWVariance;
	public String imageAnnotation;
	public String activeDuration;
	public String interaction;
	public String problemsDiscussed;
	public String teacherTime;
	public String studentTime;
	public String sTRatio;
	public String noActivity;
	public String sameTime;
	public String pace;
	public String audioDuration;
	public String score;
	public String quality;
	public Boolean analysable;
	public String analysableReason;
	public Long serverTime;
	public String teacherId;
	public String studentId;
	public String teacherName;
	public String studentName;
	public Long sessionStartTime;
	public Long teacherJoinTime;
	public Long studentJoinTime;
	public Long studentWaitTime;
	public String qualityShapesandImages;
	public String qualityColorChanges;
	public String qualityTeacherWriteLength;
	public String qualityWhiteBoardSpaceUsage;
	public String qualityTeacherWriteTime;
	public String qualityWhiteBoardInactiveTime;
	public String qualityWhiteBoards;
	public String qualityHwDensity;
	public String qualityHwVariance;
	public String qualityInteraction;
	public String qualitySTRatio;
	public String qualitySameTime;
	public Double scoreInteraction;
	public Double scoreWhiteBoardUsage;
	public Double scoreContentUsage;
	public Double scoreHandWriting;
	public Double scoreTotal;
	public String audioPresent;
	public String scheduledTime;
	private String teacherTechRating;
	private String studentTechRating;
	private String acadRating;
	private String audioReason;
	private String screenShareTime;
	public String engagementEntityType;
	public String engagementEntityId;
	

	public String getEngagementEntityType() {
		return engagementEntityType;
	}

	public void setEngagementEntityType(String engagementEntityType) {
		this.engagementEntityType = engagementEntityType;
	}

	public String getEngagementEntityId() {
		return engagementEntityId;
	}

	public void setEngagementEntityId(String engagementEntityId) {
		this.engagementEntityId = engagementEntityId;
	}

	public String getTeacherTechRating() {
		return teacherTechRating;
	}

	public void setTeacherTechRating(String teacherTechRating) {
		this.teacherTechRating = teacherTechRating;
	}

	public String getStudentTechRating() {
		return studentTechRating;
	}

	public void setStudentTechRating(String studentTechRating) {
		this.studentTechRating = studentTechRating;
	}

	public String getAcadRating() {
		return acadRating;
	}

	public void setAcadRating(String acadRating) {
		this.acadRating = acadRating;
	}

	public String getAudioReason() {
		return audioReason;
	}

	public void setAudioReason(String audioReason) {
		this.audioReason = audioReason;
	}

	public String getScreenShareTime() {
		return screenShareTime;
	}

	public void setScreenShareTime(String screenShareTime) {
		this.screenShareTime = screenShareTime;
	}


	


	public String getQualityShapesandImages() {
		return qualityShapesandImages;
	}

	public void setQualityShapesandImages(String qualityShapesandImages) {
		this.qualityShapesandImages = qualityShapesandImages;
	}

	public String getQualityColorChanges() {
		return qualityColorChanges;
	}

	public void setQualityColorChanges(String qualityColorChanges) {
		this.qualityColorChanges = qualityColorChanges;
	}

	public String getQualityTeacherWriteLength() {
		return qualityTeacherWriteLength;
	}

	public void setQualityTeacherWriteLength(String qualityTeacherWriteLength) {
		this.qualityTeacherWriteLength = qualityTeacherWriteLength;
	}

	public String getQualityWhiteBoardSpaceUsage() {
		return qualityWhiteBoardSpaceUsage;
	}

	public void setQualityWhiteBoardSpaceUsage(String qualityWhiteBoardSpaceUsage) {
		this.qualityWhiteBoardSpaceUsage = qualityWhiteBoardSpaceUsage;
	}

	public String getQualityTeacherWriteTime() {
		return qualityTeacherWriteTime;
	}

	public void setQualityTeacherWriteTime(String qualityTeacherWriteTime) {
		this.qualityTeacherWriteTime = qualityTeacherWriteTime;
	}

	public String getQualityWhiteBoardInactiveTime() {
		return qualityWhiteBoardInactiveTime;
	}

	public void setQualityWhiteBoardInactiveTime(String qualityWhiteBoardInactiveTime) {
		this.qualityWhiteBoardInactiveTime = qualityWhiteBoardInactiveTime;
	}

	public String getQualityWhiteBoards() {
		return qualityWhiteBoards;
	}

	public void setQualityWhiteBoards(String qualityWhiteBoards) {
		this.qualityWhiteBoards = qualityWhiteBoards;
	}

	public String getQualityHwVariance() {
		return qualityHwVariance;
	}

	public void setQualityHwVariance(String qualityHwVariance) {
		this.qualityHwVariance = qualityHwVariance;
	}

	public String getQualityHwDensity() {
		return qualityHwDensity;
	}

	public void setQualityHwDensity(String qualityHwDensity) {
		this.qualityHwDensity = qualityHwDensity;
	}
	
	public String getQualityInteraction() {
		return qualityInteraction;
	}

	public void setQualityInteraction(String qualityInteraction) {
		this.qualityInteraction = qualityInteraction;
	}

	public String getQualitySTRatio() {
		return qualitySTRatio;
	}

	public void setQualitySTRatio(String qualitySTRatio) {
		this.qualitySTRatio = qualitySTRatio;
	}

	public String getQualitySameTime() {
		return qualitySameTime;
	}

	public void setQualitySameTime(String qualitySameTime) {
		this.qualitySameTime = qualitySameTime;
	}

	public Double getScoreInteraction() {
		return scoreInteraction;
	}

	public void setScoreInteraction(Double scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}

	public Double getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}

	public void setScoreWhiteBoardUsage(Double scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}

	public Double getScoreContentUsage() {
		return scoreContentUsage;
	}

	public void setScoreContentUsage(Double scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}

	public Double getScoreTotal() {
		return scoreTotal;
	}

	public void setScoreTotal(Double scoreTotal) {
		this.scoreTotal = scoreTotal;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Long getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(Long sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public Long getTeacherJoinTime() {
		return teacherJoinTime;
	}

	public void setTeacherJoinTime(Long teacherJoinTime) {
		this.teacherJoinTime = teacherJoinTime;
	}

	public Long getStudentJoinTime() {
		return studentJoinTime;
	}

	public void setStudentJoinTime(Long studentJoinTime) {
		this.studentJoinTime = studentJoinTime;
	}

	public Long getStudentWaitTime() {
		return studentWaitTime;
	}

	public void setStudentWaitTime(Long studentWaitTime) {
		this.studentWaitTime = studentWaitTime;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getShapesandImages() {
		return shapesandImages;
	}

	public void setShapesandImages(String shapesandImages) {
		this.shapesandImages = shapesandImages;
	}

	public String getNoImages() {
		return noImages;
	}

	public void setNoImages(String noImages) {
		this.noImages = noImages;
	}

	public String getColourChanges() {
		return colourChanges;
	}

	public void setColourChanges(String colourChanges) {
		this.colourChanges = colourChanges;
	}

	public String getTeacherWriteLength() {
		return teacherWriteLength;
	}

	public void setTeacherWriteLength(String teacherWriteLength) {
		this.teacherWriteLength = teacherWriteLength;
	}

	public String getStudentWriteLength() {
		return studentWriteLength;
	}

	public void setStudentWriteLength(String studentWriteLength) {
		this.studentWriteLength = studentWriteLength;
	}

	public String getWhiteboardSpaceUsage() {
		return whiteboardSpaceUsage;
	}

	public void setWhiteboardSpaceUsage(String whiteboardSpaceUsage) {
		this.whiteboardSpaceUsage = whiteboardSpaceUsage;
	}

	public String getTeacherWriteTime() {
		return teacherWriteTime;
	}

	public void setTeacherWriteTime(String teacherWriteTime) {
		this.teacherWriteTime = teacherWriteTime;
	}

	public String getStudentWriteTime() {
		return studentWriteTime;
	}

	public void setStudentWriteTime(String studentWriteTime) {
		this.studentWriteTime = studentWriteTime;
	}

	public String getWhiteboardInactiveTime() {
		return whiteboardInactiveTime;
	}

	public void setWhiteboardInactiveTime(String whiteboardInactiveTime) {
		this.whiteboardInactiveTime = whiteboardInactiveTime;
	}

	public String getWhiteBoards() {
		return whiteBoards;
	}

	public void setWhiteBoards(String whiteBoards) {
		this.whiteBoards = whiteBoards;
	}

	public String getHWAveragedensity() {
		return hWAveragedensity;
	}

	public void setHWAveragedensity(String hWAveragedensity) {
		this.hWAveragedensity = hWAveragedensity;
	}

	public String getHWVariance() {
		return hWVariance;
	}

	public void setHWVariance(String hWVariance) {
		this.hWVariance = hWVariance;
	}

	public String getImageAnnotation() {
		return imageAnnotation;
	}

	public void setImageAnnotation(String imageAnnotation) {
		this.imageAnnotation = imageAnnotation;
	}

	public String getActiveDuration() {
		return activeDuration;
	}

	public void setActiveDuration(String activeDuration) {
		this.activeDuration = activeDuration;
	}

	public String getInteraction() {
		return interaction;
	}

	public void setInteraction(String interaction) {
		this.interaction = interaction;
	}

	public String getProblemsDiscussed() {
		return problemsDiscussed;
	}

	public void setProblemsDiscussed(String problemsDiscussed) {
		this.problemsDiscussed = problemsDiscussed;
	}

	public String getTeacherTime() {
		return teacherTime;
	}

	public void setTeacherTime(String teacherTime) {
		this.teacherTime = teacherTime;
	}

	public String getStudentTime() {
		return studentTime;
	}

	public void setStudentTime(String studentTime) {
		this.studentTime = studentTime;
	}

	public String getSTRatio() {
		return sTRatio;
	}

	public void setSTRatio(String sTRatio) {
		this.sTRatio = sTRatio;
	}

	public String getNoActivity() {
		return noActivity;
	}

	public void setNoActivity(String noActivity) {
		this.noActivity = noActivity;
	}

	public String getSameTime() {
		return sameTime;
	}

	public void setSameTime(String sameTime) {
		this.sameTime = sameTime;
	}

	public String getPace() {
		return pace;
	}

	public void setPace(String pace) {
		this.pace = pace;
	}

	public String getAudioDuration() {
		return audioDuration;
	}

	public void setAudioDuration(String audioDuration) {
		this.audioDuration = audioDuration;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Boolean getAnalysable() {
		return analysable;
	}

	public void setAnalysable(Boolean analysable) {
		this.analysable = analysable;
	}

	public String getAnalysableReason() {
		return analysableReason;
	}

	public void setAnalysableReason(String analysableReason) {
		this.analysableReason = analysableReason;
	}

	public Long getServerTime() {
		return serverTime;
	}

	public void setServerTime(Long serverTime) {
		this.serverTime = serverTime;
	}

	public String getAudioPresent() {
		return audioPresent;
	}

	public void setAudioPresent(String audioPresent) {
		this.audioPresent = audioPresent;
	}

	public String getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public EngagementAnalysis()
	{
		super();
	}

	public Double getScoreHandWriting() {
		return scoreHandWriting;
	}

	public void setScoreHandWriting(Double scoreHandWriting) {
		this.scoreHandWriting = scoreHandWriting;
	}

}