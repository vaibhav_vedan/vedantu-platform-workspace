/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.sessionmetrics;


import com.vedantu.platform.pojo.sessionmetrics.SessionAnalysisAggregateResult;
import com.vedantu.util.PlatformBasicResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class SessionAnalysisAggregateResponse  extends PlatformBasicResponse{
    
    private List<SessionAnalysisAggregateResult> result;

    public SessionAnalysisAggregateResponse(List<SessionAnalysisAggregateResult> result) {
        this.result = result;
    }

    public SessionAnalysisAggregateResponse() {
        this.result=new ArrayList();
    }
    
    public List<SessionAnalysisAggregateResult> getResult() {
        return result;
    }

    public void setResult(List<SessionAnalysisAggregateResult> result) {
        this.result = result;
    }
    
    public void addResult(SessionAnalysisAggregateResult result) {
       if(this.result!=null){
           this.result.add(result);
       }
    }

    @Override
    public String toString() {
        return "SessionAnalysisAggregateResponse{" + "result=" + result + '}';
    }
    
}
