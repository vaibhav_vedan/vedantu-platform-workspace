/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.social;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

/**
 *
 * @author ajith
 */
public class CommentRes extends AbstractMongoStringIdEntityBean {

    private SocialContextType socialContextType;
    private String contextId;
    private String parentId; //parentId of Socializer for nlevel commenting
    private Long userId;
    private RichTextFormat comment;
    private UserBasicInfo user;
    private int upVotes = 0;
    private boolean voted;
    private int replies = 0;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public RichTextFormat getComment() {
        return comment;
    }

    public void setComment(RichTextFormat comment) {
        this.comment = comment;
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public void setUser(UserBasicInfo user) {
        this.user = user;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public boolean isVoted() {
        return voted;
    }

    public void setVoted(boolean voted) {
        this.voted = voted;
    }

    public int getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(int upVotes) {
        this.upVotes = upVotes;
    }

    public int getReplies() {
        return replies;
    }

    public void setReplies(int replies) {
        this.replies = replies;
    }

}
