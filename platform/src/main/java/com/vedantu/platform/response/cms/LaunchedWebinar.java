package com.vedantu.platform.response.cms;

import com.vedantu.platform.enums.WebinarSpotInstanceStatus;
import lombok.Data;

@Data
public class LaunchedWebinar {

    private String id;
    private String webinarCode;
    private String title;
    private Long startTime;
    private Long endTime;
    private WebinarSpotInstanceStatus instanceStatus;

}
