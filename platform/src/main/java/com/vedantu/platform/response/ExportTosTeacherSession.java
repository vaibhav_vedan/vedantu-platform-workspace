package com.vedantu.platform.response;

import java.util.Date;

public class ExportTosTeacherSession {

	private String teacherName;
	private Long teacherId;
	private int toscount;
	private String feedback;
	private String NpsLast2weeks;
	private Date date;

	public ExportTosTeacherSession() {
		super();
	}

	public ExportTosTeacherSession(String teacherName, Long teacherId, int toscount, String feedback,
			String npsLast2weeks, Date date) {
		super();
		this.teacherName = teacherName;
		this.teacherId = teacherId;
		this.toscount = toscount;
		this.feedback = feedback;
		NpsLast2weeks = npsLast2weeks;
		this.date = date;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public int getToscount() {
		return toscount;
	}

	public void setToscount(int toscount) {
		this.toscount = toscount;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getNpsLast2weeks() {
		return NpsLast2weeks;
	}

	public void setNpsLast2weeks(String npsLast2weeks) {
		NpsLast2weeks = npsLast2weeks;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ExportTosTeacherSession [teacherName=" + teacherName + ", teacherId=" + teacherId + ", toscount="
				+ toscount + ", feedback=" + feedback + ", NpsLast2weeks=" + NpsLast2weeks + ", date=" + date + "]";
	}
	
}