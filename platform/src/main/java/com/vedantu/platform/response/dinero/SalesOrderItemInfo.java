/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.dinero;

import com.vedantu.offering.pojo.OfferingInfo;
import com.vedantu.offering.pojo.PlanInfo;
import com.vedantu.scheduling.pojo.session.Session;
import com.vedantu.session.pojo.EntityType;

/**
 *
 * @author somil
 */
public class SalesOrderItemInfo {

	private String entityId;
	private EntityType entityType;
	private Integer count;
	private Integer rate;
	private Integer cost;
	public PlanInfo plan;
	public OfferingInfo offering;
	public Session session;

	public PlanInfo getPlan() {
		return plan;
	}

	public void setPlan(PlanInfo plan) {
		this.plan = plan;
	}

	public OfferingInfo getOffering() {
		return offering;
	}

	public void setOffering(OfferingInfo offering) {
		this.offering = offering;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

}
