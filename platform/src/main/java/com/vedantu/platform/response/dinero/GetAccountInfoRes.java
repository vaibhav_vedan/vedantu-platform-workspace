package com.vedantu.platform.response.dinero;

import com.vedantu.platform.pojo.dinero.Account;
import com.vedantu.util.fos.response.AbstractRes;

public class GetAccountInfoRes extends AbstractRes {

    private Integer balance;
    private Integer lockedBalance;
    private Integer promotionalBalance;
    private Integer nonPromotionalBalance;
    // TODO verify HourBalance migration
    // private Long hourBalance;

    public GetAccountInfoRes() {
        super();
    }

    public GetAccountInfoRes(Account account) {

        super();
        this.balance = account.getBalance();
        this.lockedBalance = account.getLockedBalance();
        this.promotionalBalance = account.getPromotionalBalance();
        this.nonPromotionalBalance = account.getNonPromotionalBalance();
        // this.hourBalance = account.getHourBalance();
    }

    public Integer getPromotionalBalance() {
        return promotionalBalance;
    }

    public void setPromotionalBalance(Integer promotionalBalance) {
        this.promotionalBalance = promotionalBalance;
    }

    public Integer getNonPromotionalBalance() {
        return nonPromotionalBalance;
    }

    public void setNonPromotionalBalance(Integer nonPromotionalBalance) {
        this.nonPromotionalBalance = nonPromotionalBalance;
    }

    public Integer getBalance() {

        return balance;
    }

    public Integer getLockedBalance() {

        return lockedBalance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public void setLockedBalance(Integer lockedBalance) {
        this.lockedBalance = lockedBalance;
    }

    public GetAccountInfoRes(Integer balance, Integer lockedBalance) {
        super();
        this.balance = balance;
        this.lockedBalance = lockedBalance;
    }

}
