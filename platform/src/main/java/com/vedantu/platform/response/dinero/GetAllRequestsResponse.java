package com.vedantu.platform.response.dinero;

import java.util.List;

import com.vedantu.platform.pojo.dinero.TeacherRequest;

public class GetAllRequestsResponse {

	private List<TeacherRequest> teacherRequests;

	public List<TeacherRequest> getTeacherRequests() {
		return teacherRequests;
	}

	public void setTeacherRequests(List<TeacherRequest> teacherRequests) {
		this.teacherRequests = teacherRequests;
	}

	public GetAllRequestsResponse(List<TeacherRequest> teacherRequests) {
		super();
		this.teacherRequests = teacherRequests;
	}

	public GetAllRequestsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetAllRequestsResponse [teacherRequests=" + teacherRequests + "]";
	}

}
