package com.vedantu.platform.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
public class WaveWebinarResponse {

    private WaveWebinarResponsePojo result;
    private String errorCode;
    private String errorMessage;

    public WaveWebinarResponsePojo getResult() {
        return result;
    }

    public void setResult(WaveWebinarResponsePojo result) {
        this.result = result;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}