package com.vedantu.platform.response;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;

import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.platform.enums.cms.WebinarStatus;
import com.vedantu.platform.enums.cms.WebinarToolType;
import com.vedantu.platform.enums.cms.WebinarType;
import com.vedantu.platform.pojo.cms.LeaderBoard;
import com.vedantu.platform.pojo.cms.Testimonial;
import com.vedantu.platform.pojo.cms.WebinarLeaderBoardData;
import com.vedantu.platform.pojo.cms.WebinarScoreReport;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WebinarResponse {
    private String webinarCode;
    private String title;
    private List<String> grades;
    private List<String> targets;
    private List<String> tags;
    private List<String> boards;
    private String replayUrl;
    private Long startTime;
    private Long duration;
    private Long endTime;
    private JSONArray rescheduleData;
    private WebinarToolType toolType;
    private Map<String, Object> webinarInfo;
    private String teacherEmail;
    private Map<String, Object> teacherInfo;
    private Map<String, Object> courseInfo;
    private Map<String, Object> registerSectionInfo;
    private String gtwSeat;
    private String gtwWebinarId;
    private String emailImageUrl;
    private WebinarStatus webinarStatus;
    private Boolean showInPastWebinars = Boolean.FALSE;
    private List<LeaderBoard> leaderBoard;
    private WebinarType type;
    private Testimonial testimonial;
    private Set<String> subjects;
    private String subject;
    private Boolean gtwAttendenceSynced = false;
    private String sessionId;
    private Set<Long> taIds;
    private OTFSessionInfo sessionInfo;
    private String hlsUrl;


    private long totalParticipants;
    private List<WebinarScoreReport> report;
    private double userPercentile;
    private long myRank;
    private long totalQuizzesAsked;
    private long totalQuizzesCompleted;
    private long totalDoubtsSolved;
    private List<WebinarLeaderBoardData> webinarLeaderBoardDataList;

    private String masterClassUrl;
    private Long popUpDeplay;
    private List<String> contextTags;


    private String id;
    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String lastUpdatedBy; // For tracking last updated by
    private EntityState entityState = EntityState.ACTIVE;
    private Boolean isReminderSet = false;
}
