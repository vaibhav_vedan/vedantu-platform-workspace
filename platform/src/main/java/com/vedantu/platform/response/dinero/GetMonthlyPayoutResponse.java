package com.vedantu.platform.response.dinero;

import java.util.List;

import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.pojo.dinero.SessionPayout;

public class GetMonthlyPayoutResponse extends BaseResponse {

	private Long teacherId;
	private List<SessionPayout> sessionPayouts;
	private int totalSessions = 0;
	private int studentsTaught = 0;
	private int sessionEarnings = 0;
	private int cut = 0;
	private int incentive = 0;
	private int totalEarnings = 0;

	public GetMonthlyPayoutResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getTotalSessions() {
		return totalSessions;
	}

	public void setTotalSessions(int totalSessions) {
		this.totalSessions = totalSessions;
	}

	public int getStudentsTaught() {
		return studentsTaught;
	}

	public void setStudentsTaught(int studentsTaught) {
		this.studentsTaught = studentsTaught;
	}

	public int getSessionEarnings() {
		return sessionEarnings;
	}

	public void setSessionEarnings(int sessionEarnings) {
		this.sessionEarnings = sessionEarnings;
	}

	public int getCut() {
		return cut;
	}

	public void setCut(int cut) {
		this.cut = cut;
	}

	public int getIncentive() {
		return incentive;
	}

	public void setIncentive(int incentive) {
		this.incentive = incentive;
	}

	public int getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(int totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}



	@Override
	public String toString() {
		return "GetMonthlyPayoutResponse [teacherId=" + teacherId + ", sessionPayouts=" + sessionPayouts
				+ ", totalSessions=" + totalSessions + ", studentsTaught=" + studentsTaught + ", sessionEarnings="
				+ sessionEarnings + ", cut=" + cut + ", incentive=" + incentive + ", totalEarnings=" + totalEarnings
				+ "]";
	}



	public GetMonthlyPayoutResponse(ErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage);
		// TODO Auto-generated constructor stub
	}



	public GetMonthlyPayoutResponse(Long teacherId, List<SessionPayout> sessionPayouts, int totalSessions,
			int studentsTaught, int sessionEarnings, int cut, int incentive, int totalEarnings) {
		super();
		this.teacherId = teacherId;
		this.sessionPayouts = sessionPayouts;
		this.totalSessions = totalSessions;
		this.studentsTaught = studentsTaught;
		this.sessionEarnings = sessionEarnings;
		this.cut = cut;
		this.incentive = incentive;
		this.totalEarnings = totalEarnings;
	}



	public List<SessionPayout> getSessionPayouts() {
		return sessionPayouts;
	}



	public void setSessionPayouts(List<SessionPayout> sessionPayouts) {
		this.sessionPayouts = sessionPayouts;
	}

}
