package com.vedantu.platform.response.dinero;

import com.vedantu.util.fos.response.AbstractRes;

public class RechargeFreebiesAccountRes extends AbstractRes {

	@Override
	public String toString() {
		return "RechargeFreebiesAccountRes [balance=" + balance + "]";
	}

	public RechargeFreebiesAccountRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public RechargeFreebiesAccountRes(Integer balance) {
		super();
		this.balance = balance;
	}

	private Integer balance;

	public Integer getBalance() {

		return balance;
	}

}
