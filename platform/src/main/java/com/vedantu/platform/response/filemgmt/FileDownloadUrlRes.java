/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class FileDownloadUrlRes extends AbstractRes {

	private String url;
        private Long serverId;

	public String getUrl() {

		return url;
	}

	public void setUrl(String url) {

		this.url = url;
	}

        public Long getServerId() {
            return serverId;
        }

        public void setServerId(Long serverId) {
            this.serverId = serverId;
        }

        @Override
        public String toString() {
            return "FileDownloadUrlRes{" + "url=" + url + ", serverId=" + serverId + '}';
        }
}
