/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.board;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class RemoveTeacherMappingRes extends AbstractRes {

	private boolean removed;

	public RemoveTeacherMappingRes(boolean removed) {
		this.removed = removed;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

}
