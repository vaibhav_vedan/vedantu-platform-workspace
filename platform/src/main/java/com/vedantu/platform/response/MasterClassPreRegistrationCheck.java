package com.vedantu.platform.response;

import lombok.Data;

@Data
public class MasterClassPreRegistrationCheck {

    private Boolean isRegistered;
    private Boolean isTestAttempted;

}
