package com.vedantu.platform.response;

import com.vedantu.platform.entity.Career;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import lombok.*;

/**
 * @author MNPK
 */

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CareersWithFilterRes {
    private String technology;
    private String title;
    private String description;
    private String id;
    private Long creationTime;
    private String createdBy;
    private String createdByEmail;
    private Long lastUpdated;
    private String lastUpdatedBy;
    private String lastUpdatedByEmail;
    private EntityState entityState;

    public CareersWithFilterRes(Career career, String createdByEmail, String lastUpdatedByEmail) {
        this.technology = career.getTechnology();
        this.title = career.getTitle();
        this.description = career.getDescription();
        this.id = career.getId();
        this.creationTime = career.getCreationTime();
        this.createdBy = career.getCreatedBy();
        this.createdByEmail = createdByEmail;
        this.lastUpdated = career.getLastUpdated();
        this.lastUpdatedBy = career.getLastUpdatedBy();
        this.lastUpdatedByEmail = lastUpdatedByEmail;
        this.entityState = career.getEntityState();
    }

}
