/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.platform.request.filemgmt.UploadSignature;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author somil
 */
public class FileInitRes extends AbstractRes {

	private String name;
	private String type;
	private Long size;
	private String bucket;
	private String path;
	private Long id;
        //private String uploadUrl;

	private UploadSignature uploadSignature;

        /*
	public FileInitRes(File fileUpload, UploadSignature uploadSignature) {

		super();
		this.name = fileUpload.getName();
		this.type = fileUpload.getType();
		this.size = fileUpload.getSize();
		this.bucket = fileUpload.getBucket();
		this.path = fileUpload.getPath();
		this.id = fileUpload.getId();
		this.uploadSignature = uploadSignature;
	}
        */

    public FileInitRes(String name, String type, Long size, String bucket, String path, Long id) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.bucket = bucket;
        this.path = path;
        this.id = id;
        //this.url = url;
    }
        
        

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getType() {

		return type;
	}

	public void setType(String type) {

		this.type = type;
	}

	public String getBucket() {

		return bucket;
	}

	public void setBucket(String bucket) {

		this.bucket = bucket;
	}

	public String getPath() {

		return path;
	}

	public void setPath(String path) {

		this.path = path;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public Long getSize() {

		return size;
	}

	public void setSize(Long size) {

		this.size = size;
	}

        
	public UploadSignature getUploadSignature() {

		return uploadSignature;
	}

	public void setUploadSignature(UploadSignature uploadSignature) {

		this.uploadSignature = uploadSignature;
	}
        
/*
    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String url) {
        this.uploadUrl = url;
    }
*/
        
        
        

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("{name=").append(name).append(", type=").append(type)
				.append(", size=").append(size).append(", bucket=")
				.append(bucket).append(", path=").append(path).append(", id=")
				.append(id).append(", uploadSignature=")
				.append(uploadSignature).append("}")
                                ;
		return builder.toString();
	}

}

