package com.vedantu.platform.response.usermessage;

import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class SendQueryRes extends AbstractRes{
	private Boolean success;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "SendQueryRes {success:" + success + "}";
	}

}
