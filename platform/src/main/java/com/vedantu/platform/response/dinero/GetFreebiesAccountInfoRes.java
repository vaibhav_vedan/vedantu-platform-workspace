package com.vedantu.platform.response.dinero;

import com.vedantu.platform.pojo.dinero.Account;

public class GetFreebiesAccountInfoRes extends GetAccountInfoRes {

	@Override
	public String toString() {
		return "GetFreebiesAccountInfoRes []";
	}

	public GetFreebiesAccountInfoRes(Integer balance, Integer lockedBalance) {
		super(balance, lockedBalance);
		// TODO Auto-generated constructor stub
	}

	public GetFreebiesAccountInfoRes(Account account) {
		super(account);
	}

	public GetFreebiesAccountInfoRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
