/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.feedback;

import com.vedantu.platform.mongodbentities.feedback.FeedbackParentForm;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetParentFormsResponse {
    
    private List<FeedbackParentForm> parentForms = new ArrayList<>();

    /**
     * @return the parentForms
     */
    public List<FeedbackParentForm> getParentForms() {
        return parentForms;
    }

    /**
     * @param parentForms the parentForms to set
     */
    public void setParentForms(List<FeedbackParentForm> parentForms) {
        this.parentForms = parentForms;
    }
    
    
}
