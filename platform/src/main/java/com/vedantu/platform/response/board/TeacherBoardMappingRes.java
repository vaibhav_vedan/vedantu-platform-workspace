/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.board;

import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author somil
 */
public class TeacherBoardMappingRes extends AbstractRes {

    private Long id;
    private Long userId;
    private String category;
    private String grade;
    private List<BoardTreeInfo> boards;

    public TeacherBoardMappingRes() {
    }

    public TeacherBoardMappingRes(TeacherBoardMapping mapping, List<BoardTreeInfo> boards) {
        this.id = mapping.getId();
        this.userId = mapping.getUserId();
        this.category = mapping.getCategory();
        this.grade = mapping.getGrade();
        this.boards = boards;
    }

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public String getCategory() {
        return category;
    }

    public String getGrade() {
        return grade;
    }

    public List<BoardTreeInfo> getBoards() {
        return boards;
    }

}
