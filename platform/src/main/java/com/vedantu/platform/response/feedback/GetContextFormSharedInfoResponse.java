/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.feedback;

import com.vedantu.platform.pojo.feedback.ContextSharedFormInfo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetContextFormSharedInfoResponse {
    
    private List<ContextSharedFormInfo> contextSharedFormInfos = new ArrayList<>();

    /**
     * @return the batchSharedFormInfos
     */
    public List<ContextSharedFormInfo> getContextSharedFormInfos() {
        return contextSharedFormInfos;
    }

    /**
     * @param contextSharedFormInfos the batchSharedFormInfos to set
     */
    public void setContextSharedFormInfos(List<ContextSharedFormInfo> contextSharedFormInfos) {
        this.contextSharedFormInfos = contextSharedFormInfos;
    }
    
    
}
