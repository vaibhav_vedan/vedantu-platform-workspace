package com.vedantu.platform.response.dinero;

import com.vedantu.platform.pojo.dinero.Account;
import com.vedantu.platform.response.dinero.GetAccountInfoRes;

public class SubmitCashChequeRes extends GetAccountInfoRes {

	public SubmitCashChequeRes(Account account) {

		super(account);
	}

	public SubmitCashChequeRes() {
		super();
	}
}
