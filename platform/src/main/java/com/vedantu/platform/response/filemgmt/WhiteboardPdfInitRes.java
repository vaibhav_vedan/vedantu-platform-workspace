/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.platform.pojo.wave.WhiteboardPDF;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author jeet
 */
public class WhiteboardPdfInitRes  extends AbstractRes{
    
    WhiteboardPDF whiteboardPDFInfo;
    String uploadUrl;

    public WhiteboardPdfInitRes(WhiteboardPDF whiteboardPDFInfo, String uploadUrl) {
        this.whiteboardPDFInfo = whiteboardPDFInfo;
        this.uploadUrl = uploadUrl;
    }

    public WhiteboardPDF getWhiteboardPDFInfo() {
        return whiteboardPDFInfo;
    }

    public void setWhiteboardPDFInfo(WhiteboardPDF whiteboardPDFInfo) {
        this.whiteboardPDFInfo = whiteboardPDFInfo;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    @Override
    public String toString() {
        return "WhiteboardPdfInitRes{" + "whiteboardPDFInfo=" + whiteboardPDFInfo + ", uploadUrl=" + uploadUrl + '}';
    }
    
    
}
