package com.vedantu.platform.response.dinero;

import java.util.List;

import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.pojo.dinero.TeacherSessionInfo;

public class GetTeacherSessionInfoResponse extends BaseResponse {

	private List<TeacherSessionInfo> teacherSessionInfos;

	public GetTeacherSessionInfoResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetTeacherSessionInfoResponse(ErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage);
		// TODO Auto-generated constructor stub
	}

	public GetTeacherSessionInfoResponse(List<TeacherSessionInfo> teacherSessionInfos) {
		super();
		this.teacherSessionInfos = teacherSessionInfos;
	}

	@Override
	public String toString() {
		return "GetTeacherSessionInfoResponse [teacherSessionInfos=" + teacherSessionInfos + "]";
	}

	public List<TeacherSessionInfo> getTeacherSessionInfos() {
		return teacherSessionInfos;
	}

	public void setTeacherSessionInfos(List<TeacherSessionInfo> teacherSessionInfos) {
		this.teacherSessionInfos = teacherSessionInfos;
	}

}
