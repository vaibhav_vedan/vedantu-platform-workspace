/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.dinero;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.*;
import com.vedantu.dinero.response.InstalmentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somil
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesOrderInfo {

    private String id;
    private UserBasicInfo user;
    private Long userId;
    private List<SalesOrderItemInfo> orderItems = new ArrayList<>();
    private Integer amount;
    private Integer amountPaid;
    private PaymentStatus paymentStatus;
    private Long creationTime;
    private Boolean needRecharge;
    private String rechargeUrl;
    private PaymentGatewayName gatewayName;
    private TransactionStatus transactionStatus;
    private String forwardHttpMethod;
    private List<InstalmentInfo> instalmentInfos;
    private Integer promotionalAmount;
    private Integer nonPromotionalAmount;
    private Integer promotionalAmountPaid;
    private Integer nonPromotionalAmountPaid;
    private PaymentType paymentType;

    // Added as a part of tools enhancement
    private PaymentThrough paymentThrough;
    private TeamType teamType;
    private String agentCode;
    private String agentEmailId;
    private String reportingTeamLeadId;
    private String teamLeadEmailId;
    private Centre centre;
    private EnrollmentType enrollmentType;
    private FintechRisk fintechRisk;
    private RequestRefund requestRefund;
    private long netCollection;
    private String notes;
    private String subscriptionUpdateId;

    @Override
    public String toString() {
        return "SalesOrderInfo{" + "id=" + id + ", user=" + user + ", userId=" + userId + ", orderItems=" + orderItems + ", amount=" + amount + ", amountPaid=" + amountPaid + ", paymentStatus=" + paymentStatus + ", creationTime=" + creationTime + ", needRecharge=" + needRecharge + ", rechargeUrl=" + rechargeUrl + ", gatewayName=" + gatewayName + ", transactionStatus=" + transactionStatus + ", forwardHttpMethod=" + forwardHttpMethod + ", instalmentInfos=" + instalmentInfos + ", promotionalAmount=" + promotionalAmount + ", nonPromotionalAmount=" + nonPromotionalAmount + ", promotionalAmountPaid=" + promotionalAmountPaid + ", nonPromotionalAmountPaid=" + nonPromotionalAmountPaid + ", subscriptionUpdateId = "+subscriptionUpdateId+"  }";
    }

}
