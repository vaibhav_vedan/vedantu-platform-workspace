/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.filemgmt;

import com.vedantu.platform.mongodbentities.filemgmt.FileMetadata;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import java.util.List;

/**
 *
 * @author somil
 */
public class FileMetadataCollectionRes extends BasicRes {
    private List<FileMetadata> fileMetadataCollection;

	public List<FileMetadata> getFileMetadataCollection() {
		return fileMetadataCollection;
	}

	public void setFileMetadataCollection(List<FileMetadata> fileMetadataCollection) {
		this.fileMetadataCollection = fileMetadataCollection;
	}
}
