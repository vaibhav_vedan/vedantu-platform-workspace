/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.feedback;

import com.vedantu.platform.pojo.feedback.StudentSharedFormInfo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetFormsSharedWithStudentResponse {
    
    private List<StudentSharedFormInfo> studentSharedFormInfos = new ArrayList<>();

    /**
     * @return the studentSharedFormInfos
     */
    public List<StudentSharedFormInfo> getStudentSharedFormInfos() {
        return studentSharedFormInfos;
    }

    /**
     * @param studentSharedFormInfos the studentSharedFormInfos to set
     */
    public void setStudentSharedFormInfos(List<StudentSharedFormInfo> studentSharedFormInfos) {
        this.studentSharedFormInfos = studentSharedFormInfos;
    }

    @Override
    public String toString() {
        return "GetFormsSharedWithStudentResponse{" + "studentSharedFormInfos=" + studentSharedFormInfos + '}';
    }
    
}
