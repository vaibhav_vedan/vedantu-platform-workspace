package com.vedantu.platform.response.dinero;

import java.util.List;

import com.vedantu.platform.pojo.dinero.ApprovePrice;

public class GetPricingChangeRequestsResponse {

	private List<ApprovePrice> requests;

	@Override
	public String toString() {
		return "GetPricingChangeRequestsResponse [requests=" + requests + "]";
	}

	public GetPricingChangeRequestsResponse(List<ApprovePrice> requests) {
		super();
		this.requests = requests;
	}

	public List<ApprovePrice> getRequests() {
		return requests;
	}

	public void setRequests(List<ApprovePrice> requests) {
		this.requests = requests;
	}

	public GetPricingChangeRequestsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
