package com.vedantu.platform.response;

import com.vedantu.platform.pojo.cms.WebinarLeaderBoardData;
import com.vedantu.platform.pojo.cms.WebinarScoreReport;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@ToString
public class WaveWebinarResponsePojo {
    private long attendedStudentCount;
    private long attendingStudentCount;
    private long quizSoFar;
    private long totalQuizzesInSession;
    private long totalDoubtsResolved;
    private List<WebinarLeaderBoardData> topLeaderboardResult;
    private WebinarLeaderBoardData studentOwnRank;
    private List<WebinarScoreReport> report;

    public long getAttendedStudentCount() {
        return attendedStudentCount;
    }

    public void setAttendedStudentCount(long attendedStudentCount) {
        this.attendedStudentCount = attendedStudentCount;
    }

    public long getAttendingStudentCount() {
        return attendingStudentCount;
    }

    public void setAttendingStudentCount(long attendingStudentCount) {
        this.attendingStudentCount = attendingStudentCount;
    }

    public long getQuizSoFar() {
        return quizSoFar;
    }

    public void setQuizSoFar(long quizSoFar) {
        this.quizSoFar = quizSoFar;
    }

    public long getTotalQuizzesInSession() {
        return totalQuizzesInSession;
    }

    public void setTotalQuizzesInSession(long totalQuizzesInSession) {
        this.totalQuizzesInSession = totalQuizzesInSession;
    }

    public long getTotalDoubtsResolved() {
        return totalDoubtsResolved;
    }

    public void setTotalDoubtsResolved(long totalDoubtsResolved) {
        this.totalDoubtsResolved = totalDoubtsResolved;
    }

    public List<WebinarLeaderBoardData> getTopLeaderboardResult() {
        return topLeaderboardResult;
    }

    public void setTopLeaderboardResult(List<WebinarLeaderBoardData> topLeaderboardResult) {
        this.topLeaderboardResult = topLeaderboardResult;
    }

    public WebinarLeaderBoardData getStudentOwnRank() {
        return studentOwnRank;
    }

    public void setStudentOwnRank(WebinarLeaderBoardData studentOwnRank) {
        this.studentOwnRank = studentOwnRank;
    }

    public List<WebinarScoreReport> getReport() {
        return report;
    }

    public void setReport(List<WebinarScoreReport> report) {
        this.report = report;
    }
}
