package com.vedantu.platform.response.dinero;

import java.util.List;

import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.pojo.dinero.Transaction;

public class ExportDailyTransactionsResponse extends BaseResponse {

	private List<Transaction> transactions;

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "ExportDailyTransactionsResponse [transactions=" + transactions + "]";
	}

	public ExportDailyTransactionsResponse(List<Transaction> transactions) {
		super();
		this.transactions = transactions;
	}

	public ExportDailyTransactionsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExportDailyTransactionsResponse(ErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage);
		// TODO Auto-generated constructor stub
	}

}
