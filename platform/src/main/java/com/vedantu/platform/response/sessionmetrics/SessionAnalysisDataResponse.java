/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.sessionmetrics;

import com.vedantu.platform.pojo.sessionmetrics.SessionAnalysisData;
import com.vedantu.util.PlatformBasicResponse;
import java.util.List;

/**
 *
 * @author jeet
 */
public class SessionAnalysisDataResponse extends PlatformBasicResponse{
    
    private List<SessionAnalysisData> sessionData;

    public SessionAnalysisDataResponse(List<SessionAnalysisData> sessionData) {
        super();
        this.sessionData = sessionData;
    }
    public SessionAnalysisDataResponse() {
        super();
    }

    public List<SessionAnalysisData> getSessionData() {
        return sessionData;
    }

    public void setSessionData(List<SessionAnalysisData> sessionData) {
        this.sessionData = sessionData;
    }

    @Override
    public String toString() {
        return "SessionAnalysisDataResponse{" + "sessionData=" + sessionData + '}';
    }
    
    
}
