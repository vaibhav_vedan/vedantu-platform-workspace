/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response;

/**
 *
 * @author jeet
 */
public class TimeDifferenceClientRes {
    private Long timeDifference;
    private Long serverTime;
    private Long clientTime;

    public Long getTimeDifference() {
        return timeDifference;
    }

    public void setTimeDifference(Long timeDifference) {
        this.timeDifference = timeDifference;
    }

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public Long getClientTime() {
        return clientTime;
    }

    public void setClientTime(Long clientTime) {
        this.clientTime = clientTime;
    }

    @Override
    public String toString() {
        return "TimeDifferenceClientRes{" + "timeDifference=" + timeDifference + ", serverTime=" + serverTime + ", clientTime=" + clientTime + '}';
    }

    public TimeDifferenceClientRes(Long timeDifference, Long serverTime, Long clientTime) {
        this.timeDifference = timeDifference;
        this.serverTime = serverTime;
        this.clientTime = clientTime;
    }
    
    
}
