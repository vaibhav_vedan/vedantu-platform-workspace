package com.vedantu.platform.response.dinero;

public class GetAccountBalanceResponse {

	private int balance;
	private int promotionalBalance;
	private int nonPromotionalBalance;

	public GetAccountBalanceResponse(int balance, int promotionalBalance, int nonPromotionalBalance) {
		super();
		this.balance = balance;
		this.promotionalBalance = promotionalBalance;
		this.nonPromotionalBalance = nonPromotionalBalance;
	}

	public int getPromotionalBalance() {
		return promotionalBalance;
	}

	public void setPromotionalBalance(int promotionalBalance) {
		this.promotionalBalance = promotionalBalance;
	}

	public int getNonPromotionalBalance() {
		return nonPromotionalBalance;
	}

	public void setNonPromotionalBalance(int nonPromotionalBalance) {
		this.nonPromotionalBalance = nonPromotionalBalance;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public GetAccountBalanceResponse(int balance) {
		super();
		this.balance = balance;
	}

	public GetAccountBalanceResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetAccountBalanceResponse [balance=" + balance + "]";
	}
	

}
