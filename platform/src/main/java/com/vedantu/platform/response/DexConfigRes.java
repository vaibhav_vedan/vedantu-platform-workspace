package com.vedantu.platform.response;

public class DexConfigRes {

    private int chatTimeout;
    private Long poolTimeout;

    public int getChatTimeout() {
        return chatTimeout;
    }

    public void setChatTimeout(int chatTimeout) {
        this.chatTimeout = chatTimeout;
    }

    public Long getPoolTimeout() {
        return poolTimeout;
    }

    public void setPoolTimeout(Long poolTimeout) {
        this.poolTimeout = poolTimeout;
    }
}
