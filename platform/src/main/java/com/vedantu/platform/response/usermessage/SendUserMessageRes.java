/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.response.usermessage;

import com.vedantu.platform.mongodbentities.UserMessage;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.enums.MessageType;

/**
 *
 * @author jeet
 */
public class SendUserMessageRes extends PlatformBasicResponse {

	private String firstName;
	private String lastName;
	private MessageType messageType;
	private String messageContent;
	private String mobileNumber;

	public SendUserMessageRes() {

		super();
	}

	public SendUserMessageRes(UserMessage message) {

		if (null == message) {
			return;
		}
		this.messageType = message.getMessageType();
		this.messageContent = message.getMessage();
	}

	public String getFirstName() {

		return firstName;
	}

	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	public String getLastName() {

		return lastName;
	}

	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	public MessageType getMessageType() {

		return messageType;
	}

	public void setMessageType(MessageType messageType) {

		this.messageType = messageType;
	}

	public String getMessageContent() {

		return messageContent;
	}

	public void setMessageContent(String messageContent) {

		this.messageContent = messageContent;
	}

	public String getMobileNumber() {

		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {

		this.mobileNumber = mobileNumber;
	}
}

