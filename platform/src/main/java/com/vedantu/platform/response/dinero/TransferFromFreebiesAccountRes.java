package com.vedantu.platform.response.dinero;

import com.vedantu.platform.pojo.dinero.Account;

public class TransferFromFreebiesAccountRes extends GetAccountInfoRes {

	public TransferFromFreebiesAccountRes(Account account) {

		super(account);
	}

	@Override
	public String toString() {
		return "TransferFromFreebiesAccountRes []";
	}

	public TransferFromFreebiesAccountRes(Integer balance, Integer lockedBalance) {
		super(balance, lockedBalance);
		// TODO Auto-generated constructor stub
	}

	public TransferFromFreebiesAccountRes() {
		super();
	}
}
