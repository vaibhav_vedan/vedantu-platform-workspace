package com.vedantu.platform.response.social;

public class LastViewTime {
	private String videoId;
	private Long viewTime;
	
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public Long getViewTime() {
		return viewTime;
	}
	public void setViewTime(Long viewTime) {
		this.viewTime = viewTime;
	}
}
