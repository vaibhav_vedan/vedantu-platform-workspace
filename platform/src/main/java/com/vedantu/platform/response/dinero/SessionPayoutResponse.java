package com.vedantu.platform.response.dinero;

import com.vedantu.exception.BaseResponse;
import com.vedantu.platform.enums.SessionEntity;
import com.vedantu.util.enums.SessionModel;

public class SessionPayoutResponse extends BaseResponse {

	private Long sessionId;
	private Long subscriptionId;
	private Long duration;
	private Long date;
	private SessionModel model;
	private int teacherPayout;
	private int cut;
	private Long teacherId;
	private SessionEntity entity;

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public int getTeacherPayout() {
		return teacherPayout;
	}

	public void setTeacherPayout(int teacherPayout) {
		this.teacherPayout = teacherPayout;
	}

	public int getCut() {
		return cut;
	}

	public void setCut(int cut) {
		this.cut = cut;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SessionEntity getEntity() {
		return entity;
	}

	public void setEntity(SessionEntity entity) {
		this.entity = entity;
	}

	public SessionPayoutResponse(Long sessionId, Long subscriptionId, Long duration, Long date, SessionModel model, int teacherPayout,
			int cut, Long teacherId, SessionEntity entity) {
		super();
		this.sessionId = sessionId;
		this.subscriptionId = subscriptionId;
		this.duration = duration;
		this.date = date;
		this.model = model;
		this.teacherPayout = teacherPayout;
		this.cut = cut;
		this.teacherId = teacherId;
		this.entity = entity;
	}

	public SessionPayoutResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "SessionPayout [sessionId=" + sessionId + ", subscriptionId=" + subscriptionId + ", duration=" + duration
				+ ", date=" + date + ", model=" + model + ", teacherPayout=" + teacherPayout + ", cut=" + cut
				+ ", teacherId=" + teacherId + ", entity=" + entity + "]";
	}

}
