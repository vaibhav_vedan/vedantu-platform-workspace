package com.vedantu.platform.aws.entity;

import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.platform.aws.enums.StorageType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UploadFileDetails extends AbstractMongoStringIdEntity {
	private String key;
	private String bucketName;
	private String fileName;
	private String contentType;
	private StorageType storageType;
	private UploadFileSourceType fileSourceType;

	public UploadFileDetails(String key, String bucketName, String fileName, String contentType,
			StorageType storageType, UploadFileSourceType fileSourceType, Long creationTime, String createdBy,
			Long lastUpdated, String callingUserId) {
		super(creationTime, createdBy, lastUpdated, callingUserId);
		this.key = key;
		this.bucketName = bucketName;
		this.fileName = fileName;
		this.contentType = contentType;
		this.storageType = storageType;
		this.fileSourceType = fileSourceType;
	}
}
