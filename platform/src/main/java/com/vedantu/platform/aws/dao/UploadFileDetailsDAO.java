package com.vedantu.platform.aws.dao;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.aws.controllers.UploadFileController;
import com.vedantu.platform.aws.entity.UploadFileDetails;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class UploadFileDetailsDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UploadFileDetailsDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public UploadFileDetailsDAO() {
		super();
	}

	public void create(UploadFileDetails p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			logger.info("create UploadFileDetails exception : " + p.toString() + " ex: " + ex.toString());
		}
	}

	public UploadFileDetails getById(String id) {
		UploadFileDetails uploadFileDetails = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				uploadFileDetails = (UploadFileDetails) getEntityById(id, UploadFileDetails.class);
			}
		} catch (Exception ex) {
			logger.info("");
		}
		return uploadFileDetails;
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}
}
