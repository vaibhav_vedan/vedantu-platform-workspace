package com.vedantu.platform.aws.controllers;

import com.vedantu.User.Role;
import com.vedantu.aws.pojo.UploadFileInfo;
import com.vedantu.aws.response.UploadFileUrlRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.lms.cmds.request.GetCMDSUploadFileRes;
import com.vedantu.lms.cmds.request.GetCMDSUploadFileUrlReq;
import com.vedantu.platform.managers.aws.UploadFileManager;
import com.vedantu.util.LogFactory;

import io.swagger.annotations.ApiOperation;

import java.util.Arrays;
import java.util.Objects;

@CrossOrigin
@RestController
@RequestMapping(value = "/uploadFile")
public class UploadFileController {

	@Autowired
	private UploadFileManager uploadFileManager;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UploadFileController.class);

	@RequestMapping(value = "/cmds/answerUpload", method = RequestMethod.POST)
	@ApiOperation(value = "Upload answer url for cmds", notes = "Upload answer url for cmds")
	public GetCMDSUploadFileRes cmdsAnswerUpload(@RequestBody GetCMDSUploadFileUrlReq getCMDSUploadFileUrlReq)
			throws VException {
		getCMDSUploadFileUrlReq.verify();
		return uploadFileManager.generateUploadFileURL(getCMDSUploadFileUrlReq);
	}


	@ApiOperation(value = "Upload teachers' images", notes = "only accessible to admin users")
	@RequestMapping(value = "/teacher/uploadImage", method = RequestMethod.POST)
	public UploadFileUrlRes uploadTeacherImage(@RequestBody UploadFileInfo uploadFileInfo)
			throws VException {

		if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
			throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
		}
		sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, true);
		return uploadFileManager.uploadTeacherImage(uploadFileInfo);
	}
}
