package com.vedantu.platform.engagement.cron;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.platform.managers.aws.AwslambdaManager;
import com.vedantu.util.LogFactory;

@Service
public class AsynchronousEnagement {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(AsynchronousEnagement.class);

	@Async
	public void calculateEngagementAsync(Long id) {
		logger.info("Async function called .....");
		AwslambdaManager awslambdaManager = new AwslambdaManager();
		logger.info("awslambdaManager created");
		awslambdaManager.invokeEngagementLambda(id);
		logger.info("returned from invokeEngagementLambda");
	}
}
