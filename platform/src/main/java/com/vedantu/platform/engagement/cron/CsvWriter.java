package com.vedantu.platform.engagement.cron;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.LogFactory;

public class CsvWriter { 

	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CsvWriter.class);
	
	private final String CRLF = "\r\n"; 
	private String delimiter = ",";
	public StringBuilder sb = new StringBuilder();

	public void setDelimiter(String delimiter) { 
		this.delimiter = delimiter; 
	} 

	public CsvWriter() {
		super();
		sb.append("SessionId");
		sb.append(delimiter);
		sb.append("ServerTime");
		sb.append(delimiter);
		sb.append("Subject");
		sb.append(delimiter);
		sb.append("TeacherName");
		sb.append(delimiter);
		sb.append("TeacherId");
		sb.append(delimiter);
		sb.append("StudentWaitTime");
		sb.append(delimiter);
		sb.append("StudentName");
		sb.append(delimiter);
		sb.append("StudentId");
		sb.append(delimiter);
		sb.append("Audio");
		sb.append(delimiter);
		sb.append("ShapesandImages");
		sb.append(delimiter);
		sb.append("NoImages");
		sb.append(delimiter);
		sb.append("ColourChanges");
		sb.append(delimiter);
		sb.append("TeacherWriteLength");
		sb.append(delimiter);
		sb.append("StudentWriteLength");
		sb.append(delimiter);
		sb.append("WhiteboardSpaceUsage");
		sb.append(delimiter);
		sb.append("TeacherWriteTime");
		sb.append(delimiter);
		sb.append("StudentWriteTime");
		sb.append(delimiter);
		sb.append("WhiteboardInactiveTime");
		sb.append(delimiter);
		sb.append("WhiteBoards");
		sb.append(delimiter);
		sb.append("HWAveragedensity"); 
		sb.append(delimiter);
		sb.append("HWVariance"); 
		sb.append(delimiter);
		sb.append("ImageAnnotation"); 
		sb.append(delimiter);
		sb.append( "ActiveDuration"); 
		sb.append(delimiter);
		sb.append( "Interaction"); 
		sb.append(delimiter);
		sb.append( "ProblemsDiscussed"); 
		sb.append(delimiter);
		sb.append( "TeacherTime");
		sb.append(delimiter);
		sb.append( "StudentTime");
		sb.append(delimiter);
		sb.append( "STRatio");
		sb.append(delimiter);
		sb.append( "NoActivity");
		sb.append(delimiter);
		sb.append( "SameTime");
		sb.append(delimiter);
		sb.append( "Pace");
		sb.append(delimiter);
		sb.append( "AudioDuration");
		sb.append(delimiter);
		sb.append("qualityShapesandImages");
		sb.append(delimiter);
		sb.append("qualityColorChanges");
		sb.append(delimiter);
		sb.append("qualityTeacherWriteLength");
		sb.append(delimiter);
		sb.append("qualityWhiteBoardSpaceUsage");
		sb.append(delimiter);
		sb.append("qualityTeacherWriteTime");
		sb.append(delimiter);
		sb.append("qualityWhiteBoardInactiveTime");
		sb.append(delimiter);
		sb.append("qualityWhiteBoards");
		sb.append(delimiter);
		sb.append("qualityHandwritingDensity");
		sb.append(delimiter);
		sb.append("qualityHandwritingVariance");
		sb.append(delimiter);
		sb.append("qualityInteraction");
		sb.append(delimiter);
		sb.append("qualitySTRatio");
		sb.append(delimiter);
		sb.append("qualitySameTime");
		sb.append(delimiter);
		sb.append( "Score");
		sb.append(delimiter);
		sb.append("scoreInteraction");
		sb.append(delimiter);
		sb.append("scoreWhiteBoardUsage");
		sb.append(delimiter);
		sb.append("scoreContentUsage");
		sb.append(delimiter);
		sb.append("scoreHandwriting");
		sb.append(delimiter);
		sb.append("scoreTotal");
		sb.append(delimiter);
		sb.append( "Quality");
		sb.append(delimiter);
		sb.append("Analysable");
		sb.append(delimiter);
		sb.append( "AnalysableReason");

	}

	public void exportCsv(List<EngagementAnalysis> list, String filename) { 
		//			Filesb sb = new Filesb(filename,true);
		sb.append(delimiter+ CRLF);

		for (int j = 0; j < list.size(); j++) {
			logger.info("SessionId = "+list.get(j).get_id());
			sb.append("'");
			sb.append( list.get(j).get_id()); 
			sb.append(delimiter);
			sb.append( list.get(j).getServerTime()); 
			sb.append(delimiter);
			sb.append( list.get(j).getSubject()); 
			sb.append(delimiter);
			sb.append( list.get(j).getTeacherName());
			sb.append(delimiter);
			sb.append( "'"+list.get(j).getTeacherId());
			sb.append(delimiter);
			sb.append( list.get(j).getStudentWaitTime());
			sb.append(delimiter);
			sb.append( list.get(j).getStudentName());
			sb.append(delimiter);
			sb.append( "'"+list.get(j).getStudentId());
			sb.append(delimiter);
			sb.append( list.get(j).getAudioPresent());
			sb.append(delimiter);
			sb.append( list.get(j).getShapesandImages()); 
			sb.append(delimiter);
			sb.append( list.get(j).getNoImages()); 
			sb.append(delimiter);
			sb.append( list.get(j).getColourChanges()); 
			sb.append(delimiter);
			sb.append( list.get(j).getTeacherWriteLength()); 
			sb.append(delimiter);
			sb.append( list.get(j).getStudentWriteLength()); 
			sb.append(delimiter);
			sb.append( list.get(j).getWhiteboardSpaceUsage()); 
			sb.append(delimiter);
			sb.append( list.get(j).getTeacherWriteTime()); 
			sb.append(delimiter);
			sb.append( list.get(j).getStudentTime()); 
			sb.append(delimiter);
			sb.append( list.get(j).getWhiteboardInactiveTime()); 
			sb.append(delimiter);
			sb.append( list.get(j).getWhiteBoards()); 
			sb.append(delimiter);
			sb.append( list.get(j).getHWAveragedensity()); 
			sb.append(delimiter);
			sb.append( list.get(j).getHWVariance()); 
			sb.append(delimiter);
			sb.append( list.get(j).getImageAnnotation()); 
			sb.append(delimiter);
			sb.append( list.get(j).getActiveDuration()); 
			sb.append(delimiter);
			sb.append( list.get(j).getInteraction()); 
			sb.append(delimiter);
			sb.append( list.get(j).getProblemsDiscussed()); 
			sb.append(delimiter);
			sb.append( list.get(j).getTeacherTime());
			sb.append(delimiter);
			sb.append( list.get(j).getStudentTime());
			sb.append(delimiter);
			sb.append( list.get(j).getSTRatio());
			sb.append(delimiter);
			sb.append( list.get(j).getNoActivity());
			sb.append(delimiter);
			sb.append( list.get(j).getSameTime());
			sb.append(delimiter);
			sb.append( list.get(j).getPace());
			sb.append(delimiter);
			sb.append( list.get(j).getAudioDuration());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityShapesandImages());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityColorChanges());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityTeacherWriteLength());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityWhiteBoardSpaceUsage());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityTeacherWriteTime());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityWhiteBoardInactiveTime());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityWhiteBoards());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityHwDensity());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityHwVariance());
			sb.append(delimiter);
			sb.append( list.get(j).getQualityInteraction());
			sb.append(delimiter);
			sb.append( list.get(j).getQualitySTRatio());
			sb.append(delimiter);
			sb.append( list.get(j).getQualitySameTime());
			sb.append(delimiter);
			sb.append( list.get(j).getScore());
			sb.append(delimiter);
			sb.append(list.get(j).getScoreInteraction().toString());
			sb.append(delimiter);
			sb.append(list.get(j).getScoreWhiteBoardUsage().toString());
			sb.append(delimiter);
			sb.append(list.get(j).getScoreContentUsage()).toString();
			sb.append(delimiter);
			sb.append(list.get(j).getScoreHandWriting()).toString();
			sb.append(delimiter);
			sb.append(list.get(j).getScoreTotal().toString());
			sb.append(delimiter);
			sb.append( list.get(j).getQuality());
			sb.append(delimiter);
			sb.append( Boolean.toString(list.get(j).getAnalysable()));
			sb.append(delimiter);
			sb.append( list.get(j).getAnalysableReason());
			//						sb.append(delimiter);
			//						sb.append( Long.toString(list.get(j).getServerTime())); 
			//Add delimiter and end of the line 
			if (j < list.size() - 1) { 
				sb.append(delimiter + CRLF); 
			} 
		}
		logger.info("Export call ended");
	}

}
