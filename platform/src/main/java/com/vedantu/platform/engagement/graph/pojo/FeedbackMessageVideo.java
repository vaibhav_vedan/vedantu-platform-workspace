package com.vedantu.platform.engagement.graph.pojo;

import org.springframework.stereotype.Service;

@Service
public class FeedbackMessageVideo {

	public static String[] shapesandImagesQuality = {};
	
	public static String[] colourChangesQuality = {};
	
	public static String[] teacherWriteLengthQuality = {};

	public static String[] teacherWriteTimeQuality = {};

	public static String[] whiteboardSpaceUsageQuality = {};

	public static String[] whiteBoardsQuality = {};

	public static String[] whiteboardInactiveTimeQuality = {};

	public static String[] interactionQuality = {"https://www.youtube.com/embed/QLSwFDhBhmg"};

	public static String[] sTRatioQuality = {"https://www.youtube.com/embed/QLSwFDhBhmg"};

	public static String[] sameTimeQuality = {"https://www.youtube.com/embed/DIES3Riw_Ds"};
	
	public static String[] scoreContentUsage = {"https://www.youtube.com/embed/GdSnXMVmZxE"};

	public static String[] scoreInteraction = {"https://www.youtube.com/embed/QLSwFDhBhmg"};

	public static String[] scoreWhiteBoardUsage = {"https://www.youtube.com/embed/3Bm4TvAF7lk"};

	public static String[] getShapesandImagesQuality() {
		return shapesandImagesQuality;
	}

	public static void setShapesandImagesQuality(String[] shapesandImagesQuality) {
		FeedbackMessageVideo.shapesandImagesQuality = shapesandImagesQuality;
	}

	public static String[] getColourChangesQuality() {
		return colourChangesQuality;
	}

	public static void setColourChangesQuality(String[] colourChangesQuality) {
		FeedbackMessageVideo.colourChangesQuality = colourChangesQuality;
	}

	public static String[] getTeacherWriteLengthQuality() {
		return teacherWriteLengthQuality;
	}

	public static void setTeacherWriteLengthQuality(String[] teacherWriteLengthQuality) {
		FeedbackMessageVideo.teacherWriteLengthQuality = teacherWriteLengthQuality;
	}

	public static String[] getTeacherWriteTimeQuality() {
		return teacherWriteTimeQuality;
	}

	public static void setTeacherWriteTimeQuality(String[] teacherWriteTimeQuality) {
		FeedbackMessageVideo.teacherWriteTimeQuality = teacherWriteTimeQuality;
	}

	public static String[] getWhiteboardSpaceUsageQuality() {
		return whiteboardSpaceUsageQuality;
	}

	public static void setWhiteboardSpaceUsageQuality(String[] whiteboardSpaceUsageQuality) {
		FeedbackMessageVideo.whiteboardSpaceUsageQuality = whiteboardSpaceUsageQuality;
	}

	public static String[] getWhiteBoardsQuality() {
		return whiteBoardsQuality;
	}

	public static void setWhiteBoardsQuality(String[] whiteBoardsQuality) {
		FeedbackMessageVideo.whiteBoardsQuality = whiteBoardsQuality;
	}

	public static String[] getWhiteboardInactiveTimeQuality() {
		return whiteboardInactiveTimeQuality;
	}

	public static void setWhiteboardInactiveTimeQuality(String[] whiteboardInactiveTimeQuality) {
		FeedbackMessageVideo.whiteboardInactiveTimeQuality = whiteboardInactiveTimeQuality;
	}

	public static String[] getInteractionQuality() {
		return interactionQuality;
	}

	public static void setInteractionQuality(String[] interactionQuality) {
		FeedbackMessageVideo.interactionQuality = interactionQuality;
	}

	public static String[] getsTRatioQuality() {
		return sTRatioQuality;
	}

	public static void setsTRatioQuality(String[] sTRatioQuality) {
		FeedbackMessageVideo.sTRatioQuality = sTRatioQuality;
	}

	public static String[] getSameTimeQuality() {
		return sameTimeQuality;
	}

	public static void setSameTimeQuality(String[] sameTimeQuality) {
		FeedbackMessageVideo.sameTimeQuality = sameTimeQuality;
	}

	public static String[] getScoreContentUsage() {
		return scoreContentUsage;
	}

	public static void setScoreContentUsage(String[] scoreContentUsage) {
		FeedbackMessageVideo.scoreContentUsage = scoreContentUsage;
	}

	public static String[] getScoreInteraction() {
		return scoreInteraction;
	}

	public static void setScoreInteraction(String[] scoreInteraction) {
		FeedbackMessageVideo.scoreInteraction = scoreInteraction;
	}

	public static String[] getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}

	public static void setScoreWhiteBoardUsage(String[] scoreWhiteBoardUsage) {
		FeedbackMessageVideo.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}

}

