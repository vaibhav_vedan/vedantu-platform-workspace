package com.vedantu.platform.engagement.cron;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.LogFactory;

@Service
public class PopulateFields {
	
	@Autowired
	private LogFactory logFactory;
	
	@Autowired
	private AverageReport averageReport;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EngagementCron.class);

	
	@Autowired
	private ScoreBySubject scoreBySubject;
	
	public void roundoffValues(EngagementAnalysis engagementAnalysis) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
	{
		String[] fieldsArray = {"shapesandImages","colourChanges","teacherWriteLength","whiteboardSpaceUsage",
								"teacherWriteTime","whiteboardInactiveTime","whiteBoards","activeDuration",
								"interaction","sTRatio","sameTime","scoreTotal"};
		List<String> fields = Arrays.asList(fieldsArray);
		for(int i=0;i<fields.size();i++)
		{
			Field field = engagementAnalysis.getClass().getField(fields.get(i));
			logger.info(field.getName());
			if(field.get(engagementAnalysis) == null)
				continue;
			Double value = averageReport.getValue(field.get(engagementAnalysis));
			value = Math.round(value*100)/100.00;
			logger.info("Value " +value);
			
			if(field.get(engagementAnalysis).getClass().getSimpleName().equals("String"))
				field.set(engagementAnalysis, value.toString());
			else if(field.get(engagementAnalysis).getClass().getSimpleName().equals("Double"))
				field.set(engagementAnalysis, value);
		}
	}
	public void setUnsetFields(List<EngagementAnalysis> completeList) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
	{
		logger.info("setunsetFieldscalled");
//		String[] fieldsArray = {"qualityHwDensity","qualityHwVariance"};
//		
//		List<String> fields = Arrays.asList(fieldsArray);
		for(int i=0; i<completeList.size(); i++)
		{
//			for(int j =0;j<fields.size(); j++)
//			{
//				Field field = engagementAnalysis.getClass().getField(fields.get(j));
//				if(null == field.get(completeList.get(i)))
//				{
					scoreBySubject.scoringBySubject(completeList.get(i), completeList.get(i).getSubject());
//					break;
//				}
//			}
		}
	}
}
