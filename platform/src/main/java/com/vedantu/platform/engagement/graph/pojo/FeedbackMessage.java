package com.vedantu.platform.engagement.graph.pojo;

import org.springframework.stereotype.Service;

@Service
public class FeedbackMessage {

	public static String shapesandImagesQuality = "Use more shapes, images or question images on the whiteboard as it makes the learning experience superior.";

	public static String colourChangesQuality = "You may use different colors to write on the whiteboard as it makes learning way too effective by increasing the visibility of the written content.";

	public static String teacherWriteLengthQuality = "As a great practice write and annotate sufficiently well on the whiteboard compared to writing sparingly.";

	public static String teacherWriteTimeQuality = "As a great practice write and annotate sufficiently well on the whiteboard compared to writing sparingly.";

	public static String whiteboardSpaceUsageQuality = "Try to use the whiteboard space smartly and judiciously i.e. to ensure a lot of space is not wasted and our whiteboard content be neat and well spaced out.";

	public static String whiteBoardsQuality = "Use a new whiteboard page  rather than erasing or deleting the taught content on a whiteboard page. It makes good sense to start a question or a new concept from a fresh whiteboard. This approach helps students revise the session more effectively";

	public static String whiteboardInactiveTimeQuality = "The system has observed that your whiteboard usage is low. Try enough to keep the student visually engaged with the help of the whiteboard";

	public static String interactionQuality = "Make all efforts to engage the student by asking more questions, confirming whether the student understood the concept,etc.. as this interaction makes the learner more comfortable to ask more questions and be more open and hence improving the learning outcome.";

	public static String sTRatioQuality = "Make all efforts to engage the student by asking more questions, confirming whether the student understood the concept,etc.. as this interaction makes the learner more comfortable to ask more questions and be more open and hence improving the learning outcome.";

	public static String sameTimeQuality = "Implement the technique of writing and speaking at the same time more often on the whiteboard.This also include speaking and annotating or pointing on diagrams, images, graphs, etc... This makes the teaching more engrossing and effective";

	public static String scoreContentUsage = "Focus on the the following practices :- " + "<br>"
			+ "Making written content more visible and effective by using different colours." + "<br>"
			+ "Use of Content in the form relevant Images and Animations.";

	public static String scoreInteraction = "Focussing on making the session more interactive with the student by :- "
			+ "<br>"
			+ "Keep the student talking about whether he/she has understood, visualised, tried the question,etc.. "
			+ "<br>" + "Speak while writing and annotating on the whiteboard.";

	public static String scoreWhiteBoardUsage = "Focus on the the following practices :- " + "<br>"
			+ "Writing enough and well spaced content on the Whiteboard." + "<br>"
			+ "Using a fresh whiteboard for new concepts and questions and deleting or erasing all (to allow reuse or revise)";

	public static String getShapesandImagesQuality() {
		return shapesandImagesQuality;
	}

	public static void setShapesandImagesQuality(String shapesandImagesQuality) {
		FeedbackMessage.shapesandImagesQuality = shapesandImagesQuality;
	}

	public static String getColourChangesQuality() {
		return colourChangesQuality;
	}

	public static void setColourChangesQuality(String colourChangesQuality) {
		FeedbackMessage.colourChangesQuality = colourChangesQuality;
	}

	public static String getTeacherWriteLengthQuality() {
		return teacherWriteLengthQuality;
	}

	public static void setTeacherWriteLengthQuality(String teacherWriteLengthQuality) {
		FeedbackMessage.teacherWriteLengthQuality = teacherWriteLengthQuality;
	}

	public static String getTeacherWriteTimeQuality() {
		return teacherWriteTimeQuality;
	}

	public static void setTeacherWriteTimeQuality(String teacherWriteTimeQuality) {
		FeedbackMessage.teacherWriteTimeQuality = teacherWriteTimeQuality;
	}

	public static String getWhiteboardSpaceUsageQuality() {
		return whiteboardSpaceUsageQuality;
	}

	public static void setWhiteboardSpaceUsageQuality(String whiteboardSpaceUsageQuality) {
		FeedbackMessage.whiteboardSpaceUsageQuality = whiteboardSpaceUsageQuality;
	}

	public static String getWhiteBoardsQuality() {
		return whiteBoardsQuality;
	}

	public static void setWhiteBoardsQuality(String whiteBoardsQuality) {
		FeedbackMessage.whiteBoardsQuality = whiteBoardsQuality;
	}

	public static String getWhiteboardInactiveTimeQuality() {
		return whiteboardInactiveTimeQuality;
	}

	public static void setWhiteboardInactiveTimeQuality(String whiteboardInactiveTimeQuality) {
		FeedbackMessage.whiteboardInactiveTimeQuality = whiteboardInactiveTimeQuality;
	}

	public static String getInteractionQuality() {
		return interactionQuality;
	}

	public static void setInteractionQuality(String interactionQuality) {
		FeedbackMessage.interactionQuality = interactionQuality;
	}

	public static String getsTRatioQuality() {
		return sTRatioQuality;
	}

	public static void setsTRatioQuality(String sTRatioQuality) {
		FeedbackMessage.sTRatioQuality = sTRatioQuality;
	}

	public static String getSameTimeQuality() {
		return sameTimeQuality;
	}

	public static void setSameTimeQuality(String sameTimeQuality) {
		FeedbackMessage.sameTimeQuality = sameTimeQuality;
	}

	public static String getScoreContentUsage() {
		return scoreContentUsage;
	}

	public static void setScoreContentUsage(String scoreContentUsage) {
		FeedbackMessage.scoreContentUsage = scoreContentUsage;
	}

	public static String getScoreInteraction() {
		return scoreInteraction;
	}

	public static void setScoreInteraction(String scoreInteraction) {
		FeedbackMessage.scoreInteraction = scoreInteraction;
	}

	public static String getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}

	public static void setScoreWhiteBoardUsage(String scoreWhiteBoardUsage) {
		FeedbackMessage.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}

}