package com.vedantu.platform.engagement.cron;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class Scoring {

	public String qualityShapesandImages;
	public String qualityColorChanges;
	public String qualityTeacherWriteLength;
	public String qualityWhiteBoardSpaceUsage;
	public String qualityTeacherWriteTime;
	public String qualityWhiteBoardInactiveTime;
	public String qualityWhiteBoards;
	public String qualityHwDensity;
	public String qualityInteraction;
	public String qualitySTRatio;
	public String qualitySameTime;
	public int score;
	public int flag;
	public Scoring() {
		super();
		// TODO Auto-generated constructor stub
	}	
}
