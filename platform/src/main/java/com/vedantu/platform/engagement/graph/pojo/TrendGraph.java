package com.vedantu.platform.engagement.graph.pojo;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class TrendGraph {

	Long teacherId;
	String subject;
	String type;
	public ArrayList<Double> trendValues = null;
	
	public TrendGraph() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<Double> getTrendValues() {
		return trendValues;
	}

	public void setTrendValues(ArrayList<Double> trendValues) {
		this.trendValues = trendValues;
	}
}
