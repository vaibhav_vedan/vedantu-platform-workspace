package com.vedantu.platform.engagement.cron;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.reflect.Field;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.LogFactory;

@Service
public class AverageReport {
	
	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EngagementCron.class);

	public Long calculateAverageTeacherReport(List<EngagementAnalysis> list, AverageEngagementAnalysis averageEngagementAnalysis) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
	{
		ArrayList<Double> averageList = new ArrayList<Double>();
		int audioCount = 0;
		Long errorCount = (long) 0;
		String[] wbFieldsArray = { "studentWaitTime","shapesandImages","colourChanges","teacherWriteLength","whiteboardSpaceUsage","teacherWriteTime","whiteboardInactiveTime",
				"whiteBoards","hWAveragedensity","hWVariance","imageAnnotation"};

		String[] audioFieldsArray = {"interaction","sTRatio","sameTime"};

		String[] scoreFieldsArray = {"score","scoreInteraction",
				"scoreWhiteBoardUsage","scoreContentUsage","scoreTotal"};

		List<String> wbFields = Arrays.asList(wbFieldsArray);
		List<String> audioFields = Arrays.asList(audioFieldsArray);
		List<String> scoreFields = Arrays.asList(scoreFieldsArray);
		List<String> totalFields = new ArrayList<String>();
		totalFields.addAll(wbFields);
		totalFields.addAll(audioFields);
		totalFields.addAll(scoreFields);


		for(int j= 0; j <list.size(); j++)
		{
			Boolean errorFlag = false;

			for(String fieldname : totalFields)
			{
				Field field = EngagementAnalysis.class.getField(fieldname);
				Field fieldAverage = AverageEngagementAnalysis.class.getField(fieldname);
				Double d = null;
				if(field.get(list.get(j)) == null || field.get(list.get(j)).equals("NaN") || field.get(list.get(j)).equals("Infinity"))
				{
					errorFlag = true;
					errorCount++;
					break;
				}
				d = getValue(field.get(list.get(j)));

				if(fieldAverage.get(averageEngagementAnalysis) == null)
					fieldAverage.set(averageEngagementAnalysis, d);
				else
				{
					Double current = d + (Double)fieldAverage.get(averageEngagementAnalysis);
					fieldAverage.set(averageEngagementAnalysis, current);
				}
			}
			if(errorFlag == false && list.get(j).getAudioPresent().equals("Audio Present"))
			{
				audioCount++; //positive case 
			}
		}

		for(String total : totalFields)
		{
			Field field = averageEngagementAnalysis.getClass().getField(total);
			Double value = (Double)field.get(averageEngagementAnalysis);
			if(audioFields.contains(field.getName()) && audioCount > 0)
			{
				value /= audioCount;
			}
			else if(list.size() - errorCount > 0)
			{
				value /= (list.size() - errorCount);
			}
			logger.info(field.getName() + " = "+ value);
			field.set(averageEngagementAnalysis, value);
		}
		return (list.size() - errorCount);
	}
	public <T> Double getValue(T arg)
	{
		if(arg.getClass().getSimpleName().equals("Long"))
		{
			Double d = ((Number)arg).doubleValue();
			return d;
		}
		else if(arg.getClass().getSimpleName().equals("String"))
		{
			Double d = Double.valueOf((String)arg);
			return d;
		}
		else
		{
			return (Double) arg;
		}
	}
}
