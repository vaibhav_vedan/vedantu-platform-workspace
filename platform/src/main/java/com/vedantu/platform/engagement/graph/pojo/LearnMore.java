package com.vedantu.platform.engagement.graph.pojo;

import java.util.List;
import java.util.Map;

public class LearnMore {

	private Map<String,List<String>> video;
	private Map<String,Boolean> image;
	private Map<String,String> messageHowToImprove;
	
	public LearnMore() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Map<String, List<String>> getVideo() {
		return video;
	}

	public void setVideo(Map<String, List<String>> video) {
		this.video = video;
	}

	public Map<String, Boolean> getImage() {
		return image;
	}

	public void setImage(Map<String, Boolean> image) {
		this.image = image;
	}

	public Map<String, String> getMessageHowToImprove() {
		return messageHowToImprove;
	}

	public void setMessageHowToImprove(Map<String, String> messageHowToImprove) {
		this.messageHowToImprove = messageHowToImprove;
	}

	@Override
	public String toString() {
		return "LearnMore [video=" + video + ", image=" + image + "]";
	}
}
