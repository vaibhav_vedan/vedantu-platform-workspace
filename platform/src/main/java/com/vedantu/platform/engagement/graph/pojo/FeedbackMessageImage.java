package com.vedantu.platform.engagement.graph.pojo;

import org.springframework.stereotype.Service;

@Service
public class FeedbackMessageImage {

	public Boolean shapesandImagesQuality = true;

	public Boolean colourChangesQuality = true;
	
	public Boolean teacherWriteLengthQuality = true;

	public Boolean teacherWriteTimeQuality = true;

	public Boolean whiteboardSpaceUsageQuality = true;

	public Boolean whiteBoardsQuality = true;

	public Boolean whiteboardInactiveTimeQuality = false;

	public Boolean interactionQuality = false;

	public Boolean sTRatioQuality = false;

	public Boolean sameTimeQuality = false;
	
	public Boolean scoreContentUsage = false;

	public Boolean scoreInteraction = false;

	public Boolean scoreWhiteBoardUsage = false;

	public Boolean getShapesandImagesQuality() {
		return shapesandImagesQuality;
	}

	public void setShapesandImagesQuality(Boolean shapesandImagesQuality) {
		this.shapesandImagesQuality = shapesandImagesQuality;
	}

	public Boolean getColourChangesQuality() {
		return colourChangesQuality;
	}

	public void setColourChangesQuality(Boolean colourChangesQuality) {
		this.colourChangesQuality = colourChangesQuality;
	}

	public Boolean getTeacherWriteLengthQuality() {
		return teacherWriteLengthQuality;
	}

	public void setTeacherWriteLengthQuality(Boolean teacherWriteLengthQuality) {
		this.teacherWriteLengthQuality = teacherWriteLengthQuality;
	}

	public Boolean getTeacherWriteTimeQuality() {
		return teacherWriteTimeQuality;
	}

	public void setTeacherWriteTimeQuality(Boolean teacherWriteTimeQuality) {
		this.teacherWriteTimeQuality = teacherWriteTimeQuality;
	}

	public Boolean getWhiteboardSpaceUsageQuality() {
		return whiteboardSpaceUsageQuality;
	}

	public void setWhiteboardSpaceUsageQuality(Boolean whiteboardSpaceUsageQuality) {
		this.whiteboardSpaceUsageQuality = whiteboardSpaceUsageQuality;
	}

	public Boolean getWhiteBoardsQuality() {
		return whiteBoardsQuality;
	}

	public void setWhiteBoardsQuality(Boolean whiteBoardsQuality) {
		this.whiteBoardsQuality = whiteBoardsQuality;
	}

	public Boolean getWhiteboardInactiveTimeQuality() {
		return whiteboardInactiveTimeQuality;
	}

	public void setWhiteboardInactiveTimeQuality(Boolean whiteboardInactiveTimeQuality) {
		this.whiteboardInactiveTimeQuality = whiteboardInactiveTimeQuality;
	}

	public Boolean getInteractionQuality() {
		return interactionQuality;
	}

	public void setInteractionQuality(Boolean interactionQuality) {
		this.interactionQuality = interactionQuality;
	}

	public Boolean getsTRatioQuality() {
		return sTRatioQuality;
	}

	public void setsTRatioQuality(Boolean sTRatioQuality) {
		this.sTRatioQuality = sTRatioQuality;
	}

	public Boolean getSameTimeQuality() {
		return sameTimeQuality;
	}

	public void setSameTimeQuality(Boolean sameTimeQuality) {
		this.sameTimeQuality = sameTimeQuality;
	}

	public Boolean getScoreContentUsage() {
		return scoreContentUsage;
	}

	public void setScoreContentUsage(Boolean scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}

	public Boolean getScoreInteraction() {
		return scoreInteraction;
	}

	public void setScoreInteraction(Boolean scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}

	public Boolean getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}

	public void setScoreWhiteBoardUsage(Boolean scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}	
}
