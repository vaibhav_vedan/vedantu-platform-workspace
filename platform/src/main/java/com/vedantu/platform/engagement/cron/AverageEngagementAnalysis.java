package com.vedantu.platform.engagement.cron;

import org.springframework.stereotype.Service;

public class AverageEngagementAnalysis{

	public String teacherId;
	public String teacherName;
	public String subject;
	public Long numberOfSessions;
	public Double studentWaitTime;
	public String shapesandImagesQuality;
	public String colourChangesQuality;
	public String teacherWriteLengthQuality;
	public String whiteboardSpaceUsageQuality;
	public String teacherWriteTimeQuality;
	public String whiteboardInactiveTimeQuality;
	public String whiteBoardsQuality;
	public String interactionQuality;
	public String sTRatioQuality;
	public String sameTimeQuality;
	public String scoreTotalQuality;
	
	public Double shapesandImages;
	public Double colourChanges;
	public Double teacherWriteLength;
	public Double whiteboardSpaceUsage;
	public Double teacherWriteTime;
	public Double whiteboardInactiveTime;
	public Double whiteBoards;
	public Double hWAveragedensity;
	public Double hWVariance;
	public Double imageAnnotation;
	public Double interaction;
	public Double sTRatio;
	public Double sameTime;
	public Double score;
	public Double scoreInteraction;
	public Double scoreWhiteBoardUsage;
	public Double scoreContentUsage;
	public Double scoreTotal;
	
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getSubject() {
		return subject;
		
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Long getNumberOfSessions() {
		return numberOfSessions;
	}
	public void setNumberOfSessions(Long numberOfSessions) {
		this.numberOfSessions = numberOfSessions;
	}
	public Double getStudentWaitTime() {
		return studentWaitTime;
	}
	public void setStudentWaitTime(Double studentWaitTime) {
		this.studentWaitTime = studentWaitTime;
	}
	public String getShapesandImagesQuality() {
		return shapesandImagesQuality;
	}
	public void setShapesandImagesQuality(String shapesandImagesQuality) {
		this.shapesandImagesQuality = shapesandImagesQuality;
	}
	public String getColourChangesQuality() {
		return colourChangesQuality;
	}
	public void setColourChangesQuality(String colourChangesQuality) {
		this.colourChangesQuality = colourChangesQuality;
	}
	public String getTeacherWriteLengthQuality() {
		return teacherWriteLengthQuality;
	}
	public void setTeacherWriteLengthQuality(String teacherWriteLengthQuality) {
		this.teacherWriteLengthQuality = teacherWriteLengthQuality;
	}
	public String getWhiteboardSpaceUsageQuality() {
		return whiteboardSpaceUsageQuality;
	}
	public void setWhiteboardSpaceUsageQuality(String whiteboardSpaceUsageQuality) {
		this.whiteboardSpaceUsageQuality = whiteboardSpaceUsageQuality;
	}
	public String getTeacherWriteTimeQuality() {
		return teacherWriteTimeQuality;
	}
	public void setTeacherWriteTimeQuality(String teacherWriteTimeQuality) {
		this.teacherWriteTimeQuality = teacherWriteTimeQuality;
	}
	public String getWhiteboardInactiveTimeQuality() {
		return whiteboardInactiveTimeQuality;
	}
	public void setWhiteboardInactiveTimeQuality(String whiteboardInactiveTimeQuality) {
		this.whiteboardInactiveTimeQuality = whiteboardInactiveTimeQuality;
	}
	public String getWhiteBoardsQuality() {
		return whiteBoardsQuality;
	}
	public void setWhiteBoardsQuality(String whiteBoardsQuality) {
		this.whiteBoardsQuality = whiteBoardsQuality;
	}
	public String getInteractionQuality() {
		return interactionQuality;
	}
	public void setInteractionQuality(String interactionQuality) {
		this.interactionQuality = interactionQuality;
	}
	public String getsTRatioQuality() {
		return sTRatioQuality;
	}
	public void setsTRatioQuality(String sTRatioQuality) {
		this.sTRatioQuality = sTRatioQuality;
	}
	public String getSameTimeQuality() {
		return sameTimeQuality;
	}
	public void setSameTimeQuality(String sameTimeQuality) {
		this.sameTimeQuality = sameTimeQuality;
	}
	public String getScoreTotalQuality() {
		return scoreTotalQuality;
	}
	public void setScoreTotalQuality(String scoreTotalQuality) {
		this.scoreTotalQuality = scoreTotalQuality;
	}
	public Double getShapesandImages() {
		return shapesandImages;
	}
	public void setShapesandImages(Double shapesandImages) {
		this.shapesandImages = shapesandImages;
	}
	public Double getColourChanges() {
		return colourChanges;
	}
	public void setColourChanges(Double colourChanges) {
		this.colourChanges = colourChanges;
	}
	public Double getTeacherWriteLength() {
		return teacherWriteLength;
	}
	public void setTeacherWriteLength(Double teacherWriteLength) {
		this.teacherWriteLength = teacherWriteLength;
	}
	public Double getWhiteboardSpaceUsage() {
		return whiteboardSpaceUsage;
	}
	public void setWhiteboardSpaceUsage(Double whiteboardSpaceUsage) {
		this.whiteboardSpaceUsage = whiteboardSpaceUsage;
	}
	public Double getTeacherWriteTime() {
		return teacherWriteTime;
	}
	public void setTeacherWriteTime(Double teacherWriteTime) {
		this.teacherWriteTime = teacherWriteTime;
	}
	public Double getWhiteboardInactiveTime() {
		return whiteboardInactiveTime;
	}
	public void setWhiteboardInactiveTime(Double whiteboardInactiveTime) {
		this.whiteboardInactiveTime = whiteboardInactiveTime;
	}
	public Double getWhiteBoards() {
		return whiteBoards;
	}
	public void setWhiteBoards(Double whiteBoards) {
		this.whiteBoards = whiteBoards;
	}
	public Double gethWAveragedensity() {
		return hWAveragedensity;
	}
	public void sethWAveragedensity(Double hWAveragedensity) {
		this.hWAveragedensity = hWAveragedensity;
	}
	public Double gethWVariance() {
		return hWVariance;
	}
	public void sethWVariance(Double hWVariance) {
		this.hWVariance = hWVariance;
	}
	public Double getImageAnnotation() {
		return imageAnnotation;
	}
	public void setImageAnnotation(Double imageAnnotation) {
		this.imageAnnotation = imageAnnotation;
	}
	public Double getInteraction() {
		return interaction;
	}
	public void setInteraction(Double interaction) {
		this.interaction = interaction;
	}
	public Double getsTRatio() {
		return sTRatio;
	}
	public void setsTRatio(Double sTRatio) {
		this.sTRatio = sTRatio;
	}
	public Double getSameTime() {
		return sameTime;
	}
	public void setSameTime(Double sameTime) {
		this.sameTime = sameTime;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public Double getScoreInteraction() {
		return scoreInteraction;
	}
	public void setScoreInteraction(Double scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}
	public Double getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}
	public void setScoreWhiteBoardUsage(Double scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}
	public Double getScoreContentUsage() {
		return scoreContentUsage;
	}
	public void setScoreContentUsage(Double scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}
	public Double getScoreTotal() {
		return scoreTotal;
	}
	public void setScoreTotal(Double scoreTotal) {
		this.scoreTotal = scoreTotal;
	}
}
