package com.vedantu.platform.engagement.cron;

import java.lang.reflect.Field;
import java.util.List;

import org.springframework.stereotype.Service;


@Service
public class TeacherCsvWriter { 

	public TeacherCsvWriter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String exportCsv(List<AverageEngagementAnalysis> list) throws IllegalArgumentException, IllegalAccessException { 

//		String[] fieldsArray = { "teacherId","teacherName","subject","Number Of Sessions","studentWaitTime",
//				"shapesandImagesQuality","colourChangesQuality",
//				"teacherWriteLengthQuality","whiteboardSpaceUsageQuality","teacherWriteTimeQuality",
//				"whiteboardInactiveTimeQuality","whiteBoardsQuality","interactionQuality","sTRatioQuality","sameTimeQuality",
//				"shapesandImages","colourChanges","teacherWriteLength","whiteboardSpaceUsage",
//				"teacherWriteTime","whiteboardInactiveTime","whiteBoards","hWAveragedensity","hWVariance","imageAnnotation",
//				"interaction","sTRatio","sameTime","score","scoreInteraction","scoreWhiteBoardUsage","scoreContentUsage","scoreTotal"};
//
//		List<String> fields = Arrays.asList(fieldsArray);	
//
//		String data = StringUtils.join(fields.iterator(), ",");
		String data = "";
		for(Field field: AverageEngagementAnalysis.class.getDeclaredFields())
		{
			data += field.getName() + ",";
		}
		for(int i =0; i<list.size(); i++)
		{
			data += "\n";

			for (Field field : AverageEngagementAnalysis.class.getDeclaredFields())
			{
				data += field.get(list.get(i))+",";
			}
			
		}
		return data;

	}
}

