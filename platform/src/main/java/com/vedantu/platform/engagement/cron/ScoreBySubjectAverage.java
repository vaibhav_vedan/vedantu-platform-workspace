package com.vedantu.platform.engagement.cron;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ScoreBySubjectAverage {

	int bad;
	int score;
	ArrayList<String> scoreList =new ArrayList<String>();
	public Scoring Score(AverageEngagementAnalysis param, double[] val, Scoring scoring)
	{
		int scr=0;
		int flg=0;
		double sh = param.getShapesandImages() == null ? 0:param.getShapesandImages();
		double cc = param.getColourChanges() == null ? 0: param.getColourChanges();
		double twl = param.getTeacherWriteLength() == null ? 0: param.getTeacherWriteLength();
		double wbsu = param.getWhiteboardSpaceUsage() == null ? 0: param.getWhiteboardSpaceUsage();
		double twt = param.getTeacherWriteTime() == null ? 0: param.getTeacherWriteTime();
		double wit = param.getWhiteboardInactiveTime() == null ? 0: param.getWhiteboardInactiveTime();
		double nwb = param.getWhiteBoards() == null ? 0: param.getWhiteBoards();
		double inter = param.getInteraction() == null ? 0: param.getInteraction();
		double str = param.getsTRatio() == null ? 0: param.getsTRatio();
		double st = param.getSameTime() == null ? 0: param.getSameTime();
		double hwd = param.gethWAveragedensity() == null ? 0: param.gethWAveragedensity();
		double scoreTotal = param.getScoreTotal() == null ? 0:param.getScoreTotal();

		if(sh<val[0]){
			scr +=0;flg+=1;
			param.setShapesandImagesQuality("Bad");
		}
		else if(sh<val[1]){
			scr +=1;
			param.setShapesandImagesQuality("Okay");
		}
		else {
			scr+=2;
			param.setShapesandImagesQuality("Good");
		}

		if(cc<val[2]){
			scr +=0;flg+=1;
			param.setColourChangesQuality("Bad");
		}
		else if(cc<val[3]){
			scr +=1;
			param.setColourChangesQuality("Okay");
		}
		else {
			scr+=2;
			param.setColourChangesQuality("Good");
		}


		if(twl<val[4]){
			scr +=0;flg+=1;
			param.setTeacherWriteLengthQuality("Bad");
		}
		else if(twl<val[5]){
			scr +=1;
			param.setTeacherWriteLengthQuality("Okay");
		}
		else {
			scr+=2;
			param.setTeacherWriteLengthQuality("Good");
		}

		if(wbsu<val[6]){
			scr +=0;flg+=1;
			param.setWhiteboardSpaceUsageQuality("Bad");
		}
		else if(wbsu<val[7]){
			scr +=1;
			param.setWhiteboardSpaceUsageQuality("Okay");
		}
		else {
			scr+=2;
			param.setWhiteboardSpaceUsageQuality("Good");
		}

		if(twt<val[8]){
			scr +=0;flg+=1;
			param.setTeacherWriteTimeQuality("Bad");
		}
		else if(twt<val[9]){
			scr +=1;
			param.setTeacherWriteTimeQuality("Okay");
		}
		else {
			scr+=2;
			param.setTeacherWriteTimeQuality("Good");
		}

		if(wit>val[10]){
			scr +=0;flg+=1;
			param.setWhiteboardInactiveTimeQuality("Bad");
		}
		else if(wit>val[11]){
			scr +=1;
			param.setWhiteboardInactiveTimeQuality("Okay");
		}
		else {
			scr+=2;
			param.setWhiteboardInactiveTimeQuality("Good");
		}

		if(nwb<val[12]){
			scr +=0;flg+=1;
			param.setWhiteBoardsQuality("Bad");
		}
		else if(nwb<val[13]){
			scr +=1;
			param.setWhiteBoardsQuality("Okay");
		}
		else {
			scr+=2;
			param.setWhiteBoardsQuality("Good");
		}



		if(inter<val[14]){
			scr +=0;flg+=1;
			param.setInteractionQuality("Bad");
		}
		else if(inter<val[15]){
			scr +=1;
			param.setInteractionQuality("Okay");
		}
		else {
			scr+=2;
			param.setInteractionQuality("Good");
		}

		if(str<val[16]){
			scr +=0;flg+=1;
			param.setsTRatioQuality("Bad");
		}
		else if(str<val[17]){
			scr +=1;
			param.setsTRatioQuality("Okay");
		}
		else {
			scr+=2;
			param.setsTRatioQuality("Good");
		}

		if(st<val[18]){
			scr +=0;flg+=1;
			param.setSameTimeQuality("Bad");
		}
		else if(st<val[19]){
			scr +=1;
			param.setSameTimeQuality("Okay");
		}
		else {
			scr+=2;
			param.setSameTimeQuality("Good");
		}
		
		if(hwd < val[20])
		{
			
		}
		else if(hwd < val[21])
		{
			
		}
		else
		{
			
		}
		if(scoreTotal <= 3)
		{
			param.setScoreTotalQuality("Bad");
		}
		else
		{
			param.setScoreTotalQuality("Not Bad");
		}
		scoring.score = scr;
		scoring.flag = flg;
		return scoring;
	}
	public Scoring scoringBySubjectAverage(AverageEngagementAnalysis param, String Subject)
	{
		//String score;

		double[] val = new double[24]; 
		if(Subject.equalsIgnoreCase("Physics"))
		{
			val[0]=4.0;
			val[1]=12.0;
			val[2]=6;
			val[3]=30.0;
			val[4]=15000;
			val[5]=150000;
			val[6]=45;
			val[7]=68;
			val[8]=180;
			val[9]=940;
			val[10]=89;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=85;
			val[15]=270;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=60;
			val[19]=420;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;

		}
		else if(Subject.equalsIgnoreCase("Chemistry"))
		{
			val[0]=2.5;
			val[1]=10.0;
			val[2]=6;
			val[3]=20.0;
			val[4]=15000;
			val[5]=150000;
			val[6]=45;
			val[7]=68;
			val[8]=250;
			val[9]=1250;
			val[10]=85;
			val[11]=72;
			val[12]=7.0;
			val[13]=16.0;
			val[14]=68;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=58;
			val[19]=550;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else if(Subject.equalsIgnoreCase("Mathematics"))
		{
			val[0]=4.0;
			val[1]=13.0;
			val[2]=6;
			val[3]=30.0;
			val[4]=12000;
			val[5]=115000;
			val[6]=45;
			val[7]=68;
			val[8]=180;
			val[9]=940;
			val[10]=85;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=68;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.30;
			val[18]=26;
			val[19]=350;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else
		{
			val[0]=2.0;
			val[1]=9.0;
			val[2]=6;
			val[3]=20.0;
			val[4]=12000;
			val[5]=140000;
			val[6]=45;
			val[7]=68;
			val[8]=150;
			val[9]=890;
			val[10]=88;
			val[11]=72;
			val[12]=5.0;
			val[13]=11.0;
			val[14]=70;
			val[15]=200;
			val[16]=0.04;
			val[17]=0.15;
			val[18]=61;
			val[19]=515;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		Scoring scoring = null; 
		scoring = Score(param,val, new Scoring());
		return scoring;
	}
}
