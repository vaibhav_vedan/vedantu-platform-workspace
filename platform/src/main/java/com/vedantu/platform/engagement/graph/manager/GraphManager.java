package com.vedantu.platform.engagement.graph.manager;

import java.util.Calendar;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.platform.utils.TimeDateMonth;
import com.vedantu.platform.engagement.graph.response.GraphResponse;
import com.vedantu.platform.engagement.migration.TeacherPercentileCron;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class GraphManager {

	public static int minimumSessionThreshold = 5;

	private int dayFirst = 1;

	private int daySecond = 15;

	@Autowired
	private TrendSpiderManager trendSpiderManager;

	@Autowired
	private LogFactory logFactory;
	
	@Autowired
	private TimeDateMonth timeDateMonth;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(GraphManager.class);

	public GraphResponse computeTeacherVsVedantuGraph(Long teacherId, String subject, Long currentTime)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		return computeTrendSpider(teacherId, subject, currentTime);
	}

	public GraphResponse computeTrendSpider(Long teacherId, String subject, Long currentTime)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		int day = timeDateMonth.getDayOfTheMonth(currentTime);
		Long startTimeQuery = null, endTimeQuery = null, startTimeLastMonth = null, startTimeCurrentMonth = null;

		startTimeLastMonth = timeDateMonth.getLastMonthStartTime(currentTime);
		startTimeCurrentMonth = timeDateMonth.getCurrentMonthStartTime(currentTime);

		if (day >= dayFirst && day <= daySecond) {
			endTimeQuery = startTimeCurrentMonth;
			startTimeQuery = startTimeLastMonth + daySecond * DateTimeUtils.MILLIS_PER_DAY;
		} else {
			startTimeQuery = startTimeCurrentMonth;
			endTimeQuery = startTimeCurrentMonth + daySecond * DateTimeUtils.MILLIS_PER_DAY;
		}
		// write query to fetch from db
		logger.info("startTimeQuery = "+startTimeQuery+" endTimeQuery = "+endTimeQuery);
		GraphResponse jsonResp = trendSpiderManager.computeTrendSpider(startTimeQuery, endTimeQuery, teacherId,
				subject);

		return jsonResp;
	}

}
