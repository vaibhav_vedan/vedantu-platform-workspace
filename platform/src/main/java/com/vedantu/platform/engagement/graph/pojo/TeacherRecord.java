package com.vedantu.platform.engagement.graph.pojo;

public class TeacherRecord {
	String date;
	Long sessionId;
	Long studentId;
	String studentName;
	Double scoreTotal;
	Double scoreContentUsage;
	Double scoreInteraction;
	Double scoreWhiteBoardUsage;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Double getScoreTotal() {
		return scoreTotal;
	}
	public void setScoreTotal(Double scoreTotal) {
		this.scoreTotal = scoreTotal;
	}
	public Double getScoreContentUsage() {
		return scoreContentUsage;
	}
	public void setScoreContentUsage(Double scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}
	public Double getScoreInteraction() {
		return scoreInteraction;
	}
	public void setScoreInteraction(Double scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}
	public Double getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}
	public void setScoreWhiteBoardUsage(Double scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}
}
