package com.vedantu.platform.engagement.graph.pojo;

public class SpiderGraph {
	
	Long teacherId;
	String subject;
	String type;
	Double scoreContentUsage;
	Double scoreInteraction;
	Double scoreWhiteBoardUsage;
	
	public SpiderGraph() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getScoreContentUsage() {
		return scoreContentUsage;
	}
	public void setScoreContentUsage(Double scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}
	public Double getScoreInteraction() {
		return scoreInteraction;
	}
	public void setScoreInteraction(Double scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}
	public Double getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}
	public void setScoreWhiteBoardUsage(Double scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}
	
}
