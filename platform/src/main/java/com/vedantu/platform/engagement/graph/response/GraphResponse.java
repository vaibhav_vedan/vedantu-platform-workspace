package com.vedantu.platform.engagement.graph.response;

import java.util.List;

import com.vedantu.platform.engagement.graph.pojo.AreasToImprove;
import com.vedantu.platform.engagement.graph.pojo.LearnMore;
import com.vedantu.platform.engagement.graph.pojo.SpiderGraph;
import com.vedantu.platform.engagement.graph.pojo.TeacherRecord;
import com.vedantu.platform.engagement.graph.pojo.TrendGraph;

public class GraphResponse {

	private List<SpiderGraph> spiderGraph;
	private List<TrendGraph> trendGraph;
	private List<AreasToImprove> areasToImprove;
	private Boolean valid;
	private Double percentile;
	private List<String> labels;
	private List<String> period;
	private String month;
	private List<TeacherRecord> teacherRecord;
	
	public GraphResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public List<SpiderGraph> getSpiderGraph() {
		return spiderGraph;
	}
	public void setSpiderGraph(List<SpiderGraph> spiderGraph) {
		this.spiderGraph = spiderGraph;
	}
	public List<TrendGraph> getTrendGraph() {
		return trendGraph;
	}
	public void setTrendGraph(List<TrendGraph> trendGraph) {
		this.trendGraph = trendGraph;
	}
	public List<AreasToImprove> getAreasToImprove() {
		return areasToImprove;
	}
	public void setAreasToImprove(List<AreasToImprove> areasToImprove) {
		this.areasToImprove = areasToImprove;
	}
	public Boolean getValid() {
		return valid;
	}
	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	public Double getPercentile() {
		return percentile;
	}
	public void setPercentile(Double percentile) {
		this.percentile = percentile;
	}
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<String> getPeriod() {
		return period;
	}
	public void setPeriod(List<String> period) {
		this.period = period;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public List<TeacherRecord> getTeacherRecord() {
		return teacherRecord;
	}
	public void setTeacherRecord(List<TeacherRecord> teacherRecord) {
		this.teacherRecord = teacherRecord;
	}

}