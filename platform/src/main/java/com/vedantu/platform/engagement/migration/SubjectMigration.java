package com.vedantu.platform.engagement.migration;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.platform.controllers.engagement.EngagementController;
import com.vedantu.platform.dao.engagement.serializers.EngagementDAO;
import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class SubjectMigration {

	@Autowired
	private EngagementDAO engagementDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EngagementController.class);

	@Async
	public void convertSubjectToLowerCase(Long startTime, Long endTime)
	{
		Long time = (long)DateTimeUtils.MILLIS_PER_DAY;
		int k = 1;
		while(startTime<=endTime-time)
		{
			logger.info("startTime = " +startTime);
			List<EngagementAnalysis> list =
					engagementDAO.getByTime(startTime, startTime+time);
			for(int i=0;i<list.size(); i++)
			{
				list.get(i).setSubject(list.get(i).getSubject().toLowerCase());
				engagementDAO.create(list.get(i));
				logger.info(list.get(i).get_id() + " " +k++ );

			}
			startTime += time;
		}
		logger.info("Migration Done");
	}
}
