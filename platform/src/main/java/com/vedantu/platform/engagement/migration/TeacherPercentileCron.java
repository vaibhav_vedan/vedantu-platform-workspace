package com.vedantu.platform.engagement.migration;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.mongodb.DBObject;
import com.vedantu.platform.utils.TimeDateMonth;
import com.vedantu.platform.controllers.engagement.EngagementController;
import com.vedantu.platform.dao.engagement.serializers.EngagementDAO;
import com.vedantu.platform.dao.engagement.serializers.TeacherPercentileDAO;
import com.vedantu.platform.mongodbentities.engagement.TeacherPercentile;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class TeacherPercentileCron {

	@Autowired
	private EngagementDAO engagementDAO;
	
	@Autowired
	private TeacherPercentileDAO teacherPercentileDAO;
	
	@Autowired
	private TimeDateMonth timeDateMonth;

	@Autowired
	private LogFactory logFactory;

	public static int secondFortnight = 15;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EngagementController.class);

	@Async
	public void callTeacherPercentile() {
		logger.info("Call received for getting teacher percentile");
		Long currentTime = System.currentTimeMillis();
		currentTime = timeDateMonth.getStartTimeOfDay(currentTime);
		int day = timeDateMonth.getDayOfTheMonth(currentTime);
		Long startTimeQuery = null, endTimeQuery = null, startTimeLastMonth = null, startTimeCurrentMonth = null, startTimeoFMonth = null;
		
		startTimeLastMonth = timeDateMonth.getLastMonthStartTime(currentTime);
		startTimeCurrentMonth = timeDateMonth.getCurrentMonthStartTime(currentTime);
		int fortnight = 0;
		
		if (day == 1) {
			endTimeQuery = currentTime;
			startTimeQuery = startTimeLastMonth + secondFortnight * DateTimeUtils.MILLIS_PER_DAY;
			fortnight = 2;
			startTimeoFMonth = startTimeLastMonth;
			
		} else {
			endTimeQuery = startTimeCurrentMonth + secondFortnight * DateTimeUtils.MILLIS_PER_DAY;
			startTimeQuery = startTimeCurrentMonth;
			fortnight = 1;
			startTimeoFMonth = startTimeCurrentMonth;
		}

		List<DBObject> listPercentile = (List<DBObject>) engagementDAO.getTeacherWiseAverageScore(
				startTimeQuery, endTimeQuery);
		double[] percentileSource = new double[listPercentile.size()];
		double[] percentileDestination = new double[listPercentile.size()];
		double percentileValue;
		if (listPercentile.isEmpty()) {
			logger.info("List is empty");
		} else {
			for (int i = 0; i < listPercentile.size(); i++) {
				logger.info(listPercentile.get(i).get("_id") + " " + listPercentile.get(i).get("scoreTotal"));
				percentileSource[i] = (Double) listPercentile.get(i).get("scoreTotal");
			}
		}
		for (int i = 0; i < percentileSource.length; i++) {
			if (i == 0) {
				percentileDestination[i] = (percentileSource.length - 1) * 100.00 / (percentileSource.length);
			} else if (percentileSource[i] != percentileSource[i - 1]) {
				percentileValue = (percentileSource.length - (i + 1)) * 100.00 / percentileSource.length;
				percentileDestination[i] = percentileValue;
			} else {
				percentileDestination[i] = percentileDestination[i - 1];
			}
			logger.info(listPercentile.get(i).get("_id") + " " + percentileDestination[i]);
			// populateTeacherPercentile();
			TeacherPercentile teacherPercentile = new TeacherPercentile();
			teacherPercentile.setTeacherId(Long.parseLong(listPercentile.get(i).get("_id").toString()));
			teacherPercentile.setPercentile(percentileDestination[i]);
			teacherPercentile.setFortnight(fortnight);
			teacherPercentile.setStartTimeoFMonth(startTimeoFMonth);
			teacherPercentileDAO.create(teacherPercentile);
			
			logger.info("teacherPercentile obj created");

		}
	}	
}
