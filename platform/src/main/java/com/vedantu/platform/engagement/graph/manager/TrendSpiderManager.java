package com.vedantu.platform.engagement.graph.manager;

import java.lang.reflect.Field;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.platform.utils.TimeDateMonth;
import com.vedantu.platform.engagement.cron.AverageEngagementAnalysis;
import com.vedantu.platform.engagement.cron.AverageReport;
import com.vedantu.platform.engagement.cron.ScoreBySubjectAverage;
import com.vedantu.platform.dao.engagement.serializers.EngagementDAO;
import com.vedantu.platform.dao.engagement.serializers.TeacherPercentileDAO;
import com.vedantu.platform.dao.engagement.serializers.VedantuAverageDAO;
import com.vedantu.platform.mongodbentities.engagement.TeacherPercentile;
import com.vedantu.platform.mongodbentities.engagement.VedantuAverage;
import com.vedantu.platform.engagement.graph.pojo.AreasToImprove;
import com.vedantu.platform.engagement.graph.pojo.FeedbackMessage;
import com.vedantu.platform.engagement.graph.pojo.FeedbackMessageImage;
import com.vedantu.platform.engagement.graph.pojo.FeedbackMessageVideo;
import com.vedantu.platform.engagement.graph.pojo.LearnMore;
import com.vedantu.platform.engagement.graph.pojo.SpiderGraph;
import com.vedantu.platform.engagement.graph.pojo.TeacherRecord;
import com.vedantu.platform.engagement.graph.pojo.TrendGraph;
import com.vedantu.platform.engagement.graph.response.GraphResponse;
import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;

@Service
public class TrendSpiderManager {

    @Autowired
    private EngagementDAO engagementDAO;

    @Autowired
    private AverageReport averageReport;

    @Autowired
    private VedantuAverageDAO vedantuAverageDAO;

    @Autowired
    private TeacherPercentileDAO teacherPercentileDAO;

    @Autowired
    private FeedbackMessage message;

    @Autowired
    private FeedbackMessageImage messageImage;

    @Autowired
    private FeedbackMessageVideo messageVideo;

    @Autowired
    private TimeDateMonth timeDateMonth;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GraphManager.class);

    public GraphResponse computeTrendSpider(Long startTime, Long endTime, Long teacherId, String subject)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        // write a query to retrieve by subject and only 5 rated vedantu
        // sessions .. but after cron this is straight forward
        // for teacher make sure the query is day wise
        // make sure score interaction is only for audio present sessions
        List<EngagementAnalysis> listTeacher = engagementDAO.getExportByUserIdSubject(startTime,
                endTime, teacherId.toString(), subject);
        if(StringUtils.isNotEmpty(subject)){
	        String subject_2 = subject.substring(0, 1).toUpperCase() + subject.substring(1);
	        listTeacher.addAll(engagementDAO.getExportByUserIdSubject(startTime,endTime, teacherId.toString(), subject_2));
        }
        List<VedantuAverage> listVedantu = vedantuAverageDAO.getByStartTimeAndSubject(startTime, endTime, subject);
        logger.info("Size of the Teacher List = " + listTeacher.size());
        logger.info("Size of the Vedantu List = " + listVedantu.size());
        GraphResponse graphResponse = new GraphResponse();
        List<String> labels = computeLabels(startTime, endTime);
        List<String> period = computePeriod(labels);
        graphResponse.setPeriod(period);
        if (listTeacher.size() >= GraphManager.minimumSessionThreshold) {

            List<TrendGraph> trendGraphs = new ArrayList<>();
            computeTrendTeacher(listTeacher, teacherId, subject, trendGraphs, startTime, endTime);
            computeTrendVedantu(listVedantu, subject, trendGraphs, startTime, endTime);

            List<SpiderGraph> spiderGraphs = new ArrayList<>();
            computeSpiderTeacher(listTeacher, teacherId, subject, spiderGraphs);
            computeSpiderVedantu(listVedantu, subject, spiderGraphs);

            Double percentile = roundOff(computePercentile(teacherId, startTime, endTime));

            List<AreasToImprove> areasToImprove = new ArrayList<>();
            computeAreasToImproveParameter(listTeacher, teacherId, subject, areasToImprove, startTime, endTime);
            computeAreasToImprovePercentile(listTeacher, spiderGraphs, subject, percentile, areasToImprove);

            replicatePreviousDayValueVedantu(trendGraphs);
            try {
                mapTeacherVedantuLabel(trendGraphs, labels);
            } catch (Exception e) {
                logger.error("Error in mapping labels");
            }
            String month = computeMonth(startTime);

            List<TeacherRecord> teacherRecords = new ArrayList<>();
            computeTeacherRecord(listTeacher, teacherId, subject, startTime, endTime, teacherRecords, labels);

            graphResponse.setSpiderGraph(spiderGraphs);
            graphResponse.setTrendGraph(trendGraphs);
            graphResponse.setValid(true);
            graphResponse.setAreasToImprove(areasToImprove);
            graphResponse.setPercentile(percentile);
            graphResponse.setLabels(labels);
            graphResponse.setMonth(month);
            graphResponse.setTeacherRecord(teacherRecords);
            checkZeroScoreUsages(graphResponse);

        } else {
            logger.info("Number of sessions taken less than " + GraphManager.minimumSessionThreshold);
            graphResponse.setValid(false);
        }
        return graphResponse;
    }

    public void checkZeroScoreUsages(GraphResponse graphResponse) {
        if (graphResponse.getSpiderGraph().get(0).getScoreInteraction() == 0
                || graphResponse.getSpiderGraph().get(0).getScoreContentUsage() == 0
                || graphResponse.getSpiderGraph().get(0).getScoreWhiteBoardUsage() == 0) {
            graphResponse.setValid(false);
        }
    }

    public String computeMonth(Long time) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        // Calendar calendar =
        // Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        calendar.setTimeInMillis(time);
        int num = calendar.get(Calendar.MONTH);
        String month = null;
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    public List<String> computeLabels(Long startTime, Long endTime) {
        List<String> labels = new ArrayList<String>();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        // Calendar calendar =
        // Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));

        calendar.setTimeInMillis(startTime);
        while (calendar.getTimeInMillis() < endTime) {
            labels.add(sdf.format(calendar.getTime()));
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return labels;
    }

    public List<String> computePeriod(List<String> labels) {
        List<String> period = new ArrayList<>();
        if (ArrayUtils.isEmpty(labels)) {
            return period;
        }
        period.add(labels.get(0));
        period.add(labels.get(labels.size() - 1));
        return period;
    }

    public void computeImageMessageParameter(AverageEngagementAnalysis averageEngagementAnalysis,
            AreasToImprove areaToImprove, LearnMore learnMore, List<String> Fields)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Map<String, Boolean> image = new HashMap<String, Boolean>();
        for (String Field : Fields) {
            Field field = averageEngagementAnalysis.getClass().getField(Field);
            String value = field.get(averageEngagementAnalysis).toString();
            Field messageField = messageImage.getClass().getField(Field);
            Object object = (messageField.get(messageImage));
            String key = Field;
            Boolean valueKey = (Boolean) object;
            // put the actual condition here after testing for the pojo
            if (value.equals("Bad")) {
                image.put(key, valueKey);
            }
        }
        learnMore.setImage(image);

        areaToImprove.setLearnMore(learnMore);
    }

    public void computeVideoMessageParameter(AverageEngagementAnalysis averageEngagementAnalysis,
            AreasToImprove areaToImprove, LearnMore learnMore, List<String> Fields)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Map<String, List<String>> video = new HashMap<String, List<String>>();
        for (String Field : Fields) {
            Field field = averageEngagementAnalysis.getClass().getField(Field);
            String value = field.get(averageEngagementAnalysis).toString();
            Field messageField = messageVideo.getClass().getField(Field);
            Object object = (messageField.get(messageVideo));
            String stringArray[] = (String[]) object;
            List<String> messageValue = Arrays.asList(stringArray);
            String key = Field;
            // put the actual condition here after testing for the pojo
            if (value.equals("Bad")) {
                video.put(key, messageValue);
            }
        }
        learnMore.setVideo(video);

        areaToImprove.setLearnMore(learnMore);
    }

    public void computeImageMessagePercentile(String fieldName, Map<String, Boolean> image)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        // Map<String,Boolean> image = new HashMap<String,Boolean>();
        Field messageImageField = messageImage.getClass().getField(fieldName);
        Object object = (messageImageField.get(messageImage));
        String key = fieldName;
        Boolean value = (Boolean) object;
        image.put(key, value);
        // learnMore.setImage(image);
        //
        // areaToImprove.setLearnMore(learnMore);
    }

    public void computeVideoMessagePercentile(String fieldName, Map<String, List<String>> video)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        // Map<String,List<String>> video = new HashMap<String,List<String>>();
        Field messageImageField = messageVideo.getClass().getField(fieldName);
        Object object = (messageImageField.get(messageVideo));
        String stringArray[] = (String[]) object;
        List<String> messageValue = Arrays.asList(stringArray);
        video.put(fieldName, messageValue);
        // learnMore.setVideo(video);
        //
        // areaToImprove.setLearnMore(learnMore);
    }

    public Double computePercentile(Long teacherId, Long startTime, Long endTime) {
        int day = timeDateMonth.getDayOfTheMonth(startTime);
        Long startTimeoFMonth = timeDateMonth.getCurrentMonthStartTime(startTime);
        int fortnight = 0;
        if (day == 1) {
            fortnight = 1;
        } else {
            fortnight = 2;
        }
        TeacherPercentile teacherPercentile = teacherPercentileDAO.getTeacherPercentile(startTimeoFMonth, teacherId,
                fortnight);
        if (teacherPercentile == null
                && ((System.currentTimeMillis() - startTime) < DateTimeUtils.MILLIS_PER_DAY / 4)) {
            return computePercentilePreviousMonth(fortnight, teacherId, startTimeoFMonth);
        } else {
            return (teacherPercentile != null) ? teacherPercentile.getPercentile() : 0.0;
        }
    }

    public Double computePercentilePreviousMonth(int fortnight, Long teacherId, Long startTimeoFMonth) {
        if (2 == fortnight) {
            fortnight = 1;
        } else {
            fortnight = 2;
            startTimeoFMonth = timeDateMonth.getLastMonthStartTime(startTimeoFMonth);
        }
        TeacherPercentile teacherPercentile = teacherPercentileDAO.getTeacherPercentile(startTimeoFMonth, teacherId,
                fortnight);
        return (teacherPercentile != null) ? teacherPercentile.getPercentile() : 0.0;

    }

    public void computeAreasToImproveParameter(List<EngagementAnalysis> list, Long teacherId, String subject,
            List<AreasToImprove> areasToImprove, Long startTime, Long endTime)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        String[] FieldsArray = {"shapesandImagesQuality", "colourChangesQuality", "teacherWriteLengthQuality",
            "whiteboardSpaceUsageQuality", "teacherWriteTimeQuality", "whiteboardInactiveTimeQuality",
            "whiteBoardsQuality", "interactionQuality", "sTRatioQuality", "sameTimeQuality"};

        List<String> Fields = Arrays.asList(FieldsArray);

        AverageEngagementAnalysis averageEngagementAnalysis = new AverageEngagementAnalysis();
        averageReport.calculateAverageTeacherReport(list, averageEngagementAnalysis);

        ScoreBySubjectAverage scoreBySubject = new ScoreBySubjectAverage();
        scoreBySubject.scoringBySubjectAverage(averageEngagementAnalysis, subject);

        // List<String> feedback = new ArrayList<String>();
        Map<String, String> messageHowToImprove = new HashMap<String, String>();
        for (String Field : Fields) {
            Field field = averageEngagementAnalysis.getClass().getField(Field);
            String value = field.get(averageEngagementAnalysis).toString();
            Field messageField = message.getClass().getField(Field);
            String messageValue = messageField.get(message).toString();
            if (value.equals("Bad")) {
                // feedback.add(messageValue);
                messageHowToImprove.put(Field, messageValue);
            }
        }
        AreasToImprove areaToImprove = new AreasToImprove();
        LearnMore learnMore = new LearnMore();
        // areaToImprove.setMessageHowToImprove(feedback);
        learnMore.setMessageHowToImprove(messageHowToImprove);
        areaToImprove.setType("parameter");
        computeImageMessageParameter(averageEngagementAnalysis, areaToImprove, learnMore, Fields);
        computeVideoMessageParameter(averageEngagementAnalysis, areaToImprove, learnMore, Fields);
        areasToImprove.add(areaToImprove);

    }

    public void computeAreasToImprovePercentile(List<EngagementAnalysis> list, List<SpiderGraph> spiderGraphs,
            String subject, Double percentile, List<AreasToImprove> areasToImprove)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Field field = null;
        String value = null;
        // List<String> feedback = new ArrayList<String>();
        Map<String, String> messageHowToImprove = new HashMap<String, String>();
        AreasToImprove areaToImprove = new AreasToImprove();
        LearnMore learnMore = new LearnMore();
        Map<String, List<String>> video = new HashMap<String, List<String>>();
        Map<String, Boolean> image = new HashMap<String, Boolean>();
        // if (areasToImprove.get(0).getMessageHowToImprove().size() == 0 &&
        // percentile < 50) {
        if (areasToImprove.get(0).getLearnMore().getMessageHowToImprove().isEmpty() && percentile < 50) {
            if (spiderGraphs.get(0).getScoreContentUsage() < spiderGraphs.get(1).getScoreContentUsage()) {
                field = message.getClass().getField("scoreContentUsage");
                value = field.get(message).toString();
                // feedback.add(value);
                messageHowToImprove.put("scoreContentUsage", value);
                computeImageMessagePercentile("scoreContentUsage", image);
                computeVideoMessagePercentile("scoreContentUsage", video);
            }
            if (spiderGraphs.get(0).getScoreInteraction() < spiderGraphs.get(1).getScoreInteraction()) {
                field = message.getClass().getField("scoreInteraction");
                value = field.get(message).toString();
                // feedback.add(value);
                messageHowToImprove.put("scoreInteraction", value);
                computeImageMessagePercentile("scoreInteraction", image);
                computeVideoMessagePercentile("scoreInteraction", video);
            }
            if (spiderGraphs.get(0).getScoreWhiteBoardUsage() < spiderGraphs.get(1).getScoreWhiteBoardUsage()) {
                field = message.getClass().getField("scoreWhiteBoardUsage");
                value = field.get(message).toString();
                // feedback.add(value);
                messageHowToImprove.put("scoreWhiteBoardUsage", value);
                computeImageMessagePercentile("scoreWhiteBoardUsage", image);
                computeVideoMessagePercentile("scoreWhiteBoardUsage", video);
            }
        }

        // areaToImprove.setMessageHowToImprove(feedback);
        learnMore.setMessageHowToImprove(messageHowToImprove);
        learnMore.setImage(image);
        learnMore.setVideo(video);
        areaToImprove.setLearnMore(learnMore);
        areaToImprove.setType("percentile");
        areasToImprove.add(areaToImprove);
    }

    public void computeTeacherRecord(List<EngagementAnalysis> list, Long teacherId, String subject, Long startTime,
            Long endTime, List<TeacherRecord> teacherRecords, List<String> labels) {
        for (int i = 0; i < list.size(); i++) {
            TeacherRecord teacherRecord = new TeacherRecord();
            teacherRecord.setDate(timeDateMonth.getDateString(list.get(i).getServerTime()));
            teacherRecord.setSessionId(Long.parseLong(list.get(i).get_id()));
            teacherRecord.setStudentId(Long.parseLong(list.get(i).getStudentId()));
            teacherRecord.setStudentName(list.get(i).getStudentName());
            teacherRecord.setScoreTotal(roundOff(list.get(i).getScoreTotal()));
            teacherRecord.setScoreContentUsage(roundOff(list.get(i).getScoreContentUsage()));
            teacherRecord.setScoreInteraction(roundOff(list.get(i).getScoreInteraction()));
            teacherRecord.setScoreWhiteBoardUsage(roundOff(list.get(i).getScoreWhiteBoardUsage()));
            teacherRecords.add(teacherRecord);
        }
    }

    public void computeTrendVedantu(List<VedantuAverage> list, String subject, List<TrendGraph> trendGraphs,
            Long startTime, Long endTime) {
        ArrayList<Double> trendValues = new ArrayList<Double>();
        int days = (int) ((endTime - startTime) / (DateTimeUtils.MILLIS_PER_DAY));

        for (int i = 0; (i < list.size() && (startTime <= endTime));) {
            logger.info(list.get(i).getStartTime() + " " + startTime);
            if (0 == list.get(i).getStartTime().compareTo(startTime)) {
                // if(Math.abs(list.get(i).getStartTime()-startTime)<=1000){
                trendValues.add(roundOff(list.get(i).getScoreTotal()));
                i++;
            } else {
                trendValues.add(0.0);
            }
            startTime += (long) DateTimeUtils.MILLIS_PER_DAY;
        }
        for (int var = trendValues.size(); var < days; var++) {
            trendValues.add(0.0);
        }

        logger.info("size of the vedantu trend list = " + trendValues.size());
        TrendGraph trendGraph = new TrendGraph();
        trendGraph.setTeacherId((long) 0);
        trendGraph.setSubject(subject);
        trendGraph.setType("vedantu");
        trendGraph.setTrendValues(trendValues);

        trendGraphs.add(trendGraph);
        for (int i = 0; i < trendGraphs.get(1).getTrendValues().size(); i++) {
            logger.info(trendGraphs.get(1).getTrendValues().get(i) + " ");
        }
    }

    public void computeTrendTeacher(List<EngagementAnalysis> list, Long teacherId, String subject,
            List<TrendGraph> trendGraphs, Long startTime, Long endTime)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        ArrayList<Double> trendValues = new ArrayList<Double>();
        Double scoreTotal = 0.0;
        int count = 0;
        int days = (int) ((endTime - startTime) / (DateTimeUtils.MILLIS_PER_DAY));

        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).getServerTime() <= startTime + DateTimeUtils.MILLIS_PER_DAY) {
                scoreTotal += list.get(i).getScoreTotal();
                count++;
            } else if (count > 0) {
                trendValues.add(roundOff(scoreTotal * 1.0 / count));
                if (startTime + DateTimeUtils.MILLIS_PER_DAY <= endTime) {
                    startTime += DateTimeUtils.MILLIS_PER_DAY;
                }

                count = 0;
                scoreTotal = 0.0;
                i--;
            } else {
                trendValues.add(0.0);
                if (startTime + DateTimeUtils.MILLIS_PER_DAY <= endTime) {
                    startTime += DateTimeUtils.MILLIS_PER_DAY;
                }

                count = 0;
                scoreTotal = 0.0;
                i--;
            }
        }
        trendValues.add(roundOff(scoreTotal * 1.0 / count));
        for (int var = trendValues.size(); var < days; var++) {
            trendValues.add(0.0);
        }

        logger.info("size of the teacher trend list = " + trendValues.size());

        TrendGraph trendGraph = new TrendGraph();
        trendGraph.setTeacherId(teacherId);
        trendGraph.setSubject(subject);
        trendGraph.setType("teacher");
        trendGraph.setTrendValues(trendValues);

        trendGraphs.add(trendGraph);
        for (int i = 0; i < trendGraphs.get(0).getTrendValues().size(); i++) {
            logger.info(trendGraphs.get(0).getTrendValues().get(i) + " ");
        }

    }

    public void computeSpiderVedantu(List<VedantuAverage> list, String subject, List<SpiderGraph> spiderGraphs) {
        Double scoreContentUsage = 0.0;
        Double scoreInteraction = 0.0;
        Double scoreWhiteBoardUsage = 0.0;
        for (int i = 0; i < list.size(); i++) {
            scoreContentUsage += list.get(i).getScoreContentUsage();
            scoreInteraction += list.get(i).getScoreInteraction();
            scoreWhiteBoardUsage += list.get(i).getScoreWhiteBoardUsage();
        }
        if (list.size() > 0) {
            scoreContentUsage /= list.size();
            scoreInteraction /= list.size();
            scoreWhiteBoardUsage /= list.size();
        }

        SpiderGraph spiderGraph = new SpiderGraph();
        spiderGraph.setScoreInteraction(roundOff(scoreInteraction));
        spiderGraph.setScoreContentUsage(roundOff(scoreContentUsage));
        spiderGraph.setScoreWhiteBoardUsage(roundOff(scoreWhiteBoardUsage));
        spiderGraph.setType("vedantu");
        spiderGraph.setSubject(subject);
        spiderGraph.setTeacherId((long) 0);

        spiderGraphs.add(spiderGraph);
    }

    public void computeSpiderTeacher(List<EngagementAnalysis> list, Long teacherId, String subject,
            List<SpiderGraph> spiderGraphs) {
        Double scoreContentUsage = 0.0;
        Double scoreInteraction = 0.0;
        Double scoreWhiteBoardUsage = 0.0;
        int audioCount = 0;
        for (int i = 0; i < list.size(); i++) {
            scoreContentUsage += list.get(i).getScoreContentUsage();
            if (list.get(i).getAudioPresent().equals("Audio Present")) {
                scoreInteraction += list.get(i).getScoreInteraction();
                audioCount++;
            }
            scoreWhiteBoardUsage += list.get(i).getScoreWhiteBoardUsage();
        }
        scoreContentUsage /= list.size();
        scoreInteraction /= audioCount;
        scoreWhiteBoardUsage /= list.size();

        SpiderGraph spiderGraph = new SpiderGraph();
        spiderGraph.setScoreInteraction(roundOff(scoreInteraction));
        spiderGraph.setScoreContentUsage(roundOff(scoreContentUsage));
        spiderGraph.setScoreWhiteBoardUsage(roundOff(scoreWhiteBoardUsage));
        spiderGraph.setType("teacher");
        spiderGraph.setSubject(subject);
        spiderGraph.setTeacherId(teacherId);

        spiderGraphs.add(spiderGraph);

    }

    public void mapTeacherVedantuLabel(List<TrendGraph> trendGraphs, List<String> labels) {
        boolean labelsExist = false;
        if (ArrayUtils.isNotEmpty(labels)) {
            labelsExist = true;
        }
        for (int i = 0; i < trendGraphs.get(0).getTrendValues().size(); i++) {
            if (0 == trendGraphs.get(0).getTrendValues().get(i)) {
                trendGraphs.get(0).getTrendValues().remove(i);
                trendGraphs.get(1).getTrendValues().remove(i);
                if (labelsExist) {
                    labels.remove(i);
                }
                i--;
            }
        }
    }

    public void replicatePreviousDayValueVedantu(List<TrendGraph> trendGraphs) {
        populateFirstDayValue(trendGraphs);
        for (int i = 1; i < trendGraphs.get(1).getTrendValues().size(); i++) {
            if (0 == trendGraphs.get(1).getTrendValues().get(i)) {
                Double element = trendGraphs.get(1).getTrendValues().get(i - 1);
                trendGraphs.get(1).getTrendValues().set(i, element);
            }
        }
    }

    public void populateFirstDayValue(List<TrendGraph> trendGraphs) {
        if (0 == trendGraphs.get(1).getTrendValues().get(0)) {
            for (int i = 1; i < trendGraphs.get(1).getTrendValues().size(); i++) {
                if (trendGraphs.get(1).getTrendValues().get(i) != 0) {
                    Double element = trendGraphs.get(1).getTrendValues().get(i);
                    trendGraphs.get(1).getTrendValues().set(0, element);
                    break;
                }
            }
        }
    }

    public Double roundOff(Double value) {
        return Math.round(value * 10) * 1.0 / 10;
        // return Math.round(value * 100) * 1.0 / 100;
        // return Math.round(value)*1.0;
    }
}
