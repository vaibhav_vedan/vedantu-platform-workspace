package com.vedantu.platform.engagement.migration;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.mongodb.DBObject;
import com.vedantu.platform.utils.TimeDateMonth;
import com.vedantu.platform.controllers.engagement.EngagementController;
import com.vedantu.platform.dao.engagement.serializers.EngagementDAO;
import com.vedantu.platform.dao.engagement.serializers.VedantuAverageDAO;
import com.vedantu.platform.mongodbentities.engagement.VedantuAverage;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class VedantuAverageManager {

	@Autowired
	private VedantuAverageDAO vedantuAverageDAO;

	@Autowired
	private EngagementDAO engagementDAO;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private TimeDateMonth timeDateMonth;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EngagementController.class);

	@Async
	public void callVedantuMigration(Long startTime, Long endTime) {
		Long time = (long) DateTimeUtils.MILLIS_PER_DAY;
		logger.info(startTime);
		while (startTime <= endTime - time) {
			logger.info(startTime + " " + (startTime + time));

			List<DBObject> listWhiteBoard = (List<DBObject>) engagementDAO
					.getAverageBySubjectWhiteBoard(startTime, startTime + time);
			List<DBObject> listAudio = (List<DBObject>) engagementDAO.getAverageBySubjectAudio(
					startTime, startTime + time);
			if (listWhiteBoard.size() == 0)
				logger.info("List is empty");
			else {
				logger.info(listWhiteBoard.size());
				for (int i = 0; i < listWhiteBoard.size(); i++) {
					VedantuAverage vedantuAverage = new VedantuAverage();
					Object key = listWhiteBoard.get(i).get("_id");
					logger.info("keywhiteboard = " + key);
					vedantuAverage.setEngagementEntityType("vedantu");
					vedantuAverage.setStartTime(startTime);
					vedantuAverage.setSubject(listWhiteBoard.get(i).get("_id").toString());

					logger.info(listWhiteBoard.get(i).get("_id").toString());

					vedantuAverage.setScoreContentUsage((Double) listWhiteBoard.get(i).get("scoreContentUsage"));
					vedantuAverage.setScoreWhiteBoardUsage((Double) listWhiteBoard.get(i).get("scoreWhiteBoardUsage"));
					vedantuAverage.setScoreTotal((Double) listWhiteBoard.get(i).get("scoreTotal"));
					for (int j = 0; j < listAudio.size(); j++) {
						if (listAudio.get(j).get("_id").toString().equals(key.toString())) {
							String keyaudio = listAudio.get(j).get("_id").toString();
							logger.info("keyaudio = " + keyaudio);
							vedantuAverage.setScoreInteraction((Double) listAudio.get(j).get("scoreInteraction"));
						}
					}
					vedantuAverageDAO.create(vedantuAverage);
					logger.info("VedantuAverage entry created with key = " + key);
				}
			}
			startTime += time;
			logger.info("One day Ended");
		}
	}

	@Async
	public void callVedantuDailyMigration() {
		Long currentTime = System.currentTimeMillis();
		Long endTime = timeDateMonth.getStartTimeOfDay(currentTime);
		Long startTime = endTime - DateTimeUtils.MILLIS_PER_DAY;
		List<DBObject> listWhiteBoard = (List<DBObject>) engagementDAO.getAverageBySubjectWhiteBoard(
				startTime, endTime);
		List<DBObject> listAudio = (List<DBObject>) engagementDAO.getAverageBySubjectAudio(
				startTime, endTime);
		try {
			if (listWhiteBoard.size() == 0)
				logger.info("List is empty");
			else {
				logger.info(listWhiteBoard.size());
				for (int i = 0; i < listWhiteBoard.size(); i++) {
					VedantuAverage vedantuAverage = new VedantuAverage();
					Object key = listWhiteBoard.get(i).get("_id");
					logger.info("keywhiteboard = " + key);
					vedantuAverage.setEngagementEntityType("vedantu");
					vedantuAverage.setStartTime(startTime);
					vedantuAverage.setSubject(listWhiteBoard.get(i).get("_id").toString());

					logger.info(listWhiteBoard.get(i).get("_id").toString());

					vedantuAverage.setScoreContentUsage((Double) listWhiteBoard.get(i).get("scoreContentUsage"));
					vedantuAverage.setScoreWhiteBoardUsage((Double) listWhiteBoard.get(i).get("scoreWhiteBoardUsage"));
					vedantuAverage.setScoreTotal((Double) listWhiteBoard.get(i).get("scoreTotal"));
					for (int j = 0; j < listAudio.size(); j++) {
						if (listAudio.get(j).get("_id").toString().equals(key.toString())) {
							String keyaudio = listAudio.get(j).get("_id").toString();
							logger.info("keyaudio = " + keyaudio);
							vedantuAverage.setScoreInteraction((Double) listAudio.get(j).get("scoreInteraction"));
						}
					}
					vedantuAverageDAO.create(vedantuAverage);
					logger.info("VedantuAverage entry created with key = " + key);
				}
				logger.info("Migration of vedantu values done for " + timeDateMonth.getDate(startTime));
			}
		} catch (Exception e) {
			logger.error("Migration of values failed on " + timeDateMonth.getDate(startTime));
		}
	}
}
