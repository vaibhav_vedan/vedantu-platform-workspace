package com.vedantu.platform.engagement.cron;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.LogFactory;


@Service
public class ScoreBySubject {

	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EngagementCron.class);
	
	public void Score(EngagementAnalysis param, double[] val)
	{
		int scr=0;
		int flagBad=0;
		double handWritingScore = 0.0;
		double sh = Double.parseDouble(param.getShapesandImages());
		double cc = Double.parseDouble(param.getColourChanges());
		double twl = Double.parseDouble(param.getTeacherWriteLength());
		double wbsu = Double.parseDouble(param.getWhiteboardSpaceUsage());
		double twt = Double.parseDouble(param.getTeacherWriteTime());
		double wit = Double.parseDouble(param.getWhiteboardInactiveTime());
		double nwb = Double.parseDouble(param.getWhiteBoards());
		double inter = Double.parseDouble(param.getInteraction());
		double str = Double.parseDouble(param.getSTRatio());
		double st = Double.parseDouble(param.getSameTime());
		double hwd = Double.parseDouble(param.getHWAveragedensity());
		double hwv = Double.parseDouble(param.getHWVariance());
		double score;
		double scoreInteraction = 0.0;
		double scoreWhiteBoardUsage = 0.0;
		double scoreContentUsage = 0.0;
		double scoreTotal = 0.0;
		

		if(sh<val[0]){
			scr +=0;flagBad+=1;
			param.setQualityShapesandImages("Bad");
			scoreContentUsage += 0;
		}
		else if(sh<val[1]){
			scr +=1;
			param.setQualityShapesandImages("Okay");
			scoreContentUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityShapesandImages("Good");
			scoreContentUsage += 2;
		}

		if(cc<val[2]){
			scr +=0;flagBad+=1;
			param.setQualityColorChanges("Bad");
			scoreContentUsage += 0;
		}
		else if(cc<val[3]){
			scr +=1;
			param.setQualityColorChanges("Okay");
			scoreContentUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityColorChanges("Good");
			scoreContentUsage += 2;
		}


		if(twl<val[4]){
			scr +=0;flagBad+=1;
			param.setQualityTeacherWriteLength("Bad");
			scoreWhiteBoardUsage += 0;
		}
		else if(twl<val[5]){
			scr +=1;
			param.setQualityTeacherWriteLength("Okay");
			scoreWhiteBoardUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityTeacherWriteLength("Good");
			scoreWhiteBoardUsage += 2;
		}

		if(wbsu<val[6]){
			scr +=0;flagBad+=1;
			param.setQualityWhiteBoardSpaceUsage("Bad");
			scoreWhiteBoardUsage += 0;
		}
		else if(wbsu<val[7]){
			scr +=1;
			param.setQualityWhiteBoardSpaceUsage("Okay");
			scoreWhiteBoardUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityWhiteBoardSpaceUsage("Good");
			scoreWhiteBoardUsage += 2;
		}

		if(twt<val[8]){
			scr +=0;flagBad+=1;
			param.setQualityTeacherWriteTime("Bad");
			scoreWhiteBoardUsage += 0;
		}
		else if(twt<val[9]){
			scr +=1;
			param.setQualityTeacherWriteTime("Okay");
			scoreWhiteBoardUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityTeacherWriteTime("Good");
			scoreWhiteBoardUsage += 2;
		}

		if(wit>val[10]){
			scr +=0;flagBad+=1;
			param.setQualityWhiteBoardInactiveTime("Bad");
			scoreWhiteBoardUsage += 0;
		}
		else if(wit>val[11]){
			scr +=1;
			param.setQualityWhiteBoardInactiveTime("Okay");
			scoreWhiteBoardUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityWhiteBoardInactiveTime("Good");
			scoreWhiteBoardUsage += 2;
		}

		if(nwb<val[12]){
			scr +=0;flagBad+=1;
			param.setQualityWhiteBoards("Bad");
			scoreWhiteBoardUsage += 0;
		}
		else if(nwb<val[13]){
			scr +=1;
			param.setQualityWhiteBoards("Okay");
			scoreWhiteBoardUsage += 1;
		}
		else {
			scr+=2;
			param.setQualityWhiteBoards("Good");
			scoreWhiteBoardUsage += 2;
		}



		if(inter<val[14]){
			scr +=0;flagBad+=1;
			param.setQualityInteraction("Bad");
			scoreInteraction += 0;
		}
		else if(inter<val[15]){
			scr +=1;
			param.setQualityInteraction("Okay");
			scoreInteraction += 1;
		}
		else {
			scr+=2;
			param.setQualityInteraction("Good");
			scoreInteraction += 2;
		}

		if(str<val[16]){
			scr +=0;flagBad+=1;
			param.setQualitySTRatio("Bad");
			scoreInteraction += 0;
		}
		else if(str<val[17]){
			scr +=1;
			param.setQualitySTRatio("Okay");
			scoreInteraction+= 1;
		}
		else {
			scr+=2;
			param.setQualitySTRatio("Good");
			scoreInteraction+= 2;
		}

		if(st<val[18]){
			scr +=0;flagBad+=1;
			param.setQualitySameTime("Bad");
			scoreInteraction += 0;
		}
		else if(st<val[19]){
			scr +=1;
			param.setQualitySameTime("Okay");
			scoreInteraction += 1;
		}
		else {
			scr+=2;
			param.setQualitySameTime("Good");
			scoreInteraction += 2;
		}
		
		if(hwd < val[20])
		{
			scr+=0;flagBad+=1;
			handWritingScore += 0;
			param.setQualityHwDensity("Bad");
		}
		else if(hwd < val[21])
		{
			scr+=1;
			handWritingScore += 1;
			param.setQualityHwDensity("Okay");
		}
		else
		{
			scr+=2;
			handWritingScore += 2;
			param.setQualityHwDensity("Good");
		}
		if(hwv > val[22])
		{
			scr+=0;
			handWritingScore += 0;
			param.setQualityHwVariance("Bad");
		}
		else if(hwv > val[23])
		{
			scr+=1;
			handWritingScore += 1;
			param.setQualityHwVariance("Okay");
		}
		else
		{
			scr+=2;
			handWritingScore += 2;
			param.setQualityHwVariance("Good");
		}
	    setScoreParameters(param, scoreContentUsage, scoreWhiteBoardUsage, scoreInteraction, flagBad, handWritingScore);
		
	}
	public void setScoreParameters(EngagementAnalysis param, Double scoreContentUsage, Double scoreWhiteBoardUsage, Double scoreInteraction, int flagBad, Double handWritingScore)
	{
		Double scoreTotal = scoreContentUsage + scoreWhiteBoardUsage + scoreInteraction;
		if(param.getAudioPresent().equals("Audio Present"))
		{
			if(flagBad >5 || scoreTotal < 7)
				param.setQuality("Bad");
			else
				param.setQuality("Not Bad");
			
			scoreTotal = Math.round(scoreTotal*10.0*100/20.0)/100.00;
		}
		else if(param.getAudioPresent().equals("Audio Absent"))
		{
			if(flagBad > 8 || scoreTotal <=4 )
				param.setQuality("Bad (WB only)");
			else
				param.setQuality("Not Bad (WB only)");
			
			scoreTotal = Math.round(scoreTotal*10.0*100/14.0)/100.00;
		}
		param.setScoreHandWriting(handWritingScore);
		param.setScoreContentUsage(scoreContentUsage*10.0/4.0);
		param.setScoreWhiteBoardUsage(scoreWhiteBoardUsage*10.0/10.0);
		param.setScoreInteraction((Math.round(scoreInteraction*10.0*100/6.0)/100.00));
		param.setScoreTotal(scoreTotal);
		param.setScore(Double.toString(scoreContentUsage + scoreWhiteBoardUsage + scoreInteraction)); //Do not use scr as it contains hwd and hwv
		
	}
	public void scoringBySubject(EngagementAnalysis param, String Subject)
	{
		//String score;
		logger.info(param.get_id());
		double[] val = new double[24]; 
		if(Subject.equalsIgnoreCase("Physics"))
		{
			val[0]=4.0;
			val[1]=12.0;
			val[2]=6;
			val[3]=30.0;
			val[4]=15000;
			val[5]=150000;
			val[6]=45;
			val[7]=68;
			val[8]=180;
			val[9]=940;
			val[10]=89;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=85;
			val[15]=270;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=60;
			val[19]=420;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;

		}
		else if(Subject.equalsIgnoreCase("Chemistry"))
		{
			val[0]=2.5;
			val[1]=10.0;
			val[2]=6;
			val[3]=20.0;
			val[4]=15000;
			val[5]=150000;
			val[6]=45;
			val[7]=68;
			val[8]=250;
			val[9]=1250;
			val[10]=85;
			val[11]=72;
			val[12]=7.0;
			val[13]=16.0;
			val[14]=68;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=58;
			val[19]=550;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else if(Subject.equalsIgnoreCase("Mathematics"))
		{
			val[0]=4.0;
			val[1]=13.0;
			val[2]=6;
			val[3]=30.0;
			val[4]=12000;
			val[5]=115000;
			val[6]=45;
			val[7]=68;
			val[8]=180;
			val[9]=940;
			val[10]=85;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=68;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.30;
			val[18]=26;
			val[19]=350;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else
		{
			val[0]=2.0;
			val[1]=9.0;
			val[2]=6;
			val[3]=20.0;
			val[4]=12000;
			val[5]=140000;
			val[6]=45;
			val[7]=68;
			val[8]=150;
			val[9]=890;
			val[10]=88;
			val[11]=72;
			val[12]=5.0;
			val[13]=11.0;
			val[14]=70;
			val[15]=200;
			val[16]=0.04;
			val[17]=0.15;
			val[18]=61;
			val[19]=515;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		Score(param,val);
	}
}
