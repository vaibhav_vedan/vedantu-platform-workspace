package com.vedantu.platform.dinero.pojo;

import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.SessionModel;

public class Session {

	private String sessionId;
	private Long sessionDuration;
	private Long billingDuration;
	private Long teacherId;
	private SessionModel model;
	private Long studentId;
	private Long subscriptionId;
        private Long sessionStartTime;
        private Long sessionEndTime;
        private LiveSessionPlatformType liveSessionPlatformType;

    

	public Session() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public Long getSessionDuration() {
		return sessionDuration;
	}

	public void setSessionDuration(Long sessionDuration) {
		this.sessionDuration = sessionDuration;
	}

	public Long getBillingDuration() {
		return billingDuration;
	}

	public void setBillingDuration(Long billingDuration) {
		this.billingDuration = billingDuration;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

        public Long getSessionStartTime() {
            return sessionStartTime;
        }

        public void setSessionStartTime(Long sessionStartTime) {
            this.sessionStartTime = sessionStartTime;
        }

        public Long getSessionEndTime() {
            return sessionEndTime;
        }

        public void setSessionEndTime(Long sessionEndTime) {
            this.sessionEndTime = sessionEndTime;
        }

    public LiveSessionPlatformType getLiveSessionPlatformType() {
        return liveSessionPlatformType;
    }

    public void setLiveSessionPlatformType(LiveSessionPlatformType liveSessionPlatformType) {
        this.liveSessionPlatformType = liveSessionPlatformType;
    }
        
        

    @Override
    public String toString() {
        return "Session{" + "sessionId=" + sessionId + ", sessionDuration=" + sessionDuration + ", billingDuration=" + billingDuration + ", teacherId=" + teacherId + ", model=" + model + ", studentId=" + studentId + ", subscriptionId=" + subscriptionId + ", sessionStartTime=" + sessionStartTime + ", sessionEndTime=" + sessionEndTime + ", liveSessionPlatformType=" + liveSessionPlatformType + '}';
    }
        
        
        

}
