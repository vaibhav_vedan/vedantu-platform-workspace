package com.vedantu.platform.managers.subscription;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.AddressException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.subscription.request.AddARMFormReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.response.ARMFormInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service
public class ARMFormManager {
	
	@Autowired
	CommunicationManager emailManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

	@Autowired
    private FosUtils fosUtils;

	@Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(ARMFormManager.class);
    
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    private final Gson gson = new Gson();
    
    public ARMFormInfo addARMForm(AddARMFormReq addARMFormReq, boolean exposeEmail) throws VException, AddressException, UnsupportedEncodingException{
    	addARMFormReq.verify();
    	logger.info("Adding ARMForm for studentId " + addARMFormReq.getStudentId());
    	ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/armform/addARMForm", HttpMethod.POST, gson.toJson(addARMFormReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        ARMFormInfo aRMFormInfo = gson.fromJson(json,ARMFormInfo.class);
        List<Long> userIds = new ArrayList<>();
        userIds.add(aRMFormInfo.getStudentId());
        Long createdById = Long.parseLong(aRMFormInfo.getCreatedBy());
        userIds.add(createdById);
        if(aRMFormInfo.getAdminId() != null){
        userIds.add(aRMFormInfo.getAdminId());
        }
        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, exposeEmail);
        if(aRMFormInfo.getAdminId() != null){
	        UserInfo adminUserInfo = new UserInfo(userInfos.get(aRMFormInfo.getAdminId()), null, exposeEmail);
	        aRMFormInfo.setAdmin(adminUserInfo);
        }
        UserInfo studentUserInfo = new UserInfo(userInfos.get(aRMFormInfo.getStudentId()), null, exposeEmail);
        aRMFormInfo.setStudent(studentUserInfo);
        UserInfo createdByUserInfo = new UserInfo(userInfos.get(createdById), null, exposeEmail);
        aRMFormInfo.setCreatedByBasicInfo(createdByUserInfo);
        
        if(aRMFormInfo.getAdded()!=null ){
        	if(aRMFormInfo.getAdded()){
        		addARMFormReq.setId(aRMFormInfo.getId());
        		emailManager.sendARMFormReqEmails(addARMFormReq);
        	}
        	
        }
        return aRMFormInfo;
    	
    }
    
    public ARMFormInfo getARMForm(String id) throws VException{
    	ARMFormInfo aRMFormInfo;
    	if(id != null){
    	ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/armform/" + id, HttpMethod.GET, null);
    	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    	String jsonString = resp.getEntity(String.class);
    	aRMFormInfo = gson.fromJson(jsonString, ARMFormInfo.class);
    	}
    	else {
    		throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Query id is null");
    	}
    	List<Long> userIds = new ArrayList<>();
        userIds.add(aRMFormInfo.getStudentId());
        Long createdById = Long.parseLong(aRMFormInfo.getCreatedBy());
        userIds.add(createdById);
        if(aRMFormInfo.getAdminId() != null){
        	userIds.add(aRMFormInfo.getAdminId());
        }
        
        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, true);
        if(aRMFormInfo.getAdminId() != null){
	        UserInfo adminUserInfo = new UserInfo(userInfos.get(aRMFormInfo.getAdminId()), null, true);
	        aRMFormInfo.setAdmin(adminUserInfo);
        }
        UserInfo studentUserInfo = new UserInfo(userInfos.get(aRMFormInfo.getStudentId()), null, true);
        aRMFormInfo.setStudent(studentUserInfo);
        UserInfo createdByUserInfo = new UserInfo(userInfos.get(createdById), null, true);
        aRMFormInfo.setCreatedByBasicInfo(createdByUserInfo);
        
		return aRMFormInfo;
    	
    }
    
    public List<ARMFormInfo> getARMForms(ExportCoursePlansReq req) throws VException{
    	req.verify();
        //TODO add access control
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/armform/getARMForms?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<ARMFormInfo>>() {
        }.getType();
        List<ARMFormInfo> aRMFormInfos = gson.fromJson(jsonString, listType1);
        
        if (ArrayUtils.isNotEmpty(aRMFormInfos)) {
            Set<Long> userIds = new HashSet<>();
            for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                userIds.add(aRMFormInfo.getStudentId());
                Long createdById = Long.parseLong(aRMFormInfo.getCreatedBy());
                userIds.add(createdById);
                if(aRMFormInfo.getAdminId()!=null){
                	userIds.add(aRMFormInfo.getAdminId());
                }
                
            }
            Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                if (usermap.containsKey(aRMFormInfo.getStudentId())) {
                	aRMFormInfo.setStudent(usermap.get(aRMFormInfo.getStudentId()));
                }
                if (usermap.containsKey(Long.parseLong(aRMFormInfo.getCreatedBy()))) {
                	aRMFormInfo.setCreatedByBasicInfo(usermap.get(Long.parseLong(aRMFormInfo.getCreatedBy())));
                }
                if(aRMFormInfo.getAdminId() != null){
	                if (usermap.containsKey(aRMFormInfo.getAdminId())) {
	                	aRMFormInfo.setAdmin(usermap.get(aRMFormInfo.getAdminId()));
	                }
                }
            }
        }
    	return aRMFormInfos;
    }
    
    public List<ARMFormInfo> getARMFormByAdmin(Long adminId) throws VException{
    	if(adminId != null){
    	ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/armform/getARMFormByAdmin/" + adminId, HttpMethod.GET, null);
    	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    	String jsonString = resp.getEntity(String.class);
    	Type listType1 = new TypeToken<ArrayList<ARMFormInfo>>() {
        }.getType();
        List<ARMFormInfo> aRMFormInfos = gson.fromJson(jsonString, listType1);
        
        if (ArrayUtils.isNotEmpty(aRMFormInfos)) {
            Set<Long> userIds = new HashSet<>();
            for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                userIds.add(aRMFormInfo.getStudentId());
                userIds.add(aRMFormInfo.getAdminId());
                if(aRMFormInfo.getCreatedBy()!= null){
                Long createdById = Long.parseLong(aRMFormInfo.getCreatedBy());
                userIds.add(createdById);
                }
            }
            Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                if (usermap.containsKey(aRMFormInfo.getStudentId())) {
                	aRMFormInfo.setStudent(usermap.get(aRMFormInfo.getStudentId()));
                }
                if(aRMFormInfo.getCreatedBy()!= null){
	                if (usermap.containsKey(Long.parseLong(aRMFormInfo.getCreatedBy()))) {
	                	aRMFormInfo.setCreatedByBasicInfo(usermap.get(Long.parseLong(aRMFormInfo.getCreatedBy())));
	                }
                }
                if (usermap.containsKey(aRMFormInfo.getAdminId())) {
                	aRMFormInfo.setAdmin(usermap.get(aRMFormInfo.getAdminId()));
                }
            }
        }
        return aRMFormInfos;
    }
    	else {
    		throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Query id is null");
    	}
    }
    
    public List<ARMFormInfo> getARMFormByStartDate(Long startDate) throws VException{
    	if(startDate == null){
    		throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Query startDate is null");
    	}
    	else {
    	
         ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                 + "/armform/getARMFormByStartDate/" + startDate, HttpMethod.GET, null);
         VExceptionFactory.INSTANCE.parseAndThrowException(resp);
         String jsonString = resp.getEntity(String.class);
         Type listType1 = new TypeToken<ArrayList<ARMFormInfo>>() {
         }.getType();
         List<ARMFormInfo> aRMFormInfos = gson.fromJson(jsonString, listType1);
         
         if (ArrayUtils.isNotEmpty(aRMFormInfos)) {
             Set<Long> userIds = new HashSet<>();
             for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                 userIds.add(aRMFormInfo.getStudentId());
                 Long createdById = Long.parseLong(aRMFormInfo.getCreatedBy());
                 userIds.add(createdById);
                 if(aRMFormInfo.getAdminId()!=null){
                 	userIds.add(aRMFormInfo.getAdminId());
                 }
                 
             }
             Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
             for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                 if (usermap.containsKey(aRMFormInfo.getStudentId())) {
                 	aRMFormInfo.setStudent(usermap.get(aRMFormInfo.getStudentId()));
                 }
                 if (usermap.containsKey(Long.parseLong(aRMFormInfo.getCreatedBy()))) {
                 	aRMFormInfo.setCreatedByBasicInfo(usermap.get(Long.parseLong(aRMFormInfo.getCreatedBy())));
                 }
                 if(aRMFormInfo.getAdminId() != null){
 	                if (usermap.containsKey(aRMFormInfo.getAdminId())) {
 	                	aRMFormInfo.setAdmin(usermap.get(aRMFormInfo.getAdminId()));
 	                }
                 }
             }
         }
     	return aRMFormInfos;
    	}
    }
    
    public List<ARMFormInfo> getARMFormByStudentId(Long studentId) throws VException{
    	if(studentId == null){
    		throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Query studentId is null");
    	}
    	else {
    	
         ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                 + "/armform/getARMFormByStudentId/" + studentId, HttpMethod.GET, null);
         VExceptionFactory.INSTANCE.parseAndThrowException(resp);
         String jsonString = resp.getEntity(String.class);
         Type listType1 = new TypeToken<ArrayList<ARMFormInfo>>() {
         }.getType();
         List<ARMFormInfo> aRMFormInfos = gson.fromJson(jsonString, listType1);
         
         if (ArrayUtils.isNotEmpty(aRMFormInfos)) {
             Set<Long> userIds = new HashSet<>();
             for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                 userIds.add(aRMFormInfo.getStudentId());
                 Long createdById = Long.parseLong(aRMFormInfo.getCreatedBy());
                 userIds.add(createdById);
                 if(aRMFormInfo.getAdminId()!=null){
                 	userIds.add(aRMFormInfo.getAdminId());
                 }
                 
             }
             Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
             for (ARMFormInfo aRMFormInfo : aRMFormInfos) {
                 if (usermap.containsKey(aRMFormInfo.getStudentId())) {
                 	aRMFormInfo.setStudent(usermap.get(aRMFormInfo.getStudentId()));
                 }
                 if (usermap.containsKey(Long.parseLong(aRMFormInfo.getCreatedBy()))) {
                 	aRMFormInfo.setCreatedByBasicInfo(usermap.get(Long.parseLong(aRMFormInfo.getCreatedBy())));
                 }
                 if(aRMFormInfo.getAdminId() != null){
 	                if (usermap.containsKey(aRMFormInfo.getAdminId())) {
 	                	aRMFormInfo.setAdmin(usermap.get(aRMFormInfo.getAdminId()));
 	                }
                 }
             }
         }
     	return aRMFormInfos;
    	}
    }

    public void exportARMForm(long startTime, long endTime, long callingUserId) {
		logger.info("Invoking daily session task");
        Map<String, Object> payload = new HashMap<>();
        payload.put("startTime", startTime);
        payload.put("endTime", endTime);
        payload.put("requestEmailId", callingUserId);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_ARM_FORM_EMAIL, payload);
        asyncTaskFactory.executeTask(params);
    }
}
