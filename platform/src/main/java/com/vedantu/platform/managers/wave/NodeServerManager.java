/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.wave;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.dao.wave.SessionIdVsServer;
import com.vedantu.platform.dao.wave.WaveDAO;

import com.vedantu.platform.pojo.wave.NodeServerData;
import com.vedantu.platform.pojo.wave.NodeAddress;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NodeServerManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private WaveDAO waveDAO;

    @Autowired
    private RedisDAO redisManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(NodeServerManager.class);

    private static final String NODE_SERVER_CPU_USAGE_KEY = ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase() + "_NODE_SERVER_CPU_USAGE";
    private static final String OTM_SERVERS_CPU_USAGE_KEY = ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase() + "_OTM_SERVERS_CPU_USAGE";
    private static final long VALID_SERVER_SECONDS_IN_PAST = 15;
    private static final long REMOVE_SERVER_SECONDS_IN_PAST = 16;
    private static final double MAX_CPU_USAGE_LIMIT = 75.0;
    private static String MILLIS_PER_DAY;

    public NodeServerManager() {
    }

    @PostConstruct
    public void init() {
        MILLIS_PER_DAY = ConfigUtils.INSTANCE.getStringValue("redis.expiry.sessionvsserverkey");
    }

    public NodeAddress assignServer(String sessionId, SessionType sessionType) {
        logger.info("ENTRY sessionId " + sessionId);
        NodeAddress response = getDefaultServer();
        try {
            List<NodeServerData> availableServers = getAvailableServers(sessionType);
            String allottedServerStr = getServerOfSession(sessionId);
            logger.info("server obtained from redis " + allottedServerStr);
            if (StringUtils.isNotEmpty(allottedServerStr)) {
                response = new Gson().fromJson(allottedServerStr, NodeAddress.class);
                boolean found = false;
                for (NodeServerData data : availableServers) {
                    if (data.getServerIp().equals(response.getIp()) && data.getServerPort().equals(response.getPort())) {
                        found = true;
                    }
                }
                if (found) {
                    getSetServerOfSession(sessionId, response);
                    logger.info("exiting assignserver method " + response.toString());
                    return response;
                }
                removeServerOfSession(sessionId);
            }
            logger.info("available servers in redis " + availableServers);
            List<NodeServerData> minServerList = new ArrayList<>();
            for (NodeServerData data : availableServers) {
                if (data.getCurrentCpuUsage() < MAX_CPU_USAGE_LIMIT) {
                    minServerList.add(data);
                }
            }
            if (minServerList.isEmpty()) {
                logger.error("All servers are exceeding max limit " + MAX_CPU_USAGE_LIMIT);
                minServerList = availableServers;
            }
            if (!minServerList.isEmpty()) {
                NodeServerData selectedServer = minServerList.get(0);
                for (int i = 1; i < minServerList.size(); i++) {
                    if (minServerList.get(i).getConnectedSockets()
                            < selectedServer.getConnectedSockets()) {
                        selectedServer = minServerList.get(i);
                    }
                }
                response = new NodeAddress(selectedServer.getServerIp(), selectedServer.getServerPort());
            }
        } catch (Exception e) {
            logger.error("Some error in selecting best available server ", e);
        }

        NodeAddress newResponse = getSetServerOfSession(sessionId, response);
        //if both users get the same server allocated, then this will be a multiple save, it will not cause any problems
        if (newResponse.equals(response)) {
            SessionIdVsServer entry = new SessionIdVsServer(sessionId, newResponse.getIp(), newResponse.getPort());
            waveDAO.create(entry);
        }
        logger.info("EXIT " + newResponse.toString());
        return newResponse;

    }
    //use the added server entry only if <=15 secs in the past 
    //and delete anything >= 16 in the past

    private List<NodeServerData> getAvailableServers(SessionType sessionType) {
        List<NodeServerData> availableServers = new ArrayList<>();
        try {
            String key = NODE_SERVER_CPU_USAGE_KEY;
            if (SessionType.OTF.equals(sessionType)) {
                key = OTM_SERVERS_CPU_USAGE_KEY;
            }
            Set<String> _availableServers = getAllAvailableServers(key);
            if (_availableServers == null || _availableServers.isEmpty()) {
                logger.error("No available servers found in redis");
            } else {
                logger.info("available servers in redis " + _availableServers);
                for (String json : _availableServers) {
                    if (StringUtils.isNotEmpty(json)) {
                        NodeServerData data = new Gson().fromJson(json, NodeServerData.class);
                        if (data.getTimestampInSecs() != null
                                && (System.currentTimeMillis() / 1000 - data.getTimestampInSecs()) <= VALID_SERVER_SECONDS_IN_PAST) {
                            availableServers.add(data);
                        }
                    }
                }
            }
            removePastCpuUsageEntries(key);
        } catch (Exception e) {
            logger.error("Some error in selecting best available server ");
        }
        return availableServers;
    }

    public NodeAddress getDefaultServer() {
        return new NodeAddress(ConfigUtils.INSTANCE.getStringValue("default.node.host"), ConfigUtils.INSTANCE.getStringValue("default.node.port"));
    }

    private String getServerOfSession(String sessionId) throws InternalServerErrorException, BadRequestException {
        String key = getSessionIDVsServerIPKey(sessionId);
        return redisManager.get(key);
    }

    private Set<String> getAllAvailableServers(String key) throws InternalServerErrorException {
        Set<String> servers = redisManager.getZRevRange(key, 0, -1);
        return servers;
    }

    //in the past >=16secs
    private void removePastCpuUsageEntries(String key) throws InternalServerErrorException, BadRequestException {
        Long end = (System.currentTimeMillis() / 1000 - REMOVE_SERVER_SECONDS_IN_PAST);
        logger.info("removing old entries" + end);
        long result = redisManager.zremrangebyscore(key, 0, end);
        logger.info("result " + result);
    }

    private NodeAddress getSetServerOfSession(String sessionId, NodeAddress ipAndPort) {
        String script = "if redis.call('GET', KEYS[1]) then return redis.call('GET', KEYS[1]) else redis.call('SETEX', KEYS[1],ARGV[1],ARGV[2]) return ARGV[2] end";
        List<String> keys = new ArrayList<>();
        keys.add(getSessionIDVsServerIPKey(sessionId));
        List<String> values = new ArrayList<>();
        values.add(MILLIS_PER_DAY);
        values.add(new Gson().toJson(ipAndPort));
        NodeAddress nodeAddress = null;
        try {
            String jsonStrResp = redisManager.eval(script, keys, values);
            nodeAddress = new Gson().fromJson(jsonStrResp, NodeAddress.class);
        } catch (Exception e) {
            logger.error("Some error in getSetServerOfSession ", e);
        }
        logger.info("EXIT " + nodeAddress);
        return nodeAddress;
    }

    private Long removeServerOfSession(String sessionId) {
        logger.info("removing server of the session:" + sessionId);
        Long jsonStrResp = null;
        try {
            jsonStrResp = redisManager.del(getSessionIDVsServerIPKey(sessionId));
        } catch (Exception e) {
            logger.error("Some error in setServerOfSession ", e);
        }
        logger.info("EXIT " + jsonStrResp);
        return jsonStrResp;
    }

    private String getSessionIDVsServerIPKey(String sessionId) {
        return "SESSIONID_VS_SERVER_" + sessionId;
    }
}
