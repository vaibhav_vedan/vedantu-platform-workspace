/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.click2call;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.platform.mongodbentities.click2call.PhoneCallMetadata;
import java.util.Map;

/**
 *
 * @author somil
 */
public interface ICloudPhoneCallManager {

	public PhoneCallMetadata connectCall(String fromNumber, String toNumber,
			String contextId, String contextType) throws ForbiddenException;

	public PhoneCallMetadata updatePhoneCallMetadata(String callSid,
			         Map<String, Object> dataMap);

//	public String sendSMS(String toNumber, String message);
//
//	public boolean isDNDNumber(String number) throws BadRequestException;
//
//	public boolean addToDNDWhitelist(String number);
//
//	public boolean isWhitelistedNumber(String number);
//
//	public boolean isCallAllowed(String number) throws BadRequestException;

}
