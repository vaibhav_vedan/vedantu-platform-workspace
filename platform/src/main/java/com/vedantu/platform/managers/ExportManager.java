package com.vedantu.platform.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.*;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.User.response.TeacherListWithStatusRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dashboard.pojo.ExportTeacherDashboard;
import com.vedantu.dashboard.response.GetTeacherDashboardReq;
import com.vedantu.dashboard.response.GetTeacherDashboardResp;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.*;
import com.vedantu.dinero.response.InstalmentPaymentInfoRes;
import com.vedantu.exception.*;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GetOtfDashboardRes;
import com.vedantu.onetofew.pojo.OTFAttendance;
import com.vedantu.onetofew.pojo.StudentContentPojo;
import com.vedantu.onetofew.request.GetOTFDashboardReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.enums.ExportType;
import com.vedantu.platform.managers.Dashboard.DashboardManager;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.offering.OfferingManager;
import com.vedantu.platform.managers.review.FeedbackManager;
import com.vedantu.platform.managers.review.RatingReviewManager;
import com.vedantu.platform.managers.scheduling.CalendarManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.offering.Offering;
import com.vedantu.platform.mongodbentities.review.Feedback;
import com.vedantu.platform.mongodbentities.review.Review;
import com.vedantu.platform.pojo.*;
import com.vedantu.platform.pojo.courseplan.ExportCoursePlanPojo;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.pojo.scheduling.TeacherAvailabilityData;
import com.vedantu.platform.pojo.subscription.GetSubscriptionsResponse;
import com.vedantu.platform.request.AvailabilityTeacherExportReq;
import com.vedantu.platform.request.ExportSubscriptionReq;
import com.vedantu.platform.request.ExportUsersReq;
import com.vedantu.review.requests.GetReviewRequest;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.scheduling.pojo.session.ReferenceType;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.session.pojo.*;
import com.vedantu.subscription.pojo.ExportCoursePlan;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.request.GetSubscriptionsReq;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.*;
import com.vedantu.util.EntityType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.subscription.TrialSessionInfos;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.supercsv.io.dozer.CsvDozerBeanWriter;
import org.supercsv.io.dozer.ICsvDozerBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.mail.internet.AddressException;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;
import org.dozer.DozerBeanMapper;

@Service
public class ExportManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ExportManager.class);

    private static final Integer SIZE = 1000;

    @Autowired
    UserManager userManager;

    @Autowired
    BoardManager boardManager;

    @Autowired
    OfferingManager offeringManager;

    @Autowired
    CommunicationManager communicationManager;

    @Autowired
    HttpSessionUtils httpSessionUtils;

    @Autowired
    SessionManager sessionManager;

    @Autowired
    SubscriptionManager subscriptionManager;

    @Autowired
    FeedbackManager feedbackManager;

    @Autowired
    FosUtils userUtils;

    @Autowired
    RatingReviewManager ratingReviewManager;

    @Autowired
    CalendarManager calendarManager;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @Autowired
    CoursePlanManager coursePlanManager;

    @Autowired
    PaymentManager paymentManager;

    @Autowired
    DashboardManager dashboardManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    private static final Gson GSON = new Gson();

    @Autowired
    private DozerBeanMapper mapper;
    private static final String CSV_DELIMITER = ",";

    public BasicRes exportUsers(ExportUsersReq req, HttpSessionData httpSessionData)
            throws VException, NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, AddressException, IOException {
        req.verify();
        String[] fieldsArrayStudent = {"userId", "firstName", "lastName", "email", "board", "grade", "school",
            "contactNumber", "parentContactNumbers", "registrationDate", "registrationTime", "isEmailVerified",
            "isContactNumberVerified", "packageRequested", "requestedDate", "requestedTime", "role", "campaign",
            "campaignMedium", "campaignSource", "city", "state", "country", "numberOfSessions",
            "numberOfSessionsOutsidePackage", "numberOfSessionsOutsidePackageFree",
            "numberOfSessionsOutsidePackagePaid", "numberOfSessionsInsidePackage",
            "numberOfSessionsInsidePackageFree", "numberOfSessionsInsidePackagePaid", "activeDuration",
            "activeDurationOutsidePackage", "activeDurationOutsidePackageFree", "activeDurationOutsidePackagePaid",
            "activeDurationInsidePackage", "activeDurationInsidePackageFree", "activeDurationInsidePackagePaid",
            "parentRegistration", "referralCode", "signUpURL"};

        String[] fieldsArrayTeacher = {"active", "userId", "firstName", "lastName", "email", "contactNumber",
            "extensionNumber", "gender", "city", "state", "country", "languagePrefs", "experience",
            "latestEducation", "latestJob", "primarySubject", "secondarySubject", "biggestStrength",
            "professionalCategory", "grades", "categories", "primaryCallingNumber", "responseTime", "rating",
            "numberOfRatings", "registrationDate", "registrationTime", "isEmailVerified", "isContactNumberVerified",
            "numberOfSessions", "numberOfHoursTaught", "profilePicUploaded", "courses", "retainabilityIndex",
            "punctualityIndex", "regularityIndex", "contentSharingIndex", "responseTimeIndex", "usersChattedWith",
            "referralCode", "signUpURL"};

        List<String> fields = new ArrayList<>();

        if (req.getRole().equals(Role.TEACHER)) {
            fields = Arrays.asList(fieldsArrayTeacher);
        } else {
            fields = Arrays.asList(fieldsArrayStudent);
        }

        BasicRes res = new BasicRes();

        String data = StringUtils.join(fields.iterator(), ",");
        data += "\n";

        Integer start = 0;

        while (true) {
            GetUsersReq request = new GetUsersReq();
            request.setRole(req.getRole());
            request.setStart(start);
            request.setSize(SIZE);
            request.setFromTime(req.getFromTime());
            request.setTillTime(req.getTillTime());

            GetUsersRes response = userManager.getUsers(request);
            List<UserInfo> users = response.getUsers();
            if (users.isEmpty()) {
                break;
            }

            List<ExportUser> exportUsers = new ArrayList<>();

            for (UserInfo user : users) {
                ExportUser exportUser = new ExportUser();
                exportUser.firstName = getCSVEscapedString(user.getFirstName());
                exportUser.lastName = getCSVEscapedString(user.getLastName());
                mapper.map(user, exportUser);
                exportUser.userId = "'" + user.getUserId();
                exportUser.campaign = user.getUtm().getUtm_campaign();
                exportUser.campaignMedium = user.getUtm().getUtm_medium();
                exportUser.campaignSource = user.getUtm().getUtm_source();
                exportUser.parentRegistration = user.getParentRegistration();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                exportUser.registrationDate = dateFormat.format(user.getCreationTime());

                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
                timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                exportUser.registrationTime = timeFormat.format(user.getCreationTime());

                if (user.getStudentInfo() != null) {
                    mapper.map(user.getStudentInfo(), exportUser);
                    exportUser.school = getCSVEscapedString(user.getStudentInfo().getSchool());
                }

                if (user.getLocationInfo() != null) {
                    mapper.map(user.getLocationInfo(), exportUser);
                }
                exportUser.packageRequested = "";
                exportUser.requestedTime = "";

                if (!StringUtils.isEmpty(user.getProfilePicUrl())) {
                    exportUser.setProfilePicUploaded(true);
                }

                // using old subscription, hence not implementing
                // if (user.getRole().equals(Role.STUDENT)) {
                // Subscription subscription =
                // subscriptionDAO.getSubscription(user.getId(), null, null,
                // null, mgr);
                // if (subscription != null) {
                // exportUser.requestedDate =
                // dateFormat.format(subscription.getCreationTime());
                // exportUser.requestedTime =
                // timeFormat.format(subscription.getCreationTime());
                // Long offeringId = subscription.getOfferingId();
                // if (offeringId != null) {
                // Offering offering = offeringDAO.getOfferingById(offeringId,
                // mgr);
                // if (offering != null) {
                // exportUser.packageRequested =
                // getCSVEscapedString(offering.getTitle());
                // }
                //
                // }
                // }
                // }
                if (user.getRole().equals(Role.TEACHER)) {
                    TeacherInfo teacherInfo = user.getTeacherInfo();
                    if (teacherInfo != null) {
                        mapper.map(teacherInfo, exportUser);
                        if (teacherInfo.getExtensionNumber() != null) {
                            String formattedExtensionNumber = "'"
                                    + String.format("%04d", teacherInfo.getExtensionNumber());
                            exportUser.setExtensionNumber(formattedExtensionNumber);
                        }
                        if (teacherInfo.getResponseTime() != null) {
                            exportUser.setResponseTime(("" + ((float) teacherInfo.getResponseTime()) / 3600000));
                        }
                        if (user.getRatingInfo() != null) {
                            exportUser.setRating(user.getRatingInfo().getAvgRating());
                            exportUser.setNoOfRatings(user.getRatingInfo().getTotalRatingCount().intValue());
                        }
                        long sessionCount = teacherInfo.getSessions();
                        float sessionhours = (((float) teacherInfo.getSessionHours()) / 3600000);
                        exportUser.setNumberOfSessions(sessionCount);
                        exportUser.setNumberOfHoursTaught(sessionhours);

                        if (teacherInfo.getBiggestStrength() != null) {
                            exportUser.setBiggestStrength(this.getCSVEscapedString(teacherInfo.getBiggestStrength()));
                        }
                        if (teacherInfo.getLatestEducation() != null) {
                            exportUser.setLatestEducation(this.getCSVEscapedString(teacherInfo.getLatestEducation()));
                        }
                        if (teacherInfo.getLatestJob() != null) {
                            exportUser.setLatestJob(this.getCSVEscapedString(teacherInfo.getLatestJob()));
                        }

                        if (teacherInfo.getPrimaryCallingNumberCode() != null
                                && teacherInfo.getPrimaryCallingNumberCode() != 0) {
                            exportUser.setPrimaryCallingNumber(ConfigUtils.INSTANCE.getStringValue(
                                    "teacher.call.extension." + teacherInfo.getPrimaryCallingNumberCode()));
                        }
                    }
                    boardManager.fillBoardInfo(user);

                    List<Offering> offerings = offeringManager.getTeacherOfferings(user.getUserId());
                    List<String> offeringNames = new ArrayList<>();

                    for (Offering offering : offerings) {
                        offeringNames.add(offering.getTitle());
                    }
                    exportUser.setCourses(offeringNames);
                }
                exportUsers.add(exportUser);
            }

            for (ExportUser exportUser : exportUsers) {
                if (CollectionUtils.isNotEmpty(fields)) {
                    for (String fieldName : fields) {
                        Field field = ExportUser.class.getField(fieldName);
                        Object fieldObj = field.get(exportUser);
                        if (fieldObj != null && field.getType().equals(List.class)) {
                            String[] fieldObjArray = fieldObj.toString().split(",");
                            data += (fieldObj != null ? StringUtils.join(fieldObjArray, ";") : " ") + ",";
                        } else {
                            data += (fieldObj != null ? fieldObj.toString() : " ") + ",";
                        }
                    }
                }
                data += "\n";
            }
            start = start + SIZE;
        }
        communicationManager.sendUserDataEmail(httpSessionData, data, ExportType.USER.toString());
        return res;
    }

    private String getCSVEscapedString(String value) {
        String output = value != null ? value.replace(",", "#") : value;
        output = output != null ? output.replace("\n", "#") : output;
        return output;
    }

    public BasicRes exportScheduledTOSSessions(GetSessionsReq req, HttpSessionData httpSessionData)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            AddressException, VException, IOException {
        req.verify();
        BasicRes res = new BasicRes();
        if (req.getAfterEndTime() == null || req.getBeforeEndTime() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, null, org.apache.logging.log4j.Level.WARN);
        }

        String[] fieldsArray = {"studentId", "studentEmail", "teacherId", "teacherEmail", "sessionId", "startTime",
            "isTOS", "firstSessionState", "firstSessionBillingPeriod"};

        List<String> fields = Arrays.asList(fieldsArray);

        String data = StringUtils.join(fields.iterator(), ",");
        data += "\n";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        Set<Long> userIdsChecked = new HashSet<>();

        // scheduled tos
        Integer start = 0;
        while (true) {
            req.setStart(start);
            req.setSize(SIZE);
            req.setSessionStates(Arrays.asList(SessionState.SCHEDULED));
            req.setIncludeAttendees(true);
            List<SessionInfo> scheduledSessions = sessionManager.getSessions(req, true);

            logger.info("start {0}, limit {1}, size {2}" + new Object[]{start, SIZE, scheduledSessions.size()});

            if (scheduledSessions.isEmpty()) {
                break;
            }

            Map<Long, List<SessionInfo>> userScheduledSessionsMap = new HashMap<>();
            Set<Long> userIdsToCheck = new HashSet<>();

            if (CollectionUtils.isNotEmpty(scheduledSessions)) {
                for (SessionInfo session : scheduledSessions) {
                    Long _userId = session.getStudentIds().get(0);
                    if (userScheduledSessionsMap.get(_userId) == null) {
                        userScheduledSessionsMap.put(_userId, new ArrayList<>());
                    }
                    userScheduledSessionsMap.get(_userId).add(session);
                    userIdsToCheck.add(_userId);
                    userIdsChecked.add(_userId);
                }
                for (Long studentId : userIdsToCheck) {

                    String studentEmail = null;

                    SessionInfo scheduledSession = userScheduledSessionsMap.get(studentId).get(0);
                    if (scheduledSession.getAttendees() != null) {
                        for (UserSessionInfo userSessionInfo : scheduledSession.getAttendees()) {
                            if (studentId.equals(userSessionInfo.getUserId())) {
                                studentEmail = userSessionInfo.getEmail();
                                break;
                            }
                        }
                    }

                    List<SessionState> states = new ArrayList<>();
                    states.add(SessionState.ENDED);
                    states.add(SessionState.CANCELED);
                    states.add(SessionState.EXPIRED);

                    GetSessionsReq overSessionReq = new GetSessionsReq();
                    overSessionReq.setStart(0);
                    overSessionReq.setSize(10);
                    overSessionReq.setStudentId(studentId);
                    overSessionReq.setIncludeAttendees(true);
                    overSessionReq.setSessionStates(states);

                    List<SessionInfo> overSessions = sessionManager.getSessions(overSessionReq, true);

                    if (overSessions.size() <= 1) {
                        logger.info("Cancelled or ended sessions <=1 for {0}" + studentId);
                        ExportTOSScheduledSession exportTOSScheduledSession = new ExportTOSScheduledSession();
                        // checking if the first session want well
                        if (overSessions.size() == 1) {
                            exportTOSScheduledSession.setIsTOS(false);
                            SessionInfo overSession = overSessions.get(0);
                            exportTOSScheduledSession.setFirstSessionId("'" + overSession.getId());
                            if (overSession.getState().equals(SessionState.CANCELED)
                                    || overSession.getState().equals(SessionState.EXPIRED)) {
                                exportTOSScheduledSession.setFirstSessionState(overSession.getState().name());
                            } else {
                                UserSessionInfo student = null;
                                if (overSession.getAttendees() != null) {
                                    for (UserSessionInfo userSessionInfo : overSession.getAttendees()) {
                                        if (Role.STUDENT.equals(userSessionInfo.getRole())) {
                                            student = userSessionInfo;
                                            break;
                                        }
                                    }
                                }
                                if (student != null && student.getBillingPeriod() != null
                                        && student.getBillingPeriod() < (DateTimeUtils.MILLIS_PER_MINUTE * 30)) {
                                    exportTOSScheduledSession.setFirstSessionState(overSession.getState().name());
                                    exportTOSScheduledSession.setFirstSessionBillingPeriod(
                                            String.valueOf(student.getBillingPeriod() / 1000));
                                } else {
                                    continue;
                                }
                            }
                        } else {
                            logger.info("isTOS for this studentId " + studentId);
                            exportTOSScheduledSession.setIsTOS(true);
                            exportTOSScheduledSession.setFirstSessionState("-");
                            exportTOSScheduledSession.setFirstSessionBillingPeriod("-");
                        }
                        exportTOSScheduledSession.setStudentId("'" + studentId);
                        exportTOSScheduledSession.setStudentEmail(studentEmail);

                        // creating multiple entries for each session scheduled
                        // for this user
                        List<SessionInfo> sessionsScheduled = userScheduledSessionsMap.get(studentId);
                        if (sessionsScheduled.size() > 0) {
                            for (SessionInfo _session : sessionsScheduled) {
                                exportTOSScheduledSession.setSessionId("'" + _session.getId());
                                UserSessionInfo teacher = null;
                                for (UserSessionInfo userSessionInfo : _session.getAttendees()) {
                                    if (Role.TEACHER.equals(userSessionInfo.getRole())) {
                                        teacher = userSessionInfo;
                                        logger.info("teacher found for " + studentId);
                                        break;
                                    }
                                }
                                if (teacher == null) {
                                    continue;
                                }
                                exportTOSScheduledSession.setTeacherId("'" + _session.getTeacherId());
                                exportTOSScheduledSession.setTeacherEmail(teacher.getEmail());
                                String startTime = dateFormat.format(_session.getStartTime());
                                exportTOSScheduledSession.setStartTime(startTime);
                                // adding csv string
                                if (CollectionUtils.isNotEmpty(fields)) {
                                    for (String fieldName : fields) {
                                        Field field = ExportTOSScheduledSession.class.getField(fieldName);
                                        data += (field.get(exportTOSScheduledSession) != null
                                                ? field.get(exportTOSScheduledSession) : " ") + ",";
                                    }
                                }
                                data += "\n";
                            }
                        }
                    }
                }
            }
            start += SIZE;
        }

        // finished TOS
        String[] tosFailedAndNotScheduledFieldsArr = {"studentId", "studentEmail", "sessionIds", "states",
            "billingPeriods"};

        List<String> tosFailedAndNotScheduledFields = Arrays.asList(tosFailedAndNotScheduledFieldsArr);

        String tosFailedAndNotScheduledData = StringUtils.join(tosFailedAndNotScheduledFields.iterator(), ",");
        tosFailedAndNotScheduledData += "\n";

        // fetch cancelled sessions
        start = 0;
        Set<Long> userIdsToCheck = new HashSet<>();
        Map<Long, String> emailIdMap = new HashMap<>();
        Long diff = req.getBeforeEndTime() - req.getAfterEndTime();
        Long newStartTime = req.getAfterEndTime() - diff;

        while (true) {
            GetSessionsReq pastSessionsReq = new GetSessionsReq();
            pastSessionsReq.setAfterEndTime(newStartTime);
            pastSessionsReq.setBeforeEndTime(req.getAfterEndTime());
            pastSessionsReq.setIncludeAttendees(true);
            pastSessionsReq.setStart(start);
            pastSessionsReq.setSize(SIZE);

            List<SessionInfo> pastSessions = sessionManager.getSessions(pastSessionsReq, true);
            logger.info("start {0}, limit {1}, size {2}", new Object[]{start, SIZE, pastSessions.size()});

            if (pastSessions.isEmpty()) {
                break;
            }
            for (SessionInfo session : pastSessions) {
                if (session.getState() == SessionState.ENDED || session.getState() == SessionState.EXPIRED
                        || session.getState() == SessionState.CANCELED) {
                    Long _userId = session.getStudentIds().get(0);
                    userIdsToCheck.add(_userId);

                    if (session.getAttendees() != null) {
                        for (UserSessionInfo userSessionInfo : session.getAttendees()) {
                            if (Role.STUDENT.equals(userSessionInfo.getRole())) {
                                emailIdMap.put(_userId, userSessionInfo.getEmail());
                                break;
                            }
                        }
                    }

                }
            }
            start += SIZE;
        }

        for (Long studentId : userIdsToCheck) {
            if (userIdsChecked.contains(studentId)) {
                continue;
            }
            TOSData tOSData = calculateTOSData(studentId);
            if (!tOSData.tookTOS || (tOSData.tookTOS && tOSData.allTOSFailed)) {
                ExportTOSFailedAndNotScheduled exportTOSFailedAndNotScheduled = new ExportTOSFailedAndNotScheduled();
                String studentEmail = emailIdMap.get(studentId);
                if (studentEmail == null) {
                    continue;
                }
                exportTOSFailedAndNotScheduled.setStudentId("'" + studentId);
                exportTOSFailedAndNotScheduled.setStudentEmail(studentEmail);
                exportTOSFailedAndNotScheduled
                        .setBillingPeriods(StringUtils.join(tOSData.failedTOSBillingPeriods.iterator(), ","));
                exportTOSFailedAndNotScheduled
                        .setSessionIds(StringUtils.join(tOSData.failedTOSSessionIds.iterator(), ","));
                exportTOSFailedAndNotScheduled.setStates(StringUtils.join(tOSData.failedTOSStates.iterator(), ","));
                // adding csv string
                for (String fieldName : tosFailedAndNotScheduledFields) {
                    Field field = ExportTOSFailedAndNotScheduled.class.getField(fieldName);
                    tosFailedAndNotScheduledData += (field.get(exportTOSFailedAndNotScheduled) != null
                            ? field.get(exportTOSFailedAndNotScheduled) : " ") + ",";
                }
                tosFailedAndNotScheduledData += "\n";
            }
        }
        String fileName = "TOS Sessions(" + (dateFormat.format(req.getAfterEndTime())) + "-"
                + (dateFormat.format(req.getBeforeEndTime())) + ")";
        communicationManager.sendUserDataEmail(httpSessionData, data, fileName, fileName);

        // sending mail for failed tos and not scheduled
        String fileName2 = "TOS Sessions Failed and Not scheduled(" + (dateFormat.format(newStartTime)) + "-"
                + (dateFormat.format(req.getAfterEndTime())) + ")";
        communicationManager.sendUserDataEmail(httpSessionData, tosFailedAndNotScheduledData, fileName2, fileName2);
        return res;
    }

    private TOSData calculateTOSData(Long studentId) throws VException, IllegalAccessException {
        TOSData tOSData = new TOSData();
        List<SessionState> states = new ArrayList<>();
        states.add(SessionState.ENDED);
        states.add(SessionState.CANCELED);
        states.add(SessionState.EXPIRED);

        GetSessionsReq overSessionReq = new GetSessionsReq();
        overSessionReq.setStart(0);
        overSessionReq.setSize(10);
        overSessionReq.setStudentId(studentId);
        overSessionReq.setIncludeAttendees(true);
        overSessionReq.setSessionStates(states);

        List<SessionInfo> overSessions = sessionManager.getSessions(overSessionReq, true);

        if (overSessions.size() <= 3) {
            logger.info("Cancelled or ended sessions <=1 for {0} " + studentId);
            // checking if the first few sessions want well
            if (overSessions.size() > 0) {
                tOSData.tookTOS = true;
                tOSData.allTOSFailed = true;
                List<String> failedTOSSessionIds = new ArrayList<>();
                List<String> failedTOSStates = new ArrayList<>();
                List<Integer> failedTOSBillingPeriods = new ArrayList<>();
                for (SessionInfo overSession : overSessions) {
                    failedTOSSessionIds.add(overSession.getId().toString());
                    failedTOSStates.add(overSession.getState().name());
                    if (overSession.getState().equals(SessionState.ENDED)) {

                        UserSessionInfo student = null;
                        if (overSession.getAttendees() != null) {
                            for (UserSessionInfo userSessionInfo : overSession.getAttendees()) {
                                if (Role.STUDENT.equals(userSessionInfo.getRole())) {
                                    student = userSessionInfo;
                                    break;
                                }
                            }
                        }
                        if (student != null && student.getBillingPeriod() != null && student.getBillingPeriod() < (DateTimeUtils.MILLIS_PER_MINUTE * 30)) {
                            failedTOSBillingPeriods.add(student.getBillingPeriod() / 1000);
                        } else {
                            tOSData.allTOSFailed = false;
                            break;
                        }
                    } else {
                        failedTOSBillingPeriods.add(0);
                    }
                }
                if (tOSData.allTOSFailed) {
                    tOSData.failedTOSBillingPeriods = failedTOSBillingPeriods;
                    tOSData.failedTOSSessionIds = failedTOSSessionIds;
                    tOSData.failedTOSStates = failedTOSStates;
                }
            } else {
                tOSData.tookTOS = false;
            }
        } else {
            tOSData.tookTOS = true;
        }
        return tOSData;
    }

    public BasicRes exportUsersAsync(ExportUsersReq req, HttpSessionData currentSessionData) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        payload.put("sessionData", currentSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_USERS, payload);
        asyncTaskFactory.executeTask(params);
        return new BasicRes();
    }

    public BasicRes exportSubscriptionsAsync(ExportSubscriptionReq req, HttpSessionData currentSessionData) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        payload.put("sessionData", currentSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_SUBSCRIPTIONS, payload);
        asyncTaskFactory.executeTask(params);
        return new BasicRes();
    }

    public BasicRes exportCoursePlansAsync(ExportCoursePlansReq req, HttpSessionData currentSessionData) {
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_EXPORT;
        ExportCoursePlanPojo pojo = new ExportCoursePlanPojo(req, currentSessionData);
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, GSON.toJson(pojo));
        /*
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        payload.put("sessionData", currentSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_COURSE_PLANS, payload);
        asyncTaskFactory.executeTask(params);
        */
        return new BasicRes();
    }

    public BasicRes exportScheduledTOSSessionsAsync(GetSessionsReq req, HttpSessionData currentSessionData) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        payload.put("sessionData", currentSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_TOS, payload);
        asyncTaskFactory.executeTask(params);
        return new BasicRes();
    }

    public BasicRes exportSessionsAsync(GetSessionsReq req, HttpSessionData currentSessionData) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        payload.put("sessionData", currentSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_SESSIONS, payload);
        asyncTaskFactory.executeTask(params);
        return new BasicRes();
    }

    public BasicRes exportAvailabilityAsync(AvailabilityTeacherExportReq req, HttpSessionData currentSessionData) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        payload.put("sessionData", currentSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_AVAILABILITY, payload);
        asyncTaskFactory.executeTask(params);
        return new BasicRes();
    }

    public class TOSData {

        public boolean tookTOS;
        public boolean allTOSFailed;
        public List<String> failedTOSSessionIds;
        public List<String> failedTOSStates;
        public List<Integer> failedTOSBillingPeriods;

        public TOSData() {
            this.failedTOSSessionIds = new ArrayList<>();
            this.failedTOSStates = new ArrayList<>();
            this.failedTOSBillingPeriods = new ArrayList<>();
        }
    }

    public BasicRes exportSubscriptions(ExportSubscriptionReq req, HttpSessionData httpSessionData)
            throws BadRequestException, NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, AddressException, VException, IOException {

        req.verify();
        BasicRes res = new BasicRes();

        String[] fieldsArray = {"offeringId", "offeringType", "offeringName", "studentId", "studentName",
            "studentEmail", "studentPhone", "teacherId", "teacherName", "teacherEmail", "teacherPhone",
            "paidAmount", "paymentDate", "paymentTime", "currencyCode", "paymentStatus", "state", "transactionId",
            "planId", "hourBalance", "lockedHourBalance", "totalHourConsumed", "creationDate", "creationTime",
            "model", "active"};

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        List<String> fields = Arrays.asList(fieldsArray);

        String data = StringUtils.join(fields.iterator(), ",");
        data += "\n";

        Integer start = 0;
        while (true) {
            GetSubscriptionsReq reqPojo = new GetSubscriptionsReq(start, SIZE, null, null, null, null,
                    req.getTillTime(), req.getFromTime(), null, null);

            GetSubscriptionsResponse resPojo = subscriptionManager.getSubscriptions(reqPojo);

            if (resPojo == null || resPojo.getSubscriptionsList() == null || resPojo.getSubscriptionsList().isEmpty()) {
                break;
            }

            List<ExportSubscription> exportSubscriptions = new ArrayList<>();

            for (SubscriptionResponse subscription : resPojo.getSubscriptionsList()) {

                ExportSubscription exportSubscription = mapper.map(subscription, ExportSubscription.class);

                exportSubscription.setModel(subscription.getModel());

                UserInfo student = subscription.getStudent();
                UserInfo teacher = subscription.getTeacher();

                if (student != null) {
                    exportSubscription.setStudentId(student.getUserId());
                    exportSubscription.setStudentName(student.getFirstName() + " " + student.getLastName());
                    exportSubscription.setStudentEmail(student.getEmail());
                    exportSubscription.setStudentPhone(student.getContactNumber());
                }
                if (teacher != null) {
                    exportSubscription.setTeacherId(teacher.getUserId());
                    exportSubscription.setTeacherName(teacher.getFirstName() + " " + teacher.getLastName());
                    exportSubscription.setTeacherEmail(teacher.getEmail());
                    exportSubscription.setTeacherPhone(teacher.getContactNumber());
                }

                if (subscription.getCreationTime() != null) {
                    exportSubscription.setCreationDate(dateFormat.format(subscription.getCreationTime()));
                    exportSubscription.setCreationTime(timeFormat.format(subscription.getCreationTime()));
                }
                if (subscription.getRemainingHours() != null) {
                    exportSubscription.setHourBalance((float) subscription.getRemainingHours() / 3600000);
                }
                if (subscription.getLockedHours() != null) {
                    exportSubscription.setLockedHourBalance((float) subscription.getLockedHours() / 3600000);
                }
                if (subscription.getTotalHours() != null) {
                    exportSubscription.setTotalHourConsumed((float) subscription.getTotalHours() / 3600000);
                }
                exportSubscription
                        .setPaidAmount(subscription.getTeacherHourlyRate() * subscription.getTotalHours() / 360000000);
                Long offeringId = subscription.getOfferingId();
                if (offeringId != null && offeringId != 0) {
                    if (offeringId == -1) {
                        exportSubscription.setOfferingType("Teacher Hours");
                    } else {
                        Offering offering = offeringManager.getOfferingById(offeringId);
                        exportSubscription.setOfferingName(getCSVEscapedString(offering.getTitle()));
                        exportSubscription.setOfferingType(offering.getType().toString());
                    }
                }

                if (subscription.getCreationTime() != null) {
                    exportSubscription.setPaymentDate(dateFormat.format(subscription.getCreationTime()));
                    exportSubscription.setPaymentTime(timeFormat.format(subscription.getCreationTime()));
                }

                if (subscription.getPlanId() != null) {
                    exportSubscription.setPlanId(subscription.getPlanId());
                }
                exportSubscription.setActive("false");
                if (subscription.getIsActive() != null) {
                    if (subscription.getIsActive()) {
                        exportSubscription.setActive("true");
                    }
                }

                exportSubscriptions.add(exportSubscription);
            }

            if (CollectionUtils.isNotEmpty(exportSubscriptions)) {
                for (ExportSubscription exportSubscription : exportSubscriptions) {
                    if (CollectionUtils.isNotEmpty(fields)) {
                        for (String fieldName : fields) {
                            Field field = ExportSubscription.class.getField(fieldName);

                            Object fieldObj = field.get(exportSubscription);
                            if (fieldObj != null && field.getType().equals(List.class)) {
                                String[] fieldObjArray = fieldObj.toString().split(",");
                                data += (fieldObj != null ? StringUtils.join(fieldObjArray, ";") : " ") + ",";
                            } else {
                                data += (fieldObj != null ? fieldObj.toString() : " ") + ",";
                            }
                        }
                    }
                    data += "\n";
                }
            }
            start += SIZE;
        }

        communicationManager.sendUserDataEmail(httpSessionData, data, ExportType.SUBSCRIPTION.toString());

        return res;
    }

    public BasicRes exportSessions(GetSessionsReq req, HttpSessionData httpSessionData)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            AddressException, VException, IOException {

        req.verify();
        BasicRes res = new BasicRes();

        String[] fieldsArray = {"sessionId", "sessionTitle", "subject", "topic", "displayState", "type", "packageName",
            "grade", "studentName", "studentEmail", "studentPhoneNumber", "studentState", "teacherName",
            "teacherEmail", "teacherPhoneNumber", "teacherState", "bookedDate", "bookedTime",
            "sessionRatingStudent", "sessionRatingTeacher", "teacherRating", "chargeRate", "payRate", "sessionTime",
            "sessionDate", "startTime", "endTime", "startedAt", "endedAt", "replayLink", "studentFeedback",
            "studentComments", "teacherFeedback", "teacherComments", "remarks", "lastUpdatedName",
            "lastUpdatedRole", "lastUpdatedEmail", "lastUpdatedDate", "lastUpdatedTime", "studentReason",
            "studentReviewRating", "studentReviewReason", "studentReview", "teacherReason",
            "teacherSessionCoverage", "teacherStudentPerformance", "teacherHomeWork", "teacherNextSessionPlan",
            "teacherRemark"};

        List<String> fields = Arrays.asList(fieldsArray);

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        String data = StringUtils.join(fields.iterator(), ",");
        data += "\n";

        Integer start = 0;
        while (true) {
            req.setStart(start);
            req.setSize(SIZE);
            // req.setSessionStates(Arrays.asList(SessionState.SCHEDULED));
            req.setIncludeAttendees(true);
            List<SessionInfo> sessions = sessionManager.getSessions(req, true);
            logger.info("sessions size: " + sessions.size());
            if (sessions.isEmpty()) {
                break;
            }

            List<ExportUserSession> exportSessions = new ArrayList<>();
            Review review;
            for (SessionInfo session : sessions) {
                ExportUserSessionListDetail exportSession = new ExportUserSessionListDetail();

                mapper.map(session, exportSession);
                exportSession.sessionId = "'" + session.getId();
                exportSession.sessionTitle = getCSVEscapedString(session.getTitle());
                exportSession.displayState = (session.getDisplayState() != null) ? session.getDisplayState().toString()
                        : " ";
                exportSession.type = (session.getType() != null) ? session.getType().toString() : "";
                List<ReferenceTag> sessionTags = session.getSessionTags();
                String offeringId = null;
                for (ReferenceTag sessionTag : sessionTags) {
                    if (ReferenceType.OFFERING.equals(sessionTag.getReferenceType())) {
                        offeringId = sessionTag.getReferenceId();
                    }
                }
                Offering offering = null;
                if (offeringId != null) {
                    offering = offeringManager.getOfferingById(Long.parseLong(offeringId));
                }

                exportSession.packageName = getCSVEscapedString((offering != null) ? offering.getTitle() : " ");
                exportSession.subject = getCSVEscapedString(session.getSubject());
                exportSession.topic = getCSVEscapedString(session.getTopic());

                if (session.getLastUpdatedBy() != null && !"0".equals(session.getLastUpdatedBy())) {
                    UserBasicInfo lastUpdated = userUtils.getUserBasicInfo(session.getLastUpdatedBy(), true);
                    if(lastUpdated!=null) {
                        exportSession.lastUpdatedName = lastUpdated.getFullName();
                        exportSession.lastUpdatedRole = lastUpdated.getRole().toString();
                        exportSession.setLastUpdatedEmail(lastUpdated.getEmail());
                    }
                    exportSession.setLastUpdatedDate(dateFormat.format(session.getLastUpdated()));
                    exportSession.setLastUpdatedTime(timeFormat.format(session.getLastUpdated()));
                }

                exportSession.replayLink = ConfigUtils.INSTANCE.getStringValue("url.base")
                        + ConfigUtils.INSTANCE.getStringValue("url.replay") + session.getId();

                if (session.getCreationTime() != null) {
                    exportSession.setBookedDate(dateFormat.format(session.getCreationTime()));
                    exportSession.setBookedTime(timeFormat.format(session.getCreationTime()));
                }

                if (session.getStartTime() != null) {
                    exportSession.sessionDate = dateFormat.format(session.getStartTime());
                    exportSession.startTime = timeFormat.format(session.getStartTime());
                }

                if (session.getEndTime() != null) {
                    exportSession.endTime = timeFormat.format(session.getEndTime());
                }

                if (session.getStartedAt() != null) {
                    exportSession.startedAt = timeFormat.format(session.getStartedAt());
                }

                if (session.getEndedAt() != null) {
                    exportSession.endedAt = timeFormat.format(session.getEndedAt());
                }
                Long studentId = null;
                Long teacherId = null;
                if (CollectionUtils.isNotEmpty(session.getAttendees())) {
                    for (UserSessionInfo attendee : session.getAttendees()) {
                        if (attendee.getRole().equals(Role.STUDENT)) {
                            List<Feedback> feedbacks = feedbackManager.get(null, null, attendee.getUserId().toString(),
                                    session.getId().toString(), null, null, null, null, null, false);
                            if (feedbacks != null && !feedbacks.isEmpty()) {
                                for (Feedback feedback : feedbacks) {
                                    if (attendee.getUserId() != null
                                            && feedback.getSenderId().equals(attendee.getUserId().toString())) {
                                        exportSession
                                                .setStudentFeedback(getCSVEscapedString(feedback.getPartnerMessage()));
                                        exportSession
                                                .setStudentComments(getCSVEscapedString(feedback.getSessionMessage()));
                                        exportSession.setSessionRatingStudent(feedback.getRating());
                                        String reasonString = null;
                                        if (feedback.getReason() != null && !feedback.getReason().isEmpty()) {
                                            reasonString = ListUtils.join(feedback.getReason(), ",");
                                        }
                                        exportSession.setStudentReason(getCSVEscapedString(reasonString));
                                    }
                                }
                            }
                            exportSession.setStudentState(attendee.getUserState().toString());
                            if (attendee.getBillingPeriod() != null) {
                                exportSession.sessionTime = attendee.getBillingPeriod();
                            }
                            exportSession.grade = attendee.getGrade();
                            exportSession.studentName = getCSVEscapedString(attendee.getFullName());
                            exportSession.studentEmail = attendee.getEmail();
                            exportSession.studentPhoneNumber = attendee.getContactNumber();
                            studentId = attendee.getUserId();
                        } else if (attendee.getRole().equals(Role.TEACHER)) {
                            if (attendee.getRatingInfo() != null) {
                                exportSession.setTeacherRating(attendee.getRatingInfo().getAvgRating());
                            }
                            List<Feedback> feedbacks = feedbackManager.get(null, null, attendee.getUserId().toString(),
                                    session.getId().toString(), null, null, null, null, null, false);
                            if (feedbacks != null && !feedbacks.isEmpty()) {
                                for (Feedback feedback : feedbacks) {
                                    if (attendee.getUserId() != null
                                            && feedback.getSenderId().equals(attendee.getUserId().toString())) {
                                        exportSession
                                                .setTeacherFeedback(getCSVEscapedString(feedback.getPartnerMessage()));
                                        exportSession
                                                .setTeacherComments(getCSVEscapedString(feedback.getSessionMessage()));
                                        exportSession.setSessionRatingTeacher(feedback.getRating());
                                        String reasonString = null;
                                        if (feedback.getReason() != null && !feedback.getReason().isEmpty()) {
                                            reasonString = ListUtils.join(feedback.getReason(), ",");
                                        }
                                        exportSession.setTeacherReason(getCSVEscapedString(reasonString));
                                        exportSession.setTeacherSessionCoverage(
                                                getCSVEscapedString(feedback.getSessionCoverage()));
                                        String studentPerformanceString = null;
                                        if (feedback.getStudentPerformance() != null
                                                && !feedback.getStudentPerformance().isEmpty()) {
                                            studentPerformanceString = ListUtils.join(feedback.getStudentPerformance(),
                                                    ",");
                                        }
                                        exportSession.setTeacherStudentPerformance(
                                                getCSVEscapedString(studentPerformanceString));
                                        exportSession.setTeacherHomeWork(getCSVEscapedString(feedback.getHomeWork()));
                                        exportSession.setTeacherNextSessionPlan(
                                                getCSVEscapedString(feedback.getNextSessionPlan()));
                                        exportSession
                                                .setTeacherRemark(getCSVEscapedString(feedback.getOptionalMessage()));
                                    }
                                }
                            }
                            exportSession.setTeacherState(attendee.getUserState().toString());
                            exportSession.teacherName = getCSVEscapedString(attendee.getFullName());
                            exportSession.teacherEmail = attendee.getEmail();
                            exportSession.teacherPhoneNumber = attendee.getContactNumber();
                            teacherId = attendee.getUserId();
                        }
                    }
                }
                if (session.getRemark() != null) {
                    exportSession.setRemarks(getCSVEscapedString(session.getRemark()));
                }

                if (studentId != null && studentId > 0 && teacherId != null) {
                    GetReviewRequest getReviewRequest = new GetReviewRequest();
                    getReviewRequest.setEntityType(EntityType.USER);
                    getReviewRequest.setEntityId(teacherId.toString());
                    getReviewRequest.setUserId(studentId);
                    getReviewRequest.setContextType(ContextType.SESSION);
                    getReviewRequest.setContextId(session.getId().toString());
                    review = ratingReviewManager.getReviewBasicInfo(getReviewRequest);
                    if (review != null) {
                        exportSession.setStudentReview(getCSVEscapedString(review.getReview()));
                        exportSession.setStudentReviewRating(review.getRating());
                        exportSession.setStudentReviewReason(getCSVEscapedString(review.getReasonString()));
                    }
                }

                exportSessions.add(exportSession);
                logger.info("Adding exportSession: " + exportSession);
            }
            logger.info("exportSessionsLength " + exportSessions.size());

            if (CollectionUtils.isNotEmpty(exportSessions)) {
                for (ExportUserSession exportSession : exportSessions) {
                    if (CollectionUtils.isNotEmpty(fields)) {
                        for (String fieldName : fields) {
                            Field field = ExportUserSessionListDetail.class.getField(fieldName);
                            data += (field.get(exportSession) != null ? field.get(exportSession) : " ") + ",";
                        }
                    }
                    data += "\n";
                }
            }
            start += SIZE;
        }
        logger.info("data: " + data);

        communicationManager.sendUserDataEmail(httpSessionData, data, ExportType.SESSION.toString());

        return res;
    }

    public void exportAvailability(AvailabilityTeacherExportReq availableTeachersReq, HttpSessionData httpSessionData)
            throws Exception {
        List<AvailabilitySlotPojo> reqSlots = availableTeachersReq.getSlots();
        List<SessionSlot> slots = new ArrayList<>();
        switch (availableTeachersReq.getExportType()) {
            case CONSOLIDATED_AVAILABILITY:
            case EXPORT_BY_INTERVAL:
                if (!CollectionUtils.isEmpty(reqSlots)) {
                    for (AvailabilitySlotPojo reqSlot : reqSlots) {
                        List<SessionSlot> repeatSlots = reqSlot.createSessionSlots();
                        if (!CollectionUtils.isEmpty(repeatSlots)) {
                            for (SessionSlot slot : repeatSlots) {
                                slots.addAll(slot.splitSlotsByInterval(DateTimeUtils.MILLIS_PER_MINUTE * 30));
                            }
                        }
                    }
                }
                break;
            case EXPORT_BY_SLOTS:
                if (!CollectionUtils.isEmpty(reqSlots)) {
                    for (AvailabilitySlotPojo reqSlot : reqSlots) {
                        slots.addAll(reqSlot.createSessionSlots());
                    }
                }
                break;
            default:
                return;
        }

        Map<String, AvailableTeacherExportInfo> infoMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(slots)) {
            Long startTime = SessionSlot.getStartTimeFromSlots(slots);
            Long endTime = SessionSlot.getEndTimeFromSlots(slots);

            User teacher = null;
            if (!StringUtils.isEmpty(availableTeachersReq.getTeacherEmailId())) {
                teacher = userManager.getUserByEmail(availableTeachersReq.getTeacherEmailId());
                availableTeachersReq.setTeacherId(String.valueOf(teacher.getId()));
            }
            List<TeacherAvailabilityData> teacherAvailabilityDataList = calendarManager.getTeacherAvailabilityExport(
                    availableTeachersReq.getBoardId(), endTime, startTime, availableTeachersReq.getGrade(),
                    availableTeachersReq.getTarget(), availableTeachersReq.getTeacherId());
            if (!CollectionUtils.isEmpty(teacherAvailabilityDataList)) {
                Map<Long, Board> boardMap = new HashMap<>();
                for (TeacherAvailabilityData teacherAvailabilityDataEntry : teacherAvailabilityDataList) {
                    if (StringUtils.isEmpty(teacherAvailabilityDataEntry.getUserId())) {
                        continue;
                    }

                    if (!StringUtils.isEmpty(availableTeachersReq.getTeacherId()) && teacher != null) {
                        teacherAvailabilityDataEntry.updateTeacherInfo(teacher);
                    }

                    try {
                        AvailableTeacherExportInfo infoEntry = createAvailableTeacherExportInfo(
                                teacherAvailabilityDataEntry, boardMap, slots);
                        logger.info("Infoentry added : " + infoEntry.toString());
                        infoMap.put(infoEntry.getTeacherId(), infoEntry);
                    } catch (Exception ex) {
                        logger.error("AvailableTeachersExportTaskError" + ex.getMessage(), ex);
                    }
                }
            }

            exportAvailableTeachers(httpSessionData, infoMap, slots, availableTeachersReq);
        } else {
            logger.info("ExportNoSlotsFound");
        }
    }

    public void exportAvailableTeachers(HttpSessionData httpSessionData,
            Map<String, AvailableTeacherExportInfo> infoMap, List<SessionSlot> slots,
            AvailabilityTeacherExportReq availabilityTeacherExportReq)
            throws AddressException, VException, IOException {
        List<String> fieldsList = new ArrayList<>();
        fieldsList.add("teacherId");
        fieldsList.add("emailId");
        fieldsList.add("Full Name");
        fieldsList.add("primarySubject");
        fieldsList.add("secondarySubject");
        fieldsList.add("grades");
        fieldsList.add("categories");
        fieldsList.add("startPrice");
        fieldsList.add("sessionHours");
        fieldsList.add("sessions");
        fieldsList.add("experience");
        fieldsList.add("latestEducation");
        fieldsList.add("rating");
        if (!CollectionUtils.isEmpty(slots)) {
            for (int i = 0; i < slots.size(); i++) {
                fieldsList.add(getSlotString(slots.get(i)));
            }
        }

        String data = StringUtils.join(fieldsList.iterator(), ",");
        data += "\n";

        List<String> availableTeachers = new ArrayList<>();
        long availableDuration = 0l;
        long bookedDuration = 0l;
        long unavailableDuration = 0l;

        if (!infoMap.isEmpty()) {
            for (Map.Entry<String, AvailableTeacherExportInfo> mapEntry : infoMap.entrySet()) {
                AvailableTeacherExportInfo entryInfo = mapEntry.getValue();
                List<String> entryList = new ArrayList<>();
                entryList.add(entryInfo.getTeacherId());
                entryList.add(entryInfo.getEmailId());
                entryList.add(entryInfo.getFullName());
                entryList.add(entryInfo.getPrimarySubject());
                entryList.add(entryInfo.getSecondarySubject());
                entryList.add(entryInfo.getGrades());
                entryList.add(CollectionUtils.isEmpty(entryInfo.getCategories()) ? ""
                        : AvailableTeacherExportInfo
                                .removeCommas(Arrays.toString(entryInfo.getCategories().toArray())));
                entryList.add(entryInfo.getStartPrice());
                entryList.add(String.valueOf(entryInfo.getSessionHours()));
                entryList.add(String.valueOf(entryInfo.getSessions()));
                entryList.add(String.valueOf(entryInfo.getExperience()));
                entryList.add(String.valueOf(entryInfo.getLatestEducation()));
                entryList.add(String.valueOf(entryInfo.getRating()));
                boolean teacherAvailable = true;
                for (int i = 0; i < slots.size(); i++) {
                    String availabilityString = entryInfo.fetchAvailableString(i);
                    long duration = slots.get(i).getEndTime() - slots.get(i).getStartTime();
                    if (duration < 0l) {
                        duration = 0l;
                    }
                    switch (availabilityString) {
                        case AvailableTeacherExportInfo.AVAILABLE_STRING:
                            availableDuration += duration;
                            break;
                        case AvailableTeacherExportInfo.AVAILABLE_BOOKED:
                            bookedDuration += duration;
                            teacherAvailable = false;
                            break;
                        case AvailableTeacherExportInfo.UNAVAILABLE_STRING:
                            unavailableDuration += duration;
                            teacherAvailable = false;
                            break;
                        default:
                            availabilityString = AvailableTeacherExportInfo.UNAVAILABLE_STRING;
                            teacherAvailable = false;
                    }
                    entryList.add(availabilityString);
                }

                data += StringUtils.join(entryList.iterator(), ",");
                data += "\n";
                if (teacherAvailable) {
                    availableTeachers.add(entryInfo.getTeacherId());
                }
            }
        }

        communicationManager.sendAvailableTeacherExportEmail(data, availableTeachers, httpSessionData,
                availableDuration, bookedDuration, unavailableDuration);
    }

    private String getSlotString(SessionSlot slot) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
        String startTimeSlot = sdf.format(new Date(slot.getStartTime()));
        sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
        String endTimeSlot = sdf.format(new Date(slot.getEndTime()));
        return startTimeSlot + "-" + endTimeSlot;
    }

    // TODO: Optimizations needs to be done
    public AvailableTeacherExportInfo createAvailableTeacherExportInfo(TeacherAvailabilityData entry,
            Map<Long, Board> boardMap, List<SessionSlot> slots) throws VException {
        AvailableTeacherExportInfo availableTeacherExportInfo = new AvailableTeacherExportInfo();

        if (entry == null) {
            return availableTeacherExportInfo;
        }

        availableTeacherExportInfo.setTeacherId(entry.getUserId());
        User teacher = userManager.getUserById(Long.parseLong(availableTeacherExportInfo.getTeacherId()));
        TeacherInfo teacherInfo = teacher.getTeacherInfo();
        availableTeacherExportInfo.setEmailId(teacher.getEmail());
        availableTeacherExportInfo.setFullName(AvailableTeacherExportInfo.removeCommas(teacher.getFullName()));
        if (teacherInfo != null) {
            availableTeacherExportInfo.setSessionHours(teacherInfo.getSessionHours());
            availableTeacherExportInfo.setSessions(teacherInfo.getSessions());
            availableTeacherExportInfo
                    .setExperience(AvailableTeacherExportInfo.removeCommas(teacherInfo.getExperience()));
            availableTeacherExportInfo
                    .setLatestEducation(AvailableTeacherExportInfo.removeCommas(teacherInfo.getLatestEducation()));
            availableTeacherExportInfo.setGrades(AvailableTeacherExportInfo.removeCommas(
                    teacherInfo.getGrades() == null ? "" : Arrays.toString(teacherInfo.getGrades().toArray())));
            Long primarySubjectId = teacherInfo.getPrimarySubject();
            if (primarySubjectId != null && primarySubjectId > 0l) {
                logger.info("Primary subject found");
                if (boardMap.containsKey(primarySubjectId)) {
                    logger.info("Primary subject found in map");
                    availableTeacherExportInfo.setPrimarySubject(boardMap.get(primarySubjectId).getSlug());
                } else {
                    Board primaryBoard = boardManager.getBoardById(primarySubjectId);
                    if (primaryBoard != null) {
                        logger.info("Primary subject found : " + primaryBoard.toString());
                        availableTeacherExportInfo.setPrimarySubject(primaryBoard.getSlug());
                        boardMap.put(primarySubjectId, primaryBoard);
                    }
                }
            }

            Long secondarySubjectId = teacherInfo.getPrimarySubject();
            if (secondarySubjectId != null && secondarySubjectId > 0l) {
                logger.info("secondary subject found");
                if (boardMap.containsKey(secondarySubjectId)) {
                    logger.info("Secondary subject found in map");
                    availableTeacherExportInfo.setSecondarySubject(boardMap.get(secondarySubjectId).getSlug());
                } else {
                    Board secondaryBoard = boardManager.getBoardById(secondarySubjectId);
                    if (secondaryBoard != null) {
                        logger.info("Secondary subject found : " + secondaryBoard.toString());
                        availableTeacherExportInfo.setSecondarySubject(secondaryBoard.getSlug());
                        boardMap.put(secondarySubjectId, secondaryBoard);
                    }
                }
            }

            availableTeacherExportInfo.setCategories(teacherInfo.getCategories());
            logger.info("Categories added");
        }

        // Populate the availability fields
        if (!com.vedantu.util.CollectionUtils.isEmpty(slots)) {
            logger.info("Adding the slots : " + slots.size());
            for (SessionSlot slot : slots) {
                availableTeacherExportInfo.getAvailability()
                        .add(entry.fetchAvailabilityString(slot.getStartTime(), slot.getEndTime()));
            }
        }
        logger.info("Created availabiltiy export info" + availableTeacherExportInfo.toString());
        return availableTeacherExportInfo;
    }

    public BasicRes exportCoursePlans(ExportCoursePlansReq req, HttpSessionData httpSessionData)
            throws BadRequestException, NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, AddressException, VException, IOException {

        req.verify();
        BasicRes res = new BasicRes();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");

        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");

        dateFormatWithTime.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        List<ExportCoursePlan> exportCoursePlans = new ArrayList<>();

        Integer start = 0;
        Integer size = 100;

        while (true) {
            req.setStart(start);
            req.setSize(size);

            List<CoursePlanInfo> coursePlanInfoList = coursePlanManager.getCoursePlanInfosByCreationTime(req, true);

            if (coursePlanInfoList == null || coursePlanInfoList.isEmpty()) {
                break;
            }
            List<String> coursePlanIds = new ArrayList<>();
            List<Long> studentIds = new ArrayList<>();
            Set<String> createdBys = new HashSet<>();

            for (CoursePlanInfo coursePlanInfo : coursePlanInfoList) {
                coursePlanIds.add(coursePlanInfo.getId());
                studentIds.add(coursePlanInfo.getStudentId());
                createdBys.add(coursePlanInfo.getCreatedBy());
            }

            //fetching createdby usersinfo
            Map<String, UserBasicInfo> createdBysMap = userUtils.getUserBasicInfosMap(createdBys, true);

            List<FirstExtTransaction> transactionPojoList = paymentManager.getFirstExtTransactions(studentIds);
            Map<Long, FirstExtTransaction> transactionMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(transactionPojoList)) {
                for (FirstExtTransaction transactionPojo : transactionPojoList) {
                    transactionMap.put(transactionPojo.getUserId(), transactionPojo);
                }
            }

            List<Orders> ordersList = paymentManager.getOrdersByDeliverableOrEntity(coursePlanIds);
            Map<String, Orders> registrationOrderMap = new HashMap<>();
            Map<String, Orders> finalOrderMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(ordersList)) {
                for (Orders orders : ordersList) {
                    if (PaymentStatus.PAID.equals(orders.getPaymentStatus())
                            || PaymentStatus.PARTIALLY_PAID.equals(orders.getPaymentStatus())
                            || PaymentStatus.PAYMENT_SUSPENDED.equals(orders.getPaymentStatus())) {
                        List<OrderedItem> items = orders.getItems();
                        if (CollectionUtils.isNotEmpty(items)) {
                            OrderedItem item = orders.getItems().get(0);
                            String key = item.getDeliverableEntityId();
                            if (StringUtils.isEmpty(key)) {
                                key = item.getEntityId();
                            }
                            if (com.vedantu.session.pojo.EntityType.OTO_COURSE_REGISTRATION.equals(item.getEntityType())
                                    || com.vedantu.session.pojo.EntityType.COURSE_PLAN_REGISTRATION
                                            .equals(item.getEntityType())) {
                                registrationOrderMap.put(key, orders);
                            } else if (com.vedantu.session.pojo.EntityType.COURSE_PLAN.equals(item.getEntityType())) {
                                finalOrderMap.put(key, orders);
                            }
                        }
                    }
                }
            }

            logger.info("registrationOrderMap" + registrationOrderMap);
            logger.info("finalOrderMap" + finalOrderMap);
            Map<String, InstalmentPaymentInfoRes> orderIdInstalmentPaymentInfoMap = new HashMap<>();
            List<String> orderIds = new ArrayList<>();
            for (Orders finalOrder : finalOrderMap.values()) {
                if (PaymentType.INSTALMENT.equals(finalOrder.getPaymentType())) {
                    orderIds.add(finalOrder.getId());
                }
            }
            if (CollectionUtils.isNotEmpty(orderIds)) {
                orderIdInstalmentPaymentInfoMap = paymentManager.getOrderIdInstalmentInfoMap(orderIds);
            }

            Map<String, ExportCoursePlan> coursePlanIdsMap = new HashMap<>();
            for (CoursePlanInfo coursePlanInfo : coursePlanInfoList) {
                try {
                    ExportCoursePlan exportCoursePlan = new ExportCoursePlan();
                    exportCoursePlan.setId(coursePlanInfo.getId());
                    exportCoursePlan.setCoursePlanTitle(coursePlanInfo.getTitle());
                    if (coursePlanInfo.getStudent() != null) {
                        exportCoursePlan.setEmail(coursePlanInfo.getStudent().getEmail());
                        exportCoursePlan.setStudentName(coursePlanInfo.getStudent().getFullName());
                        exportCoursePlan.setPhoneCode(coursePlanInfo.getStudent().getPhoneCode());
                        exportCoursePlan.setContactNumber(coursePlanInfo.getStudent().getContactNumber());
                    }
                    exportCoursePlan.setParentCourseId(coursePlanInfo.getParentCourseId());
                    if (coursePlanInfo.getGrade() != null) {
                        exportCoursePlan.setGrade(coursePlanInfo.getGrade().toString());
                    }
                    if (coursePlanInfo.getCreationTime() != null) {
                        exportCoursePlan
                                .setCoursePlanSendDate(dateFormatWithTime.format(coursePlanInfo.getCreationTime()));
                    }
                    if (coursePlanInfo.getTrialRegistrationFee() != null) {
                        exportCoursePlan.setRegistrationAmount(coursePlanInfo.getTrialRegistrationFee());
                    }
                    if (coursePlanInfo.getStartDate() != null) {
                        exportCoursePlan.setFirstSessionTime(dateFormatWithTime.format(coursePlanInfo.getStartDate()));
                    }
                    Long coursePlanEndTime = null;
                    if (coursePlanInfo.getRegularSessionSchedule() != null) {
                        coursePlanEndTime = coursePlanInfo.getRegularSessionSchedule().getEndDate();
                    }
                    if (coursePlanInfo.getTrialSessionSchedule() != null) {
                        coursePlanEndTime = coursePlanInfo.getTrialSessionSchedule().getEndDate();
                    }
                    if (coursePlanEndTime != null) {
                        exportCoursePlan.setEndTime(dateFormat.format(coursePlanEndTime));
                    }
                    if (coursePlanInfo.getTeacher() != null) {
                        exportCoursePlan.setTeacherName(coursePlanInfo.getTeacher().getFullName());
                        exportCoursePlan.setTeacherEmail(coursePlanInfo.getTeacher().getEmail());
                    }
                    exportCoursePlan.setState(coursePlanInfo.getState().name());
                    if (coursePlanInfo.getEndType() != null) {
                        exportCoursePlan.setEndType(coursePlanInfo.getEndType().toString());
                    }
                    int totalPrice = 0;
                    int trialRegistrationFee = 0;
                    if (coursePlanInfo.getPrice() != null) {
                        totalPrice = coursePlanInfo.getPrice();
                    }
                    if (coursePlanInfo.getTrialRegistrationFee() != null) {
                        trialRegistrationFee = coursePlanInfo.getTrialRegistrationFee();
                    }
                    exportCoursePlan.setBulkAmount(totalPrice - trialRegistrationFee);
                    Long dueDateLong = coursePlanManager.getTrialDueDate(coursePlanInfo);
                    String dueDate = "";
                    if (dueDateLong != null) {
                        dueDate = dateFormatWithTime.format(coursePlanManager.getTrialDueDate(coursePlanInfo));
                    }
                    exportCoursePlan.setBulkPaymentDate(dueDate);
                    exportCoursePlan.setFirstInstalmentPaymentDate(dueDate);
                    exportCoursePlan.setEntityType("COURSE_PLAN");
                    Integer firstInstalmentAmount = 0;
                    List<BaseInstalmentInfo> instalmentInfoList = coursePlanInfo.getInstalmentsInfo();
                    if (instalmentInfoList != null && !instalmentInfoList.isEmpty()) {
                        Collections.sort(instalmentInfoList, new Comparator<BaseInstalmentInfo>() {
                            @Override
                            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                                return o1.getDueTime().compareTo(o2.getDueTime());
                            }
                        });
                        firstInstalmentAmount = instalmentInfoList.get(0).getAmount();
                    }
                    exportCoursePlan.setFirstInstalmentAmount(firstInstalmentAmount);
                    // if(!CoursePlanEnums.CoursePlanState.ENROLLED.equals(coursePlanInfo.getState())
                    // &&
                    // !CoursePlanEnums.CoursePlanState.FORFEITED.equals(coursePlanInfo.getState()))
                    // {
                    coursePlanIdsMap.put(coursePlanInfo.getId(), exportCoursePlan);
                    // }

                    Orders registrationOrder = registrationOrderMap.get(coursePlanInfo.getId());
                    if (registrationOrder != null) {
                        exportCoursePlan.setRegistrationDate(dateFormat.format(registrationOrder.getCreationTime()));
                    }

                    Orders finalOrder = finalOrderMap.get(coursePlanInfo.getId());
                    if (finalOrder != null) {
                        if (PaymentType.INSTALMENT.equals(finalOrder.getPaymentType())) {
                            exportCoursePlan
                                    .setInstalmentPaidDate(dateFormatWithTime.format(finalOrder.getCreationTime()));
                            InstalmentPaymentInfoRes instalmentPaymentInfoRes = orderIdInstalmentPaymentInfoMap
                                    .get(finalOrder.getId());
                            if (instalmentPaymentInfoRes != null) {
                                exportCoursePlan.setNoOfPaidInstalments(instalmentPaymentInfoRes.getNoOfPaid());
                                if (instalmentPaymentInfoRes.getDueDate() != null) {
                                    exportCoursePlan.setInstalmentDueDate(
                                            dateFormatWithTime.format(instalmentPaymentInfoRes.getDueDate()));
                                }
                                if (instalmentPaymentInfoRes.getAmount() != null) {
                                    exportCoursePlan.setInstalmentAmount(instalmentPaymentInfoRes.getAmount() / 100);
                                }
                                if (instalmentPaymentInfoRes.getLastInstalmentDate() != null) {
                                    exportCoursePlan.setLastInstalmentDate(dateFormatWithTime
                                            .format(instalmentPaymentInfoRes.getLastInstalmentDate()));
                                }
                            }
                        } else if (PaymentType.BULK.equals(finalOrder.getPaymentType())) {
                            exportCoursePlan.setBulkPaidDate(dateFormatWithTime.format(finalOrder.getCreationTime()));
                        }
                    }

                    FirstExtTransaction firstTransaction = transactionMap.get(coursePlanInfo.getStudentId());
                    if (firstTransaction != null) {
                        exportCoursePlan
                                .setExtTransactionDate(dateFormatWithTime.format(firstTransaction.getCreationTime()));
                    }

                    //adding creationTime and createdBy
                    if (coursePlanInfo.getCreatedBy() != null
                            && createdBysMap.get(coursePlanInfo.getCreatedBy()) != null) {
                        exportCoursePlan.setCreatedBy(createdBysMap.get(coursePlanInfo.getCreatedBy()).getFullName());
                    }

                    if (coursePlanInfo.getCreationTime() != null) {
                        exportCoursePlan.setCreationTime(dateFormatWithTime.format(coursePlanInfo.getCreationTime()));
                    }

                    exportCoursePlans.add(exportCoursePlan);
                } catch (Exception ex) {
                    logger.error("Exception ", ex);
                }
            }
            if (!coursePlanIdsMap.isEmpty()) {
                Map<String, TrialSessionInfos> trialSessionInfosMap = sessionManager
                        .getTrialSessionInfos(coursePlanIdsMap.keySet());
                for (Map.Entry<String, TrialSessionInfos> entry : trialSessionInfosMap.entrySet()) {
                    if (entry.getValue() != null) {
                        coursePlanIdsMap.get(entry.getKey())
                                .setTrialSessionsScheduled(entry.getValue().getTrialSessionsScheduled());
                        coursePlanIdsMap.get(entry.getKey())
                                .setTrialSessionsCancelled(entry.getValue().getTrialSessionsCancelled());
                        coursePlanIdsMap.get(entry.getKey())
                                .setTrialSessionsExpired(entry.getValue().getTrialSessionsExpired());
                        coursePlanIdsMap.get(entry.getKey())
                                .setTrialSessionsForfeited(entry.getValue().getTrialSessionsForfeited());
                        coursePlanIdsMap.get(entry.getKey())
                                .setTrialSessionsEnded(entry.getValue().getTrialSessionsEnded());
                        coursePlanIdsMap.get(entry.getKey())
                                .setTrialSessionsTotal(entry.getValue().getTrialSessionsTotal());
                    }
                }
            }
            start += size;
        }

        start = 0;
        while (true) {
            GetEnrollmentsReq getEnrollmentsReq = new GetEnrollmentsReq();
            getEnrollmentsReq.setStart(start);
            getEnrollmentsReq.setSize(size);
            getEnrollmentsReq.setFillBatchCourseInfos(true);
            getEnrollmentsReq.setFromTime(req.getFromTime());
            getEnrollmentsReq.setTillTime(req.getTillTime());
            getEnrollmentsReq.setRole(Role.STUDENT);
            String qString = WebUtils.INSTANCE.createQueryStringOfObject(getEnrollmentsReq);
            String getEnrollmentsUrl = subscriptionEndPoint + "enroll/enrollments?" + qString;
            ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info(jsonString);
            Type listType = new TypeToken<List<EnrollmentPojo>>() {
            }.getType();
            List<EnrollmentPojo> enrollments = new Gson().fromJson(jsonString, listType);

            if (enrollments == null || enrollments.isEmpty()) {
                break;
            }

            Set<String> batchIds = new HashSet<>();
            List<String> enrollmentIds = new ArrayList<>();
            List<Long> studentIds = new ArrayList<>();
            for (EnrollmentPojo enrollmentPojo : enrollments) {
                batchIds.add(enrollmentPojo.getBatchId());
                enrollmentIds.add(enrollmentPojo.getId());
                studentIds.add(Long.parseLong(enrollmentPojo.getUserId()));
            }

            List<FirstExtTransaction> transactionPojoList = paymentManager.getFirstExtTransactions(studentIds);
            Map<Long, FirstExtTransaction> transactionMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(transactionPojoList)) {
                for (FirstExtTransaction transactionPojo : transactionPojoList) {
                    transactionMap.put(transactionPojo.getUserId(), transactionPojo);
                }
            }

            List<BaseInstalment> baseInstalments = paymentManager.getBaseInstalments(InstalmentPurchaseEntity.BATCH,
                    new ArrayList<>(batchIds));
            Map<String, BaseInstalmentInfo> dueDates = new HashMap<>();
            if (ArrayUtils.isNotEmpty(baseInstalments)) {
                for (BaseInstalment baseInstalment : baseInstalments) {
                    dueDates.put(baseInstalment.getPurchaseEntityId(), baseInstalment.getFirstDueBaseInstalmentInfo());
                }
            }

            List<Orders> ordersList = paymentManager.getOrdersByDeliverableOrEntity(enrollmentIds);
            List<Orders> ordersListByDeliverableIds = paymentManager.getOrdersByDeliverableIds(enrollmentIds);
            ordersList.addAll(ordersListByDeliverableIds);
            Map<String, Orders> registrationOrderMap = new HashMap<>();
            Map<String, Orders> finalOrderMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(ordersList)) {
                for (Orders order : ordersList) {
                    if (PaymentStatus.PAID.equals(order.getPaymentStatus())
                            || PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus())
                            || PaymentStatus.PAYMENT_SUSPENDED.equals(order.getPaymentStatus())) {
                        List<OrderedItem> items = order.getItems();
                        if (CollectionUtils.isNotEmpty(items)) {
                            OrderedItem item = order.getItems().get(0);
                            String key = item.getDeliverableEntityId();
                            if (StringUtils.isEmpty(key)) {
                                key = item.getEntityId();
                            }
                            if (com.vedantu.session.pojo.EntityType.OTF_COURSE_REGISTRATION.equals(item.getEntityType())
                                    || com.vedantu.session.pojo.EntityType.OTF_BATCH_REGISTRATION
                                            .equals(item.getEntityType())) {
                                registrationOrderMap.put(key, order);
                            } else if (com.vedantu.session.pojo.EntityType.OTF.equals(item.getEntityType())) {
                                finalOrderMap.put(key, order);
                            } else if (com.vedantu.session.pojo.EntityType.BUNDLE_PACKAGE.equals(item.getEntityType())) {
                                if (ArrayUtils.isNotEmpty(order.getPurchasingEntities())) {
                                    for (PurchasingEntity purchasingEntity : order.getPurchasingEntities()) {
                                        if (StringUtils.isNotEmpty(purchasingEntity.getDeliverableId())) {
                                            finalOrderMap.put(purchasingEntity.getDeliverableId(), order);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            logger.info("registrationOrderMap" + registrationOrderMap);
            logger.info("finalOrderMap" + finalOrderMap);
            Map<String, InstalmentPaymentInfoRes> orderIdInstalmentPaymentInfoMap = new HashMap<>();
            List<String> orderIds = new ArrayList<>();
            for (Orders finalOrder : finalOrderMap.values()) {
                if (PaymentType.INSTALMENT.equals(finalOrder.getPaymentType())) {
                    orderIds.add(finalOrder.getId());
                }
            }
            if (CollectionUtils.isNotEmpty(orderIds)) {
                orderIdInstalmentPaymentInfoMap = paymentManager.getOrderIdInstalmentInfoMap(orderIds);
            }

            // Map<String, ExportCoursePlan> coursePlanIdsMap = new HashMap<>();
            for (EnrollmentPojo enrollmentPojo : enrollments) {
                try {
                    ExportCoursePlan exportCoursePlan = new ExportCoursePlan();
                    exportCoursePlan.setId(enrollmentPojo.getEnrollmentId());
                    if (enrollmentPojo.getCourse() != null) {
                        String teacherName = "";
                        String teacherEmail = "";
                        exportCoursePlan.setCoursePlanTitle(enrollmentPojo.getCourse().getTitle());
                        if (enrollmentPojo.getCourse().getRegistrationFee() != null) {
                            exportCoursePlan.setRegistrationAmount(enrollmentPojo.getCourse().getRegistrationFee());
                        }
                        if (enrollmentPojo.getCourse().getTeachers() != null
                                && !enrollmentPojo.getCourse().getTeachers().isEmpty()) {
                            UserBasicInfo teacherInfo = enrollmentPojo.getCourse().getTeachers().iterator().next();
                            teacherName = teacherInfo.getFullName();
                            teacherEmail = teacherInfo.getEmail();

                        }
                        exportCoursePlan.setTeacherName(teacherName);
                        exportCoursePlan.setTeacherEmail(teacherEmail);
                    }
                    if (enrollmentPojo.getUser() != null) {
                        exportCoursePlan.setEmail(enrollmentPojo.getUser().getEmail());
                        exportCoursePlan.setGrade(enrollmentPojo.getUser().getGrade());
                        exportCoursePlan.setStudentName(enrollmentPojo.getUser().getFullName());
                        exportCoursePlan.setPhoneCode(enrollmentPojo.getUser().getPhoneCode());
                        exportCoursePlan.setContactNumber(enrollmentPojo.getUser().getContactNumber());
                    }
                    if (enrollmentPojo.getCreationTime() != null) {
                        exportCoursePlan.setCoursePlanSendDate(dateFormat.format(enrollmentPojo.getCreationTime()));
                    }

                    if (enrollmentPojo.getState() != null) {
                        exportCoursePlan.setState(enrollmentPojo.getState().name());
                    }
                    exportCoursePlan.setBatchId(enrollmentPojo.getBatchId());
                    if (enrollmentPojo.getStatus() != null) {
                        exportCoursePlan.setEnrolmentStatus(enrollmentPojo.getStatus().name());
                    }
                    if (enrollmentPojo.getBatch() != null) {
                        exportCoursePlan.setBulkAmount(enrollmentPojo.getBatch().getPurchasePrice());
                        if (enrollmentPojo.getBatch().getStartTime() != null) {
                            exportCoursePlan.setFirstSessionTime(
                                    dateFormatWithTime.format(enrollmentPojo.getBatch().getStartTime()));
                        }
                        if (enrollmentPojo.getBatch().getEndTime() != null) {
                            exportCoursePlan
                                    .setEndTime(dateFormatWithTime.format(enrollmentPojo.getBatch().getEndTime()));
                        }
                    }
                    exportCoursePlan.setEntityType("OTF_ENROLLMENT");
                    Long dueTime;
                    String batchId = enrollmentPojo.getBatchId();
                    if (dueDates.get(batchId) != null && dueDates.get(batchId).getDueTime() > 0) {
                        dueTime = dueDates.get(batchId).getDueTime();
                    } else {
                        logger.info("no base instalments found for " + batchId);
                        dueTime = enrollmentPojo.getCreationTime() + (7 * DateTimeUtils.MILLIS_PER_DAY);
                    }
                    int instalmentAmount = 0;
                    if (dueDates.get(batchId) != null) {
                        instalmentAmount = dueDates.get(batchId).getAmount();
                    }
                    exportCoursePlan.setBulkPaymentDate(dateFormatWithTime.format(dueTime));
                    exportCoursePlan.setFirstInstalmentPaymentDate(dateFormatWithTime.format(dueTime));
                    exportCoursePlan.setFirstInstalmentAmount(instalmentAmount);

                    FirstExtTransaction firstTransaction = transactionMap
                            .get(Long.parseLong(enrollmentPojo.getUserId()));
                    if (firstTransaction != null) {
                        exportCoursePlan
                                .setExtTransactionDate(dateFormatWithTime.format(firstTransaction.getCreationTime()));
                    }

                    Orders registrationOrder = registrationOrderMap.get(enrollmentPojo.getId());
                    if (registrationOrder != null) {
                        exportCoursePlan
                                .setRegistrationDate(dateFormatWithTime.format(registrationOrder.getCreationTime()));
                    }

                    Orders finalOrder = finalOrderMap.get(enrollmentPojo.getId());
                    //this finalOrder in case of bundle package will consist of amt for all 3 enrollments
                    //since here only payment date is needed, the order entity is replicated across 
                    //all enrollments in the bundle package. Be careful when dealing with amounts

                    if (finalOrder != null) {
                        if (PaymentType.INSTALMENT.equals(finalOrder.getPaymentType())) {
                            exportCoursePlan.setInstalmentPaidDate(dateFormat.format(finalOrder.getCreationTime()));
                            InstalmentPaymentInfoRes instalmentPaymentInfoRes = orderIdInstalmentPaymentInfoMap
                                    .get(finalOrder.getId());
                            if (instalmentPaymentInfoRes != null) {
                                exportCoursePlan.setNoOfPaidInstalments(instalmentPaymentInfoRes.getNoOfPaid());
                                if (instalmentPaymentInfoRes.getDueDate() != null) {
                                    exportCoursePlan.setInstalmentDueDate(
                                            dateFormatWithTime.format(instalmentPaymentInfoRes.getDueDate()));
                                }
                                if (instalmentPaymentInfoRes.getAmount() != null) {
                                    exportCoursePlan.setInstalmentAmount(instalmentPaymentInfoRes.getAmount() / 100);
                                }
                                if (instalmentPaymentInfoRes.getLastInstalmentDate() != null) {
                                    exportCoursePlan.setLastInstalmentDate(dateFormatWithTime
                                            .format(instalmentPaymentInfoRes.getLastInstalmentDate()));
                                }
                            }
                        } else if (PaymentType.BULK.equals(finalOrder.getPaymentType())) {
                            exportCoursePlan.setBulkPaidDate(dateFormat.format(finalOrder.getCreationTime()));
                        }
                    }

                    exportCoursePlans.add(exportCoursePlan);
                } catch (Exception ex) {
                    logger.error("Exception", ex);
                }
            }
            start += size;
        }

        StringWriter stringWriter = new StringWriter();
        ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
        String[] fields = {"email", "grade", "id", "coursePlanTitle", "coursePlanSendDate", "parentCourseId",
            "registrationDate", "registrationAmount", "firstSessionTime", "trialSessionsScheduled",
            "trialSessionsCancelled", "trialSessionsExpired", "trialSessionsEnded", "trialSessionsTotal",
            "firstInstalmentAmount", "firstInstalmentPaymentDate", "bulkAmount", "bulkPaymentDate", "teacherName",
            "teacherEmail", "state", "entityType", "enrolmentStatus", "instalmentPaidDate", "bulkPaidDate",
            "endType", "extTransactionDate", "noOfPaidInstalments", "instalmentDueDate", "instalmentAmount",
            "batchId", "studentName", "phoneCode", "contactNumber", "endTime", "lastInstalmentDate", "createdBy", "creationTime"};
        csvWriter.configureBeanMapping(ExportCoursePlan.class, fields);
        csvWriter.writeHeader(fields);
        for (ExportCoursePlan exportCoursePlan : exportCoursePlans) {
            try {
                csvWriter.write(exportCoursePlan);
            } catch (Exception ex) {
                logger.error("Exception: " + exportCoursePlan, ex);
            }
        }
        csvWriter.close();
        String data = stringWriter.toString();

        communicationManager.sendUserDataEmail(httpSessionData, data, ExportType.COURSE_PLANS.toString());

        return res;
    }

    public void exportOTFDashboard(HttpSessionData httpSessionData) throws IOException, AddressException, VException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        // scheduled tos
        Integer start = 0;
        Integer size = 200;
        boolean process = true;
        int maxInstCount = 0;
        List<GetOtfDashboardRes> dashboards = new ArrayList<>();
        while (process) {
            GetOTFDashboardReq getOTFDashboardReq = new GetOTFDashboardReq();
            getOTFDashboardReq.setStart(start);
            getOTFDashboardReq.setSize(size);
            try {
                List<GetOtfDashboardRes> _dashboards = dashboardManager.getOTFDashboard(getOTFDashboardReq);
                if (ArrayUtils.isEmpty(_dashboards)) {
                    process = false;
                } else {
                    for (GetOtfDashboardRes d : _dashboards) {
                        if (ArrayUtils.isNotEmpty(d.getOtfBasicInstalmentInfos())
                                && d.getOtfBasicInstalmentInfos().size() > maxInstCount) {
                            maxInstCount = d.getOtfBasicInstalmentInfos().size();
                        }
                    }
                    dashboards.addAll(_dashboards);
                }
            } catch (Exception e) {
                process = false;
                logger.error("error in fetching dashboards " + e.getMessage());
            }
            start += size;
        }

        String[] firstFields = {"Batch Id", "Course Id", "Course Title", "Batch Start Date", "Enrollment Id",
            "Student Email Id", "Name", "Phone No", "Status", "State"};
        String[] attendenceFields = {"Subject", "Teacher", "Total Sessions", "Attended", "% Attendence",
            "Last 5 Sessions Attendence"};
        String[] contentFields = {"Content Shared", "Attempted", "Evaluated"};
        String[] amountFields = {"RegistrationFee", "Reg Fee Paid Date", "PaymentType", "Amount Paid", "Paid Date"};

        List<String> fields = new ArrayList<>();
        fields.addAll(Arrays.asList(firstFields));
        fields.addAll(Arrays.asList(attendenceFields));
        fields.addAll(Arrays.asList(contentFields));
        fields.addAll(Arrays.asList(amountFields));

        for (int k = 0; k < maxInstCount; k++) {
            fields.add("Instalment " + (k + 1) + " Date");
            fields.add("Instalment " + (k + 1) + " Amount");
            fields.add("Instalment " + (k + 1) + " Status");
        }

        String data = StringUtils.join(fields.iterator(), ",");
        data += "\n";

        for (GetOtfDashboardRes dashboard : dashboards) {

            StringBuilder sb = new StringBuilder();
            sb.append(dashboard.getBatchId());
            sb.append(CSV_DELIMITER);
            sb.append(dashboard.getCourseId());
            sb.append(CSV_DELIMITER);
            sb.append(dashboard.getCourseTitle());
            sb.append(CSV_DELIMITER);
            sb.append(dashboard.getBatchStartDate());
            sb.append(CSV_DELIMITER);
            sb.append(dashboard.getEnrollmentId());
            sb.append(CSV_DELIMITER);
            if (dashboard.getUser() != null && dashboard.getUser().getEmail() != null) {
                sb.append(dashboard.getUser().getEmail());
            }
            sb.append(CSV_DELIMITER);
            if (dashboard.getUser() != null && dashboard.getUser().getFullName() != null) {
                sb.append(dashboard.getUser().getFullName());
            }
            sb.append(CSV_DELIMITER);
            if (dashboard.getUser() != null && dashboard.getUser().getContactNumber() != null) {
                if (dashboard.getUser().getPhoneCode() != null) {
                    sb.append(dashboard.getUser().getPhoneCode());
                    sb.append(" ");
                }
                sb.append(dashboard.getUser().getContactNumber());
            }
            sb.append(CSV_DELIMITER);
            sb.append(dashboard.getStatus());
            sb.append(CSV_DELIMITER);
            sb.append(dashboard.getState());
            sb.append(CSV_DELIMITER);

            LinkedHashMap<String, OTFAttendance> attenceMap = new LinkedHashMap<>();
            Map<String, StudentContentPojo> contentMap = new HashMap<>();
            LinkedHashSet<String> subjects = new LinkedHashSet<>();
            if (ArrayUtils.isNotEmpty(dashboard.getOtfAttendances())) {
                for (OTFAttendance attendance : dashboard.getOtfAttendances()) {
                    if (StringUtils.isNotEmpty(attendance.getSubject())) {
                        subjects.add(attendance.getSubject().toLowerCase());
                        attenceMap.put(attendance.getSubject().toLowerCase(), attendance);
                    }
                }
            }
            if (ArrayUtils.isNotEmpty(dashboard.getContents())) {
                for (StudentContentPojo contentPojo : dashboard.getContents()) {
                    if (StringUtils.isNotEmpty(contentPojo.getSubject())) {
                        subjects.add(contentPojo.getSubject().toLowerCase());
                        contentMap.put(contentPojo.getSubject().toLowerCase(), contentPojo);
                    }
                }
            }

            List<StringBuilder> builders = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(subjects)) {
                int l = 0;
                for (String subject : subjects) {
                    if (l != 0) {
                        StringBuilder builder = new StringBuilder();
                        builders.add(builder);
                        for (int f = 0; f < firstFields.length; f++) {
                            builder.append(CSV_DELIMITER);
                        }
                        fillAttendanceInfo(attenceMap.get(subject), builder, attendenceFields.length);
                        fillContentInfo(contentMap.get(subject), builder, contentFields.length);
                    } else {
                        fillAttendanceInfo(attenceMap.get(subject), sb, attendenceFields.length);
                        fillContentInfo(contentMap.get(subject), sb, contentFields.length);
                    }
                    l++;
                }
            } else {
                for (int f = 0; f < (attendenceFields.length + contentFields.length); f++) {
                    sb.append(CSV_DELIMITER);
                }
            }

            if (dashboard.getRegistrationAmount() != null) {
                sb.append(dashboard.getRegistrationAmount() / 100);
            }
            sb.append(CSV_DELIMITER);
            if (dashboard.getRegistrationCreationTime() != null) {
                sb.append(dateFormat.format(new Date(dashboard.getRegistrationCreationTime())));
            }
            sb.append(CSV_DELIMITER);

            if (ArrayUtils.isNotEmpty(dashboard.getOtfBasicInstalmentInfos())) {
                if (PaymentType.BULK.equals(dashboard.getOtfBasicInstalmentInfos().get(0).getPaymentType())) {
                    DashBoardInstalmentInfo instalmentInfo = dashboard.getOtfBasicInstalmentInfos().get(0);
                    sb.append(PaymentType.BULK.name());
                    sb.append(CSV_DELIMITER);
                    if (instalmentInfo.getTotalAmount() != null) {
                        sb.append(instalmentInfo.getTotalAmount() / 100);
                    }
                    sb.append(CSV_DELIMITER);
                    if (instalmentInfo.getPaidTime() != null) {
                        sb.append(dateFormat.format(new Date(instalmentInfo.getPaidTime())));
                    }
                    sb.append(CSV_DELIMITER);

                } else {
                    sb.append(CSV_DELIMITER);
                    sb.append(CSV_DELIMITER);
                    sb.append(CSV_DELIMITER);
                }
                for (DashBoardInstalmentInfo instalment : dashboard.getOtfBasicInstalmentInfos()) {
                    if (instalment.getDueTime() != null) {
                        sb.append(dateFormat.format(new Date(instalment.getDueTime())));
                    }
                    sb.append(CSV_DELIMITER);
                    if (instalment.getTotalAmount() != null) {
                        sb.append(instalment.getTotalAmount() / 100);
                    }
                    sb.append(CSV_DELIMITER);
                    if (instalment.getPaymentStatus() != null) {
                        sb.append(instalment.getPaymentStatus().name());
                    }
                    sb.append(CSV_DELIMITER);
                }
            }

            data += sb.toString() + "\n";
            for (StringBuilder builder : builders) {
                data += builder.toString() + "\n";
            }
        }

        String fileName = "OTF Dashboard";
        communicationManager.sendUserDataEmail(httpSessionData, data, fileName);
    }

    private StringBuilder fillAttendanceInfo(OTFAttendance attendance, StringBuilder sb, int attendenceFieldCount) {
        if (attendance != null) {
            if (attendance.getSubject() != null) {
                sb.append(attendance.getSubject());
            }
            sb.append(CSV_DELIMITER);
            if (attendance.getTeacher() != null) {
                sb.append(attendance.getTeacher().getFullName());
            }
            sb.append(CSV_DELIMITER);
            if (attendance.getTotal() != null) {
                sb.append(attendance.getTotal());
            }
            sb.append(CSV_DELIMITER);
            if (attendance.getAttended() != null) {
                sb.append(attendance.getAttended());
            }
            sb.append(CSV_DELIMITER);
            if (attendance.getTotal() != null && attendance.getAttended() != null && attendance.getTotal() > 0) {
                sb.append((attendance.getAttended() * 100 / attendance.getTotal()));
            }
            sb.append(CSV_DELIMITER);
            if (attendance.getLastFive() != null) {
//				int divideBy = 5;
//				if (attendance.getTotal() < 5) {
//					divideBy = attendance.getTotal();
//				}
//				if (divideBy > 0) {
                sb.append((attendance.getLastFive()));
//				}
            }
            sb.append(CSV_DELIMITER);
        } else {
            for (int f = 0; f < attendenceFieldCount; f++) {
                sb.append(CSV_DELIMITER);
            }
        }
        return sb;
    }

    private StringBuilder fillContentInfo(StudentContentPojo contentPojo, StringBuilder sb, int contentFieldCount) {
        if (contentPojo != null) {
            if (contentPojo.getShared() != null) {
                sb.append(contentPojo.getShared());
            }
            sb.append(CSV_DELIMITER);
            if (contentPojo.getAttempted() != null) {
                sb.append(contentPojo.getAttempted());
            }
            sb.append(CSV_DELIMITER);
            if (contentPojo.getEvaluated() != null) {
                sb.append(contentPojo.getEvaluated());
            }
            sb.append(CSV_DELIMITER);
        } else {
            for (int f = 0; f < contentFieldCount; f++) {
                sb.append(CSV_DELIMITER);
            }
        }
        return sb;
    }

    public void exportTeacherDashboard(HttpSessionData httpSessionData)
            throws IOException, AddressException, VException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        List<GetTeacherDashboardResp> teacherDashboards = new ArrayList<>();
        List<TeacherListWithStatusRes> teacherIdsRes = dashboardManager.getTeachersForDashboard();
        List<Long> teacherIds = new ArrayList<>();
        int counter = 0;
        if (ArrayUtils.isNotEmpty(teacherIdsRes)) {
            for (TeacherListWithStatusRes teacherRes : teacherIdsRes) {
                teacherIds.add(teacherRes.getTeacherId());
                counter++;
                if (counter >= 20) {
                    counter = 0;
                    GetTeacherDashboardReq req = new GetTeacherDashboardReq();
                    req.setTeacherIds(teacherIds);
                    List<GetTeacherDashboardResp> teacherDashboardsTemp = dashboardManager.getTeacherDashboard(req);
                    if (ArrayUtils.isNotEmpty(teacherDashboardsTemp)) {
                        teacherDashboards.addAll(dashboardManager.getTeacherDashboard(req));
                    }
                    teacherIds = new ArrayList<>();
                }
            }

            List<ExportTeacherDashboard> exportTeacherDashboards = new ArrayList<>();
            for (GetTeacherDashboardResp resp : teacherDashboards) {
                ExportTeacherDashboard coursePlan = new ExportTeacherDashboard();
                ExportTeacherDashboard otf = new ExportTeacherDashboard();
                coursePlan.setTeacherId(resp.getTeacherId());
                if (resp.getTeacher() != null) {
                    coursePlan.setTeacherName(resp.getTeacher().getFullName());
                    coursePlan.setEmailId(resp.getTeacher().getEmail());
                    coursePlan.setPhoneCode(resp.getTeacher().getPhoneCode());
                    coursePlan.setContactNumber(resp.getTeacher().getContactNumber());
                }
                if (resp.getPrimarySubject() != null) {
                    coursePlan.setPrimarySubject(resp.getPrimarySubject());
                }
                if (resp.getCoursePlan() != null) {
                    coursePlan.setEntityType(resp.getCoursePlan().getEntityType().toString());
                    coursePlan.setSessionsBooked(resp.getCoursePlan().getBooked());
                    coursePlan.setCancelledByTeacher(resp.getCoursePlan().getCancelledByTeacher());
                    coursePlan.setCancelledByStudent(resp.getCoursePlan().getCancelledByStudent());
                    coursePlan.setTotalCancelled(resp.getCoursePlan().getCancelled());
                    coursePlan.setSessionsDone(resp.getCoursePlan().getSessionEnded());
                    coursePlan.setRescheduled(resp.getCoursePlan().getRescheduled());
                    coursePlan.setSessionsLateJoined(resp.getCoursePlan().getLateJoined());

                    coursePlan.setContentShared(resp.getCoursePlan().getContentShared());
                    coursePlan.setContentAttempted(resp.getCoursePlan().getContentAttempted());
                    coursePlan.setContentEvaluated(resp.getCoursePlan().getContentEvaluated());

                    coursePlan.setPlanShared(resp.getCoursePlan().getNewCourse());
                    coursePlan.setPlanAcceptedAndPaid(resp.getCoursePlan().getEnrolled());
                    coursePlan.setPlanEnded(resp.getCoursePlan().getCourseEnded());
                } else {
                    coursePlan.setEntityType("COURSE_PLAN");
                }
                coursePlan.setActiveStudents(resp.getCoursePlanActiveStudents());
                coursePlan.setChurn(resp.getCoursePlanChurn());

                if (resp.getOtf() != null) {
                    otf.setEntityType(resp.getOtf().getEntityType().toString());
                    otf.setSessionsBooked(resp.getOtf().getBooked());
                    otf.setCancelledByTeacher(resp.getOtf().getCancelledByTeacher());
                    otf.setCancelledByStudent(resp.getOtf().getCancelledByStudent());
                    otf.setTotalCancelled(resp.getOtf().getCancelled());
                    otf.setSessionsDone(resp.getOtf().getSessionEnded());
                    otf.setRescheduled(resp.getOtf().getRescheduled());
                    otf.setSessionsLateJoined(resp.getOtf().getLateJoined());

                    otf.setContentShared(resp.getOtf().getContentShared());
                    otf.setContentAttempted(resp.getOtf().getContentAttempted());
                    otf.setContentEvaluated(resp.getOtf().getContentEvaluated());

                    otf.setPlanShared(resp.getOtf().getNewCourse());
                    otf.setPlanAcceptedAndPaid(resp.getOtf().getEnrolled());
                    otf.setPlanEnded(resp.getOtf().getCourseEnded());
                } else {
                    otf.setEntityType("OTF");
                }
                otf.setActiveStudents(resp.getOtfActiveStudents());
                otf.setChurn(resp.getOtfChurn());
                exportTeacherDashboards.add(coursePlan);
                exportTeacherDashboards.add(otf);
            }
            StringWriter stringWriter = new StringWriter();
            ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);

            String[] fields = {"teacherId", "teacherName", "emailId", "phoneCode", "contactNumber", "primarySubject",
                "currentState", "entityType", "sessionsBooked", "sessionsDone", "cancelledByTeacher",
                "cancelledByStudent", "totalCancelled", "rescheduled", "sessionsLateJoined", "activeStudents",
                "churn", "contentShared", "contentAttempted", "contentEvaluated", "planShared", "planAcceptedAndPaid",
                "planEnded"};
            csvWriter.configureBeanMapping(ExportTeacherDashboard.class, fields);
            csvWriter.writeHeader(fields);
            for (ExportTeacherDashboard exportTeacherDashboard : exportTeacherDashboards) {
                try {
                    csvWriter.write(exportTeacherDashboard);
                } catch (Exception ex) {
                    logger.error("Exception: " + exportTeacherDashboard, ex);
                }
            }
            csvWriter.close();
            String data = stringWriter.toString();
            String fileName = "Teacher_Dashboard";

            communicationManager.sendUserDataEmail(httpSessionData, data, fileName);

        }
    }

    public void exportPasswordEmptyUsersForISL(HttpSessionData httpSessionData)
            throws VException, NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, AddressException, IOException {
        String[] fieldsArray = {"userId", "firstName", "lastName", "email", "passwordResetToken",
            "board", "grade", "school",
            "contactNumber", "registrationDate", "registrationTime", "isEmailVerified",
            "isContactNumberVerified", "campaign",
            "campaignMedium", "campaignSource", "city", "state", "country"};

        List<String> fields = Arrays.asList(fieldsArray);

        String data = StringUtils.join(fields.iterator(), CSV_DELIMITER);
        data += "\n";

        Integer start = 0;

        while (true) {

            GetUsersRes response = userManager.getPasswordEmptyUsersForISL(start, SIZE);
            List<UserInfo> users = response.getUsers();
            if (users.isEmpty()) {
                break;
            }

            for (UserInfo user : users) {
                if (!Role.STUDENT.equals(user.getRole())) {
                    continue;
                }
                //doing hardcoding to avoid creating pojos

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
                timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

                StudentInfo studentInfo = (StudentInfo) user.getInfo();

                StringBuilder entry = new StringBuilder();
                entry.append("'").append(user.getUserId());
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString(user.getFirstName()));
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString(user.getLastName()));
                entry.append(CSV_DELIMITER);
                entry.append(user.getEmail());
                entry.append(CSV_DELIMITER);
                entry.append(user.getReferralCode());
                entry.append(CSV_DELIMITER);
                entry.append(studentInfo.getBoard());
                entry.append(CSV_DELIMITER);
                entry.append(studentInfo.getGrade());
                entry.append(CSV_DELIMITER);
                entry.append(studentInfo.getSchool());
                entry.append(CSV_DELIMITER);
                entry.append(user.getContactNumber());
                entry.append(CSV_DELIMITER);
                entry.append(dateFormat.format(user.getCreationTime()));
                entry.append(CSV_DELIMITER);
                entry.append(timeFormat.format(user.getCreationTime()));
                entry.append(CSV_DELIMITER);
                entry.append(user.getIsEmailVerified());
                entry.append(CSV_DELIMITER);
                entry.append(user.getIsContactNumberVerified());
                entry.append(CSV_DELIMITER);
                entry.append(user.getUtm().getUtm_campaign());
                entry.append(CSV_DELIMITER);
                entry.append(user.getUtm().getUtm_medium());
                entry.append(CSV_DELIMITER);
                entry.append(user.getUtm().getUtm_source());
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString((user.getLocationInfo() != null) ? user.getLocationInfo().getCity() : ""));
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString((user.getLocationInfo() != null) ? user.getLocationInfo().getState() : ""));
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString((user.getLocationInfo() != null) ? user.getLocationInfo().getCountry() : ""));
                entry.append("\n");
                data += (entry.toString());
            }

            start = start + SIZE;
        }
        communicationManager.sendUserDataEmail(httpSessionData, data, ExportType.USER.toString());

    }
}
