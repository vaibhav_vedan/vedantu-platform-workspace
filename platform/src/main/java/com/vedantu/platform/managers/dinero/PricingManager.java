/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.request.UpdateCustomPlanRequest;
import com.vedantu.dinero.response.ApprovePricingChangeResponse;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.request.dinero.ApprovePricingChangeRequest;
import com.vedantu.platform.pojo.subscription.GetPlanByIdResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class PricingManager {
    


    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PricingManager.class);
    
    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    
    public String updateCustomPlan(UpdateCustomPlanRequest request) throws VException, JSONException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/updateCustomPlan", HttpMethod.POST,
                new Gson().toJson(request));
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        JSONObject jsonObject = new JSONObject(jsonString);
        String planId = jsonObject.getString("id");        
        logger.info("Exiting: " + planId);
        return planId;
    }
        
        
    public GetPlanByIdResponse getPlanByHours(Long teacherId, Double hours) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/pricing/getPlanByHours?teacherId=" + teacherId + "&hours=" + hours, HttpMethod.GET,
                null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetPlanByIdResponse response = new Gson().fromJson(jsonString, GetPlanByIdResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }
    
    
    
    public BaseResponse approvePricingChangeRequest(ApprovePricingChangeRequest request)
			throws VException {
		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/approvePricingChangeRequest",
				HttpMethod.POST, new Gson().toJson(request));

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);

		ApprovePricingChangeResponse response = new Gson().fromJson(jsonString, ApprovePricingChangeResponse.class);
		logger.info("Exiting: " + response.toString());
                //ES processing
                //eSManager.updateStartsFromInES(response.getTeacherId(), response.getTeacherMinPrice()/100.0, response.getTeacherOneHourRate()/100.0);
                Map<String, Object> payload = new HashMap<>();
                payload.put("teacherId", response.getTeacherId());
                payload.put("teacherMinPrice", response.getTeacherMinPrice()/100.0);
                payload.put("teacherOneHourRate", response.getTeacherOneHourRate()/100.0);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_UPDATE_STARTS_FROM, payload);
                asyncTaskFactory.executeTask(params);
                
                
		return new BaseResponse(ErrorCode.SUCCESS, "Approved successfully");
	}

}
