/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.lms;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import com.vedantu.util.*;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.lms.cmds.request.GetContentInfosRes;
import com.vedantu.lms.request.GetAllContentsReq;
import com.vedantu.lms.request.GetContentsReq;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.moodle.MoodleManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.utils.TimeDateMonth;
import com.vedantu.session.pojo.EntityType;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author shyam
 * 
 */
@Service
public class LMSManager {

	// Functionalities-
	// 1. Create users
	// 2. Create mappings
	// 3. Sharing OTF
	// 4. Sharing OTO

	@Autowired
	private MoodleManager moodleManager;

	@Autowired
	private CMDSTestManager cmdsTestManager;

	@Autowired
    private FosUtils fosUtils;
	
	@Autowired
	private OTFManager otfManager;
	
	@Autowired
	private TimeDateMonth timeDateMonth;
	
	@Autowired
	RedisDAO redisDAO;
	
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LMSManager.class);

	private String LMS_ENDPOINT;

	@PostConstruct
	public void init() {
		LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	}

	public static Map<String, String> inMemoryMap = new HashMap<>();
	private static Gson gson = new Gson();

	public GetContentInfosRes getContents(GetContentsReq getContentsReq)
			throws VException, IllegalArgumentException, IllegalAccessException {
		logger.info("Request : " + getContentsReq.toString());
		String getContentsUrl = LMS_ENDPOINT + ConfigUtils.INSTANCE.getStringValue("vedantu.lms.url.api.contents") + "?"
				+ WebUtils.INSTANCE.createQueryStringOfObject(getContentsReq);
		ClientResponse resp = WebUtils.INSTANCE.doCall(getContentsUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		GetContentInfosRes res = gson.fromJson(jsonString, GetContentInfosRes.class);
		
		Set<String> userIds = new HashSet<>();
		Set<String> contextIds = new HashSet<>();
		if(ArrayUtils.isNotEmpty(res.getList())){
			for(ContentInfoResp contentInfo : res.getList()){
				if(StringUtils.isNotEmpty(contentInfo.getStudentId()) 
						&& StringUtils.isEmpty(contentInfo.getStudentFullName())){
					userIds.add(contentInfo.getStudentId());
				}
				if(StringUtils.isNotEmpty(contentInfo.getTeacherId()) 
						&& StringUtils.isEmpty(contentInfo.getTeacherFullName())){
					userIds.add(contentInfo.getTeacherId());
				}
				if(contentInfo.getContextType() != null && StringUtils.isNotEmpty(contentInfo.getContextId())
						&& EntityType.OTF.equals(contentInfo.getContextType()) && StringUtils.isEmpty(contentInfo.getCourseName())){
					contextIds.add(contentInfo.getContextId());
				}
			}
			Map<String, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMap(userIds, false);
			Map<String,String> courseMap = otfManager.getCourseTitleForBatchIds(contextIds);
			for(ContentInfoResp contentInfo : res.getList()){
				if(StringUtils.isNotEmpty(contentInfo.getStudentId()) 
						&& StringUtils.isEmpty(contentInfo.getStudentFullName())){
					if(usermap.containsKey(contentInfo.getStudentId())){
						contentInfo.setStudentFullName(usermap.get(contentInfo.getStudentId()).getFullName());
					}					
				}
				if(StringUtils.isNotEmpty(contentInfo.getTeacherId()) 
						&& StringUtils.isEmpty(contentInfo.getTeacherFullName())){
					if(usermap.containsKey(contentInfo.getTeacherId())){
						contentInfo.setTeacherFullName(usermap.get(contentInfo.getTeacherId()).getFullName());
					}					
				}
				if(contentInfo.getContextType() != null && StringUtils.isNotEmpty(contentInfo.getContextId())
						&& EntityType.OTF.equals(contentInfo.getContextType()) && StringUtils.isEmpty(contentInfo.getCourseName())){
					if(courseMap.containsKey(contentInfo.getContextId())){
						contentInfo.setCourseName(courseMap.get(contentInfo.getContextId()));
					}
				}
			}
		}
		return res;
	}

	public String getTeacherDashBoardInfo(long teacherId) throws VException {
		logger.info("Request : " + teacherId);
		String getTeacherDashBoaardInfoUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.lms.url.api.teacher.dashboard.url") + "?teacherId="
				+ teacherId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(getTeacherDashBoaardInfoUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	public String getAllContents(GetAllContentsReq req)
			throws IllegalArgumentException, IllegalAccessException, VException {
		logger.info("Request : " + req.toString());
		String getAllContentsUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.lms.url.api.getAllContents") + "?"
				+ WebUtils.INSTANCE.createQueryStringOfObject(req);
		ClientResponse resp = WebUtils.INSTANCE.doCall(getAllContentsUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	public String getContentById(String contentId) throws VException {
		logger.info("Request : " + contentId);
		String getAllContentsUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.lms.url.api.getContentById") + "?contentInfoId="
				+ contentId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(getAllContentsUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		return jsonString;
	}

	public void expireContents() throws VException {
		logger.info("Request : " + System.currentTimeMillis());
		String expireContentsUrl = LMS_ENDPOINT + ConfigUtils.INSTANCE.getStringValue("vedantu.lms.expireContents.url");
		ClientResponse resp = WebUtils.INSTANCE.doCall(expireContentsUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
	}

	public void expireContentsBeforeTime(long beforeTime) throws VException {
		logger.info("Request : " + System.currentTimeMillis());
		String expireContentsUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.lms.expireContentsBeforeTime.url") + "?beforeTime="
				+ beforeTime;
		ClientResponse resp = WebUtils.INSTANCE.doCall(expireContentsUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
	}

	public void sendPendingContentNotifications(Boolean allPending) throws VException {
		logger.info("Request : " + allPending);
		String sendPendingContentNotificationsUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.lms.send.pending.evaluation.notification.url")
				+ "?allPending=" + allPending;
		ClientResponse resp = WebUtils.INSTANCE.doCall(sendPendingContentNotificationsUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
	}

	public void getPendingEvaluations(boolean allPending) {
		try {
			String getPendingEvaluationsUrl = LMS_ENDPOINT
					+ ConfigUtils.INSTANCE.getStringValue("vedantu.lms.send.pending.evaluation.notification.url")
					+ "?allPending=" + allPending;
			ClientResponse resp = WebUtils.INSTANCE.doCall(getPendingEvaluationsUrl, HttpMethod.GET, null, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String jsonString = resp.getEntity(String.class);
			logger.info(jsonString);
		} catch (VException ex) {
			logger.error("getPendingEvaluationContents - exception occured : " + ex.toString());
		}
	}


	// -------------------- Asycn updates -----------------//

	public void contentShareOTFBatch(String batchId) throws VException {
		// Share CMDS
		cmdsTestManager.shareOTFBatch(batchId);
	}

	public void contentShareOTFBatch(BatchInfo batchInfo) throws VException {
		// Share CMDS
		cmdsTestManager.shareOTFBatch(batchInfo.getBatchId());
	}

}
