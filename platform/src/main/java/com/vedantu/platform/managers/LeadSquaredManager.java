package com.vedantu.platform.managers;

import com.google.common.util.concurrent.RateLimiter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vdurmont.emoji.EmojiParser;
import com.vedantu.User.*;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.pojo.Coupon;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.pojo.KeyValueObject;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.LeadSquaredAgentDAO;
import com.vedantu.platform.dao.cms.WebinarUserRegistrationInfoDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.entity.HomeDemoRequest;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.notification.SMSManager;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.managers.onetofew.OTFBundleManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.platform.managers.subscription.BundlePackageManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.mongodbentities.LeadSquaredAgent;
import com.vedantu.platform.mongodbentities.UserMessage;
import com.vedantu.platform.pojo.ReviseIndiaReq;
import com.vedantu.platform.pojo.cms.WebinarCallbackData;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.platform.pojo.dinero.Transaction;
import com.vedantu.platform.pojo.dinero.TransactionUserInfo;
import com.vedantu.platform.pojo.leadsquared.*;
import com.vedantu.platform.pojo.subscription.TrialReq;
import com.vedantu.platform.utils.LeadSquaredUtils;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.pojo.CoursePlanSQSAlert;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.util.*;
import com.vedantu.util.enums.*;
import com.vedantu.util.logger.LoggingMarkers;
import com.vedantu.util.pojo.QueueMessagePojo;
import com.vedantu.util.subscription.SubscriptionRequestState;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LeadSquaredManager {

    private String ENV = "";
    public Long QUERY_FETCH_SIZE = Long.valueOf(1000);
    public String LS_ACCESS_ID = "";
    public String LS_SECRET_KEY = "";
    public String LS_ACCESS_ID_HIDE = "";
    public String LS_SECRET_KEY_HIDE = "";
    public String LS_UPSERT_URL = "";
    public String LS_ACTIVITY_URL = "";
    public String LS_FETCH_DETAILS_URL = "";
    public String LS_FETCH_BYEMAIL_URL = "";
    public String LS_GET_ACTIVITY_URL = "";
    public List<MessageType> queryMessageTypes = new ArrayList<>();
    public String LS_BULK_ACTIVITY_URL = "";
    public String LS_BULK_ACTIVITY_UPDATE_URL = "";
    public String LS_ACTIVITY_UPDATE_URL = "";
    public String LS_LEAD_QUICK_SEARCH_URL = "";
    public String LS_GET_LEAD_BY_PHONE = "";
    public String LS_FETCH_AGENT_BY_EMAIL = "";

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadSquaredManager.class);

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @Autowired
    SessionManager sessionManager;

    @Autowired
    LeadSquaredAgentDAO leadSquaredAgentDAO;

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private BundlePackageManager bundlePackageManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    FosUtils userUtils;

    @Autowired
    private WebinarUserRegistrationInfoDAO webinarUserRegistrationInfoDAO;

    @Autowired
    public AwsSQSManager awsSQSManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private HomeDemoRequestManager homeDemoRequestManager;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private LSWebUtils lsWebUtils;

    @Autowired
    private FosUtils fosUtils;

    private static Gson gson = new Gson();

    public final RateLimiter limiter = RateLimiter.create(3);

    @Autowired
    private RedisDAO redisDAO;

    private static final Type leadDetailsListType = new TypeToken<List<LeadDetails>>() {
    }.getType();
    private static final Type leadActivitiesRequestType= new TypeToken<List<LeadActivityReq>>(){}.getType();

    private static final Set<SQSMessageType> newMessageTypes
            = new HashSet<>(Arrays.asList(SQSMessageType.COURSEPLAN_NEW_LEADSQUARE,
            SQSMessageType.ENROLLMENT_CREATED,
            SQSMessageType.INSTALMENT_SAVED,
            SQSMessageType.ORDER_SAVED_LEADSQUARE,
            SQSMessageType.SALE_CLOSED,
            SQSMessageType.TRANSACTION_NEW_LS,
            SQSMessageType.COURSE_PLAN_LESS_THAN_3HOURS,
            SQSMessageType.WEBINAR_ATTENDED,
            SQSMessageType.WEBINAR_REGISTERED,
            SQSMessageType.WEBINAR_MOBILE_CALLBACK,
            SQSMessageType.TRIAL_REQUEST,
            SQSMessageType.LS_TASK,
            SQSMessageType.SALES_DEMO_LEADSQUARED,
            SQSMessageType.CUSTOMER_SIGNUP_SUPERKIDS,
            SQSMessageType.BOOK_DEMO_SESSION_SUPERKIDS,
            SQSMessageType.CANCEL_SESSION_SUPERKIDS,
            SQSMessageType.DEMO_SESSION_FINISHED_SUPERKIDS,
            SQSMessageType.COURSE_PAYMENT_INITIATION_SUPERKIDS,
            SQSMessageType.COURSE_PAYMENT_COMPLETION_SUPERKIDS,
            SQSMessageType.BOOKING_CANCELLED_BY_OVERBOOKING,
            SQSMessageType.REVISE_INDIA,
            SQSMessageType.DEMO_REQUESTED,
            SQSMessageType.STUDENT_ENROLLED,
            SQSMessageType.ONLINE_COUNSELLING_SESSION_SCHEDULED
    ));

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    private static final Integer DEMO_SESSION_EVENT_CODE = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.demosession.book");
    private static final Integer SETUP_SESSION_EVENT_CODE = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.setupsession.support");

    public LeadSquaredManager() {
    }

    @PostConstruct
    public void init() {
        QUERY_FETCH_SIZE = ConfigUtils.INSTANCE.getLongValue("leadsquared.query.fetch.size");
        LS_ACCESS_ID = ConfigUtils.INSTANCE.getStringValue("leadsquared.access.id");
        LS_SECRET_KEY = ConfigUtils.INSTANCE.getStringValue("leadsquared.secret.key");
        LS_ACCESS_ID_HIDE = ConfigUtils.INSTANCE.getStringValue("leadsquared.access.id.hide");
        LS_SECRET_KEY_HIDE = ConfigUtils.INSTANCE.getStringValue("leadsquared.secret.key.hide");
        LS_UPSERT_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.upsert.url");
        LS_ACTIVITY_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.create.activity.url");
        LS_FETCH_DETAILS_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.fetch.details.url");
        LS_FETCH_BYEMAIL_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.fetch.byemail.url");
        LS_GET_ACTIVITY_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.fetch.getactivity.url");
        LS_BULK_ACTIVITY_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.create.bulk.activity.url");
        LS_BULK_ACTIVITY_UPDATE_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.create.bulk.activity.update.url");
        LS_ACTIVITY_UPDATE_URL = ConfigUtils.INSTANCE.getStringValue("leadsquared.create.activity.update.url");
        LS_LEAD_QUICK_SEARCH_URL  = ConfigUtils.INSTANCE.getStringValue("leadsquared.query.lead.quicksearch");
        LS_GET_LEAD_BY_PHONE  = ConfigUtils.INSTANCE.getStringValue("leadsquared.query.lead.search.by.phone");
        LS_FETCH_AGENT_BY_EMAIL = ConfigUtils.INSTANCE.getStringValue("leadsquared.fetch.agent.byemail.url");
        ENV  = ConfigUtils.INSTANCE.getStringValue("environment");
        queryMessageTypes = Arrays.asList(MessageType.TRIAL_SESSION, MessageType.ASK_DOUBT,
                MessageType.FIND_ME_TEACHER);
    }

    public void executeAsyncTask(LeadSquaredRequest req) {
        /*
        Map<String, Object> payload = new HashMap<>();
        payload.put("request", req);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LS_TASK, payload);
        asyncTaskFactory.executeTask(params);
         */
        if (LeadSquaredDataType.LEAD_UPSERT.equals(req.getDataType())
                && req.getUser() != null && req.getUser().getId() != null) {
//            Map<String, Object> payload = new HashMap<>();
//            payload.put("request", req);
//            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LS_TASK, payload);
//            asyncTaskFactory.executeTask(params);
            String messageGorupId = req.getUser().getId() + "_UPSERT";
            awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.LS_TASK, gson.toJson(req), messageGorupId);
        } else {
            awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.LS_TASK, gson.toJson(req));
        }
    }

    public void executeBulkAsyncTask(List<LeadSquaredRequest> requests) {
        if (ArrayUtils.isNotEmpty(requests)) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("requests", requests);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BULK_LS_TASK, payload);
            asyncTaskFactory.executeTask(params);
        }
    }

    public PlatformBasicResponse executeTask(LeadSquaredRequest req) throws Exception {
        req.verify();
        LeadSquaredAction action = req.getAction();
        Long callingUserId = req.getCallingUserId();
        if (req.getUser() != null) {
            if(!Objects.equals(req.getUser().getEmail(), "null") && req.getUser().getEmail() != null) {
                req.getParams().put("userEmail", req.getUser().getEmail());
            }

            if(StringUtils.isNotEmpty(req.getUser().getContactNumber()) && !"null".equalsIgnoreCase(req.getUser().getContactNumber())) {
                req.getParams().put("mobileNumber", req.getUser().getContactNumber());
            }
        }
        if (callingUserId != null && callingUserId > 0) {
            req.getParams().put("callingUserId", callingUserId.toString());
        } else {
            req.getParams().put("callingUserId", "0");
        }
        PlatformBasicResponse response;

        switch (action) {
            case POST_LEAD_DATA:
                response = postLeadSquaredData(req);
                break;
            case SYNC_USER_DATA:
                response = syncAllUserLeadSquaredData(req);
                break;
            case SYNC_SESSION_DATA:
                response = syncAllSessionLeadSquaredData(req);
                break;
            default:
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not found");
        }
        return response;
    }

    private PlatformBasicResponse postLeadSquaredData(LeadSquaredRequest reqPojo) throws Exception {
        logger.info("postLeadSquaredData " + reqPojo);
        PlatformBasicResponse res = new PlatformBasicResponse();
        if (StringUtils.isEmpty(LS_ACCESS_ID) || StringUtils.isEmpty(LS_SECRET_KEY)) {
            logger.warn("postLeadSquaredData missing ids");
            res.setSuccess(false);
            return res;
        }

        // Map<String, String> params = reqPojo.getParams();
        LeadSquaredDataType dataType = reqPojo.getDataType();
        switch (dataType) {
            case LEAD_UPSERT:
                leadCreate(reqPojo);
                break;
            case LEAD_ACTIVITY_MESSAGE:
                leadAcitivityMessage(reqPojo);
                break;
            case LEAD_ACTIVITY_SESSION:
                leadActivitySession(reqPojo);
                if (SessionModel.TRIAL.name().equals(reqPojo.getParams().get("sessionModel"))) {
                    leadActivityTrailSession(reqPojo);
                }
                break;
            case LEAD_ACTIVITY_PROPOSAL:
                leadActivityProposal(reqPojo);
                break;
            case LEAD_ACTIVITY_QUERY:
                leadAcitivityQuery(reqPojo);
                break;
            case LEAD_TOS_DONE:
                leadTosDone(reqPojo);
                break;
            case LEAD_REQUEST_CALLBACK:
                // leadRequstCallBack(reqPojo);
                break;
            case LEAD_SUBSCRIPTION_REQUEST:
                leadSubscriptionRequest(reqPojo);
                break;
            case LEAD_ACTIVITY_SHOW_INTEREST:
                leadAcitivityShowInterest(reqPojo);
                break;
            case LEAD_ACTIVITY_COURSEPLAN_UPDATE:
                leadAcitivityCoursePlanUpdated(reqPojo);
                break;
            case LEAD_ACTIVITY_REGISTER:
                leadAcitivityRegisterStudent(reqPojo);
                break;
            case LEAD_ACTIVITY_ENROLL:
                leadAcitivityEnrollRegularStudent(reqPojo);
                break;
            case LEAD_ACTIVITY_ON_CALL:
                leadActivityOnCall(reqPojo);
                break;
            case LEAD_GPS_UPDATE:
                leadUpsert(reqPojo.getParams());
                logger.info("send the GPS data to leadsquared queue using sns");
                break;
            case LEAD_ACTIVITY_HOME_DEMO:
                leadActivityHomeDemo(reqPojo);
                break;
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Request not supported");
        }
        res.setSuccess(true);
        logger.info("postLeadSquaredData " + res);
        return res;
    }

    private boolean leadUpsert(Map<String, String> fields)
            throws IllegalArgumentException, IllegalAccessException, IOException, VException {
        // Post lead data to LeadSquared
        String url = getLeadSquaredURL(LS_UPSERT_URL);
//              Does Nothing
//		if ("true".equals(fields.get("mx_featureHide"))) {
//			url = getLeadSquaredURL(LS_UPSERT_URL);
//		}
        return postLeadData(url, HttpMethod.POST, LeadSquaredUtils.createJSONObjectLeadDetails(fields), null,
                LeadSquaredDataType.LEAD_UPSERT, fields.get(LeadDetails.Constants.EMAIL_ADDRESS),
                fields.get(LeadDetails.Constants.USER_ID), null);
    }

    private String getLeadSquaredURL(String url) throws BadRequestException {
        if (StringUtils.isEmpty(url) || StringUtils.isEmpty(LS_ACCESS_ID) || StringUtils.isEmpty(LS_SECRET_KEY)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No access details found");
        }
        return url + "?accessKey=" + LS_ACCESS_ID + "&secretKey=" + LS_SECRET_KEY;
    }

    private boolean postLeadData(String url, HttpMethod httpMethod, String json, String activityEvent,
                                 LeadSquaredDataType type, String email, String referenceNo, String note)
            throws IOException, IllegalArgumentException, IllegalAccessException, BadRequestException, VException {

        try {
            json = LeadSquaredUtils.removeEmojisFromLSRequest(json);
        } catch (Exception e) {
            logger.error("Error in removing emojis", e, json);
        }
        boolean posted = false;
        for (int i = 0; (i < 7) && !posted; i++) {
            ClientResponse cRes = postLeadData(url, HttpMethod.POST, json);

            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                if (response.contains("A Lead with same Email already exists")) {
                    logger.info("Trying again with email removal from payload");
                    LeadActivityReq leadActivityReq = gson.fromJson(json, LeadActivityReq.class);
                    leadActivityReq.setEmailAddress(null);//setting email address to null for those already having phone no as search key,avoiding ambiguity in ls
                    json=gson.toJson(leadActivityReq);
                    cRes = postLeadData(url, HttpMethod.POST, json);
                    if (cRes.getStatus() == 200)
                        posted = true;
                    response = cRes.getEntity(String.class);
                } else if (response.contains("A Lead with same Phone Number already exists")) {
                    logger.info("Trying again with phone removal from payload");
                    LeadActivityReq leadActivityReq = gson.fromJson(json, LeadActivityReq.class);
                    leadActivityReq.setPhone(null);//setting phone to null for those already having emali no as search key,avoiding ambiguity in ls
                    json=gson.toJson(leadActivityReq);
                    cRes = postLeadData(url, HttpMethod.POST, json);
                    if (cRes.getStatus() == 200)
                        posted = true;
                    response = cRes.getEntity(String.class);
                } else if (response.contains("Invalid Email Address") || response.contains("Invalid data")) {
                    logger.warn(LoggingMarkers.JSON_MASK, "Error pushing data to leadsquared- Code:" + cRes.getStatus() + " response:" + response
                            + " req: " + json + ", url:" + url);
                    return false;
                } else {
                    logger.error(LoggingMarkers.JSON_MASK, "Error pushing data to leadsquared- Code:" + cRes.getStatus() + " response:" + response
                            + " req: " + json);
                }
                if(!posted)
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Error pushing to LS: " + response);
            } else if (cRes.getStatus() == 429) {
                try {
                    logger.warn(LoggingMarkers.JSON_MASK,"Too many requests for post lead data  , response: " + response);
                    Thread.sleep(1000);
                } catch (Exception ex) {

                }
                continue;
            } else {
                posted = true;
                logger.info("Data successfully posted");
                break;
            }
        }

        if(!posted)
            throw new BadRequestException(ErrorCode.TOO_MANY_REQUESTS,"Failed to push LS data with 7 retries due to 429 response code");

        return posted;
    }

    //
    // private void leadCreate(User user)
    // throws IllegalArgumentException, IllegalAccessException, IOException,
    // VException {
    // LeadDetails lead = new LeadDetails(user);
    // logger.info("leadCreate" + lead);
    // Map<String, String> fields = leadCreate(lead);
    // leadUpsert(fields);
    // }
    private Map<String, String> leadCreate(LeadDetails lead)
            throws IllegalArgumentException, IllegalAccessException, BadRequestException, IOException {
        Map<String, String> fields = new HashMap<String, String>();
        for (Field field : LeadDetails.class.getDeclaredFields()) {
            if (field.get(lead) != null && !Objects.equals("null", field.get(lead))) {
                fields.put(field.getName(), String.valueOf(field.get(lead)));
            }
        }
        return fields;
    }

    public void leadCreate(LeadSquaredRequest request)
            throws Exception {
        User user = request.getUser();

        // PRODUCT REQUIREMENT: DON'T PUSH VOLT USERS TO LEAD SQUARED : GAURAV SINGHAL
        if (user.getSignUpFeature() == FeatureSource.VOLT_2020_JAN) {
            return;
        }

        try {
            Map<String, String> fields = leadCreate(new LeadDetails(user));
            leadUpsert(fields);

        } catch (BadRequestException e){
            throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
        }
        catch (Exception e) {
            logger.warn("Error creating lead: ", e);
            throw new BadRequestException(ErrorCode.SERVICE_ERROR,e.getMessage());
        }

    }

    // private String getLeadSquaredHideURL(String url) throws
    // BadRequestException {
    // if (StringUtils.isEmpty(url) || StringUtils.isEmpty(LS_ACCESS_ID_HIDE)
    // || StringUtils.isEmpty(LS_SECRET_KEY_HIDE)) {
    // throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No access
    // details found");
    // }
    // return url + "?accessKey=" + LS_ACCESS_ID_HIDE + "&secretKey=" +
    // LS_SECRET_KEY_HIDE;
    // }
    private void leadAcitivityMessage(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();
        String bound = params.get("bound");
        if (StringUtils.isEmpty(bound)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bound of the messsage not found");
        }

        String teacherId = params.get("teacherId");
        if (StringUtils.isEmpty(teacherId) || Long.valueOf(teacherId) == null || Long.valueOf(teacherId) <= 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacher id invalid");
        }

        UserBasicInfo teacher = userUtils.getUserBasicInfo(teacherId, true);

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.message." + bound);

        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity.setActivityNote(("sent".equals(bound) ? "sent to " : "received from ") + teacher.getFullName()
                + "(" + teacher.getEmail() + ")");
        leadActivity.setPhone(params.get("mobileNumber"));
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_MESSAGE, params.get("messageId"),
                HttpMethod.POST);
    }

    private void leadActivitySession(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();
        String teacherId = params.get("teacherId");
        List<String> userIds = new ArrayList<>();
        userIds.add(params.get("teacherId"));
        userIds.add(params.get("studentId"));
        Map<String, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMap(userIds, true);
        UserBasicInfo teacher = userInfos.get(params.get("teacherId"));
        UserBasicInfo student = userInfos.get(params.get("studentId"));

        String teacherEmail = teacher.getEmail();
        String teacherFullName = teacher.getFullName();
        if (StringUtils.isEmpty(teacherId) || Long.parseLong(teacherId) <= 0 || Long.valueOf(teacherId) == null
                || Long.valueOf(teacherId) <= 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacher id invalid");
        }

        // UserBasicInfo teacher =
        // fosUtils.getUserBasicInfo(teacherId.toString());
        if (SessionState.SCHEDULED.name().equals(params.get("sessionState"))) {
            try {

                Boolean firstSessionBooked = sessionManager
                        .checkFirstBookedSession(Long.parseLong(params.get("studentId")));
                if (firstSessionBooked != null && firstSessionBooked) {
                    // Send first session activity
                    String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.first.session.booked");
                    LeadActivity leadActivity = new LeadActivity(eventCode, student.getEmail(),
                            System.currentTimeMillis(), null);
                    leadActivity.setPhone(student.getContactNumber());
                    leadActivity.setActivityNote(" for first session with " + teacherFullName + "(" + teacherEmail
                            + ") at "
                            + LeadSquaredUtils.getLeadSquaredDate(Long.valueOf(params.get("sessionStartTime"))));
                    postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_FIRST_SESSION,
                            params.get("sessionId"), HttpMethod.POST);
                }
                // }
            }catch (BadRequestException e){
                throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
            }catch (Exception ex) {
                logger.warn("leadActivitySession - ex : " + ex.getMessage(), ex);
                throw new Exception(ex);
            }
        }

        String subscriptionSessionSource = params.get("subscriptionSessionSource");
        String sessionState = params.get("sessionState");
        try {
            if (!StringUtils.isEmpty(sessionState)) {
                SessionState state = SessionState.valueOf(sessionState);
                if (SessionState.SCHEDULED.equals(state) && SessionState.CANCELED.equals(state)
                        && SessionState.EXPIRED.equals(state)) {
                    String eventCode = ConfigUtils.INSTANCE
                            .getStringValue("leadsquared.activity.session." + state.name().toLowerCase());
                    LeadActivity leadActivity = new LeadActivity(eventCode, student.getEmail(),
                            System.currentTimeMillis(), null);
                    leadActivity.setActivityNote(" for session with " + teacherFullName + "(" + teacherEmail + ") at "
                            + LeadSquaredUtils.getLeadSquaredDate(Long.valueOf(params.get("sessionStartTime")))
                            + (!StringUtils.isEmpty(subscriptionSessionSource)
                            ? ". Session source is : " + subscriptionSessionSource
                            : ""));
                    postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_SESSION, params.get("sessionId"),
                            HttpMethod.POST);
                }
            }
        } catch (BadRequestException e){
            throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
        }catch (Exception ex) {
            logger.warn("Exception: ", ex);
            throw new Exception(ex);
        }
    }

    private void leadActivityTrailSession(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();
        String teacherId = params.get("teacherId");
        if (StringUtils.isEmpty(teacherId) || Long.parseLong(teacherId) <= 0 || Long.valueOf(teacherId) == null
                || Long.valueOf(teacherId) <= 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacher id invalid");
        }

        List<String> userIds = new ArrayList<>();
        userIds.add(params.get("teacherId"));
        userIds.add(params.get("studentId"));
        Map<String, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMap(userIds, true);
        UserBasicInfo teacher = userInfos.get(params.get("teacherId"));
        UserBasicInfo student = userInfos.get(params.get("studentId"));
        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.session.trail");
        LeadActivity leadActivity = new LeadActivity(eventCode, student.getEmail(), System.currentTimeMillis(), null);
        leadActivity.setActivityNote(" for session with " + teacher.getFullName() + "(" + teacher.getEmail() + ") at "
                + LeadSquaredUtils.getLeadSquaredDate(Long.valueOf(params.get("sessionStartTime"))));
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_TRAIL_SESSION, params.get("sessionId"),
                HttpMethod.POST);
    }

    private void leadTosDone(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();
        if (sessionManager.checkTOSSession(Long.parseLong(params.get("studentId")))) {
            String teacherId = params.get("teacherId");
            UserBasicInfo teacher = userUtils.getUserBasicInfo(teacherId, true);

            String teacherEmail = teacher.getEmail();
            String teacherFullName = teacher.getFullName();

            String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.session.tos.done");
            LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                    null);
            leadActivity.setPhone(teacher.getContactNumber());
            leadActivity.setActivityNote(" for session with " + teacherFullName + "(" + teacherEmail + ") at "
                    + LeadSquaredUtils.getLeadSquaredDate(Long.valueOf(params.get("sessionStartTime"))));
            postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_SESSION, params.get("sessionId"),
                    HttpMethod.POST);

            // Lead upsert to change lead stage to TOS-DONE
            Map<String, String> fields = new HashMap<String, String>();
            fields.put(LeadDetails.Constants.EMAIL_ADDRESS, params.get("userEmail"));
            fields.put(LeadDetails.Constants.LEAD_STAGE, "6. TOS Done");
            leadUpsert(fields);
        }

    }

    public void leadRequstCallBack(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();
        List<String> userIds = new ArrayList<>();
        userIds.add(params.get("teacherId"));
        userIds.add(params.get("studentId"));
        Map<String, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMap(userIds, true);
        UserBasicInfo teacher = userInfos.get(params.get("teacherId"));
        UserBasicInfo student = userInfos.get(params.get("studentId"));
        // User teacher =
        // userManager.getUserById(Long.parseLong(params.get("teacherId")));
        // User student =
        // userManager.getUserById(Long.parseLong(params.get("studentId")));
        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.request.callback");
        LeadActivity leadActivity = new LeadActivity(eventCode, student.getEmail(), System.currentTimeMillis(), null);
        String teacherLeadStatus = params.get("teacherLeadStatus") == null ? "true" : params.get("teacherLeadStatus");
        leadActivity.setActivityNote(" Call back requested by Student : " + student.getFullName() + "("
                + student.getEmail() + ")" + "with teacher : " + teacher.getFullName() + "(" + teacher.getEmail()
                + ") at " + LeadSquaredUtils.getLeadSquaredDate(Long.valueOf(params.get("eventTime"))) + " for grade : "
                + params.get("grade") + " for subject : " + params.get("subject") + " for target : "
                + params.get("target") + " teacher Lead status : " + teacherLeadStatus + "  Message : "
                + params.get("message"));
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_REQUEST_CALLBACK, params.get("teacherId"),
                HttpMethod.POST);

        // Lead upsert to change lead stage to TOS-DONE
        Map<String, String> fields = new HashMap<String, String>();
        fields.put(LeadDetails.Constants.EMAIL_ADDRESS, params.get("userEmail"));
        leadUpsert(fields);
    }

    private void leadSubscriptionRequest(LeadSquaredRequest req)
            throws Exception {
        logger.info("leadSubscriptionRequest - " + req.toString());
        Map<String, String> params = req.getParams();
        List<String> userIds = new ArrayList<>();
        userIds.add(params.get("teacherId"));
        userIds.add(params.get("studentId"));
        Map<String, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMap(userIds, true);
        UserBasicInfo teacher = userInfos.get(params.get("teacherId"));
        UserBasicInfo student = userInfos.get(params.get("studentId"));
        // User teacher =
        // userManager.getUserById(Long.parseLong(params.get("teacherId")));
        // User student =
        // userManager.getUserById(Long.parseLong(params.get("studentId")));

        // String studentFullName = params.get("studentFullName");
        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.subscription.request");
        LeadActivity leadActivity = new LeadActivity(eventCode, student.getEmail(), System.currentTimeMillis(), null);
        leadActivity.setPhone(student.getContactNumber());
        leadActivity.setActivityNote(" Subscription request with teacher : " + teacher.getFullName() + "("
                + teacher.getEmail() + ")" + "has been updated to state: " + params.get("subscriptionRequestState")
                + ", totalHours:" + params.get("totalHours") + ", noOfWeeks:" + params.get("noOfWeeks")
                + " and subscription request id : " + params.get("subscriptionRequestId"));
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_SUBSCRIPTION_REQUEST,
                params.get("subscriptionRequestId"), HttpMethod.POST);

        Map<String, String> fields = new HashMap<>();
        fields.put(LeadDetails.Constants.EMAIL_ADDRESS, params.get("userEmail"));
        SubscriptionRequestState subscriptionRequestState = SubscriptionRequestState
                .valueOf(params.get("subscriptionRequestState"));
        if (subscriptionRequestState != null) {
            if (SubscriptionRequestState.DRAFT.equals(subscriptionRequestState)
                    || SubscriptionRequestState.EXPIRED.equals(subscriptionRequestState)
                    || SubscriptionRequestState.REJECTED.equals(subscriptionRequestState)
                    || SubscriptionRequestState.CANCELLED.equals(subscriptionRequestState)) {
                fields.put(LeadDetails.Constants.SUBSCRIPTION_REQUEST_LEAD, "true");
            } else {
                fields.put(LeadDetails.Constants.SUBSCRIPTION_REQUEST_LEAD, "false");
            }

            leadUpsert(fields);
        }
        logger.info("leadSubscriptionRequest - " + params.toString());
    }

    private void leadActivityProposal(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();
        String teacherId = params.get("teacherId");
        if (StringUtils.isEmpty(teacherId) || Long.parseLong(teacherId) <= 0 || Long.valueOf(teacherId) == null
                || Long.valueOf(teacherId) <= 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "teacher id invalid");
        }
        UserBasicInfo teacher = userUtils.getUserBasicInfo(teacherId, true);

        if (StringUtils.isEmpty(params.get("proposalStatus"))) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "proposal status not found");
        }
        ProposalStatus proposalStatus = ProposalStatus.valueOf(params.get("proposalStatus"));
        String bound = "sent";
        Long callingUserId = Long.valueOf(params.get(Constants.CALLING_USERID));
        switch (proposalStatus) {
            case REJECTED:
                bound = "rejected";
                break;
            case CANCELED:
                bound = "rejected";
                break;
            case PENDING:
                if (teacherId.equals(callingUserId)) {
                    bound = "received";
                }
                break;
            default:
                return;
        }

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.proposal." + bound);
        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity
                .setActivityNote(" for session request with " + teacher.getFullName() + "(" + teacher.getEmail() + ")");
        leadActivity.setPhone(teacher.getContactNumber());
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_PROPOSAL, params.get("proposalId"),
                HttpMethod.POST);
    }

    private void leadAcitivityQuery(LeadSquaredRequest req)
            throws Exception {

        logger.info("leadAcitivityQuery " + req);
        Map<String, String> params = req.getParams();
        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.message.query");
        if (CommunicationType.INVITE_REQUEST.name().equals(params.get("messageType"))) {
            LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                    null);
            String messageText = params.get("messageText");
            if (!StringUtils.isEmpty(messageText)) {
                messageText = messageText.replace("\n", ".").replace("\r", "");
            }
            leadActivity.setActivityNote("regarding " + params.get("messageType") + " Subject : "
                    + params.get("subject") + " Message : " + messageText);
            postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_QUERY, null, HttpMethod.POST);

            // Update the lead details
            LeadDetails lead = new LeadDetails();
            lead.setEmailAddress(params.get("userEmail"));
            lead.setFirstName(params.get("userName"));
            lead.setMobile(params.get("mobileNumber"));
            lead.setMx_Query_Type(CommunicationType.INVITE_REQUEST.name());
            leadCreate(lead);
            return;
        }

        String messageString = params.get("messagePojo");
        if (StringUtils.isEmpty(messageString)) {
            logger.info("leadAcitivityQuery message pojo not found");
            return;
        }
        Gson gson = new Gson();
        UserMessage message = gson.fromJson(messageString, UserMessage.class);

        // Escape newlines
        String messageText = message.getMessage();
        if (messageText != null) {
            messageText = messageText.replace("\n", ".").replace("\r", "");
            message.setMessage(messageText);
        }
        MessageType messageType = message.getMessageType();

        if (queryMessageTypes.contains(messageType)) {
            LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                    null);
            leadActivity.setActivityNote("regarding " + messageType + " Message : " + message.getMessage());
            postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_QUERY, null, HttpMethod.POST);
            // Update the lead
            User user = req.getUser();
            if (user != null) {
                LeadDetails lead = new LeadDetails(user);
                lead.setMx_Query_Type(messageType.name());
                leadCreate(lead);
            }
        } else {
            logger.info("leadAcitivityQuery invalid message type");
            return;
        }
    }

    private void leadAcitivityCoursePlanUpdated(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.courseplan.update");

        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity.setPhone(params.get("mobileNumber"));
        String coursePlanStr = params.get("CoursePlanObject");
        CoursePlanInfo coursePlanInfo = gson.fromJson(coursePlanStr, CoursePlanInfo.class);
        if (coursePlanInfo == null) {
            logger.warn("No courseplan found");
        }

        leadActivity.setActivityNote(getCoursePlanUpdatetActivityNote(coursePlanInfo));
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_COURSEPLAN_UPDATE, null, HttpMethod.POST);
    }

    private void leadAcitivityShowInterest(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.show.interest");

        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity.setPhone(params.get("mobileNumber"));
        StringBuilder sb = new StringBuilder();
        sb.append(params.get("userEmail"));

        if (params.get("interestType") != null) {
            sb.append(" is interested in: ").append(params.get("interestType"));
        }

        if (params.get("entityType") != null) {
            sb.append(", ").append(params.get("entityType"));
        }

        if (params.get("entityId") != null) {
            sb.append("Id: ").append(params.get("entityId"));
        }
        if (params.get("entityName") != null) {
            sb.append(", courseName: ").append(params.get("entityName"));
        }
        if (params.get("teacherEmail") != null) {
            sb.append(", taught by teacherEmail: ").append(params.get("teacherEmail"));
        }
        if (params.get("note") != null) {
            sb.append(", note: ").append(params.get("note"));
        }
        if (params.get("subject") != null) {
            sb.append(", subject: ").append(params.get("subject"));
        }
        if (params.get("target") != null) {
            sb.append(", target: ").append(params.get("target"));
        }
        if (params.get("grade") != null) {
            sb.append(", grade: ").append(params.get("grade"));
        }
        leadActivity.setActivityNote(sb.toString());
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_SHOW_INTEREST, null, HttpMethod.POST);
    }

    private boolean postLeadActivity(LeadActivity leadActivity, LeadSquaredDataType leadActivityDataType,
                                     String referenceNo, HttpMethod httpMethod)
            throws Exception {
        String url = getLeadSquaredURL(LS_ACTIVITY_URL);
        logger.info("postLeadActivity", new Object[]{url, httpMethod, leadActivity});
        Map<String, String> fields = leadActivity.getFieldMap();

        // LE-2774 avoid duplicate lead creation -- set phone to fetch an existing lead or upsert
        // get phone number and email from the lead request object
        String leadPhone = null;
        String leadEmail = null;
        if (fields.containsKey("Phone")) {
            leadPhone = fields.get("Phone");
        }
        if (fields.containsKey("EmailAddress")) {
            leadEmail = fields.get("EmailAddress");
        }

        if(isUserHasOrgId(leadPhone, leadEmail))
            return false;

        createLeadIfDeleted(leadEmail, leadPhone);

        // if phone is empty, fetch user by email
        if (StringUtils.isEmpty(leadPhone)) {
            if (StringUtils.isNotEmpty(leadEmail)) {
                UserBasicInfo user = userUtils.getUserBasicInfoFromEmail(leadEmail, false);
                if (!Objects.equals(null, user) && StringUtils.isNotEmpty(user.getContactNumber())) {
                    // set phone to fetch an already existing lead (if present)
                    fields.put("Phone", user.getContactNumber());
                }
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Both phone and email are null");
            }
        }

        return postLeadData(url, HttpMethod.POST, LeadSquaredUtils.createJSONObjectLeadActivity(fields),
                leadActivity.getActivityEvent(), leadActivityDataType, fields.get(LeadDetails.Constants.EMAIL_ADDRESS),
                referenceNo, fields.get(LeadActivity.Constants.ACTIVITY_NOTE));
    }

    private boolean postLeadActivitywithCustomFields(LeadActivity leadActivity, LeadSquaredDataType leadActivityDataType,
                                                     String referenceNo, HttpMethod httpMethod, Map<String, String> custom)
            throws Exception {
        String url = getLeadSquaredURL(LS_ACTIVITY_URL);
        logger.info("postLeadActivity", new Object[]{url, httpMethod, leadActivity});
        Map<String, String> fields = leadActivity.getFieldMap();

        if (!custom.isEmpty()) {
            String customFieldsString = LeadSquaredUtils.createLeadActivityCustomFields(custom);
        }

        String leadPhone = null;
        String leadEmail = null;
        if (fields.containsKey("Phone")) {
            leadPhone = fields.get("Phone");
        }
        if (fields.containsKey("EmailAddress")) {
            leadEmail = fields.get("EmailAddress");
        }

        if(isUserHasOrgId(leadPhone, leadEmail))
            return false;

        createLeadIfDeleted(leadEmail, leadPhone);

        return postLeadData(url, HttpMethod.POST, LeadSquaredUtils.createJSONObjectLeadActivity(fields),
                leadActivity.getActivityEvent(), leadActivityDataType, fields.get(LeadDetails.Constants.EMAIL_ADDRESS),
                referenceNo, fields.get(LeadActivity.Constants.ACTIVITY_NOTE));
    }

    private boolean isUserHasOrgId(String contactNumber, String email){
        UserBasicInfo userBasicInfo = null;

        if(StringUtils.isNotEmpty(email))
            userBasicInfo = userUtils.getUserBasicInfoFromEmail(email,false);

        if(Objects.equals(null, userBasicInfo) && StringUtils.isNotEmpty(contactNumber))
            userBasicInfo = userUtils.getUserBasicInfoFromContactNumber(contactNumber);

        return !Objects.isNull(userBasicInfo) && StringUtils.isNotEmpty(userBasicInfo.getOrgId());

    }

    private PlatformBasicResponse syncAllUserLeadSquaredData(LeadSquaredRequest reqPojo) {
        // logger.info("syncAllUserLeadSquaredData " + reqPojo);
        // PlatformBasicResponse res = new PlatformBasicResponse();
        // try {
        // // IUserDAO userDAO = DAOFactory.INSTANCE.getUserDAO();
        // Map<String, String> params = reqPojo.getParams();
        // Long size;
        // String fetchSize = params.get("fetchSize");
        // if (StringUtils.isNotEmpty(fetchSize)) {
        // size = Long.valueOf(fetchSize);
        // } else {
        // // If no fetch size set, use the fetch size configured in
        // // appengine.propertiesf
        // size = QUERY_FETCH_SIZE;
        // }
        //
        // Long start = Long.valueOf(0);
        // Long limit = size;
        // if (StringUtils.isNotEmpty(params.get(Constants.QUERY_START))
        // && StringUtils.isNotEmpty(params.get(Constants.QUERY_LIMIT))) {
        // start = Long.valueOf(params.get(Constants.QUERY_START));
        // limit = Long.valueOf(params.get(Constants.QUERY_LIMIT));
        // }
        // GetUsersReq req = new GetUsersReq();
        // req.setRole(Role.STUDENT);
        // req.setStart(start);
        // req.setLimit(limit);
        // // TODO: Cron/Migration related, implement it later
        // List<User> userResultSet = null;
        // // List<User> userResultSet = userManager.getUsers(Role.STUDENT,
        // // null, start, limit, null, null);
        // logger.info("syncAllUserLeadSquaredData", new Object[] { start,
        // limit, userResultSet.size() });
        // Map<String, String> fields;
        // if (userResultSet != null && !userResultSet.isEmpty()) {
        // List<User> users = new ArrayList<User>();
        // users.addAll(userResultSet);
        // for (User user : users) {
        // if (Role.STUDENT.equals(user.getRole())) {
        // logger.info("syncAllLeadSquaredData", user.getId());
        // fields = leadCreate(new LeadDetails(user));
        // leadUpsert(fields);
        // wait(100);
        // }
        // }
        // start += size;
        // limit += size;
        // /*
        // * Queue queue = QueueFactory.getQueue("async-tasks-queue");
        // * TaskOptions taskOptions =
        // * TaskOptions.Builder.withUrl("/tasks/processTask");
        // */
        // reqPojo.getParams().put(Constants.QUERY_START,
        // String.valueOf(start));
        // reqPojo.getParams().put(Constants.QUERY_LIMIT,
        // String.valueOf(limit));
        // reqPojo.getParams().put("fetchSize", size.toString());
        // executeAsyncTask(reqPojo);
        // }
        // res.setSuccess(true);
        // } catch (Exception ex) {
        // logger.error("Error ", ex);
        // res.setSuccess(false);
        // }
        // logger.info("syncAllUserLeadSquaredData", res);
        // return res;
        return new PlatformBasicResponse();
    }

    private PlatformBasicResponse syncAllSessionLeadSquaredData(LeadSquaredRequest reqPojo) {
        // logger.info("syncAllSessionLeadSquaredData " + reqPojo);
        // PlatformBasicResponse res = new PlatformBasicResponse();
        // try {
        // Map<String, String> params = reqPojo.getParams();
        // Long size;
        // String fetchSize = params.get("fetchSize");
        // if (StringUtils.isNotEmpty(fetchSize)) {
        // size = Long.valueOf(fetchSize);
        // } else {
        // // If no fetch size set, use the fetch size configured in
        // // appengine.propertiesf
        // size = QUERY_FETCH_SIZE;
        // }
        // Long start = Long.valueOf(0);
        // Long limit = size;
        // if (StringUtils.isNotEmpty(params.get(Constants.QUERY_START))
        // && StringUtils.isNotEmpty(params.get(Constants.QUERY_LIMIT))) {
        // start = Long.valueOf(params.get(Constants.QUERY_START));
        // limit = Long.valueOf(params.get(Constants.QUERY_LIMIT));
        // }
        //
        // // TODO: Discuss and implement when sessions are moved
        // List<Session> sessions = null;
        // // List<Session> sessions = sessionManager.getSessionsByStates(null,
        // // null,
        // // Long.valueOf(start), Long.valueOf(limit));
        // logger.info("syncAllUserLeadSquaredData", new Object[] { start,
        // limit, sessions.size() });
        // if (sessions != null && !sessions.isEmpty()) {
        // // List<Session> sessions = new ArrayList<Session>();
        // // sessions.addAll(sessionResultSet);
        // Map<String, String> fields = new HashMap<String, String>();
        // String eventCode =
        // ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.session.scheduled");
        // fields.put("ActivityEvent", eventCode);
        // SessionState sessionState;
        // User student;
        // User teacher;
        // LeadActivity leadActivity;
        // for (Session session : sessions) {
        // logger.info("syncAllSessionLeadSquaredData", session);
        // sessionState = session.getState();
        // if (!SessionState.CANCELED.equals(sessionState) &&
        // !SessionState.EXPIRED.equals(sessionState)) {
        // student = userManager.getUserById(session.getStudentId());
        // teacher = userManager.getUserById(session.getTeacherId());
        // if (student != null && teacher != null) {
        // logger.info("syncAllSessionLeadSquaredData", session.getId());
        // leadActivity = new LeadActivity(eventCode, student.getEmail(),
        // System.currentTimeMillis(),
        // null);
        // leadActivity.setActivityNote(
        // " for session with " + teacher._getFullName() + "(" +
        // teacher.getEmail() + ") at "
        // + LeadSquaredUtils.getLeadSquaredDate(session.getStartTime()));
        // postLeadActivity(leadActivity,
        // LeadSquaredDataType.LEAD_ACTIVITY_SESSION, null,
        // HttpMethod.POST);
        // wait(100);
        // }
        // }
        // }
        // start += size;
        // limit += size;
        // /*
        // * Queue queue = QueueFactory.getQueue("async-tasks-queue");
        // * TaskOptions taskOptions =
        // * TaskOptions.Builder.withUrl("/tasks/processTask");
        // */
        // reqPojo.getParams().put(Constants.QUERY_START,
        // String.valueOf(start));
        // reqPojo.getParams().put(Constants.QUERY_LIMIT,
        // String.valueOf(limit));
        // reqPojo.getParams().put("fetchSize", size.toString());
        // executeAsyncTask(reqPojo);
        // }
        // } catch (Exception ex) {
        // ex.printStackTrace();
        // res.setSuccess(false);
        // }
        // logger.info("syncAllSessionLeadSquaredData", res);
        // return res;
        return new PlatformBasicResponse();
    }

    // public void retryLeadSquaredData(LeadSquaredData leadSquaredData)
    // throws IllegalArgumentException, IllegalAccessException,
    // BadRequestException, IOException, VException {
    // if (leadSquaredData == null) {
    // return;
    // }
    //
    // logger.info("retryLeadSquaredData", leadSquaredData.getId());
    // LeadSquaredDataType dataType = leadSquaredData.getData();
    // String json = "";
    // String url = "";
    // Map<String, String> fields;
    // LeadActivity leadActivity;
    // switch (dataType) {
    // case LEAD_UPSERT:
    // try {
    // User user =
    // userManager.getUserById(Long.parseLong(leadSquaredData.getReferenceId()));
    // if (user != null) {
    // fields = leadCreate(user);
    // if (fields != null && !fields.isEmpty()) {
    // json = LeadSquaredUtils.createJSONObjectLeadDetails(fields);
    // url = getLeadSquaredURL(LS_UPSERT_URL);
    // if ("true".equals(fields.get("mx_featureHide"))) {
    // url = getLeadSquaredURL(LS_UPSERT_URL);
    // }
    // } else {
    // return;
    // }
    // }
    // } catch (Exception ex) {
    // return;
    // }
    //
    // break;
    // case LEAD_ACTIVITY_SESSION:
    // case LEAD_ACTIVITY_PROPOSAL:
    // case LEAD_ACTIVITY_MESSAGE:
    // case LEAD_ACTIVITY_QUERY:
    // leadActivity = new LeadActivity(leadSquaredData.getActivityEvent(),
    // leadSquaredData.getEmailAddress(),
    // leadSquaredData.getCreationTime(), leadSquaredData.getNote());
    // fields = leadActivity.getFieldMap();
    // if (fields != null && !fields.isEmpty()) {
    // json = LeadSquaredUtils.createJSONObjectLeadActivity(fields);
    // url = getLeadSquaredURL(LS_UPSERT_URL);
    // } else {
    // return;
    // }
    //
    // url = getLeadSquaredURL(LS_ACTIVITY_URL);
    // break;
    // case LEAD_TOS_UPDATE:
    // break;
    // default:
    // break;
    // }
    //
    // boolean isSuccess = LeadSquaredUtils.postLeadData(url, HttpMethod.POST,
    // json);
    // if (!isSuccess) {
    // leadSquaredData.incrementRetryCount();
    // } else {
    // leadSquaredData.setStatus(DatabaseState.INACTIVE);
    // }
    // leadSquaredDataDAO.create(leadSquaredData);
    // }
    public ClientResponse postLeadData(String location, HttpMethod httpMethod, String json)
            throws IOException, IllegalArgumentException, IllegalAccessException, BadRequestException, VException {
        ClientResponse resp = null;
        if(location.contains(Constants.BULK_CHECK)){
            //changed to routing url as per threshold
            resp = lsWebUtils.leadsquaredDoBulkCallWithRetries(location, httpMethod, json, false);
        } else{
            resp = WebUtils.INSTANCE.doCall(location, httpMethod, json, false);
        }
        return resp;
    }

    public void reviseIndiaActivityPush(List<ReviseIndiaReq> request) {

        for(ReviseIndiaReq req:request) {
            awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.REVISE_INDIA, gson.toJson(req), null);
        }
    }

    public static class Constants {

        public static final String QUERY_START = "queryStart";
        public static final String QUERY_LIMIT = "queryLimit";
        public static final String CALLING_USERID = "callingUserId";
        private static final String BULK_CHECK = "/Bulk/";
    }

    private String getCoursePlanUpdatetActivityNote(CoursePlanInfo coursePlanInfo) {
        String coursePlanStr = "CoursePlanUpdate - course name:" + coursePlanInfo.getTitle();
        coursePlanStr += getCoursePlanUpdateStr(coursePlanInfo.getParentCourseId(),
                coursePlanInfo.getTrialRegistrationFee(), coursePlanInfo.getState(),
                coursePlanInfo.getTrialSessionSchedule());
        coursePlanStr += " CoursePlanState:" + coursePlanInfo.getState();
        if (coursePlanInfo.getEndType() != null) {
            coursePlanStr += " CoursePlanEndType:" + coursePlanInfo.getEndType();
        }

        coursePlanStr += " RefUrl:" + getCoursePlanUrl(coursePlanInfo.getId());
        coursePlanStr = coursePlanStr.replaceAll("  ", " ");
        return coursePlanStr;
    }

    private static String getCoursePlanUpdateStr(String parentCourseId, Integer trialRegistrationFee,
                                                 CoursePlanState coursePlanState, SessionSchedule trialSchedule) {
        String remarks = "";
        List<CoursePlanState> trialPaymentStates = new ArrayList<CoursePlanState>();
        trialPaymentStates.add(CoursePlanState.TRIAL_PAYMENT_DONE);
        trialPaymentStates.add(CoursePlanState.ENROLLED);
        trialPaymentStates.add(CoursePlanState.ENDED);

        if (!StringUtils.isEmpty(parentCourseId) && (trialRegistrationFee != null && trialRegistrationFee > 0)
                && trialPaymentStates.contains(coursePlanState) && trialSchedule != null) {
            remarks += "StructuredCourse, Registration already taken on Course Page - Join Course will schedule Trial Session";
        }

        if (!StringUtils.isEmpty(parentCourseId) && trialRegistrationFee != null && trialRegistrationFee > 0
                && !trialPaymentStates.contains(coursePlanState) && trialSchedule != null) {
            remarks += "StructuredCourse, Registration on Course Page (Pay and Join) will schedule Trial Session";
        }

        if (!StringUtils.isEmpty(parentCourseId) && (trialRegistrationFee == null || trialRegistrationFee == 0)
                && !trialPaymentStates.contains(coursePlanState) && trialSchedule != null) {
            remarks += "StructuredCourse, Registration not taken on Course Page - Join Course will schedule FREE Trial Sessions";
        }

        if (!StringUtils.isEmpty(parentCourseId) && (trialRegistrationFee == null || trialRegistrationFee == 0)
                && !trialPaymentStates.contains(coursePlanState) && trialSchedule == null) {
            remarks += "RSCase - Structured Course, Registration not taken on Course Page - Pay instalment (or Course Fee) to schedule Regular Sessions";
        }

        if (StringUtils.isEmpty(parentCourseId) && trialRegistrationFee != null && trialRegistrationFee > 0
                && !trialPaymentStates.contains(coursePlanState) && trialSchedule != null) {
            remarks += "UnstructuredCourse, Registration on Course Page (Pay and Join) will schedule Trial Session";
        }

        if (StringUtils.isEmpty(parentCourseId) && (trialRegistrationFee == null || trialRegistrationFee == 0)
                && !trialPaymentStates.contains(coursePlanState) && trialSchedule != null) {
            remarks += "UnstructuredCourse - Join Course will schedule FREE Trial Sessions";
        }

        if (StringUtils.isEmpty(parentCourseId) && (trialRegistrationFee == null || trialRegistrationFee == 0)
                && !trialPaymentStates.contains(coursePlanState) && trialSchedule == null) {
            remarks += "RSCase - Unstructured Course - Pay instalment (or Course Fee) to schedule Regular Sessions";
        }
        return remarks;
    }

    public String getCoursePlanUrl(String courseId) {
        // https://fos-qa.vedantu.com/v/courseplan/58f5c2a4e4b032e213c3d2e2
        return ConfigUtils.INSTANCE.getStringValue("url.base") + "/courseplan/" + courseId;
    }

    public void getLeadDetailsByEmailAndSetUser(String email) throws BadRequestException, ConflictException {
        logger.info("IN_getLeadDetailsByEmailAndSetUser");
        List<LeadDetailsSerialized> responseLeads = new ArrayList<>();

        //https://api.leadsquared.com/v2/LeadManagement.svc/Leads.GetByEmailaddress?accessKey=u$rf38c382db4c070e0377099379a443f1e&secretKey=d6d25b88a2f30274c72e3c561b5f68ccbcfc7111&emailaddress=shahidatabassum103@gmail.com
        String serverUrl = getLeadSquaredURL(LS_FETCH_BYEMAIL_URL);
        serverUrl += "&emailaddress=" + email;

        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET,
                null, true);

        logger.info("cRes:" + cRes.getStatus());
        if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
            logger.warn(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users by email not 200 - " + cRes.getStatus() + " cRes:"
                    + cRes.toString());
            return;
        } else if (cRes.getStatus() == 429) {
            logger.info(LoggingMarkers.JSON_MASK,"TooManyRequests :" + serverUrl + " " + email);
            throw new BadRequestException(ErrorCode.TOO_MANY_REQUESTS, "Too many Requests to LeadSquare. Try Again in SomeTime", Level.INFO);
        }

        String response = cRes.getEntity(String.class);
        Type leadDetailsListType = new TypeToken<List<LeadDetailsSerialized>>() {
        }.getType();
        responseLeads = gson.fromJson(response, leadDetailsListType);

        SaleClosedUser sc = new SaleClosedUser();
        if (responseLeads != null && responseLeads.size() > 0) {
            sc.setOwnerEmail(responseLeads.get(0).getOwnerIdEmailAddress());
            sc.setOwnerName(responseLeads.get(0).getOwnerIdName());
        }
        sc.setUserEmail(email);

        // Get webinarID
        UserBasicInfo userBasicInfo = userUtils.getUserBasicInfoFromEmail(email,false);
        WebinarUserRegistrationInfo webinar = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserIdOnly(userBasicInfo.getUserId().toString());
        if (webinar != null) {
            sc.setLastWebinarAttended(webinar.getWebinarId());
        }

        // Update User
        String url = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT") + "/updateUserSaleClosed";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST,
                gson.toJson(sc), true);

    }

    public List<LeadDetailsSerialized> getLeadDetails(String parameter, List<Long> keys) throws BadRequestException{

        List<LeadDetailsSerialized> responseLeads = new ArrayList<>();
        String serverUrl = getLeadSquaredURL(LS_FETCH_DETAILS_URL);
        String requestPojo = getJsonStringToQueryDetails(parameter, keys);
        if (StringUtils.isEmpty(requestPojo)) {
            return responseLeads;
        }
        ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, requestPojo, false);
        logger.info("cRes:" + cRes.getStatus());
        if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
            logger.warn(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                    + cRes.toString());
            return responseLeads;
        } else if (cRes.getStatus() == 429) {
            logger.info(LoggingMarkers.JSON_MASK,"TooManyRequests :" + serverUrl + " " + requestPojo);
            throw new BadRequestException(ErrorCode.TOO_MANY_REQUESTS, "Too many Requests to LeadSquare. Try Again in SomeTime", Level.INFO);
        }

        String response = cRes.getEntity(String.class);
        Type leadDetailsListType = new TypeToken<List<LeadDetailsSerialized>>() {
        }.getType();
        responseLeads = gson.fromJson(response, leadDetailsListType);

        return responseLeads;
    }

    public String getJsonStringToQueryDetails(String name, List<Long> keys) {
        String keyString = commaSeparatedStringForLong(keys);
        if (StringUtils.isEmpty(keyString)) {
            return null;
        }
        String queryString = "{\"Columns\": {\"Include_CSV\": \"\"},\"Parameter\":{\"LookupName\":\"" + name + "\",\"LookupValue\":\"" + keyString + "\",\"SqlOperator\":\"IN\"},\"Sorting\":{\"ColumnName\":\"CreatedOn\",\"Direction\":\"1\"},\"Paging\":{\"PageIndex\":1,\"PageSize\":100}}";
        return queryString;
    }

    public String commaSeparatedStringForLong(List<Long> keys) {
        if (ArrayUtils.isEmpty(keys)) {
            return null;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < keys.size(); i++) {

            builder.append(keys.get(i));
            if (keys.size() - 1 != i) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public List<LeadActivitySerialized> getLeadActivity(String leadId, Integer activityCode) throws InterruptedException, BadRequestException, ConflictException {

        List<LeadActivitySerialized> responseLeads = new ArrayList<>();
        String serverUrl = getLeadSquaredURL(LS_GET_ACTIVITY_URL);
        serverUrl = serverUrl + "&leadId=" + leadId;
        if (activityCode == null) {
            activityCode = 3002;
        }
        String requestPojo = "{\"Parameter\":{\"ActivityEvent\":" + activityCode + "}}";
        if (StringUtils.isEmpty(requestPojo)) {
            return responseLeads;
        }

        for(int i = 0; i < 3; i++){

            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.POST, requestPojo, false);
            logger.info("cRes:" + cRes.getStatus());
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());

                break;
            } else if (cRes.getStatus() == 429) {
                logger.warn(LoggingMarkers.JSON_MASK,"TooManyRequests :" + serverUrl + " " + requestPojo);
                Thread.sleep(1000);
                continue;
            }

            String response = cRes.getEntity(String.class);
            GetLeadActivityResp getLeadActivityResp = gson.fromJson(response, GetLeadActivityResp.class);
            if (getLeadActivityResp != null && ArrayUtils.isNotEmpty(getLeadActivityResp.getProspectActivities())) {
                return getLeadActivityResp.getProspectActivities();
            }

            break;

        }

        return responseLeads;

    }


    public List<LeadSquaredAgent> getLeadAgents(List<Long> studentIds) throws BadRequestException, InterruptedException, ConflictException {
        List<LeadSquaredAgent> leadSquaredAgents = new ArrayList<>();
        if (ArrayUtils.isEmpty(studentIds)) {
            return leadSquaredAgents;
        }
        Set<Long> userIds = new HashSet<>();
        List<Long> toFetch = new ArrayList<>();
        List<LeadSquaredAgent> leadAgentsDB = leadSquaredAgentDAO.getByUserIds(studentIds);
        if (ArrayUtils.isNotEmpty(leadAgentsDB)) {
            for (LeadSquaredAgent agent : leadAgentsDB) {
                userIds.add(agent.getUserId());
                leadSquaredAgents.add(agent);
            }
        }
        for (Long id : studentIds) {
            if (!userIds.contains(id)) {
                toFetch.add(id);
            }
        }

        Map<Long, String> userLeadMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(toFetch)) {
            logger.info(toFetch.toString());
            List<LeadDetailsSerialized> leadDetails = getLeadDetails("mx_User_Id", toFetch);
            if (ArrayUtils.isNotEmpty(leadDetails)) {
                for (LeadDetailsSerialized lead : leadDetails) {
                    if (StringUtils.isNotEmpty(lead.getMx_User_Id())) {
                        userLeadMap.put(Long.parseLong(lead.getMx_User_Id()), lead.getProspectID());
                        logger.info("Fetch lead for userId : " + lead.getMx_User_Id() + " prospectId : " + lead.getProspectID());
                    }
                }
                List<LeadSquaredAgent> agentsToStore = getLeadAgents(userLeadMap);
                if (ArrayUtils.isNotEmpty(agentsToStore)) {
                    leadSquaredAgents.addAll(agentsToStore);
                }
            }
        }
        return leadSquaredAgents;
    }

    public List<LeadSquaredAgent> getLeadAgents(Map<Long, String> userLeadMap) throws ConflictException {
        List<LeadSquaredAgent> leadSquaredAgents = new ArrayList<>();
        List<LeadSquaredAgent> agentsToStore = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();
        if (userLeadMap != null && !userLeadMap.isEmpty()) {
            for (Long userId : userLeadMap.keySet()) {
                try {
                    LeadSquaredAgent leadSquaredAgent = getLeadAgentFromLeadSquared(userLeadMap.get(userId));
                    if (leadSquaredAgent != null && StringUtils.isNotEmpty(leadSquaredAgent.getAgentName())) {
                        leadSquaredAgent.setUserId(userId);
                        agentsToStore.add(leadSquaredAgent);
                    }
                } catch (BadRequestException | InterruptedException e) {
                    // TODO Auto-generated catch block
                    logger.info("lead Squared Error" + e);
                    continue;
                } catch (ConflictException e) {
                    if (ArrayUtils.isNotEmpty(agentsToStore)) {
                        leadSquaredAgentDAO.updateAll(agentsToStore);
                        leadSquaredAgents.addAll(agentsToStore);
                    }
                    throw new ConflictException(ErrorCode.TOO_MANY_REQUESTS, "Too many request to LeadSquared");
                }
            }
            if (ArrayUtils.isNotEmpty(agentsToStore)) {
                leadSquaredAgentDAO.updateAll(agentsToStore);
                leadSquaredAgents.addAll(agentsToStore);
            }
        }
        return leadSquaredAgents;
    }

    public LeadSquaredAgent getLeadAgentFromLeadSquared(String leadId) throws BadRequestException, InterruptedException, ConflictException {
        LeadSquaredAgent leadSquaredAgent = new LeadSquaredAgent();
        if (StringUtils.isNotEmpty(leadId)) {
            List<LeadActivitySerialized> leadActivities = getLeadActivity(leadId, null);
            if (ArrayUtils.isNotEmpty(leadActivities)) {
                for (LeadActivitySerialized activity : leadActivities) {
                    if (activity != null && ArrayUtils.isNotEmpty(activity.getData())) {
                        String ownerNameTemp = null;
                        Boolean wonDeal = Boolean.FALSE;
                        for (KeyValueObject keyValue : activity.getData()) {
                            if (StringUtils.isNotEmpty(keyValue.getKey()) && StringUtils.isNotEmpty(keyValue.getValue())) {
                                if (keyValue.getKey().equalsIgnoreCase("CurrentStage") && keyValue.getValue().equalsIgnoreCase("Sales- Won Deal")) {
                                    wonDeal = Boolean.TRUE;
                                }
                                if (keyValue.getKey().equalsIgnoreCase("CreatedBy")) {
                                    ownerNameTemp = keyValue.getValue();
                                }
                            }
                        }
                        if (wonDeal) {
                            leadSquaredAgent.setAgentName(ownerNameTemp);
                        }
                    }
                }
            }
            leadSquaredAgent.setProspectId(leadId);
        }
        if (StringUtils.isEmpty(leadSquaredAgent.getAgentName())) {
            logger.info("LeadSquared Agent - No won deal for prospectID : " + leadId);
            leadSquaredAgent.setAgentName("Admin");
        }
        return leadSquaredAgent;
    }

    private void leadAcitivityRegisterStudent(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.register");

        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity.setPhone(params.get("mobileNumber"));
        StringBuilder sb = new StringBuilder();
        sb.append("Registration Details : ");
        sb.append("userEmail: ").append(params.get("userEmail"));

        if (params.get("entityType") != null) {
            sb.append(" , entityType: ").append(params.get("entityType"));
        }

        if (params.get("entityId") != null) {
            sb.append(" , Id: ").append(params.get("entityId"));
        }
        if (params.get("entityTitle") != null) {
            sb.append(" , entityTitle: ").append(params.get("entityTitle"));
        }
        if (params.get("note") != null) {
            sb.append(" , note: ").append(params.get("note"));
        }
        if (params.get("timing") != null) {
            sb.append(" , timing: ").append(params.get("timing"));
        }

        Map<String, String> custom = new HashMap<>();
        custom.put("mx_Custom_1", "" + params.get("entityTitle"));
        custom.put("mx_Custom_2", "" + params.get("entityType"));
        custom.put("mx_Custom_3", "" + params.get("paidAmount"));
        leadActivity.setActivityNote(sb.toString());
        postLeadActivitywithCustomFields(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_REGISTER, null, HttpMethod.POST, custom);
    }

    private void leadAcitivityEnrollRegularStudent(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.enroll_regular");

        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity.setPhone(params.get("mobileNumber"));
        StringBuilder sb = new StringBuilder();
        sb.append("Enrollment Details : ");
        sb.append("userEmail: ").append(params.get("userEmail"));

        if (params.get("entityType") != null) {
            sb.append(" , entityType: ").append(params.get("entityType"));
        }

        if (params.get("entityId") != null) {
            sb.append(" , Id: ").append(params.get("entityId"));
        }
        if (params.get("entityTitle") != null) {
            sb.append(" , entityTitle: ").append(params.get("entityTitle"));
        }
        if (params.get("note") != null) {
            sb.append(" , note: ").append(params.get("note"));
        }
        if (params.get("timing") != null) {
            sb.append(" , timing: ").append(params.get("timing"));
        }

        Map<String, String> custom = new HashMap<>();
        custom.put("mx_Custom_1", params.get("entityTitle") == null ? "" : params.get("entityTitle"));
        custom.put("mx_Custom_2", params.get("entityType") == null ? "" : params.get("entityType"));
        custom.put("mx_Custom_3", "" + params.get("paidAmount") == null ? "" : params.get("paidAmount"));

        leadActivity.setActivityNote(sb.toString());
        postLeadActivitywithCustomFields(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_ENROLL, null, HttpMethod.POST, custom);
    }

    public void pushLeadSquaredUsers(Long fromTime, Long tillTime, Integer maxSize) throws Exception {

        if (fromTime == null || tillTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "BadRequestException");
        }
        if (maxSize == null || maxSize <= 0) {
            maxSize = 100;
        }
        int start = 0;
        int size = 60;
        int counter = 0;
        Long creationTime = fromTime;
        Long userId = tillTime;
        while (true) {
            List<User> users = getUsers(fromTime, tillTime, start, size);
            if (ArrayUtils.isNotEmpty(users)) {
                for (User user : users) {
                    counter++;
                    logger.info("userId" + user.getId() + " counter " + counter);
                    if (counter % 5 == 0) {
                        Thread.sleep(5000);
                    }
                    LeadSquaredRequest request = new LeadSquaredRequest();
                    request.setUser(user);
                    try {
                        leadCreate(request);
                    } catch (BadRequestException e){
                        throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
                    } catch (Exception e) {
                        logger.warn("Error in leadCreation for user : " + user.getId() + " . error " + e);
                        throw new Exception(e);
                    }

                    creationTime = user.getCreationTime();
                    userId = user.getId();

                }
            } else {
                logger.info("Total Count : " + counter);
                break;
            }
            start = start + size;
            if (counter > maxSize) {
                logger.info("userId" + userId + " counter " + counter + "creationTime : " + creationTime);
                break;
            }
        }

    }

    public List<User> getUsers(Long fromTime, Long tillTime, int start, int size) throws VException {
        String url = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT") + "/fetchUsersByCreationTimeRandomStringadsfdcfvdfdc?size=" + size;
        url += "&start=" + start + "&fromTime=" + fromTime + "&tillTime=" + tillTime + "&role=" + Role.STUDENT;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type filterListType = new TypeToken<List<User>>() {
        }.getType();
        List<User> response = new Gson().fromJson(jsonString, filterListType);
        return response;

    }

    public void handleGPSLocalesMessagesFromLeadSquare(List<QueueMessagePojo> pojos) throws Exception {
        List<LeadActivityReq> leadActivityReqs = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(pojos)) {
            for (QueueMessagePojo pojo : pojos) {
                if(pojo.getMessageType().equals(SQSMessageType.UPDATE_LEAD_GPS_LEADSQUARED) && StringUtils.isNotEmpty(pojo.getBody())){
                    LeadActivityReq req = createLeadActivityReq(pojo);
                    if (req != null) {
                        leadActivityReqs.add(req);
                    }
                }
            }
        }
        if (ArrayUtils.isNotEmpty(leadActivityReqs)) {
            postBulkLeadActivity(leadActivityReqs, HttpMethod.POST);
        }
    }

    public void handleMessagesFromLeadSquare(List<QueueMessagePojo> pojos)
            throws Exception {

        List<QueueMessagePojo> updatePojos = new ArrayList<>();
        List<LeadActivityReq> createReqs = new ArrayList<>();
        List<UpdateLeadActivityReq> updateReqs = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(pojos)) {
            for (QueueMessagePojo pojo : pojos) {

                if (newMessageTypes.contains(pojo.getMessageType())) {
                    // Calling this one automatically handles the leadsquare calls, so the method will
                    // return null, so nothing needed to be handled in this end
                    LeadActivityReq req = createLeadActivityReq(pojo);
                    logger.info("Calling for the new message type " + pojo.getMessageType());
                    if (req != null) { // Added to handle null pointer exception of lead square
                        createReqs.add(req);
                    }
                } else {
                    logger.info("Calling for the other message type " + pojo.getMessageType());
                    updatePojos.add(pojo);
                }
            }
            if (ArrayUtils.isNotEmpty(updatePojos)) {
                UpdateLeadActivitiesRes res = updateLeadActivityReq(updatePojos);
                if (ArrayUtils.isNotEmpty(res.getCreateActivetityReqs())) {
                    createReqs.addAll(res.getCreateActivetityReqs());
                }

                if(CollectionUtils.isNotEmpty(res.getUpdateReqs())) {
                    updateReqs.addAll(res.getUpdateReqs());
                }
            }
            if (ArrayUtils.isNotEmpty(createReqs)) {
                postBulkLeadActivity(createReqs, HttpMethod.POST);
            }

            if (ArrayUtils.isNotEmpty(updateReqs)) {
                updateBulkActivity(updateReqs);
            }

        }

    }

    public LeadActivityReq createLeadActivityReq(QueueMessagePojo pojo)
            throws Exception {
        return createLeadActivityReq(pojo.getMessageType(), pojo.getBody());
    }

    public LeadActivityReq createLeadActivityReq(SQSMessageType messageType, String objString)
            throws Exception {
        LeadActivityReq req = new LeadActivityReq();
        Integer eventCode;
        req.addActivityTime(System.currentTimeMillis());
        switch (messageType) {
            case ORDER_SAVED_LEADSQUARE:
            case ORDER_UPDATED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.order");
                req.setActivityEvent(eventCode);
                Orders order = gson.fromJson(objString, Orders.class);
                req.setUserId(order.getUserId().toString());
                req.setActivityNote(order.getId());
                req.setFields(getOrderCustomValues(order));
                logger.info("ORDER_SAVED_LEADSQUARE_PEOPLE : {}", order.getId());
                break;
            case INSTALMENT_SAVED:
            case INSTALMENT_UPDATED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.instalment");
                req.setActivityEvent(eventCode);
                InstalmentInfo instalment = gson.fromJson(objString, InstalmentInfo.class);
                req.setUserId(instalment.getUserId().toString());
                req.setActivityNote(instalment.getId());

                req.setFields(getInstalmentCustomValues(instalment));
                break;
            case COURSEPLAN_NEW_LEADSQUARE:
            case COURSEPLAN_UPDATED_LEADSQUARE:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.courseplan");
                req.setActivityEvent(eventCode);
                CoursePlanInfo coursePlan = gson.fromJson(objString, CoursePlanInfo.class);
                req.setUserId(coursePlan.getStudentId().toString());
                req.setActivityNote(coursePlan.getId());

                req.setFields(getCoursePlanCustomValues(coursePlan));
                break;
            case ENROLLMENT_CREATED:
            case ENROLLMENT_UPDATED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.enrollment");
                req.setActivityEvent(eventCode);

                EnrollmentPojo enrollment = gson.fromJson(objString, EnrollmentPojo.class);
                req.setUserId(enrollment.getUserId());
                req.setActivityNote(enrollment.getId());
                req.setFields(getEnrollmentCustomValues(enrollment));
                break;
            case STUDENT_ENROLLED:
                Orders orderStudent = gson.fromJson(objString, Orders.class);
                if (orderStudent != null && orderStudent.getAmount() != null && (orderStudent.getAmount() / 100) >= 14999) {
                    setFieldsForStudentEnrolled(req, orderStudent);
                } else {
                    logger.info("Amount is less than 14999 in order");
                    return null;
                }
                logger.info("req: {}",req);
                break;
            case WEBINAR_MOBILE_CALLBACK:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.webinar_mobile_callback");
                req.setActivityEvent(eventCode);
                WebinarCallbackData webinarCallbackData = gson.fromJson(objString, WebinarCallbackData.class);
                logger.info("WEBINAR_MOBILE_CALLBACK. userId={}, webinarId={}", webinarCallbackData.getUserId(), webinarCallbackData.getWebinarId());
                req.setUserId(webinarCallbackData.getUserId().toString());
                req.setFields(getWebinarMobileCallbackFields(webinarCallbackData));
                break;
            case WEBINAR_REGISTERED:
                //NOTE: Not pushing any webinar registered activities

                /*eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.webinar_registered");
                req.setActivityEvent(eventCode);
                WebinarUserRegistrationInfo wrInfo = gson.fromJson(objString, WebinarUserRegistrationInfo.class);
                logger.info("WEBINAR_REGISTERED_LS: " + wrInfo);
                req.setUserId(wrInfo.getUserId());
                req.setEmailAddress(wrInfo.getEmailId());
                req.setPhone(wrInfo.getPhone());
                // Construct note
                String wrNote = "WebinarRegistration - userFirstName:" + wrInfo.getFirstName()
                        + ", userLastName:" + wrInfo.getLastName() + " userEmail:"
                        + wrInfo.getEmailId() + " userPhone:" + wrInfo.getPhone()
                        + ".";
                wrNote += "WebinarInfo - webinarId:" + wrInfo.getWebinarId() + " webinarSubject:" + wrInfo.isWebinarAttended();
                req.setActivityNote(wrNote);
                req.setFields(getWebinarRegisteredValues(wrInfo));

                logger.info("Request: " + req);
                break;*/
                return null;
            case WEBINAR_HOMEDEMO_LEADSQUARED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.webinar_homedemo");
                req.setActivityEvent(eventCode);
                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> data = gson.fromJson(objString, type);

                if(data.containsKey("isMigration")){
                    LeadDetails leadDetails = fetchLeadByPhone(data.get("phone"));
                    if(leadDetails != null){
                        List<LeadActivitySerialized> activities = getLeadActivity(leadDetails.getProspectID(), eventCode);

                        for(LeadActivitySerialized activity : activities){
                            List<KeyValueObject> dataObj = activity.getData();
                            List<KeyValueObject> containsData = dataObj.parallelStream()
                                    .filter(v -> v.getKey().equals("NotableEventDescription") && v.getValue().contains(data.get("sessionId"))).collect(Collectors.toList());
                            if(CollectionUtils.isNotEmpty(containsData)){
                                return null;
                            }
                        }
                    }
                }


                logger.info("WEBINAR_HOMEDEMO_LEADSQUARED: " + data);
                SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
                sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                String noteForHomeDemo = "Home Demo Webinar - webinarId: " + data.get("webinarId")
                        + "userEmail : " + data.get("emailAddress")
                        + " sessionId: " + data.get("sessionId")
                        + " timeInSession " + data.get("timeInSession")
                        + " sessionStartTime " + (StringUtils.isEmpty(data.get("sessionStartTime")) ? null : sdf.format(new Date(Long.parseLong(data.get("sessionStartTime")))))
                        + " sessionEndTime " + (StringUtils.isEmpty(data.get("sessionEndTime")) ? null : sdf.format(new Date(Long.parseLong(data.get("sessionEndTime")))));
                req.setActivityNote(noteForHomeDemo);
                if (StringUtils.isNotEmpty(data.get("userId"))) {
                    req.setUserId(data.get("userId"));
                }
                if (StringUtils.isNotEmpty(data.get("emailAddress"))) {
                    req.setEmailAddress(data.get("emailAddress"));
                }
                if (StringUtils.isNotEmpty(data.get("phone"))) {
                    req.setSearchByValue("Phone");
                    req.setPhone(data.get("phone"));
                }
                logger.info("Request: " + req);
                break;
            case SALES_DEMO_LEADSQUARED:
                salesDemoLeadSquared(objString, req);
                //NOTE : Ignoring as it is already pushed inside the method
                return null;
            case CUSTOMER_SIGNUP_SUPERKIDS:
                LeadSquaredRequest request = gson.fromJson(objString, LeadSquaredRequest.class);
                customerSignupLeadSquared(req,request);
                break;
            case BOOK_DEMO_SESSION_SUPERKIDS:
                LeadSquaredRequest createDemoRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                createSessionLeadSquared(req,createDemoRequest);
                break;
            case BOOK_DEMO_SESSION_SUPERKIDS_ALT_NUMBER:
                LeadSquaredRequest updateDemoRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                updateSessionLeadSquared(req,updateDemoRequest);
                return null;
            case CANCEL_SESSION_SUPERKIDS:
                LeadSquaredRequest cancelSessionRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                cancelSessionLeadSquared(req,cancelSessionRequest);
                break;
            case DEMO_SESSION_FINISHED_SUPERKIDS:
                LeadSquaredRequest sessionFinishedRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                createDemoSesionCompleteLeadSquared(req,sessionFinishedRequest);
                break;
            case COURSE_PAYMENT_INITIATION_SUPERKIDS:
                LeadSquaredRequest initiationPaymentRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                purchaseCourseInitiationLeadSquared(req,initiationPaymentRequest);
                break;
            case COURSE_PAYMENT_COMPLETION_SUPERKIDS:
                LeadSquaredRequest completePaymentRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                purchasedCourseLeadSquared(req,completePaymentRequest);
                break;
            case BOOKING_CANCELLED_BY_OVERBOOKING:
                LeadSquaredRequest overBookingRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                bookingCancelledByOverbooking(req,overBookingRequest);
                break;
            case WEBINAR_ATTENDED:

                //NOTE: Not pushing any webinar attended activities

                /*eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.webinar_attended");
                req.setActivityEvent(eventCode);
                WebinarUserRegistrationInfo waInfo = gson.fromJson(objString, WebinarUserRegistrationInfo.class);
                logger.info("WEBINAR_REGISTERED_LA: " + waInfo);
                req.setUserId(waInfo.getUserId());
                req.setEmailAddress(waInfo.getEmailId());
                req.setPhone(waInfo.getPhone());
                // Construct note
                String waNote = "WebinarRegistration - userFirstName:" + waInfo.getFirstName()
                        + ", userLastName:" + waInfo.getLastName() + " userEmail:"
                        + waInfo.getEmailId() + " userPhone:" + waInfo.getPhone()
                        + " timeInSession:" + waInfo.getTimeInSession() + " intervals:"
                        + (waInfo.getAttendanceIntervals() == null ? 0
                        : waInfo.getAttendanceIntervals().size())
                        + ".";

                waNote += "WebinarInfo - webinarId:" + waInfo.getWebinarId() + " webinarSubject:" + waInfo.isWebinarAttended();
                req.setActivityNote(waNote);
                req.setFields(getWebinarAttendedValues(waInfo));

                logger.info("Request: " + req);
                break;*/

                return null;
            case SALE_CLOSED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.saleclosed");
                req.setActivityEvent(eventCode);
                User user = gson.fromJson(objString, User.class);
                req.setUserId(user.getId().toString());
                req.setEmailAddress(user.getEmail());
                req.setPhone(user.getContactNumber());
                req.setActivityNote(user.getId().toString());
                req.setFields(getSaleClosedCustomValues(user.getSaleClosed()));
                getLeadDetailsByEmailAndSetUser(user.getEmail());
                break;
            case COURSE_PLAN_LESS_THAN_3HOURS:
                CoursePlanSQSAlert coursePlanSQSAlert = gson.fromJson(objString, CoursePlanSQSAlert.class);
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.courseplan_3hours");
                req.setActivityEvent(eventCode);
                if (coursePlanSQSAlert != null) {
                    req.setUserId(coursePlanSQSAlert.getUserId());
                    req.setFields(getLessThan3HourAlertValues(coursePlanSQSAlert));
                    //logger.info("Request: "+req);
                    return req;
                }
                break;
            case TRANSACTION_NEW_LS:
            case TRANSACTION_UPDATE_LS:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.transaction");
                req.setActivityEvent(eventCode);
                TransactionUserInfo transaction = gson.fromJson(objString, TransactionUserInfo.class);
                if (transaction != null && transaction.getUserBasicInfo() != null && transaction.getTransaction() != null) {
                    req.setUserId(transaction.getUserBasicInfo().getUserId().toString());
                    req.setPhone(transaction.getUserBasicInfo().getContactNumber());
                    req.setActivityNote(transaction.getTransaction().getId());
                    req.setFields(getTransactionCustomValues(transaction));
                    return req;
                } else {
                    logger.error("transaction not clear/ userId is null" + objString);
                    return null;
                }
            case TRIAL_REQUEST:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.trial_request");
                req.setActivityEvent(eventCode);
                TrialReq trialReq = gson.fromJson(objString, TrialReq.class);
                logger.info("trialReq. userId={}", trialReq.getUserId());
                req.setUserId(trialReq.getUserId());
                // req.setFields(getWebinarMobileCallbackFields(webinarCallbackData));
                break;
            case LS_TASK:
                LeadSquaredRequest leadSquaredRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                logger.info("LS_TASK calling as SQS instead of Async Task");
                try {
                    executeTask(leadSquaredRequest);
                } catch (BadRequestException e){
                    throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
                } catch (Exception e) {
                    logger.warn("Exception is executing leadsquare task ", e);
                    throw new Exception(e);
                }
                return null;
            case  UPDATE_LEAD_GPS_LEADSQUARED:
                Type objectType = new TypeToken<Map<String, String>>() {}.getType();
                Map<String, String> gpsLocalsData = gson.fromJson(objString, objectType);
                if(StringUtils.isNotEmpty(gpsLocalsData.get("userId"))){
                    req.setUserId(gpsLocalsData.get("userId"));
                    req.setFields(getSchemaValuepairForGPSLocales(gpsLocalsData));
                }
                break;
            case REVISE_INDIA:
                logger.info("REVISE_INDIA");
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.revise.india");
                req.setActivityEvent(eventCode);
                ReviseIndiaReq reviseIndiaReq = gson.fromJson(objString, ReviseIndiaReq.class);
                req.setUserId(reviseIndiaReq.getUserId());
                req.setActivityNote("Revise India Activity : ");
                req.setFields(reviseIndiaReq.getFields());
                req.setPhone(reviseIndiaReq.getPhone());
                logger.info("For revise India: {}",gson.toJson(req));
                break;
            case DEMO_REQUESTED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.jee.demo.request");
                req.setActivityEvent(eventCode);
                LeadActivityReq leadActivityReq = gson.fromJson(objString, LeadActivityReq.class);
                req.setActivityNote(leadActivityReq.getActivityNote());
                req.setEmailAddress(leadActivityReq.getEmailAddress());
                req.setPhone(leadActivityReq.getPhone());
                req.setFields(leadActivityReq.getFields());
                logger.info("DEMO_REQUESTED ", leadActivityReq);
                break;
            case ONLINE_COUNSELLING_SESSION_SCHEDULED:
                eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.counselling.session.scheduled");
                req.setActivityEvent(eventCode);
                LeadSquaredRequest counsellingSessionScheduledRequest = gson.fromJson(objString, LeadSquaredRequest.class);
                counsellingSessionScheduledLeadSquared(counsellingSessionScheduledRequest,req);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, String.format("UNKNOWN MESSAGE TYPE FOR SQS - %s", messageType)  );
        }
        return req;

    }

    private void bookingCancelledByOverbooking(LeadActivityReq activityRequest,LeadSquaredRequest req){

        Map<String,String> data = req.getParams();
        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.overbookingdemo.cancel");
        List<SchemaValuePair> pairs = new ArrayList<>();
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        logger.info("bookingCancelledByOverbooking Entered"+data.get(LeadSquaredRequest.Constants.STUDENT_ID));

        if(data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME)));
        }
        if(data.get(LeadSquaredRequest.Constants.COURSE_NAME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get(LeadSquaredRequest.Constants.COURSE_NAME)));
        }
        if(data.get(LeadSquaredRequest.Constants.BOOKING_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.BOOKING_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            pairs.add(new SchemaValuePair("Status", data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS)));
        }
        activityRequest.setFields(pairs);
        logger.info("bookingCancelledByOverbooking leadActivity"+activityRequest);
    }

    private void purchasedCourseLeadSquared(LeadActivityReq activityRequest,LeadSquaredRequest req) {

        Map<String,String> data = req.getParams();
        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.customerpayment.completed");
        List<SchemaValuePair> pairs = new ArrayList<>();
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        logger.info("purchasedCourseLeadSquared Entered"+data.get(LeadSquaredRequest.Constants.STUDENT_ID));

        if(data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME)));
        }
        if(data.get(LeadSquaredRequest.Constants.COURSE_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get(LeadSquaredRequest.Constants.COURSE_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.AIO_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.AIO_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.BATCH_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get(LeadSquaredRequest.Constants.BATCH_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            pairs.add(new SchemaValuePair("Status", data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS)));
        }
        activityRequest.setFields(pairs);
        logger.info("purchasedCourseLeadSquared leadactivity"+activityRequest);
    }

    private void purchaseCourseInitiationLeadSquared(LeadActivityReq activityRequest,LeadSquaredRequest req) {

        Map<String,String> data = req.getParams();
        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.customerpayment.initiation");
        List<SchemaValuePair> pairs = new ArrayList<>();
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        logger.info("purchaseCourseInitiationLeadSquared Entered"+data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        if(data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME)));
        }
        if(data.get(LeadSquaredRequest.Constants.COURSE_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get(LeadSquaredRequest.Constants.COURSE_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.AIO_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.AIO_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.BATCH_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get(LeadSquaredRequest.Constants.BATCH_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            pairs.add(new SchemaValuePair("Status", data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS)));
        }
        activityRequest.setFields(pairs);
        logger.info("purchaseCourseInitiationLeadSquared leadactivity"+activityRequest);
    }

    private void cancelSessionLeadSquared(LeadActivityReq activityRequest, LeadSquaredRequest req) {

        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.superkidssession.cancel");
        List<SchemaValuePair> pairs = new ArrayList<>();
        Map<String,String> data = req.getParams();
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        logger.info("cancelSessionLeadSquared Entered"+(data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        if(data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME)));
        }
        if(data.get(LeadSquaredRequest.Constants.SESSION_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get(LeadSquaredRequest.Constants.SESSION_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.BOOKING_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.BOOKING_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.CALLING_USER_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get(LeadSquaredRequest.Constants.CALLING_USER_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            pairs.add(new SchemaValuePair("Status", data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS)));
        }
        activityRequest.setFields(pairs);
        logger.info("Cancel superkids demosession : "+activityRequest);
    }

    private void createDemoSesionCompleteLeadSquared(LeadActivityReq activityRequest, LeadSquaredRequest req) {

        List<String> userIds = new ArrayList<>();
        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.demosession.complete");
        Map<String,String> data = req.getParams();
        String teacherId = data.get(LeadSquaredRequest.Constants.DEMO_TEACHER);
        userIds.add(teacherId);
        Map<String,UserBasicInfo> userBasicInfo = userUtils.getUserBasicInfosMap(userIds,true);
        List<SchemaValuePair> pairs = new ArrayList<>();
        UserBasicInfo teacherBasicInfo=userBasicInfo.get(teacherId);
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        if(data.get(LeadSquaredRequest.Constants.NOTES) != null) {
            activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.NOTES));
        }
        logger.info("createDemoSesionCompleteLeadSquared Entered : "+data.get(LeadSquaredRequest.Constants.STUDENT_ID));

        if(data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME)));
        }
        if(teacherBasicInfo != null && teacherBasicInfo.getFirstName() != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", teacherBasicInfo.getFirstName()));
        }
        if(data.get(LeadSquaredRequest.Constants.SESSION_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.SESSION_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.SESSION_SUCCESSFUL) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get(LeadSquaredRequest.Constants.SESSION_SUCCESSFUL)));
        }
        if(data.get(LeadSquaredRequest.Constants.SESSION_FAILURE_REASON) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_6", data.get(LeadSquaredRequest.Constants.SESSION_FAILURE_REASON)));
        }
        if(data.get(LeadSquaredRequest.Constants.PARENT_INTEREST) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_7", data.get(LeadSquaredRequest.Constants.PARENT_INTEREST)));
        }
        if(data.get(LeadSquaredRequest.Constants.EARLY_LEARNING_COURSE) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_8", data.get(LeadSquaredRequest.Constants.EARLY_LEARNING_COURSE)));
        }
        if(data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            pairs.add(new SchemaValuePair("Status", data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS)));
        }
        activityRequest.setFields(pairs);
        logger.info("createDemoSesionCompleteLeadSquared(  leadActivity : "+activityRequest);
    }

    private void updateSessionLeadSquared(LeadActivityReq activityRequest, LeadSquaredRequest req) throws Exception {
        Map<String,String> data = req.getParams();
        Integer eventCode;
        String callingUserId = data.get(LeadSquaredRequest.Constants.CALLING_USER_ID);

        UserBasicInfo userBasicInfo = userUtils.getUserBasicInfo(callingUserId, true);
        if(Role.STUDENT.equals(userBasicInfo.getRole())) {
            eventCode = DEMO_SESSION_EVENT_CODE;
        } else {
            eventCode = SETUP_SESSION_EVENT_CODE;
        }
        String studentId = data.get(LeadSquaredRequest.Constants.STUDENT_ID);
        if(!callingUserId.equalsIgnoreCase(studentId)){
            userBasicInfo = userUtils.getUserBasicInfo(studentId, true);
        }

        LeadDetails leadDetails = fetchLeadByPhone(userBasicInfo.getContactNumber());
        String leadId = leadDetails.getProspectID();

        List<SchemaValuePair> pairs = new ArrayList<>();
        if (Objects.nonNull(eventCode)) {

            if (data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER) != null) {

                LeadActivitySerialized activitySerializedToUpdate = null;
                List<LeadActivitySerialized> activityList = getLeadActivity(leadId, eventCode);
                if (DEMO_SESSION_EVENT_CODE.intValue() == eventCode.intValue()) {
                    pairs.add(new SchemaValuePair("mx_Custom_7", data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER)));

                    for(LeadActivitySerialized activitySerialized : activityList){
                        if(null != activitySerialized.getFields()
                                && null != activitySerialized.getFields().getMx_Custom_6()
                                && activitySerialized.getFields().getMx_Custom_6().equals(data.get(LeadSquaredRequest.Constants.BOOKING_ID))){
                            activitySerializedToUpdate = activitySerialized;
                            break;
                        }
                    }

                }else if (SETUP_SESSION_EVENT_CODE.intValue() == eventCode.intValue()) {
                    pairs.add(new SchemaValuePair("mx_Custom_6", data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER)));

                    for(LeadActivitySerialized activitySerialized : activityList){
                        if(null != activitySerialized.getFields()
                                && null != activitySerialized.getFields().getMx_Custom_5()
                                && activitySerialized.getFields().getMx_Custom_5().equals(data.get(LeadSquaredRequest.Constants.BOOKING_ID))){
                            activitySerializedToUpdate = activitySerialized;
                            break;
                        }
                    }
                }

                if(activitySerializedToUpdate != null) {
                    logger.info("Update LS Entries");
                    List<UpdateLeadActivityReq> bulkReq = new ArrayList<>();
                    UpdateLeadActivityReq updateReq = new UpdateLeadActivityReq();
                    updateReq.setProspectActivityId(activitySerializedToUpdate.getActivityId());
                    updateReq.setFields(pairs);
                    bulkReq.add(updateReq);
                    logger.info("updateSessionLeadSquared leadActivity :" + updateReq);
                    updateBulkActivity(bulkReq);
                }
            }
        }


    }

    private void createSessionLeadSquared(LeadActivityReq activityRequest, LeadSquaredRequest req) {

        List<String> userIds = new ArrayList<>();
        Map<String, String> data = req.getParams();
        Integer eventCode;

        String teacherId = data.get(LeadSquaredRequest.Constants.DEMO_TEACHER);
        String callingUserId = data.get(LeadSquaredRequest.Constants.CALLING_USER_ID);
        userIds.add(teacherId);
        userIds.add(callingUserId);

        Map<String, UserBasicInfo> userBasicInfo = userUtils.getUserBasicInfosMap(userIds, true);

        if (Role.STUDENT.equals(userBasicInfo.get(callingUserId).getRole())) {
            eventCode = DEMO_SESSION_EVENT_CODE;
        } else {
            eventCode = SETUP_SESSION_EVENT_CODE;
        }

        List<SchemaValuePair> pairs = new ArrayList<>();
        UserBasicInfo teacherBasicInfo = userBasicInfo.get(teacherId);
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        logger.info("createSessionLeadSquared Entered For User: " + data.get(LeadSquaredRequest.Constants.STUDENT_ID));

        if (data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if (data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.DEMO_SESSION_TIME)));
        }
        if (teacherBasicInfo != null && teacherBasicInfo.getFirstName() != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", teacherBasicInfo.getFirstName()));
        }
        if (data.get(LeadSquaredRequest.Constants.SESSION_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.SESSION_ID)));
        }
        if (Objects.nonNull(eventCode)) {
            if (DEMO_SESSION_EVENT_CODE.intValue() == eventCode.intValue()) {
                if (data.get(LeadSquaredRequest.Constants.BOOKING_ID) != null) {
                    pairs.add(new SchemaValuePair("mx_Custom_6", data.get(LeadSquaredRequest.Constants.BOOKING_ID)));
                }
                if (data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER) != null) {
                    pairs.add(new SchemaValuePair("mx_Custom_7", data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER)));
                }
            }
            if (SETUP_SESSION_EVENT_CODE.intValue() == eventCode.intValue()) {
                if (data.get(LeadSquaredRequest.Constants.BOOKING_ID) != null) {
                    pairs.add(new SchemaValuePair("mx_Custom_5", data.get(LeadSquaredRequest.Constants.BOOKING_ID)));
                }
                if (data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER) != null) {
                    pairs.add(new SchemaValuePair("mx_Custom_6", data.get(LeadSquaredRequest.Constants.ALTERNATE_PHONE_NUMBER)));
                }
            }
        }
        if(data.get(LeadSquaredRequest.Constants.COURSE_NAME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_8", data.get(LeadSquaredRequest.Constants.COURSE_NAME)));
        }
        if (data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            if (Role.STUDENT.equals(userBasicInfo.get(callingUserId).getRole())) {
                pairs.add(new SchemaValuePair("Status", "Demo Booked"));
            } else {
                pairs.add(new SchemaValuePair("Status", "Demo Booked by Support Team"));
            }
        }
        activityRequest.setFields(pairs);
        logger.info("createSessionLeadSquared leadActivity" + activityRequest);
    }

    private void customerSignupLeadSquared(LeadActivityReq activityRequest, LeadSquaredRequest req)  {

        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.actvity.customer.signup");
        List<SchemaValuePair> pairs = new ArrayList<>();
        Map<String,String> data = req.getParams();
        logger.info("customerSignupLeadSquared Entered for User "+data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityEvent(eventCode);
        activityRequest.setUserId(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        activityRequest.setActivityNote(data.get(LeadSquaredRequest.Constants.STUDENT_ID));
        if(data.get(LeadSquaredRequest.Constants.PARENT_NAME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get(LeadSquaredRequest.Constants.PARENT_NAME)));
        }
        if(data.get(LeadSquaredRequest.Constants.PARENT_EMAIL) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get(LeadSquaredRequest.Constants.PARENT_EMAIL)));
        }
        if(data.get(LeadSquaredRequest.Constants.PARENT_PHONE) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get(LeadSquaredRequest.Constants.PARENT_PHONE)));
        }
        if(data.get(LeadSquaredRequest.Constants.CHILD_NAME) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get(LeadSquaredRequest.Constants.CHILD_NAME)));
        }
        if(data.get(LeadSquaredRequest.Constants.STUDENT_ID) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get(LeadSquaredRequest.Constants.STUDENT_ID)));
        }
        if(data.get(LeadSquaredRequest.Constants.GRADE) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_7", data.get(LeadSquaredRequest.Constants.GRADE)));
        }
        if(data.get(LeadSquaredRequest.Constants.BOARD) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_8", data.get(LeadSquaredRequest.Constants.BOARD)));
        }
        if(data.get(LeadSquaredRequest.Constants.DEVICE_TYPE) != null) {
            pairs.add(new SchemaValuePair("mx_Custom_9", data.get(LeadSquaredRequest.Constants.DEVICE_TYPE)));
        }
        if(data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS) != null) {
            pairs.add(new SchemaValuePair("Status", data.get(LeadSquaredRequest.Constants.CUSTOMER_STATUS)));
        }
        activityRequest.setFields(pairs);
        logger.info("leadActivity : "+activityRequest);
    }

    private LeadActivityReq salesDemoLeadSquared(String objString, LeadActivityReq req) throws Exception {
        Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.sales.demo");
        LeadActivityReq leadActivity = new LeadActivityReq(null,eventCode, "");
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> data = gson.fromJson(objString, type);
        logger.info("SALES_DEMO_LEADSQUARED: " + data);
        leadActivity.setActivityEvent(eventCode);
        leadActivity.setUserId(data.get("userId"));
        List<SchemaValuePair> pairs = new ArrayList<>();
        if (data.get("sessionDuration") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get("sessionDuration")));
        }
        if (data.get("sessionOnlineDuration") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_2", data.get("sessionOnlineDuration")));
        }
        if (data.get("timeInSession") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get("timeInSession")));
        }
        if (data.get("quizAttemptedRatio") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_4", data.get("quizAttemptedRatio")));
        }
        if (data.get("quizNumbersCorrect") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get("quizNumbersCorrect")));
        }
        if (data.get("hotSpotAttemptedRatio") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_6", data.get("hotSpotAttemptedRatio")));
        }
        if (data.get("sessionId") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_7", data.get("sessionId")));
        }
        if (StringUtils.isNotEmpty(data.get("sessionStartTime")) && !"null".equalsIgnoreCase(data.get("sessionStartTime"))) {
            pairs.add(new SchemaValuePair("mx_Custom_8", LeadSquaredUtils.getLeadSquaredDate(Long.parseLong(data.get("sessionStartTime")))));
        }
        if (StringUtils.isNotEmpty(data.get("teacherJoinTime")) && !"null".equalsIgnoreCase(data.get("teacherJoinTime"))) {
            pairs.add(new SchemaValuePair("mx_Custom_9", LeadSquaredUtils.getLeadSquaredDate(Long.parseLong(data.get("teacherJoinTime")))));
        }
        if (StringUtils.isNotEmpty(data.get("studentJoinTime")) && !"null".equalsIgnoreCase(data.get("studentJoinTime"))) {
            pairs.add(new SchemaValuePair("mx_Custom_10", LeadSquaredUtils.getLeadSquaredDate(Long.parseLong(data.get("studentJoinTime")))));
        }
        String presenterEmail = "";
        if (StringUtils.isNotEmpty(data.get("teacherId")) && !"null".equalsIgnoreCase(data.get("teacherId"))) {
            List<String> userIds = new ArrayList<>();
            userIds.add(data.get("teacherId"));
            Map<String, UserBasicInfo> userMap = userUtils.getUserBasicInfosMap(userIds, true);
            if(userMap.containsKey(data.get("teacherId"))){
                presenterEmail = userMap.get(data.get("teacherId")).getEmail();
            }
            pairs.add(new SchemaValuePair("mx_Custom_11", presenterEmail));
        }
        if (StringUtils.isNotEmpty(data.get("taIds")) && !"null".equalsIgnoreCase(data.get("taIds"))) {
            pairs.add(new SchemaValuePair("mx_Custom_12", (data.get("taIds"))));
        }
        if(StringUtils.isNotEmpty(presenterEmail)) {
            presenterEmail = presenterEmail.replaceAll("\\+.*@","@");
            List<LeadsquaredUser> owners = fetchAgentsByEmailId(presenterEmail);
            LeadsquaredUser owner = null;
            if (owners != null && !owners.isEmpty()) {
                owner = owners.get(0);
                pairs.add(new SchemaValuePair("Owner", owner.getUserId()));
            }

        }


        leadActivity.setFields(pairs);
        logger.info("Request: " + leadActivity);

        // check if the activity exists
        Set<Long> userIds = new HashSet<>();
        userIds.add(Long.parseLong(data.get("userId")));
        Map<String, String> userLeadMap = getUserLeadMap(userIds);
        String leadId = userLeadMap.get(data.get("userId"));
        String activityNote = data.get("userId") + "_" + data.get("sessionId");
        List<LeadActivitySerialized> activityList = getLeadActivity(leadId, eventCode);

        //logger.info("Session Activity List Before:" + activityList);
        List<LeadActivitySerialized> sessionActivityList = new ArrayList<>();
        for(LeadActivitySerialized activitySerialized : activityList){
            if(null != activitySerialized.getFields() && null != activitySerialized.getFields().getMx_Custom_7() && activitySerialized.getFields().getMx_Custom_7().equals(data.get("sessionId"))){
                sessionActivityList.add(activitySerialized);
                break;
            }
        }

        logger.info("Session Activity List :" + sessionActivityList);

        if (CollectionUtils.isEmpty(sessionActivityList)) {
            logger.info("Insert into LS");
            leadActivity.setFields(pairs);
            postBulkLeadActivity(Collections.singletonList(leadActivity), HttpMethod.POST);
        } else {
            logger.info("Update LS Entries");
            List<UpdateLeadActivityReq> bulkReq = new ArrayList<>();
            String prospectId = fetchActivityIdForSalesDemo(leadId, activityNote, eventCode, data.get("sessionId"));
            if (StringUtils.isEmpty(prospectId)) {
                return null;
            }
            UpdateLeadActivityReq updateReq = new UpdateLeadActivityReq();
            updateReq.setProspectActivityId(prospectId);
            updateReq.setFields(leadActivity.getFields());
            bulkReq.add(updateReq);
            updateBulkActivity(bulkReq);
        }

        return null;

    }

    private void counsellingSessionScheduledLeadSquared(LeadSquaredRequest leadSquaredRequest, LeadActivityReq req) throws Exception
    {
        Map<String, String> data=leadSquaredRequest.getParams();
        String studentId=data.get(LeadSquaredRequest.Constants.STUDENT_ID);
        String activityNote = "Online counselling session scheduled-"+studentId;
        req.setUserId(studentId);
        UserBasicInfo student=userUtils.getUserBasicInfo(studentId,true);
        List<SchemaValuePair> pairs = new ArrayList<>();
        if (data.get("sessionId") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_1", data.get("sessionId")));
        }
        if (data.get("sessionDuration") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_5", data.get("sessionDuration")));
        }
        if (data.get("sessionStartTime") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_6", data.get("sessionCreationTime")));
        }
        if (data.get("sessionCreationTime") != null) {
            pairs.add(new SchemaValuePair("mx_Custom_3", data.get("sessionStartTime")));
        }
        if(StringUtils.isNotEmpty(data.get("agentEmailId")))
        {
            String agentEmailId=data.get("agentEmailId");
            pairs.add(new SchemaValuePair("mx_Custom_7",agentEmailId));
            agentEmailId=agentEmailId.replaceAll("\\+.*@","@");
            pairs.add(new SchemaValuePair("mx_Custom_2", agentEmailId));
            List<LeadsquaredUser> owners = fetchAgentsByEmailId(agentEmailId);
            LeadsquaredUser owner = null;
            if (CollectionUtils.isNotEmpty(owners)) {
                owner = owners.get(0);
                logger.info("owner of activity"+owner);
                pairs.add(new SchemaValuePair("Owner", owner.getUserId()));
                pairs.add(new SchemaValuePair("mx_Custom_4", owner.getUserId()));
            }

        }
        req.setActivityNote(activityNote);
        req.setFields(pairs);
        logger.info("LS Activity formed for counselling scheduled- {}",req);

    }

    private List<SchemaValuePair> getWebinarMobileCallbackFields(WebinarCallbackData webinarCallbackData) {
        List<String> values = new ArrayList<>();
        values.add(webinarCallbackData.getWebinarId());
        values.add(LeadSquaredUtils.getLeadSquaredDate(webinarCallbackData.getTime()));
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public String getStringValueInRupee(Integer integer) {
        if (integer == null) {
            return "";
        } else {
            Double doubleNumber = integer * 1.0 / 100.0;
            return doubleNumber.toString();
        }
    }

    public boolean postBulkLeadActivity(List<LeadActivityReq> leadActivities,
                                        HttpMethod httpMethod)
            throws Exception {

        Set<String> userIds = new HashSet<>();
        for (LeadActivityReq req : leadActivities) {
            logger.info("LeadActivityReq " + req);

            if (StringUtils.isNotEmpty(req.getEmailAddress()) || StringUtils.isNotEmpty(req.getPhone())) {
                continue;
            }
            userIds.add(req.getUserId());
        }
        Map<String, UserBasicInfo> userMap = userUtils.getUserBasicInfosMap(userIds, true);

        Map<String, List<LeadActivityReq>> map = new HashMap<>();

        for (LeadActivityReq req : leadActivities) {
            logger.info("leadSquare : " + req.toString());
            UserBasicInfo user = userMap.get(req.getUserId());

            //ignore if orgid is not empty
            if(user != null && StringUtils.isNotEmpty(user.getOrgId()))
                continue;
            logger.info("leadsquare req in json :"+gson.toJson(req));

            boolean email = false;
            if (user != null && StringUtils.isNotEmpty(user.getContactNumber())) {
                req.setPhone(user.getContactNumber());
                if(StringUtils.isNotEmpty(user.getEmail())) {
                        req.setEmailAddress(LeadSquaredUtils.validateLSEmails(user.getEmail()) ? user.getEmail() : null);
                }
                req.setSearchByValue(user.getContactNumber());
            } else if (StringUtils.isNotEmpty(req.getPhone())) {
                if(user != null && StringUtils.isNotEmpty(user.getEmail())) {
                        req.setEmailAddress(user.getEmail());
                }
                if(StringUtils.isNotEmpty(req.getEmailAddress()) && !LeadSquaredUtils.validateLSEmails(req.getEmailAddress())){
                    req.setEmailAddress(null);
                }
                req.setSearchByValue(req.getPhone());
            }else if (user != null && user.getEmail() != null) {
                email = true;
                req.setSearchByValue(user.getEmail());
                req.setEmailAddress(user.getEmail());
            } else if (StringUtils.isNotEmpty(req.getEmailAddress())) {
                email = true;
                req.setSearchByValue(req.getEmailAddress());
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "userEmail is not present");
            }

            if(isUserHasOrgId(req.getPhone(), req.getEmailAddress()))
                continue;
            if (email) {

                List<LeadActivityReq> list = CollectionUtils.putIfAbsentAndGet(map, "Email", ArrayList::new);
                list.add(req);
                createLeadIfDeleted(req.getSearchByValue(), null);
            } else {
                List<LeadActivityReq> list = CollectionUtils.putIfAbsentAndGet(map, "Phone", ArrayList::new);
                list.add(req);
                createLeadIfDeleted(null, req.getSearchByValue());
            }
            logger.info("Lead Activity : "+gson.toJson(req));
        }
        logger.info("limiter acquired");

        limiter.acquire();
        boolean ans = false;
        if (ArrayUtils.isNotEmpty(map.get("Email"))) {

            List<LeadActivityReq> emailActivities = map.get("Email");

            if(emailActivities.size() <= 5){

                for(LeadActivityReq req : emailActivities) {
                    try {

                        if(StringUtils.isEmpty(req.getEmailAddress()))
                            req.setEmailAddress(req.getSearchByValue());

                        String url = getLeadSquaredURL(LS_ACTIVITY_URL);
                        logger.info("postLeadActivity normal API", new Object[]{url, httpMethod, req});

                        ans = postLeadData(url, HttpMethod.POST, gson.toJson(req),
                                "", null, "","", "");
                    } catch (BadRequestException e){
                        throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
                    } catch (Exception e) {
                        logger.warn("Error in postLeadData normal call" + e);
                        throw new Exception(e);
                    }
                }
            }else {
                try {

                    String url = getLeadSquaredBulkActivityURL(LS_BULK_ACTIVITY_URL, LeadDetails.Constants.EMAIL_ADDRESS);
                    logger.info("postLeadActivity", new Object[]{url, httpMethod, emailActivities});

                    ans = postLeadData(url, HttpMethod.POST, gson.toJson(emailActivities),
                            "", LeadSquaredDataType.LEAD_BULK_ACTIVITY_ADD, "",
                            "", "");
                } catch (BadRequestException e){
                    throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
                } catch (Exception e) {
                    logger.info("Error in postLeadData", e);
                    ans = false;
                    throw new BadRequestException(ErrorCode.SERVICE_ERROR,e.getMessage());
                }
                logger.info("lead Activity request email"+gson.toJson(emailActivities));
            }
        }
        if (ArrayUtils.isNotEmpty(map.get("Phone"))) {

            List<LeadActivityReq> phoneActivities = map.get("Phone");

            if(phoneActivities.size() <= 5){
                for(LeadActivityReq req : phoneActivities) {
                    try {

                        req.setSearchBy("Phone");

                        String url = getLeadSquaredURL(LS_ACTIVITY_URL);
                        logger.info("postLeadActivity normal API", new Object[]{url, httpMethod, req});

                        ans = postLeadData(url, HttpMethod.POST, gson.toJson(req),
                                "", null, "","", "");
                    } catch (BadRequestException e){
                        throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
                    } catch (Exception e) {
                        logger.warn("Error in postLeadData normal call", e);
                        throw new BadRequestException(ErrorCode.SERVICE_ERROR,e.getMessage());
                    }
                    logger.info("lead Activity request req"+gson.toJson(req));
                }
            }else {
                try {
                    String url = getLeadSquaredBulkActivityURL(LS_BULK_ACTIVITY_URL, LeadDetails.Constants.PHONE);
                    logger.info(LoggingMarkers.JSON_MASK,"postLeadActivity", new Object[]{url, httpMethod, phoneActivities});

                    ans = postLeadData(url, HttpMethod.POST, gson.toJson(phoneActivities),
                            "", LeadSquaredDataType.LEAD_BULK_ACTIVITY_ADD, "",
                            "", "");
                } catch (BadRequestException e){
                    throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
                } catch (Exception e) {
                    logger.warn("Error in postLeadData" + e);
                    throw new BadRequestException(ErrorCode.SERVICE_ERROR,e.getMessage());
                }
                logger.info("lead Activity request phone"+gson.toJson(phoneActivities));
            }
        }
        return ans;
    }

    private String getLeadSquaredBulkActivityURL(String url, String fieldName) throws BadRequestException {
        if (StringUtils.isEmpty(url) || StringUtils.isEmpty(LS_ACCESS_ID) || StringUtils.isEmpty(LS_SECRET_KEY)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No access details found");
        }
        return url + "?accessKey=" + LS_ACCESS_ID + "&secretKey=" + LS_SECRET_KEY + "&searchBy=" + fieldName;
    }

    private String getEntityInfo(OrderedItem item) throws VException {
        String title = "";
        if(null == item){
            return title;
        }

        if(null == item.getEntityType()){
            return title;
        }
//            return "name_"+item.getEntityType().name();
        switch (item.getEntityType()) {
            case OTF:
            case OTF_BATCH_REGISTRATION:
                BatchBasicInfo batchInfo = enrollmentManager.getBatch(item.getEntityId());
                title = batchInfo.getCourseInfo().getTitle();
                break;
            case OTF_COURSE_REGISTRATION:
                CourseInfo courseInfo = otfManager.getCourse(item.getEntityId());
                title = courseInfo.getTitle();
                break;
            case COURSE_PLAN:
            case COURSE_PLAN_REGISTRATION:
            case OTO_COURSE_REGISTRATION:
//                CoursePlanBasicInfo coursePlanInfo = coursePlanManager.getCoursePlanBasicInfo(item.getEntityId(), false, true);
                CoursePlanBasicInfo coursePlanInfo = coursePlanManager.getCoursePlanTitle(item.getEntityId());
                if(coursePlanInfo!= null && StringUtils.isNotEmpty(coursePlanInfo.getTitle())){
                    title = coursePlanInfo.getTitle();
                }
                break;
            case PLAN:
                title = "Plan";
                break;
            case OTM_BUNDLE:
            case OTM_BUNDLE_ADVANCE_PAYMENT:
            case OTM_BUNDLE_REGISTRATION:
                OTFBundleInfo otfBundleInfo = otfBundleManager.getOTFBundle(item.getEntityId());
                title = otfBundleInfo.getTitle();
                break;
            case BUNDLE_PACKAGE:
                BundlePackageInfo bpInfo = bundlePackageManager.getBundlePackage(item.getEntityId());
                title = bpInfo.getTitle();
                break;
            case BUNDLE:
                GetBundlesReq req = new GetBundlesReq();
                List<String> ids = new ArrayList<>();
                ids.add(item.getEntityId());
                req.setBundleIds(ids);
                BundleInfo bundleInfo = bundleManager.getBundleName(req);
                title = bundleInfo.getTitle();
                break;
            default:

                break;

        }
        return title;
    }

    public List<SchemaValuePair> getLessThan3HourAlertValues(CoursePlanSQSAlert coursePlanSQSAlert) {
        List<String> req = new ArrayList<>();

        req.add(coursePlanSQSAlert.getStudentEmailId());
        req.add(coursePlanSQSAlert.getTeacherEmailId());
        req.add(coursePlanSQSAlert.getStudentContactNumber());
        req.add(coursePlanSQSAlert.getTeacherContactNumber());
        req.add(LeadSquaredUtils.getLeadSquaredDate(coursePlanSQSAlert.getSessionEndTime()));

        return LeadSquaredUtils.getSchemaValuePair(req);
    }

    public List<SchemaValuePair> getOrderCustomValues(Orders order) throws VException {
        List<String> req = new ArrayList<>();
        OrderedItem item = order.getItems().get(0);

        String title = getEntityInfo(item);
        req.add(order.getId());
        req.add(title);
        req.add(item.getEntityId());
        if(null != item.getEntityType()) {
            req.add(item.getEntityType().name());
        }
        req.add(getStringValueInRupee(order.getNonPromotionalAmountPaid()));
        req.add(getStringValueInRupee(order.getPromotionalAmountPaid()));
        req.add(getStringValueInRupee(item.getVedantuDiscountAmount()));
        req.add(LeadSquaredUtils.getLeadSquaredDate(order.getCreationTime()));
        req.add(getStringValueInRupee(item.getCost()));
        req.add(getStringValueInRupee(order.getNonPromotionalAmount()));
        req.add(order.getPaymentStatus().name());
        req.add(getStringValueInRupee(order.getAmount()));
        req.add(order.getPaymentSource());
        req.add(order.getSubscriptionPlanId());
        return LeadSquaredUtils.getSchemaValuePair(req);

    }

    public List<SchemaValuePair> getInstalmentCustomValues(InstalmentInfo instalment) {
        List<String> values = new ArrayList<>();
        values.add(instalment.getId());
        values.add(instalment.getContextId());
        values.add(instalment.getContextType().name());
        values.add(LeadSquaredUtils.getLeadSquaredDate(instalment.getCreationTime()));
        values.add(LeadSquaredUtils.getLeadSquaredDate(instalment.getDueTime()));
        values.add(instalment.getOrderId());
        values.add(LeadSquaredUtils.getLeadSquaredDate(instalment.getPaidTime()));
        values.add(instalment.getPaymentStatus().name());
        values.add(getStringValueInRupee(instalment.getTotalAmount()));
        values.add(getStringValueInRupee(instalment.getTotalNonPromotionalAmount()));
        values.add(getStringValueInRupee(instalment.getTotalPromotionalAmount()));
        return LeadSquaredUtils.getSchemaValuePair(values);

    }

    public List<SchemaValuePair> getEnrollmentCustomValues(EnrollmentPojo enrollment) {
        List<String> values = new ArrayList<>();
        values.add(enrollment.getId());
        values.add(LeadSquaredUtils.getLeadSquaredDate(enrollment.getCreationTime()));
        values.add(enrollment.getStatus().name());
        values.add(enrollment.getEntityTitle());
        values.add(enrollment.getEntityId());
        values.add(enrollment.getEntityType().name());
        values.add(enrollment.getState().name());
        values.add(null);//np amount
        values.add(enrollment.getBatchId());
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public List<SchemaValuePair> getCoursePlanCustomValues(CoursePlanInfo coursePlan) {
        List<String> values = new ArrayList<>();
        UserBasicInfo userInfo = userUtils.getUserBasicInfo(coursePlan.getTeacherId(), true);
        String teacherEmail = "";
        if (userInfo != null) {
            teacherEmail = userInfo.getEmail();
        }
        values.add(coursePlan.getId());
        values.add(LeadSquaredUtils.getLeadSquaredDate(coursePlan.getCreationTime()));
        values.add(coursePlan.getState().name());
        values.add(coursePlan.getTitle());
        values.add(teacherEmail);
        values.add(getStringValueInRupee(coursePlan.getTrialRegistrationFee()));
        values.add(getStringValueInRupee(coursePlan.getPrice()));
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public List<SchemaValuePair> getWebinarRegisteredValues(WebinarUserRegistrationInfo info) {
        List<String> values = new ArrayList<>();
//        values.add(info.getWebinarId());
        values.add(info.getRegisterJoinUrl());
//        values.add(info.getRegisterStatus());
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public List<SchemaValuePair> getWebinarAttendedValues(WebinarUserRegistrationInfo info) {
        List<String> values = new ArrayList<>();
//        values.add(info.getWebinarId());
        values.add(info.getRegisterJoinUrl());
//        values.add(info.getRegisterStatus());
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public List<SchemaValuePair> getSaleClosedCustomValues(SaleClosed saleClosed) {
        List<String> values = new ArrayList<>();
        values.add(null);
        values.add(LeadSquaredUtils.getLeadSquaredDate(saleClosed.getClosedTime()));
        values.add(saleClosed.getEntityType() == null ? null : saleClosed.getEntityType().name());
        values.add(saleClosed.getModel() == null ? null : saleClosed.getModel().name());
        values.add(saleClosed.getEntityId());
        values.add(getStringValueInRupee(saleClosed.getNonPromotionalAmount()));
        values.add(getStringValueInRupee(saleClosed.getPromotionalAmount()));
        values.add(getStringValueInRupee(saleClosed.getVedantuDiscount()));// vedantu discount
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public List<SchemaValuePair> getTransactionCustomValues(TransactionUserInfo transactionUserInfo) {
        List<String> values = new ArrayList<>();
        Transaction transaction = transactionUserInfo.getTransaction();
        values.add(transaction.getId());
        values.add(getStringValueInRupee(transaction.getAmount()));
        values.add(transaction.getReasonRefType() == null ? null : transaction.getReasonRefType().name());
        values.add(LeadSquaredUtils.getLeadSquaredDate(transaction.getCreationTime()));
        return LeadSquaredUtils.getSchemaValuePair(values);
    }

    public List<SchemaValuePair> getSchemaValuepairForGPSLocales(Map<String, String> gpsLocalsData){
        List<SchemaValuePair> pairs = new ArrayList<>();
        if(StringUtils.isNotEmpty(gpsLocalsData.get("city"))){
            pairs.add(new SchemaValuePair("mx_GPS_City", gpsLocalsData.get("city")));
        }
        if(StringUtils.isNotEmpty(gpsLocalsData.get("longitude"))){
            pairs.add(new SchemaValuePair("mx_GPS_Long", gpsLocalsData.get("longitude")));
        }
        if(StringUtils.isNotEmpty(gpsLocalsData.get("latitude"))){
            pairs.add(new SchemaValuePair("mx_GPS_lat", gpsLocalsData.get("latitude")));
        }
        return pairs;
    }

    public UpdateLeadActivitiesRes updateLeadActivityReq(List<QueueMessagePojo> pojos)
            throws Exception {
        UpdateLeadActivitiesRes res = new UpdateLeadActivitiesRes();
        List<LeadActivityReq> tempReqs = new ArrayList<>();
        List<LeadActivityReq> newActivityReqs = new ArrayList<>();
        List<UpdateLeadActivityReq> bulkReq = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(pojos)) {
            Set<Long> userIds = new HashSet<>();
            for (QueueMessagePojo pojo : pojos) {
                LeadActivityReq req = createLeadActivityReq(pojo.getMessageType(), pojo.getBody());
                if (req != null) {
                    tempReqs.add(req);
                }
            }
            if (ArrayUtils.isEmpty(tempReqs)) {
                logger.info("No update Requests");
                return res;
            }
            tempReqs.forEach((req) -> {
                userIds.add(Long.parseLong(req.getUserId()));
            });

            Map<String, String> userLeadMap = getUserLeadMap(userIds);

            for (LeadActivityReq req : tempReqs) {
                String leadId = userLeadMap.get(req.getUserId());
                if (StringUtils.isEmpty(leadId)) {
                    logger.info("user leadId is null for " + req.getUserId());
                    continue;
                }
                String prospectId = fetchActivityId(leadId, req.getActivityNote(), req.getActivityEvent());

                if (StringUtils.isEmpty(prospectId)) {
                    newActivityReqs.add(req);
                    continue;
                }
                UpdateLeadActivityReq updateReq = new UpdateLeadActivityReq();
                updateReq.setProspectActivityId(prospectId);
                updateReq.setFields(req.getFields());
                bulkReq.add(updateReq);
            }
        }
        res.setUpdateReqs(bulkReq);
        res.setCreateActivetityReqs(newActivityReqs);
        return res;
    }

    public Map<String, String> getUserLeadMap(Set<Long> userIds) throws InterruptedException, BadRequestException {
        logger.info("limiter acquired");
        limiter.acquire();
        List<LeadDetailsSerialized> leadDetails = getLeadDetails("mx_User_Id", new ArrayList<>(userIds));
        Map<String, String> userLeadMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(leadDetails)) {
            for (LeadDetailsSerialized lead : leadDetails) {
                if (StringUtils.isNotEmpty(lead.getMx_User_Id())) {
                    userLeadMap.put(lead.getMx_User_Id(), lead.getProspectID());
                }
            }
        }
        return userLeadMap;
    }

    public String fetchActivityId(String leadId, String id, Integer code) throws BadRequestException, InterruptedException, VException {
        if (StringUtils.isEmpty(leadId) || StringUtils.isEmpty(id) || code == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Req params null" + leadId + "  " + id + "  " + code);
        }
        logger.info("limiter acquired");
        limiter.acquire();
        List<LeadActivitySerialized> activities = getLeadActivity(leadId, code);
        if (ArrayUtils.isNotEmpty(activities)) {
            for (LeadActivitySerialized activity : activities) {
                if (activity.getFields() != null) {
                    if (id.equals(activity.getFields().getMx_Custom_1())) {
                        return activity.getActivityId();
                    }
                }

            }
        }
        return "";
    }

    public String fetchActivityIdForSalesDemo(String leadId, String id, Integer code, String sessionId) throws VException, InterruptedException {
        if (StringUtils.isEmpty(leadId) || StringUtils.isEmpty(id) || code == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Req params null" + leadId + "  " + id + "  " + code);
        }
        logger.info("limiter acquired");
        limiter.acquire();
        List<LeadActivitySerialized> activities = getLeadActivity(leadId, code);
        if (ArrayUtils.isNotEmpty(activities)) {
            for (LeadActivitySerialized activity : activities) {
                if (activity.getFields() != null) {
                    if (activity.getFields().getMx_Custom_7().equals(sessionId)) {
                        return activity.getActivityId();
                    }
                }

            }
        }
        return "";
    }

    public void updateBulkActivity(List<UpdateLeadActivityReq> reqs)
            throws Exception {

        logger.info("limiter acquired");
        limiter.acquire();

        boolean isBulk = true;
        String url = "";
        if(reqs.size() <= 5){
            isBulk = false;
            url = getLeadSquaredURL(LS_ACTIVITY_UPDATE_URL);
        }else {
            url = getLeadSquaredURL(LS_BULK_ACTIVITY_UPDATE_URL);
        }

        List<UpdateLeadActivityReq> filteredList = new ArrayList<>();

        for (UpdateLeadActivityReq req : reqs) {
            List<SchemaValuePair> fields = req.getFields();

            String leadPhone = null;
            String leadEmail = null;
            for (SchemaValuePair valuePair : fields) {
                if (valuePair.getSchemaName().equalsIgnoreCase("Phone")) {
                    leadPhone = valuePair.getValue();
                    createLeadIfDeleted(null, leadPhone);
                    break;
                } else if (valuePair.getSchemaName().equalsIgnoreCase("EmailAddress")) {
                    leadEmail = valuePair.getValue();
                    createLeadIfDeleted(leadEmail, null);
                    break;
                }
            }
            logger.info("Updated Lead Activity : "+gson.toJson(req));
            if(isUserHasOrgId(leadPhone,leadEmail))
                continue;
            else if(!isBulk)
                postLeadData(url, HttpMethod.POST, gson.toJson(req),"", null, "","", "");
            else
                filteredList.add(req);

        }

        if(isBulk) {
            postLeadData(url, HttpMethod.POST, gson.toJson(filteredList), "", null, "", "", "");
        }
    }

    private void leadActivityOnCall(LeadSquaredRequest req)
            throws Exception {
        Map<String, String> params = req.getParams();

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.on.call.record");

        LeadActivity leadActivity = new LeadActivity(eventCode, params.get("userEmail"), System.currentTimeMillis(),
                null);
        leadActivity.setPhone(params.get("mobileNumber"));
        if (StringUtils.isNotEmpty(params.get("prospectId"))) {
            leadActivity.setProspectId(params.get("prospectId"));
        }

        StringBuilder sb = new StringBuilder();
        sb.append("On Call Activity : ");
        sb.append("userEmail: ").append(params.get("userEmail"));

        String duration = params.get("duration");
        if (duration != null) {
            duration = DateTimeUtils.printableTimeStringHHmmSS(Long.parseLong(duration));
            sb.append("\nduration: ").append(duration);
        }
        if (params.get("loc") != null) {
            sb.append("\nlocation: ").append(params.get("loc"));
        }
        if (params.get("callee") != null) {
            sb.append("\nlead phone no: ").append(params.get("callee"));
        }
        if (params.get("addr") != null) {
            sb.append("\naddr: ").append(params.get("addr"));
        }
        if (params.get("direction") != null) {
            sb.append("\ndirection: ").append(params.get("direction"));
        }
        if (params.get("sharedLink") != null) {
            sb.append("\naudio link: ").append(params.get("sharedLink"));
        }
        if (params.get("created") != null) {
            sb.append("\ncreated: ").append(params.get("created"));
        }

        String activityNote = StringEscapeUtils.escapeJson(sb.toString());
        leadActivity.setActivityNote(activityNote);
        postLeadActivity(leadActivity, LeadSquaredDataType.LEAD_ACTIVITY_ON_CALL, null, HttpMethod.POST);
    }

    public void createLeadIfDeleted(String email, String phone) throws Exception {

        try {

            if (StringUtils.isEmpty(email) && StringUtils.isEmpty(phone))
                return;

            //Check if present in rediss
            if (StringUtils.isNotEmpty(phone)) {
                String key = phone + "_LS_LEAD_EXIST";
                String isExist = redisDAO.get(key);

                //return if lead exist
                if (StringUtils.isNotEmpty(isExist))
                    return;
            }
            if (StringUtils.isNotEmpty(email)) {
                String key = email + "_LS_LEAD_EXIST";

                String isExist = redisDAO.get(key);

                //return if lead exist
                if (StringUtils.isNotEmpty(isExist))
                    return;
            }

            for (int i = 0; i < 3; i++) {
                try {

                    logger.info("Checking if lead present email: {}, phone: {}", email, phone);
                    List<LeadDetails> leadDetails = null;

                    //Avoiding 1 additional call to LS
                    if (StringUtils.isNotEmpty(phone))
                        leadDetails = fetchLeadsByPhone(phone);

                    if (CollectionUtils.isEmpty(leadDetails) && StringUtils.isNotEmpty(email))
                        leadDetails = fetchLeadsByEmailId(email);

                    if (CollectionUtils.isEmpty(leadDetails)) {

                        logger.info("Lead not found creating new lead email: {}, phone: {}", email, phone);

                        User user = null;
                        if (StringUtils.isNotEmpty(phone)) {
                            List<User> users = userManager.getUsersByContactNumber(phone);
                            if (CollectionUtils.isNotEmpty(users))
                                user = users.get(0);
                        }

                        if (user == null && StringUtils.isNotEmpty(email) && CustomValidator.validEmail(email))
                            user = userManager.getUserByEmail(email);

                        if (user == null) {
                            logger.warn("User not found:: email: {}, phone: {}", email, phone);
                            return;
                        }

                        if(StringUtils.isNotEmpty(user.getOrgId())) {

                            //Store in rediss for 30 days
                            String key = "";
                            if (StringUtils.isNotEmpty(phone))
                                key = phone + "_LS_LEAD_EXIST";
                            else
                                key = email + "_LS_LEAD_EXIST";

                            redisDAO.setex(key, "TRUE", 30 * 24 * 60 * 60);

                            return;
                        }

                        LeadSquaredRequest req = new LeadSquaredRequest();
                        req.setUser(user);

                        leadCreate(req);

                    }

                    //Store in rediss for 30 days
                    String key = "";
                    if (StringUtils.isNotEmpty(phone))
                        key = phone + "_LS_LEAD_EXIST";
                    else
                        key = email + "_LS_LEAD_EXIST";

                    redisDAO.setex(key, "TRUE", 30 * 24 * 60 * 60);

                    //Wait for 3 seconds for the lead record to get created in LS
                    Thread.sleep(3000);

                    break;
                } catch (Exception e) {
                    logger.warn("Error saving lead details email: {}, phone: {}", email, phone, e);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        logger.warn(ex);
                    }
                }
            }
        } catch (BadRequestException e){
            throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
        } catch (Exception e) {
            logger.warn("Error: createLeadIfDeleted", e);
            throw new Exception(e);
        }
    }

    public List<LeadDetails> fetchLeadsByEmailId(String emailId) throws InterruptedException, BadRequestException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = getLeadSquaredURL(LS_LEAD_QUICK_SEARCH_URL) + "&key=" + emailId;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK, "Too many requests for fetchLeadsByEmailId " + emailId + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                leads = gson.fromJson(response, leadDetailsListType);
                logger.info("Lead details: {}",leads);
                return leads;
            }
        }
        return leads;
    }

    public LeadDetails fetchLeadByEmailId(String emailId) throws InterruptedException, BadRequestException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = getLeadSquaredURL(LS_FETCH_BYEMAIL_URL) + "&emailaddress=" + emailId;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.error(LoggingMarkers.JSON_MASK, "Too many requests for fetchLeadByEmailId " + emailId + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {

                logger.info("Lead details: {}",response);
                leads = gson.fromJson(response, leadDetailsListType);

                if(CollectionUtils.isNotEmpty(leads))
                    return leads.get(0);
                return null;
            }
        }

        if(CollectionUtils.isNotEmpty(leads))
            return leads.get(0);

        return null;
    }

    public List<LeadDetails> fetchLeadsByPhone(String phoneNumber) throws InterruptedException, BadRequestException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = getLeadSquaredURL(LS_LEAD_QUICK_SEARCH_URL) + "&key=" + phoneNumber;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
                break;
                //TODO: throw exception
            } else if (cRes.getStatus() == 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "Too many requests for fetchLeadsByPhone " + phoneNumber + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                leads = gson.fromJson(response, leadDetailsListType);
                logger.info(leads);
                return leads;
            }
        }
        return leads;
    }

    public void updateGPSLocalesToLSUsingSNS(String request) {
        logger.info("calling sqs for updatePaymentThroughSNS with request : " + request);
        if (StringUtils.isNotEmpty(request)) {
            awsSQSManager.sendToSQS(SQSQueue.SEND_GPS_LOCALS_TO_LS, SQSMessageType.UPDATE_LEAD_GPS_LEADSQUARED, request, SQSMessageType.UPDATE_LEAD_GPS_LEADSQUARED.name());
        }
    }


    public void updateGPSLocalesToLSUsingSQS(String request) throws VException, IOException, IllegalAccessException {
        logger.info("sqs triggered for updateGPSLocalesToLSUsingSQS with : " + request);
        if (StringUtils.isNotEmpty(request)) {
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> data = gson.fromJson(request, type);
            if (StringUtils.isNotEmpty(data.get("userId"))) {
                logger.info("getting user Details by using userid : " + data.get("userId"));

                UserBasicInfo user = userUtils.getUserBasicInfo(data.get("userId"), true);

                if (user == null || StringUtils.isEmpty(user.getContactNumber())) {
                    throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found");
                }
                logger.info("user Details : " + user.toString());
                Map<String, String> params = new HashMap<>();
                params.put("Phone", user.getContactNumber());
                params.put("mx_GPS_City", data.get("city"));
                params.put("mx_GPS_Long", data.get("longitude"));
                params.put("mx_GPS_lat", data.get("latitude"));
                leadUpsert(params);
            }
        }
    }



    public void leadActivityHomeDemo(LeadSquaredRequest req) throws Exception {

        Map<String,String> param=req.getParams();
        List<HomeDemoRequest>  homeDemoRequests = homeDemoRequestManager.getRecentRequestByContactNumber(param.get("contact"));

        if(CollectionUtils.isEmpty(homeDemoRequests))
            return;

        logger.info("Activities to be pushed {}", homeDemoRequests);

        String eventCode = ConfigUtils.INSTANCE.getStringValue("leadsquared.activity.home.demo");

        List<LeadActivityReq> leadActivityReqs = new ArrayList<>();

        List<HomeDemoRequest> homeDemoRequestsSubList = new ArrayList<>();

        for (HomeDemoRequest request : homeDemoRequests){
            StringBuilder sb = new StringBuilder();
            sb.append("Request Home Demo Activity : ");
            sb.append("\nuserId: ").append(org.apache.commons.lang3.StringUtils.defaultString(request.getUserId(), "NA"));
            LeadActivityReq leadActivity = new LeadActivityReq(null, Integer.parseInt(eventCode), (sb.toString()));
            leadActivity.setPhone(request.getContactNumber());
            leadActivity.setUserId(request.getUserId());

            List<SchemaValuePair> pairs = new ArrayList<>();
            if (StringUtils.isNotEmpty(request.getFullName())) {
                pairs.add(new SchemaValuePair("mx_Custom_1", request.getFullName()));
            }
            if (StringUtils.isNotEmpty(request.getContactNumber())) {
                pairs.add(new SchemaValuePair("mx_Custom_2", request.getContactNumber()));
            }
            if (StringUtils.isNotEmpty(request.getCity())) {
                pairs.add(new SchemaValuePair("mx_Custom_3", request.getCity()));
            }
            if (StringUtils.isNotEmpty(request.getPreferredDate())) {
                pairs.add(new SchemaValuePair("mx_Custom_4", request.getPreferredDate()));
            }
            if (StringUtils.isNotEmpty(request.getPreferredTime())) {
                pairs.add(new SchemaValuePair("mx_Custom_5", request.getPreferredTime()));
            }
            if (StringUtils.isNotEmpty(request.getFullAddress())) {
                pairs.add(new SchemaValuePair("mx_Custom_6", request.getFullAddress()));
            }
            if (StringUtils.isNotEmpty(request.getGrade())) {
                pairs.add(new SchemaValuePair("mx_Custom_7", request.getGrade()));
            }
            if (StringUtils.isNotEmpty(request.getPageURL())) {
                pairs.add(new SchemaValuePair("mx_Custom_8", request.getPageURL().length()>=200?request.getPageURL().substring(0,200):request.getPageURL()));
            }
            if (StringUtils.isNotEmpty(request.getUserId())) {
                pairs.add(new SchemaValuePair("mx_Custom_9", request.getUserId()));
            }

            if(!request.isUserVerified()){

                if(ENV.equalsIgnoreCase("prod") || ENV.equalsIgnoreCase("local")) {

                    List<LeadDetails> leadDetails = fetchLeadsByPhone(request.getContactNumber());
                    if (CollectionUtils.isEmpty(leadDetails)) {

                        User user = new User();
                        user.setFirstName(request.getFullName());
                        user.setContactNumber(request.getContactNumber());

                        String[] name = request.getFullName().trim().split("\\s+");
                        user.setFirstName(name[0]);
                        if (null != name && name.length > 1) {
                            StringBuffer _sb = new StringBuffer();

                            for (int i = 1; i < name.length; i++) {
                                _sb.append(name[i] + " ");
                            }

                            String userLastName = _sb.toString();

                            user.setLastName(userLastName);
                        }

                        LeadSquaredRequest leadSquaredRequest = new LeadSquaredRequest();
                        leadSquaredRequest.setUser(user);
                        leadCreate(leadSquaredRequest);
                    }
                }
            }

            leadActivity.setFields(pairs);
            leadActivityReqs.add(leadActivity);

            homeDemoRequestsSubList.add(request);

            try {

                logger.info("pushing activitiesActivities {}", leadActivityReqs);
                postBulkLeadActivity(leadActivityReqs, HttpMethod.POST);


                for (HomeDemoRequest requestSub : homeDemoRequestsSubList) {
                    requestSub.setActivityPushed(true);
                    homeDemoRequestManager.persist(requestSub);
                }

                logger.info("updated activities push status");

            } catch (Exception e) {
                logger.error("Error in pushing the activity: " + gson.toJson(leadActivity) + " for the home demo request:" + gson.toJson(request), e);
            }
            leadActivityReqs.clear();
            homeDemoRequestsSubList.clear();

        }
        logger.info("updated activities push status");
        logger.info("Done");

    }

    public LeadDetails fetchLeadByPhone(String phoneNumber) throws InterruptedException, BadRequestException {
        List<LeadDetails> leads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = getLeadSquaredURL(LS_GET_LEAD_BY_PHONE) + "&phone=" + phoneNumber;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.error(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "Too many requests for fetchLeadsByPhone " + phoneNumber + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                leads = gson.fromJson(response, leadDetailsListType);

                if(CollectionUtils.isNotEmpty(leads))
                    return leads.get(0);
                return null;
            }
        }

        if(CollectionUtils.isNotEmpty(leads))
            return leads.get(0);

        return null;
    }


    public void requestHomeDemo(LeadSquaredRequest request) throws Exception {

        String contactNumber = request.getParams().get("contactNumber");
        String fullName = request.getParams().get("fullName");
        String city = request.getParams().get("city");
        Long preferredDateTime = request.getParams().containsKey("preferredDateTime")  ? Long.parseLong(request.getParams().get("preferredDateTime")) : null;
        String fullAddress = request.getParams().get("fullAddress");
        String grade = request.getParams().get("grade");
        String pageURL = request.getParams().get("pageURL");

        if(StringUtils.isEmpty(city))
            city = "Other";

        if(StringUtils.isEmpty(contactNumber))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Phone number cannot be empty");

        logger.info("Checking if user present with contact number : {}", contactNumber);
        User user = userManager.getUserByContactNumber(contactNumber);

        boolean isUserVerified = true;

        String userId = null;
        if(user == null) {
            isUserVerified = false;
        }else {
            userId = user.getId().toString();
        }


        HomeDemoRequest homeDemoRequest = new HomeDemoRequest();
        homeDemoRequest.setUserId(userId);
        homeDemoRequest.setContactNumber(contactNumber);
        homeDemoRequest.setFullName(fullName);
        homeDemoRequest.setPageURL(pageURL);
        homeDemoRequest.setUserVerified(isUserVerified);
        homeDemoRequest.setCity(city);
        homeDemoRequest.setFullAddress(fullAddress);
        homeDemoRequest.setGrade(grade);

        String date = "";
        String time = "";
        if(preferredDateTime != null) {
            SimpleDateFormat preferredDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            preferredDateFormat.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
            date = preferredDateFormat.format(new Date(preferredDateTime));
            homeDemoRequest.setPreferredDate(date);

            SimpleDateFormat preferredTimeFormat = new SimpleDateFormat("hh:mm aa");
            preferredTimeFormat.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
            time = preferredTimeFormat.format(new Date(preferredDateTime));
            homeDemoRequest.setPreferredTime(time);
        }
        logger.info("demo upsert {}", homeDemoRequest);
        homeDemoRequestManager.persist(homeDemoRequest);

        try {

            String smsBody = "";

            if ("Other".equalsIgnoreCase(city)) {
                smsBody = "Hi " + fullName + ", Thank your for your interest. Vedantu Academic counsellor will be contacting you shortly for your Online Counselling session.";
            } else {
                smsBody = "Hi " + fullName + ", we have scheduled counselling session at " + date + " " + time + ". Vedantu Academic counsellor will be contacting you at this time for your Online Counselling session.";
            }

            TextSMSRequest textSMSRequest = new TextSMSRequest();
            textSMSRequest.setTo(contactNumber);
            textSMSRequest.setType(CommunicationType.HOME_DEMO_REQUEST);
            textSMSRequest.setBody(smsBody);
            smsManager.sendSMSViaRest(textSMSRequest);
            logger.info("Notification Sent");

            pushToLsRequestHomeDemoAsync(contactNumber);

        } catch (BadRequestException e){
            throw new BadRequestException(e.getErrorCode(),e.getErrorMessage(),e.getLevel());
        }catch (Exception e){
            logger.warn("Error Pushing activity to LS", e);
            throw new Exception(e);
        }

    }

    public void pushToLsRequestHomeDemo(LeadSquaredRequest request) throws Exception {

        leadActivityHomeDemo(request);

    }

    public void pushToLsRequestHomeDemoAsync(String contactNumber) {

        LeadSquaredRequest leadSquaredRequest = new LeadSquaredRequest();
        leadSquaredRequest.setAction(LeadSquaredAction.POST_LEAD_DATA);
        leadSquaredRequest.setDataType(LeadSquaredDataType.LEAD_ACTIVITY_HOME_DEMO);
        Map<String,String> data=leadSquaredRequest.getParams();
        data.put("contact",contactNumber);
        leadSquaredRequest.setParams(data);
        executeAsyncTask(leadSquaredRequest);

    }

    public List<LeadsquaredUser> fetchAgentsByEmailId(String emailId) throws InterruptedException, InternalServerErrorException {

        String key = getAgentDetailsByEmailIdKey(emailId);
        try {
            String resp = redisDAO.get(key);
            if (StringUtils.isNotEmpty(resp)) {
                logger.info("======found am  in cache");
                List<LeadsquaredUser> ags = gson.fromJson(resp, new TypeToken<List<LeadsquaredUser>>() {
                }.getType());
                return ags;
            }
        } catch (Exception e) {
            //swallow
        }

        logger.info("======not found am  in cache, fetching from LS");

        List<LeadsquaredUser> agents = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String serverUrl = appendAccessKeys(LS_FETCH_AGENT_BY_EMAIL) + "&emailaddress=" + emailId;
            ClientResponse cRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
            logger.info(LoggingMarkers.JSON_MASK,"cRes:" + cRes.toString());
            String response = cRes.getEntity(String.class);
            if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "InvalidStatusCodeGetUsers - get lead users not 200 - " + cRes.getStatus() + " cRes:"
                        + cRes.toString());
            } else if (cRes.getStatus() == 429) {
                logger.warn(LoggingMarkers.JSON_MASK, "Too many requests for fetchAgentsByEmailId " + emailId + " , response: " + response);
                Thread.sleep(1000);
                continue;
            } else {
                try {
                    redisDAO.set(key, response);
                } catch (Exception e) {
                    //swallow
                }
                agents = gson.fromJson(response, new TypeToken<List<LeadsquaredUser>>() {
                }.getType());
                return agents;
            }
        }
        return agents;
    }

    public String getAgentDetailsByEmailIdKey(String email) {
        return "LS_AGENT_EMAIL_" + email;
    }

    private String appendAccessKeys(String url) {
        if ("PROD".equals(ConfigUtils.INSTANCE.properties.getProperty("environment"))
                || "LOCAL".equals(ConfigUtils.INSTANCE.properties.getProperty("environment"))
        ) {
            if (url.contains("?")) {
                return url + "&accessKey=" + LS_ACCESS_ID + "&secretKey=" + LS_SECRET_KEY;
            } else {
                return url + "?accessKey=" + LS_ACCESS_ID + "&secretKey=" + LS_SECRET_KEY;
            }
        } else {
            return url;
        }
    }

    private void setFieldsForStudentEnrolled(LeadActivityReq req, Orders order) throws VException, InterruptedException {
        LeadDetails leadDetails = null;
        Boolean isValueAdded = false;
        UserBasicInfo user = fosUtils.getUserBasicInfo(order.getUserId(), true);
        logger.info("->user" + user);
        if (user != null && StringUtils.isNotEmpty(user.getContactNumber())) {
            leadDetails = fetchLeadByPhone(user.getContactNumber());
            if (leadDetails != null) {
                List<SchemaValuePair> pairs = new ArrayList<>();
                pairs.add(new SchemaValuePair("Status", "Enrolled"));
                if (StringUtils.isNotEmpty(leadDetails.getOwnerId())) {
                    pairs.add(new SchemaValuePair("Owner", leadDetails.getOwnerId()));
                }
                if (StringUtils.isNotEmpty(user.getEmail())) {
                    pairs.add(new SchemaValuePair("mx_Custom_1", user.getEmail()));
                }
                if (StringUtils.isNotEmpty(user.getContactNumber())) {
                    pairs.add(new SchemaValuePair("mx_Custom_2", user.getContactNumber()));
                }
                String couponCode = getCouponCodeForStudentEnrolled(order.getItems().get(0).getVedantuDiscountCouponId());
                if (StringUtils.isNotEmpty(couponCode)) {
                    pairs.add(new SchemaValuePair("mx_Custom_3", couponCode));
                }
                if (StringUtils.isNotEmpty(order.getId())) {
                    pairs.add(new SchemaValuePair("mx_Custom_4", order.getId()));
                }
                if (null != order.getAmount()) {
                    pairs.add(new SchemaValuePair("mx_Custom_5", String.valueOf(order.getAmount() / 100)));
                }
                if (StringUtils.isNotEmpty(order.getAgentCode())) {
                    pairs.add(new SchemaValuePair("mx_Custom_6", order.getAgentCode()));
                }
                Integer eventCode = ConfigUtils.INSTANCE.getIntValue("leadsquared.activity.student.enrolled");
                req.setActivityEvent(eventCode);
                req.setActivityNote("Student Enrolled");
                req.setUserId(String.valueOf(order.getUserId()));
                req.setFields(pairs);
                isValueAdded = true;
            } else {
                logger.info("There is no existing lead with phone");
            }
        } else {
            logger.info("user not exists");
        }
        if(!isValueAdded){
            req = null;
        }
    }

    private String getCouponCodeForStudentEnrolled(String couponId) throws VException {
        String code = null;
        if (StringUtils.isNotEmpty(couponId)) {
            List<String> couponIds = Arrays.asList(couponId);
            String couponUrl = DINERO_ENDPOINT + "/coupons/getCouponsByIdsApp";
            ClientResponse couponResp = WebUtils.INSTANCE.doCall(couponUrl, HttpMethod.POST, new Gson().toJson(couponIds));
            VExceptionFactory.INSTANCE.parseAndThrowException(couponResp);
            String jsonString = couponResp.getEntity(String.class);
            List<Coupon> coupons = new Gson().fromJson(jsonString, new TypeToken<List<Coupon>>() {
            }.getType());
            if (ArrayUtils.isNotEmpty(coupons)){
                code = coupons.get(0).getCode();
            }
        }
        return code;
    }

}