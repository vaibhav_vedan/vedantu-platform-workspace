package com.vedantu.platform.managers.onetofew;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.vedantu.onetofew.pojo.BatchEnrolmentInfo;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFUpdateSessionRes;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.scheduling.CalendarManager;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.request.session.UpdateMultipleSlotsReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.response.GetRegistrationSubscriptionResp;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;
import java.util.Collections;
import java.util.Comparator;

@Service
public class OTFAsyncTaskManager {

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private CalendarManager calendarManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFAsyncTaskManager.class);

    private final Gson gson = new Gson();

    private String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    private String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT") + "/";

    private static final int MAX_DAYS_AFTER_TRIAL_WITHOUT_PAYMENT = 7;

    private static final int MAX_DAYS_AFTER_DUE_DATE_WITHOUT_PAYMENT = 10;

    private static final int MAX_COUNT_OF_SESSIONS = 400;

    private static final int[] DAYS_BEFORE_DUE_DATE_FOR_REMINDER = {1, 2};

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");


    public void updateCalendar(EnrollmentPojo enrollmentInfo) throws VException {
        logger.info("Request : " + enrollmentInfo.toString());
        boolean remove = EntityStatus.ACTIVE.equals(enrollmentInfo.getStatus());//remove is technically added

//        String url = ontofewEndpoint + "batch/" + enrollmentInfo.getBatchIds();
//        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
//        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        String jsonString = resp.getEntity(String.class);
//        logger.info(jsonString);
//        BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);
        List<OTFSessionPojoUtils> sessions = getSessions(enrollmentInfo.getBatchId());

        // Block the schedule for the sessions
        try {
            UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(
                    enrollmentInfo.getUser().getUserId(), getSessionSlots(sessions, null, remove), CalendarEntrySlotState.SESSION,
                    !remove, CalendarReferenceType.OTF_BATCH, enrollmentInfo.getBatchId());
            if (remove) {
                calendarManager.markCalendarEntries(updateMultipleSlotsReq);
            } else {
                calendarManager.unmarkCalendarEntries(updateMultipleSlotsReq);
            }
        } catch (VException ex) {
            logger.error("Error blocking calendar for otf batch - " + ex.toString() + " batchInfo : "
                    + enrollmentInfo.toString());
        }
    }

    //handles calender unblocking and blocking for reschedule session
    public void rescheduleSession(OTFUpdateSessionRes updateSessionRes) throws VException {
        OTFSessionPojoUtils preUpdateSession = updateSessionRes.getPreUpdateSession();
        OTFSessionPojoUtils postUpdateSession = updateSessionRes.getPostUpdateSession();

        if (!Objects.equals(preUpdateSession.getStartTime(), postUpdateSession.getStartTime())
                || !Objects.equals(preUpdateSession.getEndTime(), postUpdateSession.getEndTime())
                || !Objects.equals(preUpdateSession.getPresenter(), postUpdateSession.getPresenter())) {

            List<BatchEnrolmentInfo> batchEnrolmentInfo = enrollmentManager.getBatchEnrolmentsByIds( preUpdateSession.getBatchIds());
            List<Long> studentIds = new ArrayList<>();
            batchEnrolmentInfo.forEach(a -> a.getEnrolledStudentIds().forEach( b -> studentIds.add((Long.parseLong(b)))));
            unblockCalenderEntries(preUpdateSession, studentIds);
            blockCalenderEntries(postUpdateSession, studentIds);
        }
    }

    @Deprecated
    public Set<Long> getEnrolledStudents(OTFSessionPojoUtils session) throws VException {

        Set<Long> userIds = new HashSet<>();
        if (session != null && ArrayUtils.isNotEmpty(session.getBatchIds())) {
            String queryString = "";
            for (String batchId : session.getBatchIds()) {
                queryString += ("batchIds=" + batchId + "&");
            }
            String getEnrollmentsUrl = subscriptionEndpoint + "enroll/getActiveStudentEnrollmentsByBatchIds";
            if (!StringUtils.isEmpty(queryString)) {
                getEnrollmentsUrl += "?" + queryString;
            }

            ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
            }.getType();
            List<EnrollmentPojo> enrollments = gson.fromJson(jsonString, listType);

            if (!CollectionUtils.isEmpty(enrollments)) {
                enrollments.forEach(a -> userIds.add((Long.parseLong(a.getUserId()))));
            }
        }
        return userIds;
    }

    @Deprecated
    public void cancelSessionTask(OTFUpdateSessionRes updateSessionRes) throws VException {
        OTFSessionPojoUtils cancelledSession = updateSessionRes.getPreUpdateSession();
        if (cancelledSession != null && cancelledSession.getStartTime() < System.currentTimeMillis()) {
            logger.info("Session is in past/forfeited");
            return;
        }
        List<BatchEnrolmentInfo> batchEnrolmentInfo = enrollmentManager.getBatchEnrolmentsByIds( cancelledSession.getBatchIds());
        List<Long> studentIds = new ArrayList<>();
        batchEnrolmentInfo.forEach(a -> a.getEnrolledStudentIds().forEach( b -> studentIds.add((Long.parseLong(b)))));
        unblockCalenderEntries(cancelledSession, studentIds);
    }

    private void blockCalenderEntries(OTFSessionPojoUtils session, List<Long> studentIds) throws VException {
        // Block the calendar first for students
        List<SessionSlot> sessionSlots = new ArrayList<>();
        sessionSlots.add(session.createSessionSlot());
        UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(studentIds, sessionSlots,
                CalendarEntrySlotState.SESSION, false, CalendarReferenceType.OTF_SESSION,
                session.getId());
        calendarManager.markCalendarEntries(updateMultipleSlotsReq);

        // Block the calendar first for teacher
        if (!StringUtils.isEmpty(session.getPresenter())) {
            sessionSlots = new ArrayList<>();
            sessionSlots.add(session.createSessionSlot());
            updateMultipleSlotsReq = new UpdateMultipleSlotsReq(Long.parseLong(session.getPresenter()),
                    sessionSlots, CalendarEntrySlotState.SESSION, false, CalendarReferenceType.OTF_SESSION,
                    session.getId());
            calendarManager.markCalendarEntries(updateMultipleSlotsReq);
        }

    }

    public void unblockCalenderEntries(OTFSessionPojoUtils session, List<Long> studentIds) throws VException {
        List<SessionSlot> sessionSlots = new ArrayList<>();
        sessionSlots.add(session.createSessionSlot());
        // Unblock the previous times for teacher
        if (!StringUtils.isEmpty(session.getPresenter())) {
            UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(Long.parseLong(session.getPresenter()),
                    sessionSlots, CalendarEntrySlotState.SESSION, false, CalendarReferenceType.OTF_SESSION,
                    session.getId());
            calendarManager.unmarkCalendarEntries(updateMultipleSlotsReq);
        }

        // Unblock the previous times for students
        sessionSlots = new ArrayList<>();
        sessionSlots.add(session.createSessionSlot());
        UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(studentIds, sessionSlots,
                CalendarEntrySlotState.SESSION, true, CalendarReferenceType.OTF_SESSION, session.getId());
        calendarManager.unmarkCalendarEntries(updateMultipleSlotsReq);
    }

    private List<OTFSessionPojoUtils> getSessions(String batchId) throws VException {
        if (StringUtils.isEmpty(batchId)) {
            throw new BadRequestException(com.vedantu.exception.ErrorCode.BAD_REQUEST_ERROR, "batchId is empty");
        }

        String getUpcomingSessionsUrl = schedulingEndpoint + "/onetofew/session/get?batchId=" + batchId + "&fromTime=" + (System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR)
                + "&size=" + MAX_COUNT_OF_SESSIONS;

        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();
        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        return sessions;
    }

    private static List<SessionSlot> getSessionSlots(List<OTFSessionPojoUtils> sessions) {

        return getSessionSlots(sessions, null, null);
    }

    private static List<SessionSlot> getSessionSlots(List<OTFSessionPojoUtils> sessions, String teacherId, Boolean scheduleCheck) {
        // Collect slots from otf agenda
        List<SessionSlot> sessionSlots = new ArrayList<>();

        if (!CollectionUtils.isEmpty(sessions)) {
            for (OTFSessionPojoUtils session : sessions) {
                if (session != null
                        && (StringUtils.isEmpty(teacherId) || teacherId.equals(session.getPresenter()))) {
                    if (Boolean.TRUE.equals(scheduleCheck) && !SessionState.SCHEDULED.equals(session.getState())) {
                        continue;
                    }
                    sessionSlots.add(session.createSessionSlot());
                }
            }
        }

        return sessionSlots;
    }

    public String markEnrollmentStatus(String userId, String batchId, String status, Role role, String orderId)
            throws JSONException, VException {
        return markEnrollmentStatus(userId, batchId, status, role, orderId, null, true);
    }

    public String markEnrollmentStatus(String userId, String batchId, String status,
            Role role, String orderId, String enrollmentId, boolean markOrderForfieted) throws JSONException, VException {
        String updateEnrollmentUrl = subscriptionEndpoint + "enroll/markStatus";
        JSONObject updateEnrollmentReq = new JSONObject();
        updateEnrollmentReq.put("batchId", batchId);
        updateEnrollmentReq.put("userId", userId);
        updateEnrollmentReq.put("status", status);
        updateEnrollmentReq.put("role", role);
        updateEnrollmentReq.put("id", enrollmentId);

        ClientResponse otfupdateResp = WebUtils.INSTANCE.doCall(updateEnrollmentUrl, HttpMethod.POST,
                updateEnrollmentReq.toString());
        VExceptionFactory.INSTANCE.parseAndThrowException(otfupdateResp);
        String updateEnrollmentRes = otfupdateResp.getEntity(String.class);
        logger.info("Response of otf.updateEnrollment " + updateEnrollmentRes);
        EnrollmentPojo enrollmentInfo = new Gson().fromJson(updateEnrollmentRes, EnrollmentPojo.class);

        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentInfo", enrollmentInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_UPDATE_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);

        if (markOrderForfieted) {
            logger.info("marking the order forfeited if the payment is done by instalment and not fully paid");
            try {
                String markOrderForfeited = DINERO_ENDPOINT + "/payment/markOrderForfeited";
                JSONObject markOrderForfeitedReq = new JSONObject();
                if (StringUtils.isNotEmpty(orderId)) {
                    markOrderForfeitedReq.put("orderId", orderId);
                } else {
                    markOrderForfeitedReq.put("entityType", EntityType.OTF);
                    markOrderForfeitedReq.put("entityId", batchId);
                    markOrderForfeitedReq.put("userId", userId);
                }

                ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                        markOrderForfeitedReq.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
                String jsonString = markOrderForfeitedRes.getEntity(String.class);
                logger.info("Response for markOrderForfeited  : " + jsonString);
            } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                logger.error("Error in markOrderForfeited for batchId  " + batchId + ", userId " + userId, ex);
            }
        }
        return updateEnrollmentRes;
    }

    public void checkTrialEnrollments(int start, int size) throws VException, JSONException {
        logger.info("checkTrialEnrollments  start " + start + " size " + size);
        String getEnrollmentsUrl = subscriptionEndpoint + "enroll/checkTrialEnrollments";
        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

    }

    public List<BaseInstalmentInfo> getBaseInstalmentFromRegistration(Long userId, String entityId) throws VException {
        String url = subscriptionEndpoint
                + "registration/getRegistrationsForUserAndEntity";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?userId=" + userId + "&entityId=" + entityId, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
        String jsonString = resp.getEntity(String.class);
//        Type listType = new TypeToken<ArrayList<GetRegistrationSubscriptionResp>>() {
//		}.getType();
        GetRegistrationSubscriptionResp response = new Gson().fromJson(jsonString, GetRegistrationSubscriptionResp.class);
        if (response != null && ArrayUtils.isNotEmpty(response.getInstalmentDetails())) {
            return response.getInstalmentDetails();
        }
        return new ArrayList<>();
    }
}
