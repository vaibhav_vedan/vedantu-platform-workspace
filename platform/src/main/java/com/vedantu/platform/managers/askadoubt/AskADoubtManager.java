package com.vedantu.platform.managers.askadoubt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.askadoubt.DoubtDao;
import com.vedantu.platform.enums.askadoubt.DoubtStatus;
import com.vedantu.platform.request.AddDoubtRequest;
import com.vedantu.platform.pojo.askadoubt.Doubt;
import com.vedantu.platform.pojo.askadoubt.DoubtInfo;
import com.vedantu.platform.async.BroadcastAsync;
import com.vedantu.platform.controllers.BroadcastController;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.pojo.broadcast.Broadcast;
import com.vedantu.platform.pojo.broadcast.BroadcastInfo;
import com.vedantu.platform.pojo.broadcast.SendBroadcastRequest;
import com.vedantu.platform.controllers.notification.ChatController;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.platform.pojo.requestcallback.request.AddRequestCallbackDetailsReq;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import java.util.HashSet;
import java.util.Set;
import org.dozer.DozerBeanMapper;

@Service
public class AskADoubtManager {

    @Autowired
    private DoubtDao doubtDao;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(AskADoubtManager.class);

    @Autowired
    private RequestCallbackManager requestCallbackManager;

    @Autowired
    private ChatController chatController;

    @Autowired
    private BroadcastController broadcastController;

    @Autowired
    private BroadcastAsync broadcastAsync;

    @Autowired
    private FosUtils fosUtils;
    
    @Autowired
    private DozerBeanMapper mapper;     

    public AskADoubtManager() {
        super();
    }

    public List<DoubtInfo> getDoubts(String id, Long userId,
            DoubtStatus status, Long assignedTo, Integer start, Integer size) {

        List<Doubt> doubts = doubtDao.get(id, userId, status, assignedTo,
                start, size);

        return getDoubtInfo(doubts);
    }

    public List<DoubtInfo> getDoubtInfo(List<Doubt> doubts) {

        if (doubts.isEmpty()) {
            return new ArrayList<>();
        }

        List<DoubtInfo> doubtInfos = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();
        List<Long> boardIds = new ArrayList<>();

        for (Doubt doubt : doubts) {
            if (doubt.getUserId() != null) {
                userIds.add(doubt.getUserId());
            }

            if (doubt.getAssignedTo() != null) {
                userIds.add(doubt.getAssignedTo());
            }

            if (doubt.getBoardId() != null) {
                Long boardId = Long.parseLong(doubt.getBoardId());
                if (!boardIds.contains(boardId)) {
                    boardIds.add(boardId);
                }
            }
        }

        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);

        for (Doubt doubt : doubts) {
            UserBasicInfo user = userMap.get(doubt.getUserId());

            UserBasicInfo assignedTo = null;
            if (doubt.getAssignedTo() != null) {
                assignedTo = userMap.get(doubt.getAssignedTo());
            }

            Board subject = null;
            if (doubt.getBoardId() != null) {
                subject = boardMap.get(Long.parseLong(doubt.getBoardId()));
            }
            doubtInfos.add(toDoubtInfo(doubt, user, assignedTo, subject));
        }

        return doubtInfos;

    }

    public Map<String, DoubtInfo> getDoubtInfoMap(List<DoubtInfo> doubtInfos) {
        Map<String, DoubtInfo> doubtInfoMap = new HashMap<String, DoubtInfo>();
        for (DoubtInfo info : doubtInfos) {
            doubtInfoMap.put(info.getId(), info);
        }
        return doubtInfoMap;
    }

    public DoubtInfo toDoubtInfo(Doubt doubt, UserBasicInfo user,
            UserBasicInfo assignedTo, Board subject) {

        DoubtInfo doubtInfo = mapper.map(doubt, DoubtInfo.class);
        doubtInfo.setUser(user);
        doubtInfo.setAssignedToUser(assignedTo);
        doubtInfo.setSubject(subject);

        return doubtInfo;
    }

    public DoubtInfo addDoubts(AddDoubtRequest addDoubtRequest) throws VException {

        try {
            Doubt doubt = new Doubt(addDoubtRequest);
            doubt.setStatus(DoubtStatus.INITIATED);
            doubt.setBroadcastedTime(System.currentTimeMillis());
            doubtDao.create(doubt);

            logger.info("Doubt received from db" + doubt.toString());

            //Todo: Broadcast
            SendBroadcastRequest sendBroadcastRequest = new SendBroadcastRequest(null, BroadcastType.ASKADOUBT, 1, doubt.getId().toString(), doubt.getGrade(), doubt.getBoardId());
            sendBroadcastRequest.setCallingUserId(doubt.getUserId());
            broadcastController.sendBroadcast(sendBroadcastRequest);

            List<Doubt> doubts = new ArrayList<Doubt>();

            doubts.add(doubt);

            if (getDoubtInfo(doubts).get(0) != null) {
                return getDoubtInfo(doubts).get(0);
            } else {
                throw new VException(ErrorCode.SERVICE_ERROR, "Error occured while adding doubt info");
            }
        } catch (Exception e) {
            logger.error("failed to add doubt: " + e.getMessage());
            throw new VException(ErrorCode.SERVICE_ERROR, "Error occured while adding doubt info");
        }
    }

    public void acceptDoubt(Broadcast broadcast) throws VException {

        String referenceId = broadcast.getReferenceId();

        Long toUserId = broadcast.getToUserId();

        Doubt doubt = doubtDao.getById(referenceId);

        doubt.setStatus(DoubtStatus.ACCEPTED);
        doubt.setAssignedTo(toUserId);
        doubt.setLastUpdated(System.currentTimeMillis());
        //doubt.setLastUpdatedBy(toUserId.toString());
        doubt.setDoubtAcceptanceTime(System.currentTimeMillis());
        doubtDao.upsert(doubt, toUserId.toString());

        //initiateChat
        try {
            String chatJson = chatController.initiateChatWithUser(toUserId, doubt.getUserId());

//		JsonObject jsonObject =new Gson().fromJson(chatJson, JsonObject.class);
//		
//		String channelName = jsonObject.get("channelName").getAsString();
            JsonObject offlineChatMessage = new JsonObject();
            offlineChatMessage.addProperty("message", doubt.getDoubtText());
            offlineChatMessage.addProperty("fromUserId", doubt.getUserId());
            offlineChatMessage.addProperty("toUserId", toUserId);

            chatController.replyToOfflineMessage(offlineChatMessage.toString());

            for (String attachment : doubt.getAttachments()) {
                JsonObject offlineChatMessageTemp = new JsonObject();
                offlineChatMessageTemp.addProperty("message", attachment);
                offlineChatMessageTemp.addProperty("fromUserId", doubt.getUserId());
                offlineChatMessageTemp.addProperty("toUserId", toUserId);
                chatController.replyToOfflineMessageFile(offlineChatMessageTemp.toString());
            }

            Thread.sleep(50);

            offlineChatMessage = new JsonObject();
            offlineChatMessage.addProperty("message", "Hello!! Thanks for using Vedantu!");
            offlineChatMessage.addProperty("toUserId", doubt.getUserId());
            offlineChatMessage.addProperty("fromUserId", toUserId);

            chatController.replyToOfflineMessage(offlineChatMessage.toString());

            Thread.sleep(50);

            offlineChatMessage = new JsonObject();
            offlineChatMessage.addProperty("message", "I'm looking over your problem now.\nCan you tell me more about what you don't understand?");
            offlineChatMessage.addProperty("toUserId", doubt.getUserId());
            offlineChatMessage.addProperty("fromUserId", toUserId);

            chatController.replyToOfflineMessage(offlineChatMessage.toString());

            // send notifications
            List<Broadcast> broadcastList = new ArrayList<Broadcast>();
            broadcastList.add(broadcast);
            broadcastAsync.sendNotifications(broadcastList, BroadcastType.ASKADOUBT, "ASK_A_DOUBT_ACCEPTANCE_STUDENT");

        } catch (Exception e) {
            logger.error("failed to initiate chat between : " + toUserId + " :" + doubt.getUserId());
            throw new VException(ErrorCode.CHAT_CREATION_FAILED, e.getMessage());
        }

    }

    public Boolean markDoubtSolutionSubmission(String doubtId, Long callingUserId) throws VException {
        try {

            Doubt doubt = doubtDao.getById(doubtId);
            //Broadcast broadcast = broadcastDao.getById(broadcastId);
            if (doubt == null) {
                throw new VException(ErrorCode.BAD_REQUEST_ERROR, "No broadcast present for this broadcast ID: " + doubtId);
            }

            doubt.setStatus(DoubtStatus.SOLUTION_SUBMITTED);
            doubt.setSolutionSubmissionTime(System.currentTimeMillis());
            doubtDao.upsert(doubt);

            //List<Doubt> doubts = new ArrayList<Doubt>();
            //doubts.add(doubt);
            //List<DoubtInfo> doubtInfos = getDoubtInfo(doubts);
            //	if(doubtInfos==null || doubtInfos.isEmpty()){
            //	throw new VException(ErrorCode.SERVICE_ERROR, "failed to mark solution submitted");
            //}
            //add RCB
            try {
                AddRequestCallbackDetailsReq addRequestCallbackDetailsReq = null;

                String message = "You have recently answered a doubt for this student.";
                if (doubt.getLastUpdatedBy() == null) {
                    addRequestCallbackDetailsReq = new AddRequestCallbackDetailsReq(callingUserId, doubt.getUserId(), doubt.getAssignedTo(), message, Long.parseLong(doubt.getBoardId()), doubt.getGrade().longValue(), "N/A", false);
                } else {
                    addRequestCallbackDetailsReq = new AddRequestCallbackDetailsReq(callingUserId, doubt.getUserId(), doubt.getAssignedTo(), message, Long.parseLong(doubt.getBoardId()), doubt.getGrade().longValue(), "N/A", false);
                }
                requestCallbackManager.addRequestCallback(addRequestCallbackDetailsReq);
            } catch (Exception e) {
                logger.error("Unable to add RCB for broadcast solution acceptance");
            }

            //return doubtInfos.get(0);
            return true;
        } catch (Exception e) {
            throw new VException(ErrorCode.SERVICE_ERROR, "failed to mark solution submitted" + e.getMessage());
        }

    }

    public List<BroadcastInfo> toDoubtInfos(List<String> doubtIds) {
        // TODO Auto-generated method stub
        return null;
    }

}
