/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.feedback;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchEnrolmentInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.feedback.FeedbackFormDAO;
import com.vedantu.platform.dao.feedback.FeedbackFormResponseDAO;
import com.vedantu.platform.dao.feedback.FeedbackParentFormDAO;
import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.enums.feedback.FeedbackDynamicEntity;
import com.vedantu.platform.enums.feedback.FeedbackFormType;
import com.vedantu.platform.enums.feedback.RuleSetType;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.mongodbentities.feedback.FeedbackForm;
import com.vedantu.platform.mongodbentities.feedback.FeedbackFormResponse;
import com.vedantu.platform.mongodbentities.feedback.FeedbackParentForm;
import com.vedantu.platform.pojo.feedback.ContextSharedFormInfo;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamic;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamicSection;
import com.vedantu.platform.pojo.feedback.FeedbackShareMetadata;
import com.vedantu.platform.pojo.feedback.FormNamePojo;
import com.vedantu.platform.pojo.feedback.FormSharedInContextTypeInfo;
import com.vedantu.platform.pojo.feedback.ParentFormContextSharedInfo;
import com.vedantu.platform.pojo.feedback.StudentSharedFormInfo;
import com.vedantu.platform.request.feedback.CreateFeedbackFormReq;
import com.vedantu.platform.request.feedback.ShareFeedbackFormReq;
import com.vedantu.platform.response.feedback.GetContextFormSharedInfoResponse;
import com.vedantu.platform.response.feedback.GetFormsSharedInContextResponse;
import com.vedantu.platform.response.feedback.GetFormsSharedWithStudentResponse;
import com.vedantu.platform.response.feedback.GetParentFormSharedInfoResponse;
import com.vedantu.platform.response.feedback.GetParentFormsResponse;
import com.vedantu.platform.response.feedback.UserFeedbackFormResponseInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class FeedbackParentFormManager {

    @Autowired
    private FeedbackParentFormDAO feedbackParentFormDAO;

    @Autowired
    private FeedbackFormDAO feedbackFormDAO;

    @Autowired
    private FeedbackFormResponseDAO feedbackFormResponseDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private LogFactory logfactory;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(FeedbackParentFormManager.class);

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private final String schedulingEndPoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private final String fosEndPoint = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

    private final Gson gson = new Gson();

    public void processFeedbackFormForInstanceCreation() {
        Long currentTime = System.currentTimeMillis();
        List<FeedbackParentForm> feedbackParentForms = feedbackParentFormDAO.getParentFormsToEvaluate(currentTime);
        logger.info("FeedbackParentForms: " + feedbackParentForms);
        if (ArrayUtils.isNotEmpty(feedbackParentForms)) {
            for (FeedbackParentForm feedbackParentForm : feedbackParentForms) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("feedbackParentForm", feedbackParentForm);
                payload.put("time", currentTime);
                AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.CREATE_FEEDBACK_FORM_INSTANCE, payload);
                asyncTaskFactory.executeTask(asyncTaskParams);
                // processFeedbackForm(feedbackParentForm, currentTime);
            }
        }

    }

    public List<EnrollmentPojo> getEnrollmentsForBatchId(String batchId) throws VException {
        String url = subscriptionEndPoint + "/enroll/getActiveStudentEnrollmentsByBatchIds?batchIds=" + batchId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type enrollmentsListType = new TypeToken<List<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> enrollmentPojos = gson.fromJson(jsonString, enrollmentsListType);
        return enrollmentPojos;
    }

    public List<String> getTeacherForBatchId(String batchId, Long startTime, Long endTime) throws VException {

        String url = schedulingEndPoint + "/onetofew/session/getTeacherIdsForBatchSessions?batchId="
                + batchId + "&startTime=" + startTime + "&endTime="
                + endTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type sessionTeachListType = new TypeToken<Set<String>>() {
        }.getType();
        Set<String> teacherIds = gson.fromJson(jsonString, sessionTeachListType);
        if(ArrayUtils.isEmpty(teacherIds)){
            return new ArrayList<>();
        }

        String burl = subscriptionEndPoint + "/batch/getNonTeachersIds/" + batchId;
        ClientResponse resp1 = WebUtils.INSTANCE.doCall(burl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
        String jsonString1 = resp1.getEntity(String.class);
        Type sessionTeachListType1 = new TypeToken<List<String>>() {
        }.getType();
        List<String> nonTeacherIds = gson.fromJson(jsonString1, sessionTeachListType1);
        logger.info("nonTeacherIds "+nonTeacherIds);
        
        teacherIds.removeAll(nonTeacherIds);
        
        logger.info("teacherIds "+teacherIds);
        List<String> teacherNames=new ArrayList<>();
        if(ArrayUtils.isNotEmpty(teacherIds)){
            List<UserBasicInfo> users = fosUtils.getUserBasicInfosSet(teacherIds, false);
            if(ArrayUtils.isNotEmpty(users)){
                for(UserBasicInfo userBasicInfo : users){
                    teacherNames.add(userBasicInfo.getFirstName()+" "+userBasicInfo.getLastName());
                }
            }
        }        
        return teacherNames;
    }

    public List<FeedbackFormDynamicSection> getPopulatedDynamicSections(FeedbackParentForm feedbackParentForm, FeedbackShareMetadata feedbackShareMetadata, Long time) throws VException {

        List<FeedbackForm> feedbackForms = feedbackFormDAO.getFeedbackFormsForParentIdAndContextId(feedbackParentForm.getId(), feedbackShareMetadata.getContextId());
        Long lastSharedTime = null;
        List<FeedbackFormDynamicSection> feedbackFormDynamicSections = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(feedbackForms)) {
            for (FeedbackForm feedbackForm : feedbackForms) {

                if (lastSharedTime == null) {
                    lastSharedTime = feedbackForm.getCreationTime();
                }

                if (feedbackForm.getCreationTime() > lastSharedTime) {
                    lastSharedTime = feedbackForm.getCreationTime();
                }
            }
        }
        if (lastSharedTime == null) {

            lastSharedTime = time - (long) 30 * DateTimeUtils.MILLIS_PER_DAY;

        }

        logger.info("Time range => " + lastSharedTime + " :: " + time);
        FeedbackFormDynamic feedbackFormDynamic = feedbackParentForm.getDynamicSection();

        if (feedbackFormDynamic != null) {

            if (FeedbackDynamicEntity.SESSION_TEACHERS.equals(feedbackFormDynamic.getFeedbackDynamicEntity())) {
                List<String> teachers = getTeacherForBatchId(feedbackShareMetadata.getContextId(), lastSharedTime, time);

                logger.info("teachers: " + teachers);

                if (ArrayUtils.isNotEmpty(teachers)) {

                    for (String teacherName : teachers) {

                        FeedbackFormDynamicSection feedbackFormDynamicSection = new FeedbackFormDynamicSection();
                        feedbackFormDynamicSection.setDynamicEntityName(teacherName);
                        feedbackFormDynamicSection.setQuestions(feedbackFormDynamic.getQuestions());
                        feedbackFormDynamicSections.add(feedbackFormDynamicSection);

                    }

                }

            }

        }
        return feedbackFormDynamicSections;
    }

    public void processFeedbackForm(FeedbackParentForm feedbackParentForm, Long time) {
        if (time == null) {
            time = System.currentTimeMillis();
        }
        logger.info("processing: " + feedbackParentForm);
        if (ArrayUtils.isNotEmpty(feedbackParentForm.getFeedbackShareMetadatas())) {
            for (FeedbackShareMetadata feedbackShareMetadata : feedbackParentForm.getFeedbackShareMetadatas()) {
                if (!EntityState.ACTIVE.equals(feedbackShareMetadata.getEntityState())) {
                    continue;
                }
                try {
                    if (feedbackShareMetadata.getValidTill() > time) {

                        if (RuleSetType.FIXED_DATE.equals(feedbackShareMetadata.getRuleSetType())) {
                            logger.info("Putting session teachers: ");
                            Long dayStartTime = DateTimeUtils.getISTDayStartTime(time);
                            Long monthStartTime = DateTimeUtils.getISTMonthStartTime(time);
                            Long sharingTime = monthStartTime + (long) (feedbackShareMetadata.getRuleSetValue() - 1) * DateTimeUtils.MILLIS_PER_DAY;
                            if (dayStartTime + DateTimeUtils.MILLIS_PER_DAY == DateTimeUtils.getISTMonthStartTime(sharingTime)) {
                                sharingTime = dayStartTime;
                            }
                            logger.info("dayStart Time: " + dayStartTime);
                            logger.info("monthStartTime: " + monthStartTime);
                            logger.info("sharingTine: " + sharingTime);
                            if (dayStartTime.equals(sharingTime)) {
                                logger.info("Satisfied:");
                                List<BatchEnrolmentInfo> enrollments = new ArrayList<>();
                                if (EntityType.OTF.equals(feedbackParentForm.getContextType())) {
                                    Set<Long> userIds = new HashSet<>();
                                    BatchBasicInfo batchBasicInfo = getBatchInfo(feedbackShareMetadata.getContextId());
                                    Set<String> context = new HashSet<String>();
                                    context.add(feedbackShareMetadata.getContextId());

                                    enrollments = enrollmentManager.getBatchEnrolmentsByIds(context);
                                    if (ArrayUtils.isNotEmpty(enrollments)) {
                                        for (String uId : enrollments.get(0).getEnrolledStudentIds()) {
                                            userIds.add(Long.parseLong(uId));
                                        }
                                        FeedbackForm feedbackForm = new FeedbackForm();
                                        feedbackForm.setContextId(feedbackShareMetadata.getContextId());
                                        feedbackForm.setContextType(feedbackParentForm.getContextType());
                                        feedbackForm.setExpiresIn(feedbackShareMetadata.getExpiresIn());
                                        feedbackForm.setFeedbackCommunication(feedbackParentForm.getCommunication());
                                        feedbackForm.setFeedbackFormType(feedbackParentForm.getFeedbackFormType());
                                        feedbackForm.setFormTitle(feedbackParentForm.getFormTitle());
                                        feedbackForm.setParentFormId(feedbackParentForm.getId());
                                        feedbackForm.setStaticQuestions(feedbackParentForm.getStaticFeedbackQuestions());
                                        feedbackForm.setUserIds(userIds);
                                        feedbackForm.setContextName(batchBasicInfo.getCourseInfo().getTitle());
                                        //feedbackForm.s
                                        List<FeedbackFormDynamicSection> feedbackFormDynamicSections = getPopulatedDynamicSections(feedbackParentForm, feedbackShareMetadata, time);

                                        for (FeedbackFormDynamicSection feedbackFormDynamicSection : feedbackFormDynamicSections) {
                                            feedbackFormDynamicSection.setUserIds(userIds);
                                        }

                                        if (feedbackParentForm.getDynamicSection() != null && ArrayUtils.isEmpty(feedbackFormDynamicSections)) {
                                            continue;
                                        }
                                        feedbackForm.setFeedbackFormDynamicSections(feedbackFormDynamicSections);
                                        feedbackFormDAO.create(feedbackForm, null);
                                        if (FeedbackCommunication.BOTH.equals(feedbackForm.getFeedbackCommunication()) || FeedbackCommunication.EMAIL.equals(feedbackForm.getFeedbackCommunication())) {
                                            for (Long userId : feedbackForm.getUserIds()) {
                                                Map<String, Object> payload = new HashMap<>();
                                                payload.put("feedbackForm", feedbackForm);
                                                payload.put("userId", userId);
                                                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_MAIL_FOR_FEEDBACK_FORM, payload);
                                                asyncTaskFactory.executeTask(params);
                                            }
                                        }
                                    }

                                } else if (EntityType.OTM_BUNDLE.equals(feedbackParentForm.getContextType())) {

                                    FeedbackForm feedbackForm = getFeedbackFormForGroupName(feedbackShareMetadata, feedbackParentForm);

                                    if (ArrayUtils.isEmpty(feedbackForm.getUserIds())) {
                                        continue;
                                    }

                                    if (feedbackParentForm.getDynamicSection() != null && ArrayUtils.isEmpty(feedbackForm.getFeedbackFormDynamicSections())) {
                                        continue;
                                    }

                                    feedbackFormDAO.create(feedbackForm, null);
                                    if (FeedbackCommunication.BOTH.equals(feedbackForm.getFeedbackCommunication()) || FeedbackCommunication.EMAIL.equals(feedbackForm.getFeedbackCommunication())) {
                                        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(feedbackForm.getUserIds(), true);
                                        for (Long userId : feedbackForm.getUserIds()) {
                                            UserBasicInfo userBasicInfo = userMap.get(userId);
                                            sendMail(userBasicInfo, feedbackForm);
                                            //Map<String, Object> payload = new HashMap<>();
                                            //payload.put("feedbackForm", feedbackForm);
                                            //payload.put("userId", userId);
                                            //AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_MAIL_FOR_FEEDBACK_FORM, payload);
                                            //asyncTaskFactory.executeTask(params);
                                        }
                                    }

                                }

                            }

                        } else if (RuleSetType.SESSION_COUNT.equals(feedbackShareMetadata.getRuleSetType())) {

                        }

                    }
                } catch (Exception ex) {
                    logger.error("Error in processing feedback form share metadata for: " + feedbackParentForm.getId() + ", exception: " + ex.getMessage());
                }

            }
        }

    }

    public FeedbackForm getFeedbackFormForGroupName(FeedbackShareMetadata feedbackShareMetadata, FeedbackParentForm feedbackParentForm) throws VException {

        String groupName = feedbackShareMetadata.getContextId();
        List<BatchBasicInfo> batchBasicInfos = getBatchBasicInfosForGroupName(groupName);
        List<FeedbackForm> feedbackForms = feedbackFormDAO.getFeedbackFormsForParentIdAndContextId(feedbackParentForm.getId(), feedbackShareMetadata.getContextId());
        Long lastSharedTime = null;
        Long time = System.currentTimeMillis();
        List<FeedbackFormDynamicSection> feedbackFormDynamicSections = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(feedbackForms)) {
            for (FeedbackForm feedbackForm : feedbackForms) {

                if (lastSharedTime == null) {
                    lastSharedTime = feedbackForm.getCreationTime();
                }

                if (feedbackForm.getCreationTime() > lastSharedTime) {
                    lastSharedTime = feedbackForm.getCreationTime();
                }
            }
        }
        if (lastSharedTime == null) {

            lastSharedTime = time - (long) 30 * DateTimeUtils.MILLIS_PER_DAY;

        }

        logger.info("Time range => " + lastSharedTime + " :: " + time);
        FeedbackFormDynamic feedbackFormDynamic = feedbackParentForm.getDynamicSection();
        Map<String, Set<Long>> teacherUserMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(batchBasicInfos)) {
            Set<String> context = new HashSet<String>();
            for (BatchBasicInfo batchBasicInfo : batchBasicInfos) {
                context.add(batchBasicInfo.getBatchId());
            }
            Map<String,BatchEnrolmentInfo> BatchEnrolmentMap = new HashMap<>();
            List<BatchEnrolmentInfo> enrollmentPojos = enrollmentManager.getBatchEnrolmentsByIds(context);
            for (BatchEnrolmentInfo enrollment : enrollmentPojos) {
                BatchEnrolmentMap.put(enrollment.getBatchId(),enrollment);
            }
            for (BatchBasicInfo batchBasicInfo : batchBasicInfos) {

                Set<Long> userIds = new HashSet<>();
                BatchEnrolmentInfo batchEnrolmentInfo  =   BatchEnrolmentMap.get(batchBasicInfo.getBatchId());
                if (batchEnrolmentInfo == null || ArrayUtils.isEmpty(batchEnrolmentInfo.getEnrolledStudentIds())) {
                    continue;
                }
                for (String uId : batchEnrolmentInfo.getEnrolledStudentIds()) {
                    userIds.add(Long.parseLong(uId));
                }
                List<String> batchTeachers = getTeacherForBatchId(batchBasicInfo.getId(), lastSharedTime, time);

                if (ArrayUtils.isNotEmpty(batchTeachers)) {
                    for (String teacherName : batchTeachers) {
                        if (!teacherUserMap.containsKey(teacherName)) {
                            teacherUserMap.put(teacherName, new HashSet<>());
                        }
                        teacherUserMap.get(teacherName).addAll(userIds);
                    }
                }

            }
        }
        Set<Long> allUserIds = new HashSet<>();
        for (Map.Entry<String, Set<Long>> entry : teacherUserMap.entrySet()) {

            FeedbackFormDynamicSection feedbackFormDynamicSection = new FeedbackFormDynamicSection();
            feedbackFormDynamicSection.setUserIds(entry.getValue());
            allUserIds.addAll(entry.getValue());
            feedbackFormDynamicSection.setDynamicEntityName(entry.getKey());
            feedbackFormDynamicSection.setQuestions(feedbackFormDynamic.getQuestions());
            feedbackFormDynamicSections.add(feedbackFormDynamicSection);

        }
        // OTFBundleInfo oTFBundleInfo = getBundleInfo(groupName);
        List<FormNamePojo> formNames = new ArrayList<>();
        FormNamePojo formNamePojo = new FormNamePojo();
        // formNamePojo.setContextId(groupName);
        // formNamePojo.setContextName(oTFBundleInfo.getTitle());
        // formNames.add(formNamePojo);
        for (BatchBasicInfo batchBasicInfo : batchBasicInfos) {
            //formNamePojo = new FormNamePojo();
            FormNamePojo fnp = new FormNamePojo();
            fnp.setContextId(batchBasicInfo.getId());
            fnp.setContextName(batchBasicInfo.getCourseInfo().getTitle());
            formNames.add(fnp);
        }

        FeedbackForm feedbackForm = new FeedbackForm();
        feedbackForm.setFormNames(formNames);
        feedbackForm.setContextId(feedbackShareMetadata.getContextId());
        feedbackForm.setContextType(feedbackParentForm.getContextType());
        feedbackForm.setParentFormId(feedbackParentForm.getId());
        feedbackForm.setExpiresIn(feedbackShareMetadata.getExpiresIn());
        feedbackForm.setFeedbackCommunication(feedbackParentForm.getCommunication());
        feedbackForm.setFormTitle(feedbackParentForm.getFormTitle());
        feedbackForm.setFeedbackFormType(FeedbackFormType.TEACHER_FEEDBACK);
        feedbackForm.setFeedbackFormDynamicSections(feedbackFormDynamicSections);
        feedbackForm.setUserIds(allUserIds);
        feedbackForm.setStatus(EntityStatus.ACTIVE);
        feedbackForm.setStaticQuestions(feedbackParentForm.getStaticFeedbackQuestions());
        return feedbackForm;

    }

    public List<BatchBasicInfo> getBatchBasicInfosForGroupName(String groupName) throws VException {
        String url = subscriptionEndPoint + "/batch/getBatchBasicInfosForGroupName?groupName=" + WebUtils.getUrlEncodedValue(groupName);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type batchBasicInfoListType = new TypeToken<List<BatchBasicInfo>>() {
        }.getType();
        List<BatchBasicInfo> batchBasicInfos = gson.fromJson(jsonString, batchBasicInfoListType);
        return batchBasicInfos;
    }

    public GetParentFormsResponse getParentForms(int start, int size) {

        List<FeedbackParentForm> feedbackParentForms = feedbackParentFormDAO.getParentForms(start, size);

        GetParentFormsResponse getParentFormsResponse = new GetParentFormsResponse();
        if (ArrayUtils.isNotEmpty(feedbackParentForms)) {
            getParentFormsResponse.setParentForms(feedbackParentForms);
        }
        return getParentFormsResponse;

    }

    public PlatformBasicResponse shareParentFeedbackForm(ShareFeedbackFormReq req) throws NotFoundException, ConflictException, VException {
        FeedbackParentForm feedbackParentForm = feedbackParentFormDAO.getParenFormById(req.getParentFormId());
        if (feedbackParentForm == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Parent feedback not found");
        }

        if (EntityType.OTF.equals(feedbackParentForm.getContextType())) {
            BatchBasicInfo basicInfo = getBatchInfo(req.getContextId());
            if (basicInfo == null) {
                throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "Batch not found");
            }
        }

        if (EntityType.OTM_BUNDLE.equals(feedbackParentForm.getContextType())) {
            List<BatchBasicInfo> batchBasicInfos = getBatchBasicInfosForGroupName(req.getContextId());
            if (ArrayUtils.isEmpty(batchBasicInfos)) {
                throw new NotFoundException(ErrorCode.OTM_BUNDLE_NOT_FOUND, "bundle not found");
            }
        }

        /* else{
            OTFBundleInfo oTFBundleInfo = getBundleInfo(req.getContextId());
            if(oTFBundleInfo == null){
                throw new NotFoundException(ErrorCode.OTM_BUNDLE_NOT_FOUND, "bundle not found");
            }
        } */
        if (ArrayUtils.isNotEmpty(feedbackParentForm.getFeedbackShareMetadatas())) {
            for (FeedbackShareMetadata feedbackShareMetadata : feedbackParentForm.getFeedbackShareMetadatas()) {
                if (req.getContextId().equals(feedbackShareMetadata.getContextId())) {
                    throw new ConflictException(ErrorCode.FEEDBACK_FORM_ALREADY_SHARED, "already shared in batch/course");
                }
            }
        } else {
            feedbackParentForm.setFeedbackShareMetadatas(new ArrayList<>());
        }

        FeedbackShareMetadata feedbackShareMetadata = new FeedbackShareMetadata();
        feedbackShareMetadata.setSharedOn(System.currentTimeMillis());
        feedbackShareMetadata.setContextId(req.getContextId());
        feedbackShareMetadata.setRuleSetType(req.getRuleSetType());
        feedbackShareMetadata.setRuleSetValue(req.getRuleSetValue());
        feedbackShareMetadata.setExpiresIn(req.getExpiresIn());
        feedbackShareMetadata.setValidTill(req.getValidTill());
        feedbackShareMetadata.setEntityState(EntityState.ACTIVE);

        feedbackParentForm.getFeedbackShareMetadatas().add(feedbackShareMetadata);
        feedbackParentFormDAO.save(feedbackParentForm);

        return new PlatformBasicResponse(true, null, null);

    }

    public PlatformBasicResponse createParentForm(CreateFeedbackFormReq req) throws ForbiddenException {
        FeedbackParentForm feedbackParentForm = new FeedbackParentForm();
        feedbackParentForm.setCommunication(req.getCommunication());
        feedbackParentForm.setContextType(req.getContextType());
        if (req.getDynamicSection() != null && ArrayUtils.isNotEmpty(req.getDynamicSection().getQuestions())) {
            feedbackParentForm.setDynamicSection(req.getDynamicSection());
        }
        feedbackParentForm.setFeedbackFormType(req.getFeedbackFormType());
        feedbackParentForm.setFormTitle(req.getFormTitle());
        feedbackParentForm.setStaticFeedbackQuestions(req.getStaticFeedbackQuestions());

        if (!EntityType.OTM_BUNDLE.equals(req.getContextType())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Only bundle forms allowed");
        }

        if (req.getDynamicSection() != null && !FeedbackDynamicEntity.SESSION_TEACHERS.equals(req.getDynamicSection().getFeedbackDynamicEntity())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "only session teachers allowed as dynamic entity");
        }

        feedbackParentFormDAO.save(feedbackParentForm);
        return new PlatformBasicResponse(true, null, null);
    }

    public GetParentFormSharedInfoResponse getParenFormSharedInfo(String parentFormId) throws NotFoundException {
        GetParentFormSharedInfoResponse getParentFormSharedInfoResponse = new GetParentFormSharedInfoResponse();
        FeedbackParentForm feedbackParentForm = feedbackParentFormDAO.getParenFormById(parentFormId);
        if (feedbackParentForm == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, parentFormId);
        }
        List<ParentFormContextSharedInfo> parentFormContextSharedInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(feedbackParentForm.getFeedbackShareMetadatas())) {
            for (FeedbackShareMetadata feedbackShareMetadata : feedbackParentForm.getFeedbackShareMetadatas()) {

                ParentFormContextSharedInfo parentFormContextSharedInfo = new ParentFormContextSharedInfo();
                parentFormContextSharedInfo.setContextId(feedbackShareMetadata.getContextId());
                parentFormContextSharedInfo.setRuleSetType(feedbackShareMetadata.getRuleSetType());
                parentFormContextSharedInfo.setRuleSetValue(feedbackShareMetadata.getRuleSetValue());
                parentFormContextSharedInfo.setState(feedbackShareMetadata.getEntityState());
                parentFormContextSharedInfo.setExpiresIn(feedbackShareMetadata.getExpiresIn());
                parentFormContextSharedInfo.setValidTill(feedbackShareMetadata.getValidTill());
                Long numOfShares = feedbackFormDAO.getCountOfFormsSharedInContextForParentForm(parentFormId, feedbackShareMetadata.getContextId());

                if (numOfShares != null) {
                    parentFormContextSharedInfo.setNumOfShare(numOfShares.intValue());
                } else {
                    parentFormContextSharedInfo.setNumOfShare(0);
                }
                parentFormContextSharedInfos.add(parentFormContextSharedInfo);

            }
        }

        getParentFormSharedInfoResponse.setSharedInfos(parentFormContextSharedInfos);
        getParentFormSharedInfoResponse.setContextType(feedbackParentForm.getContextType());
        return getParentFormSharedInfoResponse;

    }

    public GetContextFormSharedInfoResponse getContextFormSharedInfo(String parentFormId, String contextId) {
        GetContextFormSharedInfoResponse getContextFormSharedInfoResponse = new GetContextFormSharedInfoResponse();
        List<ContextSharedFormInfo> contextSharedFormInfos = new ArrayList<>();
        List<FeedbackForm> feedbackFormsShared = feedbackFormDAO.getFeedbackFormsForParentIdAndContextId(parentFormId, contextId);

        if (ArrayUtils.isNotEmpty(feedbackFormsShared)) {
            for (FeedbackForm feedbackForm : feedbackFormsShared) {
                ContextSharedFormInfo contextSharedFormInfo = new ContextSharedFormInfo();
                contextSharedFormInfo.setContextId(contextId);
                contextSharedFormInfo.setSharedOn(feedbackForm.getCreationTime());
                contextSharedFormInfo.setNumOfResponses(feedbackForm.getRespondedBy().size());
                contextSharedFormInfo.setFormId(feedbackForm.getId());
                contextSharedFormInfo.setNumOfStudentsSharedWith(feedbackForm.getUserIds().size());
                contextSharedFormInfos.add(contextSharedFormInfo);
            }
        }

        getContextFormSharedInfoResponse.setContextSharedFormInfos(contextSharedFormInfos);
        return getContextFormSharedInfoResponse;

    }

    public GetFormsSharedInContextResponse getFormsSharedInContextResponse(String contextId, int start, int size) {
        List<FeedbackForm> feedbackForms = feedbackFormDAO.getFeedbackFormsForContextId(contextId, start, size);
        List<FormSharedInContextTypeInfo> formSharedInContextTypeInfos = new ArrayList<>();
        for (FeedbackForm feedbackForm : feedbackForms) {

            FormSharedInContextTypeInfo formSharedInContextTypeInfo = new FormSharedInContextTypeInfo();
            formSharedInContextTypeInfo.setFeedbackFormType(feedbackForm.getFeedbackFormType());
            formSharedInContextTypeInfo.setFormId(feedbackForm.getId());
            formSharedInContextTypeInfo.setFormName(feedbackForm.getFormTitle());
            formSharedInContextTypeInfo.setNumOfResponses(feedbackForm.getRespondedBy().size());
            formSharedInContextTypeInfo.setSharedOn(feedbackForm.getCreationTime());
            formSharedInContextTypeInfos.add(formSharedInContextTypeInfo);

        }
        GetFormsSharedInContextResponse getFormsSharedInContextResponse = new GetFormsSharedInContextResponse();
        getFormsSharedInContextResponse.setFormSharedInfos(formSharedInContextTypeInfos);
        return getFormsSharedInContextResponse;

    }

    public GetFormsSharedWithStudentResponse getFormsSharedWithStudent(Long userId, int start, int size) {
        GetFormsSharedWithStudentResponse getFormsSharedWithStudentResponse = new GetFormsSharedWithStudentResponse();

        List<FeedbackForm> feedbackForms = feedbackFormDAO.getFormForUser(userId, start, size);
        List<FeedbackFormResponse> feedbackFormResponses = feedbackFormResponseDAO.getResponsesForUser(userId);

        Map<String, FeedbackFormResponse> responseMap = new HashMap<>();

        for (FeedbackFormResponse feedbackFormResponse : feedbackFormResponses) {
            responseMap.put(feedbackFormResponse.getFormId(), feedbackFormResponse);
        }

        List<StudentSharedFormInfo> sharedFormInfos = new ArrayList<>();

        if (ArrayUtils.isNotEmpty(feedbackForms)) {
            for (FeedbackForm feedbackForm : feedbackForms) {
                StudentSharedFormInfo sharedFormInfo = new StudentSharedFormInfo();
                sharedFormInfo.setContextId(feedbackForm.getContextId());
                sharedFormInfo.setFeedbackFormType(feedbackForm.getFeedbackFormType());
                sharedFormInfo.setFormName(feedbackForm.getFormTitle());
                sharedFormInfo.setSharedOn(feedbackForm.getCreationTime());
                sharedFormInfo.setContextType(feedbackForm.getContextType());
                sharedFormInfo.setFormId(feedbackForm.getId());
                if (feedbackForm.getRespondedBy().contains(userId)) {
                    sharedFormInfo.setResponded(true);
                    FeedbackFormResponse feedbackFormResponse = responseMap.get(feedbackForm.getId());
                    if (feedbackFormResponse != null) {
                        sharedFormInfo.setRespondedOn(feedbackFormResponse.getCreationTime());
                    }
                }

                sharedFormInfos.add(sharedFormInfo);
            }
        }
        getFormsSharedWithStudentResponse.setStudentSharedFormInfos(sharedFormInfos);
        return getFormsSharedWithStudentResponse;

    }

    public List<UserFeedbackFormResponseInfo> getFeedbackFormResponses(String formId, Long userId) {

        List<FeedbackFormResponse> feedbackFormResponses = feedbackFormResponseDAO.getResponsesForForm(formId, userId);
        Set<Long> userIds = new HashSet<>();

        List<UserFeedbackFormResponseInfo> userFeedbackFormResponseInfos = new ArrayList<>();

        if (ArrayUtils.isEmpty(feedbackFormResponses)) {
            return userFeedbackFormResponseInfos;
        }

        for (FeedbackFormResponse feedbackFormResponse : feedbackFormResponses) {
            userIds.add(feedbackFormResponse.getUserId());
        }

        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);

        for (FeedbackFormResponse feedbackFormResponse : feedbackFormResponses) {
            UserFeedbackFormResponseInfo userFeedbackFormResponseInfo = mapper.map(feedbackFormResponse, UserFeedbackFormResponseInfo.class);

            UserBasicInfo userBasicInfo = userMap.get(feedbackFormResponse.getUserId());
            if (userBasicInfo != null) {
                userFeedbackFormResponseInfo.setContactNumber(userBasicInfo.getContactNumber());
                userFeedbackFormResponseInfo.setEmail(userBasicInfo.getEmail());
                userFeedbackFormResponseInfo.setName(userBasicInfo.getFullName());
            }

            userFeedbackFormResponseInfos.add(userFeedbackFormResponseInfo);
        }

        return userFeedbackFormResponseInfos;

    }

    public void markFormShareStatus(String contextId, String parentFormId, EntityState entityState) throws NotFoundException, BadRequestException {

        FeedbackParentForm feedbackParentForm = feedbackParentFormDAO.getParenFormById(parentFormId);

        if (feedbackParentForm == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Form not found");
        }

        if (ArrayUtils.isNotEmpty(feedbackParentForm.getFeedbackShareMetadatas())) {
            for (FeedbackShareMetadata feedbackShareMetadata : feedbackParentForm.getFeedbackShareMetadatas()) {
                if (feedbackShareMetadata.getContextId().equals(contextId)) {
                    if (entityState.equals(feedbackShareMetadata.getEntityState())) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Already marked");
                    }
                    feedbackShareMetadata.setEntityState(entityState);
                    break;
                }
            }
        }
        feedbackParentFormDAO.save(feedbackParentForm);

    }

    public OTFBundleInfo getBundleInfo(String contextId) throws VException {
        String url = subscriptionEndPoint + "/otfBundle/getBundleInfoByGroupName?groupName=" + contextId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        // Type enrollmentsListType = new TypeToken<List<EnrollmentPojo>>(){}.getType();
        OTFBundleInfo oTFBundleInfo = gson.fromJson(jsonString, OTFBundleInfo.class);
        return oTFBundleInfo;
    }

    public BatchBasicInfo getBatchInfo(String batchId) throws VException {
        String url = subscriptionEndPoint + "/batch/basicInfo/" + batchId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        // Type enrollmentsListType = new TypeToken<List<EnrollmentPojo>>(){}.getType();
        BatchBasicInfo batchBasicInfo = gson.fromJson(jsonString, BatchBasicInfo.class);
        return batchBasicInfo;
    }

    public void sendMail(Long userId, FeedbackForm feedbackForm){

        if (!(EntityType.OTM_BUNDLE.equals(feedbackForm.getContextType()))) {
            return;
        }

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);
        //BatchBasicInfo batchBasicInfo = getBatchInfo(feedbackForm.getContextId());

        if(userBasicInfo == null){
            logger.error("No user found");
            return;
        }
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("userName", userBasicInfo.getFullName());
        bodyScopes.put("feedbackFormLink", fosEndPoint + "/studentfbform/" + feedbackForm.getId());
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("courseName", "Vedantu");
        try {
            communicationManager.sendMailForFeedbackForm(bodyScopes, subjectScopes, userBasicInfo.getEmail(), userBasicInfo.getFullName());
        } catch (Exception ex) {
            logger.error("Error while sending email for feedback: " + ex.getMessage());
        }
    }
    
    public void sendMail(UserBasicInfo userBasicInfo, FeedbackForm feedbackForm){

        if (!(EntityType.OTM_BUNDLE.equals(feedbackForm.getContextType()))) {
            return;
        }

        //BatchBasicInfo batchBasicInfo = getBatchInfo(feedbackForm.getContextId());

        if(userBasicInfo == null){
            logger.error("No user found");
            return;
        }
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("userName", userBasicInfo.getFullName());
        bodyScopes.put("feedbackFormLink", fosEndPoint + "/studentfbform/" + feedbackForm.getId());
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("courseName", "Vedantu");
        try {
            communicationManager.sendMailForFeedbackForm(bodyScopes, subjectScopes, userBasicInfo.getEmail(), userBasicInfo.getFullName());
        } catch (Exception ex) {
            logger.error("Error while sending email for feedback: " + ex.getMessage());
        }
    }    

}
