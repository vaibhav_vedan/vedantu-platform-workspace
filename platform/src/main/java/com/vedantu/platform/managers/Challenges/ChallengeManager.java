package com.vedantu.platform.managers.Challenges;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.lms.cmds.pojo.CMDSTestBasicInfoUtils;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.enums.ChallengerGradeType.ChallengerGradeType;
import com.vedantu.platform.managers.lms.CMDSTestManager;
import com.vedantu.platform.pojo.Challenges.ChallengeLeaderboard;
import com.vedantu.platform.pojo.Challenges.ChallengePOJO;
import com.vedantu.platform.pojo.Challenges.Challenges;
import com.vedantu.platform.request.Challenges.ChallengeReq;
import com.vedantu.platform.response.Challenges.ChallengesRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Service
public class ChallengeManager {

	@Autowired
	private LogFactory logFactory;
	
	@Autowired
	private FosUtils fosUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ChallengeManager.class);

    private String lmsEndpoint = ConfigUtils.INSTANCE.getStringValue("platform.lms.direct");

	@Autowired
	private RedisDAO redisDAO;
	
    private final Gson gson = new Gson();

	public ChallengeReq setChallenges(ChallengeReq req) throws BadRequestException, InternalServerErrorException {
		req.verify();
		logger.info("Set Challenge Request : " + req);
		
		// Pre Populate User data in leaderboard before setting value in redis
		for(ChallengeLeaderboard challengeLeaderboard : req.getLeaderboards()) {
			String email = challengeLeaderboard.getEmail();
			UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfoFromEmail(email, false);
			
			if (userBasicInfo == null) {
				throw new BadRequestException(ErrorCode.USER_NOT_FOUND, "Please provide valid email");
			}
			
			String grade = userBasicInfo.getGrade();
			// Populating grade here
			challengeLeaderboard.setGrade(grade);
			// Populating gradeType here
			if(grade.equals("6") || grade.equals("7") || grade.equals("8")) {
				challengeLeaderboard.setChallengerGradeType(ChallengerGradeType.JUNIOR);
			}
			else if (grade.equals("9") || grade.equals("10")) {
				challengeLeaderboard.setChallengerGradeType(ChallengerGradeType.INTERMEDIATE);
			}
			else {
				challengeLeaderboard.setChallengerGradeType(ChallengerGradeType.SENIOR);
			}
		}
		
		// Prepopulate Test Information from test link
		for(ChallengePOJO challenge : req.getDayChallenges()) {
			String[] splitTestLink = challenge.getTestLink().split("/");
			String testId = splitTestLink[splitTestLink.length -1];
			
			String queryString = "?testId=" + testId ;
	    	String url = lmsEndpoint + "/cmds/test/getTestBasicDetails" + queryString;
	    	
	    	ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
	    	String output = response.getEntity(String.class);

	    	CMDSTestBasicInfoUtils cMDSTestBasicInfo = new CMDSTestBasicInfoUtils();
	    	
	    	if (!StringUtils.isEmpty(output)) {
	            try {
	                if (response.getStatus() != 200) {
	                    throw new Exception("getTestBasicDetails: getting CMDSTests' BasicInfo failed");
	                }
	                cMDSTestBasicInfo = gson.fromJson(output, CMDSTestBasicInfoUtils.class);

	            } catch (Exception ex) {
	                logger.info("Exception while parsing cMDSTestBasicInfo", ex);
	            }
	    	}
	    	
	    	if(cMDSTestBasicInfo == null) {
	    		throw new BadRequestException(ErrorCode.CMDS_CONTENT_NOT_FOUND, "Please provide valid test link");
	    	}
	    	
	    	challenge.setDuration(cMDSTestBasicInfo.getDuration());
	    	challenge.setNumberOfQuestions(cMDSTestBasicInfo.getQusCount());
	    	
	    	redisDAO.set("CHALLENGES_" + challenge.getChallengerGradeType(), gson.toJson(req));
		}
		
		return req;
	}

	public ChallengesRes fetchChallenges(ChallengerGradeType challengerGradeType) throws BadRequestException, JsonSyntaxException, InternalServerErrorException {

		logger.info("Fetching Challenges for grade level : " + challengerGradeType);
		if(challengerGradeType.equals(ChallengerGradeType.JUNIOR) && 
				redisDAO.get("CHALLENGES_JUNIOR") != null) {
			Challenges challenges = gson.fromJson(redisDAO.get("CHALLENGES_JUNIOR"), Challenges.class);
			logger.info("CHALLLENGES_JUNIOR : " + challenges.toString());
			
			return getResponseForChallenges(challenges);
		}
		else if(challengerGradeType.equals(ChallengerGradeType.INTERMEDIATE) && 
				redisDAO.get("CHALLENGES_INTERMEDIATE") != null) {
			Challenges challenges = gson.fromJson(redisDAO.get("CHALLENGES_INTERMEDIATE"), Challenges.class);
			logger.info("CHALLLENGES_INTERMEDIATE : " + challenges.toString());
			
			return getResponseForChallenges(challenges);
		}
		else if(challengerGradeType.equals(ChallengerGradeType.SENIOR) && 
				redisDAO.get("CHALLENGES_SENIOR") != null) {
			Challenges challenges = gson.fromJson(redisDAO.get("CHALLENGES_SENIOR"), Challenges.class);
			logger.info("CHALLLENGES_SENIOR : " + challenges.toString());
			
			return getResponseForChallenges(challenges);
		}
		else return null;
	}
	
	private ChallengesRes getResponseForChallenges(Challenges challenges) {
		
		List<ChallengePOJO> todaysChallenge = new ArrayList();
		List<ChallengePOJO> tomorrowsChallenge = new ArrayList();
		List<ChallengePOJO> yesterdaysChallenge = new ArrayList();
		
		for (ChallengePOJO dayChallenge : challenges.getDayChallenges()) {
			Date challengeDate = new Date(dayChallenge.getStartTime());
			int currentDate = new DateTime(challengeDate).getDayOfMonth();
			
			if(currentDate == DateTime.now().getDayOfMonth()) {
				todaysChallenge.add(dayChallenge);
			}
			else if (currentDate == DateTime.now().plusDays(1).getDayOfMonth()) {
				tomorrowsChallenge.add(dayChallenge);
			}
			else if (currentDate == DateTime.now().minusDays(1).getDayOfMonth()) {
				yesterdaysChallenge.add(dayChallenge);
			}
		}
		
		return new ChallengesRes(todaysChallenge, yesterdaysChallenge, tomorrowsChallenge, 
				challenges.getLeaderboards());
	}

}
