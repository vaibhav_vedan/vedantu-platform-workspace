/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.user;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.*;
import com.vedantu.User.Pojo.ELTeachersProficiencies;
import com.vedantu.User.AbstractInfo;
import com.vedantu.User.AbstractInfoAdapter;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.Gender;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UTMParams;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserEvents;
import com.vedantu.User.UserInfo;
import com.vedantu.User.UserUtils;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.User.enums.LoginTokenContext;
import com.vedantu.User.enums.TeacherCategory;
import com.vedantu.User.request.*;
import com.vedantu.User.response.*;
import com.vedantu.User.request.AcceptTncReq;
import com.vedantu.User.request.AddOrUpdatePhoneNumberReq;
import com.vedantu.User.request.AddUserAddressReq;
import com.vedantu.User.request.AddUserSystemIntroReq;
import com.vedantu.User.request.ChangePasswordReq;
import com.vedantu.User.request.CreateUserAuthenticationTokenReq;
import com.vedantu.User.request.EditContactNumberReq;
import com.vedantu.User.request.EditProfileReq;
import com.vedantu.User.request.EditUserDetailsReq;
import com.vedantu.User.request.ExistingUserISLRegReq;
import com.vedantu.User.request.ExtensionNumberReq;
import com.vedantu.User.request.ForgotPasswordReq;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.request.GetActiveTutorsReq;
import com.vedantu.User.request.GetUserInfosByEmailReq;
import com.vedantu.User.request.GetUserSystemIntroReq;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.User.request.ReSendContactNumberVerificationCodeReq;
import com.vedantu.User.request.SetBlockStatusForUserReq;
import com.vedantu.User.request.SetProfilePicReq;
import com.vedantu.User.request.SignInReq;
import com.vedantu.User.request.SignUpForISLReq;
import com.vedantu.User.request.SignUpReq;
import com.vedantu.User.request.SocialSource;
import com.vedantu.User.request.SocialUserInfo;
import com.vedantu.User.request.StudentTeacherExotelCallConnectReq;
import com.vedantu.User.request.VerifyContactNumberReq;
import com.vedantu.User.response.ConnectSessionCallRes;
import com.vedantu.User.response.CreateUserAuthenticationTokenRes;
import com.vedantu.User.response.CreateUserResponse;
import com.vedantu.User.response.EditContactNumberRes;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.User.response.GetActiveTutorsRes;
import com.vedantu.User.response.GetUserProfileRes;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.User.response.IsRegisteredForISLRes;
import com.vedantu.User.response.PhoneNumberRes;
import com.vedantu.User.response.ProcessVerifyContactNumberResponse;
import com.vedantu.User.response.ReSendContactNumberVerificationCodeRes;
import com.vedantu.User.response.SetProfilePicRes;
import com.vedantu.User.response.SignUpForISLRes;
import com.vedantu.User.response.SignUpRes;
import com.vedantu.User.response.StatusCounterResponse;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.User.response.UserLeadInfo;
import com.vedantu.User.response.UserSystemIntroRes;
import com.vedantu.User.response.VerifyContactNumberRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojos.MessageUserLeader;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.notification.requests.MessageUserReplyReq;
import com.vedantu.notification.responses.GetMessageLeadersRes;
import com.vedantu.onetofew.enums.SessionLabel;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.board.BoardDAO;
import com.vedantu.platform.dao.board.TeacherBoardMappingDAO;
import com.vedantu.platform.dao.filemgmt.FileDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.enums.dinero.CouponType;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.ESManager;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.MiscManager;
import com.vedantu.platform.managers.NonStudentRoleCreatorManager;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.managers.SessionHourCountManager;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.click2call.ExotelPhoneCallManager;
import com.vedantu.platform.managers.dinero.AccountManager;
import com.vedantu.platform.managers.dinero.CouponManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.notification.MessageTriggerManager;
import com.vedantu.platform.managers.notification.MessageUserManager;
import com.vedantu.platform.managers.notification.NotificationManager;
import com.vedantu.platform.managers.notification.SMSManager;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.platform.mongodbentities.click2call.PhoneCallMetadata;
import com.vedantu.platform.mongodbentities.filemgmt.File;
import com.vedantu.platform.mongodbentities.review.CumilativeRating;
import com.vedantu.platform.pojo.ISLStudentData;
import com.vedantu.platform.pojo.scheduling.ESSearchResponse;
import com.vedantu.platform.pojo.scheduling.EsTeacherInfo;
import com.vedantu.platform.pojo.user.AppSocialLoginReq;
import com.vedantu.platform.pojo.user.GetMessageUsersReq;
import com.vedantu.platform.pojo.user.GetMessageUsersRes;
import com.vedantu.platform.pojo.user.MessageUserInfo;
import com.vedantu.platform.pojo.user.MessageUserReq;
import com.vedantu.platform.pojo.user.MessageUserServletRes;
import com.vedantu.platform.pojo.user.SendUserEmailReq;
import com.vedantu.platform.pojo.user.ShowInterestCourseListingReq;
import com.vedantu.platform.pojo.user.ShowInterestReq;
import com.vedantu.platform.request.CreateUserDetailsFromReq;
import com.vedantu.platform.request.dinero.ProcessCouponReq;
import com.vedantu.platform.response.AppSocialLoginRes;
import com.vedantu.platform.response.dinero.TransferFromFreebiesAccountRes;
import com.vedantu.platform.utils.PojoUtils;
import com.vedantu.session.pojo.EntityRatingInfo;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.*;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.CustomValidator;
import com.vedantu.util.EntityType;
import com.vedantu.util.FosUtils;
import com.vedantu.util.IPUtil;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebCommunicator;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.redis.RateLimitingRedisDao;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.security.LoginType;
import com.vedantu.util.security.UserRedisManager;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;

/*
 * 
 *
 * @author somil
 */
@Service
public class UserManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private CommunicationManager emailManager;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private ReferralManager referralManager;

    @Autowired
    private FileDAO fileDAO;

    @Autowired
    private UserUtils userUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private MessageUserManager messageUserManager;

    @Autowired
    private MessageTriggerManager messageTriggerManager;

    @Autowired
    private MiscManager miscManager;

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private TeacherBoardMappingDAO teacherBoardMappingDAO;

    @Autowired
    private UserRedisManager userRedisManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private ESManager eSManager;

    @Autowired
    @Resource(name = "googleManager")
    private ISocialManager googleManager;

    @Autowired
    @Resource(name = "facebookManager")
    private ISocialManager facebookManager;

    @Autowired
    private SessionHourCountManager sessionHourCountManager;

    @Autowired
    private AwsS3Manager s3Manager;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private CumilativeRatingDao cumilativeRatingDao;

    @Autowired
    private ExotelPhoneCallManager exotelPhoneCallManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private VerificationTokenManager verificationTokenManager;

    @Autowired
    private NotificationManager notificationManager;

    @Autowired
    private PojoUtils pojoUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private NonStudentRoleCreatorManager nonStudentRoleCreatorManager;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private AwsSNSManager awsSnsManager;

    public static final String STATUS_SOCIAL_LOGIN = "/social/statusSocialLogin.jsp";

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");

    private final String apigurusUrl = ConfigUtils.INSTANCE.getStringValue("apigurus.url");

    private final Gson gson = new Gson();

    private static List<String> allowedCountriesList = new ArrayList(Arrays.asList("India", "Saudi Arabia", "singapore", "United Arab Emirates", "Kuwait", "Oman", "Qatar", "Bahrain",  "Nigeria", "New Zealand", "Kazakhstan"));

    @Autowired
    private RateLimitingRedisDao rateLimitRedisDAO;


    public UserManager() {

    }

    // Written only for experimental purposes for Signup Funnel improvement
    public SignUpRes signUpOrLoginApp(SignUpReq signUpReq, HttpServletRequest request,
            HttpServletResponse response, String authenticatedThrough) throws IOException, URISyntaxException, BadRequestException, VException {
        if (StringUtils.isNotEmpty(signUpReq.getContactNumber())) {
            signUpReq.setEmail(signUpReq.getContactNumber() + "-app@vedantu.com");
            signUpReq.setFirstName("Vedan");
            signUpReq.setPassword(signUpReq.getContactNumber());
            return signUpUser(signUpReq, request, response, authenticatedThrough);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Contact Number not present");
        }
    }

    public SignUpRes signUpUser(SignUpReq signUpReq, HttpServletRequest request,
            HttpServletResponse response, String authenticatedThrough)
            throws VException, IOException, URISyntaxException {//authenticatedThrough will be passed internally after authentication from social sources
        logger.info("Request received to signup User :" + signUpReq);
        signUpReq.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();

        if ("GOOGLE".equalsIgnoreCase(authenticatedThrough)
                && signUpReq.getEmail().toLowerCase().endsWith("@vedantu.com")) {
            signUpReq.setRole(Role.STUDENT_CARE);
        } else {
            //doing check for ADMIN if a non student role user is created
            if (signUpReq.getRole() != null && !Role.STUDENT.equals(signUpReq.getRole())) {
                boolean allowedToCreateNonAdminRole = false;
                List<String> emailsAllowedToCreateNonStudentRoleUser
                        = Arrays.asList(ConfigUtils.INSTANCE.getStringValue("allowed.to.create.non.admin.user").split(","));
                if (sessionData == null ||
                        ((StringUtils.isEmpty(sessionData.getEmail()) || !emailsAllowedToCreateNonStudentRoleUser.contains(sessionData.getEmail()))
                                && (sessionData.getUserId() == null || sessionData.getRole() == null || !nonStudentRoleCreatorManager.isAllowedToCreateNonStudentRole(sessionData.getUserId(), signUpReq.getRole())))
                ) {
                    throw new ForbiddenException(ErrorCode.NON_STUDENT_USER_CREATED_BY_NON_ADMIN,
                            "You are not allowed to create user with non student roles");
                }
            }
        }
        SocialSource source = null;
        boolean redirectAfterSignup = false;
        if (signUpReq.getRedirectAfterSignup() != null && signUpReq.getRedirectAfterSignup()) {
            redirectAfterSignup = true;
        }

        // TODO: is auth token being used?
        source = SocialSource.valueOfKey(signUpReq.getSocialSource());
        // if ((source == SocialSource.FACEBOOK || source ==
        // SocialSource.GOOGLE)
        // && (sessionData == null || sessionData.getAuthToken() == null
        // || !sessionData.getAuthToken().equals(signUpReq.getAuthToken()))) {
        //
        // throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid
        // Auth Token");
        // }
        if (sessionData != null) {
            signUpReq.setPicUrl(sessionData.getProfilePicUrl());
        }
        // this parameter is used for creating temp pasword if the user is
        // not providing a passowrd
        if (SocialSource.UNKNOWN.equals(source) && StringUtils.isEmpty(signUpReq.getPassword())) {
            signUpReq.setPassword(UUID.randomUUID().toString());
        }

        if (request != null && null == signUpReq.getSignUpURL()) {
            String refererUrl = request.getHeader("referer");
            if (StringUtils.isEmpty(refererUrl)) {
                refererUrl = request.getHeader("Referer");
                if (StringUtils.isEmpty(refererUrl) && !StringUtils.isEmpty(request.getParameter("refererUrl"))) {
                    refererUrl = request.getParameter("refererUrl");
                }
            }
            signUpReq.setSignUpURL(refererUrl);
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/createUser", HttpMethod.POST,
                new Gson().toJson(signUpReq), true);
        // TODO handle error correctly
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        } catch (ConflictException ex) {
            if (ErrorCode.USER_ALREADY_EXISTS.equals(ex.getErrorCode())
                    && Role.STUDENT.equals(signUpReq.getRole()) && (FeatureSource.SEM_PAGES.equals(signUpReq.getSignUpFeature())
                    || FeatureSource.SEO_PAGES.equals(signUpReq.getSignUpFeature()))) {
                Map<String, String> lsParams = new HashMap<>();
                lsParams.put("interestType", signUpReq.getSignUpFeature().name());
                User user = getUserByEmail(signUpReq.getEmail());
                if (user == null) {
                    List<User> users = getUsersByContactNumber(signUpReq.getContactNumber());
                    if (ArrayUtils.isNotEmpty(users)) {
                        user = users.get(0);
                    }
                }
                if (user != null) {
                    LeadSquaredRequest leadReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                            LeadSquaredDataType.LEAD_ACTIVITY_SHOW_INTEREST, user, lsParams,
                            null);
                    leadSquaredManager.executeAsyncTask(leadReq);
                }
            }
            throw ex;
        }
        String jsonString = resp.getEntity(String.class);
        CreateUserResponse createUserResponse = new Gson().fromJson(jsonString, CreateUserResponse.class);
        logger.info("Response from createUser " + jsonString);
        User user = createUserResponse.getUser();
        logger.info("Response user " + user);
        SignUpRes res = new SignUpRes(user);
        String mobileTokenCode = createUserResponse.getMobileTokenCode();
        String emailTokenCode = createUserResponse.getEmailTokenCode();

        // creating account
        try {
            accountManager.createUserAccount(user);
        } catch (VException e1) {
            logger.error("Error in creating account while signUp", e1);
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("user", user);
        payload.put("mobileTokenCode", mobileTokenCode);
        payload.put("emailTokenCode", emailTokenCode);
        payload.put("callingUserId", signUpReq.getCallingUserId());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SIGNUP_TASKS, payload);
        asyncTaskFactory.executeTask(params);

        if (!SocialSource.UNKNOWN.equals(source) && request != null && response != null) {
            sessionUtils.setCookieAndHeaders(request, response, user);
        }

        if (StringUtils.isNotEmpty(signUpReq.getCouponCode())) {
            try {
                ProcessCouponReq req = new ProcessCouponReq(signUpReq.getCouponCode(), user.getId(), CouponType.CREDIT);
                Object appliedCoupon = couponManager.processCoupon(req);
                res.setAppliedCoupon(appliedCoupon != null);
            } catch (VException ex) {
                if (ex instanceof InternalServerErrorException) {
                    logger.error(ex);
                }
            }
        }
        // try {
        // AcceptTncReq acceptTncReq = new AcceptTncReq();
        // acceptTncReq.setUserId(signUpRes.getUserId());
        // acceptTncReq.setTncVersion(reqPojo.getTncVersion());
        // acceptTnc(acceptTncReq);
        // } catch (Throwable e) {
        // log("accept tnc signup : userId:" + signUpRes.getUserId()
        // + "; tnc version : " + reqPojo.getTncVersion(), e);
        // }

        if (redirectAfterSignup && request != null && response != null) {
            String redirectUrl = ConfigUtils.INSTANCE.getStringValue("domain.link");
            String urlRedirect = getRedirectUrl(request);
            if (StringUtils.isNotEmpty(urlRedirect)) {
                redirectUrl = urlRedirect;
            }
            logger.info("redirecting to " + redirectUrl + " after signup");
            response.sendRedirect(redirectUrl);
        }

//        if (Role.TEACHER.equals(user.getRole())) {
//            evictELTeachersKey();
//        }


        return res;
    }

    @Deprecated
    private void evictELTeachersKey() {
        String ELTeachersSuperCoderKey = "EARLY_LEARNING_TEACHERS_SUPER_CODER";
        String ELTeachersSuperReaderKey = "EARLY_LEARNING_TEACHERS_SUPER_READER";
        List<String> keys = new ArrayList<>();
        EnumSet.allOf(TeacherCategory.ProficiencyType.class).forEach(proficiencyType -> {
            String superCoderKey = ELTeachersProficiencies.getELRedisKey(SessionLabel.SUPER_CODER, proficiencyType);
            String superReaderKey = ELTeachersProficiencies.getELRedisKey(SessionLabel.SUPER_READER, proficiencyType);
            keys.add(superCoderKey);
            keys.add(superReaderKey);
        });
        keys.add(ELTeachersSuperCoderKey);
        keys.add(ELTeachersSuperReaderKey);

        // set and orchestrated in user as well
        awsSnsManager.triggerSNS(SNSTopic.SCHEDULING_REDIS_OPS, SNSSubject.EVICT_EL_TEACHERS.name(), gson.toJson(keys));
    }

    public void sendContactNumberVerificationSMS(UserInfo userInfo, String number, String phoneCode,
            String mobileTokenCode, String utm_campaign) throws VException {
        smsManager.sendSMS(CommunicationType.PHONE_VERIFICATION, userInfo, number, phoneCode, mobileTokenCode,
                utm_campaign);
    }

    public PlatformBasicResponse forgotPassword(ForgotPasswordReq req)
            throws BadRequestException, VException, UnsupportedEncodingException {
        req.verify();
        String email = req.getEmailId();
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No valid email found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/forgotPassword", HttpMethod.POST,
                new Gson().toJson(req), true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CreateUserResponse response = new Gson().fromJson(jsonString, CreateUserResponse.class);
        String tokenCode = response.getEmailTokenCode();
        Map<String, Object> payload = new HashMap<>();
        payload.put("user", new UserBasicInfo(response.getUser(), true));
        payload.put("type", CommunicationType.FORGOT_PASSWORD);
        payload.put("tokenCode", tokenCode);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.VERIFICATION_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        // emailManager.sendVerificationTokenEmail(response.getUser(),
        // CommunicationType.FORGOT_PASSWORD, tokenCode);
        logger.info("Response from forgotPassword " + jsonString);
        return new PlatformBasicResponse();
    }

    public String changePassword(ChangePasswordReq request) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/changePassword", HttpMethod.POST,
                new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        if (request.isReloginRequired()) {
            userRedisManager.addPasswordChangedAt(request.getUserId());
        }
        return resp.getEntity(String.class);
    }

    public HttpSessionData authenticateUserByOTP(String requestBody, HttpServletRequest request,
            HttpServletResponse response) throws VException, IOException, URISyntaxException {
        try {
            String ipAddress = request.getHeader("X_FORWARDED_FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            if (ipAddress != null) {
                logger.info("ip address of the user trying to login:" + ipAddress);
            }
        } catch (Exception e) {
            logger.warn(e);
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/authenticateUserByOTP", HttpMethod.POST,
                requestBody, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String userJsonStr = resp.getEntity(String.class);
        User user = gson.fromJson(userJsonStr, User.class);
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user);
        sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());
        return sessionData;
    }

    public HttpSessionData authenticateUser(SignInReq req, HttpServletRequest request,
            HttpServletResponse response) throws VException, IOException, URISyntaxException {

        List<String> allowedCountriesListFromRedis;
        String redisKey = "ALLOWED_COUNTRIES_LIST";
        try{
            String redisCountriesString = rateLimitRedisDAO.get(redisKey);
            String[] redisCountriesArr = redisCountriesString.split(",");
            allowedCountriesListFromRedis = new ArrayList(Arrays.asList(redisCountriesArr));
            allowedCountriesList = allowedCountriesListFromRedis;
            for(String country : allowedCountriesList){
                logger.info("Available countries :" +country);
            }
        }catch (Exception e){
            logger.error("Error in getting key from redis " + redisKey);
        }
        String ipAddress = req.getIpAddress();
        logger.info("ip address of the user trying to login:" + ipAddress);

        int blockTime = 864000;
        String key = "RL:" + ipAddress + ":" + "/platform/user/login";

        try{
            rateLimitRedisDAO.setex("BL:" + key, "", blockTime);
            logger.warn("USER BLOCKED: " + key + "Requests Blocked from: " + ipAddress);
        }catch(Exception e){
            logger.info("error in blocking user");
        }

        throw new UnauthorizedException(ErrorCode.PASSWORD_DOES_NOT_MATCH, "Incorrect credentials.");


//        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/authenticateUser", HttpMethod.POST,
//                gson.toJson(req), true);
//        try {
//            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        }catch(VException ex){
//            if(ErrorCode.PASSWORD_DOES_NOT_MATCH.equals(ex.getErrorCode())
//                    || ErrorCode.EMAIL_NOT_REGISTERED.equals(ex.getErrorCode())
//                        || ErrorCode.PASSWORD_DOES_NOT_MATCH_LOGIN_DATA.equals(ex.getErrorCode())){
//                if(StringUtils.isNotEmpty(ipAddress)) {
//                    LocationInfo locationFromIp = ipUtil.getLocationFromIp(ipAddress);
//                    String countryFromIp = null;
//                    if (locationFromIp != null) {
//                        countryFromIp = locationFromIp.getCountry();
//                    }
//                    boolean blockIp = true;
//                    for (String country : allowedCountriesList) {
//                        if (StringUtils.isNotEmpty(countryFromIp) && country.equalsIgnoreCase(countryFromIp)) {
//                            blockIp = false;
//                            break;
//                        }
//                    }
//
//                    if (StringUtils.isEmpty(countryFromIp)) {
//                        blockIp = false;
//                    }
//
//
//                    if (null != locationFromIp && "ANONYMOUS_PROXY_OR_VPN".equalsIgnoreCase(locationFromIp.getQueryStatusCode())) {
//                        blockIp = true;
//                    }
//
//                    if (blockIp) {
//                        String key = "RL:" + ipAddress + ":" + "/platform/user/login";
//                        int blockTime = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("rl.blockTime"));
//                        blockTime = 864000;
//                        try {
//                            String email = req.getEmail();
//                            User user = null;
//                            if(CustomValidator.validEmail(email)){
//                                user = getUserByEmail(email);
//                            }
//                            // 1 jan 2018
//                            if (user == null || user.getCreationTime() < 1514745000000L) {
//                                rateLimitRedisDAO.setex("BL:" + key, "", blockTime);
//                                logger.warn("USER BLOCKED: " + key + "Requests Blocked from: " + countryFromIp);
//                            }
//                        }catch (Exception e){
//                            logger.error("Error in adding value to set: " + " to key: " + key + ",  exception: " + e.getMessage());
//                        }
//                    }
//                }
//                if(ErrorCode.PASSWORD_DOES_NOT_MATCH_LOGIN_DATA.equals(ex.getErrorCode())){
//                    logger.warn("PASSWORD_DOES_NOT_MATCH_LOGIN_DATA " + req.getEmail());
//                }
//            }
//            throw new UnauthorizedException(ErrorCode.PASSWORD_DOES_NOT_MATCH, "Incorrect credentials.");
//        }
//
//
//        String userJsonStr = resp.getEntity(String.class);
//        User user = gson.fromJson(userJsonStr, User.class);
//        logger.info("authenticateUser - platform - user data received for onboarding - {}",user.isUserInProcessOfOnboarding());
//        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user);
//        sessionData.setPasswordAutogenerated(user.getPasswordAutogenerated());
//        logger.info("authenticateUser - platform - userAllowedToTakeOnboarding - {}",sessionData.isUserInProcessOfOnboarding());
//        return sessionData;
    }

    public HttpSessionData authenticateUserByToken(String requestBody, HttpServletRequest request,
            HttpServletResponse response) throws VException, IOException, URISyntaxException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/authenticateUserByToken", HttpMethod.POST,
                requestBody, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String userJsonStr = resp.getEntity(String.class);
        User user = gson.fromJson(userJsonStr, User.class);
        HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user, LoginType.TOKEN_BASED);
        return sessionData;
    }

    public CreateUserAuthenticationTokenRes createUserAuthenticationToken(Long userId, LoginTokenContext contextType,
            String contextId, Long callingUserId) throws VException, IOException, URISyntaxException {
        CreateUserAuthenticationTokenReq createUserAuthenticationTokenReq = new CreateUserAuthenticationTokenReq(contextType, contextId, 90l, userId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/createUserAuthenticationToken", HttpMethod.POST,
                new Gson().toJson(createUserAuthenticationTokenReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String createUserAuthenticationTokenResStr = resp.getEntity(String.class);
        CreateUserAuthenticationTokenRes createUserAuthenticationTokenRes = gson.fromJson(createUserAuthenticationTokenResStr, CreateUserAuthenticationTokenRes.class);
        return createUserAuthenticationTokenRes;
    }

    public CreateUserAuthenticationTokenRes createUserAuthenticationTokenForTest(Long userId, LoginTokenContext contextType,
            String testLink, Long callingUserId) throws VException, IOException, URISyntaxException {
        CreateUserAuthenticationTokenReq createUserAuthenticationTokenReq = new CreateUserAuthenticationTokenReq(contextType, null, 180l, userId);
        createUserAuthenticationTokenReq.setTestLink(testLink);
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/createUserAuthenticationToken", HttpMethod.POST,
                new Gson().toJson(createUserAuthenticationTokenReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String createUserAuthenticationTokenResStr = resp.getEntity(String.class);
        CreateUserAuthenticationTokenRes createUserAuthenticationTokenRes = gson.fromJson(createUserAuthenticationTokenResStr, CreateUserAuthenticationTokenRes.class);
        return createUserAuthenticationTokenRes;
    }

    public GetUserProfileRes getUserProfile(Long userId, Boolean exposeEmail) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No userId Found");
        }
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserProfile?userId=" + userId,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson _gson = gsonBuilder.create();
        GetUserProfileRes response = _gson.fromJson(jsonString, GetUserProfileRes.class);
        if (response != null) {
            CumilativeRating rating = cumilativeRatingDao.getCumulativeRating(response.getUserId().toString(),
                    EntityType.USER);
            if (rating != null) {
                com.vedantu.session.pojo.CumilativeRating ratingPojo = pojoUtils.convertToRatingPojo(rating);
                response.setRatingInfo(new EntityRatingInfo(ratingPojo));
            }
            boardManager.fillBoardInfo(response);
            if (!exposeEmail) {
                response.setPhones(null);
                response.setContactNumber(null);
                response.setPhoneCode(null);
                response.setEmail(null);
                response.setSocialInfo(null);
                response.setFirstName(null);
                response.setLastName(null);
                response.setFullName(null);

                if (response.getStudentInfo() != null) {
                    response.getStudentInfo().setParentInfos(null);
                }
                if (response.getInfo() != null && response.getInfo() instanceof StudentInfo) {
                    ((StudentInfo) response.getInfo()).setParentInfos(null);
                }
            }
            if (!Role.TEACHER.equals(response.getRole())) {
                //when requesting student/admin info
                if (sessionData == null //if not logged in do not show pic at all
                        || (!Role.ADMIN.equals(sessionData.getRole())
                        && !Role.TEACHER.equals(sessionData.getRole())
                        && !userId.equals(sessionData.getUserId()))) {//if logged in, only teacher and admin or the same user can see student/admin pic
                    response.setProfilePicUrl(null);
                }
            }
        }
        return response;
    }

    public PlatformBasicResponse acceptTnc(AcceptTncReq request, HttpServletRequest httpRequest,
            HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/acceptTnc", HttpMethod.POST,
                new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        PlatformBasicResponse response = gson.fromJson(resp.getEntity(String.class), PlatformBasicResponse.class);
        if (response.isSuccess()) {
            HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            sessionData.setTncVersion(request.getTncVersion());
            sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
        }
        return response;
    }

    public EditProfileRes editProfile(EditProfileReq request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/editProfile", HttpMethod.POST,
                new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        EditProfileRes response = gson.fromJson(jsonString, EditProfileRes.class);
        if (Role.STUDENT.equals(response.getRole()) && request.getStudentInfo() != null) {
            HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            boolean setCookie = false;
            if (request.getStudentInfo().getGrade() != null) {
                sessionData.setGrade(request.getStudentInfo().getGrade());
                setCookie = true;
            }

            if (request.getStudentInfo().getBoard() != null) {
                sessionData.setBoard(request.getStudentInfo().getBoard());
                setCookie = true;
            }

            if (ArrayUtils.isNotEmpty(request.getStudentInfo().getExamTargets())) {
                sessionData.setExamTargets(request.getStudentInfo().getExamTargets());
                setCookie = true;
            }

            if (StringUtils.isNotEmpty(request.getFirstName())) {
                sessionData.setFirstName(request.getFirstName());
                setCookie = true;
            }

//            if (StringUtils.isNotEmpty((request.getLastName()))) {
//                sessionData.setLastName(request.getLastName());
//                setCookie = true;
//            }
//            else {
//                    sessionData.setLastName(request.getLastName());
//                    setCookie = true;
//            }

            if(null!=request.getLastName())
            {
               // logger.info("****** inside edit profile last name");
                sessionData.setLastName(request.getLastName());
                setCookie = true;
            }

            if (setCookie) {
                sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
            }
        }

        if (Role.STUDENT.equals(response.getRole()) && request.getStudentInfo() != null && request.getStudentInfo().getBoard() != null) {
            HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            sessionData.setBoard(request.getStudentInfo().getBoard());
            sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
        }

        if (request.getStudentInfo() != null && ArrayUtils.isNotEmpty(request.getStudentInfo().getExamTargets())) {
            HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            sessionData.setExamTargets(request.getStudentInfo().getExamTargets());
            sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
        }

        boardManager.fillBoardInfo(response);
        if (Role.TEACHER.equals(response.getRole())) {
            updateTeacherBoardMapping(response.getUserId(), (TeacherInfo) response.getInfo(),
                    request.getCallingUserId());
            // evictELTeachersKey();
            HashMap<String, HashMap<Object, Object>> diffUserData = null;
            try {
                diffUserData = boardManager.diffEditTeacher(response.getPrevTeacherInfo(), request);
                if (diffUserData != null) {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("editProfileRes", response);
                    payload.put("diffUserData", diffUserData);
                    payload.put("callerUser", httpSessionUtils.getCurrentSessionData());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TEACHER_EDIT_PROFILE_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                    // emailManager.sendTeacherEditProfileToAdmin(response,
                    // diffUserData);
                }
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        response.setPrevTeacherInfo(null);
        return response;
    }

    public EditProfileRes editStudentInfo(EditProfileReq request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws VException, URISyntaxException, IOException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/editStudentInfo", HttpMethod.POST,
                new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();

        if (request.getStudentInfo() != null) {
            HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            boolean setCookie = false;
            if (request.getStudentInfo().getGrade() != null) {
                sessionData.setGrade(request.getStudentInfo().getGrade());
                setCookie = true;
            }

            if (request.getStudentInfo().getBoard() != null) {
                sessionData.setBoard(request.getStudentInfo().getBoard());
                setCookie = true;
            }

            if (ArrayUtils.isNotEmpty(request.getStudentInfo().getExamTargets())) {
                sessionData.setExamTargets(request.getStudentInfo().getExamTargets());
                setCookie = true;
            }

            if (setCookie) {
                sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
            }
        }

        EditProfileRes response = gson.fromJson(jsonString, EditProfileRes.class);
        boardManager.fillBoardInfo(response);
        if (Role.TEACHER.equals(response.getRole())) {
            updateTeacherBoardMapping(response.getUserId(), (TeacherInfo) response.getInfo(),
                    request.getCallingUserId());
            HashMap<String, HashMap<Object, Object>> diffUserData = null;
            try {
                diffUserData = boardManager.diffEditTeacher(response.getPrevTeacherInfo(), request);
                if (diffUserData != null) {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("editProfileRes", response);
                    payload.put("diffUserData", diffUserData);
                    payload.put("callerUser", httpSessionUtils.getCurrentSessionData());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TEACHER_EDIT_PROFILE_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                    // emailManager.sendTeacherEditProfileToAdmin(response,
                    // diffUserData);
                }
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        response.setPrevTeacherInfo(null);
        return response;
    }

    public GetUsersRes getUsers(GetUsersReq request) throws VException {
        request.verify();
        logger.info("GetUsersReq : " + request);
        logger.info("GETUSERS query: " + request.getQuery());
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsers", HttpMethod.POST,
                new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        GetUsersRes response = gson.fromJson(jsonString, GetUsersRes.class);
        if (response.getUsers() != null) {
            for (UserInfo userInfo : response.getUsers()) {
                // boardManager.fillBoardInfo(userInfo);
                if (Role.TEACHER.equals(userInfo.getRole())) {
                    // TODO: optimize this, leaving for now as used in only
                    // tools now
                    CumilativeRating rating = cumilativeRatingDao.getCumulativeRating(userInfo.getUserId().toString(),
                            EntityType.USER);
                    if (rating != null) {
                        com.vedantu.session.pojo.CumilativeRating ratingPojo = pojoUtils.convertToRatingPojo(rating);
                        userInfo.setRatingInfo(new EntityRatingInfo(ratingPojo));
                    }
                }
                logger.info("Platform user teacherpooltype: " + userInfo.getTeacherPoolType());
            }
        }

        return response;
    }

    public EditContactNumberRes editContactNumber(EditContactNumberReq request, HttpServletRequest httpRequest,
            HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        request.verify();
        if (request.getUserId() == null) {
            request.setUserId(sessionUtils.getCallingUserId());
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/editContactNumber", HttpMethod.POST,
                new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        EditContactNumberRes response = gson.fromJson(jsonString, EditContactNumberRes.class);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        sessionData.setIsContactNumberDND(response.isDND());
        // sessionData.setIsContactNumberWhitelisted(response.isWhitelisted());
        sessionData.setIsContactNumberVerified(response.isIsVerified());
        sessionData.setContactNumber(response.getContactNumber());
        sessionData.setPhoneCode(response.getPhoneCode());
        sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);

        if (!StringUtils.isEmpty(response.getMobileTokenCode())) {
            logger.info("EDIT-CONTACT-NUMBER:SEND-SMS");
            logger.info(response);
            logger.info(response.toString());
            sendContactNumberVerificationSMS(response.getUserInfo(), response.getContactNumber(),
                    response.getPhoneCode(), response.getMobileTokenCode(), response.getUtm_campaign());
        }

        // Must do it
        response.setUtm_campaign(null);
        response.setMobileTokenCode(null);
        response.setUserInfo(null);
        return response;
    }

    public ReSendContactNumberVerificationCodeRes reSendContactNumberVerificationCode(
            ReSendContactNumberVerificationCodeReq request) throws VException {
        request.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/reSendContactNumberVerificationCode",
                HttpMethod.POST, new Gson().toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        ReSendContactNumberVerificationCodeRes response = gson.fromJson(jsonString,
                ReSendContactNumberVerificationCodeRes.class);
        UserInfo userInfo = response.getUserInfo();

        boolean isPrimaryNumber = StringUtils.isEmpty(request.getNumber());
        String number = isPrimaryNumber ? userInfo.getTempContactNumber() : request.getNumber();
        String phoneCode = isPrimaryNumber ? userInfo.getTempPhoneCode() : request.getPhoneCode();

        logger.info("RESPONSE-MOBILETOKENCODE: " + response.getMobileTokenCode());
        logger.info("Number: " + number);

        if (!StringUtils.isEmpty(response.getMobileTokenCode()) && StringUtils.isNotEmpty(number)) {
            sendContactNumberVerificationSMS(userInfo, number, phoneCode, response.getMobileTokenCode(),
                    response.getUtm_campaign());
        }
        // Must do it
        response.setUtm_campaign(null);
        response.setMobileTokenCode(null);
        response.setUserInfo(null);

        return response;
    }

    public PhoneNumberRes updatePhoneNumber(AddOrUpdatePhoneNumberReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/updatePhoneNumber", HttpMethod.POST,
                new Gson().toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        PhoneNumberRes response = gson.fromJson(jsonString, PhoneNumberRes.class);

        if (!StringUtils.isEmpty(response.getMobileTokenCode())) {
            sendContactNumberVerificationSMS(response.getUserInfo(), response.getNumber(), response.getPhoneCode(),
                    response.getMobileTokenCode(), response.getUtm_campaign());
        }

        // Must do it
        response.setUtm_campaign(null);
        response.setMobileTokenCode(null);
        response.setUserInfo(null);
        return response;
    }

    public PhoneNumberRes updateAndMarkActivePhoneNumber(AddOrUpdatePhoneNumberReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/updateAndMarkActivePhoneNumber",
                HttpMethod.POST, new Gson().toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        PhoneNumberRes response = gson.fromJson(jsonString, PhoneNumberRes.class);

        /*
		 * if (!StringUtils.isEmpty(response.getMobileTokenCode())) {
		 * sendContactNumberVerificationSMS(response.getUserInfo(),
		 * response.getNumber(), response.getPhoneCode(),
		 * response.getMobileTokenCode()); }
         */
        // Must do it
        response.setMobileTokenCode(null);
        response.setUserInfo(null);
        return response;
    }

    public VerifyContactNumberRes verifyContactNumber(VerifyContactNumberReq req, HttpServletRequest request,
            HttpServletResponse httpResponse) throws VException, IOException, URISyntaxException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/verifyContactNumber", HttpMethod.POST,
                new Gson().toJson(req), true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        ProcessVerifyContactNumberResponse response = new Gson().fromJson(jsonString,
                ProcessVerifyContactNumberResponse.class);

        VerifyContactNumberRes res = new VerifyContactNumberRes();
        res.setNumber(response.getNumber());
        res.setDnd(response.isDnd());
        res.setVerified(response.isVerified());
        res.setWhitelisted(response.isWhitelisted());
        res.setUser(response.getUserDetails());
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();

//      if (StringUtils.isEmpty(req.getNumber())) {
        if (response.isVerified()) {
            sessionData.setIsContactNumberDND(response.isDnd());
            sessionData.setIsContactNumberWhitelisted(response.isWhitelisted());
            sessionData.setIsContactNumberVerified(response.isVerified());
            sessionData.setContactNumber(response.getNumber());
            sessionData.setPhoneCode(response.getPhoneCode());

            sessionUtils._setCookieAndHeaders(request, httpResponse, sessionData);
        }

        // SEND successful phone numberAssociatedWithToken verification sms
//        try {
//            if (response.isVerified()) {
//                smsManager.sendSMS(CommunicationType.PHONE_VERIFICATION_SUCCESS, new UserInfo(response.getUser(), null, true),
//                        response.getNumber(), response.getPhoneCode());
////				if (response.getAllowFreebies()) {
////					allowFreebies(response.getUser(), true);
////				}
//            }
//        } catch (ForbiddenException ex) {
//            logger.warn("ForbiddenException in verifyContactNumber", ex);
//        } catch (Exception ex) {
//            logger.error("Exception in verifyContactNumber", ex);
//        }
        // processing Referral
        try {
            if (response.getProcessReferralBonus()) {
                if (response.getUser().getReferrerCode() != null) {
                    referralManager.processReferralBonusAsync(ReferralStep.VMOBILE, response.getUser().getId());
                }
            }
        } catch (Exception ex) {
            logger.error("Exception in processReferralBonus", ex);
        }

        return res;
    }

    private void allowFreebies(User user, Boolean noSMS) throws VException {

        String utm_campaign = user.getUtm_campaign();
        String allowFreebiesCampaigns = ConfigUtils.INSTANCE.getStringValue("freebies.allow.to.campaign");
        if (StringUtils.isNotEmpty(utm_campaign) && StringUtils.isNotEmpty(allowFreebiesCampaigns)) {
            String[] campaignNames = allowFreebiesCampaigns.split(",");
            for (String campaignName : campaignNames) {
                if (!campaignName.trim().equals(utm_campaign)) {
                    continue;
                }

                int amount = ConfigUtils.INSTANCE.getIntValue("freebies.amount.rupees." + campaignName.trim());
                int amountInPaisa = amount * 100;

                addFreebieToUser(amountInPaisa, user, noSMS);
                return;
            }
        }

        // Give default freebies in case user has no used tokens and campaign
        // freebies not recieved
        // using this method to get latest data from write server
        // User checkUser =
        // DAOFactory.INSTANCE.getUserDAO().getUserByIdFromWriteServer(user.getId());
        // if (!user.getIsEmailVerified()) {
        addJoinFreebieToUser(user, noSMS);
    }

    public void addJoinFreebieToUser(User user, Boolean noSMS) throws VException {
        int amountInPaisa = ConfigUtils.INSTANCE.getIntValue("freebies.rupees.join.bonus") * 100;
        if (amountInPaisa > 0) {
            addFreebieToUser(amountInPaisa, user, noSMS);
        }
    }

    public TransferFromFreebiesAccountRes addFreebieToUser(int amountInPaisa, User user, Boolean sendNoSMS) throws VException {

        TransferFromFreebiesAccountReq transferReq = new TransferFromFreebiesAccountReq(user.getId(), amountInPaisa,
                TransactionRefType.SINGUP_CREDIT, BillingReasonType.FREEBIES, false, sendNoSMS, true, null, null);

        TransferFromFreebiesAccountRes response = accountManager.transferFromFreebiesAccount(transferReq);
        /*
			 * Integer amount = transferReq.getAmount(); // TODO When FOS is
			 * removed, move it to dinero itself if (amount != null && amount >
			 * 0 &&
			 * !transferReq.getTransactionRefType().equals(TransactionRefType.
			 * REFERRAL_CREDIT) && !transferReq.getNoAlert()) { // since the
			 * transfer succeeded, lets add the notification // in case of
			 * TransactionRefType.REFERRAL_CREDIT the email will // be // send
			 * separately
			 * 
			 * FreebieInfo freebieInfo = new FreebieInfo(); int rupees =
			 * MoneyUtils.toRupees(amount);
			 * freebieInfo.setType("walletBalance");
			 * freebieInfo.setValue(rupees);
			 * 
			 * NotificationType notificationType = NotificationType.FREEBIES;
			 * try { CreateNotificationRequest request = new
			 * CreateNotificationRequest(user.getId(), notificationType, new
			 * Gson().toJson(freebieInfo), user.getRole());
			 * //notificationManager.createNotification(toUserId,
			 * notificationType, freebieInfo, null);
			 * notificationManager.createNotification(request); } catch
			 * (Exception e) { logger.error("sendFreebieBalanceNotification",
			 * e); }
			 * 
			 * try { emailManager.sendFreebieBalanceEmail(user, amount); } catch
			 * (Exception e) { logger.error("sendFreebieBalanceEmail", e); }
			 * 
			 * }
         */
        return response;

    }

    public String getUserPhoneNumbers(Long userId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserPhoneNumbers?userId=" + userId,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    // public String updateUser(String request) throws VException {
    // String userEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint +
    // "/updateUser", HttpMethod.POST, request,true);
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // return resp.getEntity(String.class);
    // }
    public User getUserById(Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user id found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + userId, HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        User response = new Gson().fromJson(jsonString, User.class);
        return response;
    }

    public List<User> getUsersByContactNumber(String contactNumber) throws VException {
        if (StringUtils.isEmpty(contactNumber)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user ids found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsersByContactNumber?contactNumber="
                + contactNumber, HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type filterListType = new TypeToken<List<User>>() {
        }.getType();
        List<User> response = new Gson().fromJson(jsonString, filterListType);
        return response;
    }

    public User getUserByEmail(String email) throws VException, UnsupportedEncodingException {
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No valid email found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserByEmail?email=" + URLEncoder.encode(email, "UTF-8"), HttpMethod.GET, null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        User response = new Gson().fromJson(jsonString, User.class);
        return response;
    }

    public UserBasicInfo getUserBasicInfoByEmail(String email) throws VException, UnsupportedEncodingException {
        if (!CustomValidator.validEmail(email)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No valid email found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserBasicInfoByEmail?email=" + URLEncoder.encode(email, "UTF-8"), HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        UserBasicInfo response = new Gson().fromJson(jsonString, UserBasicInfo.class);
        return response;
    }

    public SetProfilePicRes setProfilePic(SetProfilePicReq request) throws VException {
        request.verify();
        SetProfilePicRes setProfilePicRes = new SetProfilePicRes();
        if (request.getFileId() != null) {
            File file = fileDAO.getById(request.getFileId());
            if (file == null) {
                throw new NotFoundException(ErrorCode.FILE_NOT_FOUND, null);
            }

            if (!file.getUserId().equals(request.getUserId())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User doesn't own the specified file");
            }
            setProfilePicRes.setProfilePicUrl(userUtils.toProfilePicUrl(file.getPath()));
            request.setPath(file.getPath());
            // This is sent to user subsystem so that elastic search can be updated
            // for now
            request.setUrl(setProfilePicRes.getProfilePicUrl());
        } else {
            request.setPath(null);
            request.setUrl(null);
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/setProfilePic", HttpMethod.POST,
                new Gson().toJson(request), true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        // PlatformBasicResponse response = new Gson().fromJson(jsonString,
        // PlatformBasicResponse.class);
        logger.info("Response from setProfilePic " + jsonString);
        return setProfilePicRes;

    }

    // public void adminAccessOnlyCheck(Long userId) throws VException {
    // String userEndpoint =
    // ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    // ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint +
    // "/getRole?userId=" + userId,
    // HttpMethod.GET, null,true);
    // //handle error correctly
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    // Role role = new Gson().fromJson(jsonString, Role.class);
    //
    // if (role == null || !Role.ADMIN.equals(role)) {
    // throw new ForbiddenException(
    // ErrorCode.FORBIDDEN_ERROR,
    // "Operation not allowed to non-admin user["
    // + userId + "]");
    // }
    // }
    public void updateTeacherBoardMapping(Long userId, TeacherInfo teacherInfo, Long callingUserId) {

        List<String> grades = teacherInfo.getGrades();
        List<String> categories = teacherInfo.getCategories();

        Long primarySubject = teacherInfo.getPrimarySubject();
        Long secondarySubject = teacherInfo.getSecondarySubject();

        List<Board> boards = new ArrayList<>();
        if (primarySubject != null) {
            Board primaryBoard = boardDAO.getById(primarySubject);
            if (primaryBoard != null) {
                boards.add(primaryBoard);
            }
        }

        List<TeacherBoardMapping> oldTBM = teacherBoardMappingDAO.getTeacherBoardMappings(userId);
        List<TeacherBoardMapping> newTBM = new ArrayList<>();

        Set<TeacherBoardMapping> oldTBMSet = new HashSet<>(oldTBM);
        logger.info("oldTBMSet " + oldTBMSet);
        if (secondarySubject != null && !primarySubject.equals(secondarySubject)) {
            Board secondaryBoard = boardDAO.getById(secondarySubject);
            if (secondaryBoard != null) {
                boards.add(secondaryBoard);
            }
        }
        logger.info("creating the mappings for boards " + boards + ", categories " + categories + ", grades " + grades);
        for (Board board : boards) {
            for (String category : categories) {
                if (board.getCategories() != null && board.getCategories().contains(category)) {
                    for (String grade : grades) {
                        if (board.getGrades() != null && board.getGrades().contains(grade)) {
                            Set<Long> boardIds = new HashSet<>();
                            boardIds.add(board.getId());
                            TeacherBoardMapping teacherBoardMapping = new TeacherBoardMapping(userId, category, grade,
                                    boardIds);
                            newTBM.add(teacherBoardMapping);
                        }
                    }
                }
            }
        }
        // for(TeacherBoardMapping teacherBoardMapping : newTBM){
        // newTBMSet.add(teacherBoardMapping);
        // }
        Set<TeacherBoardMapping> newTBMSet = new HashSet<>(newTBM);
        logger.info("newTBMSet " + newTBMSet);
        if (oldTBM != null && !oldTBM.isEmpty()) {
            for (TeacherBoardMapping boardMapping : oldTBMSet) {
                if (!newTBMSet.contains(boardMapping)) {
                    logger.info("deleting old mapping" + boardMapping);
                    teacherBoardMappingDAO.deleteById(boardMapping.getId());
                }
            }
        }
        logger.info("upserting the new tbms " + newTBM);
        for (TeacherBoardMapping boardMapping : newTBM) {
            if (oldTBMSet.isEmpty() || !oldTBMSet.contains(boardMapping)) {
                logger.info("inserting tbm " + boardMapping);
                teacherBoardMappingDAO.upsert(boardMapping, callingUserId);
            }
        }
        // if (!newTBM.isEmpty()) {
        // teacherBoardMappingDAO.upsert(newTBM.get(0));
        // }
    }

    public void addSessionDuration(Long teacherId, Long duration, Long callingUserId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/addSessionDuration?teacherId=" + teacherId
                + "&duration=" + duration + "&callingUserId=" + callingUserId, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public PlatformBasicResponse markBlockStatus(SetBlockStatusForUserReq setBlockStatusForUserReq)
            throws VException, UnsupportedEncodingException {
        setBlockStatusForUserReq.verify();
        Boolean profileEnabled = setBlockStatusForUserReq.isProfileEnabled();
        if (!profileEnabled) {
            CreateNotificationRequest request = new CreateNotificationRequest(setBlockStatusForUserReq.getUserId(),
                    NotificationType.BLOCKED, null, Role.STUDENT);
            notificationManager.createNotification(request);
        } else {
            MarkSeenReq markSeenReq = new MarkSeenReq();
            markSeenReq.setUserId(setBlockStatusForUserReq.getUserId());
            markSeenReq.setCallingUserId(setBlockStatusForUserReq.getCallingUserId());
            markSeenReq.setNotificationType(NotificationType.BLOCKED);
            notificationManager.markSeenByType(markSeenReq);
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/markBlockStatus", HttpMethod.POST,
                gson.toJson(setBlockStatusForUserReq), true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse response = gson.fromJson(jsonString, PlatformBasicResponse.class);

        userRedisManager.addUserStatus(setBlockStatusForUserReq.getUserId(),
                setBlockStatusForUserReq.isProfileEnabled());
        if (!profileEnabled) {
            String reason = setBlockStatusForUserReq.getReason();
            // emailManager.sendBlockUserToAdmin(setBlockStatusForUserReq.getCallingUserId(),
            // setBlockStatusForUserReq.getUserId(), reason);
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("callingUserId", setBlockStatusForUserReq.getCallingUserId());
            payload.put("userId", setBlockStatusForUserReq.getUserId());
            payload.put("reason", reason);
            payload.put("callerUser", httpSessionUtils.getCurrentSessionData());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BLOCK_USER_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
        }
        return response;
    }

    public PlatformBasicResponse logout(HttpServletRequest request, HttpServletResponse response) {
        boolean resp = sessionUtils.removeVedTokenFromCookie(request, response);
        PlatformBasicResponse logoutResponse = new PlatformBasicResponse(resp, null, null);
        return logoutResponse;
    }

    public UserInfo getUserInfo(Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user id found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserInfo?userId=" + userId, HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        UserInfo response = gson.fromJson(jsonString, UserInfo.class);
        return response;
    }

    public void getCounter(HttpServletRequest req, HttpServletResponse resp) throws IOException, VException {
        Long counter = 0L;

        String action = req.getParameter("action");

        if ("memberCount".equals(action)) {
            Role role = req.getParameter("role") == null ? Role.STUDENT : Role.valueOfKey(req.getParameter("role"));

            counter = getMemberCount(role, false, true);
        } else if ("sessionDurationCount".equals(action)) {
            try {
                counter = sessionHourCountManager.getTotalSessionDuration(false);
            } catch (Exception ex) {
                logger.error("Error fetching the duration of sessions : " + ex.toString() + " current time : "
                        + System.currentTimeMillis());
            }
        } else {
            try {
                StatusCounterResponse statusCounterRes = new StatusCounterResponse(
                        961395l, 4066967877745l);

                resp.setHeader("Pragma", "private"); // HTTP 1.0
                resp.setDateHeader("Expires", new Date().getTime() + 3600000); // Proxies.
                resp.setHeader("Cache-Control", "private, max-age=3600, must-revalidate"); // HTTP
                // 1.1

                resp.getWriter().print(gson.toJson(statusCounterRes));
                return;
            } catch (Exception ex) {
                logger.error("Error fetching the duration of sessions : " + ex.toString() + " current time : "
                        + System.currentTimeMillis());
            }
        }

        resp.getWriter().print(counter);
    }

    public Long getMemberCount(Role role, Boolean reloadCache, Boolean verifiedCustomersOnly) throws VException {
        Long memberCount = 0L;
        String value = redisDAO.get(getCacheKey(role));
        if (value != null) {
            return Long.parseLong(value);
        }
        /*
		 * Query query = newQuery(User.class); query.setResult("count(this)");
		 * if (role != null) { query.setFilter(User.Constants.ROLE +
		 * " == roleParam && " + User.Constants.IS_EMAIL_VERIFIED +
		 * " == isEmailVerifiedParam"); query.
		 * declareParameters("String roleParam, Boolean isEmailVerifiedParam");
		 * } Object result = (role != null && verifiedCustomersOnly) ?
		 * query.execute(role, verifiedCustomersOnly) : query .execute();
		 * logger.info("getMemberCount result : " + result); memberCount =
		 * ((Long) result);
		 * 
         */
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getMemberCount?role=" + role + "&verifiedCustomersOnly=" + verifiedCustomersOnly,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        StatusCounterResponse response = new Gson().fromJson(jsonString, StatusCounterResponse.class);
        memberCount = response.getMemberCount();

        redisDAO.set(getCacheKey(role), memberCount.toString());
        return memberCount;
    }

    public String getCacheKey(Object... params) {
        return "User_count_" + params[0];
    }

    public void teacherAccessOnlyCheck(Long userId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getRole?userId=" + userId, HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Role role = new Gson().fromJson(jsonString, Role.class);

        if (role == null || !Role.TEACHER.equals(role)) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "Operation not allowed to non-admin user[" + userId + "]");
        }
    }

    public String getUserProfileLink(User user) {
        String userProfileLink = ConfigUtils.INSTANCE.getStringValue("url.base");
        userProfileLink += ConfigUtils.INSTANCE.getStringValue(user.getRole().getProfileUrlPropertyKey());
        userProfileLink += user.getId();
        return userProfileLink;
    }

    public String getUserProfileLink(UserBasicInfo user) {
        String userProfileLink = ConfigUtils.INSTANCE.getStringValue("url.base");
        userProfileLink += ConfigUtils.INSTANCE.getStringValue(user.getRole().getProfileUrlPropertyKey());
        userProfileLink += user.getUserId();
        return userProfileLink;
    }

    public String getFullTeacherExtensionNumber(User teacher) {
        if (!Role.TEACHER.equals(teacher.getRole())) {
            return null;
        }

        String extPhoneNumber = ConfigUtils.INSTANCE
                .getStringValue("teacher.call.extension." + teacher.getTeacherInfo().getPrimaryCallingNumberCode());
        String extensionNumber = String.format("%04d", teacher.getTeacherInfo().getExtensionNumber());
        return extPhoneNumber + ", Ext - " + extensionNumber;
    }

    public String getFullTeacherExtensionNumber(UserBasicInfo teacher) {
        if (!Role.TEACHER.equals(teacher.getRole())) {
            return null;
        }

        String extPhoneNumber = teacher.getPrimaryCallingNumberForTeacher();
        String extensionNumber = String.format("%04d", teacher.getExtensionNumber());
        return extPhoneNumber + ", Ext - " + extensionNumber;
    }

    public PlatformBasicResponse checkUsername(String username) throws VException, UnsupportedEncodingException {
        if (StringUtils.isEmpty(username)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "username missing");
        }
        User user = getUserByEmail(username);
        PlatformBasicResponse basicResp = new PlatformBasicResponse();
        if (user != null) {
            basicResp.setSuccess(Boolean.TRUE);
        } else {
            basicResp.setSuccess(Boolean.FALSE);
        }
        return basicResp;
    }

    public String getUserSystemIntroData(GetUserSystemIntroReq req, HttpServletRequest request) throws VException {
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(USER_ENDPOINT + "/getUserSystemIntroData?" + request.getQueryString(), HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    public UserSystemIntroRes addUserSystemIntroData(AddUserSystemIntroReq request) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/addUserSystemIntroData", HttpMethod.POST,
                gson.toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        UserSystemIntroRes userSystemIntroRes = new Gson().fromJson(jsonString, UserSystemIntroRes.class);
        return userSystemIntroRes;
    }

    private String getRedirectUrl(HttpServletRequest req) {
        String redirectUrl = getQueryParams(req.getHeader("referer")).get("redirect");
        try {
            URL url = new URL(req.getHeader("referer"));
            if (StringUtils.isEmpty(redirectUrl)) {
                String domainLink = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
                domainLink += "?" + url.getQuery();
                return domainLink;
            }
            redirectUrl += "?" + url.getQuery();
        } catch (Exception e) {
            logger.warn("Signup ", e);
            return null;
        }
        return redirectUrl;
    }

    private Map<String, String> getQueryParams(String urlStr) {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        if (StringUtils.isNotEmpty(urlStr)) {
            try {
                URL url = new URL(urlStr);
                final String[] pairs = url.getQuery().split("&");
                for (String pair : pairs) {
                    int idx = pair.indexOf("=");
                    query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
                            URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
                }
            } catch (Exception e) {
                logger.warn("Signup ", e);
            }
        }
        return query_pairs;
    }

    public void authenticate(HttpServletRequest request, HttpServletResponse response, SocialSource socialSource)
            throws VException, UnsupportedEncodingException, IOException, URISyntaxException, ServletException {

        logger.info("state " + request.getParameter("state"));
        String generatedString = request.getParameter("state");
        String metaData = redisDAO.get(generatedString);
        logger.info("metaData: " + metaData);
        if (metaData == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bad authenticate request");
        }
        String utmParams = null;
        String referrer = null;
        String signUpURL = null;
        String optIn = null;

        JSONObject json = new JSONObject(metaData);
        logger.info("jsonObject: " + json.toString());

        if (json.has("utmParams") && json.getString("utmParams") != null) {
            utmParams = json.getString("utmParams");
            logger.info("utmParams: " + utmParams);
        }
        if (json.has("signUpURL") && json.get("signUpURL") != null) {
            signUpURL = json.getString("signUpURL");
            logger.info("signUpURL: " + signUpURL);
        }
        if (json.has("referrer") && json.get("referrer") != null) {
            referrer = json.getString("referrer");
            logger.info("referrer: " + referrer);
        }
        if (json.has("optIn") && json.get("optIn") != null) {
            optIn = json.getString("optIn");
            logger.info("optIn: " + optIn);
        }

        UTMParams utm = gson.fromJson(utmParams, UTMParams.class);

        ISocialManager socialManager = getSocialManager(socialSource);
        String code = request.getParameter("code");
        logger.info("code: " + code);
        SocialUserInfo sociaUserInfo = socialManager.getUserInfo(code);
        JsonObject jsonObject = new JsonObject();
        request.setAttribute("data", jsonObject);
        request.setAttribute("source", socialSource.name());
        if (sociaUserInfo == null || StringUtils.isEmpty(sociaUserInfo.email)) {
            jsonObject.addProperty("error", "missing user email");
            request.getRequestDispatcher(STATUS_SOCIAL_LOGIN).forward(request, response);
            return;
        }
        // try to add user in db
        User user = getUserByEmail(sociaUserInfo.email);

        Boolean isSignUpURLSet = true;
        if (user != null && StringUtils.isNotEmpty(user.getSocialSource())) {
            if (StringUtils.isEmpty(user.getSignUpURL())) {
                isSignUpURLSet = false;
            } else {
                if (user.getSignUpURL().contains("google.com") || user.getSignUpURL().contains("facebook.com")) {
                    isSignUpURLSet = false;
                } else {
                    isSignUpURLSet = true;
                }
            }
        }

        if (user != null) {
            if (user.getProfileEnabled() != null && !user.getProfileEnabled()) {
                jsonObject.addProperty("error", ErrorCode.USER_BLOCKED.name());
                request.getRequestDispatcher(STATUS_SOCIAL_LOGIN).forward(request, response);
                return;
            }
            jsonObject.addProperty("login", true);
            jsonObject.addProperty("isSignUpURLSet", isSignUpURLSet);
            HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user);
            // createSession(request, response, user, request.getSession());
            jsonObject.add("userDetails", gson.toJsonTree(sessionData));
            // create the login session
        } else {
            /*
            //Not doing auto signup for new flow of signup--phone compulsory, email optional
            if (sessionUtils.getCurrentSessionData() == null) {

                // Redis check
                SignUpReq signUpReq = new SignUpReq();
                signUpReq.setEmail(sociaUserInfo.email);
                signUpReq.setFirstName(sociaUserInfo.firstName);
                signUpReq.setLastName(sociaUserInfo.lastName);
                if (sociaUserInfo.gender == null) {
                    signUpReq.setGender(Gender.UNKNOWN);
                } else {
                    signUpReq.setGender(sociaUserInfo.gender);
                }
                signUpReq.setPicUrl(sociaUserInfo.picUrl);
                signUpReq.setSocialSource(socialSource.toString());
                signUpReq.setTncVersion("v5");
                signUpReq.setRole(Role.STUDENT);
                if (utm != null) {
                    signUpReq.setUtm(utm);
                }
                signUpReq.setSignUpURL(signUpURL);
                signUpReq.setReferrer(referrer);
                if (StringUtils.isNotEmpty(optIn) && optIn.equals("true")) {
                    signUpReq.setOptIn(true);
                } else {
                    signUpReq.setOptIn(false);
                }

                SignUpRes sRes = signUpUser(signUpReq, request, response, socialSource.toString());
                sociaUserInfo.setUserId(sRes.getUserId().toString());
            }
             */
            // TODO: Check whether this works
            // String authToken = getAuthToke();
            // HttpSession session = request.getSession();
            // session.setAttribute(SignInReq.AUTH_TOKEN, authToken);
            // request.setAttribute(SignInReq.AUTH_TOKEN, authToken);
            // HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
            /*if (sessionData != null) {
                sessionData.setProfilePicUrl(sociaUserInfo.picUrl);
                sessionUtils._setCookieAndHeaders(request, response, sessionData);
                // session.setAttribute(USER_PIC_URL, sociaUserInfo.picUrl);
            }*/
            jsonObject.add("response", gson.toJsonTree(sociaUserInfo));
        }
        request.getRequestDispatcher(STATUS_SOCIAL_LOGIN).forward(request, response);

    }

    public HttpSessionData authenticateAppLogin(HttpServletRequest request, HttpServletResponse response,
            SocialSource socialSource, String code)
            throws ServletException, IOException, ForbiddenException, VException, URISyntaxException {
        ISocialManager socialManager = getSocialManager(socialSource);
        logger.info("code from app: " + code);
        SocialUserInfo sociaUserInfo = null;
        if (socialSource.equals(SocialSource.FACEBOOK)) {
            sociaUserInfo = facebookManager.getUserInfoUsingToken(code);// here
            // the
            // code
            // is
            // actually
            // access
            // token
        } else {
            sociaUserInfo = socialManager.getUserInfo(code);
        }
        if (sociaUserInfo == null || StringUtils.isEmpty(sociaUserInfo.email)) {
            throw new ForbiddenException(ErrorCode.INVALID_SOCIAL_USER_INFO, null);
        }
        User user = getUserByEmail(sociaUserInfo.email);
        if (user != null) {
            if (user.getProfileEnabled() != null && !user.getProfileEnabled()) {
                throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked");
            } else {
                // SignInRes signInRes =
                // LoginManager.INSTANCE.createSession(request,
                // response, user, request.getSession());
                HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user);
                return sessionData;
            }
        } else {
            throw new ForbiddenException(ErrorCode.USER_NOT_FOUND, null);
        }
    }

    public AppSocialLoginRes authenticateAppLoginNew(HttpServletRequest request, HttpServletResponse response,
            AppSocialLoginReq req)
            throws ServletException, IOException, ForbiddenException, VException, URISyntaxException {
        SocialSource socialSource = req.getSource();
        String code = req.getCode();
        ISocialManager socialManager = getSocialManager(socialSource);
        logger.info("code from app: " + code);
        SocialUserInfo sociaUserInfo = null;
        if (socialSource.equals(SocialSource.FACEBOOK)) {
            sociaUserInfo = facebookManager.getUserInfoUsingToken(code);// here
            // the
            // code
            // is
            // actually
            // access
            // token
        } else {
            sociaUserInfo = socialManager.getUserInfo(code);
        }
        if (sociaUserInfo == null || StringUtils.isEmpty(sociaUserInfo.email)) {
            throw new ForbiddenException(ErrorCode.INVALID_SOCIAL_USER_INFO, null);
        }
        User user = getUserByEmail(sociaUserInfo.email);
        if (user != null) {
            if (user.getProfileEnabled() != null && !user.getProfileEnabled()) {
                throw new ForbiddenException(ErrorCode.USER_BLOCKED, "User is blocked");
            } else {
                // SignInRes signInRes =
                // LoginManager.INSTANCE.createSession(request,
                // response, user, request.getSession());
                HttpSessionData sessionData = sessionUtils.setCookieAndHeaders(request, response, user);
                return new AppSocialLoginRes(sessionData);
            }
        } else {
//            throw new ForbiddenException(ErrorCode.USER_NOT_FOUND, null);            
            if (sessionUtils.getCurrentSessionData() == null) {
                logger.info("user not found in db, so creating a new user and setting an Xvedtoken in headers");
                SignUpReq signUpReq = new SignUpReq();
                signUpReq.setEmail(sociaUserInfo.email);
                signUpReq.setFirstName(sociaUserInfo.firstName);
                signUpReq.setLastName(sociaUserInfo.lastName);
                if (sociaUserInfo.gender == null) {
                    signUpReq.setGender(Gender.UNKNOWN);
                } else {
                    signUpReq.setGender(sociaUserInfo.gender);
                }
                signUpReq.setPicUrl(sociaUserInfo.picUrl);
                signUpReq.setSocialSource(socialSource.toString());
                signUpReq.setTncVersion("v5");
                signUpReq.setRole(Role.STUDENT);
                signUpReq.setSignUpFeature(FeatureSource.DOUBT_APP);
                if (req.getUtm() != null) {
                    signUpReq.setUtm(req.getUtm());
                }
                signUpReq.setSignUpURL(req.getSignUpURL());
                signUpReq.setReferrer(req.getReferrer());

                SignUpRes sRes = signUpUser(signUpReq, request, response, socialSource.toString());
                return new AppSocialLoginRes(sRes);
            } else {
                throw new BadRequestException(ErrorCode.ALREADY_REGISTERED, "valid-token-already being sent in request");
            }
        }
    }

    private ISocialManager getSocialManager(SocialSource socialSource) {
        switch (socialSource) {
            case GOOGLE:
                return googleManager;
            case FACEBOOK:
                return facebookManager;
            default:
                return null;

        }
    }

    public void authorize(HttpServletResponse response, SocialSource socialSource, String utmParams, String signUpURL, String referrer, String optIn) throws IOException, InternalServerErrorException {

        ISocialManager socialManager = getSocialManager(socialSource);

        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

        JSONObject json = new JSONObject();
        json.put("utmParams", utmParams);
        json.put("signUpURL", signUpURL);
        json.put("referrer", referrer);
        json.put("optIn", optIn);

        redisDAO.setex(generatedString, json.toString(), 3600);

        String authUrl = socialManager.getAuthorizeUrl(generatedString);
        logger.info("auth url : " + authUrl);
        response.sendRedirect(authUrl);
    }

    public ConnectSessionCallRes connectCRMCall(StudentTeacherExotelCallConnectReq req) throws VException {

        UserBasicInfo teacher = null;
        UserBasicInfo student = null;

        Set<Long> userIds = new HashSet<>();
        userIds.add(req.getStudentId());
        userIds.add(req.getTeacherId());
        Map<Long, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        if (userInfos.containsKey(req.getStudentId())) {
            student = userInfos.get(req.getStudentId());
        }
        if (userInfos.containsKey(req.getTeacherId())) {
            teacher = userInfos.get(req.getTeacherId());
        }
        if (student == null || teacher == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "Either teacher/student not found");
        }

        String teacherPhoneNumberForCall = teacher.getContactNumber();
        String studentPhoneNumberForCall = student.getContactNumber();
        String teacherPhoneCode = teacher.getPhoneCode();
        String studentPhoneCode = student.getPhoneCode();

        if (teacherPhoneNumberForCall == null) {
            throw new ConflictException(ErrorCode.TEACHER_ACTIVE_NUMBER_NOT_FOUND,
                    "teacher number [" + teacherPhoneNumberForCall + "] not active");
        }
        if (studentPhoneNumberForCall == null) {
            throw new ConflictException(ErrorCode.STUDENT_ACTIVE_NUMBER_NOT_FOUND,
                    "student number[" + studentPhoneNumberForCall + "] not active");
        }
        if (!"91".equals(teacherPhoneCode)) {
            throw new ForbiddenException(ErrorCode.TEACHER_CALL_SERVICE_NOT_ALLOWED,
                    "teacher phoneCode [" + teacher.getPhoneCode() + "] not allowed");
        }
        if (!"91".equals(studentPhoneCode)) {
            throw new ForbiddenException(ErrorCode.STUDENT_CALL_SERVICE_NOT_ALLOWED,
                    "student phoneCode [" + student.getPhoneCode() + "] not allowed");
        }
        PhoneCallMetadata phoneCallMetadata = exotelPhoneCallManager.connectCall(teacherPhoneNumberForCall,
                studentPhoneNumberForCall, null, ExotelPhoneCallManager.CALL_CONTEXT_CRM, req.getCallingUserId());

        ConnectSessionCallRes res = new ConnectSessionCallRes(phoneCallMetadata.getCallSid());
        return res;
    }

    public PlatformBasicResponse messageUser(MessageUserReq req) throws VException {
        req.verify();
        messageUser(req.getFromUserId(), req.getToUserId(), req.getContextType(), req.getMessage(),
                req.isSendNoEmail());
        return new PlatformBasicResponse();
    }

    public void messageUser(Long fromUserId, Long toUserId, String contextType, String message, boolean sendNoEmail)
            throws VException {
        User toUser = getUserById(toUserId);
        if (toUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id[" + toUserId + "]");
        }

        User fromUser = getUserById(fromUserId);
        if (fromUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id[" + fromUserId + "]");
        }

        if (fromUser.getRole().equals(toUser.getRole())) {
            throw new ForbiddenException(ErrorCode.FROM_TO_ROLES_ARE_SAME, fromUser.getRole()
                    + " cannot send message to " + toUser.getRole());
        }

        if (Role.STUDENT_CARE.equals(fromUser.getRole())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Student care not allowed to send mail");
            //return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("fromName", fromUser._getFullName());
        String fromUserRole = "User";
        switch (fromUser.getRole()) {
            case STUDENT:
                fromUserRole = "Student";
                break;
            case TEACHER:
                fromUserRole = "Teacher";
                break;
            case ADMIN:
                fromUserRole = "Vedantu";
                break;
            case STUDENT_CARE:
                fromUserRole = "Vedantu Counselor";
                break;
            case SEO_MANAGER:
                fromUserRole = "Seo Manager";
                break;
            case SEO_EDITOR:
                fromUserRole = "Seo Editor";
                break;
            default:
                fromUserRole = "User";
                break;
        }
        bodyScopes.put("fromUserRole", fromUserRole.toLowerCase());
        bodyScopes.put("fromUserMobileNumber", fromUser.getContactNumber());
        bodyScopes.put("fromUserEmailId", fromUser.getEmail());
        bodyScopes.put("message", message);
        bodyScopes.put("fromUserLink", getUserProfileLink(fromUser));

        MessageUserRes messageUser = new MessageUserRes(fromUser.getId(), toUser.getId(), contextType, message);
        messageUser = messageUserManager.addMessageUser(messageUser);
        if (!messageUser.isSendSMS()) {
            return;
        }
        String replyURL = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT")
                + ConfigUtils.INSTANCE.getStringValue("message.user.email.url");
        replyURL += "messageId=" + messageUser.getId();

        // URL shortening
        replyURL = WebCommunicator.getShortUrl(replyURL);
        bodyScopes.put("replyURL", replyURL);

        // Check if there is already an entry in MessageTrigger
//        messageTriggerManager.handleMessageTrigger(fromUser, toUser, messageUser);
        // Send Email if needed TODO
        if (!sendNoEmail) {
            List<Role> allowedRoles = Arrays.asList(Role.STUDENT, Role.TEACHER);
            if (allowedRoles.contains(toUser.getRole()) && allowedRoles.contains(fromUser.getRole())) {

                bodyScopes.put("teacherName", toUser.getFullName());
                bodyScopes.put("studentName", fromUser.getFullName());
                if (Role.TEACHER.equals(fromUser.getRole())) {
                    bodyScopes.put("teacherName", fromUser.getFullName());
                    bodyScopes.put("studentName", toUser.getFullName());
                }

                Map<String, Object> payload = new HashMap<>();
                payload.put("fromUser", fromUser);
                payload.put("toUser", toUser);
                payload.put("bodyScopes", bodyScopes);
                payload.put("type", CommunicationType.MESSAGE_TO_USER);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.MESSAGE_USER_EMAIL, payload);
                asyncTaskFactory.executeTask(params);
            }
            // emailManager.sendMessageUserEmail(fromUser, toUser, bodyScopes,
            // CommunicationType.MESSAGE_TO_USER);
        }
    }

    @Deprecated
    public PlatformBasicResponse messageUserReply(MessageUserReplyReq req) throws VException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/messageuser/messageUserReply", HttpMethod.POST,
                gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);

        return platformBasicResponse;

        /*
        MessageUserRes messageUser = messageUserManager.getMessageById(req.getMessageId());

        //messageUser(messageUser.getToUserId(), messageUser.getUserId(), messageUser.getContextType(), req.getMessage(), false);
        User toUser = getUserById(messageUser.getUserId());
        if (toUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id[" + messageUser.getUserId() + "]");
        }

        User fromUser = getUserById(messageUser.getToUserId());
        if (fromUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id[" + messageUser.getToUserId() + "]");
        }

        if (fromUser.getRole().equals(toUser.getRole())) {
            throw new ForbiddenException(ErrorCode.FROM_TO_ROLES_ARE_SAME, fromUser.getRole()
                    + " cannot send message to " + toUser.getRole());
        }

        String fromUserRole = "User";
        switch (fromUser.getRole()) {
            case STUDENT:
                fromUserRole = "Student";
                break;
            case TEACHER:
                fromUserRole = "Teacher";
                break;
            case ADMIN:
                fromUserRole = "Vedantu";
                break;
            case STUDENT_CARE:
                fromUserRole = "Vedantu Counselor";
                break;
            case SEO_MANAGER:
                fromUserRole = "Seo Manager";
                break;
            case SEO_EDITOR:
                fromUserRole = "Seo Editor";
                break;
            default:
                fromUserRole = "User";
                break;
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("fromName", fromUser._getFullName());
        bodyScopes.put("fromUserRole", fromUserRole.toLowerCase());
        bodyScopes.put("fromUserMobileNumber", fromUser.getContactNumber());
        bodyScopes.put("fromUserEmailId", fromUser.getEmail());
        bodyScopes.put("message", req.getMessage());
        bodyScopes.put("fromUserLink", getUserProfileLink(fromUser));

        String replyURL = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT")
                + ConfigUtils.INSTANCE.getStringValue("message.user.email.url");
        replyURL += "messageId=" + messageUser.getId();

        // URL shortening
        replyURL = WebCommunicator.getShortUrl(replyURL);
        bodyScopes.put("replyURL", replyURL);

        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("fromUser", fromUser);
        payload.put("toUser", toUser);
        payload.put("bodyScopes", bodyScopes);
        payload.put("type", CommunicationType.MESSAGE_TO_USER);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.MESSAGE_USER_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        return platformBasicResponse;
         */
    }

    public MessageUserServletRes getMessageById(Long messageId) throws VException {
        if (messageId == null || messageId <= 0) {
            throw new NotFoundException(ErrorCode.INVALID_MESSAGE_ID, "Invalid message id");
        }
        MessageUserRes messageUser = messageUserManager.getMessageById(messageId);
        if (messageUser == null) {
            throw new NotFoundException(ErrorCode.MESSAGE_NOT_FOUND, "no message found with id[" + messageId + "]");
        }
        Set<Long> userIds = new HashSet<>();
        userIds.add(messageUser.getUserId());
        userIds.add(messageUser.getToUserId());
        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        UserBasicInfo fromUser = userMap.get(messageUser.getUserId());
        UserBasicInfo toUser = userMap.get(messageUser.getToUserId());
        MessageUserServletRes messageUserRes = new MessageUserServletRes(fromUser, toUser, messageUser.getMessage());
        return messageUserRes;
    }

    public GetMessageUsersRes getMessageUsers(GetMessageUsersReq req) throws VException {
        List<MessageUserRes> messageUsers = messageUserManager.getMessages(req);

        return _putUserInfoForMessages(messageUsers);
    }

    public GetMessageLeadersRes getMessageLeaders(GetMessageUsersReq req) throws VException {
        GetMessageLeadersRes messageLeaders = messageUserManager.getMessageLeaders(req);

        return _putLeaderInfoForMessages(messageLeaders);
    }

    private GetMessageUsersRes _putUserInfoForMessages(List<MessageUserRes> messageUsers) throws VException {
        List<MessageUserInfo> messageUserInfos = new ArrayList<>();
        if (messageUsers != null && !messageUsers.isEmpty()) {
            List<String> userIdList = new ArrayList<>();
            for (MessageUserRes messageUser : messageUsers) {
                if (messageUser != null) {
                    // messageUserInfos.add(new MessageUserInfo(messageUser));
                    String  userId = messageUser.getUserId().toString();
                    String toUserId = messageUser.getToUserId().toString();
                    if (!userIdList.contains(userId)) {
                        userIdList.add(userId);
                    }
                    if (!userIdList.contains(toUserId)) {
                        userIdList.add(toUserId);
                    }
                }
            }
            List<UserBasicInfo> usersList = fosUtils.getUserBasicInfosSet(new HashSet(userIdList), false);
            Map<Long, UserBasicInfo> usersMap = new HashMap<>();
            for (UserBasicInfo messageUser : usersList) {
                if (messageUser != null) {
                    usersMap.put(messageUser.getUserId(), messageUser);
                }
            }
            for (MessageUserRes message : messageUsers) {
                UserBasicInfo fromUser = null;
                UserBasicInfo toUser = null;
                if (usersMap.containsKey(message.getUserId())) {
                    fromUser = usersMap.get(message.getUserId());
                }
                if (usersMap.containsKey(message.getToUserId())) {
                    toUser = usersMap.get(message.getToUserId());
                }
                messageUserInfos.add(new MessageUserInfo(message, fromUser, toUser));
            }
        }

        GetMessageUsersRes res = new GetMessageUsersRes();
        res.setList(messageUserInfos);
        return res;
    }

    private GetMessageLeadersRes _putLeaderInfoForMessages(GetMessageLeadersRes messageLeaders) throws VException {
        if (messageLeaders != null && !(messageLeaders.getCount() == 0)) {
            List<Long> userIdList = new ArrayList<Long>();
            for (MessageUserLeader messageUser : messageLeaders.getList()) {
                if (messageUser != null) {
                    // messageUserInfos.add(new MessageUserInfo(messageUser));
                    Long userId = messageUser.getTeacherId();
                    if (!userIdList.contains(userId)) {
                        userIdList.add(userId);
                    }
                }
            }
            Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIdList, false);
            for (MessageUserLeader messageUser : messageLeaders.getList()) {
                if (messageUser != null) {
                    // messageUserInfos.add(new MessageUserInfo(messageUser));
                    Long userId = messageUser.getTeacherId();
                    if (userMap.containsKey(userId)) {
                        messageUser.setTeacher(userMap.get(userId));
                    }
                }
            }
        }
        return messageLeaders;
    }

    public void userUpdationOperation(User user) throws VException {
        Role role = user.getRole();
        if (role != null) {
            if (Role.STUDENT.equals(role)) {
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_UPSERT, user, null, null);
                leadSquaredManager.executeAsyncTask(req);
            } else if (Role.TEACHER.equals(role)) {
                Map<String, Object> payload = new HashMap<String, Object>();
                payload.put("user", user);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_UPDATE_TEACHER_DATA, payload);
                asyncTaskFactory.executeTask(params);
            }
        }
    }

    public void performSignUpTasks(User user, String mobileTokenCode,
            String emailTokenCode, Long callingUserId) {
        //stopping verification email and sms in case of seo manager signup flow
        boolean stopverificationEmail = false;
        if (FeatureSource.SEO_MANAGER_DOWNLOAD.equals(user.getSignUpFeature())) {
            stopverificationEmail = true;
        }

        // sending verification sms
        if (user != null && (FeatureSource.ISL.equals(user.getSignUpFeature())
                || FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user.getSignUpFeature()))
                || FeatureSource.VSAT.equals(user.getSignUpFeature())) {
            if (Role.STUDENT.equals(user.getRole())) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("user", user);
                if (FeatureSource.ISL.equals(user.getSignUpFeature())) {
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_ISL, payload);
                    asyncTaskFactory.executeTask(params);
                } else if (FeatureSource.ISL_REGISTRATION_VIA_TOOLS.equals(user.getSignUpFeature())) {
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SIGNUP_VIA_ISL_REGISTRATION_TOOLS, payload);
                    asyncTaskFactory.executeTask(params);
                } else if (FeatureSource.VSAT.equals(user.getSignUpFeature())) {
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VSAT, payload);
                    asyncTaskFactory.executeTask(params);

                }else if (FeatureSource.VOLT_2020_JAN.equals(user.getSignUpFeature())) {
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VOLT, payload);
                    asyncTaskFactory.executeTask(params);
                }
            }
        } else {
            try {
                if (StringUtils.isNotEmpty(user.getContactNumber()) && StringUtils.isNotEmpty(mobileTokenCode) && !stopverificationEmail) {
                    sendContactNumberVerificationSMS(new UserInfo(user, null, true), user.getContactNumber(), user.getPhoneCode(),
                            mobileTokenCode, user.getUtm_campaign());
                }
            } catch (VException ex) {
                logger.error("Error sending verification sms", ex);
            }

            // email sending
            boolean isAppUser = false;
            if (user.getAppId() != null && user.getAppId() > 0) {
                isAppUser = true;
            }

            // Try/catch because welcome mail should not block subsequent operations
            try {
                boolean welcomeEmailSent = false;
                if (Role.STUDENT.equals(user.getRole())) {
                    welcomeEmailSent = true;
                    if (FeatureSource.SEO_MANAGER_DOWNLOAD.equals(user.getSignUpFeature())) {
//                        emailManager.sendUserEmailToDownloadSEOContent(user, user.getSignUpFeatureRefId());
                    } else {
                        Map<String, Object> payload = new HashMap<>();
                        payload.put("user", user);
                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WELCOME_EMAIL, payload);
                        asyncTaskFactory.executeTask(params);
                    }
                }
                if (user.getIsEmailVerified() && !isAppUser && !stopverificationEmail) {
                    if (!welcomeEmailSent) {
                        emailManager.sendWelcomeEmail(user);
                    }
                } else if (!stopverificationEmail) {
                    emailManager.sendVerificationTokenEmail(new UserBasicInfo(user, true), CommunicationType.SIGNUP,
                            emailTokenCode);
                }
            } catch (IOException | VException ex) {
                logger.error("Error sending verification/welcome mail", ex);
            }
        }
        // This needs referrer userId and also needs to check
        try {
            if (user.getReferrerCode() != null && !user.getRole().equals(Role.ADMIN)) {
                referralManager.processReferralBonus(ReferralStep.SIGNUP, user.getId());
            }
        } catch (Exception e) {
            logger.error("processReferralBonus", e);
        }

        if (user.getRole().equals(Role.TEACHER)) {
            try {
                eSManager.setTeacherData(user);
            } catch (Exception e) {
                logger.error("Exception in Elastic search add teacher task", e);
            }
        }

        if (user.getRole().equals(Role.TEACHER)) {
            updateTeacherBoardMapping(user.getId(), user.getTeacherInfo(), callingUserId);
        }

        if (Role.STUDENT.equals(user.getRole())) {
            try {
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_UPSERT, user, null, null);
                leadSquaredManager.executeAsyncTask(req);
            } catch (Exception e) {
                logger.error("Exception occured in pushing data to lead squared");
            }
        }

    }

    public void updateLead(User user) {
        if (Role.STUDENT.equals(user.getRole())) {
            try {
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_UPSERT, user, null, null);
                leadSquaredManager.executeAsyncTask(req);
            } catch (Exception e) {
                logger.error("Exception occured in pushing data to lead squared");
            }
        }
    }

    public PlatformBasicResponse indexUser(Long userId, String key) throws VException, IOException {
        if (key.equals("xgA9me8EvwCccX5D")) {
            User user = getUserById(userId);
            if (user == null || !Role.TEACHER.equals(user.getRole())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "user null or role not teacher");
            }
            eSManager.indexElasticSearchData(Arrays.asList(user));
        } else {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "key not correct");
        }
        return new PlatformBasicResponse();
    }

    public void userEventsOperation(UserEvents eventType, String message) throws VException {
        switch (eventType) {
            case USER_UPDATED:
                User user = gson.fromJson(message, User.class);
                if (user != null) {
                    userUpdationOperation(user);
                }
                break;
            case USER_UPDATED_ISL:
                break;
            case USER_LEAD_SMS:
                UserLeadInfo info = gson.fromJson(message, UserLeadInfo.class);
                if (info != null && StringUtils.isNotEmpty(info.getContactNumber())) {
                    miscManager.sendUserLeadMessage(info);
                }
                break;
            default:
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "eventType not supported");
        }
    }

    public GetActiveTutorsRes getActiveTutorsList(HttpServletRequest httpRequest, GetActiveTutorsReq req)
            throws VException {
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(USER_ENDPOINT + "/getActiveTutorsList?" + httpRequest.getQueryString(), HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GetActiveTutorsRes response = new Gson().fromJson(jsonString, GetActiveTutorsRes.class);
        return response;
    }

    public String getTeacherContactFromExtension(ExtensionNumberReq req)
            throws InternalServerErrorException, VException, IllegalArgumentException, IllegalAccessException {
        String CallSid = req.getCallSid();
        String number = redisDAO.get(CallSid);
        if (StringUtils.isNotEmpty(number)) {
            return number;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getTeacherContactFromExtension?" + WebUtils.INSTANCE.createQueryStringOfObject(req),
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String phoneNumber = resp.getEntity(String.class);
        redisDAO.set(req.getCallSid(), phoneNumber);
        return phoneNumber;
    }

    public PlatformBasicResponse sendUserRelatedEmail(SendUserEmailReq req) throws VException {
        PlatformBasicResponse sendUserEmailRes = new PlatformBasicResponse();
        VerificationTokenType verificationTokenType = null;
        if (CommunicationType.SIGNUP == req.getEmailType()) {
            verificationTokenType = VerificationTokenType.EMAIL_VERIFICATION;
        }
        if (null == verificationTokenType) {
            sendUserEmailRes.setSuccess(false);
            return sendUserEmailRes;
        }

        UserBasicInfo user = fosUtils.getUserBasicInfo(req.getUserId().toString(), true);
        if (null == user) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, null);
        }
        String callingUserId = null;
        if (req.getCallingUserId() != null) {
            callingUserId = req.getCallingUserId().toString();
        }

        String emailToken = verificationTokenManager.getEmailToken(req.getUserId(), user.getEmail(),
                verificationTokenType, callingUserId);

        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("user", user);
        payload.put("type", req.getEmailType());
        payload.put("tokenCode", emailToken);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.VERIFICATION_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        return sendUserEmailRes;
    }

    public List<EsTeacherInfo> getTeacherInfos(String userIdsList) throws Exception {
        List<EsTeacherInfo> response = new ArrayList<>();
        String[] ids = org.apache.commons.lang3.StringUtils.split(userIdsList, ",");
        if (ids != null) {
            List<Long> longIds = new ArrayList<>();
            for (String id : ids) {
                longIds.add(Long.parseLong(id));
            }
            ESSearchResponse esSearchResponse = eSManager.getTeachersByIds(longIds);
            if (esSearchResponse != null) {
                return esSearchResponse.getTeacherData();
            }
        }
        return response;
    }

    public List<UserBasicInfo> getUserInfos(String userIdsList, boolean exposeEmail) throws Exception {
        List<UserBasicInfo> response = new ArrayList<>();
        String[] ids = org.apache.commons.lang3.StringUtils.split(userIdsList, ",");
        if (ids != null) {
            Set<Long> longIds = new HashSet<>();
            for (String id : ids) {
                longIds.add(Long.parseLong(id));
            }

            if (ArrayUtils.isNotEmpty(longIds)) {
                List<UserBasicInfo> userBasicInfos = fosUtils.getUserBasicInfosFromLongIds(longIds, exposeEmail);
                if (ArrayUtils.isNotEmpty(userBasicInfos)) {
                    response.addAll(userBasicInfos);
                }
            }
        }
        return response;
    }

    public String incOtfSubscriptionCount(Long userId) throws VException {
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/incOtfSubscriptionCount/" + userId,
                HttpMethod.POST, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String incRespStr = incResp.getEntity(String.class);
        logger.info("Response from incOtfSubscriptionCount: " + incRespStr);
        return incRespStr;
    }

    public PlatformBasicResponse showInterestFromCourseListing(ShowInterestCourseListingReq showInterestCourseListingReq) throws BadRequestException, Exception {
        showInterestCourseListingReq.verify();
        ShowInterestReq showInterestReq = mapper.map(showInterestCourseListingReq, ShowInterestReq.class);
        if (ArrayUtils.isNotEmpty(showInterestCourseListingReq.getSubjects())) {
            showInterestReq.setSubject(StringUtils.join(showInterestCourseListingReq.getSubjects().toArray(), ","));
        }
        return showInterest(showInterestReq);

    }

    public PlatformBasicResponse showInterest(ShowInterestReq req) throws
            BadRequestException, NotFoundException, Exception {
        req.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "not logged in");
        }
        User user = getUserById(sessionUtils.getCallingUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user with id "
                    + req.getUserId() + " not found");
        }

        Map<String, String> lsParams = new HashMap<>();
        lsParams.put("userEmail", user.getEmail());
        if (req.getInterestType() != null) {
            lsParams.put("interestType", req.getInterestType().name());
        }
        if (req.getEntityType() != null) {
            lsParams.put("entityType", req.getEntityType().name());
        }
        lsParams.put("entityId", req.getEntityId());
        lsParams.put("entityName", req.getEntityName());
        lsParams.put("note", req.getNote());
        lsParams.put("subject", req.getSubject());
        lsParams.put("target", req.getTarget());
        lsParams.put("grade", req.getGrade());
        if (req.getTeacherId() != null) {
            UserBasicInfo teacher = fosUtils.getUserBasicInfo(req.getTeacherId(), true);
            lsParams.put("teacherEmail", teacher.getEmail());
        }
        lsParams.put("userId", req.getUserId().toString());
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            lsParams.put("mobileNumber", user.getContactNumber());
        }
        LeadSquaredRequest leadReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                LeadSquaredDataType.LEAD_ACTIVITY_SHOW_INTEREST, user, lsParams,
                req.getCallingUserId());
        leadSquaredManager.executeTask(leadReq);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse addUserAddress(AddUserAddressReq addAddressReq) throws VException {
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/addAddress",
                HttpMethod.POST, gson.toJson(addAddressReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String incRespStr = incResp.getEntity(String.class);
        logger.info("Response from incOtfSubscriptionCount: " + incRespStr);
        return new PlatformBasicResponse();
    }

    public UserDetailsInfo createUserDetailsForExistingUser(ExistingUserISLRegReq req) throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "not logged in");
        }
        req.setUserId(sessionData.getUserId());
        return _createUserDetailsForExistingUser(req);
    }

    public UserDetailsInfo _createUserDetailsForExistingUser(ExistingUserISLRegReq req) throws VException {
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/createUserDetailsForExistingUser",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);

        logger.info("createUserDetailsForExistingUser: User BasicInfo : {}", jsonString);
        UserDetailsInfo response = new Gson().fromJson(jsonString, UserDetailsInfo.class);
        try {
            User user = new User();
            user.setFirstName(response.getStudentName());
            if (response.getStudent() == null || (StringUtils.isEmpty(response.getStudent().getEmail())
                    && StringUtils.isEmpty(response.getStudent().getContactNumber()))) {
                logger.error("User BasicInfo or email and contact number is null");
            }
            user.setEmail(response.getStudent().getEmail());
            StudentInfo studentInfo = new StudentInfo();
            user.setStudentInfo(studentInfo);
            user.setRole(Role.STUDENT);
            user.setId(req.getUserId());
            user.setContactNumber(response.getParentPhoneNo());
            user.setPhoneCode(response.getParentPhoneCode());
            if (StringUtils.isEmpty(user.getContactNumber())) {
                user.setContactNumber(req.getContactNumber());
                user.setPhoneCode(req.getPhoneCode());
            }
            user.setTempContactNumber(response.getStudentPhoneNo());
            Map<String, Object> payload = new HashMap<>();
            payload.put("user", user);
            payload.put("userDetails", response);
            AsyncTaskParams params = null;

            if (null != req.getFeatureSource()) {
                switch (req.getFeatureSource()) {
                    case ISL_REGISTRATION_VIA_TOOLS:
                        params = new AsyncTaskParams(AsyncTaskName.USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS, payload);
                        break;
                    case ISL:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_ISL, payload);
                        break;
                    case VSAT:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VSAT, payload);
                        break;
                    case VOLT_2020_JAN:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VOLT, payload);
                        break;
                    case JRP_SEP_2019:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTERED_EMAIL_JRP, payload);
                        break;

                }
            }
            else if(null != req.getSignUpFeature()) {
                switch (req.getSignUpFeature()) {
                    case REVISEINDIA_2020_FEB:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_REVISEINDIA, payload);
                        break;
                    case REVISE_JEE_2020_MARCH:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_REVISEJEE, payload);
                        break;
                    case VSAT:
                        params = new AsyncTaskParams(AsyncTaskName.REGISTRATION_EMAIL_VSAT, payload);
                        break;
                }
            }
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.error("Error sending Registration mail");
        }
        return response;
    }

    public UserDetailsInfo getUserDetailsForISLInfo(Long userId, String event) throws VException {
        UserDetailsInfo info = getUserDetailsInfo(userId, event);
        if (info != null) {
            info.setServerTime(System.currentTimeMillis());
            return info;
        }
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserDetailsForISLInfo?userId=" + userId + "&event=" + event,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);
        UserDetailsInfo response = new Gson().fromJson(jsonString, UserDetailsInfo.class);
        response.setServerTime(System.currentTimeMillis());
        return response;
    }

    public IsRegisteredForISLRes isRegisteredForISL(Long userId, String event) throws VException {
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/isRegisteredForISL?userId=" + userId + "&event=" + event,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);
        IsRegisteredForISLRes response = new Gson().fromJson(jsonString, IsRegisteredForISLRes.class);
        return response;

    }

    public IsRegisteredForISLRes isRegisteredForVolt(Long userId, String event) throws VException {
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/isRegisteredForVolt?userId=" + userId + "&event=" + event,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);
        IsRegisteredForISLRes response = new Gson().fromJson(jsonString, IsRegisteredForISLRes.class);
        return response;

    }

    public SignUpForISLRes signUpForISL(SignUpForISLReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        req.verify();
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/signUpForISL",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);
        String jsonString = incResp.getEntity(String.class);
        SignUpForISLRes resp = new Gson().fromJson(jsonString, SignUpForISLRes.class);
        String event = req.getEvent();
        if (StringUtils.isEmpty(event)) {
            event = "ISL_2017";
        }
        switch (resp.getResult()) {
            case EMAILS_NOT_MATCHED:
                SignUpRes signUpRes = signUpUser(req, request, response, null);
                UserDetailsInfo newInfo = getUserDetailsForISLInfo(signUpRes.getUserId(), event);
                resp.setUserDetailsInfo(newInfo);
                resp.setUserId(signUpRes.getUserId());
                break;
            case PASSWORDS_NOT_MATCHED:
                break;
            case PASSWORDS_MATCHED:
            case PASSWORD_RESET_DONE:
                ExistingUserISLRegReq existingUserISLRegReq = new ExistingUserISLRegReq();
                existingUserISLRegReq.setUserId(resp.getUserId());
                existingUserISLRegReq.setStudentFirstName(req.getFirstName());
                existingUserISLRegReq.setStudentLastName(req.getLastName());
                existingUserISLRegReq.setStudentInfo(req.getStudentInfo());
                existingUserISLRegReq.setLocationInfo(req.getLocationInfo());
                existingUserISLRegReq.setUtm(req.getUtm());

                existingUserISLRegReq.setEvent(event);
                UserDetailsInfo info = _createUserDetailsForExistingUser(existingUserISLRegReq);
                resp.setUserDetailsInfo(info);
                //create session for the user
                sessionUtils.setCookieAndHeaders(request, response, getUserById(resp.getUserId()));
                break;
        }
        return resp;
    }

    public String getUsersRegisteredForISL(GetUsersReq req) throws VException {
        String queryParam = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsersRegisteredForISL?" + queryParam,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);
        return jsonString;
    }

    public UserDetailsInfo editUserDetailsForEvent(EditUserDetailsReq req) throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData == null || sessionData.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "not logged in");
        }
        if (Role.STUDENT.equals(sessionData.getRole())) {
            req.setUserId(sessionData.getUserId());
        }
        req.verify();
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/editUserDetailsForEvent",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);
        UserDetailsInfo response = gson.fromJson(jsonString, UserDetailsInfo.class);
        return response;
    }

    public void createISLUserDetailsFromList(CreateUserDetailsFromReq req) {
        if (ArrayUtils.isNotEmpty(req.getStudentList())) {
            for (ISLStudentData studentData : req.getStudentList()) {
                try {
                    _createISLUserDetailsFromList(studentData);
                } catch (Exception e) {
                    logger.error("error in createISLUserDetailsFromList " + studentData.getEmail() + ", Error " + e.getMessage());
                }

            }
        }
    }

    public void _createISLUserDetailsFromList(ISLStudentData studentData) throws VException,
            UnsupportedEncodingException,
            IOException,
            URISyntaxException {
        User user = getUserByEmail(studentData.getEmail());
        if (user == null) {
            SignUpReq req = new SignUpReq();
            req.setEmail(studentData.getEmail());
            req.setContactNumber(studentData.getParentPhoneNo());
            req.setPhoneCode(studentData.getParentPhoneCode());
            req.setFirstName(studentData.getStudentName());
            StudentInfo studentInfo = new StudentInfo();
            studentInfo.setGrade(studentData.getGrade());
            studentInfo.setSchool(studentData.getSchoolName());
            req.setStudentInfo(studentInfo);
            LocationInfo locationInfo = new LocationInfo();
            locationInfo.setCity(studentData.getCity());
            req.setLocationInfo(locationInfo);
            req.setAgentEmail(studentData.getAgentEmail());
            req.setAgentName(studentData.getAgentName());
            req.setSignUpFeature(FeatureSource.ISL_REGISTRATION_VIA_TOOLS);
            req.setUtm(studentData.getUtm());
            req.setSocialSource(SocialSource.VEDANTU.name());
            req.setRole(Role.STUDENT);
            req.setGender(Gender.UNKNOWN);
            signUpUser(req, null, null, null);
        } else {
            UserDetailsInfo userDetailsInfo = null;
            try {
                userDetailsInfo = getUserDetailsForISLInfo(user.getId(), "ISL_2017");
            } catch (Exception e) {
                if (!(e instanceof NotFoundException)) {
                    logger.error("Some error in fecthing user details of already registered user "
                            + user.getId() + ", Error " + e.getMessage());
                }
            }
            if (userDetailsInfo == null) {
                ExistingUserISLRegReq existingUserISLRegReq = new ExistingUserISLRegReq();
                existingUserISLRegReq.setUserId(user.getId());
                existingUserISLRegReq.setStudentFirstName(studentData.getStudentName());
                StudentInfo studentInfo = new StudentInfo();
                studentInfo.setGrade(studentData.getGrade());
                studentInfo.setSchool(studentData.getSchoolName());
                existingUserISLRegReq.setStudentInfo(studentInfo);
                LocationInfo locationInfo = new LocationInfo();
                locationInfo.setCity(studentData.getCity());
                existingUserISLRegReq.setLocationInfo(locationInfo);
                existingUserISLRegReq.setEvent("ISL_2017");
                existingUserISLRegReq.setFeatureSource(FeatureSource.ISL_REGISTRATION_VIA_TOOLS);
                existingUserISLRegReq.setUtm(studentData.getUtm());
                existingUserISLRegReq.setAgentEmail(studentData.getAgentEmail());
                existingUserISLRegReq.setAgentName(studentData.getAgentName());
                _createUserDetailsForExistingUser(existingUserISLRegReq);
            } else {
                //do nothing
            }
        }
    }

    public GetUsersRes getPasswordEmptyUsersForISL(Integer start, Integer size) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT
                + "/getPasswordEmptyUsersForISL?start="
                + start + "&size=" + size, HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson2 = gsonBuilder.create();
        GetUsersRes response = gson2.fromJson(jsonString, GetUsersRes.class);
        return response;
    }

    public PlatformBasicResponse generatePassword(GeneratePasswordReq req) throws VException, IOException {
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/generatePassword",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);
        String jsonString = incResp.getEntity(String.class);
        GeneratePasswordRes response = gson.fromJson(jsonString, GeneratePasswordRes.class);
        String contactNumber = response.getContactNumber();
        if (StringUtils.isNotEmpty(contactNumber)) {
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < contactNumber.length(); k++) {
                if (k < contactNumber.length() - 4) {
                    sb.append("x");
                } else {
                    sb.append(contactNumber.charAt(k));
                }
            }
            contactNumber = sb.toString();
        }
        return new PlatformBasicResponse(true, contactNumber, null);
    }

    public PlatformBasicResponse getUserISLStatus(String email, String event) throws VException, UnsupportedEncodingException {
        if (event == null) {
            event = "ISL_2017";
        }
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserISLStatus?event=" + event + "&email=" + URLEncoder.encode(email, "UTF-8"),
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);
        String jsonString = incResp.getEntity(String.class);
        return gson.fromJson(jsonString, PlatformBasicResponse.class);
    }

    public List<UserInfo> getUserInfosByEmail(GetUserInfosByEmailReq req) throws VException {
        if (ArrayUtils.isEmpty(req.getEmailIds())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No emaiIds found Found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserInfosByEmail?" + WebUtils.INSTANCE.createQueryStringOfObject(req),
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson _gson = gsonBuilder.create();
        Type listType1 = new TypeToken<ArrayList<UserInfo>>() {
        }.getType();
        List<UserInfo> responses = _gson.fromJson(jsonString, listType1);
        if (responses != null) {
            Set<Long> boardIds = new HashSet<>();
            for (UserInfo info : responses) {
                TeacherInfo tInfo = info.getTeacherInfo();
                if (tInfo != null) {
                    if (tInfo.getPrimarySubject() != null) {
                        boardIds.add(tInfo.getPrimarySubject());
                    }
                    if (tInfo.getSecondarySubject() != null) {
                        boardIds.add(tInfo.getSecondarySubject());
                    }
                }
                info.setContactNumber(null);
                info.setPhoneCode(null);
            }
            Map<Long, com.vedantu.board.pojo.Board> boardMap = boardDAO.getBoardsMapInBoardPojo(boardIds);

            for (UserInfo info : responses) {
                TeacherInfo tInfo = info.getTeacherInfo();
                if (tInfo != null) {
                    if (tInfo.getPrimarySubject() != null) {
                        info.setPrimaryBoard(boardMap.get(tInfo.getPrimarySubject()));
                    }
                    if (tInfo.getSecondarySubject() != null) {
                        info.setSecondaryBoard(boardMap.get(tInfo.getSecondarySubject()));
                    }
                }
            }
        }
        return responses;
    }


    public UserDetailsInfo getUserDetailsInfo(Long userId, String event) {

        String key = "PROD_USER_DETAILS_" + userId + "_EVENT_" + event;
        String keybasicInfo = "PROD_USER_BASIC_" + userId;
        try {
            String detailstring = redisDAO.get(key);
            if (StringUtils.isNotEmpty(detailstring)) {
                UserDetailsInfo info = gson.fromJson(detailstring, UserDetailsInfo.class);
                if (info != null) {
                    UserBasicInfo basicInfo = null;
                    String userString = redisDAO.get(keybasicInfo);
                    if (StringUtils.isNotEmpty(userString)) {
                        basicInfo = gson.fromJson(userString, UserBasicInfo.class);
                    } else {
                        basicInfo = fosUtils.getUserBasicInfo(userId, true);
                    }

                    info.setStudent(basicInfo);
                    return info;
                }
            }
        } catch (Exception e) {
            logger.info("Exception");
        }

        return null;
    }

    public void uploadUserResultS3(java.io.File file, Long userId, String category) throws VException {
        UploadFileSourceType uploadFileSourceType = UploadFileSourceType.VSAT_RESULT_SALES;
        String s3location = s3Manager.getS3Location(uploadFileSourceType);
        String key = String.valueOf(userId) + category;
        s3Manager.uploadFile(s3location, key, file);
    }

    public void vsatSalesSendResultEmail(Long userId, Integer score, Integer rank, Long duration, String category) throws IOException, MessagingException, VException, InterruptedException, JSONException {
        User user = getUserById(userId);

        String email = user.getEmail();
        String grade = user.getStudentInfo() != null ? user.getStudentInfo().getGrade() : "X";
        String name = user.getFullName();

        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(email));

        HashMap<String, Object> subjectScopes = new HashMap<>();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", name);
        bodyScopes.put("discount", getDiscount(rank));
        bodyScopes.put("score", score);
        bodyScopes.put("rank", String.valueOf(rank));
        bodyScopes.put("grade", grade);
        bodyScopes.put("duration", String.valueOf(duration/60000));

        EmailRequest emailRequest = new EmailRequest(toList, subjectScopes, bodyScopes, CommunicationType.VSAT_RESULT_MAIL_SALES, Role.STUDENT);

        UploadFileSourceType uploadFileSourceType = UploadFileSourceType.VSAT_RESULT_SALES;
        String s3location = s3Manager.getS3Location(uploadFileSourceType);

        String key = userId + category;
        emailRequest.setBucket(s3location);
        emailRequest.setKey(key);

        emailManager.sendEmail(emailRequest);

    }

    private String getDiscount(Integer rank) {
        if(rank == 1)
            return "100%";
        if(rank >=2 && rank <=3)
            return "90%";
        if(rank >=4 && rank <= 10)
            return "75%";
        if(rank >=11 && rank <= 20)
            return "50%";
        if(rank >=11 && rank <= 20)
            return "50%";
        if(rank >=21 && rank <= 100)
            return "40%";
        if(rank >=101 && rank <= 500)
            return "30%";
        if(rank >=501 && rank <= 1000)
            return "20%";
        else
            return "10%";
    }

    public User getUserByContactNumber(String contactNumber){
        logger.info("Getting Info for for phone : " + contactNumber);
        if(StringUtils.isEmpty(contactNumber))
        {
            return null;
        }

        String queryString = "?contactNumber=" + WebUtils.getUrlEncodedValue(contactNumber);
        String url = USER_ENDPOINT + "/getUsersByContactNumber" + queryString;

        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);

        User user = null;

        if (!StringUtils.isEmpty(output)) {
            try {
                if (response.getStatus() != 200) {
                    throw new Exception("getUserBasicInfoByEmail: getting Users' BasicInfo failed");
                }

                Type _type = new TypeToken<List<User>>() {
                }.getType();
                List<User> users = gson.fromJson(output, _type);

                if(CollectionUtils.isNotEmpty(users))
                    user = users.get(0);

            } catch (Exception ex) {
                logger.info("Exception while parsing userBasicInfos", ex);
            }
        }

        return user;
    }
}
