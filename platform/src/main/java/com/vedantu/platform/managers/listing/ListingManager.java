package com.vedantu.platform.managers.listing;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.GetUsersReq;
import com.vedantu.User.response.GetUsersRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import java.util.List;

import com.vedantu.platform.listing.dao.entity.FeatureMapping;
import com.vedantu.platform.listing.manager.FeatureMappingManager;
import com.vedantu.platform.listing.viewobject.request.GetTeacherReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.listing.pojo.GetTeacherResponse;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.ESManager;
import com.vedantu.platform.pojo.es.SearchFilter2;
import com.vedantu.platform.pojo.es.SearchSortReq;
import com.vedantu.platform.dao.listing.ListingDailyCountDao;
import com.vedantu.platform.mongodbentities.listing.ListingDailyCount;
import com.vedantu.platform.managers.offering.OfferingManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;

@Service
public class ListingManager {

    @Autowired
    ListingDailyCountDao listingDailyCountDao;

    @Autowired
    BoardManager boardManager;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @Autowired
    OfferingManager offeringManager;

    @Autowired
    UserManager userManager;

    @Autowired
    ESManager eSManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FeatureMappingManager featureMappingManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ListingManager.class);

    @Async
    public void updateFaceCount(List<EsTeacherData> teachers) {

        for (EsTeacherData esTeacherData : teachers) {
            if (esTeacherData.getShownToday() != null) {
                ListingDailyCount listingDailyCount = new ListingDailyCount(System.currentTimeMillis(), esTeacherData.getTeacherId(), esTeacherData.getShownToday());
                listingDailyCountDao.create(listingDailyCount);
            }
        }
    }

    public String getTeachers(String boardId, String grade, String target, String subject,
            String filtersJson, String sortingParams, String startString, String sizeString, Long offeringId) throws Exception {
        String reqPojo = getTeachersPojo(boardId, grade, target, subject, filtersJson, sortingParams, startString,
                sizeString, offeringId);
        String jsonString = getTeachers(reqPojo);

        GetTeacherResponse response = new Gson().fromJson(jsonString, GetTeacherResponse.class);

//        Map<String, Object> payload = new HashMap<>();
//        payload.put("getTeacherResponse", response);
//        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_LISTING_FACE_COUNT_ADDITION, payload);
//        asyncTaskFactory.executeTask(params);
        return jsonString;
    }

    public String getTeachers(String reqPojo) throws Exception {
        GetTeacherReq getTeacherReq = new Gson().fromJson(reqPojo, GetTeacherReq.class);
        return new Gson().toJson(featureMappingManager.getTeachers("3", getTeacherReq));
    }

    public String getTeachersPojo(String boardId, String grade, String target, String subject,
            String filtersJson, String sortingParams, String startString, String sizeString, Long offeringId)
            throws IllegalAccessException, VException {

        String request = "{";
        String functions = "\"functions\":{\"name\": \"none\",\"value\":[{\"type\": \"script\"}]}";
        String filterString = "\"filter\":{\"type\": \"bool\",\"params\": [{\"name\": \"must\",\"value\": [";
        filterString += createTermFilter("active", true, "boolean");

        //String boardFilter = createHasChildFilter("boardIds", boardId, "teacherBoardMappings", "list");
        //String gradeFilter = createHasChildFilter("grade", grade, "teacherBoardMappings", "int");
        //String targetFilter = createHasChildFilter("category", target, "teacherBoardMappings", "string");
        String boardFilter = "";
        String gradeFilter = "";
        String targetFilter = "";
        String filterStringTemp = "";

        int start;
        int size;

        if (StringUtils.isNotEmpty(boardId)) {
            boardFilter = createTermFilter("boardIds", boardId, "string");
            filterStringTemp += boardFilter;
        } else if (StringUtils.isNotEmpty(subject)) {
            subject = subject.toLowerCase();
            Board board = boardManager.getBoardBySlug(subject);
            if (board != null) {
                boardFilter = createTermFilter("boardIds", board.getId(), "string");
//					boardFilter = createHasChildFilter("boardIds", board.getId(), "teacherBoardMappings", "list");
                filterStringTemp += boardFilter;
            }
        }
        if (StringUtils.isNotEmpty(grade))  {
            gradeFilter = createTermFilter("grade", grade, "int");
            if (filterStringTemp.isEmpty()) {
                filterStringTemp += gradeFilter;
            } else {
                filterStringTemp += "," + gradeFilter;
            }

        }
        if (StringUtils.isNotEmpty(target)) {
            target = target.toLowerCase();
            targetFilter = createTermFilter("category", target, "string");
            if (filterStringTemp.isEmpty()) {
                filterStringTemp += targetFilter;
            } else {
                filterStringTemp += "," + targetFilter;
            }
        }

        if (!filterStringTemp.isEmpty()) {
            filterString += "," + createHasChildFilter(filterStringTemp, "teacherBoardMappings", "string");
        }

        logger.info("filter temp string:" + filterStringTemp);

        Type filterListType = new TypeToken<List<SearchFilter2>>() {
        }.getType();
        List<SearchFilter2> filters = new ArrayList<>();
        if (!StringUtils.isEmpty(filtersJson)) {
            filters = new Gson().fromJson(filtersJson, filterListType);

            for (SearchFilter2 filter : filters) {
                switch (filter.getFilterType()) {
                    case "range":
                        if (filter.getfilterParams().length == 0) {
                        } else if (filter.getFilterName().equals("availability")) {
                        } else if ( StringUtils.isNotEmpty(filter.getFilterName()) && StringUtils.isNotEmpty(filter.getfilterParams()[0])
                                && StringUtils.isNotEmpty(filter.getfilterParams()[1])) {
                            String rangeFilter = createRangeFilter(filter.getFilterName(),
                                    filter.getfilterParams()[0], filter.getfilterParams()[1]);
                            filterString += "," + rangeFilter;
                        }
                        break;
                    case "term":
                        if (filter.getfilterParams() != null && filter.getfilterParams().length > 0) {
                            if (StringUtils.isNotEmpty(filter.getFilterName()) && StringUtils.isNotEmpty(filter.getfilterParams()[0])) {
                                String termFilter = createTermFilter(filter.getFilterName(), filter.getfilterParams()[0],
                                        filter.getFilterValueType());
                                filterString += "," + termFilter;
                            }
                        }
                        break;
                    case "terms":
                        if (filter.getfilterParams() != null && filter.getfilterParams().length > 0) {
                            if (StringUtils.isNotEmpty(filter.getFilterName()) 
                                    && StringUtils.isNotEmpty(filter.getfilterParams()[0])) {
                                String termsFilter = createTermsFilter(filter.getFilterName(), filter.getfilterParams(),
                                        filter.getFilterValueType());
                                filterString += "," + termsFilter;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        filterString += "]}]}";
        logger.info(filterString);

        String sort = "\"sort\":{\"params\":[";
        Type sortListType = new TypeToken<List<SearchSortReq>>() {
        }.getType();
        List<SearchSortReq> sortParams = new ArrayList<>();
        if (!StringUtils.isEmpty(sortingParams)) {
            sortParams = new Gson().fromJson(sortingParams, sortListType);
        }

        for (SearchSortReq sortParam : sortParams) {

            if (sortParam.getSortField().equals("rating")) {
                sort += "\"rating\"";
            }
            if (sortParam.getSortField().equals("sessions")) {
                sort += "\"sessions\"";
            }
        }
        sort += "]}";

        String fromsize = null;
        if (startString != null) {
            start = Integer.parseInt(startString);
            fromsize = "\"from\":" + start;
        }
        if (sizeString != null) {
            size = Integer.parseInt(sizeString);
            if (fromsize != null) {
                fromsize += ",\"size\":" + size;
            } else {
                fromsize += "\"size\":" + size;
            }
        }

        if (fromsize != null) {
            request = "{" + functions + "," + filterString + "," + sort + "," + fromsize;
        } else {
            request = "{" + functions + "," + filterString + "," + sort;
        }

        if (offeringId != null) {
            List<Long> teacherIds = offeringManager
                    .getOfferingTeacherIds(offeringId);
            String offeringIdString = ",\"offeringId\":\"" + offeringId + "\"";
            request += offeringIdString;
            if (teacherIds != null) {
                String teacherIdsString = new Gson().toJson(teacherIds);
                teacherIdsString = ",\"teacherIds\":" + teacherIdsString;
                request += teacherIdsString;
            }
        }

        request += "}";
        logger.info(request);
        return request;
    }

    private String createHasChildFilter(String filters, String childType, String filterValueType) {

        String filter;
        //if (filterValueType != null && filterValueType.equals("string")) {
        filter = "{\"type\": \"hasChild\",\"params\": [{\"name\": \"type\",\"value\": [\"%s\"]},{\"name\": \"filter\",\"value\": [{\"type\": \"bool\",\"params\": [{\"name\": \"must\",\"value\": [%s]}]}]}]}";
        //} else {
        //	filter = "{\"type\": \"hasChild\",\"params\": [{\"name\": \"type\",\"value\": [\"%s\"]},{\"name\": \"filter\",\"value\": [{\"type\": \"bool\",\"params\": [{\"name\": \"must\",\"value\": [{\"type\": \"term\",\"params\": [{\"name\": \"%s\",\"value\": [%s]}]}]}]}]}]}";
        //}
        filter = String.format(filter, childType, filters);
        return filter;
    }

    private String createTermFilter(String name, Object value, String filterValueType) {
        String filter = "";
        if (filterValueType != null && filterValueType.equals("string")) {
            filter = "{\"type\": \"term\",\"params\": [{\"name\": \"%s\",\"value\": [\"%s\"]}]}";
        } else {
            filter = "{\"type\": \"term\",\"params\": [{\"name\": \"%s\",\"value\": [%s]}]}";
        }
        filter = String.format(filter, name, value);
        return filter;
    }

    public String createTermsFilter(String name, Object[] value, String filterValueType) {
        String filter = "";
        if (filterValueType != null && filterValueType.equals("string")) {
            filter = "{\"type\": \"terms\",\"params\": [{\"name\": \"%s\",\"value\": \"%s\"}]}";
        } else {
            filter = "{\"type\": \"terms\",\"params\": [{\"name\": \"%s\",\"value\": %s}]}";
        }
        filter = String.format(filter, name, Arrays.toString(value));
        return filter;
    }

    public String createRangeFilter(String name, Object from, Object to) {
        String filter = "{\"type\": \"range\",\"params\": [{\"name\": \"name\",\"value\": [\"%s\"]},{\"name\": \"gte\",\"value\": [%s]},{\"name\": \"lte\",\"value\": [%s]}]}";
        filter = String.format(filter, name, from, to);
        return filter;
    }

    public String createHasChildFilter(String name, Object value, String childType, String filterValueType) {

        String filter = "";
        if (filterValueType != null && filterValueType.equals("string")) {
            filter = "{\"type\": \"hasChild\",\"params\": [{\"name\": \"type\",\"value\": [\"%s\"]},{\"name\": \"filter\",\"value\": [{\"type\": \"bool\",\"params\": [{\"name\": \"must\",\"value\": [{\"type\": \"term\",\"params\": [{\"name\": \"%s\",\"value\": [\"%s\"]}]}]}]}]}]}";
        } else {
            filter = "{\"type\": \"hasChild\",\"params\": [{\"name\": \"type\",\"value\": [\"%s\"]},{\"name\": \"filter\",\"value\": [{\"type\": \"bool\",\"params\": [{\"name\": \"must\",\"value\": [{\"type\": \"term\",\"params\": [{\"name\": \"%s\",\"value\": [%s]}]}]}]}]}]}";
        }
        filter = String.format(filter, childType, name, value);
        return filter;
    }

    public void filtertosteachers() {
        Map<String, Object> payload = new HashMap<>();
        payload.put("start", 0);
        payload.put("limit", 10);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_TOS_DATA, payload);
        asyncTaskFactory.executeTask(params);
    }

    static long LAST_LOGGED_TIME = 0l;
    static long DIFF_BETWEEN_ERROR_LOGGING=2*3600000;//2hrs
    
    public void updateTOSData(Integer start, Integer limit) throws VException {
        GetUsersReq req = new GetUsersReq();
        req.setRole(Role.TEACHER);
        req.setStart(start);
        req.setSize(limit);
        List<UserInfo> users;
        GetUsersRes res = userManager.getUsers(req);
        if (res != null && res.getUsers() != null) {
            users = res.getUsers();
        } else {
            return;
        }
        for (UserInfo user : users) {
            try {
                eSManager.updateTruerAvailableTime(user.getUserId());
            } catch (Exception e) {
                if (System.currentTimeMillis() - LAST_LOGGED_TIME > DIFF_BETWEEN_ERROR_LOGGING) {
                    logger.error(e);
                    LAST_LOGGED_TIME = System.currentTimeMillis();
                }
            }
        }
        start = start + limit;
        logger.info("next task " + start);
        if (users.size() > 0) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("start", start);
            payload.put("limit", limit);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_TOS_DATA, payload);
            asyncTaskFactory.executeTask(params);
        }

    }

    public void updateDailyCount() throws VException {
        GetUsersReq req = new GetUsersReq();
        req.setRole(Role.TEACHER);
        List<UserInfo> users = new ArrayList<>();
        GetUsersRes res = userManager.getUsers(req);
        if (res != null) {
            users = res.getUsers();
        }
        for (UserInfo user : users) {
            try {
                eSManager.dailyListingCountUpdate(user.getUserId());
            } catch (Exception e) {
                logger.error(e);
            }
        }
    }

}
