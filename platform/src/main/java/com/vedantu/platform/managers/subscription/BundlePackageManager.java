package com.vedantu.platform.managers.subscription;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.request.AddCouponRedeemEntryReq;
import com.vedantu.dinero.request.ValidateCouponReq;
import com.vedantu.dinero.response.RedeemCouponInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.pojo.EnrollmentUpdatePojo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.dinero.CouponManager;
import com.vedantu.platform.response.dinero.ValidateCouponRes;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.scheduling.pojo.session.ReferenceType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.ProcessPaymentReq;
import com.vedantu.dinero.request.UpdateDeliverableDataReq;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.pojo.referral.request.ProcessReferralBonusReq;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.pojo.BundlePricingPojo;
import com.vedantu.subscription.pojo.EntityInfo;
import com.vedantu.subscription.response.PurchaseBundleRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.dozer.DozerBeanMapper;

/**
 * Created by somil on 10/05/17.
 */
@Service
public class BundlePackageManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(BundlePackageManager.class);

    @Autowired
    private DozerBeanMapper mapper; 
    private final Gson gson = new Gson();

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private final String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    @Deprecated
    public BundlePackageInfo addEditBundlePackage(AddEditBundlePackageReq addEditBundleReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/addEditBundlePackage", HttpMethod.POST, new Gson().toJson(addEditBundleReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundlePackageInfo bundlePackageInfo = new Gson().fromJson(jsonString, BundlePackageInfo.class);
        return bundlePackageInfo;
    }

    public BundlePackageInfo getBundlePackage(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/" + id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundlePackageInfo bundlePackageInfo = new Gson().fromJson(jsonString, BundlePackageInfo.class);
        return bundlePackageInfo;
    }

    public List<BundlePackageInfo> getBundlePackageInfos(GetBundlePackagesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/get/public?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundlePackageInfo>>() {
        }.getType();
        List<BundlePackageInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }

    public BundlePackageDetailsInfo getBundlePackageDetails(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/getBundlePackageDetails/" + id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundlePackageDetailsInfo bundlePackageInfo = new Gson().fromJson(jsonString, BundlePackageDetailsInfo.class);
        return bundlePackageInfo;
    }

    @Deprecated
    public List<BundlePackageDetailsInfo> getBundlePackageDetailsList(GetBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/getBundlePackageDetailsList?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundlePackageDetailsInfo>>() {
        }.getType();
        List<BundlePackageDetailsInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }

    public List<BundleEnrolmentInfo> enrolViaPass(EnrolViaPassReq req) throws VException {

        ValidateCouponReq validateCouponReq = new ValidateCouponReq();
        validateCouponReq.setCode(req.getPassCode());
        validateCouponReq.setEntityId(req.getEntityId());
        validateCouponReq.setEntityType(EntityType.BUNDLE_PACKAGE);
        validateCouponReq.setCallingUserId(req.getUserId());
        validateCouponReq.setCallingUserRole(req.getCallingUserRole());
        ValidateCouponRes validateCouponRes = couponManager.checkValidity(validateCouponReq);
        Long expirationTime = null;
        if (validateCouponRes.getPassDurationInMillis() != null) {
            expirationTime = System.currentTimeMillis() + validateCouponRes.getPassDurationInMillis();
        }

        BundleCreateEnrolmentReq enrolmentReq = new BundleCreateEnrolmentReq();
        enrolmentReq.setEntityId(req.getEntityId());
        enrolmentReq.setUserId(req.getUserId().toString());
        enrolmentReq.setState(EnrollmentState.TRIAL);
        List<BundleEnrolmentInfo> bundleEnrolmentInfos = createEnrolment(enrolmentReq);
        List<ReferenceTag> referenceTags = new ArrayList<>();
        for (BundleEnrolmentInfo bundleEnrolmentInfo : bundleEnrolmentInfos) {
            ReferenceTag referenceTag = new ReferenceTag(ReferenceType.BUNDLE_ENROLMENT, bundleEnrolmentInfo.getId());
            referenceTags.add(referenceTag);
            bundleEnrolmentInfo.setPassExpirationTime(expirationTime);
        }

        AddCouponRedeemEntryReq addCouponRedeemEntryReq = new AddCouponRedeemEntryReq();
        addCouponRedeemEntryReq.setEntityId(req.getEntityId());
        addCouponRedeemEntryReq.setEntityType(EntityType.BUNDLE_PACKAGE);
        addCouponRedeemEntryReq.setPassCode(req.getPassCode());
        addCouponRedeemEntryReq.setState(RedeemedCouponState.PROCESSED);
        addCouponRedeemEntryReq.setUserId(req.getUserId());
        addCouponRedeemEntryReq.setPassProcessingState(PassProcessingState.VALID);
        addCouponRedeemEntryReq.setPassExpirationTime(expirationTime);
        addCouponRedeemEntryReq.setReferenceTags(referenceTags);
        RedeemCouponInfo redeemCouponInfo = couponManager.addRedeemEntry(addCouponRedeemEntryReq);

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", req.getUserId());
        payload.put("entityId", req.getEntityId());
        payload.put("entityType", EntityType.BUNDLE_PACKAGE);
        payload.put("redeemCouponInfo", redeemCouponInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BUNDLE_ENROLLMENT_PASS_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        return bundleEnrolmentInfos;

    }
    
    public PlatformBasicResponse checkForBundleEnrollment(String bundleId, String userId) throws VException{
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/checkForEnrollmentInBundleBatch?bundleId=" + bundleId + "&userId=" + userId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);       
        PlatformBasicResponse platformBasicResponse = gson.fromJson(jsonString, PlatformBasicResponse.class);
        return platformBasicResponse;
    }

    public PurchaseBundleRes purchaseBundlePackage(PurchaseBundlePackageReq req) throws VException, CloneNotSupportedException {
        req.verify();
        String bundlePackageId = req.getBundlePackageId();

        logger.info("finding the amount to pay");
        BundlePackageInfo bundlePackageInfo = getBundlePackage(bundlePackageId);
        if (bundlePackageInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }
        
        PlatformBasicResponse platformBasicResponse = checkForBundleEnrollment(bundlePackageId, req.getUserId().toString());
        if(!platformBasicResponse.isSuccess()){
            if(StringUtils.isNotEmpty(platformBasicResponse.getErrorString()) && ErrorCode.ALREADY_ENROLLED.toString().equals(platformBasicResponse.getErrorString())){
                throw new ConflictException(ErrorCode.ALREADY_ENROLLED, "enrollments for one of the batch already exists");
            }else if (StringUtils.isNotEmpty(platformBasicResponse.getErrorString()) && ErrorCode.BATCH_ALREADY_FULL.toString().equals(platformBasicResponse.getErrorString())){
                throw new ForbiddenException(ErrorCode.BATCH_ALREADY_FULL, "one of the batch already full");
            }
        }
        
        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        int amountToPay = bundlePackageInfo.getPrice();
        if (ArrayUtils.isNotEmpty(req.getPurchasingEntities())) {
            List<BundlePricingPojo> pricing = bundlePackageInfo.getPricing();
            for (BundlePricingPojo pricingPojo : pricing) {
                if (pricingPojo.getItemCount() == req.getPurchasingEntities().size()) {
                    amountToPay = pricingPojo.getPrice();
                    break;
                }
            }
        } else {
            req.setPurchasingEntities(new ArrayList<>());
            if (ArrayUtils.isNotEmpty(bundlePackageInfo.getEntities())) {
                for (EntityInfo entityInfo : bundlePackageInfo.getEntities()) {
                    req.getPurchasingEntities().add(new PurchasingEntity(entityInfo.getEntityType(), entityInfo.getEntityId()));
                }
            }
        }

        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(bundlePackageId);
        buyItemsReqNew.setEntityType(EntityType.BUNDLE_PACKAGE);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setPaymentType(PaymentType.BULK);
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setPurchasingEntities(req.getPurchasingEntities());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
        
        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        if (!orderInfo.getNeedRecharge()) {
            processBundlePurchaseAfterPayment(orderInfo.getId(), orderInfo.getUserId(),
                    bundlePackageInfo, req.getPurchasingEntities());
        }
        return mapper.map(orderInfo, PurchaseBundleRes.class);
    }

    public void processBundlePurchaseAfterPayment(String orderId, Long userId,
            BundlePackageInfo bundlePackageInfo, List<PurchasingEntity> entitiesToBuy)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);
        UpdateDeliverableDataReq updateDeliverableDataReq = new UpdateDeliverableDataReq();
        updateDeliverableDataReq.setOrderId(orderId);
        if (ArrayUtils.isNotEmpty(entitiesToBuy)) {
            logger.info("creating enrollments for purchasing entities ");
            int successCount = 0;
            for (PurchasingEntity purchasingEntity : entitiesToBuy) {
                try {
                    if (EntityType.OTF.equals(purchasingEntity.getEntityType())) {
                        OTFEnrollmentReq oTFEnrollmentReq = new OTFEnrollmentReq(userId.toString(),
                                purchasingEntity.getEntityId(), null, null, Role.STUDENT, "ACTIVE", null, EnrollmentState.REGULAR);
                        //let this enrollment have entitytype OTF_COURSE
//                        oTFEnrollmentReq.setEntityId(bundlePackageInfo.getId());
//                        oTFEnrollmentReq.setEntityTitle(bundlePackageInfo.getTitle());
//                        oTFEnrollmentReq.setEntityType(EntityType.BUNDLE_PACKAGE);
                        EnrollmentPojo oTFEnrollmentInfo = enrollmentManager.enrollment(oTFEnrollmentReq);
                        logger.info("Enrollment res " + oTFEnrollmentInfo);
                        successCount++;
                        updateDeliverableDataReq.addDeliverableId(oTFEnrollmentInfo.getId());
                        updateDeliverableDataReq.addPurchasingEntityId(purchasingEntity.getEntityId());
                        updateDeliverableDataReq.addEntityDeliverableMap(purchasingEntity.getEntityId(),oTFEnrollmentInfo.getId());
                        createGttAttendeeDetails(oTFEnrollmentInfo);
                    } else {
                        logger.error("Bundlepackage api is being used for " + purchasingEntity.getEntityType() + ", " + purchasingEntity.getEntityId());
                    }
                } catch (VException e) {
                    logger.error("error in creating enrollments " + e.getMessage());
                }
            }
            if (successCount == 0) {
                paymentManager.reverseOrderTransaction(orderId);
                throw new ConflictException(ErrorCode.ENROLLMENT_FAILED, "enrollment of all purchasingEntities failed");
            }
        } else {
            logger.error("processBundlePurchaseAfterPayment for empty PurchasingEntity,  orderId " + orderId);
        }

        if (!updateDeliverableDataReq.getDeliverableIds().isEmpty()) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("updateDeliverableDataReq", updateDeliverableDataReq);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_ORDER_DELIVERABLE_DATA, payload);
            asyncTaskFactory.executeTask(params);
        }
        res.setId(orderId);
        logger.info(res);
        
        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", userId);
        payload.put("bundlePackageInfo", bundlePackageInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POST_BUNDLE_PURCHASE_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        ProcessReferralBonusReq referralReq = new ProcessReferralBonusReq();
        referralReq.setUserId(userId);
        referralReq.setReferralStep(ReferralStep.NEW_BUNDLE_PURCHASE);
        referralReq.setEntityId(bundlePackageInfo.getId());
        awsSNSManager.triggerSNS(SNSTopic.ENROLL_BUNDLE_REFERRAL, "ENROLL_BUNDLE_REFERRAL", gson.toJson(referralReq));
        
        createEnrollmentConsumption(res);

    }

    public void createEnrollmentConsumption(OrderInfo orderInfo) throws VException{
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundlePackage/processBundlePurchaseAfterPayment",
                HttpMethod.POST, new Gson().toJson(orderInfo));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response String: "+jsonString);
    }
    
    public List<BundleEnrolmentInfo> createEnrolment(BundleCreateEnrolmentReq req) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundlePackage/createEnrolments",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        Type listType1 = new TypeToken<ArrayList<BundleEnrolmentInfo>>() {
        }.getType();
        List<BundlePackageInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        List<BundleEnrolmentInfo> bundleEnrolmentInfos = new Gson().fromJson(jsonString, listType1);
        return bundleEnrolmentInfos;
    }

    public List<BundlePackageInfo> getBundlesForEntity(GetBundlePackagesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/bundlePackage/getBundlesForEntity?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundlePackageInfo>>() {
        }.getType();
        List<BundlePackageInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }


    public void createGttAttendeeDetails(EnrollmentPojo enrollment) throws VException{
        EnrollmentUpdatePojo req = new EnrollmentUpdatePojo();
        req.setNewEnrollment(enrollment);
        String url = schedulingEndpoint + "/onetofew/session/createAsyncGttAttendeeTask";
        ClientResponse response = WebUtils.INSTANCE.doCall(url,
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
    }
}
