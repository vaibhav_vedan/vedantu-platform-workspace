/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.user;

import com.vedantu.User.request.SocialUserInfo;
import java.util.List;
import org.apache.http.NameValuePair;

/**
 *
 * @author somil
 */
public interface ISocialManager {
	public String getClientId();

	public String getClientSecret();

	public String getAuthUrl();

	public String getRedirectUrl();
	
	public String getTokenUrl(List<NameValuePair> httpParams);

	public String getUserInfoUrl(String access_token);

	public String getAuthorizeUrl(String generatedString);

	public SocialUserInfo getUserInfo(String code);

        public SocialUserInfo getUserInfoUsingToken(String code);
}
