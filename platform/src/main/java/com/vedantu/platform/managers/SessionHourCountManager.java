package com.vedantu.platform.managers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.SessionHourCountDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.mongodbentities.SessionHourCount;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;

@Service
public class SessionHourCountManager {

	@Autowired
	private OTFManager otfManager;

	@Autowired
	private SessionManager sessionManager;

	@Autowired
	private SessionHourCountDAO sessionHourCountDAO;

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionHourCountManager.class);

	public long getTotalSessionDuration(boolean reload) throws InternalServerErrorException, BadRequestException {
		String hourCount = redisDAO.get(SessionManager.getCacheKey());
		if (reload || StringUtils.isEmpty(hourCount)) {
			updateSessionHourCount();
			hourCount = redisDAO.get(SessionManager.getCacheKey());
		}

		return Long.parseLong(hourCount);
	}

	public void updateSessionHourCount() {
		long currentTime = System.currentTimeMillis();
		SessionHourCount sessionHourCount = getLatestHourCountEntry();

		long lastUpdatedOtoHours = 0l;
		long lastUpdatedOtfHours = 0l;
		long lastUpdatedValue = 0l;
		if (sessionHourCount == null) {
			/*
			 * #Session count config session.duration.last.count.hours.value=1000
			 * session.duration.last.count.date=1496860200000
			 */
			lastUpdatedOtoHours = ConfigUtils.INSTANCE.getLongValue("session.duration.last.count.hours.value.oto")*DateTimeUtils.MILLIS_PER_HOUR;
			lastUpdatedOtfHours = ConfigUtils.INSTANCE.getLongValue("session.duration.last.count.hours.value.otf")*DateTimeUtils.MILLIS_PER_HOUR;
			lastUpdatedValue = ConfigUtils.INSTANCE.getLongValue("session.duration.last.count.date");

			// OTF - Fetch from last updated value
		} else {
			lastUpdatedOtoHours = sessionHourCount.getOtoHourCount();
			lastUpdatedOtfHours = sessionHourCount.getOtfHourCount();
			lastUpdatedValue = sessionHourCount.getEndTime();
		}

		try {
			long otoHours = sessionManager.getOTOTotalSessionHourDuration(lastUpdatedValue, currentTime);
			long otfHours = otfManager.getEndedSessionDuration(lastUpdatedValue, currentTime);
			sessionHourCount = new SessionHourCount(currentTime, lastUpdatedOtoHours + otoHours,
					lastUpdatedOtfHours + otfHours);
			sessionHourCountDAO.create(sessionHourCount);
			logger.info("Entry created:" + sessionHourCount.toString());
			redisDAO.set(SessionManager.getCacheKey(), String.valueOf(sessionHourCount.getTotalHourCount()));
		} catch (VException ex) {
			logger.error("Error calculating total hours ex:" + ex.getMessage());
		}

	}

	/*
	 * Long duration = 0L; String value = redisDAO.get(getCacheKey()); if (value !=
	 * null && !Boolean.TRUE.equals(reloadCache)) { return Long.parseLong(value); }
	 * 
	 * Long lastUpdatedTime = 0L; Long previousSessionCountInHours = 0L;
	 * 
	 * String lastUpdatedTimeString =
	 * keyValuePairDAO.getByKey("session.duration.last.count.date"); String
	 * previousSessionCountInHoursString =
	 * keyValuePairDAO.getByKey("session.duration.last.count.hours.value");
	 * 
	 * if (StringUtils.isNotEmpty(lastUpdatedTimeString) &&
	 * StringUtils.isNotEmpty(previousSessionCountInHoursString)) { lastUpdatedTime
	 * = Long.parseLong(lastUpdatedTimeString); previousSessionCountInHours =
	 * Long.parseLong(previousSessionCountInHoursString); }
	 * 
	 * logger.info("previousSessionCountInHours: " + previousSessionCountInHours);
	 * String getTotalSessionDurationUrl = SCHEDULING_ENDPOINT +
	 * ConfigUtils.INSTANCE.getStringValue(
	 * "scheduling.api.session.getTotalSessionDuration") + "?lastUpdatedTime=" +
	 * lastUpdatedTime; ClientResponse resp =
	 * WebUtils.INSTANCE.doCall(getTotalSessionDurationUrl, HttpMethod.GET, null);
	 * VExceptionFactory.INSTANCE.parseAndThrowException(resp); String jsonString =
	 * resp.getEntity(String.class); logger.info("calculatedDuration: " +
	 * jsonString); Long calculatedDuration = Long.parseLong(jsonString);
	 * 
	 * Long otfDuration = otfManager.getEndedSessionDuration(lastUpdatedTime);
	 * logger.info("otfDuration: " + otfDuration);
	 * 
	 * duration += previousSessionCountInHours * DateTimeUtils.MILLIS_PER_HOUR;
	 * duration += calculatedDuration;
	 * 
	 * if (otfDuration != null) { duration += otfDuration; }
	 * 
	 * logger.info("value in cache: " + value); logger.info("value calculated: " +
	 * duration); redisDAO.set(getCacheKey(), duration.toString()); return duration;
	 * 
	 */
	public SessionHourCount getLatestHourCountEntry() {
		Query query = new Query();
		query.with(Sort.by(Direction.DESC, SessionHourCount.Constants.END_TIME));
		query.limit(1);
		List<SessionHourCount> entries = sessionHourCountDAO.runQuery(query, SessionHourCount.class);

		if (!CollectionUtils.isEmpty(entries)) {
			return entries.get(0);
		} else {
			return null;
		}
	}
}
