package com.vedantu.platform.managers.listing;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.platform.dao.ESConfig;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service
public class ListingOffsetManager {

	@Autowired
	LogFactory logFactory;

	@Autowired
	ESConfig esConfig;

	

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(ListingOffsetManager.class);

	@Async
	public void uploadToElasticSearch(File file) throws VException {

		try {
			// Create Workbook instance holding reference to .xlsx file
			Workbook workbook = WorkbookFactory.create(file);
			Sheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			
			Long weight = Long.MAX_VALUE/100000000000l;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				weight -= 1000;
//				logger.info(row.getCell(0).getNumericCellValue());
				int cellType = row.getCell(0).getCellType();
				Long cellValue;
				if(cellType==1){
					cellValue =new BigDecimal(row.getCell(0).getStringCellValue()).longValue();
				}else{
					Double cellValueDouble  = row.getCell(0).getNumericCellValue();
					cellValue = cellValueDouble.longValue();
				}
				updateElasticSearch(cellValue, weight.longValue(), false);

			}
		} catch (IOException | InvalidFormatException e) {
			logger.error(e.getMessage());
		}
	}

	@Async
	public void resetOffset() throws VException {

		updateElasticSearch(null, null, true);

	}

	private void updateElasticSearch(Long teacherId, Long scoreOffset,
			Boolean updateByQuery) throws VException {
		UpdateResponse updateResponse = null;
		logger.info("Request received for updating teacherId: " + teacherId + "\tscoreOffset: "
                        + scoreOffset+", updateByQuery"+updateByQuery);

		try {
			Client client = esConfig.getTransportClient();

			if (updateByQuery) {
				String url = ConfigUtils.INSTANCE
						.getStringValue("elasticsearch.baseurl.scheduling")
						+ esConfig.getIndex()
						+ "/"
						+ esConfig.getType()
						+ "/_update_by_query";
				String query = "{\"script\": \"ctx._source.scoreOffset=0\",\"query\" : { \"match_all\": {} }}";
				ClientResponse response=WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query);
                                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                                String jsonResp = response.getEntity(String.class);                                
                                logger.info("Response for updatebyquery "+jsonResp);
			} else {
				if (teacherId != null && scoreOffset != null) {
					UpdateRequest updateRequest = new UpdateRequest(
							esConfig.getIndex(), esConfig.getType(),
							teacherId.longValue() + "")
							.script("ctx._source.scoreOffset = " + scoreOffset);
//					String url = configUtils
//							.getStringValue("elasticsearch.baseurl.scheduling")
//							+ esConfig.getIndex()
//							+ "/"
//							+ esConfig.getType()
//							+"/" + teacherId
//							+ "/_update";
//					logger.info(url);
//					String query = "{\"script\": \"ctx._source.scoreOffset=" + scoreOffset + "\",\"query\" : { \"match_all\": {} }}";
//					ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query);
					logger.info(updateRequest.toString());
					
					updateResponse = client.update(updateRequest).get();
					logger.info(updateResponse);
//					if (response == null || response.getStatus()!=200) {
//						throw new VException(ErrorCode.SERVICE_ERROR.name(),
//								"failed to update es for id: " + teacherId);
//					}
				}
			}
		} catch ( InterruptedException | ExecutionException e) {
			logger.error(e.getMessage());
		}
	}
}
