/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.filemgmt;

/**
 *
 * @author somil
 */
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.vedantu.platform.mongodbentities.filemgmt.File;
import com.vedantu.platform.request.filemgmt.FileACL;
import com.vedantu.platform.request.filemgmt.UploadSignature;
import com.vedantu.util.ConfigUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// google client id password == "notasecret"
// live example of AWS and GCS at http://blog.appharbor.com/2013/01/10/asynchronous-browser-uploads-to-s3-and-gcs-using-cors-aspnet-mvc
@Service
public class GCSAppIdentityServiceUrlSigner {

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GCSAppIdentityServiceUrlSigner.class);

    private static final String BASE_URL = "https://storage.googleapis.com";
    private String clientId;
    private String secretKeyFileName;
    private String bucket;
    private int expirationMinutes;
    private PrivateKey privateKey;
    private Storage storage = null;

    public GCSAppIdentityServiceUrlSigner() {
        super();
    }

    @PostConstruct
    public void init() {
        logger.info("intiing");
        String confDir = "ENV-"+ ConfigUtils.INSTANCE.getStringValue("environment");

        clientId = ConfigUtils.INSTANCE.getStringValue("gcs.client.id");
        logger.info("clientId "+clientId);
        secretKeyFileName = confDir + java.io.File.separator
                + ConfigUtils.INSTANCE.getStringValue("gcs.key.file");
        //logger.info("secretKeyFileName "+secretKeyFileName);
        String secretKeyFileNameJson = confDir + java.io.File.separator
                + ConfigUtils.INSTANCE.getStringValue("gcs.key.filejson");
        //logger.info("secretKeyFileNameJson "+secretKeyFileNameJson);
        bucket = ConfigUtils.INSTANCE.getStringValue("gcs.bucket.name");
        logger.info("bucket "+bucket);
        expirationMinutes = ConfigUtils.INSTANCE.getIntValue("gcs.signed.url.expiration.minutes");
        try {
            privateKey = loadKeyFromPkcs12(getClass(), secretKeyFileName,
                    "notasecret".toCharArray());
            //logger.info("privateKey "+privateKey);
            storage = StorageOptions.newBuilder().setProjectId(clientId).setCredentials(ServiceAccountCredentials.fromStream(getClass().getClassLoader().getResourceAsStream(secretKeyFileNameJson))).build().getService();
            logger.info(storage);
        } catch (Exception e) {
            //throw new RuntimeException(e);
            //TODO: Fix this error
            logger.error(e);
        }
        logger.info(privateKey);
        }

    private PrivateKey loadKeyFromPkcs12(Class<?> clazz, String filename,
            char[] password) throws Exception {

        InputStream fis = clazz.getClassLoader().getResourceAsStream(filename);

        KeyStore ks = KeyStore.getInstance("PKCS12");
        try {
            ks.load(fis, password);
        } catch (IOException e) {
            if (e.getCause() != null
                    && e.getCause() instanceof UnrecoverableKeyException) {
                System.err.println("Incorrect password");
            }
            throw e;
        }

        return (PrivateKey) ks.getKey("privatekey", password);
    }

    public String getSignedUrl(final String httpVerb, final String filePath,
            final FileACL acl, final Integer expirationMinutes)
            throws Exception {

        if (FileACL.PUBLIC_READ.equals(acl)) {
            return new StringBuilder(BASE_URL).append("/").append(bucket)
                    .append("/").append(filePath).toString();
        }
        final long expiration = expirationEPOCHSeconds((expirationMinutes == null || expirationMinutes
                .intValue() < 1) ? this.expirationMinutes : expirationMinutes
                        .intValue());
        final String unsigned = stringToSign(expiration, filePath, httpVerb);
        final String signature = sign(unsigned);

        return new StringBuilder(BASE_URL).append("/").append(bucket)
                .append("/").append(filePath).append("?GoogleAccessId=")
                .append(clientId).append("&Expires=").append(expiration)
                .append("&Signature=")
                .append(URLEncoder.encode(signature, "UTF-8")).toString();
    }

    private long expirationEPOCHSeconds(int expirationMinutes) {

        final long millisPerSecond = 1000L;
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, expirationMinutes);
        final long expiration = calendar.getTimeInMillis() / millisPerSecond;
        return expiration;
    }

    private String stringToSign(final long expiration, String filename,
            String httpVerb) {

        final String contentType = StringUtils.EMPTY;
        final String contentMD5 = StringUtils.EMPTY;
        final String canonicalizedExtensionHeaders = StringUtils.EMPTY;
        final String canonicalizedResource = "/" + bucket + "/" + filename;
        final String stringToSign = httpVerb + "\n" + contentMD5 + "\n"
                + contentType + "\n" + expiration + "\n"
                + canonicalizedExtensionHeaders + canonicalizedResource;
        return stringToSign;
    }

    private String sign(final String stringToSign) throws Exception {

        String signature = signatureFromFile(stringToSign);
        return signature;
    }

    private String signatureFromFile(final String stringToSign)
            throws Exception {

        return signData(privateKey, stringToSign);
    }

    public String getBucket() {

        return bucket;
    }

    public String getMediaFolder() {
        return "media";
    }
    
    public String uploadFile(java.io.File file, String path, String bucketName, String acl) throws IOException {
        BlobInfo blobInfo;
        blobInfo = storage.create(
                   BlobInfo
                           .newBuilder(bucketName, path)
                           .setAcl(new ArrayList<>(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                           .build(),
                   new FileInputStream(file));
        return blobInfo.getMediaLink();
    }

    
    private String signData(PrivateKey key, String data) throws Exception {

        Signature signer = Signature.getInstance("SHA256withRSA");
        signer.initSign(key);
        signer.update(data.getBytes("UTF-8"));
        byte[] rawSignature = signer.sign();
        String encodedSignature = new String(Base64.encodeBase64(rawSignature,
                false), "UTF-8");
        return encodedSignature;
    }

    public UploadSignature getUploadSignature(FileACL acl) throws Exception {
        if (acl == null) {
            acl = FileACL.PRIVATE;
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, expirationMinutes);
        //System.out.println("================="
        //		+ DateUtil.formatDateTime(calendar.getTime())
        //		+ "============================");
        String policy = createPolicyDocument(acl.getAclValue());

        String signature = null;
        try {
            signature = sign(policy);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String actionUrl = new StringBuilder().append(BASE_URL)
                .append("/").append(bucket).toString();
        UploadSignature uploadSignature = new UploadSignature(actionUrl, signature, policy, clientId, acl.getAclValue());

        return uploadSignature;
    }

    public String getChunkedUploadUrl(File fileUpload) {

        try {
            URL url = new URL("https://www.googleapis.com/upload/storage/v1/b/"
                    + getBucket() + "/o?uploadType=resumable&name="
                    + fileUpload.getPath());
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("X-Upload-Content-Type",
                    fileUpload.getType());
            connection.setRequestProperty("X-Upload-Content-Length",
                    String.valueOf(fileUpload.getSize()));
            connection.setRequestProperty("Host", "www.googleapis.com");
            connection.setRequestProperty("Origin", ConfigUtils.INSTANCE.getStringValue("gcs.upload.allowed.origin"));
            connection.setRequestProperty("Accept", "*/*");

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                for (Map.Entry<String, List<String>> entry : connection
                        .getHeaderFields().entrySet()) {
                    logger.info("Key : " + entry.getKey() + " ,Value : "
                            + entry.getValue());
                }

                String location = connection.getHeaderField("Location");
                logger.info("upload Location : " + location);

                return location;
            } else {
                logger.info("connection.getResponseCode(): "
                        + connection.getResponseCode()
                        + " connection.getResponseMessage(): "
                        + connection.getResponseMessage());
                // Server returned HTTP error code.
            }

        } catch (MalformedURLException e) {
            logger.info("getChunkedUploadUrl", e);
            // ...
        } catch (IOException e) {
            logger.info("getChunkedUploadUrl", e);
            // ...
        }
        return null;
    }

    private String createPolicyDocument(String acl) {

        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.MINUTE, 20);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String expiration = df.format(gc.getTime());

        StringBuilder buf = new StringBuilder();
        buf.append("{\"expiration\": \"");
        buf.append(expiration);
        buf.append("\"");
        buf.append(",\"conditions\": [");
        buf.append("[\"starts-with\",\"$key\",\"\"],");
        buf.append("[\"starts-with\",\"$content-type\",\"\"],");
        buf.append("[\"starts-with\",\"$acl\",\"" + acl + "\"],");
        buf.append("{\"success_action_status\":\"201\"},{\"bucket\":\""
                + bucket + "\"},");
        buf.append("{\"acl\": \"").append(acl).append("\" } ");
        buf.append("]}");

        return Base64.encodeBase64String(buf.toString().getBytes());
    }

}
