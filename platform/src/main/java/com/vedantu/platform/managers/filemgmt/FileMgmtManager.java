/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.filemgmt;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.platform.dao.filemgmt.FileDAO;
import com.vedantu.platform.dao.filemgmt.FileMetadataDAO;
import com.vedantu.platform.mongodbentities.filemgmt.FileMetadata;
import com.vedantu.platform.mongodbentities.filemgmt.File;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.wave.WaveDAO;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.platform.request.filemgmt.FileDownloadUrlReq;
import com.vedantu.platform.request.filemgmt.FileInitReq;
import com.vedantu.platform.request.filemgmt.FileMetadataSetter;
import com.vedantu.platform.request.filemgmt.GetFileMetadataReq;
import com.vedantu.platform.request.filemgmt.UpdateFileMetadataReq;
import com.vedantu.platform.request.filemgmt.UpdateFileUploadStatusReq;
import com.vedantu.platform.request.filemgmt.UploadFileMetadataCollectionReq;
import com.vedantu.platform.request.filemgmt.UploadFileMetadataReq;
import com.vedantu.platform.request.filemgmt.UploadSignature;
import com.vedantu.platform.request.filemgmt.UploadType;
import com.vedantu.platform.response.filemgmt.FileDownloadUrlRes;
import com.vedantu.platform.response.filemgmt.FileInitRes;
import com.vedantu.platform.response.filemgmt.UpdateFileUploadStatusRes;
import com.vedantu.platform.response.filemgmt.FileMetadataCollectionRes;
import com.vedantu.platform.response.filemgmt.GetFileMetadatRes;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.pojo.wave.WhiteboardPDF;
import com.vedantu.platform.request.filemgmt.PdfToImageReq;
import com.vedantu.platform.request.filemgmt.UpdateWhiteboardPdfUploadStatusReq;
import com.vedantu.platform.request.filemgmt.WhiteboardPdfInitReq;
import com.vedantu.platform.response.filemgmt.PdfToImageRes;
import com.vedantu.platform.response.filemgmt.WhiteboardPdfInitRes;
import com.vedantu.platform.response.filemgmt.GetWhiteboardPdfInfoRes;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.imageio.ImageIO;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author somil
 */
@Service
public class FileMgmtManager {

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FileMgmtManager.class);
    
    @Autowired
    GCSAppIdentityServiceUrlSigner signer;
    
//    @Autowired
//    GCSFileUploader gCSFileUploader;
    
    @Autowired
    FileMetadataDAO fileMetadataDAO;

    @Autowired
    FileDAO fileDAO;
    
    @Autowired
    WaveDAO waveDAO;
    
    @Autowired
    AwsS3Manager awsS3Manager;
    
    @Autowired
    HttpSessionUtils sessionUtils;

    private AmazonS3Client s3Client;

    public FileMgmtManager() {
        s3Client = new AmazonS3Client();
    }
    
        
    public FileInitRes init(FileInitReq req) throws VException, Exception {

        req.verify();
        File fileUpload = new File(req);

        fileUpload.setBucket(signer.getBucket());
        String folder=signer.getMediaFolder();
        if(req.getFolder()!=null){
            folder=new StringBuilder()
                .append(signer.getMediaFolder())
                .append(java.io.File.separator)
                .append(req.getFolder()).toString();
        }
        fileDAO.upsert(fileUpload, req.getCallingUserId());
        String path = new StringBuilder()
                .append(folder)
                .append(java.io.File.separator)
                .append(fileUpload.getId())
                .append(fileUpload.getName().substring(
                        fileUpload.getName().lastIndexOf("."))).toString();
        fileUpload.setPath(path);
        fileDAO.upsert(fileUpload, req.getCallingUserId());
        UploadSignature uploadSignature = signer.getUploadSignature(fileUpload.getAcl());
        uploadSignature.contentType = fileUpload.getType();
        uploadSignature.key = path;
        if (UploadType.CHUNKED.equals(fileUpload.getUploadType())) {
            String location = signer.getChunkedUploadUrl(fileUpload);
            uploadSignature.actionUrl = location;
        }
        FileInitRes initRes = new FileInitRes(fileUpload.getName(), fileUpload.getType(), fileUpload.getSize(), fileUpload.getBucket(), fileUpload.getPath(), fileUpload.getId());
        initRes.setUploadSignature(uploadSignature);

        return initRes;

    }
    
    
    public UpdateFileUploadStatusRes changeState(UpdateFileUploadStatusReq req, boolean app) throws VException {
        File fileUpload = fileDAO.getById(
                req.getId());
        if (fileUpload == null) {
            throw new NotFoundException(ErrorCode.FILE_NOT_FOUND, null);
        }

        if (fileUpload.getState().equals(req.getState())) {
            throw new ConflictException(
                    ErrorCode.INVALID_FILE_STATE,
                    "can not update as file is already at "
                    + fileUpload.getState() + " state");
        }
        if(!app){
            sessionUtils.checkIfAllowed(fileUpload.getUserId(), null, Boolean.FALSE);
        }

        UpdateFileUploadStatusRes res = new UpdateFileUploadStatusRes();
        res.setId(req.getId());
        res.setOldState(fileUpload.getState());
        fileUpload.setState(req.getState());
        fileDAO.upsert(fileUpload, req.getCallingUserId());
        res.setNewState(fileUpload.getState());
        return res;
    }
    
    
    public FileDownloadUrlRes fetchDownloadUrl(FileDownloadUrlReq req) throws InternalServerErrorException,
            NotFoundException {

        File fileUpload = fileDAO.getById(req.getId());
        if (fileUpload == null) {
            throw new NotFoundException(ErrorCode.FILE_NOT_FOUND, null);
        }
        FileDownloadUrlRes res = null;
        try {
            res = new FileDownloadUrlRes();
            res.setUrl(signer.getSignedUrl("GET",
                    fileUpload.getPath(), fileUpload.getAcl(),
                    req.getExpirationMinutes()));
            res.setServerId(req.getId());
        } catch (Exception e) {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        return res;
    }
    
    
    public BasicRes insertFileMetadata(UploadFileMetadataReq fileMetaDataReq) throws VException {
        BasicRes insertRes = new BasicRes();
        FileMetadata fileMetadata = new FileMetadata(fileMetaDataReq.getSessionId(), fileMetaDataReq.getUserId(), fileMetaDataReq.getFileId(), fileMetaDataReq.getFileType());
        insertFileMetadata(fileMetadata, fileMetaDataReq.getCallingUserId());
        return insertRes;
    }
    
    
    public FileMetadataCollectionRes insertFileMetadataCollection(UploadFileMetadataCollectionReq fileMetadataCollectionReq)
            throws VException {
        FileMetadataCollectionRes fileMetadataCollectionRes = new FileMetadataCollectionRes();
        List<FileMetadata> fileMetadataCollection = new ArrayList<FileMetadata>();
        for (FileMetadataSetter fileMetaDataSetter : fileMetadataCollectionReq.getCollection()) {
            FileMetadata fileMetadata
                    = new FileMetadata(fileMetaDataSetter.getSessionId(), fileMetaDataSetter.getUserId(), fileMetaDataSetter.getFileData(), fileMetaDataSetter.getFileId(), fileMetaDataSetter.getFileType(), fileMetaDataSetter.getLocalObjectId());
            fileMetadata = insertFileMetadata(fileMetadata, fileMetadataCollectionReq.getCallingUserId());
            fileMetadataCollection.add(fileMetadata);
        }
        fileMetadataCollectionRes.setFileMetadataCollection(fileMetadataCollection);
        return fileMetadataCollectionRes;
    }
    
    
    public FileMetadataCollectionRes insertFileMetadataForAWS(UploadFileMetadataCollectionReq fileMetadataCollectionReq)
            throws VException {
        FileMetadataCollectionRes fileMetadataCollectionRes = new FileMetadataCollectionRes();
        List<FileMetadata> fileMetadataCollection = new ArrayList<FileMetadata>();
        for (FileMetadataSetter fileMetaDataSetter : fileMetadataCollectionReq.getCollection()) {
            FileMetadata fileMetadata = new FileMetadata(fileMetaDataSetter.getSessionId(), fileMetaDataSetter.getUserId(), fileMetaDataSetter.getFileData(), fileMetaDataSetter.getFileId(), fileMetaDataSetter.getFileType(), fileMetaDataSetter.getLocalObjectId());
            fileMetadata = insertFileMetadataForAWS(fileMetadata, fileMetadataCollectionReq.getCallingUserId());
            fileMetadataCollection.add(fileMetadata);
        }
        fileMetadataCollectionRes.setFileMetadataCollection(fileMetadataCollection);
        return fileMetadataCollectionRes;
    }
    
    
    
    public GetFileMetadatRes fetchFileMetadata(GetFileMetadataReq getReq)
            throws VException {
        List<FileMetadata> retrievedData = fileMetadataDAO.getData(
                getReq.getSessionId(), getReq.getUserId(), getReq.getFrom(),
                getReq.getTill(), getReq.getFileType(), getReq.getStart(),
                getReq.getLimit(), getReq.getOrderDesc());
        GetFileMetadatRes response = new GetFileMetadatRes();
        response.setData(retrievedData);
        return response;
    }
        
        
        
        
    public BasicRes updateFileMetadata(UpdateFileMetadataReq updateReq) throws VException {
        BasicRes response = new BasicRes();
        FileMetadata fileMetadata = updateFileMetadata(updateReq.getFileMetadataId(), updateReq.getFileId(), updateReq.getCallingUserId());
        if (fileMetadata != null) {
            response.setSuccess(true);
        } else {
            response.setSuccess(false);
        }
        return response;
    }
    
    
    
    
    public FileMetadata insertFileMetadata(FileMetadata fileMetadata, Long callingUserId) {
        FileMetadata uploadedFileMetadata = null;
        uploadedFileMetadata = fileMetadataDAO.getFileMetadataByLocalObjectId(fileMetadata
                .getLocalObjectId());
        if (uploadedFileMetadata == null) {
            fileMetadataDAO.create(fileMetadata, callingUserId);
            uploadedFileMetadata = fileMetadata;
        }
        return uploadedFileMetadata;
    }
    
    
    public FileMetadata insertFileMetadataForAWS(FileMetadata fileMetadata, Long callingUserId) {
        FileMetadata uploadedFileMetadata = null;
        uploadedFileMetadata = fileMetadataDAO.getFileMetadataByUserIdAndFileId(fileMetadata.getUserId(),
                fileMetadata.getFileId());
        if (uploadedFileMetadata == null) {
            fileMetadataDAO.create(fileMetadata, callingUserId);
            uploadedFileMetadata = fileMetadata;
        }
        return uploadedFileMetadata;
    }
    
    
    public FileMetadata updateFileMetadata(String fileMetadataId, Long fileId, Long callingUserId)
            throws VException {
        FileMetadata fileMetadata = fileMetadataDAO.getById(fileMetadataId);
        if (fileMetadata == null) {
            throw new NotFoundException(ErrorCode.FILE_NOT_FOUND, null);
        }
        if (fileId != null
                && fileMetadata.getFileId() == null) {
            sessionUtils.checkIfAllowed(fileMetadata.getUserId(), null, Boolean.FALSE);
            fileMetadata.setFileId(fileId);
            fileMetadataDAO.create(fileMetadata, callingUserId);
        }
        return fileMetadata;
    }
    
    public List<PdfToImageRes> convertPdftoImages(PdfToImageReq pdfToImageReq) throws InternalServerErrorException,
            NotFoundException,
            BadRequestException,
            IOException {
        MultipartFile file=pdfToImageReq.getFile();
        List<PdfToImageRes> res = new ArrayList();
        PDDocument document = PDDocument.load(file.getInputStream());
        String baseFileName = file.getOriginalFilename();
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        float scale=pdfToImageReq.getScale();
//        GCSFileUploader gCSFileUploader=new GCSFileUploader();
        for (int pageIndex = 0; pageIndex < document.getNumberOfPages(); pageIndex++) {
            String destinationFileName = baseFileName + pageIndex + ".png";
            java.io.File outputfile = java.io.File.createTempFile(destinationFileName, ".png");
            BufferedImage bim = pdfRenderer.renderImage(pageIndex, scale);
            ImageIO.write(bim, "png", outputfile);
            File fileUpload = new File(pdfToImageReq);
            fileUpload.setName(destinationFileName);
            fileUpload.setBucket(signer.getBucket());
            String folder = signer.getMediaFolder();
            if (pdfToImageReq.getFolder() != null) {
                folder = new StringBuilder()
                        .append(signer.getMediaFolder())
                        .append(java.io.File.separator)
                        .append(pdfToImageReq.getFolder()).toString();
            }
            fileDAO.upsert(fileUpload, pdfToImageReq.getCallingUserId());
            String path = new StringBuilder()
                    .append(folder)
                    .append(java.io.File.separator)
                    .append(fileUpload.getId())
                    .append(fileUpload.getName().substring(
                            fileUpload.getName().lastIndexOf("."))).toString();
            fileUpload.setPath(path);
            fileDAO.upsert(fileUpload, pdfToImageReq.getCallingUserId());
            String downloadUrl=signer.uploadFile(outputfile, path,fileUpload.getBucket(), "aaa");
            PdfToImageRes pdfToImageRes=new PdfToImageRes(downloadUrl,fileUpload.getId(),bim.getHeight(),bim.getWidth());
            res.add(pdfToImageRes);
            outputfile.delete();
        }
        document.close();
        return res;
    }

    
    public String getPreSignedUrlForProdAWSAccount(String bucket, String key, String contentType) {
        String presignedUrl = "";

        try {
            java.util.Date expiration = new java.util.Date();
            long milliSeconds = expiration.getTime();
            milliSeconds += 1000 * 60 * 60; // Add 1 hour.
            expiration.setTime(milliSeconds);

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key);
            generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
            generatePresignedUrlRequest.setExpiration(expiration);
            generatePresignedUrlRequest.setContentType(contentType);

            presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();

            logger.info("preSignedUrl:" + presignedUrl);
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }
    
    
    public WhiteboardPdfInitRes initWhiteboardPdf(WhiteboardPdfInitReq req) throws VException {

        req.verify();
        WhiteboardPDF fileUpload = new WhiteboardPDF(req);
        fileUpload.setS3BucketName(ConfigUtils.INSTANCE.getStringValue("aws.bucket.wave.sessiondata"));
        waveDAO.upsertWhiteboardPdf(fileUpload, req.getCallingUserId());
        String path = new StringBuilder()
                .append("whiteboardPdf")
                .append(java.io.File.separator)
                .append(ConfigUtils.INSTANCE.getStringValue("environment"))
                .append(java.io.File.separator)
                .append(fileUpload.getId())
                .append(".pdf")
                .toString();
        fileUpload.setS3BucketPath(path);
        waveDAO.upsertWhiteboardPdf(fileUpload, req.getCallingUserId());
        String uploadUrl=awsS3Manager.getPreSignedUrl(fileUpload.getS3BucketName(), path,fileUpload.getType());
        WhiteboardPdfInitRes initRes = new WhiteboardPdfInitRes(fileUpload,uploadUrl);
        return initRes;
    }
    
    
    public WhiteboardPDF changeWhiteboardPdfState(UpdateWhiteboardPdfUploadStatusReq req) throws VException {
        WhiteboardPDF fileUpload = waveDAO.getWhiteboardPdfById(
                req.getId());
        if (fileUpload == null) {
            throw new NotFoundException(ErrorCode.FILE_NOT_FOUND, null);
        }
        fileUpload.setState(req.getState());
        fileUpload.setImages(req.getImages());
        if(req.getTotalPageCount()!=null){
            fileUpload.setTotalPageCount(req.getTotalPageCount());
        }
        if(req.getConvertedPageCount()!=null){
            fileUpload.setConvertedPageCount(req.getConvertedPageCount());
        }
        waveDAO.upsertWhiteboardPdf(fileUpload,null);
        return fileUpload;
    }
    
    public GetWhiteboardPdfInfoRes getWhiteboardPdfInfo(String pdfId) throws VException, Exception {
        WhiteboardPDF fileUpload = waveDAO.getWhiteboardPdfById(
                pdfId);
        if (fileUpload == null) {
            throw new NotFoundException(ErrorCode.FILE_NOT_FOUND, null);
        }
        List<PdfToImageRes> imageIds=fileUpload.getImages();
        List<PdfToImageRes> images=new ArrayList<>();
        GetWhiteboardPdfInfoRes getWhiteboardPdfInfoRes= new GetWhiteboardPdfInfoRes(fileUpload);
        String keyRoot = "whiteboardPdf/" + ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase() + "/";
        String bucketName = ConfigUtils.INSTANCE.getStringValue("aws.bucket.wave.sessiondata");
        if(!imageIds.isEmpty()){
            for(int i=0;i<imageIds.size();i++){
                File file = fileDAO.getById(imageIds.get(i).getServerId());

//                String url = signer.getSignedUrl("GET",
//                            file.getPath(), file.getAcl(),
//                        Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("max.session.duration")));
                String key;
                if (file.getPath() != null) {
                    key = keyRoot + file.getPath();
                }
                else {
                    key = keyRoot + "media/wavebook-media/" + file.getId() + ".png";
                }
                String url = s3Client.getUrl(bucketName, key).toString();
                images.add(new PdfToImageRes(url, imageIds.get(i).getServerId(), imageIds.get(i).getHeight(), imageIds.get(i).getWidth()));
                
            }
            getWhiteboardPdfInfoRes.setImages(images);
        }
        return getWhiteboardPdfInfoRes;
    }
//    private static String profilePicBucketName;
//    private static final String mediaFolder = "media";
//
//    @PostConstruct
//    public void init() {
//        profilePicBucketName = ConfigUtils.INSTANCE.getStringValue("aws.profilePicBucketName");
//    }

//    public FileInitRes init(FileInitReq req) throws VException {
//        req.verify();
//        File fileUpload = new File(req);
//
//        //GCSAppIdentityServiceUrlSigner signer = GCSAppIdentityServiceUrlSigner.INSTANCE;
//        fileUpload.setBucket(profilePicBucketName);
//
//        fileDAO.create(fileUpload);
//        String path = mediaFolder + java.io.File.separator + fileUpload.getId() + fileUpload.getName().substring(
//                fileUpload.getName().lastIndexOf("."));
//        fileUpload.setPath(path);
//        fileDAO.create(fileUpload);
//        /*
//        GCSAppIdentityServiceUrlSigner.UploadSignature uploadSignature = signer.getUploadSignature(fileUpload.getAcl());
//        uploadSignature.contentType = fileUpload.getType();
//        uploadSignature.key = path;
//        if (UploadType.CHUNKED.equals(fileUpload.getUploadType())) {
//                String location = signer.getChunkedUploadUrl(fileUpload);
//                uploadSignature.actionUrl = location;
//        }
//         */
//
//        FileInitRes initRes = new FileInitRes(fileUpload.getName(), fileUpload.getType(), fileUpload.getSize(), fileUpload.getBucket(), fileUpload.getPath(), fileUpload.getId());
//        String signedUrl = getSignedUrl(fileUpload);
//        if (StringUtils.isEmpty(signedUrl)) {
//            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error generating signed url");
//        }
//        initRes.setUploadUrl(signedUrl);
//
//        return initRes;
//    }


//    private String getSignedUrl(File fileUpload) {
//        AmazonS3 s3client = new AmazonS3Client();
//
//        try {
//            logger.info("Generating pre-signed URL.");
//            java.util.Date expiration = new java.util.Date();
//            long milliSeconds = expiration.getTime();
//            milliSeconds += 1000 * 60 * 60; // Add 1 hour.
//            expiration.setTime(milliSeconds);
//
//            GeneratePresignedUrlRequest generatePresignedUrlRequest
//                    = new GeneratePresignedUrlRequest(fileUpload.getBucket(), fileUpload.getPath());
//            generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
//            generatePresignedUrlRequest.setExpiration(expiration);
//
//            URL url = s3client.generatePresignedUrl(generatePresignedUrlRequest);
//            logger.info("Pre-Signed URL = " + url.toString());
//            return url.toString();
//        } catch (AmazonServiceException exception) {
//            logger.info("Caught an AmazonServiceException, "
//                    + "which means your request made it "
//                    + "to Amazon S3, but was rejected with an error response "
//                    + "for some reason.");
//            logger.info("Error Message: " + exception.getMessage());
//            logger.info("HTTP  Code: " + exception.getStatusCode());
//            logger.info("AWS Error Code:" + exception.getErrorCode());
//            logger.info("Error Type:    " + exception.getErrorType());
//            logger.info("Request ID:    " + exception.getRequestId());
//        } catch (AmazonClientException ace) {
//            logger.info("Caught an AmazonClientException, "
//                    + "which means the client encountered "
//                    + "an internal error while trying to communicate"
//                    + " with S3, "
//                    + "such as not being able to access the network.");
//            logger.info("Error Message: " + ace.getMessage());
//        }
//        return null;
//    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }    

}
