package com.vedantu.platform.managers.scheduling;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.onetofew.pojo.LastUpdatedUsers;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.pojo.scheduling.AvailabilityRange;
import com.vedantu.platform.pojo.scheduling.AvailabilityResponse;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.pojo.scheduling.CalendarEntry;
import com.vedantu.platform.pojo.scheduling.ESSearchResponse;
import com.vedantu.platform.pojo.scheduling.ElasticSearchResponseParser;
import com.vedantu.platform.pojo.scheduling.EsTeacherInfo;
import com.vedantu.platform.pojo.scheduling.IntervalSlot;
import com.vedantu.platform.pojo.scheduling.RouteScheduling;
import com.vedantu.platform.pojo.scheduling.TeacherAvailabilityData;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.scheduling.pojo.calendar.RetryCalendarEventReq;
import com.vedantu.scheduling.request.session.UpdateMultipleSlotsReq;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SlotCheckPojo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.Constants;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.scheduling.SyncAllCalendarReq;


@Service
public class CalendarManager {

	@Autowired
	private OTFManager otfManager;

	private static Gson gson = new Gson();

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private RouteScheduling routeScheduling;

	@Autowired
	private SessionManager sessionManager;

	@Autowired
	private AwsSQSManager awsSQSManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CalendarManager.class);

	private static Type SESSION_SLOT_LIST_TYPE = new TypeToken<List<SessionSlot>>() {
	}.getType();

	private static final int CAL_ENTRIES_BATCH_SIZE = 100;

	public void markCalendarEntries(UpdateMultipleSlotsReq updateMultipleSlotsReq) throws VException {
		logger.info("Request : " + updateMultipleSlotsReq.toString());
		if (CollectionUtils.isEmpty(updateMultipleSlotsReq.collectErrors())) {
			logger.info("Invalid request : " + updateMultipleSlotsReq.toString());
			return;
		}

		int idx = 0;
		List<AddCalendarEntryReq> request = new ArrayList<>(CAL_ENTRIES_BATCH_SIZE + 5);
		Map<String, Object> payload = new HashMap<>();
		payload.put("mark", Boolean.TRUE);
		// payload.put("callingUserId", sessionUtils.getCallingUserId());
		

		// init with common fields
		while(idx < CAL_ENTRIES_BATCH_SIZE) {
			AddCalendarEntryReq req = new AddCalendarEntryReq();
			req.setState(com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION);
			req.setReferenceType(updateMultipleSlotsReq.getReferenceType());
			req.setReferenceId(updateMultipleSlotsReq.getReferenceId());
			request.add(req);
			idx += 1;
		}

		// -- loop through batches of studentIds * sessionSlots in sizes of 100 each
		for (SessionSlot sessionSlot : updateMultipleSlotsReq.getSessionSlots()) {
			idx = 0;

			for (Long userId : updateMultipleSlotsReq.getUserIds()) {
				AddCalendarEntryReq calendarEntryReq = request.get(idx);

				calendarEntryReq.setUserId((String.valueOf(userId)));
				calendarEntryReq.setStartTime(sessionSlot.getStartTime());
				calendarEntryReq.setEndTime(sessionSlot.getEndTime());

				idx += 1;
				if (idx % CAL_ENTRIES_BATCH_SIZE == 0) {
					try {
						payload.put("req", request);
						awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload));

						idx = 0;
						// request.clear();
					} catch (Exception e) {
						logger.error("error while marking calendar entries for the list of users" + request);
					}
				}
			}

			// check for pending tail request objects
			if (idx % CAL_ENTRIES_BATCH_SIZE != 0) {
				payload.put("req", request.subList(0, idx % CAL_ENTRIES_BATCH_SIZE));
				awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload));
				// request.clear();
			}
		}

	}

	public BasicRes markCalendarEntries(List<AddCalendarEntryReq> calendarEntryReqList) {
		logger.info("entering mark state for slots user");
		String markStateUrl = "/calendarEntry/multiple";
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		logger.info("post url is " + schedulingEndpoint + markStateUrl);
		ClientResponse markedCalendarEntries = WebUtils.INSTANCE.doCall(schedulingEndpoint + markStateUrl,
				HttpMethod.POST, gson.toJson(calendarEntryReqList));

		String output = markedCalendarEntries.getEntity(String.class);
		logger.info("output from mark calendar entry in platform with basics " + output);
		Type basicResType = new TypeToken<BasicRes>() {
		}.getType();
		BasicRes basicRes = new Gson().fromJson(output, basicResType);

		return basicRes;
	}

	public BasicRes markCalendarEntries(AddCalendarEntryReq calendarEntryReq) {

		logger.info("entering mark state for slots user");
		String markStateUrl = "/calendarEntry";
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		logger.info("post url is " + schedulingEndpoint + markStateUrl);
		ClientResponse markedCalendarEntries = WebUtils.INSTANCE.doCall(schedulingEndpoint + markStateUrl,
				HttpMethod.POST, gson.toJson(calendarEntryReq));

		String output = markedCalendarEntries.getEntity(String.class);
		logger.info("output from mark calendar entry in platform with basics " + output);
		Type calendarEntryListType = new TypeToken<List<CalendarEntry>>() {
		}.getType();
		List<CalendarEntry> markedCalendarEntriesList = new Gson().fromJson(output, calendarEntryListType);
		BasicRes basicRes = new BasicRes();
		if (markedCalendarEntriesList.size() > 0) {
			basicRes.setSuccess(true);
		} else {
			basicRes.setSuccess(false);
		}

		return basicRes;
	}

	public void unmarkCalendarEntries(UpdateMultipleSlotsReq updateMultipleSlotsReq) throws VException {
		logger.info("Request : " + updateMultipleSlotsReq.toString());
		if (CollectionUtils.isEmpty(updateMultipleSlotsReq.collectErrors())) {
			logger.info("Invalid request : " + updateMultipleSlotsReq.toString());
			return;
		}

		// TODO : Needs optimization. Will pick it up later
		for (Long userId : updateMultipleSlotsReq.getUserIds()) {
			for (SessionSlot sessionSlot : updateMultipleSlotsReq.getSessionSlots()) {
				AddCalendarEntryReq calendarEntryReq = new AddCalendarEntryReq(String.valueOf(userId),
						sessionSlot.getStartTime(), sessionSlot.getEndTime(),
						com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION,
						updateMultipleSlotsReq.getReferenceType(), updateMultipleSlotsReq.getReferenceId());
				unmarkCalendarEntries(calendarEntryReq);
			}
		}
	}

	public BasicRes unmarkCalendarEntries(AddCalendarEntryReq calendarEntryReq) {
		logger.info("Request : " + calendarEntryReq.toString());
		String getCalendarEntriesUrl = "/calendarEntries";
		String params = "?startTime=" + calendarEntryReq.getStartTime() + "&endTime=" + calendarEntryReq.getEndTime()
				+ "&userId=" + calendarEntryReq.getUserId();

		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		ClientResponse getCalendarEntries = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEntriesUrl + params, HttpMethod.GET, null);

		String outputCalendarIds = getCalendarEntries.getEntity(String.class);
		logger.info("output from get calendar entry in platform with basics " + outputCalendarIds);
		Type calendarEntryListType = new TypeToken<List<CalendarEntry>>() {
		}.getType();
		List<CalendarEntry> calendarEntriesList = new Gson().fromJson(outputCalendarIds, calendarEntryListType);
		BasicRes basicRes = new BasicRes();
		basicRes.setSuccess(true);

		long start = calendarEntryReq.getStartTime();
		long end = calendarEntryReq.getEndTime();
		for (CalendarEntry calendarEntry : calendarEntriesList) {
			start = calendarEntryReq.getStartTime();
			end = calendarEntryReq.getEndTime();

			String removeStateUrl = "/calendarEntry/" + calendarEntry.getId() + "/removeSlots";
			if (calendarEntry.getDayStartTime() >= calendarEntryReq.getStartTime())
				start = calendarEntry.getDayStartTime();
			if (calendarEntryReq.getEndTime() >= calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY)
				end = calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY;
			params = "?startTime=" + start + "&endTime=" + end + "&slotState=" + calendarEntryReq.getState();

			ClientResponse removedCalendarSlots = WebUtils.INSTANCE.doCall(schedulingEndpoint + removeStateUrl + params,
					HttpMethod.POST, null);
			String output = removedCalendarSlots.getEntity(String.class);
			logger.info("output from remove calendar entry slots in platform with basics " + output);

		}
		return basicRes;
	}

	public String retryCalendarEvents(RetryCalendarEventReq request)
			throws VException, IllegalArgumentException, IllegalAccessException {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarEventsUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.retryevents");
		String queryString = WebUtils.INSTANCE.createQueryStringOfObject(request);
		logger.info("retryCalendarEvents : " + queryString);
		ClientResponse getSlotStringResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEventsUrl + "?" + queryString, HttpMethod.GET, null);
		String output = getSlotStringResponse.getEntity(String.class);
		logger.info("retryCalendarEvents : " + output);
		return output;
	}

	public void retryCalendarEvents() throws VException, IllegalArgumentException, IllegalAccessException {
		Long currentTime = System.currentTimeMillis();
		currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
		logger.info("SubscriptionRequestProcessingTask - " + currentTime);
		Long timeDelay = 60 * 1000 * ConfigUtils.INSTANCE.getLongValue("retry.calendar.event.cron.delay.mins");
		Long endTime = currentTime - timeDelay;
		long cronTimeWindow = 15 * DateTimeUtils.MILLIS_PER_MINUTE;
		Long startTime = endTime - cronTimeWindow;
		logger.info("SubscriptionRequestProcessingTask - startTime : " + startTime + " endTime : " + endTime);
		RetryCalendarEventReq req = new RetryCalendarEventReq();
		req.setCreationStartTime(startTime);
		req.setCreationEndTime(endTime);
		retryCalendarEvents(req);

	}

	public Double getWeightedTruerAvailability(String userId, Long startTime, Long endtime, int repeat,
			String matchString) throws Exception {

		float match = Float.parseFloat(matchString);
		logger.info("entering CALENDARENTRYTRUER: teacherId-" + userId + " startTime-" + startTime + "endTime-"
				+ endtime + " repeat-" + repeat);

		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getWeightedAvailabilityRangesUrl = "/getWeightedAvailabilityRanges";
		List<AvailabilityRange> availabilityRanges = new ArrayList<>();
		try {
			ClientResponse response = WebUtils.INSTANCE.doCall(schedulingEndpoint + getWeightedAvailabilityRangesUrl,
					HttpMethod.GET, null);
			String output = response.getEntity(String.class);
			Type AvailabilityRange = new TypeToken<List<AvailabilityRange>>() {
			}.getType();
			availabilityRanges = new Gson().fromJson(output, AvailabilityRange);
		} catch (Exception e) {
			logger.error("could not get availability ranges for teacher listing");
		}

		Long weekStart = startTime;
		weekStart = weekStart - weekStart % DateTimeUtils.MILLIS_PER_DAY;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(weekStart);

		cal.setTimeInMillis(
				cal.getTimeInMillis() - ((cal.get(Calendar.DAY_OF_WEEK) % 7) * DateTimeUtils.MILLIS_PER_DAY));

		List<Integer> defaultAvailability = new ArrayList<>();

		Long requestEndTime = endtime + repeat * DateTimeUtils.MILLIS_PER_WEEK;
		for (Integer slot = 0; slot < 96; ++slot)
			defaultAvailability.add(slot);
		String getCalendarEntriesUrl = "/calendarEntries";
		Long endTime = weekStart + 8 * DateTimeUtils.MILLIS_PER_DAY + repeat * DateTimeUtils.MILLIS_PER_WEEK;

		String params = "?startTime=" + (cal.getTimeInMillis()) + "&endTime=" + endTime + "&userId=" + userId;

		ClientResponse teacherCalendarEntries = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarEntriesUrl + params, HttpMethod.GET, null);

		String outputCalendarIds = teacherCalendarEntries.getEntity(String.class);
		logger.info("output from get calendar entry in truer availability for teacher :" + userId + " ->"
				+ outputCalendarIds);
		JSONArray jsonArray = new JSONArray(outputCalendarIds);
		ObjectMapper objectMapper = new ObjectMapper();
		List<CalendarEntry> teacherCalendarEntriesList = objectMapper.readValue(jsonArray.toString(),
				objectMapper.getTypeFactory().constructCollectionType(List.class, CalendarEntry.class));
		Map<Long, List<Integer>> teacherAvailability = new HashMap<>();
		Map<Long, List<Integer>> actualAvailability = new HashMap<>();
		Map<Long, List<Integer>> booked = new HashMap<>();
		Long lastUpdateTeacherEntry = -1L;
		for (CalendarEntry calendarEntry : teacherCalendarEntriesList) {
			if (calendarEntry.getDayStartTime() > lastUpdateTeacherEntry
					&& (calendarEntry.getAvailability().size() > 0))
				lastUpdateTeacherEntry = calendarEntry.getDayStartTime();

			List<Integer> entry = new ArrayList<>();
			teacherAvailability.put(calendarEntry.getDayStartTime(), entry);
			List<Integer> availability = calendarEntry.getAvailability();
			actualAvailability.put(calendarEntry.getDayStartTime(), availability);
			List<Integer> session = calendarEntry.getBooked();
			List<Integer> sessionRequest = calendarEntry.getSessionRequest();
			List<Integer> unavailable = new ArrayList<>();
			unavailable.addAll(calendarEntry.getBooked());
			unavailable.addAll(calendarEntry.getSessionRequest());
			booked.put(calendarEntry.getDayStartTime(), unavailable);
			for (Integer available : availability) {
				if (!session.contains(available) && !sessionRequest.contains(available)) {
					if (teacherAvailability.containsKey(calendarEntry.getDayStartTime())) {
						entry = teacherAvailability.get(calendarEntry.getDayStartTime());
						entry.add(available);
						teacherAvailability.put(calendarEntry.getDayStartTime(), entry);
					}
				}
			}
		}

		List<IntervalSlot> intervalSlots = new ArrayList<>();

		Double weightedTruerAvailabilityNumerator = 0.0;
		Double weightedTruerAvailabilityDenominator = 0.0;
		for (; weekStart <= requestEndTime; weekStart += DateTimeUtils.MILLIS_PER_DAY) {
			for (Integer intervalStart = 0; intervalStart < 96; intervalStart += 2) {

				Long requestedStartSlot = ((startTime % DateTimeUtils.MILLIS_PER_DAY)
						/ (15 * DateTimeUtils.MILLIS_PER_MINUTE));

				Long requestedEndSlot = ((requestEndTime % DateTimeUtils.MILLIS_PER_DAY)
						/ (15 * DateTimeUtils.MILLIS_PER_MINUTE)) + 1;
				boolean skipEndSlots = false;
				boolean skipStartSlots = false;
				if (requestEndTime - weekStart < DateTimeUtils.MILLIS_PER_DAY) {
					skipEndSlots = true;
				}

				if (weekStart <= startTime) {
					skipStartSlots = true;
				}

				if (startTime - weekStart > DateTimeUtils.MILLIS_PER_DAY) {
					continue;
				}

				if (skipEndSlots && intervalStart > requestedEndSlot) {
					break;
				}

				if (skipStartSlots && intervalStart < requestedStartSlot) {
					continue;
				}

				double count = 0;
				Double weight = getSlotWeight(intervalStart, availabilityRanges);
				for (Long dayStart = weekStart; dayStart < requestEndTime; dayStart += 7
						* DateTimeUtils.MILLIS_PER_DAY) {
					if (teacherAvailability.containsKey(dayStart)
							&& teacherAvailability.get(dayStart).contains(intervalStart)
							&& teacherAvailability.get(dayStart).contains(intervalStart + 1)) {
						if (dayStart + intervalStart * DateTimeUtils.MILLIS_PER_HOUR / 4 >= startTime && dayStart
								+ (intervalStart + 2) * DateTimeUtils.MILLIS_PER_HOUR / 4 <= requestEndTime) {
							++count;
						}

					}

				}
				// find weight for the slot
				weightedTruerAvailabilityDenominator += weight;
				Long startInterval = weekStart + intervalStart * DateTimeUtils.MILLIS_PER_HOUR / 4;
				Long endInterval = startInterval + DateTimeUtils.MILLIS_PER_HOUR / 2;
				if ((count / (repeat + 1)) >= (match - 0.0001) && startInterval >= startTime
						&& endInterval <= endtime) {
					weightedTruerAvailabilityNumerator += weight;
				}
			}
		}
		if (intervalSlots.size() > 0) {
			logger.info("interval slots Truer" + new ArrayList<>(intervalSlots).toString());
		}
		return weightedTruerAvailabilityNumerator / weightedTruerAvailabilityDenominator;
	}

	private Double getSlotWeight(Integer intervalStart, List<AvailabilityRange> availabilityRanges) {

		intervalStart = convertToIndianSlot(intervalStart);
		for (AvailabilityRange availabilityRange : availabilityRanges) {
			if (intervalStart >= availabilityRange.getMin() * 4 && intervalStart < availabilityRange.getMax() * 4) {
				return availabilityRange.getWeight();
			}
		}
		return 1.0;
	}

	private Integer convertToIndianSlot(Integer intervalStart) {
		return (intervalStart + 22) % 96;
	}

	public List<TeacherAvailabilityData> getTeacherAvailabilityExport(String boardId, Long endTime, Long startTime,
			String grade, String target, String reqTeacherId) throws Exception {

		List<TeacherAvailabilityData> teacherAvailabilityDataList = new ArrayList<TeacherAvailabilityData>();
		// start and limit??
		Map<String, EsTeacherData> teachers = new HashMap<String, EsTeacherData>();
		if ("null".equals(boardId)) {
			boardId = "";
		}
		if ("null".equals(grade)) {
			grade = "";
		}
		if ("null".equals(target)) {
			target = "";
		}

		String teacherHits = routeScheduling.getTeachersWithFilters(boardId, grade, target, "0", "1000", true);
		if (StringUtils.isEmpty(reqTeacherId) || "null".equals(reqTeacherId)) {
			ESSearchResponse esSearchResponse = ElasticSearchResponseParser.INSTANCE.parse(teacherHits);

			List<EsTeacherInfo> esTeacherDatas = esSearchResponse.getTeacherData();
			if (esTeacherDatas == null || esTeacherDatas.isEmpty()) {
				// logEntering("getAvailableTeachers",
				// "getAvailableTeachersEmpty no
				// teachers found for req", req);
				return teacherAvailabilityDataList;
			}

			// Filter out inactive teachers
			Iterator<EsTeacherInfo> esTeacherInfoIterator = esTeacherDatas.iterator();
			while (esTeacherInfoIterator.hasNext()) {
				EsTeacherInfo teacherInfoEntry = esTeacherInfoIterator.next();
				if (!Boolean.TRUE.equals(teacherInfoEntry.getActive())) {
					esTeacherInfoIterator.remove();
					continue;
				}
			}

			// create a map out of the teacher data
			for (EsTeacherData esTeacherData : esTeacherDatas) {
				teachers.put(String.valueOf(esTeacherData.getTeacherId()), esTeacherData);
			}
		} else {
			EsTeacherData esTeacherData = new EsTeacherData();
			esTeacherData.setTeacherId(Long.parseLong(reqTeacherId));
			teachers.put(String.valueOf(esTeacherData.getTeacherId()), esTeacherData);
		}

		List<String> teacherslist = new ArrayList<String>(teachers.keySet());

		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT")
				+ "/calendarEntry/getTeacherAvailabilityBitSet";
		String queryString = "startTime=" + startTime + "&endTime=" + endTime;
		for (String teacherId : teacherslist) {
			queryString += "&teacherIds=" + teacherId;
		}

		ClientResponse availableUsers = WebUtils.INSTANCE.doCall(schedulingEndpoint + "?" + queryString, HttpMethod.GET,
				null);
		String output = availableUsers.getEntity(String.class);
		logger.info("output from available in platform with basics " + output);
		Type availabilityResponseListType = new TypeToken<List<AvailabilityResponse>>() {
		}.getType();
		List<AvailabilityResponse> response = new Gson().fromJson(output, availabilityResponseListType);
		if (!com.vedantu.util.CollectionUtils.isEmpty(response)) {
			for (AvailabilityResponse availabilityResponseEntry : response) {
				TeacherAvailabilityData teacherAvailabilityData = new TeacherAvailabilityData();
				teacherAvailabilityData.setStartTime(startTime);
				teacherAvailabilityData.setEndTime(endTime);
				teacherAvailabilityData.setTeacherData(teachers.get(availabilityResponseEntry.getTeacherId()));
				teacherAvailabilityData.setUserId(availabilityResponseEntry.getTeacherId());
				teacherAvailabilityData.setAvailabilityBitSet(availabilityResponseEntry.getTrueAvailabilityBitSet());
				teacherAvailabilityData.setMarkedAvailabilityBitSet(availabilityResponseEntry.getAvailabilityBitSet());
				teacherAvailabilityDataList.add(teacherAvailabilityData);
			}
		}

		return teacherAvailabilityDataList;
	}

	public String syncNextWeekCalendar() throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarSelectedSlotsUrl = ConfigUtils.INSTANCE
				.getStringValue("scheduling.api.sync.next.week.calendar");
		logger.info("syncNextWeekCalendar : " + System.currentTimeMillis());
		ClientResponse getSelecetedSlotResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarSelectedSlotsUrl, HttpMethod.GET, null);
		String output = getSelecetedSlotResponse.getEntity(String.class);
		logger.info("syncNextWeekCalendar : " + output);
		return output;
	}

	public String syncNextMonthCalendar() throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarSelectedSlotsUrl = ConfigUtils.INSTANCE
				.getStringValue("scheduling.api.sync.next.month.calendar");
		logger.info("syncNextWeekCalendar : " + System.currentTimeMillis());
		ClientResponse getSelecetedSlotResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarSelectedSlotsUrl, HttpMethod.GET, null);
		String output = getSelecetedSlotResponse.getEntity(String.class);
		logger.info("syncNextWeekCalendar : " + output);
		return output;
	}

	public String syncNextOneYearCalendar() throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getCalendarSelectedSlotsUrl = ConfigUtils.INSTANCE
				.getStringValue("scheduling.api.sync.next.year.calendar");
		logger.info("syncNextWeekCalendar : " + System.currentTimeMillis());
		ClientResponse getSelecetedSlotResponse = WebUtils.INSTANCE
				.doCall(schedulingEndpoint + getCalendarSelectedSlotsUrl, HttpMethod.GET, null);
		String output = getSelecetedSlotResponse.getEntity(String.class);
		logger.info("syncNextWeekCalendar : " + output);
		return output;
	}

	public String syncUsersCalendar(SyncAllCalendarReq syncAllCalendarReq) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String getUsersCalendarUrl = ConfigUtils.INSTANCE.getStringValue("scheduling.api.sync.users.calendar");
		logger.info("syncNextWeekCalendar : " + System.currentTimeMillis());
		ClientResponse getUsersCalendarResponse = WebUtils.INSTANCE.doCall(schedulingEndpoint + getUsersCalendarUrl,
				HttpMethod.POST, gson.toJson(syncAllCalendarReq));
		String output = getUsersCalendarResponse.getEntity(String.class);
		logger.info("syncNextWeekCalendar : " + output);
		return output;
	}

	public void syncCalendarLastUpdated() throws Exception {
		long lastUpdatedEndTime = System.currentTimeMillis();
		long lastUpdatedStartTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis())
				- 3 * DateTimeUtils.MILLIS_PER_DAY;
		LastUpdatedUsers lastUpdatedUsers = new LastUpdatedUsers();//getLastUpdatedUsers(lastUpdatedStartTime, lastUpdatedEndTime);
		logger.info("lastupdateusers : " + lastUpdatedUsers);
		List<String> errors = new ArrayList<>();
		long syncStartTime = CommonCalendarUtils.getDayStartTime(System.currentTimeMillis())
				+ DateTimeUtils.MILLIS_PER_DAY;
		long syncEndTime = syncStartTime + 4 * DateTimeUtils.MILLIS_PER_WEEK;
		int size = Constants.DEFAULT_ASYNC_BATCH_SIZE;

		// Sync teachers calendar
		int start = 0;
		List<String> teacherIds = new ArrayList<>(lastUpdatedUsers.getTeacherIds()); // Risky to use skip and limit of
																						// stream API over a set
		if (!CollectionUtils.isEmpty(teacherIds)) {
			while (start < size && start < teacherIds.size()) {
				SyncAllCalendarReq syncAllCalendarReq = new SyncAllCalendarReq();
				syncAllCalendarReq.setStartTime(syncStartTime);
				syncAllCalendarReq.setEndTime(syncEndTime);
				syncAllCalendarReq.setUserIds(teacherIds.stream().skip(start).limit(size).collect(Collectors.toList()));
				try {
					syncUsersCalendar(syncAllCalendarReq);
				} catch (Exception ex) {
					errors.add("TeacherIdsError processing sync calendar - ex: " + syncAllCalendarReq.toString()
							+ " message:" + ex.getMessage());
				}
				start += size;
			}
		}

		// Sync students calendar
		start = 0;
		List<String> studentIds = new ArrayList<>(lastUpdatedUsers.getStudentIds()); // Risky to use skip and limit of
																						// stream API over a set
		if (!CollectionUtils.isEmpty(studentIds)) {
			while (start < size && start < studentIds.size()) {
				SyncAllCalendarReq syncAllCalendarReq = new SyncAllCalendarReq();
				syncAllCalendarReq.setStartTime(syncStartTime);
				syncAllCalendarReq.setEndTime(syncEndTime);
				syncAllCalendarReq.setUserIds(studentIds.stream().skip(start).limit(size).collect(Collectors.toList()));
				try {
					syncUsersCalendar(syncAllCalendarReq);
				} catch (Exception ex) {
					errors.add("StudentIdsError processing sync calendar - ex: " + syncAllCalendarReq.toString()
							+ " message:" + ex.getMessage());
				}
				start += size;
			}
		}

		if (!CollectionUtils.isEmpty(errors)) {
			logger.info("SyncCalendarExceptions:" + Arrays.toString(errors.toArray()));
		}
	}

	public List<SessionSlot> checkSlotAvailablilty(SlotCheckPojo slotCheckList) throws Exception {
		String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		String slotListAvailabiltyUrl = "/calendarEntry/slotListAvailability";
		// logger.info("request url : " + params);
		ClientResponse availableUsers = WebUtils.INSTANCE.doCall(schedulingEndpoint + slotListAvailabiltyUrl,
				HttpMethod.POST, gson.toJson(slotCheckList));
		String output = availableUsers.getEntity(String.class);
		logger.info(" output from  checkslotavailability in platform with basics " + output);
		List<SessionSlot> sessionSlots = gson.fromJson(output, SESSION_SLOT_LIST_TYPE);
		return sessionSlots;
	}

}
