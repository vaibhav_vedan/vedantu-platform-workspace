/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.user;

import com.vedantu.User.request.SocialSource;
import com.vedantu.util.ConfigUtils;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public abstract class AbstractSocialManager implements
        ISocialManager {

    private static final String FIELD_CLIENT_ID = "client_id";
    private static final String FIELD_REDIRECT_URI = "redirect_uri";
    private static final String STATE = "state";

    String CLIENT_ID;
    String CLIENT_SECRET;
    String AUTH_URL;
    String TOKEN_URL;
    String USER_INFO_URL;

    public List<NameValuePair> httpParams = new ArrayList<>();

    public AbstractSocialManager() {
        super();
    }

    protected void init() {
        CLIENT_ID = getProperty("CLIENT_ID");
        CLIENT_SECRET = getProperty("CLIENT_SECRET");
        AUTH_URL = getProperty("AUTH_URL");
        TOKEN_URL = getProperty("TOKEN_URL");
        USER_INFO_URL = getProperty("USER_INFO_URL");
        httpParams.add(new BasicNameValuePair(FIELD_CLIENT_ID, getClientId()));
        httpParams.add(new BasicNameValuePair(FIELD_REDIRECT_URI,
                getRedirectUrl()));
    }

    @Override
    public String getAuthorizeUrl(String generatedString) {

        StringBuilder redirect_url = new StringBuilder();
        redirect_url.append(getAuthUrl());
        List<NameValuePair> httpParamsFinal = new ArrayList<>(
                this.httpParams);
        httpParamsFinal.add(new BasicNameValuePair(STATE,
        		generatedString));
        redirect_url.append(URLEncodedUtils.format(httpParamsFinal, "UTF-8"));
        return redirect_url.toString();
    }

    @Override
    public String getRedirectUrl() {
        return ConfigUtils.INSTANCE.getStringValue("PLATFORM_PUBLIC_URL") + getProperty("REDIRECT_URL");
    }

    protected String getProperty(String key) {
        StringBuilder sb = new StringBuilder();
        sb.append(getSocialSource()).append(".").append(key);
        return ConfigUtils.INSTANCE.getStringValue(sb.toString());
    }

    protected String getLogTag() {
        return getClass().getName();
    }

    public abstract SocialSource getSocialSource();

}
