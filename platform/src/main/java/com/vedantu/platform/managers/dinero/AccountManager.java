/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.ExtTransactionPojo;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.AddAccountPancardReq;
import com.vedantu.dinero.request.GetInstalmentsForOrdersReq;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.dinero.request.SalesTransactionReq;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.offering.pojo.PlanInfo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.pojo.dinero.Account;
import com.vedantu.platform.request.dinero.GetAccountInfoReq;
import com.vedantu.platform.response.dinero.GetAccountInfoRes;
import com.vedantu.platform.response.dinero.SalesOrderInfo;
import com.vedantu.platform.response.dinero.SalesOrderItemInfo;
import com.vedantu.platform.response.dinero.SalesTransactionInfo;
import com.vedantu.platform.response.dinero.TransferFromFreebiesAccountRes;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.dozer.DozerBeanMapper;

/**
 *
 * @author somil
 */
@Service
public class AccountManager {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AccountManager.class);

    private final Gson gson = new Gson();

    private final String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    
    @Autowired
    private DozerBeanMapper mapper;     

    public AccountManager() {
    }

    public Account createUserAccount(User user) throws VException {

        logger.info("Request received to create User Account:" + user.getId());

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/createUserAccount", HttpMethod.POST,
                gson.toJson(user));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Account response = gson.fromJson(jsonString, Account.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    public GetAccountInfoRes getAccountInfo(GetAccountInfoReq getAccountInfoReq) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/getAccountInfo", HttpMethod.POST,
                gson.toJson(getAccountInfoReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetAccountInfoRes response = gson.fromJson(jsonString, GetAccountInfoRes.class);
        return response;
    }

    public TransferFromFreebiesAccountRes transferFromFreebiesAccount(
            TransferFromFreebiesAccountReq transferFromFreebiesAccountReq) throws VException {
        logger.info("Entering " + transferFromFreebiesAccountReq.toString());

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/transferFromFreebiesAccount",
                HttpMethod.POST, gson.toJson(transferFromFreebiesAccountReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Integer amount = transferFromFreebiesAccountReq.getAmount();
        Long toUserId = transferFromFreebiesAccountReq.getUserId();
        if (amount != null && amount > 0
                && !transferFromFreebiesAccountReq.getTransactionRefType().equals(TransactionRefType.REFERRAL_CREDIT)
                && !transferFromFreebiesAccountReq.getNoAlert()) {
            // since the transfer succeeded, lets add the notification
            // in case of TransactionRefType.REFERRAL_CREDIT the email will
            // be
            // send separately
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("amount", amount);
            payload.put("toUserId", toUserId);
            payload.put("noSMS", transferFromFreebiesAccountReq.getNoSMS());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRANSFER_FROM_FREEBIES_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
        }

        TransferFromFreebiesAccountRes response = gson.fromJson(jsonString, TransferFromFreebiesAccountRes.class);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    public List<SalesTransactionInfo> getTransactions(SalesTransactionReq salesTransactionReq) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/getTransactions", HttpMethod.POST,
                gson.toJson(salesTransactionReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<SalesTransactionInfo>>() {
        }.getType();
        List<SalesTransactionInfo> response = gson.fromJson(jsonString, listType);
        return response;
    }

    public List<SalesOrderInfo> getOrders(SalesTransactionReq salesOrderReq)
            throws VException, IllegalAccessException, InvocationTargetException {
        // TODO: Highly unoptimized, change this
        GetOrdersReq req = new GetOrdersReq();
        req.setUserId(salesOrderReq.getUserId());
        req.setStart(salesOrderReq.getStart());
        req.setSize(salesOrderReq.getLimit());
        req.setOrderSortType(salesOrderReq.getOrderSortType());
        req.setEntityType(salesOrderReq.getEntityType());
        req.setPaymentStatus(salesOrderReq.getPaymentStatus());
        req.setPaymentType(salesOrderReq.getPaymentType());
        if(salesOrderReq.getMinAmounForFilter() != null){
            req.setMinAmounForFilter(salesOrderReq.getMinAmounForFilter());
        }
        if(salesOrderReq.getMaxAmounForFilter() != null){
            req.setMaxAmounForFilter(salesOrderReq.getMaxAmounForFilter());
        }
        if(salesOrderReq.getTeamType() != null){
            req.setTeamType(salesOrderReq.getTeamType());
        }
        if(salesOrderReq.getAgentCode() != null){
            req.setAgentCode(salesOrderReq.getAgentCode());
        }
        if(salesOrderReq.getPaymentType() != null){
            req.setPaymentType(salesOrderReq.getPaymentType());
        }
        if(salesOrderReq.getPaymentThrough() != null){
            req.setPaymentThrough(salesOrderReq.getPaymentThrough());
        }
        if(ArrayUtils.isNotEmpty(salesOrderReq.getPaymentStatuses())){
            req.setPaymentStatuses(salesOrderReq.getPaymentStatuses());
        }

        if(salesOrderReq.getCreationTimeLowerLimit()!=null && salesOrderReq.getCreationTimeLowerLimit()>0l
        && salesOrderReq.getCreationTimeUpperLimit()!=null && salesOrderReq.getCreationTimeUpperLimit()>0l){
            req.setCreationTimeLowerLimit(salesOrderReq.getCreationTimeLowerLimit());
            req.setCreationTimeUpperLimit(salesOrderReq.getCreationTimeUpperLimit());
        }

        if(Boolean.TRUE.equals(salesOrderReq.getConditionalFiltering())){
            req.setConditionalFiltering(Boolean.TRUE);
        }

        List<Orders> orders = paymentManager.getOrders(req);

        if (orders == null) {
            orders = new ArrayList<>();
        }
        List<String> vedantuOrderIds = new ArrayList<>();
        for (Orders order : orders) {
            vedantuOrderIds.add(order.getId());
        }
        List<ExtTransactionPojo> extTransactions = paymentManager.getExtTransactionsFromOrderIds(vedantuOrderIds);
        Map<String, ExtTransactionPojo> orderIdExtMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(extTransactions)) {
            for (ExtTransactionPojo extTransactionPojo : extTransactions) {
                orderIdExtMap.put(extTransactionPojo.getVedantuOrderId(), extTransactionPojo);
            }
        }

        List<String> orderIds = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        List<SalesOrderInfo> salesOrderInfos = new ArrayList<>();
        for (Orders order : orders) {
            orderIds.add(order.getId());
            SalesOrderInfo info = mapper.map(order, SalesOrderInfo.class);
            List<SalesOrderItemInfo> orderItemInfos = new ArrayList<>();
            for (OrderedItem item : order.getItems()) {
                SalesOrderItemInfo salesOrderItemInfo = mapper.map(item, SalesOrderItemInfo.class);
                if (item.getEntityType() == EntityType.PLAN) {
                    PlanInfo plan = new PlanInfo();
                    plan.setName(item.getEntityId());
                    salesOrderItemInfo.setPlan(plan);
                }
                if (item.getEntityType() == EntityType.SESSION) {
                    if (item.getEntityId() != null) {
                        SessionInfo session = sessionManager.getSessionById(Long.parseLong(item.getEntityId()));
                        salesOrderItemInfo.setSession(session);
                    }
                }
                orderItemInfos.add(salesOrderItemInfo);
            }
            info.setOrderItems(orderItemInfos);
            if (order.getUserId() != null) {
                userIds.add(order.getUserId());
            }
            if(orderIdExtMap.containsKey(order.getId())){
                ExtTransactionPojo extTransactionPojo=orderIdExtMap.get(order.getId());
                info.setGatewayName(extTransactionPojo.getGatewayName());
                info.setTransactionStatus(extTransactionPojo.getStatus());
            }
            salesOrderInfos.add(info);
        }
        if (!orderIds.isEmpty()) {
            GetInstalmentsForOrdersReq getInstalmentsForOrdersReq = new GetInstalmentsForOrdersReq(orderIds,
                    PaymentStatus.PAID);
            String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getInstalmentsForOrdersReq);
            String url = "/payment/getInstalmentsForOrderIds?" + queryString;
            ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type mapType = new TypeToken<List<InstalmentInfo>>() {
            }.getType();
            List<InstalmentInfo> instalmentInfos = gson.fromJson(jsonString, mapType);
            Map<String, List<InstalmentInfo>> instalMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(instalmentInfos)) {
                for (InstalmentInfo instalmentInfo : instalmentInfos) {
                    if (!instalMap.containsKey(instalmentInfo.getOrderId())) {
                        instalMap.put(instalmentInfo.getOrderId(), new ArrayList<>());
                    }
                    List<InstalmentInfo> existing = instalMap.get(instalmentInfo.getOrderId());
                    existing.add(instalmentInfo);
                }
            }

            Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            for (SalesOrderInfo salesOrderInfo : salesOrderInfos) {
                if (instalMap.containsKey(salesOrderInfo.getId())) {
                    salesOrderInfo.setInstalmentInfos(instalMap.get(salesOrderInfo.getId()));
                }
                if (usersMap.containsKey(salesOrderInfo.getUserId())) {
                    salesOrderInfo.setUser(usersMap.get(salesOrderInfo.getUserId()));
                }
            }
        }
        return salesOrderInfos;
    }

    public String addAccountPancard(AddAccountPancardReq req) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/addAccountPancard", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }
}
