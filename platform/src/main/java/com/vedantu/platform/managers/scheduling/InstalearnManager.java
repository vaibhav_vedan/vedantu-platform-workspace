/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.scheduling;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.request.BuyItemsReq;
import com.vedantu.dinero.request.OrderItem;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.dinero.AccountManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.dinero.PricingManager;
import com.vedantu.platform.request.dinero.GetAccountInfoReq;
import com.vedantu.platform.response.dinero.GetAccountInfoRes;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.platform.pojo.subscription.GetPlanByIdResponse;
import com.vedantu.scheduling.enums.instalearn.InstaState;
import com.vedantu.scheduling.request.instalearn.AddInstaRequestReq;
import com.vedantu.scheduling.request.instalearn.GetInstaRequestsReq;
import com.vedantu.scheduling.request.instalearn.SetTeacherSubscriptionReq;
import com.vedantu.scheduling.request.instalearn.UpdateInstaRequestReq;
import com.vedantu.scheduling.response.instalearn.GetInstaRequestsRes;
import com.vedantu.scheduling.response.instalearn.SubmitInstaRequestRes;
import com.vedantu.scheduling.response.instalearn.TeacherSubscriptionResponse;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SessionModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class InstalearnManager {

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalearnManager.class);

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private PricingManager pricingManager;

    @Autowired
    private PaymentManager paymentManager;    

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private String schedulingEndpoint;
    private final String SESSION_TITLE = "Instant learning";

    private final Gson gson = new Gson();

    public InstalearnManager() {
        super();
    }
    
    @PostConstruct
    public void init() {
        schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    }        

    

    public SubmitInstaRequestRes addInstaRequest(AddInstaRequestReq addInstaRequestReq) throws VException {
        addInstaRequestReq.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/instalearn/addRequest", HttpMethod.POST,
                gson.toJson(addInstaRequestReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        SubmitInstaRequestRes submitInstaRequestRes = gson.fromJson(jsonString, SubmitInstaRequestRes.class);
        List<SubmitInstaRequestRes> submitInstaRequestReses = new ArrayList<>();
        submitInstaRequestReses.add(submitInstaRequestRes);
        fillUserAndBoardInfos(submitInstaRequestReses);        
        return submitInstaRequestRes;
    }

    public SubmitInstaRequestRes submitInstaRequest(UpdateInstaRequestReq updateInstaRequestReq) throws VException, IOException, AddressException {

        updateInstaRequestReq.verify();
        HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
        Long callingUserId = httpSessionData.getUserId();
        if (!Role.ADMIN.equals(httpSessionData.getRole())
                && (!callingUserId.equals(updateInstaRequestReq.getFrom())
                && !callingUserId.equals(updateInstaRequestReq.getTo()))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "User forbidden to use this API");
        }

//        Integer chargeRate;
//        Long sessionDuration;

        SubmitInstaRequestRes submitInstaRequestRes = fireSubmitReq(updateInstaRequestReq);
        
        if(InstaState.INIT.equals(updateInstaRequestReq.getData().getState())){
            return submitInstaRequestRes;
        }

        Long toUserId = updateInstaRequestReq.getTo();
        Long fromUserId = updateInstaRequestReq.getFrom();

        List<Long> userIds = new ArrayList<>();
        if (toUserId != null) {
            userIds.add(toUserId);
        }

        if (fromUserId != null) {
            userIds.add(fromUserId);
        }

        Map<Long, UserBasicInfo> userInfosMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        UserBasicInfo fromUser = userInfosMap.get(fromUserId);
        UserBasicInfo toUser = userInfosMap.get(toUserId);
        if (fromUser == null || toUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "Either fromuser or touser not found");
        }

        //TODO what if the charge rate changes or the student balance changes then the duration will no longer
        //be valid
        if (updateInstaRequestReq.getData().getState() == InstaState.INPROGRESS) {
            if (toUser.getRole().equals(Role.TEACHER)) {
//                chargeRate = getChargeRate(toUser);
//                sessionDuration = getDuration(chargeRate, fromUserId);
            } else {
//                chargeRate = getChargeRate(fromUser);
//                sessionDuration = getDuration(chargeRate, toUserId);
            }
//            updateInstaRequestReq.getData().setChargeRate(chargeRate);
//            updateInstaRequestReq.getData().setDuration(sessionDuration);
        }
        // Build Response
        

        submitInstaRequestRes.setFromUser(fromUser);
        submitInstaRequestRes.setToUser(toUser);

        //setting the board/subject name
        com.vedantu.platform.mongodbentities.board.Board board = boardManager.getBoardById(submitInstaRequestRes.getData().getBoardId());
        if (board != null) {
            submitInstaRequestRes.setSubjectName(board.getName());
        }

        // DO State related stuff
        InstaState state = submitInstaRequestRes.getData().getState();
        switch (state) {
            case ACCEPTED:
                //doing check conflict,buyitems,send emails,update proposal
                submitInstaRequestRes = _bookSession(submitInstaRequestRes, updateInstaRequestReq);
                //sending this only to from user
                sendInstantReqCommunicationtoFromUser(submitInstaRequestRes);
            case REJECTED:
            case INPROGRESS:
                //sending this only to to user
                sendInstantReqCommunicationtoToUser(submitInstaRequestRes);

                // Send SMS to teacher if he is subscribed
                Map<String, Object> payload3 = new HashMap<>();
                payload3.put("emailRecipientUser", submitInstaRequestRes.getToUser());//won't get used anyway
                payload3.put("submitInstaRequestRes", submitInstaRequestRes);
                payload3.put("subject", submitInstaRequestRes.getSubjectName());
                payload3.put("grade", submitInstaRequestRes.getData().getGrade());
                payload3.put("communicationType", CommunicationType.INSTALEARN_REQUEST_TO_TEACHER);
                AsyncTaskParams params3 = new AsyncTaskParams(AsyncTaskName.INSTALEARN_COMMUNICATION, payload3);
                asyncTaskFactory.executeTask(params3);
                break;
        }
        return submitInstaRequestRes;
    }

    public SubmitInstaRequestRes retryBooking(String id) throws VException, IOException, AddressException {
        SubmitInstaRequestRes submitInstaRequestRes = getRequestById(id, false);

        HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
        Long callingUserId = httpSessionData.getUserId();
        if (!Role.ADMIN.equals(httpSessionData.getRole())
                && (!callingUserId.equals(submitInstaRequestRes.getFrom())
                && !callingUserId.equals(submitInstaRequestRes.getTo()))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "User forbidden to use this API");
        }

        UpdateInstaRequestReq updateInstaRequestReq = new UpdateInstaRequestReq();
        updateInstaRequestReq.setFrom(submitInstaRequestRes.getFrom());
        updateInstaRequestReq.setTo(submitInstaRequestRes.getTo());
        updateInstaRequestReq.setCallingUserId(httpSessionData.getUserId());
        updateInstaRequestReq.setData(submitInstaRequestRes.getData());
        updateInstaRequestReq.setCallingUserId(httpSessionData.getUserId());
        updateInstaRequestReq.setCallingUserRole(httpSessionData.getRole());

        SubmitInstaRequestRes response = _bookSession(submitInstaRequestRes, updateInstaRequestReq);

        sendInstantReqCommunicationtoFromUser(submitInstaRequestRes);
        sendInstantReqCommunicationtoToUser(submitInstaRequestRes);
        return response;
    }

    public SubmitInstaRequestRes getRequestById(String id, boolean authorize) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/instalearn/getRequestById/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        SubmitInstaRequestRes submitInstaRequestRes = gson.fromJson(jsonString, SubmitInstaRequestRes.class);

        if (authorize) {
            //checking if the user is the owner of the request or is involved
            HttpSessionData httpSessionData = httpSessionUtils.getCurrentSessionData();
            Long callingUserId = httpSessionData.getUserId();
            if (!Role.ADMIN.equals(httpSessionData.getRole())
                    && (!callingUserId.equals(submitInstaRequestRes.getFrom())
                    && !callingUserId.equals(submitInstaRequestRes.getTo()))) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                        "User forbidden to use this API");
            }
        }

        List<SubmitInstaRequestRes> submitInstaRequestReses = new ArrayList<>();
        submitInstaRequestReses.add(submitInstaRequestRes);
        fillUserAndBoardInfos(submitInstaRequestReses);
        return submitInstaRequestRes;
    }

    public GetInstaRequestsRes getInstaRequests(GetInstaRequestsReq getInstaRequestsReq) throws VException, IllegalArgumentException, IllegalAccessException {
        getInstaRequestsReq.verify();
        httpSessionUtils.checkIfAllowed(getInstaRequestsReq.getUserId(), null, true);
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getInstaRequestsReq);
        String getAllRequestsUrl = schedulingEndpoint + "/instalearn/getInstaRequests";
        if (!StringUtils.isEmpty(queryString)) {
            getAllRequestsUrl += "?" + queryString;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(getAllRequestsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GetInstaRequestsRes getInstaRequestsRes = gson.fromJson(jsonString, GetInstaRequestsRes.class);
        List<SubmitInstaRequestRes> submitInstaRequestReses = getInstaRequestsRes.getList();
        fillUserAndBoardInfos(submitInstaRequestReses);
        return getInstaRequestsRes;
    }

    public String getTeacherILRequestsCount(Long id) throws VException {
        httpSessionUtils.checkIfAllowed(id, null, true);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/instalearn/getTeacherILRequestsCount/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    public TeacherSubscriptionResponse addTeacherSubscription(SetTeacherSubscriptionReq setTeacherSubscriptionReq) throws VException {
        setTeacherSubscriptionReq.verify();
        httpSessionUtils.checkIfAllowed(setTeacherSubscriptionReq.getUserId(), null, true);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/instalearn/setTeacherSubscription",
                HttpMethod.POST, gson.toJson(setTeacherSubscriptionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        TeacherSubscriptionResponse teacherSubscriptionResponse = gson.fromJson(jsonString, TeacherSubscriptionResponse.class);
        return teacherSubscriptionResponse;
    }

    public TeacherSubscriptionResponse getTeacherSubscription(Long id, boolean authorize) throws VException {
        if (authorize) {
            httpSessionUtils.checkIfAllowed(id, null, true);
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/instalearn/getTeacherSubscription/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        TeacherSubscriptionResponse teacherSubscriptionResponse = gson.fromJson(jsonString, TeacherSubscriptionResponse.class);
        return teacherSubscriptionResponse;
    }

    private SubmitInstaRequestRes _bookSession(SubmitInstaRequestRes response, UpdateInstaRequestReq updateInstaRequestReq) throws VException, IOException, AddressException {
        try {
            BuyItemsReq buyItemsReq = getBuyItemsReq(response);
            SessionSlot sessionSlot = buyItemsReq.getItems().get(0).getSchedule().getSessionSlots().get(0);
            if (hasConflict()) {
                throw new ConflictException(ErrorCode.SESSION_INVALID_TIME_PERIOD, "Conflict found");
            }
            if(null != buyItemsReq && null != buyItemsReq.getUserAgent() 
            		&& StringUtils.isNotEmpty(buyItemsReq.getUserAgent()) && null != updateInstaRequestReq.getUserAgent())
            	buyItemsReq.setUserAgent(updateInstaRequestReq.getUserAgent());
            paymentManager.buyItems(buyItemsReq);

            //from user
            Map<String, Object> payload = new HashMap<>();
            payload.put("emailRecipientUser", response.getFromUser());
            payload.put("submitInstaRequestRes", response);
            payload.put("sessionSlot", sessionSlot);
            payload.put("subject", response.getSubjectName());
            payload.put("grade", response.getData().getGrade());
            payload.put("communicationType", CommunicationType.INSTALEARN_SESSION_SCHEDULE);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.INSTALEARN_COMMUNICATION, payload);
            asyncTaskFactory.executeTask(params);

            //to user
            Map<String, Object> payload2 = new HashMap<>(payload);
            payload2.put("emailRecipientUser", response.getToUser());
            AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.INSTALEARN_COMMUNICATION, payload2);
            asyncTaskFactory.executeTask(params2);

            response.getData().setState(InstaState.SESSION_BOOK_STATUS);
            updateInstaRequestReq.getData().setState(InstaState.SESSION_BOOK_STATUS);
            fireSubmitReq(updateInstaRequestReq);
        } catch (VException exception) {
            if (ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE.name().equals(exception.getErrorCode())) {
                response.getData().setState(InstaState.PAYMENT_PENDING);
                updateInstaRequestReq.getData().setState(InstaState.PAYMENT_PENDING);
                fireSubmitReq(updateInstaRequestReq);
            } else {
                throw exception;
            }
        }
        return response;
    }

    private BuyItemsReq getBuyItemsReq(SubmitInstaRequestRes submitInstaRequestRes) throws VException {
        Long requestUserId = Long.parseLong(submitInstaRequestRes.getCreatedBy());
        UserBasicInfo studentBasicInfo = submitInstaRequestRes.getToUser();
        UserBasicInfo teacherBasicInfo = submitInstaRequestRes.getFromUser();
        if (!studentBasicInfo.getRole().equals(Role.STUDENT)) {
            studentBasicInfo = submitInstaRequestRes.getFromUser();
            teacherBasicInfo = submitInstaRequestRes.getToUser();
        }
        GetAccountInfoRes getAccountInfoRes = accountManager.getAccountInfo(new GetAccountInfoReq(studentBasicInfo.getUserId()));
        GetPlanByIdResponse getPlanByIdResponse = pricingManager.getPlanByHours(teacherBasicInfo.getUserId(), 1.0);
        BuyItemsReq buyItemsReq = new BuyItemsReq();
        int chargeRate = getPlanByIdResponse.getPrice().intValue();
        int balance = getAccountInfoRes.getBalance();
        Long durationPossible = (((long) balance * 3600000) / chargeRate);
        logger.info("durationPossible: " + durationPossible + ", chargeRate:" + chargeRate + ", balance:" + balance);

        if (durationPossible < 1800000) {
            throw new ForbiddenException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE,
                    "Not sufficient Balance to schedule more than 30 minutes session");
        }

        if (durationPossible > 3600000) {
            durationPossible = 3600000l;
        }
        buyItemsReq.setUseAccountBalance(true);
        buyItemsReq.setCallingUserId(requestUserId);
        OrderItem orderItem = new OrderItem();
        orderItem.setCount(1);
        orderItem.setEntityType(EntityType.INSTALEARN);
        orderItem.setEntityId(getPlanByIdResponse.getId());
        orderItem.setHours(durationPossible);
        orderItem.setModel(SessionModel.IL);
        orderItem.setRenew(false);
        orderItem.setBoardId(submitInstaRequestRes.getData().getBoardId());
        SessionSchedule sessionSchedule = new SessionSchedule();
        List<SessionSlot> sessionSlots = new ArrayList<>();
        Long sessionStartTime = System.currentTimeMillis() + 10000l;
        Long sessionEndTime = sessionStartTime + durationPossible;

        SessionSlot sessionSlot = new SessionSlot(sessionStartTime, sessionEndTime,
                null, SESSION_TITLE, null, false);
        sessionSlots.add(sessionSlot);
        sessionSchedule.setSessionSlots(sessionSlots);
        sessionSchedule.setNoOfWeeks(1l);
        orderItem.setSchedule(sessionSchedule);
        orderItem.setTeacherId(teacherBasicInfo.getUserId());
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);
        buyItemsReq.setItems(orderItems);
        buyItemsReq.setCallingUserId(studentBasicInfo.getUserId());
        buyItemsReq.setCallingUserRole(Role.STUDENT);
        return buyItemsReq;
    }

    private void sendInstantReqCommunicationtoFromUser(SubmitInstaRequestRes submitInstaRequestRes) {
        Map<String, Object> payload = getPayloadForInstantReqCommunication(submitInstaRequestRes);
        payload.put("emailRecipientUser", submitInstaRequestRes.getFromUser());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.INSTALEARN_COMMUNICATION, payload);
        asyncTaskFactory.executeTask(params);
    }

    private void sendInstantReqCommunicationtoToUser(SubmitInstaRequestRes submitInstaRequestRes) {
        Map<String, Object> payload = getPayloadForInstantReqCommunication(submitInstaRequestRes);
        payload.put("emailRecipientUser", submitInstaRequestRes.getToUser());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.INSTALEARN_COMMUNICATION, payload);
        asyncTaskFactory.executeTask(params);
    }

    private Map<String, Object> getPayloadForInstantReqCommunication(SubmitInstaRequestRes submitInstaRequestRes) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("submitInstaRequestRes", submitInstaRequestRes);
        payload.put("subject", submitInstaRequestRes.getSubjectName());
        payload.put("grade", submitInstaRequestRes.getData().getGrade());
        payload.put("communicationType", CommunicationType.INSTANT_SESSION_REQUEST);
        return payload;
    }

    private boolean hasConflict() {
        return false;
    }

    private SubmitInstaRequestRes fireSubmitReq(UpdateInstaRequestReq updateInstaRequestReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/instalearn/submitRequest", HttpMethod.POST,
                gson.toJson(updateInstaRequestReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String firstSubmitResStr = resp.getEntity(String.class);
        SubmitInstaRequestRes submitInstaRequestRes = gson.fromJson(firstSubmitResStr, SubmitInstaRequestRes.class);
        return submitInstaRequestRes;
    }

    private List<SubmitInstaRequestRes> fillUserAndBoardInfos(List<SubmitInstaRequestRes> submitInstaRequestReses) {
        List<Long> userIds = new ArrayList<>();
        List<Long> boardIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(submitInstaRequestReses)) {
            for (SubmitInstaRequestRes submitInstaRequestRes : submitInstaRequestReses) {
                if(submitInstaRequestRes.getFrom()!=null){
                    userIds.add(submitInstaRequestRes.getFrom());
                }
                if(submitInstaRequestRes.getTo()!=null){
                    userIds.add(submitInstaRequestRes.getTo());
                }
                if(submitInstaRequestRes.getData().getBoardId()!=null){
                    boardIds.add(submitInstaRequestRes.getData().getBoardId());
                }
            }
            Map<Long, UserBasicInfo> userInfosMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
            Map<Long, Board> boardInfosMap = fosUtils.getBoardInfoMap(boardIds);
            for (SubmitInstaRequestRes submitInstaRequestRes : submitInstaRequestReses) {
                submitInstaRequestRes.setFromUser(userInfosMap.get(submitInstaRequestRes.getFrom()));
                submitInstaRequestRes.setToUser(userInfosMap.get(submitInstaRequestRes.getTo()));
                if (boardInfosMap.containsKey(submitInstaRequestRes.getData().getBoardId())) {
                    submitInstaRequestRes.setSubjectName(boardInfosMap.get(submitInstaRequestRes.getData().getBoardId()).getName());
                }
            }
        }
        return submitInstaRequestReses;
    }
}
