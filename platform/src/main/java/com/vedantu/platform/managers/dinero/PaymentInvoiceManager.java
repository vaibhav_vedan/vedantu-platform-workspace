package com.vedantu.platform.managers.dinero;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.pojo.PaymentInvoiceInfo;
import com.vedantu.dinero.pojo.PaymentInvoiceUserInfo;
import com.vedantu.dinero.request.GetPaymentInvoiceReq;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.dozer.DozerBeanMapper;

@Service
public class PaymentInvoiceManager {

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings({ "static-access" })
	private Logger logger = logFactory.getLogger(PaymentInvoiceManager.class);

	private static Gson gson = new Gson();

        @Autowired
        private DozerBeanMapper mapper; 

	private static final Type PAYMENT_INVOICE_INFO_LIST_TYPE = new TypeToken<List<PaymentInvoiceInfo>>() {
	}.getType();

	public PaymentInvoiceInfo getPaymentInvoiceById(String id) throws VException {
		String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
		String url = "/invoice/" + id;
		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Json response : " + jsonString);
		return gson.fromJson(jsonString, PaymentInvoiceInfo.class);
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseBody
	public List<PaymentInvoiceInfo> getPaymentInvoices(@ModelAttribute GetPaymentInvoiceReq getPaymentInvoiceReq)
			throws VException {
		String getSessionsUrl = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT") + "/invoice/get" + "?"
				+ WebUtils.INSTANCE.createQueryStringOfObject(getPaymentInvoiceReq);
		logger.info("Url : " + getSessionsUrl);
		ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);

		List<PaymentInvoiceInfo> paymentInvoices = gson.fromJson(jsonString, PAYMENT_INVOICE_INFO_LIST_TYPE);

		// Fill user basic info
		if (!CollectionUtils.isEmpty(paymentInvoices)) {
			Set<Long> allUserIds = new HashSet<>();
			Set<Long> fromUserIds = paymentInvoices.stream().map(PaymentInvoiceInfo::getFromUserId)
					.filter(Objects::nonNull).collect(Collectors.toSet());
			if (!CollectionUtils.isEmpty(fromUserIds)) {
				allUserIds.addAll(fromUserIds);
			}

			Set<Long> toUserIds = paymentInvoices.stream().map(PaymentInvoiceInfo::getToUserId).filter(Objects::nonNull)
					.collect(Collectors.toSet());
			if (!CollectionUtils.isEmpty(toUserIds)) {
				allUserIds.addAll(toUserIds);
			}

			Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(allUserIds, false);
			for (PaymentInvoiceInfo paymentInvoiceInfo : paymentInvoices) {
				if (paymentInvoiceInfo.getFromUserId() != null
						&& userMap.get(paymentInvoiceInfo.getFromUserId()) != null) {
					paymentInvoiceInfo.setFromUserInfo(
							mapper.map(userMap.get(paymentInvoiceInfo.getFromUserId()), PaymentInvoiceUserInfo.class));
				}

				if (paymentInvoiceInfo.getToUserId() != null && userMap.get(paymentInvoiceInfo.getToUserId()) != null) {
					paymentInvoiceInfo.setToUserInfo(
							mapper.map(userMap.get(paymentInvoiceInfo.getToUserId()), PaymentInvoiceUserInfo.class));
				}
			}
		}

		return paymentInvoices;
	}

}
