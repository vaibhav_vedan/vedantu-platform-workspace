package com.vedantu.platform.managers.offering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.request.UpdateCustomPlanRequest;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.offering.pojo.OfferingInfo;
import com.vedantu.offering.pojo.TeacherPlanInfo;
import com.vedantu.offering.request.CreateOfferingRequest;
import com.vedantu.offering.request.EditOfferingRequest;
import com.vedantu.offering.request.GetOfferingRequest;
import com.vedantu.offering.request.GetOfferingsRequest;
import com.vedantu.offering.response.CreateOfferingResponse;
import com.vedantu.offering.response.EditOfferingResponse;
import com.vedantu.offering.response.GetOfferingResponse;
import com.vedantu.offering.response.GetOfferingsResponse;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.board.BoardDAO;
import com.vedantu.platform.managers.dinero.PricingManager;
import com.vedantu.platform.dao.offering.OfferingDao;
import com.vedantu.platform.mongodbentities.offering.Offering;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.util.BoardUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.IBoardAware;
import com.vedantu.util.enums.Scope;
import org.dozer.DozerBeanMapper;

@Service
public class OfferingManager {

    @Autowired
    private DozerBeanMapper mapper; 

    @Autowired
    OfferingDao offeringDao;

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(OfferingManager.class);

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private UserManager userManager;

    @Autowired
    private PricingManager pricingManager;

    @Autowired
    private BoardUtils boardUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    public CreateOfferingResponse createOffering(CreateOfferingRequest req)
            throws ConflictException {

        com.vedantu.offering.pojo.Offering offeringPojo = req.toOffering();

        Offering offering = mapper.map(offeringPojo, Offering.class);

        List<TeacherPlanInfo> plans = updateTeacherPlans(offering, null);
        offering.setTeacherPlans(plans);
        offering.setLastUpdated(System.currentTimeMillis());
        //if (req.getCallingUserId() != null) {
        //    offering.setLastUpdatedBy(req.getCallingUserId().toString());
        //}
        offeringDao.save(offering, req.getCallingUserId() != null ? req.getCallingUserId().toString() : null);
        CreateOfferingResponse createOfferingRes = new CreateOfferingResponse(
                offeringPojo);
        return createOfferingRes;
    }

    public EditOfferingResponse editOffering(EditOfferingRequest editOfferingReq)
            throws VException, Throwable {

        logger.info("editOffering" + editOfferingReq.toString());

        editOfferingReq.verify();
        Offering previousOffering = offeringDao.getById(editOfferingReq
                .getOfferingId());
        Offering offering = updateOfferingForEditRequest(previousOffering,
                editOfferingReq);
        offering.setLastUpdated(System.currentTimeMillis());
        //if (editOfferingReq.getCallingUserId() != null) {
        //    offering.setLastUpdatedBy(editOfferingReq.getCallingUserId().toString());
        //}

        offeringDao.save(offering, editOfferingReq.getCallingUserId() != null ? editOfferingReq.getCallingUserId().toString() : null);
        com.vedantu.offering.pojo.Offering offeringPojo = mapper.map(offering,
                com.vedantu.offering.pojo.Offering.class);
        EditOfferingResponse editOfferingRes = new EditOfferingResponse(
                offeringPojo);

        // Update the plan if required.
        List<TeacherPlanInfo> plans = updateTeacherPlans(offering,
                previousOffering);
        offering.setTeacherPlans(plans);
        offering.setLastUpdated(System.currentTimeMillis());
        //if (editOfferingReq.getCallingUserId() != null) {
        //    offering.setLastUpdatedBy(editOfferingReq.getCallingUserId().toString());
        //}
        offeringDao.save(offering, editOfferingReq.getCallingUserId() != null ? editOfferingReq.getCallingUserId().toString() : null);
        logger.info("editOffering", editOfferingRes);
        return editOfferingRes;
    }

    private Offering updateOfferingForEditRequest(Offering offering,
            EditOfferingRequest editOfferingReq) throws VException, Throwable {
        Boolean edited = false;
        if (null == offering) {
            throw new VException(ErrorCode.OFFERING_NOT_FOUND, null);
        }
        List<String> editList = editOfferingReq.getEditList();
        if (editList.contains(Offering.Constants.TITLE)
                && StringUtils.isNotEmpty(editOfferingReq.getTitle())) {
            offering.setTitle(editOfferingReq.getTitle());
            edited = true;
        }
        if (editList.contains(Offering.Constants.GRADES)
                && CollectionUtils.isNotEmpty(editOfferingReq.getGrades())) {
            offering.setGrades(editOfferingReq.getGrades());
            edited = true;
        }
        if (editList.contains(Offering.Constants.BOARD_IDS)
                && CollectionUtils.isNotEmpty(editOfferingReq.getBoardIds())) {
            offering.setBoardIds(new HashSet<String>(editOfferingReq
                    .getBoardIds()));
            edited = true;
        }
        if (editList.contains(Offering.Constants.DESCRIPTION)
                && StringUtils.isNotEmpty(editOfferingReq.getDescription())) {
            offering.setDescription(editOfferingReq.getDescription());
            edited = true;
        }
        if (editList.contains(Offering.Constants.TARGET_AUDIENCE_TEXT)
                && StringUtils.isNotEmpty(editOfferingReq
                        .getTargetAudienceText())) {
            offering.setTargetAudienceText(editOfferingReq
                    .getTargetAudienceText());
            edited = true;
        }
        if (editList.contains(Offering.Constants.PRICE)
                && editOfferingReq.getPrice() != null) {
            offering.setPrice(editOfferingReq.getPrice());
            edited = true;
        }
        if (editList.contains(Offering.Constants.DISPLAY_PRICE)
                && editOfferingReq.getDisplayPrice() != null) {
            offering.setDisplayPrice(editOfferingReq.getDisplayPrice());
            edited = true;
        }
        if (editList.contains(Offering.Constants.TAGS)) {
            offering.setTags(editOfferingReq.getTags());
            edited = true;
        }
        if (editList.contains(Offering.Constants.EXAM_TAGS)) {
            offering.setExamTags(editOfferingReq.getExamTags());
            edited = true;
        }
        if (editList.contains(Offering.Constants.RECOMMENDED_SCHEDULE)
                && StringUtils.isNotEmpty(editOfferingReq
                        .getRecommendedSchedule())) {
            offering.setRecommendedSchedule(editOfferingReq
                    .getRecommendedSchedule());
            edited = true;
        }
        if (editList.contains(Offering.Constants.TOTAL_DAYS)
                && editOfferingReq.getTotalDays() != null) {
            offering.setTotalDays(editOfferingReq.getTotalDays());
            edited = true;
        }
        if (editList.contains(Offering.Constants.TOTAL_HOURS)
                && editOfferingReq.getTotalHours() != null) {
            offering.setTotalHours(editOfferingReq.getTotalHours());
            edited = true;
        }
        if (editList.contains(Offering.Constants.TYPE)
                && editOfferingReq.getType() != null) {
            offering.setType(editOfferingReq.getType());
            edited = true;
        }
        if (editList.contains(Offering.Constants.STATE)
                && editOfferingReq.getState() != null) {
            offering.setState(editOfferingReq.getState());
            edited = true;
        }
        if (editList.contains(Offering.Constants.IMAGE_URLS)) {
            offering.setImageUrls(editOfferingReq.getImageUrls());
            edited = true;
        }
        if (editList.contains(Offering.Constants.VIDEO_URLS)) {
            offering.setVideoUrls(editOfferingReq.getVideoUrls());
            edited = true;
        }
        if (editList.contains(Offering.Constants.CONTENTS)) {
            offering.setContents(editOfferingReq.getContents());
            edited = true;
        }
        if (editList.contains(Offering.Constants.CATEGORIES)) {
            offering.setCategories(editOfferingReq.getCategories());
            edited = true;
        }
        if (editList.contains(Offering.Constants.PARENT_TOPICS)) {
            offering.setParentTopics(editOfferingReq.getParentTopics());
            edited = true;
        }
        if (editList.contains(Offering.Constants.PRIORITY)) {
            offering.setPriority(editOfferingReq.getPriority());
            edited = true;
        }
        if (editList.contains(Offering.Constants.TAKEAWAYS)) {
            offering.setTakeAways(editOfferingReq.getTakeAways());
            edited = true;
        }

        if (editList.contains(Offering.Constants.SCOPE)) {
            offering.setScope(editOfferingReq.getScope());
            edited = true;
        }
        if (editList.contains(Offering.Constants.IS_FEATURED)) {
            offering.setIsFeatured(editOfferingReq.getIsFeatured());
            edited = true;
        }
        if (editList.contains(Offering.Constants.OFFERING_LENGTH)) {
            offering.setOfferingLength(editOfferingReq.getOfferingLength());
            edited = true;
        }
        if (editList.contains(Offering.Constants.OFFERING_SUB_TYPE)) {
            offering.setSubtypes(editOfferingReq.getSubtypes());
            edited = true;
        }

        if (editList.contains(Offering.Constants.TEACHER_IDS)) {

            List<String> oldTeacherIds = offering.getTeacherIds() == null ? null
                    : new ArrayList<>(offering.getTeacherIds());

            offering.setTeacherIds(editOfferingReq.getTeacherIds());
            edited = true;
            // IDENTIFY NEWLY ADDED TEACHERS
            List<String> newTeacherIds = new ArrayList<>(
                    offering.getTeacherIds());
            if (oldTeacherIds != null) {
                for (String teacherId : oldTeacherIds) {
                    newTeacherIds.remove(teacherId);
                }
            }
            logger.info("oldTeacherIds : " + oldTeacherIds
                    + ", newTeacherIds: " + newTeacherIds);
            // NOW SEND addToCourse sms & email to newly added teacher
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("teacherIds", newTeacherIds);
            payload.put("offering", offering);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ADD_TO_OFFERING_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
            //emailManager.sendAddToOfferingEmail(newTeacherIds, offering);
        }
        return offering;
    }

    public GetOfferingResponse getOffering(GetOfferingRequest getOfferingReq)
            throws VException {

        Offering offering = offeringDao.getById(getOfferingReq.getOfferingId());
        if (offering == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Offering not found");
        }
        GetOfferingResponse getOfferingRes = new GetOfferingResponse();

        Map<Long, com.vedantu.platform.mongodbentities.board.Board> boardInfoMapTemp = boardDAO
                .getBoardsMap(offering.getBoardIdsInLong());
        Map<Long, Board> boardInfoMap = new HashMap<>();
        for (Long id : boardInfoMapTemp.keySet()) {
            Board board = mapper.map(boardInfoMapTemp.get(id), Board.class);
            boardInfoMap.put(id, board);
        }

        Map<String, UserBasicInfo> users = fosUtils.getUserBasicInfosMap(offering.getTeacherIds(), false);

        com.vedantu.offering.pojo.Offering offeringPojo = mapper.map(offering,
                com.vedantu.offering.pojo.Offering.class);

        OfferingInfo offeringInfo = new OfferingInfo(offeringPojo, users);
        offeringInfo.setBoards(boardUtils.toBoardTree(offering, boardInfoMap));
        getOfferingRes.setOfferingInfo(offeringInfo);

        // if (getOfferingReq.getUserId() != null) {
        // Subscription subscription =
        // subscriptionController.getSubscriptions(teacherId, studentId, active,
        // model, beforeTime, afterTime, start, size, subModel,
        // lastUpdatedTime).getSubscription(getOfferingReq.getUserId(),
        // offering.getId());
        // if (subscription != null && getOfferingRes.getOfferingInfo() != null)
        // {
        // getOfferingRes.getOfferingInfo().setSubscription(subscription);
        // PlanInfo planInfo = subscription.getPlanId() == null ? null
        // : PlanManager.INSTANCE.getPlan(subscription.getPlanId());
        // getOfferingRes.getOfferingInfo().setPlanInfo(planInfo);
        // ISessionDAO sessionDAO = DAOFactory.INSTANCE.getSessionDAO();
        // List<SessionInfo> sessionsInfo = sessionDAO.getSessionInfos(null,
        // null, subscription.getId(), null,
        // null, null);
        // Collections.sort(sessionsInfo, new SessionInfoComparator());
        // getOfferingRes.setSessionsInfo(sessionsInfo);
        // }
        // }
        return getOfferingRes;
    }

    public GetOfferingsResponse getOfferings(GetOfferingsRequest getOfferingsReq)
            throws VException {

        logger.info("getOfferings" + getOfferingsReq);
        List<String> includeScopes = new ArrayList<>();
        includeScopes.add(Scope.PUBLIC.name());
        User user = (getOfferingsReq.getCallingUserId() == null || getOfferingsReq.getCallingUserId() == 0) ? null
                : userManager.getUserById(getOfferingsReq.getCallingUserId());

        if (user != null && Role.ADMIN.equals(user.getRole())) {
            includeScopes.add(Scope.PRIVATE.name());
        }

        if ((getOfferingsReq.getBoardId() == null || getOfferingsReq
                .getBoardId().isEmpty())
                && StringUtils.isNotEmpty(getOfferingsReq.getBoardSlug())) {
            com.vedantu.platform.mongodbentities.board.Board board = boardDAO
                    .getBoardByField("slug", getOfferingsReq.getBoardSlug());
            if (board != null) {
                getOfferingsReq.setBoardId(board.getId().toString());
            }
        }

        List<Offering> offerings;
        if (CollectionUtils.isEmpty(getOfferingsReq.getOfferingIds())) {
            offerings = offeringDao.get(null,
                    getOfferingsReq.getOfferingType(), getOfferingsReq
                    .getTags(), getOfferingsReq.getExam(),
                    getOfferingsReq.getGrade(), getOfferingsReq.getBoardId(),
                    null, getOfferingsReq.getCategory(), null, getOfferingsReq
                    .getOfferingState(), getOfferingsReq.getQuery(),
                    null, null, getOfferingsReq.getSubtypes(), includeScopes,
                    getOfferingsReq.isFeatured(), null, null, getOfferingsReq
                    .getStart(), getOfferingsReq.getSize());
        } else {
            offerings = offeringDao.get(getOfferingsReq.getOfferingIds(), null,
                    null, null, null, null, null, null, null, null, null, null,
                    null, null, null, false, null, null, null, null);
        }

        GetOfferingsResponse getOfferingsRes = new GetOfferingsResponse();
        Set<Long> boardIds = getBoardIds(offerings);

        Map<Long, Board> boardInfoMap = boardDAO
                .getBoardsMapInBoardPojo(boardIds);

        List<OfferingInfo> offeringInfoList = new ArrayList<>();

        Set<String> teacherIds = getTeacherIds(offerings);
        Map<String, UserBasicInfo> teacherInfosMap = fosUtils.getUserBasicInfosMap(teacherIds, false);

        for (Offering offering : offerings) {
            OfferingInfo offeringInfo;
            com.vedantu.offering.pojo.Offering offeringPojo = mapper.map(
                    offering, com.vedantu.offering.pojo.Offering.class);
            if (Boolean.TRUE.equals(getOfferingsReq.getOnlyRequiredFields())) {
                offeringInfo = new OfferingInfo(offeringPojo, teacherInfosMap);
            } else {
                offeringInfo = new OfferingInfo(offeringPojo, teacherInfosMap);
            }
            offeringInfo.setBoards(boardUtils.toBoardTree(offering, boardInfoMap));
            offeringInfoList.add(offeringInfo);
        }
        getOfferingsRes.setOfferingInfoList(offeringInfoList);

        return getOfferingsRes;
    }

    public List<TeacherPlanInfo> updateTeacherPlans(Offering offering,
            Offering previousOffering) {
        logger.info("updateTeacherPlans - " + offering.toString());
        List<TeacherPlanInfo> previousPlans = new ArrayList<>();
        if (previousOffering != null) {
            previousPlans = previousOffering.getTeacherPlans();
        }
        Map<String, String> previousPlanMap = TeacherPlanInfo
                .fetchTeacherIdMap(previousPlans);
        List<String> teacherIds = offering.getTeacherIds();

        List<TeacherPlanInfo> updatedPlans = new ArrayList<>();
        if (!CollectionUtils.isEmpty(teacherIds)
                && offering.getTotalHours() != null
                && offering.getTotalHours() > 0) {
            for (String teacherId : teacherIds) {
                Long teacherIdLong = Long.parseLong(teacherId);
                String planId = previousPlanMap.containsKey(String
                        .valueOf(teacherIdLong)) ? previousPlanMap.get(String
                        .valueOf(teacherIdLong)) : null;
                UpdateCustomPlanRequest updateCustomPlanRequest = new UpdateCustomPlanRequest(
                        planId, teacherIdLong, offering.getTotalHours(),
                        offering.getPrice(), false);
                try {
                    planId = pricingManager
                            .updateCustomPlan(updateCustomPlanRequest);
                    if (StringUtils.isEmpty(planId)) {
                        logger.error("PlanId is empty for request - "
                                + updateCustomPlanRequest.toString());
                        continue;
                    }
                } catch (Exception ex) {
                    logger.error("updateTeacherPlans" + ex.getMessage());
                }
                updatedPlans.add(new TeacherPlanInfo(teacherIdLong, planId));
            }
        }
        logger.info("updateTeacherPlans - " + offering.toString());
        return updatedPlans;
    }

    protected Set<Long> getBoardIds(
            Collection<? extends IBoardAware> boardAwares) {
        Set<Long> boardIds = new HashSet<Long>();
        if (boardAwares != null) {
            for (IBoardAware boardAware : boardAwares) {
                if (boardAware._getBoardIds() != null) {
                    boardIds.addAll(boardAware._getBoardIds());
                }
            }
        }
        return boardIds;
    }

    protected Set<String> getTeacherIds(
            Collection<? extends Offering> offerings) {
        Set<String> teacherIds = new HashSet<>();
        if (offerings != null) {
            for (Offering offering : offerings) {
                if (offering.getTeacherIds() != null) {
                    teacherIds.addAll(offering.getTeacherIds());
                }
            }
        }
        return teacherIds;
    }

    // userIds--> teacherIds
//	private void sendAddToOfferingEmail(List<String> userIds, Offering offering)
//			throws Throwable, VException {
//		logger.info("sendAddToOfferingEmail" + userIds);
//		if (CollectionUtils.isEmpty(userIds)) {
//			return;
//		}
//
//		for (String userId : userIds) {
//			User user = userManager.getUserById(Long.parseLong(userId));
//                    if (Role.TEACHER.equals(user.getRole())) {
//				try {
//					emailManager.sendOfferingRelatedEmail(user, offering,
//							CommunicationType.OFFERING_ADD_TEACHER, null, null);
//				} catch (AddressException | NotFoundException | IOException e) {
//					logger.error("sendAddToOfferingEmail" + e.getMessage());
//                    }
//		}
//		}
//		logger.info("sendAddToOfferingEmail");
//	}
    public List<Long> getOfferingTeacherIds(Long offeringId)
            throws VException {
        Offering offering = offeringDao.getById(offeringId);
        if (offering == null) {
            throw new VException(ErrorCode.NOT_FOUND_ERROR,
                    "Offering not found");
        } else {
            return offering.getTeacherIdsInLong();
        }
    }

    public List<Offering> getTeacherOfferings(Long userId) {
        return offeringDao.getTeacherOfferings(userId);
    }

    public Offering getOfferingById(Long offeringId) {
        return offeringDao.getById(offeringId);
    }
}
