/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.platform.dao.AdminAccessDAO;
import com.vedantu.platform.mongodbentities.AdminAccess;
import com.vedantu.platform.request.AdminAccessReq;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 *
 * @author ajith
 */
@Service
public class AdminAccessManager {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(AdminAccessManager.class);

    @Autowired
    private AdminAccessDAO adminAccessDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public AdminAccess createUpdate(AdminAccessReq req) throws BadRequestException {
        req.verify();
        AdminAccess reqEntity = mapper.map(req, AdminAccess.class);
        AdminAccess existingEntity = adminAccessDAO.getByAdminId(req.getAdminId());
        if (existingEntity == null) {
            existingEntity = reqEntity;
        } else {
            existingEntity.setFeatures(req.getFeatures());
        }
        adminAccessDAO.create(existingEntity);
        return existingEntity;
    }

    public AdminAccess getAdminAccess(Long adminId) {
        AdminAccess existingEntity = adminAccessDAO.getByAdminId(adminId);
        if (existingEntity == null) {
            existingEntity = new AdminAccess(adminId);
        }
        return existingEntity;
    }
}
