package com.vedantu.platform.managers.subscription;

import java.lang.reflect.Type;
import java.util.*;

import com.vedantu.User.Role;
import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.request.AddCouponRedeemEntryReq;
import com.vedantu.dinero.request.ValidateCouponReq;
import com.vedantu.dinero.response.RedeemCouponInfo;
import com.vedantu.lms.cmds.enums.VideoType;
import com.vedantu.lms.cmds.pojo.TestAndAttemptDetails;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.dinero.CouponManager;
import com.vedantu.platform.managers.lms.CMDSTestManager;
import com.vedantu.platform.pojo.subscription.TrialReq;
import com.vedantu.platform.response.dinero.ValidateCouponRes;
import com.vedantu.subscription.response.BundleEnrollmentResp;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.scheduling.pojo.session.ReferenceType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.BundleContentType;
import com.vedantu.subscription.pojo.*;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.response.VideoInfo;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.OTFWebinarSession;
import com.vedantu.onetofew.pojo.SessionPojoWithRefId;
import com.vedantu.onetofew.request.UpdateWebinarSessionsAttendenceReq;
import com.vedantu.onetofew.request.CancelOTFWebinarSessionsReq;
import com.vedantu.onetofew.request.GetOTFWebinarSessionsReq;
import com.vedantu.onetofew.request.OTFWebinarSessionReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.filemgmt.FileMgmtManager;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.managers.onetofew.OTFBundleManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.scheduling.CalendarManager;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SlotCheckPojo;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.TestBundleContentInfo;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.VideoDetails;
import com.vedantu.subscription.pojo.WebinarBundleContentInfo;
import com.vedantu.subscription.request.AddEditBundleReq;
import com.vedantu.subscription.request.AddEditBundleSessionReq;
import com.vedantu.subscription.request.AddEditBundleWebinarReq;
import com.vedantu.subscription.request.AddVideoAnalyticsDataReq;
import com.vedantu.subscription.request.CheckBundleEnrollmentReq;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.request.GetBundleTestDetailsReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import org.json.JSONObject;

/**
 * Created by somil on 10/05/17.
 */
@Service
public class BundleManager {

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(BundleManager.class);

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FileMgmtManager fileMgmtManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private CMDSTestManager cmdsTestManager;

    @Autowired
    private FosUtils userUtils;

    @Autowired
    private CalendarManager calendarManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    public AwsSQSManager awsSQSManager;

    private final Gson gson = new Gson();

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    @Deprecated
    public BundleInfo addEditBundle(AddEditBundleReq addEditBundleReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/addEditBundle", HttpMethod.POST,
                new Gson().toJson(addEditBundleReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);

        // Temp code
        // if (StringUtils.isEmpty(addEditBundleReq.getId())) {
        // addEditBundleReq.setId(bundleInfo.getId());
        // scheduleTracksWebinar(bundleInfo.getId(),
        // addEditBundleReq.getWebinarCategories());
        // resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT +
        // "/bundle/addEditBundle", HttpMethod.POST,
        // new Gson().toJson(addEditBundleReq));
        // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        // jsonString = resp.getEntity(String.class);
        // bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
        // }
        // TODO: Add error handling
        return bundleInfo;
    }

    @Deprecated
    public BundleInfo addEditBundleWebinar(AddEditBundleWebinarReq addEditBundleWebinarReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/addEditBundleWebinar",
                HttpMethod.POST, new Gson().toJson(addEditBundleWebinarReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
        // TODO : Calendar integration pending
        return bundleInfo;
    }

    public OTFSessionPojoUtils addEditBundleSession(AddEditBundleSessionReq addEditBundleSessionReq) throws Exception {

        // Fetch the session before update
        OTFSessionPojoUtils sessionPreUpdate = null;
        if (!StringUtils.isEmpty(addEditBundleSessionReq.getOtfWebinarSession().getId())) {
            sessionPreUpdate = otfManager.getSessionById(addEditBundleSessionReq.getOtfWebinarSession().getId());
        }
        // Check for conflicts
        boolean calendarUpdateRequired = calendarUpdateRequired(addEditBundleSessionReq, sessionPreUpdate);
        OTFWebinarSession webinarSession = addEditBundleSessionReq.getOtfWebinarSession();
        if (calendarUpdateRequired && !StringUtils.isEmpty(webinarSession.getPresenter())) {
            // TODO : In case overlap, ConflictCheck should only be done for the new slots
            List<Long> teacherIds = new ArrayList<>();
            teacherIds.add(Long.parseLong(webinarSession.getPresenter()));

            List<SessionSlot> sessionSlots = new ArrayList<>();
            SessionSlot sessionSlot = new SessionSlot(webinarSession.getStartTime(), webinarSession.getEndTime());

            // TODO : Check overlap and remove the slots in case of same teacher
            // if (sessionPreUpdate != null &&
            // !StringUtils.isEmpty(sessionPreUpdate.getPresenter())
            // && sessionPreUpdate.getPresenter().equals(webinarSession.getPresenter())) {
            // CommonCalendarUtils.removeOverlapTime(sessionSlot,
            // sessionPreUpdate.getStartTime(),
            // sessionPreUpdate.getEndTime());
            // }
            sessionSlots.add(sessionSlot);

            SlotCheckPojo slotCheckList = new SlotCheckPojo(new ArrayList<Long>(), teacherIds, sessionSlots);
            List<SessionSlot> slots = calendarManager.checkSlotAvailablilty(slotCheckList);
            if (CollectionUtils.isEmpty(slots)) {
                String errorMessage = "Slot not available for the req:" + addEditBundleSessionReq.toString();
                logger.info(errorMessage);
                throw new ConflictException(ErrorCode.USER_NOT_AVAILABLE, errorMessage);
            }
        }

        // Update the session
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/addEditBundleSession",
                HttpMethod.POST, new Gson().toJson(addEditBundleSessionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        OTFSessionPojoUtils sessionPojo = new Gson().fromJson(jsonString, OTFSessionPojoUtils.class);

        // Update the calendar
        if (calendarUpdateRequired) {

            // Mark the new slot
            AddCalendarEntryReq markReq = new AddCalendarEntryReq(sessionPojo.getPresenter(), sessionPojo.getStartTime(),
                    sessionPojo.getEndTime(), CalendarEntrySlotState.SESSION, CalendarReferenceType.WEBINAR_SESSION,
                    sessionPojo.getId());
            logger.info("mark req:" + markReq.toString());
            calendarManager.markCalendarEntries(markReq);

            // Unmark previous slot
            if (sessionPreUpdate != null) {
                AddCalendarEntryReq unmarkReq = new AddCalendarEntryReq(sessionPreUpdate.getPresenter(),
                        sessionPreUpdate.getStartTime(), sessionPreUpdate.getEndTime(), CalendarEntrySlotState.SESSION,
                        CalendarReferenceType.WEBINAR_SESSION, sessionPojo.getId());
                logger.info("unmark req:" + unmarkReq.toString());
                calendarManager.unmarkCalendarEntries(unmarkReq);
            }
        }

        return sessionPojo;
    }

    private boolean calendarUpdateRequired(AddEditBundleSessionReq addEditBundleSessionReq,
            OTFSessionPojoUtils sessionPreUpdate) {
        if (sessionPreUpdate != null
                && addEditBundleSessionReq.getOtfWebinarSession().getStartTime() == sessionPreUpdate.getStartTime()
                && addEditBundleSessionReq.getOtfWebinarSession().getEndTime() == sessionPreUpdate.getEndTime()
                && !StringUtils.isEmpty(addEditBundleSessionReq.getOtfWebinarSession().getPresenter())
                && !StringUtils.isEmpty(sessionPreUpdate.getPresenter()) && sessionPreUpdate.getPresenter()
                .equals(addEditBundleSessionReq.getOtfWebinarSession().getPresenter())) {
            return false;
        }
        return true;
    }

    public List<OTFSessionPojoUtils> cancelWebinarSession(CancelOTFWebinarSessionsReq cancelOTFWebinarSessionsReq)
            throws VException {
        List<OTFSessionPojoUtils> sessions = otfManager.cancelWebinarSession(cancelOTFWebinarSessionsReq);
        if (!CollectionUtils.isEmpty(sessions)) {
            for (OTFSessionPojoUtils sessionPojo : sessions) {
                AddCalendarEntryReq unmarkReq = new AddCalendarEntryReq(sessionPojo.getPresenter(),
                        sessionPojo.getStartTime(), sessionPojo.getEndTime(), CalendarEntrySlotState.SESSION,
                        CalendarReferenceType.WEBINAR_SESSION, sessionPojo.getId());
                logger.info("unmark req:" + unmarkReq.toString());
                calendarManager.unmarkCalendarEntries(unmarkReq);
            }
        }

        return sessions;
    }

    public List<OTFSessionPojoUtils> attendWebinarSession(UpdateWebinarSessionsAttendenceReq attendWebinarSessionsReq)
            throws VException {
        logger.info("Request : " + attendWebinarSessionsReq.toString());
        if (!Role.ADMIN.equals(httpSessionUtils.getCallingUserRole())) {
            attendWebinarSessionsReq.setStudentId(String.valueOf(httpSessionUtils.getCallingUserId()));
        }

        return otfManager.attendWebinarSession(attendWebinarSessionsReq);
    }

    public BundleInfo getBundle(String id, GetBundlesReq req) throws VException {
        String queryString = "";
        if (req != null) {
            queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        }

        logger.info("queryString : " + queryString);
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/" + id;
        if (!StringUtils.isEmpty(queryString)) {
            url += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);

        // Populate users
        populateUserInfos(bundleInfo, false);

        if (bundleInfo.getVideos() != null) {
            addVideoViewCount(bundleInfo);
        }
        return bundleInfo;
    }

    public BundleInfo getBundleBasicInfo(String id) throws VException {
        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id not found");
        }
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/" + id;

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
        return bundleInfo;
    }

    public List<OTFSessionPojoUtils> getBundleSessions(GetOTFWebinarSessionsReq req) throws VException {
        return otfManager.getBundleSessions(req);
    }

    @Deprecated
    public List<BundleInfo> getBundleInfos(GetBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/get?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleInfo>>() {
        }.getType();
        List<BundleInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }

    @Deprecated
    public List<BundleInfo> getPublicBundleInfos(GetBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/get/public?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleInfo>>() {
        }.getType();
        List<BundleInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }

    @Deprecated
    public VideoDetails getVideoDetails(Long id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getVideoDetails/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        VideoDetails videoDetails = new Gson().fromJson(jsonString, VideoDetails.class);
        return videoDetails;
    }

    public PlatformBasicResponse getPreSignedUrl(String contentType, String extension) {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bundle.bucket");
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        String key = env + "/bundle-thumbnails/" + ObjectId.get().toString() + "." + extension;

        try {
            String preSignedUrl = fileMgmtManager.getPreSignedUrlForProdAWSAccount(bucket, key, contentType);

            platformBasicResponse.setResponse(preSignedUrl);
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @Deprecated
    public void scheduleTracksWebinar(String bundleId, List<Track> webinarCategories) throws VException {
        OTFWebinarSessionReq req = new OTFWebinarSessionReq();
        req.setBundleId(bundleId);
        List<OTFWebinarSession> webinarSessions = new ArrayList<>();
        if (CollectionUtils.isEmpty(webinarCategories)) {
            return;
        }

        Map<String, WebinarBundleContentInfo> webinarRefMap = new HashMap<>();
        for (Track track : webinarCategories) {
            if (CollectionUtils.isEmpty(track.getWebinars())) {
                continue;
            }

            for (WebinarBundleContentInfo webinarBundleContentInfo : track.getWebinars()) {
                OTFWebinarSession otfWebinarSession = new OTFWebinarSession();
                otfWebinarSession.setDescription(webinarBundleContentInfo.getDescription());
                otfWebinarSession.setTitle(webinarBundleContentInfo.getTitle());
                otfWebinarSession.setStartTime(webinarBundleContentInfo.getStartTime());
                otfWebinarSession.setEndTime(webinarBundleContentInfo.getEndTime());
                otfWebinarSession.setSessionToolType(webinarBundleContentInfo.getSessionToolType());
                otfWebinarSession.setPresenter(webinarBundleContentInfo.getPresenter());
                String refId = ObjectId.get().toString();
                otfWebinarSession.setReferenceId(refId);
                webinarRefMap.put(refId, webinarBundleContentInfo);
                webinarSessions.add(otfWebinarSession);

            }
        }
        req.setSessions(webinarSessions);
        List<SessionPojoWithRefId> sessions = otfManager.addWebinarSessions(req);

        if (CollectionUtils.isNotEmpty(sessions)) {
            for (SessionPojoWithRefId sessionPojoWithRefId : sessions) {
                WebinarBundleContentInfo webinarBundleContentInfo = webinarRefMap
                        .get(sessionPojoWithRefId.getReferenceId());
                if (webinarBundleContentInfo != null) {
                    webinarBundleContentInfo.setContentId(sessionPojoWithRefId.getId());
                }
            }
        }

        // Calendar blocking
        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("sessions", sessions);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WEBINAR_CALENDAR_UPDATE_TASK, payload);
        asyncTaskFactory.executeTask(params);
    }

    @Deprecated
    public List<BundleEnrollmentResp> getEnrolledBundles(String userId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getEnrolledBundles?userId=" + userId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleEnrollmentResp>>() {
        }.getType();
        List<BundleEnrollmentResp> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos;
    }

    public List<BundleEnrolmentInfo> getEnrolments(GetBundleEnrolmentsReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getEnrolments?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleEnrolmentInfo>>() {
        }.getType();
        List<BundleEnrolmentInfo> enrolmentInfos = new Gson().fromJson(jsonString, listType1);
        if (CollectionUtils.isNotEmpty(enrolmentInfos)) {
            List<String> userIds = new ArrayList<>();
            for (BundleEnrolmentInfo bundleEnrolmentInfo : enrolmentInfos) {
                userIds.add(bundleEnrolmentInfo.getUserId());
            }
            Map<String, UserBasicInfo> userMap = userUtils.getUserBasicInfosMap(userIds, true);
            for (BundleEnrolmentInfo bundleEnrolmentInfo : enrolmentInfos) {
                UserBasicInfo userBasicInfo = userMap.get(bundleEnrolmentInfo.getUserId());
                bundleEnrolmentInfo.setUserBasicInfo(userBasicInfo);
            }
        }
        return enrolmentInfos;
    }

    public List<BundleEnrolmentInfo> checkEnrolment(CheckBundleEnrollmentReq checkBundleEnrollmentReq)
            throws VException {
        logger.info("Request:" + checkBundleEnrollmentReq.toString());
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(checkBundleEnrollmentReq);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/checkEnrolment?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleEnrolmentInfo>>() {
        }.getType();
        List<BundleEnrolmentInfo> enrolmentInfos = new Gson().fromJson(jsonString, listType1);
        // if (CollectionUtils.isNotEmpty(enrolmentInfos)) {
        // List<String> userIds = new ArrayList<>();
        // for (BundleEnrolmentInfo bundleEnrolmentInfo : enrolmentInfos) {
        // userIds.add(bundleEnrolmentInfo.getUserId());
        // }
        // Map<String, UserBasicInfo> userMap =
        // userUtils.getUserBasicInfosMap(userIds);
        // for (BundleEnrolmentInfo bundleEnrolmentInfo : enrolmentInfos) {
        // UserBasicInfo userBasicInfo =
        // userMap.get(bundleEnrolmentInfo.getUserId());
        // bundleEnrolmentInfo.setUserBasicInfo(userBasicInfo);
        // }
        // }

        // TODO: Optimize this in future if needed, right now there is only one
        // bundleEnrolment is possible in for a combination
        // of bundleId and userId
        for (BundleEnrolmentInfo bundleEnrolmentInfo : enrolmentInfos) {
            if (EnrollmentState.TRIAL.equals(bundleEnrolmentInfo.getState())) {
                RedeemCouponInfo redeemCouponInfo = couponManager
                        .getRedeemCouponInfoByRefId(bundleEnrolmentInfo.getId());
                if (redeemCouponInfo != null) {
                    bundleEnrolmentInfo.setPassExpirationTime(redeemCouponInfo.getPassExpirationTime());
                }
            }
        }

        return enrolmentInfos;
    }

    private void populateUserInfos(BundleInfo bundleInfo, boolean exposeEmail) {
        Set<String> userIds = new HashSet<>();
        if (bundleInfo != null) {
            if (!CollectionUtils.isEmpty(bundleInfo.getWebinarCategories())) {
                for (Track track : bundleInfo.getWebinarCategories()) {
                    List<WebinarBundleContentInfo> webinarBundleContentInfos = track.getWebinars();
                    if (!CollectionUtils.isEmpty(webinarBundleContentInfos)) {
                        for (WebinarBundleContentInfo weBundleContentInfo : webinarBundleContentInfos) {
                            if (!StringUtils.isEmpty(weBundleContentInfo.getPresenter())) {
                                userIds.add(weBundleContentInfo.getPresenter());
                            }
                        }
                    }
                }
            }

            if (!CollectionUtils.isEmpty(userIds)) {
                bundleInfo.setUserBasicInfos(userUtils.getUserBasicInfosSet(userIds, exposeEmail));
            }
        }
    }

    public Map<String, TestAndAttemptDetails> getBundleTestDetails(GetBundleTestDetailsReq req) throws VException {
        GetBundlesReq bundleReq = new GetBundlesReq();

        Map<String, TestAndAttemptDetails> responseMap = new HashMap<>();

        bundleReq.setReturnContentTypes(Arrays.asList(BundleContentType.TEST));
        bundleReq.setCallingUserRole(req.getCallingUserRole());
        bundleReq.setCallingUserId(req.getCallingUserId());
        bundleReq.setCheckAccessControl(true);
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(bundleReq);

        logger.info("queryString : " + queryString);
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/" + req.getBundleId();
        if (!StringUtils.isEmpty(queryString)) {
            url += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
        List<String> testIds = new ArrayList<>();
        if (bundleInfo != null && bundleInfo.getTests() != null) {
            if (CollectionUtils.isNotEmpty(bundleInfo.getTests().getChildren())) {
                for (TestContentData testContentData : bundleInfo.getTests().getChildren()) {
                    if (testContentData != null && CollectionUtils.isNotEmpty(testContentData.getContents())) {
                        for (TestBundleContentInfo testBundleContentInfo : testContentData.getContents()) {
                            if (testBundleContentInfo != null && testBundleContentInfo.getContentId() != null) {
                                testIds.add(testBundleContentInfo.getContentId());
                            }
                        }
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(testIds)) {
            req.setTestIds(testIds);
            responseMap = cmdsTestManager.getTestAndAttemptDetails(req);
        }
        return responseMap;

    }

    public PlatformBasicResponse addVideoAnalyticsData(AddVideoAnalyticsDataReq addVideoAnalyticsDataReq)
            throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/addVideoAnalyticsData",
                HttpMethod.POST, new Gson().toJson(addVideoAnalyticsDataReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        PlatformBasicResponse response = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
        redisDAO.increment(
                getVideoCacheKey(addVideoAnalyticsDataReq.getVideoType(), addVideoAnalyticsDataReq.getVideoId()), 1);
        return response;
    }

    // public VideoCountResponse getVideoCount(VideoType videoType, String videoId)
    // throws VException {
    // VideoCountResponse videoCountResponse = new VideoCountResponse();
    // videoCountResponse.setVideoType(videoType);
    // videoCountResponse.setVideoId(videoId);
    // Integer viewCount = 0;
    // String value = redisDAO.get(getVideoCacheKey(videoType, videoId));
    // if (value != null) {
    // viewCount = Integer.parseInt(value);
    // videoCountResponse.setViewCount(viewCount);
    // return videoCountResponse;
    // }
    // ClientResponse resp = WebUtils.INSTANCE.doCall(
    // SUBSCRIPTION_ENDPOINT + "/bundle/getVideoCount?videoType=" + videoType +
    // "&videoId=" + videoId,
    // HttpMethod.GET, null);
    // VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    // String jsonString = resp.getEntity(String.class);
    // VideoCountResponse response = new Gson().fromJson(jsonString,
    // VideoCountResponse.class);
    // viewCount = response.getViewCount();
    //
    // redisDAO.set(getVideoCacheKey(videoType, videoId), viewCount.toString());
    // return videoCountResponse;
    // }
    private void addVideoViewCount(BundleInfo bundleInfo) throws VException {
        List<VideoInfo> videoInfos = new ArrayList<>();
        List<String> redisKeys = new ArrayList<>();
        VideoContentData videoContentData = bundleInfo.getVideos();
        if (videoContentData.getContents() != null) {
            for (VideoBundleContentInfo videoBundleContentInfo : videoContentData.getContents()) {
                videoInfos.add(
                        new VideoInfo(videoBundleContentInfo.getVideoType(), videoBundleContentInfo.getContentId()));
                redisKeys.add(
                        getVideoCacheKey(videoBundleContentInfo.getVideoType(), videoBundleContentInfo.getContentId()));
            }
        }
        if (CollectionUtils.isNotEmpty(videoContentData.getChildren())) {
            for (VideoContentData child : videoContentData.getChildren()) {
                if (child.getContents() != null) {
                    for (VideoBundleContentInfo videoBundleContentInfo : child.getContents()) {
                        videoInfos.add(new VideoInfo(videoBundleContentInfo.getVideoType(),
                                videoBundleContentInfo.getContentId()));
                        redisKeys.add(getVideoCacheKey(videoBundleContentInfo.getVideoType(),
                                videoBundleContentInfo.getContentId()));
                    }
                }
            }
        }

        if (CollectionUtils.isEmpty(redisKeys)) {
            return;
        }

        Map<String, String> redisValueMaps = redisDAO.getValuesForKeys(redisKeys);
        List<VideoInfo> videoInfosNotFoundInRedis = new ArrayList<>();
        for (VideoInfo videoInfo : videoInfos) {
            String redisValue = redisValueMaps.get(getVideoCacheKey(videoInfo.getVideoType(), videoInfo.getVideoId()));
            if (com.vedantu.util.StringUtils.isEmpty(redisValue)) {
                if (videoInfo.getVideoType() == null) {
                    videoInfo.setVideoType(VideoType.VZAAR);
                }
                videoInfosNotFoundInRedis.add(videoInfo);
            }
        }

        List<VideoInfo> response = getVideoViewInfoFromAnalytics(videoInfosNotFoundInRedis);
        Map<String, String> analyticsValueMaps = new HashMap<>();
        if (CollectionUtils.isNotEmpty(response)) {
            for (VideoInfo videoInfo : response) {
                if (videoInfo.getViewCount() != null) {
                    analyticsValueMaps.put(getVideoCacheKey(videoInfo.getVideoType(), videoInfo.getVideoId()),
                            videoInfo.getViewCount().toString());
                }
            }
        }

        // Adding to 0 to values which were not in analytics map
        for (VideoInfo videoInfo : videoInfosNotFoundInRedis) {
            String key = getVideoCacheKey(videoInfo.getVideoType(), videoInfo.getVideoId());
            String value = analyticsValueMaps.get(key);
            if (StringUtils.isEmpty(value)) {
                analyticsValueMaps.put(key, "0");
            }
        }

        videoContentData = bundleInfo.getVideos();
        if (videoContentData.getContents() != null) {
            for (VideoBundleContentInfo videoBundleContentInfo : videoContentData.getContents()) {
                fillViewCount(videoBundleContentInfo, redisValueMaps, analyticsValueMaps);
            }
        }
        if (CollectionUtils.isNotEmpty(videoContentData.getChildren())) {
            for (VideoContentData child : videoContentData.getChildren()) {
                if (child.getContents() != null) {
                    for (VideoBundleContentInfo videoBundleContentInfo : child.getContents()) {
                        fillViewCount(videoBundleContentInfo, redisValueMaps, analyticsValueMaps);
                    }
                }
            }
        }
        redisDAO.setKeyPairs(analyticsValueMaps);

    }

    private void fillViewCount(VideoBundleContentInfo videoBundleContentInfo, Map<String, String> redisValueMaps,
            Map<String, String> analyticsValueMaps) {
        String key = getVideoCacheKey(videoBundleContentInfo.getVideoType(), videoBundleContentInfo.getContentId());
        String value = null;
        value = redisValueMaps.get(key);
        if (com.vedantu.util.StringUtils.isEmpty(value)) {
            value = analyticsValueMaps.get(key);
        }
        if (com.vedantu.util.StringUtils.isNotEmpty(value)) {
            videoBundleContentInfo.setViewCount(Integer.parseInt(value));
        }
    }

    private List<VideoInfo> getVideoViewInfoFromAnalytics(List<VideoInfo> videoInfosNotFoundInRedis) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getVideoViewInfoFromAnalytics",
                HttpMethod.POST, new Gson().toJson(videoInfosNotFoundInRedis));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<VideoInfo>>() {
        }.getType();
        List<VideoInfo> videoInfos = new Gson().fromJson(jsonString, listType1);
        return videoInfos;
    }

    private String getVideoCacheKey(VideoType videoType, String videoId) {
        return (videoType == null ? VideoType.VZAAR : videoType) + "-" + videoId + "-count";
    }

    public BundleEnrolmentInfo enrolViaPass(EnrolViaPassReq req) throws VException {

        ValidateCouponReq validateCouponReq = new ValidateCouponReq();
        validateCouponReq.setCode(req.getPassCode());
        validateCouponReq.setEntityId(req.getEntityId());
        validateCouponReq.setEntityType(EntityType.BUNDLE);
        validateCouponReq.setCallingUserId(req.getUserId());
        validateCouponReq.setCallingUserRole(req.getCallingUserRole());
        ValidateCouponRes validateCouponRes = couponManager.checkValidity(validateCouponReq);
        Long expirationTime = null;
        if (validateCouponRes.getPassDurationInMillis() != null) {
            expirationTime = System.currentTimeMillis() + validateCouponRes.getPassDurationInMillis();
        }

        BundleCreateEnrolmentReq enrolmentReq = new BundleCreateEnrolmentReq();
        enrolmentReq.setEntityId(req.getEntityId());
        enrolmentReq.setUserId(req.getUserId().toString());
        enrolmentReq.setState(EnrollmentState.TRIAL);
        BundleEnrolmentInfo bundleEnrolmentInfo = createEnrolment(enrolmentReq);

        AddCouponRedeemEntryReq addCouponRedeemEntryReq = new AddCouponRedeemEntryReq();
        addCouponRedeemEntryReq.setEntityId(req.getEntityId());
        addCouponRedeemEntryReq.setEntityType(EntityType.BUNDLE);
        addCouponRedeemEntryReq.setPassCode(req.getPassCode());
        addCouponRedeemEntryReq.setState(RedeemedCouponState.PROCESSED);
        addCouponRedeemEntryReq.setUserId(req.getUserId());
        addCouponRedeemEntryReq.setPassProcessingState(PassProcessingState.VALID);
        addCouponRedeemEntryReq.setPassExpirationTime(expirationTime);
        ReferenceTag referenceTag = new ReferenceTag(ReferenceType.BUNDLE_ENROLMENT, bundleEnrolmentInfo.getId());
        addCouponRedeemEntryReq.setReferenceTags(Arrays.asList(referenceTag));
        RedeemCouponInfo redeemCouponInfo = couponManager.addRedeemEntry(addCouponRedeemEntryReq);
        bundleEnrolmentInfo.setPassExpirationTime(redeemCouponInfo.getPassExpirationTime());

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", req.getUserId());
        payload.put("entityId", req.getEntityId());
        payload.put("entityType", EntityType.BUNDLE);
        payload.put("redeemCouponInfo", redeemCouponInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BUNDLE_ENROLLMENT_PASS_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        return bundleEnrolmentInfo;

    }

    public BundleEnrolmentInfo createEnrolment(BundleCreateEnrolmentReq req) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/createEnrolment",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        BundleEnrolmentInfo bundleEnrolmentInfo = new Gson().fromJson(jsonString, BundleEnrolmentInfo.class);
        return bundleEnrolmentInfo;
    }

    public void markEnrolmentStateForTrials(List<String> enrolmentIds, EntityStatus entityStatus) throws VException {
        MarkEnrolmentStateReq req = new MarkEnrolmentStateReq();
        req.setEnrolmentIds(enrolmentIds);
        req.setEntityStatus(entityStatus);

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/markEnrolmentStateForTrials",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json response : " + jsonString);
        PlatformBasicResponse response = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
    }

    public void autoEnrollInBundleCourses(String bundleId, Long userId, String bundleEnrollmentId) throws VException {
        String reqUrl = SUBSCRIPTION_ENDPOINT + "/bundle/autoEnrollInBundleCourses";
        JSONObject req = new JSONObject();
        req.put("bundleId", bundleId);
        req.put("userId", userId);
        req.put("enrollmentId", bundleEnrollmentId);
        WebUtils.INSTANCE.doCall(reqUrl, HttpMethod.POST, req.toString());
    }

    public void processInstalmentPayment(InstalmentInfo instalmentInfo,
            Long userId)
            throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "bundle/processInstalmentAfterPayment", HttpMethod.POST, new Gson().toJson(instalmentInfo));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public void processStatusUpdate(String bundleId, String userId, String orderId, String bundleEnrollmentId)
            throws VException {
        try {
            JSONObject req = new JSONObject();
            req.put("bundleEnrollmentId", bundleEnrollmentId);
            req.put("bundleId", bundleId);
            req.put("userId", userId);
            req.put("status", EntityStatus.INACTIVE);
            req.put("orderId", orderId);
            ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                    + "bundle/updateStatusBybundleId", HttpMethod.POST, req.toString());
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        } catch (Exception e) {
            logger.error("error in processing instalment dues for bundleId " + bundleId + " userId " + userId + " error ",
                    e);
        }
    }

    public void requestTrail(TrialReq trialReq) {

        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.TRIAL_REQUEST, gson.toJson(trialReq));

    }

    public BundleInfo getBundleName(GetBundlesReq req) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getBundlesNames", HttpMethod.POST,
                new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleInfo>>() {
        }.getType();
        List<BundleInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        return bundleInfos.get(0);
    }

}
