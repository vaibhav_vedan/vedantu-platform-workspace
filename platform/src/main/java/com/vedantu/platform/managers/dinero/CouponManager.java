/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.request.AddCouponRedeemEntryReq;
import com.vedantu.dinero.request.GetPassRedeemInfoReq;
import com.vedantu.dinero.request.MarkPassProcessingStateReq;
import com.vedantu.dinero.request.ValidateCouponReq;
import com.vedantu.dinero.response.RedeemCouponInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.platform.request.dinero.ProcessCouponReq;
import com.vedantu.platform.response.dinero.ValidateCouponRes;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somil
 */
@Service
public class CouponManager {

    

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private BundleManager bundleManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CouponManager.class);



    public String processCoupon(ProcessCouponReq req) throws VException {
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/processCoupon",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        return jsonString;
    }

    public ValidateCouponRes checkValidity(ValidateCouponReq req) throws VException {
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/checkValidity",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        ValidateCouponRes res = new Gson().fromJson(jsonString, ValidateCouponRes.class);
        return res;
    }

    public RedeemCouponInfo addRedeemEntry(AddCouponRedeemEntryReq req) throws VException {
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/addRedeemEntry",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        RedeemCouponInfo res = new Gson().fromJson(jsonString, RedeemCouponInfo.class);
        return res;
    }

    public void checkPassExpiration(int start, int size) throws VException {
        logger.info("ENTRY " + start + ", " + size);
        Long currentTime = System.currentTimeMillis();
        GetPassRedeemInfoReq req = new GetPassRedeemInfoReq();
        req.setPassProcessingState(PassProcessingState.VALID);
        req.setPassExpirationTime(currentTime);
        req.setStart(start);
        req.setSize(size);
        List<RedeemCouponInfo> results = getPassRedeemInfo(req);
        if (CollectionUtils.isNotEmpty(results)) {
            for (RedeemCouponInfo redeemCouponInfo : results) {
                try {
                    if(EntityType.BUNDLE.equals(redeemCouponInfo.getEntityType()) || EntityType.BUNDLE_PACKAGE.equals(redeemCouponInfo.getEntityType())) {
                        List<String> enrolmentIds = new ArrayList<>();
                        for(ReferenceTag srcEntity: redeemCouponInfo.getReferenceTags()) {
                            enrolmentIds.add(srcEntity.getReferenceId());
                        }
                        bundleManager.markEnrolmentStateForTrials(enrolmentIds, EntityStatus.INACTIVE);
                        markPassProcessingState(redeemCouponInfo.getId(), PassProcessingState.EXPIRED);
                    }


                } catch (Exception ex) {
                    logger.error("Error in checkPassExpiration "+ redeemCouponInfo, ex);
                }

            }
            if (results.size() == size) {
                logger.info("fetching more pass expiration");
                checkPassExpiration((size + start), size);
            }
        }
    }

    private PlatformBasicResponse markPassProcessingState(String id, PassProcessingState state) throws VException {
        MarkPassProcessingStateReq req = new MarkPassProcessingStateReq();
        req.setId(id);
        req.setPassProcessingState(state);
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/markPassProcessingState",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        PlatformBasicResponse res = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
        return res;
    }


    private List<RedeemCouponInfo> getPassRedeemInfo(GetPassRedeemInfoReq req) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/getPassRedeemInfo?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<RedeemCouponInfo>>() {
        }.getType();
        List<RedeemCouponInfo> response = new Gson().fromJson(jsonString, listType1);
        return response;
    }

    public RedeemCouponInfo getRedeemCouponInfoByRefId(String refId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/getRedeemCouponInfoByRefId?refId="+refId,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        RedeemCouponInfo redeemCouponInfo = new Gson().fromJson(jsonString, RedeemCouponInfo.class);
        return redeemCouponInfo;
    }
}
