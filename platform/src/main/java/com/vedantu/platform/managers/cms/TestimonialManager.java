package com.vedantu.platform.managers.cms;

import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.request.cms.CreateTestimonialReq;
import com.vedantu.util.LogFactory;

@Service
public class TestimonialManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(TestimonialManager.class);

	@Autowired
	private RedisDAO redisDAO;
	
    private final Gson gson = new Gson();

	public CreateTestimonialReq setTestimonial(CreateTestimonialReq req) throws BadRequestException, InternalServerErrorException {
		req.verify();
		// Redis Data Format key : "WEBINAR_TESTIMONIALS", value : "{}"

		redisDAO.set("WEBINAR_TESTIMONIALS", gson.toJson(req));
		return gson.fromJson(redisDAO.get("WEBINAR_TESTIMONIALS"), CreateTestimonialReq.class);
	}

	public CreateTestimonialReq fetchTestimonial() throws BadRequestException, JsonSyntaxException, InternalServerErrorException {
		// Redis Data Format key : "WEBINAR_TESTIMONIALS", value : "{}"
		CreateTestimonialReq createTestimonialReq = new CreateTestimonialReq();
		if(StringUtils.isNotEmpty(redisDAO.get("WEBINAR_TESTIMONIALS"))) {
			createTestimonialReq = gson.fromJson(redisDAO.get("WEBINAR_TESTIMONIALS"), CreateTestimonialReq.class);
		}
		return createTestimonialReq;
	}

}
