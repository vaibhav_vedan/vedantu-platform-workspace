package com.vedantu.platform.managers;

import com.vedantu.platform.dao.HomeDemoRequestDao;
import com.vedantu.platform.entity.HomeDemoRequest;
import com.vedantu.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeDemoRequestManager {

    @Autowired
    private HomeDemoRequestDao homeDemoRequestDao;

    public String persist(HomeDemoRequest homeDemoRequest) {

        return homeDemoRequestDao.persist(homeDemoRequest);

    }

    public List<HomeDemoRequest> getRecentRequestByContactNumber(String contactNumber) {

        if(StringUtils.isEmpty(contactNumber))
            return null;

        return homeDemoRequestDao.getRecentRequestByContactNumber(contactNumber);

    }

    public HomeDemoRequest getLastLSPushedHomeDemoDetails() {

        return homeDemoRequestDao.getLastLSPushedHomeDemoDetails();

    }

    public List<HomeDemoRequest> getHomeDemoRequestByTimeRange(Long startTime, Long endTime) {

        return homeDemoRequestDao.getHomeDemoRequestByTimeRange(startTime, endTime);

    }
    public List<HomeDemoRequest> getHomeDemoRequestNotPushed() {

        return homeDemoRequestDao.getHomeDemoRequestNotPushed();

    }
}
