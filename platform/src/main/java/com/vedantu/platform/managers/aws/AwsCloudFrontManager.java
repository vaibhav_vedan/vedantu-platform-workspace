package com.vedantu.platform.managers.aws;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CreateInvalidationRequest;
import com.amazonaws.services.cloudfront.model.InvalidationBatch;
import com.amazonaws.services.cloudfront.model.Paths;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

@Service
public class AwsCloudFrontManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsCloudFrontManager.class);

    private AmazonCloudFrontClient cloudFrontClient;

    public static final String ACADTACK_DISTRIBUTION_ID = ConfigUtils.INSTANCE.getStringValue("aws.cloudfront.acadstack.distribution.id");
    public static final String VEDANTU_DISTRIBUTION_ID = ConfigUtils.INSTANCE.getStringValue("aws.cloudfront.vedantu.distribution.id");

    @Autowired
    private AmazonClient amazonClient;

    public AwsCloudFrontManager() {
        cloudFrontClient = new AmazonCloudFrontClient();
    }

    public void invalidateCache(String distributionId, List<String> paths) {
        Paths path = new Paths();
        path.setItems(paths);
        InvalidationBatch batch = new InvalidationBatch(path, String.valueOf(System.currentTimeMillis()));
        CreateInvalidationRequest request = new CreateInvalidationRequest(distributionId, batch);
        cloudFrontClient.createInvalidation(request);
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (cloudFrontClient != null) {
                cloudFrontClient.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
