/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vedantu.User.request.SocialSource;
import com.vedantu.User.request.SocialUserInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebCommunicator;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class FacebookManager extends AbstractSocialManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FacebookManager.class);

    public FacebookManager() {
        super();
        httpParams.add(new BasicNameValuePair("scope", "email"));
        super.init();
    }

//    @Override
//    protected void init() {
//        httpParams.add(new BasicNameValuePair("scope", "email"));
//        super.init();
//    }

    @Override
    public String getClientId() {
        return CLIENT_ID;
    }

    @Override
    public String getClientSecret() {

        return CLIENT_SECRET;
    }

    @Override
    public String getAuthUrl() {

        return AUTH_URL;
    }

    @Override
    public String getTokenUrl(List<NameValuePair> httpParams) {
        StringBuilder tokenUrl = new StringBuilder();
        tokenUrl.append(TOKEN_URL);
        tokenUrl.append(URLEncodedUtils.format(httpParams, "UTF-8"));
        return tokenUrl.toString();
    }

    @Override
    public String getUserInfoUrl(String access_token) {
        StringBuilder sb = new StringBuilder();
        try {
            String fieldString = "fields=id,email,first_name,last_name,gender&access_token=";
            sb.append(USER_INFO_URL).append(fieldString)
                    .append(URLEncoder.encode(access_token, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.error("getUserInfoUrl", e);
        }
        return sb.toString();
    }

    @Override
    public SocialUserInfo getUserInfo(String code) {

        List<NameValuePair> httpParams = new ArrayList<NameValuePair>(
                this.httpParams);
        httpParams.remove(0);
        httpParams.add(new BasicNameValuePair("client_secret",
                getClientSecret()));
        httpParams.add(new BasicNameValuePair("code", code));

        String tokenUrl = getTokenUrl(httpParams);
        logger.info("token url : " + tokenUrl);

        String tokenRes = WebCommunicator.loadGetWebData(tokenUrl);

        logger.info("tokenUrl : " + tokenUrl);

        logger.info("tokenRes : " + tokenRes);

        if (StringUtils.isEmpty(tokenRes)) {
            return null;
        }

//        List<NameValuePair> resParams = URLEncodedUtils.parse(
//                URI.create("http://localhost:80?" + tokenRes), "UTF-8");
//        logger.info("resParams : " + resParams);

        String accessToken = null;
        JsonObject userJson = null;
        Gson gson = new Gson();
        JsonObject tokenJson = gson.fromJson(tokenRes, JsonObject.class);
        
        if (tokenJson.has("access_token") && tokenJson.get("access_token").getAsString() != null) {
            // Do something with object.
        	accessToken = tokenJson.get("access_token").getAsString();
        } else {
        	return null;
        }
        

        //String accessToken = null;//resParams.get(0).getValue();

        String userInfoString = WebCommunicator
                .loadGetWebData(getUserInfoUrl(accessToken));
        if (userInfoString == null) {
            return null;
        }
        logger.info("userInfoString : " + userInfoString);

        userJson = new Gson().fromJson(userInfoString, JsonObject.class);

        return getSocialSource().getSocialUserInfo(userJson, accessToken);
    }

    public SocialUserInfo getUserInfoUsingToken(String accessToken) {

        String userInfoString = WebCommunicator
                .loadGetWebData(getUserInfoUrl(accessToken));
        if (userInfoString == null) {
            return null;
        }
        logger.info("userInfoString : " + userInfoString);
        JsonObject userJson = null;

        userJson = new Gson().fromJson(userInfoString, JsonObject.class);

        return getSocialSource().getSocialUserInfo(userJson, accessToken);
    }

    @Override
    public SocialSource getSocialSource() {
        return SocialSource.FACEBOOK;
    }


//    public static void main(String argv[]) {
//        String test = "{\"access_token\":\"EAADEeAZAcbSQBADVw5v2CmxjKRW4vPx0KZCGuiB9gm6WFsjwvBiRfmTAidJZACsCnyTZBdRYE5JRBG5rWDTABHhnU\n" +
//                "W7tQZCjj2hIc7k3jqWG4MHTDll5KZA6ZBtyDY7aszR4QBMfAycy6FQ8d1rY9Cp2Xy27cDPcEJZASDuCSwS6zQZDZD\",\"token_type\":\"bearer\",\"expires_in\":5183428}";
//        JsonParser parser = new JsonParser();
//        JsonObject object = parser.parse(test).getAsJsonObject();
//        String data = object.get("access_token").getAsString();
//        System.out.println(data);
//    }

}
