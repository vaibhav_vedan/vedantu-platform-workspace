/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers;

import com.google.gson.Gson;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.User;
import com.vedantu.User.UserUtils;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.listing.pojo.GetTeacherResponse;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.board.TeacherBoardMappingManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.platform.pojo.requestcallback.request.ESPopulatorRequest;
import com.vedantu.platform.pojo.requestcallback.request.IncrementType;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.dao.ESConfig;
import com.vedantu.platform.pojo.es.EsBoardData;
import com.vedantu.platform.mongodbentities.review.CumilativeRating;
import com.vedantu.platform.managers.scheduling.CalendarManager;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.pojo.scheduling.ESSearchResponse;
import com.vedantu.platform.pojo.scheduling.ElasticSearchResponseParser;
import com.vedantu.platform.pojo.scheduling.RouteScheduling;
import com.vedantu.scheduling.pojo.TotalSessionDuration;
import com.vedantu.scheduling.pojo.session.Session;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.EntityType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.script.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class ESManager {

    private Integer DEFAULT_ONLINE_STATUS = -1;

    @Autowired
    private TeacherBoardMappingManager boardMappingManager;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ESManager.class);

    @Autowired
    private UserUtils userUtils;

    @Autowired
    private CumilativeRatingDao cumilativeRatingDao;

    @Autowired
    private CalendarManager calendarManager;

    @Autowired
    public ESConfig eSConfig;

    @Autowired
    RouteScheduling routeScheduling;

    public Client client;

    @Autowired
    private DozerBeanMapper mapper;

    @PostConstruct
    public void init() {
        client = eSConfig.getTransportClient();
    }

    /*
    public void execute(ElasticSearchTask elasticSearchTask, String params) throws InterruptedException, ExecutionException, BadRequestException, NotFoundException, IllegalAccessException {
        Gson gson = new Gson();
        switch (elasticSearchTask) {
//            case ADD_BOARD:
//                Board boardAdd = gson.fromJson(params, Board.class);
//                ElasticSearchDataPopulator.Instance.setBoardData(boardAdd);
//                break;
//            case UPDATE_BOARD:
//                Board boardUpdate = gson.fromJson(params, Board.class);
//                ElasticSearchDataPopulator.Instance.setBoardData(boardUpdate);
//                break;
//            case ADD_TEACHER:
//                User userAdd = gson.fromJson(params, User.class);
//                ElasticSearchDataPopulator.Instance.setTeacherData(userAdd);
//                break;
//            case UPDATE_TEACHER_DATA:
//                User userUpdate = gson.fromJson(params, User.class);
//                ElasticSearchDataPopulator.Instance.updateTeacherData(userUpdate);
//                break;
//            case UPDATE_RATE:
//                Plan plan = gson.fromJson(params, Plan.class);
//                IUserDAO userDAO = DAOFactory.INSTANCE.getUserDAO();
//                User userUpdate1 = userDAO.getUserById(plan.getTeacherId());
//                if (userUpdate1 != null) {
//                    ElasticSearchDataPopulator.Instance.updateRate(userUpdate1);
//                }
//                break;
//            case UPDATE_RATING:
//                CumilativeRating ratingUpdate = gson.fromJson(params, CumilativeRating.class);
//                ElasticSearchDataPopulator.Instance.updateRating(ratingUpdate);
//                break;
//            case UPDATE_SESSION_DATA:
//                Session sessionUpdate = gson.fromJson(params, Session.class);
//                ElasticSearchDataPopulator.Instance.updateSessionData(sessionUpdate);
//                break;
    //TODO: Implement this after sessions are migrated
            case UPDATE_AVAILABILITY_PERCENT:
                Session sessionAvailabilityUpdate = gson.fromJson(params, Session.class);
                ElasticSearchDataPopulator.Instance.updateAvailabilityPercent(sessionAvailabilityUpdate);
                break;
//            case UPDATE_TEACHER_BOARD_MAPPING:
//                TeacherBoardMapping teacherBoardMappingUpdate = gson.fromJson(params, TeacherBoardMapping.class);
//                ElasticSearchDataPopulator.Instance.updateTeacherBoardMapping(teacherBoardMappingUpdate);
//                break;
            //TODO: Implement this after sessions are migrated
            case UPDATE_AVAILABILITY_DATA:
                AvailabilityCalendar availabilityCalendar = gson.fromJson(params, AvailabilityCalendar.class);
                ElasticSearchDataPopulator.Instance.updateAvailability(availabilityCalendar);
                break;
//            case DELETE_TEACHER_BOARD_MAPPING:
//                TeacherBoardMapping teacherBoardMappingDelete = gson.fromJson(params, TeacherBoardMapping.class);
//                ElasticSearchDataPopulator.Instance.deleteTeacherBoardMapping(teacherBoardMappingDelete);
//                break;
//            case UPDATE_ELASTIC_SEARCH_DATA_MIGRATION:
//                Type filterListType = new TypeToken<List<User>>() {
//                }.getType();
//                List<User> users = gson.fromJson(params, filterListType);
//                ElasticSearchDataPopulator.Instance.indexElasticSearchData(users);
//                break;
            //TODO: Implement this after sessions are migrated
            case UPDATE_TEACHER_SESSION_INFO_DATA_MIGRATION:
                Type filterListType2 = new TypeToken<List<User>>() {
                }.getType();
                List<User> users2 = gson.fromJson(params, filterListType2);
                ElasticSearchDataPopulator.Instance.indexElasticSearchDataSessionInfo(users2);
                break;
//            case UPDATE_ELASTIC_SEARCH_DATA_MIGRATION_RATE:
//                Type filterListType1 = new TypeToken<List<User>>() {
//                }.getType();
//                List<User> users1 = gson.fromJson(params, filterListType1);
//                ElasticSearchDataPopulator.Instance.indexElasticSearchDataRate(users1);
//                break;
            default:
                throw new IllegalArgumentException("Elastic Search Task not supported");
        }
    }
     */
    public void updateTeacherData(User user) {
        logger.info("request received for updating teacher id-" + user.getId());
        EsTeacherData esTeacherData = mapper.map(user, EsTeacherData.class);
        TeacherInfo teacherInfo = user.getTeacherInfo();
        mapper.map(teacherInfo, esTeacherData);
        esTeacherData.setProfilePicUrl(user.getProfilePicUrl());
        esTeacherData.setTeacherId(user.getId());

        //String response = ElasticSearchClient.INSTANCE.updateDocument("vedantu", "teachers",
        //		Long.toString(user.getId()), gson.toJson(esTeacherData), null);
        //logExiting(getLogTag() + " : " + response.toString());
        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(user.getId()))
                .setDoc(new Gson().toJson(esTeacherData)).get();
        logger.info("Return : " + response.toString());
        printESUpdateResponse(response);
    }

    //@Async
    public void setTeacherData(User user) {
        logger.info("Request recieved for adding teacher id-" + user.getId());
        EsTeacherData esTeacherData = mapper.map(user, EsTeacherData.class);
        mapper.map(user.getTeacherInfo(), esTeacherData);
        List<TeacherBoardMapping> boardMappings = boardMappingManager.getTeacherBoardMappings(user.getId());
        List<Long> boardIds = new ArrayList<>();
        for (TeacherBoardMapping boardMapping : boardMappings) {
            if (!boardIds.contains(boardMapping.getBoardIds().iterator().next())) {
                boardIds.addAll(boardMapping.getBoardIds());
            }
        }
        String profilePicUrl = StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
                : userUtils.toProfilePicUrl(user.getProfilePicPath());
        esTeacherData.setProfilePicUrl(profilePicUrl);
        esTeacherData.setBoards(boardIds);
        esTeacherData.setTeacherId(user.getId());
        esTeacherData.setOnlineStatus(DEFAULT_ONLINE_STATUS);
        esTeacherData.setRating(0.0);
        esTeacherData.setShownTillDate(0l);
        esTeacherData.setShownToday(0l);
        // SessionDAO sessionDAO = new SessionDAO();
        // esTeacherData.setSessionHours(sessionDAO.getSessionCount(
        // user.getId(), Role.TEACHER,
        // VPersistenceManagerFactory.getPersistenceManager()));

        String primaryCallingNumber = ConfigUtils.INSTANCE
                .getStringValue("teacher.call.extension." + user.getTeacherInfo().getPrimaryCallingNumberCode());
        if (StringUtils.isEmpty(primaryCallingNumber)) {
            primaryCallingNumber = "0";
        }
        esTeacherData.setPrimaryCallingNumber(primaryCallingNumber);
        esTeacherData.setRate(Double.valueOf(getRate(user.getTeacherInfo())));
        esTeacherData.setStartPrice(Long.valueOf(getStartPrice(user)));
        esTeacherData.setSessionHours(0l);
        esTeacherData.setSessions(0l);
        //String response = ElasticSearchClient.INSTANCE.indexDocument("vedantu", "teachers", Long.toString(user.getId()),
        //		gson.toJson(esTeacherData), null);
        IndexResponse response = null;
        try {
            response = client.prepareIndex("vedantu", "teachers", Long.toString(user.getId()))
                    .setSource(new Gson().toJson(esTeacherData)).get();
        } catch (Exception e) {
            logger.error("Error in indexing teacher " + user.getId() + ", error: " + e.getMessage());
        }
        printESIndexResponse(response);
        logger.info("Return : " + response);
    }

    public int getRate(TeacherInfo teacherInfo) {
        int rate = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("teacher.base.rate.rupees.per.hour"));
        if (teacherInfo != null && teacherInfo.getChargeRate() > 100) {
            rate = teacherInfo.getChargeRate() / 100;
        }
        return rate;
    }

    public int getStartPrice(User user) {
        int rate = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("teacher.base.rate.rupees.per.hour"));

        TeacherInfo teacherInfo = user.getTeacherInfo();
        if (teacherInfo != null && teacherInfo.getChargeRate() > 100) {
            rate = teacherInfo.getChargeRate() / 100;
        }

        //TODO: Implement below when plan, also discuss
        /*
		IPlanDAO planDAO = DAOFactory.INSTANCE.getPlanDAO();
		PersistenceManager mgr = VPersistenceManagerFactory.getPersistenceManager();
		List<Plan> plans = planDAO.getPlans(null, user.getId(), SubscriptionState.ACTIVE, mgr);

		for (Plan plan : plans) {
			int planRate = (int) (((float) plan.getRate() / (float) plan.getHours()) / 100);
			if (planRate < rate) {
				rate = planRate;
			}
		}
         */
        return rate;
    }

    //@Async
    public void setBoardData(Board board) throws VException {
        logger.info("request recievde for adding board id-" + board.getId());
        EsBoardData esBoardData = mapper.map(board, EsBoardData.class);
        if (board.getParentId() != 0) {
            com.vedantu.platform.mongodbentities.board.Board parentBoard = boardManager.getBoardById(board.getParentId());
            if (parentBoard != null) {
                esBoardData.setParentInfo(parentBoard.getName());
            }
        }
        //TODO: Test this, changes from sending board to esBoardData
        IndexResponse response = client.prepareIndex("vedantu", "boards", Long.toString(board.getId()))
                .setSource(new Gson().toJson(esBoardData)).get();
        logger.info("Return : " + response.toString());
        printESIndexResponse(response);
    }

    //@Async
    public void updateRating(CumilativeRating rating) {
        logger.info("Request recieved for updating rating for user id-" + rating.getEntityId());

        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", rating.getEntityId())
                .setDoc("{\"rating\":" + rating.getAvgRating() + "}").get();
        logger.info("Return : " + response.toString());
        printESUpdateResponse(response);
    }

    //@Async
    public void updateTeacherBoardMapping(TeacherBoardMapping teacherBoardMapping) throws VException, IOException {
        logger.info("Request recieved for updating teacher board mapping id-"
                + teacherBoardMapping.getId());
        List<TeacherBoardMapping> teacherBoardMappings = boardMappingManager
                .getTeacherBoardMappings(teacherBoardMapping.getUserId());

        Board targetBoard;
        Board board = boardManager.getBoardById(teacherBoardMapping.getBoardIds().iterator().next());
        Long parentId = board.getParentId();
        if (parentId != null && parentId != 0l) {
            Board parentBoard = boardManager.getBoardById(parentId);
            targetBoard = parentBoard;
        } else {
            targetBoard = board;
        }
        Map<String, Set<String>> ESSubjectTargetMap = new HashMap<>();

        for (TeacherBoardMapping boardMapping : teacherBoardMappings) {

            Board boardTemp = boardManager.getBoardById(boardMapping.getBoardIds().iterator().next());
            Long parentIdTemp = boardTemp.getParentId();
            Board targetTempBoard;
            if (parentIdTemp != null && parentIdTemp != 0l) {
                Board parentTempBoard = boardManager.getBoardById(parentIdTemp);
                targetTempBoard = parentTempBoard;
            } else {
                targetTempBoard = boardTemp;
            }
            Set<String> esList = null;
            if (ESSubjectTargetMap.containsKey(board.getName())) {
                esList = ESSubjectTargetMap.get(board.getName());
            } else {
                esList = new HashSet<>();
            }

            if (!esList.contains(boardMapping.getCategory())) {
                esList.add(boardMapping.getCategory());
            }
            ESSubjectTargetMap.put(targetTempBoard.getName(), esList);
        }

        UpdateResponse response = updateUsingScript(Long.toString(teacherBoardMapping.getUserId()), "subjectTargetMap", new Gson().toJson(ESSubjectTargetMap));
        logger.info("response of updating  subjectTargetMap: " + response);
        printESUpdateResponse(response);

        IndexResponse responseMapping = client.prepareIndex("vedantu", "teacherBoardMappings", Long.toString(teacherBoardMapping.getId()))
                .setSource(new Gson().toJson(teacherBoardMapping)).setParent(Long.toString(teacherBoardMapping.getUserId())).get();
        logger.info("response of adding  teacherBoardMappings : " + responseMapping);
        printESIndexResponse(responseMapping);
    }

    //@Async
    public void deleteTeacherBoardMapping(Long id) {
        DeleteResponse responseTeacherBoardMappings = client.prepareDelete("vedantu",
                "teacherBoardMappings", Long.toString(id)).get();
        printESDeleteResponse(responseTeacherBoardMappings);
        logger.info("Return : " + responseTeacherBoardMappings.toString());
    }

    //@Async
    public void updateRate(User user) {
        logger.info("Request recieved for updating plan for user id-" + user.getId());
        int rate = getRate(user.getTeacherInfo());
        int planRate = getStartPrice(user);
        //String response = null;
        //response = ElasticSearchClient.INSTANCE.updateDocument("vedantu", "teachers", Long.toString(user.getId()),
        //		"{\"rate\" :" + rate + ", \"startPrice\":" + planRate + "}", null);
        //logExiting(getLogTag() + " : " + response);

        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(user.getId()))
                .setDoc("{\"rate\" :" + rate + ", \"startPrice\":" + planRate + "}").get();
        logger.info("Return : " + response.toString());
        printESUpdateResponse(response);
    }

    //@Async
    public void updateSessionData(Session session) {
        logger.info("Request recieved for updating session hours for user id-" + session.getTeacherId());
        if (session.getState().equals(SessionState.ENDED) && session.getEndedAt() != null
                && session.getStartedAt() != null) {
            String updateQuery = "ctx._source.sessionHours += " + (session.getEndedAt() - session.getStartedAt());
            //String response = ElasticSearchClient.INSTANCE.updateDocumentDef("vedantu", "teachers",
            //		Long.toString(session.getTeacherId()), updateQuery, null);
            UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(session.getTeacherId()))
                    .setScript(updateQuery, ScriptService.ScriptType.INLINE).get();
            logger.info("Return : " + response.toString());

            String updateSessionQuery = "ctx._source.sessions += 1";
            //String responseSession = ElasticSearchClient.INSTANCE.updateDocumentDef("vedantu", "teachers",
            //		Long.toString(session.getTeacherId()), updateSessionQuery, null);
            UpdateResponse responseSession = client.prepareUpdate("vedantu", "teachers", Long.toString(session.getTeacherId()))
                    .setScript(updateSessionQuery, ScriptService.ScriptType.INLINE).get();
            logger.info("Return : " + responseSession.toString());
            printESUpdateResponse(response);

            //logger.info("UpdateSessionData" + " : " + response);
            //logger.info("UpdateSessionData" + " : " + responseSession);
            return;
        }
        logger.info("UpdateSessionData" + " :  Not Updated" + session);

    }

    //@Async
    public int indexElasticSearchData(List<User> users) throws VException, IOException {
        int count = 0;
        // Update Users and Sessions Data per User

        for (User user : users) {
            Gson gson = new Gson();

            User userTemp = gson.fromJson(gson.toJson(user), User.class);
            logger.info("Request recieved for adding teacher id-" + userTemp.getId());
            EsTeacherData esTeacherData = mapper.map(userTemp, EsTeacherData.class);
            mapper.map(userTemp.getTeacherInfo(), esTeacherData);
            List<TeacherBoardMapping> boardMappings = boardMappingManager.getTeacherBoardMappings(userTemp.getId());
            List<Long> boardIds = new ArrayList<>();
            for (TeacherBoardMapping boardMapping : boardMappings) {
                if (!boardIds.contains(boardMapping.getBoardIds().iterator().next())) {
                    boardIds.addAll(boardMapping.getBoardIds());
                }
            }
            String profilePicUrl = StringUtils.isEmpty(user.getProfilePicPath()) ? user.getProfilePicUrl()
                    : userUtils.toProfilePicUrl(user.getProfilePicPath());
            esTeacherData.setProfilePicUrl(profilePicUrl);
            esTeacherData.setBoards(boardIds);
            esTeacherData.setTeacherId(userTemp.getId());
            esTeacherData.setOnlineStatus(DEFAULT_ONLINE_STATUS);
            CumilativeRating cumilativeRating = cumilativeRatingDao.getCumulativeRating(user.getId().toString(),
                    EntityType.USER);
            if (cumilativeRating != null && cumilativeRating.getAvgRating() != null) {
                esTeacherData.setRating(Double.valueOf(cumilativeRating.getAvgRating()));
            } else {
                esTeacherData.setRating(0.0);
            }
            String primaryCallingNumber = ConfigUtils.INSTANCE.getStringValue(
                    "teacher.call.extension." + userTemp.getTeacherInfo().getPrimaryCallingNumberCode());
            if (StringUtils.isEmpty(primaryCallingNumber)) {
                primaryCallingNumber = "0";
            }
            esTeacherData.setPrimaryCallingNumber(primaryCallingNumber);

            esTeacherData.setOnlineStatus(-1);

            TotalSessionDuration totalSessionDuration = sessionManager.getTotalSessionDurationForTeacher(userTemp.getId());
            Long sessionCount = totalSessionDuration != null ? totalSessionDuration.getCount() : 0l;
            Long sessionHours = totalSessionDuration != null ? totalSessionDuration.getDuration() : 0l;

            esTeacherData.setSessionHours(sessionHours);
            esTeacherData.setSessions(sessionCount);
            esTeacherData.setRate(Double.valueOf(getRate(userTemp.getTeacherInfo())));
            esTeacherData.setStartPrice(Long.valueOf(getStartPrice(userTemp)));
            //String response = ElasticSearchClient.INSTANCE.indexDocument("vedantu", "teachers",
            //		Long.toString(userTemp.getId()), gson.toJson(esTeacherData), null);
            //LOG.info("TEACHER ADDED : " + response);

            IndexResponse response = client.prepareIndex("vedantu", "teachers", Long.toString(userTemp.getId()))
                    .setSource(new Gson().toJson(esTeacherData)).get();
            logger.info("TEACHER ADDED : " + response.toString());
            printESIndexResponse(response);

            // UpdateTeacherBoardMapping
            List<TeacherBoardMapping> teacherBoardMappings = boardMappingManager
                    .getTeacherBoardMappings(userTemp.getId());
            for (TeacherBoardMapping teacherBoardMapping : teacherBoardMappings) {
                //String responseTeacherBoardMappings = ElasticSearchClient.INSTANCE.indexDocument("vedantu",
                //		"teacherBoardMappings", Long.toString(teacherBoardMapping.getId()),
                //		gson.toJson(teacherBoardMapping), Long.toString(teacherBoardMapping.getUserId()));
                //LOG.info(getLogTag() + " : " + responseTeacherBoardMappings);

                IndexResponse responseTeacherBoardMappings = client.prepareIndex("vedantu", "teacherBoardMappings", Long.toString(teacherBoardMapping.getId()))
                        .setSource(new Gson().toJson(teacherBoardMapping)).setParent(Long.toString(teacherBoardMapping.getUserId())).get();
                logger.info("MAPPING ADDED : " + responseTeacherBoardMappings.toString());
                printESIndexResponse(responseTeacherBoardMappings);
            }
            if (teacherBoardMappings != null && !teacherBoardMappings.isEmpty()) {
                updateTeacherBoardMapping(teacherBoardMappings.get(0));
            }
            count++;
        }

        // Update Boards
        List<Board> boards = boardManager.getBoards(null, null, null, null, null);

        for (Board board : boards) {
            setBoardData(board);
        }
        // mgr.flush();
        logger.info("Users Updated:" + count);
        return count;
    }

    //@Async
    public void indexElasticSearchDataRate(List<User> users) {

        for (User user : users) {

            logger.info("Request recieved for updating rate  for user id-" + user.getId());
            int rate = getRate(user.getTeacherInfo());

            String updateQuery = "ctx._source.rate = " + rate;
            UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(user.getId()))
                    .setScript(updateQuery, ScriptService.ScriptType.INLINE).get();
            logger.info("indexElasticSearchDataRate : " + response.toString());
            printESUpdateResponse(response);
        }
    }

    public void updateStartsFromInES(Long teacherId, Double teacherMinPrice, Double teacherOneHourRate) throws IOException {

        String doc;
        if (teacherOneHourRate != null && !teacherOneHourRate.equals(new Double(0))) {
            doc = "{\"rate\" :" + teacherOneHourRate
                    + ", \"startPrice\":" + teacherMinPrice + "}";
        } else {
            doc = "{\"startPrice\" :" + teacherMinPrice + "}";
        }
//        String json = new Gson().toJson(doc);
        logger.info("setting " + doc);
        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(teacherId))
                .setDoc(doc).get();
        logger.info("Return : " + response);
        printESUpdateResponse(response);
    }

    public void updateScoreOffset(Long userId, Float offset) {
        logger.info("request recieved for updating score offset for user id-" + userId);
        String tempQuery = "ctx._source.scoreOffset = " + offset;
        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(userId))
                .setScript(tempQuery, ScriptService.ScriptType.INLINE).get();
        logger.info("updateScoreOffset" + " : " + response);
        printESUpdateResponse(response);
    }

    public void updateTruerAvailableTime(Long userId) throws Exception {
        logger.info("updateTruerAvailableTime " + userId);
        Double weightedTruerAvailabilityTime = calendarManager.getWeightedTruerAvailability(userId.toString(), System.currentTimeMillis() + 1 * DateTimeUtils.MILLIS_PER_DAY,
                System.currentTimeMillis() + 5 * DateTimeUtils.MILLIS_PER_DAY, 0, "1");
        String updatequery = "ctx._source.truerAvailableTime=" + weightedTruerAvailabilityTime;
        String upsertQuery = "{\"truerAvailableTime\": " + weightedTruerAvailabilityTime + "}";
        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(userId))
                .setScript(updatequery, ScriptService.ScriptType.INLINE).setUpsert(upsertQuery).get();
        logger.info("updateScoreOffset" + " : " + response);
        printESUpdateResponse(response);
    }

    //TODO: Test this
    public BasicRes populateESParam(ESPopulatorRequest esPopulatorRequest) {

//		logger.info("es populator request : " + esPopulatorRequest.toString());
//		String teachermetrics_updateurl = baseUrl + "vedantu/teachers/" + esPopulatorRequest.getUserId() + "/_update";
//		String teachermetrics_url = baseUrl + "vedantu/teachers/" + esPopulatorRequest.getUserId();
//		LOG.info("tos teacher update url is " + teachermetrics_url);
        BasicRes basicRes = new BasicRes();
//		WebResource updateResource = clientj.resource(teachermetrics_url);
//		ClientResponse updateresponse = null;
        String updatequery = null;
        String upsertQuery = null;
        try {
            GetResponse getResponse = client.prepareGet("vedantu", "teachers", Long.toString(esPopulatorRequest.getUserId())).get();
            Map<String, Object> source = getResponse.getSource();
            logger.info("source json for es populate:" + getResponse.toString());
            if (source != null && !source.isEmpty()) {

                if (esPopulatorRequest.getIncrementType().equals(IncrementType.INCREMENTAL)) {
                    if (source.get(esPopulatorRequest.getParam()) != null) {
                        updatequery = "ctx._source." + esPopulatorRequest.getParam() + "+="
                                + esPopulatorRequest.getIncrement();
                        upsertQuery = "{\"" + esPopulatorRequest.getParam() + "\": " + esPopulatorRequest.getIncrement() + "}";
                    } else {
                        updatequery = "ctx._source." + esPopulatorRequest.getParam() + "="
                                + esPopulatorRequest.getIncrement();
                        upsertQuery = "{\"" + esPopulatorRequest.getParam() + "\": " + esPopulatorRequest.getIncrement() + "}";
                    }

                } else if (esPopulatorRequest.getIncrementType().equals(IncrementType.AVERAGE)) {
                    Long ParameterAverage = 0l;
                    if (source.get("\"" + esPopulatorRequest.getParam() + "\"") != null) {
                        ParameterAverage = (Long) source.get("\"" + esPopulatorRequest.getParam() + "\"");
                        logger.info("parameter average now :" + ParameterAverage);
                    }
                    Long rcbCalledCount = (Long) source.get("rcbCalledCount");
                    Long newParameterAverage = (ParameterAverage * rcbCalledCount + esPopulatorRequest.getIncrement())
                            / (rcbCalledCount + 1);
                    updatequery = "ctx._source." + esPopulatorRequest.getParam() + "="
                            + (newParameterAverage);
                    upsertQuery = "{\"" + esPopulatorRequest.getParam() + "\": " + esPopulatorRequest.getIncrement() + "}";
                } else if (esPopulatorRequest.getIncrementType().equals(IncrementType.FIXED)) {
                    //Long ParameterAverage = 0l;
//					if (source.has("\"" + esPopulatorRequest.getParam() + "\"")) {
//						ParameterAverage = source.getLong("\"" + esPopulatorRequest.getParam() + "\"");
//						LOG.info("parameter average now :"+ParameterAverage);
//					}

                    updatequery = "ctx._source." + esPopulatorRequest.getParam() + "="
                            + esPopulatorRequest.getIncrement();
                    upsertQuery = "{\"" + esPopulatorRequest.getParam() + "\": " + esPopulatorRequest.getIncrement() + "}";
                }
            }
            logger.info("update query is :" + updatequery);
            UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(esPopulatorRequest.getUserId()))
                    .setScript(updatequery, ScriptService.ScriptType.INLINE).setUpsert(upsertQuery).get();
            logger.info("Return : " + response.toString());
            printESUpdateResponse(response);
        } catch (Exception e) {
            logger.info("could not update : " + esPopulatorRequest.toString());
            logger.info(" error in updating : " + e.getMessage());
            basicRes.setSuccess(false);
            return basicRes;
        }

        basicRes.setSuccess(true);
        return basicRes;
    }

    public void listingFaceCountAddition(GetTeacherResponse response) {
        for (com.vedantu.listing.pojo.EsTeacherData teacher : response.getTeachers()) {
            try {
                String updateQuery = "ctx._source.shownTillDate += 1; ctx._source.shownToday += 1";
                UpdateResponse resp = client.prepareUpdate("vedantu", "teachers", Long.toString(teacher.getTeacherId()))
                        .setScript(updateQuery, ScriptService.ScriptType.INLINE).get();
                logger.info("listingFaceCountAddition : " + resp.toString());
                printESUpdateResponse(resp);
            } catch (Exception e) {
                logger.info("could not update listingFaceCountAddition :"
                        + teacher.getTeacherId(), e);
            }
        }
    }

    public void dailyListingCountUpdate(Long userId) {
        String updateQuery = "ctx._source.shownToday = 0";
        UpdateResponse resp = client.prepareUpdate("vedantu", "teachers", Long.toString(userId))
                .setScript(updateQuery, ScriptService.ScriptType.INLINE).get();
        printESUpdateResponse(resp);
    }

    public ESSearchResponse getTeachersByIds(List<Long> userIds) throws Exception {
        if(ArrayUtils.isEmpty(userIds)){
            return null;
        }
        String responseJson = null;
        try {
            responseJson = routeScheduling.getTeachersByIds(userIds);
        } catch (Throwable e) {
            logger.error(e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Search Failed due to internal server error");
        }
        ESSearchResponse response = null;
        if (StringUtils.isNotEmpty(responseJson)) {
            response = ElasticSearchResponseParser.INSTANCE.parse(responseJson);
        }
        return response;
    }

    private UpdateResponse updateUsingScript(String esId, String keyToUpdate, String json) throws IOException {
        logger.info("esId: " + esId + ", keyToUpdate: " + keyToUpdate + ", json: " + json);
        if (StringUtils.isEmpty(esId) || StringUtils.isEmpty(keyToUpdate)) {
            return null;
        }

        Map<String, Object> jsonMap = new ObjectMapper().readValue(json, HashMap.class);
        String script = "ctx._source." + keyToUpdate + " = jsonMap";
        UpdateRequestBuilder builder = client.prepareUpdate("vedantu", "teachers", esId)
                .setScript(script, ScriptService.ScriptType.INLINE);
        builder.addScriptParam("jsonMap", jsonMap);
        UpdateResponse response = builder.get();
        printESUpdateResponse(response);
        return response;
    }

    private void printESUpdateResponse(UpdateResponse response) {
        if (response != null) {
            logger.info("id: " + response.getId());
            logger.info("index: " + response.getIndex());
            logger.info("type: " + response.getType());
            logger.info("version: " + response.getVersion());
        }
    }

    private void printESIndexResponse(IndexResponse response) {
        if (response != null) {
            logger.info("id: " + response.getId());
            logger.info("index: " + response.getIndex());
            logger.info("type: " + response.getType());
            logger.info("version: " + response.getVersion());
        }
    }

    private void printESDeleteResponse(DeleteResponse response) {
        if (response != null) {
            logger.info("id: " + response.getId());
            logger.info("index: " + response.getIndex());
            logger.info("type: " + response.getType());
            logger.info("version: " + response.getVersion());
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing es connection ", e);
        }
    }

}
