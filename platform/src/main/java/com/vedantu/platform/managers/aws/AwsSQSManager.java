package com.vedantu.platform.managers.aws;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.Statement.Effect;
import com.amazonaws.auth.policy.actions.SQSActions;
import com.amazonaws.auth.policy.conditions.ConditionFactory;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Created by somil on 31/08/17.
 * http://developer.lightbend.com/docs/alpakka/latest/sqs.html
 */
@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    @Autowired
    private AwsSNSManager awsSNSManager;

    private String env;

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {

                CreateQueueRequest notification_dl = new CreateQueueRequest(SQSQueue.NOTIFICATION_QUEUE_DL
                        .getQueueName(env));
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(notification_dl);
                logger.info("created queue for notification deadletter");

                CreateQueueRequest cqr = new CreateQueueRequest(SQSQueue.NOTIFICATION_QUEUE
                        .getQueueName(env));
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(cqr);
                logger.info("created queue for notifications");

                assignDeadLetterQueue(SQSQueue.NOTIFICATION_QUEUE
                        .getQueueName(env), SQSQueue.NOTIFICATION_QUEUE_DL
                        .getQueueName(env), 4);

                CreateQueueRequest otf_recording_dl = new CreateQueueRequest(SQSQueue.OTF_RECORDING_DL
                        .getQueueName(env));
                otf_recording_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(otf_recording_dl);
                logger.info("created queue for otf recording deadletter");
                CreateQueueRequest otf_recording = new CreateQueueRequest(SQSQueue.OTF_RECORDING
                        .getQueueName(env));
                otf_recording.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                otf_recording.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTF_RECORDING.getVisibilityTimeout());
                sqsClient.createQueue(otf_recording);
                logger.info("created queue for otf recording");
                assignDeadLetterQueue(SQSQueue.OTF_RECORDING
                        .getQueueName(env), SQSQueue.OTF_RECORDING_DL
                        .getQueueName(env), 4);
                CreateQueueRequest otf_session_dl = new CreateQueueRequest(SQSQueue.OTF_SESSION_QUEUE_DL
                        .getQueueName(env));
                otf_session_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(otf_session_dl);
                logger.info("created queue for otf session deadletter");
                CreateQueueRequest otfSessionLink = new CreateQueueRequest(SQSQueue.OTF_SESSION_QUEUE
                        .getQueueName(env));
                otfSessionLink.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                otfSessionLink.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTF_SESSION_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(otfSessionLink);
                assignDeadLetterQueue(SQSQueue.OTF_SESSION_QUEUE
                        .getQueueName(env), SQSQueue.OTF_SESSION_QUEUE_DL
                        .getQueueName(env), 1);

                CreateQueueRequest leadSquare_dl = new CreateQueueRequest(SQSQueue.LEADSQUARED_QUEUE_DL
                        .getQueueName(env));
                sqsClient.createQueue(leadSquare_dl);
                logger.info("created queue for leadSquare deadletter");

                CreateQueueRequest leadSquaredQueue = new CreateQueueRequest(SQSQueue.LEADSQUARED_QUEUE
                        .getQueueName(env));
                leadSquaredQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.LEADSQUARED_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(leadSquaredQueue);

                assignDeadLetterQueue(SQSQueue.LEADSQUARED_QUEUE
                        .getQueueName(env), SQSQueue.LEADSQUARED_QUEUE_DL
                        .getQueueName(env), 6);

                //creating queue for cleverTap events
               /* CreateQueueRequest cleverTapQueue = new CreateQueueRequest(SQSQueue.CLEVERTAP_QUEUE
                        .getQueueName(env));
                cleverTapQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.CLEVERTAP_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(cleverTapQueue);
                logger.info("created queue:" + SQSQueue.CLEVERTAP_QUEUE.getQueueName(env));*/

                CreateQueueRequest auditQueue = new CreateQueueRequest(SQSQueue.AUDIT_QUEUE
                        .getQueueName(env));
                auditQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                auditQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.AUDIT_QUEUE.getVisibilityTimeout());
                sqsClient.createQueue(auditQueue);

                //creating queue for post session ops dead letter.
                CreateQueueRequest postSessionOps_dl = new CreateQueueRequest(SQSQueue.OTO_POSTSESSION_OPS_DL.
                        getQueueName(env));
                //postSessionOps_dl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTO_POSTSESSION_OPS_DL.getVisibilityTimeout());
                postSessionOps_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(postSessionOps_dl);
                logger.info("created dead letter queue" + SQSQueue.OTO_POSTSESSION_OPS_DL.getQueueName(env));

                //creating queue for post session ops.
                CreateQueueRequest postSessionOps = new CreateQueueRequest(SQSQueue.OTO_POSTSESSION_OPS.
                        getQueueName(env));
                postSessionOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.OTO_POSTSESSION_OPS.getVisibilityTimeout());
                postSessionOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(postSessionOps);
                logger.info("created queue:" + SQSQueue.OTO_POSTSESSION_OPS.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.OTO_POSTSESSION_OPS
                        .getQueueName(env), SQSQueue.OTO_POSTSESSION_OPS_DL
                        .getQueueName(env), 6);

                //creating queue for send GPS data to Leadsquare
                //creating queue for post session ops dead letter.
                CreateQueueRequest gpstoleadsquared_dl = new CreateQueueRequest(SQSQueue.SEND_GPS_LOCALS_TO_LS_DL.
                        getQueueName(env));
                gpstoleadsquared_dl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SEND_GPS_LOCALS_TO_LS_DL.getVisibilityTimeout());
                if (SQSQueue.SEND_GPS_LOCALS_TO_LS_DL.getFifo()) {
                    gpstoleadsquared_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                }
                sqsClient.createQueue(gpstoleadsquared_dl);
                logger.info("created dead letter queue" + SQSQueue.SEND_GPS_LOCALS_TO_LS_DL.getQueueName(env));

                CreateQueueRequest gpstoleadsquared = new CreateQueueRequest(SQSQueue.SEND_GPS_LOCALS_TO_LS.
                        getQueueName(env));

                gpstoleadsquared.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SEND_GPS_LOCALS_TO_LS.getVisibilityTimeout());
                if (SQSQueue.SEND_GPS_LOCALS_TO_LS.getFifo()) {
                    gpstoleadsquared.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                }
                sqsClient.createQueue(gpstoleadsquared);
                logger.info("created queue:" + SQSQueue.SEND_GPS_LOCALS_TO_LS.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.SEND_GPS_LOCALS_TO_LS
                        .getQueueName(env), SQSQueue.SEND_GPS_LOCALS_TO_LS_DL
                        .getQueueName(env), 6);

                CreateQueueRequest seoPageRegenerateContentImageDl = new CreateQueueRequest(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE_DL.
                        getQueueName(env));
                seoPageRegenerateContentImageDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE_DL.getVisibilityTimeout());
                seoPageRegenerateContentImageDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(seoPageRegenerateContentImageDl);
                logger.info("created dead letter queue" + SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE_DL.getQueueName(env));

                CreateQueueRequest seoPageRegenerateContentImage = new CreateQueueRequest(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE.
                        getQueueName(env));

                seoPageRegenerateContentImage.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE.getVisibilityTimeout());
                seoPageRegenerateContentImage.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(seoPageRegenerateContentImage);
                logger.info("created queue:" + SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE
                        .getQueueName(env), SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE_DL
                        .getQueueName(env), 1);

                // creating queue - pageSpeedLightHouseSQS
                CreateQueueRequest pageSpeedLightHouseSQSDl = new CreateQueueRequest(SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS_DL.
                        getQueueName(env));
                pageSpeedLightHouseSQSDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS_DL.getVisibilityTimeout());
//                pageSpeedLightHouseSQSDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "false");
                sqsClient.createQueue(pageSpeedLightHouseSQSDl);
                logger.info("created dead letter queue" + SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS_DL.getQueueName(env));

                CreateQueueRequest pageSpeedLightHouseSQS = new CreateQueueRequest(SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS.
                        getQueueName(env));

                pageSpeedLightHouseSQS.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS.getVisibilityTimeout());
//                pageSpeedLightHouseSQS.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "false");
                sqsClient.createQueue(pageSpeedLightHouseSQS);
                logger.info("created queue:" + SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS
                        .getQueueName(env), SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS_DL
                        .getQueueName(env), 4);

            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

        try {
            // Course Plan Hours Transaction Queue
            SQSQueue mainQueue = SQSQueue.COURSE_PLAN_PLATFORM_OPS;
            CreateQueueRequest coursePlanHoursTxnQueue = new CreateQueueRequest(mainQueue.getQueueName(env));
            coursePlanHoursTxnQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), mainQueue.getVisibilityTimeout());
            coursePlanHoursTxnQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), String.valueOf(mainQueue.getFifo()));
            sqsClient.createQueue(coursePlanHoursTxnQueue);
            logger.info("sqs queue created -- coursePlanHoursTxnQueue " + mainQueue.getQueueName(env));

            // dead letter queue for subscription package operations
            SQSQueue deadLetterQueue = SQSQueue.COURSE_PLAN_PLATFORM_OPS_DL;
            CreateQueueRequest coursePlanHoursTxnDlQueue = new CreateQueueRequest(deadLetterQueue.getQueueName(env));
            coursePlanHoursTxnDlQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), String.valueOf(deadLetterQueue.getFifo()));
            sqsClient.createQueue(coursePlanHoursTxnDlQueue);
            logger.info("dead letter queue created -- coursePlanHoursTxnQueue" + deadLetterQueue.getQueueName(env));

            assignDeadLetterQueue(mainQueue.getQueueName(env), deadLetterQueue.getQueueName(env), 10);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        try {
            // Enrollment created LS queue
            CreateQueueRequest enrollmentCreatedDL = new CreateQueueRequest(SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE_DL
                    .getQueueName(env));
            sqsClient.createQueue(enrollmentCreatedDL);
            logger.info("created queue for ENROLLMENT_CREATED_LEADSQUARED_QUEUE_DL");

            CreateQueueRequest enrollmentCreatedLeadSquaredQueue = new CreateQueueRequest(SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE
                    .getQueueName(env));
            enrollmentCreatedLeadSquaredQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE.getVisibilityTimeout());
            sqsClient.createQueue(enrollmentCreatedLeadSquaredQueue);

            assignDeadLetterQueue(SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE
                    .getQueueName(env), SQSQueue.ENROLLMENT_CREATED_LEADSQUARED_QUEUE_DL
                    .getQueueName(env), 6);
        } catch (Exception e) {
            logger.error("Error in creating queue ENROLLMENT_CREATED_LEADSQUARED_QUEUE " + e.getMessage());
        }

        try {
            // Enrollment created LS queue
            CreateQueueRequest enrollmentUpdatedDL = new CreateQueueRequest(SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE_DL
                    .getQueueName(env));
            sqsClient.createQueue(enrollmentUpdatedDL);
            logger.info("created queue for ENROLLMENT_UPDATED_LEADSQUARED_QUEUE_DL");

            CreateQueueRequest enrollmentCreatedLeadSquaredQueue = new CreateQueueRequest(SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE
                    .getQueueName(env));
            enrollmentCreatedLeadSquaredQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE.getVisibilityTimeout());
            sqsClient.createQueue(enrollmentCreatedLeadSquaredQueue);

            assignDeadLetterQueue(SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE
                    .getQueueName(env), SQSQueue.ENROLLMENT_UPDATED_LEADSQUARED_QUEUE_DL
                    .getQueueName(env), 6);
        } catch (Exception e) {
            logger.error("Error in creating queue ENROLLMENT_UPDATED_LEADSQUARED_QUEUE " + e.getMessage());
        }

    }

    private void assignDeadLetterQueue(String src_queue_name, String dl_queue_name, Integer maxReceive) {
        if (maxReceive == null) {
            maxReceive = 4;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                        .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\"" + maxReceive + "\", \"deadLetterTargetArn\":\""
                        + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: " + dl_queue_name + "  to:" + src_queue_name);

    }

    public void setPermissions(String m_snsName, String sqsArn, String queueUrl) {

        Statement statement = new Statement(Effect.Allow)
                .withActions(SQSActions.SendMessage)
                .withPrincipals(new Principal("*"))
                .withConditions(ConditionFactory.newSourceArnCondition(m_snsName))
                .withResources(new Resource(sqsArn));
        Policy policy = new Policy("SubscriptionPermission")
                .withStatements(statement);

        HashMap<String, String> attributes = new HashMap();
        attributes.put("Policy", policy.toJson());
        SetQueueAttributesRequest request = new SetQueueAttributesRequest(queueUrl, attributes);
        sqsClient.setQueueAttributes(request);

    }

    public void createSubscription(SNSTopic topicName, SQSQueue queueName) {
        try{
            String queueUrl = getQueueURL(queueName);
            String sqsArn = getQueueArn(queueName);
            awsSNSManager.createSubscription(topicName, "sqs", sqsArn);
            setPermissions(awsSNSManager.getTopicArn(topicName), sqsArn, queueUrl);
        }catch (Exception e){

        }
    }

    @PostConstruct
    public void createSNSSubscription() {
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
//            createSubscription(SNSTopic.CMS_WEBINAR_EVENTS, SQSQueue.CLEVERTAP_QUEUE);
        }

    }

}
