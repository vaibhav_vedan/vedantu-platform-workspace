package com.vedantu.platform.managers.wave;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.wave.WavebookDAO;
import com.vedantu.platform.enums.wave.WavebookScope;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.pojo.wave.Wavebook;
import com.vedantu.platform.pojo.wave.WavebookVersion;
import com.vedantu.platform.request.wave.CreateWavebookReq;
import com.vedantu.platform.request.wave.EditWavebookReq;
import com.vedantu.util.FosUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WavebookManager {

    @Autowired
    LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(WavebookManager.class);

    @Autowired
    private WavebookDAO wavebookDAO;

    @Autowired
    UserManager userManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private FosUtils fosUtils;

    public Wavebook createWavebook(CreateWavebookReq createWavebookReq) throws BadRequestException, VException, Exception {
        createWavebookReq.verify();
        //TODO validate topics and topicgrades coming in req with lms curriculum/base tree
        Wavebook res = new Wavebook(createWavebookReq);
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        res.setUserId(httpSessionData.getUserId());
        if (!res.getUserId().equals(httpSessionData.getUserId())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Selected userId doesnot match with logged in user");
        }
        User user = userManager.getUserById(res.getUserId());
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "No user found with this userId:" + res.getUserId());
        }
        if(WavebookVersion.PAPERJS_WHITEBOARD.equals(res.getWavebookVersion())){
            AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
            atte.settTopics(createWavebookReq.getTopics());
            atte.setTargetGrades(createWavebookReq.getTargetGrades());
            AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);
            res.setTags(abstractTargetTopicEntity);
        }
        res.setUserRole(user.getRole());
        wavebookDAO.upsert(res, createWavebookReq.getCallingUserId());
        return res;
    }

    public Wavebook deleteWavebook(String wavebookId, Long callingUserId) throws BadRequestException, NotFoundException, ForbiddenException {
        if (StringUtils.isEmpty(wavebookId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No wavebookId");
        }
        Wavebook res = wavebookDAO.getById(wavebookId);
        if (res == null) {
            throw new NotFoundException(ErrorCode.WAVEBOOK_NOT_FOUND, "No wavebook found with id:" + wavebookId);
        }
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (!(res.getUserId().equals(httpSessionData.getUserId()) || (res.getUserRole().equals(Role.ADMIN) && res.getUserRole().equals(httpSessionData.getRole())))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "You are forbidden from deleting this wavebook");
        }
        res.setDeleted(true);
        wavebookDAO.upsert(res, callingUserId);
        return res;
    }

    public Wavebook editWavebook(EditWavebookReq editWavebookReq) throws BadRequestException, NotFoundException, ForbiddenException, Exception {
        editWavebookReq.verify();
        Wavebook res = wavebookDAO.getById(editWavebookReq.getWavebookId());
        if (res == null) {
            throw new NotFoundException(ErrorCode.WAVEBOOK_NOT_FOUND, "No wavebook found with id:" + editWavebookReq.getWavebookId());
        }
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
//        if (!(res.getUserId().equals(httpSessionData.getUserId()) || (res.getUserRole().equals(Role.ADMIN) && res.getUserRole().equals(httpSessionData.getRole())))) {
//            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "You are forbidden from editing this wavebook");
//        }
        if (editWavebookReq.getTitle() != null) {
            res.setTitle(editWavebookReq.getTitle());
        }
        if (editWavebookReq.getPagesMetaData() != null) {
            res.setPagesMetaData(editWavebookReq.getPagesMetaData());
        }

        if (editWavebookReq.getPagesMetaDataNew() != null) {
            res.setPagesMetaDataNew(editWavebookReq.getPagesMetaDataNew());
        }
        if (editWavebookReq.getWavebookScope() != null) {
            res.setWavebookScope(editWavebookReq.getWavebookScope());
        }
        if (editWavebookReq.getGrades() != null) {
            res.setGrades(editWavebookReq.getGrades());
        }
        if (editWavebookReq.getSubjects() != null) {
            res.setSubjects(editWavebookReq.getSubjects());
        }

        if(WavebookVersion.PAPERJS_WHITEBOARD.equals(res.getWavebookVersion())) {
            AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
            atte.settTopics(editWavebookReq.getTopics());
            atte.setTargetGrades(editWavebookReq.getTargetGrades());
            AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);
            res.setTags(abstractTargetTopicEntity);
        }

        res.setWavebookType(editWavebookReq.getWavebookType());
        wavebookDAO.upsert(res, editWavebookReq.getCallingUserId());
        return res;
    }

    public List<Wavebook> getWavebooksByUserId(Long userId, WavebookScope scope, Integer start, Integer size,
                                               WavebookVersion wavebookVersion) throws BadRequestException {
        List<Wavebook> res = null;
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No userId");
        }
        res = wavebookDAO.getWavebooksByUserId(userId, scope, start, size, wavebookVersion);
        return res;
    }

    public List<Wavebook> getPublicWavebooks(Integer start, Integer size) throws BadRequestException {
        List<Wavebook> res = null;
        res = wavebookDAO.getPublicWavebooks(start, size, Role.ADMIN);
        return res;
    }

    public Wavebook getWavebookDetails(String wavebookId) throws BadRequestException, NotFoundException {
        if (StringUtils.isEmpty(wavebookId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No wavebookId");
        }
        Wavebook res = wavebookDAO.getById(wavebookId);
        if (res == null || res.isDeleted()) {
            throw new NotFoundException(ErrorCode.WAVEBOOK_NOT_FOUND, "No wavebook found with id:" + wavebookId);
        }
        return res;
    }

    public List<Wavebook> getAllWavebooks(WavebookScope scope, Integer start, Integer size,
                                          WavebookVersion wavebookVersion) throws BadRequestException {
        List<Wavebook> res = null;

        res = wavebookDAO.getAllWavebooks(scope, start, size, wavebookVersion);
        return res;
    }

}
