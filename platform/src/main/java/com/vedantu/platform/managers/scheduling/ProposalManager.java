/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.scheduling;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.pojo.subscription.SessionVO;
import com.vedantu.platform.pojo.subscription.SubscriptionSessionRequestInfo;
import com.vedantu.platform.utils.PojoUtils;
import com.vedantu.scheduling.request.proposal.AddProposalReq;
import com.vedantu.scheduling.request.proposal.GetProposalReq;
import com.vedantu.scheduling.request.proposal.GetProposalsReq;
import com.vedantu.scheduling.response.proposal.ProposalRes;
import com.vedantu.scheduling.request.proposal.UpdateProposalReq;
import com.vedantu.scheduling.response.proposal.GetProposalsRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.proposal.ProposalSlot;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.ProposalStatus;
import com.vedantu.util.enums.SessionModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author ajith
 */
@Service
public class ProposalManager {

    private final static Gson gson = new Gson();

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ProposalManager.class);

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private String SCHEDULING_ENDPOINT;

    @Autowired
    PojoUtils pojoUtils;

    @PostConstruct
    public void init() {
        SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    }

    public ProposalManager() {
        super();
    }

    public ProposalRes addProposal(AddProposalReq req)
            throws VException {
        req.verify();
        List<Role> roles = new ArrayList<>();
        roles.add(Role.TEACHER);
        roles.add(Role.STUDENT);
        httpSessionUtils.checkIfAllowedList(req.getFromUserId(), roles, true);

        List<ProposalSlot> proposalSlots = req.getProposalSlotList();
        if (ArrayUtils.isNotEmpty(proposalSlots)) {
            Long allowedStartTime = System.currentTimeMillis()
                    + ConfigUtils.INSTANCE.getLongValue("proposalSlot.window.time.millis");
            for (ProposalSlot proposalSlot : proposalSlots) {
                if (proposalSlot.getStartTime() < allowedStartTime) {
                    throw new ConflictException(ErrorCode.PROPOSAL_SLOT_5MIN_ERROR, "Cannot add proposal 5min before starttime");
                }
                if (StringUtils.isEmpty(proposalSlot.getTitle())) {
                    proposalSlot.setTitle("Subscription session");
                }
            }
        }

        SubscriptionResponse subscriptionResponse = null;
        if (req.getSubscriptionId() != null) {
            subscriptionResponse = subscriptionManager.getSubscriptionBasicInfo(req.getSubscriptionId());
            if (subscriptionResponse.getTeacherId().equals(req.getFromUserId())
                    && subscriptionResponse.getTeacherId().equals(req.getToUserId())
                    && subscriptionResponse.getStudentId().equals(req.getFromUserId())
                    && subscriptionResponse.getStudentId().equals(req.getToUserId())) {
                throw new ForbiddenException(ErrorCode.PROPOSAL_NO_ACCESS, "Not part of the subscription sent");
            }
            if(req.getBoardId()==null){
                req.setBoardId(subscriptionResponse.getBoardId());
            }
        }

        ProposalRes proposalRes = null;
        //TODO change this and ask front end to send multiple requests or product
        //to give option to edit slots/cancel slots separately        
        if (ArrayUtils.isNotEmpty(proposalSlots)) {
            List<ProposalSlot> oldproposalSlots = new ArrayList<>(req.getProposalSlotList());
            for (ProposalSlot proposalSlot : oldproposalSlots) {
                req.getProposalSlotList().clear();
                List<ProposalSlot> newProposalSlots = req.getProposalSlotList();
                newProposalSlots.add(proposalSlot);
                req.setProposalSlotList(newProposalSlots);
                req.setStartTime(proposalSlot.getStartTime());
                req.setEndTime(proposalSlot.getEndTime());
                String jsonReq = gson.toJson(req);
                logger.info(jsonReq);
                ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/proposal/add", HttpMethod.POST,
                        jsonReq);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String proposalResStr = resp.getEntity(String.class);
                proposalRes = gson.fromJson(proposalResStr, ProposalRes.class);
                fillProposalDataAndSendLeadActivity(proposalRes, req.getCallingUserId());
                sendProposalCommunication(proposalRes, (subscriptionResponse != null) ? subscriptionResponse.getTitle() : null);
            }
        }
        return proposalRes;
    }

    public ProposalRes updateProposal(UpdateProposalReq req)
            throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not supported for now, contact backend team");
    }

    public ProposalRes confirmProposal(UpdateProposalReq req) throws VException {
        req.verify();
        List<ProposalSlot> proposalSlots = req.getProposalSlotList();
        if (ArrayUtils.isNotEmpty(proposalSlots)) {
            Long allowedStartTime = System.currentTimeMillis()
                    + ConfigUtils.INSTANCE.getLongValue("proposalSlot.window.time.millis");
            for (ProposalSlot proposalSlot : proposalSlots) {
                if (proposalSlot.getStartTime() < allowedStartTime) {
                    throw new ConflictException(ErrorCode.PROPOSAL_SLOT_5MIN_ERROR, "Cannot add proposal 5min before starttime");
                }
            }
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/proposal/confirm", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String proposalResStr = resp.getEntity(String.class);
        ProposalRes proposalRes = gson.fromJson(proposalResStr, ProposalRes.class);

        fillProposalDataAndSendLeadActivity(proposalRes, req.getCallingUserId());
        if (ProposalStatus.CONFIRMED.equals(proposalRes.getProposalStatus())) {
            createSubscriptionSession(proposalRes, proposalRes.getFromUser(), proposalRes.getToUser());
        }
        sendProposalCommunication(proposalRes, null);
        return proposalRes;
    }

    private void sendProposalCommunication(ProposalRes proposalRes, String communicationTitle) {
        Long teacherId = null;
        Long studentId = null;
        UserBasicInfo fromUserBasicInfo = proposalRes.getFromUser();
        UserBasicInfo toUserBasicInfo = proposalRes.getToUser();

        if (Role.STUDENT.equals(fromUserBasicInfo.getRole())) {
            teacherId = toUserBasicInfo.getUserId();
            studentId = fromUserBasicInfo.getUserId();
        } else if (Role.TEACHER.equals(fromUserBasicInfo.getRole())) {
            studentId = toUserBasicInfo.getUserId();
            teacherId = fromUserBasicInfo.getUserId();
        }

        List<ProposalSlot> proposalSlots = proposalRes.getProposalSlotList();
        if (teacherId != null && studentId != null && ArrayUtils.isNotEmpty(proposalSlots)) {
            ProposalSlot proposalSlot = proposalSlots.get(0);
            SubscriptionSessionRequestInfo subscriptionSessionRequestInfo = new SubscriptionSessionRequestInfo(
                    studentId, teacherId, proposalSlot.getStartTime(), proposalSlot.getEndTime(),
                    proposalRes.getReason(), proposalRes.getSubscriptionId(),
                    (communicationTitle != null) ? communicationTitle : proposalSlot.getTitle());

            CommunicationType communicationType = null;
            if (null != proposalRes.getProposalStatus()) {
                switch (proposalRes.getProposalStatus()) {
                    case PENDING:
                        communicationType = CommunicationType.SUBSCRIPTION_SESSION_REQUEST_BY_TEACHER;
                        break;
                    case CONFIRMED:
                        communicationType = CommunicationType.SUBSCRIPTION_SESSION_REQUEST_ACCEPTED;
                        break;
                    case REJECTED:
                        communicationType = CommunicationType.SUBSCRIPTION_SESSION_REQUEST_REJECTED;
                        break;
                    case CANCELED:
                        communicationType = CommunicationType.SUBSCRIPTION_SESSION_REQUEST_CANCEL_BY_TEACHER;
                        break;
                    default:
                        break;
                }
            }
            if (communicationType == null) {
                return;
            }
            Map<String, Object> payload = new HashMap<>();
            payload.put("communicationType", communicationType);
            payload.put("subscriptionSessionRequestInfo", subscriptionSessionRequestInfo);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_SESSION_REQUEST_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
        }

    }

    public ProposalRes getProposal(GetProposalReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        req.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/proposal/getProposal?" + WebUtils.INSTANCE.createQueryStringOfObject(req), HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String proposalResStr = resp.getEntity(String.class);
        ProposalRes proposalRes = gson.fromJson(proposalResStr, ProposalRes.class);
        fillProposalData(proposalRes);
        return proposalRes;
    }

    public GetProposalsRes getProposals(GetProposalsReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        if (req.getUserId() != null) {
            httpSessionUtils.checkIfAllowed(req.getUserId(), null, true);
        } else {
            httpSessionUtils.checkIfAllowed(null, Role.STUDENT_CARE, true);
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/proposal/getProposals?" + WebUtils.INSTANCE.createQueryStringOfObject(req),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String proposalResStr = resp.getEntity(String.class);
        GetProposalsRes getProposalsRes = gson.fromJson(proposalResStr, GetProposalsRes.class);

        Set<Long> userIds = new HashSet<>();
        List<Long> boardIds = new ArrayList<>();
        List<ProposalRes> res = getProposalsRes.getList();
        if (ArrayUtils.isNotEmpty(res)) {
            for (ProposalRes proposalRes : res) {
                userIds.add(proposalRes.getFromUserId());
                userIds.add(proposalRes.getToUserId());
                if (proposalRes.getBoardId() != null) {
                    boardIds.add(proposalRes.getBoardId());
                }
            }

            Map<Long, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
            Map<Long, com.vedantu.platform.mongodbentities.board.Board> boardsMap = boardManager.getBoardsMap(boardIds);

            for (ProposalRes proposalRes : res) {
                proposalRes.setFromUser(userInfos.get(proposalRes.getFromUserId()));
                proposalRes.setToUser(userInfos.get(proposalRes.getToUserId()));
                if (proposalRes.getBoardId() != null && boardsMap.containsKey(proposalRes.getBoardId())) {
                    proposalRes.setBoard(pojoUtils.convertToBoardPojo(boardsMap.get(proposalRes.getBoardId())));
                }
            }
        }
        return getProposalsRes;
    }

    private void createSubscriptionSession(final ProposalRes proposal, UserBasicInfo fromUser, UserBasicInfo toUser) throws InternalServerErrorException, VException {
        SessionVO sessionVO = new SessionVO();
        sessionVO.setRequestorId(proposal.getCreatedBy());

        //TODO create multiple sessions
        List<ProposalSlot> proposalSlots = proposal.getProposalSlotList();
        ProposalSlot proposalSlot = proposalSlots.get(0);
        SessionSlot slot = new SessionSlot(proposalSlot.getStartTime(), proposalSlot.getEndTime(),
                proposal.getTopicName(), proposalSlot.getTitle(), null, Boolean.FALSE);
        List<SessionSlot> sessionSlots = new ArrayList<>();
        sessionSlots.add(slot);
        sessionVO.setSessionSlots(sessionSlots);
        sessionVO.setEntitytype(EntityType.SUBSCRIPTION);
        sessionVO.setEntityId(proposal.getSubscriptionId());
        sessionVO.setProposalId(proposal.getId());
        sessionVO.setModel(SessionModel.OTO);
        sessionVO.setReason(proposal.getReason());
        if (fromUser.getRole().equals(Role.STUDENT)) {
            sessionVO.setStudentId(fromUser.getUserId());
            sessionVO.setTeacherId(toUser.getUserId());
        } else {
            sessionVO.setStudentId(toUser.getUserId());
            sessionVO.setTeacherId(fromUser.getUserId());
        }
        sessionVO.setTitle(proposal.getTitle());
        subscriptionManager.bookSessionFromSubscriptionHours(sessionVO);
    }

    private void sendLeadActivity(ProposalRes proposal, User toUser, User fromUser, Long callingUserId)
            throws BadRequestException {
        Map<String, String> params = new HashMap<>();
        params.put("proposalStatus", proposal.getProposalStatus().name());
        params.put("proposalId", String.valueOf(proposal.getId()));
        params.put("teacherId", Role.STUDENT.equals(toUser.getRole()) ? String.valueOf(proposal.getFromUserId())
                : String.valueOf(proposal.getToUserId()));
        params.put("proposalId", String.valueOf(proposal.getId()));
        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_PROPOSAL,
                Role.STUDENT.equals(toUser.getRole()) ? toUser : fromUser, params, callingUserId);
        leadSquaredManager.executeAsyncTask(request);
    }

    private void fillProposalData(ProposalRes proposalRes) throws VException {
        List<Long> userIds = new ArrayList<>();
        Long fromUserId = proposalRes.getFromUserId();
        Long toUserId = proposalRes.getToUserId();
        userIds.add(fromUserId);
        userIds.add(toUserId);
        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, false);
        User fromUser = userInfos.get(proposalRes.getFromUserId());
        User toUser = userInfos.get(proposalRes.getToUserId());
        proposalRes.setFromUser(new UserBasicInfo(fromUser, false));
        proposalRes.setToUser(new UserBasicInfo(toUser, false));
        if (proposalRes.getBoardId() != null) {
            Board board = pojoUtils.convertToBoardPojo(boardManager.getBoardById(proposalRes.getBoardId()));
            proposalRes.setBoard(board);
        }
    }

    private void fillProposalDataAndSendLeadActivity(ProposalRes proposalRes, Long callingUserId) throws VException {
        List<Long> userIds = new ArrayList<>();
        Long fromUserId = proposalRes.getFromUserId();
        Long toUserId = proposalRes.getToUserId();
        userIds.add(fromUserId);
        userIds.add(toUserId);
        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, false);
        User fromUser = userInfos.get(proposalRes.getFromUserId());
        User toUser = userInfos.get(proposalRes.getToUserId());
        proposalRes.setFromUser(new UserBasicInfo(fromUser, false));
        proposalRes.setToUser(new UserBasicInfo(toUser, false));
        if (proposalRes.getBoardId() != null) {
            Board board = pojoUtils.convertToBoardPojo(boardManager.getBoardById(proposalRes.getBoardId()));
            proposalRes.setBoard(board);
        }
        if (!ProposalStatus.UNSENT.equals(proposalRes.getProposalStatus())) {
            sendLeadActivity(proposalRes, toUser, fromUser, callingUserId);
        }
    }
}
