/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.notification;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Gender;
import com.vedantu.User.MyEntry;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.VoltStatus;
import com.vedantu.User.request.GeneratePasswordReq;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.User.response.EditProfileRes;
import com.vedantu.User.response.GeneratePasswordRes;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.request.SendInstalmentPaidEmailReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.RedeemCouponInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptPojo;
import com.vedantu.lms.cmds.pojo.TestContentInfoMetadataPojo;
import com.vedantu.lms.cmds.request.ShareContentAndSendMailRes;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailService;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojos.FreebieInfo;
import com.vedantu.notification.pojos.InstalmentNotificationReq;
import com.vedantu.notification.pojos.MoodleContentInfo;
import com.vedantu.notification.pojos.MoodleNotificationData;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.GetUnsubscribedListReq;
import com.vedantu.notification.requests.SendMoodleNotificationsReq;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.notification.requests.UnsubscribeEmailReq;
import com.vedantu.notification.responses.GetUnsubscribedListRes;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.onetofew.pojo.StudentSlotPreferencePojo;
import com.vedantu.onetofew.request.GetBatchesReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.board.BoardDAO;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.cms.WebinarManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.scheduling.InstalearnManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.managers.subscription.ARMFormManager;
import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.platform.managers.subscription.BundlePackageManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager.CoursePlanHourInconsistentDetails;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.mongodbentities.ISLReport;
import com.vedantu.platform.mongodbentities.UserMessage;
import com.vedantu.platform.mongodbentities.offering.Offering;
import com.vedantu.platform.mongodbentities.review.Feedback;
import com.vedantu.platform.mongodbentities.review.Review;
import com.vedantu.platform.pojo.ISLReportDetail;
import com.vedantu.platform.pojo.cms.Webinar;
import com.vedantu.platform.pojo.dinero.Transaction;
import com.vedantu.platform.pojo.onetofew.Conflict;
import com.vedantu.platform.pojo.referral.model.ReferralStepBonus;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestNotificationInfo;
import com.vedantu.platform.pojo.subscription.SubscriptionSessionRequestInfo;
import com.vedantu.platform.request.dinero.SendInstalmentEmailReq;
import com.vedantu.platform.userleads.SendISLEmailReq;
import com.vedantu.scheduling.enums.instalearn.InstaState;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.request.session.AdminSessionUpdateReq;
import com.vedantu.scheduling.request.session.CheckSessionTOSReq;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.scheduling.response.instalearn.SubmitInstaRequestRes;
import com.vedantu.scheduling.response.instalearn.TeacherSubscriptionResponse;
import com.vedantu.scheduling.response.session.CheckSessionTOSRes;
import com.vedantu.session.pojo.OTFCourseBasicInfo;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionSortType;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.subscription.enums.BundleContentType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.pojo.CoursePlanAdminData;
import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.subscription.request.AddARMFormReq;
import com.vedantu.subscription.request.CoursePlanAdminAlertReq;
import com.vedantu.subscription.request.CoursePlanReqFormReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.response.ARMFormInfo;
import com.vedantu.subscription.response.BundleEnrollmentResp;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.ListUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.MoneyUtils;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebCommunicator;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.pojo.VsatSchedule;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

/**
 *
 * @author somil
 */
@Service
public class CommunicationManager {

    public static final String PARTNER_LINK = "partnerLink";
    public static final String PARTNER_NAME = "partnerName";
    public static final String PARTNER_CONTACT_NUMBER = "partnerContactNumber";
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    public static final String TIME_ZONE_GMT = "GMT";
    public static String TEACHER_CARE_EMAIL_ID = new String();
    public static String ACADEMICS_EMAIL_ID = new String();
    public static String DNA_EMAIL_ID = new String();
    public static String PRODUCT_EMAIL_ID = new String();
    public static String TRAIL_UPDATE_EMAIL_ID = new String();

    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mma");
    private static final SimpleDateFormat sdfDateDay = new SimpleDateFormat("E, dd MMM yyyy");

    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

    public static final int TASK_BUFFER_IN_MILLIES = 5 * DateTimeUtils.MILLIS_PER_MINUTE;
    public String[] hyderabadVariations = {"Hyd", "Hyderabad", "Hderabad", "Hyd", "Hyderbad", "Hyderebad", "hydreabad",
        "hydrabad", "hyedrabad", "hyderabad`", "hyderabah"};

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CommunicationManager.class);

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private UserManager userManager;

    @Autowired
    private NotificationManager notificationManager;

    @Autowired
    private FosUtils userUtils;

    @Autowired
    private InstalearnManager instalearnManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SubscriptionManager subscriptionManager;

//    @Autowired
//    private CoursePlanManager coursePlanManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private BundlePackageManager bundlePackageManager;

    @Autowired
    private ARMFormManager armFormManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private ReferralManager referralManager;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    private static final String URL_SHORTNER_API_KEY = ConfigUtils.INSTANCE.getStringValue("google.url.shortner.api.key");

    @PostConstruct
    public void init() {
        TEACHER_CARE_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.teacherCare");
        ACADEMICS_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.academics");
        DNA_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.dna");
        PRODUCT_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.product");
        TRAIL_UPDATE_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.trail.update");
    }

    public void sendWelcomeEmail(User user) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        CommunicationType emailType = CommunicationType.WELCOME;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", user.getFirstName());
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);
        if (Role.STUDENT.equals(user.getRole())) {
            if (user.getStudentInfo() != null) {
                String grade = user.getStudentInfo().getGrade();
                bodyScopes.put("grade", grade);
                if (org.apache.commons.lang3.StringUtils.equals(grade, "11")
                        || org.apache.commons.lang3.StringUtils.equals(grade, "12")) {
                    bodyScopes.put("isSeniorStudent", true);
                }
            }
        }
        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        bodyScopes.put("signupBonus", ConfigUtils.INSTANCE.getStringValue("freebies.rupees.join.bonus"));

        String freeBieHTML = "";

        bodyScopes.put("freeBieHTML", freeBieHTML);

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());
        if (Role.STUDENT.equals(user.getRole())) {
            request.setIncludeHeaderFooter(false);
            request.setClickTrackersEnabled(true);
        }
        sendEmail(request);

        // postEmailLog(user.getId(), null, emailType,
        // email.getFrom().toString(), email.getTo().toString(),
        // InternetAddress.toString(email.getBcc()),
        // InternetAddress.toString(email.getCc()), email.getSubject(),
        // email.getBody());
    }

    public void sendVerificationTokenEmail(UserBasicInfo user, CommunicationType emailType, String emailTokenCode)
            throws VException, UnsupportedEncodingException {
        if(StringUtils.isEmpty(user.getEmail())){
            return;
        }
        logger.info("Sending verification email" + user);
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", user.getFirstName());
        bodyScopes.put("role", user.getRole());
        String pronoun = (null != user.getGender() && null != user.getGender().getPronoun())
                ? user.getGender().getPronoun()
                : Gender.OTHER.getPronoun();
        bodyScopes.put("pronoun", pronoun);
        bodyScopes.put("Pronoun", StringUtils.captialize(pronoun));
        String verifyUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl")
                + ConfigUtils.INSTANCE.getStringValue("url.verify.token") + emailTokenCode;
        bodyScopes.put("link", verifyUrl);

        String signupBonus = ConfigUtils.INSTANCE.getStringValue("freebies.rupees.join.bonus");
        bodyScopes.put("signupBonus", signupBonus);

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        subjectScopes.put("rupees", signupBonus);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        sendEmail(request);
    }

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail" + new Gson().toJson(request));
//		String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
//				new Gson().toJson(request));
//
//		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//		String jsonString = resp.getEntity(String.class);
//		logger.info("Response from notification-centre " + jsonString);
//		return jsonString;

        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, new Gson().toJson(request));
        return null;
    }

    public String sendEmailViaRest(EmailRequest request) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public String sendBulkEmail(EmailRequest request) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendBulkEmail", HttpMethod.POST,
                new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public void sendFreebieBalanceEmail(UserBasicInfo userBasicInfo, Integer amount, Boolean noSMS)
            throws VException, UnsupportedEncodingException {
        logger.info("sending sendFreebieBalanceEmail : " + userBasicInfo.getEmail());
        if (Boolean.TRUE.equals(noSMS)) {
            logger.info("sendFreebieBalanceEmail - SMS skipped");
            return;
        }
        CommunicationType communicationType = CommunicationType.FREEBIES;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", userBasicInfo.getFirstName());
        int rupees = MoneyUtils.toRupees(amount);
        bodyScopes.put("rupees", rupees);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        sendEmail(request);
        if (StringUtils.isNotEmpty(userBasicInfo.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(userBasicInfo.getContactNumber(),
                    userBasicInfo.getPhoneCode(), bodyScopes, communicationType, userBasicInfo.getRole());
            smsManager.sendSMS(textSMSRequest);
        }
    }

    public void sendTransferFromFreebiesCommunication(Integer amount, Long toUserId, Boolean noSMS)
            throws VException, UnsupportedEncodingException {
        FreebieInfo freebieInfo = new FreebieInfo();
        int rupees = MoneyUtils.toRupees(amount);
        freebieInfo.setType("walletBalance");
        freebieInfo.setValue(rupees);

        NotificationType notificationType = NotificationType.FREEBIES;

        UserBasicInfo userBasicInfo = userUtils.getUserBasicInfo(toUserId.toString(), true);

        if (!Boolean.TRUE.equals(noSMS)) {
            CreateNotificationRequest request = new CreateNotificationRequest(toUserId, notificationType,
                    new Gson().toJson(freebieInfo), userBasicInfo.getRole());
            // notificationManager.createNotification(toUserId,
            // notificationType, freebieInfo, null);
            notificationManager.createNotification(request);
        }
        sendFreebieBalanceEmail(userBasicInfo, amount, noSMS);

    }

    public void sendOTFEnrollmentEmail(EnrollmentPojo enrollmentInfo, OrderInfo orderInfo) throws UnsupportedEncodingException, VException {
        CommunicationType communicationType;

        if (enrollmentInfo.isCreatedNew()) {//usually no registration is there and direct regular enrollment is created
            if (orderInfo != null && PaymentType.INSTALMENT.equals(orderInfo.getPaymentType())) {
                communicationType = CommunicationType.OTF_BATCH_NEW_ENROLLMENT_FIRST_INSTALMENT_PAYMENT;
            } else {
                communicationType = CommunicationType.OTF_BATCH_NEW_ENROLLMENT_BULK_PAYMENT;//fallback
            }
        } else {
            if (orderInfo != null && PaymentType.INSTALMENT.equals(orderInfo.getPaymentType())) {
                communicationType = CommunicationType.OTF_BATCH_OLD_ENROLLMENT_FIRST_INSTALMENT_PAYMENT;
            } else {
                communicationType = CommunicationType.OTF_BATCH_OLD_ENROLLMENT_BULK_PAYMENT;//fallback
            }
        }

        UserBasicInfo userBasicInfo = enrollmentInfo.getUser();
        CourseBasicInfo courseInfo = enrollmentInfo.getCourse();
        if (userBasicInfo == null || courseInfo == null) {
            logger.error("sendOTFEnrollmentEmail - userBasicInfo pr course basic info not found, enrollmentId " + enrollmentInfo.getId());
            return;
        }
        BatchBasicInfo batchBasicInfo = enrollmentInfo.getBatch();
        if (batchBasicInfo == null) {
            logger.error("sendOTFEnrollmentEmail - student info or batch basic info not found, enrollmentId " + enrollmentInfo.getId());
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("courseName", courseInfo.getTitle());

        if (orderInfo != null) {
            Integer amountPaid = orderInfo.getAmount();
            if (PaymentType.INSTALMENT.equals(orderInfo.getPaymentType())) {
                amountPaid = orderInfo.getAmountPaid();
            }
            bodyScopes.put("amountPaid", (amountPaid / 100));
        }

        SimpleDateFormat datefb = new SimpleDateFormat("EEE, dd MMM yyyy");
        datefb.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String paymentDate = datefb.format(new Date());
        bodyScopes.put("paymentDate", paymentDate);

//        bodyScopes.put("instalmentDueDate", null);//TODO
        if (ArrayUtils.isNotEmpty(courseInfo.getTargets())) {
            List<String> targets = new ArrayList<>(courseInfo.getTargets());
            String batchLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + targets.get(0)
                    + "/course/" + courseInfo.getId() + "/batch-details/list?enrolled=true&batchId="
                    + batchBasicInfo.getBatchId();
            bodyScopes.put("batchLink", batchLink);
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        sendEmail(request);
    }

    // subscription related emails
    public void sendSubscriptionEndedMail(CancelSubscriptionResponse cancelSubscriptionResponse,
            HttpSessionData httpSessionData)
            throws NotFoundException, UnsupportedEncodingException, AddressException, VException {
        List<Long> userIds = new ArrayList<>();
        Long teacherId = cancelSubscriptionResponse.getTeacherId();
        Long studentId = cancelSubscriptionResponse.getStudentId();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacherBasicInfo = userInfos.get(teacherId);
        UserBasicInfo studentBasicInfo = userInfos.get(studentId);
        Map<String, Object> bodyScopes = new HashMap<>();

        if (teacherBasicInfo == null || studentBasicInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "teacherBasicInfo or studentBasicInfo not found");
        }

        String teacherName = teacherBasicInfo.getFullName();
        String studentName = studentBasicInfo.getFullName();
        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }
        bodyScopes.put("studentName", studentName);
        bodyScopes.put("teacherName", teacherName);
        bodyScopes.put("subscriptionName", cancelSubscriptionResponse.getSubscriptionName());
        bodyScopes.put("totalHours", cancelSubscriptionResponse.getTotalHours() / DateTimeUtils.MILLIS_PER_HOUR);
        bodyScopes.put("perHourPrice", cancelSubscriptionResponse.getHourlyRate() / 100);

        List<InternetAddress> toList = new ArrayList<>();
        List<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress(TEACHER_CARE_EMAIL_ID));
        CommunicationType emailType;
        if (httpSessionData != null
                && (Role.TEACHER.equals(httpSessionData.getRole()) || Role.STUDENT.equals(httpSessionData.getRole()))) {
            if (Role.TEACHER.equals(httpSessionData.getRole())) {
                emailType = CommunicationType.SUBSCRIPTION_ENDED_BY_TEACHER;
            } else {
                emailType = CommunicationType.SUBSCRIPTION_ENDED;
            }
        } else {
            return;
        }

        Long consumedHoursInSecs = (cancelSubscriptionResponse.getTotalHours()
                - (cancelSubscriptionResponse.getRemainingHours() + cancelSubscriptionResponse.getLockedHours()))
                / DateTimeUtils.MILLIS_PER_SECOND;
        String consumedHoursStr = DateTimeUtils.printableTimeStringHHmmSS(consumedHoursInSecs.intValue()) + " hrs";

        Long remainingHoursInSecs = cancelSubscriptionResponse.getRemainingHours() / DateTimeUtils.MILLIS_PER_SECOND;
        String remainingHoursStr = DateTimeUtils.printableTimeStringHHmmSS(remainingHoursInSecs.intValue()) + " hrs";

        bodyScopes.put("consumedHours", consumedHoursStr);
        bodyScopes.put("remainingHours", remainingHoursStr);
        Long refundAmount = (cancelSubscriptionResponse.getRemainingHours() * cancelSubscriptionResponse.getHourlyRate()
                / DateTimeUtils.MILLIS_PER_HOUR);
        refundAmount += cancelSubscriptionResponse.getSecurity();
        bodyScopes.put("refundAmount", refundAmount / 100);
        bodyScopes.put("refundReason", cancelSubscriptionResponse.getRefundReason());
        EmailRequest request;
        TextSMSRequest smsRequest;
        CreateNotificationRequest createNotificationRequest;
        switch (emailType) {
            case SUBSCRIPTION_ENDED:

                toList.add(new InternetAddress(studentBasicInfo.getEmail(), studentBasicInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, studentBasicInfo.getRole());
                request.setBcc(bccList);
                sendEmail(request);

                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(studentBasicInfo.getContactNumber(), studentBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                toList.clear();
                toList.add(new InternetAddress(teacherBasicInfo.getEmail(), teacherBasicInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.TEACHER);
                request.setBcc(bccList);
                sendEmail(request);
                if (StringUtils.isNotEmpty(teacherBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.TEACHER);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(cancelSubscriptionResponse.getTeacherId(),
                        NotificationType.valueOf(emailType.name()), new Gson().toJson(bodyScopes), Role.TEACHER);

                notificationManager.createNotification(createNotificationRequest);

                break;
            case SUBSCRIPTION_ENDED_BY_TEACHER:

                toList.add(new InternetAddress(studentBasicInfo.getEmail(), studentBasicInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, studentBasicInfo.getRole());
                request.setBcc(bccList);
                sendEmail(request);

                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(studentBasicInfo.getContactNumber(), studentBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(cancelSubscriptionResponse.getStudentId(),
                        NotificationType.valueOf(emailType.name()), new Gson().toJson(bodyScopes), Role.STUDENT);

                notificationManager.createNotification(createNotificationRequest);

                toList.clear();
                toList.add(new InternetAddress(teacherBasicInfo.getEmail(), teacherBasicInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.TEACHER);
                request.setBcc(bccList);
                sendEmail(request);

                if (StringUtils.isNotEmpty(teacherBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.TEACHER);
                    smsManager.sendSMS(smsRequest);
                }

                // NotificationManager.INSTANCE.createNotification(subscription.getTeacherId(),
                // NotificationType.SUBSCRIPTION_ENDED_BY_TEACHER, subscription,
                // null, mgr);
                break;
        }

    }

    public void sendSubscriptionSessionRelatedEmail(SessionInfo session, Role role, SessionModel model,
            SubModel subModel, Boolean scheduled, Integer grade, String target, String note, Long subscriptionId)
            throws IOException, AddressException, NotFoundException, VException, IllegalAccessException {
        logger.info("ENTRY " + session + ", " + role + ", " + model + ", " + subModel + ", " + scheduled + ", " + grade
                + ", " + target + ", " + note + ", " + subscriptionId);
        // TODO in ASYNC this will send email twice to the first user in case
        // the email to the second user fails

        // The logic of sending emails in case of multiple students is not
        // defined, so assuming
        // one teacher and one student per session
        if (session == null && subscriptionId != null) {
            GetSessionsReq getSessionsReq = new GetSessionsReq(0, 1);
            getSessionsReq.setSubscriptionId(subscriptionId);
            List<SessionInfo> sessions = sessionManager.getSessions(getSessionsReq, true);
            if (ArrayUtils.isNotEmpty(sessions)) {
                session = sessions.get(0);
            }
        }
        if (session == null) {
            logger.error("session is null for sending sendSubscriptionSessionRelatedEmail");
            return;
        }
        CommunicationType emailType = getEmailType(role, model, scheduled, subModel);
        if (emailType == null) {
            logger.info("emailType null, not sending communication");
            return;
        }
        Long teacherId = session.getTeacherId();
        Long studentId = session.getStudentIds().get(0);
        Set<Long> userIds = new HashSet<>();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacherBasicInfo = userInfos.get(teacherId);
        UserBasicInfo studentBasicInfo = userInfos.get(studentId);
        // sending email to student
        _sendSubscriptionSessionRelatedEmail(studentBasicInfo, teacherBasicInfo, session, emailType, grade, target,
                note);
        // sending email to teacher
        _sendSubscriptionSessionRelatedEmail(teacherBasicInfo, studentBasicInfo, session, emailType, grade, target,
                note);

    }

    private void _sendSubscriptionSessionRelatedEmail(UserBasicInfo emailRecipientUser, UserBasicInfo partnerUser,
            SessionInfo session, CommunicationType emailType, Integer grade, String target, String note)
            throws IOException, AddressException, NotFoundException, VException {

        logger.info("User: " + emailRecipientUser + ", partneruser:  " + partnerUser + ", session: " + session
                + ", emailType: " + emailType + ", grade: " + grade + ", board: " + target);
        if (emailRecipientUser == null || partnerUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user or partneruser not found");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(session.getStartTime()));
        String date = dateTime.substring(0, 7);
        String month = dateTime.substring(7, 16);
        String time = dateTime.substring(17, 25);
        sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String endTime = sdf.format(new Date(session.getEndTime()));
        SimpleDateFormat smsDisplayDate = new SimpleDateFormat("EEE, dd MMM yyyy");
        smsDisplayDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String smsDisplayDateString = smsDisplayDate.format(new Date(session.getStartTime()));

        String teacherName = "";
        String studentName = "";
        String studentPhoneNo = "";
        if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
            teacherName = emailRecipientUser.getFirstName();
        } else if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
            studentName = emailRecipientUser.getFirstName();
            studentPhoneNo = emailRecipientUser.getContactNumber();
        }

        if (partnerUser.getRole().equals(Role.TEACHER)) {
            teacherName = partnerUser.getFirstName();
        } else if (partnerUser.getRole().equals(Role.STUDENT)) {
            studentName = partnerUser.getFirstName();
            studentPhoneNo = partnerUser.getContactNumber();
        }

        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        // setting subject scopes
        subjectScopes.put("board", target);
        subjectScopes.put("grade", grade + "" );
        subjectScopes.put("subscriptionName", session.getTitle());
        subjectScopes.put("studentName", studentName);
        subjectScopes.put("teacherName", teacherName);
        subjectScopes.put("subject", session.getSubject());
        subjectScopes.put("startTime", dateTime);
        subjectScopes.put("endTime", endTime);

        // setting body scopes
        bodyScopes.put("studentPhoneNo", studentPhoneNo);
        bodyScopes.put("teacherName", teacherName);
        bodyScopes.put("studentName", studentName);
        bodyScopes.put("board", target);
        bodyScopes.put("note", note);
        bodyScopes.put("grade", grade + "");
        bodyScopes.put("subscriptionName", session.getTitle());
        bodyScopes.put("title", session.getTitle());
        bodyScopes.put("subject", session.getSubject());
        if (session.getRemark() != null) {
            bodyScopes.put("reason", session.getRemark());
        }
        bodyScopes.put("date", date + " " + month);
        bodyScopes.put("month", month);
        bodyScopes.put("time", time);
        long duration = session.getEndTime() - session.getStartTime();
        bodyScopes.put("duration", (duration) / (1000 * 60));
        int hours = (int) duration / DateTimeUtils.MILLIS_PER_HOUR;
        int minutes = ((int) duration % DateTimeUtils.MILLIS_PER_HOUR) / DateTimeUtils.MILLIS_PER_MINUTE;
        String durationDisplayString = " ";
        if (hours > 0) {
            durationDisplayString += hours + "hr ";
        }
        durationDisplayString += minutes + "min";
        bodyScopes.put("durationDisplay", durationDisplayString);
        bodyScopes.put("smsDisplayDate", smsDisplayDateString);
        bodyScopes.put("subscriptionId", session.getSubscriptionId());
        bodyScopes.put("startDate", session.getStartTime());

        logger.info("subjectScopes: " + subjectScopes.toString());
        logger.info("bodyScopes: " + bodyScopes.toString());

        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(emailRecipientUser.getEmail(), emailRecipientUser.getFullName()));
        EmailRequest request;
        TextSMSRequest smsRequest;
        CreateNotificationRequest createNotificationRequest;
        switch (emailType) {
            case SUBSCRIPTION_SESSION_SCHEDULE:
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }

                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_SCHEDULE, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case SUBSCRIPTION_SESSION_SCHEDULE_BY_SC:
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                        NotificationType.SUBSCRIPTION_SESSION_SCHEDULE_BY_SC, new Gson().toJson(bodyScopes),
                        emailRecipientUser.getRole());
                notificationManager.createNotification(createNotificationRequest);
                break;
            case SUBSCRIPTION_SESSION_CANCEL_BY_STUDENT:
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);

                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_CANCEL_BY_STUDENT, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }

                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case SUBSCRIPTION_SESSION_CANCEL_BY_TEACHER:
                if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_CANCEL_BY_TEACHER, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case SUBSCRIPTION_SESSION_CANCEL_BY_SC:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                        NotificationType.SUBSCRIPTION_SESSION_CANCEL_BY_SC, new Gson().toJson(bodyScopes),
                        emailRecipientUser.getRole());
                notificationManager.createNotification(createNotificationRequest);
                break;
            case SUBSCRIPTION_SESSION_REQUEST_BY_TEACHER:
                if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                        smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(),
                                emailRecipientUser.getPhoneCode(), bodyScopes, emailType, emailRecipientUser.getRole());
                        smsManager.sendSMS(smsRequest);
                    }
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_REQUEST_BY_TEACHER, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case SUBSCRIPTION_SESSION_REQUEST_CANCEL_BY_TEACHER:
                if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                        smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(),
                                emailRecipientUser.getPhoneCode(), bodyScopes, emailType, emailRecipientUser.getRole());
                        smsManager.sendSMS(smsRequest);
                    }
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_REQUEST_CANCEL_BY_TEACHER, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case SUBSCRIPTION_SESSION_REQUEST_ACCEPTED:
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                        smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(),
                                emailRecipientUser.getPhoneCode(), bodyScopes, emailType, emailRecipientUser.getRole());
                        smsManager.sendSMS(smsRequest);
                    }
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_REQUEST_ACCEPTED, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case SUBSCRIPTION_SESSION_REQUEST_REJECTED:
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                        smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(),
                                emailRecipientUser.getPhoneCode(), bodyScopes, emailType, emailRecipientUser.getRole());
                        smsManager.sendSMS(smsRequest);
                    }
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.SUBSCRIPTION_SESSION_REQUEST_REJECTED, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case TRIAL_SESSION_SCHEDULE:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.TRIAL_SESSION_SCHEDULE, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case TRIAL_SESSION_SCHEDULE_BY_SC:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                        NotificationType.TRIAL_SESSION_SCHEDULE_BY_SC, new Gson().toJson(bodyScopes),
                        emailRecipientUser.getRole());
                notificationManager.createNotification(createNotificationRequest);
                break;
            case TRIAL_SESSION_CANCEL_BY_STUDENT:
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.TRIAL_SESSION_CANCEL_BY_STUDENT, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case TRIAL_SESSION_CANCEL_BY_TEACHER:
                if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.TRIAL_SESSION_CANCEL_BY_TEACHER, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case TRIAL_SESSION_CANCEL_BY_SC:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                        NotificationType.TRIAL_SESSION_CANCEL_BY_SC, new Gson().toJson(bodyScopes),
                        emailRecipientUser.getRole());
                notificationManager.createNotification(createNotificationRequest);
                break;
            case LOOSE_SESSION_SCHEDULE:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.LOOSE_SESSION_SCHEDULE, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case LOOSE_SESSION_SCHEDULE_BY_TEACHER:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.LOOSE_SESSION_SCHEDULE_BY_TEACHER, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case LOOSE_SESSION_SCHEDULE_BY_SC:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                        NotificationType.LOOSE_SESSION_SCHEDULE_BY_SC, new Gson().toJson(bodyScopes),
                        emailRecipientUser.getRole());
                notificationManager.createNotification(createNotificationRequest);
                break;
            case LOOSE_SESSION_CANCEL_BY_STUDENT:
                if (emailRecipientUser.getRole().equals(Role.TEACHER)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.LOOSE_SESSION_CANCEL_BY_STUDENT, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case LOOSE_SESSION_CANCEL_BY_TEACHER:
                if (emailRecipientUser.getRole().equals(Role.STUDENT)) {
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                    createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                            NotificationType.LOOSE_SESSION_CANCEL_BY_TEACHER, new Gson().toJson(bodyScopes),
                            emailRecipientUser.getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case LOOSE_SESSION_CANCEL_BY_SC:
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);
                if (StringUtils.isNotEmpty(emailRecipientUser.getContactNumber())) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(), emailRecipientUser.getPhoneCode(),
                            bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                createNotificationRequest = new CreateNotificationRequest(emailRecipientUser.getUserId(),
                        NotificationType.LOOSE_SESSION_CANCEL_BY_SC, new Gson().toJson(bodyScopes),
                        emailRecipientUser.getRole());
                notificationManager.createNotification(createNotificationRequest);
                break;
            default:
                break;
        }
        logger.info("exit sendSubscriptionSessionRelatedEmail");
    }

    private CommunicationType getEmailType(Role role, SessionModel model, Boolean scheduled, SubModel subModel) {
        CommunicationType emailType = null;
        logger.info(" Role " + role + ", model " + model + ", scheduled " + scheduled + " submodel " + subModel);
        switch (model) {
            case OTO:
                switch (subModel) {
                    case LOOSE:
                        switch (role) {
                            case TEACHER:
                                if (scheduled) {
                                    emailType = CommunicationType.LOOSE_SESSION_SCHEDULE_BY_TEACHER;
                                } else {
                                    emailType = CommunicationType.LOOSE_SESSION_CANCEL_BY_TEACHER;
                                }
                                break;
                            case STUDENT:
                                if (scheduled) {
                                    emailType = CommunicationType.LOOSE_SESSION_SCHEDULE;
                                } else {
                                    emailType = CommunicationType.LOOSE_SESSION_CANCEL_BY_STUDENT;
                                }
                                break;
                            default:
                                if (scheduled) {
                                    emailType = CommunicationType.LOOSE_SESSION_SCHEDULE_BY_SC;
                                } else {
                                    emailType = CommunicationType.LOOSE_SESSION_CANCEL_BY_SC;
                                }
                                break;
                        }
                        break;
                    default:
                        switch (role) {
                            case TEACHER:
                                if (scheduled) {
                                    emailType = CommunicationType.SUBSCRIPTION_SESSION_SCHEDULE_BY_TEACHER;
                                } else {
                                    emailType = CommunicationType.SUBSCRIPTION_SESSION_CANCEL_BY_TEACHER;
                                }
                                break;
                            case STUDENT:
                                if (scheduled) {
                                    emailType = CommunicationType.SUBSCRIPTION_SESSION_SCHEDULE;
                                } else {
                                    emailType = CommunicationType.SUBSCRIPTION_SESSION_CANCEL_BY_STUDENT;
                                }
                                break;
                            default:
                                if (scheduled) {
                                    emailType = CommunicationType.SUBSCRIPTION_SESSION_SCHEDULE_BY_SC;
                                } else {
                                    emailType = CommunicationType.SUBSCRIPTION_SESSION_CANCEL_BY_SC;
                                }
                                break;
                        }
                }
                break;
            case IL:
            case TRIAL:
                switch (role) {
                    case TEACHER:
                        if (scheduled) {
                        } else {
                            emailType = CommunicationType.TRIAL_SESSION_CANCEL_BY_TEACHER;
                        }
                        break;
                    case STUDENT:
                        if (scheduled) {
                            emailType = CommunicationType.TRIAL_SESSION_SCHEDULE;
                        } else {
                            emailType = CommunicationType.TRIAL_SESSION_CANCEL_BY_STUDENT;
                        }
                        break;
                    default:
                        if (scheduled) {
                            emailType = CommunicationType.TRIAL_SESSION_SCHEDULE_BY_SC;
                        } else {
                            emailType = CommunicationType.TRIAL_SESSION_CANCEL_BY_SC;
                        }
                        break;
                }
                break;
            default:
                break;
        }
        logger.info("Exit " + emailType);
        return emailType;
    }

    // subscription request emails
    public void sendSubscriptionRequestReminder(SubscriptionRequestNotificationInfo subscriptionRequestInfo)
            throws AddressException, NotFoundException, IOException, BadRequestException, VException {
        CommunicationType communicationType = subscriptionRequestInfo.getCommunicationType();
        if (communicationType == null) {
            throw new BadRequestException(ErrorCode.INVALID_COMMUNICATION_TYPE, "Not communication type given ");
        }

        Set<Long> userIds = new HashSet<>();
        Long teacherId = subscriptionRequestInfo.getTeacherId();
        Long studentId = subscriptionRequestInfo.getStudentId();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacherBasicInfo = userInfos.get(teacherId);
        UserBasicInfo studentBasicInfo = userInfos.get(studentId);
        Map<String, Object> bodyScopes = new HashMap<>();

        if (teacherBasicInfo == null || studentBasicInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "teacherBasicInfo or studentBasicInfo not found");
        }

        String teacherName = teacherBasicInfo.getFirstName();
        String studentName = studentBasicInfo.getFirstName();
        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }

        String tutorListLink = ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/tutors";
        String mySubscriptionsLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/subscription/"
                + subscriptionRequestInfo.getSubscriptionId();
        String extNo = ConfigUtils.INSTANCE.getStringValue("teacher.call.extension.1");
        if (teacherBasicInfo.getExtensionNumber() != null) {
            extNo += " Ext:" + teacherBasicInfo.getExtensionNumber().toString();
        }
        subscriptionRequestInfo.updateDetails(studentName, teacherName, extNo, studentBasicInfo.getContactNumber(),
                tutorListLink, mySubscriptionsLink);

        for (Field field : subscriptionRequestInfo.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(subscriptionRequestInfo) != null) {
                    bodyScopes.put(field.getName(), field.get(subscriptionRequestInfo));
                }
            } catch (IllegalArgumentException e) {
                logger.info(e.toString());
            } catch (IllegalAccessException e) {
                logger.info(e.toString());
            }
        }

        if (CommunicationType.SUBSCRIPTION_REQUEST_ACCEPTED.equals(communicationType)
                && PaymentType.INSTALMENT.equals(subscriptionRequestInfo.getPaymentType())) {

            SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy");
            sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            Integer hoursCost = 0;
            Integer security = 0;
            for (InstalmentInfo instalmentInfo : subscriptionRequestInfo.getInstalments()) {
                if (instalmentInfo.getDueTime() != null) {
                    instalmentInfo.setDueDate(sdf.format(new Date(instalmentInfo.getDueTime())));
                }
                if (instalmentInfo.getPaidTime() != null) {
                    instalmentInfo.setPaidDate(sdf.format(new Date(instalmentInfo.getPaidTime())));
                }
                if (instalmentInfo.getHours() != null) {
                    instalmentInfo.setHours(instalmentInfo.getHours() / DateTimeUtils.MILLIS_PER_HOUR);
                }
                if (instalmentInfo.getTotalAmount() != null) {
                    instalmentInfo.setTotalAmount(instalmentInfo.getTotalAmount() / 100);
                }
                hoursCost += instalmentInfo.getHoursCost();
                if (instalmentInfo.getSecurityCharge() != null) {
                    security += instalmentInfo.getSecurityCharge();
                }
                if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())) {
                    instalmentInfo.setPaymentStatusStr("Not Paid");
                } else if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                    instalmentInfo.setPaymentStatusStr("Paid");
                }
            }

            bodyScopes.put("instalments", subscriptionRequestInfo.getInstalments());
            bodyScopes.put("hoursCost", hoursCost / 100);
            bodyScopes.put("security", security / 100);
        }
        bodyScopes.put("subscriptionLink", subscriptionLink);

        List<InternetAddress> toList = new ArrayList<>();
        List<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress(TEACHER_CARE_EMAIL_ID));
        EmailRequest request;
        TextSMSRequest smsRequest;
        CreateNotificationRequest createNotificationRequest;
        switch (communicationType) {
            case SUBSCRIPTION_REQUEST_CREATED:
            case SUBSCRIPTION_REQUEST_CANCELLED:
            case SUBSCRIPTION_REQUEST_REJECTED:
            case SUBSCRIPTION_REQUEST_EXPIRED:
            case SUBSCRIPTION_REQUEST_ACCEPTED:
                bodyScopes.put("price", ((subscriptionRequestInfo.getTotalHoursLong() / DateTimeUtils.MILLIS_PER_HOUR)
                        * subscriptionRequestInfo.getHourlyRate()) / 100);

                toList.add(new InternetAddress(studentBasicInfo.getEmail(), studentBasicInfo.getFullName()));
                if (CommunicationType.SUBSCRIPTION_REQUEST_ACCEPTED.equals(communicationType)
                        && PaymentType.INSTALMENT.equals(subscriptionRequestInfo.getPaymentType())) {
                    request = new EmailRequest(toList, bodyScopes, bodyScopes,
                            CommunicationType.SUBSCRIPTION_REQUEST_ACCEPTED_INSTALMENTS, Role.STUDENT);
                } else {
                    request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, Role.STUDENT);
                }
                sendEmail(request);

                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(studentBasicInfo.getContactNumber(), studentBasicInfo.getPhoneCode(),
                            bodyScopes, communicationType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(subscriptionRequestInfo.getStudentId(),
                        NotificationType.valueOf(communicationType.name()), new Gson().toJson(bodyScopes), Role.STUDENT);

                notificationManager.createNotification(createNotificationRequest);

                toList.clear();
                toList.add(new InternetAddress(teacherBasicInfo.getEmail(), teacherBasicInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, Role.TEACHER);
                request.setBcc(bccList);
                sendEmail(request);

                if (StringUtils.isNotEmpty(teacherBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                            bodyScopes, communicationType, Role.TEACHER);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(subscriptionRequestInfo.getTeacherId(),
                        NotificationType.valueOf(communicationType.name()), new Gson().toJson(bodyScopes), Role.TEACHER);

                notificationManager.createNotification(createNotificationRequest);

                break;

            case SUBSCRIPTION_REQUEST_EXPIRY_REMINDER:
                toList.add(new InternetAddress(teacherBasicInfo.getEmail(), teacherBasicInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, Role.TEACHER);
                request.setBcc(bccList);
                sendEmail(request);

                if (StringUtils.isNotEmpty(teacherBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                            bodyScopes, communicationType, Role.TEACHER);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(subscriptionRequestInfo.getTeacherId(),
                        NotificationType.valueOf(communicationType.name()), new Gson().toJson(bodyScopes), Role.TEACHER);

                notificationManager.createNotification(createNotificationRequest);
                break;
            default:
                break;
        }
    }

    // session related communication
    public void sendSessionRelatedEmail(UserBasicInfo emailRecipientUser, SessionInfo session,
            CommunicationType emailType) throws UnsupportedEncodingException, VException, AddressException, IOException,
            IllegalArgumentException, IllegalAccessException {
        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(session.getStartTime()));
        String date = dateTime.substring(0, 7);
        String month = dateTime.substring(7, 16);
        String time = dateTime.substring(17, 25);

        SimpleDateFormat smsDisplayDate = new SimpleDateFormat("EEE, dd MMM yyyy");
        smsDisplayDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String smsDisplayDateString = smsDisplayDate.format(new Date(session.getStartTime()));
        bodyScopes.put("smsDisplayDate", smsDisplayDateString);

        bodyScopes.put("name", emailRecipientUser.getFirstName());
        bodyScopes.put(PARTNER_LINK, "NA");
        bodyScopes.put(PARTNER_NAME, "NA");
        sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String endTime = sdf.format(new Date(session.getEndTime()));

        UserBasicInfo partnerUser = session.getAttendees().get(0);
        if (partnerUser.getRole().equals(emailRecipientUser.getRole())) {
            partnerUser = session.getAttendees().get(1);
        }

        partnerUser = userUtils.getUserBasicInfo(partnerUser.getUserId().toString(), true);

        bodyScopes.put(PARTNER_LINK, userManager.getUserProfileLink(partnerUser));
        String partnerName = partnerUser.getFirstName();
        if (CommunicationType.SESSION_SCHEDULE.equals(emailType)
                || CommunicationType.SESSION_RESCHEDULE.equals(emailType)) {
            String activePhoneForCallNumber = "";
            if (Role.TEACHER.equals(partnerUser.getRole())) {
                activePhoneForCallNumber = userManager.getFullTeacherExtensionNumber(partnerUser);
            } else {
                activePhoneForCallNumber = partnerUser.getContactNumber();
            }
            partnerName += " (" + activePhoneForCallNumber + ")";
        }
        bodyScopes.put(PARTNER_NAME, partnerName);
        bodyScopes.put("title", session.getTitle());
        bodyScopes.put("subject", session.getSubject());

        if (session.getRemark() != null) {
            bodyScopes.put("remark", session.getRemark());
        }

        String context = StringUtils.defaultIfEmpty(session.getTopic());

        bodyScopes.put("context", context);
        bodyScopes.put("date", date);
        bodyScopes.put("month", month);
        bodyScopes.put("time", time);
        long duration = session.getEndTime() - session.getStartTime();
        bodyScopes.put("duration", (duration) / (1000 * 60));
        int hours = (int) duration / DateTimeUtils.MILLIS_PER_HOUR;
        int minutes = ((int) duration % DateTimeUtils.MILLIS_PER_HOUR) / DateTimeUtils.MILLIS_PER_MINUTE;
        String durationDisplayString = " ";
        if (hours > 0) {
            durationDisplayString += hours + "hr ";
        }
        durationDisplayString += minutes + "min";

        bodyScopes.put("durationDisplay", durationDisplayString);
        bodyScopes.put("startTime", dateTime);
        bodyScopes.put("endTime", endTime);
        subjectScopes.put("startTime", dateTime);
        subjectScopes.put("endTime", endTime);
        subjectScopes.put("title", session.getTitle());

        logger.info("bodyScopes " + bodyScopes);
        logger.info("subjectScopes " + subjectScopes);

        CheckSessionTOSReq req = new CheckSessionTOSReq();
        req.setSessionId(session.getId());
        CheckSessionTOSRes res = sessionManager.isTOS(req);
        if (res.isIsTOS()) {
            if (Role.STUDENT.equals(partnerUser.getRole())) {
                subjectScopes.put("teacherFirstSession", "Student FIRST Session ");
            } else if (Role.TEACHER.equals(partnerUser.getRole())) {
                subjectScopes.put("studentFirstSession", "Your FIRST session ");
            }
        }

        List<InternetAddress> toList = new ArrayList<>();
        EmailRequest request;
        TextSMSRequest smsRequest;
        switch (emailType) {
            case SESSION_RESCHEDULE:
            case SESSION_POST:
                toList.add(new InternetAddress(emailRecipientUser.getEmail(), emailRecipientUser.getFullName()));
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                sendEmail(request);

                if (emailRecipientUser.getContactNumber() != null) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(),
                            emailRecipientUser.getPhoneCode(), bodyScopes, emailType, emailRecipientUser.getRole());
                    smsManager.sendSMS(smsRequest);
                }
                break;
            case SESSION_REMINDER:
                if (emailRecipientUser.getContactNumber() != null) {
                    smsRequest = new TextSMSRequest(emailRecipientUser.getContactNumber(),
                            emailRecipientUser.getPhoneCode(), bodyScopes, emailType, emailRecipientUser.getRole());
                    smsRequest.setExpectedDeliveryMillis(session.getEndTime());
                    smsManager.sendSMS(smsRequest);
                }
                if (Role.STUDENT.equals(emailRecipientUser.getRole()) && StringUtils.isNotEmpty(emailRecipientUser.getPhoneCode()) && !emailRecipientUser.getPhoneCode().equals("91")) {
                    toList.add(new InternetAddress(emailRecipientUser.getEmail(), emailRecipientUser.getFullName()));
                    request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, emailRecipientUser.getRole());
                    sendEmail(request);
                }
                break;
            default:
                logger.warn("Unknown communication type " + emailType);
        }

        if (Role.STUDENT.equals(emailRecipientUser.getRole())
                && !CommunicationType.SESSION_RESCHEDULE.equals(emailType) && !CommunicationType.SESSION_REMINDER.equals(emailType)) {
            // TODO: Change this to UserInfo
            User user = userManager.getUserById(emailRecipientUser.getUserId());
            sendCommunicationToParent(user, subjectScopes, bodyScopes, emailType);
        }

    }

    public void sendSubscriptionSessionRequestRelatedEmail(SubscriptionSessionRequestInfo sessionRequestInfo,
            CommunicationType emailType) throws IOException, AddressException, NotFoundException, VException {

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(sessionRequestInfo.getStartTime()));
        String date = dateTime.substring(0, 7);
        String month = dateTime.substring(7, 16);
        String time = dateTime.substring(17, 25);
        SimpleDateFormat smsDisplayDate = new SimpleDateFormat("EEE, dd MMM yyyy");
        smsDisplayDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String smsDisplayDateString = smsDisplayDate.format(new Date(sessionRequestInfo.getStartTime()));
        bodyScopes.put("smsDisplayDate", smsDisplayDateString);
        bodyScopes.put("mySubscriptionLink", ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysessions");

        List<Long> userIds = new ArrayList<>();
        Long teacherId = sessionRequestInfo.getTeacherId();
        Long studentId = sessionRequestInfo.getStudentId();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacherBasicInfo = userInfos.get(teacherId);
        UserBasicInfo studentBasicInfo = userInfos.get(studentId);

        String teacherName = teacherBasicInfo.getFirstName();
        String studentName = studentBasicInfo.getFirstName();

        sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String endTime = sdf.format(new Date(sessionRequestInfo.getEndTime()));

        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }
        String subscriptionName = sessionRequestInfo.getSubscriptionName();

        if (!emailType.equals(CommunicationType.SUBSCRIPTION_SESSION_REQUEST_BY_TEACHER)) {
            SubscriptionResponse subscriptionResponse = subscriptionManager
                    .getSubscriptionBasicInfo(sessionRequestInfo.getSubscriptionId());
            if (subscriptionResponse != null) {
                sessionRequestInfo.setSubscriptionName(subscriptionResponse.getTitle());
                subscriptionName = subscriptionResponse.getTitle();
            }
        }

        logger.info("SubscriptionName after: " + subscriptionName);
        bodyScopes.put("teacherName", teacherName);
        subjectScopes.put("teacherName", teacherName);
        bodyScopes.put("studentName", studentName);
        subjectScopes.put("studentName", studentName);
        subjectScopes.put("subscriptionName", subscriptionName);
        bodyScopes.put("subscriptionName", subscriptionName);
        if (sessionRequestInfo.getNote() != null) {
            bodyScopes.put("reason", sessionRequestInfo.getNote());
        }
        bodyScopes.put("date", date + " " + month);
        bodyScopes.put("month", month);
        bodyScopes.put("time", time);
        long duration = sessionRequestInfo.getEndTime() - sessionRequestInfo.getStartTime();
        bodyScopes.put("duration", (duration) / (1000 * 60));
        int hours = (int) duration / DateTimeUtils.MILLIS_PER_HOUR;
        int minutes = ((int) duration % DateTimeUtils.MILLIS_PER_HOUR) / DateTimeUtils.MILLIS_PER_MINUTE;
        String durationDisplayString = " ";
        if (hours > 0) {
            durationDisplayString += hours + "hr ";
        }
        durationDisplayString += minutes + "min";
        bodyScopes.put("durationDisplay", durationDisplayString);
        subjectScopes.put("startTime", dateTime);
        subjectScopes.put("endTime", endTime);

        String studentPhoneNo = studentBasicInfo.getContactNumber();

        bodyScopes.put("subscriptionId", sessionRequestInfo.getSubscriptionId());
        bodyScopes.put("studentPhoneNo", studentPhoneNo);

        List<InternetAddress> toList = new ArrayList<>();
        EmailRequest request;
        TextSMSRequest smsRequest;
        CreateNotificationRequest createNotificationRequest;
        switch (emailType) {
            case SUBSCRIPTION_SESSION_REQUEST_BY_TEACHER:

                toList.add(new InternetAddress(studentBasicInfo.getEmail(), studentBasicInfo.getFullName()));
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, studentBasicInfo.getRole());
                sendEmail(request);

                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(studentBasicInfo.getContactNumber(), studentBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(studentBasicInfo.getUserId(),
                        NotificationType.valueOf(emailType.name()), new Gson().toJson(bodyScopes), Role.STUDENT);
                notificationManager.createNotification(createNotificationRequest);
                break;
            case SUBSCRIPTION_SESSION_REQUEST_CANCEL_BY_TEACHER:
                toList.add(new InternetAddress(studentBasicInfo.getEmail(), studentBasicInfo.getFullName()));
                request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, studentBasicInfo.getRole());
                sendEmail(request);

                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(studentBasicInfo.getContactNumber(), studentBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(studentBasicInfo.getUserId(),
                        NotificationType.valueOf(emailType.name()), new Gson().toJson(bodyScopes), Role.STUDENT);
                notificationManager.createNotification(createNotificationRequest);
                break;
            case SUBSCRIPTION_SESSION_REQUEST_ACCEPTED:
                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.TEACHER);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(teacherBasicInfo.getUserId(),
                        NotificationType.valueOf(emailType.name()), new Gson().toJson(bodyScopes), Role.TEACHER);
                notificationManager.createNotification(createNotificationRequest);
                break;
            case SUBSCRIPTION_SESSION_REQUEST_REJECTED:
                if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
                    smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                            bodyScopes, emailType, Role.TEACHER);
                    smsManager.sendSMS(smsRequest);
                }

                createNotificationRequest = new CreateNotificationRequest(teacherBasicInfo.getUserId(),
                        NotificationType.valueOf(emailType.name()), new Gson().toJson(bodyScopes), Role.TEACHER);
                notificationManager.createNotification(createNotificationRequest);
                break;
            default:
                break;
        }
    }

    // instalearn communications
    public void sendInstalLearnCommunication(UserBasicInfo emailRecipientUser,
            SubmitInstaRequestRes submitInstaRequestRes, SessionSlot sessionSlot, String subject, Integer grade,
            CommunicationType communicationType) throws NotFoundException, UnsupportedEncodingException, VException {

        UserBasicInfo studentBasicInfo = submitInstaRequestRes.getToUser();
        UserBasicInfo teacherBasicInfo = submitInstaRequestRes.getFromUser();
        if (!studentBasicInfo.getRole().equals(Role.STUDENT)) {
            studentBasicInfo = submitInstaRequestRes.getFromUser();
            teacherBasicInfo = submitInstaRequestRes.getToUser();
        }

        if (teacherBasicInfo == null || studentBasicInfo == null || emailRecipientUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "teacherBasicInfo or studentBasicInfo not found");
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        if (sessionSlot != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
            sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            String dateTime = sdf.format(new Date(sessionSlot.getStartTime()));
            String date = dateTime.substring(0, 7);
            String month = dateTime.substring(7, 16);
            String time = dateTime.substring(17, 25);

            SimpleDateFormat smsDisplayDate = new SimpleDateFormat("EEE, dd MMM yyyy");
            smsDisplayDate.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            String smsDisplayDateString = smsDisplayDate.format(new Date(sessionSlot.getStartTime()));

            sdf = new SimpleDateFormat("hh:mm a");
            sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            String endTime = sdf.format(new Date(sessionSlot.getEndTime()));
            bodyScopes.put("smsDisplayDate", smsDisplayDateString);
            bodyScopes.put("title", sessionSlot.getTitle());
            bodyScopes.put("date", date);
            bodyScopes.put("month", month);
            bodyScopes.put("time", time);
            String context = StringUtils.defaultIfEmpty(sessionSlot.getTopic());
            bodyScopes.put("context", context);

            long duration = sessionSlot.getEndTime() - sessionSlot.getStartTime();
            bodyScopes.put("duration", (duration) / (1000 * 60));

            int hours = (int) duration / DateTimeUtils.MILLIS_PER_HOUR;
            int minutes = ((int) duration % DateTimeUtils.MILLIS_PER_HOUR) / DateTimeUtils.MILLIS_PER_MINUTE;
            String durationDisplayString = " ";
            if (hours > 0) {
                durationDisplayString += hours + "hr ";
            }
            durationDisplayString += minutes + "min";
            bodyScopes.put("durationDisplay", durationDisplayString);
            bodyScopes.put("startTime", dateTime);
            bodyScopes.put("endTime", endTime);
            subjectScopes.put("startTime", dateTime);
            subjectScopes.put("endTime", endTime);
            subjectScopes.put("title", sessionSlot.getTitle());
        }

        bodyScopes.put("name", emailRecipientUser.getFirstName());
        bodyScopes.put(PARTNER_LINK, "NA");
        bodyScopes.put(PARTNER_NAME, "NA");

        UserBasicInfo partnerUser;
        if (Role.STUDENT.equals(emailRecipientUser.getRole())) {
            partnerUser = teacherBasicInfo;
        } else {
            partnerUser = studentBasicInfo;
        }

        bodyScopes.put(PARTNER_LINK, userManager.getUserProfileLink(partnerUser));
        String partnerName = partnerUser.getFirstName();
        String activePhoneForCallNumber = "";
        if (Role.TEACHER.equals(partnerUser.getRole())) {
            activePhoneForCallNumber = userManager.getFullTeacherExtensionNumber(partnerUser);
        } else // TODO fetch active phone number and do
        /*
		 * List<PhoneNumberRes> phones = UserManager.getUserPhoneNumberRes(partnerUser);
		 * 
		 * for (PhoneNumberRes pNumber : phones) { if (pNumber.getIsActiveForCall()) {
		 * activePhoneForCallNumber = pNumber.getNumber(); break; } }
         */ {
            if (StringUtils.isEmpty(activePhoneForCallNumber)) {
                activePhoneForCallNumber = partnerUser.getContactNumber();
            }
        }
        partnerName += " (" + activePhoneForCallNumber + ")";

        bodyScopes.put(PARTNER_NAME, partnerName);
        bodyScopes.put("subject", subject);

        List<InternetAddress> toList = new ArrayList<>();
        EmailRequest request;
        TextSMSRequest smsRequest;
        CreateNotificationRequest createNotificationRequest;
        switch (communicationType) {
            case INSTALEARN_SESSION_SCHEDULE:
                // there was no email before migration, so removing it. When product
                // needs it will enable it
                // toList.add(new InternetAddress(emailRecipientUser.getEmail(),
                // emailRecipientUser.getFullName()));
                // request = new EmailRequest(toList, subjectScopes, bodyScopes,
                // communicationType, emailRecipientUser.getRole());
                // sendEmail(request);
                //
                // smsRequest = new
                // TextSMSRequest(emailRecipientUser.getContactNumber(),
                // emailRecipientUser.getPhoneCode(), bodyScopes, communicationType,
                // emailRecipientUser.getRole());
                // smsManager.sendSMS(smsRequest);
                break;
            case INSTANT_SESSION_REQUEST:
                if (InstaState.ACCEPTED.equals(submitInstaRequestRes.getData().getState())) {
                    // sending this only to from user
                    createNotificationRequest = new CreateNotificationRequest(
                            submitInstaRequestRes.getFromUser().getUserId(),
                            NotificationType.valueOf(communicationType.name()), new Gson().toJson(submitInstaRequestRes),
                            submitInstaRequestRes.getFromUser().getRole());
                    notificationManager.createNotification(createNotificationRequest);
                } else if (InstaState.REJECTED.equals(submitInstaRequestRes.getData().getState())
                        || InstaState.INPROGRESS.equals(submitInstaRequestRes.getData().getState())) {
                    // sending this only to to user
                    createNotificationRequest = new CreateNotificationRequest(submitInstaRequestRes.getToUser().getUserId(),
                            NotificationType.valueOf(communicationType.name()), new Gson().toJson(submitInstaRequestRes),
                            submitInstaRequestRes.getToUser().getRole());
                    notificationManager.createNotification(createNotificationRequest);
                }
                break;
            case INSTALEARN_REQUEST_TO_TEACHER:
                TeacherSubscriptionResponse teacherSubscriptionResponse = instalearnManager
                        .getTeacherSubscription(teacherBasicInfo.getUserId(), false);
                if (teacherSubscriptionResponse.isSubscribed()) {
                    String studentName = studentBasicInfo.getFullName();
                    String studentNameTrimmed = studentName.substring(0, Math.min(10, studentName.length()));
                    String subjectNameTrimmed = subject.substring(0, Math.min(25, subject.length()));

                    bodyScopes.put("subject", subjectNameTrimmed);
                    bodyScopes.put("studentName", studentNameTrimmed);
                    bodyScopes.put("class", grade);
                    if (StringUtils.isNotEmpty(teacherBasicInfo.getContactNumber())) {
                        smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                                bodyScopes, communicationType, teacherBasicInfo.getRole());
                        smsManager.sendSMS(smsRequest);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void sendReferralEmail(UserInfo userInfo, UserInfo referrerUserInfo, ReferralStep referralStep,
            ReferralStepBonus referralStepBonus) throws UnsupportedEncodingException, VException {
        logger.info("sendReferralEmail: " + referralStep + userInfo);
        String signupBonus = ConfigUtils.INSTANCE.getStringValue("freebies.rupees.join.bonus");

        HashMap<String, Object> bodyScopes = new HashMap<>();
        String refereeName = userInfo._getFullName();
        String referrerName = referrerUserInfo._getFullName();

        if (refereeName != null && refereeName.length() > 1) {
            refereeName = refereeName.substring(0, 1).toUpperCase() + refereeName.substring(1);
        }
        if (referrerName != null && referrerName.length() > 1) {
            referrerName = referrerName.substring(0, 1).toUpperCase() + referrerName.substring(1);
        }

        String refereeFirstName = userInfo.getFirstName();
        String referrerFirstName = referrerUserInfo.getFirstName();

        if (refereeFirstName != null && refereeFirstName.length() > 1) {
            refereeFirstName = refereeFirstName.substring(0, 1).toUpperCase() + refereeFirstName.substring(1);
        }
        if (referrerFirstName != null && referrerFirstName.length() > 1) {
            referrerFirstName = referrerFirstName.substring(0, 1).toUpperCase() + referrerFirstName.substring(1);
        }

        String referralLink = ConfigUtils.INSTANCE.getStringValue("url.base")
                + ConfigUtils.INSTANCE.getStringValue("user.referral.generic.link")
                + referrerUserInfo.getReferralCode();

        bodyScopes.put("refereeName", refereeName);
        bodyScopes.put("referrerName", referrerName);
        bodyScopes.put("refereeFirstName", refereeFirstName);
        bodyScopes.put("referrerFirstName", referrerFirstName);
        bodyScopes.put("signupBonus", signupBonus);
        bodyScopes.put("refereeBonusAmount", referralStepBonus.getReferreeBonusAmount());
        bodyScopes.put("referrerBonusAmount", referralStepBonus.getReferrerBonusAmount());
        bodyScopes.put("refereeLink", referralLink);

        if (referralStepBonus.getReferrerBonusAmount() != null && referralStepBonus.getReferrerBonusAmount() <= 0) {
            return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(referrerUserInfo.getEmail(), referrerUserInfo._getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.REFERRAL_TOREFERRER,
                referrerUserInfo.getRole());
        sendEmail(request);

    }

    public void sendReviewEmail(User toUser, User fromUser, Review review)
            throws AddressException, IOException, VException {
        logger.info("sendReviewEmail" + review.toString());
        logger.info("sending sendReviewEmail  from : " + fromUser);
        logger.info("sending sendReviewEmail  to : " + toUser.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", toUser.getFirstName());
        bodyScopes.put("fromName", fromUser._getFullName());
        bodyScopes.put("rating", review.getRating());
        if (StringUtils.isNotEmpty(review.getReview())) {
            bodyScopes.put("review", review.getReview());
        }
        if (review.getRating() <= 3) {
            bodyScopes.put("isLowRating", true);
        } else {
            bodyScopes.put("isHighRating", true);
        }

        String reason = review.getReasonString();
        if (StringUtils.isNotEmpty(reason)) {
            bodyScopes.put("isReasonSet", true);
            bodyScopes.put("reason", reason);
        }
        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress(toUser.getEmail(), toUser._getFullName()));
        EmailRequest emailRequest = new EmailRequest(toAddress, bodyScopes, bodyScopes, CommunicationType.ADD_REVIEW,
                toUser.getRole());
        sendEmail(emailRequest);
        sendCommunicationToParent(fromUser, bodyScopes, bodyScopes, CommunicationType.ADD_REVIEW);
//		TextSMSRequest textSMSRequest = new TextSMSRequest(toUser.getContactNumber(), toUser.getPhoneCode(), bodyScopes,
//				CommunicationType.ADD_REVIEW, toUser.getRole());
//		smsManager.sendSMS(textSMSRequest);
    }

    private void sendCommunicationToParent(User user, Map<String, Object> subjectScopes, Map<String, Object> bodyScopes,
            CommunicationType communicationType) throws AddressException, VException {
        sendCommunicationToParent(user, subjectScopes, bodyScopes, communicationType, null, null);
    }

    private void sendCommunicationToParent(User user, Map<String, Object> subjectScopes, Map<String, Object> bodyScopes,
            CommunicationType communicationType, List<InternetAddress> ccList, List<InternetAddress> replyToList)
            throws AddressException, VException {

        Set<String> parentEmails = user.getStudentInfo().getParentEmails(user.getEmail());
        if (CollectionUtils.isNotEmpty(parentEmails)) {
            ArrayList<InternetAddress> parentToAddress = new ArrayList<>();
            for (String parentEmail : parentEmails) {
                if (StringUtils.isEmpty(parentEmail)) {
                    continue;
                }
                parentToAddress.add(new InternetAddress(parentEmail));
            }

            if (!parentToAddress.isEmpty()) {
                EmailRequest emailRequest = new EmailRequest(parentToAddress, subjectScopes, bodyScopes,
                        communicationType, Role.PARENT);
                if (ArrayUtils.isNotEmpty(ccList)) {
                    emailRequest.setCc(ccList);
                }

                if (ArrayUtils.isNotEmpty(replyToList)) {
                    emailRequest.setReplyTo(replyToList);
                }
                sendEmail(emailRequest);
            }
        }

        Set<String> toNumbers = user.getStudentInfo().getParentContactNumbers(user.getContactNumber());
        if (CollectionUtils.isNotEmpty(toNumbers)) {
            List<String> finalNumbers = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(toNumbers)) {
                for (String no : toNumbers) {
                    if (StringUtils.isNotEmpty(no)) {
                        finalNumbers.add(no);
                    }
                }
            }
            if (!finalNumbers.isEmpty()) {
                BulkTextSMSRequest bulkSmsRequest = new BulkTextSMSRequest((ArrayList<String>) toNumbers, bodyScopes,
                        communicationType, Role.PARENT);
                smsManager.sendBulkSMS(bulkSmsRequest);
            }
            // SMSManager.INSTANCE.sendBulkSMS(bodyScopes, communicationType,
            // toNumbers, Role.PARENT);
        }
    }

    // TODO: Highly unoptimized
    public void sendAddToOfferingEmail(List<String> userIds, Offering offering) throws VException {
        logger.info("sendAddToOfferingEmail" + userIds);
        if (CollectionUtils.isEmpty(userIds)) {
            return;
        }

        for (String userId : userIds) {
            User user = userManager.getUserById(Long.parseLong(userId));
            if (Role.TEACHER.equals(user.getRole())) {
                try {
                    sendOfferingRelatedEmail(user, offering, CommunicationType.OFFERING_ADD_TEACHER, null, null);
                } catch (AddressException | NotFoundException | IOException e) {
                    logger.error("sendAddToOfferingEmail" + e.getMessage());
                }
            }
        }
        logger.info("sendAddToOfferingEmail");
    }

    public void sendOfferingRelatedEmail(User user, Offering offering, CommunicationType emailType,
            Transaction transaction, Long referenceTime, Object... extraParams)
            throws IOException, AddressException, VException {
        HashMap<String, Object> bodyScopes = new HashMap<>();

        bodyScopes.put("packageName", offering.getTitle());
        bodyScopes.put("name", user._getFullName());
        if (offering.getType().getDisplayString() != null) {
            bodyScopes.put("offeringType", offering.getType().getDisplayString() + " Course");
        }

        if (CollectionUtils.isNotEmpty(offering.getGrades())) {
            bodyScopes.put("grades", StringUtils.join(offering.getGrades().toArray(), ","));
        }

        if (CollectionUtils.isNotEmpty(offering.getCategories())) {
            bodyScopes.put("targets", StringUtils.join(offering.getCategories().toArray(), ","));
        }

        Map<Long, Board> boardsMap = boardDAO.getBoardsMapInBoardPojo(offering.getBoardIdsInLong());
        List<String> subjectNames = new ArrayList<>();
        for (Entry<Long, Board> entry : boardsMap.entrySet()) {
            if (entry.getValue().getParentId() == null || entry.getValue().getParentId() == 0) {
                subjectNames.add(entry.getValue().getName());
            }
        }

        bodyScopes.put("subjects", StringUtils.join(subjectNames.toArray(), ","));

        String baseLink = ConfigUtils.INSTANCE.getStringValue("url.base");
        String packagePath = String.format(ConfigUtils.INSTANCE.getStringValue("url.package"), offering.getId());
        if (extraParams != null && extraParams.length > 0) {
            @SuppressWarnings("unchecked")
            HashMap<String, Object> extraBodyParams = (HashMap<String, Object>) extraParams[0];
            if (extraBodyParams != null) {
                bodyScopes.putAll(extraBodyParams);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        bodyScopes.put("packageLink", baseLink + packagePath);
        bodyScopes.put("name", user._getFullName());
        if (transaction != null) {
            bodyScopes.put("orderNumber", transaction.getId());
            bodyScopes.put("amountPaid", MoneyUtils.toRupees(transaction.getAmount()));
            String orderDate = sdf.format(new Date(transaction.getCreationTime()));
            bodyScopes.put("orderDate", orderDate);
        }

        if (referenceTime != null) {
            String dateTime = sdf.format(new Date(referenceTime));
            bodyScopes.put("sessionTimeText", "Requested Time: " + dateTime);
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest req = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, user.getRole());

        sendEmail(req);

        // //SMS
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(), user.getPhoneCode(), bodyScopes,
                    emailType, user.getRole());
            smsManager.sendSMS(textSMSRequest);
        }
        sendCommunicationToParent(user, bodyScopes, bodyScopes, emailType);
        logger.info("sendOfferingRelatedEmail");
    }

    public void sendSessionFeedbackEmail(Feedback feedback) throws AddressException, IOException, VException {

        User teacher = userManager.getUserById(Long.parseLong(feedback.getSenderId()));
        User student = userManager.getUserById(Long.parseLong(feedback.getReceiverId()));
        if (student == null || teacher == null || !Role.TEACHER.equals(teacher.getRole())) {
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", student.getFirstName());
        bodyScopes.put("studentProfileLink", userManager.getUserProfileLink(student));
        bodyScopes.put("teacherName", teacher.getFirstName());
        bodyScopes.put("teacherFullName", teacher._getFullName());
        bodyScopes.put("feedback", feedback.getSessionMessage());
        bodyScopes.put("feedbackPartnerMessage", feedback.getPartnerMessage());
        List<String> reason = feedback.getReason();
        if (reason != null && !reason.isEmpty()) {
            bodyScopes.put("feedbackReason", ListUtils.join(reason, ","));
        } else {
            bodyScopes.put("feedbackReason", "NA");
        }
        bodyScopes.put("feedbackOptionalMessage",
                StringUtils.isNotEmpty(feedback.getOptionalMessage()) ? feedback.getOptionalMessage() : "NA");
        bodyScopes.put("feedbackSessionCoverage",
                StringUtils.isNotEmpty(feedback.getSessionCoverage()) ? feedback.getSessionCoverage() : "NA");
        bodyScopes.put("feedbackNextSessionPlan",
                StringUtils.isNotEmpty(feedback.getNextSessionPlan()) ? feedback.getNextSessionPlan() : "NA");
        bodyScopes.put("feedbackHomeWork",
                StringUtils.isNotEmpty(feedback.getHomeWork()) ? feedback.getHomeWork() : "NA");
        List<String> studentPerformance = feedback.getStudentPerformance();
        if (studentPerformance != null && !studentPerformance.isEmpty()) {
            bodyScopes.put("feedbackStudentPerformance", ListUtils.join(studentPerformance, ","));
        } else {
            bodyScopes.put("feedbackStudentPerformance", "NA");
        }
        Address[] toAddress = new Address[1];
        toAddress[0] = new InternetAddress(student.getEmail(), student._getFullName());

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student._getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.SESSION_FEEDBACK_ADD,
                student.getRole());

        sendEmail(request);

        // parent communication
        sendCommunicationToParent(student, bodyScopes, bodyScopes, CommunicationType.SESSION_FEEDBACK_ADD);
    }

    public void sendInstalmentDueEmail(SendInstalmentEmailReq req) throws IOException, AddressException, VException {

        if (req.getCommunicationType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid instalment communication type");
        }
        CommunicationType communicationType = CommunicationType.valueOf(req.getCommunicationType().toString());

        HashMap<String, Object> scopeParams = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dueDate = sdf.format(new Date(req.getDueTime()));
        scopeParams.put("dueDate", dueDate);

        Integer amount = req.getAmount();
        if (amount != null) {
            amount = amount / 100;
        } else {
            amount = 0;
        }
        scopeParams.put("amount", amount);

        String subscriptionLink;
        List<InternetAddress> toList = new ArrayList<>();
        EmailRequest request;
        TextSMSRequest smsRequest;

        switch (communicationType) {
            case INSTALMENT_PAYMENT_DUE_7DAYS_LEFT:
            case INSTALMENT_PAYMENT_DUE_1DAY_LEFT:
            case INSTALMENT_PAYMENT_DUE_1DAY_PASSED:
            case INSTALMENT_PAYMENT_DUE_SUBSCRIPTION_ENDED:

                User student = userManager.getUserById(req.getStudentId());
                User teacher = userManager.getUserById(req.getTeacherId());
                if (student == null || teacher == null) {
                    throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + req);
                }

                subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/subscription/"
                        + req.getSubscriptionId();
                if (com.vedantu.session.pojo.EntityType.COURSE_PLAN.equals(req.getEntityType())) {
                    subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/courseplan/"
                            + req.getSubscriptionId();
                }
                scopeParams.put("subscriptionLink", subscriptionLink);
                scopeParams.put("subscriptionTitle", req.getSubscriptionTitle());

                String subscriptionEndDate = sdf.format(new Date((req.getDueTime() + 7 * DateTimeUtils.MILLIS_PER_DAY)));
                scopeParams.put("subscriptionEndDate", subscriptionEndDate);
                scopeParams.put("teacherName", teacher.getFullName());
                scopeParams.put("studentName", student.getFullName());

                Integer refundAmount = req.getRefundAmount();
                if (refundAmount != null) {
                    refundAmount = refundAmount / 100;
                } else {
                    refundAmount = 0;
                }
                scopeParams.put("refundAmount", refundAmount);

                toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
                request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
                sendEmail(request);

                if (StringUtils.isNotEmpty(student.getContactNumber())) {
                    smsRequest = new TextSMSRequest(student.getContactNumber(), student.getPhoneCode(), scopeParams,
                            communicationType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                InstalmentNotificationReq instalmentDueNotificationReq = new InstalmentNotificationReq(communicationType,
                        req.getSubscriptionId(), subscriptionLink, req.getSubscriptionTitle(), dueDate,
                        teacher.getFullName(), amount.toString(), refundAmount.toString(), student.getFullName());

                CreateNotificationRequest createNotificationRequest = new CreateNotificationRequest(req.getStudentId(),
                        NotificationType.valueOf(communicationType.name()), new Gson().toJson(instalmentDueNotificationReq),
                        Role.STUDENT);
                notificationManager.createNotification(createNotificationRequest);
                break;
            case INSTALMENT_PAYMENT_DUE_7DAYS_LEFT_OTF:
            case INSTALMENT_PAYMENT_DUE_1DAY_LEFT_OTF:
            case INSTALMENT_PAYMENT_DUE_OTF_BLOCKED:
            case INSTALMENT_PAYMENT_OTF_FINAL_REMINDER:
            case INSTALMENT_PAYMENT_OTF_DUE_2DAYS_PASSED:
                student = userManager.getUserById(req.getUserId());
                if (student == null) {
                    throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + req.getUserId());
                }
                subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + req.getSubscriptionLink();
                String shortUrl = WebCommunicator.getShortUrl(subscriptionLink);
                if (!StringUtils.isEmpty(shortUrl)) {
                    subscriptionLink = shortUrl;
                }
                scopeParams.put("subscriptionLink", subscriptionLink);
                scopeParams.put("subscriptionTitle", req.getSubscriptionTitle());
                scopeParams.put("studentName", student.getFullName());
                toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
                request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
                sendEmail(request);

                if ((CommunicationType.INSTALMENT_PAYMENT_OTF_FINAL_REMINDER.equals(communicationType)
                        || CommunicationType.INSTALMENT_PAYMENT_OTF_DUE_2DAYS_PASSED.equals(communicationType))) {
                    break;
                }
                if (StringUtils.isNotEmpty(student.getContactNumber())) {
                    smsRequest = new TextSMSRequest(student.getContactNumber(), student.getPhoneCode(), scopeParams,
                            communicationType, Role.STUDENT);
                    smsManager.sendSMS(smsRequest);
                }

                break;
            default:
                logger.error("Invalid communication type {0}", req.getCommunicationType());
                break;
        }

    }

    public void sendInstalmentPaidEmail(SendInstalmentPaidEmailReq req)
            throws VException, UnsupportedEncodingException {
        if (req.getSubscriptionId() == null) {
            sendInstalmentPaidEmailOTF(req);
            return;
        }
        CommunicationType communicationType = CommunicationType.INSTALMENT_PAID;
        User student = userManager.getUserById(req.getStudentId());
        User teacher = userManager.getUserById(req.getTeacherId());
        if (student == null || teacher == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + req);
        }

        HashMap<String, Object> scopeParams = new HashMap<>();

        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/subscription/"
                + req.getSubscriptionId();
        scopeParams.put("subscriptionLink", subscriptionLink);
        scopeParams.put("subscriptionTitle", req.getSubscriptionTitle());

        String teacherName = teacher.getFullName();
        String studentName = student.getFullName();
        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }

        scopeParams.put("teacherName", teacherName);
        scopeParams.put("studentName", studentName);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        for (InstalmentInfo instalmentInfo : req.getInstalments()) {
            if (instalmentInfo.getDueTime() != null) {
                instalmentInfo.setDueDate(sdf.format(new Date(instalmentInfo.getDueTime())));
            }
            if (instalmentInfo.getPaidTime() != null) {
                instalmentInfo.setPaidDate(sdf.format(new Date(instalmentInfo.getPaidTime())));
            }
            if (instalmentInfo.getHours() != null) {
                instalmentInfo.setHours(instalmentInfo.getHours() / DateTimeUtils.MILLIS_PER_HOUR);
            }
            if (instalmentInfo.getTotalAmount() != null) {
                instalmentInfo.setTotalAmount(instalmentInfo.getTotalAmount() / 100);
            }
            if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())) {
                instalmentInfo.setPaymentStatusStr("Not Paid");
            } else if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                instalmentInfo.setPaymentStatusStr("Paid");
            }
        }

        scopeParams.put("instalments", req.getInstalments());

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
        sendEmail(request);

        if (StringUtils.isNotEmpty(student.getContactNumber())) {
            TextSMSRequest smsRequest = new TextSMSRequest(student.getContactNumber(), student.getPhoneCode(), scopeParams,
                    communicationType, Role.STUDENT);
            smsManager.sendSMS(smsRequest);
        }

        InstalmentNotificationReq instalmentDueNotificationReq = new InstalmentNotificationReq(communicationType,
                req.getSubscriptionId(), subscriptionLink, req.getSubscriptionTitle(), null, teacher.getFullName(),
                null, null, student.getFullName());

        CreateNotificationRequest createNotificationRequest = new CreateNotificationRequest(student.getId(),
                NotificationType.valueOf(communicationType.name()), new Gson().toJson(instalmentDueNotificationReq),
                Role.STUDENT);

        notificationManager.createNotification(createNotificationRequest);

    }

    public void sendInstalmentPaidEmailOTF(SendInstalmentPaidEmailReq req)
            throws VException, UnsupportedEncodingException {

        CommunicationType communicationType = CommunicationType.INSTALMENT_PAID_OTF;
        User student = userManager.getUserById(req.getStudentId());
        if (student == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + req);
        }

        HashMap<String, Object> scopeParams = new HashMap<>();

        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + req.getSubscriptionLink();
        scopeParams.put("subscriptionLink", subscriptionLink);
        scopeParams.put("subscriptionTitle", req.getSubscriptionTitle());
        scopeParams.put("studentName", student.getFullName());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        Long dueTime = null;
        Long paidTime = null;
        Float paidAmount = null;
        Long nextDueTime = null;
        Collections.sort(req.getInstalments(), (InstalmentInfo o1, InstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
        for (InstalmentInfo instalmentInfo : req.getInstalments()) {
            if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                dueTime = instalmentInfo.getDueTime();
                paidTime = instalmentInfo.getPaidTime();
                paidAmount = instalmentInfo.getTotalAmount() * 1.0f / 100;

            }
            if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())
                    || PaymentStatus.PAYMENT_SUSPENDED.equals(instalmentInfo.getPaymentStatus())) {
                nextDueTime = instalmentInfo.getDueTime();
                break;
            }

        }

        if (paidTime != null) {
            scopeParams.put("paymentDate", sdf.format(new Date(paidTime)));
        }
        if (dueTime != null) {
            scopeParams.put("dueDate", sdf.format(new Date(dueTime)));
        }
        if (paidAmount != null) {
            scopeParams.put("amountPaid", paidAmount);
        }
        if (nextDueTime != null) {
            scopeParams.put("nextDueDate", sdf.format(new Date(nextDueTime)));
        }

//        for (InstalmentInfo instalmentInfo : req.getInstalments()) {
//            if (instalmentInfo.getDueTime() != null) {
//                instalmentInfo.setDueDate(sdf.format(new Date(instalmentInfo.getDueTime())));
//            }
//            if (instalmentInfo.getPaidTime() != null) {
//                instalmentInfo.setPaidDate(sdf.format(new Date(instalmentInfo.getPaidTime())));
//            }
//            // if (instalmentInfo.getHours() != null) {
//            // instalmentInfo.setHours(instalmentInfo.getHours() /
//            // DateTimeUtils.MILLIS_PER_HOUR);
//            // }
//            if (instalmentInfo.getTotalAmount() != null) {
//                instalmentInfo.setTotalAmount(instalmentInfo.getTotalAmount() / 100);
//            }
//            if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())) {
//                instalmentInfo.setPaymentStatusStr("Not Paid");
//            } else if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
//                instalmentInfo.setPaymentStatusStr("Paid");
//            }
//        }
//
//        scopeParams.put("instalments", req.getInstalments());
        logger.info(req.getInstalments().toString());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendTeacherEditProfileToAdmin(EditProfileRes response,
            HashMap<String, HashMap<Object, Object>> diffData, HttpSessionData callerUser)
            throws VException, UnsupportedEncodingException {
        String emailAddrTC = ConfigUtils.INSTANCE.getStringValue("email.ADDRESS.TEACHER_CARE");
        String emailAddrAtom = ConfigUtils.INSTANCE.getStringValue("email.display.emailId");

        // HttpSessionData callerUser = sessionUtils.getCurrentSessionData();
        CommunicationType emailType = CommunicationType.TEACHER_EDIT_PROFILE;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        String userFullName = (response.getFirstName() + " "
                + (StringUtils.isEmpty(response.getLastName()) ? "" : response.getLastName()));
        subjectScopes.put("teacherName", userFullName);
        bodyScopes.put("teacherName", userFullName);
        bodyScopes.put("teacherId", response.getUserId());
        Set<Map.Entry<String, Set<Map.Entry<Object, Object>>>> diffDataEntrySet = new HashSet<>();
        Iterator it = diffData.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry data = (Map.Entry) it.next();
            String key = data.getKey().toString();
            Map<Object, Object> value = (Map<Object, Object>) data.getValue();
            Set<Map.Entry<Object, Object>> valueSet = value.entrySet();
            Map.Entry<String, Set<Map.Entry<Object, Object>>> entry = new MyEntry<String, Set<Map.Entry<Object, Object>>>(
                    key, valueSet);
            diffDataEntrySet.add(entry);
        }
        bodyScopes.put("diffData", diffDataEntrySet);
        bodyScopes.put("callerUserName", (callerUser.getFirstName() + " "
                + (StringUtils.isEmpty(callerUser.getLastName()) ? "" : callerUser.getLastName())));
        bodyScopes.put("callerUserId", callerUser.getUserId());
        bodyScopes.put("callerUserRole", callerUser.getRole());

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(emailAddrTC, "Teacher Care"));
        toList.add(new InternetAddress(emailAddrAtom, "Atom"));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.ADMIN);
        sendEmail(request);
    }

    public void sendTeacherBoardMappingUpdateToAdmin(Long userId, String subjectName, String grade, String category,
            String changeType, HttpSessionData callerUser) throws UnsupportedEncodingException, VException {
        CommunicationType emailType = CommunicationType.TEACHER_BOARD_MAPPING_CHANGED;
        String emailAddrTC = ConfigUtils.INSTANCE.getStringValue("email.ADDRESS.TEACHER_CARE");
        String emailAddrAtom = ConfigUtils.INSTANCE.getStringValue("email.display.emailId");

        UserInfo user = userManager.getUserInfo(userId);

        // HttpSessionData callerUser = sessionUtils.getCurrentSessionData();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        String userFullName = (user.getFirstName() + " "
                + (StringUtils.isEmpty(user.getLastName()) ? "" : user.getLastName()));
        subjectScopes.put("teacherName", userFullName);
        bodyScopes.put("teacherName", userFullName);
        bodyScopes.put("teacherId", user.getUserId());
        bodyScopes.put("callerUserName", (callerUser.getFirstName() + " "
                + (StringUtils.isEmpty(callerUser.getLastName()) ? "" : callerUser.getLastName())));
        bodyScopes.put("callerUserId", callerUser.getUserId());
        bodyScopes.put("callerUserRole", callerUser.getRole());

        bodyScopes.put("subjectName", subjectName);
        bodyScopes.put("grade", grade);
        bodyScopes.put("category", category);
        bodyScopes.put("changeType", changeType);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(emailAddrTC, "Teacher Care"));
        toList.add(new InternetAddress(emailAddrAtom, "Atom"));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.ADMIN);
        sendEmail(request);

    }

    public void sendBlockUserToAdmin(Long callingUserId, Long userId, String reason, HttpSessionData callerUser)
            throws VException, UnsupportedEncodingException {
        String emailAddrSC = ConfigUtils.INSTANCE.getStringValue("email.ADDRESS.STUDENT_CARE");
        String emailAddrTC = ConfigUtils.INSTANCE.getStringValue("email.ADDRESS.TEACHER_CARE");
        String emailAddrAtom = ConfigUtils.INSTANCE.getStringValue("email.display.emailId");
        UserInfo blockedUser = userManager.getUserInfo(userId);
        // HttpSessionData callerUser = sessionUtils.getCurrentSessionData();
        logger.info("sendBlockUserToAdmin", blockedUser._getFullName(), callerUser.getFirstName());
        logger.info("sending block user email to : " + emailAddrSC + ", " + emailAddrTC + ", " + emailAddrAtom);
        CommunicationType emailType = CommunicationType.BLOCKED_USER;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();

        String callerFullName = callerUser.getFirstName() + " "
                + (!StringUtils.isEmpty(callerUser.getLastName()) ? callerUser.getLastName() : "");
        String blockedUserFullName = blockedUser.getFirstName() + " "
                + (!StringUtils.isEmpty(blockedUser.getLastName()) ? blockedUser.getLastName() : "");

        bodyScopes.put("callerUserRole", callerUser.getRole().name().toLowerCase());
        bodyScopes.put("callerUserName", callerFullName);
        bodyScopes.put("callerUserId", callerUser.getUserId());

        bodyScopes.put("blockedUserRole", blockedUser.getRole().name().toLowerCase());
        bodyScopes.put("blockedUserName", blockedUserFullName);
        bodyScopes.put("blockedUserId", blockedUser.getUserId());
        bodyScopes.put("reason", reason);

        subjectScopes.put("callerUserRole", callerUser.getRole().name().toLowerCase());
        subjectScopes.put("callerUserName", callerFullName);

        subjectScopes.put("blockedUserRole", blockedUser.getRole().name().toLowerCase());
        subjectScopes.put("blockedUserName", blockedUserFullName);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(emailAddrSC, "Student Care"));
        toList.add(new InternetAddress(emailAddrTC, "Teacher Care"));
        toList.add(new InternetAddress(emailAddrAtom, "Atom"));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.ADMIN);
        sendEmail(request);
    }

    public void sendMessageUserEmail(User fromUser, User toUser, HashMap<String, Object> bodyScopes,
            CommunicationType emailType) throws VException, UnsupportedEncodingException {
        bodyScopes.put("fromUserRoleUpper", fromUser.getRole().toString());
//        sendPublicEmail(toUser.getFirstName(), toUser.getEmail(), bodyScopes, emailType);

        TextSMSRequest textSMSRequest;
        if (StringUtils.isNotEmpty(toUser.getContactNumber())) {
            textSMSRequest = new TextSMSRequest(toUser.getContactNumber(), toUser.getPhoneCode(), bodyScopes,
                    emailType, toUser.getRole());
            smsManager.sendSMS(textSMSRequest);
        }
//
//
//        ArrayList<InternetAddress> toAddress = new ArrayList<>();
//        toAddress.add(new InternetAddress(fromUser.getEmail(), fromUser._getFullName()));
//
//        logger.info("sending sendMessageUserEmail  to : " + toAddress.toString());
//        bodyScopes.put("name", fromUser.getFirstName());
//        bodyScopes.put("toUserName", toUser._getFullName());
//        bodyScopes.put("toUserRole", toUser.getRole().toString().toLowerCase());
//        bodyScopes.put("toUserLink", userManager.getUserProfileLink(toUser));
//        bodyScopes.put("fromUserRoleUpper", fromUser.getRole());
//
//        EmailRequest request = new EmailRequest(toAddress, bodyScopes, bodyScopes,
//                CommunicationType.MESSAGE_CONFIRMATION, fromUser.getRole());
//        sendEmail(request);
//
//        if(StringUtils.isNotEmpty(fromUser.getContactNumber())){
//            textSMSRequest = new TextSMSRequest(fromUser.getContactNumber(), fromUser.getPhoneCode(), bodyScopes,
//                    CommunicationType.MESSAGE_CONFIRMATION, fromUser.getRole());
//            smsManager.sendSMS(textSMSRequest);            
//        }

    }

    @Deprecated
    public void sendPublicEmail(String name, String emailId, HashMap<String, Object> bodyScopes,
            CommunicationType emailType) throws UnsupportedEncodingException, VException {

        String domainUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("domainUrl", domainUrl);

        logger.info("sendPublicEmail");
        bodyScopes.put("name", name);
        bodyScopes.put("emailId", emailId);

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress(emailId, name));

        EmailRequest request = new EmailRequest(toAddress, bodyScopes, bodyScopes, emailType, Role.STUDENT);
        sendEmail(request);

    }

    public void sendUserMessageEmail(User user, UserMessage message)
            throws VException, AddressException, UnsupportedEncodingException {

        logger.info("sendUserMessageEmail");

        CommunicationType emailType = CommunicationType.USER_MESSAGE;
        HashMap<String, Object> bodyScopes = new HashMap<>();

        bodyScopes.put("name", user._getFullName());
        bodyScopes.put("messageType", message.getMessageType());
        bodyScopes.put("message", message.getMessage());
        bodyScopes.put("mobileNumber", user.getContactNumber());
        bodyScopes.put("emailId", user.getEmail());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        if (message.getReferenceTime() != null) {
            String dateTime = sdf.format(new Date(message.getReferenceTime()));
            bodyScopes.put("sessionTimeText", "Requested Time: " + dateTime);
        }

        List<String> extraInfoList = new ArrayList<>();

        if (StringUtils.isNotEmpty(message.getSubjectName())) {
            extraInfoList.add("Subject: " + message.getSubjectName());
        }

        if (StringUtils.isNotEmpty(message.getCategoryName())) {
            extraInfoList.add("Category: " + message.getCategoryName());
        }

        if (StringUtils.isNotEmpty(message.getGrade())) {
            extraInfoList.add("Grade: " + message.getGrade());
        }

        if (!extraInfoList.isEmpty()) {
            String extraInfoHtml = "";
            for (String s : extraInfoList) {
                extraInfoHtml += ("<div>" + s + "</div>");
            }
            bodyScopes.put("extraInfoHtml", extraInfoHtml);
        }

        if (CollectionUtils.isNotEmpty(message.getFileUrls())) {
            String attachmentHtml = "";
            for (String url : message.getFileUrls()) {
                attachmentHtml += ("<div><a href=\"" + url + "\">" + url + "</div>");
            }
            bodyScopes.put("attachmentHtml", attachmentHtml);
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user._getFullName());
        subjectScopes.put("mobileNumber", user.getContactNumber());

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(InternetAddress.parse(ConfigUtils.INSTANCE.getStringValue("email.TO"))[0]);

        ArrayList<InternetAddress> replyToAddress = new ArrayList<>();
        replyToAddress.add(new InternetAddress(user.getEmail(), user._getFullName()));

        EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.ADMIN);
        request.setReplyTo(replyToAddress);
        sendEmail(request);

    }

    public void sendQueryEmail(String name, String emailId, String mobileNumber, String subject, String message,
            CommunicationType emailType) throws VException, UnsupportedEncodingException, AddressException {

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", name);
        bodyScopes.put("messageType", "INVITE_REQUEST");
        bodyScopes.put("emailId", emailId);
        bodyScopes.put("mobileNumber", mobileNumber);
        bodyScopes.put("subject", subject);
        bodyScopes.put("message", message);

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", name);
        subjectScopes.put("mobileNumber", mobileNumber);

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(InternetAddress.parse(ConfigUtils.INSTANCE.getStringValue("email.TO"))[0]);

        ArrayList<InternetAddress> replyToAddress = new ArrayList<>();
        replyToAddress.add(new InternetAddress(emailId, name));

        EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.ADMIN);
        request.setReplyTo(replyToAddress);
        sendEmail(request);

    }

    public void sendTrialReminder(SessionInfo session)
            throws AddressException, NotFoundException, IOException, VException {
        List<UserSessionInfo> attendees = session.getAttendees();
        UserBasicInfo student = null;
        UserBasicInfo teacher = null;
        for (UserSessionInfo attendee : attendees) {
            if (Role.STUDENT.equals(attendee.getRole())) {
                student = attendee;
            } else if (Role.TEACHER.equals(attendee.getRole())) {
                teacher = attendee;
            }
        }
        Map<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("teacherFirstName", teacher.getFirstName());
        bodyScopes.put("studentFirstName", student.getFirstName());
        bodyScopes.put("teacherPhoneNumber", teacher.getPrimaryCallingNumberForTeacher());
        bodyScopes.put("teacherExtensionNumber", teacher.getExtensionNumber());
        bodyScopes.put("studentPhoneNumber", student.getContactNumber());
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        String startTime = sdf.format(new Date(session.getStartTime()));
        bodyScopes.put("startTime", startTime);
        bodyScopes.put("subject", session.getSubject());

        TextSMSRequest textSMSRequest;
        if (student.getContactNumber() != null) {
            textSMSRequest = new TextSMSRequest(student.getContactNumber(), student.getPhoneCode(), bodyScopes,
                    CommunicationType.TRIAL_SESSION_REMINDER, Role.STUDENT);
            smsManager.sendSMS(textSMSRequest);
        }

        if (StringUtils.isNotEmpty(teacher.getContactNumber())) {
            textSMSRequest = new TextSMSRequest(teacher.getContactNumber(), teacher.getPhoneCode(), bodyScopes,
                    CommunicationType.TRIAL_SESSION_REMINDER, Role.TEACHER);
            smsManager.sendSMS(textSMSRequest);
        }
    }

    public void sendRequestCallbackCommunication(RequestCallBackDetails requestCallBackDetails,
            CommunicationType communicationType) throws AddressException, IOException, VException {

        List<Long> userIds = new ArrayList<>();
        Long teacherId = requestCallBackDetails.getTeacherId();
        Long studentId = requestCallBackDetails.getStudentId();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacherBasicInfo = userInfos.get(teacherId);
        UserBasicInfo studentBasicInfo = userInfos.get(studentId);

        HashMap<String, Object> bodyScopes = new HashMap<>();

        String teacherName = teacherBasicInfo.getFullName();
        String studentName = studentBasicInfo.getFullName();
        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }
        String teacherExtensionNumber = teacherBasicInfo.getPrimaryCallingNumberForTeacher();
        com.vedantu.platform.mongodbentities.board.Board board = boardManager
                .getBoardById(requestCallBackDetails.getBoardId());
        bodyScopes.put("studentName", studentName);
        bodyScopes.put("teacherName", teacherName);
        bodyScopes.put("studentContactNumber", studentBasicInfo.getContactNumber());
        bodyScopes.put("teacherExtensionNumber",
                teacherExtensionNumber + " ext:" + teacherBasicInfo.getExtensionNumber());
        if (requestCallBackDetails.getGrade() != null) {
            bodyScopes.put("grade", requestCallBackDetails.getGrade().toString());
        }
        bodyScopes.put("target", requestCallBackDetails.getTarget());
        if (board != null) {
            bodyScopes.put("subject", board.getName());
        }
        bodyScopes.put("message", requestCallBackDetails.getMessage());

        List<InternetAddress> toList = new ArrayList<>();
        List<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress(TEACHER_CARE_EMAIL_ID));
        EmailRequest request;
        TextSMSRequest smsRequest;
        CreateNotificationRequest createNotificationRequest;

        toList.add(new InternetAddress(studentBasicInfo.getEmail(), studentBasicInfo.getFullName()));
        request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, studentBasicInfo.getRole());
        request.setBcc(bccList);
        sendEmail(request);

        if (StringUtils.isNotEmpty(studentBasicInfo.getContactNumber())) {
            smsRequest = new TextSMSRequest(studentBasicInfo.getContactNumber(), studentBasicInfo.getPhoneCode(),
                    bodyScopes, communicationType, studentBasicInfo.getRole());
            smsManager.sendSMS(smsRequest);
        }

        toList.clear();
        toList.add(new InternetAddress(teacherBasicInfo.getEmail(), teacherBasicInfo.getFullName()));
        request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, teacherBasicInfo.getRole());
        request.setBcc(bccList);
        sendEmail(request);

        if (StringUtils.isNotEmpty(teacherBasicInfo.getContactNumber())) {
            smsRequest = new TextSMSRequest(teacherBasicInfo.getContactNumber(), teacherBasicInfo.getPhoneCode(),
                    bodyScopes, communicationType, teacherBasicInfo.getRole());
            smsManager.sendSMS(smsRequest);
        }

        createNotificationRequest = new CreateNotificationRequest(teacherBasicInfo.getUserId(),
                NotificationType.valueOf(communicationType.name()), new Gson().toJson(bodyScopes),
                teacherBasicInfo.getRole());
        notificationManager.createNotification(createNotificationRequest);
    }

    public void sendMoodleNotifications(SendMoodleNotificationsReq request) {
        String eventName = request.getEventName();
        MoodleContentInfo moodleContentInfo = request.getContentInfo();
        if (moodleContentInfo.getEngagementType() != null && StringUtils.isNotEmpty(moodleContentInfo.getContextId())
                && EngagementType.OTF.equals(moodleContentInfo.getEngagementType()) && StringUtils.isEmpty(moodleContentInfo.getCourseName())) {
            Set<String> contextId = new HashSet<>();
            contextId.add(moodleContentInfo.getContextId());
            try {
                Map<String, String> courseMap = otfManager.getCourseTitleForBatchIds(contextId);
                if (courseMap != null) {
                    String courseName = courseMap.get(moodleContentInfo.getContextId());
                    if (StringUtils.isNotEmpty(courseName)) {
                        moodleContentInfo.setCourseName(courseName);
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.error("Error in getting courseTitle", e);
            }
        }
        switch (eventName) {
            case "ATTEMPTED":
                try {
                    sendMoodleContentAttempted(moodleContentInfo);
                } catch (Exception ex) {
                    logger.error("MoodleNotificationsTaskError", ex);
                }
                break;
            case "EVALUATED":
                try {
                    sendMoodleContentReport(moodleContentInfo);
                } catch (Exception ex) {
                    logger.error("MoodleNotificationsTaskError", ex);
                }
                break;
            case "SHARED":
                try {
                    sendMoodleContentShared(moodleContentInfo);
                } catch (Exception ex) {
                    logger.error("MoodleNotificationsTaskError", ex);
                }
                break;
            case "PENDING_EVALUATION":
                try {
                    sendMoodleContentPendingEvaluation(moodleContentInfo);
                } catch (Exception ex) {
                    logger.error("MoodleNotificationsTaskError", ex);
                }
                break;
            default:
                return;
        }

    }

    public void sendMoodleContentShared(MoodleContentInfo contentInfo)
            throws AddressException, NotFoundException, IOException, VException {

        UserBasicInfo teacher = null;
        UserBasicInfo student = null;
        if ("OBJECTIVE".equals(contentInfo.getContentInfoType())) {
            logger.info("Test is objective, so no notification");
        }

        List<Long> userIds = new ArrayList<>();
        if (contentInfo.getTeacherId() != null && contentInfo.getStudentId() != null) {
            Long teacherId = Long.parseLong(contentInfo.getTeacherId());
            userIds.add(teacherId);
            Long studentId = Long.parseLong(contentInfo.getStudentId());
            userIds.add(studentId);
            Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            teacher = userInfos.get(teacherId);
            student = userInfos.get(studentId);
            if (student == null || teacher == null) {
                logger.info("sendMoodleContentShared",
                        "sendMoodleContentSharedError : student or teacher does not exit");
                return;
            }
        } else {
            logger.info("sendMoodleContentShared", "sendMoodleContentSharedError : student or teacher does not exit");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", student.getFirstName());
        bodyScopes.put("studentFullName", student.getFullName());
        bodyScopes.put("teacherName", teacher.getFirstName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("contentName", contentInfo.getContentTitle());
        bodyScopes.put("courseName", contentInfo.getCourseName());
        bodyScopes.put("engagementType", contentInfo.getEngagementType());
        bodyScopes.put("attemptNowLink", contentInfo.getStudentActionLink());
        bodyScopes.put("contentLink", contentInfo.getContentLink());
        bodyScopes.put("attemptedTime", contentInfo.getAttemptedTime());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        boolean isObjective = false;
        if ("OBJECTIVE".equals(contentInfo.getContentInfoType())) {
            isObjective = true;
        }
        bodyScopes.put("isObjective", isObjective);
        bodyScopes.put("urlBase", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("contentId", contentInfo.getId());
        bodyScopes.put("attemptId", contentInfo.getAttemptId());
        bodyScopes.put("isObjective", isObjective);

        String actionLink = contentInfo.getStudentActionLink();
        if (StringUtils.isEmpty(actionLink)) {
            actionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("actionLink", actionLink);

        String teacherActionLink = contentInfo.getTeacherActionLink();
        if (StringUtils.isEmpty(teacherActionLink)) {
            teacherActionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("teacherActionLink", teacherActionLink);

        Long expiryTime = contentInfo.getExpiryTime();
        if (expiryTime != null && expiryTime > 0) {
            String dateTime = sdf.format(new Date(expiryTime));
            bodyScopes.put("expiryTime", dateTime);
        }

        Long creationTime = contentInfo.getCreationTime();
        if (creationTime != null && creationTime > 0) {
            bodyScopes.put("creationTime", sdf.format(new Date(creationTime)));
        }

        String appUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.android.app.link");
        String shortUrl = WebCommunicator.getShortUrl(appUrl);
        if (!StringUtils.isEmpty(shortUrl)) {
            appUrl = shortUrl;
        }
        bodyScopes.put("appUrl", appUrl);
        CommunicationType communicationType = null;
        switch (contentInfo.getContentType()) {
            case "TEST":
                communicationType = CommunicationType.LMS_TEST_SHARED;
                // we do not want to send this email
                return;
            case "ASSIGNMENT":
                communicationType = CommunicationType.LMS_ASSIGNMENT_SHARED;
                break;
            case "NOTES":
                communicationType = CommunicationType.LMS_NOTES_SHARED;
                break;
            default:
                return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, student.getRole());
        sendEmail(request);

        // Send SMS
        // if (expiryTime != null && expiryTime > 0) {
        // String dateTime = sdf.format(new Date(expiryTime));
        // if (contentInfo.getContentType().equals("ASSIGNMENT")) {
        // bodyScopes.put("expiryTime", ". Last date to submit it is " +
        // dateTime);
        // } else if (contentInfo.getContentType().equals("TEST")) {
        // bodyScopes.put("expiryTime", ". Last date to attempt the test is " +
        // dateTime);
        // }
        // }
        // TextSMSRequest textSMSRequest = new
        // TextSMSRequest(student.getContactNumber(), student.getPhoneCode(),
        // bodyScopes, communicationType, student.getRole());
        // smsManager.sendSMS(textSMSRequest);
        //
        // MoodleNotificationData notificationData = new
        // MoodleNotificationData(contentInfo, communicationType.name());
        //
        // CreateNotificationRequest createNotificationRequest = new
        // CreateNotificationRequest(student.getUserId(),
        // NotificationType.LMS_NOTIFICATION, new
        // Gson().toJson(notificationData), student.getRole());
        //
        // notificationManager.createNotification(createNotificationRequest);
    }

    public void sendMoodleContentReport(MoodleContentInfo contentInfo)
            throws AddressException, NotFoundException, IOException, VException {
        UserBasicInfo teacher = null;
        UserBasicInfo student = null;
        if ("OBJECTIVE".equals(contentInfo.getContentInfoType())) {
            logger.info("Test is objective, so no notification");
        }

        List<Long> userIds = new ArrayList<>();
        if (contentInfo.getTeacherId() != null && contentInfo.getStudentId() != null) {
            Long teacherId = Long.parseLong(contentInfo.getTeacherId());
            userIds.add(teacherId);
            Long studentId = Long.parseLong(contentInfo.getStudentId());
            userIds.add(studentId);
            Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            teacher = userInfos.get(teacherId);
            student = userInfos.get(studentId);
            if (student == null || teacher == null) {
                logger.info("sendMoodleContentReport",
                        "sendMoodleContentReportError : student or teacher does not exit");
                return;
            }
        } else {
            logger.info("sendMoodleContentReport", "sendMoodleContentReportError : student or teacher does not exit");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("studentName", student.getFirstName());
        bodyScopes.put("studentFullName", student.getFullName());
        bodyScopes.put("teacherName", teacher.getFirstName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("contentName", contentInfo.getContentTitle());
        bodyScopes.put("courseName", contentInfo.getCourseName());
        bodyScopes.put("engagementType", contentInfo.getEngagementType());
        bodyScopes.put("attemptNowLink", contentInfo.getStudentActionLink());
        bodyScopes.put("contentLink", contentInfo.getContentLink());
        bodyScopes.put("attemptedTime", contentInfo.getAttemptedTime());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        boolean isObjective = false;
        if ("OBJECTIVE".equals(contentInfo.getContentInfoType())) {
            isObjective = true;
        }
        bodyScopes.put("isObjective", isObjective);
        bodyScopes.put("urlBase", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("contentId", contentInfo.getId());
        bodyScopes.put("attemptId", contentInfo.getAttemptId());
        bodyScopes.put("isObjective", isObjective);

        String attemptTimeStr = null;
        if (StringUtils.isNotEmpty(contentInfo.getAttemptId())) {
            CMDSTestAttemptPojo cmdsTestAttemptPojo = getTestAttemptFromContentInfo(contentInfo,
                    contentInfo.getAttemptId());
            Long attemptedTime = cmdsTestAttemptPojo.getStartedAt();
            if (attemptedTime != null && attemptedTime > 0) {
                bodyScopes.put("attemptTimeStr", sdf.format(new Date(attemptedTime)));
            }
        }
        // subjectName

        String actionLink = contentInfo.getStudentActionLink();
        if (StringUtils.isEmpty(actionLink)) {
            actionLink = contentInfo.getContentLink();
        }

        // String actionLink = ConfigUtils.INSTANCE.getStringValue("url.base") +
        // "/contents/test/result/"+contentInfo.getId();
        // if(StringUtils.isNotEmpty(contentInfo.getAttemptId())) {
        // actionLink += "?attemptId="+contentInfo.getAttemptId();
        // }
        bodyScopes.put("actionLink", actionLink);

        String teacherActionLink = contentInfo.getTeacherActionLink();
        if (StringUtils.isEmpty(teacherActionLink)) {
            teacherActionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("teacherActionLink", teacherActionLink);

        Long expiryTime = contentInfo.getExpiryTime();
        if (expiryTime != null && expiryTime > 0) {
            String dateTime = sdf.format(new Date(expiryTime));
            bodyScopes.put("expiryTime", dateTime);
        }

        Long creationTime = contentInfo.getCreationTime();
        if (creationTime != null && creationTime > 0) {
            bodyScopes.put("creationTime", sdf.format(new Date(creationTime)));
        }

        String appUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.android.app.link");
        String shortUrl = WebCommunicator.getShortUrl(appUrl);
        if (!StringUtils.isEmpty(shortUrl)) {
            appUrl = shortUrl;
        }
        bodyScopes.put("appUrl", appUrl);
        CommunicationType communicationType = null;
        switch (contentInfo.getContentType()) {
            case "TEST":
                communicationType = CommunicationType.LMS_TEST_REPORT;
                break;
            case "ASSIGNMENT":
                communicationType = CommunicationType.LMS_ASSIGNMENT_REPORT;
                break;
            default:
                return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, student.getRole());
        sendEmail(request);

        // Send SMS
        // TextSMSRequest textSMSRequest = new
        // TextSMSRequest(student.getContactNumber(), student.getPhoneCode(),
        // bodyScopes, communicationType, student.getRole());
        // smsManager.sendSMS(textSMSRequest);
        // MoodleNotificationData notificationData = new
        // MoodleNotificationData(contentInfo, communicationType.name());
        // CreateNotificationRequest createNotificationRequest = new
        // CreateNotificationRequest(student.getUserId(),
        // NotificationType.LMS_NOTIFICATION, new
        // Gson().toJson(notificationData), student.getRole());
        //
        // notificationManager.createNotification(createNotificationRequest);
    }

    public void sendMoodleContentAttempted(MoodleContentInfo contentInfo)
            throws AddressException, NotFoundException, IOException, VException {
        UserBasicInfo teacher = null;
        UserBasicInfo student = null;
        if ("OBJECTIVE".equals(contentInfo.getContentInfoType())) {
            logger.info("Test is objective, so no notification");
        }

        List<Long> userIds = new ArrayList<>();
        if (contentInfo.getTeacherId() != null && contentInfo.getStudentId() != null) {
            Long teacherId = Long.parseLong(contentInfo.getTeacherId());
            userIds.add(teacherId);
            Long studentId = Long.parseLong(contentInfo.getStudentId());
            userIds.add(studentId);
            Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            teacher = userInfos.get(teacherId);
            student = userInfos.get(studentId);
            if (student == null || teacher == null) {
                logger.info("sendMoodleContentAttempted",
                        "sendMoodleContentAttemptedError : student or teacher does not exit");
                return;
            }
        } else {
            logger.info("sendMoodleContentAttempted",
                    "sendMoodleContentAttemptedError : student or teacher does not exit");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("studentName", student.getFirstName());
        bodyScopes.put("studentFullName", student.getFullName());
        bodyScopes.put("teacherName", teacher.getFirstName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("contentName", contentInfo.getContentTitle());
        bodyScopes.put("courseName", contentInfo.getCourseName());
        bodyScopes.put("engagementType", contentInfo.getEngagementType());
        bodyScopes.put("attemptNowLink", contentInfo.getStudentActionLink());
        bodyScopes.put("contentLink", contentInfo.getContentLink());
        bodyScopes.put("attemptedTime", contentInfo.getAttemptedTime());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        boolean isObjective = false;
        if ("OBJECTIVE".equals(contentInfo.getContentInfoType())) {
            isObjective = true;
        }
        bodyScopes.put("isObjective", isObjective);
        bodyScopes.put("urlBase", ConfigUtils.INSTANCE.getStringValue("url.base"));
        bodyScopes.put("contentId", contentInfo.getId());
        bodyScopes.put("attemptId", contentInfo.getAttemptId());
        bodyScopes.put("isObjective", isObjective);

        String attemptTimeStr = null;
        if (StringUtils.isNotEmpty(contentInfo.getAttemptId())) {
            CMDSTestAttemptPojo cmdsTestAttemptPojo = getTestAttemptFromContentInfo(contentInfo,
                    contentInfo.getAttemptId());
            Long attemptedTime = cmdsTestAttemptPojo.getStartedAt();
            if (attemptedTime != null && attemptedTime > 0) {
                bodyScopes.put("attemptTimeStr", sdf.format(new Date(attemptedTime)));
            }
        }
        // subjectName

        String actionLink = contentInfo.getStudentActionLink();
        if (StringUtils.isEmpty(actionLink)) {
            actionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("actionLink", actionLink);

        String teacherActionLink = contentInfo.getTeacherActionLink();
        if (StringUtils.isEmpty(teacherActionLink)) {
            teacherActionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("teacherActionLink", teacherActionLink);

        Long expiryTime = contentInfo.getExpiryTime();
        if (expiryTime != null && expiryTime > 0) {
            String dateTime = sdf.format(new Date(expiryTime));
            bodyScopes.put("expiryTime", dateTime);
        }

        Long creationTime = contentInfo.getCreationTime();
        if (creationTime != null && creationTime > 0) {
            bodyScopes.put("creationTime", sdf.format(new Date(creationTime)));
        }

        String appUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.android.app.link");
        String shortUrl = WebCommunicator.getShortUrl(appUrl);
        if (!StringUtils.isEmpty(shortUrl)) {
            appUrl = shortUrl;
        }
        bodyScopes.put("appUrl", appUrl);
        CommunicationType communicationType = null;
        switch (contentInfo.getContentType()) {
            case "TEST":
                communicationType = CommunicationType.LMS_TEST_ATTEMPTED;
                break;
            case "ASSIGNMENT":
                communicationType = CommunicationType.LMS_ASSIGNMENT_ATTEMPTED;
                break;
            default:
                return;
        }

        if ("YOUSCORE".equals(contentInfo.getLmsType()) || (!"TEST".equals(contentInfo.getContentType())
                || !"OBJECTIVE".equals(contentInfo.getContentInfoType()))) {

            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(teacher.getEmail(), teacher.getFullName()));
            EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                    teacher.getRole());
            sendEmail(request);

            // Send SMS
            // TextSMSRequest textSMSRequest = new
            // TextSMSRequest(teacher.getContactNumber(),
            // teacher.getPhoneCode(), bodyScopes, communicationType,
            // teacher.getRole());
            // smsManager.sendSMS(textSMSRequest);
        }
        // MoodleNotificationData notificationData = new
        // MoodleNotificationData(contentInfo, communicationType.name());
        // CreateNotificationRequest createNotificationRequest = new
        // CreateNotificationRequest(teacher.getUserId(),
        // NotificationType.LMS_NOTIFICATION, new
        // Gson().toJson(notificationData), teacher.getRole());
        //
        // notificationManager.createNotification(createNotificationRequest);
    }

    public void sendMoodleContentPendingEvaluation(MoodleContentInfo contentInfo)
            throws AddressException, NotFoundException, IOException, VException {
        List<Long> userIds = new ArrayList<>();
        Long teacherId = Long.parseLong(contentInfo.getTeacherId());
        Long studentId = Long.parseLong(contentInfo.getStudentId());
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacher = userInfos.get(teacherId);
        UserBasicInfo student = userInfos.get(studentId);
        if (student == null || teacher == null) {
            logger.error("sendMoodleContentPendingEvaluation",
                    "sendMoodlePendingEvaluationError : student or teacher does not exit");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("studentFullName", student.getFullName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("teacherEmail", teacher.getEmail());
        // bodyScopes.put("studentFullName",contentInfo.getStudentFullName() );
        // bodyScopes.put("teacherFullName", contentInfo.getTeacherFullName());
        // bodyScopes.put("teacherEmail", contentInfo.getTeacherFullName());
        bodyScopes.put("contentName", contentInfo.getContentTitle());
        bodyScopes.put("attemptedTime", contentInfo.getAttemptedTime());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        if (contentInfo.getAttemptedTime() != null && contentInfo.getAttemptedTime() > 0) {
            String dateTime = sdf.format(new Date(contentInfo.getAttemptedTime()));
            bodyScopes.put("attemptedTime", dateTime);
        }

        String appUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.android.app.link");
        String shortUrl = WebCommunicator.getShortUrl(appUrl);
        if (!StringUtils.isEmpty(shortUrl)) {
            appUrl = shortUrl;
        }
        bodyScopes.put("appUrl", appUrl);
        CommunicationType communicationType = CommunicationType.LMS_PENDING_EVALUATION;

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress("leena.arun@vedantu.com", "leena arun"));
        toAddress.add(new InternetAddress("manjunath.mohan@vedantu.com", "manjunath mohan"));
        toAddress.add(new InternetAddress("sundar.r@vedantu.com", "sundar"));

        EmailRequest request = new EmailRequest(toAddress, bodyScopes, bodyScopes, communicationType, Role.ADMIN);
        sendEmail(request);

    }

    public void sendFMATEndSessionMail(SessionInfo session, String sessionAction)
            throws AddressException, NotFoundException, IOException, VException {
        List<Long> userIds = new ArrayList<>();
        CommunicationType emailType = CommunicationType.FMAT_END_SESSION;
        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        Long teacherId = session.getTeacherId();
        Long studentId = session.getStudentIds().get(0);
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacher = userInfos.get(teacherId);
        UserBasicInfo student = userInfos.get(studentId);
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        bodyScopes.put("studentName", student.getFullName());
        bodyScopes.put("studentEmail", student.getEmail());
        bodyScopes.put("studentPhoneNumber", student.getContactNumber());

        bodyScopes.put("teacherName", teacher.getFullName());
        bodyScopes.put("teacherEmail", teacher.getEmail());
        bodyScopes.put("teacherPhoneNumber", teacher.getContactNumber());

        bodyScopes.put("sessionStartTime", sdf.format(new Date(session.getStartTime())));
        bodyScopes.put("sessionEndTime", sdf.format(new Date(session.getEndTime())));
        bodyScopes.put("title", session.getTitle());
        bodyScopes.put("subject", session.getSubject());
        bodyScopes.put("scheduledAt", sdf.format(new Date(session.getCreationTime())));
        bodyScopes.put("sessionSource", session.getSessionSource() == null ? "" : session.getSessionSource().name());
        bodyScopes.put("sessionModel", session.getModel() == null ? "" : session.getModel().name());
        bodyScopes.put("sessionRequestSource",
                session.getDeviceSource() == null ? "" : session.getDeviceSource().name());

        String lastUpdatedUserRole = "";
        if (StringUtils.isNotEmpty(session.getLastUpdatedBy())) {
            lastUpdatedUserRole = String.valueOf(session.getLastUpdatedBy()).equals(String.valueOf(student.getUserId()))
                    ? Role.STUDENT.name()
                    : Role.TEACHER.name();
        }
        switch (sessionAction) {
            case "SESSION_END":
                bodyScopes.put("sessionState", "ended");
                break;
            case "SESSION_CANCEL":
                bodyScopes.put("sessionState", "cancelled by " + lastUpdatedUserRole);
                break;
            case "SESSION_EXPIRE":
                String expireString = "expired.";

                List<UserSessionInfo> attendees = session.getAttendees();
                if (!CollectionUtils.isEmpty(attendees)) {
                    for (UserSessionInfo attendee : attendees) {
                        if (com.vedantu.session.pojo.SessionAttendee.SessionUserState.JOINED
                                .equals(attendee.getUserState())) {
                            try {
                                expireString += attendee.getRole().name() + " has joined.";
                            } catch (Exception ex) {
                                logger.error("sendFMATEndSessionMail", ex);
                            }
                        } else {
                            try {
                                expireString += attendee.getRole().name() + " did not joined.";
                            } catch (Exception ex) {
                                logger.error("sendFMATEndSessionMail", ex);
                            }
                        }
                    }
                }
                bodyScopes.put("sessionState", expireString);
                break;
            case "SESSION_SCHEDULE":
                bodyScopes.put("sessionState", "scheduled");
                break;
            default:
                logger.info("SessionUpdateTriggerTaskNoAction - Session action not defined " + sessionAction);
                return;
        }

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress(TRAIL_UPDATE_EMAIL_ID, "Trail updates"));
        toAddress.add(new InternetAddress("shyam.krishna@vedantu.com", "Shyam"));

        EmailRequest request = new EmailRequest(toAddress, bodyScopes, bodyScopes, emailType, Role.ADMIN);
        sendEmail(request);

    }

    public void communicationReminder() throws VException, IllegalArgumentException, IllegalAccessException {
        send1HrReminder();
        send10MinReminder();
    }

    private void send1HrReminder() throws VException, IllegalArgumentException, IllegalAccessException {
        long fromStartTime = System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_HOUR;
        long tillStartTime = fromStartTime + TASK_BUFFER_IN_MILLIES;
        sendReminderEmails(fromStartTime, tillStartTime);
    }

    private void send10MinReminder() throws VException, IllegalArgumentException, IllegalAccessException {
        long fromStartTime = System.currentTimeMillis() + (DateTimeUtils.MILLIS_PER_MINUTE * 10);
        long tillStartTime = fromStartTime + TASK_BUFFER_IN_MILLIES;
        sendReminderEmails(fromStartTime, tillStartTime);
    }

    private void sendReminderEmails(long fromStartTime, long tillStartTime)
            throws VException, IllegalArgumentException, IllegalAccessException {

        GetSessionsReq req = new GetSessionsReq();
        List<SessionState> states = new ArrayList<>();
        states.add(SessionState.SCHEDULED);
        req.setSessionStates(states);
        req.setBeforeStartTime(tillStartTime);
        req.setAfterStartTime(fromStartTime);
        req.setSortType(SessionSortType.START_TIME_ASC);
        req.setIncludeAttendees(true);

        Integer start = 0;
        Integer size = 20;

        while (true) {
            req.setStart(start);
            req.setSize(size);

            List<SessionInfo> sessions = sessionManager.getSessions(req, true);

            logger.info("sendReminderEmails session list : " + sessions);
            if (CollectionUtils.isNotEmpty(sessions)) {
                for (SessionInfo session : sessions) {
                    List<UserSessionInfo> attendees = session.getAttendees();
                    for (UserBasicInfo emailRecipientUser : attendees) {
                        if (emailRecipientUser != null) {
                            Map<String, Object> payload = new HashMap<>();
                            payload.put("sessionInfo", session);
                            payload.put("communicationType", CommunicationType.SESSION_REMINDER);
                            payload.put("emailRecipientUser", emailRecipientUser);
                            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_RELATED_EMAIL, payload);
                            asyncTaskFactory.executeTask(params);
                        }
                    }
                }
            }

            if (CollectionUtils.isEmpty(sessions) || sessions.size() < size) {
                break;
            }
            start += size;
        }
    }

    public void sendMoodleContentReminder(MoodleContentInfo contentInfo)
            throws AddressException, NotFoundException, IOException, VException {
        List<Long> userIds = new ArrayList<>();
        Long teacherId = Long.parseLong(contentInfo.getTeacherId());
        Long studentId = Long.parseLong(contentInfo.getStudentId());
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacher = userInfos.get(teacherId);
        UserBasicInfo student = userInfos.get(studentId);
        if (student == null || teacher == null) {
            logger.error("sendMoodleContentReminder",
                    "sendMoodleContentReminderError : student or teacher does not exit");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("studentName", student.getFirstName());
        bodyScopes.put("studentFullName", student.getFullName());
        bodyScopes.put("teacherName", teacher.getFirstName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("contentName", contentInfo.getContentTitle());
        bodyScopes.put("courseName", contentInfo.getCourseName());
        bodyScopes.put("engagementType", contentInfo.getEngagementType());
        bodyScopes.put("attemptNowLink", contentInfo.getStudentActionLink());
        bodyScopes.put("contentLink", contentInfo.getContentLink());
        bodyScopes.put("attemptedTime", contentInfo.getAttemptedTime());

        String actionLink = contentInfo.getStudentActionLink();
        if (StringUtils.isEmpty(actionLink)) {
            actionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("actionLink", actionLink);

        String teacherActionLink = contentInfo.getTeacherActionLink();
        if (StringUtils.isEmpty(teacherActionLink)) {
            teacherActionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("teacherActionLink", teacherActionLink);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        Long expiryTime = contentInfo.getExpiryTime();
        if (expiryTime != null && expiryTime > 0) {
            String dateTime = sdf.format(new Date(expiryTime));
            bodyScopes.put("expiryTime", dateTime);
        }

        Long creationTime = contentInfo.getCreationTime();
        if (creationTime != null && creationTime > 0) {
            bodyScopes.put("creationTime", sdf.format(new Date(creationTime)));
        }

        String appUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.android.app.link");
        String shortUrl = WebCommunicator.getShortUrl(appUrl);
        if (!StringUtils.isEmpty(shortUrl)) {
            appUrl = shortUrl;
        }
        bodyScopes.put("appUrl", appUrl);
        CommunicationType communicationType = null;
        switch (contentInfo.getContentType()) {
            case "TEST":
                communicationType = CommunicationType.LMS_TEST_REMINDER;
                break;
            case "ASSIGNMENT":
                communicationType = CommunicationType.LMS_ASSIGNMENT_REMINDER;
                break;
            default:
                return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, student.getRole());
        sendEmail(request);

        // Send SMS
        if (StringUtils.isNotEmpty(student.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(student.getContactNumber(), student.getPhoneCode(),
                    bodyScopes, communicationType, student.getRole());
            smsManager.sendSMS(textSMSRequest);
        }

        MoodleNotificationData notificationData = new MoodleNotificationData(contentInfo, communicationType.name());
        CreateNotificationRequest createNotificationRequest = new CreateNotificationRequest(student.getUserId(),
                NotificationType.LMS_NOTIFICATION, new Gson().toJson(notificationData), student.getRole());

        notificationManager.createNotification(createNotificationRequest);

    }

    public void sendMoodleEvaluationReminder(MoodleContentInfo contentInfo)
            throws AddressException, NotFoundException, IOException, VException {
        List<Long> userIds = new ArrayList<>();
        Long teacherId = Long.parseLong(contentInfo.getTeacherId());
        Long studentId = Long.parseLong(contentInfo.getStudentId());
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        UserBasicInfo teacher = userInfos.get(teacherId);
        UserBasicInfo student = userInfos.get(studentId);
        if (student == null || teacher == null) {
            logger.error("sendMoodleEvaluationReminder",
                    "sendMoodleEvaluationReminderError : student or teacher does not exit");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("studentName", student.getFirstName());
        bodyScopes.put("studentFullName", student.getFullName());
        bodyScopes.put("teacherName", teacher.getFirstName());
        bodyScopes.put("teacherFullName", teacher.getFullName());
        bodyScopes.put("contentName", contentInfo.getContentTitle());
        bodyScopes.put("courseName", contentInfo.getCourseName());
        bodyScopes.put("engagementType", contentInfo.getEngagementType());
        bodyScopes.put("attemptNowLink", contentInfo.getStudentActionLink());
        bodyScopes.put("contentLink", contentInfo.getContentLink());

        String actionLink = contentInfo.getStudentActionLink();
        if (StringUtils.isEmpty(actionLink)) {
            actionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("actionLink", actionLink);

        String teacherActionLink = contentInfo.getTeacherActionLink();
        if (StringUtils.isEmpty(teacherActionLink)) {
            teacherActionLink = contentInfo.getContentLink();
        }
        bodyScopes.put("teacherActionLink", teacherActionLink);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        Long expiryTime = contentInfo.getExpiryTime();
        if (expiryTime != null && expiryTime > 0) {
            String dateTime = sdf.format(new Date(expiryTime));
            bodyScopes.put("expiryTime", dateTime);
        }

        Long creationTime = contentInfo.getCreationTime();
        if (creationTime != null && creationTime > 0) {
            bodyScopes.put("creationTime", sdf.format(new Date(creationTime)));
        }

        Long attemptedTime = contentInfo.getCreationTime();
        if (attemptedTime != null && attemptedTime > 0) {
            bodyScopes.put("attemptedTime", sdf.format(new Date(attemptedTime)));
        }

        String appUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.android.app.link");
        String shortUrl = WebCommunicator.getShortUrl(appUrl);
        if (!StringUtils.isEmpty(shortUrl)) {
            appUrl = shortUrl;
        }
        bodyScopes.put("appUrl", appUrl);
        CommunicationType communicationType = null;
        switch (contentInfo.getContentType()) {
            case "TEST":
                communicationType = CommunicationType.LMS_TEST_EVAULATION_REMINDER;
                break;
            case "ASSIGNMENT":
                communicationType = CommunicationType.LMS_ASSIGNMENT_EVAULATION_REMINDER;
                break;
            default:
                return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(teacher.getEmail(), teacher.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, teacher.getRole());
        sendEmail(request);

        // Send SMS
        if (StringUtils.isNotEmpty(teacher.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(teacher.getContactNumber(), teacher.getPhoneCode(),
                    bodyScopes, communicationType, teacher.getRole());
            smsManager.sendSMS(textSMSRequest);
        }

        MoodleNotificationData notificationData = new MoodleNotificationData(contentInfo, communicationType.name());
        CreateNotificationRequest createNotificationRequest = new CreateNotificationRequest(teacher.getUserId(),
                NotificationType.LMS_NOTIFICATION, new Gson().toJson(notificationData), teacher.getRole());

        notificationManager.createNotification(createNotificationRequest);

    }

    public void sendOTFSessionFeedbackReminder(UserBasicInfo student, UserBasicInfo teacher, OTFSessionPojoUtils session)
            throws AddressException, NotFoundException, IOException, VException {

        if (student == null && teacher == null) {
            return;
        }
        logger.info("sendOTFSessionFeedbackReminder", student, teacher);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        Role role;
        if (student != null) {
            toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
            role = Role.STUDENT;
        } else {
            toList.add(new InternetAddress(teacher.getEmail(), teacher.getFullName()));
            role = Role.TEACHER;
        }

        CommunicationType emailType = CommunicationType.OTF_SESSION_FEEDBACK;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        if (student != null) {
            bodyScopes.put("studentName", student.getFirstName());
            bodyScopes.put("teacherName", teacher.getFirstName());
        }

        if (teacher != null) {
            bodyScopes.put("teacherName", teacher.getFirstName());
        }
        List<OTFCourseBasicInfo> courseInfos = session.getCourseInfos();
        if (ArrayUtils.isEmpty(courseInfos)) {
            logger.error("sendOTFSessionReminder : courseInfo not found for session", session);
            return;
        }

        bodyScopes.put("courseName", courseInfos.get(0).getTitle());
        bodyScopes.put("sessionTitle", session.getTitle());
        String feedbackLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + new ArrayList<>(session.getBatchIds()).get(0)
                + "/postsession/" + session.getId() + "?utm_source=otf_feedback&utm_medium=email&utm_campaign=1-to-few";

        bodyScopes.put("feedbackLink", feedbackLink);

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, role);
        sendEmail(request);
    }

    public void sendOTFDailySessionsEmail(List<OTFSessionPojoUtils> sessions, List<Conflict> conflicts)
            throws AddressException, NotFoundException, IOException, VException {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(DateTimeUtils.getISTDayStartTime(System.currentTimeMillis())));

        List<Map<String, String>> sessionMapList = new ArrayList<>();
        List<Map<String, String>> conflictMapList = new ArrayList<>();

        String sessionString = "";
        if (sessions == null || sessions.isEmpty()) {
            logger.info("sendOTFDailySessionsEmail - No OTF sessions for date " + dateTime);
            sessionString = "No OTF sessions today";
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            sessionString = "The following sessions are scheduled for today.";

            List<String> userIds = new ArrayList<>();
            for (OTFSessionPojoUtils session : sessions) {
                userIds.add(session.getPresenter());
            }
            Map<String, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMap(userIds, true);

            for (OTFSessionPojoUtils session : sessions) {
                Map<String, String> sessionParams = new HashMap<>();
                if (session.getCourseInfos() != null) {
                    sessionParams.put("courseId", session.getCourseInfos().get(0).getId().toString());
                    sessionParams.put("courseName", session.getCourseInfos().get(0).getTitle());
                }
                if (ArrayUtils.isNotEmpty(session.getBatchIds())) {
                    sessionParams.put("batchIds", Arrays.toString(session.getBatchIds().toArray()));
                }
                sessionParams.put("sessionName", session.getTitle());
                sessionParams.put("sessionId", session.getId());
                sessionParams.put("sessionStartTime", dateFormat.format(session.getStartTime()));
                sessionParams.put("sessionEndTime", dateFormat.format(session.getEndTime()));
                sessionParams.put("sessionURL", session.getSessionURL());
                sessionParams.put("sessionOrganiserToken", session.getOrganizerAccessToken());
                try {
                    sessionParams.put("sessionPresenter", userInfos.get(session.getPresenter()).getFullName());
                } catch (Exception ex) {
                    logger.error("sendOTFDailySessionsEmail", ex);
                }

                sessionMapList.add(sessionParams);
            }

            for (Conflict conflict : conflicts) {
                Map<String, String> conflictParams = new HashMap<String, String>();
                conflictParams.put("startTime", dateFormat.format(conflict.getStartTime()));
                conflictParams.put("endTime", dateFormat.format(conflict.getEndTime()));
                conflictParams.put("organiserId", conflict.getOrganiserId());

                sessionMapList.add(conflictParams);
            }
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("dateTime", dateTime);
        bodyScopes.put("sessionCount", sessions == null ? 0 : sessions.size());
        bodyScopes.put("sessionString", sessionString);
        bodyScopes.put("sessions", sessionMapList);
        bodyScopes.put("conflicts", conflictMapList);

        CommunicationType emailType = CommunicationType.OTF_DAILY_SESSIONS;

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress("atom@vedantu.com", "Atom"));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("shyam.krishna@vedantu.com", "Shyam Krishna"));
        bccList.add(new InternetAddress("gaurav.singhal@vedantu.com", "Gaurav Singhal"));
        bccList.add(new InternetAddress("otf-support@vedantu.com", "OTF"));
        bccList.add(new InternetAddress("nisharg.golash@vedantu.com", "Nisharg Golash"));
        bccList.add(new InternetAddress("ajith.reddy@vedantu.com", "Ajith Reddy"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.ADMIN);
        request.setBcc(bccList);
        sendEmail(request);

    }

    public void sendOTFSessionReminder(UserBasicInfo student, UserBasicInfo teacher, OTFSessionPojoUtils session)
            throws AddressException, NotFoundException, IOException, VException {
        if (student == null && teacher == null) {
            return;
        }
        ArrayList<InternetAddress> toList = new ArrayList<>();

        Role role;
        if (student != null) {
            toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
            role = Role.STUDENT;
        } else {
            toList.add(new InternetAddress(teacher.getEmail(), teacher.getFullName()));
            role = Role.TEACHER;
        }

        CommunicationType emailType = CommunicationType.OTF_SESSION_REMINDER;

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        if (student != null) {
            bodyScopes.put("studentName", student.getFirstName());
            bodyScopes.put("teacherName", teacher.getFirstName());
        }

        if (teacher != null) {
            bodyScopes.put("teacherName", teacher.getFirstName());
        }
        List<OTFCourseBasicInfo> courseInfos = session.getCourseInfos();
        if (ArrayUtils.isEmpty(courseInfos)) {
            logger.error("sendOTFSessionReminder : courseInfo not found for session", session);
            return;
        }

        bodyScopes.put("courseName", courseInfos.get(0).getTitle());
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(session.getStartTime()));
        String date = dateTime.substring(0, 7);
        String month = dateTime.substring(7, 16);
        String time = dateTime.substring(17, 25);
        bodyScopes.put("sessionStartTime", dateTime);
        bodyScopes.put("formattedTime", time);
        bodyScopes.put("formattedDate", date + month);
        SimpleDateFormat durationSDF = new SimpleDateFormat("HH:mm");
        durationSDF.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_GMT));
        bodyScopes.put("duration", durationSDF.format(new Date((session.getEndTime() - session.getStartTime()))));

        // feedback link
//        String feedbackLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + session.getBatchId()
//                + "/postsession/" + session.getId() + "?utm_source=otf_feedback&utm_medium=email&utm_campaign=1-to-few";
//        bodyScopes.put("feedbackLink", feedbackLink);
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, role);

        sendEmail(request);

        if (OTMSessionType.DEMO.equals(session.getOtmSessionType()) && student != null && StringUtils.isNotEmpty(student.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(student.getContactNumber(), student.getPhoneCode(),
                    bodyScopes, CommunicationType.OTF_SESSION_REMINDER, student.getRole());
            smsManager.sendSMS(textSMSRequest);
        }

    }

    public void sendOTFTeacherNotJoined(OTFSessionPojoUtils session) throws AddressException, NotFoundException, IOException {
        if (session == null) {
            logger.error("sendOTFTeacherNotJoinedError : Session is null");
            return;
        }
        CommunicationType communicationType = CommunicationType.OTF_TEACHER_NOT_JOINED;
        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("sessionId", session.getId());
        if (ArrayUtils.isNotEmpty(session.getBatchIds())) {
            bodyScopes.put("batchIds", StringUtils.join(session.getBatchIds().toArray(), ","));
        }
        bodyScopes.put("presenter", session.getPresenter());
        bodyScopes.put("sessionURL", session.getSessionURL());
        bodyScopes.put("organizerAccessToken", session.getOrganizerAccessToken());
        bodyScopes.put("sessionTitle", session.getTitle());

        UserBasicInfo teacher = userUtils.getUserBasicInfo(session.getPresenter(), true);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
            sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            bodyScopes.put("sessionStartTime", sdf.format(new Date(session.getStartTime())));
            sdf = new SimpleDateFormat("HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            bodyScopes.put("sessionShortStartTime", sdf.format(new Date(session.getStartTime())));
            // User presenter =
            // DAOFactory.INSTANCE.getUserDAO().getUserById(Long.parseLong(session.getPresenter()));
            bodyScopes.put("presenterName", teacher.getFirstName());
        } catch (Exception ex) {
            logger.error("sendOTFTeacherNotJoinedError : " + ex.getMessage(), ex);
        }

        // Send Email
        try {
            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(ConfigUtils.INSTANCE.getStringValue("otf.group.email"), "OTF"));
            EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, Role.ADMIN);

            sendEmail(request);

        } catch (Exception ex) {
            logger.error("sendOTFTeacherNotJoinedError - Error Sending email. ErrorMessage : " + ex.getMessage(), ex);
        }

        // Send SMS
        try {
            TextSMSRequest textSMSRequest = new TextSMSRequest(
                    ConfigUtils.INSTANCE.getStringValue("otf.alert.sms.number"), "91", bodyScopes, communicationType,
                    Role.ADMIN);
            smsManager.sendSMS(textSMSRequest);
        } catch (Exception ex) {
            logger.error("sendOTFTeacherNotJoinedError - Error Sending sms. ErrorMessage : " + ex.getMessage(), ex);
        }
    }

    public void sendUserDataEmail(HttpSessionData httpSessionData, String exportUserData, String fileName)
            throws IOException, AddressException, VException {
        sendUserDataEmail(httpSessionData, exportUserData, fileName, null);
    }

    public void sendUserDataEmail(HttpSessionData httpSessionData, String exportUserData, String fileName,
            String subject) throws IOException, AddressException, VException {

        EmailRequest email = new EmailRequest();
        email.setBody("Please find the attached doc");
        if (StringUtils.isEmpty(subject)) {
            subject = "Here is the system details in the attached document";
        }

        subject = (ConfigUtils.INSTANCE.getBooleanValue("email.SUBJECT.env.append")
                ? ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                : "") + subject;

        email.setSubject(subject);

        // email.setTo((InternetAddress.parse(getpropertyValue(EmailProperty.TO,
        // CommunicationType.SYSTEM_INFO, Role.ADMIN))));
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(httpSessionData.getEmail(), httpSessionData.getFirstName()));
        email.setTo(toList);
        email.setType(CommunicationType.SYSTEM_INFO);

        // email.setFrom((InternetAddress
        // .parse(getpropertyValue(EmailProperty.FROM,
        // CommunicationType.SYSTEM_INFO, user.getRole())))[0]);
        EmailAttachment attachment = new EmailAttachment();
        attachment.setApplication("application/csv");
        attachment.setAttachmentData(exportUserData);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(System.currentTimeMillis());

        attachment.setFileName(fileName + ":" + dateTime + ".csv");
        email.setAttachment(attachment);

        sendEmailViaRest(email);
    }

    public void sendAvailableTeacherExportEmail(String data, List<String> availableTeachers,
            HttpSessionData httpSessionData, long availableDuration, long bookedDuration, long unavailableDuration)
            throws AddressException, VException, IOException {
        CommunicationType emailType = CommunicationType.AVAILABLE_TEACHER_EXPORT;
        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("availableTeachers", Arrays.toString(availableTeachers.toArray()));
        // TODO : Calculate hours
        bodyScopes.put("totalAvailableHours", availableDuration / DateTimeUtils.MILLIS_PER_HOUR);
        bodyScopes.put("totalUnavailableHours", unavailableDuration / DateTimeUtils.MILLIS_PER_HOUR);
        bodyScopes.put("totalBookedHours", bookedDuration / DateTimeUtils.MILLIS_PER_HOUR);

        EmailAttachment attachment = new EmailAttachment();
        attachment.setApplication("application/csv");
        attachment.setAttachmentData(data);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(System.currentTimeMillis());
        attachment.setFileName("AvailableTeacherExport:" + dateTime + ".csv");

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(httpSessionData.getEmail(), httpSessionData.getFirstName()));

        if (httpSessionData != null) {
            toList.add(new InternetAddress(httpSessionData.getEmail(), httpSessionData.getFirstName()));
        }
        toList.add(new InternetAddress("shyam.krishna@vedantu.com", "Shyam"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.ADMIN);
        request.setAttachment(attachment);

        sendEmail(request);

    }

    public void sendISLEmail(SendISLEmailReq sendISLEmailReq)
            throws IOException, AddressException, NotFoundException, VException {

        CommunicationType emailType = CommunicationType.ISL_REGISTRATION;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String studentName = sendISLEmailReq.getStudentName();
        bodyScopes.put("studentName", studentName);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        if (StringUtils.isNotEmpty(sendISLEmailReq.getStudentEmail())) {
            toList.add(new InternetAddress(sendISLEmailReq.getStudentEmail(), studentName));
            EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.STUDENT);
            request.setIncludeHeaderFooter(false);
            sendEmail(request);
        } else if (StringUtils.isNotEmpty(sendISLEmailReq.getParentEmail())) {
            toList.add(new InternetAddress(sendISLEmailReq.getParentEmail(), ""));
            EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.PARENT);
            request.setIncludeHeaderFooter(false);
            sendEmail(request);
        }
        if (StringUtils.isNotEmpty(sendISLEmailReq.getStudentPhoneNo())) {
            if (StringUtils.isEmpty(sendISLEmailReq.getStudentPhoneCode())) {
                sendISLEmailReq.setStudentPhoneCode("91");
            }
            TextSMSRequest textSMSRequest = new TextSMSRequest(sendISLEmailReq.getStudentPhoneNo(),
                    sendISLEmailReq.getStudentPhoneCode(), bodyScopes, emailType, Role.STUDENT);
            smsManager.sendSMS(textSMSRequest);
        } else if (StringUtils.isNotEmpty(sendISLEmailReq.getParentPhoneNo())) {
            if (StringUtils.isEmpty(sendISLEmailReq.getParentPhoneCode())) {
                sendISLEmailReq.setParentPhoneCode("91");
            }
            TextSMSRequest textSMSRequest = new TextSMSRequest(sendISLEmailReq.getParentPhoneNo(),
                    sendISLEmailReq.getParentPhoneCode(), bodyScopes, emailType, Role.PARENT);
            smsManager.sendSMS(textSMSRequest);
        }
    }
    public boolean sendCampainRelatedEnrolmentEmail(Long userId, String entityId,
                                                 String campaign) throws AddressException, UnsupportedEncodingException {
        boolean isCampaign = false;
        switch (campaign) {
            case "corona":
                try {
                    sendCoronaEmail(userId, entityId);
                    isCampaign = true;
                } catch (VException ex) {
                    logger.error("BundleEnrollment ex:" + ex.getMessage() + " userId :" + userId + " entityId:" + entityId);
                }
                break;

        }
        return isCampaign;
    }

    public void sendBundleRelatedEnrolmentEmail(Long userId, String entityId,
            com.vedantu.session.pojo.EntityType entityType) throws AddressException, UnsupportedEncodingException {
        switch (entityType) {
            case BUNDLE:
                try {
                    sendBundleEnrolmentEmail(userId, entityId);
                } catch (VException ex) {
                    logger.error("BundleEnrollment ex:" + ex.getMessage() + " userId :" + userId + " entityId:" + entityId);
                }
                break;
            case BUNDLE_PACKAGE:
                try {
                    sendBundlePackageEnrolmentEmail(userId, entityId);
                } catch (VException ex) {
                    logger.error("BundlePackageEnrollment ex:" + ex.getMessage() + " userId :" + userId + " entityId:"
                            + entityId);
                }
                break;
            case BUNDLE_TRIAL:
                break;                
            default:
                logger.error("Invalid entity type for bundle enrolment communication : " + userId + " entityId:" + entityId
                        + " entityType:" + entityType);
                break;
        }
    }

    public void sendBundleEnrolmentEmail(Long userId, String entityId)
            throws VException, AddressException, UnsupportedEncodingException {
        logger.info("userId:" + userId + " entityId:" + entityId);
        GetBundlesReq req = new GetBundlesReq();
        List<BundleContentType> bundleContentTypes = new ArrayList<>();
//        bundleContentTypes.add(BundleContentType.TEST);
//        bundleContentTypes.add(BundleContentType.VIDEO);
//        bundleContentTypes.add(BundleContentType.WEBINAR);
        req.setReturnContentTypes(bundleContentTypes);
        BundleInfo bundleInfo = bundleManager.getBundle(entityId, req);
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);
        com.vedantu.platform.mongodbentities.board.Board board = boardManager.getBoardById(bundleInfo.getBoardId());

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user.getFirstName());
        bodyScopes.put("courseName", bundleInfo.getTitle());
        bodyScopes.put("courseId", bundleInfo.getId());
        bodyScopes.put( "hasVideo", bundleInfo.getNoOfVideos() != null && bundleInfo.getNoOfVideos() > 0);
        bodyScopes.put("hasCourse", ArrayUtils.isNotEmpty(bundleInfo.getBundleEntityInfos()));
        bodyScopes.put("hasRecordedCourses", bundleInfo.getIsRankBooster() == null ? false : bundleInfo.getIsRankBooster());
        bodyScopes.put( "hasTest", bundleInfo.getNoOfTests() != null && bundleInfo.getNoOfTests() > 0);
        bodyScopes.put("hasDoubt", bundleInfo.getUnLimitedDoubtsIncluded());
        String url = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("encodedUrl", URLEncoder.encode(url, "UTF-8"));
        bodyScopes.put("webBaseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("vWebBaseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        // check if email is empty
        if(StringUtils.isEmpty(user.getEmail())){
            return;
        }

        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.BUNDLE_ENROLMENT,
                Role.STUDENT);
        logger.info("Email:" + request.toString());
        sendEmail(request);
    }

    public void sendCoronaEmail(Long userId, String entityId)
            throws VException, AddressException, UnsupportedEncodingException {
        logger.info("userId:" + userId + " entityId:" + entityId);
        GetBundlesReq req = new GetBundlesReq();
        List<BundleContentType> bundleContentTypes = new ArrayList<>();

        req.setReturnContentTypes(bundleContentTypes);
        BundleInfo bundleInfo = bundleManager.getBundle(entityId, req);
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);
        com.vedantu.platform.mongodbentities.board.Board board = boardManager.getBoardById(bundleInfo.getBoardId());

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user.getFirstName());
        bodyScopes.put("courseName", bundleInfo.getTitle());
        bodyScopes.put("courseId", bundleInfo.getId());
        bodyScopes.put( "hasVideo", (bundleInfo.getNoOfVideos() != null && bundleInfo.getNoOfVideos() > 0) ? true : false );
        bodyScopes.put("hasCourse", ArrayUtils.isNotEmpty(bundleInfo.getBundleEntityInfos()));
        bodyScopes.put("hasRecordedCourses", bundleInfo.getIsRankBooster() == null ? false : bundleInfo.getIsRankBooster());
        bodyScopes.put( "hasTest", (bundleInfo.getNoOfTests() != null && bundleInfo.getNoOfTests() > 0) ? true : false );
        bodyScopes.put("hasDoubt", bundleInfo.getUnLimitedDoubtsIncluded());
        String url = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("encodedUrl", URLEncoder.encode(url, "UTF-8"));
        bodyScopes.put("webBaseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("vWebBaseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        // check if email is empty
        if(StringUtils.isEmpty(user.getEmail())){
            return;
        }

        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.CORONA_ENROLMENT,
                Role.STUDENT);
        logger.info("Email:" + request.toString());
        sendEmail(request);
    }

    public void sendBundlePackageEnrolmentEmail(Long userId, String entityId)
            throws VException, AddressException, UnsupportedEncodingException {
        logger.info("userId:" + userId + " entityId:" + entityId);
        BundlePackageDetailsInfo bundlePackageInfo = bundlePackageManager.getBundlePackageDetails(entityId);
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user.getFirstName());
        bodyScopes.put("packageName", bundlePackageInfo.getTitle());
        bodyScopes.put("webBaseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("vWebBaseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        List<PackageCourseInfo> packageCourses = new ArrayList<>();
        for (BundleInfo bundleInfo : bundlePackageInfo.getEntities()) {
            com.vedantu.platform.mongodbentities.board.Board board = boardManager.getBoardById(bundleInfo.getBoardId());

            PackageCourseInfo packageCourseInfo = new PackageCourseInfo();
            packageCourseInfo.setCourseName(bundleInfo.getTitle());
            packageCourseInfo.setCourseSubject(board.getName());
            packageCourseInfo.setCourseId(bundleInfo.getId());
            String url = ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/tracks/courseinfo/"
                    + bundleInfo.getId();
            packageCourseInfo.setEncodedUrl(URLEncoder.encode(url, "UTF-8"));
            packageCourses.add(packageCourseInfo);
        }
        bodyScopes.put("packageCourses", packageCourses);

        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                CommunicationType.BUNDLE_PACKAGE_ENROLMENT, Role.STUDENT);
        logger.info("Email:" + request.toString());
        sendEmail(request);
    }

    public void sendBundleRelatedEnrolmentPassEmail(Long userId, String entityId,
            com.vedantu.session.pojo.EntityType entityType, RedeemCouponInfo redeemCouponInfo)
            throws AddressException, UnsupportedEncodingException {
        switch (entityType) {
            case BUNDLE:
                try {
                    sendBundleEnrolmentViaPassEmail(userId, entityId, redeemCouponInfo);
                } catch (VException ex) {
                    logger.error("BundleEnrollment ex:" + ex.getMessage() + " userId :" + userId + " entityId:" + entityId);
                }
                break;
            case BUNDLE_PACKAGE:
                try {
                    sendBundlePackageEnrolmentViaPassEmail(userId, entityId, redeemCouponInfo);
                } catch (VException ex) {
                    logger.error("BundlePackageEnrollment ex:" + ex.getMessage() + " userId :" + userId + " entityId:"
                            + entityId);
                }
                break;
            case BUNDLE_TRIAL:
                break;                        
            default:
                logger.error("Invalid entity type for bundle enrolment communication : " + userId + " entityId:" + entityId
                        + " entityType:" + entityType);
                break;
        }
    }

    public void sendBundleEnrolmentViaPassEmail(Long userId, String entityId, RedeemCouponInfo redeemCouponInfo)
            throws VException, AddressException, UnsupportedEncodingException {
        logger.info("userId:" + userId + " entityId:" + entityId);
        BundleInfo bundleInfo = bundleManager.getBundle(entityId, null);
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);
        // com.vedantu.platform.mongodbentities.board.Board board =
        // boardManager.getBoardById(bundleInfo.getBoardId());
        Long currentTime = System.currentTimeMillis();
        Long passValidityInMillis = redeemCouponInfo.getPassExpirationTime() - currentTime;

        double noOfDaysDouble = Math.ceil(passValidityInMillis.doubleValue() / (DateTimeUtils.MILLIS_PER_DAY));
        int noOfDays = (int) noOfDaysDouble;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user.getFirstName());
        bodyScopes.put("courseName", bundleInfo.getTitle());
        // bodyScopes.put("courseSubject", board.getName());
        bodyScopes.put("courseId", bundleInfo.getId());
        bodyScopes.put("webBaseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("vWebBaseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        String url = ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/tracks/courseinfo/" + bundleInfo.getId();
        bodyScopes.put("encodedUrl", URLEncoder.encode(url, "UTF-8"));
        bodyScopes.put("validity", noOfDays);
        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.BUNDLE_ENROLMENT_PASS,
                Role.STUDENT);
        logger.info("Email:" + request.toString());
        sendEmail(request);
    }

    public void sendBundlePackageEnrolmentViaPassEmail(Long userId, String entityId, RedeemCouponInfo redeemCouponInfo)
            throws VException, AddressException, UnsupportedEncodingException {
        logger.info("userId:" + userId + " entityId:" + entityId);
        BundlePackageDetailsInfo bundlePackageInfo = bundlePackageManager.getBundlePackageDetails(entityId);
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);

        Long currentTime = System.currentTimeMillis();
        Long passValidityInMillis = redeemCouponInfo.getPassExpirationTime() - currentTime;

        double noOfDaysDouble = Math.ceil(passValidityInMillis.doubleValue() / (DateTimeUtils.MILLIS_PER_DAY));
        int noOfDays = (int) noOfDaysDouble;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user.getFirstName());
        bodyScopes.put("packageName", bundlePackageInfo.getTitle());
        bodyScopes.put("webBaseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        bodyScopes.put("vWebBaseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        bodyScopes.put("validity", noOfDays);

        List<PackageCourseInfo> packageCourses = new ArrayList<>();
        for (BundleInfo bundleInfo : bundlePackageInfo.getEntities()) {
            com.vedantu.platform.mongodbentities.board.Board board = boardManager.getBoardById(bundleInfo.getBoardId());

            PackageCourseInfo packageCourseInfo = new PackageCourseInfo();
            packageCourseInfo.setCourseName(bundleInfo.getTitle());
            packageCourseInfo.setCourseSubject(board.getName());
            packageCourseInfo.setCourseId(bundleInfo.getId());
            String url = ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/tracks/courseinfo/"
                    + bundleInfo.getId();
            packageCourseInfo.setEncodedUrl(URLEncoder.encode(url, "UTF-8"));
            packageCourses.add(packageCourseInfo);
        }
        bodyScopes.put("packageCourses", packageCourses);

        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                CommunicationType.BUNDLE_PACKAGE_ENROLMENT_PASS, Role.STUDENT);
        logger.info("Email:" + request.toString());
        sendEmail(request);
    }

    public void sendCourseRegistrationEmail(Long userId, String courseId, Integer otfRegistrationFee,
            CommunicationType communicationType) throws AddressException, VException, IOException {
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + userId);
        }

        String courseTitle = null;
        if (CommunicationType.OTF_REGISTRATION.equals(communicationType)) {
            CourseInfo courseInfo = otfManager.getCourse(courseId);
            courseTitle = courseInfo.getTitle();
        } else if (CommunicationType.OTO_REGISTRATION.equals(communicationType)) {

            StructuredCourseInfo structuredCourseInfo = getStructuredCourse(courseId);
            courseTitle = structuredCourseInfo.getTitle();
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user.getFullName());
        bodyScopes.put("studentId", userId);
        bodyScopes.put("contactNumber", user.getContactNumber());
        bodyScopes.put("phoneCode", user.getPhoneCode());
        bodyScopes.put("email", user.getEmail());
        bodyScopes.put("courseTitle", courseTitle);
        bodyScopes.put("courseId", courseId);
        if (otfRegistrationFee != null) {
            bodyScopes.put("amount", otfRegistrationFee / 100);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date());
        bodyScopes.put("dateTime", dateTime);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, user.getRole());
        sendEmail(request);

        toList.clear();
        toList.add(new InternetAddress(ACADEMICS_EMAIL_ID));
        request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, Role.ADMIN);
        sendEmail(request);

    }

    public StructuredCourseInfo getStructuredCourse(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/courseplan/structuredCourse/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        StructuredCourseInfo structuredCourseInfo = new Gson().fromJson(jsonString, StructuredCourseInfo.class);
        return structuredCourseInfo;
    }

    public void sendISLReportEmail(ISLReport report) throws AddressException, VException, IOException {

        String city = report.getCity();
        if (isHyderabad(city)) {
            return;
        }

        CommunicationType communicationType = CommunicationType.ISL_REPORT;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", report.getStudentName());

        try {
            addISLReportParams(report, bodyScopes);
        } catch (Exception ex) {
            logger.info(ex);
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(report.getParentEmail()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, Role.STUDENT);
        sendEmail(request);
    }

    public void sendOTFManualEnrollmentEmail(EnrollmentPojo enrollmentInfo)
            throws UnsupportedEncodingException, VException {
        CommunicationType communicationType = CommunicationType.ENROLLMENT_ADD_MANUAL;
        UserBasicInfo userBasicInfo = enrollmentInfo.getUser();
        CourseBasicInfo courseInfo = enrollmentInfo.getCourse();
        if (userBasicInfo == null || courseInfo == null) {
            logger.error("sendOTFEnrollmentEmail - userBasicInfo pr course basic info not found");
            return;
        }
        BatchBasicInfo batchBasicInfo = enrollmentInfo.getBatch();
        if (userBasicInfo == null || batchBasicInfo == null) {
            logger.error("sendOTFEnrollmentEmail - student info or batch basic info not found");
            return;
        }
        List<UserBasicInfo> teachers = batchBasicInfo.getTeachers();
        if (teachers == null || teachers.isEmpty()) {
            logger.error("sendOTFEnrollmentEmail - teacher info not found");
            return;
        }
        String teacherNameList = "";
        for (int i = 0; i < teachers.size(); i++) {
            teacherNameList += teachers.get(i).getFullName();
            if (i < (teachers.size() - 1)) {
                teacherNameList += ", ";
            }
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("teachersNameList", teacherNameList);
        bodyScopes.put("courseName", courseInfo.getTitle());

        // firstSessionTime - {day}, {date} at {time}
        // List<AgendaPojo> agendaList = batchBasicInfo.getAgenda();
        //
        // Long firstSessionTime = Long.MAX_VALUE;
        // if (agendaList != null) {
        // for (AgendaPojo otfAgenda : agendaList) {
        // SessionPojo session = otfAgenda.getSessionPOJO();
        // if (session != null && session.getStartTime() < firstSessionTime) {
        // firstSessionTime = session.getStartTime();
        // }
        // }
        // }
        //
        // if (firstSessionTime != Long.MAX_VALUE) {
        // SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm
        // a");
        // sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        // String dateTime = sdf.format(new Date(firstSessionTime));
        // bodyScopes.put("firstSessionTime", dateTime);
        // }
        if (ArrayUtils.isNotEmpty(courseInfo.getTargets())) {
            List<String> targets = new ArrayList<>(courseInfo.getTargets());
            String batchLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + targets.get(0)
                    + "/course/" + courseInfo.getId() + "/batch-details/list?enrolled=true&batchId="
                    + batchBasicInfo.getBatchId();
            bodyScopes.put("batchLink", batchLink);
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        sendEmail(request);
    }

    public void sendOTFTrialPaymentReminder(EnrollmentPojo enrollmentInfo, Long dueTime,
            CommunicationType communicationType) throws UnsupportedEncodingException, VException {
        UserBasicInfo userBasicInfo = enrollmentInfo.getUser();
        CourseBasicInfo courseInfo = otfManager.getCourse(enrollmentInfo.getCourseId());
        if (userBasicInfo == null || courseInfo == null) {
            logger.error("sendOTFTrialPaymentReminder - userBasicInfo or course basic info not found");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("courseName", courseInfo.getTitle());

        String targetString = null;
        for (String target : courseInfo.getTargets()) {
            targetString = target;
            break;
        }

        String batchLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + targetString + "/course/"
                + courseInfo.getId() + "/batch-details/list?enrolled=true&batchId=" + enrollmentInfo.getBatchId();
        String shortUrl = WebCommunicator.getShortUrl(batchLink);
        if (!StringUtils.isEmpty(shortUrl)) {
            batchLink = shortUrl;
        }
        bodyScopes.put("batchLink", batchLink);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(dueTime));
        bodyScopes.put("dueDate", dateTime);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("nisharg.g@vedantu.com", "Nisharg"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        request.setBcc(bccList);
        sendEmail(request);

        if (StringUtils.isNotEmpty(userBasicInfo.getContactNumber())) {
            TextSMSRequest smsRequest = new TextSMSRequest(userBasicInfo.getContactNumber(), userBasicInfo.getPhoneCode(),
                    bodyScopes, communicationType, Role.STUDENT);
            smsManager.sendSMS(smsRequest);
        }

    }

    public void sendCoursePlanEmails(CoursePlanInfo coursePlanInfo, CommunicationType type)
            throws NotFoundException, UnsupportedEncodingException, VException, AddressException {
        List<Long> userIds = new ArrayList<>();
        Long teacherId = coursePlanInfo.getTeacherId();
        Long studentId = coursePlanInfo.getStudentId();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, User> userInfos = userUtils.getUserInfosMap(userIds, true);
        User teacherInfo = userInfos.get(teacherId);
        User studentInfo = userInfos.get(studentId);
        Map<String, Object> bodyScopes = new HashMap<>();

        if (teacherInfo == null || studentInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "teacherBasicInfo or studentBasicInfo not found");
        }

        String teacherName = teacherInfo.getFullName();
        String studentName = studentInfo.getFullName();
        if (teacherName != null && teacherName.length() > 1) {
            teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
        }
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }
        bodyScopes.put("studentName", studentName);
        bodyScopes.put("name", studentName);
        bodyScopes.put("teacherName", teacherName);
        bodyScopes.put("urlBase", ConfigUtils.INSTANCE.getStringValue("url.base"));
        bodyScopes.put("coursePlanId", coursePlanInfo.getId());
        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/courseplan/"
                + coursePlanInfo.getId();
        bodyScopes.put("subscriptionLink", subscriptionLink);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(studentInfo.getEmail(), studentInfo.getFullName()));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("nisharg.g@vedantu.com", "Nisharg"));

        List<InternetAddress> adminEmails = new ArrayList<>();

        if (coursePlanInfo.getAdminData() != null && !coursePlanInfo.getAdminData().isEmpty()) {
            for (CoursePlanAdminData coursePlanAdminData : coursePlanInfo.getAdminData()) {
                if (coursePlanAdminData != null && StringUtils.isNotEmpty(coursePlanAdminData.getEmail())) {
                    adminEmails.add(new InternetAddress(coursePlanAdminData.getEmail()));
                }
            }
        }

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, type, studentInfo.getRole());
        request.setBcc(bccList);
        if (!adminEmails.isEmpty()) {
            request.setCc(adminEmails);
            request.setReplyTo(adminEmails);
        }
        sendEmail(request);

        if (StringUtils.isNotEmpty(studentInfo.getContactNumber())) {
            TextSMSRequest smsRequest = new TextSMSRequest(studentInfo.getContactNumber(), studentInfo.getPhoneCode(),
                    bodyScopes, type, Role.STUDENT);
            smsManager.sendSMS(smsRequest);
        }

        if (StringUtils.isNotEmpty(teacherInfo.getContactNumber())) {
            TextSMSRequest teacherSMS = new TextSMSRequest(teacherInfo.getContactNumber(), teacherInfo.getPhoneCode(),
                    bodyScopes, type, Role.TEACHER);
            smsManager.sendSMS(teacherSMS);
        }

        toList.clear();
        toList.add(new InternetAddress(teacherInfo.getEmail(), teacherInfo.getFullName()));
        EmailRequest requestTeacher = new EmailRequest(toList, bodyScopes, bodyScopes, type, teacherInfo.getRole());
        sendEmail(requestTeacher);

        sendCommunicationToParent(studentInfo, bodyScopes, bodyScopes, type, adminEmails, adminEmails);
    }

    public HashMap<String, Object> getBasicOTOSessionInfoMap(SessionInfo sessionInfo) {
        HashMap<String, Object> bodyScopes = new HashMap<>();
        if (sessionInfo == null) {
            return null;
        }
        Long teacherId = sessionInfo.getTeacherId();
        UserBasicInfo teacherInfo = userUtils.getUserBasicInfo(teacherId, true);

        bodyScopes.put("sessionTitle", sessionInfo.getTitle());
        sdfDateDay.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfDateDay.format(new Date(sessionInfo.getStartTime()));
        bodyScopes.put("sessionDate", timestr);
        sdfTime.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        timestr = sdfTime.format(new Date(sessionInfo.getStartTime()));
        bodyScopes.put("sessionTime", timestr);

        Long duration = sessionInfo.getEndTime() - sessionInfo.getStartTime();
        duration = (Long) (duration / DateTimeUtils.MILLIS_PER_MINUTE);
        bodyScopes.put("teacherName", teacherInfo.getFullName());
        bodyScopes.put("teacherExt", teacherInfo.getExtensionNumber());
        bodyScopes.put("teacherProfileUrl", FOS_ENDPOINT + "/v/teacher/" + teacherId);
        bodyScopes.put("sessionDuration", duration);
        bodyScopes.put("mySessionsLink", FOS_ENDPOINT + "/v/mysessions");

        return bodyScopes;
    }

    public void sendCoursePlanSessionEmails(SessionInfo sessionInfo, CoursePlanInfo coursePlanInfo,
            SessionState sessionState, Long callingUserId, Role callingUserRole)
            throws NotFoundException, UnsupportedEncodingException, VException, AddressException {
        List<Long> userIds = new ArrayList<>();
        Long teacherId = sessionInfo.getTeacherId();
        List<Long> studentId = sessionInfo.getStudentIds();
        userIds.add(teacherId);
        userIds.addAll(studentId);
        Map<Long, User> userInfos = userUtils.getUserInfosMap(userIds, true);
        User teacherInfo = userInfos.get(teacherId);
        User studentInfo = userInfos.get(studentId.get(0));
        if (teacherInfo == null || studentInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "teacherBasicInfo or studentBasicInfo not found");
        }
        Map<String, Object> bodyScopes = getBasicOTOSessionInfoMap(sessionInfo);
        if (bodyScopes == null) {
            logger.error("there was a error sending cancelation email for session:" + sessionInfo);
            return;
        }

        bodyScopes.put("reason", sessionInfo.getRemark());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        TextSMSRequest textSMSRequest = null;
        EmailRequest request = null;
        switch (callingUserRole) {
            case TEACHER:
                // Need to send it to student
                bodyScopes.put("userName", studentInfo.getFullName());
                toList.add(new InternetAddress(studentInfo.getEmail(), studentInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes,
                        CommunicationType.OTM_OTO_SESSION_CANCELLED, Role.STUDENT);
                sendEmail(request);
                if (StringUtils.isNotEmpty(studentInfo.getContactNumber())) {
                    textSMSRequest = new TextSMSRequest(studentInfo.getContactNumber(),
                            studentInfo.getPhoneCode(), bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED, studentInfo.getRole());
                    smsManager.sendSMS(textSMSRequest);
                }
                sendCommunicationToParent(studentInfo, bodyScopes, bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED);
                break;
            case STUDENT:
                // Need to send it to teacher
                bodyScopes.put("userName", teacherInfo.getFullName());
                toList.add(new InternetAddress(teacherInfo.getEmail(), teacherInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes,
                        CommunicationType.OTM_OTO_SESSION_CANCELLED, Role.TEACHER);
                sendEmail(request);
                if (StringUtils.isNotEmpty(teacherInfo.getContactNumber())) {
                    textSMSRequest = new TextSMSRequest(teacherInfo.getContactNumber(),
                            teacherInfo.getPhoneCode(), bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED, teacherInfo.getRole());
                    smsManager.sendSMS(textSMSRequest);
                }
                break;
            default:
                // Send it to teacher
                bodyScopes.put("userName", teacherInfo.getFullName());
                toList.add(new InternetAddress(teacherInfo.getEmail(), teacherInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes,
                        CommunicationType.OTM_OTO_SESSION_CANCELLED, Role.TEACHER);
                sendEmail(request);
                if (StringUtils.isNotEmpty(teacherInfo.getContactNumber())) {
                    textSMSRequest = new TextSMSRequest(teacherInfo.getContactNumber(),
                            teacherInfo.getPhoneCode(), bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED, teacherInfo.getRole());
                    smsManager.sendSMS(textSMSRequest);
                }

                // Send it to student
                bodyScopes.put("userName", studentInfo.getFullName());
                toList = new ArrayList<>();
                toList.add(new InternetAddress(studentInfo.getEmail(), studentInfo.getFullName()));
                request = new EmailRequest(toList, bodyScopes, bodyScopes,
                        CommunicationType.OTM_OTO_SESSION_CANCELLED, Role.STUDENT);
                sendEmail(request);
                if (StringUtils.isNotEmpty(studentInfo.getContactNumber())) {
                    textSMSRequest = new TextSMSRequest(studentInfo.getContactNumber(),
                            studentInfo.getPhoneCode(), bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED, studentInfo.getRole());
                    smsManager.sendSMS(textSMSRequest);
                }
                sendCommunicationToParent(studentInfo, bodyScopes, bodyScopes, CommunicationType.OTM_OTO_SESSION_CANCELLED);
                break;
        }
    }

    private boolean isHyderabad(String city) {
        for (String variation : hyderabadVariations) {
            if (variation.equalsIgnoreCase(city)) {
                logger.info("City is hyderabad: " + city);
                return true;
            }
        }
        return false;
    }

    private void addISLReportParams(ISLReport report, HashMap<String, Object> bodyScopes) {
        logger.info("Inside addISLReportParams");
        Integer rank = report.getTestMarksData().getRank();
        List<ISLReportDetail> details = new ArrayList<>();

        // TODO: Add brochure link
        switch (report.getParentPhoneCode()) {
            // India
            case "91":
            case "65":
            case "44":
            case "61":
            // case "92":
            case "1":
            case "6":

                switch (report.getGrade()) {
                    case "6":
                    case "7":
                        if (rank >= 1 && rank <= 100) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLA43");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 7,500 OFF</b> on Vedantu's <b>Get Ahead Courses</b>.");
                            detail1.setLinkCbse("http://courses.vedantu.com/gacourse-southindia-cbse6-9/");
                            detail1.setLinkIcse("http://courses.vedantu.com/gacourse-southindia-icse6-9/");
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLA83");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 5,000 OFF</b> on Vedantu's <b>Mental Ability Course</b>.");
                            detail2.setLinkCbse("https://www.vedantu.com/");
                            detail2.setLinkIcse("https://www.vedantu.com/");
                            details.add(detail2);

                        } else if (rank >= 101 && rank <= 500) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLA131");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 5,000 OFF</b> on Vedantu's <b>Get Ahead Courses</b>.");
                            detail1.setLinkCbse("http://courses.vedantu.com/gacourse-southindia-cbse6-9/");
                            detail1.setLinkIcse("http://courses.vedantu.com/gacourse-southindia-icse6-9/");
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLA757");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 2,500 OFF</b> on Vedantu's <b>Mental Ability Course</b> OR <b>Regular Tuition Courses</b>.<br/>Note: Voucher is applicable for single use on any ONE course.");
                            detail2.setLinkCbse("https://www.vedantu.com/");
                            detail2.setLinkIcse("https://www.vedantu.com/");
                            details.add(detail2);

                        } else {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLA1021");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 2,500 OFF</b> on one of the following Vedantu Courses - <b>Regular Tuition Courses</b> OR <b>Get Ahead Course</b> OR <b>Mental Ability Course</b>. <br/>"
                                    + "Note: Voucher is applicable for single use on any ONE course.");
                            detail1.setLinkCbse("https://www.vedantu.com/");
                            detail1.setLinkIcse("https://www.vedantu.com/");
                            details.add(detail1);
                        }
                        bodyScopes.put("brochureLink",
                                "https://storage.googleapis.com/vedantu-fos-qa-public-media/feb/Grade_7-8_In.pdf");
                        break;

                    case "8":
                    case "9":

                        if (rank >= 1 && rank <= 100) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLK43");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 7,500 OFF</b> on Vedantu's <b>Regular Tuition Courses of Maths/Science</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-9-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-9-combo/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-10-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-10-combo/");
                            }
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLK83");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 5,000 OFF</b> on Vedantu's <b>Mental Ability Course</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            }
                            details.add(detail2);

                        } else if (rank >= 101 && rank <= 500) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLK131");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 5,000 OFF</b> on Vedantu's <b>Regular Tuition Courses of Maths/Science</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-9-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-9-combo/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-10-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-10-combo/");
                            }
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLK759");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 2,500 OFF</b> on Vedantu's <b>Mental Ability Course</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            }
                            details.add(detail2);

                        } else {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLK1021");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of INR 2,500 OFF</b> on one of the following Vedantu Courses - <b>Regular Tuition Courses of Maths/Science</b> OR <b>Mental Ability Course</b>.<br/>"
                                    + "Note: Voucher is applicable for single use on any ONE course.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            }
                            details.add(detail1);
                        }
                        if ("8".equals(report.getGrade())) {
                            bodyScopes.put("brochureLink",
                                    "https://storage.googleapis.com/vedantu-fos-qa-public-media/feb/Grade_9_In.pdf");
                        } else if ("9".equals(report.getGrade())) {
                            bodyScopes.put("brochureLink",
                                    "https://storage.googleapis.com/vedantu-fos-qa-public-media/feb/Grade_10_In.pdf");
                        }

                }

                break;

            // Middle East
            default:

                switch (report.getGrade()) {
                    case "6":
                    case "7":
                        if (rank >= 1 && rank <= 100) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLA47");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 500 OFF</b> on Vedantu's <b>Get Ahead Courses</b>.");
                            detail1.setLinkCbse("http://courses.vedantu.com/gacourse-me-cbse6-9/");
                            detail1.setLinkIcse("http://courses.vedantu.com/gacourse-me-icse6-9/");
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLA89");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 300 OFF</b> on Vedantu's <b>Mental Ability Course</b>.");
                            detail2.setLinkCbse("https://www.vedantu.com/");
                            detail2.setLinkIcse("https://www.vedantu.com/");
                            details.add(detail2);

                        } else if (rank >= 101 && rank <= 500) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLA151");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 300 OFF</b> on Vedantu's <b>Get Ahead Courses</b>.");
                            detail1.setLinkCbse("http://courses.vedantu.com/gacourse-me-cbse6-9/");
                            detail1.setLinkIcse("http://courses.vedantu.com/gacourse-me-icse6-9/");
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLA769");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 150 OFF</b> on Vedantu's <b>Mental Ability Course</b> OR <b>Regular Tuition Courses</b>.<br/>"
                                    + "Note: Voucher is applicable for single use on any ONE course.");
                            detail2.setLinkCbse("https://www.vedantu.com/");
                            detail2.setLinkIcse("https://www.vedantu.com/");
                            details.add(detail2);

                        } else {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLA1039");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 150 OFF</b> on one of the following Vedantu Courses - <b>Regular Tuition Courses</b> OR <b>Get Ahead Course</b> OR <b>Mental Ability Course</b>.<br/>"
                                    + "Note: Voucher is applicable for single use on any ONE course.");
                            detail1.setLinkCbse("https://www.vedantu.com/");
                            detail1.setLinkIcse("https://www.vedantu.com/");
                            details.add(detail1);
                        }
                        bodyScopes.put("brochureLink",
                                "https://storage.googleapis.com/vedantu-fos-qa-public-media/feb/Grade_7-8_ME.pdf");
                        break;

                    case "8":
                    case "9":

                        if (rank >= 1 && rank <= 100) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLK47");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 500 OFF</b> on Vedantu's <b>Regular Tuition Courses of Maths/Science</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-9-me-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-9-me-combo/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-10-me-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-10-me-combo/");
                            }
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLK89");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 300 OFF</b> on Vedantu's <b>Mental Ability Course</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            }
                            details.add(detail2);

                        } else if (rank >= 101 && rank <= 500) {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLK151");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 300 OFF</b> on Vedantu's <b>Regular Tuition Courses of Maths/Science</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-9-me-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-9-me-combo/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("http://courses.vedantu.com/otf-cbse-10-me-combo/");
                                detail1.setLinkIcse("http://courses.vedantu.com/otf-icse-10-me-combo/");
                            }
                            details.add(detail1);

                            ISLReportDetail detail2 = new ISLReportDetail();
                            detail2.setVoucherCode("ISLK769");
                            detail2.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 150 OFF</b> on Vedantu's <b>Mental Ability Course</b>.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            }
                            details.add(detail2);

                        } else {
                            ISLReportDetail detail1 = new ISLReportDetail();
                            detail1.setVoucherCode("ISLK1039");
                            detail1.setOffer(
                                    "Use the below code to get a <b>Scholarship of AED 150 OFF</b> on one of the following Vedantu Courses - <b>Regular Tuition Courses of Maths/Science</b> OR <b>Mental Ability Course</b>.<br/>"
                                    + "Note: Voucher is applicable for single use on any ONE course.");
                            if ("8".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            } else if ("9".equals(report.getGrade())) {
                                detail1.setLinkCbse("https://www.vedantu.com/");
                                detail1.setLinkIcse("https://www.vedantu.com/");
                            }
                            details.add(detail1);
                        }
                        if ("8".equals(report.getGrade())) {
                            bodyScopes.put("brochureLink",
                                    "https://storage.googleapis.com/vedantu-fos-qa-public-media/feb/Grade_9_ME.pdf");
                        } else if ("9".equals(report.getGrade())) {
                            bodyScopes.put("brochureLink",
                                    "https://storage.googleapis.com/vedantu-fos-qa-public-media/feb/Grade_10_ME.pdf");
                        }

                }

        }
        bodyScopes.put("details", details);
    }

    public void sendTrialCoursePlanDueEmail(CommunicationType communicationType, CoursePlanInfo coursePlanInfo,
            Long dueTime) throws UnsupportedEncodingException, VException {
        UserBasicInfo userBasicInfo = coursePlanInfo.getStudent();
        UserBasicInfo teacherBasicInfo = coursePlanInfo.getTeacher();
        if (userBasicInfo == null || teacherBasicInfo == null) {
            logger.error("sendTrialCoursePlanDueEmail - userBasicInfo or teacherBasicInfo not found");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("teacherName", teacherBasicInfo.getFullName());
        bodyScopes.put("courseName", coursePlanInfo.getTitle());

        String courseLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/courseplan/" + coursePlanInfo.getId();
        String shortUrl = WebCommunicator.getShortUrl(courseLink);
        if (!StringUtils.isEmpty(shortUrl)) {
            courseLink = shortUrl;
        }
        bodyScopes.put("courseLink", courseLink);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(dueTime));
        bodyScopes.put("dueDate", dateTime);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("nisharg.g@vedantu.com", "Nisharg"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        request.setBcc(bccList);
        sendEmail(request);

        if (StringUtils.isNotEmpty(userBasicInfo.getContactNumber())) {
            TextSMSRequest smsRequest = new TextSMSRequest(userBasicInfo.getContactNumber(), userBasicInfo.getPhoneCode(),
                    bodyScopes, communicationType, Role.STUDENT);
            smsManager.sendSMS(smsRequest);
        }
    }

    public void sendOTFTrialPaymentBlocked(EnrollmentPojo enrollmentInfo)
            throws UnsupportedEncodingException, VException {
        UserBasicInfo userBasicInfo = enrollmentInfo.getUser();
        CourseBasicInfo courseInfo = otfManager.getCourse(enrollmentInfo.getCourseId());
        if (userBasicInfo == null || courseInfo == null) {
            logger.error("sendOTFTrialPaymentReminder - userBasicInfo or course basic info not found");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("courseName", courseInfo.getTitle());

        String targetString = null;
        for (String target : courseInfo.getTargets()) {
            targetString = target;
            break;
        }

        String batchLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + targetString + "/course/"
                + courseInfo.getId() + "/batch-details/list?enrolled=true&batchId=" + enrollmentInfo.getBatchId();
        String shortUrl = WebCommunicator.getShortUrl(batchLink);
        if (!StringUtils.isEmpty(shortUrl)) {
            batchLink = shortUrl;
        }
        bodyScopes.put("batchLink", batchLink);

        // SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        // sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        // String dateTime = sdf.format(new Date(dueTime));
        // bodyScopes.put("dueDate", dateTime);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("nisharg.g@vedantu.com", "Nisharg"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                CommunicationType.OTF_TRIAL_PAYMENT_BLOCKED, userBasicInfo.getRole());
        request.setBcc(bccList);
        sendEmail(request);
    }

    public void sendCoursePlanReqEmails(CoursePlanReqFormReq req)
            throws NotFoundException, UnsupportedEncodingException, VException, AddressException {
        Map<String, Object> bodyScopes = new HashMap<>();
        List<Long> userIds = new ArrayList<>();
        Long studentId = req.getStudentId();
        userIds.add(studentId);
        if (req.getTeacherId() != null) {
            userIds.add(req.getTeacherId());
        }
        Map<Long, User> userInfos = userUtils.getUserInfosMap(userIds, true);
        User studentInfo = userInfos.get(studentId);
        if (studentInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "studentBasicInfo not found");
        }
        String studentName = studentInfo.getFullName();
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }
        bodyScopes.put("studentName", studentName);
        bodyScopes.put("studentId", studentId);
        bodyScopes.put("studentEmail", studentInfo.getEmail());
        bodyScopes.put("grade", studentInfo.getStudentInfo().getGrade());
        bodyScopes.put("board", studentInfo.getStudentInfo().getBoard());

        if (req.getTeacherId() != null) {
            User teacherInfo = userInfos.get(req.getTeacherId());
            String teacherName = teacherInfo.getFullName();

            if (teacherName != null && teacherName.length() > 1) {
                teacherName = teacherName.substring(0, 1).toUpperCase() + teacherName.substring(1);
            }
            bodyScopes.put("teacherName", teacherName);
            bodyScopes.put("teacherId", req.getTeacherId());
        }

        bodyScopes.put("requirement", req.getRequirement());
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        List<String> dates = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getCompactSchedule())) {
            for (CoursePlanCompactSchedule schedule : req.getCompactSchedule()) {
                String startTime = sdf.format(new Date(schedule.getStartTime()));
                String endTime = sdf.format(new Date(schedule.getEndTime()));
                dates.add(schedule.getDay().name() + ", " + startTime + " - " + endTime);
            }
        }
        bodyScopes.put("dates", dates);

        sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        String requestSentOn = sdf.format(new Date());
        bodyScopes.put("requestSentOn", requestSentOn);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        String[] ccEmails = ConfigUtils.INSTANCE.getStringValue("courseplanform.email.cc").split(",");
        for (String email : ccEmails) {
            toList.add(new InternetAddress(email));
        }

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.COURSE_PLAN_FORM_REQ,
                Role.ADMIN);
        sendEmail(request);
    }

    private CMDSTestAttemptPojo getTestAttemptFromContentInfo(MoodleContentInfo contentInfo, String testAttemptId) {
        TestContentInfoMetadataPojo testContentInfoMetadata = contentInfo.getMetadata();
        for (CMDSTestAttemptPojo cmdsTestAttempt : testContentInfoMetadata.getTestAttempts()) {
            if (cmdsTestAttempt.getId().equals(testAttemptId)) {
                return cmdsTestAttempt;
            }
        }
        return null;
    }

    public void sendCoursePlanAdminAlerts(CoursePlanAdminAlertReq coursePlanAdminAlertReq, CommunicationType type)
            throws AddressException, VException {

        Map<String, Object> bodyScopes = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        List<CoursePlanInfo> alertCoursePlanInfos = coursePlanAdminAlertReq.getFirstPaymentApproachingPlans();
        switch (type) {
            case COURSE_PLAN_TRIAL_PAYMENT_NOT_DONE_ALERT:
            case COURSE_PLAN_FIRST_PAYMENT_DUE_APPROACHING_ALERT:
                if (ArrayUtils.isEmpty(alertCoursePlanInfos)) {
                    return;
                }
                Set<Long> userIds = new HashSet<>();
                for (CoursePlanInfo coursePlanInfo : alertCoursePlanInfos) {
                    Long teacherId = coursePlanInfo.getTeacherId();
                    Long studentId = coursePlanInfo.getStudentId();
                    userIds.add(teacherId);
                    userIds.add(studentId);
                }
                Map<Long, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);
                for (CoursePlanInfo coursePlanInfo : alertCoursePlanInfos) {
                    Long teacherId = coursePlanInfo.getTeacherId();
                    Long studentId = coursePlanInfo.getStudentId();
                    if (userInfos.get(teacherId) != null) {
                        coursePlanInfo.setTeacher(userInfos.get(teacherId));
                    }
                    if (userInfos.get(studentId) != null) {
                        coursePlanInfo.setStudent(userInfos.get(studentId));
                    }
                    String link = ConfigUtils.INSTANCE.getStringValue("url.base") + "/courseplan/" + coursePlanInfo.getId();
                    coursePlanInfo.setDescription(link);
                    if (CommunicationType.COURSE_PLAN_TRIAL_PAYMENT_NOT_DONE_ALERT.equals(type)) {
                        // just using an existting field instead of creating a new
                        // pojo
                        coursePlanInfo.setDemoVideoUrl(sdf.format(new Date(coursePlanInfo.getStartDate())));
                    } else if (CommunicationType.COURSE_PLAN_FIRST_PAYMENT_DUE_APPROACHING_ALERT.equals(type)) {
                        coursePlanInfo.setDemoVideoUrl(sdf.format(new Date(coursePlanInfo.getRegularSessionStartDate())));
                    }
                }
                bodyScopes.put("coursePlanInfos", alertCoursePlanInfos);
                break;
            case COURSE_PLAN_TRAIL_SESSIONS_DONE:
                CoursePlanBasicInfo trialCoursePlanInfo = coursePlanAdminAlertReq.getTrialDoneCoursePlan();
                List<SessionInfo> trailDoneSessions = coursePlanAdminAlertReq.getTrailDoneSessions();
                if (trialCoursePlanInfo == null || ArrayUtils.isEmpty(trailDoneSessions)) {
                    return;
                }
                String link = ConfigUtils.INSTANCE.getStringValue("url.base") + "/courseplan/"
                        + trialCoursePlanInfo.getId();
                trialCoursePlanInfo.setDescription(link);
                Set<Long> userIds2 = new HashSet<>();
                Long teacherId = trialCoursePlanInfo.getTeacherId();
                Long studentId = trialCoursePlanInfo.getStudentId();
                userIds2.add(teacherId);
                userIds2.add(studentId);
                Map<Long, UserBasicInfo> userInfos2 = userUtils.getUserBasicInfosMapFromLongIds(userIds2, true);
                if (userInfos2.get(teacherId) != null) {
                    trialCoursePlanInfo.setTeacher(userInfos2.get(teacherId));
                }
                if (userInfos2.get(studentId) != null) {
                    trialCoursePlanInfo.setStudent(userInfos2.get(studentId));
                }
                for (SessionInfo sessionInfo : trailDoneSessions) {
                    sessionInfo.setRemark(sdf.format(new Date(sessionInfo.getStartTime())));
                    if (ArrayUtils.isNotEmpty(sessionInfo.getAttendees())) {
                        for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                            if (userSessionInfo.getBillingPeriod() != null) {
                                sessionInfo.setDescription(DateTimeUtils.printableTimeStringHHmmSS(
                                        userSessionInfo.getBillingPeriod() / DateTimeUtils.MILLIS_PER_SECOND));
                            } else {
                                sessionInfo.setDescription(DateTimeUtils.printableTimeStringHHmmSS(0));
                            }
                        }
                    }
                }
                trailDoneSessions.forEach(session->session.setSessionId(Long.toString(session.getId())));
                bodyScopes.put("trialCoursePlanInfo", trialCoursePlanInfo);
                bodyScopes.put("trailDoneSessions", trailDoneSessions);
                break;

        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        String[] ccEmails = ConfigUtils.INSTANCE.getStringValue("courseplanalerts.email.cc").split(",");
        for (String email : ccEmails) {
            toList.add(new InternetAddress(email));
        }

        if (CommunicationType.COURSE_PLAN_TRAIL_SESSIONS_DONE.equals(type)
                && coursePlanAdminAlertReq.getTrialDoneCoursePlan() != null
                && ArrayUtils.isNotEmpty(coursePlanAdminAlertReq.getTrialDoneCoursePlan().getAdminData())) {
            List<CoursePlanAdminData> coursePlanAdminDatas = coursePlanAdminAlertReq.getTrialDoneCoursePlan()
                    .getAdminData();
            for (CoursePlanAdminData coursePlanAdminData : coursePlanAdminDatas) {
                if (coursePlanAdminData.getEmail() != null) {
                    toList.add(new InternetAddress(coursePlanAdminData.getEmail()));
                }
            }
        }

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, type, Role.ADMIN);
        sendEmail(request);
    }

    public void sendAdminSessionUpdateEmail(AdminSessionUpdateReq req, User user, SessionInfo sessionInfo)
            throws UnsupportedEncodingException, AddressException, VException {
        logger.info("sendAdminSessionUpdateEmail - " + req.toString() + " user:" + user.toString());
        CommunicationType emailType = CommunicationType.SESSION_UPDATE_REQUEST;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("userRole", user.getRole().name().toLowerCase());
        bodyScopes.put("userName", user._getFullName());
        bodyScopes.put("userEmail", user.getEmail());
        bodyScopes.put("userNote", req.getNote());
        bodyScopes.put("userContactNumber", user.getContactNumber());
        bodyScopes.put("sessionId", sessionInfo.getId());
        bodyScopes.put("sessionTitle", sessionInfo.getTitle());
        bodyScopes.put("updateRequest", "reschedule");

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(sessionInfo.getStartTime()));
        bodyScopes.put("sessionStartTime", dateTime + " IST");

        String[] emailToAddress = null;
        if (Role.STUDENT.equals(user.getRole())) {
            String emailTo = ConfigUtils.INSTANCE.getStringValue("email.session.update.student.request.to.address");
            if (!StringUtils.isEmpty(emailTo)) {
                emailToAddress = emailTo.split(",");
            }
        } else {
            String emailTo = ConfigUtils.INSTANCE.getStringValue("email.session.update.teacher.request.to.address");
            if (!StringUtils.isEmpty(emailTo)) {
                emailToAddress = emailTo.split(",");
            }
        }

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress("shyam.krishna@vedantu.com", "Shyam"));
        if (emailToAddress != null && emailToAddress.length > 0) {
            for (String emailid : emailToAddress) {
                toAddress.add(new InternetAddress(emailid));
            }
        }

        ArrayList<InternetAddress> replyToAddress = new ArrayList<>();
        replyToAddress.add(new InternetAddress(user.getEmail(), user._getFullName()));

        EmailRequest request = new EmailRequest(toAddress, bodyScopes, bodyScopes, emailType, Role.ADMIN);
        request.setReplyTo(replyToAddress);
        sendEmail(request);
    }

    public void sendOTFPreviousDayRecordingsEmail(List<OTFSessionPojoUtils> sessions)
            throws UnsupportedEncodingException, VException {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date(DateTimeUtils.getISTDayStartTime(System.currentTimeMillis())));

        List<Map<String, String>> sessionMapList = new ArrayList<>();
        String sessionString = "";
        if (sessions == null || sessions.isEmpty()) {
            logger.info("sendPreviousDayRecordingsEmail - No OTF sessions for date " + dateTime);
            sessionString = "No OTF sessions yesteday";
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            sessionString = "Recording data for yesteday.";

            List<String> userIds = new ArrayList<>();
            for (OTFSessionPojoUtils session : sessions) {
                userIds.add(session.getPresenter());
            }
            Map<String, UserBasicInfo> userInfos = userUtils.getUserBasicInfosMap(userIds, true);

            for (OTFSessionPojoUtils session : sessions) {
                Map<String, String> sessionParams = new HashMap<String, String>();
                if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                    sessionParams.put("courseId", session.getCourseInfos().get(0).getId().toString());
                    sessionParams.put("courseName", session.getCourseInfos().get(0).getTitle());
                }
                if (ArrayUtils.isNotEmpty(session.getBatchIds())) {
                    sessionParams.put("batchIds", StringUtils.join(session.getBatchIds().toArray(), ","));
                }
                sessionParams.put("sessionName", session.getTitle());
                sessionParams.put("sessionId", session.getId());
                sessionParams.put("sessionStartTime", dateFormat.format(session.getStartTime()));
                sessionParams.put("sessionEndTime", dateFormat.format(session.getEndTime()));
                sessionParams.put("sessionOrganiserToken", session.getOrganizerAccessToken());
                if (!CollectionUtils.isEmpty(session.getReplayUrls())) {
                    sessionParams.put("recordingString",
                            Arrays.toString(getRecordingLinks(session.getId(), session.getReplayUrls()).toArray()));
                } else {
                    sessionParams.put("recordingString", "No recording found");
                }

                try {
                    sessionParams.put("sessionPresenter", userInfos.get(session.getPresenter()).getFullName());
                } catch (Exception ex) {
                    logger.error("sendOTFDailySessionsEmail", ex);
                }

                sessionMapList.add(sessionParams);
            }
        }

        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();
        bodyScopes.put("dateTime", dateTime);
        bodyScopes.put("sessionCount", sessions == null ? 0 : sessions.size());
        bodyScopes.put("sessionString", sessionString);
        bodyScopes.put("sessions", sessionMapList);
        CommunicationType emailType = CommunicationType.OTF_SESSION_RECORDINGS;

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress("atom@vedantu.com", "Atom"));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("shyam.krishna@vedantu.com", "Shyam Krishna"));
        bccList.add(new InternetAddress("otf-support@vedantu.com", "OTF"));
        bccList.add(new InternetAddress("nisharg.g@vedantu.com", "Nisharg Golash"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, emailType, Role.ADMIN);
        request.setBcc(bccList);
        sendEmail(request);
    }

    public void sendARMFormReqEmails(AddARMFormReq req)
            throws NotFoundException, UnsupportedEncodingException, VException, AddressException {
        Map<String, Object> bodyScopes = new HashMap<>();
        List<Long> userIds = new ArrayList<>();
        Long studentId = req.getStudentId();
        userIds.add(studentId);

        Map<Long, User> userInfos = userUtils.getUserInfosMap(userIds, true);
        User studentInfo = userInfos.get(studentId);
        if (studentInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "studentBasicInfo not found");
        }
        String studentName = studentInfo.getFullName();
        if (studentName != null && studentName.length() > 1) {
            studentName = studentName.substring(0, 1).toUpperCase() + studentName.substring(1);
        }
        bodyScopes.put("studentName", studentName);
        bodyScopes.put("studentId", studentId);
        bodyScopes.put("studentEmail", studentInfo.getEmail());
        bodyScopes.put("grade", studentInfo.getStudentInfo().getGrade());
        bodyScopes.put("board", studentInfo.getStudentInfo().getBoard());
        bodyScopes.put("lookingFor", req.getLookingFor());
        bodyScopes.put("subjectName", req.getSubject());
        bodyScopes.put("target", req.getTarget());
        bodyScopes.put("ARMformId", req.getId());
        String baseUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("urlBase", baseUrl);

        // TODO: other values to be added
        bodyScopes.put("requirement", req.getComment());
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        List<String> dates = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getCompactSchedule())) {
            for (CoursePlanCompactSchedule schedule : req.getCompactSchedule()) {
                String startTime = sdf.format(new Date(schedule.getStartTime()));
                String endTime = sdf.format(new Date(schedule.getEndTime()));
                dates.add(schedule.getDay().name() + ", " + startTime + " - " + endTime);
            }
        }
        bodyScopes.put("dates", dates);

        sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        String requestSentOn = sdf.format(new Date());
        bodyScopes.put("requestSentOn", requestSentOn);

        sdf = new SimpleDateFormat("EEE, dd MMM yyyy");
        String startDate = sdf.format(new Date(req.getStartDate()));
        bodyScopes.put("startDate", startDate);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        //
        String[] ccEmails = ConfigUtils.INSTANCE.getStringValue("armform.email.cc").split(",");
        for (String email : ccEmails) {
            toList.add(new InternetAddress(email));
        }

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                CommunicationType.ADD_ARM_FORM_REQ_EMAIL, Role.ADMIN);
        sendEmail(request);
    }

    public void sendInactiveSessionEmail(SessionInfo session) throws UnsupportedEncodingException, VException {
        List<String> attendeeString = new ArrayList<String>();
        attendeeString.add("Session Id : " + session.getId() + " SessionStatus : " + session.getState());
        HashMap<String, Object> bodyScopes = new HashMap<String, Object>();

        String joinStatusString = "";
        if (!CollectionUtils.isEmpty(session.getAttendees())) {
            for (int i = 0; i < session.getAttendees().size(); i++) {
                // construct attendee string for email here.
                int count = i + 1;
                UserSessionInfo attendeeUser = session.getAttendees().get(i);
                bodyScopes.put("attendee" + count + "Id", attendeeUser.getUserId());
                bodyScopes.put("attendee" + count + "Role", attendeeUser.getRole().toString());
                bodyScopes.put("attendee" + count + "Name", attendeeUser.getFullName());
                bodyScopes.put("attendee" + count + "Number", attendeeUser.getContactNumber());
                bodyScopes.put("attendee" + count + "State", attendeeUser.getUserState());
                joinStatusString += " " + attendeeUser.getRole().name() + " " + attendeeUser.getUserState() + ".";
            }
        }

        bodyScopes.put("joinStatusString", joinStatusString);
        String scheduledBy = "";
        Long scheduledByUserId = session.getScheduledBy();
        if (scheduledByUserId != null && scheduledByUserId > 0) {
            UserBasicInfo scheduledByUser = userUtils.getUserBasicInfo(scheduledByUserId, true);
            scheduledBy = scheduledByUser.getFullName();
        }

        bodyScopes.put("scheduledBy", scheduledBy);
        bodyScopes.put("title", session.getTitle());
        bodyScopes.put("subject", session.getSubject());
        bodyScopes.put("sessionDetails", "Session Id : " + session.getId() + " SessionStatus : " + session.getState());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        bodyScopes.put("startTime", sdf.format(new Date(session.getStartTime())));
        bodyScopes.put("endTime", sdf.format(new Date(session.getEndTime())));

        ArrayList<InternetAddress> toList = new ArrayList<>();
        String inactiveEmailAddressList = ConfigUtils.INSTANCE.getStringValue("email.session.inactive.admin");
        if (!StringUtils.isEmpty(inactiveEmailAddressList)) {
            String[] emails = inactiveEmailAddressList.split(",");
            for (String email : emails) {
                if (!StringUtils.isEmpty(email)) {
                    toList.add(new InternetAddress(email.trim(), "Admin"));
                }
            }
        }

        toList.add(new InternetAddress("shyam.krishna@vedantu.com", "Shyam Krishna"));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.SESSION_INACTIVE,
                Role.ADMIN);
        sendEmail(request);
    }

    public void sendArmFormExportEmail(Long startTime, Long endTime, Long callingUserId)
            throws UnsupportedEncodingException, VException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("startDate", sdf.format(new Date(startTime)));
        bodyScopes.put("endDate", sdf.format(new Date(endTime)));

        // Fetch the course plans
        ExportCoursePlansReq req = new ExportCoursePlansReq();
        req.setFromTime(startTime);
        req.setTillTime(endTime);
        req.setStart(0);
        req.setSize(900);
        List<ARMFormInfo> armFormInfos = armFormManager.getARMForms(req);

        String armFormData = "";
        if (!CollectionUtils.isEmpty(armFormInfos)) {
            Set<Long> boardIds = new HashSet<>();
            armFormInfos.forEach(armForm -> {
                if (!CollectionUtils.isEmpty(armForm.getBoardId())) {
                    boardIds.addAll(armForm.getBoardId());
                }
            });

            List<String> exportArmFormEntries = new ArrayList<>();
            Map<Long, Board> boardMap = userUtils.getBoardInfoMap(boardIds);

            armFormInfos.forEach(armForm -> {
                exportArmFormEntries.add(getArmFormExportString(armForm, boardMap));
            });

            if (!CollectionUtils.isEmpty(exportArmFormEntries)) {
                armFormData = String.join("\n", exportArmFormEntries);
                List<String> headers = Arrays.asList("CreationTime", "LastUpdatedTime", "id", "school", "city",
                        "studentId", "studentEmailId", "lookingFor", "target", "grade", "subjects", "schoolBooks",
                        "startDate", "modeOfLearning", "comment", "deliverableEntityType", "deliverableEntityId",
                        "chapterSequence", "marksScored", "doubts", "registrationDone", "added", "description",
                        "adminId", "adminEmailId", "createdByEmailId", "boards", "isClosed", "adminComment");
                armFormData = String.join(",", headers) + "\n" + armFormData;
            }
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        UserBasicInfo callingUser = userUtils.getUserBasicInfo(callingUserId, true);
        toList.add(new InternetAddress(callingUser.getEmail(), "Admin"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.ARM_FORM_EXPORT_EMAIL,
                Role.ADMIN);

        if (!StringUtils.isEmpty(armFormData)) {
            EmailAttachment attachment = new EmailAttachment();
            attachment.setApplication("application/csv");
            attachment.setAttachmentData(armFormData);
            attachment.setFileName("ARM_form_export_" + sdf.format(new Date(System.currentTimeMillis())) + ".csv");
            request.setAttachment(attachment);
        }
        sendEmailViaRest(request);
    }

    public void sendCoursePlanInconsistentHours(List<CoursePlanHourInconsistentDetails> errorCoursePlanEntries,
            long calculatedStartTime, long calculatedEndTime) throws UnsupportedEncodingException, VException {
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("calculatedStartTime", calculatedStartTime);
        bodyScopes.put("calculatedEndTime", calculatedEndTime);
        bodyScopes.put("errorCoursePlanEntries", errorCoursePlanEntries);
        bodyScopes.put("reportStatus",
                !CollectionUtils.isEmpty(errorCoursePlanEntries) ? "Inconsistent" : "Consistent");

        ArrayList<InternetAddress> toAddress = new ArrayList<>();
        toAddress.add(new InternetAddress(ConfigUtils.INSTANCE.getStringValue("email.platformTeam"), "Platform"));

        EmailRequest request = new EmailRequest(toAddress, bodyScopes, bodyScopes,
                CommunicationType.COURSE_PLAN_HOURS_INCONSISTENCY_REPORT, Role.ADMIN);
        sendEmail(request);
    }

    private String getArmFormExportString(ARMFormInfo armFormInfo, Map<Long, Board> boardMap) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        List<String> resultEntries = new ArrayList<>();
        resultEntries.add(escapeCommas(
                armFormInfo.getCreationTime() != null ? sdf.format(new Date(armFormInfo.getCreationTime())) : "")); // Creation
        // time
        resultEntries.add(escapeCommas(
                armFormInfo.getLastUpdated() != null ? sdf.format(new Date(armFormInfo.getLastUpdated())) : "")); // last
        // updated
        resultEntries.add(escapeCommas(armFormInfo.getId())); // Id
        resultEntries.add(escapeCommas(armFormInfo.getSchool())); // school
        resultEntries.add(escapeCommas(armFormInfo.getCity())); // city
        resultEntries.add(String.valueOf(armFormInfo.getStudentId())); // studentId
        resultEntries.add(armFormInfo.getStudent() != null ? armFormInfo.getStudent().getEmail() : ""); // studentEmailId
        resultEntries.add(escapeCommas(armFormInfo.getLookingFor())); // lookingFor
        resultEntries.add(armFormInfo.getTarget()); // target
        resultEntries.add(String.valueOf(armFormInfo.getGrade())); // grade
        resultEntries.add(
                !CollectionUtils.isEmpty(armFormInfo.getSubject()) ? String.join("|", armFormInfo.getSubject()) : ""); // subjects
        resultEntries.add(escapeCommas(armFormInfo.getSchoolBooks())); // schoolBooks
        resultEntries
                .add((armFormInfo.getStartDate() != null) ? sdf.format(new Date(armFormInfo.getStartDate())) : null); // startDate
        resultEntries.add(String.valueOf(armFormInfo.getModeOfLearning())); // modeOfLearning
        resultEntries.add(escapeCommas(armFormInfo.getComment())); // comment
        resultEntries.add(String.valueOf(armFormInfo.getDeliverableEntityType())); // deliverableEntityType
        resultEntries.add(armFormInfo.getDeliverableEntityId()); // deliverableEntityId
        resultEntries.add(escapeCommas(armFormInfo.getChapterSequence())); // chapterSequence
        resultEntries.add(armFormInfo.getMarksScored()); // marksScored
        resultEntries.add(escapeCommas(armFormInfo.getDoubts())); // doubts
        resultEntries.add(String.valueOf(armFormInfo.getRegistrationDone())); // registrationDone
        resultEntries.add(String.valueOf(armFormInfo.getAdded())); // added
        resultEntries.add(escapeCommas(armFormInfo.getDescription())); // description
        resultEntries.add(String.valueOf(armFormInfo.getAdminId())); // adminId
        resultEntries.add(armFormInfo.getAdmin() != null ? armFormInfo.getAdmin().getEmail() : ""); // adminEmailId
        resultEntries
                .add(armFormInfo.getCreatedByBasicInfo() != null ? armFormInfo.getCreatedByBasicInfo().getEmail() : ""); // createdByEmailId

        // Fill board details
        if (!CollectionUtils.isEmpty(armFormInfo.getBoardId())) {
            List<String> boardSlug = new ArrayList<>();
            armFormInfo.getBoardId().forEach(boardId -> {
                if (boardMap.containsKey(boardId)) {
                    boardSlug.add(boardMap.get(boardId).getSlug());
                }
            });
            resultEntries.add(String.join("|", boardSlug)); // boards
        }
        resultEntries.add(String.valueOf(armFormInfo.isIsClosed()));
        resultEntries.add(armFormInfo.getAdminComment());

        return String.join(",", resultEntries);
    }

    private String escapeCommas(String input) {
        if (!StringUtils.isEmpty(input)) {
            input = input.replace(",", "-");
            input = input.replace("\n", " ");
        }
        return input;
    }

    public List<String> getRecordingLinks(String sessionId, List<String> replayUrls) {
        List<String> replayLinks = new ArrayList<>();
        if (!CollectionUtils.isEmpty(replayUrls)) {
            // https://www.vedantu.com/onetofew/getSessionRecordingUrl?recordingId=118266&sessionId=59477b35e4b0af7f33fffcb3
            for (String replayUrl : replayUrls) {
                replayLinks.add(ConfigUtils.INSTANCE.getStringValue("web.baseUrl")
                        + "/onetofew/getSessionRecordingUrl?recordingId=" + replayUrl + "&sessionId=" + sessionId);
            }
        }
        return replayLinks;
    }

    public PlatformBasicResponse markUnsubscribeStatus(UnsubscribeEmailReq req) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/markUnsubscribeStatus", HttpMethod.POST,
                new Gson().toJson(req));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, PlatformBasicResponse.class);
    }

    public GetUnsubscribedListRes getUnsubscribedList(GetUnsubscribedListReq req) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        String url = notificationEndpoint
                + "/email/getUnsubscribedList" + "?" + WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, GetUnsubscribedListRes.class);
    }

    public void sendMoodleNotificationsOnSharing(ShareContentAndSendMailRes request) {
        if (request != null && ArrayUtils.isNotEmpty(request.getMoodleContentInfos())) {
            Set<String> contextId = new HashSet<>();
            for (MoodleContentInfo moodleContentInfo : request.getMoodleContentInfos()) {
                if (moodleContentInfo.getEngagementType() != null && StringUtils.isNotEmpty(moodleContentInfo.getContextId())
                        && EngagementType.OTF.equals(moodleContentInfo.getEngagementType())
                        && StringUtils.isEmpty(moodleContentInfo.getCourseName())) {
                    contextId.add(moodleContentInfo.getContextId());
                }
            }
            try {
                Map<String, String> courseMap = otfManager.getCourseTitleForBatchIds(contextId);
                for (MoodleContentInfo moodleContentInfo : request.getMoodleContentInfos()) {
                    if (courseMap != null && StringUtils.isNotEmpty(moodleContentInfo.getContextId())
                            && StringUtils.isEmpty(moodleContentInfo.getCourseName())
                            && courseMap.containsKey(moodleContentInfo.getContextId())) {
                        moodleContentInfo.setCourseName(courseMap.get(moodleContentInfo.getContextId()));
                    }
                    SendMoodleNotificationsReq req = new SendMoodleNotificationsReq("SHARED", moodleContentInfo);
                    sendMoodleNotifications(req);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.error("Error in getting courseTitle", e);
            }
        }
    }

    public void sendRegistrationEmailForISL(User user) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_ISL;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "ISL_2017");
        if (newInfo != null) {
            String category = getISLCategoryString(newInfo);
            bodyScopes.put("category", category);
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);

    }

    public void sendEmailRegistrationISLViaTools(User user) throws IOException, VException {
        CommunicationType emailType = CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "ISL_2017");

        GeneratePasswordReq generatePasswordReq = new GeneratePasswordReq();
        generatePasswordReq.setEmail(user.getEmail());
        String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/generatePassword", HttpMethod.POST,
                new Gson().toJson(generatePasswordReq), true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GeneratePasswordRes response = new Gson().fromJson(jsonString, GeneratePasswordRes.class);
        String password = response.getNewPassword();

//        String passwordResetLink = ConfigUtils.INSTANCE.getStringValue("web.baseUrl")
//                + ConfigUtils.INSTANCE.getStringValue("url.verify.token") + tokenCode;
//
//        bodyScopes.put("passwordResetLink", passwordResetLink);
        bodyScopes.put("password", password);
        if (newInfo != null) {
            String category = getISLCategoryString(newInfo);
            bodyScopes.put("category", category);
        }
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, emailType, Role.STUDENT);
            smsManager.sendSMS(textSMSRequest);
        }
    }

    public void sendEmailUserDetailsCreatedViaTools(User user) throws IOException, VException {
        CommunicationType emailType = CommunicationType.USER_DETAILS_CREATED_VIA_ISL_REGISTRATION_TOOLS;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));
        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "ISL_2017");
        if (newInfo != null) {
            String category = getISLCategoryString(newInfo);
            bodyScopes.put("category", category);
        }
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
        if (StringUtils.isNotEmpty(user.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, emailType, Role.STUDENT);
            smsManager.sendSMS(textSMSRequest);
        }
    }

    public void sendReferralBundleEmail(UserInfo userInfo, UserInfo referrerUserInfo, ReferralStep referralStep,
            ReferralStepBonus referralStepBonus, String bundleId) throws UnsupportedEncodingException, VException {
        logger.info("sendReferralEmail: " + referralStep + userInfo);
        String signupBonus = ConfigUtils.INSTANCE.getStringValue("freebies.rupees.join.bonus");

        BundlePackageInfo bundlePackageInfo = bundlePackageManager.getBundlePackage(bundleId);

        if (bundlePackageInfo == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundlePackageInfo not Found for id  : " + bundleId);
        }
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String refereeName = userInfo._getFullName();
        String referrerName = referrerUserInfo._getFullName();

        if (refereeName != null && refereeName.length() > 1) {
            refereeName = refereeName.substring(0, 1).toUpperCase() + refereeName.substring(1);
        }
        if (referrerName != null && referrerName.length() > 1) {
            referrerName = referrerName.substring(0, 1).toUpperCase() + referrerName.substring(1);
        }

        String refereeFirstName = userInfo.getFirstName();
        String referrerFirstName = referrerUserInfo.getFirstName();

        if (refereeFirstName != null && refereeFirstName.length() > 1) {
            refereeFirstName = refereeFirstName.substring(0, 1).toUpperCase() + refereeFirstName.substring(1);
        }
        if (referrerFirstName != null && referrerFirstName.length() > 1) {
            referrerFirstName = referrerFirstName.substring(0, 1).toUpperCase() + referrerFirstName.substring(1);
        }

        String couponCode = "";
        if (StringUtils.isNotEmpty(bundlePackageInfo.getReferralCouponCode())) {
            couponCode = bundlePackageInfo.getReferralCouponCode();
        }

        String referralLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/otf-bundle-checkout/BUNDLE_PACKAGE/"
                + bundlePackageInfo.getId() + "?couponCode=" + couponCode + "&referralCode=" + userInfo.getReferralCode();

        if (referralStepBonus.getReferrerBonusAmount() == null) {
            referralStepBonus.setReferrerBonusAmount(0l);
        }
        if (referralStepBonus.getReferreeBonusAmount() == null) {
            referralStepBonus.setReferreeBonusAmount(0l);
        }
        bodyScopes.put("refereeName", refereeName);
        bodyScopes.put("referrerName", referrerName);
        bodyScopes.put("refereeFirstName", refereeFirstName);
        bodyScopes.put("referrerFirstName", referrerFirstName);
        bodyScopes.put("signupBonus", signupBonus);
        bodyScopes.put("refereeBonusAmount", referralStepBonus.getReferreeBonusAmount());
        bodyScopes.put("referrerBonusAmount", referralStepBonus.getReferrerBonusAmount());
        bodyScopes.put("refereeLink", referralLink);

        if (referralStepBonus.getReferrerBonusAmount() <= 0 && referralStepBonus.getReferreeBonusAmount() <= 0) {
            return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(referrerUserInfo.getEmail(), referrerUserInfo._getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.REFERRAL_TOREFERRER_BUNDLE,
                referrerUserInfo.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);

    }

    public void sendPostBundlePurchaseEmail(Long userId,
            BundlePackageInfo bundlePackageInfo) throws UnsupportedEncodingException, VException {

        logger.info("sending post bundle purchase email for {}", userId);
        if (bundlePackageInfo == null || userId == null) {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "bundlePackageInfo not Found or user null");
        }
        User userInfo = userUtils.getUserInfo(userId, true);
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String userName = userInfo._getFullName();
        String couponCode = "";
        if (StringUtils.isNotEmpty(bundlePackageInfo.getReferralCouponCode())) {
            couponCode = bundlePackageInfo.getReferralCouponCode();
        }
        ReferralStepBonus referralStepBonus = referralManager.getReferralStepBonusesForEntityId(bundlePackageInfo.getId(), ReferralStep.NEW_BUNDLE_PURCHASE);
        if (referralStepBonus == null) {
            logger.info("referralStepBonus is null.");
            return;
        }
        String referralLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/otf-bundle-checkout/BUNDLE_PACKAGE/"
                + bundlePackageInfo.getId() + "?couponCode=" + couponCode + "&referralCode=" + userInfo.getReferralCode();

        String referralLinkShort = "";
        String urlShortner = "https://www.googleapis.com/urlshortener/v1/url?key="
                + URL_SHORTNER_API_KEY;
        JSONObject json = new JSONObject();
        json.put("longUrl", referralLink);
        ClientResponse urlshortresp = WebUtils.INSTANCE.doCall(urlShortner, HttpMethod.POST,
                json.toString(), false);
        VExceptionFactory.INSTANCE.parseAndThrowException(urlshortresp);
        String urlshortrespStr = urlshortresp.getEntity(String.class);
        logger.info("respone of urlshortner " + urlshortrespStr);
        JSONObject responseObj = new JSONObject(urlshortrespStr);
        if (StringUtils.isNotEmpty(responseObj.getString("id"))) {
            referralLinkShort = responseObj.getString("id");
        } else {
            referralLinkShort = referralLink;
        }
        if (referralStepBonus.getReferrerBonusAmount() == null) {
            referralStepBonus.setReferrerBonusAmount(0l);
        }
        if (referralStepBonus.getReferreeBonusAmount() == null) {
            referralStepBonus.setReferreeBonusAmount(0l);
        }
        bodyScopes.put("studentName", userName);
        bodyScopes.put("title", bundlePackageInfo.getTitle());

        bodyScopes.put("referralLink", referralLinkShort);

        bodyScopes.put("refereeBonusAmount", referralStepBonus.getReferreeBonusAmount());
        bodyScopes.put("referrerBonusAmount", referralStepBonus.getReferrerBonusAmount());

        if (referralStepBonus.getReferrerBonusAmount() <= 0 && referralStepBonus.getReferreeBonusAmount() <= 0) {
            logger.info("referralStepBonus amounnts are 0.");
            return;
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userInfo.getEmail(), userInfo._getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.POST_BUNDLE_PURCHASE,
                userInfo.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
        if (StringUtils.isNotEmpty(userInfo.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(),
                    userInfo.getPhoneCode(), bodyScopes, CommunicationType.POST_BUNDLE_PURCHASE, userInfo.getRole());
            smsManager.sendSMS(textSMSRequest);
        }
    }

    public void sendWebinarQuestionsEmail(Webinar webinar, String data, List<InternetAddress> ccList) throws AddressException, VException {
        if (StringUtils.isEmpty(webinar.getTeacherEmail())) {
            return;
        }
        EmailRequest email = new EmailRequest();
        SimpleDateFormat sdfWebinarReg = new SimpleDateFormat("hh:mma, dd MMM yyyy");
        sdfWebinarReg.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfWebinarReg.format(new Date(webinar.getStartTime()));
        email.setBody("Please find the attachment. It contains Questions, Which were asked by students on registration");
        String subject = "Questions for webinar on " + webinar.getTitle() + " at " + timestr;

        subject = (ConfigUtils.INSTANCE.getBooleanValue("email.SUBJECT.env.append")
                ? ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                : "") + subject;

        email.setSubject(subject);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(webinar.getTeacherEmail()));
        email.setTo(toList);
        email.setType(CommunicationType.SYSTEM_INFO);
        EmailAttachment attachment = new EmailAttachment();
        attachment.setApplication("application/csv");
        attachment.setAttachmentData(data);

        attachment.setFileName(webinar.getTitle() + ":" + timestr + ".csv");
        email.setAttachment(attachment);
        email.setCc(ccList);
        sendEmailViaRest(email);
    }

    public void sendOTMBundlePurchaseEmail(Long userId, OTFBundleInfo otfBundle, Integer paidAmount,
            CommunicationType communicationType) throws AddressException, VException, IOException {
        UserBasicInfo user = userUtils.getUserBasicInfo(userId, true);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + userId);
        }
        if (otfBundle == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "otfbundle is null.");
        }
        GetBatchesReq request = new GetBatchesReq();
        request.setCourseId(otfBundle.getId());
        Set<String> studentIds = new HashSet<>();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        studentIds.add(userId.toString());
        request.setStudentIds(studentIds);
        List<StudentSlotPreferencePojo> pref = otfManager.fetchStudentSlotPreference(request);
        if (ArrayUtils.isNotEmpty(pref)) {
            if (ArrayUtils.isNotEmpty(pref.get(0).getPreferredSlots())) {
                OTFSlot slot = pref.get(0).getPreferredSlots().get(0);
                bodyScopes.put("startDate", slot.getStartDay());
                bodyScopes.put("dayType", slot.getDayType());
                bodyScopes.put("timing", slot.getSlotTimes() == null ? "" : slot.getSlotTimes().get(0));
            }
        }

        bodyScopes.put("studentName", user.getFullName());
        bodyScopes.put("studentId", userId);
        bodyScopes.put("contactNumber", user.getContactNumber());
        bodyScopes.put("phoneCode", user.getPhoneCode());
        bodyScopes.put("email", user.getEmail());
        bodyScopes.put("courseTitle", otfBundle.getTitle());
        bodyScopes.put("courseId", otfBundle.getId());
        if (paidAmount != null) {
            bodyScopes.put("paidAmount", paidAmount);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date());
        bodyScopes.put("dateTime", dateTime);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
        EmailRequest emailRequest = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, user.getRole());
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(emailRequest);

        try {
            Map<String, String> lsParams = new HashMap<>();
            lsParams.put("userEmail", user.getEmail());
            lsParams.put("entityType", "OTM_BUNDLE");
            lsParams.put("entityId", otfBundle.getId());
            lsParams.put("entityTitle", otfBundle.getTitle());
            String timings = "startDate : " + (String) bodyScopes.get("startDate") + " timing : " + (String) bodyScopes.get("timing");
            lsParams.put("timing", timings);
            if (paidAmount != null) {
                lsParams.put("paidAmount", paidAmount.toString());
            } else {
                lsParams.put("paidAmount", "");
            }

            LeadSquaredRequest lsReq = null;
            if (CommunicationType.POST_OTM_BUNDLE_REGISTRATION.equals(communicationType)) {
                lsReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_REGISTER, null, lsParams, null);
            } else if (CommunicationType.POST_OTM_BUNDLE_PURCHASE.equals(communicationType)) {
                lsReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_ENROLL, null, lsParams, null);
            }
            if (lsReq != null) {
                leadSquaredManager.executeAsyncTask(lsReq);
            } else {
                logger.error("leadSquared activity req is null. i.e. CommunicationType is null.");
            }
        } catch (Exception e) {
            logger.error("Error in sending lead activity to leadsquare");
        }

    }

    //TODO: Check if this method is being used, if not need to remove this
    public void sendRegistrationEmailForVSAT(User user) throws IOException, VException {
        logger.info("sending vsat welcome email to : " + user.getEmail());
        VsatEventDetailsPojo vsatEventDetailsPojo = userUtils.getVsatEventDetails();
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_VSAT;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if(StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if(StringUtils.isNotEmpty(user.getTempContactNumber())){
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        //building VSAT start Date text
        List<String> dateStringsList = new ArrayList<>();
        String yearString = "";

        List<VsatSchedule> vsatSchedules = vsatEventDetailsPojo.getVsatSchedules();
        if(ArrayUtils.isNotEmpty(vsatSchedules)){
            for(VsatSchedule vsatSchedule: vsatSchedules){
                if(vsatSchedule.getStartTime() != null){
                    SimpleDateFormat sdfForDateString = new SimpleDateFormat("EEEEE, dd MMM yyyy hh:mm a");
                    sdfForDateString.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                    dateStringsList.add(sdfForDateString.format(new Date(vsatSchedule.getStartTime())));
                }
            }
            SimpleDateFormat sdfForyearString = new SimpleDateFormat("yyyy");
            sdfForyearString.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            yearString = sdfForyearString.format(new Date(vsatSchedules.get(vsatSchedules.size()-1).getStartTime()));
        }
        bodyScopes.put("dateString", String.join(" & ",dateStringsList));
        bodyScopes.put("yearString", yearString);

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), vsatEventDetailsPojo.getEventName());

        // NEED TO MAKE CHANGES HERE
        if (newInfo != null) {
            bodyScopes.put("studentTarget", newInfo.getExam());
            if (null != newInfo.getGrade()) {
                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
            }
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                user.getPhoneCode(), bodyScopes, emailType, user.getRole());
        smsManager.sendSMSViaRest(textSMSRequest);
    }

    public void sendRegistrationEmailForVolt(User user) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if(StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if(StringUtils.isNotEmpty(user.getTempContactNumber())){
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "VOLT_2020_JAN");
        CommunicationType emailType;
        if (newInfo.getVoltStatus() == VoltStatus.REGISTERED) {
            emailType = CommunicationType.REGISTRATION_EMAIL_VOLT;
        } else {
            emailType = CommunicationType.REQUEST_CONFIRM_EMAIL_VOLT;
        }

        // NEED TO MAKE CHANGES HERE
        if (newInfo != null) {
            bodyScopes.put("studentTarget", newInfo.getExam());
            if (null != newInfo.getGrade()) {
                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
            }
        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_VOLT) {
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, emailType, user.getRole());
            smsManager.sendSMSViaRest(textSMSRequest);
        }
    }
    public void sendRegistrationEmailForJRP(User user, UserDetailsInfo info) throws IOException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        CommunicationType emailType = CommunicationType.REGISTERED_JRP;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", info.getStudentName());
        bodyScopes.put("email", info.getEmail());
        bodyScopes.put("contactNumber", info.getStudentPhoneNo());
        bodyScopes.put("grade", info.getGrade().toString());

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        bodyScopes.put("link1", ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/v/studentprofile");
        bodyScopes.put("link2", ConfigUtils.INSTANCE.getStringValue("web.baseUrl") + "/iit-jee/jee-online-mock-test");

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        subjectScopes.put("studentName", info.getStudentName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        request.setIncludeHeaderFooter(Boolean.FALSE);
//        request.setEmailService(EmailService.MANDRILL);
        sendEmail(request);
        TextSMSRequest textSMSRequest = new TextSMSRequest(info.getStudentPhoneNo(),
                info.getStudentPhoneCode(), bodyScopes, CommunicationType.REGISTERED_JRP, Role.STUDENT);
        smsManager.sendSMS(textSMSRequest);

    }

    public void sendMailToNotifyPageDeleted(String url) throws UnsupportedEncodingException, VException {
        logger.info("Sending mail to notify PageDeleted");
        CommunicationType emailType = CommunicationType.LMS_SEO_PAGE_DELETED;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("url", url);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        String emailSeo = ConfigUtils.INSTANCE.getStringValue("email.seo");

        toList.add(new InternetAddress(emailSeo, "Vedantu SEO"));

        EmailRequest request = new EmailRequest(toList, null, bodyScopes, emailType, Role.ADMIN);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    private String getISLCategoryString(UserDetailsInfo userDetailsInfo) {
        String category = null;
        if (null != userDetailsInfo.getCategory()) {
            switch (userDetailsInfo.getCategory()) {
                case "ISL_2017_CAT_3_5":
                    category = "In Category 1 : Grades 3-5";
                    break;
                case "ISL_2017_CAT_6_8":
                    category = "In Category 2 : Grades 6-8";
                    break;
                case "ISL_2017_CAT_9_10":
                    category = "In Category 3 : Grades 9-10";
                    break;
                default:
                    break;
            }
        }
        return category;
    }

    public void trackEmailLinkClicked(String emailId, String linkRandomId) {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        String url = notificationEndpoint + "/email/emailLinkClicked?emailId=" + emailId + "&linkRandomId=" + linkRandomId;
        WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
    }

    public void sendMailForFeedbackForm(HashMap<String, Object> bodyScopes, HashMap<String, Object> subjectScopes, String emailId, String fullName) throws UnsupportedEncodingException, VException {
        CommunicationType communicationType = CommunicationType.FEEDBACK_FORM_EMAIL;

        ArrayList<InternetAddress> toList = new ArrayList<>();

        toList.add(new InternetAddress(emailId, fullName));
        EmailRequest emailRequest = new EmailRequest(toList, subjectScopes, bodyScopes, communicationType, Role.STUDENT);
        sendEmail(emailRequest);
    }

    public void sendRegistrationEmailForReviseIndia(User user) throws UnsupportedEncodingException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if(StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if(StringUtils.isNotEmpty(user.getTempContactNumber())){
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "REVISEINDIA_2020_FEB");
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_REVISEINDIA;

//        // NEED TO MAKE CHANGES HERE
//        if (newInfo != null) {
//            bodyScopes.put("studentTarget", newInfo.getExam());
//            if (null != newInfo.getGrade()) {
//                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
//            }
//        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_REVISEINDIA) {
            CommunicationType communicationType = CommunicationType.REGISTRATION_SMS_REVISEINDIA;
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
            smsManager.sendSMSViaRest(textSMSRequest);
        }
    }

    public void sendRegistrationEmailForReviseJee(User user) throws UnsupportedEncodingException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if(StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if(StringUtils.isNotEmpty(user.getTempContactNumber())){
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(user.getId(), "REVISE_JEE_2020_MARCH");
        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_REVISEJEE;

//        // NEED TO MAKE CHANGES HERE
//        if (newInfo != null) {
//            bodyScopes.put("studentTarget", newInfo.getExam());
//            if (null != newInfo.getGrade()) {
//                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
//            }
//        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_REVISEJEE) {
            CommunicationType communicationType = CommunicationType.REGISTRATION_SMS_REVISEJEE;
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
            smsManager.sendSMSViaRest(textSMSRequest);
        }
    }

    public void sendResultEmailForReviseJee(Map<String,Object> payload) throws UnsupportedEncodingException, VException {
        logger.info("sending jee result email to : " + payload.get("userId"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(Long.parseLong(payload.get("userId").toString()), "REVISE_JEE_2020_MARCH");

        CommunicationType emailType = CommunicationType.TEST_RESULT_EMAIL_REVISEJEE;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", newInfo.getStudentName());
        bodyScopes.put("predictedRankRange", payload.get("predictedRankRange"));
        bodyScopes.put("physicsScore", payload.get("physicsScore"));
        bodyScopes.put("physicsC", payload.get("physicsC"));
        bodyScopes.put("physicsW", payload.get("physicsW"));
        bodyScopes.put("physicsU", payload.get("physicsU"));
        bodyScopes.put("chemistryScore", payload.get("chemistryScore"));
        bodyScopes.put("chemistryC", payload.get("chemistryC"));
        bodyScopes.put("chemistryW", payload.get("chemistryW"));
        bodyScopes.put("chemistryU", payload.get("chemistryU"));
        bodyScopes.put("mathsScore", payload.get("mathsScore"));
        bodyScopes.put("mathsC", payload.get("mathsC"));
        bodyScopes.put("mathsW", payload.get("mathsW"));
        bodyScopes.put("mathsU", payload.get("mathsU"));

        HashMap<String, Object> subjectScopes = new HashMap<>();

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(newInfo.getStudentEmail(), newInfo.getStudentName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        sendEmail(request);
    }


    public static class PackageCourseInfo {

        private String courseName;
        private String courseSubject;
        private String courseId;
        private String encodedUrl;

        public PackageCourseInfo() {
            super();
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public String getCourseSubject() {
            return courseSubject;
        }

        public void setCourseSubject(String courseSubject) {
            this.courseSubject = courseSubject;
        }

        public String getCourseId() {
            return courseId;
        }

        public void setCourseId(String courseId) {
            this.courseId = courseId;
        }

        public String getEncodedUrl() {
            return encodedUrl;
        }

        public void setEncodedUrl(String encodedUrl) {
            this.encodedUrl = encodedUrl;
        }

        @Override
        public String toString() {
            return "PackageCourseInfo [courseName=" + courseName + ", courseSubject=" + courseSubject + ", courseId="
                    + courseId + ", toString()=" + super.toString() + "]";
        }
    }

    public void sendDescripancyEmail()
            throws VException, AddressException, UnsupportedEncodingException {
        HashMap<String, Object> bodyScopes = new HashMap<>();

        Gson gson = new Gson();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/getDueInstallment", HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<InstalmentInfo>>() {
        }.getType();
        List<InstalmentInfo> installments = gson.fromJson(jsonString, listType);



        String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        ClientResponse resp1 = WebUtils.INSTANCE.doCall(
                subscriptionEndpoint + "/bundle/getDuplicateEnrollment", HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
        String jsonString1 = resp1.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<BundleEnrollmentResp>>() {
        }.getType();
        List<BundleEnrollmentResp> bundleEnrollmentResps = gson.fromJson(jsonString1, listType1);




        bodyScopes.put("count", installments.size() );
        bodyScopes.put("bundleCount",  bundleEnrollmentResps.size()  );
        String results = "";


        if(!ArrayUtils.isEmpty(installments)) {
            for (InstalmentInfo instalment : installments) {
                results = results + instalment.getId() + " , ";
            }
        }
        String enrollmentResults = "";
        if(!ArrayUtils.isEmpty(bundleEnrollmentResps)) {
            for (BundleEnrollmentResp instalment : bundleEnrollmentResps) {
                enrollmentResults = enrollmentResults + instalment.getId() + " , ";
            }
        }


        bodyScopes.put("report", results);
        bodyScopes.put("bundleReport", enrollmentResults);
        List<InternetAddress> toList = new ArrayList<>();
        toList.add(new  InternetAddress("darshit.padhya@vedantu.com"));
        toList.add(new  InternetAddress("manish.singh@vedantu.com"));
//        toList.add(new  InternetAddress("ajith.reddy@vedantu.com"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.DATA_DESCRIPANCY,
                Role.STUDENT);
        logger.info("Email:" + request.toString());
        sendEmail(request);
    }


}
