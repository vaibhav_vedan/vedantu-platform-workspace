package com.vedantu.platform.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserInfo;
import com.vedantu.User.response.ReferralInfoRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.dinero.AccountManager;
import com.vedantu.platform.dao.referral.ReferralDao;
import com.vedantu.platform.dao.referral.ReferralStepBonusDao;
import com.vedantu.platform.pojo.referral.model.Referral;
import com.vedantu.platform.pojo.referral.model.ReferralStepBonus;
import com.vedantu.platform.pojo.referral.response.ReferralStepResponse;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.session.pojo.SessionAttendee;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.util.Arrays;

@Service
public class ReferralManager {

    @Autowired
    private ReferralStepBonusDao referralStepBonusDao;

    @Autowired
    private ReferralDao referralDao;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReferralManager.class);
    
    public void processReferralBonusAsync(ReferralStep referralStep, Long userId) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", userId);
        payload.put("referralStep", referralStep);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.PROCESS_REFERRAL_BONUS, payload);
        asyncTaskFactory.executeTask(params);
    }
    
    public void processReferralBonus(ReferralStep referralStep, Long userId) throws Exception {
    	processReferralBonus(referralStep,userId,null);
    }

    public void processReferralBonus(ReferralStep referralStep, Long userId, String entityId) throws Exception {

        ReferralInfoRes referralInfo = getReferralInfo(userId);

        UserInfo userInfo = referralInfo.getUserInfo();
        UserInfo referrerUserInfo = referralInfo.getReferrerUserInfo();

        if (referrerUserInfo == null) {
            logger.info("referrerUserInfo not found for userId " + userId);
            return;
        }

        String referralCode = referrerUserInfo.getReferralCode();
        Long referrerUserId = referrerUserInfo.getUserId();

        if (referralStep.equals(ReferralStep.FIRST_SESSION) && !checkIfFirstSession(userId)) {
            return;
        }

        List<ReferralStep> stepsDone = new ArrayList<>();
        Referral referral = referralDao.getByUserIds(userId, referrerUserId);

        if (referral == null) {//if userId, referrerUserId were not involved earlier, assign the latest policy to their combo
            Long referralPolicyId = referralStepBonusDao.getLatestPolicyId();
            referral = new Referral(userId, referrerUserId, referralCode, referralPolicyId, stepsDone, 0l, 0l);
            //referral.setCreatedBy(userId.toString());
            //referral.setLastUpdatedBy(userId.toString());
            referral.setCreationTime(System.currentTimeMillis());
            referral.setLastUpdated(System.currentTimeMillis());
            referralDao.upsert(referral, userId.toString());
        }

        stepsDone = referral.getStepsDone();

        if (stepsDone.contains(referralStep)) {// add check here for new step and count
            return;
        }

        if (referralStep.equals(ReferralStep.FIRST_PURCHASE)) {
            if (referral.getStepsDone().contains(ReferralStep.FIRST_OTF_PURCHASE)
                    || referral.getStepsDone().contains(ReferralStep.FIRST_PURCHASE)) {
                return;
            }
        }

        if (referralStep.equals(ReferralStep.FIRST_OTF_PURCHASE)) {
            if (referral.getStepsDone().contains(ReferralStep.FIRST_PURCHASE)
                    || referral.getStepsDone().contains(ReferralStep.FIRST_OTF_PURCHASE)) {
                return;
            }
        }
        
        ReferralStepBonus referralStepBonus = referralStepBonusDao.getPolicyStep(referral.getReferralPolicyId(),
                referralStep, entityId);

        if(referralStepBonus == null){
        	referralStepBonus = referralStepBonusDao.getPolicyStep(referral.getReferralPolicyId(),
                    referralStep, null);
        }
        ///////////////NEW PART
        boolean maxReferred = false;
        if(referralStep.equals(ReferralStep.NEW_BUNDLE_PURCHASE)){
        	if(StringUtils.isNotEmpty(entityId)){
        		List<Referral> referrals = referralDao.getByReferrerId(referrerUserId, referralStep,entityId);
        		if(ArrayUtils.isNotEmpty(referrals) && referralStepBonus != null && referralStepBonus.getMaxReferrals() != null
        				&& referrals.size() >= referralStepBonus.getMaxReferrals()){
        			maxReferred = true;
        			logger.info("ReferrerId : "+ referrerUserId + " has got max referral bonus for entityId : " + entityId);
        			
        		}
        	}else {
    			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Entity id is missing for referral");
    		}
        	referral.setEntityId(entityId);
        }
        stepsDone.add(referralStep);


        if (referralStepBonus != null) {
            try {
                if (referralStepBonus.getReferreeBonusAmount() != 0) {
                    addAmountToWallet(referralStep, userId, referralStepBonus.getReferreeBonusAmount() * 100);
                    referral.setTotalAmountToReferee(
                            referral.getTotalAmountToReferee() + referralStepBonus.getReferreeBonusAmount());
                }
            } catch (Exception e) {
                logger.error("Failed to add referral Credits to referree:" + referral.toString() + "ReferralStep : "
                        + referralStep);
                throw e;
            }

            try {
                if (referralStepBonus.getReferrerBonusAmount() != 0 && !maxReferred) {
                    addAmountToWallet(referralStep, referrerUserId, referralStepBonus.getReferrerBonusAmount() * 100);
                    referral.setTotalAmountToReferrer(
                            referral.getTotalAmountToReferrer() + referralStepBonus.getReferrerBonusAmount());
                }
            } catch (Exception e) {
                logger.error("Failed to add referral Credits to referrer:" + referral.toString() + "ReferralStep : "
                        + referralStep);
                throw e;
            }

            referral.setStepsDone(stepsDone);

            referralDao.upsert(referral);

            if(maxReferred){
            	return;
            }
            try {
                referralStepBonus = getReferralStepBonusByReferralsId(userId, referrerUserId, referralStep);
                Map<String, Object> payload = new HashMap<>();
                payload.put("userInfo", userInfo);
                payload.put("referrerUserInfo", referrerUserInfo);
                payload.put("referralStep", referralStep);
                payload.put("referralStepBonus", referralStepBonus);
                if(ReferralStep.NEW_BUNDLE_PURCHASE.equals(referralStep)){
                	payload.put("bundleId", entityId);
                	AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REFERRAL_EMAIL_BUNDLE, payload);
                    asyncTaskFactory.executeTask(params);
                    return;
                }
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REFERRAL_EMAIL, payload);
                asyncTaskFactory.executeTask(params);

                // emailManager.sendReferralEmail(userInfo, referrerUserInfo,
                // referralStep, referralStepBonus);
            } catch (Exception e) {
                logger.error("Exception :", e);
            }
        } else {
            logger.info("No referral to add Credits fr this step:" + referral.toString() + "ReferralStep : "
                    + referralStep);
        }
    }

    public List<ReferralStepBonus> getReferralStepBonuses() {

        Long referralPolicyId = referralStepBonusDao.getLatestPolicyId();

        List<ReferralStepBonus> referralStepBonuses = referralStepBonusDao.getAllByPolicyId(referralPolicyId);

        return referralStepBonuses;

    }

    public List<ReferralStepBonus> addOrUpdateReferralStepBonus(List<ReferralStepBonus> referralStepBonuses,
            Long callingUserId) throws VException {

        // List<ReferralStepBonus> listSteps = getReferralStepBonuses();
        List<ReferralStepBonus> newListSteps = new ArrayList<ReferralStepBonus>();

        for (ReferralStepBonus referralStepBonus : referralStepBonuses) {
            // Boolean added = false;
            // if(listSteps!=null && !listSteps.isEmpty()){
            // for(ReferralStepBonus stepBonus : listSteps){
            // if(referralStepBonus.getReferralStep().equals(stepBonus.getReferralStep())){
            // newListSteps.add(getNewUpdatedReferralStepBonus(referralStepBonus));
            // added = true;
            // }else{
            // newListSteps.add(getNewUpdatedReferralStepBonus(stepBonus));
            // }
            // }
            // }
            //
            // if(!added){
            newListSteps.add(getNewUpdatedReferralStepBonus(referralStepBonus));
            // }
            // }
        }
        Boolean result = referralStepBonusDao.updateAll(newListSteps, callingUserId != null ? callingUserId.toString() : null);

        if (result) {
            return newListSteps;
        } else {
            throw new VException(ErrorCode.SERVICE_ERROR, "updating failed for referral steps");
        }
    }

    private ReferralStepBonus getNewUpdatedReferralStepBonus(ReferralStepBonus referralStepBonus) {

        if (referralStepBonus.getReferralPolicyId() == null) {
            referralStepBonus.setReferralPolicyId(0l);
        }
        ReferralStepBonus stepBonus = new ReferralStepBonus(referralStepBonus.getReferralPolicyId() + 1,
                referralStepBonus.getReferralStep(), referralStepBonus.getReferrerBonusAmount(),
                referralStepBonus.getReferreeBonusAmount());
        //stepBonus.setCreatedBy(callingUserId.toString());
        //stepBonus.setLastUpdatedBy(callingUserId.toString());
        if(referralStepBonus != null){
        	if(StringUtils.isNotEmpty(referralStepBonus.getEntityId())){
        		stepBonus.setEntityId(referralStepBonus.getEntityId());
        	}
        	if(referralStepBonus.getMaxReferrals() != null){
        		stepBonus.setMaxReferrals(referralStepBonus.getMaxReferrals());
        	}
        }
        stepBonus.setCreationTime(System.currentTimeMillis());
        stepBonus.setLastUpdated(System.currentTimeMillis());

        return stepBonus;
    }

    private void addAmountToWallet(ReferralStep referralStep, Long userId, Long bonusAmount) throws VException {

        //AccountController accountController = new AccountController();
        TransferFromFreebiesAccountReq transferFromFreebiesAccountReq = new TransferFromFreebiesAccountReq(userId,
                bonusAmount.intValue());
        transferFromFreebiesAccountReq.setTransactionRefType(TransactionRefType.REFERRAL);
        transferFromFreebiesAccountReq.setReasonNote(referralStep.name());
        transferFromFreebiesAccountReq.setNoAlert(true);
        transferFromFreebiesAccountReq.setCallingUserId(userId);
        transferFromFreebiesAccountReq.setAllowNegativeBalance(true);
        transferFromFreebiesAccountReq.setBillingReasonType(BillingReasonType.FREEBIES);
        transferFromFreebiesAccountReq.setTags(Arrays.asList(referralStep.name()));
        accountManager.transferFromFreebiesAccount(transferFromFreebiesAccountReq);
    }

    public List<ReferralStepResponse> getAllReferralSteps() {
        List<ReferralStepResponse> referralSteps = new ArrayList<>();
        for (ReferralStep status : ReferralStep.values()) {
            ReferralStepResponse referralStepResponse = new ReferralStepResponse();
            referralStepResponse.setName(status);
            referralStepResponse.setValue(ReferralStep.getStatusString(status));
            referralSteps.add(referralStepResponse);
        }
        return referralSteps;
    }

    public ReferralStepBonus getReferralStepBonusByReferralsId(Long referreeId, Long referrerId,
            ReferralStep referralStep) throws VException {

        Referral referral = referralDao.getByUserIds(referreeId, referrerId);

        if (referral == null) {
            logger.error("unable to fetch referral. referreeId:" + referreeId + "referrerId : " + referrerId);
            throw new VException(ErrorCode.SERVICE_ERROR, "unable to fetch referral");
        }

        List<ReferralStepBonus> referralStepBonuses = new ArrayList<>();
        if(referralStep.equals(ReferralStep.NEW_BUNDLE_PURCHASE) && StringUtils.isNotEmpty(referral.getEntityId())){
        	referralStepBonuses = referralStepBonusDao
                    .getStepByPolicyId(referral.getReferralPolicyId(), referralStep, referral.getEntityId() );
        	
        }
        if(ArrayUtils.isEmpty(referralStepBonuses)){
        	referralStepBonuses = referralStepBonusDao
                    .getStepByPolicyId(referral.getReferralPolicyId(), referralStep, null);
        }
        

        if (referralStepBonuses.isEmpty()) {
            logger.error("unable to fetch referral Step bonus for policy id :" + referral.getReferralPolicyId()
                    + " . referreeId:" + referreeId + "referrerId : " + referrerId);
            throw new VException(ErrorCode.SERVICE_ERROR, "unable to fetch referral step bonus");
        } else {
            return referralStepBonuses.get(0);
        }
    }

    private boolean checkIfFirstSession(Long studentId)
            throws IllegalArgumentException, IllegalAccessException, VException {
        // We consider the no of ended sessions to conclude first session
        boolean rewardCredits = false;
        int sessionCountForRenewCredits = 5;
        int validSessionCount = 0;

        GetSessionsReq getSessionsReq = new GetSessionsReq();
        getSessionsReq.setIncludeAttendees(true);
        getSessionsReq.setStudentId(studentId);

        List<SessionState> states = new ArrayList<>();
        states.add(SessionState.ENDED);
        getSessionsReq.setSessionStates(states);
        getSessionsReq.setSize(10);
        List<SessionInfo> sessionInfos = sessionManager.getSessionsInfo(getSessionsReq);
        //TODO use billing period instead of joined logic
        if (ArrayUtils.isNotEmpty(sessionInfos)) {
            for (SessionInfo sessionInfo : sessionInfos) {
                int joinCount = 0;
                if (sessionInfo != null && ArrayUtils.isNotEmpty(sessionInfo.getAttendees())) {
                    List<UserSessionInfo> attendees = sessionInfo.getAttendees();
                    for (UserSessionInfo attendee : attendees) {
                        if (SessionAttendee.SessionUserState.JOINED.equals(attendee.getUserState())) {
                            joinCount++;
                        }
                    }
                    if (joinCount == attendees.size()) {
                        validSessionCount++;
                    }
                }
            }
        }
        logger.info("validSessionCount for OTO " + validSessionCount);
        if (validSessionCount > sessionCountForRenewCredits) {//not need to check OTF
            return rewardCredits;
        }
        logger.info("now checking for OTF sessions");

        List<GTTAttendeeDetailsInfo> details = otfManager.getJoinedAttendeeDetailsOfUser(studentId.toString(), 0, 10);
        logger.info("otf attendee details " + details);
        if (details != null) {//expecting a non null response all the times,
            //cannot credit to user in case of service error
            validSessionCount += details.size();
            logger.info("validSessionCount for OTF+OTO " + validSessionCount);
            if (validSessionCount == sessionCountForRenewCredits) {
                rewardCredits = true;
            }
        }
        return rewardCredits;
    }

    private ReferralInfoRes getReferralInfo(Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user id found");
        }
        String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/getReferralInfo?userId=" + userId,
                HttpMethod.GET, null,true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        ReferralInfoRes response = new Gson().fromJson(jsonString, ReferralInfoRes.class);
        return response;
    }
    
    
    public ReferralStepBonus getReferralStepBonusesForEntityId(String entityId, ReferralStep referralStep) throws BadRequestException {

    	if(referralStep != null && ReferralStep.NEW_BUNDLE_PURCHASE.equals(referralStep) && StringUtils.isEmpty(entityId)){
    		throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Bad Request error entityId is null");
    	}
        Long referralPolicyId = referralStepBonusDao.getLatestPolicyId();

        ReferralStepBonus referralStepBonus = referralStepBonusDao.getPolicyStep(referralPolicyId, referralStep, entityId);

        return referralStepBonus;

    }
//        public static void main(String[] args){
//            System.err.println(">>> it>????? ");
//            String s="{\"userInfo\":{\"userId\":22,\"profileEnabled\":true,\"email\":\"ajith+13@vedantu.com\",\"contactNumber\":\"7382447382\",\"phoneCode\":\"91\",\"firstName\":\"Ajtih\",\"lastName\":\"\",\"gender\":\"UNKNOWN\",\"role\":\"STUDENT\",\"profilePicUrl\":null,\"languagePrefs\":null,\"info\":{\"grade\":\"6\",\"board\":null,\"school\":null,\"parentContactNo\":null,\"parentContactNumbers\":[],\"parentEmails\":[]},\"locationInfo\":{\"country\":\"\",\"state\":\"\",\"city\":\"\",\"street\":null,\"pincode\":null},\"socialInfo\":{\"linkedIn\":null,\"facebook\":null,\"twitter\":null,\"profileLine\":null},\"isEmailVerified\":false,\"isContactNumberVerified\":false,\"creationTime\":1482911240219,\"utm\":{\"utm_source\":\"\",\"utm_medium\":\"\",\"utm_campaign\":\"\",\"utm_term\":\"\",\"utm_content\":\"\",\"channel\":\"\"},\"ratingInfo\":null,\"parentRegistration\":false,\"primaryCallingNumberForTeacher\":null,\"primaryBoard\":null,\"secondaryBoard\":null,\"signupInfo\":{\"signUpUrl\":null,\"signupFeature\":null},\"referralCode\":\"ajti1f0e\"},\"referrerUserInfo\":{\"userId\":2,\"profileEnabled\":true,\"email\":\"ajith+1@vedantu.com\",\"contactNumber\":\"7382447382\",\"phoneCode\":\"91\",\"firstName\":\"Ajith\",\"lastName\":\"Redd\",\"gender\":\"MALE\",\"role\":\"STUDENT\",\"profilePicUrl\":null,\"languagePrefs\":null,\"info\":{\"grade\":\"7\",\"board\":\"ICSE\",\"school\":null,\"parentContactNo\":null,\"parentContactNumbers\":[\"\"],\"parentEmails\":[\"\"]},\"locationInfo\":{\"country\":\"\",\"state\":\"\",\"city\":\"\",\"street\":null,\"pincode\":null},\"socialInfo\":{\"linkedIn\":null,\"facebook\":null,\"twitter\":null,\"profileLine\":null},\"isEmailVerified\":true,\"isContactNumberVerified\":true,\"creationTime\":1478864749021,\"utm\":{\"utm_source\":null,\"utm_medium\":null,\"utm_campaign\":null,\"utm_term\":null,\"utm_content\":null,\"channel\":null},\"ratingInfo\":null,\"parentRegistration\":false,\"primaryCallingNumberForTeacher\":null,\"primaryBoard\":null,\"secondaryBoard\":null,\"signupInfo\":{\"signUpUrl\":null,\"signupFeature\":null},\"referralCode\":\"ajit6be5\",\"teacherInfo\":null,\"studentInfo\":null}}";
//            ReferralInfoRes response = new Gson().fromJson(s, ReferralInfoRes.class);
//            System.err.println(">>> it isone "+response);
//        }
//        
}
