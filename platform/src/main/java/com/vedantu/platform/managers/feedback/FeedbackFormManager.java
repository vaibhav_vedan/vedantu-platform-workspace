/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.feedback;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.platform.dao.feedback.FeedbackFormDAO;
import com.vedantu.platform.dao.feedback.FeedbackFormResponseDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.mongodbentities.feedback.FeedbackForm;
import com.vedantu.platform.mongodbentities.feedback.FeedbackFormResponse;
import com.vedantu.platform.mongodbentities.feedback.FeedbackToSlack;
import com.vedantu.platform.pojo.feedback.FeedbackFormDynamicSection;
import com.vedantu.platform.pojo.feedback.FormNamePojo;
import com.vedantu.platform.pojo.feedback.StudentSharedFormInfo;
import com.vedantu.platform.request.feedback.SubmitFeedbackResponseReq;
import com.vedantu.platform.response.feedback.GetFormsSharedWithStudentResponse;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.GetCourseIdByUsingUserIdBatchIdsReq;
import com.vedantu.util.*;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import java.net.URLEncoder;
/**
 *
 * @author parashar
 */
@Service
public class FeedbackFormManager {
    
    @Autowired
    private FeedbackFormDAO feedbackFormDAO;
    
    @Autowired
    private FeedbackFormResponseDAO feedbackFormResponseDAO;
    
    @Autowired
    private LogFactory logfactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(FeedbackFormManager.class);
    
    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private final Gson gson = new Gson();

    
    public void createFeedbackForm(FeedbackForm feedbackForm){
        
    }
    
    
    public BatchBasicInfo getBatchInfo(String batchId) throws VException{       
        String url = subscriptionEndPoint + "/batch/basicInfo/"+batchId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        // Type enrollmentsListType = new TypeToken<List<EnrollmentPojo>>(){}.getType();
        BatchBasicInfo batchBasicInfo = gson.fromJson(jsonString, BatchBasicInfo.class);
        return batchBasicInfo;        
    }      
    
    public PlatformBasicResponse submitFeedbackFormResponse(SubmitFeedbackResponseReq req, Long userId) throws ForbiddenException, ConflictException{
     
        FeedbackForm feedbackForm = feedbackFormDAO.getFormForUserAndId(req.getFeedbackFormId(), userId, EntityStatus.ACTIVE);
        req.setUserId(userId);
        if(feedbackForm == null){
            throw new ForbiddenException(ErrorCode.FEEDBACK_FORM_SUBMISSION_NOT_ALLOWED, "Feedback response submission not allowed for user");
        }
        
        if(feedbackForm.getRespondedBy().contains(userId)){
            throw new ForbiddenException(ErrorCode.FEEDBACK_FORM_ALREADY_SUBMITED, "Form already submitted");
        }
        
        Long currentTime = System.currentTimeMillis();
        if(feedbackForm.getExpiresIn() < currentTime - feedbackForm.getCreationTime()){
            feedbackForm.setStatus(EntityStatus.ENDED);
            feedbackFormDAO.save(feedbackForm);
            throw new ConflictException(ErrorCode.FEEDBACK_FORM_EXPIRED, "Feedback for has expired");
        }
        
        FeedbackFormResponse feedbackFormResponse = new FeedbackFormResponse();
        feedbackFormResponse.setContextId(feedbackForm.getContextId());
        feedbackFormResponse.setContextType(feedbackForm.getContextType());
        feedbackFormResponse.setFeedbackFormDynamicSections(req.getFeedbackFormDynamicSections());
        feedbackFormResponse.setFormId(req.getFeedbackFormId());
        feedbackFormResponse.setStaticQuestions(req.getStaticQuestions());
        feedbackFormResponse.setUserId(req.getUserId());
        feedbackFormResponse.setParentFormId(feedbackForm.getParentFormId());
        feedbackFormResponse.setFeedbackCommunication(req.getFeedbackCommunication());
        feedbackFormResponseDAO.save(feedbackFormResponse);
        
        feedbackForm.getRespondedBy().add(req.getUserId());
        feedbackFormDAO.save(feedbackForm);
        
        return new PlatformBasicResponse(true, null, null);
    }
    
    public FeedbackForm getFeedbackFormToSubmit(String formId, Long userId) throws NotFoundException, ForbiddenException{
        
        FeedbackForm feedbackForm = feedbackFormDAO.getFormForUserAndId(formId, userId, EntityStatus.ACTIVE);
        
        if(feedbackForm == null){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Form to be submitted not found for user");
        }
        if(feedbackForm.getRespondedBy().contains(userId)){
            throw new ForbiddenException(ErrorCode.FEEDBACK_FORM_ALREADY_SUBMITED, "Form already submitted");
        }  
        
        List<FeedbackFormDynamicSection> sections = new ArrayList<>();
        
        if(ArrayUtils.isNotEmpty(feedbackForm.getFeedbackFormDynamicSections())){
            for(FeedbackFormDynamicSection feedbackFormDynamicSection : feedbackForm.getFeedbackFormDynamicSections()){
                if(feedbackFormDynamicSection.getUserIds().contains(userId) && ArrayUtils.isNotEmpty(feedbackFormDynamicSection.getQuestions())){
                    feedbackFormDynamicSection.setUserIds(new HashSet<>());
                    sections.add(feedbackFormDynamicSection);
                }
            }
        }
        feedbackForm.setFeedbackFormDynamicSections(sections);
        
        if(ArrayUtils.isEmpty(feedbackForm.getStaticQuestions()) && (ArrayUtils.isEmpty(feedbackForm.getFeedbackFormDynamicSections()))){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No form to submit");
        }
        
        return feedbackForm;
    }
    
    public GetFormsSharedWithStudentResponse getNotAnsweredFormForStudent(Long userId){
        GetFormsSharedWithStudentResponse getFormsSharedWithStudentResponse = new GetFormsSharedWithStudentResponse();
        Long currentTime = System.currentTimeMillis();
        List<FeedbackForm> feedbackForms = feedbackFormDAO.getFeedbackFormNotRespondedByUser(userId, EntityStatus.ACTIVE, Arrays.asList(FeedbackCommunication.WEBSITE, FeedbackCommunication.BOTH));
        
        if(ArrayUtils.isEmpty(feedbackForms)){
            return getFormsSharedWithStudentResponse;
        }
        logger.info("Feedback Forms not answered by user: "+feedbackForms);
        List<StudentSharedFormInfo> studentSharedFormInfos = new ArrayList<>();
        for(FeedbackForm feedbackForm : feedbackForms){
            
            if(FeedbackCommunication.EMAIL.equals(feedbackForm.getFeedbackCommunication())){
                continue;
            }
            
            if(feedbackForm.getExpiresIn()!=null && feedbackForm.getExpiresIn() >= currentTime - feedbackForm.getCreationTime()){
                StudentSharedFormInfo studentSharedFormInfo = new StudentSharedFormInfo();
                studentSharedFormInfo.setContextId(feedbackForm.getContextId());
                studentSharedFormInfo.setContextType(feedbackForm.getContextType());
                studentSharedFormInfo.setFeedbackFormType(feedbackForm.getFeedbackFormType());
                studentSharedFormInfo.setFormName(feedbackForm.getFormTitle());
                studentSharedFormInfo.setFormId(feedbackForm.getId());
                studentSharedFormInfo.setResponded(false);
                if(StringUtils.isEmpty(feedbackForm.getContextName()) && EntityType.OTF.equals(feedbackForm.getContextType())){
                    try{
                        studentSharedFormInfo.setContextName(getBatchInfo(feedbackForm.getContextId()).getCourseInfo().getTitle());
                    }catch(Exception ex){
                        logger.error("Error in setting context name: "+ex.getMessage());
                    }
                }
                /* if(ArrayUtils.isNotEmpty(feedbackForm.getFormNames()) && EntityType.OTM_BUNDLE.equals(feedbackForm.getContextType())){
                    boolean isSet = false;
                    for(FormNamePojo formNamePojo : feedbackForm.getFormNames()){
                        if(feedbackForm.getContextId().equals(formNamePojo.getContextId())){
                            studentSharedFormInfo.setContextName(formNamePojo.getContextName());
                            isSet = true;
                            break;
                        }
                    }
                    if(!isSet){
                        continue;
                    }
                } */
                
                if(!ArrayUtils.isEmpty(feedbackForm.getFeedbackFormDynamicSections()) && EntityType.OTM_BUNDLE.equals(feedbackForm.getContextType())){
                    boolean isBundle = true;
                    for(FeedbackFormDynamicSection feedbackFormDynamicSection : feedbackForm.getFeedbackFormDynamicSections()){
                        if(!feedbackFormDynamicSection.getUserIds().contains(userId)){
                            isBundle = false;
                            break;
                        }
                    }
                    if(!isBundle){
                        continue;
                    }else{
                        
                        studentSharedFormInfo.setContextName(feedbackForm.getFormTitle());
                        
                    }
                }
                
                if(StringUtils.isEmpty(studentSharedFormInfo.getContextName())){
                    studentSharedFormInfo.setContextName(feedbackForm.getFormTitle());
                }
                
                studentSharedFormInfo.setSharedOn(feedbackForm.getCreationTime());
                studentSharedFormInfos.add(studentSharedFormInfo);
            }
            
        }
        getFormsSharedWithStudentResponse.setStudentSharedFormInfos(studentSharedFormInfos);
        return getFormsSharedWithStudentResponse;
    }
    
    public void expireFeedbackForms(){
        Long currentTime = System.currentTimeMillis();
        List<FeedbackForm> feedbackForms = feedbackFormDAO.getActiveForms();
        
        if(ArrayUtils.isNotEmpty(feedbackForms)){
            for(FeedbackForm feedbackForm : feedbackForms){
                if(feedbackForm.getExpiresIn() + feedbackForm.getCreationTime() > currentTime){
                    feedbackForm.setStatus(EntityStatus.INACTIVE);
                    feedbackFormDAO.save(feedbackForm);
                }
            }
        }
              
    }
    
    public GetFormsSharedWithStudentResponse getFeedbackFormsForBatchId(String batchId, Long userId) throws VException{
        BatchBasicInfo basicInfo = getBatchInfo(batchId);
        List<FeedbackForm> feedbackForms;
        if(basicInfo == null){
            throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, batchId);
        }
        if(StringUtils.isEmpty(basicInfo.getGroupName())){
            feedbackForms = feedbackFormDAO.getFeedbackFormNotRespondedByUser(userId, EntityStatus.ACTIVE, Arrays.asList(FeedbackCommunication.WEBSITE, FeedbackCommunication.BOTH), Arrays.asList(batchId));
        }else{
            feedbackForms = feedbackFormDAO.getFeedbackFormNotRespondedByUser(userId, EntityStatus.ACTIVE, Arrays.asList(FeedbackCommunication.WEBSITE, FeedbackCommunication.BOTH), Arrays.asList(batchId, basicInfo.getGroupName()));            
        }
        /*Set<String> groupNames = new HashSet<>();
        for(FeedbackForm feedbackForm : feedbackForms){
            if(EntityType.OTM_BUNDLE.equals(feedbackForm.getContextType())){
                
                groupNames.add(feedbackForm.getContextId());
                
            }
        }
        
        List<BatchBasicInfo> batchInfos = getBatchBasicInfosForGroupName(new ArrayList<>(groupNames));
        
        groupNames = new HashSet<>();
        for(BatchBasicInfo basicInfo : batchInfos){
            if(basicInfo.getId().equals(batchId)){
                groupNames.add(basicInfo.getGroupName());
            }
        }*/
        Long currentTime = System.currentTimeMillis();
        List<StudentSharedFormInfo> studentSharedFormInfos = new ArrayList<>();
        for(FeedbackForm feedbackForm : feedbackForms){
                if(feedbackForm.getExpiresIn()!=null && feedbackForm.getExpiresIn() >= currentTime - feedbackForm.getCreationTime()){
                    StudentSharedFormInfo studentSharedFormInfo = new StudentSharedFormInfo();
                    studentSharedFormInfo.setContextId(feedbackForm.getContextId());
                    studentSharedFormInfo.setContextType(feedbackForm.getContextType());
                    studentSharedFormInfo.setFeedbackFormType(feedbackForm.getFeedbackFormType());
                    studentSharedFormInfo.setFormName(feedbackForm.getFormTitle());
                    
                    /*if(ArrayUtils.isNotEmpty(feedbackForm.getFormNames())){
                        boolean isNameSet = false;
                        for(FormNamePojo formNamePojo : feedbackForm.getFormNames()){
                            if(formNamePojo.getContextId().equals(batchId)){
                                studentSharedFormInfo.setContextName(formNamePojo.getContextName());
                                isNameSet = true;
                                break;
                            }
                        }
                        if(!isNameSet){
                            continue;
                        }
                    }
                    */
                    studentSharedFormInfo.setContextName(basicInfo.getCourseInfo().getTitle());
                    if(ArrayUtils.isNotEmpty(feedbackForm.getFeedbackFormDynamicSections()) && EntityType.OTM_BUNDLE.equals(feedbackForm.getContextType())){
                        boolean isBundle = true;
                        for(FeedbackFormDynamicSection feedbackFormDynamicSection : feedbackForm.getFeedbackFormDynamicSections()){
                            if(!feedbackFormDynamicSection.getUserIds().contains(userId)){
                                isBundle = false;
                                break;
                            }
                        }
                        if(isBundle){
                            /* if(ArrayUtils.isNotEmpty(feedbackForm.getFormNames())){
                                boolean isNameSet = false;
                                for(FormNamePojo formNamePojo : feedbackForm.getFormNames()){
                                    if(formNamePojo.getContextId().equals(feedbackForm.getContextId())){
                                        studentSharedFormInfo.setContextName(formNamePojo.getContextName());
                                        isNameSet = true;
                                        break;
                                    }
                                }
                                if(!isNameSet){
                                    continue;
                                }
                            }  */ 
                            studentSharedFormInfo.setContextName(feedbackForm.getFormTitle());
                        }
                    }
                    
                    
                    
                    studentSharedFormInfo.setFormId(feedbackForm.getId());
                    studentSharedFormInfo.setResponded(false);
                    studentSharedFormInfo.setSharedOn(feedbackForm.getCreationTime());
                    studentSharedFormInfos.add(studentSharedFormInfo);
                }                
        }
        GetFormsSharedWithStudentResponse getFormsSharedWithStudentResponse = new GetFormsSharedWithStudentResponse();
        getFormsSharedWithStudentResponse.setStudentSharedFormInfos(studentSharedFormInfos);
        
        return getFormsSharedWithStudentResponse;
    }
    
    public List<BatchBasicInfo> getBatchBasicInfosForGroupName(List<String> groupNames) throws VException{
        String url = subscriptionEndPoint + "/batch/getBatchBasicInfosForMultipleGroupNames?groupNames="+WebUtils.getUrlEncodedValue(String.join(",", groupNames));
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type batchBasicInfoListType = new TypeToken<List<BatchBasicInfo>>(){}.getType();
        List<BatchBasicInfo> batchBasicInfos = gson.fromJson(jsonString, batchBasicInfoListType);
        return batchBasicInfos;        
    }

    public void sendFeedbackToSlack(String reqString,String subject) throws UnsupportedEncodingException, VException {
        if(StringUtils.isNotEmpty(reqString)){
            FeedbackToSlack feedbackToSlack = gson.fromJson(reqString ,FeedbackToSlack.class);
            if( feedbackToSlack == null){
                return;
            }
            if(StringUtils.isNotEmpty(feedbackToSlack.getComment()) && feedbackToSlack.getComment().length() < 50){
                return;
            }
            if(feedbackToSlack.getUserId() != null && feedbackToSlack.getSessionId() != null){
                String redisKey = "SEND_FEEDBACK_TO_SLACK_" + feedbackToSlack.getSessionId() + "_" + feedbackToSlack.getUserId();
                logger.info("redisKey for feedback send to slack : " + redisKey);
                if(redisDAO.get(redisKey) == null){
                    redisDAO.setex( redisKey,String.valueOf(System.currentTimeMillis()),180);
                }else{
                    logger.warn("session : "+feedbackToSlack.getSessionId() + " userId : "+feedbackToSlack.getUserId() + " OTF_SESSION_FEEDBACK_GENERATED Feedback sns got multiple times");
                    return;
                }
            }
            logger.info(feedbackToSlack.toString());
            String url = ConfigUtils.INSTANCE.getStringValue("slack.post.api.url");
//            logger.info("url : " + url);
            String token = ConfigUtils.INSTANCE.getStringValue("send.sessionfeedback.slack.token");
            url += "?token=" + token;
//            logger.info("token : " + token);
            String channel = ConfigUtils.INSTANCE.getStringValue("send.sessionfeedback.slack.channel");
            url += "&channel=" + channel;
//            logger.info("channel : " + channel);
//            url += "&as_user=false&username=Voice%20of%20Student";
            OTFSessionInfo sessionInfo =null;
            if(StringUtils.isNotEmpty(feedbackToSlack.getSessionId())){
                ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/onetofew/session/" + feedbackToSlack.getSessionId(), HttpMethod.GET,
                        null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                sessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);
                logger.info("sessionInfo : " + sessionInfo.toString());
            }
            List<String> userIds = new ArrayList<>();
            if(StringUtils.isNotEmpty(feedbackToSlack.getUserId())){
                userIds.add(feedbackToSlack.getUserId());
            }
            if(sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getPresenter())){
                userIds.add(sessionInfo.getPresenter());
            }
            logger.info("userIds List : " + userIds );
            Map<String, UserBasicInfo> userIdInfoMap = null;
            if(ArrayUtils.isNotEmpty(userIds)){
                userIdInfoMap = fosUtils.getUserBasicInfosMap(userIds,true);
                logger.info("users List : " + userIdInfoMap.toString());
            }

            CourseInfo courseInfo = null;
            Set<String> grades = new HashSet<>();
            if(sessionInfo != null && ArrayUtils.isNotEmpty(sessionInfo.getBatchIds()) && StringUtils.isNotEmpty(feedbackToSlack.getUserId())) {
                GetCourseIdByUsingUserIdBatchIdsReq req = new GetCourseIdByUsingUserIdBatchIdsReq();
                req.setBatchIds(sessionInfo.getBatchIds());
                req.setUserId(feedbackToSlack.getUserId());
                String courseUrl = subscriptionEndPoint + "/batch/getCourseIdByUsingUserIdBatchIds";
                ClientResponse resp = WebUtils.INSTANCE.doCall(courseUrl, HttpMethod.POST, new Gson().toJson(req));
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String courseId = resp.getEntity(String.class);
                if (StringUtils.isNotEmpty(courseId)) {
                    courseInfo = otfManager.getCourse(courseId);
                    logger.info("courseInfo : "+courseInfo);
                }
            }
            if(courseInfo != null && ArrayUtils.isNotEmpty(courseInfo.getGrades())){
                for(String grade : courseInfo.getGrades()){
                    grades.add(grade);
                }
            }


            if(userIdInfoMap != null && StringUtils.isNotEmpty(feedbackToSlack.getUserId()) && userIdInfoMap.get(feedbackToSlack.getUserId())!=null){
                if(StringUtils.isNotEmpty(userIdInfoMap.get(feedbackToSlack.getUserId()).getProfilePicUrl())){
                    url +="&icon_url=" + URLEncoder.encode(userIdInfoMap.get(feedbackToSlack.getUserId()).getProfilePicUrl(),"UTF-8");
                }
                else {
                    url +="&icon_url=" + URLEncoder.encode("https://cdn3.iconfinder.com/data/icons/education-2-2/256/Student_Reading-512.png","UTF-8");
                }
                if(StringUtils.isNotEmpty(userIdInfoMap.get(feedbackToSlack.getUserId()).getFullName())){
                    url += "&as_user=false&username="+URLEncoder.encode(userIdInfoMap.get(feedbackToSlack.getUserId()).getFullName(),"UTF-8");
                }
                else {
                    url += "&as_user=false&username="+ feedbackToSlack.getUserId();
                }
            }else {
                url += "&as_user=false&username="+ feedbackToSlack.getUserId();
                url +="&icon_url=" + URLEncoder.encode("https://cdn3.iconfinder.com/data/icons/education-2-2/256/Student_Reading-512.png","UTF-8");
            }
            String text = "";
            if(StringUtils.isNotEmpty(feedbackToSlack.getComment()) && feedbackToSlack.getComment().length() > 50){
                List<String> badWordList = Arrays.asList(ConfigUtils.INSTANCE.getStringValue("slack.comment.abusive.words").split(","));
                String comment = feedbackToSlack.getComment();
                for(String badWord : badWordList){
                    if(comment.contains(" " + badWord + " ")){
                        comment = comment.replaceAll(badWord, " ****** ");
                    }
                }
                if (sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getTitle())) {
                    text += "*" + sessionInfo.getTitle() +" Session Feedback*\n_";
                }else {
                    if(StringUtils.isNotEmpty(feedbackToSlack.getUserId())){
                        text += "*" + feedbackToSlack.getUserId() +"*\n_";
                    }
                }
                if(ArrayUtils.isNotEmpty(grades)){
                    text += "Grades : " + String.join(",",grades) + "  :zap:  ";
                }
                if(StringUtils.isNotEmpty(feedbackToSlack.getSessionId())){
                    text += "SessionId : " + feedbackToSlack.getSessionId();
                }
                text += "_\n";
                if(feedbackToSlack.getRating()>0){
                    for(int i=0; i<feedbackToSlack.getRating();i++){
                        text += ":star: ";
                    }
                    for(int i=0; i<5-feedbackToSlack.getRating();i++){
                        text += ":empty_star: ";
                    }
//                    text +="`"+feedbackToSlack.getRating()+"/5`\n";
                    text += "\n";
                }
                text +=":writing_hand: " + comment + "\n";
                logger.info("text : "+text);
                if (userIdInfoMap != null && sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getPresenter()) && userIdInfoMap.get(sessionInfo.getPresenter())!=null && StringUtils.isNotEmpty(userIdInfoMap.get(sessionInfo.getPresenter()).getFullName())) {
                    for(int i=0;i<27;i++){
                        text += "     ";
                    }
                    text += ":point_right:   Teacher : *@" + userIdInfoMap.get(sessionInfo.getPresenter()).getFullName() + "*";
                }
            }




//            if(StringUtils.isNotEmpty(feedbackToSlack.getComment()) && feedbackToSlack.getComment().length() > 50){
//                text += ":speech_balloon: *OTF session Feedback*";
//                if (sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getTitle())) {
//                    text += "  ( " + sessionInfo.getTitle() + " )";
//                }
//                text += "\n```";
//                if(userIdInfoMap != null && StringUtils.isNotEmpty(feedbackToSlack.getUserId()) && userIdInfoMap.get(feedbackToSlack.getUserId())!=null && StringUtils.isNotEmpty(userIdInfoMap.get(feedbackToSlack.getUserId()).getFullName())){
//                    text += " • Student Name : " + userIdInfoMap.get(feedbackToSlack.getUserId()).getFullName();
//                    text += "\n";
//                }
//                if (userIdInfoMap != null && sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getPresenter()) && userIdInfoMap.get(sessionInfo.getPresenter())!=null && StringUtils.isNotEmpty(userIdInfoMap.get(sessionInfo.getPresenter()).getFullName())) {
//                    text += " • Teacher Name : " + userIdInfoMap.get(sessionInfo.getPresenter()).getFullName() + "\n";
//                }
//                if(StringUtils.isNotEmpty(feedbackToSlack.getSessionId())){
//                    text += " • Session Id : " + feedbackToSlack.getSessionId() + "\n";
//                }
//                if(ArrayUtils.isNotEmpty(grades)){
//                    text += " • Grades : " + String.join(",",grades) + "\n";
//                }
//                text += " • Comment : " + feedbackToSlack.getComment() + "```";
//            }


            logger.info("text : " + text);
            if(StringUtils.isNotEmpty(text) && ConfigUtils.INSTANCE.properties.getProperty("environment").equals("PROD")){
                url += "&text=" + URLEncoder.encode(text,"UTF-8");
                logger.info(url);
                WebUtils.INSTANCE.doCall(url, HttpMethod.POST, null, false, false);
            }
        }
    }

    
}
