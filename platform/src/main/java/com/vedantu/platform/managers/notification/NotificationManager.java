/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.notification;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserInfo;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.GetNotificationsReq;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.notification.responses.GetNotificationsRes;
import com.vedantu.notification.responses.MarkSeenRes;
import com.vedantu.platform.pojo.broadcast.SendBroadcastNotificationReq;
import com.vedantu.platform.pojo.scheduling.BasicRes;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.util.*;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpMethod;

/**
 *
 * @author somil
 */
@Service
public class NotificationManager {

    

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(NotificationManager.class);

    @Autowired
    SMSManager smsManager;
    
    @Autowired
    UserManager userManager;


    public BaseResponse createNotification(CreateNotificationRequest request) throws VException {
        request.verify();
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        logger.info("CreateNotification request "+new Gson().toJson(request));
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/createNotification", HttpMethod.POST, new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        BaseResponse response = new Gson().fromJson(jsonString, BaseResponse.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }
    

    public GetNotificationsRes getNotifications(GetNotificationsReq req) throws VException {
        String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
	ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/getNotifications", HttpMethod.POST, new Gson().toJson(req));
	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, GetNotificationsRes.class);
    }

    public BasicRes markSeen(MarkSeenReq req) throws VException {
        String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
	ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/markSeen", HttpMethod.POST, new Gson().toJson(req));
	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, BasicRes.class);
    }

    public MarkSeenRes markSeenByEntity(MarkSeenReq req) throws VException {
        String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
	ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/markSeenByEntity", HttpMethod.POST, new Gson().toJson(req));
	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, MarkSeenRes.class);
    }
    
    
    public MarkSeenRes markSeenByType(MarkSeenReq req) throws VException {
        String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
	ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/markSeenByType", HttpMethod.POST, new Gson().toJson(req));
	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, MarkSeenRes.class);
    }
        
    
    public MarkSeenRes setMarkSeen(MarkSeenReq req) throws VException {
        String notificationEndpoint= ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
	ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/notification/setMarkSeen", HttpMethod.POST, new Gson().toJson(req));
	VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, MarkSeenRes.class);
    }
    
    
    public void sendAskADoubtNotifications(List<SendBroadcastNotificationReq> notificationParams) throws VException {
        for (SendBroadcastNotificationReq sendAskADoubtNotificationReq : notificationParams) {
            UserInfo user = userManager.getUserInfo(sendAskADoubtNotificationReq.getUserId());
            try {
                if (sendAskADoubtNotificationReq.getNotificationType()
                        .equals(NotificationType.ASK_A_DOUBT_ACCEPTANCE_STUDENT)) {
                    CreateNotificationRequest createNotification = new CreateNotificationRequest(sendAskADoubtNotificationReq
                            .getNotificationInfo().getStudentId(),
                            sendAskADoubtNotificationReq.getNotificationType(), new Gson().toJson(sendAskADoubtNotificationReq.getNotificationInfo()), Role.STUDENT);
                    createNotification(createNotification);                    
                } else {
                    CreateNotificationRequest createNotification = new CreateNotificationRequest(sendAskADoubtNotificationReq.getUserId(),
                            sendAskADoubtNotificationReq.getNotificationType(), new Gson().toJson(sendAskADoubtNotificationReq.getNotificationInfo()), user.getRole());
                    createNotification(createNotification);
                }
                
                if (sendAskADoubtNotificationReq.getNotificationType()
                        .equals(NotificationType.ASK_A_DOUBT_ACCEPTANCE_TEACHER)) {
                } else {
                    try {
                        // Map<String, Object> bodyScopes= new
                        // HashMap<String,Object>();
                        // SendAskADoubtNotificationInfo
                        // notificationInfo =
                        // sendAskADoubtNotificationReq.getNotificationInfo();
                        // for(Field
                        // field:SendAskADoubtNotificationInfo.class.getFields()){
                        // bodyScopes.put(field.getName(),
                        // field.get(notificationInfo));
                        // }
                        if (sendAskADoubtNotificationReq.getNotificationType()
                                .equals(NotificationType.ASK_A_DOUBT_ACCEPTANCE_STUDENT)) {
                            user = userManager.getUserInfo(sendAskADoubtNotificationReq
                                    .getNotificationInfo()
                                    .getStudentId());
                        }
                        
                        smsManager.sendSMS(CommunicationType
                                .valueOf(sendAskADoubtNotificationReq
                                        .getNotificationType().name()),
                                user, sendAskADoubtNotificationReq
                                .getNotificationInfo());
                    } catch (Exception e) {
                        logger.error("Sending sms failed to :"
                                + sendAskADoubtNotificationReq
                                .getUserId(),
                                e);
                    }
                }
            } catch (Exception e) {
                logger.error(e);
            }
            
        }
    }

}

