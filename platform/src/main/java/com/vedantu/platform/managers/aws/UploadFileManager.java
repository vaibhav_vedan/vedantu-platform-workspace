package com.vedantu.platform.managers.aws;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.platform.pojo.wave.WhiteboardPDF;
import com.vedantu.platform.response.filemgmt.WhiteboardPdfInitRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import org.apache.commons.fileupload.FileUpload;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.aws.pojo.UploadFileInfo;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.aws.response.UploadFileUrlRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.request.GetCMDSUploadFileRes;
import com.vedantu.lms.cmds.request.GetCMDSUploadFileUrlReq;
import com.vedantu.platform.aws.dao.UploadFileDetailsDAO;
import com.vedantu.platform.aws.entity.UploadFileDetails;
import com.vedantu.platform.aws.enums.StorageType;
import com.vedantu.util.LogFactory;

import inti.ws.spring.exception.client.BadRequestException;

import java.net.URL;
import java.util.Date;
import java.util.UUID;

@Service
public class UploadFileManager {

	@Autowired
	private UploadFileDetailsDAO uploadFileDetailsDAO;

	@Autowired
	private AwsS3Manager awsS3Manager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UploadFileManager.class);

	public static final String VEDANTU_LE_IMAGES_BUCKET = ConfigUtils.INSTANCE.getStringValue("vedantu.le.images.bucket");
	public static final String VEDANTU_LE_IMAGES_FOLDER = "vedantu-teacher-images";

	public GetCMDSUploadFileRes generateUploadFileURL(GetCMDSUploadFileUrlReq req)
			throws VException {
		GetCMDSUploadFileRes response = new GetCMDSUploadFileRes();
		response.setUploadFileSourceType(UploadFileSourceType.CMDS_ANSWER_UPLOAD);
		response.setUploadFileSourceId(req.getUploadFileSourceId());
		if(ArrayUtils.isNotEmpty(req.getFileInfos())) {
			for (UploadFileInfo uploadFileInfo : req.getFileInfos()) {
				try {
					UploadFileUrlRes uploadFileUrlRes = awsS3Manager.generatePresignedUploadUrl(
							UploadFileSourceType.CMDS_ANSWER_UPLOAD, uploadFileInfo.getFileName(),
							uploadFileInfo.getContentType());
					UploadFileDetails uploadFileDetails = new UploadFileDetails(uploadFileUrlRes.getKeyName(),
							awsS3Manager.getS3Location(UploadFileSourceType.CMDS_ANSWER_UPLOAD),
							uploadFileUrlRes.getFileName(), uploadFileInfo.getContentType(), StorageType.AMAZON_S3,
							UploadFileSourceType.CMDS_ANSWER_UPLOAD, System.currentTimeMillis(), req.getUserId(), null,
							req.getUserId());
					logger.info("upload file details : {}",uploadFileDetails);
					uploadFileDetailsDAO.create(uploadFileDetails);
					logger.info("uploadFileDetails : {}",uploadFileDetails);
					response.addFileInfo(uploadFileUrlRes);
				} catch (Exception ex) {
					String errorMessage = "Error occurred generating the url : " + uploadFileInfo.toString() + " request : "
							+ req.toString() + " ex:" + ex.getMessage();
					logger.error(errorMessage, ex);
					throw new VException(ErrorCode.ERROR_GENERATING_UPLOAD_URL, errorMessage);
				}
			}
		}
		return response;
	}


	public UploadFileUrlRes uploadTeacherImage(UploadFileInfo uploadFileInfo) throws VException {

		String uploadUrl = "";
		UploadFileUrlRes uploadFileUrlRes = new UploadFileUrlRes();
		try {
			String path = new StringBuilder()
					.append(VEDANTU_LE_IMAGES_FOLDER)
					.append(java.io.File.separator)
					.append(ConfigUtils.INSTANCE.getStringValue("environment"))
					.append(java.io.File.separator)
					.append(UUID.randomUUID().toString())
					.toString();
			uploadUrl = awsS3Manager.getPreSignedUrl(VEDANTU_LE_IMAGES_BUCKET, path ,uploadFileInfo.getContentType());

		} catch (Exception ex) {
			throw new VException(ErrorCode.ERROR_GENERATING_UPLOAD_URL, "Error while generating upload url");
		}

		uploadFileUrlRes.setUploadUrl(uploadUrl);
		// hard coded for now.. will use a const after having a discussion on the value to be set
		uploadFileUrlRes.setUploadExpiryTime(1000 * 60 * 60);
		return uploadFileUrlRes;
	}
}
