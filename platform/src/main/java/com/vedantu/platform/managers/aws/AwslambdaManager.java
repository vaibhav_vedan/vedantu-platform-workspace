package com.vedantu.platform.managers.aws;

import com.amazonaws.services.lambda.model.InvokeAsyncRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambdaAsyncClient;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.google.gson.Gson;
import com.vedantu.aws.pojo.AwsFunctionName;
import com.vedantu.aws.pojo.AwsLambdaEvent;
import com.vedantu.aws.pojo.InvokeLambdaRecord;
import com.vedantu.aws.pojo.InvokeLambdaReq;
import com.vedantu.onetofew.pojo.HandleOTFRecordingLambda;
import com.vedantu.platform.background.events.request.BranchEventRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import javax.annotation.PreDestroy;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Service
public class AwslambdaManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwslambdaManager.class);

    private static Gson gson = new Gson();

    private final AWSLambdaClient lambdaClient;

    private final AWSLambdaAsyncClient asynclambdaClient;

    public AwslambdaManager() {
        lambdaClient = new AWSLambdaClient();
        lambdaClient.setRegion(Region.getRegion(Regions.AP_NORTHEAST_1));

        asynclambdaClient = new AWSLambdaAsyncClient();
        asynclambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
    }

    public void invokeEngagementLambda(Long sessionId) {
        logger.info("invokeEngagementLambda called with sessionId =" + sessionId);
        InvokeRequest invokeRequest = new InvokeRequest();
        logger.info(ConfigUtils.INSTANCE.getStringValue("engagement.analysis.env"));
        invokeRequest.setFunctionName(
                "ENGAGEMENT_ANALYSIS_" + ConfigUtils.INSTANCE.getStringValue("engagement.analysis.env"));
        invokeRequest.setPayload(ConfigUtils.INSTANCE.getStringValue("payload1") + sessionId.toString()
                + ConfigUtils.INSTANCE.getStringValue("payload2"));
        logger.info("PAYLOAD");
        logger.info(ConfigUtils.INSTANCE.getStringValue("payload1") + sessionId.toString()
                + ConfigUtils.INSTANCE.getStringValue("payload2"));
        lambdaClient.invoke(invokeRequest);
    }

    public void invokeSessionMetricsLambda(String sessionId) {
        logger.info("invokeSessionMetricsLambda called with sessionId =" + sessionId);
        InvokeRequest invokeRequest = new InvokeRequest();
        logger.info(ConfigUtils.INSTANCE.getStringValue("environment"));
        invokeRequest.setFunctionName(
                "SESSION_METRICS_" + ConfigUtils.INSTANCE.getStringValue("environment"));
        invokeRequest.setPayload("{\"Records\": [{\"Sns\": {\"Message\": \"{\\\"sessionId\\\": " + sessionId + "}\", \"Subject\": \"SESSION_ENDED\"}}]}");
        lambdaClient.invoke(invokeRequest);
    }

    public void invokeOTFRecordingLambda() {
        HandleOTFRecordingLambda req = new HandleOTFRecordingLambda("DEV2", "58b7ea0b60b2717279c83ac0");
        req.addRecordingEntry("86734",
                "https://s3-ap-south-1.amazonaws.com/vedantu-otf-recordings-mumbai/PROD/585bfd88e4b0e33e9e4c464b-85504.mp4");
        req.addRecordingEntry("86773",
                "https://s3-ap-south-1.amazonaws.com/vedantu-otf-recordings-mumbai/PROD/585bfd88e4b0e33e9e4c464b-85504.mp4");
        invokeOTFRecordingLambda(gson.toJson(req));
    }

    public void invokeOTFRecordingLambda(HandleOTFRecordingLambda req) {
        invokeOTFRecordingLambda(gson.toJson(req));
    }

    private void invokeOTFRecordingLambda(String jsonObject) {
        logger.info(" Request : " + jsonObject);

        InvokeRequest invokeRequest = new InvokeRequest();
        invokeRequest.setFunctionName(AwsFunctionName.HANDLE_OTF_RECORDINGS.name());

        // arn:aws:lambda:ap-southeast-1:617558729962:function:HANDLE_OTF_RECORDINGS
        InvokeLambdaRecord record = new InvokeLambdaRecord(AwsLambdaEvent.HANDLE_OTF_RECORDING.name(), jsonObject);
        InvokeLambdaReq req = new InvokeLambdaReq();
        req.addRecords(record);
        String payload = gson.toJson(req);
        logger.info("PAYLOAD : " + payload);

        invokeRequest.setPayload(jsonObject);
        lambdaClient.invoke(invokeRequest);
    }

    public String invokePdfToImageConverterLambda(String key) {

        logger.info("invokePdfToImageConverterLambda called with key {}", key);

        logger.info(ConfigUtils.INSTANCE.getStringValue("environment"));

        String SEO_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

        Map<String, String> lamdaPayload = new HashMap<>();
        lamdaPayload.put("bucket",SEO_BUCKET);
        lamdaPayload.put("key",key);
        String inputJSON = gson.toJson(lamdaPayload);

        logger.info(inputJSON);

        String functionName = "pdftoimageConverterMigration";

        InvokeRequest invokeRequest = new InvokeRequest();
        invokeRequest.setFunctionName(functionName);
        invokeRequest.setPayload(inputJSON);

        lambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        InvokeResult lmbResult = lambdaClient.invoke(invokeRequest);
        String resultJSON = new String(lmbResult.getPayload().array(), Charset.forName("UTF-8"));

        return resultJSON;
    }

    public String invokeCloudfrontClearCacheLambda(String key) {

        logger.info("invokeCloudfrontClearCacheLambda called with key {}", key);

        String inputJSON = gson.toJson(key);

        logger.info(inputJSON);

        String functionName = "invalidateCFCache";

        InvokeRequest invokeRequest = new InvokeRequest();
        invokeRequest.setFunctionName(functionName);
        invokeRequest.setPayload(inputJSON);

        lambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        InvokeResult lmbResult = lambdaClient.invoke(invokeRequest);
        String resultJSON = new String(lmbResult.getPayload().array(), Charset.forName("UTF-8"));

        return resultJSON;
    }
    
    public void invokeLambdaToTriggerBranchEvents(BranchEventRequest requestPayload) {
    	InvokeRequest invokeRequest = new InvokeRequest();
    	lambdaClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        invokeRequest.setFunctionName("TRIGGER_EVENTS_BRANCH");
        invokeRequest.setPayload(gson.toJson(requestPayload));
        logger.info("invokeRequest: "+invokeRequest.toString());
        lambdaClient.invoke(invokeRequest);
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (lambdaClient != null) {
                lambdaClient.shutdown();
            }
            if (asynclambdaClient != null) {
                asynclambdaClient.shutdown();
            }            
        } catch (Exception e) {
            logger.error("Error in shutting down asynclambdaClient, lambdaClient ", e);
        }
    }         
}
