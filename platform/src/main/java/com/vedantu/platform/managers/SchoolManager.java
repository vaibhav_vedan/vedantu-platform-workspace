/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers;

import com.vedantu.platform.dao.SchoolDataDAO;
import com.vedantu.platform.mongodbentities.SchoolData;
import com.vedantu.platform.request.GetSchoolsReq;
import com.vedantu.platform.response.GetSchoolsRes;
import com.vedantu.util.LogFactory;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class SchoolManager {
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SchoolManager.class);

    @Autowired
    SchoolDataDAO schoolDataDAO;
    
    public GetSchoolsRes getSchools(GetSchoolsReq req){
        GetSchoolsRes res=new GetSchoolsRes();
        List<SchoolData> schools=schoolDataDAO.getSchools(req.getPinCode(), req.getState(), req.getCountry(), req.getBoard(),req.getNameQuery(),req.getAddressQuery(), req.getStart(), req.getSize());
        res.setList(schools);
        return res;
    }
}
