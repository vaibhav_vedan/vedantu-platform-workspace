package com.vedantu.platform.managers.offering;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.offering.request.AddStudyPlanRequest;
import com.vedantu.offering.request.EditStudyPlanInfoRequest;
import com.vedantu.offering.request.GetStudyPlansRequest;
import com.vedantu.offering.response.GetStudyPlansResponse;
import com.vedantu.offering.response.StudyPlanInfo;
import com.vedantu.platform.dao.offering.StudyPlanDao;
import com.vedantu.platform.mongodbentities.offering.StudyPlan;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.util.LogFactory;
import org.dozer.DozerBeanMapper;

@Service
public class StudyPlanManager {

    @Autowired
    private DozerBeanMapper mapper; 

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(StudyPlanManager.class);

    @Autowired
    StudyPlanDao studyPlanDao;

    @Autowired
    UserManager userManager;

    public StudyPlanInfo addStudyPlan(AddStudyPlanRequest req)
            throws VException {

        logger.info("addStudyPlan", req);
        StudyPlan studyPlan = mapper.map(req, StudyPlan.class);

        studyPlanDao.upsert(studyPlan);

        User user = userManager.getUserById(Long.parseLong(studyPlan
                .getUserId()));

        StudyPlanInfo studyPlanInfo = mapper
                .map(studyPlan, StudyPlanInfo.class);
        studyPlanInfo.setUser(new UserBasicInfo(user, false));
        logger.info("addStudyPlan" + studyPlanInfo.toString());
        return studyPlanInfo;
    }

    public StudyPlanInfo editStudyPlan(EditStudyPlanInfoRequest req)
            throws VException {

        logger.info("editStudyPlan" + req.toString());
        userManager.teacherAccessOnlyCheck(req.getCallingUserId());

        StudyPlanInfo studyPlanInfo = null;

        try {
            StudyPlan studyPlan = studyPlanDao.getById(req.getId());

            if (studyPlan == null) {
                throw new VException(ErrorCode.NOT_FOUND_ERROR,
                        "no study plan found for id:" + req.getId());
            }

            if (req.getCallingUserId() != null && !studyPlan.getUserId().equals(req.getCallingUserId().toString())) {
                throw new VException(ErrorCode.ACTION_NOT_ALLOWED,
                        "user can not modify studyplan added by someone else");
            }
            mapper.map(req, studyPlan);

            User user = userManager.getUserById(Long.parseLong(studyPlan
                    .getUserId()));

            studyPlanDao.upsert(studyPlan);
            studyPlanInfo = mapper.map(studyPlan, StudyPlanInfo.class);
            studyPlanInfo.setUser(new UserBasicInfo(user, false));
        } finally {
        }
        logger.info("editStudyPlan" + studyPlanInfo.toString());
        return studyPlanInfo;
    }

    public GetStudyPlansResponse getStudyPlans(GetStudyPlansRequest req)
            throws NumberFormatException, VException {
        logger.info("getStudyPlans" + req.toString());
        GetStudyPlansResponse res = new GetStudyPlansResponse();

        List<StudyPlan> studyPlans = studyPlanDao.get(null, null,
                req.getOfferingId(), req.getTargetUserId(), null, null, null,
                null, null, null, null, null, null);
        for (StudyPlan studyPlan : studyPlans) {
            StudyPlanInfo studyPlanInfo = new StudyPlanInfo();
            User user = userManager.getUserById(Long.parseLong(studyPlan
                    .getUserId()));

            studyPlanDao.upsert(studyPlan);
            studyPlanInfo = mapper.map(studyPlan, StudyPlanInfo.class);
            studyPlanInfo.setUser(new UserBasicInfo(user, false));
            res.addItem(studyPlanInfo);
        }
        logger.info("getStudyPlans" + res.toString());
        return res;
    }

    public GetStudyPlansResponse getStudyPlansByUserId(GetStudyPlansRequest req)
            throws NumberFormatException, VException {
        logger.info("getStudyPlans" + req.toString());
        GetStudyPlansResponse res = new GetStudyPlansResponse();

        List<StudyPlan> studyPlans = studyPlanDao.get(null, req.getUserId(),
                null, req.getTargetUserId(), null, null, null, null, null,
                null, null, null, null);

        for (StudyPlan studyPlan : studyPlans) {
            StudyPlanInfo studyPlanInfo = new StudyPlanInfo();
            User user = userManager.getUserById(Long.parseLong(studyPlan
                    .getUserId()));

            studyPlanDao.upsert(studyPlan);
            studyPlanInfo = mapper.map(studyPlan, StudyPlanInfo.class);
            studyPlanInfo.setUser(new UserBasicInfo(user, false));
            res.addItem(studyPlanInfo);
        }
        logger.info("getStudyPlans" + res);
        return res;
    }

    public StudyPlanInfo deleteStudyPlan(EditStudyPlanInfoRequest req)
            throws VException {

        logger.info("editStudyPlan" + req.toString());
        userManager.teacherAccessOnlyCheck(req.getCallingUserId());

        StudyPlan studyPlan = studyPlanDao.getById(req.getId());

        if (studyPlan == null) {
            throw new VException(ErrorCode.NOT_FOUND_ERROR,
                    "no study plan found for id:" + req.getId());
        }

        if (req.getCallingUserId() != null && !studyPlan.getUserId().equals(req.getCallingUserId().toString())) {
            throw new VException(ErrorCode.ACTION_NOT_ALLOWED,
                    "user can not delete studyplan added by someone else");
        }
        studyPlanDao.deleteById(studyPlan.getId());
        StudyPlanInfo studyPlanInfo = null;
        User user = userManager.getUserById(Long.parseLong(studyPlan
                .getUserId()));

        studyPlanDao.upsert(studyPlan);
        studyPlanInfo = mapper.map(studyPlan, StudyPlanInfo.class);
        studyPlanInfo.setUser(new UserBasicInfo(user, false));
        logger.info("deleteStudyPlan" + studyPlanInfo.toString());
        return studyPlanInfo;
    }
}
