/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.notification;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.notification.requests.HandleMessageTriggerReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class MessageTriggerManager {
    
    

    public void handleMessageTrigger(User fromUser, User toUser,
            MessageUserRes messageUser) throws VException {
        HandleMessageTriggerReq request=new HandleMessageTriggerReq();
        request.setFromuser(fromUser);
        request.setToUser(toUser);
        request.setMessageUser(messageUser);
        String notificationEndPoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndPoint + "/messagetrigger/handleMessageTrigger",
                HttpMethod.POST, new Gson().toJson(request));
        //handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }
}
