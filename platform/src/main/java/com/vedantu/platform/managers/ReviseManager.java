package com.vedantu.platform.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.ReviseDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.mongodbentities.ReviseIndiaStats;
import com.vedantu.platform.mongodbentities.ReviseJeeStats;
import com.vedantu.platform.pojo.AnalyticsInfo;
import com.vedantu.platform.pojo.ReviseJeeUsageStats;
import com.vedantu.platform.pojo.TestAnalytics;
import com.vedantu.platform.pojo.TestCategoryAnalytics;
import com.vedantu.platform.request.UpdateStatsPostClassReq;
import com.vedantu.platform.request.UpdateStatsPostTestReq;
import com.vedantu.platform.utils.ReviseJeeFactory;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReviseManager {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReviseManager.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    ReviseDAO reviseDao;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private final Gson gson = new Gson();

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static final String REVISE_JEE_TEST_IDS = "REVISE_JEE_TEST_IDS";
    private static final String REVISE_JEE_SESSION_IDS = "REVISE_JEE_SESSION_IDS";

    public ReviseIndiaStats getStats(Long userId) {
        String key = "REVISE_INDIA_" + userId;
        try {
            String stats = redisDAO.get(key);
            if (stats != null) {
                return gson.fromJson(stats, ReviseIndiaStats.class);
            }
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.error(e.getErrorMessage(), e);
        }

        ReviseIndiaStats stats =  reviseDao.getStats(userId);

        if(stats == null){
            return null;
        }
        try{
            redisDAO.set(key, gson.toJson(stats, ReviseIndiaStats.class));
        }
        catch(InternalServerErrorException e) {
            logger.error(e.getErrorMessage(), e);
        }

        return stats;
    }

    public ReviseJeeStats jeeStats(Long userId) {
        String key = "REVISE_JEE_RESULT_" + userId;
        try {
            String stats = redisDAO.get(key);
            if (stats != null) {
                return gson.fromJson(stats, ReviseJeeStats.class);
            }
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.error(e.getErrorMessage(), e);
        }

        ReviseJeeStats stats =  reviseDao.getJeetats(userId);

        if(stats == null){
            return null;
        }
        try{
            redisDAO.set(key, gson.toJson(stats, ReviseJeeStats.class));
        }
        catch(InternalServerErrorException e) {
            logger.error(e.getErrorMessage(), e);
        }

        return stats;
    }


    public void addToRemindWebinar(String userId, String webinarId) {
        Long id = Long.parseLong(userId);
        ReviseIndiaStats reviseIndiaStats = reviseDao.getStats(id);
        if(reviseIndiaStats == null){
            return;
        }
        List<String> remindList = reviseIndiaStats.getRemindWebinars();
        if(null != remindList){
            remindList.add(webinarId);
        }
        else{
            remindList = new ArrayList<>();
            remindList.add(webinarId);
        }

        reviseIndiaStats.setRemindWebinars(remindList);

        reviseDao.save(reviseIndiaStats, String.valueOf(userId));

        deleteRedisData(userId);
    }

    public PlatformBasicResponse updateAfterTest(String payload) throws VException {
        if (StringUtils.isNotEmpty(payload)) {
            logger.info("calling sqs with sns payload : " + payload);

            Type type = new TypeToken<UpdateStatsPostTestReq>() {
            }.getType();
            UpdateStatsPostTestReq req = gson.fromJson(payload, type);

            Update update = new Update();
            update.inc(ReviseIndiaStats.Constants.TESTS, 1);
            if (StringUtils.isNotEmpty(req.getTestAttemptId())) {
                update.set(ReviseIndiaStats.Constants.TEST_ATTEMPT_ID, req.getTestAttemptId());
            }
            if (StringUtils.isNotEmpty(req.getPerformanceCode())) {
                update.set(ReviseIndiaStats.Constants.TEST_PERFORMANCE_CODE, req.getPerformanceCode());
            }
            if (null != (req.getAnalytics())) {
                update.set(ReviseIndiaStats.Constants.TEST_ANALYTICS, req.getAnalytics());
            }

            if (StringUtils.isNotEmpty(req.getS3Link())) {
                update.set("s3Link", req.getS3Link());
            }
            reviseDao.updateAfterTest(update, req.getUserId());
            deleteRedisData(req.getUserId());
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse updateReviseJeeAfterTest(String payload) {
        if (StringUtils.isNotEmpty(payload)) {
            logger.info("updateReviseJeeAfterTest : " + payload);

            Type type = new TypeToken<UpdateStatsPostTestReq>() {
            }.getType();
            UpdateStatsPostTestReq req = gson.fromJson(payload, type);

            Update update = new Update();

            if(StringUtils.isNotEmpty(req.getType()) && "POST_SESSION".equals(req.getType())){

                try {
                    String key = "REVISE_JEE_POST_SESSION_" + req.getUserId() + "_" + req.getSessionId();
                    String val = redisDAO.get(key);

                    if(StringUtils.isNotEmpty(val))
                        new PlatformBasicResponse();

                    redisDAO.setex(key,"TRUE",DateTimeUtils.SECONDS_PER_DAY * 6);
                }catch (Exception e){
                    logger.error("Error fetching the session count from rediss:",e);

                }
                update.inc(ReviseJeeStats.Constants.SESSIONS_ATTENDED, 1);
            }else {

                ReviseJeeStats reviseJeeStats = jeeStats(Long.parseLong(req.getUserId()));

                List<String> attemptedTestsList = new ArrayList<>();

                if(reviseJeeStats!= null && CollectionUtils.isNotEmpty(reviseJeeStats.getTestAttempted()))
                    attemptedTestsList = reviseJeeStats.getTestAttempted();

                String testId = req.getTestId();

                update.set(ReviseJeeStats.Constants.TEST_ATTEMPTED, attemptedTestsList);

                if (StringUtils.isNotEmpty(req.getTestAttemptId())) {
                    update.set(ReviseJeeStats.Constants.TEST_ATTEMPT_ID, req.getTestAttemptId());
                }
                if (StringUtils.isNotEmpty(req.getPerformanceCode())) {
                    update.set(ReviseJeeStats.Constants.TEST_PERFORMANCE_CODE, req.getPerformanceCode());
                }
                if (null != (req.getAnalytics())) {

                    TestAnalytics testAnalytics = req.getAnalytics();

                    testId = testAnalytics.getTestId();

                    AnalyticsInfo analyticsInfo = testAnalytics.getInfo();
                    Integer marks = (analyticsInfo.getCorrect() * 4) - analyticsInfo.getIncorrect() * 1;
                    analyticsInfo.setMarks(marks);
                    analyticsInfo.setCutOff(72);

                    Map<String, Object> emailPayload = new HashMap<>();

                    List<TestCategoryAnalytics> testCategoryAnalytics = testAnalytics.getCategories();
                    for (TestCategoryAnalytics categoryAnalytics : testCategoryAnalytics) {
                        AnalyticsInfo categoryAnalyticsInfo = categoryAnalytics.getInfo();
                        Integer categoryMarks = (categoryAnalyticsInfo.getCorrect() * 4) - categoryAnalyticsInfo.getIncorrect() * 1;
                        categoryAnalyticsInfo.setMarks(categoryMarks);

                        if("Physics".equalsIgnoreCase(categoryAnalytics.getName())){
                            emailPayload.put("physicsScore",categoryMarks);
                            emailPayload.put("physicsC",categoryAnalyticsInfo.getCorrect());
                            emailPayload.put("physicsW",categoryAnalyticsInfo.getIncorrect());
                            emailPayload.put("physicsU",categoryAnalyticsInfo.getUnattempted());
                        }else if("Chemistry".equalsIgnoreCase(categoryAnalytics.getName())){
                            emailPayload.put("chemistryScore",categoryMarks);
                            emailPayload.put("chemistryC",categoryAnalyticsInfo.getCorrect());
                            emailPayload.put("chemistryW",categoryAnalyticsInfo.getIncorrect());
                            emailPayload.put("chemistryU",categoryAnalyticsInfo.getUnattempted());
                        }else if("Mathematics".equalsIgnoreCase(categoryAnalytics.getName())){
                            emailPayload.put("mathsScore",categoryMarks);
                            emailPayload.put("mathsC",categoryAnalyticsInfo.getCorrect());
                            emailPayload.put("mathsW",categoryAnalyticsInfo.getIncorrect());
                            emailPayload.put("mathsU",categoryAnalyticsInfo.getUnattempted());
                        }

                    }

                    update.set(ReviseJeeStats.Constants.TEST_ANALYTICS, req.getAnalytics());

                    Map<String, String> predictedRankObj = ReviseJeeFactory.getPredictedRank(marks);
                    String predictedRank = predictedRankObj.get("predictedRank");
                    String nextRankToAchieve = predictedRankObj.get("nextRankToAchieve");

                    update.set(ReviseJeeStats.Constants.PREDICTED_RANK_RANGE, predictedRank);
                    update.set(ReviseJeeStats.Constants.AREAS_TO_IMPROVE, req.getAreasToWorkOn());
                    update.set(ReviseJeeStats.Constants.NEXT_RANK_TO_ACHIEVE, nextRankToAchieve);


                    emailPayload.put("userId",req.getUserId());
                    emailPayload.put("predictedRankRange",predictedRank);

                    logger.info("Sending jee result mail");
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TEST_RESULT_EMAIL_REVISEJEE, emailPayload);
                    asyncTaskFactory.executeTask(params);

                }

                if (StringUtils.isNotEmpty(testId) && !attemptedTestsList.contains(testId)) {
                    update.inc(ReviseJeeStats.Constants.TESTS, 1);
                    attemptedTestsList.add(req.getTestId());
                }

                if (StringUtils.isNotEmpty(req.getS3Link())) {
                    update.set("s3Link", req.getS3Link());
                }
            }
            reviseDao.updateReviseJeeAfterTest(update, req.getUserId());
            deleteJeeRedisData(req.getUserId());
            if(null != req.getAnalytics()){
                try {
                    setReviseJeeAttemptStatus(Long.parseLong(req.getUserId()), req.getAnalytics().getTestId());
                } catch (Exception e) {
                    logger.info("Error in updating user details after revise jee" + e.getMessage());
                }
            }
        }
        return new PlatformBasicResponse();
    }

    public void setReviseJeeAttemptStatus(Long userId, String testId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/revisejee/userdetails/attempt?" +
                        String.format("userId=%d&testId=%s", userId, testId)
                , HttpMethod.POST,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public PlatformBasicResponse updatePostClass(String payload) {
        if (StringUtils.isNotEmpty(payload)) {
            logger.info("calling sqs with sns payload : " + payload);
            Type type = new TypeToken<UpdateStatsPostClassReq>() {
            }.getType();
            UpdateStatsPostClassReq data = gson.fromJson(payload, type);

            Update update = new Update();
            update.inc(ReviseIndiaStats.Constants.CLASSES, 1);
            update.inc(ReviseIndiaStats.Constants.DOUBTS, data.getDoubts());
            reviseDao.updateAfterClass(update, data.getUserId());
        }

        return new PlatformBasicResponse();
    }

    public void deleteJeeRedisData(String userId){
        String key = "REVISE_JEE_RESULT_" + userId;
        try{
            redisDAO.del(key);
        } catch (Exception e) {
            logger.warn("Could not delete redis key : {}", key);
        }
    }


    public void deleteRedisData(String userId){
        String key = "REVISE_INDIA_" + userId;
        try{
            redisDAO.del(key);
        } catch (Exception e) {
            logger.warn("Could not delete redis key : {}", key);
        }
    }

    public Map<String, String> s3Links() throws InternalServerErrorException, BadRequestException {

        String resp = redisDAO.get("REVISEINDIA_S3LINK");

        if(StringUtils.isNotEmpty(resp)){
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            return gson.fromJson(resp, type);
        }

        Map<String, String> map = new HashMap<>();
        if("prod".equalsIgnoreCase(env)) {
            map.put("unitTest", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/untitests.json");
            map.put("overallJourney", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/overall.json");
            map.put("studymaterial", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/studyMaterial.json");
            map.put("subjectwise", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/subjectwise.json");
            map.put("fullTest", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/fulltests.json");
            map.put("chapterTest", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/chaptertests.json");
            map.put("revision", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-india/file/revision.json");
        }else {
            map.put("unitTest", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/untitests.json");
            map.put("overallJourney", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/overall.json");
            map.put("studymaterial", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/studyMaterial.json");
            map.put("subjectwise", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/subjectwise.json");
            map.put("fullTest", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/fulltests.json");
            map.put("chapterTest", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/chaptertests.json");
            map.put("revision", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/revise-india/revision.json");
        }
        redisDAO.set("REVISEINDIA_S3LINK", gson.toJson(map));
        return map;
    }

    public Map<String, String> getJeeS3Links() {

        Map<String, String> map = new HashMap<>();

        if("prod".equalsIgnoreCase(env)) {
            map.put("classes", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes.json");
            map.put("pdfs", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/pdfs.json");
            map.put("tests", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/tests.json");
            map.put("rankedStudents", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/rankList.json");
        }else {
            map.put("classes", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes.json");
            map.put("pdfs", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/pdfs.json");
            map.put("tests", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/tests.json");
            map.put("rankedStudents", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/rankList.json");
        }

        return map;

    }

    public void reloadReviseJeeTestIds(List<String> testIds) throws BadRequestException, InternalServerErrorException {

        redisDAO.del(REVISE_JEE_TEST_IDS);

        for (String id : testIds){
            redisDAO.addToList(REVISE_JEE_TEST_IDS, id);
        }

    }

    public List<String> getReviseJeeTestIds() {

        List<String> reviseJeeTestIds = new ArrayList<>();

        try {
            reviseJeeTestIds = redisDAO.getList(REVISE_JEE_TEST_IDS, 0 , 200);
        }catch (Exception e){
            logger.error("Error fetching REVISE_JEE_TEST_IDS", e);
        }

        return reviseJeeTestIds;

    }

    public void reloadReviseJeeSessionIds(List<String> sessionIds) throws BadRequestException, InternalServerErrorException {

        redisDAO.del(REVISE_JEE_SESSION_IDS);

        for (String id : sessionIds){
            redisDAO.addToList(REVISE_JEE_SESSION_IDS, id);
        }

    }

    public List<String> getReviseJeeSessionIds() {

        List<String> reviseJeeSessionIds = new ArrayList<>();

        try {
            reviseJeeSessionIds = redisDAO.getList(REVISE_JEE_SESSION_IDS, 0 , 200);
        }catch (Exception e){
            logger.error("Error fetching REVISE_JEE_SESSION_IDS", e);
        }

        return reviseJeeSessionIds;

    }

    public ReviseJeeUsageStats jeeUsageStats(Long callingUserId) {

        ReviseJeeStats reviseJeeStats = jeeStats(callingUserId);

        ReviseJeeUsageStats reviseJeeUsageStats = new ReviseJeeUsageStats();

        if(reviseJeeStats != null){

            if(reviseJeeStats.getTests() != null)
                reviseJeeUsageStats.setTests(reviseJeeStats.getTests());

            if(reviseJeeStats.getSessionsAttended() != null)
                reviseJeeUsageStats.setSessionsAttended(reviseJeeStats.getSessionsAttended());

        }

        return reviseJeeUsageStats;

    }
}
