package com.vedantu.platform.managers.aws;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.util.HashMap;
import java.util.Map;
import java.io.InputStream;
import java.util.UUID;
import javax.annotation.PreDestroy;

@Service
public class AwsS3SEOManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsS3SEOManager.class);

    private AmazonS3Client s3Client;

    @Autowired
    private AmazonClient amazonClient;

    public AwsS3SEOManager() {
        s3Client = new AmazonS3Client();
    }

    public String getPresignedUploadUrl(String bucket, String key, String contentType) {
        String presignedUrl = "";

        try {
            java.util.Date expiration = new java.util.Date();
            long milliSeconds = expiration.getTime();
            milliSeconds += 1000 * 60 * 60; // Add 1 hour.
            expiration.setTime(milliSeconds);

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key);
            generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
            generatePresignedUrlRequest.setExpiration(expiration);
            if (StringUtils.isNotEmpty(contentType)) {
                generatePresignedUrlRequest.setContentType(contentType);
            }

            presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();

            logger.info("preSignedUrl:" + presignedUrl);
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }

    public List<String> getBucketContents(String bucket, String prefix) {
        List<String> contents = new ArrayList<>();

        try {
            s3Client.listObjects(bucket, prefix).getObjectSummaries().forEach(objectSummary -> {
                contents.add(s3Client.getUrl(bucket, objectSummary.getKey()).toString());
            });
        } catch (Exception ex) {
        }

        return contents;
    }

    public Boolean uploadFile(String bucket, String key, File contentFile) {
        Boolean isUploadContentFileSuccessfull = false;

        try {
            s3Client.putObject(bucket, key, contentFile);

            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            isUploadContentFileSuccessfull = false;
        }

        return isUploadContentFileSuccessfull;
    }

    public Boolean uploadFile(String bucket, String key, File contentFile, ObjectMetadata metadata) {
        Boolean isUploadContentFileSuccessfull;

        try {
            s3Client.putObject(bucket, key, new FileInputStream(contentFile), metadata);

            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            isUploadContentFileSuccessfull = false;
        }

        return isUploadContentFileSuccessfull;
    }

    public Map<String,Object> getPresignedUploadUrlForAppendPdf() {
        UploadTarget uploadTarget = UploadTarget.APPEND_PDF_PAGES;
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fileName = UUID.randomUUID().toString() + ".pdf";
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + fileName;
        String uploadUrl = getPresignedUploadUrl(uploadTarget.getBucketName(), fullKey, "application/pdf");
        Map<String,Object> obj = new HashMap<>();
        obj.put("uploadUrl", uploadUrl);
        obj.put("fileName", fileName);
        obj.put("uploadTarget", UploadTarget.APPEND_PDF_PAGES.name());
        return obj;
    }

    public String getPresignedDownloadUrl(CMDSImageDetails imageDetails) {
        UploadTarget uploadTarget = imageDetails.getUploadTarget();
        String bucket;
        if (uploadTarget == null) {
            return null;
        } else {
            bucket = uploadTarget.getBucketName();
            if (StringUtils.isEmpty(bucket)) {
                return null;
            }
        }
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + imageDetails.getFileName();
        return amazonClient.getPresignedDownloadUrl(fullKey, bucket);
    }
    
    public InputStream getObjectInputStream(String bucketName, String key) {
        S3Object fullObject = s3Client.getObject(new GetObjectRequest(bucketName, key));
        return fullObject.getObjectContent();
    }
    
    public String getPublicBucketDownloadUrl(String bucket, String key) {
        return s3Client.getUrl(bucket, key).toExternalForm();
    }

    public void deleteFile(String bucket, String key) {
        s3Client.deleteObject(bucket, key);
    }

    public boolean checkKeyExists(String bucket, String key) {
        return s3Client.doesObjectExist(bucket, key);
    }
    
    public void uploadPdf(String bucket, String key, File file) {
        PutObjectRequest request = new PutObjectRequest(bucket, key, file);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType("application/pdf");
        request.setMetadata(metadata);
        s3Client.putObject(request);
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
