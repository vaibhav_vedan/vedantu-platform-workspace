package com.vedantu.platform.managers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.vedantu.util.logger.LoggingMarkers;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.leadsquared.pojo.JotFormTeacherBlueBookPojo;
import com.vedantu.leadsquared.pojo.TeacherBlueBookFormPojo;
import com.vedantu.leadsquared.pojo.TeacherLeadDetails;
import com.vedantu.platform.pojo.leadsquared.LeadDetails;
import com.vedantu.platform.utils.LeadSquaredUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredDataType;

@Service
public class TeacherLeadSquaredManager {

	
	public String LS_ACCESS_ID_1 = new String();
	public String LS_SECRET_KEY_1 = new String();
	public String LS_ACCESS_ID_HIDE = new String();
	public String LS_SECRET_KEY_HIDE = new String();
	public String LS_UPSERT_URL_1 = new String();
	
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(TeacherLeadSquaredManager.class);

	@Autowired
	AsyncTaskFactory asyncTaskFactory;

	@Autowired
	FosUtils userUtils;
	
	@Autowired
	private LeadSquaredManager leadSquaredManager; 

	private static Gson gson = new Gson();
	
	@PostConstruct
	public void init() {
		
		LS_ACCESS_ID_1 = ConfigUtils.INSTANCE.getStringValue("leadsquared.teacher.access.id");
		LS_SECRET_KEY_1 = ConfigUtils.INSTANCE.getStringValue("leadsquared.teacher.secret.key");
		LS_UPSERT_URL_1 = ConfigUtils.INSTANCE.getStringValue("leadsquared.teacher.upsert.url");
	}
	
	public void processJotFormAndCreateLead(JotFormTeacherBlueBookPojo req) throws IllegalArgumentException, IllegalAccessException, IOException, VException{
		
		logger.info("lead Square request : "+req);
		if(req != null && req.getRawRequest() != null){
			TeacherLeadDetails teacherLead = new TeacherLeadDetails(req.getRawRequest());
			Map<String, String> fields = leadCreate(teacherLead);
			logger.info("LeadSquared_TeacherLeadDetaisl : "+ teacherLead);
			leadUpsert(fields);
		}
		
	}
	
	public void processJotFormAndCreateLead(TeacherBlueBookFormPojo rawReq) throws IllegalArgumentException, IllegalAccessException, IOException, VException{
		if(rawReq != null){
			TeacherLeadDetails teacherLead = null;
			try {
				teacherLead = new TeacherLeadDetails(rawReq);
				
			
		//	logger.info("LeadSquared_TeacherLeadDetails_1 : "+ teacherLead);
				Map<String, String> fields = leadCreate(teacherLead);
				logger.info("LeadSquared_TeacherLeadDetaisl : "+ teacherLead);
				leadUpsert(fields);
			}catch(Exception e){
				logger.error("Error in sending to LeadSquare." + e);
			}
		}
	}
	
	private boolean leadUpsert(Map<String, String> fields)
			throws IllegalArgumentException, IllegalAccessException, BadRequestException, IOException, VException {
		// Post lead data to LeadSquared
		String url = getLeadSquaredURL(LS_UPSERT_URL_1);
		return postLeadDetails(url, HttpMethod.POST, LeadSquaredUtils.createJSONObjectLeadDetails(fields));
	}

	private String getLeadSquaredURL(String url) throws BadRequestException {
		if (StringUtils.isEmpty(url) || StringUtils.isEmpty(LS_ACCESS_ID_1) || StringUtils.isEmpty(LS_SECRET_KEY_1)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No access details found");
		}
		return url + "?accessKey=" + LS_ACCESS_ID_1 + "&secretKey=" + LS_SECRET_KEY_1;
	}
	
	private boolean postLeadDetails(String url, HttpMethod httpMethod, String json)
			throws IOException, IllegalArgumentException, IllegalAccessException, BadRequestException, VException {
		boolean posted = false;
		for (int i = 0; (i < 3) && !posted; i++) {
			ClientResponse cRes = postLeadData(url, HttpMethod.POST, json);

			String response = cRes.getEntity(String.class);
			if (cRes.getStatus() != 200 && cRes.getStatus() != 429) {
				logger.info(LoggingMarkers.JSON_MASK, "Error pushing data to leadsquared- Code:" + cRes.getStatus() + " response:" + response
						+ " req: " + json + ", url:" + url);
			} else if (cRes.getStatus() == 429) {
				try {
					Thread.sleep(1000);
				} catch (Exception ex) {

				}
				continue;
			} else {
				posted = true;
				logger.info("Data successfully posted");
			}
		}
		return posted;
	}
	
	public ClientResponse postLeadData(String location, HttpMethod httpMethod, String json)
			throws IOException, IllegalArgumentException, IllegalAccessException, BadRequestException, VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(location, httpMethod, json, false);
		return resp;
	}
	
	private Map<String, String> leadCreate(TeacherLeadDetails lead)
			throws IllegalArgumentException, IllegalAccessException, BadRequestException, IOException {
		Map<String, String> fields = new HashMap<String, String>();
		for (Field field : TeacherLeadDetails.class.getDeclaredFields()) {
			if (field.get(lead) != null) {
				fields.put(field.getName(), String.valueOf(field.get(lead)));
			}
		}
		return fields;
	}
	
}
