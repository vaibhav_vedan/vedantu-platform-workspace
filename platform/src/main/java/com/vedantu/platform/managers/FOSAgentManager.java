package com.vedantu.platform.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.dao.fosAgent.FOSAgentDAO;
import com.vedantu.platform.entity.FOSAgent;
import com.vedantu.platform.pojo.FOSAgentPOJO;
import com.vedantu.platform.request.FOSAgentReq;
import com.vedantu.platform.request.FosAgentRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.response.FOSAgentRes;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FOSAgentManager {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FOSAgentManager.class);

    @Autowired
    private FOSAgentDAO fosAgentDAO;

    @Autowired
    private DozerBeanMapper mapper;


    public List<FOSAgentRes> getFOSEmployeeByEmailOrEmployeeCode(FosAgentRequest req) {
    	List<FOSAgentRes> fosAgentResponse = new ArrayList<>();
        List<FOSAgent> fosAgents = fosAgentDAO.getFOSEmployeeByEmailOrEmployeeCode(req);
        for (FOSAgent fosAgent : fosAgents) {
			FOSAgentRes fosAgentRes = new FOSAgentRes();
			if(fosAgent.getId() != null) {
				fosAgentRes.setId(fosAgent.getId());
			}
			if(fosAgent.getAgentEmailId() != null) {
				fosAgentRes.setAgentEmailId(fosAgent.getAgentEmailId());
			}
			if(fosAgentRes != null) {
				fosAgentResponse.add(fosAgentRes);
			}
		}

        return fosAgentResponse;
    }


    public FOSAgent getFOSAgentById(String employeeCode) {
        return fosAgentDAO.getFOSAgentById(employeeCode);
    }

    public List<FOSAgent> getFOSAgentByIds(List<String> employeeCodes) {
        return fosAgentDAO.getFOSAgentByIds(employeeCodes, null);
    }

    public FOSAgent createFOSAgent(FOSAgentReq fosAgent) throws BadRequestException {
        FOSAgent fosAgent1 = mapper.map(fosAgent, FOSAgent.class);
        fosAgent1.setAgentEmailId(fosAgent1.getAgentEmailId().trim().toLowerCase());
        return fosAgentDAO.create(fosAgent1);
    }

    public List<FOSAgent> getAgentForMatchingEmailString(String email) {
        return fosAgentDAO.getByEmail(email);
    }

    public List<FOSAgent> getEmployeesMatchingEmailStringOrAgentCode(String pattern) {
        return fosAgentDAO.getEmployeesMatchingEmailStringOrAgentCode(pattern);
    }

    public List<FOSAgentPOJO> createFOSAgentInBulk(FOSAgentReq fosAgentsReq) throws BadRequestException {
        List<FOSAgentPOJO> fosAgentList = fosAgentsReq.getFosAgents();
        if (fosAgentList == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "You need to specify at least one FOSAgent", Level.ERROR);
        }
        List<FOSAgent> fosAgents = new ArrayList<>();
        Set<String> existingAgentIds = new HashSet<>();
        fosAgentList.forEach(fosAgent -> {
            FOSAgent newFOSAgent = mapper.map(fosAgent, FOSAgent.class);
            newFOSAgent.setCreatedBy(Long.toString(fosAgentsReq.getCallingUserId()));
            newFOSAgent.setCreationTime(System.currentTimeMillis());
            newFOSAgent.setLastUpdated(System.currentTimeMillis());
            newFOSAgent.setLastUpdatedBy(Long.toString(fosAgentsReq.getCallingUserId()));
            fosAgents.add(newFOSAgent);

            if (newFOSAgent.getReportingManagerId() != null) {
                existingAgentIds.add(newFOSAgent.getReportingManagerId());
            }

            if (newFOSAgent.getReportingTeamLeadId() != null) {
                existingAgentIds.add(newFOSAgent.getReportingTeamLeadId());
            }
        });

        if (ArrayUtils.isNotEmpty(existingAgentIds)) {
            List<String> ids = existingAgentIds.stream().collect(Collectors.toList());
            List<FOSAgent> agents = fosAgentDAO.getFOSAgentByIds(ids, Arrays.asList(FOSAgent.Constants.AGENT_CODE));
            if (agents.size() != ids.size()) {
                logger.error("There are some reporting manager ids or team lead ids which are invalid.");
            } else {
                logger.info("FOS agents with lead role or managerial roles are " + agents);
            }
        }
        fosAgents.forEach(fosAgent -> {
            try {
                fosAgentDAO.create(fosAgent);
            } catch (BadRequestException e) {
                logger.error("Error in creating fos agent ", e);
            }
        });
        return fosAgentList;
    }
}
