/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.notification;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.notification.responses.GetMessageLeadersRes;
import com.vedantu.platform.pojo.user.GetMessageUsersReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.WebUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;



/**
 *
 * @author jeet
 */
@Service
public class MessageUserManager {
    
        public MessageUserRes addMessageUser(MessageUserRes message) throws VException{
            String notificationEndPoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndPoint + "/messageuser/addMessageUser",
                    HttpMethod.POST, new Gson().toJson(message));
            //handle error correctly
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            MessageUserRes response = new Gson().fromJson(jsonString, MessageUserRes.class);
            return response;
        }
        
        public MessageUserRes getMessageById(Long messageId) throws VException{
            String notificationEndPoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndPoint + "/messageuser/getMessageById?messageId=" + messageId,
                    HttpMethod.GET, null);
            //handle error correctly
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            MessageUserRes response = new Gson().fromJson(jsonString, MessageUserRes.class);
            return response;
        }
        
        public List<MessageUserRes> getMessages(GetMessageUsersReq req) throws VException{
            String notificationEndPoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
            String queryString=WebUtils.INSTANCE.createQueryStringOfObject(req);
            ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndPoint + "/messageuser/getMessageUsers?"+queryString,
                    HttpMethod.GET, null);
            //handle error correctly
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            
            Type listType = new TypeToken<ArrayList<MessageUserRes>>(){}.getType();
            List<MessageUserRes> response = new Gson().fromJson(jsonString, listType);
            return response;
        }
        
        public GetMessageLeadersRes getMessageLeaders(GetMessageUsersReq req) throws VException{
            String notificationEndPoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
            String queryString=WebUtils.INSTANCE.createQueryStringOfObject(req);
            ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndPoint + "/messageuser/getMessageLeaders?"+queryString,
                    HttpMethod.GET, null);
            //handle error correctly
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            GetMessageLeadersRes response = new Gson().fromJson(jsonString, GetMessageLeadersRes.class);
            return response;
        }
}
