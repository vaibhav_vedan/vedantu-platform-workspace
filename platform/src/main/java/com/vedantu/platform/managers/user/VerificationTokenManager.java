/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.user;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.VerificationTokenType;
import com.vedantu.User.response.ProcessVerificationResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.notification.SMSManager;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.UserRedisManager;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class VerificationTokenManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VerificationTokenManager.class);

    @Autowired
    private ReferralManager referralManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private UserManager userManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private UserRedisManager userRedisManager;

    public VerificationTokenManager() {

    }

    public ProcessVerificationResponse processToken(String tokenCode, String password,
            boolean reloginRequired) throws VException, IOException {
        if (StringUtils.isEmpty(tokenCode)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "tokenCode empty");
        }
        logger.info("Request received to process token:" + tokenCode);
        String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        boolean resettingPassword = false;

        String url = userEndpoint + "/token/processToken?tokenCode=" + tokenCode;
        if (StringUtils.isNotEmpty(password)) {
            url += "&password=" + password;
            resettingPassword = true;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        //handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        ProcessVerificationResponse response = new Gson().fromJson(jsonString, ProcessVerificationResponse.class);
        User user = response.getUser();
        if (response.getSendWelcomeMail() != null && response.getSendWelcomeMail()
                && user != null && !Role.STUDENT.equals(user.getRole())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("user", user);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WELCOME_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
            //emailManager.sendWelcomeEmail(response.getUser());
        }
        if (response.getProcessReferralBonus()) {
            try {
                referralManager.processReferralBonusAsync(ReferralStep.VEMAIL, response.getUser().getId());
            } catch (Exception ex) {
                logger.error("Error in processReferralBonus", ex);
            }
            redisDAO.increment(userManager.getCacheKey(response.getUser().getRole()), 1);
        }

        if (resettingPassword && reloginRequired && response.getUserId() != null) {
            userRedisManager.addPasswordChangedAt(response.getUserId());
        }

        //Not needed at fos
        response.setUser(null);

        return response;

    }

    public String getEmailToken(Long userId, String email, VerificationTokenType verificationTokenType, String callingUserId) throws VException {
        String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        String url = userEndpoint + "/token/getEmailToken?userId=" + userId + "&email=" + email + "&verificationTokenType=" + verificationTokenType + "&callingUserId=" + callingUserId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

}
