/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.click2call;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.platform.dao.click2call.PhoneCallMetadataDAO;
import com.vedantu.platform.mongodbentities.click2call.PhoneCallMetadata;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebCommunicator;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.collections.MapUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

/**
 *
 * @author somil
 */
@Service
public class ExotelPhoneCallManager {//implements ICloudPhoneCallManager {

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ExotelPhoneCallManager.class);

    @Autowired
    PhoneCallMetadataDAO phoneCallMetadataDAO;

    private static String EXOTEL_BASE_URL;
    private static String EXOTEL_BASE_URL_CRM;
    private static String CALL_URL = "/Calls/connect";
    private static String CALL_STATUS_URL = "/Calls/";
    private static String SEND_SMS_URL = "/Sms/send";
    private static String DND_URL = "/Numbers/";
    private static String WHITELIST_URL = "/CustomerWhitelist/";
    public static final String CALL_CONTEXT_SESSION = "SESSION";
    public static final String CALL_CONTEXT_CRM = "CRM";
    public static final String CALL_CONTEXT_HOME_DEMO = "HOME_DEMO";

    private static String CALLER_ID;
    private static String CALLER_ID_CRM;
    private static String CALL_STATUS_UPDATE_ULR;
    static DocumentBuilderFactory dbf;
    static DocumentBuilder db = null;

//    public ExotelPhoneCallManager() {
//        super();
//        
//    }

    @PostConstruct
    public void init() {
        CALLER_ID = ConfigUtils.INSTANCE.getStringValue("exotel.caller.id");
        CALLER_ID_CRM = ConfigUtils.INSTANCE.getStringValue("exotel.caller.id.crm");
        CALL_STATUS_UPDATE_ULR = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT") + ConfigUtils.INSTANCE.getStringValue("url.exotel.call.status.update");
        try {
            dbf = DocumentBuilderFactory.newInstance();
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error(e);
        }
        EXOTEL_BASE_URL = "https://"
                + ConfigUtils.INSTANCE.getStringValue("exotel.sid") + ":"
                + ConfigUtils.INSTANCE.getStringValue("exotel.token")
                + "@twilix.exotel.in/v1/Accounts/"
                + ConfigUtils.INSTANCE.getStringValue("exotel.sid");
        EXOTEL_BASE_URL_CRM = "https://"
                + ConfigUtils.INSTANCE.getStringValue("exotel.sid.crm") + ":"
                + ConfigUtils.INSTANCE.getStringValue("exotel.token.crm")
                + "@twilix.exotel.in/v1/Accounts/"
                + ConfigUtils.INSTANCE.getStringValue("exotel.sid.crm");
    }

    //@Override
    public PhoneCallMetadata connectCall(String fromNumber, String toNumber,
            String contextId, String contextType, Long callingUserId) throws ForbiddenException {
        String url = EXOTEL_BASE_URL + CALL_URL;
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("From", fromNumber));
        params.add(new BasicNameValuePair("To", toNumber));
        params.add(new BasicNameValuePair("CallerId", CALLER_ID));
        if (contextType.equals(CALL_CONTEXT_CRM)) {
            params.add(new BasicNameValuePair("CallerId", CALLER_ID_CRM));
        }
        params.add(new BasicNameValuePair("CallType", "trans"));
        if (StringUtils.isNotEmpty(CALL_STATUS_UPDATE_ULR)) {
            params.add(new BasicNameValuePair("StatusCallback",
                    CALL_STATUS_UPDATE_ULR));
        }

        String response = WebCommunicator.loadPostWebData(url, params);

        logger.info("response: " + response);

        Element tResponse = getRootElement(response, "Call");

        if (tResponse == null) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Call not allowed to number :"
                    + toNumber);
        }

        String callSid = getFirstElementValueByTagName(tResponse, "Sid");

        PhoneCallMetadata phoneCallMetadata = new PhoneCallMetadata(callSid,
                contextId, contextType, fromNumber, toNumber);

        String status = getFirstElementValueByTagName(tResponse, "Status");
        phoneCallMetadata.setStatus(status);

        phoneCallMetadataDAO.create(phoneCallMetadata, callingUserId);

        logger.info("connectCall " + phoneCallMetadata);

        return phoneCallMetadata;
    }

    //@Override
    public PhoneCallMetadata updatePhoneCallMetadata(String callSid,
            Map<String, Object> dataMap, Long callingUserId) {

        if (StringUtils.isEmpty(callSid)) {
            logger.warn("no phoneCallMetadata found for callSid:" + callSid);
            return null;
        }
        PhoneCallMetadata phoneCallMetadata = phoneCallMetadataDAO
                .getPhoneCallMetadataCallSid(callSid);
        if (phoneCallMetadata == null || MapUtils.isEmpty(dataMap)) {
            logger.warn("no phoneCallMetadata found for callSid:" + callSid);
            return null;
        }

        phoneCallMetadata.setStatus((String) dataMap.get("Status"));

        String recordingUrl = dataMap.get("RecordingUrl") != null ? dataMap
                .get("RecordingUrl").toString() : null;
        phoneCallMetadata.setRecordingUrl(recordingUrl);

        String callStatusUrl = EXOTEL_BASE_URL + CALL_STATUS_URL + callSid;
        String response = WebCommunicator.loadGetWebData(callStatusUrl);
        logger.info("response: " + response);
        Element tResponse = getRootElement(response, "Call");

        String endTimeStr = getFirstElementValueByTagName(tResponse, "EndTime");
        try {
            Date endDate = DateFormat.getInstance().parse(endTimeStr);
            phoneCallMetadata.setEndTime(endDate.getTime());
        } catch (ParseException e) {
            logger.error("updatePhoneCallMetadata", e);
        }

        String durationString = getFirstElementValueByTagName(tResponse,
                "Duration");
        int duration = StringUtils.isEmpty(durationString) ? 0 : Integer
                .parseInt(durationString);
        phoneCallMetadata.setDuration(duration);
        phoneCallMetadataDAO.create(phoneCallMetadata, callingUserId);
        return phoneCallMetadata;
    }

//	@Override
//	public String sendSMS(String toNumber, String message) {
//
//		LOG.info("...........5............" + toNumber + " " + message);
//		logEntering("sendSMS [toNumber:" + toNumber + ", " + message + "]");
//		String url = EXOTEL_BASE_URL + SEND_SMS_URL;
//		List<NameValuePair> params = new ArrayList<NameValuePair>();
//		params.add(new BasicNameValuePair("From", CALLER_ID));
//		params.add(new BasicNameValuePair("To", toNumber));
//		params.add(new BasicNameValuePair("Body", message.trim()));
//		String response = WebCommunicator.loadPostWebData(url, params);
//
//		LOG.fine("response : " + response);
//
//		Element tResponse = getRootElement(response, "SMSMessage");
//		if (tResponse == null) {
//			return null;
//		}
//		String sid = getFirstElementValueByTagName(tResponse, "Sid");
//
//		logExiting("sendSMS", sid);
//		return sid;
//
//	}
//	@Override
//	public boolean isDNDNumber(String number) throws BadRequestException {
//		logEntering("isDNDNumber [number:" + number + "]");
//		String url = EXOTEL_BASE_URL + DND_URL + number;
//		String response = WebCommunicator.loadGetWebData(url);
//		LOG.fine("response: " + response);
//
//		Element tResponse = getRootElement(response, "Numbers");
//		if (tResponse == null) {
//			throw VExceptionFactory.INSTANCE.badRequestException(
//					ErrorCode.BAD_REQUEST_ERROR, "Number provide is not valid",
//					null);
//		}
//
//		String dndText = getFirstElementValueByTagName(tResponse, "DND");
//		/*
//		  for now disabling the operator name check String operatorName =
//		  getFirstElementValueByTagName(tResponse, "OperatorName"); if
//		  (StringUtils.isEmpty(operatorName)) { throw
//		  VExceptionFactory.INSTANCE.badRequestException(
//		  ErrorCode.INVALID_PHONE_NUMBER, "provided number [" + number +
//		  "] is not valid", null); }
//		 */
//
//		LOG.fine("dndText: " + dndText);
//		boolean isDNDNumber = dndText == null
//				|| "Yes".equalsIgnoreCase(dndText);
//		logExiting("isDNDNumber", isDNDNumber);
//		return isDNDNumber;
//	}
//
//	@Override
//	public boolean addToDNDWhitelist(String number) {
//
//		logEntering("addToDNDWhitelist [number:" + number + "]");
//		String url = EXOTEL_BASE_URL + WHITELIST_URL;
//
//		List<NameValuePair> params = new ArrayList<NameValuePair>();
//		params.add(new BasicNameValuePair("VirtualNumber", CALLER_ID));
//		params.add(new BasicNameValuePair("Number[]", number));
//
//		String response = WebCommunicator.loadPostWebData(url, params);
//
//		LOG.fine("response: " + response);
//
//		Element tResponse = getRootElement(response, "Result");
//		if (tResponse == null) {
//			return false;
//		}
//
//		int addedNumberCount = Integer.parseInt(getFirstElementValueByTagName(
//				tResponse, "Processed"));
//		LOG.fine("addToDoNDWhitelist: " + addedNumberCount);
//
//		logExiting("addToDNDWhitelist", addedNumberCount);
//
//		return addedNumberCount >= 1;
//	}
//
//	@Override
//	public boolean isWhitelistedNumber(String number) {
//
//		logEntering("isWhitelistedNumber [number:" + number + "]");
//		String url = EXOTEL_BASE_URL + WHITELIST_URL + number;
//		String response = WebCommunicator.loadGetWebData(url);
//		LOG.fine("response: " + response);
//
//		Element tResponse = getRootElement(response, "Result");
//		if (tResponse == null) {
//			return false;
//		}
//
//		String statusText = getFirstElementValueByTagName(tResponse, "Status");
//		LOG.fine("isWhitelistedNumber: " + statusText);
//
//		boolean isWhitelistedNumber = statusText != null
//				&& "Whitelist".equals(statusText);
//
//		logExiting("isWhitelistedNumber", isWhitelistedNumber);
//
//		return isWhitelistedNumber;
//	}
//
//	@Override
//	public boolean isCallAllowed(String number) throws BadRequestException {
//		logEntering("isCallAllowed [number:" + number + "]");
//		boolean isCallAllowed = !isDNDNumber(number)
//				|| isWhitelistedNumber(number);
//		logExiting("isCallAllowed", isCallAllowed);
//		return isCallAllowed;
//	}
    private Element getRootElement(String xmlResponse, String rootTagName) {
        InputSource is = new InputSource();
        Element tResponse = null;
        try {
            is.setCharacterStream(new StringReader(xmlResponse));
            Document doc = db.parse(is);
            tResponse = (Element) doc.getElementsByTagName(rootTagName).item(0);
        } catch (Exception e) {
            logger.error("getRootElement", e);
        }
        return tResponse;
    }

    private String getFirstElementValueByTagName(Element xmlRootElement,
            String tagName) {
        Element tagElement = (Element) xmlRootElement.getElementsByTagName(
                tagName).item(0);
        String tagValue = getCharacterDataFromElement(tagElement);
        return tagValue;
    }

    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof org.w3c.dom.CharacterData) {
            org.w3c.dom.CharacterData cd = (org.w3c.dom.CharacterData) child;
            return cd.getData();
        }
        return null;
    }

    public List<PhoneCallMetadata> getPhonecallMetadata(String contextId, String contextType) throws NotFoundException {
        return phoneCallMetadataDAO.getPhonecallMetadata(contextId, contextType);
    }
}
