package com.vedantu.platform.managers.subscription;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.BuyItemsReq;
import com.vedantu.dinero.request.OrderItem;
import com.vedantu.dinero.request.PrepareInstalmentsReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.lms.LMSManager;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestInfo;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestNotificationInfo;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.scheduling.SchedulingUtils;
import com.vedantu.util.subscription.SubscriptionRequestSubState;
import com.vedantu.util.subscription.UpdateCalendarSessionSlotsReq;
import com.vedantu.util.subscription.UpdateSubscriptionRequestSubState;

@Service
public class SubscriptionRequestTaskManager {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private PaymentManager paymentManager;

	@Autowired
	private LMSManager lmsManager;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionRequestTaskManager.class);

	public String fosEndpoint;
	public String schedulingEndpoint;
	public String subscriptionEndpoint;
	public String dineroEndpoint;
	private final Gson gson = new Gson();

	@PostConstruct
	public void init() {
		fosEndpoint = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
		schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
		subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
		dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
	}

	public void sendCommunication(SubscriptionRequestInfo subscriptionRequestInfo, CommunicationType communicationType)
			throws VException {
		logger.info("Entering: " + communicationType);
		SubscriptionRequestNotificationInfo subscriptionRequestNotificationInfo = new SubscriptionRequestNotificationInfo(
				subscriptionRequestInfo);
		subscriptionRequestNotificationInfo.setCommunicationType(communicationType);

		if (CommunicationType.SUBSCRIPTION_REQUEST_ACCEPTED.equals(communicationType)) {
			// fetching order info and seeing if it is instalments
			Orders order = paymentManager.getOrderBySubscriptionId(subscriptionRequestInfo.getSubscriptionId());
			if (order != null && PaymentType.INSTALMENT.equals(order.getPaymentType())) {
				List<InstalmentInfo> instalmentInfos = paymentManager.getInstalments(order.getId());
				subscriptionRequestNotificationInfo.setPaymentType(order.getPaymentType());
				subscriptionRequestNotificationInfo.setInstalments(instalmentInfos);
			}
		}

		Map<String, Object> payload = new HashMap<>();
		payload.put("subscriptionRequestNotificationInfo", subscriptionRequestNotificationInfo);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_REQUEST_REMINDER, payload);
		asyncTaskFactory.executeTask(params);
		logger.info("Exiting");
	}

	@Async
	public void processSubscriptionRequestSubStateAsync(SubscriptionRequestInfo subscriptionRequestInfo) {
		processSubscriptionRequestSubState(subscriptionRequestInfo);
	}

	public void processSubscriptionRequestSubState(SubscriptionRequestInfo subscriptionRequestInfo) {
		logger.info("processSubscriptionRequestSubState : " + subscriptionRequestInfo.toString());
		SubscriptionRequestSubState subState = subscriptionRequestInfo.getSubState();
		if (subState == null) {
			logger.error("SubscriptionRequestSubStateError - substate missing for request id "
					+ subscriptionRequestInfo.toString());
		}

		SubscriptionRequestSubState nextState = null;
		switch (subState) {
		case INIT:
		case AMOUNT_DEDUCTED:
			try {
				OrderInfo response = buyItems(subscriptionRequestInfo);
				subscriptionRequestInfo.setSubscriptionId(response.getContextId());
				nextState = SubscriptionRequestSubState.SUBSCRIPTION_CREATED;
			} catch (VException ex) {
				logger.error("scheduleSubscriptionRequestSessionError - " + subscriptionRequestInfo.getId() + " ex : "
						+ ex.getMessage() + " ex : " + ex.toString());
			}
			break;
		case SUBSCRIPTION_CREATED:
			try {
				scheduleSubscriptionRequestSession(subscriptionRequestInfo);
			} catch (Exception ex) {
				logger.error("scheduleSubscriptionRequestSessionError - " + subscriptionRequestInfo.getId() + " ex : "
						+ ex.getMessage() + " ex : " + ex.toString());
			}
			nextState = SubscriptionRequestSubState.SESSIONS_SCHEDULED;
			break;
		case SESSIONS_SCHEDULED:
			try {
				updateSubscriptionRequestCalendarEntries(subscriptionRequestInfo);
			} catch (Exception ex) {
				logger.error("updateSubscriptionRequestCalendarEntriesError - " + subscriptionRequestInfo.getId()
						+ ex.getMessage() + " ex : " + ex.toString(), ex);
			}
			nextState = SubscriptionRequestSubState.CALENDAR_ENTRIES_UPDATED;
			break;
		case CALENDAR_ENTRIES_UPDATED:
			try {
				updateSubscriptionRequestCalendarBlock(subscriptionRequestInfo);
			} catch (Exception ex) {
				logger.error("updateSubscriptionRequestCalendarBlockError - " + subscriptionRequestInfo.getId()
						+ " ex : " + ex.getMessage() + " ex : " + ex.toString());
			}
			nextState = SubscriptionRequestSubState.CALENDAR_BLOCK_UPDATED;
			break;
		case CALENDAR_BLOCK_UPDATED:
		case SUCCESSFUL:
			try {
				updateSubscriptionRequestSubState(subscriptionRequestInfo.getId(),
						SubscriptionRequestSubState.SUCCESSFUL);
				sendCommunication(subscriptionRequestInfo, CommunicationType.SUBSCRIPTION_REQUEST_ACCEPTED);

				// Trigger lms changes
				if (subscriptionRequestInfo.getOfferingId() != null && subscriptionRequestInfo.getOfferingId() > 0l) {
//					lmsManager.contentShareOTOStructured(subscriptionRequestInfo.getOfferingId(),
//							subscriptionRequestInfo.getStudentId(), subscriptionRequestInfo.getTeacherId());
				}
			} catch (VException ex) {
				logger.error("updateSubscriptionRequestSubStateError - " + subscriptionRequestInfo.getId() + " ex : "
						+ ex.getMessage() + " ex : " + ex.toString());
			}
			break;
		default:
			break;
		}

		if (nextState != null) {
			logger.info("next state : " + nextState);
			try {
				subscriptionRequestInfo = updateSubscriptionRequestSubState(subscriptionRequestInfo.getId(), nextState);
				processSubscriptionRequestSubState(subscriptionRequestInfo);
			} catch (VException ex) {

			}
		}
	}

	public SubscriptionRequestInfo updateSubscriptionRequestSubState(Long id, SubscriptionRequestSubState subState)
			throws VException {
		UpdateSubscriptionRequestSubState req = new UpdateSubscriptionRequestSubState(id, subState);
		logger.info("updateSubscriptionRequestSubState - " + req.toString());
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/updateSubState",
				HttpMethod.POST, new Gson().toJson(req));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String jsonString = resp.getEntity(String.class);
		logger.info("Response from Subscription: " + jsonString);
		SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
				SubscriptionRequestInfo.class);
		return subscriptionRequestInfo;
	}

	public OrderInfo buyItems(SubscriptionRequestInfo subscriptionRequestInfo) throws VException {
		String paymentDetails = subscriptionRequestInfo.getPaymentDetails();
		JsonObject json = new Gson().fromJson(paymentDetails, JsonObject.class);

		String teacherDiscountCouponId = null, vedantuDiscountCouponId = null;
		PaymentType paymentType = PaymentType.BULK;
		if (json.get("teacherDiscountCouponId") != null) {
			teacherDiscountCouponId = json.get("teacherDiscountCouponId").getAsString();
		}

		if (json.get("vedantuDiscountCouponId") != null) {
			vedantuDiscountCouponId = json.get("vedantuDiscountCouponId").getAsString();
		}

		if (json.get("paymentType") != null && PaymentType.valueOf(json.get("paymentType").getAsString()) != null) {
			paymentType = PaymentType.valueOf(json.get("paymentType").getAsString());
		}

		SessionSchedule schedule = new Gson().fromJson(subscriptionRequestInfo.getSlots(), SessionSchedule.class);

		List<OrderItem> items = new ArrayList<>();
		OrderItem item = new OrderItem(subscriptionRequestInfo.getPlanId(), EntityType.PLAN, 1,
				subscriptionRequestInfo.getTotalHours(), subscriptionRequestInfo.getModel(),
				subscriptionRequestInfo.getSubModel(), subscriptionRequestInfo.getTarget(),
				subscriptionRequestInfo.getGrade(), subscriptionRequestInfo.getBoardId(), schedule,
				subscriptionRequestInfo.getTeacherId(), subscriptionRequestInfo.getOfferingId(), false,
				teacherDiscountCouponId, vedantuDiscountCouponId, subscriptionRequestInfo.getNote());
		items.add(item);

		// TODO there is no contextId in subscriptionRequestInfo ??
		BuyItemsReq buyItemsReq = new BuyItemsReq(items, true, null, subscriptionRequestInfo.getId().toString(),
				PurchaseFlowType.SUBSCRIPTION_REQUEST, paymentType);
		buyItemsReq.setCallingUserId(subscriptionRequestInfo.getStudentId());
		buyItemsReq.setCallingUserRole(Role.STUDENT);

		ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/buyItems", HttpMethod.POST,
				new Gson().toJson(buyItemsReq));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String json2 = resp.getEntity(String.class);
		OrderInfo orderInfo = gson.fromJson(json2, OrderInfo.class);
		if (orderInfo != null && orderInfo.getNeedRecharge()) {
			throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE,
					"No suffucient locked balance to purchase subscription");
		}
		return orderInfo;
	}

	private void scheduleSubscriptionRequestSession(SubscriptionRequestInfo subscriptionRequestInfo) throws VException {
		logger.info("scheduleSubscriptionRequestSession req : " + subscriptionRequestInfo.toString());
		MultipleSessionScheduleReq scheduleReq = createScheduleRequestFromSubscriptionRequest(subscriptionRequestInfo);
		logger.info("scheduleSubscriptionRequestSessions pre session scheduling " + subscriptionRequestInfo.toString());
		logger.info("Scheduling request " + scheduleReq);
		if (scheduleReq != null && ArrayUtils.isNotEmpty(scheduleReq.getSlots())) {
			ClientResponse fosResp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/session/createMultiple",
					HttpMethod.POST, new Gson().toJson(scheduleReq));
			VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
			String resp = fosResp.getEntity(String.class);
			logger.info("Subscription scheduleSession Response came: " + resp);
		}
	}

	private MultipleSessionScheduleReq createScheduleRequestFromSubscriptionRequest(
			SubscriptionRequestInfo subscriptionRequest) throws VException {
		MultipleSessionScheduleReq req = new MultipleSessionScheduleReq();
		List<Long> studentIds = new ArrayList<>();
		studentIds.add(subscriptionRequest.getStudentId());
		req.setStudentIds(studentIds);
		req.setTeacherId(subscriptionRequest.getTeacherId());
		req.setSubscriptionId(subscriptionRequest.getSubscriptionId());
		req.setSubject(subscriptionRequest.getSubject());
		req.setSessionModel(subscriptionRequest.getModel());
		if (subscriptionRequest.getCreatedBy() != null) {
			req.setCallingUserId(Long.parseLong(subscriptionRequest.getCreatedBy()));
		}

		SessionSchedule schedule = getSessionSchedule(subscriptionRequest);
		List<SessionSlot> sessionSlots = SchedulingUtils.getSessionSlotsForAllWeeks(schedule);
		req.setTitle(subscriptionRequest.getTitle());
		req.setSlots(sessionSlots);
		// TODO : req.setTopic(subscriptionRequest.getT);
		// TODO : req.setDescription();
		return req;
	}

	private void updateSubscriptionRequestCalendarEntries(SubscriptionRequestInfo subscriptionRequestInfo)
			throws VException {
		logger.info("scheduleSubscriptionRequestSession req : " + subscriptionRequestInfo.toString());
		UpdateCalendarSessionSlotsReq updateCalendarReq = createUpdateCalendarReqFromSubscriptionRequest(
				subscriptionRequestInfo);
		logger.info("scheduleSubscriptionRequestSessions pre session scheduling " + subscriptionRequestInfo.toString());
		if (updateCalendarReq.getScheduledSlots() != null && updateCalendarReq.getScheduledSlots().length > 0) {
			ClientResponse fosResp = WebUtils.INSTANCE.doCall(
					schedulingEndpoint + "/calendarEntry/updateCalendarSessionSlots", HttpMethod.POST,
					new Gson().toJson(updateCalendarReq));
			VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
			String resp = fosResp.getEntity(String.class);
			logger.info("Subscription scheduleSession Response came: " + resp);
		}
	}

	private UpdateCalendarSessionSlotsReq createUpdateCalendarReqFromSubscriptionRequest(
			SubscriptionRequestInfo subscriptionRequestInfo) throws VException {
		UpdateCalendarSessionSlotsReq req = new UpdateCalendarSessionSlotsReq();
		req.setStudentId(String.valueOf(subscriptionRequestInfo.getStudentId()));
		req.setTeacherId(String.valueOf(subscriptionRequestInfo.getTeacherId()));
		req.setSubscriptionRequestId(subscriptionRequestInfo.getId());

		SessionSchedule schedule = getSessionSchedule(subscriptionRequestInfo);
		req.setStartTime(schedule.getStartTime());
		req.setEndTime(schedule.getEndTime());
		req.setScheduledSlots(removeConflictSlots(schedule));
		return req;
	}

	private SessionSchedule getSessionSchedule(SubscriptionRequestInfo subscriptionRequest) throws VException {
		// finding the payment type to decide the slots to be scheduled
		String paymentDetails = subscriptionRequest.getPaymentDetails();
		JsonObject json = new Gson().fromJson(paymentDetails, JsonObject.class);
		PaymentType paymentType = PaymentType.BULK;

		if (json.get("paymentType") != null && PaymentType.valueOf(json.get("paymentType").getAsString()) != null) {
			paymentType = PaymentType.valueOf(json.get("paymentType").getAsString());
		}

		SessionSchedule schedule = new Gson().fromJson(subscriptionRequest.getSlots(), SessionSchedule.class);

		if (PaymentType.INSTALMENT.equals(paymentType)) {
			PrepareInstalmentsReq request = new PrepareInstalmentsReq(schedule, 0, null, null, 0);
			ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/prepareInstalments",
					HttpMethod.POST, new Gson().toJson(request));
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String json2 = resp.getEntity(String.class);
			logger.info("prepare Instalments response " + json2);
			Type listType = new TypeToken<ArrayList<InstalmentInfo>>() {
			}.getType();
			List<InstalmentInfo> instalments = new Gson().fromJson(json2, listType);
			logger.info("Instalments " + instalments);
			if (ArrayUtils.isNotEmpty(instalments)) {
				schedule = instalments.get(0).getSessionSchedule();
				long[] slotBitSet = SchedulingUtils.createSlotBitSetLongValues(schedule);
				schedule.setSlotBitSet(slotBitSet);
				// TODO get conflicts to be on the safe side
			} else {
				logger.error("No Instalments found for " + subscriptionRequest);
				throw new NotFoundException(ErrorCode.INSTALMENTS_INCONSISTENT,
						"instalments empty for " + subscriptionRequest);
			}
		}
		return schedule;
	}

	private long[] removeConflictSlots(SessionSchedule schedule) {
		long[] conflictBitSetArr = schedule.getConflictSlotBitSet();
		if (conflictBitSetArr != null && conflictBitSetArr.length > 0) {
			long[] slotBitSetArr = schedule.getSlotBitSet();
			BitSet slotBitSet = BitSet.valueOf(slotBitSetArr);
			BitSet conflictBitSet = BitSet.valueOf(conflictBitSetArr);
			slotBitSet.andNot(conflictBitSet);
			return slotBitSet.toLongArray();
		} else {
			return schedule.getSlotBitSet();
		}
	}

	private void updateSubscriptionRequestCalendarBlock(SubscriptionRequestInfo subscriptionRequestInfo)
			throws VException {
		logger.info("updateSubscriptionRequestCalendarBlock req : " + subscriptionRequestInfo.toString());
		String getUrl = schedulingEndpoint + "/calendarBlockEntry/markProcessed?referenceId="
				+ subscriptionRequestInfo.getId();
		ClientResponse fosResp = WebUtils.INSTANCE.doCall(getUrl, HttpMethod.POST, null);
		VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
		String resp = fosResp.getEntity(String.class);
		logger.info("updateSubscriptionRequestCalendarBlock Response came: " + resp);
	}
}
