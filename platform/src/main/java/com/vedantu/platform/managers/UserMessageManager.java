package com.vedantu.platform.managers;

import com.google.gson.Gson;
import com.vedantu.User.User;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.dao.UserMessageDAO;
import com.vedantu.platform.mongodbentities.UserMessage;
import com.vedantu.platform.request.usermessage.SendQueryReq;
import com.vedantu.platform.response.usermessage.SendQueryRes;
import com.vedantu.platform.request.usermessage.SendUserMessageReq;
import com.vedantu.platform.response.usermessage.SendUserMessageRes;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.AddressException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class UserMessageManager {

    @Autowired
    UserManager userManager;

    @Autowired
    UserMessageDAO messageDAO;

    @Autowired
    CommunicationManager emailManager;
    
    @Autowired
    LeadSquaredManager leadSquaredManager;
    
    @Autowired
    AsyncTaskFactory asyncTaskFactory;
    
    public SendUserMessageRes sendMessage(SendUserMessageReq sendMessageReq) throws VException, AddressException, UnsupportedEncodingException {
        sendMessageReq.verify();
        SendUserMessageRes sendMessageRes;

        User user = userManager.getUserById(sendMessageReq.getUserId());
        if (null == user) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user does not exists");
        }
        UserMessage message = sendMessageReq.toMessage();
        messageDAO.send(message);
        sendMessageRes = new SendUserMessageRes(message);
        sendMessageRes.setFirstName(user.getFirstName());
        sendMessageRes.setLastName(user.getLastName());
        sendMessageRes.setMobileNumber(user.getContactNumber());
            
        Map<String, Object> payload = new HashMap<>();
        payload.put("userPojo", user);
        payload.put("message", message);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_MESSAGE_EMAIL, payload);
        asyncTaskFactory.executeTask(params);


          //emailManager.sendUserMessageEmail(user, message);
        try {
            Map<String, String> lsParams = new HashMap<String, String>();
            Gson gson = new Gson();
            lsParams.put("userEmail", user.getEmail());
            lsParams.put("mobileNumber", user.getContactNumber());
            lsParams.put("messagePojo", gson.toJson(message));
            LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_QUERY, user, lsParams, sendMessageReq.getCallingUserId());
            leadSquaredManager.executeAsyncTask(req);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sendMessageRes;
    }

    public SendQueryRes sendQuery(SendQueryReq sendQueryReq) throws VException, UnsupportedEncodingException, AddressException {
        sendQueryReq.verify();
        SendQueryRes sendQueryRes = new SendQueryRes();

        if(StringUtils.isNotEmpty(sendQueryReq.getEmailId())) {
            emailManager.sendQueryEmail(sendQueryReq.getName(), sendQueryReq.getEmailId(),
                    sendQueryReq.getMobileNumber(), sendQueryReq.getSubject(), sendQueryReq.getMessage(),
                    sendQueryReq.getEmailType());
        }
        sendQueryRes.setSuccess(true);
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("userEmail", sendQueryReq.getEmailId());
            params.put("messageText", sendQueryReq.getMessage());
            params.put("mobileNumber", sendQueryReq.getMobileNumber());
            params.put("userName", sendQueryReq.getName());
            params.put("subject", sendQueryReq.getSubject());
            params.put("messageType", CommunicationType.INVITE_REQUEST.name());

            LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_QUERY, null, params, sendQueryReq.getCallingUserId());
//            LeadSquaredTask.createtask(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_QUERY,
//                    TaskName.POST_TO_LEADSQUARED, sendQueryReq.getCallingUserId(), null, params);
            leadSquaredManager.executeAsyncTask(req);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return sendQueryRes;
    }
}
