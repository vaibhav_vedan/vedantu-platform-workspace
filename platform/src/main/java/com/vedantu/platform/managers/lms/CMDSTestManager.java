package com.vedantu.platform.managers.lms;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.reflect.TypeToken;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.lms.cmds.pojo.TestAndAttemptDetails;
import com.vedantu.lms.cmds.request.*;
import com.vedantu.lms.request.AddOrEditTestUserReq;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.onetofew.enums.AgendaType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.GetBundleTestDetailsReq;
import com.vedantu.util.*;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.CMDSShareContentinfo;
import com.vedantu.lms.cmds.request.CMDSEvaluateTestAttemptReq;
import com.vedantu.lms.cmds.request.CMDSShareOTFContentsReq;
import com.vedantu.lms.cmds.request.CMDSStartTestAttemptReq;
import com.vedantu.lms.cmds.request.CMDSSubmitTestAttemptReq;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.AgendaPojo;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.ContentPojo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.WebUtils;

@Service
public class CMDSTestManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSTestManager.class);

	private String LMS_ENDPOINT;
        
        private String SUBSCRIPTION_ENDPOINT;

	private static Gson gson = new Gson();

	@Autowired
	private FosUtils userUtils;

	@Autowired
	AsyncTaskFactory asyncTaskFactory;

	@PostConstruct
	public void init() {
		LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
		SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT")+"/";
	}

	public void shareOTFBatch(String batchId) {
		if (StringUtils.isEmpty(batchId)) {
			logger.error("contentShareOTFBatch - Invalid batch id " + batchId);
			return;
		}

		String location = SUBSCRIPTION_ENDPOINT + "/batch/" + batchId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(location, HttpMethod.GET, null);
		try {
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		} catch (Exception ex) {
			logger.error("Error processing cmds share events for otf batch : " + batchId);
			return;
		}
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);

		try {
			BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);
			logger.info("batchInfo: "+batchInfo);
			if (batchInfo != null) {
				logger.info("batch info not null");

				List<AgendaPojo> agenda = batchInfo.getAgenda();
                                //TODO fetch batch students in batches 
				List<UserBasicInfo> students = new ArrayList<>();//batchInfo.getEnrolledStudents();
				List<UserBasicInfo> teachers = batchInfo.getTeachers();
				Map<String, UserBasicInfo> userMap = getUserInfoMap(teachers);

				if (!CollectionUtils.isEmpty(agenda) && !CollectionUtils.isEmpty(students)
						&& !CollectionUtils.isEmpty(teachers)) {
					logger.info("agenda: "+ agenda);
					CMDSShareOTFContentsReq contentShareReq = new CMDSShareOTFContentsReq();
					contentShareReq.setEngagementType(EngagementType.OTF);
					contentShareReq.setContextType(EntityType.OTF);
					contentShareReq.setContextId(batchId);
					contentShareReq.setStudents(students);
					if (batchInfo.getCourseInfo() != null
							&& !StringUtils.isEmpty(batchInfo.getCourseInfo().getTitle())) {
						contentShareReq.setCourseName(batchInfo.getCourseInfo().getTitle());
					}
					for (AgendaPojo agendaEntry : agenda) {
						logger.info("agendaEntry: "+agendaEntry);
						if (AgendaType.CONTENT.equals(agendaEntry.getType())) {
							ContentPojo contentPojo = agendaEntry.getContentPOJO();
							logger.info("contentPojo"+ contentPojo);
							if (contentPojo != null) {
								List<ContentInfo> contentInfos = contentPojo.getContentList();
								logger.info("contentInfos" + contentInfos);
								if (!CollectionUtils.isEmpty(contentInfos)) {
									for (ContentInfo contentInfo : contentInfos) {
										String url = contentInfo.getUrl();
										logger.info("contentInfo: "+contentInfo);
										if (com.vedantu.util.StringUtils.isEmpty(url) || !url.contains("/cmds")) {
											continue;
										}

										CMDSShareContentinfo shareContentinfo = new CMDSShareContentinfo();
										shareContentinfo.setContentLink(url);
										if (!com.vedantu.util.StringUtils.isEmpty(contentInfo.getTeacherId())) {
											if (!userMap.containsKey(contentInfo.getTeacherId())) {
												continue;
											}
											shareContentinfo.setTeacherInfo(userMap.get(contentInfo.getTeacherId()));
										} else {
											shareContentinfo.setTeacherInfo(teachers.get(0));
										}
										contentShareReq.addShareContentInfo(shareContentinfo);
									}
									logger.info("Sharing content "+contentShareReq);

									if (contentShareReq.getShareInfo().size() > 0) {
										createCMDSShareEvents(contentShareReq);
									}
								}
							}
						}
					}
				}

			}
		} catch (Exception ex) {
			logger.error("contentShareOTFBatch - " + ex.getMessage() + " batchId:" + batchId);
		}
	}

	public Map<String, UserBasicInfo> getUserInfoMap(List<UserBasicInfo> infos) {
		Map<String, UserBasicInfo> userInfoMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(infos)) {
			for (UserBasicInfo userBasicInfo : infos) {
				userInfoMap.put(String.valueOf(userBasicInfo.getUserId()), userBasicInfo);
			}
		}

		return userInfoMap;
	}

	public void createCMDSShareEvents(CMDSShareOTFContentsReq req) {
		logger.info("createOTFContentShares request :" + req.toString());
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/shareBulk", HttpMethod.POST,
				new Gson().toJson(req), true);
		logger.info("resp : " + resp.toString());

	}

	public PlatformBasicResponse shareTest(ShareTestReq request) {
		List<Long> userIds = new ArrayList<>(Arrays.asList(request.getTeacherId()));
		userIds.addAll(request.getStudentIds());
		Map<Long, UserBasicInfo> map = userUtils.getUserBasicInfosMapFromLongIds(userIds, true);

		UserBasicInfo teacherInfo = map.get(request.getTeacherId());
		CMDSShareOTFContentsReq req = new CMDSShareOTFContentsReq();
		CMDSShareContentinfo cmdsShareContentinfo = new CMDSShareContentinfo();
		cmdsShareContentinfo.setTeacherInfo(teacherInfo);
		cmdsShareContentinfo.setTestId(request.getTestId());
		map.remove(request.getTeacherId());
		req.addShareContentInfo(cmdsShareContentinfo);
		List<UserBasicInfo> studentInfos = new ArrayList<>(map.values());
		req.setStudents(studentInfos);
		req.setSendCommunication(true);
		createCMDSShareEvents(req);
		return new PlatformBasicResponse();
	}



	public String createTest(HttpServletRequest request) throws VException, IOException {
		String requestBody = PlatformTools.getBody(request);
		logger.info("Request body : " + requestBody);
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/create", HttpMethod.POST, requestBody, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	@Deprecated
	public String getTests(HttpServletRequest request) throws VException {
		String queryString = request.getQueryString();
		logger.info("Request body : " + queryString);
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/getTests?" + queryString,
				HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	public void getTestDetails(HttpServletRequest request, HttpServletResponse response)
			throws VException, IOException {
		String queryString = request.getQueryString();
		logger.info("Request body : " + queryString);
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/getTestDetails?" + queryString,
				HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		InputStream is = resp.getEntityInputStream();
		ServletOutputStream sos = response.getOutputStream();
		IOUtils.copy(is, sos);
		logger.info("Response copied");
	}

	@Deprecated
	public void startCMDSTest(CMDSStartTestAttemptReq req, HttpServletResponse response) throws IOException, VException {
		if(StringUtils.isEmpty(req.getContentInfoId()) && req.getCallingUserId()!=null) { //public test registered user
			UserBasicInfo studentInfo = userUtils.getUserBasicInfo(req.getCallingUserId(), false);
			req.setStudentInfo(studentInfo);
		}
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/startAttempt", HttpMethod.POST,
				gson.toJson(req), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		InputStream is = resp.getEntityInputStream();
		ServletOutputStream sos = response.getOutputStream();
		IOUtils.copy(is, sos);
		logger.info("Response copied");

	}

	@Deprecated
	public String submitCMDSTest(CMDSSubmitTestAttemptReq request) throws IOException, VException {
		logger.info("Request body : " + request.toString());
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/submitAttempt", HttpMethod.POST,
				gson.toJson(request), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	@Deprecated
	public String evaluateCMDSTest(CMDSEvaluateTestAttemptReq request) throws IOException, VException {
		logger.info("Request body : " + request.toString());
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/evaluateTest", HttpMethod.POST,
				gson.toJson(request), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	@Deprecated
	public void getTestAttemptById(String attemptId, Long callingUserId, Role callingUserRole, HttpServletResponse response) throws VException, IOException {
		logger.info("callingUserRole : " + callingUserRole + " callingUserId : " + callingUserId + " attemptId : "
				+ attemptId);
		String url = LMS_ENDPOINT + "cmds/test/getAttemptDetails?attemptId="+ attemptId;
		if(callingUserId!=null && callingUserRole!=null) {
			url += "&callingUserId=" + callingUserId + "&callingUserRole=" + callingUserRole;
		}
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
				null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		InputStream is = resp.getEntityInputStream();
		ServletOutputStream sos = response.getOutputStream();
		IOUtils.copy(is, sos);
		logger.info("Response copied");
	}

	public String attemptViewed(String attemptId, Long callingUserId, Role callingUserRole) throws VException {
		logger.info("callingUserRole : " + callingUserRole + " callingUserId : " + callingUserId + " attemptId : "
				+ attemptId);

		String url = LMS_ENDPOINT + "cmds/test/attemptViewed?attemptId="+ attemptId;
		if(callingUserId!=null && callingUserRole!=null) {
			url += "&callingUserId=" + callingUserId + "&callingUserRole=" + callingUserRole;
		}
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	@Deprecated
    public String submitQuestionAttempt(BulkSubmitQuestionAttemptRequest request) throws VException {
		logger.info("Request body : " + request.toString());
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/submitQuestionAttempt", HttpMethod.POST,
				gson.toJson(request), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
    }


	public String endTests() throws VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/endTests", HttpMethod.POST,
				null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

	public String deleteTest(DeleteTestReq request) throws VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/delete", HttpMethod.POST,
				new Gson().toJson(request), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
	}

    public String addOrEditTestUser(AddOrEditTestUserReq req) throws VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/test/addOrEditTestUser", HttpMethod.POST,
				new Gson().toJson(req), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info("Response : " + jsonString);
		return jsonString;
    }

    public Map<String,TestAndAttemptDetails> getTestAndAttemptDetails(GetBundleTestDetailsReq req) throws VException {
		String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
		logger.info("queryString : " + queryString);
		String url = LMS_ENDPOINT + "cmds/test/getTestAndAttemptDetails?" + queryString;
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
				null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		Type listType1 = new TypeToken<Map<String, TestAndAttemptDetails>>() {
		}.getType();
		Map<String, TestAndAttemptDetails> response = new Gson().fromJson(jsonString, listType1);
		return response;
	}
}
