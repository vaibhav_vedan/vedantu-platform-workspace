/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.request.dinero.SessionPayoutRequest;
import com.vedantu.platform.response.dinero.GetHourlyRateResponse;
import com.vedantu.platform.response.dinero.GetTeacherSessionInfoResponse;
import com.vedantu.platform.response.dinero.SessionPayoutResponse;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.managers.wave.BillingManager;
import com.vedantu.scheduling.request.session.ProcessSessionPayoutReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.pojo.Subscription;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SessionModel;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class PayoutManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    UserManager userManager;

    @Autowired
    SessionManager sessionManager;

    @Autowired
    BillingManager billingManager;

    @Autowired
    SubscriptionManager subscriptionManager;

    @Autowired
    CoursePlanManager coursePlanManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PayoutManager.class);

    public GetTeacherSessionInfoResponse getTeacherSessionInfos(Long startDate, Long endDate) throws VException {

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payout/getTeacherSessionInfos?startDate=" + startDate + "&endDate=" + endDate,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetTeacherSessionInfoResponse response = new Gson().fromJson(jsonString, GetTeacherSessionInfoResponse.class);

        return response;

    }

    public SessionPayoutResponse calculateSessionPayout(SessionBillingDetails sessionBillingDetails)
            throws VException {
        SessionPayoutResponse sessionPayoutResponse = new SessionPayoutResponse();
        logger.info(sessionBillingDetails.toString());
        String jsonString;

        if (sessionBillingDetails.getSubscriptionId() == null && !EntityType.COURSE_PLAN.equals(sessionBillingDetails.getContextType())) {
            logger.warn("Null SubscriptionId received");
            return sessionPayoutResponse;
        }

        Long subscriptionId = sessionBillingDetails.getSubscriptionId();
        Long sessionDuration = sessionBillingDetails.getSessionScheduledDuration();
        Long billingDuration = billingManager.getBillingDuration(sessionBillingDetails);

//        int minBillDurationInMillis = ConfigUtils.INSTANCE.getIntValue("payout.minimumBillableMinutes")
//                * DateTimeUtils.MILLIS_PER_MINUTE;

        long netBillDurationInMillis = billingDuration > 0 ?  billingDuration : 0;


        logger.info("Final billing duration:" + netBillDurationInMillis);
        billingDuration = netBillDurationInMillis;
        sessionBillingDetails.setBillingDuration(billingDuration);



        if(EntityType.COURSE_PLAN.equals(sessionBillingDetails.getContextType())) {
            if(!SessionModel.TRIAL.equals(sessionBillingDetails.getModel())) {
                coursePlanManager.completeCoursePlanSession(sessionBillingDetails);
            }

            processSessionPayout(Long.parseLong(sessionBillingDetails.getSessionId()),
                    sessionBillingDetails.getTeacherId(), sessionBillingDetails.getBillingDuration(),
                    0,
                    0, false);

            return sessionPayoutResponse;
        }

        SubscriptionResponse subscription = subscriptionManager.getSubscriptionBasicInfo(sessionBillingDetails.getSubscriptionId());


        GetHourlyRateResponse getHourlyRateResponse;
        String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                subscriptionEndpoint + "subscription/getHourlyRate/" + subscriptionId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        jsonString = resp.getEntity(String.class);

        getHourlyRateResponse = new Gson().fromJson(jsonString, GetHourlyRateResponse.class);

        if (getHourlyRateResponse.getIsActive().equals(false)) {
            logger.error("Subscription has already Ended "+subscriptionId
                    +", cannot calculate payout for session "+sessionBillingDetails.getSessionId());
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Subscription has already Ended");
        }

        // Update subscription details
        logger.info("Calling Subscription to complete session");
        Subscription subscriptionResponse = null;
        if (sessionBillingDetails.getModel().equals(SessionModel.TRIAL) || sessionBillingDetails.getModel().equals(SessionModel.IL)
                || (sessionBillingDetails.getModel().equals(SessionModel.OTO) && subscription.getSubModel() != null
                && subscription.getSubModel().equals(SubModel.LOOSE))) {
            subscriptionResponse = subscriptionManager.completeTrialSession(sessionBillingDetails);
        } else {
            subscriptionResponse = subscriptionManager.completeSession(sessionBillingDetails);
        }
        logger.info(subscriptionResponse.toString());

        // Set hourlyRate and data
        SessionPayoutRequest sessionPayoutRequest = new SessionPayoutRequest();
        sessionPayoutRequest.setDate(System.currentTimeMillis());
        sessionPayoutRequest.setSessionDuration(sessionDuration);
        sessionPayoutRequest.setBillingDuration(sessionBillingDetails.getBillingDuration());
        sessionPayoutRequest.setHourlyRate(getHourlyRateResponse.getHourlyRate());
        sessionPayoutRequest.setTeacherId(sessionBillingDetails.getTeacherId());
        sessionPayoutRequest.setSessionId(Long.parseLong(sessionBillingDetails.getSessionId()));
        sessionPayoutRequest.setModel(sessionBillingDetails.getModel());
        sessionPayoutRequest.setCallingUserId(sessionBillingDetails.getTeacherId());
        sessionPayoutRequest.setStudentId(sessionBillingDetails.getStudentId());
        sessionPayoutRequest.setSubscriptionId(sessionBillingDetails.getSubscriptionId());
        sessionPayoutRequest.setTotalHours(getHourlyRateResponse.getTotalHours());
        sessionPayoutRequest.setConsumedHours(getHourlyRateResponse.getConsumedHours());

        // Update Session Payout
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payout/calculateSessionPayout", HttpMethod.POST,
                new Gson().toJson(sessionPayoutRequest));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        jsonString = resp.getEntity(String.class);

        sessionPayoutResponse = new Gson().fromJson(jsonString, SessionPayoutResponse.class);

        // cancelling the trail sessionBillingDetails
        CancelSubscriptionResponse completeTrialResponse = null;
        if (sessionBillingDetails.getModel().equals(SessionModel.TRIAL) || sessionBillingDetails.getModel().equals(SessionModel.IL)
                || (sessionBillingDetails.getModel().equals(SessionModel.OTO) && subscription.getSubModel() != null
                && subscription.getSubModel().equals(SubModel.LOOSE))) {
            completeTrialResponse = subscriptionManager.endTrialSubscription(sessionBillingDetails.getStudentId(),
                    subscriptionId);
            logger.info(" completeTrialResponse " + completeTrialResponse);
        }

        processSessionPayout(sessionPayoutResponse.getSessionId(),
                sessionPayoutResponse.getTeacherId(), sessionPayoutResponse.getDuration(),
                sessionPayoutResponse.getTeacherPayout(),
                sessionPayoutResponse.getTeacherPayout() + sessionPayoutResponse.getCut(), false);

        return sessionPayoutResponse;
    }

    public void processSessionPayout(Long sessionId, Long teacherId, Long billingDuration,
            int teacherCharge, int studentCharge, boolean isUpdateRequest) throws VException {
        ProcessSessionPayoutReq req = new ProcessSessionPayoutReq(sessionId, billingDuration,
                teacherCharge, studentCharge, isUpdateRequest);
        sessionManager.processSessionPayout(req);
        sessionManager.triggerPayoutSNS(sessionId);
        if (!isUpdateRequest) {
            userManager.addSessionDuration(teacherId, billingDuration, 0L);
        }
    }

}
