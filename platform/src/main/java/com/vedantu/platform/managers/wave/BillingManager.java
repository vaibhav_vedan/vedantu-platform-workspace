package com.vedantu.platform.managers.wave;

import com.vedantu.exception.VException;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.dao.wave.SessionBillingData;
import com.vedantu.platform.dao.wave.SessionBillingDataDAO;
import com.vedantu.platform.dao.wave.WiziqBillingDataReq;
import com.vedantu.platform.enums.wave.SessionBillingVersion;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillingManager {

    public static final long UNINITIALIZED = -1L;
    public static final long CUTOFF_PERIOD = 15 * DateTimeUtils.MILLIS_PER_MINUTE;
    public static final long NO_OVERLAP_TEACHER_BILLING_PERIOD = 15 * DateTimeUtils.MILLIS_PER_MINUTE;
    public static final long MINIMUM_TEACHER_ACTIVE_DURATION = 10 * DateTimeUtils.MILLIS_PER_MINUTE;

    @Autowired
    private SessionBillingDataDAO sessionBillingDataDAO;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BillingManager.class);

    public BillingManager() {
        super();
    }

    public Long getBillingDuration(SessionBillingDetails session) throws VException {
        logger.info("Entering " + session);
        // updated billing logic to version 3 on 00:10AM 02/05/2018
        Long totalDuration = 0L;
        Long sessionId = Long.parseLong(session.getSessionId());

        SessionInfo sessionInfo = sessionManager.getSessionById(sessionId, false, false);

        List<SessionBillingData> results = sessionBillingDataDAO.getBySessionId(sessionId);
        if (ArrayUtils.isNotEmpty(results)) {
            Collections.sort(results, (SessionBillingData o1, SessionBillingData o2) -> o1.getCreationTime().compareTo(o2.getCreationTime()));
            boolean overlap = overlapHappened(results, session);
            logger.info("was there overlap in session " + overlap);
            if (overlap) {
                if (sessionInfo.getEndedAt() != null) {
                    session.setSessionEndTime(sessionInfo.getEndedAt());
                } else {
                    session.setSessionEndTime(sessionInfo.getEndTime());
                }
                totalDuration = getTeacherActiveDuration(results, session, false);
                logger.info("calculating teacher Active duration " + totalDuration);
            } else {
                if (sessionInfo.getEndedAt() != null && sessionInfo.getEndedAt() < (sessionInfo.getStartTime() + CUTOFF_PERIOD)) {
                    return totalDuration;
                }
                Long teacherDuration = getTeacherActiveDuration(results, session, true);
                logger.info("calculating teacher Active duration in first 15 minutes" + teacherDuration);
                if (teacherDuration >= MINIMUM_TEACHER_ACTIVE_DURATION) {
                    totalDuration = NO_OVERLAP_TEACHER_BILLING_PERIOD;
                    logger.info("calculating teacher Active duration " + totalDuration);
//                    Long teacherFirstJoined = getTeacherFirstJoinedTime(results, session);
//                    logger.info("getTeacherFirstJoinedTime:" + teacherFirstJoined);
//                    if(teacherFirstJoined != null){
//                        totalDuration = session.getSessionEndTime() - teacherFirstJoined;
//                        logger.info("calculating teacher Active duration " + totalDuration);
//                    }
                }
            }
        }
        return totalDuration;
    }

    private boolean overlapHappened(List<SessionBillingData> results, SessionBillingDetails session) {
        Long sessionStartTime = session.getSessionStartTime();
        Long sessionEndTime = session.getSessionEndTime();
        Long teacherId = session.getTeacherId();
        Long studentId = session.getStudentId();
        HashSet<String> teacherSockets = new HashSet<>();
        HashSet<String> studentSockets = new HashSet<>();
        for (SessionBillingData sessionAppData : results) {
            String type = sessionAppData.getType();
            if (StringUtils.isNotEmpty(type)) {
                Long time = sessionAppData.getCreationTime();
                if (time > sessionEndTime) {
                    time = sessionEndTime;
                } else if (time <= sessionStartTime) {
                    time = sessionStartTime;
                }
                Long userId = sessionAppData.getUserId();
                if (type.equalsIgnoreCase("JOIN")) {
                    if (userId.equals(teacherId)) {
                        teacherSockets.add(sessionAppData.getSocketId());
                    } else if (userId.equals(studentId)) {
                        studentSockets.add(sessionAppData.getSocketId());
                    }
                    if (!teacherSockets.isEmpty() && !studentSockets.isEmpty()) {
                        return true;
                    }
                } else if (type.equalsIgnoreCase("DISCONNECT")) {
                    if (userId.equals(teacherId)) {
                        teacherSockets.remove(sessionAppData.getSocketId());
                    } else if (userId.equals(studentId)) {
                        studentSockets.remove(sessionAppData.getSocketId());
                    }
                }
            }
        }
        return false;
    }

    private Long getTeacherActiveDuration(List<SessionBillingData> results, SessionBillingDetails session, boolean onlyBeforeCutoff) {
        logger.info("Entering getTeacherActiveDuration" + session);
        Long totalDuration = 0L;
        Long startTime = UNINITIALIZED;
        Long sessionStartTime = session.getSessionStartTime();
        Long sessionEndTime = session.getSessionEndTime();
        if (onlyBeforeCutoff) {
            sessionEndTime = sessionStartTime + CUTOFF_PERIOD;
        }
        Long teacherId = session.getTeacherId();
        HashSet<String> teacherSockets = new HashSet<>();
        for (SessionBillingData sessionAppData : results) {
            String type = sessionAppData.getType();
            if (StringUtils.isNotEmpty(type)) {
                Long time = sessionAppData.getCreationTime();
                if (time > sessionEndTime) {
                    time = sessionEndTime;
                } else if (time <= sessionStartTime) {
                    time = sessionStartTime;
                }
                Long userId = sessionAppData.getUserId();
                if (type.equalsIgnoreCase("JOIN")) {
                    if (userId.equals(teacherId)) {
                        teacherSockets.add(sessionAppData.getSocketId());
                    }
                    if (!teacherSockets.isEmpty()) {
                        if (startTime != UNINITIALIZED) {
                            totalDuration += time - startTime;
                        }
                        startTime = time;
                    }
                } else if (type.equalsIgnoreCase("DISCONNECT")) {
                    if (userId.equals(teacherId)) {
                        teacherSockets.remove(sessionAppData.getSocketId());
                    }
                    if ((teacherSockets.isEmpty()) && startTime != UNINITIALIZED
                            && time <= sessionEndTime) {
                        totalDuration += time - startTime;
                        startTime = UNINITIALIZED;
                    }
                }
            }
        }

        if (startTime != UNINITIALIZED && startTime < sessionEndTime) {
            Long currentDuration = sessionEndTime - startTime;
            totalDuration += currentDuration;
        }

        logger.info("Exiting " + totalDuration);
        return totalDuration;
    }

    private Long getTeacherFirstJoinedTime(List<SessionBillingData> results, SessionBillingDetails session) {
        logger.info("Entering getTeacherActiveDuration" + session);
        Long sessionStartTime = session.getSessionStartTime();
        Long sessionEndTime = session.getSessionEndTime();
        Long teacherId = session.getTeacherId();
        for (SessionBillingData sessionAppData : results) {
            String type = sessionAppData.getType();
            if (StringUtils.isNotEmpty(type)) {
                Long time = sessionAppData.getCreationTime();
                if (time > sessionEndTime) {
                    time = sessionEndTime;
                } else if (time <= sessionStartTime) {
                    time = sessionStartTime;
                }
                Long userId = sessionAppData.getUserId();
                if (type.equalsIgnoreCase("JOIN")) {
                    if (userId.equals(teacherId)) {
                        return time;
                    }
                }
            }
        }
        return null;
    }

    private Long getBillingDurationVersion1(List<SessionBillingData> results, SessionBillingDetails session) {
        logger.info("Entering getBillingDurationVersion1" + session);
        Long totalDuration = 0L;
        Long startTime = UNINITIALIZED;
        Long sessionStartTime = session.getSessionStartTime();
        Long sessionEndTime = session.getSessionEndTime();
        if (null != results && !results.isEmpty()) {
            //TODO remove this hack in migration
            Collections.sort(results, new Comparator<SessionBillingData>() {
                @Override
                public int compare(SessionBillingData o1, SessionBillingData o2) {
                    return o1.getCreationTime().compareTo(o2.getCreationTime());
                }
            });
            for (SessionBillingData sessionAppData : results) {
                if (StringUtils.isNotEmpty(sessionAppData.getType())) {
                    //using creationtime instead of 
                    //because there can be difference between node server time
                    //and app engine time

                    Long time = sessionAppData.getCreationTime();
                    logger.info("creationtime is " + time);
                    if (time > sessionEndTime) {
                        time = sessionEndTime;
                    } else if (time <= sessionStartTime) {
                        time = sessionStartTime;
                    }
                    logger.info("corrected time is " + time);
                    logger.info("start time is " + startTime);
                    if (sessionAppData.getType().trim().equalsIgnoreCase("BILLING_STARTED")) {
                        logger.info("billing started");
                        if (startTime != UNINITIALIZED) {
                            logger.info("adding to totalduration " + (time - startTime));
                            totalDuration += time - startTime;
                        }
                        startTime = time;
                        //not using socketid anymore, as the billing messages
                        //are properly handled from node server
                    } else if (sessionAppData.getType().trim().equalsIgnoreCase("BILLING_PAUSED")
                            && startTime != UNINITIALIZED
                            && time <= sessionEndTime) {
                        logger.info("eligigle BILLING_PAUSED");
                        logger.info("adding to totalduration " + (time - startTime));
                        totalDuration += time - startTime;
                        startTime = UNINITIALIZED;
                    }
                }
                logger.info("totalDuration " + totalDuration + ", startTime: " + startTime);
            }
        }

        if (startTime != UNINITIALIZED && startTime < sessionEndTime) {
            logger.info("No further billing paused found, so calculating according to endtime");
            Long currentDuration = sessionEndTime - startTime;
            logger.info("adding to totalduration " + currentDuration);
            totalDuration += currentDuration;
        }

        logger.info("Exiting " + totalDuration);
        return totalDuration;
    }

    private Long getBillingDurationVersion2(List<SessionBillingData> results, SessionBillingDetails session) {
        logger.info("Entering getBillingDurationVersion2" + session);
        Long totalDuration = 0L;
        Long startTime = UNINITIALIZED;
        Long sessionStartTime = session.getSessionStartTime();
        Long sessionEndTime = session.getSessionEndTime();
        Long teacherId = session.getTeacherId();
        Long studentId = session.getStudentId();
        if (null != results && !results.isEmpty()) {
            //TODO remove this hack in migration
            Collections.sort(results, new Comparator<SessionBillingData>() {
                @Override
                public int compare(SessionBillingData o1, SessionBillingData o2) {
                    return o1.getCreationTime().compareTo(o2.getCreationTime());
                }
            });
            HashSet<String> teacherSockets = new HashSet<>();
            HashSet<String> studentSockets = new HashSet<>();
            for (SessionBillingData sessionAppData : results) {
                String type = sessionAppData.getType();
                if (StringUtils.isNotEmpty(type)) {
                    //using creationtime instead of 
                    //because there can be difference between node server time
                    //and app engine time
                    Long time = sessionAppData.getCreationTime();
                    logger.info("creationtime is " + time);
                    if (time > sessionEndTime) {
                        time = sessionEndTime;
                    } else if (time <= sessionStartTime) {
                        time = sessionStartTime;
                    }
                    logger.info("corrected time is " + time);
                    logger.info("start time is " + startTime);
                    Long userId = sessionAppData.getUserId();
                    if (type.equalsIgnoreCase("JOIN")) {
                        if (userId.equals(teacherId)) {
                            teacherSockets.add(sessionAppData.getSocketId());
                        } else if (userId.equals(studentId)) {
                            studentSockets.add(sessionAppData.getSocketId());
                        }
                        if (!teacherSockets.isEmpty() && !studentSockets.isEmpty()) {
                            logger.info("billing started");
                            if (startTime != UNINITIALIZED) {
                                logger.info("adding to totalduration " + (time - startTime));
                                totalDuration += time - startTime;
                            }
                            startTime = time;
                        }
                    } else if (type.equalsIgnoreCase("DISCONNECT")) {
                        if (userId.equals(teacherId)) {
                            teacherSockets.remove(sessionAppData.getSocketId());
                        } else if (userId.equals(studentId)) {
                            studentSockets.remove(sessionAppData.getSocketId());
                        }
                        if ((teacherSockets.isEmpty() || studentSockets.isEmpty()) && startTime != UNINITIALIZED
                                && time <= sessionEndTime) {
                            logger.info("eligigle BILLING_PAUSED");
                            logger.info("adding to totalduration " + (time - startTime));
                            totalDuration += time - startTime;
                            startTime = UNINITIALIZED;
                        }
                    }
                }
                logger.info("totalDuration " + totalDuration + ", startTime: " + startTime);
            }
        }

        if (startTime != UNINITIALIZED && startTime < sessionEndTime) {
            logger.info("No further billing paused found, so calculating according to endtime");
            Long currentDuration = sessionEndTime - startTime;
            logger.info("adding to totalduration " + currentDuration);
            totalDuration += currentDuration;
        }

        logger.info("Exiting " + totalDuration);
        return totalDuration;
    }

    public void insertBillingDataForWiziqSessions(WiziqBillingDataReq req) {
        List<SessionBillingData> billingdatas = new ArrayList<>();

        if (req.getTeacher_entry_time() != null && req.getTeacher_entry_time() > 0) {
            String socketId = UUID.randomUUID().toString();
            SessionBillingData teaEntry = new SessionBillingData(req.getSessionId(),
                    req.getTeacherId(), "JOIN", req.getTeacher_entry_time(), socketId);
            billingdatas.add(teaEntry);
            if (req.getTeacher_exit_time() != null && req.getTeacher_exit_time() > 0) {
                SessionBillingData teaExit = new SessionBillingData(req.getSessionId(),
                        req.getTeacherId(), "DISCONNECT", req.getTeacher_exit_time(), socketId);
                billingdatas.add(teaExit);
            }
        }

        if (req.getStudent_entry_time() != null && req.getStudent_entry_time() > 0) {
            String socketId = UUID.randomUUID().toString();
            SessionBillingData stuEntry = new SessionBillingData(req.getSessionId(),
                    req.getStudentId(), "JOIN", req.getStudent_entry_time(), socketId);
            billingdatas.add(stuEntry);
            if (req.getStudent_exit_time() != null && req.getStudent_exit_time() > 0) {
                SessionBillingData stuExit = new SessionBillingData(req.getSessionId(),
                        req.getStudentId(), "DISCONNECT", req.getStudent_exit_time(), socketId);
                billingdatas.add(stuExit);
            }
        }

        if (!billingdatas.isEmpty()) {
            logger.info(billingdatas);
            sessionBillingDataDAO.insertAllEntities(billingdatas);
        }
    }
}
