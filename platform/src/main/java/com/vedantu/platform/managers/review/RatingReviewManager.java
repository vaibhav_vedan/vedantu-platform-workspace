package com.vedantu.platform.managers.review;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.dao.review.ReviewDao;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.mongodbentities.review.CumilativeRating;
import com.vedantu.platform.mongodbentities.review.Review;
import com.vedantu.platform.utils.EntityTypeUtils;
import com.vedantu.review.pojo.SessionReviewInfo;
import com.vedantu.review.requests.AddPrioritiesRequest;
import com.vedantu.review.requests.AddReviewRequest;
import com.vedantu.review.requests.EditReviewRequest;
import com.vedantu.review.requests.GetBestReviewsForSuggestionRequest;
import com.vedantu.review.requests.GetReviewRequest;
import com.vedantu.review.requests.GetReviewsRequest;
import com.vedantu.review.response.AddReviewResponse;
import com.vedantu.review.response.EditReviewResponse;
import com.vedantu.review.response.GetReviewsResponse;
import com.vedantu.review.response.ReviewInfo;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.interfaces.IEntity;

@Service
public class RatingReviewManager {

    @Autowired
    LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(RatingReviewManager.class);

    @Autowired
    private EntityTypeUtils entityTypeUtils;

    @Autowired
    private ReviewDao reviewDAO;

    @Autowired
    private CumilativeRatingDao cumilativeRatingDao;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    private static final int BASE_RATING_WEITAGE = 10;

    public AddReviewResponse addReview(AddReviewRequest req)
            throws VException, Exception {
        return addReview(req, false);
    }

    private AddReviewResponse addReview(AddReviewRequest req,
            boolean addBaseRatingReq) throws Exception, VException {

        logger.info("addReview" + req.toString());
        Long callingUserId = req.getCallingUserId();
        logger.info("sending sendReviewEmail  from calling user Id : {0}"
                + callingUserId);

        AddReviewResponse res = null;
        IEntity entity = entityTypeUtils.getEntity(
                req.getEntityId().toString(), req.getEntityType());
        if (entity == null) {
            throw new NotFoundException(ErrorCode.ENTITY_NOT_FOUND,
                    "no entity found for [type:" + req.getEntityType()
                    + ", id:" + req.getEntityId() + "]");
        }

        // TODO: add check if the student has taken any session with this
        // teacher (target entity)
        // PersistenceManager mgr =
        // VPersistenceManagerFactory.getPersistenceManager();
        // Transaction tx = mgr.currentTransaction();
        // tx.begin();
        Review review = null;
        try {
            String entityId = null;
            String contextId = null;
            if (req.getEntityId() != null) {
                entityId = req.getEntityId().toString();
            }
            if (req.getContextId() != null) {
                contextId = req.getContextId().toString();
            }

            List<Review> reviewList = reviewDAO.get(null, req
                    .getCallingUserId().toString(), entityId, req
                    .getEntityType(), null, null, null,
                    VisibilityState.VISIBLE, req.getContextType(), contextId,
                    null, null, null, null, null, null, null, null);

            if (!reviewList.isEmpty()) {
                review = reviewList.get(0);
            }

            List<CumilativeRating> cumilativeRatingList = cumilativeRatingDao
                    .get(null, entityId, req.getEntityType(), null, null, null,
                            null, null, null, null, null);

            CumilativeRating cumilativeRating = null;

            if (!cumilativeRatingList.isEmpty()) {
                cumilativeRating = cumilativeRatingList.get(0);
            }

            if (cumilativeRating == null) {
                cumilativeRating = new CumilativeRating(entityId,
                        req.getEntityType());
            }

            if (addBaseRatingReq && cumilativeRating.getBaseRatingAssigned()) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                        "Base rating has already been assigned to entity [id:"
                        + req.getEntityId() + ", type="
                        + req.getEntityType() + "]");
            }

            if (review != null) {
                // user has already reviewed, hence now update the review and
                // rating
                cumilativeRating.decRating(review.getRating());
                cumilativeRating.decTotalCount(1);
                review.setRating(req.getRating().intValue());
                boolean isReviewed = StringUtils.isNotEmpty(req.getReview());
                review.setReview(StringUtils.defaultIfEmpty(req.getReview()));
                review.setReason(req.getReason());
                review.setIsReviewed(isReviewed);
                review.setPriority(0);
            } else {
                boolean isReviewed = StringUtils.isNotEmpty(req.getReview());

                review = new Review(req.getCallingUserId().toString(),
                        entityId, req.getEntityType(), req.getRating()
                        .intValue(), req.getReview(),
                        req.getContextType(), contextId, isReviewed,
                        req.getReason(), 0);
            }

            // Check first session
            boolean updateCumulativeRating = true;
            boolean poorFeedback = req.isPoorFeedback();
            if (poorFeedback) {
                //TODO: session data

//				ISessionDAO sessionDAO = DAOFactory.INSTANCE.getSessionDAO();
//				List<SessionState> states = new ArrayList<SessionState>();
//				states.add(SessionState.ACTIVE);
//				states.add(SessionState.ENDED);
//
//				List<com.vedantu.fos.entity.Session> sessions = sessionDAO
//						.getSessionsByStates(req.getSessionUserId(), states,
//								0l, 2l, mgr);
//				if (sessions != null && sessions.size() == 1) {
//					updateCumulativeRating = false;
//				}
            }

            if (addBaseRatingReq) {
                cumilativeRating.setBaseRatingAssigned(true);
                cumilativeRating
                        .incRating((int) (req.getRating() * BASE_RATING_WEITAGE));
                cumilativeRating.incTotalCount(1 * BASE_RATING_WEITAGE);
            } else if (updateCumulativeRating) {
                cumilativeRating.incRating(review.getRating());
                cumilativeRating.incTotalCount(1);
            }

            logger.info("review : {0}", review);
            cumilativeRating.calculateAvgRating();
            review.setLastUpdated(System.currentTimeMillis());
            //review.setLastUpdatedBy(req.getCallingUserId().toString());
            reviewDAO.upsert(review, req.getCallingUserId()!=null? req.getCallingUserId().toString(): null);
            logger.info("cumilativeRating : {0}", cumilativeRating);
            cumilativeRating.setLastUpdated(System.currentTimeMillis());
            //cumilativeRating.setLastUpdatedBy(req.getCallingUserId().toString());
            cumilativeRatingDao.upsert(cumilativeRating, req.getCallingUserId()!=null? req.getCallingUserId().toString(): null);
            // //ES Rating Indexing/////
            Map<String, Object> payload = new HashMap<>();
            payload.put("rating", cumilativeRating);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_UPDATE_RATING, payload);
            asyncTaskFactory.executeTask(params);

            res = new AddReviewResponse(review.getId(),
                    cumilativeRating.getAvgRating(),
                    cumilativeRating.getTotalRatingCount(), review.getRating(),
                    review.getReview(), review.getReason());
        } finally {
        }

        if (!addBaseRatingReq) {
            // send rating sms & email
            List<Long> userIds = new ArrayList<>();
            userIds.add(req.getEntityId());
            userIds.add(callingUserId);
            Map<Long, User> usersMap = fosUtils.getUserInfosMap(userIds, false);
            User toUser = null;
            User fromUser = null;
            if (usersMap.containsKey(callingUserId)) {
                fromUser = usersMap.get(callingUserId);
            }
            if (usersMap.containsKey(req.getEntityId())) {
                toUser = usersMap.get(req.getEntityId());
            }
            //emailManager.sendReviewEmail(toUser, fromUser, review);
            if(!ContextType.SESSION.equals(req.getContextType())){
                Map<String, Object> payload = new HashMap<>();
                payload.put("toUser", toUser);
                payload.put("fromUser", fromUser);
                payload.put("review", review);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REVIEW_EMAIL, payload);
                asyncTaskFactory.executeTask(params);
            }
        }
        if (ContextType.SESSION.equals(req.getContextType())
                && req.getContextId() != null && review != null) {
            Map<String, Object> fillReviewInfoPayload = new HashMap<>();
            fillReviewInfoPayload.put("reviewId", review.getId());
            fillReviewInfoPayload.put("sessionId", req.getContextId());
            AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.FILL_SESSION_REVIEW_INFO,
                    fillReviewInfoPayload, 3000l);
            asyncTaskFactory.executeTask(params2);
        }
        logger.info("addReview", res);
        return res;
    }

    public EditReviewResponse editReview(EditReviewRequest req)
            throws VException {

        logger.info("editReview : " + req.toString());

        EditReviewResponse res = null;
        Review review = reviewDAO.getById(req.getId());
        if (review == null) {
            throw new NotFoundException(ErrorCode.REVIEW_NOT_FOUND,
                    "no entity found for id[" + req.getId() + "]");
        }
        review.setState(req.getState());
        review.setLastUpdated(System.currentTimeMillis());
        //review.setLastUpdatedBy(req.getCallingUserId().toString());
        reviewDAO.upsert(review, req.getCallingUserId()!=null? req.getCallingUserId().toString(): null);
        res = new EditReviewResponse(review.getState());
        logger.info("editReview" + res.toString());
        return res;
    }

    public AddReviewResponse assignBaseRating(AddReviewRequest req)
            throws Exception {

        //TODO: Add authorization
        //userManager.adminAccessOnlyCheck(req.getCallingUserId());
        AddReviewResponse res = addReview(req, true);
        logger.info("assignBaseRating" + res.toString());
        return res;
    }

    public ReviewInfo getReview(GetReviewRequest req) throws VException {
        logger.info("getReview" + req.toString());
        String id = req.getId();
        Long userId = req.getCallingUserId();
        if (userId == null || userId <= 0) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND,
                    "User not found");
        }

        Review review = null;
        if (id != null && !id.isEmpty()) {
            review = reviewDAO.getById(id);
        } else {
            List<Review> reviewList = reviewDAO.get(null,
                    (req.getUserId() != null) ? req.getUserId().toString() : null, req.getEntityId(), req.getEntityType(),
                    null, null, null, null, req.getContextType(), req
                    .getContextId(), null, null, null, null, null,
                    null, true, null);
            if (ArrayUtils.isNotEmpty(reviewList)) {
                review = reviewList.get(0);
            }
        }

        if (review == null) {
            throw new NotFoundException(ErrorCode.REVIEW_NOT_FOUND, "Review not found for request " + req);
        }

        User user = fosUtils.getUserInfo(Long.parseLong(review.getUserId()), false);
        com.vedantu.review.pojo.Review review2 = review.getReviewPojo();
        ReviewInfo reviewInfo = new ReviewInfo(review2, user);
        logger.info("getReview" + reviewInfo);
        return reviewInfo;
    }

    public GetReviewsResponse getReviews(GetReviewsRequest req)
            throws VException {
        logger.info("getReviews " + req.toString());
        GetReviewsResponse res = new GetReviewsResponse();
        List<Review> reviews = reviewDAO.get(null, (req.getUserId() != null) ? req.getUserId().toString() : null,
                req.getEntityId(), req.getEntityType(), null, null, null, req
                .getState(), null, null, null, null, null, req
                .getStart(), req.getSize(), req
                .getExcludeUserId(), req.getSkipReviewCheck(), req
                .getSortByPriority());

        if (reviews != null && !reviews.isEmpty()) {
            Set<Long> userIds = new HashSet<>();
            for (Review review : reviews) {
                if (review.getUserId() != null) {
                    userIds.add(Long.parseLong(review.getUserId()));
                }
            }

            Map<Long, User> usersMap = fosUtils.getUserInfosMap(userIds, false);

            for (Review review : reviews) {
                if (review.getUserId() != null && usersMap.containsKey(Long.parseLong(review.getUserId()))) {
                    com.vedantu.review.pojo.Review review2 = review.getReviewPojo();
                    res.addItem(new ReviewInfo(review2, usersMap.get(Long.parseLong(review
                            .getUserId()))));
                }
            }
        }
        if (req.getCallingUserId() != null) {
            List<Review> userReviews = reviewDAO.get(null,
                    (req.getUserId() != null) ? req.getUserId().toString() : null, req.getEntityId(), req.getEntityType(), null,
                    null, null, null, req.getContextType(), req.getContextId(),
                    null, null, null, null, null, null, null, null);
            if (userReviews != null && !userReviews.isEmpty()) {
                com.vedantu.review.pojo.Review review2 = userReviews.get(0).getReviewPojo();
                res.setUserReview(new ReviewInfo(review2));
            }
        }
        logger.info("getReviews" + res.toString());
        return res;
    }

    public boolean addPriorities(AddPrioritiesRequest req) throws VException {
        logger.info("addPriorities" + req.toString());
        //TODO: Add authorization
        //userManager.adminAccessOnlyCheck(req.getCallingUserId());

        List<AddPrioritiesRequest.AddPriorityReq> prioritiesReq = req
                .getAddPriorityReqs();

        Map<String, Integer> priorityMap = new HashMap<>();
        Set<String> reviewIds = new HashSet<>();
        for (AddPrioritiesRequest.AddPriorityReq addPriorityReq : prioritiesReq) {
            reviewIds.add(addPriorityReq.getReviewId());
            priorityMap.put(addPriorityReq.getReviewId(),
                    addPriorityReq.getPriority());
        }
        List<String> ids = new ArrayList<>();
        for (String reviewId : reviewIds) {
            ids.add(reviewId);
        }

        List<Review> reviews = reviewDAO.get(ids, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null,
                null, null);
        for (Review review : reviews) {
            if (review != null && priorityMap.containsKey(review.getId())) {
                review.setPriority(priorityMap.get(review.getId()));
            }
        }

        reviewDAO.updateAll(reviews);

        logger.info("Priorities added");
        return true;
    }

    public List<ReviewInfo> getBestReviewsForSuggestion(
            GetBestReviewsForSuggestionRequest getBestReviewsForSuggestionRequest)
            throws VException {
            List<Review> reviews = reviewDAO.get(null, null,
                    (getBestReviewsForSuggestionRequest.getUserId() != null) ? getBestReviewsForSuggestionRequest.getUserId().toString() : null,
                    EntityType.USER, null, null, null, null, null, null, null,
                    null, null, 0, 2000, null, true, null);
            List<Review> reviewList = new ArrayList<>();
            Set<Long> userIds = new HashSet<>();
            List<ReviewInfo> reviewInfos = new ArrayList<>();
            // List<ReviewInfo> reviewInfosQualified = new
            // ArrayList<ReviewInfo>();
            int y = 0;
            for (Review review : reviews) {
                String reviewString = review.getReview();
                if (reviewString != null
                        && reviewString.length() > 0
                        && reviewString.length() > getBestReviewsForSuggestionRequest
                        .getLengthPreferred()
                        && y < getBestReviewsForSuggestionRequest
                        .getNoOfReviews()) {
                    reviewList.add(review);
                    userIds.add(Long.parseLong(review.getUserId()));
                    y++;
                }
                // else if (reviewString != null && reviewString.length() > 0) {
                // reviewInfosQualified.add(new ReviewInfo(review, mgr));
                // }
            }

            // getSessionsUsersInfoReq.setUserIds(userIds);
            Map<Long, User> usersMap = fosUtils.getUserInfosMap(userIds, false);

            for (Review review : reviewList) {
                if (review.getUserId() != null) {
                    User user = usersMap.get(Long.parseLong(review.getUserId()));

                    com.vedantu.review.pojo.Review reviewTemp = review.getReviewPojo();
                    reviewInfos.add(new ReviewInfo(reviewTemp, user));
                }
            }

            // if (!reviews.isEmpty() && !reviewInfosQualified.isEmpty()
            // && reviewInfos.size() < reqPojo.getNoOfReviews()) {
            // Integer size = reviewInfos.size();
            // for (int i = 0; i < reqPojo.getNoOfReviews() - size; i++) {
            // if (i < reviewInfosQualified.size()) {
            // reviewInfos.add(reviewInfosQualified.get(i));
            // }
            // }
            // }
            return reviewInfos;
    }

    public void fillSessionReviewInfo(String reviewId, Long sessionId) throws VException {
        SessionInfo sessionInfo = sessionManager.getSessionById(sessionId, false, false);
        if (sessionInfo != null) {
            SessionReviewInfo reviewInfo = new SessionReviewInfo();
            reviewInfo.setSubject(sessionInfo.getSubject());
            if (sessionInfo.getSubscriptionId() != null) {
                SubscriptionResponse resPojo = subscriptionManager.getSubscriptionBasicInfo(sessionInfo.getSubscriptionId());
                if (resPojo != null) {
                    reviewInfo.setSubject(resPojo.getSubject());
                    if (resPojo.getGrade() != null) {
                        reviewInfo.setGrade(resPojo.getGrade().toString());
                    }
                    reviewInfo.setTarget(resPojo.getTarget());
                }
            }
            Review review = reviewDAO.getById(reviewId);
            if (review != null) {
                review.setReviewExtraInfo(reviewInfo);
                reviewDAO.upsert(review);
            }
        }
    }


    public Review getReviewBasicInfo(GetReviewRequest req) throws VException {
        logger.info("getReview" + req.toString());
        String id = req.getId();

        Review review = null;
        if (id != null && !id.isEmpty()) {
            review = reviewDAO.getById(id);
        } else {
            List<Review> reviewList = reviewDAO.get(null,
                    (req.getUserId() != null) ? req.getUserId().toString() : null, req.getEntityId(), req.getEntityType(),
                    null, null, null, null, req.getContextType(), req
                            .getContextId(), null, null, null, null, null,
                    null, null, null);
            if (ArrayUtils.isNotEmpty(reviewList)) {
                review = reviewList.get(0);
            }
        }
        return review;
    }

}
