/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.sessionmetrics;

import com.vedantu.platform.dao.SessionAnalysisDAO;
import com.vedantu.platform.enums.sessionmetrics.SessionAnalysisAggregatorAction;
import com.vedantu.platform.managers.aws.AwslambdaManager;
import com.vedantu.platform.pojo.sessionmetrics.SessionAnalysisAggregateResult;
import com.vedantu.platform.response.sessionmetrics.SessionAnalysisAggregateResponse;
import com.vedantu.platform.response.sessionmetrics.SessionAnalysisDataResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class SessionMetricsManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionMetricsManager.class);

    @Autowired
    private SessionAnalysisDAO sessionAnalysisDAO;

    @Autowired
    private AwslambdaManager awslambdaManager;

    public SessionAnalysisDataResponse getBySessionId(String sessionId) {
        SessionAnalysisDataResponse response = new SessionAnalysisDataResponse(sessionAnalysisDAO.getBySessionId(sessionId));
        return response;
    }

    public SessionAnalysisDataResponse getByUserId(String userId,String email,Long fromTime,Long tillTime,String order) {
        SessionAnalysisDataResponse response = new SessionAnalysisDataResponse();
        if(StringUtils.isEmpty(userId)&&StringUtils.isEmpty(email) ){
                response.setSessionData(new ArrayList());
                return response;
        }
        if(!StringUtils.isEmpty(userId)){
            response.setSessionData(sessionAnalysisDAO.getByUserId(userId,fromTime,tillTime,order));
        }else{
            response.setSessionData(sessionAnalysisDAO.getByUserEmail(email,fromTime,tillTime,order));
        }
        return response;
    }

    public PlatformBasicResponse analyseSession(String sessionId) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        awslambdaManager.invokeSessionMetricsLambda(sessionId);
        res.setSuccess(true);
        return res;
    }

    public SessionAnalysisAggregateResponse runqueries(Long startTime, Long endTime, List<SessionAnalysisAggregatorAction> reqList) {
        SessionAnalysisAggregateResponse response = new SessionAnalysisAggregateResponse();
        
        long diff = (endTime - startTime);
        int minSessionsCountReq = 0;
        if (diff <= 86400000) {
            minSessionsCountReq = 0;
        } else if (diff > 8640000 && diff <= 604800000) {
            minSessionsCountReq = 5;
        }
        if(reqList==null){
            reqList=Arrays.asList(SessionAnalysisAggregatorAction.values());
        }
        for (SessionAnalysisAggregatorAction action : reqList) {
            SessionAnalysisAggregateResult result = getSessionAnalysisAggregateResult(startTime,endTime, action, minSessionsCountReq);
            if (result != null) {
                response.addResult(result);
            }
        }
        return response;
    }

    private SessionAnalysisAggregateResult getSessionAnalysisAggregateResult(Long startTime, Long endTime, SessionAnalysisAggregatorAction action, int minSession) {
        logger.info("getSessionAnalysisAggregateResult  startTime:" + startTime + "  endTime:" + endTime + "  action:" + action);
        SessionAnalysisAggregateResult result = new SessionAnalysisAggregateResult(action);
        Aggregation session;
        Aggregation user;
        Criteria criteria = Criteria.where("startTime").gte(startTime).lte(endTime).and("analysable").is(true);
        Criteria callCountCriteria = Criteria.where("startTime").gte(startTime).lte(endTime).and("analysable").is(true).and("callsCount").gt(0);
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        switch (action) {
            case SOCKET_DISCONNECTIONS:
                session = newAggregation(match(criteria),
                        group("sessionId").sum("disconnectionsNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("disconnectionsNormalized").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                break;
            case AUDIO_DISCONNECTIONS:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("audioDisconnectedTimesNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("audioDisconnectedTimesNormalized").as("avg")
                                .count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case SOCKET_DISCONNECTION_TIME:
                session = newAggregation(match(criteria),
                        group("sessionId").avg("totalSessionInactiveTimeNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("offlineTimeNormalized").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case AUDIO_DISCONNECTION_TIME:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("audioDisconnectionTimeNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("audioDisconnectionTimeNormalized").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case POOR_AUDIO_QUALITY_TIME:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("poorAudioQualityTimeNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("poorAudioQualityTimeNormalized").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case CALL_COUNT:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("callsCountNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("callsCountNormalized").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case CALL_CONNECTION_TIME:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("avgCallConnectionTime").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("avgCallConnectionTime").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case JOIN_DELAY:
                session = newAggregation(match(criteria),
                        group("sessionId").sum("joinDelay").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("joinDelay").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case HIGH_PACKET_LOSS_TIME:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("highPacketLossTimeNormalized").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("highPacketLossTimeNormalized").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case ICE_FAILURES:
                session = newAggregation(match(callCountCriteria),
                        group("sessionId").avg("sessionErrors.ICE_CONNECTION_STATE_FAILED").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("sessionErrors.ICE_CONNECTION_STATE_FAILED").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case PAGE_REFRESHES:
                session = newAggregation(match(criteria),
                        group("sessionId").sum("disconnectionMap.ON_PAGE_LEAVE").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("disconnectionMap.ON_PAGE_LEAVE").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            case AUDIO_CALL_TRIES:
                session = newAggregation(match(criteria),
                        group("sessionId").sum("audioCallTries").as("perSession"),
                        group().avg("perSession").as("avg").count().as("sessionsCount")).withOptions(aggregationOptions);
                user = newAggregation(match(criteria),
                        group("email", "role").avg("audioCallTries").as("avg").count().as("sessionsCount"), match(Criteria.where("sessionsCount").gte(minSession)), sort(Sort.Direction.DESC, "avg"), limit(10)).withOptions(aggregationOptions);
                
                break;
            default:
                return null;
        }
        if (session != null) {
            AggregationResults<Map> groupResults = sessionAnalysisDAO.getMongoOperations().aggregate(session,
                    SessionAnalysisDAO.COLLECTION_NAME, Map.class);
            List<Map> resultSession = groupResults.getMappedResults();
            if(!resultSession.isEmpty()){
                result.setAvgResp(resultSession.get(0));
            }
        }
        if (user != null) {
            AggregationResults<Map> groupResultsUser = sessionAnalysisDAO.getMongoOperations().aggregate(user,
                    SessionAnalysisDAO.COLLECTION_NAME, Map.class);
            List<Map> resultUser = groupResultsUser.getMappedResults();
            result.setPerUserAvgResp(resultUser);
        }
        return result;
    }
}
