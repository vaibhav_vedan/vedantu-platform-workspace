/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.onetofew;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.BuyItemsReq;
import com.vedantu.dinero.request.GetOrdersByDeliverableEntityReq;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.request.CheckOrJoinBatchAfterRegReq;
import com.vedantu.onetofew.request.CreateTrialEnrollmentReq;
import com.vedantu.onetofew.request.EndOTFEnrollmentReq;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.onetofew.request.MarkRegistrationStatusReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.pojo.leadsquared.LeadDetailsSerialized;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.request.RegPaymentOTFCourseReq;
import com.vedantu.subscription.response.CheckIfRegFeePaidRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;

/**
 *
 * @author ajith
 */
@Service
public class EnrollmentManager {

    @Autowired
    private OTFManager otfManager;

    // one to few end point is configured to contain / at the end
    private final String ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    @Autowired
    PaymentManager paymentManager;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private FosUtils fosUtils;

    private final Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EnrollmentManager.class);

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    @Autowired
    private DozerBeanMapper mapper;

    public CheckIfRegFeePaidRes checkIfRegFeePaidForCourse(CheckOrJoinBatchAfterRegReq req)
            throws VException {
        req.verify();
        logger.info("checking the course has reg fee and if the payment is paid");
        CourseInfo courseInfo = otfManager.getCourse(req.getCourseId());
        BatchBasicInfo batchInfo = getBatch(req.getBatchId());
        CheckIfRegFeePaidRes res = new CheckIfRegFeePaidRes();
        String orderId = null;

        ClientResponse resp = WebUtils.INSTANCE.doCall(
                DINERO_ENDPOINT + "/payment/getNonRefundedOrdersWithoutDeliverableEntityId?entityId="
                + req.getCourseId() + "&userId=" + req.getUserId(),
                HttpMethod.GET, null);
        String listResp = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(listResp, listType);
        if (ArrayUtils.isEmpty(orders)) {
            if (batchInfo != null) {
                if (batchInfo.getRegistrationFee() == null) {

                } else if (batchInfo.getRegistrationFee() >= 0) {
                    throw new ForbiddenException(ErrorCode.NOT_REGISTERED_FOR_COURSE_OR_REG_FEE_REFUNDED, "Batch Registration Fee not paid");
                }

            } else {
                if (courseInfo.getRegistrationFee() != null && courseInfo.getRegistrationFee() >= 0) {
                    throw new ForbiddenException(ErrorCode.NOT_REGISTERED_FOR_COURSE_OR_REG_FEE_REFUNDED, "Batch Registration Fee not paid");
                }
            }

        } else {
            orderId = orders.get(0).getId();
        }

        res.setOrderId(orderId);
        res.setRegFeePaid(true);
        return res;
    }

    public PlatformBasicResponse createTrialEnrollment(CreateTrialEnrollmentReq manualEnrollmentReq)
            throws BadRequestException, VException {
        manualEnrollmentReq.verify();

        BatchBasicInfo batch = getBatch( manualEnrollmentReq.getBatchId() );
        if (batch == null) {
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND, "Batch not found");
        }

        if(batch.getLastPurchaseDate() != null && batch.getLastPurchaseDate() < System.currentTimeMillis()) {
            throw new BadRequestException( ErrorCode.LAST_PURCHASE_DATE_IS_OVER,
                    "LastPurchaseDate is over for this Batch " + batch.getId() );
        }

        logger.info("checking the course has reg fee and if the payment is paid");
        CheckOrJoinBatchAfterRegReq req = mapper.map(manualEnrollmentReq, CheckOrJoinBatchAfterRegReq.class);
        CheckIfRegFeePaidRes checkIfRegFeePaidRes = checkIfRegFeePaidForCourse(req);

        logger.info("Enrolling the student " + manualEnrollmentReq.getUserId() + " in batch "
                + manualEnrollmentReq.getBatchId());

        OTFEnrollmentReq oTFEnrollmentReq = new OTFEnrollmentReq(manualEnrollmentReq.getUserId().toString(),
                manualEnrollmentReq.getBatchId(), manualEnrollmentReq.getCourseId(), null,
                Role.STUDENT, "ACTIVE", null, EnrollmentState.TRIAL);
        EnrollmentPojo enrollmentInfo = enrollment(oTFEnrollmentReq);
        logger.info("Enrolled the student " + enrollmentInfo);

        if (checkIfRegFeePaidRes.getOrderId() != null) {
            paymentManager.updateDeliverableEntityIdInOrder(checkIfRegFeePaidRes.getOrderId(), enrollmentInfo.getEnrollmentId(),
                    DeliverableEntityType.OTF_BATCH_ENROLLMENT);
        }

        MarkRegistrationStatusReq markRegistrationStatusReq = new MarkRegistrationStatusReq();
        markRegistrationStatusReq.setEntityId(manualEnrollmentReq.getCourseId());
        markRegistrationStatusReq.setEntityType(EntityType.OTF_COURSE_REGISTRATION);
        markRegistrationStatusReq.setUserId(manualEnrollmentReq.getUserId());
        markRegistrationStatusReq.setNewstatus("ENROLLED");
        markRegistrationStatusReq.setOldStatuses(Arrays.asList("REGISTERED"));
        markRegistrationStatusReq.setDeliverableEntityIds(Arrays.asList(enrollmentInfo.getId()));
        ClientResponse resp1 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/registration/markRegistrationStatus",
                HttpMethod.POST, new Gson().toJson(markRegistrationStatusReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp1);

        RegPaymentOTFCourseReq regPaymentOTFCourseReq = new RegPaymentOTFCourseReq();
        regPaymentOTFCourseReq.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
        regPaymentOTFCourseReq.setBatchId(manualEnrollmentReq.getBatchId());
        regPaymentOTFCourseReq.setCourseId(manualEnrollmentReq.getCourseId());
        regPaymentOTFCourseReq.setEnrollmentId(enrollmentInfo.getId());
        regPaymentOTFCourseReq.setOrderId(checkIfRegFeePaidRes.getOrderId());
        ClientResponse clientResponse = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/enroll/createEnrollmentConsumption", HttpMethod.POST,
                new Gson().toJson(regPaymentOTFCourseReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(clientResponse);
        String jsonString = clientResponse.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);

        if (!platformBasicResponse.isSuccess()) {
            logger.error("EnrollmentConsumption creation failed for: " + manualEnrollmentReq);
        }

        logger.info("sending mail to the student");
        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentInfo", enrollmentInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_MANUAL_ENROLLMENT_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        return new PlatformBasicResponse();
    }

    public EnrollmentPojo enrollment(OTFEnrollmentReq enrollmentReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "enroll", HttpMethod.POST,
                new Gson().toJson(enrollmentReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        EnrollmentPojo enrollmentInfo = gson.fromJson(jsonString, EnrollmentPojo.class);

        // Update calendar
        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentInfo", enrollmentInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_UPDATE_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);

        if (enrollmentInfo.isCreatedNew()) {
            logger.info("sharing content through curriculum");
            Map<String, Object> payload4 = new HashMap<>();
            ShareContentEnrollmentReq request = new ShareContentEnrollmentReq();
            request.setContextId(enrollmentInfo.getBatchId());
            request.setStudentId(enrollmentInfo.getUserId());
            payload4.put("shareContentCurriculumReq", request);
            AsyncTaskParams params4 = new AsyncTaskParams(AsyncTaskName.POST_ENROLLMENT_CONTENT_SHARE_CURRICULUM, payload4);
            asyncTaskFactory.executeTask(params4);
        }

        return enrollmentInfo;
    }

    public EnrollmentPojo markStatus(OTFEnrollmentReq enrollmentReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "enroll/markStatus", HttpMethod.POST,
                new Gson().toJson(enrollmentReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        EnrollmentPojo enrollmentInfo = gson.fromJson(jsonString, EnrollmentPojo.class);

        // Update calendar
        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentInfo", enrollmentInfo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_UPDATE_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);

        if (EntityStatus.INACTIVE.equals(enrollmentInfo.getStatus())) {
            logger.info("marking the order forfeited if the payment is done by instalment and not fully paid");
            try {
                String markOrderForfeited = DINERO_ENDPOINT + "/payment/markOrderForfeited";
                JSONObject markOrderForfeitedReq = new JSONObject();
                if (EntityType.OTM_BUNDLE.equals(enrollmentInfo.getEntityType())) {
                    markOrderForfeitedReq.put("entityType", EntityType.OTM_BUNDLE);
                    markOrderForfeitedReq.put("entityId", enrollmentInfo.getEntityId());
                } else {
                    markOrderForfeitedReq.put("entityType", EntityType.OTF);
                    markOrderForfeitedReq.put("entityId", enrollmentReq.getBatchId());
                }
                markOrderForfeitedReq.put("userId", enrollmentReq.getUserId());

                ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                        markOrderForfeitedReq.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
                String jsonString1 = markOrderForfeitedRes.getEntity(String.class);
                logger.info("Response for markOrderForfeited  : " + jsonString1);
            } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                logger.error("Error in markOrderForfeited for batchId  "
                        + enrollmentReq.getBatchId() + ", userId " + enrollmentReq.getUserId(), ex);
            }
        } else if (EntityStatus.ACTIVE.equals(enrollmentInfo.getStatus())) {
            logger.info("marking the order partially_paid from forfeited");
            try {
                String resetOrderInstallmentState = DINERO_ENDPOINT + "/payment/resetOrderInstallmentState";
                JSONObject resetOrderInstallmentStateReq = new JSONObject();
                if (EntityType.OTM_BUNDLE.equals(enrollmentInfo.getEntityType())) {
                    resetOrderInstallmentStateReq.put("entityType", EntityType.OTM_BUNDLE);
                    resetOrderInstallmentStateReq.put("entityId", enrollmentInfo.getEntityId());
                } else {
                    resetOrderInstallmentStateReq.put("entityType", EntityType.OTF);
                    resetOrderInstallmentStateReq.put("entityId", enrollmentReq.getBatchId());
                }
                resetOrderInstallmentStateReq.put("userId", enrollmentReq.getUserId());
                resetOrderInstallmentStateReq.put("deliverableEntityId", enrollmentInfo.getId());

                ClientResponse resetOrderInstallmentStateRes = WebUtils.INSTANCE.doCall(resetOrderInstallmentState, HttpMethod.POST,
                        resetOrderInstallmentStateReq.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(resetOrderInstallmentStateRes);
                String jsonString1 = resetOrderInstallmentStateRes.getEntity(String.class);
                logger.info("Response for resetOrderInstallmentState  : " + jsonString1);
            } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                logger.error("Error in resetOrderInstallmentState for batchId  "
                        + enrollmentReq.getBatchId() + ", userId " + enrollmentReq.getUserId(), ex);
            }
            try {
                ClientResponse resp1 = WebUtils.INSTANCE.doCall(ontofewEndpoint + "enroll/getChangeTimeForContentShare?id="
                        + enrollmentInfo.getId(), HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
                String jsonString1 = resp1.getEntity(String.class);
                logger.info(jsonString1);

                EnrollmentPojo enrollmentPojo = gson.fromJson(jsonString1, EnrollmentPojo.class);
                logger.info("sharing content through curriculum");
                Map<String, Object> payload3 = new HashMap<>();
                ShareContentEnrollmentReq request = new ShareContentEnrollmentReq();
                request.setContextId(enrollmentPojo.getBatchId());
                request.setStudentId(enrollmentPojo.getUserId());
                request.setStartTime(enrollmentPojo.getChangeTime());
                payload3.put("shareContentCurriculumReq", request);
                AsyncTaskParams params3 = new AsyncTaskParams(AsyncTaskName.POST_ENROLLMENT_CONTENT_SHARE_CURRICULUM, payload3);
                asyncTaskFactory.executeTask(params3);

            } catch (Exception ex) {
                logger.info("share content exception for userId " + enrollmentInfo.getUserId() + " and enrollmet " + enrollmentInfo.getId());
            }
        }

        return enrollmentInfo;
    }

    public OrderInfo enrollToRegularState(BuyItemsReq buyItemsReq) throws VException, IOException, AddressException {
        buyItemsReq.verify();

        String batchId = buyItemsReq.getItems().get(0).getEntityId();
        String courseUrl = ontofewEndpoint + "/batch/basicInfo/" + batchId;
        ClientResponse courseResp = WebUtils.INSTANCE.doCall(courseUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
        String jsonString = courseResp.getEntity(String.class);
        BatchBasicInfo batchBasicInfo = new Gson().fromJson(jsonString, BatchBasicInfo.class);
        boolean hasRegFee = false;
        if (batchBasicInfo.getRegistrationFee() != null
                && batchBasicInfo.getRegistrationFee() >= 0) {
            hasRegFee = true;
        }

        try {
            String url = ontofewEndpoint + "/enroll/getEnrollmentForBatch?batchId=" + batchId + "&userId=" + buyItemsReq.getUserId();
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonResp = resp.getEntity(String.class);
            EnrollmentPojo enrollmentPojo = new Gson().fromJson(jsonResp, EnrollmentPojo.class);
            if (EnrollmentState.REGULAR.equals(enrollmentPojo.getState())) {
                throw new BadRequestException(ErrorCode.ALREADY_ENROLLED, "Already enrolled in the batch");
            } else if (hasRegFee && !EnrollmentState.TRIAL.equals(enrollmentPojo.getState())) {
                throw new ConflictException(ErrorCode.BATCH_ENROLLMENT_NOT_IN_TRIAL_STATE,
                        "batch enrollment not in trial state or the user is trying to pay again");
            }
        } catch (NotFoundException e) {
            if (ErrorCode.BATCH_ENROLLMENT_NOT_FOUND.equals(e.getErrorCode())) {
                if (hasRegFee) {
                    throw new BadRequestException(ErrorCode.REGISTRATION_FEE_NOT_PAID, "Registration fee not ");
                }
            } else {
                throw e;
            }
        }
        return paymentManager.buyItems(buyItemsReq);
    }

    public EntityType checkOrderType(EnrollmentPojo enrollmentInfo) throws VException {
        logger.info("Check order type for " + enrollmentInfo.getId());
        GetOrdersByDeliverableEntityReq getOrdersReq = new GetOrdersByDeliverableEntityReq();
        List<String> deliverableId = Arrays.asList(enrollmentInfo.getId());
        getOrdersReq.setDeliverableorEntityIds(deliverableId);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/getOrdersByDeliverableOrEntity", HttpMethod.POST, new Gson().toJson(getOrdersReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type listType1 = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(jsonString, listType1);
        if (ArrayUtils.isEmpty(orders)) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "order not found for the enrollmentId : " + enrollmentInfo.getId());
        }
        EntityType entityType = EntityType.OTF_COURSE_REGISTRATION;
        for (Orders order : orders) {
            if (enrollmentInfo.getId().equals(order.getItems().get(0).getDeliverableEntityId())) {
                if (EntityType.OTF_BATCH_REGISTRATION.equals(order.getItems().get(0).getEntityType())) {
                    return EntityType.OTF_BATCH_REGISTRATION;
                }
            }
        }
        return entityType;
    }

    public List<EnrollmentPojo> getEnrollmentsByEnrollmentIds(GetEnrollmentsReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String getEnrollmentsUrl = ontofewEndpoint + "enroll/getEnrollmentsByEnrollmentIds";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> slist = gson.fromJson(jsonString, listType);

        return slist;
    }

    public BatchBasicInfo getBatch(String id) throws VException {
        String batchUrl = ontofewEndpoint + "/batch/basicInfo/" + id;
        ClientResponse batchResp = WebUtils.INSTANCE.doCall(batchUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
        String jsonString = batchResp.getEntity(String.class);
        BatchBasicInfo batchBasicInfo = new Gson().fromJson(jsonString, BatchBasicInfo.class);

        return batchBasicInfo;
    }

    public List<GetOTFInstalmentReminderRes> getOTFFirstInstalmentReminderList(Long startTime, Long endTime) throws VException, InterruptedException {
        if (startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "startTime or endTime is null");
        }
        List<GetOTFInstalmentReminderRes> resList = new ArrayList<>();
        List<BaseInstalment> baseInstalments = paymentManager.getFirstBaseInstalmentsForDueTime(startTime, endTime);

        if (ArrayUtils.isEmpty(baseInstalments)) {
            return resList;
        }
        List<String> batchIds = new ArrayList<>();
        Map<String, Long> batchDueTime = new HashMap<>();
        for (BaseInstalment baseInstalment : baseInstalments) {
            batchIds.add(baseInstalment.getPurchaseEntityId());
            if (ArrayUtils.isNotEmpty(baseInstalment.getInfo()) && baseInstalment.getInfo().get(0) != null) {
                batchDueTime.put(baseInstalment.getPurchaseEntityId(), baseInstalment.getInfo().get(0).getDueTime());
            }
        }
        GetBatchesForDashboardReq req = new GetBatchesForDashboardReq();
        req.setBatchIds(batchIds);
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String url = ontofewEndpoint + "enroll/getTrialEnrollmentsForBatchList";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<List<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> enrollmentPojos = gson.fromJson(jsonString, listType);

        List<Long> userIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(enrollmentPojos)) {
            for (EnrollmentPojo enrollmentPojo : enrollmentPojos) {
                if (StringUtils.isNotEmpty(enrollmentPojo.getUserId())) {
                    userIds.add(Long.parseLong(enrollmentPojo.getUserId()));
                }
            }
            Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            Map<Long, DashboardUserInstalmentHistoryRes> historyMap = paymentManager.getUserInstalmentTransactionHistory(userIds);
            List<LeadDetailsSerialized> leadDetails = leadSquaredManager.getLeadDetails("mx_User_Id", userIds);
            Map<Long, LeadDetailsSerialized> leadMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(leadDetails)) {
                for (LeadDetailsSerialized lead : leadDetails) {
                    if (StringUtils.isNotEmpty(lead.getMx_User_Id()) && !"null".equalsIgnoreCase(lead.getMx_User_Id())) {
                        leadMap.put(Long.parseLong(lead.getMx_User_Id()), lead);
                    }
                }
            }
            for (EnrollmentPojo enrollmentPojo : enrollmentPojos) {
                if (StringUtils.isNotEmpty(enrollmentPojo.getUserId())) {
                    GetOTFInstalmentReminderRes res = new GetOTFInstalmentReminderRes();
                    Long userId = Long.parseLong(enrollmentPojo.getUserId());
                    res.setBatchId(enrollmentPojo.getBatchId());
                    res.setEnrollmentId(enrollmentPojo.getEnrollmentId());
                    if (res.getState() != null) {
                        res.setState(enrollmentPojo.getState().name());
                    }
                    if (res.getStatus() != null) {
                        res.setStatus(enrollmentPojo.getStatus().name());
                    }
                    if (leadMap.containsKey(userId)) {
                        res.setAgentName(leadMap.get(userId).getOwnerIdName());
                    }
                    if (userMap.containsKey(userId)) {
                        res.setStudent(userMap.get(userId));
                    }
                    if (historyMap.containsKey(userId)) {
                        res.setExtraInfos(historyMap.get(userId));
                    }
                    res.setStudentId(userId);
                    if (batchDueTime.containsKey(res.getBatchId())) {
                        res.setInstalmentDueTime(batchDueTime.get(res.getBatchId()));
                    }
                    resList.add(res);
                }
            }
        }
        return resList;
    }

    public List<EnrollmentPojo> getEnrollments(GetEnrollmentsReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String getEnrollmentsUrl = ontofewEndpoint + "enroll/enrollments";
        if (!StringUtils.isEmpty(queryString)) {
            getEnrollmentsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
        }.getType();
        List<EnrollmentPojo> slist = gson.fromJson(jsonString, listType);
        List<String> enrollIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(slist)) {
            for (EnrollmentPojo pojo : slist) {
                enrollIds.add(pojo.getId());
            }
        }

//        List<Orders> itemOrders = paymentManager.getOrdersByDeliverableOrEntity(enrollIds);
//        List<Orders> orders = paymentManager.getOrdersByDeliverableIds(enrollIds);
//        Map<String, Orders> orderMap = new HashMap<>();
//        if (ArrayUtils.isNotEmpty(orders)) {
//            for (Orders order : orders) {
//            	if(ArrayUtils.isNotEmpty(order.getItems())){
//            		OrderedItem item = order.getItems().get(0);
//            		if(StringUtils.isNotEmpty(item.getDeliverableEntityId())){
//            			orderMap.put(item.getDeliverableEntityId(), order);
//            		}
//            	}
//            	
//                if(ArrayUtils.isNotEmpty(order.getPurchasingEntities())){
//                    for(PurchasingEntity purchasingEntity:order.getPurchasingEntities()){
//                    	if(StringUtils.isNotEmpty(purchasingEntity.getDeliverableId())){
//                    		orderMap.put(purchasingEntity.getDeliverableId(), order);
//                    	}
//                    }
//                }
//            }
//                    
//            }
//                       
        return slist;
    }

    public void endEnrollmentTrialOTM(EndOTFEnrollmentReq endOTFEnrollmentReq, EnrollmentPojo enrollmentInfo) throws VException {

        if (Boolean.TRUE.equals(endOTFEnrollmentReq.getGetRefundInfoOnly())) {
            return;
        }
        OTFEnrollmentReq enrollmentReq = new OTFEnrollmentReq();
        enrollmentReq.setBatchId(enrollmentInfo.getBatchId());
        enrollmentReq.setUserId(enrollmentInfo.getUserId());
        enrollmentReq.setRole(enrollmentInfo.getRole());
        enrollmentReq.setStatus("ENDED");
        enrollmentReq.setEndReason(endOTFEnrollmentReq.getReason());
        enrollmentReq.setEndedBy(endOTFEnrollmentReq.getCallingUserId());
        enrollmentReq.setId(endOTFEnrollmentReq.getId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "enroll/markStatus", HttpMethod.POST,
                new Gson().toJson(enrollmentReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        EnrollmentPojo enrollmentPojo = gson.fromJson(jsonString, EnrollmentPojo.class);

        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentInfo", enrollmentPojo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_UPDATE_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);
    }

    public List<BatchEnrolmentInfo> getBatchEnrolmentsByIds(Set<String> batchIds) throws VException {

        if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }

        ClientResponse resp = WebUtils.INSTANCE
                .doCall(SUBSCRIPTION_ENDPOINT + "batch/getEnrolmentsByBatchIds?", HttpMethod.POST, gson.toJson(batchIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BatchEnrolmentInfo>>() {
        }.getType();
        List<BatchEnrolmentInfo> infos = gson.fromJson(jsonString, listType);
        return infos;
    }
}
