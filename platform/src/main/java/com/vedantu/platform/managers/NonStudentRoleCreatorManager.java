package com.vedantu.platform.managers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.platform.dao.NonStudentRoleCreatorDAO;
import com.vedantu.platform.mongodbentities.NonStudentRoleCreator;
import com.vedantu.platform.request.NonStudentRoleCreatorReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NonStudentRoleCreatorManager {

    @Autowired
    private  LogFactory logFactory;

    @SuppressWarnings("static-access")
    private  Logger logger = logFactory.getLogger(NonStudentRoleCreatorManager.class);

    @Autowired
    private NonStudentRoleCreatorDAO nonStudentRoleCreatorDAO;


    public NonStudentRoleCreator addEditNonStudentRoleCreator(NonStudentRoleCreatorReq request) throws BadRequestException {
        request.verify();

        logger.info("request: "+request.toString());
        NonStudentRoleCreator creator = nonStudentRoleCreatorDAO.getByCreatorId(request.getCreatorId());
        if(creator == null){
            creator = new NonStudentRoleCreator(request.getCreatorId(), request.getAllowedRoles());
        }else{
            creator.setAllowedRoles(request.getAllowedRoles());
        }
        logger.info("NonStudentRoleCreator: "+creator.toString());
        nonStudentRoleCreatorDAO.create(creator);
        logger.info("NonStudentRoleCreator created in db");
        return creator;
    }

    public boolean isAllowedToCreateNonStudentRole(Long userId, Role role){
        NonStudentRoleCreator creator = nonStudentRoleCreatorDAO.getByCreatorId(userId);
        if(creator != null && ArrayUtils.isNotEmpty(creator.getAllowedRoles()) && creator.getAllowedRoles().contains(role)){
            return true;
        }
        return false;
    }

    public List<Role> getExistedRoles(Long userId) {
        List<Role> existedRoles = new ArrayList<>();
        if(userId != null){
            NonStudentRoleCreator creator = nonStudentRoleCreatorDAO.getByCreatorId(userId);
            if(creator != null && ArrayUtils.isNotEmpty(creator.getAllowedRoles())){
                existedRoles.addAll(creator.getAllowedRoles());
            }
        }
        return existedRoles;
    }
}
