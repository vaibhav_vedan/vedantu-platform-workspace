package com.vedantu.platform.managers.loam;

import com.vedantu.platform.dao.loam.LOAMAmbassadorPlatformDao;
import com.vedantu.platform.mongodbentities.board.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LOAMAmbassadorPlatformManager {

    @Autowired
    LOAMAmbassadorPlatformDao loamAmbassadorPlatformDao;

    public List<Board> loam_getBoardData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<Board> boards = new ArrayList<>();
        Set<String> requiredFields = new HashSet<>();

        boards = loamAmbassadorPlatformDao.loam_getBoardData(fromTime, thruTime,requiredFields,true, start, size);

        return boards;
    }
}
