package com.vedantu.platform.managers.moodle;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.NewSubjectUser;
import com.vedantu.User.Role;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.request.EnrollStudentToPublicReq;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.notification.pojos.MoodleContentInfo;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.pojo.lms.StudentTeacherMappingReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SessionModel;

/**
 *
 * @author somil
 */
@Service
public class MoodleManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MoodleManager.class);

	private static String createTeacherStudentSubjectContainerUrl;

	@Autowired
	private BoardManager boardManager;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private UserManager userManager;

	@Autowired
	private PlatformTools platformTools;

	@Autowired
	private CommunicationManager communicationManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private SessionManager sessionManager;

	public String moodleLandingPageUrl;

	public String enrollPublicUrl;
	// one to few end point is configured to contain / at the end
	private String LMS_ENDPOINT;

	public MoodleManager() {
	}

	@PostConstruct
	public void init() {
		LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
		createTeacherStudentSubjectContainerUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.moodle.url.api.teacherStudentSubject.createContainer");
		moodleLandingPageUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.moodle.url.landingPage");
		enrollPublicUrl = LMS_ENDPOINT
				+ ConfigUtils.INSTANCE.getStringValue("vedantu.moodle.url.api.student.enroll.public");
	}

	public void createTeacherContainer(User user) throws VException {
		if (!Role.TEACHER.equals(user.getRole()) && !Role.STUDENT.equals(user.getRole())) {
			logger.info("Invalid moodle role:" + user.getRole());
			return;
		}

		String targetUrl = LMS_ENDPOINT + "createUser";
		UserInfo userInfo = new UserInfo(user, null, false);
		boardManager.fillBoardInfo(userInfo);
		ClientResponse resp = WebUtils.INSTANCE.doCall(targetUrl, HttpMethod.POST, new Gson().toJson(userInfo), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		logger.info("Response:" + resp.getEntity(String.class));
	}

	public String addTeacherSubjectContainer(NewSubjectUser newSubjectUser) throws VException {
		String targetUrl = LMS_ENDPOINT + "addNewSubjectUser";
		ClientResponse resp = WebUtils.INSTANCE.doCall(targetUrl, HttpMethod.POST, new Gson().toJson(newSubjectUser), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		return resp.getEntity(String.class);
	}

	public void createTeacherStudentSubjectContainerTask(String subject, SessionInfo session) throws VException {
		// Populate attendees
		sessionManager.populateUserBasicInfos(session, true);

		// Moodle
		StudentTeacherMappingReq studentTeacherMappingReq = new StudentTeacherMappingReq();
		String teacherPrimarySubject = "";
		logger.info("going to add teacher");
		Long studentId = session.getStudentIds().get(0);
		if (studentId == null || studentId <= 0) {
			logger.info("student not found");
			return;
		}

		Long teacherId = session.getTeacherId();
		if (teacherId == null || teacherId <= 0) {
			logger.info("teacher not found");
			return;
		}
		UserBasicInfo student = null;

		if (session.getAttendees() != null) {
			for (UserBasicInfo userBasicInfo : session.getAttendees()) {
				if (Role.STUDENT.equals(userBasicInfo.getRole())) {
					student = userBasicInfo;
					break;
				}
			}
		}
		if (student == null) {
			List<Long> userIds = new ArrayList<>();
			userIds.add(studentId);
			Map<Long, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
			student = userInfos.get(studentId);
		}

		User teacher = userManager.getUserById(teacherId);
		logger.info("user : " + student.toString() + " teacher : " + teacher.toString());
		studentTeacherMappingReq.setTeacherInfo(new UserBasicInfo(teacher, true));
		TeacherInfo teacherInfo = teacher.getTeacherInfo();
		logger.info("teacherInfo : " + teacherInfo);
		if (teacherInfo != null) {
			logger.info("taking info found");
			Long primarySubjectId = teacherInfo.getPrimarySubject();
			Board board = boardManager.getBoardById(primarySubjectId);
			if (board != null) {
				teacherPrimarySubject = board.getSlug();
			}
		}

		logger.info("taking student profile");
		if (student.getRole() == null) {
			student.setRole(Role.STUDENT);
		}
		studentTeacherMappingReq.setStudentInfo(student);

		if (session.getModel() == null) {
			session.setModel(SessionModel.OTO);
		}

		logger.info(String.valueOf(session.getModel()));
		switch (session.getModel()) {
		case IL:
			logger.info("il case");
			studentTeacherMappingReq.setEngagementType(EngagementType.INSTALEARN);
			break;
		case TRIAL:
			logger.info("trial case");
			studentTeacherMappingReq.setEngagementType(EngagementType.TRIAL);
			break;
		default:
			if (subject.toLowerCase().equals("orientation")) {
				logger.info("orientation case");
				studentTeacherMappingReq.setEngagementType(EngagementType.TRIAL);
				subject = teacherPrimarySubject;
			} else if (session.getSessionSource() != null
					&& session.getSessionSource().equals(FeatureSource.READY_TO_TEACH)) {
				logger.info("ready to teach case");
				studentTeacherMappingReq.setEngagementType(EngagementType.INSTALEARN);
			} else {
				logger.info("oto case");
				studentTeacherMappingReq.setEngagementType(EngagementType.OTO);
			}
			break;
		}

		if (StringUtils.isEmpty(subject) || subject.toLowerCase().equals("orientation")) {
			subject = teacherPrimarySubject;
		}

		studentTeacherMappingReq.setSubject(subject);

		logger.info("going to invoke teacher student taks");
		createTeacherStudentSubjectContainer(studentTeacherMappingReq);
	}

	public PlatformBasicResponse createTeacherStudentSubjectContainer(
			StudentTeacherMappingReq studentTeacherMappingReq) {
		String targetUrl = createTeacherStudentSubjectContainerUrl;
		PlatformBasicResponse response = platformTools.postPlatformData(targetUrl,
				new Gson().toJson(studentTeacherMappingReq), true, HttpMethod.POST);
		return response;
	}

	public String expireContents() throws VException {
		String targetUrl = LMS_ENDPOINT + "expireContents";
		ClientResponse resp = WebUtils.INSTANCE.doCall(targetUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		return resp.getEntity(String.class);
	}


	public List<MoodleContentInfo> getAllContents(Long startTime, Long endTime, String queryParam) throws VException {
		String targetUrl = LMS_ENDPOINT + "getAllContents";
		targetUrl += "?startTime=" + startTime + "&endTime=" + endTime + "&queryParam=" + queryParam;
		ClientResponse resp = WebUtils.INSTANCE.doCall(targetUrl, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String contentInfoString = resp.getEntity(String.class);
		Type listType = new TypeToken<List<MoodleContentInfo>>() {
		}.getType();
		List<MoodleContentInfo> contentInfos = new Gson().fromJson(contentInfoString, listType);
		return contentInfos;
	}

	public void moodleReminders() throws VException {
		Long currentTime = System.currentTimeMillis();
		currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
		logger.info("sendDailyContentInfoEmail currentTime : " + currentTime);

		// 1 day before the deadline
		List<MoodleContentInfo> contentInfos = getAllContents(currentTime + DateTimeUtils.MILLIS_PER_DAY,
				currentTime + 2 * DateTimeUtils.MILLIS_PER_DAY, "expiryTime");
		if (contentInfos != null) {
			for (MoodleContentInfo contentInfo : contentInfos) {
				try {
					if ("SHARED".equals(contentInfo.getContentState())) {
						communicationManager.sendMoodleContentReminder(contentInfo);
					}
				} catch (Exception ex) {
					logger.error("sendDailyContentInfoEmailError : error sending email for contentInfo" + contentInfo,
							ex);
				}
			}
		}

		// 2 days after evaluation
		contentInfos = getAllContents(currentTime - 2 * DateTimeUtils.MILLIS_PER_DAY,
				currentTime - DateTimeUtils.MILLIS_PER_DAY, "attemptedTime");
		if (contentInfos != null) {
			for (MoodleContentInfo contentInfo : contentInfos) {
				try {
					if ("ATTEMPTED".equals(contentInfo.getContentState())) {
						communicationManager.sendMoodleEvaluationReminder(contentInfo);
					}
				} catch (Exception ex) {
					logger.error("sendDailyContentInfoEmailError : error sending email for contentInfo" + contentInfo,
							ex);
				}
			}
		}
	}

	public void moodleLandingPage(HttpServletRequest request, HttpServletResponse response) throws VException {
            return;
            /*
		try {
			String returnUrl = moodleLandingPageUrl;
			HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
			Long userId = sessionData.getUserId();
			if (userId != null && userId > 0l) {
				try {
					User user = userManager.getUserById(userId);
					if (user != null && Role.TEACHER.equals(user.getRole())) {
						TeacherInfo teacherInfo = user.getTeacherInfo();
						if (teacherInfo != null) {
							Long primarySubjectId = teacherInfo.getPrimarySubject();
							Board board = boardManager.getBoardById(primarySubjectId);
							if (board != null) {
								String courseName = userId + "-" + board.getSlug();
								String url = getCourseUrl + "?courseName=" + courseName;
								PlatformBasicResponse courseResponse = platformTools.postPlatformData(url, null, true,
										HttpMethod.GET);
								returnUrl = courseResponse.getResponse();
							}
						}
					}
				} catch (Exception ex) {
					logger.error("moodleLandingPageError", ex);
				}
			}

			response.sendRedirect(returnUrl);
		} catch (Exception ex) {
			throw new VException(ErrorCode.SERVICE_ERROR, "Something went wrong. Please try again later.");
		}
                */
	}

	public String moodleContainerUrl(String sessionId) throws VException {
            return null;
            /*
		if (StringUtils.isEmpty(sessionId)) {
			logger.info("moodleContainerPage", "moodleContainerUrlError : session id not found");
			return moodleLandingPageUrl;
		}
		SessionInfo session = sessionManager.getSessionById(Long.parseLong(sessionId));

		EngagementType engagementType = EngagementType.OTO;
		if (session.getSubject().toLowerCase().equals("orientation")) {
			engagementType = EngagementType.TRIAL;
		} else if (session.getSessionSource() != null
				&& session.getSessionSource().equals(FeatureSource.READY_TO_TEACH)) {
			engagementType = EngagementType.INSTALEARN;
		}

		String subject = session.getSubject();
		if (subject.toLowerCase().equals("orientation")) {
			TeacherInfo teacherInfo = userManager.getUserById(session.getTeacherId()).getTeacherInfo();
			if (teacherInfo != null) {
				Long primarySubjectId = teacherInfo.getPrimarySubject();
				Board board = boardManager.getBoardById(primarySubjectId);
				if (board != null) {
					subject = board.getSlug();
				}
			}
		}

		String courseName = session.getStudentIds().get(0) + "-" + getValidSubject(subject) + "-"
				+ session.getTeacherId() + "-" + engagementType;
		String url = getCourseUrl + "?courseName=" + courseName;
		PlatformBasicResponse response = platformTools.postPlatformData(url, null, true, HttpMethod.GET);
		return response.getResponse();
*/
	}

	public void enrollToPublicGroup(HttpServletRequest request, HttpServletResponse response)
			throws IOException, VException {
		String targetUrl = enrollPublicUrl;
		// String queryString = request.getQueryString();

		if (request.getParameter("callingUserId") == null) {
			throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "action not allowed");
		}
		Long userId = Long.parseLong(request.getParameter("callingUserId"));
		String contentId = request.getParameter("contentId");
		User student = userManager.getUserById(userId);

		EnrollStudentToPublicReq enrollStudentToPublicReq = new EnrollStudentToPublicReq();
		enrollStudentToPublicReq.setStudentInfo(student);
		enrollStudentToPublicReq.setContentId(contentId);

		PlatformBasicResponse resp = platformTools.postPlatformData(targetUrl,
				new Gson().toJson(enrollStudentToPublicReq), true, HttpMethod.POST);
		response.sendRedirect(resp.getResponse());
	}

	public String getValidSubject(String slug) {
		if (!StringUtils.isEmpty(slug)) {
			slug = slug.replaceAll(" ", "_");
			slug = slug.replaceAll("-", "_");
		}
		return slug;
	}
}
