package com.vedantu.platform.managers.review;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.review.FeedbackDao;
import com.vedantu.platform.mongodbentities.review.Feedback;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.review.requests.GetFeedbackRequest;
import com.vedantu.review.requests.GiveFeedbackRequest;
import com.vedantu.review.requests.UpdateFeedbackRequest;
import com.vedantu.review.response.FeedbackInfo;
import com.vedantu.review.response.GetFeedbackResponse;
import com.vedantu.review.response.GiveFeedbackResponse;
import com.vedantu.review.response.UpdateFeedbackResponse;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

@Service
public class FeedbackManager {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(FeedbackManager.class);

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private DozerBeanMapper mapper; 

    @Autowired
    private UserManager userManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public GiveFeedbackResponse giveFeedback(GiveFeedbackRequest feedbackRequest)
            throws AddressException, IOException, VException {

        Feedback feedback = mapper.map(feedbackRequest.toFeedback(),
                Feedback.class);
        feedback.setLastUpdated(System.currentTimeMillis());
        //feedback.setLastUpdatedBy(feedbackRequest.getCallingUserId().toString());
        feedbackDao.upsert(feedback, feedbackRequest.getCallingUserId()!=null? feedbackRequest.getCallingUserId().toString(): null);

        com.vedantu.review.pojo.Feedback result = mapper.map(feedback,
                com.vedantu.review.pojo.Feedback.class);
        GiveFeedbackResponse feedbackRes = new GiveFeedbackResponse(result);

        // Communicate the changes to student, teacher and parent
//        if (feedbackRequest.getNoEmailAlert() == null
//                || !feedbackRequest.getNoEmailAlert()) {
//            Map<String, Object> payload = new HashMap<>();
//            payload.put("feedback", feedback);
//            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_FEEDBACK_EMAIL, payload);
//            asyncTaskFactory.executeTask(params);
//        }

        logger.info("giveFeedbackRes : " + feedbackRes.toString());
        return feedbackRes;
    }

    // public GiveOfferingFeedbackRes
    // giveOfferingFeedback(GiveOfferingFeedbackReq feedbackReq)
    // throws BadRequestException, ConflictException {
    //
    // logEntering("giveOfferingFeedbackRes", feedbackReq);
    //
    // feedbackReq.verify();
    // if (feedbackReq.getRating() < 1 || feedbackReq.getRating() > 5) {
    // throw
    // VExceptionFactory.INSTANCE.conflictException(ErrorCode.INVALID_RATING,
    // "Invalid rating provided",
    // null);
    //
    // }
    // IFeedbackDAO feedbackDAO = DAOFactory.INSTANCE.getFeedbackDAO();
    // OfferingFeedback feedback =
    // feedbackDAO.insert(feedbackReq.toOfferingFeedback());
    // GiveOfferingFeedbackRes feedbackRes = new
    // GiveOfferingFeedbackRes(feedback);
    //
    // logExiting("giveFeedbackRes", feedbackRes);
    // return feedbackRes;
    //
    // }
    public GetFeedbackResponse fetchFeedback(GetFeedbackRequest getReq) {

        List<Feedback> retrievedData = feedbackDao.get(null,
                getReq.getReceiverId(),
                getReq.getSenderId(), getReq.getSessionId(),
                null, getReq.getStartTime(), getReq.getEndTime(),
                (getReq.getFrom() == null) ? null : getReq.getFrom().intValue(),
                (getReq.getTill() == null) ? null : getReq.getTill().intValue(),
                getReq.getOrderDesc());
        List<FeedbackInfo> result = new ArrayList<>();
        Set<String> userIds = new HashSet<>();
        for (Feedback feedback : retrievedData) {
            userIds.add(feedback.getSenderId());
            userIds.add(feedback.getReceiverId());
        }
        Map<String, UserBasicInfo> userInfosMap = fosUtils.getUserBasicInfosMap(userIds, false);

        for (Feedback feedback : retrievedData) {
            com.vedantu.review.pojo.Feedback tempFeedback = mapper.map(
                    feedback, com.vedantu.review.pojo.Feedback.class);
            FeedbackInfo feedbackInfo=new FeedbackInfo(tempFeedback);
            
            if(userInfosMap.containsKey(feedback.getSenderId())){
                feedbackInfo.setSender(userInfosMap.get(feedback.getSenderId()));
            }
            if(userInfosMap.containsKey(feedback.getReceiverId())){
                feedbackInfo.setReceiver(userInfosMap.get(feedback.getReceiverId()));
            }
            result.add(feedbackInfo);
        }

        GetFeedbackResponse response = new GetFeedbackResponse();
        response.setData(result);
        logger.info("fetchFeedback" + response.toString());
        return response;
    }

    public UpdateFeedbackResponse updateFeedback(UpdateFeedbackRequest updateReq)
            throws VException {

        Feedback feedback = feedbackDao.getById(updateReq.getFeedbackId());
        if (feedback == null) {
            throw new VException(ErrorCode.FEEDBACK_NOT_FOUND, null);
        }
        Integer rating = updateReq.getRating();
        if (rating != null && rating > 0 && rating <= 5) {
            feedback.setRating(rating);
        }

        if (updateReq.getPartnerMessage() != null) {
            feedback.setPartnerMessage(updateReq.getPartnerMessage());
        }
        if (updateReq.getSessionMessage() != null) {
            feedback.setSessionMessage(updateReq.getSessionMessage());
        }
        if (updateReq.getReason() != null) {
            feedback.setReason(updateReq.getReason());
        }
        if (updateReq.getOptionalMessage() != null) {
            feedback.setOptionalMessage(updateReq.getOptionalMessage());
        }

        feedback.setLastUpdated(System.currentTimeMillis());
        //feedback.setLastUpdatedBy(updateReq.getCallingUserId().toString());

        feedbackDao.upsert(feedback, updateReq.getCallingUserId()!=null? updateReq.getCallingUserId().toString(): null);

        com.vedantu.review.pojo.Feedback result = mapper.map(feedback,
                com.vedantu.review.pojo.Feedback.class);
        UpdateFeedbackResponse response = new UpdateFeedbackResponse(result);
        logger.info("updateFeedback" + response.toString());
        return response;
    }

    public GiveFeedbackResponse giveTechRating(
            GiveFeedbackRequest giveFeedbackRequest) throws VException,
            IOException {

        User user = userManager.getUserById(giveFeedbackRequest
                .getCallingUserId());

        if (user == null) {
            throw new VException(ErrorCode.USER_NOT_FOUND,
                    "no user found with id["
                    + giveFeedbackRequest.getCallingUserId() + "]");
        } else if (!user.getRole().equals(Role.STUDENT)
                && !user.getRole().equals(Role.TEACHER)) {
            throw new VException(ErrorCode.FORBIDDEN_ERROR,
                    "Not allowed for User : " + user.getRole());
        }

        Feedback feedback = mapper.map(giveFeedbackRequest.toFeedback(),
                com.vedantu.platform.mongodbentities.review.Feedback.class);

        feedback.setLastUpdated(System.currentTimeMillis());
        //feedback.setLastUpdatedBy(giveFeedbackRequest.getCallingUserId().toString());
        feedbackDao.upsert(feedback, giveFeedbackRequest.getCallingUserId()!=null? giveFeedbackRequest.getCallingUserId().toString(): null);
        com.vedantu.review.pojo.Feedback feedbackRes = mapper.map(feedback,
                com.vedantu.review.pojo.Feedback.class);

        GiveFeedbackResponse giveFeedbackResponse = new GiveFeedbackResponse(
                feedbackRes);

        // Communicate the changes to student, teacher and parent
//        if (giveFeedbackRequest.getNoEmailAlert() == null
//                || !giveFeedbackRequest.getNoEmailAlert()) {
//            Map<String, Object> payload = new HashMap<>();
//            payload.put("feedback", feedback);
//            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_FEEDBACK_EMAIL, payload);
//            asyncTaskFactory.executeTask(params);
//        }

        logger.info("giveTechRatingServletRes: " + feedbackRes.toString());
        return giveFeedbackResponse;
    }


    public List<Feedback> get(List<String> ids, String receiverId,
                              String senderId, String sessionId, Integer rating, Long fromTime,
                              Long toTime, Integer start, Integer limit, Boolean orderDesc) {
        return feedbackDao.get(ids, receiverId, senderId, sessionId, rating, fromTime, toTime, start, limit, orderDesc);
    }
}
