package com.vedantu.platform.managers;


import com.vedantu.platform.dao.MobileRequestDAO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.platform.mongodbentities.mobile.MobileConfig;
import com.vedantu.platform.request.mobile.MobileConfigReq;
import com.vedantu.platform.mongodbentities.mobile.MobileGlobalConfiguration;
import com.vedantu.platform.enums.mobile.MobilePlatform;
import com.vedantu.platform.enums.mobile.MobileRole;
import com.vedantu.platform.request.mobile.UpdateGlobalMobileParamsReq;
import com.vedantu.util.LogFactory;

@Service
public class MobileConfigManager {

	@Autowired
	private MobileRequestDAO mobileRequestDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MobileConfigManager.class);

	public List<MobileConfig> upsertMobileConfig(MobileConfigReq mobileConfigReq) {
		logger.info("upsertMobileconfig : " + mobileConfigReq.toString());
		List<MobileConfig> res = new ArrayList<MobileConfig>();
		MobileConfig mobileConfig;
		if (mobileConfigReq.getUserId() != null)
			mobileConfig = new MobileConfig(Long.parseLong(mobileConfigReq.getUserId()), mobileConfigReq.getKey(),
					mobileConfigReq.getValue(), mobileConfigReq.getDeviceId(), mobileConfigReq.getVersionCode(),
					mobileConfigReq.getPlatform(), mobileConfigReq.getPlatformVersion());
		else
			mobileConfig = new MobileConfig(null, mobileConfigReq.getKey(), mobileConfigReq.getValue(),
					mobileConfigReq.getDeviceId(), mobileConfigReq.getVersionCode(), mobileConfigReq.getPlatform(),
					mobileConfigReq.getPlatformVersion());

		mobileRequestDAO.update(mobileConfig, createMobileConfigUpdateObject(mobileConfig));
		res.add(mobileConfig);
		return res;
	}

	public Update createMobileConfigUpdateObject(MobileConfig p) {
		Update update = new Update();
		update.set("key", p.getKey());
		update.set("platform", p.getPlatform());
		update.set("platformVersion", p.getPlatformVersion());
		update.set("deviceId", p.getDeviceId());
		update.set("userId", p.getUserId());
		update.set("value", p.getValue());
		update.set("versionCode", p.getVersionCode());
		return update;

	}

	public Update createGlobalMobileConfigUpdateObject(MobileGlobalConfiguration p) {

		Update update = new Update();
		update.set("userId", p.getUserId());
		update.set("deviceId", p.getDeviceId());
		update.set("force_update_version", p.getForce_update_version());
		update.set("optional_update_version", p.getOptional_update_version());
		update.set("mobile_support_email_address", p.getMobile_support_email_address());
		update.set("platform", p.getPlatform());
		update.set("role", p.getRole());
		update.set("global_message", p.getGlobal_message());
		update.set("rtc_client_type", p.getRtc_client_type());
		update.set("rtc_ice_server", p.getRtc_ice_server());
		update.set("rtc_turn_server", p.getRtc_turn_server());
		update.set("rtc_turn_server_user_name", p.getRtc_turn_server_user_name());
		update.set("rtc_turn_server_password", p.getRtc_turn_server_password());
		update.set("socialvid_tenant_name", p.getSocialvid_tenant_name());
		update.set("session", p.isSession());
		update.set("cmds", p.isCmds());
		update.set("teacherCmdsHelpVideo", p.getTeacherCmdsHelpVideo());
		update.set("studentCmdsHelpVideo", p.getStudentCmdsHelpVideo());
		return update;

	}

	public MobileGlobalConfiguration upsertGlobalMobileConfiguration(UpdateGlobalMobileParamsReq mobileConfigReq) {
		logger.info("update mobile global params : " + mobileConfigReq.toString());

		MobileGlobalConfiguration mobileGlobalConfiguration = new MobileGlobalConfiguration(mobileConfigReq.getUserId(),
				mobileConfigReq.getDeviceId(), mobileConfigReq.getPlatform(), mobileConfigReq.getRole(),
				mobileConfigReq.getForce_update_version(), mobileConfigReq.getOptional_update_version(),
				mobileConfigReq.getMobile_support_email_address(), mobileConfigReq.getGlobal_message(),
				mobileConfigReq.getRtc_client_type(), mobileConfigReq.getRtc_ice_server(),
				mobileConfigReq.getRtc_turn_server(), mobileConfigReq.getRtc_turn_server_user_name(),
				mobileConfigReq.getRtc_turn_server_password(), mobileConfigReq.getSocialvid_tenant_name(),
				mobileConfigReq.isSession(), mobileConfigReq.isCmds(), mobileConfigReq.getTeacherCmdsHelpVideo(),
				mobileConfigReq.getStudentCmdsHelpVideo());
		mobileRequestDAO.updateGlobalConfig(mobileGlobalConfiguration,
				createGlobalMobileConfigUpdateObject(mobileGlobalConfiguration));

		return mobileGlobalConfiguration;
	}

	public MobileConfig getMobileConfig(String userId, String deviceId, MobilePlatform platform, String version,
			String versionCode) {
		logger.info("getMobileconfig : ");

		List<MobileConfig> mobileConfig = mobileRequestDAO.get(userId, deviceId, platform, version, versionCode);
		if (mobileConfig.size() > 0)
			return mobileConfig.get(0);
		else
			return null;
	}

	public MobileGlobalConfiguration getMobileGlobalConfiguration(String userId, String deviceId,
			MobilePlatform platform, MobileRole mobilerole) {
		logger.info("getMobileconfig : ");

		List<MobileGlobalConfiguration> mobileGlobalConfiguration = mobileRequestDAO.getGlobalMobileParams(userId,
				deviceId, platform, mobilerole);

		if (mobileGlobalConfiguration.size() > 0)
			return mobileGlobalConfiguration.get(0);
		else
			return null;
	}
}
