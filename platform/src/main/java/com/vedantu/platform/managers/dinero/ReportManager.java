/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.pojo.dinero.Transaction;
import com.vedantu.platform.pojo.dinero.TransactionUserInfo;
import com.vedantu.subscription.pojo.Subscription;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.supercsv.io.dozer.CsvDozerBeanWriter;
import org.supercsv.io.dozer.ICsvDozerBeanWriter;
import org.supercsv.prefs.CsvPreference;

/**
 *
 * @author somil
 */
@Service
public class ReportManager {

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ReportManager.class);

    public ReportManager() {
        super();
    }

    public String getSubscriptionTransactionsDetailsCSV(Long fromTime, Long toTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getSubscriptionTransactionsDetails?fromTime="+fromTime+"&toTime="+toTime,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<TransactionUserInfo>>() {
        }.getType();
        logger.info("jsonString " + jsonString);

        List<TransactionUserInfo> responseList = new Gson().fromJson(jsonString, listType);
        String response = null;
        if (responseList.size() > 0) {

            List<Long> subscriptionIds = new ArrayList<>();
            for (TransactionUserInfo transactionUserInfo : responseList) {
                if (transactionUserInfo.getOrders() != null && transactionUserInfo.getOrders().getContextId() != null) {
                    subscriptionIds.add(transactionUserInfo.getOrders().getContextId());
                }
            }

            if (!subscriptionIds.isEmpty()) {
                String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
                ClientResponse subscriptionResp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/getSubscriptionBasicInfoList",
                        HttpMethod.POST, new Gson().toJson(subscriptionIds));

                VExceptionFactory.INSTANCE.parseAndThrowException(subscriptionResp);

                String jsonStringSubscription = subscriptionResp.getEntity(String.class);
                Type listTypeSubscription = new TypeToken<ArrayList<Subscription>>() {
                }.getType();
                logger.info("jsonStringSubscription " + jsonString);

                Map<Long, Subscription> subscriptionMap = new HashMap<Long, Subscription>();

                List<Subscription> responseListSubscription = new Gson().fromJson(jsonStringSubscription, listTypeSubscription);
                if (responseListSubscription != null) {
                    for (Subscription subscription : responseListSubscription) {
                        if (subscription != null && subscription.getId() != null) {
                            subscriptionMap.put(subscription.getId(), subscription);
                        }
                    }
                }

                for (TransactionUserInfo transactionUserInfo : responseList) {
                    if (transactionUserInfo.getOrders() != null && transactionUserInfo.getOrders().getContextId() != null) {
                        transactionUserInfo.setSubscription(subscriptionMap.get(transactionUserInfo.getOrders().getContextId()));
                    }
                }

            }

            try {
                String[] FIELD_MAPPING = new String[]{
                    "orders.transactionId",
                    "orders.amount",
                    "orders.promotionalAmount",
                    "orders.nonPromotionalAmount",
                    "userBasicInfo.userId",
                    "userBasicInfo.email",
                    "userBasicInfo.contactNumber",
                    "userBasicInfo.fullName",
                    "userBasicInfo.role",
                    "subscription.teacherId",
                    "subscription.totalHours",
                    "subscription.model"
                };
                StringWriter stringWriter = new StringWriter();
                ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
                csvWriter.configureBeanMapping(TransactionUserInfo.class, FIELD_MAPPING);
                String[] header = {"Id", "Amount", "PromotionalAmount", "NonPromotionalAmount", 
                    "userId", "email", "contactNumber", "fullName", "role",
                    "teacherId", "totalHours", "model"};
                csvWriter.writeHeader(header);
                for (TransactionUserInfo transactionUserInfo : responseList) {
                    csvWriter.write(transactionUserInfo);
                }
                csvWriter.close();
                response = stringWriter.toString();
            } catch (IOException e) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in writing to CSV file");
            }

        }
        logger.info(response);
        return response;
    }
    
    public String getTransactionsToVedantuWalletsCSV(Long fromTime, Long toTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getTransactionsToVedantuWallets?fromTime="+fromTime+"&toTime="+toTime,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Transaction>>() {
        }.getType();
        logger.info("jsonString " + jsonString);

        List<Transaction> responseList = new Gson().fromJson(jsonString, listType);
        String response = null;
        if (responseList.size() > 0) {
            try {
                String[] FIELD_MAPPING = new String[]{
                    "id",
                    "amount",
                    "promotionalAmount",
                    "nonPromotionalAmount",
                    "debitFromAccount",
                    "creditToAccount",
                    "reasonType",
                    "reasonRefNo",
                    "reasonRefType",
                    "reasonNote",
                    "triggredBy"
                };
                StringWriter stringWriter = new StringWriter();
                ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
                csvWriter.configureBeanMapping(Transaction.class, FIELD_MAPPING);
                csvWriter.writeHeader(FIELD_MAPPING);
                for (Transaction transaction : responseList) {
                    csvWriter.write(transaction);
                }
                csvWriter.close();
                response = stringWriter.toString();
            } catch (IOException e) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in writing to CSV file");
            }

        }
        logger.info(response);
        return response;
    }

    public String getTransactionsFromVedantuWalletsCSV(Long fromTime, Long toTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getTransactionsFromVedantuWallets?fromTime="+fromTime+"&toTime="+toTime,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<TransactionUserInfo>>() {
        }.getType();
        logger.info("jsonString " + jsonString);

        List<TransactionUserInfo> responseList = new Gson().fromJson(jsonString, listType);
        String response = null;
        if (responseList.size() > 0) {
            try {
                String[] FIELD_MAPPING = new String[]{
                    "transaction.id",
                    "transaction.amount",
                    "transaction.promotionalAmount",
                    "transaction.nonPromotionalAmount",
                    "transaction.debitFromAccount",
                    "transaction.creditToAccount",
                    "transaction.reasonType",
                    "transaction.reasonRefNo",
                    "transaction.reasonRefType",
                    "transaction.reasonNote",
                    "transaction.triggredBy",
                    "userBasicInfo.userId",
                    "userBasicInfo.email",
                    "userBasicInfo.contactNumber",
                    "userBasicInfo.fullName",
                    "userBasicInfo.role"
                };
                StringWriter stringWriter = new StringWriter();
                ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
                csvWriter.configureBeanMapping(TransactionUserInfo.class, FIELD_MAPPING);
                String[] header = {"Id", "Amount", "PromotionalAmount", "NonPromotionalAmount", "debitFromAccount", "creditToAccount", "reasonType", "reasonRefNo", "reasonRefType", "reasonNote", "triggredBy", 
                    "userId", "email", "contactNumber", "fullName", "role"};
                csvWriter.writeHeader(header);
                for (TransactionUserInfo transactionUserInfo : responseList) {
                    csvWriter.write(transactionUserInfo);
                }
                csvWriter.close();
                response = stringWriter.toString();
            } catch (IOException e) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in writing to CSV file");
            }

        }
        logger.info(response);
        return response;
    }

    public String getExternalTransactionsDetailsCSV(Long fromTime, Long toTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getExternalTransactionsDetails?fromTime="+fromTime+"&toTime="+toTime,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<TransactionUserInfo>>() {
        }.getType();
        logger.info("jsonString " + jsonString);

        List<TransactionUserInfo> responseList = new Gson().fromJson(jsonString, listType);
        String response = null;
        if (responseList.size() > 0) {
            try {
                String[] FIELD_MAPPING = new String[]{
                    "transaction.id",
                    "transaction.amount",
                    "transaction.promotionalAmount",
                    "transaction.nonPromotionalAmount",
                    "transaction.debitFromAccount",
                    "transaction.creditToAccount",
                    "transaction.reasonType",
                    "transaction.reasonRefNo",
                    "transaction.reasonRefType",
                    "transaction.reasonNote",
                    "transaction.triggredBy",
                    "extTransaction.gatewayName",
                    "extTransaction.paymentChannelTransactionId",
                    "extTransaction.paymentMethod",
                    "extTransaction.vedantuOrderId",
                    "userBasicInfo.userId",
                    "userBasicInfo.email",
                    "userBasicInfo.contactNumber",
                    "userBasicInfo.fullName",
                    "userBasicInfo.role"
                };
                StringWriter stringWriter = new StringWriter();
                ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
                csvWriter.configureBeanMapping(TransactionUserInfo.class, FIELD_MAPPING);
                String[] header = {"Id", "Amount", "PromotionalAmount", "NonPromotionalAmount", "debitFromAccount", "creditToAccount", "reasonType", "reasonRefNo", "reasonRefType", "reasonNote", "triggredBy", 
                    "gatewayName",
                    "paymentChannelTransactionId",
                    "paymentMethod",
                    "vedantuOrderId", "userId", "email", "contactNumber", "fullName", "role"};
                csvWriter.writeHeader(header);
                for (TransactionUserInfo transactionUserInfo : responseList) {
                    csvWriter.write(transactionUserInfo);
                }
                csvWriter.close();
                response = stringWriter.toString();
            } catch (IOException e) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in writing to CSV file");
            }

        }
        logger.info(response);
        return response;
    }

    public String getTeacherPayoutTransactionsDetailsCSV(Long fromTime, Long toTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getTeacherPayoutTransactionsDetails?fromTime="+fromTime+"&toTime="+toTime,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<TransactionUserInfo>>() {
        }.getType();
        logger.info("jsonString " + jsonString);

        List<TransactionUserInfo> responseList = new Gson().fromJson(jsonString, listType);
        String response = null;
        if (responseList.size() > 0) {
            try {
                String[] FIELD_MAPPING = new String[]{
                    "transaction.id",
                    "transaction.amount",
                    "transaction.promotionalAmount",
                    "transaction.nonPromotionalAmount",
                    "transaction.debitFromAccount",
                    "transaction.creditToAccount",
                    "transaction.reasonType",
                    "transaction.reasonRefNo",
                    "transaction.reasonRefType",
                    "transaction.reasonNote",
                    "transaction.triggredBy",
                    "sessionPayout.id",
                    "sessionPayout.sessionId",
                    "sessionPayout.subscriptionId",
                    "sessionPayout.billingDuration",
                    "sessionPayout.sessionDuration",
                    "sessionPayout.date",
                    "sessionPayout.teacherPayout",
                    "sessionPayout.teacherPromotionalPayout",
                    "sessionPayout.teacherNonPromotionalPayout",
                    "sessionPayout.cut",
                    "sessionPayout.promotionalCut",
                    "sessionPayout.nonPromotionalCut",
                    "sessionPayout.teacherId",
                    "sessionPayout.studentId",
                    "userBasicInfo.email",
                    "userBasicInfo.contactNumber",
                    "userBasicInfo.fullName",
                    "teacherBasicInfo.email",
                    "teacherBasicInfo.contactNumber",
                    "teacherBasicInfo.fullName",};
                StringWriter stringWriter = new StringWriter();
                ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
                csvWriter.configureBeanMapping(TransactionUserInfo.class, FIELD_MAPPING);
                String[] header = {"Id", "Amount", "PromotionalAmount", "NonPromotionalAmount", "debitFromAccount", "creditToAccount", "reasonType", "reasonRefNo", "reasonRefType", "reasonNote", "triggredBy",
                    "sessionPayoutId", "sessionId", "subscriptionId", "billingDuration", "sessionDuration",
                    "date", "teacherPayout", "teacherPromotionalPayout", "teacherNonPromotionalPayout",
                    "cut", "promotionalCut", "nonPromotionalCut", "teacherId", "studentId",
                     "studentEmail", "studentContactNumber", "studentFullName", "teacherEmail", "teacherContactNumber", "teacherFullName"};
                csvWriter.writeHeader(header);
                for (TransactionUserInfo transactionUserInfo : responseList) {
                    csvWriter.write(transactionUserInfo);
                }
                csvWriter.close();
                response = stringWriter.toString();
            } catch (IOException e) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in writing to CSV file");
            }

        }
        logger.info(response);
        return response;
    }

    public String getVedantuCutTransactionsDetailsCSV(Long fromTime, Long toTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/report/getVedantuCutTransactionsDetails?fromTime="+fromTime+"&toTime="+toTime,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<TransactionUserInfo>>() {
        }.getType();
        logger.info("jsonString " + jsonString);

        List<TransactionUserInfo> responseList = new Gson().fromJson(jsonString, listType);
        String response = null;
        if (responseList.size() > 0) {
            try {
                String[] FIELD_MAPPING = new String[]{
                    "transaction.id",
                    "transaction.amount",
                    "transaction.promotionalAmount",
                    "transaction.nonPromotionalAmount",
                    "transaction.debitFromAccount",
                    "transaction.creditToAccount",
                    "transaction.reasonType",
                    "transaction.reasonRefNo",
                    "transaction.reasonRefType",
                    "transaction.reasonNote",
                    "transaction.triggredBy",
                    "sessionPayout.id",
                    "sessionPayout.sessionId",
                    "sessionPayout.subscriptionId",
                    "sessionPayout.billingDuration",
                    "sessionPayout.sessionDuration",
                    "sessionPayout.date",
                    "sessionPayout.teacherPayout",
                    "sessionPayout.teacherPromotionalPayout",
                    "sessionPayout.teacherNonPromotionalPayout",
                    "sessionPayout.cut",
                    "sessionPayout.promotionalCut",
                    "sessionPayout.nonPromotionalCut",
                    "sessionPayout.teacherId",
                    "sessionPayout.studentId",
                    "userBasicInfo.email",
                    "userBasicInfo.contactNumber",
                    "userBasicInfo.fullName",
                    "teacherBasicInfo.email",
                    "teacherBasicInfo.contactNumber",
                    "teacherBasicInfo.fullName",};
                StringWriter stringWriter = new StringWriter();
                ICsvDozerBeanWriter csvWriter = new CsvDozerBeanWriter(stringWriter, CsvPreference.STANDARD_PREFERENCE);
                csvWriter.configureBeanMapping(TransactionUserInfo.class, FIELD_MAPPING);
                String[] header = {"Id", "Amount", "PromotionalAmount", "NonPromotionalAmount", "debitFromAccount", "creditToAccount", "reasonType", "reasonRefNo", "reasonRefType", "reasonNote", "triggredBy",
                    "sessionPayoutId", "sessionId", "subscriptionId", "billingDuration", "sessionDuration",
                    "date", "teacherPayout", "teacherPromotionalPayout", "teacherNonPromotionalPayout",
                    "cut", "promotionalCut", "nonPromotionalCut", "teacherId", "studentId",
                     "studentEmail", "studentContactNumber", "studentFullName",  "teacherEmail", "teacherContactNumber", "teacherFullName"};
                csvWriter.writeHeader(header);
                for (TransactionUserInfo transactionUserInfo : responseList) {
                    csvWriter.write(transactionUserInfo);
                }
                csvWriter.close();
                response = stringWriter.toString();
            } catch (IOException e) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in writing to CSV file");
            }

        }
        logger.info(response);
        return response;
    }

}
