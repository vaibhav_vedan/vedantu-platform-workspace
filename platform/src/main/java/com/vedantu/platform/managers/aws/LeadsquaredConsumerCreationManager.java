package com.vedantu.platform.managers.aws;

import com.amazonaws.util.EC2MetadataUtils;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Set;

@Service
public class LeadsquaredConsumerCreationManager {

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LeadsquaredConsumerCreationManager.class);

    private static final Integer MAX_LS_CONSUMER_LIMIT = 5;

    public static final String PLATFORM_LS_CONSUMER_LIMIT_KEY = "PLATFORM_LS_CONSUMER_MAC_ADDRESS_USED";

    public boolean canCreateConsumer(){

        try {

            Set<String> consumerServersList = redisDAO.getSet(PLATFORM_LS_CONSUMER_LIMIT_KEY);

            logger.info("checking LS consumerlimit : {}", consumerServersList);

            String macAddress = EC2MetadataUtils.getMacAddress();

            if(CollectionUtils.isNotEmpty(consumerServersList)){

                if(consumerServersList.size() >= MAX_LS_CONSUMER_LIMIT && !consumerServersList.contains(macAddress)){
                    logger.info("ignoring consumer creation as the limit exceeded : {} of {}", consumerServersList.size(), MAX_LS_CONSUMER_LIMIT);
                    return false;
                }
            }

            redisDAO.addToSet(PLATFORM_LS_CONSUMER_LIMIT_KEY, macAddress);

        }catch (Exception e){
            //Swallow
        }

        return true;

    }

    @PreDestroy
    private void releaseLsConsumerLimit() {

        try {

            logger.info("lsConsumerList before release: {}", redisDAO.getSet(PLATFORM_LS_CONSUMER_LIMIT_KEY));

            String macAddress = EC2MetadataUtils.getMacAddress();
            redisDAO.popFromSet(PLATFORM_LS_CONSUMER_LIMIT_KEY, macAddress);

            logger.info("lsConsumerList after release: {}", redisDAO.getSet(PLATFORM_LS_CONSUMER_LIMIT_KEY));

        }catch (Exception e){
            logger.error("Error deleting the mac entry from LS consumer rediss :", e);
        }

    }

}
