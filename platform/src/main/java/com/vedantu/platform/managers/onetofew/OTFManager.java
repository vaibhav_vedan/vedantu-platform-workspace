/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.onetofew;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.google.common.collect.Lists;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.util.enums.*;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.OTFSessionFlag;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.request.*;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.aws.AwslambdaManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.notification.NotificationManager;
import com.vedantu.platform.managers.review.RemarkManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.pojo.onetofew.Conflict;
import com.vedantu.scheduling.request.session.GetSessionPartnersReq;
import com.vedantu.scheduling.response.session.GetLatestUpComingOrOnGoingSessionRes;
import com.vedantu.scheduling.response.session.GetSessionPartnersRes;
import com.vedantu.session.pojo.OTFCalendarSessionInfo;
import com.vedantu.session.pojo.OTFCourseBasicInfo;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.subscription.request.ProcessOtfPostPaymentReq;
import com.vedantu.util.*;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author shyam
 */
@Service
public class OTFManager {

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private AwslambdaManager awsLambdaManager;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private FosUtils fosUtils;

    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    private final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    @Autowired
    CommunicationManager communicationManager;

    @Autowired
    NotificationManager notificationManager;

    @Autowired
    SessionManager sessionManager;

    @Autowired
    PaymentManager paymentManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    RemarkManager remarkManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ReferralManager referralManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFManager.class);

    private final Gson gson = new Gson();
    public final static Long cronRunTime = 5 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);

    private static int LATEST_SESSION_CACHE_EXPIRY_MINUTE = 30;

    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private static final Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
    }.getType();

    private static Type HANDLE_RECORDING_LAMBDA_LIST_TYPE = new TypeToken<List<HandleOTFRecordingLambda>>() {
    }.getType();
    private static Long UPDATE_RECORDING_TIME = ConfigUtils.INSTANCE
            .getLongValue("otf.update.recording.time.window.millis"); // 4 hour
    private static Long DEFAULT_UPDATE_RECORDING_TIME = 4 * Long.valueOf(DateTimeUtils.MILLIS_PER_HOUR); // 5

    @Deprecated
    public List<OTFSessionPojoUtils> getUpcomingSessions(GetOTFUpcomingSessionReq getOTFUpcomingSessionReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getUpcomingSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getUpcomingSessions";
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getOTFUpcomingSessionReq);
        if (!StringUtils.isEmpty(queryString)) {
            getUpcomingSessionsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    @Deprecated
    public List<OTFSessionPojoUtils> getUserPastSessions(GetOTFSessionsReq getOTFSessionsReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        switch (getOTFSessionsReq.getCallingUserRole()) {
            case STUDENT:
                getOTFSessionsReq.setStudentId(getOTFSessionsReq.getCallingUserId());
                break;
            case TEACHER:
                getOTFSessionsReq.setTeacherId(getOTFSessionsReq.getCallingUserId());
                break;
            default:
                throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED,
                        "Calling user role is not supported for this request");
        }
        return getPastSessions(getOTFSessionsReq, false);
    }

    @Deprecated
    public List<OTFSessionPojoUtils> getPastSessions(GetOTFSessionsReq getOTFSessionsReq, boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getUpcomingSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getPastSessions";
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getOTFSessionsReq);
        if (!StringUtils.isEmpty(queryString)) {
            getUpcomingSessionsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());

        populatePresenterInfos(sessions, exposeEmail);
        populateAttendeeInfos(sessions, exposeEmail);
        return sessions;
    }

    @Deprecated
    public OTFSessionPojoUtils getSessionById(String sessionId) throws VException {
        logger.info("Request:" + sessionId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/onetofew/session/" + sessionId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return gson.fromJson(jsonString, OTFSessionPojoUtils.class);
    }

    public GetLatestUpComingOrOnGoingSessionRes getUpcomingSessionsForTicker(
            GetOTFUpcomingSessionReq getOTFUpcomingSessionReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getUpcomingSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getUpcomingSessionsForTicker";
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(getOTFUpcomingSessionReq);
        if (!StringUtils.isEmpty(queryString)) {
            getUpcomingSessionsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        GetLatestUpComingOrOnGoingSessionRes sessions = gson.fromJson(jsonString,
                GetLatestUpComingOrOnGoingSessionRes.class);
        // if(sessions != null && sessions.getOtfSession() != null){
        // SessionPojo otf = sessions.getOtfSession();
        // if(otf != null && otf.getBatchIds() != null && otf.getBoardId() != null){
        // try{
        // GetLastSessionRemarkResp previousSessionResp =
        // remarkManager.getLastSessionRemark(otf.getBatchIds(),
        // otf.getBoardId(),otf.getId());
        // sessions.getOtfSession().setPreviousSessionRemark(previousSessionResp.getLastSessionRemark());
        // }catch(Exception e){
        // logger.info("remark not found");
        // }
        // }
        // }
        return sessions;
    }

    @Deprecated
    public List<OTFCalendarSessionInfo> getCalendarSessions(String beforeStartTime, String afterStartTime,
                                                            String beforeEndTime, String afterEndTime, Long userId, Role userRole, String query) throws VException {
        String getCalendarSessiosnUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getCalendarSessions?callingUserId=" + userId
                + "&callingUserRole=" + userRole;
        if (!StringUtils.isEmpty(beforeStartTime)) {
            getCalendarSessiosnUrl += "&beforeStartTime=" + beforeStartTime;
        }
        if (!StringUtils.isEmpty(afterStartTime)) {
            getCalendarSessiosnUrl += "&afterStartTime=" + afterStartTime;
        }
        if (!StringUtils.isEmpty(beforeEndTime)) {
            getCalendarSessiosnUrl += "&beforeEndTime=" + beforeEndTime;
        }
        if (!StringUtils.isEmpty(afterEndTime)) {
            getCalendarSessiosnUrl += "&afterEndTime=" + afterEndTime;
        }

        if (!StringUtils.isEmpty(query)) {
            getCalendarSessiosnUrl += "&query=" + query;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getCalendarSessiosnUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        List<OTFCalendarSessionInfo> calendarSessionInfos = populateUsersAndBoards(sessions);
        return calendarSessionInfos;
    }

    @Deprecated
    public List<OTFCalendarSessionInfo> getUserUpcomingSessions(Integer start, Integer size, String batchId, Long userId, Role userRole, String query, Long startTime, Long endTime, Set<String> batchIds) throws VException {
        String getCalendarSessiosnUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getUserUpcomingSessions?callingUserId=" + userId
                + "&callingUserRole=" + userRole.toString();
        if (!StringUtils.isEmpty(batchId)) {
            getCalendarSessiosnUrl += "&batchId=" + batchId;
        }
        if (ArrayUtils.isNotEmpty(batchIds)) {
            getCalendarSessiosnUrl += "&batchIds=" + String.join(",", batchIds);
        }
        if (start != null) {
            getCalendarSessiosnUrl += "&start=" + start;
        }
        if (size != null) {
            getCalendarSessiosnUrl += "&size=" + size;
        }
        if (!StringUtils.isEmpty(query)) {
            getCalendarSessiosnUrl += "&query=" + query;
        }
        if (startTime != null && endTime != null) {
            getCalendarSessiosnUrl += "&startTime=" + startTime;
            getCalendarSessiosnUrl += "&endTime=" + endTime;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getCalendarSessiosnUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        List<OTFCalendarSessionInfo> calendarSessionInfos = populateUsersAndBoards(sessions);
        return calendarSessionInfos;
    }

    @Deprecated
    public List<OTFCalendarSessionInfo> getUserPastSessions(Integer start, Integer size, String batchId, Long userId,
                                                            Role userRole, String query, Long startTime, Long endTime,
                                                            Set<String> batchIds) throws VException {
        String getCalendarSessiosnUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getUserPastSessions?callingUserId=" + userId
                + "&callingUserRole=" + userRole.toString();
        if (!StringUtils.isEmpty(batchId)) {
            getCalendarSessiosnUrl += "&batchId=" + batchId;
        }
        if (ArrayUtils.isNotEmpty(batchIds)) {
            getCalendarSessiosnUrl += "&batchIds=" + String.join(",", batchIds);
        }
        if (start != null) {
            getCalendarSessiosnUrl += "&start=" + start;
        }
        if (size != null) {
            getCalendarSessiosnUrl += "&size=" + size;
        }
        if (!StringUtils.isEmpty(query)) {
            getCalendarSessiosnUrl += "&query=" + query;
        }
        if (startTime != null && endTime != null) {
            getCalendarSessiosnUrl += "&startTime=" + startTime;
            getCalendarSessiosnUrl += "&endTime=" + endTime;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(getCalendarSessiosnUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        List<OTFCalendarSessionInfo> calendarSessionInfos = populateUsersAndBoards(sessions);
        return calendarSessionInfos;
    }


    @Deprecated
    public List<OTFSessionPojoUtils> getSessions(HttpServletRequest request) throws VException {
        String queryString = request.getQueryString();
        logger.info("queryString - " + queryString);
        String getSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session";
        if (!StringUtils.isEmpty(queryString)) {
            getSessionsUrl += "?" + queryString;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        if (!StringUtils.isEmpty(jsonString)) {
            List<OTFSessionPojoUtils> results = gson.fromJson(jsonString, otfSessionListType);
            if (!CollectionUtils.isEmpty(results)) {
                sessions.addAll(results);
            }
        }

        populatePresenterInfos(sessions, false);
        return sessions;
    }

    @Deprecated
    public List<OTFSessionPojoUtils> getUpcomingSessionsWithAttendeeInfos(long startTime, long endTime, Integer start, Integer size, String sessionId) throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/getUpcomingSessionsWithAttendeeInfos?startTime=" + startTime
                + "&endTime=" + endTime;
        if (start != null) {
            url += "&start=" + start;
        }
        if (size != null) {
            url += "&size=" + size;
        }
        if (sessionId != null) {
            url += "&sessionId=" + sessionId;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        // Parse the response
        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        if (!StringUtils.isEmpty(jsonString)) {
            List<OTFSessionPojoUtils> results = gson.fromJson(jsonString, otfSessionListType);
            if (!CollectionUtils.isEmpty(results)) {
                sessions.addAll(results);
            }
        }

        populatePresenterInfos(sessions, true);
        populateAttendeeInfos(sessions, true);
        return sessions;
    }

    public List<OTFSessionPojoUtils> getSessionsForRecordingDeletion(GetOTFSessionsForRecordingDeletionReq req)
            throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/getSessionsForRecordingDeletion?"
                + WebUtils.INSTANCE.createQueryStringOfObject(req);

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        // Parse the response
        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        if (!StringUtils.isEmpty(jsonString)) {
            List<OTFSessionPojoUtils> results = gson.fromJson(jsonString, otfSessionListType);
            if (!CollectionUtils.isEmpty(results)) {
                sessions.addAll(results);
            }
        }

        populatePresenterInfos(sessions, false);
        populateAttendeeInfos(sessions, false);
        return sessions;
    }

    private void populatePresenterInfos(List<OTFSessionPojoUtils> sessions, boolean exposeEmail) {
        if (!CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                userIds.add(session.getPresenter());
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojoUtils session : sessions) {
                if (userMap.containsKey(session.getPresenter())) {
                    session.setPresenterInfo(userMap.get(session.getPresenter()));
                }
            }
        }
    }

    private void populateAttendeeInfos(List<OTFSessionPojoUtils> sessions, boolean exposeEmail) {
        if (!CollectionUtils.isEmpty(sessions)) {
            Set<String> userIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                if (!CollectionUtils.isEmpty(session.getAttendees())) {
                    userIds.addAll(session.getAttendees());
                }
                if (!CollectionUtils.isEmpty(session.getAttendeeInfos())) {
                    Set<String> allAttendees = session.getAttendeeInfos().stream()
                            .map(OTFSessionAttendeeInfo::getUserId).filter(Objects::nonNull).map(String::valueOf)
                            .collect(Collectors.toSet());
                    if (allAttendees != null) {
                        userIds.addAll(allAttendees);
                    }
                }
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (OTFSessionPojoUtils session : sessions) {
                if (!CollectionUtils.isEmpty(session.getAttendees())) {
                    for (String studentId : session.getAttendees()) {
                        if (userMap.containsKey(studentId)) {
                            session.updateUserBasicInfo(userMap.get(studentId));
                        }
                    }
                }
            }
        }
    }


    public void removeNonStudentStudentAttendeeInfos(OTFSessionPojoUtils session, Map<String, UserBasicInfo> userMap) {

        int i = 0;

        while (i < session.getAttendeeInfos().size()) {

            if (userMap.containsKey(session.getAttendeeInfos().get(i).getUserId().toString())) {
                UserBasicInfo userBasicInfo = userMap.get(session.getAttendeeInfos().get(i).getUserId().toString());

                if (!Role.STUDENT.equals(userBasicInfo.getRole())) {
                    session.getAttendeeInfos().remove(i);
                    continue;
                } else {
                    i++;
                }

            } else {
                i++;
            }

        }

    }

    public List<OTFSessionPojoUtils> getSessions(String beforeStartTime, String afterStartTime, String afterEndTime,
                                                 Long userId, Role userRole) throws VException {
        String getSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session";
        if (!StringUtils.isEmpty(afterStartTime)) {
            getSessionsUrl += "?afterStartTime=" + afterStartTime + "&beforeStartTime=" + beforeStartTime;
        } else if (!StringUtils.isEmpty(afterEndTime)) {
            getSessionsUrl += "?afterEndTime=" + afterEndTime;
        } else {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR,
                    "after or before starttime or endtime required for fetching the results");
        }

        // Add session user data
        if (Role.STUDENT.equals(userRole)) {
            getSessionsUrl += "&studentId=" + userId + "&role=" + Role.STUDENT.name();
        } else if (Role.TEACHER.equals(userRole)) {
            getSessionsUrl += "&teacherId=" + userId + "&role=" + Role.TEACHER.name();
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    public List<OTFSessionPojoUtils> getSessionsForFeedback(long afterEndTime, long beforeEndTime) throws VException {
        logger.info("Request : " + afterEndTime + " beforeEndTime : " + beforeEndTime);
        String getSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/feedback/getSessions?afterEndTime=" + afterEndTime
                + "&beforeEndTime=" + beforeEndTime;

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("json:" + jsonString);
        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        if (!StringUtils.isEmpty(jsonString)) {
            sessions = gson.fromJson(jsonString, otfSessionListType);
        }
        return sessions;
    }

    @Deprecated
    private List<OTFCalendarSessionInfo> populateUsersAndBoards(List<OTFSessionPojoUtils> sessions) {
        List<OTFCalendarSessionInfo> calendarSessionInfos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(sessions)) {
            Set<Long> boardIds = new HashSet<>();
            Set<Long> userIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                if (session.getBoardId() != null) {
                    boardIds.add(session.getBoardId());
                } else {
                    if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                        for (OTFCourseBasicInfo courseBasicInfo : session.getCourseInfos()) {
                            List<String> subjects = courseBasicInfo.getSubjects();
                            if (!CollectionUtils.isEmpty(subjects)) {
                                boardIds.add(Long.parseLong(subjects.get(0)));
                            }
                        }
                    }
                }

                if (!StringUtils.isEmpty(session.getPresenter())) {
                    userIds.add(Long.parseLong(session.getPresenter()));
                }
//				if (!CollectionUtils.isEmpty(session.getAttendees())) {
//					for (String studentId : session.getAttendees()) {
//						userIds.add(Long.parseLong(studentId));
//					}
//				}
            }

            // Fetch boards and users
            Map<Long, Board> boards = fosUtils.getBoardInfoMap(boardIds);
            Map<Long, User> users = fosUtils.getUserInfosMap(userIds, false);

            for (OTFSessionPojoUtils session : sessions) {
                OTFCalendarSessionInfo calendarSessionInfo = new OTFCalendarSessionInfo(session);
                if (session.getBoardId() != null && session.getBoardId() > 0L) {
                    Board board = boards.get(session.getBoardId());
                    if (board != null) {
                        calendarSessionInfo.setSubject(board.getName());
                    }
                } else {

                    if (ArrayUtils.isNotEmpty(session.getCourseInfos())) {
                        for (OTFCourseBasicInfo courseBasicInfo : session.getCourseInfos()) {
                            List<String> subjects = courseBasicInfo.getSubjects();
                            if (!CollectionUtils.isEmpty(subjects)) {
                                Board board = boards.get(Long.parseLong(subjects.get(0)));
                                if (board != null) {
                                    calendarSessionInfo.setSubject(board.getName());
                                }
                            }
                        }
                    }
                }

                List<UserSessionInfo> attendees = new ArrayList<>();
                if (!StringUtils.isEmpty(session.getPresenter())) {
                    User user = users.get(Long.parseLong(session.getPresenter()));
                    if (user != null) {
                        attendees.add(new UserSessionInfo(user));
                    }
                }
                if (!CollectionUtils.isEmpty(session.getAttendees())) {
                    for (String studentId : session.getAttendees()) {
                        User user = users.get(Long.parseLong(studentId));
                        if (user != null) {
                            attendees.add(new UserSessionInfo(user));
                        }
                    }
                }
                if (ArrayUtils.isNotEmpty(session.getGroupNames())) {
                    calendarSessionInfo.setGroupNames(session.getGroupNames());
                }
                calendarSessionInfo.setAttendees(attendees);
                calendarSessionInfo.setId(calendarSessionInfo.getOtfSessionId());
                calendarSessionInfos.add(calendarSessionInfo);

            }
        }

        return calendarSessionInfos;
    }

    // Need to be triggered every 1 hour
    public void otfFeedbackScheduler() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("sendFeedbackSessionEmails starting at time : " + currentTime);
        Long timeWindow = ConfigUtils.INSTANCE.getLongValue("otf.scheduler.cron.session.feedback.timewindow");
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long startTime = currentTime - timeWindow - DateTimeUtils.MILLIS_PER_HOUR;
        Long endTime = currentTime - timeWindow;
        logger.info("sendFeedbackSessionEmails cronRunTime " + cronRunTime + " timeWindow " + timeWindow);
        handleFeedbacks(startTime, endTime);
    }

    public void handleFeedbacks(long startTime, long endTime) throws VException {
        logger.info("sendFeedbackSessionEmails startTime" + startTime + " endTime" + endTime);
        try {
            updateAttendeeReport(startTime, endTime);
        } catch (VException e) {
            logger.info("Error in updating attendee report(added to check if errors other than gateway " +
                    "timeout are coming or not)", e);
        }

        List<OTFSessionPojoUtils> sessions = getSessionsForFeedback(startTime, endTime);
        UserBasicInfo user;
        List<String> studentIds;
        if (!CollectionUtils.isEmpty(sessions)) {
            logger.info("sendFeedbackSessionEmails sessions" + sessions.size());
            List<String> sessionIds = new ArrayList<>();
            for (OTFSessionPojoUtils session : sessions) {
                if (session.containsFlag(OTFSessionFlag.OTF_FEEDBACK_HANDLED)) {
                    logger.info("Feedback already processed for session id:" + session.getId());
                    continue;
                }

                studentIds = session.getAttendees();

                redisDAO.increment(SessionManager.getCacheKey(),
                        studentIds.size() * (session.getEndTime() - session.getStartTime()));
                sessionIds.add(session.getId());
            }

            // Update the session with the feedback
            if (!CollectionUtils.isEmpty(sessionIds)) {
                UpdateOTFSessionFlag updateOTFSessionFlag = new UpdateOTFSessionFlag();
                updateOTFSessionFlag.setSessionIds(sessionIds);
                updateOTFSessionFlag.addFlag(OTFSessionFlag.OTF_FEEDBACK_HANDLED);
                try {
                    updateSessionFlags(updateOTFSessionFlag);
                } catch (Exception ex) {
                    logger.error("Error updating the feedback processed flag for req :" + updateOTFSessionFlag.toString());
                }
            }
        }
    }

    public void updateSessionFlags(UpdateOTFSessionFlag updateOTFSessionFlag) throws VException {
        logger.info("Request:" + updateOTFSessionFlag.toString());
        String updateSessionFlagsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/updateflag";
        ClientResponse resp = WebUtils.INSTANCE.doCall(updateSessionFlagsUrl, HttpMethod.POST,
                gson.toJson(updateOTFSessionFlag));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
    }

    public void sendDailySessionEmail() throws VException {
        Long startTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis());
        Integer createSessionLinkRetryCount = ConfigUtils.INSTANCE.getIntValue("otf.scheduler.cron.session.link.retry");
        if (createSessionLinkRetryCount < 0) {
            createSessionLinkRetryCount = 3;
        }

        Integer fetchUpcomingSessionsRetryCount = 3;
        List<OTFSessionPojoUtils> sessions = new ArrayList<>();
        List<Conflict> conflicts = new ArrayList<>();

        logger.info("sendDailySessionEmail : " + startTime);
        Long endTime = startTime + DateTimeUtils.MILLIS_PER_DAY + DateTimeUtils.MILLIS_PER_HOUR;

        GetOTFWebinarSessionsReq getOTFWebinarSessionsReq = new GetOTFWebinarSessionsReq();
        getOTFWebinarSessionsReq.setAfterStartTime(startTime);
        getOTFWebinarSessionsReq.setBeforeStartTime(endTime);
        getOTFWebinarSessionsReq.addSessionStates(SessionState.SCHEDULED);
        awsSQSManager.sendToSQS(SQSQueue.OTF_SESSION_QUEUE, SQSMessageType.CREATE_LINK, gson.toJson(getOTFWebinarSessionsReq));
        while (fetchUpcomingSessionsRetryCount > 0) {
            Map<String, BitSet> scheduleMap = new HashMap<>();
            sessions = getSessions(endTime.toString(), startTime.toString(), null, null, null);


            List<OTFSessionPojoUtils> webinarSessions = getBundleSessions(getOTFWebinarSessionsReq);
            if (!CollectionUtils.isEmpty(webinarSessions)) {
                sessions.addAll(webinarSessions);
            }

            if (sessions != null) {
                long conflictCheckReferenceTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis());
                List<String> conflictErrors = new ArrayList<>();
                for (OTFSessionPojoUtils session : sessions) {

                    if (session.getStartTime() < System.currentTimeMillis()) {
                        logger.info("Session timing is in past id:" + session.getId());
                        continue;
                    }

                    if (session.hasVedantuWaveFeatures()) {
                        continue;
                    }

                    if (!StringUtils.isEmpty(session.getOrganizerAccessToken())) {
                        // Check conflicts
                        checkConflict(scheduleMap, session, conflictCheckReferenceTime, conflictErrors);
                    }

                    if (!StringUtils.isEmpty(session.getSessionURL())) {
                        logger.info("sendDailySessionEmail LinkAlreadyExists for session with id : " + session.getId()
                                + "URL : " + session.getSessionURL());
                        continue;
                    }


                    String sessionId = session.getId();
//					int count = createSessionLinkRetryCount;
//					SessionPojo temp = null;
//					while (count > 0) {
//						temp = gson.fromJson(createSessionLinks(sessionId), SessionPojo.class);
//						;
//						if (temp != null && !org.apache.commons.lang3.StringUtils.isEmpty(temp.getSessionURL())) {
//							session.setSessionURL(temp.getSessionURL());
//							break;
//						}
//						count--;
//					}
//
//					if (temp == null || StringUtils.isEmpty(temp.getSessionURL())) {
//						logger.error("LinkCreationFail - Failure in creation of link for session with id " + sessionId);
//					} else {
//						logger.info("LinkCreationSuccess - Creation of link for session with id : " + temp.getId()
//								+ " is successful. Link : " + temp.getSessionURL());
//					}
                }

                if (!CollectionUtils.isEmpty(conflictErrors)) {
                    logger.error("Found conflicts in today's sessions : " + Arrays.toString(conflictErrors.toArray()));
                }
                break;
            } else {
                logger.warn("Sessions returned are null, so retrying again");
            }
            fetchUpcomingSessionsRetryCount--;
        }


        // TODO : Compose Email for all the sessions
        try {
            communicationManager.sendOTFDailySessionsEmail(sessions, conflicts);
        } catch (Exception ex) {
            logger.error("sendDailySessionEmail", ex);
        }

        try {
            sendPreviousDayRecordingsEmail();
        } catch (Exception ex) {
            logger.info("sendRecordingEmail + " + ex.getMessage(), ex);
        }
    }

    public void performSanityCheckForFutureSessionsAsync() {
        logger.info("Invoking daily session task");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_SANITY_CHECK_TASK, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    // Considers next 3 days
    public void performSanityCheckForFutureSessions() throws VException {
        OTFSessionSanityCheckResponse response = new OTFSessionSanityCheckResponse();
        Long startTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis()) + DateTimeUtils.MILLIS_PER_DAY;
        Long endTime = startTime + 3 * DateTimeUtils.MILLIS_PER_DAY;
        for (long time = startTime; time < endTime; time += DateTimeUtils.MILLIS_PER_DAY) {
            OTFSessionSanityCheckResponse entry = performSanityCheckForFutureSessions(time,
                    time + DateTimeUtils.MILLIS_PER_DAY);
            logger.info("Result for time: " + time + " response:" + entry.toString());
            response.addNewEntry(entry);
        }

        if (response.containsErrors()) {
            logger.error("SantiyErrors - " + response.toString());
        }
    }

    public OTFSessionSanityCheckResponse performSanityCheckForFutureSessions(Long startTime, Long endTime)
            throws VException {
        // Fetch the sessions for the given intervals
        List<OTFSessionPojoUtils> sessions = getSessions(endTime.toString(), startTime.toString(), null, null, null);
        GetOTFWebinarSessionsReq getOTFWebinarSessionsReq = new GetOTFWebinarSessionsReq();
        getOTFWebinarSessionsReq.setAfterEndTime(startTime);
        getOTFWebinarSessionsReq.setBeforeEndTime(endTime);
        getOTFWebinarSessionsReq.addSessionStates(SessionState.SCHEDULED);
        List<OTFSessionPojoUtils> webinarSessions = getBundleSessions(getOTFWebinarSessionsReq);
        if (!CollectionUtils.isEmpty(webinarSessions)) {
            sessions.addAll(webinarSessions);
        }

        OTFSessionSanityCheckResponse response = new OTFSessionSanityCheckResponse();
        if (CollectionUtils.isEmpty(sessions)) {
            return response;
        }

        // Collect sessions where organizer token is missing
        Set<String> tokenMissingSet = sessions.stream().filter(s -> StringUtils.isEmpty(s.getOrganizerAccessToken()) &&
                s.hasVedantuWaveFeatures())
                .map(OTFSessionPojoUtils::getId).collect(Collectors.toSet());
        logger.info("MissingTokens : " + (tokenMissingSet == null ? "" : Arrays.toString(tokenMissingSet.toArray())));
        response.addTokenMissingsessionIds(tokenMissingSet);

        // Check Token conflicts and collect errors
        List<String> conflictErrors = checkOrganizerConflicts(sessions);
        logger.info("ConflictErrors : " + (conflictErrors == null ? "" : Arrays.toString(tokenMissingSet.toArray())));
        response.addConflictCheckErrors(conflictErrors);

        return response;
    }

    // Returns the conflict session ids list
    public List<String> checkOrganizerConflicts(List<OTFSessionPojoUtils> sessionPojo) {
        long conflictCheckReferenceTime = CommonCalendarUtils.getDayStartTime_IST(System.currentTimeMillis());
        List<String> conflictErrors = new ArrayList<>();
        if (!CollectionUtils.isEmpty(sessionPojo)) {
            Map<String, BitSet> scheduleMap = new HashMap<>();
            for (OTFSessionPojoUtils session : sessionPojo) {
                if (!session.hasVedantuWaveFeatures()) {
                    checkConflict(scheduleMap, session, conflictCheckReferenceTime, conflictErrors);
                }
            }
        }

        return conflictErrors;
    }

    private void checkConflict(Map<String, BitSet> scheduleMap, OTFSessionPojoUtils session, long referenceTime,
                               List<String> conflictErrors) {
        Long startTime = session.getStartTime();
        Long endTime = session.getEndTime();
        BitSet bitSet = new BitSet();
        if (scheduleMap.containsKey(session.getOrganizerAccessToken())) {
            bitSet = scheduleMap.get(session.getOrganizerAccessToken());
        }

        // Add the schedule times
        int startIndex = CommonCalendarUtils.getSlotStartIndex(startTime, referenceTime);
        int endIndex = CommonCalendarUtils.getSlotEndIndex(endTime, referenceTime);
        if (bitSet.nextSetBit(startIndex) > 0 && bitSet.nextSetBit(startIndex) <= endIndex) {
            logger.info("OTFSessionConflict observed for organizer token:" + session.getOrganizerAccessToken()
                    + " sessionId:" + session.getId() + " session start time:" + session.getStartTime() + " end time:"
                    + session.getEndTime());
            conflictErrors.add(session.getId());
        }

        // End index is exclusive. For 4PM to 5PM session, end index points to 4:45.
        // Hence we need to use endIndex+1;
        bitSet.set(startIndex, endIndex + 1);
        scheduleMap.put(session.getOrganizerAccessToken(), bitSet);
    }

    @Deprecated
    public String createSessionLinks(String id) throws VException {
        logger.info("Request:" + id);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/onetofew/session/createLink/" + id, HttpMethod.POST,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    public void otfSchedulerAsync() throws VException {
        logger.info("Invoking daily session task");
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_DAILY_SCHEDULER_TASK, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void otfScheduler() throws VException {
        try {
            sendSessionEmails();
        } catch (Exception ex) {
            logger.error("sendSessionEmails + " + ex.getMessage(), ex);
        }

        try {
            sendTeacherJoinFailureSMS();
        } catch (Exception ex) {
            logger.error("sendTeacherJoinFailureSMS + " + ex.getMessage(), ex);
        }
    }

    public void sendPreviousDayRecordingsEmail() throws VException, UnsupportedEncodingException {
        long currentTime = System.currentTimeMillis();
        currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;

        updateSessionReplayUrls(currentTime - DateTimeUtils.MILLIS_PER_DAY, currentTime);

        List<OTFSessionPojoUtils> results = getUpcomingSessionsWithAttendeeInfos(currentTime - DateTimeUtils.MILLIS_PER_DAY,
                currentTime, 0, 500, null);
        if (ArrayUtils.isNotEmpty(results) && results.size() >= 500) {
            logger.error("more than 500 sizes available for sending previous day recording email");
        }
        communicationManager.sendOTFPreviousDayRecordingsEmail(results);
    }

    // Need to be executed every 1 hour and processes sessions for past 4 hours
    public void otfRecordingSchedulerAsync() throws VException {
        logger.info("Invoking daily session task");
        Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_RECORDING_SCHEDULER_TASK, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void otfRecordingScheduler() throws VException {
        try {
            long currentTime = System.currentTimeMillis();
            currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            if (UPDATE_RECORDING_TIME == null) {
                UPDATE_RECORDING_TIME = DEFAULT_UPDATE_RECORDING_TIME;
            }
            updateRecordings(currentTime - UPDATE_RECORDING_TIME - DateTimeUtils.MILLIS_PER_HOUR,
                    currentTime - DateTimeUtils.MILLIS_PER_HOUR);

            // Update session replay urls
            updateSessionReplayUrls(currentTime - UPDATE_RECORDING_TIME - 2 * DateTimeUtils.MILLIS_PER_HOUR,
                    currentTime - 2 * DateTimeUtils.MILLIS_PER_HOUR);

            // Migrate session data
            sendSessionDelayedEvent(currentTime - 3 * DateTimeUtils.MILLIS_PER_HOUR, currentTime - 2 * DateTimeUtils.MILLIS_PER_HOUR);

        } catch (Exception ex) {
            logger.error("Error updating recordings:" + ex.getMessage());
        }

    }

    // Need to be executed every 1 hour and processes sessions for past 4 hours
    public String updateAttendeeReport(Long afterEndTime, Long beforeEndTime) throws VException {
        logger.info("Request startTime:" + afterEndTime + " endTime:" + beforeEndTime);
        String updateAttendeeUrl = SCHEDULING_ENDPOINT + "/onetofew/session/updateAttendeeReport?afterEndTime=" + afterEndTime
                + "&beforeEndTime=" + beforeEndTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(updateAttendeeUrl, HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    public void sendSessionNotifications() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("sendSessionNotifications starting at time : " + currentTime);
        Long timeWindow = ConfigUtils.INSTANCE.getLongValue("otf.scheduler.cron.session.notification.timewindow");
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;

        Long startTime = currentTime + timeWindow;
        Long endTime = currentTime + timeWindow + cronRunTime;
        List<OTFSessionPojoUtils> sessions = getSessions(endTime.toString(), startTime.toString(), null, null, null);

        UserBasicInfo user;
        List<String> studentIds;
        if (sessions != null) {

            for (OTFSessionPojoUtils session : sessions) {
                List<String> userIdsList = new ArrayList<>();
                if (!StringUtils.isEmpty(session.getPresenter())) {
                    userIdsList.add(session.getPresenter());
                } else {
                    continue;
                }

                studentIds = session.getAttendees();
                if (studentIds != null) {
                    userIdsList.addAll(studentIds);
                } else {
                    continue;
                }

                Map<String, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMap(userIdsList, true);

                UserBasicInfo teacher = userInfoMap.get(session.getPresenter());
                if (teacher == null || !Role.TEACHER.equals(teacher.getRole())) {
                    continue;
                }

                if (!StringUtils.isEmpty(session.getId())) {

                    CreateNotificationRequest request = new CreateNotificationRequest(teacher.getUserId(),
                            NotificationType.OTF_SESSION, new Gson().toJson(session), teacher.getRole());
                    notificationManager.createNotification(request);

                }

                for (String studentId : studentIds) {
                    user = userInfoMap.get(studentId);
                    if (user == null || !Role.STUDENT.equals(user.getRole())) {
                        logger.info("sendSessionNotificationsStudentError",
                                "no student found for student id" + studentId);
                        continue;
                    }
                    CreateNotificationRequest request = new CreateNotificationRequest(teacher.getUserId(),
                            NotificationType.OTF_SESSION, new Gson().toJson(session), teacher.getRole());
                    notificationManager.createNotification(request);

                }
            }
        }
    }

    public void sendSessionEmails() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("sendSessionEmails starting at time : " + currentTime);
        Long timeWindow = ConfigUtils.INSTANCE.getLongValue("otf.scheduler.cron.session.email.timewindow");
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long startTime = currentTime + timeWindow;
        Long endTime = currentTime + timeWindow + cronRunTime;
        List<OTFSessionPojoUtils> sessions = getSessions(endTime.toString(), startTime.toString(), null, null, null);
        UserBasicInfo user;
        List<String> studentIds;
        if (sessions != null) {

            for (OTFSessionPojoUtils session : sessions) {
                List<String> userIdsList = new ArrayList<>();
                if (!StringUtils.isEmpty(session.getPresenter())) {
                    userIdsList.add(session.getPresenter());
                } else {
                    continue;
                }

                studentIds = session.getAttendees();
                if (studentIds != null) {
                    userIdsList.addAll(studentIds);
                } else {
                    continue;
                }

                Map<String, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMap(userIdsList, true);

                UserBasicInfo teacher = userInfoMap.get(session.getPresenter());
                if (teacher == null || !Role.TEACHER.equals(teacher.getRole())) {
                    continue;
                }

                try {
                    communicationManager.sendOTFSessionReminder(null, teacher, session);
                } catch (Exception ex) {
                    logger.error(ex);
                }

                for (String studentId : studentIds) {
                    user = userInfoMap.get(studentId);
                    if (user == null || !Role.STUDENT.equals(user.getRole())) {
                        logger.info("sendSessionEmails", "no student found for student id" + studentId);
                        continue;
                    }
                    try {
                        communicationManager.sendOTFSessionReminder(user, teacher, session);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }

                }

            }
        }
    }

    public void sendTeacherJoinFailureSMS() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("sendSessionEmails starting at time : " + currentTime);
        Long timeWindow = ConfigUtils.INSTANCE.getLongValue("otf.scheduler.cron.teacher.join.failure.timewindow");
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;

        Long startTime = currentTime - timeWindow - cronRunTime;
        Long endTime = currentTime - timeWindow;
        List<OTFSessionPojoUtils> sessions = getSessions(endTime.toString(), startTime.toString(), null, null, null);

        if (sessions != null) {
            for (OTFSessionPojoUtils session : sessions) {
                if (!Boolean.TRUE.equals(session.getTeacherJoined())) {
                    // Send Email to otf@vedantu.com and sms to 9886576953
                    try {
                        communicationManager.sendOTFTeacherNotJoined(session);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            }
        }
    }

    public Long getEndedSessionDuration(Long afterEndTime, Long beforeEndTime) throws VException {
        String getEndedSessionDurationUrl = SCHEDULING_ENDPOINT + "/onetofew/session/endedSessionDuration";
        if (afterEndTime != null) {
            getEndedSessionDurationUrl += "?afterEndTime=" + afterEndTime + "&beforeEndTime=" + beforeEndTime;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(getEndedSessionDurationUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return Long.parseLong(jsonString);
    }

    public CourseInfo getCourse(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/course/" + id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        CourseInfo courseInfo = gson.fromJson(jsonString, CourseInfo.class);
        return courseInfo;
    }

    public List<CourseInfo> getCoursesBasicInfos(List<String> courseIds)
            throws VException, JSONException, IllegalArgumentException, IllegalAccessException {
        String ids = "";
        for (String id : courseIds) {
            ids += "ids=" + id + "&";
        }
        String getCoursesUrl = SUBSCRIPTION_ENDPOINT + "/course/getCoursesBasicInfos" + "?" + ids;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getCoursesUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<CourseInfo>>() {
        }.getType();
        List<CourseInfo> courses = new Gson().fromJson(jsonString, listType);
        return courses;
    }

    public List<GTTAttendeeDetailsInfo> getJoinedAttendeeDetailsOfUser(String userId, Integer start, Integer size)
            throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/getJoinedAttendeeDetailsOfUser?userId=" + userId + "&start=" + start
                + "&size=" + size;

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type listType = new TypeToken<List<GTTAttendeeDetailsInfo>>() {
        }.getType();

        List<GTTAttendeeDetailsInfo> details = gson.fromJson(jsonString, listType);
        return details;
    }

    public void otfTriggerSessionEnd() throws VException {
        Long currentTime = System.currentTimeMillis();
        logger.info("otfTriggerSessionEnd starting at time : " + currentTime);
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long afterEndTime = currentTime - cronRunTime;
        Long beforeEndTime = currentTime;
        logger.info("otfTriggerSessionEnd afterEndTime " + afterEndTime + " beforeEndTime " + beforeEndTime);
        String getSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getSessionsBasicInfos?" + "afterEndTime=" + afterEndTime
                + "&beforeEndTime=" + beforeEndTime;

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        if (ArrayUtils.isNotEmpty(sessions)) {
            Set<String> studentIds = new HashSet<>();
            for (OTFSessionPojoUtils session : sessions) {
                if (ArrayUtils.isNotEmpty(session.getAttendees())) {
                    studentIds.addAll(session.getAttendees());
                }
            }
            for (String studentId : studentIds) {
                if (!StringUtils.isEmpty(studentId)) {
                    referralManager.processReferralBonusAsync(ReferralStep.FIRST_SESSION, Long.parseLong(studentId));
                }
            }
        }
    }

    public GetSessionPartnersRes getOTFSessionPartners(GetSessionPartnersReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String url = SUBSCRIPTION_ENDPOINT + "/batch/getSessionPartners?" + WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        GetSessionPartnersRes response = gson.fromJson(jsonString, GetSessionPartnersRes.class);
        return response;

    }

    @Deprecated
    public PlatformBasicResponse updateRecordings(Long afterEndTime, Long beforeEndTime) throws VException {
        String getSessionsForHandlingRecordingsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/handleSesssionRecordingsBulk?afterEndTime=" + afterEndTime + "&beforeEndTime="
                + beforeEndTime;
        logger.info("url : " + getSessionsForHandlingRecordingsUrl);

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsForHandlingRecordingsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Sessions:" + jsonString);

        if (!StringUtils.isEmpty(jsonString)) {
            List<HandleOTFRecordingLambda> lambdaRequests = gson.fromJson(jsonString,
                    HANDLE_RECORDING_LAMBDA_LIST_TYPE);
            if (!CollectionUtils.isEmpty(lambdaRequests)) {
                List<String> errors = new ArrayList<>();
                for (HandleOTFRecordingLambda request : lambdaRequests) {
                    try {
                        sendRecordingRequestToSQS(request);
//						awsLambdaManager.invokeOTFRecordingLambda(request);
                    } catch (Exception ex) {
                        String errorMessage = "Error invoking ex:" + ex.toString() + " message:" + ex.getMessage()
                                + " request:" + request.toString();
                        logger.info(errorMessage);
                        errors.add(errorMessage);
                    }
                }

                if (!CollectionUtils.isEmpty(errors)) {
                    logger.info("RecordingLambdaErrors:" + Arrays.toString(errors.toArray()));
                }
            }
        }

        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setResponse(jsonString);
        return response;
    }

    @Deprecated
    public PlatformBasicResponse updateSessionReplayUrls(Long afterEndTime, Long beforeEndTime) throws VException {
        String getSessionsForHandlingRecordingsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/updateSessionReplayUrls?afterEndTime="
                + afterEndTime + "&beforeEndTime=" + beforeEndTime;
        logger.info("url : " + getSessionsForHandlingRecordingsUrl);

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsForHandlingRecordingsUrl, HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Sessions:" + jsonString);
        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setResponse(jsonString);
        return response;
    }

    @Deprecated
    public void handleSessionRecordings(String sessionId) {
        try {
            String handleSessionRecordingUrl = SCHEDULING_ENDPOINT + "/onetofew/session/handleSesssionRecordings?sessionId="
                    + sessionId;
            logger.info("url : " + handleSessionRecordingUrl);

            ClientResponse resp = WebUtils.INSTANCE.doCall(handleSessionRecordingUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String handleRecordingResponse = resp.getEntity(String.class);
            logger.info("handle session response for id : " + sessionId + " response:" + handleRecordingResponse);

            HandleOTFRecordingLambda handleOTFRecordingLambda = gson.fromJson(handleRecordingResponse,
                    HandleOTFRecordingLambda.class);
            if (!CollectionUtils.isEmpty(handleOTFRecordingLambda.getRecordings())) {
                sendRecordingRequestToSQS(handleOTFRecordingLambda);
//				awsLambdaManager.invokeOTFRecordingLambda(handleOTFRecordingLambda);
            }
        } catch (Exception ex) {
            logger.info("Exception:" + ex.getMessage() + " session:" + sessionId);
        }
    }

    public void sendSessionDelayedEvent(Long afterEndTime, Long beforeEndTime) throws VException, JSONException {
//		Long currentTime = System.currentTimeMillis();
//		long snsDelay = DateTimeUtils.MILLIS_PER_MINUTE * 15;
//		logger.info("otfSendSessionDelayedEvent starting at time : " + currentTime);
//		currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
//		Long afterEndTime = currentTime - cronRunTime - snsDelay - 1 * DateTimeUtils.MILLIS_PER_MINUTE;
//		Long beforeEndTime = currentTime - snsDelay;
        logger.info("otfSendSessionDelayedEvent afterEndTime " + afterEndTime + " beforeEndTime " + beforeEndTime);
        String getSessionsUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getSessionsBasicInfos?" + "afterEndTime=" + afterEndTime
                + "&beforeEndTime=" + beforeEndTime;

        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSessionPojoUtils session : sessions) {
                triggerSessionEndedSNS(session.getId());
            }
        }
    }

    private void triggerSessionEndedSNS(String sessionId) throws JSONException {
        JSONObject sessionDetails = new JSONObject();
        sessionDetails.put("sessionId", sessionId);
        sessionDetails.put("otf", true);
        // Long delay=Long.valueOf(15*DateTimeUtils.MILLIS_PER_MINUTE);
        awsSNSManager.triggerSNS(SNSTopic.SESSION_DELAYED_EVENTS, "SESSION_ENDED", sessionDetails.toString());
    }

// Not deleting recording in platform now
//	public void deleteRecordingsDailyAsync() throws VException {
//		logger.info("Invoking daily session task");
//		Map<String, Object> payload = new HashMap<>();
//		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_DELETE_RECORDING_CRON, payload);
//		asyncTaskFactory.executeTask(params);
//	}

//	public void deleteRecordingsDaily() throws VException {
//		long currentTime = System.currentTimeMillis();
//		long dayStartTime = CommonCalendarUtils.getDayStartTime_IST(currentTime);
//		long startTime = dayStartTime - 4 * DateTimeUtils.MILLIS_PER_DAY;
//		long endTime = dayStartTime - DateTimeUtils.MILLIS_PER_DAY;
//		for (long time = startTime; time < endTime; time += DateTimeUtils.MILLIS_PER_DAY) {
//			deleteRecordings(time, time + DateTimeUtils.MILLIS_PER_DAY);
//			logger.info("Delete recordings respons for start time:" + time);
//		}
//	}

//	public void deleteRecordings(long startTime, long endTime) throws VException {
//
//		List<String> errors = new ArrayList<>();
//		int batchStart = 0;
//		final int BATCH_PROCESSING_SIZE = AbstractMongoDAO.MAX_ALLOWED_FETCH_SIZE;
//
//		long taskCounter = 0;
//		final int TASK_BATCH_SIZE = Constants.DEFAULT_ASYNC_BATCH_SIZE;
//		final long TASK_DELAY_INCREMENT_DURATION = 5 * DateTimeUtils.MILLIS_PER_MINUTE;
//		while (true) {
//			GetOTFSessionsForRecordingDeletionReq req = new GetOTFSessionsForRecordingDeletionReq(startTime, endTime);
//			req.setStart(batchStart);
//			req.setSize(BATCH_PROCESSING_SIZE);
//			List<SessionPojo> sessions = getSessionsForRecordingDeletion(req);
//			if (CollectionUtils.isEmpty(sessions)) {
//				break;
//			}
//
//			for (SessionPojo session : sessions) {
//				if (session.getEndTime() > System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY) {
//					logger.info(
//							"Skipping the delete recording option as it is too early. Upload might still be going on");
//					continue;
//				}
//
//				logger.info("Session id : " + session.getId());
//				// Trigger SNS event to delete the recording
//				if (!CollectionUtils.isEmpty(session.getReplayUrls())) {
//					logger.info(" ReplayUrls:" + Arrays.toString(session.getReplayUrls().toArray()) + " for session id:"
//							+ session.getId());
//					for (String recordingId : session.getReplayUrls()) {
//						logger.info("Processing recording id : " + recordingId);
//						try {
//							if (StringUtils.isEmpty(session.getOrganizerAccessToken())) {
//								// Without organizer token we cannot delete the recording
//								logger.info("No orgainzer token for session id : " + session.getId());
//								continue;
//							}
//
//							String credentials = ConfigUtils.INSTANCE
//									.getStringValue("gtt.account.credentials." + session.getOrganizerAccessToken());
//							if (StringUtils.isEmpty(credentials)) {
//								errors.add("Credentials missing for session id : " + session.getId());
//								continue;
//							}
//
//							if (awsS3Manager.checkRecordingExists(session.getId() + "-" + recordingId)) {
//								String[] credentialArr = credentials.split("-");
//								JSONObject messageObject = new JSONObject();
//								messageObject.put("email", credentialArr[0]);
//								messageObject.put("password", credentialArr[1]);
//								messageObject.put("recordingId", recordingId);
//
//								awsSNSManager.triggerAsyncSNS(SNSTopic.DELETE_GTT_RECORDING, "DELETE_GTT_RECORDING",
//										messageObject.toString(),
//										TASK_DELAY_INCREMENT_DURATION * (taskCounter / TASK_BATCH_SIZE));
//								taskCounter++;
//							}
//						} catch (Exception ex) {
//							logger.info("Error : " + ex.toString() + " message:" + ex.getMessage());
//						}
//					}
//				}
//			}
//
//			// If session size is less than batch size, exit
//			if (sessions.size() < BATCH_PROCESSING_SIZE) {
//				break;
//			}
//
//			batchStart += BATCH_PROCESSING_SIZE;
//		}
//
//		if (!CollectionUtils.isEmpty(errors)) {
//			logger.error("DeleteRecordingProcessingErrors - " + Arrays.toString(errors.toArray()));
//		}
//	}

    @Deprecated
    public void invokeSessionRecordingLambda(HandleOTFRecordingLambda handleOTFRecordingLambda) throws VException {
//		awsLambdaManager.invokeOTFRecordingLambda(handleOTFRecordingLambda);
        sendRecordingRequestToSQS(handleOTFRecordingLambda);
    }

    public List<SessionPojoWithRefId> addWebinarSessions(OTFWebinarSessionReq req) throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/webinar/updateBulk";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<SessionPojoWithRefId>>() {
        }.getType();

        List<SessionPojoWithRefId> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    public List<OTFSessionPojoUtils> cancelWebinarSession(CancelOTFWebinarSessionsReq cancelOTFWebinarSessionsReq)
            throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/webinar/cancel";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST,
                new Gson().toJson(cancelOTFWebinarSessionsReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    public List<OTFSessionPojoUtils> attendWebinarSession(UpdateWebinarSessionsAttendenceReq req) throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/webinar/attend";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    public List<OTFSessionPojoUtils> getBundleSessions(GetOTFWebinarSessionsReq req) throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/webinar/get";
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        logger.info("queryString:" + queryString);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }


    public String getStudentAttendenceCountWithTotal(StudentBatchProgressReq req) throws VException {
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/getStudentAttendenceCountWithTotal";
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }

    // TODO Removal
    @Deprecated
    public List<OTFAttendance> getAttendanceForStudentsAndBoard(TeacherBatchProgressReq req) throws VException {
		/*String url = SCHEDULING_ENDPOINT+ "/onetofew/session/getAttendanceForStudentsAndBoard";
		String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(req));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		Type otfattListType = new TypeToken<List<OTFAttendance>>() {
		}.getType();

		List<OTFAttendance> attendance = gson.fromJson(jsonString, otfattListType);
		return attendance;*/
        return new ArrayList<>();
    }

    public List<OTFSessionInfo> getPastSessionsForBatchProgress(TeacherBatchProgressReq req) throws VException {
        req.verify();
        String url = SCHEDULING_ENDPOINT + "/onetofew/session/getPastSessionsForBatchProgress";
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otfattListType = new TypeToken<List<OTFSessionInfo>>() {
        }.getType();

        List<OTFSessionInfo> attendance = gson.fromJson(jsonString, otfattListType);
        Set<String> userIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(attendance)) {
            for (OTFSessionInfo otfSessionInfo : attendance) {
                if (ArrayUtils.isNotEmpty(otfSessionInfo.getAbsentees())) {
                    userIds.addAll(otfSessionInfo.getAbsentees());
                }
                if (ArrayUtils.isNotEmpty(otfSessionInfo.getAttended())) {
                    userIds.addAll(otfSessionInfo.getAttended());
                }
                if (!StringUtils.isEmpty(otfSessionInfo.getPresenter())) {
                    userIds.add(otfSessionInfo.getPresenter());
                }
            }
            Map<String, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMap(userIds, true);
            for (OTFSessionInfo otfSessionInfo : attendance) {
                List<UserBasicInfo> attendedUsers = new ArrayList<>();
                List<String> attended = new ArrayList<>();
                List<String> absentees = new ArrayList<>();
                List<UserBasicInfo> absentUsers = new ArrayList<>();
                int total = 0;
                if (!StringUtils.isEmpty(otfSessionInfo.getPresenter())) {
                    if (usermap.containsKey(otfSessionInfo.getPresenter())) {
                        UserBasicInfo userBasicInfo = usermap.get(otfSessionInfo.getPresenter());
                        otfSessionInfo.setTeacher(userBasicInfo);
                    }
                }
                if (ArrayUtils.isNotEmpty(otfSessionInfo.getAbsentees())) {
                    for (String id : otfSessionInfo.getAbsentees()) {
                        if (usermap.containsKey(id)) {
                            UserBasicInfo userBasicInfo = usermap.get(id);
                            if (userBasicInfo.getRole() != null && userBasicInfo.getRole().equals(Role.STUDENT)) {
                                absentees.add(id);
                                absentUsers.add(usermap.get(id));
                                total++;
                            }
                        }
                    }

                }
                if (ArrayUtils.isNotEmpty(otfSessionInfo.getAttended())) {
                    for (String id : otfSessionInfo.getAttended()) {
                        UserBasicInfo userBasicInfo = usermap.get(id);
                        if (userBasicInfo.getRole() != null && userBasicInfo.getRole().equals(Role.STUDENT)) {
                            attended.add(id);
                            attendedUsers.add(usermap.get(id));
                            total++;
                        }
                    }

                    otfSessionInfo.setAttendance(attended.size());
                }
                otfSessionInfo.setAbsentees(absentees);
                otfSessionInfo.setAttended(attended);
                otfSessionInfo.setTotalAttendance(total);
                otfSessionInfo.setAbsentUsers(absentUsers);
                otfSessionInfo.setAttendedUsers(attendedUsers);
            }
        }
        return attendance;
    }

    public Map<String, String> getCourseTitleForBatchIds(Set<String> batchIds) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/batch/getCourseTitleForBatchIds?batchIds=";
        if (ArrayUtils.isNotEmpty(batchIds)) {
            url += batchIds.iterator().next();
            for (String batchId : batchIds) {
                url += "&batchIds=" + batchId;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type otfMapType = new TypeToken<Map<String, String>>() {
            }.getType();

            Map<String, String> response = gson.fromJson(jsonString, otfMapType);
            return response;
        } else {
            logger.info("batchIds is null.");
            return new HashMap<>();
        }
    }

    public GetBatchesRes getBasicInfosForAllBatchFromBatchId(String batchId) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/batch/getBatchBasicInfosFromCourse";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?batchId=" + batchId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetBatchesRes getBatches = gson.fromJson(jsonString, GetBatchesRes.class);
        return getBatches;
    }

    public GetBatchesRes getBatchBasicInfosFromCourse(String courseId) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/batch/getBatchBasicInfosFromCourse" + "?courseId=" + courseId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetBatchesRes getBatches = gson.fromJson(jsonString, GetBatchesRes.class);
        return getBatches;
    }

    public List<StudentSlotPreferencePojo> fetchStudentSlotPreference(GetBatchesReq req) throws VException {
        String fetchStudentSlotPreferenceUrl = SUBSCRIPTION_ENDPOINT + "/course/fetchStudentSlotPreference" + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(req);

        ClientResponse resp = WebUtils.INSTANCE.doCall(fetchStudentSlotPreferenceUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        Type otfListType = new TypeToken<List<StudentSlotPreferencePojo>>() {
        }.getType();

        List<StudentSlotPreferencePojo> response = gson.fromJson(jsonString, otfListType);
        return response;
    }

    public String getBatchCacheKey(String id) {

        return env + "_BATCH_INFO_" + id;
    }

    public List<EnrollmentPojo> getEnrollmentsForBatchIds(List<String> batchIds) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/enroll/getActiveStudentEnrollmentsByBatchIds?batchIds=" + String.join(",", batchIds), HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        Type otfListType = new TypeToken<List<EnrollmentPojo>>() {
        }.getType();

        List<EnrollmentPojo> response = gson.fromJson(jsonString, otfListType);
        return response;

    }

    @Deprecated
	public void handleSessionEventsForOTF(SessionEventsOTF eventType, String message) throws InternalServerErrorException{
		OTFSessionPojoUtils sessionPojo = null;
		sessionPojo = gson.fromJson(message, OTFSessionPojoUtils.class);
		if(sessionPojo != null){
			Long currentTime = System.currentTimeMillis();
			Long expiryTime = new Long(DateTimeUtils.MILLIS_PER_MINUTE * (LATEST_SESSION_CACHE_EXPIRY_MINUTE*2));
			expiryTime += currentTime;
			/*if(sessionPojo.getStartTime() != null && sessionPojo.getStartTime() > expiryTime
					&& !SessionEventsOTF.OTF_SESSION_RESCHEDULED.equals(eventType)){
				return;
			}*/
            List<String> cacheKeys = getSessionAttedeeCacheKeys(sessionPojo);
            if (ArrayUtils.isEmpty(cacheKeys)) {
                return;
            }
            logger.info("cacheKeys: " + cacheKeys);
            switch (eventType) {
                //TODO:reschedule of webinar
                case WEBINAR_SESSION_CANCELED:
                case WEBINAR_SESSION_SCHEDULED:
                case OTF_SESSION_SCHEDULED:
                case OTF_SESSION_CANCELED:
                case OTF_SESSION_RESCHEDULED:
                case ADD_BATCH_IDS_TO_SESSION:
                case OTF_SESSION_ENDED:
                case CLEAR_CACHE:
                    if (ArrayUtils.isNotEmpty(cacheKeys)) {
                        String[] deleteKeys = new String[cacheKeys.size()];
                        try {
                            redisDAO.deleteKeys(cacheKeys.toArray(deleteKeys));
                        } catch (Exception e) {
                            logger.info("Error in deleting Keys." + e);
                        }
                    }
                    return;
                default:
                    if (ArrayUtils.isNotEmpty(cacheKeys)) {
                        logger.info("Default handling as event is not handled : " + eventType);
                        String[] deleteKeys = new String[cacheKeys.size()];
                        try {
                            redisDAO.deleteKeys(cacheKeys.toArray(deleteKeys));
                        } catch (Exception e) {
                            logger.info("Error in deleting Keys." + e);
                        }
                    }

            }
//			Map<String, String> redisMap = null;
//			try {
//				redisMap = redisDAO.getValuesForKeys(cacheKeys);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				logger.info("redis get throws exception."+e);
//				}
//			if(redisMap == null || redisMap.isEmpty()){
//				return;
//			}
//			sessionManager.populateUserBasicInfos(sessionPojo,false);
//			switch(eventType){
//			case OTF_SESSION_RESCHEDULED:
//				String[] keysToDelete = new String[cacheKeys.size()];
//				int i = 0;
//				 for(Map.Entry<String, String> entry : redisMap.entrySet()){
//					 if(!StringUtils.isEmpty(entry.getValue())){
//						 GetLatestUpComingOrOnGoingSessionRes res = gson.fromJson(entry.getValue(), GetLatestUpComingOrOnGoingSessionRes.class);
//						 if(res != null && res.getOtfSession()!= null && !StringUtils.isEmpty(res.getOtfSession().getId()) 
//							 && res.getOtfSession().getId().equals(sessionPojo.getId())){
//							 keysToDelete[i++] = entry.getKey();
//						 }else if(res != null && sessionPojo.getStartTime() != null && sessionPojo.getStartTime() < expiryTime){
//							 if(res.getOtfSession() != null && res.getOtfSession().getStartTime() > sessionPojo.getStartTime()){
//								 res.setOtfSession(sessionPojo);
//								 String redisValue = gson.toJson(res);
//								 int expiry = sessionManager.getExpiryTimeForLastestSessions(res);
//								 try{
//									 redisDAO.setex(entry.getKey(), redisValue, expiry);
//								 }catch(Exception e){
//										logger.info("redis OTF Reschedule setex throws exception."+e);
//									}
//							 }
//						 }
//					 }
//				 }
//				 if(keysToDelete.length > 0){
//					 try{
//						 redisDAO.deleteKeys(keysToDelete);
//					 }catch(Exception e){
//						 logger.info("Error in deleting Keys."+e);
//					 }
//					 
//				 }
//				 break;
//			}
//			case OTF_SESSION_SCHEDULED:
//				 for(Map.Entry<String, String> entry : redisMap.entrySet()){
//					 if(!StringUtils.isEmpty(entry.getValue())){
//						 GetLatestUpComingOrOnGoingSessionRes res = gson.fromJson(entry.getValue(), GetLatestUpComingOrOnGoingSessionRes.class);
//						 if(res != null && sessionPojo.getStartTime() != null && sessionPojo.getStartTime() < expiryTime){
//							 if(res.getOtfSession() != null && res.getOtfSession().getStartTime() > sessionPojo.getStartTime()){
//								 res.setOtfSession(sessionPojo);
//								 String redisValue = gson.toJson(res);
//								 int expiry = sessionManager.getExpiryTimeForLastestSessions(res);
//								 try{
//									 redisDAO.setex(entry.getKey(), redisValue, expiry);
//								 }catch(Exception e){
//										logger.info("redis OTF Schedule setex throws exception." +e);
//									}
//							 }
//						 }
//					 }
//				 }
//				 break;
//			case OTF_SESSION_CANCELED:
//				String[] keyToDelete = new String[cacheKeys.size()];
//				int k = 0;
//				 for(Map.Entry<String, String> entry : redisMap.entrySet()){
//					 if(!StringUtils.isEmpty(entry.getValue())){
//						 GetLatestUpComingOrOnGoingSessionRes res = gson.fromJson(entry.getValue(), GetLatestUpComingOrOnGoingSessionRes.class);
//						 if(res != null && res.getOtfSession()!= null && !StringUtils.isEmpty(res.getOtfSession().getId()) 
//							 && res.getOtfSession().getId().equals(sessionPojo.getId())){
//							 keyToDelete[k++] = entry.getKey();
//						 }
//					 }
//					 if(keyToDelete.length > 0){
//						 try{
//							 redisDAO.deleteKeys(keyToDelete);
//						 }catch(Exception e){
//							 logger.info("Error in deleting Keys."+e);
//						 }
//					 }
//					 break;


        }
    }

    public String getFormattedDate(Long epoch) {
        Date date = new Date(epoch);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String formattedDate = format.format(date);
        return formattedDate;
    }


    public List<String> getSessionAttedeeCacheKeys(OTFSessionPojoUtils sessionPojo) {
        List<String> cacheKeys = new ArrayList<>();
        String teacher = null;
        if (sessionPojo.getPresenter() != null) {
            teacher = redisDAO.getUserLatestSessionCacheKey(sessionPojo.getPresenter());
            if (!StringUtils.isEmpty(teacher)) {
                cacheKeys.add(teacher);
            }
            String teacherKey = redisDAO.getCachedKeyForUserLatestSessions(sessionPojo.getPresenter(), env);
            if (!StringUtils.isEmpty(teacherKey)) {
                cacheKeys.add(teacherKey);
            }
        }
        if (sessionPojo.getPreviousPresenter() != null) {
            String prevTeacher = redisDAO.getUserLatestSessionCacheKey(sessionPojo.getPreviousPresenter());
            if (!StringUtils.isEmpty(prevTeacher)) {
                cacheKeys.add(prevTeacher);
            }
            String prevTeacherKey = redisDAO.getCachedKeyForUserLatestSessions(sessionPojo.getPreviousPresenter(), env);
            if (!StringUtils.isEmpty(prevTeacherKey)) {
                cacheKeys.add(prevTeacherKey);
            }

        }
        try {
            if (ArrayUtils.isNotEmpty(sessionPojo.getAttendees())) {
                Set<String> attendeesSet = new HashSet<>(sessionPojo.getAttendees());
                List<BatchEnrolmentInfo> batchEnrolmentInfo = enrollmentManager.getBatchEnrolmentsByIds(sessionPojo.getBatchIds());
                if (ArrayUtils.isNotEmpty(batchEnrolmentInfo)) {
                    for (BatchEnrolmentInfo enrollmentPojo : batchEnrolmentInfo) {
                        attendeesSet.addAll(enrollmentPojo.getEnrolledStudentIds());
                    }
                }
                sessionPojo.setAttendees(new ArrayList<>(attendeesSet));
            }
        } catch (Exception ex) {
            logger.error("Error in getting user active enrollments: " + ex.getMessage());
        }
        if (ArrayUtils.isNotEmpty(sessionPojo.getAttendees())) {
            for (String attendee : sessionPojo.getAttendees()) {
                String student = redisDAO.getUserLatestSessionCacheKey(attendee);
                if (!StringUtils.isEmpty(student)) {
                    cacheKeys.add(student);
                }
                String studentSessionKey = redisDAO.getCachedKeyForUserLatestSessions(attendee, env);
                if (!StringUtils.isEmpty(studentSessionKey)) {
                    cacheKeys.add(studentSessionKey);
                }
            }
        }
        return cacheKeys;
    }

    // migrated to scheduling
	@Deprecated
    public void handleBatchEventsForOTF(BatchEventsOTF eventType, String message) throws InternalServerErrorException {

        switch (eventType) {
            case USER_ACTIVE:
            case USER_INACTIVE:
            case USER_ENROLLED:
            case USER_ENDED:
                EnrollmentPojo enrollment = gson.fromJson(message, EnrollmentPojo.class);
                List<String> cachedUserSessionKeys = new ArrayList<>();if (enrollment != null && !StringUtils.isEmpty(enrollment.getUserId())) {
                    cachedUserSessionKeys.add(redisDAO.getUserLatestSessionCacheKey(enrollment.getUserId()));
                    cachedUserSessionKeys.add( redisDAO.getCachedKeyForUserLatestSessions(enrollment.getUserId(), env));
                    removeCachedSessionsForAttendees(cachedUserSessionKeys);
                }
                break;
            case BATCH_ACTIVE:
            case BATCH_INACTIVE:
                List<String> cacheKeys = new ArrayList<>();
                BatchInfo batchInfo = gson.fromJson(message, BatchInfo.class);
                if (batchInfo != null) {
                    cacheKeys.add(getBatchCacheKey(batchInfo.getBatchId()));
                    if (ArrayUtils.isNotEmpty(batchInfo.getTeachers())) {
                        for (UserBasicInfo user : batchInfo.getTeachers()) {
                            if (user.getUserId() != null) {
                                cacheKeys.add(redisDAO.getUserLatestSessionCacheKey(user.getUserId()));
                                cacheKeys.add(redisDAO.getCachedKeyForUserLatestSessions(user.getUserId().toString(), env));
                            }
                        }
                    }
//				if(ArrayUtils.isNotEmpty(batchInfo.getEnrolledStudents())){
//					for(UserBasicInfo user:batchInfo.getEnrolledStudents()){
//						if(user.getUserId() != null){
//							cacheKeys.add(redisDAO.getUserLatestSessionCacheKey(user.getUserId()));
//                                                        cacheKeys.add(redisDAO.getCachedKeyForUserLatestSessions(user.getUserId().toString(),env));
//						}
//					}
//				}
//				
                    if (CollectionUtils.isNotEmpty(cacheKeys)) {
                        removeCachedSessionsForAttendees(cacheKeys);
                    }
                }
                break;
        }
    }

    private void sendRecordingRequestToSQS(HandleOTFRecordingLambda handleOTFRecordingLambda) throws VException {
        if (handleOTFRecordingLambda.getSessionId() != null) {
            OTFSessionPojoUtils session = getSessionById(handleOTFRecordingLambda.getSessionId());
            try {
                if (StringUtils.isEmpty(session.getOrganizerAccessToken())) {
                    // Without organizer token we cannot delete the recording
                    logger.error("No orgainzer token for session id : " + session.getId());
                    return;
                }

                String credentials = ConfigUtils.INSTANCE
                        .getStringValue("gtt.account.credentials." + session.getOrganizerAccessToken());
                if (StringUtils.isEmpty(credentials)) {
                    logger.error("Credentials missing for session id : " + session.getId());
                    return;
                }
                String[] credentialArr = credentials.split("-");
                HandleOTFRecordingLambda.Credentials messageObject = new HandleOTFRecordingLambda.Credentials(credentialArr[0], credentialArr[1]);
                handleOTFRecordingLambda.setCredentials(messageObject);
                awsSQSManager.sendToSQS(SQSQueue.OTF_RECORDING, SQSMessageType.HANDLE_OTF_RECORDING, new Gson().toJson(handleOTFRecordingLambda));
            } catch (Exception ex) {
                logger.error("Error : " + ex.toString() + " message:" + ex.getMessage());
            }

        }
    }

    public void processBatchRegAfterPayment(String orderId, Long userId, String entityId)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessOtfPostPaymentReq req = new ProcessOtfPostPaymentReq();
        req.setOrderId(orderId);
        req.setUserId(userId);
        req.setEntityId(entityId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/batch/processOTFRegFeePayment", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public void processCourseRegAfterPayment(String orderId, Long userId, String entityId)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessOtfPostPaymentReq req = new ProcessOtfPostPaymentReq();
        req.setOrderId(orderId);
        req.setUserId(userId);
        req.setEntityId(entityId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/course/processCourseRegFeePayment", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public void processBatchAfterPayment(String orderId, Long userId, String purchaseContextId, String entityId)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessOtfPostPaymentReq req = new ProcessOtfPostPaymentReq();
        req.setOrderId(orderId);
        req.setUserId(userId);
        req.setEntityId(entityId);
        if (purchaseContextId != null) {
            req.setPurchaseContextId(purchaseContextId);
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/batch/processBatchAfterPayment", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public void processInstalmentForBatch(InstalmentInfo info)
            throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                + "/batch/processInstalmentAfterPayment", HttpMethod.POST, new Gson().toJson(info));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    @Deprecated
    public List<OTFCalendarSessionInfo> getPastSessionsForUserWithFilter(Integer start, Integer size, String batchId, Long userId, Role userRole, String query, Set<String> batchIds) throws VException {
        String getCalendarSessiosnUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getUserPastSessionsWithFilter?callingUserId=" + userId
                + "&callingUserRole=" + userRole.toString();
        if (!StringUtils.isEmpty(batchId)) {
            getCalendarSessiosnUrl += "&batchId=" + batchId;
        }
        if (ArrayUtils.isNotEmpty(batchIds)) {
            getCalendarSessiosnUrl += "&batchIds=" + String.join(",", batchIds);
        }
        if (start != null) {
            getCalendarSessiosnUrl += "&start=" + start;
        }
        if (size != null) {
            getCalendarSessiosnUrl += "&size=" + size;
        }
        if (!StringUtils.isEmpty(query)) {
            getCalendarSessiosnUrl += "&query=" + query;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(getCalendarSessiosnUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        List<OTFCalendarSessionInfo> calendarSessionInfos = populateUsersAndBoards(sessions);
        return calendarSessionInfos;
    }

	public void removeCachedSessionsForAttendees(final List<String> cacheKeys) {
		List<List<String>> cacheKeyPartitions = Lists.partition(cacheKeys, 300);
		for (List<String> cacheKeyPartition : cacheKeyPartitions) {
			awsSNSManager.triggerSNS(SNSTopic.SCHEDULING_REDIS_OPS, SNSSubject.REMOVE_USER_CACHED_SESSIONS.name(), gson.toJson(cacheKeys));
		}
	}
}