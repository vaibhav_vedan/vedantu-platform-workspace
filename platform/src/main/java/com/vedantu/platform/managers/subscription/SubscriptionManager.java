package com.vedantu.platform.managers.subscription;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;

import com.vedantu.User.Role;
import com.vedantu.subscription.request.*;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionData;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.TeacherSlabPlan;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.request.RefundStudentWalletRequest;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.pojo.subscription.CompleteSessionRequest;
import com.vedantu.platform.pojo.subscription.GetCancelSubscriptionReq;
import com.vedantu.platform.pojo.subscription.GetPlanByIdResponse;
import com.vedantu.platform.pojo.subscription.GetSubscriptionsResponse;
import com.vedantu.platform.pojo.subscription.GetUnlockHoursResponse;
import com.vedantu.platform.pojo.subscription.SessionVO;
import com.vedantu.platform.pojo.subscription.SubscriptionReq;
import com.vedantu.platform.pojo.subscription.SubscriptionVO;
import com.vedantu.platform.pojo.subscription.UnlockHoursRequest;
import com.vedantu.platform.pojo.subscription.UnlockHoursResponse;
import com.vedantu.platform.response.dinero.GetTeacherPlansResponse;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.CumilativeRating;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.pojo.Subscription;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.SessionResponse;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;

@Service
public class SubscriptionManager {

	

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private FosUtils userUtils;

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@Autowired
	private CumilativeRatingDao cumilativeRatingDao;   
        
	@Autowired
	RequestCallbackManager requestCallbackManager;        
        
	private final Gson gson = new Gson();

	@Autowired
	AsyncTaskFactory asyncTaskFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionManager.class);

	private String subscriptionEndpoint;
	private String dineroEndpoint;
	private String schedulingEndPoint;
        
        @PostConstruct
        public void init() {
            subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
            dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
            schedulingEndPoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
        }        
                

	public SubscriptionResponse createSubscription(SubscriptionVO subscriptionVO) throws VException {
		logger.info("createSubscription Request:" + subscriptionVO.toString());

		SubscriptionReq subscriptionRequest = getSubscriptionRequest(subscriptionVO);
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/createSubscription",
				HttpMethod.POST, new Gson().toJson(subscriptionRequest));

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String jsonString = resp.getEntity(String.class);
		SubscriptionResponse subscriptionResponse = new Gson().fromJson(jsonString, SubscriptionResponse.class);
		logger.info("createSubscription Response:" + subscriptionResponse.toString());
/*                
                // Adding RCB for trial session
                if (subscriptionResponse.getModel() != null
                                && subscriptionResponse.getModel().equals(SessionModel.TRIAL)) {
                        try {

                                AddRequestCallbackDetailsReq requestCallbackDetailsReq=new AddRequestCallbackDetailsReq();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                String sessionDate = dateFormat
                                                .format(subscriptionVO.getSessionSchedule().getSessionSlots().get(0).getStartTime());

                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
                                timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                String sessionTime = timeFormat
                                                .format(subscriptionVO.getSessionSchedule().getSessionSlots().get(0).getStartTime());

                                String defaultMessage = subscriptionResponse.getStudent().getFirstName()
                                                + " has scheduled a trial session with you for class "
                                                + subscriptionResponse.getGrade().toString() + " " + subscriptionResponse.getSubject()
                                                + " at " + sessionTime + " on " + sessionDate + ".";

                                logger.info("Student name:" + defaultMessage);
                                requestCallbackDetailsReq.setBoardId(subscriptionVO.getBoardId());
                                requestCallbackDetailsReq.setCallingUserId(subscriptionVO.getCallingUserId());
                                requestCallbackDetailsReq.setGrade(subscriptionVO.getGrade().longValue());
                                
                                if (subscriptionVO.getNote() == null || subscriptionVO.getNote().isEmpty()) {
                                        requestCallbackDetailsReq.setMessage(defaultMessage);
                                } else {
                                        requestCallbackDetailsReq.setMessage(defaultMessage + " Note: " + subscriptionVO.getNote());
                                }
                                requestCallbackDetailsReq.setStudentId(subscriptionVO.getStudentId());
                                // jsonObject.addProperty("requestSource",
                                // "AUTO_ADDITION_TRIAL");
                                requestCallbackDetailsReq.setTarget(subscriptionVO.getTarget());
                                requestCallbackDetailsReq.setTeacherId(subscriptionVO.getTeacherId());

                                requestCallbackManager.addRequestCallback(requestCallbackDetailsReq);
                        } catch (Exception e) {
                                logger.error("Unable to add rcb for TRIAL session: " + subscriptionVO.toString() , e);
                        }
                }*/
		return subscriptionResponse;
	}        
        
        
	public SubscriptionResponse getSubscription(Long id) throws VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/getSubscription/" + id,
				HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String jsonString = resp.getEntity(String.class);
		logger.info("Subscription Response:" + jsonString);
		SubscriptionResponse subscriptionResponse = new Gson().fromJson(jsonString, SubscriptionResponse.class);


		// filling teacher and student info
		List<Long> userIds = new ArrayList<>();
		Long teacherId = subscriptionResponse.getTeacherId();
		Long studentId = subscriptionResponse.getStudentId();
		userIds.add(teacherId);
		userIds.add(studentId);
		Map<Long, User> userInfos = userUtils.getUserInfosMap(userIds, false);
                
                
                List<String> userIdsString= new ArrayList<>();
                userIdsString.add(teacherId.toString());
                userIdsString.add(studentId.toString());
                Map<String,CumilativeRating> ratingsMap = cumilativeRatingDao.getCumulativeRatingsMap(userIdsString, EntityType.USER);
                UserInfo teacherUserInfo=new UserInfo(userInfos.get(teacherId), ratingsMap.get(teacherId.toString()), false);
                UserInfo studentUserInfo=new UserInfo(userInfos.get(studentId), ratingsMap.get(studentId.toString()), false);
                
		subscriptionResponse.setTeacher(teacherUserInfo);
		subscriptionResponse.setStudent(studentUserInfo);

		// fetching session infos
		resp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "/session/getSessions?subscriptionId="
				+ subscriptionResponse.getSubscriptionId()
                        + "&includeAttendees=true&sortType=START_TIME_DESC&size=100", HttpMethod.GET, null);
		String sessionListResp = resp.getEntity(String.class);
		Type SessionListType = new TypeToken<ArrayList<SessionInfo>>() {
		}.getType();
		List<SessionInfo> slist = gson.fromJson(sessionListResp, SessionListType);
                
                //adding the teacher and student infos
                if(ArrayUtils.isNotEmpty(slist)){
                    for(SessionInfo sessionInfo: slist){
                        if(sessionInfo!=null&&ArrayUtils.isNotEmpty(sessionInfo.getAttendees())){
                            for(UserSessionInfo userSessionInfo:sessionInfo.getAttendees()){
                                userSessionInfo.updateUserDetails(new UserBasicInfo(userInfos.get(userSessionInfo.getUserId()),false));
                            }
                        }
                    }
                }
                
		subscriptionResponse.setSessionList(slist);
                
                //filling plan info
		if (subscriptionResponse.getPlanId() != null) {
			resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/getTeacherPlans?teacherId="
					+ subscriptionResponse.getTeacherId(), HttpMethod.GET, null);
			String planResponse = resp.getEntity(String.class);
			GetTeacherPlansResponse plan = new Gson().fromJson(planResponse, GetTeacherPlansResponse.class);
			List<TeacherSlabPlan> plans = plan.getPlans();
			if (plans != null && !plans.isEmpty()) {
				for (TeacherSlabPlan t : plans) {
					if (t.getId().equals(subscriptionResponse.getPlanId())) {
						subscriptionResponse.setPlan(t);
						break;
					}
				}
			}
		}                

		return subscriptionResponse;
	}

	public SubscriptionResponse getSubscriptionBasicInfo(Long id) throws VException {
		logger.info("ENTRY: " + id);
		ClientResponse resp = WebUtils.INSTANCE
				.doCall(subscriptionEndpoint + "subscription/getSubscriptionBasicInfo/" + id, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		logger.info("Subscription Response:" + resp.toString());
		String jsonString = resp.getEntity(String.class);
		SubscriptionResponse subscriptionResponse = new Gson().fromJson(jsonString, SubscriptionResponse.class);
		logger.info("Response:" + subscriptionResponse);
		return subscriptionResponse;
	}

	public SessionResponse bookSessionFromSubscriptionHours(SessionVO sessionVO) throws VException {
		SessionRequest sessionRequest = getSessionRequest(sessionVO);
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/bookSession",
				HttpMethod.POST, new Gson().toJson(sessionRequest));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		SessionResponse sessionResponse = new Gson().fromJson(resp.getEntity(String.class), SessionResponse.class);
		// sending email
		sendSubscriptionSessionRelatedEmail(sessionResponse, true);
		return sessionResponse;
	}

	public Subscription completeSession(SessionBillingDetails sessionBillingDetails) throws VException {
		logger.info("completeSession Request:" + sessionBillingDetails.toString());
		CompleteSessionRequest completeSessionRequest = getCompleteSessionRequest(sessionBillingDetails);
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/completeSession",
				HttpMethod.POST, new Gson().toJson(completeSessionRequest));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String res = resp.getEntity(String.class);
		Subscription subscriptionResponse = new Gson().fromJson(res, Subscription.class);
		logger.info("completeSession Response:" + subscriptionResponse.toString());
		return subscriptionResponse;
	}

	public Subscription completeTrialSession(SessionBillingDetails session) throws VException {
		logger.info("completeSession Request:" + session.toString());
		CompleteSessionRequest completeSessionRequest = getCompleteSessionRequest(session);
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/completeSession",
				HttpMethod.POST, new Gson().toJson(completeSessionRequest));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		Subscription subscriptionResponse = new Gson().fromJson(jsonString, Subscription.class);
		logger.info("EXIT " + subscriptionResponse);
		return subscriptionResponse;
	}

	public SessionResponse cancelSession(CancelSubscriptionSessionRequest cancelSessionRequest) throws VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/cancelSession",
				HttpMethod.POST, new Gson().toJson(cancelSessionRequest));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		SessionResponse sessionResponse = new Gson().fromJson(resp.getEntity(String.class), SessionResponse.class);
		sendSubscriptionSessionRelatedEmail(sessionResponse, false);
		return sessionResponse;
	}

	public SessionResponse cancelTrialSession(CancelSubscriptionRequest cancelSubscriptionRequest)
			throws VException, UnsupportedEncodingException, AddressException {

		CancelSubscriptionResponse cancelSubscriptionResponse = cancelSubscription(cancelSubscriptionRequest);

		SessionResponse sessionResponse = new SessionResponse(cancelSubscriptionResponse.getSessionList(),
				cancelSubscriptionResponse);
		sendSubscriptionSessionRelatedEmail(sessionResponse, false);
		return sessionResponse;
	}

	public CancelSubscriptionResponse cancelSubscription(CancelSubscriptionRequest cancelSubscriptionRequest)
			throws VException, NotFoundException, UnsupportedEncodingException, AddressException {
		Long id = cancelSubscriptionRequest.getSubscriptionId();
		logger.info("cancelSubscription Request for subscriptionId: " + id + ", refundReason: "
				+ cancelSubscriptionRequest.getRefundReason());
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/cancelSubscription",
				HttpMethod.POST, gson.toJson(cancelSubscriptionRequest));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String cancelResp = resp.getEntity(String.class);
		CancelSubscriptionResponse cancelSubscriptionResponse = gson.fromJson(cancelResp,
				CancelSubscriptionResponse.class);

		logger.info(cancelSubscriptionResponse.toString());
		int refundAmount = cancelSubscriptionResponse.getRefundAmount();
		Float remainingPercentage = (float) (cancelSubscriptionResponse.getLockedHours()
				+ cancelSubscriptionResponse.getRemainingHours()) / cancelSubscriptionResponse.getTotalHours();
		if (cancelSubscriptionRequest.getRefundInfoCall().equals(false)) {
			RefundStudentWalletRequest refundStudentWalletRequest = new RefundStudentWalletRequest(
					cancelSubscriptionResponse.getStudentId(), refundAmount, id, remainingPercentage);
			resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/refundToStudentWallet", HttpMethod.POST,
					gson.toJson(refundStudentWalletRequest));

			try {
				VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			} catch (VException e) {
				logger.error(e);
				GetCancelSubscriptionReq req = new GetCancelSubscriptionReq(id,
						cancelSubscriptionResponse.getSubscriptionDetailsIds(), null);
				WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/revertCancelSubscription/" + id,
						HttpMethod.POST, gson.toJson(req));
			}
		} else {
			String queryStr = "studentId=" + cancelSubscriptionResponse.getStudentId() + "&subscriptionId=" + id
					+ "&refundAmount=" + refundAmount + "&remainingPercentage=" + remainingPercentage;
			resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/getRefundInfo?" + queryStr, HttpMethod.GET,
					null);
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		}

		String refundResStr = resp.getEntity(String.class);
		logger.info("getRefundInfo Response:" + refundResStr);
		RefundRes refundRes = gson.fromJson(refundResStr, RefundRes.class);
		cancelSubscriptionResponse.fillCancelSubscriptionResponse(refundRes);

		// deblocking the calendar for instalment orders
		// for new deblocking the calendar for first detailsId
		// TODO do it for all details under this subscription
		if (Boolean.FALSE.equals(cancelSubscriptionRequest.getRefundInfoCall())
				&& ArrayUtils.isNotEmpty(refundRes.getPaymentTypes())
				&& ArrayUtils.isNotEmpty(refundRes.getDeliverableEntityIds())) {
			PaymentType paymentType = refundRes.getPaymentTypes().get(0);
			if (PaymentType.INSTALMENT.equals(paymentType)) {
				String subscriptionDetailsId = refundRes.getDeliverableEntityIds().get(0);
				try {
					String markprocessedUrl = schedulingEndPoint
							+ ConfigUtils.INSTANCE.getStringValue("calendar.block.slot.markprocessed") + "?referenceId="
							+ subscriptionDetailsId;
					ClientResponse markprocessedResp = WebUtils.INSTANCE.doCall(markprocessedUrl, HttpMethod.POST,
							null);
					VExceptionFactory.INSTANCE.parseAndThrowException(markprocessedResp);
					String jsonString = markprocessedResp.getEntity(String.class);
					logger.info("Response for block slot markprocessed : " + jsonString);
				} catch (Exception ex) {
					logger.error("Error in marking calendar processed for subscriptionDetailsId "
							+ subscriptionDetailsId + ", ex " + ex.getMessage());
				}

				String orderId = refundRes.getOrderIds().get(0);
				try {
					logger.info("Marking the order and instalments as forfeited if the full payment is not paid");
					String markOrderForfeited = dineroEndpoint + "/payment/markOrderForfeited";
					JSONObject markOrderForfeitedReq = new JSONObject();
					markOrderForfeitedReq.put("orderId", orderId);

					ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
							markOrderForfeitedReq.toString());
					VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
					String jsonString = markOrderForfeitedRes.getEntity(String.class);
					logger.info("Response for markOrderForfeited  : " + jsonString);
				} catch (Exception ex) {
					logger.error("Error in markOrderForfeited for orderId " + orderId + ", ex " + ex.getMessage());
				}
			}
		}

		// sending mail in case of ending/cancelling the subscription
		if (Boolean.FALSE.equals(cancelSubscriptionRequest.getRefundInfoCall())
				&& SessionModel.OTO.equals(cancelSubscriptionResponse.getModel())
				&& SubModel.DEFAULT.equals(cancelSubscriptionResponse.getSubModel())) {
			Map<String, Object> payload = new HashMap<>();
			payload.put("cancelSubscriptionResponse", cancelSubscriptionResponse);
			payload.put("httpSessionData", httpSessionUtils.getCurrentSessionData());
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_ENDED_EMAIL, payload);
			asyncTaskFactory.executeTask(params);
		}
		return cancelSubscriptionResponse;
	}

	public CancelSubscriptionResponse cancelTrialSubscription(CancelSubscriptionRequest cancelSubscriptionRequest)
			throws VException {
		Long id = cancelSubscriptionRequest.getSubscriptionId();
		logger.info("cancelTrialSubscription Request for subscriptionId: " + id + "refundReason: "
				+ cancelSubscriptionRequest.getRefundReason());
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/cancelTrialSubscription",
				HttpMethod.POST, new Gson().toJson(cancelSubscriptionRequest));

		String cancelResp = resp.getEntity(String.class);
		logger.info("Response from platform:" + cancelResp);
		CancelSubscriptionResponse cancelSubscriptionResponse = new Gson().fromJson(cancelResp,
				CancelSubscriptionResponse.class);

		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		logger.info(cancelSubscriptionResponse.toString());
		if (cancelSubscriptionRequest.getRefundInfoCall().equals(false)) {
			int refundAmount = cancelSubscriptionResponse.getRefundAmount();
			Float consumedPercentage = (float) (cancelSubscriptionResponse.getLockedHours()
					+ cancelSubscriptionResponse.getRemainingHours()) / cancelSubscriptionResponse.getTotalHours();
			RefundStudentWalletRequest refundStudentWalletRequest = new RefundStudentWalletRequest(
					cancelSubscriptionResponse.getStudentId(), refundAmount, id, consumedPercentage);
			logger.info("Calling dinero to refund student wallet");
			resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/refundToStudentWallet", HttpMethod.POST,
					new Gson().toJson(refundStudentWalletRequest));
			try {
				VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			} catch (Exception e) {
				GetCancelSubscriptionReq req = new GetCancelSubscriptionReq(id,
						cancelSubscriptionResponse.getSubscriptionDetailsIds(), null);
				WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/revertCancelSubscription/" + id,
						HttpMethod.POST, new Gson().toJson(req));
				throw e;
			}
		}
		logger.info("cancelTrialSubscription Response:" + cancelSubscriptionResponse.toString());
		return cancelSubscriptionResponse;
	}

	public CancelSubscriptionResponse endTrialSubscription(Long studentId, Long subscriptionId) throws VException {
		logger.info("ENTRY: studentId " + studentId + ", subscriptionId " + subscriptionId);
		CancelSubscriptionRequest cancelSubscriptionRequest = new CancelSubscriptionRequest(false, subscriptionId,
				"Trial Session Completed", studentId);
		CancelSubscriptionResponse cancelSubscriptionResponse = cancelTrialSubscription(cancelSubscriptionRequest);
		logger.info("EXIT:" + cancelSubscriptionResponse);
		return cancelSubscriptionResponse;
	}

	private CompleteSessionRequest getCompleteSessionRequest(SessionBillingDetails session) {
		CompleteSessionRequest completeSessionRequest = new CompleteSessionRequest(session.getSubscriptionId(),
				session.getSessionScheduledDuration(), session.getBillingDuration(), Long.parseLong(session.getSessionId()));
		return completeSessionRequest;
	}

	public SubscriptionReq getSubscriptionRequest(SubscriptionVO subscriptionVO) {
		/* EntityId = PlanId */
		/* Fetching teacher hourly rate based on planId */
		try {
			ClientResponse resp = WebUtils.INSTANCE.doCall(
					dineroEndpoint + "/pricing/getPlanById?id=" + subscriptionVO.getEntityId(), HttpMethod.GET, null);
			String planResponse = resp.getEntity(String.class);
			GetPlanByIdResponse planRes = new Gson().fromJson(planResponse, GetPlanByIdResponse.class);
			Long teacherHourlyRate = planRes.getPrice();
			if (teacherHourlyRate == null) {
				logger.error("Null hourly rate from dinero");
				return null;
			}
			if (subscriptionVO.getTeacherDiscountAmount() != null && subscriptionVO.getTeacherDiscountAmount() > 0l) {
				if (subscriptionVO.getHours() != null && subscriptionVO.getHours() > 0) {
					teacherHourlyRate -= (subscriptionVO.getTeacherDiscountAmount()
							/ (subscriptionVO.getHours() / DateTimeUtils.MILLIS_PER_HOUR));
				}
			}

			ScheduleType scheduleType = null;
			if (subscriptionVO.getSessionSchedule() == null) {
				scheduleType = ScheduleType.NFS;
			} else {
				scheduleType = ScheduleType.FS;
			}

			SubscriptionReq subscriptionRequest = new SubscriptionReq(subscriptionVO.getTeacherId(),
					subscriptionVO.getStudentId(), subscriptionVO.getEntityId(), subscriptionVO.getOfferingId(),
					subscriptionVO.getModel(), scheduleType, subscriptionVO.getHours(),
					subscriptionVO.getTeacherDiscountCouponId(), subscriptionVO.getTeacherDiscountAmount(),
					teacherHourlyRate, subscriptionVO.getBoardId().toString(), subscriptionVO.getTarget(),
					subscriptionVO.getGrade(), subscriptionVO.getSessionSchedule(), null, subscriptionVO.getNote(),
					subscriptionVO.getSubModel(), subscriptionVO.getSessionSource());
			subscriptionRequest.setCallingUserId(subscriptionVO.getStudentId());
			return subscriptionRequest;
		} catch (Throwable e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public SessionRequest getSessionRequest(SessionVO sessionVO) {
		try {
			SessionRequest sessionRequest = new SessionRequest(sessionVO.getStudentId(), sessionVO.getTeacherId(),
					sessionVO.getEntityId(), sessionVO.getModel(), sessionVO.getSessionSlots(),
					sessionVO.getSessionId(), sessionVO.getRequestorId(), 0l, sessionVO.getReason());
			sessionRequest.setCallingUserId(sessionVO.getRequestorId());
                        sessionRequest.setProposalId(sessionVO.getProposalId());
                        sessionRequest.setSessionSource(sessionVO.getSessionSource());
			return sessionRequest;
		} catch (Throwable e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public void sendSubscriptionSessionRelatedEmail(SessionResponse sessionResponse, boolean scheduled) {
                logger.info("sendSubscriptionSessionRelatedEmail "+sessionResponse+" scheduled "+scheduled);
		if (sessionResponse != null && ArrayUtils.isNotEmpty(sessionResponse.getSessionInfo())
				&& sessionResponse.getSubscriptionResponse() != null) {
			SessionInfo sessionInfo = sessionResponse.getSessionInfo().get(0);
			SubscriptionResponse subscriptionResponse = sessionResponse.getSubscriptionResponse();
			Map<String, Object> payload = new HashMap<>();
			payload.put("sessionInfo", sessionInfo);
			payload.put("role", httpSessionUtils.getCurrentSessionData().getRole());
			payload.put("model", subscriptionResponse.getModel());
			payload.put("subModel", subscriptionResponse.getSubModel());
			payload.put("scheduled", scheduled);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_SESSION_EMAIL, payload);
			asyncTaskFactory.executeTask(params);
		}
	}
        
        
        public GetUnlockHoursResponse unlockHours(List<UnlockHoursRequest> unlockHoursRequests)
			throws VException {
		logger.info("Request received for unlockHours: " + unlockHoursRequests);
		ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscription/unlockHours",
				HttpMethod.POST, new Gson().toJson(unlockHoursRequests));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String unlockResp = resp.getEntity(String.class);
		GetUnlockHoursResponse getUnlockHoursResponse = new Gson().fromJson(unlockResp, GetUnlockHoursResponse.class);

		for (UnlockHoursResponse u : getUnlockHoursResponse.getSessionHoursList()) {
			CancelSubscriptionResponse cancelSubscriptionResponse = u.getCancelSubscriptionResponse();
			if (cancelSubscriptionResponse != null) {
				int refundAmount = cancelSubscriptionResponse.getRefundAmount();
				Float consumedPercentage = (float) (cancelSubscriptionResponse.getLockedHours()
						+ cancelSubscriptionResponse.getRemainingHours()) / cancelSubscriptionResponse.getTotalHours();

				RefundStudentWalletRequest refundStudentWalletRequest = new RefundStudentWalletRequest(
						cancelSubscriptionResponse.getStudentId(), refundAmount, u.getSubscriptionId(),
						consumedPercentage);
				resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/refundToStudentWallet", HttpMethod.POST,
						new Gson().toJson(refundStudentWalletRequest));

				try {
					VExceptionFactory.INSTANCE.parseAndThrowException(resp);
				} catch (VException e) {
					logger.error(e);
					GetCancelSubscriptionReq req = new GetCancelSubscriptionReq(u.getSubscriptionId(),
							cancelSubscriptionResponse.getSubscriptionDetailsIds(), null);
					WebUtils.INSTANCE.doCall(
							subscriptionEndpoint + "subscription/revertCancelSubscription/" + u.getSubscriptionId(),
							HttpMethod.POST, new Gson().toJson(req));
				}

			}
		}

		return getUnlockHoursResponse;
	}


	public GetSubscriptionsResponse getSubscriptions(GetSubscriptionsReq req) throws VException, IllegalArgumentException, IllegalAccessException {

		String getString = WebUtils.INSTANCE.createQueryStringOfObject(req);
		ClientResponse resp = WebUtils.INSTANCE
				.doCall(subscriptionEndpoint + "subscription/getSubscriptions?" + getString, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String subResp = resp.getEntity(String.class);
		GetSubscriptionsResponse response = new Gson().fromJson(subResp, GetSubscriptionsResponse.class);

		HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
		if (sessionData == null || (!Role.ADMIN.equals(sessionData.getRole()))) {

			if(CollectionUtils.isNotEmpty(response.getSubscriptionsList())){
				for(SubscriptionResponse subscriptionResponse : response.getSubscriptionsList()){
					subscriptionResponse.getTeacher().setContactNumber(null);
					subscriptionResponse.getTeacher().setEmail(null);
				}
			}
		}

		return response;
	}

}
