package com.vedantu.platform.managers.onetofew;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.onetofew.pojo.StudentSlotPreferencePojo;
import com.vedantu.onetofew.request.AddEditOTFBundleReq;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.onetofew.request.PurchaseOTFBundleReq;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.dinero.CouponManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.ProcessOtfBundlePostPaymentReq;
import com.vedantu.subscription.response.CheckIfRegFeePaidRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.json.JSONException;
import org.json.JSONObject;

@Service
public class OTFBundleManager {

    @Autowired
    private LogFactory logFactory;


    @Autowired
    private PaymentManager paymentManager;

    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(OTFBundleManager.class);

    private final Gson gson = new Gson();

//	    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String ONETOFEW_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT") + "/";

    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    public OTFBundleInfo addEditOTFBundle(AddEditOTFBundleReq addEditBundleReq) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/addEditOTFBundle", HttpMethod.POST, new Gson().toJson(addEditBundleReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        OTFBundleInfo otfBundleInfo = new Gson().fromJson(jsonString, OTFBundleInfo.class);

        if (ArrayUtils.isNotEmpty(addEditBundleReq.getInstalmentDetails())) {
            List<BaseInstalmentInfo> baseInstalmentInfos = addEditBundleReq.getInstalmentDetails();
            BaseInstalment baseInstalment = new BaseInstalment(InstalmentPurchaseEntity.OTF_BUNDLE, otfBundleInfo.getId(),
                    baseInstalmentInfos);
            paymentManager.updateBaseInstalment(baseInstalment);
            otfBundleInfo.setInstalmentDetails(baseInstalmentInfos);
        }

        return otfBundleInfo;
    }

    public OTFBundleInfo getOTFBundle(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/" + id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        OTFBundleInfo otfBundleInfo = new Gson().fromJson(jsonString, OTFBundleInfo.class);
        BaseInstalment baseInstalment = paymentManager.getBaseInstalment(InstalmentPurchaseEntity.OTF_BUNDLE, id);
        if (baseInstalment != null) {
            otfBundleInfo.setInstalmentDetails(baseInstalment.getInfo());
        }
        return otfBundleInfo;
    }

    public List<OTFBundleInfo> getOTFBundleInfos(GetOTFBundlesReq req) throws VException {
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/get?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<OTFBundleInfo>>() {
        }.getType();
        List<OTFBundleInfo> bundleInfos = new Gson().fromJson(jsonString, listType1);
        if (bundleInfos != null && !bundleInfos.isEmpty()) {
            List<String> otfBundleIds = new ArrayList<>();
            for (OTFBundleInfo otfBundleInfo : bundleInfos) {
                otfBundleIds.add(otfBundleInfo.getId());
            }
            List<BaseInstalment> baseInstalments = paymentManager
                    .getBaseInstalments(InstalmentPurchaseEntity.OTF_BUNDLE, otfBundleIds);
            if (baseInstalments != null && !baseInstalments.isEmpty()) {
                Map<String, BaseInstalment> baseInstalmentMap = new HashMap<>();
                for (BaseInstalment baseInstalment : baseInstalments) {
                    baseInstalmentMap.put(baseInstalment.getPurchaseEntityId(), baseInstalment);
                }
                for (OTFBundleInfo otfBundleInfo : bundleInfos) {
                    BaseInstalment baseInstalment = baseInstalmentMap.get(otfBundleInfo.getId());
                    if (baseInstalment != null) {
                        otfBundleInfo.setInstalmentDetails(baseInstalment.getInfo());
                    }
                }

            }
        }
        return bundleInfos;
    }

    public OrderInfo purchaseAndRegisterOTFBundle(PurchaseOTFBundleReq req) throws VException, CloneNotSupportedException {
        req.verify();
        String otfBundleId = req.getOtfBundleId();

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId);
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }

        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();

        int amountToPay = 0;
        if (otfBundleInfo.getRegistrationFee() != null) {

            amountToPay = otfBundleInfo.getRegistrationFee();
        }

        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(otfBundleId);
        buyItemsReqNew.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setPaymentType(PaymentType.BULK);
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);
//	        buyItemsReqNew.setPurchasingEntities(req.getPurchasingEntities());

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        StudentSlotPreferencePojo pojo = new StudentSlotPreferencePojo();
        pojo.setUserId(req.getUserId().toString());
        pojo.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        pojo.setEntityId(otfBundleId);
        pojo.setPreferredSlots(req.getPreferredSlots());

        if (!orderInfo.getNeedRecharge()) {
            processOTFBundlePurchaseAfterRegFeePayment(orderInfo.getId(), orderInfo.getUserId(),
                    otfBundleId);
            pojo.setPostPayment(Boolean.TRUE);
        } else {
            pojo.setPostPayment(Boolean.FALSE);
        }
        if (ArrayUtils.isNotEmpty(req.getPreferredSlots())) {
            ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT + "course/updateStudentPreferenceSessionSlot", HttpMethod.POST, new Gson().toJson(pojo));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        }

        return orderInfo;
    }

    public void processOTFBundlePurchaseAfterRegFeePayment(String orderId, Long userId,
            String otfbundleId)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessOtfBundlePostPaymentReq req = new ProcessOtfBundlePostPaymentReq();
        req.setOrderId(orderId);
        req.setUserId(userId);
        req.setOtfBundleId(otfbundleId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/processOTFBundlePurchaseAfterRegFeePayment", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public OrderInfo purchaseOTFBundleByAdvancePayment(PurchaseOTFBundleReq req) throws VException {
        req.verify();
        String otfBundleId = req.getOtfBundleId();

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId);
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }

        checkIfRegFeePaid(req, otfBundleInfo);

        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        if (otfBundleInfo.getPrice() == null) {
            logger.error("price is null in bundlepackage : " + otfBundleId);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bundle price is null");
        }
        int amountToPay = otfBundleInfo.getPrice();

        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(otfBundleId);
        buyItemsReqNew.setEntityType(EntityType.OTM_BUNDLE_ADVANCE_PAYMENT);
        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        if (req.getPaymentType() == null) {
            req.setPaymentType(PaymentType.BULK);
        }
        buyItemsReqNew.setPaymentType(req.getPaymentType());
        buyItemsReqNew.setIpAddress(req.getIpAddress());

        List<PurchasingEntity> entities = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(otfBundleInfo.getEntities())) {
            for (OTMBundleEntityInfo otfEntity : otfBundleInfo.getEntities()) {
                PurchasingEntity entity = new PurchasingEntity(otfEntity.getEntityType(), otfEntity.getEntityId());
                entity.setPrice(otfEntity.getPrice());
                entities.add(entity);
            }
        }
        buyItemsReqNew.setPurchasingEntities(entities);

        if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
            throw new BadRequestException(ErrorCode.INSTALMENT_NOT_FOUND, "Instalment not allowed");
        }
        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        if (!orderInfo.getNeedRecharge()) {
            processOtfBundlePurchaseAfterAdvancePayment(orderInfo.getId(), orderInfo.getUserId(), otfBundleId);
        }
        return orderInfo;
    }

    public CheckIfRegFeePaidRes checkIfRegFeePaid(PurchaseOTFBundleReq req) throws VException {
        req.verify();
        String otfBundleId = req.getOtfBundleId();

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId);
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "otf bundle not found");
        }

        return checkIfRegFeePaid(req, otfBundleInfo);
    }

    public void processOtfBundlePurchaseAfterAdvancePayment(String orderId, Long userId, String otfbundleId)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessOtfBundlePostPaymentReq req = new ProcessOtfBundlePostPaymentReq();
        req.setOrderId(orderId);
        req.setUserId(userId);
        req.setOtfBundleId(otfbundleId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/processOtfBundlePurchaseAfterAdvancePayment", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public CheckIfRegFeePaidRes checkIfRegFeePaid(PurchaseOTFBundleReq req, OTFBundleInfo otfBundleInfo) throws ForbiddenException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                DINERO_ENDPOINT + "/payment/getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded?entityId="
                + req.getOtfBundleId() + "&userId=" + req.getUserId() + "&entityType=OTM_BUNDLE_REGISTRATION",
                HttpMethod.GET, null);
        String listResp = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(listResp, listType);
        CheckIfRegFeePaidRes res = new CheckIfRegFeePaidRes();
        String orderId = null;
        if (ArrayUtils.isEmpty(orders)) {
            throw new ForbiddenException(ErrorCode.NOT_REGISTERED, "Bundle Registration Fee not paid");
//            if (otfBundleInfo.getRegistrationFee() != null && otfBundleInfo.getRegistrationFee() > 0) {
//                throw new ForbiddenException(ErrorCode.NOT_REGISTERED, "Bundle Registration Fee not paid");
//            }
        } else {
            orderId = orders.get(0).getId();
        }
        res.setOrderId(orderId);
        res.setRegFeePaid(true);
        return res;
    }

    public void processOtfBundlePurchaseAfterPayment(String orderId, Long userId, String otfbundleId)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessOtfBundlePostPaymentReq req = new ProcessOtfBundlePostPaymentReq();
        req.setOrderId(orderId);
        req.setUserId(userId);
        req.setOtfBundleId(otfbundleId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/processOtmBundlePurchaseAfterPayment", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public void processInstalmentPayment(InstalmentInfo instalmentInfo,
            Long userId)
            throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/processInstalmentAfterPayment", HttpMethod.POST, new Gson().toJson(instalmentInfo));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public void markEnrollmentStatus(Long userId, String entityId, EntityStatus newStatus)
            throws VException {
        markEnrollmentStatus(userId, entityId, newStatus,null);
    }

    public void markEnrollmentStatus(Long userId, String entityId, EntityStatus newStatus, String orderId)
            throws VException {
        if (userId == null || StringUtils.isEmpty(entityId) || newStatus == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId" + userId + " entityId " + entityId + " is null");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(ONETOFEW_ENDPOINT
                + "otfBundle/markEnrollmentStatus?userId=" + userId + "&entityId=" + entityId + "&newStatus=" + newStatus.name(), HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        if (StringUtils.isNotEmpty(orderId)) {
            try {
                String markOrderForfeited = DINERO_ENDPOINT + "/payment/markOrderForfeited";
                JSONObject markOrderForfeitedReq = new JSONObject();
                markOrderForfeitedReq.put("orderId", orderId);

                ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                        markOrderForfeitedReq.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
                String jsonString = markOrderForfeitedRes.getEntity(String.class);
                logger.info("Response for markOrderForfeited  : " + jsonString);
            } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                logger.error("Error in markOrderForfeited for entityId  " + entityId + ", userId " + userId, ex);
            }
        }
        //Mail
    }
}
