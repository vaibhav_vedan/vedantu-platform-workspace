package com.vedantu.platform.managers.Dashboard;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.response.TeacherListWithStatusRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dashboard.pojo.TeacherDashboardByEntityInfo;
import com.vedantu.dashboard.response.GetTeacherDashboardReq;
import com.vedantu.dashboard.response.GetTeacherDashboardResp;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.GetInstallmentforBatchesReq;
import com.vedantu.dinero.request.GetRegistrationforEnrollmentsReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.TeacherContentCount;
import com.vedantu.lms.cmds.request.GetContentSharedForDashboardReq;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchDashboardInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GetOtfDashboardRes;
import com.vedantu.onetofew.pojo.OTFAttendance;
import com.vedantu.onetofew.pojo.StudentContentPojo;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.onetofew.request.GetOTFDashboardReq;
import com.vedantu.onetofew.request.ViewAttendanceForUserBatchReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.dashboard.OTFDashboardDAO;
import com.vedantu.platform.dao.dashboard.TeacherDailyDashboardDAO;
import com.vedantu.platform.mongodbentities.dashboard.OTFDashboard;
import com.vedantu.platform.mongodbentities.dashboard.TeacherDailyDashboard;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.platform.utils.TimeDateMonth;
import com.vedantu.scheduling.pojo.session.TeacherSessionDashboard;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.TeacherCourseStateCount;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.io.IOException;
import javax.mail.internet.AddressException;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

@Service
public class DashboardManager {

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private OTFDashboardDAO otfDashboardDAO;

	@Autowired
	private TeacherDailyDashboardDAO teacherDailyDashboardDAO;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(DashboardManager.class);

	private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

	private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

        @Autowired
        private DozerBeanMapper mapper;

        private static Gson gson = new Gson();
        
	public void createDashboardAsync() {
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_DASHBOARD_CRON, null);
		asyncTaskFactory.executeTask(params);
	}

	public void createDashboard() {

		Integer start = 0;
		Integer limit = 200;
		Boolean batchLimit = Boolean.TRUE;

		List<BatchDashboardInfo> batchInfos = new ArrayList<>();
		while (batchLimit) {
			try {
				GetBatchesForDashboardReq req = new GetBatchesForDashboardReq();
				req.setBatchStatus(EntityStatus.ACTIVE);
				req.setStart(start);
				req.setSize(limit);
				String qString = WebUtils.INSTANCE.createQueryStringOfObject(req);
				String batchUrl = SUBSCRIPTION_ENDPOINT + "/dashboard/getBatches?" + qString;
				ClientResponse batchResp = WebUtils.INSTANCE.doCall(batchUrl, HttpMethod.GET, null);
				VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
				String jsonString = batchResp.getEntity(String.class);
				Type listType1 = new TypeToken<ArrayList<BatchDashboardInfo>>() {
				}.getType();
				List<BatchDashboardInfo> batchDashboardInfos = new Gson().fromJson(jsonString, listType1);
				batchInfos.addAll(batchDashboardInfos);
				if (ArrayUtils.isEmpty(batchDashboardInfos)) {
					batchLimit = Boolean.FALSE;
				}
			} catch (NotFoundException e) {
				batchLimit = Boolean.FALSE;
				logger.info("Dashboard for batches size : " + batchInfos.size());
			} catch (Exception e) {
				batchLimit = Boolean.FALSE;
				logger.error("error while fetching batchinfo : " + e);
			}
			start = start + limit;
		}
		// for all now create asyncTasks
		int counter = 0;
		int threadSize = 25;
		Long time5 = new Long(DateTimeUtils.MILLIS_PER_MINUTE * 3);
		for (BatchDashboardInfo batchInfo : batchInfos) {
			Map<String, Object> payload = new HashMap<>();
			payload.put("batchInfo", batchInfo);
			Long timeDelay = time5 * (counter / threadSize);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_OTF_ENROLLMENT_DASHBOARD, payload,
					timeDelay);
			asyncTaskFactory.executeTask(params);
			counter++;
		}
	}

	public void createDashboardForBatch(String batchId) throws VException {
		String batchUrl = SUBSCRIPTION_ENDPOINT + "/dashboard/getBatch?batchId=" + batchId;
		ClientResponse batchResp = WebUtils.INSTANCE.doCall(batchUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
		String jsonString = batchResp.getEntity(String.class);
		BatchDashboardInfo batchInfo = new Gson().fromJson(jsonString, BatchDashboardInfo.class);
		Map<String, Object> payload = new HashMap<>();
		payload.put("batchInfo", batchInfo);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_OTF_ENROLLMENT_DASHBOARD, payload);
		asyncTaskFactory.executeTask(params);
	}

	public void updateDashboardInfo(BatchDashboardInfo batchInfo) throws VException {
		if (batchInfo == null || batchInfo.getBatchId() == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchInfo is null");
		}
		String enrollUrl = SUBSCRIPTION_ENDPOINT + "/dashboard/getEnrollmentsByBatchIds?batchIds=" + batchInfo.getBatchId();
		ClientResponse batchResp = WebUtils.INSTANCE.doCall(enrollUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
		String jsonString = batchResp.getEntity(String.class);
		Type listType1 = new TypeToken<ArrayList<EnrollmentPojo>>() {
		}.getType();
		List<EnrollmentPojo> enrollmentInfos = new Gson().fromJson(jsonString, listType1);
		if (CollectionUtils.isEmpty(enrollmentInfos)) {
			return;
		}

		List<String> studentIds = new ArrayList<>();
		for (EnrollmentPojo enrollmentPojo : enrollmentInfos) {
			studentIds.add(enrollmentPojo.getUserId());
		}
		Map<String, List<OTFAttendance>> enrollmentAttendance = getAttendanceMapping(batchInfo.getBatchId());
		Map<String, List<DashBoardInstalmentInfo>> enrollmentInstallment = getEnrollmentInstallmentMap(enrollmentInfos);
		Map<String, Orders> enrollmentOrders = getEnrollmentRegisteredOrderMap(enrollmentInfos);
		Map<String, List<StudentContentPojo>> studentContentMap = getStudentContentMap(studentIds,
				batchInfo.getBatchId());

		Map<Long, Board> boardIdSubjectMap = new HashMap<>();
		List<OTFDashboard> otfDashboards = toOTFDashboard(batchInfo, enrollmentInfos, enrollmentAttendance,
				enrollmentInstallment, studentContentMap, enrollmentOrders,boardIdSubjectMap);

		if (CollectionUtils.isNotEmpty(otfDashboards)) {
			List<String> ids = new ArrayList<>();
			for (OTFDashboard oTFDashboard : otfDashboards) {
				ids.add(oTFDashboard.getEnrollmentId());
			}
			otfDashboardDAO.remove(ids);
			otfDashboardDAO.updateAll(otfDashboards);
		}
	}

	public List<OTFDashboard> toOTFDashboard(BatchDashboardInfo batchInfo, List<EnrollmentPojo> enrollmentInfos,
			Map<String, List<OTFAttendance>> enrollmentAttendance,
			Map<String, List<DashBoardInstalmentInfo>> enrollmentInstallment,
			Map<String, List<StudentContentPojo>> studentContentMap, Map<String, Orders> enrollmentOrders, 
			Map<Long, Board> boardIdSubjectMap) {

		List<OTFDashboard> otfDashboards = new ArrayList<>();
		String totalString = "total";

		for (EnrollmentPojo enrollInfo : enrollmentInfos) {
			OTFDashboard otfDashboard = new OTFDashboard();
			otfDashboard.setUserId(Long.parseLong(enrollInfo.getUserId()));
			otfDashboard.setBatchId(enrollInfo.getBatchId());
			otfDashboard.setBatchStartDate(batchInfo.getBatchStartDate());
			otfDashboard.setEnrollmentId(enrollInfo.getEnrollmentId());
			otfDashboard.setCourseId(enrollInfo.getCourseId());
			otfDashboard.setCourseTitle(batchInfo.getCourseTitle());
			otfDashboard.setStartTime(enrollInfo.getCreationTime());
			otfDashboard.setState(enrollInfo.getState());
			otfDashboard.setStatus(enrollInfo.getStatus());
			if (enrollmentAttendance != null && enrollmentAttendance.containsKey(enrollInfo.getUserId())) {
				OTFAttendance totalAtt = new OTFAttendance();
				totalAtt.setSubject(totalString);
				List<OTFAttendance> attendances = enrollmentAttendance.get(enrollInfo.getUserId());
				for(OTFAttendance otfAttendance : attendances){
					if(otfAttendance.getBoardId() != null && boardIdSubjectMap.containsKey(otfAttendance.getBoardId())){
						otfAttendance.setSubject(boardIdSubjectMap.get(otfAttendance.getBoardId()).getName());
					}
					else {
						if(otfAttendance.getBoardId() != null){
							boardIdSubjectMap.putAll(fosUtils.getBoardInfoMap(Arrays.asList(otfAttendance.getBoardId())));
							if(boardIdSubjectMap.containsKey(otfAttendance.getBoardId()))
							{
								if(StringUtils.isNotEmpty(boardIdSubjectMap.get(otfAttendance.getBoardId()).getName())){
									otfAttendance.setSubject(boardIdSubjectMap.get(otfAttendance.getBoardId()).getName());
								}
							}
							else {
								logger.info("Not able to Map board for boardId : " + otfAttendance.getBoardId());
							}
						}
					}
					totalAtt.setAttended(totalAtt.getAttended()+otfAttendance.getAttended());
					totalAtt.setTotal(totalAtt.getTotal()+otfAttendance.getTotal());
					//totalAtt.setLastFive(totalAtt.getLastFive()+otfAttendance.getLastFive());
				}
				attendances.add(0, totalAtt);
				otfDashboard.setOtfAttendances(attendances);
			} else {
				if (enrollmentAttendance != null && enrollmentAttendance.entrySet().size() > 0) {
					List<OTFAttendance> tempAttendance = enrollmentAttendance.entrySet().iterator().next().getValue();
					List<OTFAttendance> newAttendances = new ArrayList<>();
					OTFAttendance totalAtt = new OTFAttendance();
					totalAtt.setSubject(totalString);
					Boolean totalExists = Boolean.FALSE;
					if (ArrayUtils.isNotEmpty(tempAttendance)) {
						for (OTFAttendance otfAttendance : tempAttendance) {
							OTFAttendance newAttendance = new OTFAttendance();
							newAttendance.setTeacherId(otfAttendance.getTeacherId());
							newAttendance.setTotal(otfAttendance.getTotal());
							newAttendance.setBoardId(otfAttendance.getBoardId());
							if(boardIdSubjectMap.containsKey(newAttendance.getBoardId())){
								newAttendance.setSubject(boardIdSubjectMap.get(newAttendance.getBoardId()).getName());
							}
							else {
								if(newAttendance.getBoardId() != null){
									boardIdSubjectMap.putAll(fosUtils.getBoardInfoMap(Arrays.asList(newAttendance.getBoardId())));
									if(boardIdSubjectMap.containsKey(newAttendance.getBoardId()))
									{
										if(StringUtils.isNotEmpty(boardIdSubjectMap.get(newAttendance.getBoardId()).getName())){
											newAttendance.setSubject(boardIdSubjectMap.get(newAttendance.getBoardId()).getName());
										}
									}
									else {
										logger.info("Not able to Map board for boardId : " + newAttendance.getBoardId());
									}
								}
							}
							newAttendances.add(newAttendance);
							if(StringUtils.isNotEmpty(newAttendance.getSubject()) && totalString.equalsIgnoreCase(newAttendance.getSubject())){
								totalExists = Boolean.TRUE;
							}
							totalAtt.setTotal(totalAtt.getTotal()+newAttendance.getTotal());
						}
						if(!totalExists){
							newAttendances.add(0, totalAtt);
						}
						otfDashboard.setOtfAttendances(newAttendances);
					}
				}
			}
			if (enrollmentInstallment != null && enrollmentInstallment.containsKey(enrollInfo.getEnrollmentId())) {
				otfDashboard.setOtfBasicInstalmentInfos(enrollmentInstallment.get(enrollInfo.getEnrollmentId()));
			}
			if (studentContentMap != null && studentContentMap.containsKey(enrollInfo.getUserId())) {
				List<StudentContentPojo> contentPojos = studentContentMap.get(enrollInfo.getUserId());
				StudentContentPojo totalContent = new StudentContentPojo();
				totalContent.setSubject(totalString);
				for(StudentContentPojo content : contentPojos){
					totalContent.setShared(totalContent.getShared()+content.getShared());
					totalContent.setAttempted(totalContent.getAttempted()+content.getAttempted());
					totalContent.setEvaluated(totalContent.getEvaluated()+content.getEvaluated());
				}
				contentPojos.add(0, totalContent);
				otfDashboard.setContents(contentPojos);
			}
			if (batchInfo.getTeacherIds() != null) {
				List<Long> teacherIds = new ArrayList<>();
				for (String id : batchInfo.getTeacherIds()) {
					teacherIds.add(Long.parseLong(id));
				}
				otfDashboard.setTeacherIds(teacherIds);
			}
			if (enrollmentOrders != null && enrollmentOrders.containsKey(enrollInfo.getEnrollmentId())) {
				// TODO
				Orders orderTemp = enrollmentOrders.get(enrollInfo.getEnrollmentId());
				otfDashboard.setRegistrationOrderId(orderTemp.getId());
				otfDashboard.setRegistrationCreationTime(orderTemp.getCreationTime());
				otfDashboard.setRegistrationAmount(orderTemp.getAmount());
				if (orderTemp.getItems().get(0).getRefundStatus() != null) {
					otfDashboard.setRegistrationRefundStatus(orderTemp.getItems().get(0).getRefundStatus().toString());
				}
			}
			otfDashboards.add(otfDashboard);
		}
		return otfDashboards;
	}

	public Map<String, List<OTFAttendance>> getAttendanceMapping(String batchId) throws VException {
		Map<String, List<OTFAttendance>> enrollmentAttendance = null;
		try {
			String attendUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getAttendanceForBatch?batchId=" + batchId
					+ "&beforeEndTime=" + System.currentTimeMillis();
			ClientResponse attendResp = WebUtils.INSTANCE.doCall(attendUrl, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(attendResp);
			String jsonString1 = attendResp.getEntity(String.class);
			Type mapType = new TypeToken<Map<String, List<OTFAttendance>>>() {
			}.getType();
			enrollmentAttendance = new Gson().fromJson(jsonString1, mapType);
		} catch (NotFoundException e) {
			logger.info("attendance info not found for batchId " + batchId);
		}
		return enrollmentAttendance;
	}

	public Map<String, List<DashBoardInstalmentInfo>> getEnrollmentInstallmentMap(List<EnrollmentPojo> enrollmentInfos)
			throws VException {
		Map<String, List<DashBoardInstalmentInfo>> enrollmentInstallment = null;
		try {
			int regularCount = 0;                        
                        List<String> ids=new ArrayList<>();
			String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
			String url = "/payment/getInstallmentforBatches";
			if (ArrayUtils.isNotEmpty(enrollmentInfos)) {
				if (EnrollmentState.REGULAR.equals(enrollmentInfos.get(0).getState())) {
					regularCount++;
				}
                                ids.add(enrollmentInfos.get(0).getEnrollmentId());
				for (int i = 1; i < enrollmentInfos.size(); i++) {
                                        ids.add(enrollmentInfos.get(i).getEnrollmentId());
					if (EnrollmentState.REGULAR.equals(enrollmentInfos.get(i).getState())) {
						regularCount++;
					}
				}
			}
			if (regularCount > 0) {
                                GetInstallmentforBatchesReq req=new GetInstallmentforBatchesReq();
                                req.setIds(ids);
				ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST, gson.toJson(req));
				VExceptionFactory.INSTANCE.parseAndThrowException(resp);
				String jsonString1 = resp.getEntity(String.class);
				Type mapType = new TypeToken<Map<String, List<DashBoardInstalmentInfo>>>() {
				}.getType();
				enrollmentInstallment = new Gson().fromJson(jsonString1, mapType);
			}
		} catch (Exception e) {
			logger.info("Installment info not found for batchId " + enrollmentInfos.get(0).getBatchId());
		}

		return enrollmentInstallment;
	}

	public Map<String, Orders> getEnrollmentRegisteredOrderMap(List<EnrollmentPojo> enrollmentInfos) throws VException {
		Map<String, Orders> enrollmentOrders = null;
		try {
			if(ArrayUtils.isEmpty(enrollmentInfos)){
				return enrollmentOrders;
			}
                        
                        List<String> ids=new ArrayList<>();
			String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
			String url = "/payment/getRegistrationforEnrollments";
			for (int i = 0; i < enrollmentInfos.size(); i++) {
				ids.add(enrollmentInfos.get(i).getEnrollmentId());
			}

                        GetRegistrationforEnrollmentsReq req=new GetRegistrationforEnrollmentsReq();
                        req.setIds(ids);
			ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST, gson.toJson(req));
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String jsonString1 = resp.getEntity(String.class);
			Type mapType = new TypeToken<Map<String, Orders>>() {
			}.getType();
			enrollmentOrders = new Gson().fromJson(jsonString1, mapType);

		} catch (Exception e) {
			logger.info("Order info not found for batchId " + enrollmentInfos.get(0).getBatchId());
		}
		return enrollmentOrders;
	}

	public Map<String, List<StudentContentPojo>> getStudentContentMap(List<String> studentIds, String batchId)
			throws VException {
		Map<String, List<StudentContentPojo>> studentContentMap = null;
		if (ArrayUtils.isNotEmpty(studentIds)) {
			try {
				String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
				GetContentSharedForDashboardReq req = new GetContentSharedForDashboardReq();
				req.setStudentIds(studentIds);
				req.setBatchId(batchId);
				String getcontentUrl = LMS_ENDPOINT + "getContentInfoForDashboard";
				ClientResponse resp = WebUtils.INSTANCE.doCall(getcontentUrl, HttpMethod.POST, gson.toJson(req), true);
				VExceptionFactory.INSTANCE.parseAndThrowException(resp);
				String jsonString1 = resp.getEntity(String.class);
				Type mapType = new TypeToken<Map<String, List<StudentContentPojo>>>() {
				}.getType();
				studentContentMap = new Gson().fromJson(jsonString1, mapType);
			} catch (Exception e) {
				logger.info("Content info not found for batchId " + batchId);
			}
		}
		return studentContentMap;
	}

	public List<GetOtfDashboardRes> getOTFDashboard(GetOTFDashboardReq req) {
		List<GetOtfDashboardRes> resp = new ArrayList<>();
		Query query = new Query();
		if (StringUtils.isNotEmpty(req.getUserId())) {
			query.addCriteria(Criteria.where("userId").is(req.getUserId()));
		}
		otfDashboardDAO.setFetchParameters(query, req);
		List<OTFDashboard> otfDashboard = otfDashboardDAO.runQuery(query, OTFDashboard.class);
		if (CollectionUtils.isEmpty(otfDashboard)) {
			return resp;
		}

		Set<Long> userIds = new HashSet<>();
		for (OTFDashboard dashboard : otfDashboard) {
			userIds.add(dashboard.getUserId());
			userIds.addAll(dashboard.getTeacherIds());
		}
		Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);

		for (OTFDashboard dashboard : otfDashboard) {
			GetOtfDashboardRes res = mapper.map(dashboard, GetOtfDashboardRes.class);
			if (usermap.containsKey(dashboard.getUserId())) {
				res.setUser(usermap.get(dashboard.getUserId()));
			}
			if (CollectionUtils.isNotEmpty(dashboard.getTeacherIds())) {
				List<UserBasicInfo> teachers = new ArrayList<>();
				for (Long id : dashboard.getTeacherIds()) {
					if (usermap.containsKey(id)) {
						teachers.add(usermap.get(id));
					}
				}
				res.setTeachers(teachers);
			}
			if (ArrayUtils.isNotEmpty(res.getOtfAttendances())) {
				for (OTFAttendance attendance : res.getOtfAttendances()) {
					if (attendance.getTeacherId() != null && usermap.containsKey(attendance.getTeacherId())) {
						attendance.setTeacher(usermap.get(attendance.getTeacherId()));
					}
				}
			}
			resp.add(res);
		}
		return resp;
	}

	public String viewAttendanceForUser(ViewAttendanceForUserBatchReq req) throws VException {

		String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
		String attendUrl = SCHEDULING_ENDPOINT + "/onetofew/session/viewAttendanceForUser?" + queryString;
		ClientResponse attendResp = WebUtils.INSTANCE.doCall(attendUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(attendResp);
		String jsonString1 = attendResp.getEntity(String.class);
		return jsonString1;
	}

	public PlatformBasicResponse exportOTFDashboard(HttpSessionData httpSessionData)
			throws IOException, AddressException, VException {
		Map<String, Object> payload = new HashMap<>();
		payload.put("sessionData", httpSessionData);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_OTF_DASHBOARD, payload);
		asyncTaskFactory.executeTask(params);
		return new PlatformBasicResponse();
	}

	public PlatformBasicResponse exportTeacherDashboard(HttpSessionData httpSessionData)
			throws IOException, AddressException, VException {
		Map<String, Object> payload = new HashMap<>();
		payload.put("sessionData", httpSessionData);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_TEACHER_DASHBOARD, payload);
		asyncTaskFactory.executeTask(params);
		return new PlatformBasicResponse();
	}

	// TeacherDashboard
	public Map<Long, TeacherSessionDashboard> getTeacherSessionOTOForDashboard(Long startTime, Long endTime)
			throws VException {
		Map<Long, TeacherSessionDashboard> teacherSessionDashboard = null;
		try {
			String sessionUrl = SCHEDULING_ENDPOINT + "/session/getTeacherSessionForDashboard?startTime=" + startTime
					+ "&endTime=" + endTime;
			ClientResponse sessionResp = WebUtils.INSTANCE.doCall(sessionUrl, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(sessionResp);
			String jsonString = sessionResp.getEntity(String.class);
			Type mapType = new TypeToken<Map<Long, TeacherSessionDashboard>>() {
			}.getType();
			teacherSessionDashboard = new Gson().fromJson(jsonString, mapType);
		} catch (VException e) {
			logger.info("getTeacherSessionForDashboard failed for OTO : " + e);
		}
		if (teacherSessionDashboard == null) {
			teacherSessionDashboard = new HashMap<>();
		}
		logger.info("getTeacherSessionForDashboard size " + teacherSessionDashboard.size());
		return teacherSessionDashboard;
	}

	public Map<Long, TeacherCourseStateCount> getTeacherCoursePlanForDashboard(Long startTime, Long endTime)
			throws VException {
		Map<Long, TeacherCourseStateCount> teacherCourseStateCount = null;
		try {
			String coursePlanUrl = SUBSCRIPTION_ENDPOINT + "/courseplan/getTeacherCoursePlanMapping?startTime="
					+ startTime + "&endTime=" + endTime;
			ClientResponse sessionResp = WebUtils.INSTANCE.doCall(coursePlanUrl, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(sessionResp);
			String jsonString = sessionResp.getEntity(String.class);
			Type mapType = new TypeToken<Map<Long, TeacherCourseStateCount>>() {
			}.getType();
			teacherCourseStateCount = new Gson().fromJson(jsonString, mapType);
		} catch (VException e) {
			logger.info("getTeacherCoursePlanForDashboard failed for OTO : " + e);
		}
		if (teacherCourseStateCount == null) {
			teacherCourseStateCount = new HashMap<>();
		}
		logger.info("getTeacherCoursePlanForDashboard size " + teacherCourseStateCount.size());
		return teacherCourseStateCount;
	}

	public Map<Long, TeacherSessionDashboard> getTeacherSessionOTFForDashboard(Long startTime, Long endTime)
			throws VException {
		Map<Long, TeacherSessionDashboard> teacherSessionDashboard = null;
		try {
			String otfSessionUrl = SCHEDULING_ENDPOINT + "/onetofew/session/getTeacherSessionForDashboard?startTime=" + startTime
					+ "&endTime=" + endTime;
			ClientResponse sessionResp = WebUtils.INSTANCE.doCall(otfSessionUrl, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(sessionResp);
			String jsonString = sessionResp.getEntity(String.class);
			Type mapType = new TypeToken<Map<Long, TeacherSessionDashboard>>() {
			}.getType();
			teacherSessionDashboard = new Gson().fromJson(jsonString, mapType);
		} catch (VException e) {
			logger.info("getTeacherSessionOTFForDashboard failed for OTF : " + e);
		}
		if (teacherSessionDashboard == null) {
			teacherSessionDashboard = new HashMap<>();
		}
		logger.info("getTeacherSessionOTFForDashboard size " + teacherSessionDashboard.size());
		return teacherSessionDashboard;
	}

	public Map<Long, TeacherCourseStateCount> getTeacherEnrollmentCountMapping(Long startTime, Long endTime)
			throws VException {
		Map<Long, TeacherCourseStateCount> teacherCourseStateCount = null;
		try {
			String otfSessionUrl = SUBSCRIPTION_ENDPOINT + "/dashboard/getTeacherEnrollmentCountMapping?startTime="
					+ startTime + "&endTime=" + endTime;
			ClientResponse sessionResp = WebUtils.INSTANCE.doCall(otfSessionUrl, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(sessionResp);
			String jsonString = sessionResp.getEntity(String.class);
			Type mapType = new TypeToken<Map<Long, TeacherSessionDashboard>>() {
			}.getType();
			teacherCourseStateCount = new Gson().fromJson(jsonString, mapType);
		} catch (VException e) {
			logger.info("getTeacherSessionForDashboard failed for OTF : " + e);
		}
		if (teacherCourseStateCount == null) {
			teacherCourseStateCount = new HashMap<>();
		}
		logger.info("getTeacherEnrollmentCountMapping size " + teacherCourseStateCount.size());
		return teacherCourseStateCount;
	}

	public Map<Long, List<TeacherContentCount>> getTeacherContentForDashboard(Long startTime, Long endTime)
			throws VException {
		Map<Long, List<TeacherContentCount>> teacherContentCount = null;
		try {
			String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
			String contentUrl = LMS_ENDPOINT + "/getTeacherContentForDashboard?startTime=" + startTime + "&endTime="
					+ endTime;
			ClientResponse sessionResp = WebUtils.INSTANCE.doCall(contentUrl, HttpMethod.GET, null, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(sessionResp);
			String jsonString = sessionResp.getEntity(String.class);
			Type mapType = new TypeToken<Map<Long, List<TeacherContentCount>>>() {
			}.getType();
			teacherContentCount = new Gson().fromJson(jsonString, mapType);
		} catch (VException e) {
			logger.info("getTeacherSessionForDashboard failed for OTF : " + e);
		}
		if (teacherContentCount == null) {
			teacherContentCount = new HashMap<>();
		}
		logger.info("getTeacherContentForDashboard size " + teacherContentCount.size());
		return teacherContentCount;
	}

	public void createTeacherDashboard() throws VException {
		Long day = new Long(DateTimeUtils.MILLIS_PER_DAY);
		TimeDateMonth timeDateMonth = new TimeDateMonth();
		Long endTime = timeDateMonth.getStartTimeOfDay(System.currentTimeMillis());
		// Long endTime = DateTimeUtils.getStartOfDay(new Date()).getTime();
		Long startTime = endTime - day;
                
                Map<String, Object> payload = new HashMap<>();
                payload.put("startTime", startTime);
                payload.put("endTime", endTime);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_TEACHER_DASHBOARD, payload);
                asyncTaskFactory.executeTask(params);                
	}

	public void updatePastTeacherDashboard(Long fromTime, Integer days) throws VException {
		Long day = new Long(DateTimeUtils.MILLIS_PER_DAY);
		TimeDateMonth timeDateMonth = new TimeDateMonth();
		Long startTime = timeDateMonth.getStartTimeOfDay(fromTime);
		// Long startTime = DateTimeUtils.getStartOfDay(new
		// Date(fromTime)).getTime();
		int counter = 0;
		int threadSize = 20;
		Long time5 = new Long(DateTimeUtils.MILLIS_PER_MINUTE * 5);
		if (days != null && days > 0 && days < 31) {
			for (int numberOfDays = 0; numberOfDays < days; numberOfDays++) {
				Long endTime = startTime + day;
				// TODO : create Async Task
				Map<String, Object> payload = new HashMap<>();
				payload.put("startTime", startTime);
				payload.put("endTime", endTime);
				Long time_delay = time5*(counter/threadSize);
				AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_TEACHER_DASHBOARD, payload,time_delay);
				asyncTaskFactory.executeTask(params);
				startTime = endTime;
				counter++;
			}
		}
	}
	
	public PlatformBasicResponse createTeacherDashboardAsync(){
		Long day = new Long(DateTimeUtils.MILLIS_PER_DAY);
		TimeDateMonth timeDateMonth = new TimeDateMonth();
		Long endTime = timeDateMonth.getStartTimeOfDay(System.currentTimeMillis());
		Long startTime = endTime - day;
		Map<String, Object> payload = new HashMap<>();
		payload.put("startTime", startTime);
		payload.put("endTime", endTime);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CREATE_TEACHER_DASHBOARD, payload);
		asyncTaskFactory.executeTask(params);
		return new PlatformBasicResponse();
	}

	public List<TeacherListWithStatusRes> getTeachersForDashboard() throws VException {
		List<TeacherListWithStatusRes> teacherIds = null;
		try {
			String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

			ClientResponse sessionResp = WebUtils.INSTANCE.doCall(userEndpoint + "/getActiveTeacherIdList",
                            HttpMethod.GET, null, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(sessionResp);
			String jsonString = sessionResp.getEntity(String.class);
			Type listType = new TypeToken<ArrayList<TeacherListWithStatusRes>>() {
			}.getType();
			teacherIds = new Gson().fromJson(jsonString, listType);
		} catch (VException e) {
			logger.info("Error in fetching teacherIds : " + e);
		}

		if (teacherIds == null) {
			teacherIds = new ArrayList<>();
		}
		return teacherIds;
	}

	public void createTeacherDashboard(Long startTime, Long endTime) throws VException {

		Long day = new Long(DateTimeUtils.MILLIS_PER_DAY);
		if ((endTime - startTime) > day) {
			int days = (int) ((endTime - startTime)/day);
			updatePastTeacherDashboard(startTime,days);
			return;
		}

		List<TeacherListWithStatusRes> teacherIds = getTeachersForDashboard();

		Map<Long, TeacherSessionDashboard> teacherSessionOTOMap = getTeacherSessionOTOForDashboard(startTime, endTime);
		Map<Long, TeacherCourseStateCount> teacherCoursePlanMap = getTeacherCoursePlanForDashboard(startTime, endTime);
		Map<Long, TeacherSessionDashboard> teacherSessionOTFMap = getTeacherSessionOTFForDashboard(startTime, endTime);
		Map<Long, TeacherCourseStateCount> teacherEnrollmentMap = getTeacherEnrollmentCountMapping(startTime, endTime);
		Map<Long, List<TeacherContentCount>> teacherContentListMap = getTeacherContentForDashboard(startTime,
				endTime);

		List<TeacherDailyDashboard> teacherDailyDashboards = new ArrayList<>();
		int counter = 0;
		if (ArrayUtils.isNotEmpty(teacherIds)) {
			for (TeacherListWithStatusRes res : teacherIds) {
				// TeacherDailyDashboard coursePlan = new
				// TeacherDailyDashboard(res.getTeacherId(),
				// EntityType.COURSE_PLAN,
				// startTime, res.getCurrentStatus());
				// TeacherDailyDashboard otf = new
				// TeacherDailyDashboard(res.getTeacherId(), EntityType.OTF,
				// startTime,
				// res.getCurrentStatus());
				TeacherDailyDashboard teacherDailyDashboard = new TeacherDailyDashboard(res.getTeacherId(), startTime,
						res.getCurrentStatus());
				teacherDailyDashboard.setPrimarySubject(res.getPrimarySubject());
				TeacherDashboardByEntityInfo coursePlan = new TeacherDashboardByEntityInfo(EntityType.COURSE_PLAN);
				TeacherDashboardByEntityInfo otf = new TeacherDashboardByEntityInfo(EntityType.OTF);
				TeacherSessionDashboard teacherSessionOTO = teacherSessionOTOMap.get(res.getTeacherId());
				TeacherCourseStateCount teacherCountCoursePlan = teacherCoursePlanMap.get(res.getTeacherId());
				TeacherSessionDashboard teacherSessionOTF = teacherSessionOTFMap.get(res.getTeacherId());
				TeacherCourseStateCount teacherCountEnrollment = teacherEnrollmentMap.get(res.getTeacherId());
				List<TeacherContentCount> teacherContent = teacherContentListMap.get(res.getTeacherId());

				if (teacherSessionOTO != null) {
					coursePlan.setBooked(teacherSessionOTO.getBooked());
					// TODO: change based on new cancel logic
					coursePlan.setCancelledByTeacher(teacherSessionOTO.getCancelledByTeacher());
					coursePlan.setCancelledByStudent(teacherSessionOTO.getCancelledByStudent());
					coursePlan.setCancelledByAdmin(teacherSessionOTO.getCancelledByAdmin());
					coursePlan.setCancelled(teacherSessionOTO.getCancelled());
					coursePlan.setExpired(teacherSessionOTO.getExpired());
					coursePlan.setSessionEnded(teacherSessionOTO.getEnded());
					coursePlan.setForfeited(teacherSessionOTO.getForfeited());
					coursePlan.setLateJoined(teacherSessionOTO.getLateJoined());
					coursePlan.setStudentIds(teacherSessionOTO.getStudentIds());
					coursePlan.setRescheduled(teacherSessionOTO.getRescheduled());
				}
				if (teacherCountCoursePlan != null) {
					coursePlan.setNewCourse(teacherCountCoursePlan.getPublished());
					coursePlan.setEnrolled(teacherCountCoursePlan.getEnrolled());
					coursePlan.setCourseEnded(teacherCountCoursePlan.getEnded());

					coursePlan.setCourseEndedStudents(teacherCountCoursePlan.getEndedStudentIds());
				}
				if (teacherSessionOTF != null) {
					otf.setBooked(teacherSessionOTF.getBooked());
					otf.setCancelledByAdmin(teacherSessionOTF.getCancelled());
					otf.setCancelled(teacherSessionOTF.getCancelled());
					if (ArrayUtils.isNotEmpty(teacherSessionOTF.getAttendees())) {

						teacherSessionOTF.getAttendees().remove(res.getTeacherId().toString());
						otf.setAttendees(teacherSessionOTF.getAttendees());
					}
					otf.setSessionEnded(teacherSessionOTF.getEnded());
					otf.setExpired(teacherSessionOTF.getExpired());
					otf.setForfeited(teacherSessionOTF.getForfeited());
					otf.setTeacherNoShow(teacherSessionOTF.getTeacherNoShow());
					otf.setRescheduled(teacherSessionOTF.getRescheduled());
					otf.setLateJoined(teacherSessionOTF.getLateJoined());
					otf.setCancelledByTeacher(teacherSessionOTF.getCancelledByTeacher());
					otf.setCancelledByStudent(teacherSessionOTF.getCancelledByStudent());
				}
				if (teacherCountEnrollment != null) {
					otf.setNewCourse(teacherCountEnrollment.getPublished());
					otf.setEnrolled(teacherCountEnrollment.getEnrolled());
					otf.setCourseEnded(teacherCountEnrollment.getEnded());

					otf.setCourseEndedStudents(teacherCountEnrollment.getEndedStudentIds());
				}
				if (ArrayUtils.isNotEmpty(teacherContent)) {
					for (TeacherContentCount contentCount : teacherContent) {
						if (contentCount.getContextType().equals(EntityType.OTF)) {
							otf.setContentShared(contentCount.getShared());
							otf.setContentAttempted(contentCount.getAttempted());
							otf.setContentEvaluated(contentCount.getEvaluated());
						} else if (contentCount.getContextType().equals(EntityType.COURSE_PLAN)) {
							coursePlan.setContentShared(contentCount.getShared());
							coursePlan.setContentAttempted(contentCount.getAttempted());
							coursePlan.setContentEvaluated(contentCount.getEvaluated());
						}
					}
				}
				teacherDailyDashboard.setCoursePlan(coursePlan);
				teacherDailyDashboard.setOtf(otf);
				teacherDailyDashboards.add(teacherDailyDashboard);
				counter++;
				if (counter >= 50) {
					counter = 0;
					teacherDailyDashboardDAO.updateAll(teacherDailyDashboards);
					teacherDailyDashboards = new ArrayList<TeacherDailyDashboard>();
				}
			}
		}
	}

	public List<GetTeacherDashboardResp> getTeacherDashboard(GetTeacherDashboardReq req) {

		List<GetTeacherDashboardResp> getTeacherDashboardResps = new ArrayList<>();
		Query query = new Query();
		Boolean teacherReq = Boolean.TRUE; // if TeacherIds are present directly
											// fetch last 30 days report
		TimeDateMonth timeDateMonth = new TimeDateMonth();
		Long currentDay = timeDateMonth.getStartTimeOfDay(System.currentTimeMillis());
		if (ArrayUtils.isNotEmpty(req.getTeacherIds())) {
			query.addCriteria(Criteria.where(TeacherDailyDashboard.Constants.TEACHER_ID).in(req.getTeacherIds()));

			Long startTime = currentDay - ((new Long(DateTimeUtils.MILLIS_PER_DAY)) * 30l);
			query.addCriteria(Criteria.where(TeacherDailyDashboard.Constants.START_TIME).gte(startTime));
			teacherReq = Boolean.FALSE;
		}
		if (req.getTeacherStatus() != null) {
			query.addCriteria(
					Criteria.where(TeacherDailyDashboard.Constants.TEACHER_STATUS).in(req.getTeacherStatus()));
		}
		if (teacherReq) {
			Long endTime = currentDay;
			Long startTime = endTime - (new Long(DateTimeUtils.MILLIS_PER_DAY));
			query.addCriteria(Criteria.where(TeacherDailyDashboard.Constants.START_TIME).is(startTime));
			teacherDailyDashboardDAO.setFetchParameters(query, req);
			query.with(Sort.by(Sort.Direction.DESC, TeacherDailyDashboard.Constants.CREATION_TIME));
			List<TeacherDailyDashboard> teacherDailyDashboardTemp = teacherDailyDashboardDAO.runQuery(query,
					TeacherDailyDashboard.class);
			if (ArrayUtils.isEmpty(teacherDailyDashboardTemp)) {
				return getTeacherDashboardResps;
			}
			List<Long> teacherIds = new ArrayList<>();
			for (TeacherDailyDashboard dailyDashboard : teacherDailyDashboardTemp) {
				teacherIds.add(dailyDashboard.getTeacherId());
			}
			GetTeacherDashboardReq dashboardReq = new GetTeacherDashboardReq();
			dashboardReq.setTeacherIds(teacherIds);
			dashboardReq.setTeacherStatus(req.getTeacherStatus());
			return getTeacherDashboard(dashboardReq);
		}
		query.with(Sort.by(Sort.Direction.ASC, TeacherDailyDashboard.Constants.START_TIME));
		List<TeacherDailyDashboard> teacherDailyDashboards = teacherDailyDashboardDAO.runQuery(query,
				TeacherDailyDashboard.class);

		if (ArrayUtils.isEmpty(teacherDailyDashboards)) {
			return getTeacherDashboardResps;
		}
		Map<Long, GetTeacherDashboardResp> map = new HashMap<>();
		Set<Long> teacherIds = new HashSet<>();
		for (TeacherDailyDashboard teacherDailyDashboard : teacherDailyDashboards) {
			teacherIds.add(teacherDailyDashboard.getTeacherId());
			GetTeacherDashboardResp resp;
			Boolean last7Days = Boolean.FALSE;

			if (teacherDailyDashboard.getStartTime() > (currentDay - DateTimeUtils.MILLIS_PER_WEEK)) {
				last7Days = Boolean.TRUE;
			}
			if (map.containsKey(teacherDailyDashboard.getTeacherId())) {
				resp = map.get(teacherDailyDashboard.getTeacherId());
			} else {
				resp = mapper.map(teacherDailyDashboard, GetTeacherDashboardResp.class);
			}
			incrementGetTeacherDashboardResp(teacherDailyDashboard, resp, last7Days);
			map.put(teacherDailyDashboard.getTeacherId(), resp);
		}

		Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(teacherIds, true);

		for (Entry<Long, GetTeacherDashboardResp> entry : map.entrySet()) {
			GetTeacherDashboardResp resp = entry.getValue();
			if (resp.getCoursePlan().getStudentIds() != null) {
				resp.setCoursePlanActiveStudents(resp.getCoursePlan().getStudentIds().size());
			}
			if (resp.getOtf().getAttendees() != null) {
				resp.setOtfActiveStudents(resp.getOtf().getAttendees().size());
			}
			if (resp.getCoursePlan().getCourseEndedStudents() != null) {
				resp.setCoursePlanChurn(resp.getCoursePlan().getCourseEndedStudents().size());
			}
			if (resp.getOtf().getCourseEndedStudents() != null) {
				resp.setOtfChurn(resp.getOtf().getCourseEndedStudents().size());
			}
			if (usermap.containsKey(resp.getTeacherId())) {
				resp.setTeacher(usermap.get(resp.getTeacherId()));
			}
			getTeacherDashboardResps.add(resp);
		}
		return getTeacherDashboardResps;
	}

	public void incrementGetTeacherDashboardResp(TeacherDailyDashboard teacherDailyDashboard,
			GetTeacherDashboardResp resp, Boolean last7Days) {
		if (teacherDailyDashboard.getCoursePlan() != null) {
			TeacherDashboardByEntityInfo coursePlanResp = resp.getCoursePlan();
			TeacherDashboardByEntityInfo coursePlanEntity = teacherDailyDashboard.getCoursePlan();

			coursePlanResp.setBooked(coursePlanResp.getBooked() + coursePlanEntity.getBooked());
			coursePlanResp.setCancelled(coursePlanResp.getCancelled() + coursePlanEntity.getCancelled());
			coursePlanResp.setCancelledByTeacher(
					coursePlanResp.getCancelledByTeacher() + coursePlanEntity.getCancelledByTeacher());
			coursePlanResp.setRescheduled(coursePlanResp.getRescheduled() + coursePlanEntity.getRescheduled());
			coursePlanResp.setSessionEnded(coursePlanResp.getSessionEnded() + coursePlanEntity.getSessionEnded());
			coursePlanResp.setCancelledByStudent(
					coursePlanResp.getCancelledByStudent() + coursePlanEntity.getCancelledByStudent());
			coursePlanResp
					.setCancelledByAdmin(coursePlanResp.getCancelledByAdmin() + coursePlanEntity.getCancelledByAdmin());
			coursePlanResp.setLateJoined(coursePlanResp.getLateJoined() + coursePlanEntity.getLateJoined());
			coursePlanResp.setContentShared(coursePlanResp.getContentShared() + coursePlanEntity.getContentShared());
			coursePlanResp
					.setContentAttempted(coursePlanResp.getContentAttempted() + coursePlanEntity.getContentAttempted());
			coursePlanResp
					.setContentEvaluated(coursePlanResp.getContentEvaluated() + coursePlanEntity.getContentEvaluated());
			if (last7Days) {
				coursePlanResp.setNewCourse(coursePlanResp.getNewCourse() + coursePlanEntity.getNewCourse());
				coursePlanResp.setEnrolled(coursePlanResp.getEnrolled() + coursePlanEntity.getEnrolled());
				coursePlanResp.setCourseEnded(coursePlanResp.getCourseEnded() + coursePlanEntity.getCourseEnded());
			}
			if (coursePlanResp.getStudentIds() == null) {
				coursePlanResp.setStudentIds(new HashSet<>());
			}
			if (coursePlanResp.getCourseEndedStudents() == null) {
				coursePlanResp.setCourseEndedStudents(new HashSet<>());
			}
			if (ArrayUtils.isNotEmpty(coursePlanEntity.getStudentIds())) {
				// coursePlanResp.getStudentIds().addAll(coursePlanEntity.getStudentIds());
				for (Long id : coursePlanEntity.getStudentIds()) {
					coursePlanResp.getStudentIds().add(id);
					if (coursePlanResp.getCourseEndedStudents().contains(id)) {
						coursePlanResp.getCourseEndedStudents().remove(id);
					}
				}
			}
			if (ArrayUtils.isNotEmpty(coursePlanEntity.getCourseEndedStudents())) {
				coursePlanResp.getCourseEndedStudents().addAll(coursePlanEntity.getCourseEndedStudents());
			}
		}

		if (teacherDailyDashboard.getOtf() != null) {
			TeacherDashboardByEntityInfo otfResp = resp.getOtf();
			TeacherDashboardByEntityInfo otfEntity = teacherDailyDashboard.getOtf();

			otfResp.setBooked(otfResp.getBooked() + otfEntity.getBooked());
			otfResp.setCancelled(otfResp.getCancelled() + otfEntity.getCancelled());
			otfResp.setCancelledByTeacher(otfResp.getCancelledByTeacher() + otfEntity.getCancelledByTeacher());
			otfResp.setRescheduled(otfResp.getRescheduled() + otfEntity.getRescheduled());
			otfResp.setSessionEnded(otfResp.getSessionEnded() + otfEntity.getSessionEnded());
			otfResp.setCancelledByStudent(otfResp.getCancelledByStudent() + otfEntity.getCancelledByStudent());
			otfResp.setCancelledByAdmin(otfResp.getCancelledByAdmin() + otfEntity.getCancelledByAdmin());
			otfResp.setLateJoined(otfResp.getLateJoined() + otfEntity.getLateJoined());
			otfResp.setContentShared(otfResp.getContentShared() + otfEntity.getContentShared());
			otfResp.setContentAttempted(otfResp.getContentAttempted() + otfEntity.getContentAttempted());
			otfResp.setContentEvaluated(otfResp.getContentEvaluated() + otfEntity.getContentEvaluated());
			if (last7Days) {
				otfResp.setNewCourse(otfResp.getNewCourse() + otfEntity.getNewCourse());
				otfResp.setEnrolled(otfResp.getEnrolled() + otfEntity.getEnrolled());
				otfResp.setCourseEnded(otfResp.getCourseEnded() + otfEntity.getCourseEnded());
			}
			if (otfResp.getStudentIds() == null) {
				otfResp.setAttendees(new HashSet<>());
			}
			if (otfResp.getCourseEndedStudents() == null) {
				otfResp.setCourseEndedStudents(new HashSet<>());
			}
			if (ArrayUtils.isNotEmpty(otfEntity.getStudentIds())) {
				// otfResp.getAttendees().addAll(otfEntity.getAttendees());
				for (String id : otfEntity.getAttendees()) {
					otfResp.getAttendees().add(id);
					if (otfResp.getCourseEndedStudents().contains(Long.parseLong(id))) {
						otfResp.getCourseEndedStudents().remove(Long.parseLong(id));
					}
				}
			}
			if (ArrayUtils.isNotEmpty(otfEntity.getCourseEndedStudents())) {
				otfResp.getCourseEndedStudents().addAll(otfEntity.getCourseEndedStudents());
			}
		}
	}
}
