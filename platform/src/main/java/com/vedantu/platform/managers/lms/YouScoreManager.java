package com.vedantu.platform.managers.lms;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.youscore.pojo.YouScoreConstants;
import com.vedantu.lms.youscore.pojo.YouScoreCookie;
import com.vedantu.lms.youscore.pojo.YouScoreOTFContentShareReq;
import com.vedantu.lms.youscore.pojo.YouScoreTokenRes;
import com.vedantu.lms.youscore.request.YouScoreGetTokenReq;
import com.vedantu.lms.youscore.request.YouScoreMappingEntry;
import com.vedantu.lms.youscore.request.YouScorePackagePaymentReq;
import com.vedantu.lms.youscore.request.YouScoreRegisterStudentReq;
import com.vedantu.lms.youscore.request.YouScoreRegisterTeacherReq;
import com.vedantu.lms.youscore.request.YouScoreStudentTeacherMappingReq;
import com.vedantu.offering.request.GetOfferingRequest;
import com.vedantu.offering.response.GetOfferingResponse;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.AgendaPojo;
import com.vedantu.onetofew.pojo.ContentInfo;
import com.vedantu.onetofew.pojo.ContentPojo;
import com.vedantu.platform.managers.offering.OfferingManager;
import com.vedantu.platform.pojo.youscore.YouScoreBasicRes;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.WebUtils;

@Service
public class YouScoreManager extends YouScoreConstants {

	@Autowired
	private HttpSessionUtils sessionUtils;

	@Autowired
	private OfferingManager offeringManager;

	@Autowired
	private PlatformTools platformTools;

	@Autowired
	private FosUtils fosUtils;

	private static Gson gson = new Gson();

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(YouScoreManager.class);

	private String lmsEndpoint;

	private String ontofewEndpoint;

	private static final Type youScoreBasicResListType = new TypeToken<List<YouScoreBasicRes>>() {
	}.getType();

	@PostConstruct
	public void init() {
		lmsEndpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
		ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("OTF_ENDPOINT");
		youScoreContainerUrl = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT") + "/static/html/youscore/index.jsp";
	}

	public YouScoreTokenRes validateVedantuLogin(HttpServletRequest request, HttpServletResponse response)
			throws ForbiddenException, InternalServerErrorException, BadRequestException {
		HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
		Long userId = sessionData.getUserId();
		Role role = sessionData.getRole();

		if (userId == null || userId <= 0l || role == null
				|| (!Role.STUDENT.equals(role) && !Role.TEACHER.equals(role))) {
			throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user.");
		}

		Long currentTime = System.currentTimeMillis();
		try {
			String token = "";
			switch (role) {
			case STUDENT:
				token = generateStudentToken(String.valueOf(userId), role);
				break;
			case TEACHER:
				token = generateTeacherToken(String.valueOf(userId), role);
				break;
			default:
				throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user");
			}

			String expiryTime = String.valueOf(currentTime + YouScoreManager.expiryDuration);
			request.setAttribute("ysToken", token);
			request.setAttribute("expiryDuration", expiryTime);
			YouScoreTokenRes res = new YouScoreTokenRes(token, expiryTime);

			// Set cookie for validation for future requests
			YouScoreCookie youScoreCookie = new YouScoreCookie(String.valueOf(userId), role.name(), token, expiryTime);
			Cookie cookie = new Cookie(YOUSCORE_COOKIE, URLEncoder.encode(youScoreCookie.getJsonString(), "UTF-8"));
			response.addCookie(cookie);
			return res;
		} catch (Exception ex) {
			throw new BadRequestException(ErrorCode.YOUSCORE_LOGIN_ERROR,
					"Error authenticating the user with Youscore" + ex.getMessage());
		}
	}

	private String generateStudentToken(String userId, Role role)
			throws InternalServerErrorException, IllegalArgumentException, IllegalAccessException {

		// Fetch token
		YouScoreGetTokenReq req = new YouScoreGetTokenReq(generateUserEmailId(userId), role);
		String targetUrl = youScoreGetTokenUrl + "?" + req.createQueryString();
		PlatformBasicResponse platformResponse = platformTools.postPlatformData(targetUrl, null, true, HttpMethod.GET);
		String response = platformResponse.getResponse();
		List<YouScoreBasicRes> youScoreResponse = parseYouScoreBasicRes(response);
		if (CollectionUtils.isEmpty(youScoreResponse)) {
			logger.info("Youscore student login failed for user : " + userId + " response : " + response);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "No response from youscore" + response);
		}

		User user = fosUtils.getUserInfo(Long.parseLong(userId),false);
		YouScoreBasicRes res = youScoreResponse.get(0);
		String token = "";
		switch (res.getStatus()) {
		case "0":
			// Register the user
			YouScoreRegisterStudentReq youScoreRegisterStudentReq = new YouScoreRegisterStudentReq(
					generateUserEmailId(userId), userId, user.getFullName());
			targetUrl = youScoreRegisterTokenUrl + "?" + youScoreRegisterStudentReq.createQueryString();
			PlatformBasicResponse platformBasicResponse = platformTools.postPlatformData(targetUrl, null, true,
					HttpMethod.GET);
			String registrationResponse = platformBasicResponse.getResponse();
			List<YouScoreBasicRes> youScoreRegistrationResponse = parseYouScoreBasicRes(registrationResponse);
			if (CollectionUtils.isEmpty(youScoreRegistrationResponse)) {
				logger.info("Youscore student registration failed for user : " + userId + " response : "
						+ registrationResponse);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
						"No response from youscore" + registrationResponse);
			}
			YouScoreBasicRes registrationRes = youScoreRegistrationResponse.get(0);
			try {
				// Mark the student as paid
				YouScorePackagePaymentReq youScorePackagePaymentReq = new YouScorePackagePaymentReq(
						generateUserEmailId(userId));
				targetUrl = youScorePackagePaymentUrl + "?" + youScorePackagePaymentReq.createQueryString();
				logger.info("YouScorePackagePaymentReq - targetUrl : " + targetUrl);
				PlatformBasicResponse youScorePackagePaymentRes = platformTools.postPlatformData(targetUrl, null, true,
						HttpMethod.GET);
				logger.info("Response : " + youScorePackagePaymentRes.toString());
			} catch (Exception ex) {
				logger.info("Youscore student package payment failed for user : " + userId + " response : "
						+ ex.toString());
			}

			switch (registrationRes.getStatus()) {
			case "0":
			case "-1":
				logger.info("Youscore app id invalid " + targetUrl + " registrationRes: " + registrationRes);
				break;
			default:
				token = registrationRes.getStatus();
				break;
			}
			break;
		case "-1":
			// TODO : throw sentry error
			logger.info("Youscore appi id invalid " + targetUrl + " res" + res);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
					"Error occured while fetching token from youscore");
		default:
			token = res.getStatus();
			break;
		}
		return token;
	}

	private String generateTeacherToken(String userId, Role role)
			throws InternalServerErrorException, IllegalArgumentException, IllegalAccessException {

		// Fetch token
		YouScoreGetTokenReq req = new YouScoreGetTokenReq(generateUserEmailId(userId), role);
		String targetUrl = youScoreGetTokenUrl + "?" + req.createQueryString();
		PlatformBasicResponse platformResponse = platformTools.postPlatformData(targetUrl, null, true, HttpMethod.GET);
		String response = platformResponse.getResponse();
		List<YouScoreBasicRes> youScoreResponse = parseYouScoreBasicRes(response);
		if (CollectionUtils.isEmpty(youScoreResponse)) {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "No response from youscore" + response);
		}

		User user = fosUtils.getUserInfo(Long.parseLong(userId), false);
		YouScoreBasicRes res = youScoreResponse.get(0);
		String token = "";
		switch (res.getStatus()) {
		case "0":
			// Register the teacher
			YouScoreRegisterTeacherReq youScoreRegisterTeacherReq = new YouScoreRegisterTeacherReq(
					generateUserEmailId(userId), userId, user.getFullName());
			targetUrl = youScoreRegisterTeacherTokenUrl + "?" + youScoreRegisterTeacherReq.createQueryString();
			PlatformBasicResponse platformBasicResponse = platformTools.postPlatformData(targetUrl, null, true,
					HttpMethod.GET);
			String registrationResponse = platformBasicResponse.getResponse();
			List<YouScoreBasicRes> youScoreRegistrationResponse = parseYouScoreBasicRes(registrationResponse);
			if (CollectionUtils.isEmpty(youScoreRegistrationResponse)) {
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
						"No response from youscore" + registrationResponse);
			}
			YouScoreBasicRes registrationRes = youScoreRegistrationResponse.get(0);
			switch (registrationRes.getStatus()) {
			case "0":
			case "-1":
				// TODO : throw sentry error
				logger.info("Youscore appi id invalid " + targetUrl + " registrationRes: " + registrationRes);
				break;
			default:
				token = registrationRes.getStatus();
				// TODO : Create mapping task
				break;
			}
			break;
		case "-1":
			// TODO : throw sentry error
			logger.info("Youscore appi id invalid " + targetUrl + " res" + res);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
					"Error occured while fetching token from youscore");
		default:
			token = res.getStatus();
			break;
		}
		return token;
	}

	public void redirectToTeacherPortal(HttpServletRequest request, HttpServletResponse response)
			throws ForbiddenException, InternalServerErrorException, BadRequestException, IllegalArgumentException,
			IllegalAccessException, IOException {
		HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
		Long userId = sessionData.getUserId();
		Role userRole = sessionData.getRole();
		logger.info("redirectToTeacherPortal- " + userId + " sessionUserRole:" + userRole);
		if (userId == null || userId <= 0l || !Role.TEACHER.equals(userRole)) {
			throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user");
		}

		// Validate cookie
		YouScoreCookie youScoreCookie = getValidCookieFromRequest(request);
		if (youScoreCookie == null) {
			YouScoreTokenRes youScoreTokenRes = validateVedantuLogin(request, response);
			youScoreCookie = new YouScoreCookie(String.valueOf(userId), userRole.name(), youScoreTokenRes.getToken(),
					youScoreTokenRes.getExpiryTime());

			// Set cookie for validation for future requests
			Cookie cookie = new Cookie(YOUSCORE_COOKIE, URLEncoder.encode(youScoreCookie.getJsonString(), "UTF-8"));
			response.addCookie(cookie);
		}

		String youScoreTargetUrl = String.format(YOUSCORE_TEACHER_PORTAL_URL, youScoreCookie.getToken(), userId,
				youScoreCookie.getExpiryTime());

		logger.info("youScoreTargetUrl :" + youScoreTargetUrl);
		String containerUrl = youScoreContainerUrl + "?" + "iframesrc=" + URLEncoder.encode(youScoreTargetUrl, "UTF-8");
		logger.info("containerUrl :" + containerUrl);
		response.sendRedirect(containerUrl);
	}

	public YouScoreCookie createCookie(HttpServletRequest request, HttpServletResponse response)
			throws ForbiddenException, InternalServerErrorException, BadRequestException, IllegalArgumentException,
			IllegalAccessException, IOException {
		HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
		Long userId = sessionData.getUserId();
		Role userRole = sessionData.getRole();
		logger.info("createCookie- " + userId + " sessionUserRole:" + userRole);
		if (userId == null || userId <= 0l) {
			throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user");
		}

		// Validate cookie
		YouScoreCookie youScoreCookie = getValidCookieFromRequest(request);
		if (youScoreCookie == null) {
			YouScoreTokenRes youScoreTokenRes = validateVedantuLogin(request, response);
			youScoreCookie = new YouScoreCookie(String.valueOf(userId), userRole.name(), youScoreTokenRes.getToken(),
					youScoreTokenRes.getExpiryTime());
		}

		return youScoreCookie;
	}

	public void createStudentTeacherMapping(HttpServletRequest request) throws BadRequestException {
		String studentStr = request.getParameter("studentId");
		String teacherStr = request.getParameter("teacherId");
		if (StringUtils.isEmpty(studentStr) || StringUtils.isEmpty(teacherStr)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student id or teacher id is missing");
		}

		Long studentId = Long.parseLong(studentStr);
		Long teacherId = Long.parseLong(teacherStr);
		createStudentTeacherMapping(studentId, teacherId, true);
	}

	public void createStudentTeacherMapping(Long studentId, Long teacherId, boolean createUsers)
			throws BadRequestException {
		logger.info("studentId : " + studentId + " teacherId:" + teacherId + " createUsers: " + createUsers);
		if (createUsers) {
			try {
				generateTeacherToken(String.valueOf(teacherId), Role.TEACHER);
			} catch (Exception ex) {
				logger.info("createStudentTeacherMappingError - teacher account creation failed" + studentId
						+ " teacherId:" + teacherId);
			}
			try {
				generateStudentToken(String.valueOf(studentId), Role.STUDENT);
			} catch (Exception ex) {
				logger.info("createStudentTeacherMappingError - student account creation failed" + studentId
						+ " teacherId:" + teacherId);
			}
		}
		createStudentTeacherMapping(studentId, teacherId);
	}

	public void createStudentTeacherMapping(List<Long> studentIds, Long teacherId) {
		if (!CollectionUtils.isEmpty(studentIds) && teacherId != null) {
			for (Long studentId : studentIds) {
				createStudentTeacherMapping(studentId, teacherId);
			}
		}
	}

	public void createStudentTeacherMapping(Long studentId, Long teacherId) {
		logger.info("createStudentTeacherMapping - " + studentId + " teacherId" + teacherId);
		if (studentId == null || studentId <= 0l || teacherId == null || teacherId <= 0l) {
			logger.info("createStudentTeacherMapping-Invalid student id and teacherId" + studentId + " " + teacherId);
		}
		YouScoreStudentTeacherMappingReq youScoreStudentTeacherMappingReq = new YouScoreStudentTeacherMappingReq();
		youScoreStudentTeacherMappingReq.addMapping(new YouScoreMappingEntry(studentId, teacherId));
		String body = new Gson().toJson(youScoreStudentTeacherMappingReq);
		PlatformBasicResponse platformBasicResponse = platformTools.postPlatformData(youScoreStudentTeacherMappingUrl,
				body, true, HttpMethod.POST);
		String response = platformBasicResponse.getResponse();
		logger.info("response :" + response);
		boolean success = false;
		if (!StringUtils.isEmpty(response)) {
			List<YouScoreBasicRes> youScoreBasicRes = gson.fromJson(response, youScoreBasicResListType);
			if (!CollectionUtils.isEmpty(youScoreBasicRes) && "1".equals(youScoreBasicRes.get(0).getStatus())) {
				success = true;
			}
		}

		if (!success) {
			logger.info("Youscore mapping creation failed for studentId :" + studentId + " teacherId : " + teacherId
					+ " response : " + response);
		}
	}

	public void createOTFBatchMappings(String batchId) throws VException {
		BatchInfo batchInfo = getOTFBatchInfo(batchId);
		if (batchInfo == null) {
			logger.info("batch info not found for id : " + batchId);
			return;
		}
		createOTFBatchMappings(batchInfo);
	}

	public void createOTFBatchMappings(BatchInfo batchBasicInfo) throws VException {
                throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED,"Not Supported anymore, contact tech team");
            /*
		logger.info("createOTFBatchMappings - " + batchBasicInfo.toString());
		List<UserBasicInfo> students = batchBasicInfo.getEnrolledStudents();
		List<UserBasicInfo> teachers = batchBasicInfo.getTeachers();

		if (!CollectionUtils.isEmpty(students) && !CollectionUtils.isEmpty(teachers)) {

			for (UserBasicInfo teacher : teachers) {
				try {
					generateTeacherToken(String.valueOf(teacher.getUserId()), Role.TEACHER);
				} catch (Exception ex) {
					logger.info("createOTFBatchMappingsError - " + batchBasicInfo.toString() + " teacherId:"
							+ teacher.getUserId());
				}
			}

			for (UserBasicInfo student : students) {
				try {
					generateStudentToken(String.valueOf(student.getUserId()), Role.STUDENT);
				} catch (Exception ex) {
					logger.info("createOTFBatchMappingsError - " + batchBasicInfo.toString() + " studentId:"
							+ student.getUserId());
				}
			}
			for (UserBasicInfo teacher : teachers) {
				for (UserBasicInfo student : students) {
					try {
						createStudentTeacherMapping(student.getUserId(), teacher.getUserId());
					} catch (Exception ex) {
						logger.info("createOTFBatchMappings", ex);
					}
				}
			}

			// Share the contents between student and teacher
			contentShareOTFBatch(batchBasicInfo.getBatchId());
		}
*/
	}

	private String generateUserEmailId(String userId) {
		return userId + EMAIL_ID_SUFFIX;
	}

	private List<YouScoreBasicRes> parseYouScoreBasicRes(String response) {
		Type youScoreResponseType = new TypeToken<List<YouScoreBasicRes>>() {
		}.getType();
		return new Gson().fromJson(response, youScoreResponseType);
	}

	@Deprecated
	private void contentShareOTFBatch(String batchId) throws VException {
            throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED,"Not Supported anymore, contact tech team");
            /*
		if (StringUtils.isEmpty(batchId)) {
			logger.info("contentShareOTFBatch - Invalid batch id " + batchId);
			return;
		}

		String location = ontofewEndpoint + "/batch/" + batchId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(location, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);

		try {
			BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);
			if (batchInfo != null) {
				List<AgendaPojo> agenda = batchInfo.getAgenda();
				List<UserBasicInfo> students = batchInfo.getEnrolledStudents();
				List<UserBasicInfo> teachers = batchInfo.getTeachers();

				List<String> examCodes = new ArrayList<String>();
				if (!CollectionUtils.isEmpty(agenda) && !CollectionUtils.isEmpty(students)
						&& !CollectionUtils.isEmpty(teachers)) {
					for (AgendaPojo agendaEntry : agenda) {
						if ("CONTENT".equals(agendaEntry.getType())) {
							ContentPojo contentPojo = agendaEntry.getContentPOJO();
							if (contentPojo != null) {
								List<ContentInfo> contentInfos = contentPojo.getContentList();
								if (!CollectionUtils.isEmpty(contentInfos)) {
									for (ContentInfo contentInfo : contentInfos) {
										String url = contentInfo.getUrl();
										String examCode = getValidExamCode(url);
										logger.info("examCode : " + examCode);
										if (!StringUtils.isEmpty(examCode)) {
											examCodes.add(examCode);
										}
									}
								}
							}
						}
					}

					if (!CollectionUtils.isEmpty(examCodes)) {
						createOTFContentShares(students, teachers, examCodes);
					}
				}

			}
		} catch (Exception ex) {
			logger.info("contentShareOTFBatch - " + ex.getMessage() + " batchId:" + batchId);
		}
*/
	}

	@Deprecated
	public void contentShareOTOStructured(Long offeringId, Long userId, Long teacherId) throws VException {

		if (StringUtils.isEmpty(offeringId) || StringUtils.isEmpty(userId)) {
			logger.info("contentShareOTOStructured - Invalid offering id " + offeringId + " or userId : " + userId);
			return;
		}
		GetOfferingRequest getOfferingReq = new GetOfferingRequest(offeringId, null, null);
		GetOfferingResponse response = offeringManager.getOffering(getOfferingReq);
		// Get offering info
		// String location = fosEndpoint + "/offering/getOffering?offeringId=" +
		// offeringId;
		// ClientResponse resp = WebUtils.INSTANCE.doCall(location,
		// HttpMethod.GET, null);
		// VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		// String jsonString = resp.getEntity(String.class);
		String jsonString = new Gson().toJson(response);
		logger.info(jsonString);

		try {

			// Extract examCodes from the offering info
			List<String> examCodes = new ArrayList<String>();
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONObject offeringObject = jsonObject.getJSONObject("offeringInfo");
			if (!offeringObject.has("contents")) {
				return;
			}

			JSONArray contentArray = offeringObject.getJSONArray("contents");
			if (contentArray != null && contentArray.length() > 0) {
				for (int i = 0; i < contentArray.length(); i++) {
					JSONObject contentObject = new JSONObject(contentArray.getString(i));
					String forRole = contentObject.getString("forRole");
					if ("TEACHER".equals(forRole)) {
						continue;
					}

					String url = contentObject.getString("url");
					if (!StringUtils.isEmpty(url)) {
						String examCode = getValidExamCode(url);
						if (!StringUtils.isEmpty(examCode)) {
							examCodes.add(examCode);
						}
					}
				}
			}

			if (CollectionUtils.isEmpty(examCodes)) {
				logger.info(
						"No examcodes found for req : " + offeringId + " userId:" + userId + " teacherId:" + teacherId);
				return;
			}

			// Fetch user basic info
			ArrayList<String> userIds = new ArrayList<>();
			userIds.add(String.valueOf(userId));
			userIds.add(String.valueOf(teacherId));

			// String userFetchUrl = fosEndpoint +
			// "/onetofew/getEntityInfoByIds?type=user&userIdsList="
			// + StringUtils.arrayToDelimitedString(userIds.toArray(), ",");
			// ClientResponse response = WebUtils.INSTANCE.doCall(userFetchUrl,
			// HttpMethod.GET, null);
			// String output = response.getEntity(String.class);
			List<UserBasicInfo> result = fosUtils.getUserBasicInfos(userIds, false);
			List<UserBasicInfo> students = new ArrayList<>();
			List<UserBasicInfo> teachers = new ArrayList<>();
			if (result != null) {
				try {
					if (CollectionUtils.isEmpty(result)) {
						logger.info("contentShareOTOStructured - User info not found " + result);
						return;
					}

					for (UserBasicInfo userBasicInfo : result) {
						if (userId.equals(userBasicInfo.getUserId())) {
							students.add(userBasicInfo);
						}

						if (teacherId.equals(userBasicInfo.getUserId())) {
							teachers.add(userBasicInfo);
						}
					}
				} catch (Exception ex) {
					logger.info("contentShareOTOStructured - Error occured while fetching user " + result);
					return;
				}
			}

			if (CollectionUtils.isEmpty(students) || CollectionUtils.isEmpty(teachers)) {
				logger.info("contentShareOTOStructured - No users found for req : " + offeringId + " studentId:"
						+ userId + " teacherId:" + teacherId);
				return;
			}

			// Share the content between the users
			createOTFContentShares(students, teachers, examCodes);
		} catch (Exception ex) {
			logger.info("contentShareOTOStructured - exception offering id " + offeringId + " or userId : " + userId
					+ " ex:" + ex.getMessage());
		}
	}

	private String getValidExamCode(String url) {
		logger.info("getValidExamCode - url : " + url);
		String urlLower = url.toLowerCase();
		if (urlLower.contains("examcode") && (urlLower.contains("youscore") || urlLower.contains("scobotic"))) {
			int questionMarkIndex = url.indexOf("?");
			if (questionMarkIndex >= 0) {
				String queryString = url.substring(questionMarkIndex + 1);
				if (queryString != null && queryString.length() > 0) {
					String[] queryParams = queryString.split("&");
					if (queryParams != null && queryParams.length > 0) {
						for (String param : queryParams) {
							if (param.toLowerCase().contains("examcode")) {
								int index = param.indexOf("=");
								if (param.length() > index) {
									return param.substring(index + 1);
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	@Deprecated
	private void createOTFContentShares(List<UserBasicInfo> students, List<UserBasicInfo> teachers,
			List<String> examCodes) {
		YouScoreOTFContentShareReq youScoreOTFContentShareReq = new YouScoreOTFContentShareReq(students, teachers,
				examCodes);
		logger.info("createOTFContentShares request :" + youScoreOTFContentShareReq);
		ClientResponse resp = WebUtils.INSTANCE.doCall(lmsEndpoint + "youscore/content/shareBulk", HttpMethod.POST,
				new Gson().toJson(youScoreOTFContentShareReq), true);
		logger.info("resp : " + resp.toString());
	}

	public YouScoreCookie getValidCookieFromRequest(HttpServletRequest request) throws UnsupportedEncodingException {
		YouScoreCookie cookie = getYouScoreCookieFromRequest(request);
		if (cookie != null && validateCookie(cookie)) {
			return cookie;
		}
		return null;
	}

	public YouScoreCookie getYouScoreCookieFromRequest(HttpServletRequest request) throws UnsupportedEncodingException {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (YouScoreConstants.YOUSCORE_COOKIE.equals(cookie.getName())) {
					String jsonString = cookie.getValue();
					logger.info("Request : " + jsonString);
					String jsonDecodedString = URLDecoder.decode(jsonString, "UTF-8");
					YouScoreCookie youScoreCookie = new Gson().fromJson(jsonDecodedString, YouScoreCookie.class);
					if (validateCookie(youScoreCookie)) {
						return youScoreCookie;
					}
					break;
				}
			}
		}

		return null;
	}

	public boolean validateCookie(YouScoreCookie youScoreCookie) {
		if (!StringUtils.isEmpty(youScoreCookie.getToken()) && !StringUtils.isEmpty(youScoreCookie.getUserId())
				&& !StringUtils.isEmpty(youScoreCookie.getUserRole())
				&& !StringUtils.isEmpty(youScoreCookie.getExpiryTime())) {
			HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
			Long loggedInUserId = sessionData.getUserId();
			if (loggedInUserId == null || youScoreCookie.getUserId() == null
					|| !youScoreCookie.getUserId().equals(loggedInUserId)) {
				return false;
			}
			Long _expiryTime = Long.parseLong(youScoreCookie.getExpiryTime());
			if (_expiryTime > (System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_HOUR)) {
				return true;
			}
		}
		return false;
	}

	public BatchInfo getOTFBatchInfo(String batchId) throws VException {
		String location = ontofewEndpoint + "/batch/" + batchId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(location, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);
		return batchInfo;
	}
}
