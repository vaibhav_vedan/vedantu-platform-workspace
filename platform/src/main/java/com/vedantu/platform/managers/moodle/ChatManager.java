/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.moodle;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 *
 * @author somil
 */
@Service
public class ChatManager {

	@Autowired
	private LogFactory logFactory;
        
        private final Gson gson = new Gson();
        
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ChatManager.class);
        
        private String subscriptionEndPoint;
        
        @PostConstruct
	public void init() {
		subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	}

	public String initiateChatWithUser(Long fromUserId, Long toUserId) throws VException {
		String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
		ClientResponse resp = WebUtils.INSTANCE.doCall(
				notificationEndpoint + "/chat/initiateChatWithUser?fromUserId=" + fromUserId + "&toUserId=" + toUserId,
				HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		return resp.getEntity(String.class);
	}
        public void handleSessionEvents(SessionEvents event, String sessionInfoMessage) throws VException {
		try {
			if (SessionEvents.SESSION_SCHEDULED.equals(event) ) {
                            SessionInfo sessionInfo = gson.fromJson(sessionInfoMessage, SessionInfo.class);
                            List<Long> students=sessionInfo.getStudentIds();
                            for(Long student:students){
                                initiateChatWithUser(sessionInfo.getTeacherId(), student);
                            }
			}
		} catch (JsonSyntaxException | IllegalArgumentException e) {
			logger.error("Error in handling  sessionevent  ", e);
		}
	}
        public void handleOtfBatchEvents(BatchEventsOTF event, String enrollmentInfoMessage) throws VException {
		try {
			if (BatchEventsOTF.USER_ENROLLED.equals(event) ) {
                            EnrollmentPojo enrollmentInfo = gson.fromJson(enrollmentInfoMessage, EnrollmentPojo.class);
                            if(Role.STUDENT.equals(enrollmentInfo.getRole())){
                                String studentId=enrollmentInfo.getUserId();
                                String url = subscriptionEndPoint + "/batch/" + enrollmentInfo.getBatchId();
                                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                                String jsonString = resp.getEntity(String.class);
                                BatchInfo batchInfo = gson.fromJson(jsonString, BatchInfo.class);
                                List<UserBasicInfo> teachers=batchInfo.getTeachers();
                                for(UserBasicInfo teacher:teachers){
                                    initiateChatWithUser(teacher.getUserId(), Long.parseLong(studentId));
                                }
                            }
			}
		} catch (JsonSyntaxException | IllegalArgumentException e) {
			logger.error("Error in handling  batchEvent  ", e);
		}
	}
}
