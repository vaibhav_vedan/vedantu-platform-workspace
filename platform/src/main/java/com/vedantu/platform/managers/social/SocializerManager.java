/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.social;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.request.SetQuestionVoteCountReq;
import com.vedantu.lms.cmds.request.SetVideoCountReq;
import com.vedantu.lms.cmds.response.GetUserVoteRes;
import com.vedantu.lms.cmds.response.GetVotesRes;
import com.vedantu.lms.cmds.response.VoteRes;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.social.BookmarkDAO;
import com.vedantu.platform.dao.social.CommentDAO;
import com.vedantu.platform.dao.social.ViewDAO;
import com.vedantu.platform.dao.social.VoteDAO;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.mongodbentities.social.Bookmark;
import com.vedantu.platform.mongodbentities.social.Comment;
import com.vedantu.platform.mongodbentities.social.View;
import com.vedantu.platform.mongodbentities.social.Vote;
import com.vedantu.platform.pojo.ViewSession;
import com.vedantu.platform.pojo.social.CommentsRepliesCount;
import com.vedantu.platform.pojo.social.VoteCount;
import com.vedantu.platform.request.social.AddCommentReq;
import com.vedantu.platform.request.social.GetCommentsReq;
import com.vedantu.platform.request.social.GetVotesReq;
import com.vedantu.platform.response.social.AddViewReq;
import com.vedantu.platform.response.social.UserActivity;
import com.vedantu.util.request.VoteReq;
import com.vedantu.platform.response.social.CommentRes;
import com.vedantu.platform.response.social.GetCommentsRes;
import com.vedantu.platform.response.social.LastViewTime;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.BookmarkReq;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import static com.vedantu.platform.enums.SocialContextType.*;

/**
 *
 * @author ajith
 */
@Service
public class SocializerManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SocializerManager.class);

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private VoteDAO voteDAO;

    @Autowired
    private BookmarkDAO bookmarkDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private ViewDAO viewDAO;

    @Autowired
    private DozerBeanMapper mapper;

    private static final Integer MAX_VIEWS_COUNT_PER_TIMEINSECS = ConfigUtils.INSTANCE.getIntValue("video.view.per.timeinsecs");

    public PlatformBasicResponse addComment(AddCommentReq req, Long userId) throws BadRequestException, VException {
        req.verify();
        Comment comment = mapper.map(req, Comment.class);
        comment.setUserId(userId);
        if (req.getParentId() != null) {
            Comment parentComment = commentDAO.getEntityById(req.getParentId(), Comment.class);
            if (parentComment == null) {
                throw new NotFoundException(ErrorCode.PARENT_COMMENT_NOT_FOUND, "parent comment not found");
            }
            comment.setContextId(parentComment.getContextId());
            comment.setSocialContextType(parentComment.getSocialContextType());
            comment.setRootCommentId(parentComment.getRootCommentId());
        }
        commentDAO.save(comment);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse addVote(VoteReq req, Long userId) throws BadRequestException, VException {
        req.verify();
        Vote vote = mapper.map(req, Vote.class);
        vote.setUserId(userId);
        if (vote.getVoteValue() < -1) {
            vote.setVoteValue(-1);
        } else if (vote.getVoteValue() > 1) {
            vote.setVoteValue(1);
        }
        //update> upsert true,multi false
        //TODO validate userId, contextId and featureContextId
        Query query = new Query();
        query.addCriteria(Criteria.where(Vote.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Vote.Constants.SOCIAL_CONTEXT_TYPE).is(req.getSocialContextType()));
        query.addCriteria(Criteria.where(Vote.Constants.CONTEXT_ID).is(req.getContextId()));
        Update update = new Update();
        update.set(Vote.Constants.VOTE_VALUE, vote.getVoteValue());
        update.set(Vote.Constants.LAST_UPDATED, System.currentTimeMillis());
        update.set(Vote.Constants.FEATURE_CONTEXT, vote.getFeatureContext());
        update.set(Vote.Constants.FEATURE_CONTEXT_ID, vote.getFeatureContextId());
        update.set(Vote.Constants.LAST_UPDATED_BY, (req.getCallingUserId() == null)
                ? null : req.getCallingUserId().toString());
        voteDAO.upsertEntity(query, update, Vote.class);
        if (SocialContextType.CMDSQUESTION.equals(req.getSocialContextType())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("questionId", req.getContextId());
            payload.put("socialContextType",req.getSocialContextType());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.RECALCULATE_VOTES, payload);
            asyncTaskFactory.executeTask(params);
        }
        
        if (SocialContextType.CMDSVIDEO.equals(req.getSocialContextType())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("videoId", req.getContextId());
            payload.put("voteValue", req.getVoteValue());
            payload.put("socialContextType",req.getSocialContextType());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.RECALCULATE_VIDEO_LIKES, payload);
            asyncTaskFactory.executeTask(params);
        }

        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse addBookmark(BookmarkReq req, Long userId) throws BadRequestException, VException {
        req.verify();

        //update> upsert true,multi false
        //TODO validate userId, contextId and featureContextId
        Query query = new Query();
        query.addCriteria(Criteria.where(Bookmark.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Bookmark.Constants.SOCIAL_CONTEXT_TYPE).is(req.getSocialContextType()));
        query.addCriteria(Criteria.where(Bookmark.Constants.CONTEXT_ID).is(req.getContextId()));
        Update update = new Update();
        update.set(Bookmark.Constants.BOOKMARK_VALUE, req.isBookmark());
        update.set(Bookmark.Constants.LAST_UPDATED, System.currentTimeMillis());
        update.set(Bookmark.Constants.FEATURE_CONTEXT, req.getFeatureContext());
        update.set(Bookmark.Constants.FEATURE_CONTEXT_ID, req.getFeatureContextId());
        update.set(Bookmark.Constants.LAST_UPDATED_BY, (req.getCallingUserId() == null)
                ? null : req.getCallingUserId().toString());
        bookmarkDAO.upsertEntity(query, update, Bookmark.class);
        return new PlatformBasicResponse();
    }

    public GetCommentsRes getComments(GetCommentsReq req) throws VException {
        req.verify();
        List<Comment> comments = commentDAO.getEntities(req.getSocialContextType(),
                req.getContextId(),
                req.getParentId(),
                req.getStart(),
                req.getSize(),
                req.getOrderBy(),
                req.getSortOrder());
        GetCommentsRes res = new GetCommentsRes();
        if (ArrayUtils.isNotEmpty(comments)) {
            List<Long> userIds = new ArrayList<>();
            List<String> commentIds = new ArrayList<>();
            for (Comment comment : comments) {
                userIds.add(comment.getUserId());
                commentIds.add(comment.getId());
            }
            Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
            Map<String, CommentsRepliesCount> repliesMap = commentDAO.getRepliesCount(new HashSet<>(commentIds));
            Map<String, VoteCount> votesMap = voteDAO.votesCount(new HashSet<>(commentIds));
            Map<String, Boolean> votedMap = null;
            if (req.getUserId() != null) {
                votedMap = voteDAO.getUpvotedMap(req.getUserId(), commentIds);
            }

            for (Comment comment : comments) {
                CommentRes _socializerRes = mapper.map(comment, CommentRes.class);
                _socializerRes.setUser(usermap.get(_socializerRes.getUserId()));
                if (repliesMap.get(comment.getId()) != null) {
                    _socializerRes.setReplies(repliesMap.get(comment.getId()).getReplies());
                }
                if (votesMap.get(comment.getId()) != null) {
                    _socializerRes.setUpVotes(votesMap.get(comment.getId()).getUpVotes());
                }
                if (votedMap != null && Boolean.TRUE.equals(votedMap.get(comment.getId()))) {
                    _socializerRes.setVoted(true);
                }
                res.addItem(_socializerRes);
            }
            if (res.getCount() == req.getSize()) {
                Long count = commentDAO.getEntitiesCount(
                        req.getSocialContextType(), req.getContextId(), req.getParentId());
                res.setCount(count.intValue());
            }
        }
        return res;
    }

    public GetVotesRes getVotes(GetVotesReq req) throws VException {
        req.verify();
        //filling socialinfo
        List<Vote> votes = voteDAO.getVotes(req.getUserId(),
                req.getSocialContextType(), req.getContextId(), 0, 10);
        List<VoteRes> votesList = new ArrayList<>();
        for (Vote vote : votes) {
            VoteRes voteRes = mapper.map(vote, VoteRes.class);
            votesList.add(voteRes);
        }
        GetVotesRes res = new GetVotesRes();
        res.setList(votesList);
        return res;
    }

    public void setQuestionVotes(String qid) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Vote.Constants.SOCIAL_CONTEXT_TYPE).is(SocialContextType.CMDSQUESTION));
        query.addCriteria(Criteria.where(Vote.Constants.CONTEXT_ID).is(qid));
        query.addCriteria(Criteria.where(Vote.Constants.VOTE_VALUE).is(1));
        long count = voteDAO.queryCount(query, Vote.class);
        SetQuestionVoteCountReq req = new SetQuestionVoteCountReq();
        req.setQuestionId(qid);
        req.setCount(count);
        String lmsendpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(lmsendpoint + "/cmds/question/setQuestionVoteCount", HttpMethod.POST,
                new Gson().toJson(req), true);
        // TODO handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }
    

    public void setVideoVotes(Map<String, Object> payload) throws VException {
    	Query query = new Query();
        query.addCriteria(Criteria.where(Vote.Constants.SOCIAL_CONTEXT_TYPE).is(SocialContextType.CMDSVIDEO));
        query.addCriteria(Criteria.where(Vote.Constants.CONTEXT_ID).is(payload.get("videoId")));
        query.addCriteria(Criteria.where(Vote.Constants.VOTE_VALUE).is(1));
        long count = voteDAO.queryCount(query, Vote.class);

        Query query2 = new Query();
        query2.addCriteria(Criteria.where(Vote.Constants.SOCIAL_CONTEXT_TYPE).is(SocialContextType.CMDSVIDEO));
        query2.addCriteria(Criteria.where(Vote.Constants.CONTEXT_ID).is(payload.get("videoId")));
        query2.addCriteria(Criteria.where(Vote.Constants.VOTE_VALUE).is(-1));
        long dislikecount = voteDAO.queryCount(query2, Vote.class);

        SetVideoCountReq req = new SetVideoCountReq();
        req.setVideoId(payload.get("videoId").toString());
        req.setCount(count);
        req.setDisLikeCount(dislikecount);

        String lmsendpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(lmsendpoint + "/cmds/video/setVideoVoteCount", HttpMethod.POST,
                new Gson().toJson(req), true);
        // TODO handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public GetUserVoteRes getUserVoteRes(GetVotesReq req) throws BadRequestException {
        //TODO use redis with a small expiry time
        req.verify();
        GetUserVoteRes getUserVoteRes = new GetUserVoteRes();
        Map<String, VoteCount> votesMap = voteDAO.votesCount(req.getContextId());

        if (votesMap.containsKey("1")) {
            getUserVoteRes.setLikeCount(votesMap.get("1").getUpVotes());
        }

        if (votesMap.containsKey("-1")) {
            getUserVoteRes.setDislikeCount(-(votesMap.get("-1").getUpVotes()));
        }

        Vote vote = voteDAO.getVotesForUser(req.getUserId(),
                req.getSocialContextType(), req.getContextId(), 0, 10);

        if (vote != null) {
            if (vote.getVoteValue() > 0) {
                getUserVoteRes.setLiked(true);
            } else if (vote.getVoteValue() < 0) {
                getUserVoteRes.setDisliked(true);
            }

        }

        return getUserVoteRes;
    }

    public Map<String, Boolean> getBookmarkedMap(Long userId, List<String> entityIds) {
        return bookmarkDAO.getBookmarkedMap(userId, entityIds);
    }

    public List<String> getBookmarkedContextIds(Long userId, SocialContextType socialContextType, List<String> inputContextIds,
            Integer start, Integer size) {
        //filling socialinfo
        List<Bookmark> bookmarks = bookmarkDAO.getBookmarks(userId, inputContextIds,
                socialContextType, null, ((start == null) ? 0 : start), ((size == null) ? 100 : size));
        List<String> contextIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(bookmarks)) {
            for (Bookmark bookmark : bookmarks) {
                contextIds.add(bookmark.getContextId());
            }
        }
        return contextIds;
    }

    public PlatformBasicResponse addView(AddViewReq req, Long userId) throws BadRequestException{
        req.verify();

        View view = viewDAO.getByContextIdAndUserId(req.getContextId(), userId);

        if(view == null){
            view = new View();
            view.setContextId(req.getContextId());
            view.setSocialContextType(req.getSocialContextType());
            view.setUserId(userId);
            view.setViewSessions(new ArrayList<>());
        }
        
        boolean isNew = true;
        boolean updateViewCount = true;
        Long currentTime = System.currentTimeMillis();
        Long lastHourTime = req.getStartTime() - (long) MAX_VIEWS_COUNT_PER_TIMEINSECS * 1000;
        for(ViewSession viewSession : view.getViewSessions()){
            if(viewSession.getStartTime().equals(req.getStartTime())){
                isNew = false;
                viewSession.setLastUpdated(currentTime);
                viewSession.setDuration(currentTime - viewSession.getStartTime());
                updateViewCount = false;
            }

            if(viewSession.getStartTime().compareTo(lastHourTime) > 0){
                updateViewCount = false;
            }

        }

        if(isNew){
            ViewSession viewSession = new ViewSession();
            viewSession.setStartTime(req.getStartTime());
            viewSession.setDuration(currentTime - req.getStartTime());
            viewSession.setLastUpdated(currentTime);
            view.getViewSessions().add(viewSession);
            //update video.new = false .... async task
        }
        viewDAO.save(view);

        if(updateViewCount){

            if(CMDSVIDEO.equals(req.getSocialContextType())){
                try{
                    incrementVideoViewCount(req.getContextId());
                }catch(Exception ex){
                    logger.error("Error in incrementing cmds video views "+ex.getMessage());
                }
            }
            if(SocialContextType.STUDY_ENTRY_ITEM.equals(req.getSocialContextType())) {
                try {
                    incrementStudyViewCount(req.getContextId());
                } catch (Exception ex) {
                    logger.error("Error in incrementing study pdf views "+ex.getMessage());
                }
            }

        }

        return new PlatformBasicResponse(true, null, null);
    }

    public void incrementVideoViewCount(String videoId) throws VException{
        String lmsendpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
        lmsendpoint = lmsendpoint + "/cmds/video/incrementCMDSVideoView?id="+videoId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(lmsendpoint, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public void incrementStudyViewCount(String videoId) throws VException{
        String lmsendpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
        lmsendpoint = lmsendpoint + "/study/incrementStudyView?id="+videoId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(lmsendpoint, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }
    
    public LastViewTime getLastView(Long userId, String socialConextType) {
    	return viewDAO.getLastView(userId, socialConextType);
    }

    public List<UserActivity> getRecentActivities(String userId, int limit) {
        logger.info("method=getRecentActivities, class=SocializerManager.");
        Set<SocialContextType> contextTypes = new HashSet<>(Arrays.asList(CMDSVIDEO, CMDSTEST, STUDY_ENTRY_ITEM, DOUBT));
        List<View> views = viewDAO.getRecentActivities(userId, contextTypes, limit);
        logger.info("Number of Views found in database={}", views.size());
        return views.stream()
                .map(view -> new UserActivity(view.getSocialContextType(), view.getContextId(), view.getLastUpdated()))
                .collect(Collectors.toList());
    }

    public List<String> getRecentlyWatchedVideos(String userId, int limit) {
        logger.info("method=getRecentlyWatchedVideos, class=SocializerManager.");
        Set<SocialContextType> videoContextType = new HashSet<>(Collections.singletonList(CMDSVIDEO));
        List<View> views = viewDAO.getRecentActivities(userId, videoContextType, limit);
        logger.info("Number of Videos found in database={}", views.size());
        return views.stream()
                .map(view -> new UserActivity(view.getSocialContextType(), view.getContextId()))
                .map(UserActivity::getContextId)
                .collect(Collectors.toList());
    }
}
