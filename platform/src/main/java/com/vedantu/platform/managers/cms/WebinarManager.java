/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.cms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.AbstractAwsSQSManager;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.SMSPriorityType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.enums.OTMSessionInProgressState;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.converters.WebinarToWebinarResponseConverter;
import com.vedantu.platform.dao.cms.WebinarDAO;
import com.vedantu.platform.dao.cms.WebinarSpotInstanceMetadataDao;
import com.vedantu.platform.dao.cms.WebinarUserRegistrationInfoDAO;
import com.vedantu.platform.dao.cms.WebinarUserRegistrationTokenDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.entity.WebinarSpotInstanceMetadata;
import com.vedantu.platform.enums.WebinarNotifcation;
import com.vedantu.platform.enums.WebinarSpotInstanceStatus;
import com.vedantu.platform.enums.cms.TimeFrame;
import com.vedantu.platform.enums.cms.WebinarStatus;
import com.vedantu.platform.enums.cms.WebinarToolType;
import com.vedantu.platform.enums.cms.WebinarType;
import com.vedantu.platform.enums.cms.WebinarUserRegistrationStatus;
import com.vedantu.platform.managers.ReviseManager;
import com.vedantu.platform.managers.aws.AwsEC2Manager;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.notification.SMSManager;
import com.vedantu.platform.pojo.WebinarSpotInstanceFilterReq;
import com.vedantu.platform.pojo.cms.Webinar;
import com.vedantu.platform.pojo.cms.WebinarCallbackData;
import com.vedantu.platform.pojo.cms.WebinarInfo;
import com.vedantu.platform.pojo.cms.WebinarLeaderBoardData;
import com.vedantu.platform.pojo.cms.WebinarRegistrationQuestion;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationToken;
import com.vedantu.platform.request.cms.AddQuestionToRegistrationReq;
import com.vedantu.platform.request.cms.ConfirmWebinarRegisterOTPReq;
import com.vedantu.platform.request.cms.CreateWebinarReq;
import com.vedantu.platform.request.cms.EditWebinarReq;
import com.vedantu.platform.request.cms.FindWebinarsReq;
import com.vedantu.platform.request.cms.GetWebinarRegistrationInfoReq;
import com.vedantu.platform.request.cms.WebinarCallbackReq;
import com.vedantu.platform.request.cms.WebinarRegistrationReq;
import com.vedantu.platform.response.WaveWebinarResponse;
import com.vedantu.platform.response.WaveWebinarResponsePojo;
import com.vedantu.platform.response.WebinarResponse;
import com.vedantu.platform.response.cms.FindQuizzesRes;
import com.vedantu.platform.response.cms.LaunchedWebinar;
import com.vedantu.platform.response.cms.WebinarLaunchStatusRes;
import com.vedantu.platform.response.cms.WebinarMonitorActionButtonResponse;
import com.vedantu.platform.response.cms.WebinarRegisterUserRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.CourseMobileRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;


/**
 * @author jeet
 */
@Service
public class WebinarManager extends AbstractAwsSQSManager {

    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    public static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    public static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment");
    public final static Long syncGtwAttendenceCronRunTime = 15 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    public final static Long sendWebinarRemindersCronRunTime = 15 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private static final String URL_SHORTNER_API_KEY = ConfigUtils.INSTANCE.getStringValue("google.url.shortner.api.key");
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat sdfWebinarReg = new SimpleDateFormat("hh:mma, dd MMM yyyy");
    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
    private static final String WEBINAR_URL = ConfigUtils.INSTANCE.getStringValue("webinar_url");
    private static final String WAVE_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("WAVE_ENDPOINT");
    private static final String CSV_DELIMITER = ",";
    private static final String WAVE_VIMEO = "WAVE_VIMEO_";
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final Integer WEBINAR_SPOT_INSTANCE_MAX_RETRY = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("webinar.spot,instance.max.retry", "3"));
    private static final String WEBINAR_SPOT_INSTANCE_BUCKET = ConfigUtils.INSTANCE.getStringValue("webinar.spot.instance.bucket");
    private static ArrayList<InternetAddress> webinarCCList = new ArrayList<>();
    private final Gson gson = new Gson();
    @Autowired
    public AwsSQSManager awsSQSManager;
    @Autowired
    CommunicationManager communicationManager;
    @Autowired
    private LogFactory logFactory;
    @Autowired
    private WebinarDAO webinarDAO;
    @Autowired
    private WebinarUserRegistrationTokenDAO webinarUserRegistrationTokenDAO;
    @Autowired
    private WebinarGTWManager webinarGTWManager;
    @Autowired
    private SMSManager smsManager;
    @Autowired
    private AwsSNSManager awsSNSManager;
    @Autowired
    private FosUtils fosUtils;
    @Autowired
    private HttpSessionUtils httpSessionUtils;
    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    @Autowired
    private WebinarUserRegistrationInfoDAO webinarUserRegistrationInfoDAO;
    @Autowired
    private ReviseManager reviseManager;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(WebinarManager.class);
    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private WebinarSpotInstanceMetadataDao webinarSpotInstanceMetadataDao;

    @Autowired
    private AwsEC2Manager awsEC2Manager;

    @Autowired
    private AwsS3Manager awsS3Manager;

    private static final Integer MAX_WEBINAR_BULK_UPLOAD_SIZE = 25;

    public static String generateRandomChars(int length) {
        String candidateChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }

    @PostConstruct
    public void init() {
        try {
            webinarCCList.add(new InternetAddress("anshul.goswami@vedantu.com"));
            webinarCCList.add(new InternetAddress("aman.jhalani@vedantu.com"));
            webinarCCList.add(new InternetAddress("tarun.singhal@vedantu.com"));
            webinarCCList.add(new InternetAddress("jitendra.swami@vedantu.com"));
        } catch (AddressException ex) {
            logger.error("not able to create webinarCCList" + ex.getMessage());
        }
    }

    public Webinar createWebinar(CreateWebinarReq req) throws VException {
        req.verify();
        Webinar webinar = webinarDAO.getByWebinarCode(req.getWebinarCode());

        if (webinar != null) {
            throw new ConflictException(ErrorCode.WEBINAR_ALREADY_EXISTS,
                    "a webinar with this code already exists");
        }
        if (StringUtils.isNotEmpty(req.getGtwSeat()) && !webinarGTWManager.isGTWseatSupported(req.getGtwSeat())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "gtw emailId not supported");
        }

        webinar = new Webinar(req);
        webinarDAO.upsert(webinar, req.getCallingUserId());
        logger.info("createWebinar webinar: " + webinar);
        if (WebinarToolType.VEDANTU_WAVE.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_OTO.equals(webinar.getToolType())) {
            if (WebinarStatus.ACTIVE.equals(webinar.getWebinarStatus())) {
                setupSimLiveWebinar(webinar, req);
                addEditOTMSession(webinar, req);
            }
        }

        logger.info("upserting webinar with title -- " + webinar.getTitle() + "webinarCode -- " + webinar.getWebinarCode());
        webinarDAO.upsert(webinar, req.getCallingUserId());
        return webinar;
    }

    public Map<String, String> createWebinarBulk(List<CreateWebinarReq> createWebinarReqs) throws VException {

        Set<String> webinarCodes = new HashSet<>();
        Set<String> unsupportedGTWseat = new HashSet<>();

        logger.info("Validating the webinar bulk creation request");

        if(CollectionUtils.isEmpty(createWebinarReqs))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Request cannot be empty");

        if(createWebinarReqs.size() > MAX_WEBINAR_BULK_UPLOAD_SIZE)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Maximum webinar creation limit is: " + MAX_WEBINAR_BULK_UPLOAD_SIZE);

        for(CreateWebinarReq webinarReq : createWebinarReqs){

            webinarReq.verify();

            String webinarCode = webinarReq.getWebinarCode();

            if(webinarCodes.contains(webinarCode))
                throw new ConflictException(ErrorCode.DUPLICATE_ENTRY,"duplicate webinarCode found in request :" + webinarCode);

            webinarCodes.add(webinarCode);

            if (StringUtils.isNotEmpty(webinarReq.getGtwSeat()) && !webinarGTWManager.isGTWseatSupported(webinarReq.getGtwSeat())) {
                unsupportedGTWseat.add(webinarReq.getGtwSeat());
            }

        }

        if (CollectionUtils.isNotEmpty(unsupportedGTWseat))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"gtw emailId(s) not supported :" + unsupportedGTWseat);

        validateRequestedWebinarCodes(webinarCodes);

        logger.info("Validated the webinar bulk creation request");

        Map<String, String> failedWebinars = new HashMap<>();

        try {
            for(CreateWebinarReq webinarReq : createWebinarReqs){

                try {

                    Webinar webinar = createWebinar(webinarReq);

                }catch (Exception e){
                    logger.error("Error creating webinar", e);
                    failedWebinars.put(webinarReq.getWebinarCode(), e.getMessage());
                }

            }
            logger.info("Webinars failed {}", failedWebinars);
        }catch (Exception e){
            logger.error("Error while creating webinars in bulk", e);
        }finally {
            return failedWebinars;
        }

    }

    private void validateRequestedWebinarCodes(Set<String> webinarCodes) throws ConflictException {
        List<Webinar> webinarList = webinarDAO.getByWebinarsCodes(webinarCodes);

        if(CollectionUtils.isNotEmpty(webinarList)){
            Set<String> usedWebinarCodes = webinarList.parallelStream().map(webinar -> webinar.getWebinarCode()).collect(Collectors.toSet());
            throw new ConflictException(ErrorCode.WEBINAR_ALREADY_EXISTS,"Webinar already exist with the code(s) :" + usedWebinarCodes);
        }
    }

    private void setupSimLiveWebinar(Webinar webinar, CreateWebinarReq req) throws VException {
        logger.info("setupSimLiveWebinar-input: " + req + " " + webinar);
        boolean isSimLiveWebinar = req.getIsSimLive() != null && req.getIsSimLive();
        if (isSimLiveWebinar) {
            String parentWebinarId = req.getParentWebinarId();
            Webinar parentWebinar = webinarDAO.getById(parentWebinarId);
            if (parentWebinar == null) {
                logger.error("Webinar not found for id : " + parentWebinarId);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "parent webinar not found with id " + parentWebinarId);
            }
            String parentWebinarSessionId = parentWebinar.getSessionId();
            if (StringUtils.isEmpty(parentWebinarSessionId)) {
                logger.error("parent sessionId not found for id : " + parentWebinarSessionId);
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "sessionId missing for webinar " + parentWebinarSessionId);
            }
            webinar.setParentWebinarId(parentWebinarId);
            webinar.setParentWebinarSessionId(parentWebinarSessionId);
        }
        webinar.setIsSimLive(isSimLiveWebinar);
        logger.info("setupSimLiveWebinar-return: " + webinar);
    }

    public Webinar editWebinar(EditWebinarReq req) throws VException {
        req.verify();
        Webinar webinar = webinarDAO.getById(req.getWebinarId());

        if (webinar == null) {
            throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with id:" + req.getWebinarId());
        }
        if (StringUtils.isNotEmpty(req.getGtwSeat()) && !webinarGTWManager.isGTWseatSupported(req.getGtwSeat())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "gtw emailId not supported");
        }
        webinar.mapDataFromReq(req);

        if (WebinarToolType.VEDANTU_WAVE.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_OTO.equals(webinar.getToolType())) {
            if (WebinarStatus.ACTIVE.equals(webinar.getWebinarStatus())) {
                setupSimLiveWebinar(webinar, req);
                addEditOTMSession(webinar, req);
            } else {
                if (StringUtils.isNotEmpty(webinar.getSessionId())) {
                    JSONObject cancelReq = new JSONObject();
                    cancelReq.put("sessionId", webinar.getSessionId());
                    cancelReq.put("remark", "Webinar status changed to " + webinar.getWebinarStatus());
                    String createSessionUrl = SCHEDULING_ENDPOINT
                            + "/onetofew/session/cancelSession";
                    ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
                            cancelReq.toString());
                    VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                    String jsonString = resp.getEntity(String.class);
                    webinar.setSessionId(null);
                }
            }
        }
        webinarDAO.upsert(webinar, req.getCallingUserId());

        String cachedKey = "WEBINAR_" + req.getWebinarId();
        try {
            // evict key from redis cache for webinar edit ops
            redisDAO.del(cachedKey);
        } catch (BadRequestException exc) {
            logger.error("error while deleting key from redis cache {}", cachedKey);
        }
        return webinar;
    }

    public Webinar changeWebinarStatus(EditWebinarReq req) throws VException {
        req.verify();
        Webinar webinar = webinarDAO.getById(req.getWebinarId());

        if (webinar == null) {
            throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with id:" + req.getWebinarId());
        }
        webinar.setWebinarStatus(req.getWebinarStatus());

        if (WebinarToolType.VEDANTU_WAVE.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_OTO.equals(webinar.getToolType())) {
            if (WebinarStatus.ACTIVE.equals(webinar.getWebinarStatus())) {
                addEditOTMSession(webinar, req);
            } else {
                if (StringUtils.isNotEmpty(webinar.getSessionId())) {
                    JSONObject cancelReq = new JSONObject();
                    cancelReq.put("sessionId", webinar.getSessionId());
                    cancelReq.put("remark", "Webinar status changed to " + webinar.getWebinarStatus());
                    String createSessionUrl = SCHEDULING_ENDPOINT
                            + "/onetofew/session/cancelSession";
                    ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
                            cancelReq.toString());
                    VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                    String jsonString = resp.getEntity(String.class);
                    webinar.setSessionId(null);
                }
            }

        }

        webinarDAO.upsert(webinar, req.getCallingUserId());
        return webinar;
    }

    private void addEditOTMSession(Webinar webinar, CreateWebinarReq req) throws VException {
        JSONObject addSessionReq = new JSONObject();
        addSessionReq.put("sessionId", webinar.getSessionId());
        addSessionReq.put("webinarId", webinar.getId());
        addSessionReq.put("presenter", req.getPresenter().toString());
        addSessionReq.put("taIds", req.getTaIds());
        addSessionReq.put("startTime", webinar.getStartTime());
        addSessionReq.put("endTime", webinar.getEndTime());
        addSessionReq.put("title", webinar.getTitle());
        addSessionReq.put("sessionToolType", webinar.getToolType());
        addSessionReq.put("isSimLive", webinar.getIsSimLive());
        addSessionReq.put("parentWebinarSessionId", webinar.getParentWebinarSessionId());

        if (req.getStartTime() > System.currentTimeMillis()) {
            String createSessionUrl = SCHEDULING_ENDPOINT
                    + "/onetofew/session/addWebinarSession";

            logger.info("addEditOTMSession - addSessionReq: " + addSessionReq);
            ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
                    addSessionReq.toString());
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            OTFSessionInfo sessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);
            webinar.setSessionId(sessionInfo.getId());
            webinar.setTaIds(sessionInfo.getTaIds());
        }

    }

    public WebinarResponse getWebinarByCodeMobile(FindWebinarsReq req) throws VException {
        req.verify();
        Webinar webinar = webinarDAO.getByWebinarCode(req.getWebinarCode());
        if (webinar == null) {
            throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with code:" + req.getWebinarCode());
        }

        String sessionId = webinar.getSessionId();
        Long callingUserId = httpSessionUtils.getCallingUserId();
        logger.info("method : getWebinarByCodeMobile, userId : {}, sessionId : {}", callingUserId, sessionId);
        WebinarResponse webinarResponse = null;
        if (WebinarToolType.VEDANTU_WAVE.equals(webinar.getToolType()) && StringUtils.isNotEmpty(sessionId)) {
            String createSessionUrl = SCHEDULING_ENDPOINT
                    + "/onetofew/session/" + sessionId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.GET,
                    null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            OTFSessionInfo sessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);

            String hlsUrl = "";
            if (sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getVimeoId())) {
                hlsUrl = FOS_ENDPOINT + "/session-replay/" + sessionId;
            }

            webinar.setSessionInfo(sessionInfo);
            webinarResponse = WebinarToWebinarResponseConverter.convert(webinar);
            webinarResponse.setHlsUrl(hlsUrl);

            Long currentMillis = System.currentTimeMillis();

            if (webinar.getStartTime() != null && webinar.getStartTime() < currentMillis && callingUserId != null)
            {
                try {
                    if (httpSessionUtils.getCallingUserId() != null) {
                        WaveWebinarResponse waveWebinarResponse = getWebinarPerSessionFromWave(sessionId, httpSessionUtils.getCallingUserId().toString());
                        if (waveWebinarResponse != null) {
                            logger.info("Response from wave={}", waveWebinarResponse);
                            addDataFromWave(waveWebinarResponse, webinarResponse);
                        }
                    }
                } catch (VException vex) {
                    logger.info("Could not parse WaveWebinarInfo={}", sessionId);
                }
            }

        } else {
            webinarResponse = WebinarToWebinarResponseConverter.convert(webinar);
        }

        String popupDelay = redisDAO.get("POP_UP_DELAY");
        webinarResponse.setPopUpDeplay(Long.parseLong(popupDelay));
        String fosEnvironment = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
        if (StringUtils.isEmpty(fosEnvironment)) {
            fosEnvironment = "https://www.vedantu.com";
        }
        String masterClassUrl = fosEnvironment + "/masterclass/" + webinar.getWebinarCode() + "?utm_source=seo&utm_medium=seo_hp&utm_campaign=seo_hp-masterclass&utm_content=homepage_tiles&utm_term=seo_hp_list_masterclass";
        webinarResponse.setMasterClassUrl(masterClassUrl);

        return webinarResponse;
    }

    public WebinarResponse getWebinarByCode(FindWebinarsReq req) throws VException {
        req.verify();
        Webinar webinar = webinarDAO.getByWebinarCode(req.getWebinarCode());

        if (webinar == null) {
            throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with code:" + req.getWebinarCode());
        }
        WebinarResponse webinarResponse = null;
        String sessionId = webinar.getSessionId();
        if ((WebinarToolType.VEDANTU_WAVE.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_OTO.equals(webinar.getToolType())) && StringUtils.isNotEmpty(webinar.getSessionId())) {
            String createSessionUrl = SCHEDULING_ENDPOINT
                    + "/onetofew/session/" + sessionId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            OTFSessionInfo sessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);
            webinar.setSessionInfo(sessionInfo);
            webinarResponse = WebinarToWebinarResponseConverter.convert(webinar);
        } else {
            webinarResponse = WebinarToWebinarResponseConverter.convert(webinar);
        }
        return webinarResponse;
    }

    public Webinar getWebinarById(String id) throws VException {
        Webinar webinar = webinarDAO.getById(id);
        if (webinar == null) {
            throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with id:" + id);
        }
        if ((WebinarToolType.VEDANTU_WAVE.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinar.getToolType()) || WebinarToolType.VEDANTU_WAVE_OTO.equals(webinar.getToolType())) && StringUtils.isNotEmpty(webinar.getSessionId())) {
            String createSessionUrl = SCHEDULING_ENDPOINT
                    + "/onetofew/session/" + webinar.getSessionId();
            ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            OTFSessionInfo sessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);
            webinar.setSessionInfo(sessionInfo);
        }
        return webinar;
    }

    public PlatformBasicResponse isWebinarCodeAvailable(FindWebinarsReq req) throws BadRequestException, NotFoundException {
        req.verify();
        Webinar webinarListing = webinarDAO.getByWebinarCode(req.getWebinarCode());
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setSuccess(true);
        if (webinarListing != null) {
            platformBasicResponse.setSuccess(false);
        }
        return platformBasicResponse;
    }

    public List<Webinar> findWebinars(FindWebinarsReq req) throws VException {
        req.verify();
        logger.info("FINDWEBINARS: " + req.getType());
        if(req.getShowSimLiveWebinars() == null) {
            req.setShowSimLiveWebinars(false);
        }
        return webinarDAO.findWebinars(req);
    }

    private void addDataFromWave(WaveWebinarResponse waveWebinarResponse, WebinarResponse webinarResponse) {
        if (waveWebinarResponse != null) {
            WaveWebinarResponsePojo result = waveWebinarResponse.getResult();
            webinarResponse.setTotalParticipants(result.getAttendedStudentCount());
            WebinarLeaderBoardData studentOwnRank = result.getStudentOwnRank();
            webinarResponse.setTotalQuizzesAsked(result.getTotalQuizzesInSession());
            webinarResponse.setTotalQuizzesCompleted(result.getQuizSoFar());
            webinarResponse.setTotalDoubtsSolved(result.getTotalDoubtsResolved());
            webinarResponse.setWebinarLeaderBoardDataList(result.getTopLeaderboardResult());
            webinarResponse.setReport(result.getReport());
            if (studentOwnRank != null) {
                webinarResponse.setMyRank(studentOwnRank.getSessionRank());
                double perc = 0.00;
                if (result.getQuizSoFar() != 0) {
                    perc = studentOwnRank.getPoints() / result.getQuizSoFar();
                }
                webinarResponse.setUserPercentile(perc);
            }
        }
    }

    public WaveWebinarResponse getWebinarPerSessionFromWave(String sessionId, String userId) throws VException {
        logger.info("method=getWebinarPerSessionFromWave, sessionId : {}, userId : {}", sessionId, userId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(WAVE_ENDPOINT + "/webinar/getWebinarInfo?sessionId="
                + sessionId + "&userId=" + userId, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("WAVE-WEBINAR-RESP: " + jsonString);
        ObjectMapper objectMapper = new ObjectMapper();

        WaveWebinarResponse webinarResponse = new Gson().fromJson(jsonString, WaveWebinarResponse.class);

        logger.info("WAVE-WEBINAR-RESP-OBJ: " + webinarResponse.toString());

        return webinarResponse;
    }

    public List<WebinarResponse> findWebinarsMobile(FindWebinarsReq req) throws VException {
        req.verify();
        logger.info("FINDWEBINARS: " + req.getType());

        if(req.getTimeFrame() != null) {
            if(TimeFrame.UPCOMING.equals(req.getTimeFrame())) {
                req.setAfterEndTime(System.currentTimeMillis());
            }
            if(TimeFrame.PAST.equals(req.getTimeFrame())) {
                req.setBeforeEndTime(System.currentTimeMillis());
            }
        }

        List<Webinar> webinars = webinarDAO.findWebinarsClassRoomMobile(req);

        List<Webinar> filteredWebinars = null;

        if(null != req.getCallingUserId()){
            filteredWebinars = filterSimLives(webinars, req.getCallingUserId().toString());
        }

        if (filteredWebinars != null && !filteredWebinars.isEmpty())
            webinars = filteredWebinars;

        List<WebinarResponse> webinarResponseList = new ArrayList<>();
        Long currentMillis = System.currentTimeMillis();

        for (Webinar webinar : webinars) {
            WebinarResponse webinarResponse = WebinarToWebinarResponseConverter.convert(webinar);

            if (webinar.getSubjects() != null && webinar.getSubjects().size() != 0) {
            	webinarResponse.setSubjects(webinar.getSubjects());
                webinarResponse.setSubject(Collections.min(webinar.getSubjects()));
            }
            String popupDelay = redisDAO.get("POP_UP_DELAY");
            webinarResponse.setPopUpDeplay(Long.parseLong(popupDelay));
            String fosEnvironment = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
            if (StringUtils.isEmpty(fosEnvironment)) {
                fosEnvironment = "https://www.vedantu.com";
            }
            String masterClassUrl = fosEnvironment + "/masterclass/" + webinar.getWebinarCode() + "?utm_source=seo&utm_medium=seo_hp&utm_campaign=seo_hp-masterclass&utm_content=homepage_tiles&utm_term=seo_hp_list_masterclass";
            webinarResponse.setMasterClassUrl(masterClassUrl);

            if (StringUtils.isNotEmpty(webinar.getSessionId()) && webinar.getStartTime() != null && webinar.getStartTime() < currentMillis) {
                String sessionId = webinar.getSessionId();
                try {
                    if (httpSessionUtils.getCallingUserId() != null) {
                        WaveWebinarResponse waveWebinarResponse = getWebinarPerSessionFromWave(sessionId, httpSessionUtils.getCallingUserId().toString());
                        if (waveWebinarResponse != null) {
                            logger.info("Response from wave={}", waveWebinarResponse);
                            addDataFromWave(waveWebinarResponse, webinarResponse);
                        }
                    }
                } catch (VException vex) {
                    logger.info("Response from WaveWebinarInfo error={}", sessionId);
                }
                if(StringUtils.isNotEmpty(redisDAO.get("HIDE_WEBINARS"))) {
                    String[] initialHideWebinars = redisDAO.get("HIDE_WEBINARS").split(",");
                    List<String> hideWebinars = Arrays.asList(initialHideWebinars);
                    if (!hideWebinars.contains(webinar.getId())) {
                        webinarResponseList.add(webinarResponse);
                    }
                } else{
                    webinarResponseList.add(webinarResponse);
                }
//                if (!webinar.getId().equals("5e4bf8c21571f202107d4168") && !webinar.getId().equals("5e4bfa2a1571f202107d429a") && !webinar.getId().equals("5e4bfa9b1c8b7b6e84a00d3f")
//                        && !webinar.getId().equals("5e4bfb051c8b7b6e84a00d98") && !webinar.getId().equals("5e4bfb521c8b7b6e84a00dd0") && !webinar.getId().equals("5e4bfbce42713d714f73ea6f")
//                        && !webinar.getId().equals("5e4bfc5242713d714f73ead1") && !webinar.getId().equals("5e4bfcfc0ed65806a979cf7b") && !webinar.getId().equals("5e4bfdadd3f2430fd18b8fa6")
//                        && !webinar.getId().equals("5e4bfe1d1571f202107d4566") && !webinar.getId().equals("5e4bfe7e1c8b7b6e84a00fd1") && !webinar.getId().equals("5e4bff1942713d714f73ecc2")
//                        && !webinar.getId().equals("5e4bff921571f202107d4695") && !webinar.getId().equals("5e4c0015d3f2430fd18b9810")) {
//                    webinarResponseList.add(webinarResponse);
//                }
            }
            else {
                String[] initialHideWebinars = redisDAO.get("HIDE_WEBINARS").split(",");
                List<String> hideWebinars = Arrays.asList(initialHideWebinars);
                if(!hideWebinars.contains(webinar.getId())) {
                    webinarResponseList.add(webinarResponse);
                }
//                if (!webinar.getId().equals("5e4bf8c21571f202107d4168") && !webinar.getId().equals("5e4bfa2a1571f202107d429a") && !webinar.getId().equals("5e4bfa9b1c8b7b6e84a00d3f")
//                        && !webinar.getId().equals("5e4bfb051c8b7b6e84a00d98") && !webinar.getId().equals("5e4bfb521c8b7b6e84a00dd0") && !webinar.getId().equals("5e4bfbce42713d714f73ea6f")
//                        && !webinar.getId().equals("5e4bfc5242713d714f73ead1") && !webinar.getId().equals("5e4bfcfc0ed65806a979cf7b") && !webinar.getId().equals("5e4bfdadd3f2430fd18b8fa6")
//                        && !webinar.getId().equals("5e4bfe1d1571f202107d4566") && !webinar.getId().equals("5e4bfe7e1c8b7b6e84a00fd1") && !webinar.getId().equals("5e4bff1942713d714f73ecc2")
//                        && !webinar.getId().equals("5e4bff921571f202107d4695") && !webinar.getId().equals("5e4c0015d3f2430fd18b9810")) {
//                    webinarResponseList.add(webinarResponse);
//                }
            }
        }
//            webinarResponseList.add(webinarResponse);

        logger.info("Webinar response list : {}", webinarResponseList);

        Long callingUserId = req.getCallingUserId() != null ?  req.getCallingUserId() : httpSessionUtils.getCallingUserId();

        String callingUserIdString = callingUserId != null ? callingUserId.toString() : "";

// Commenting as per discussion with Nitin, this A/B Testing was never evolved later - GA-2346
//        if (req.getAppVersionCode() != null && req.getAppVersionCode().compareTo("1.6.2") >= 0 && req.getAfterEndTime() != null && req.getBeforeEndTime() == null
//                && hasNeverAttendedWebinar(callingUserIdString) && webinarResponseList.size() > 1)
//            return webinarResponseList.subList(0,1);

        return webinarResponseList;
    }

    public Webinar getUpcomingWebinar(FindWebinarsReq req) throws VException {
        req.setAfterStartTime(System.currentTimeMillis());
        return webinarDAO.findWebinar(req);
    }

    public WebinarRegisterUserRes webinarRegister(WebinarRegistrationReq webinarRegistrationReq) throws VException {
        webinarRegistrationReq.verify();
        WebinarRegisterUserRes response;
        Webinar webinarListing = webinarDAO.getById(webinarRegistrationReq.getWebinarId());
        if (webinarListing == null) {
            throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with id:" + webinarRegistrationReq.getWebinarId());
        }
        Long callingUserId = (httpSessionUtils.getCallingUserId() != null)?
                httpSessionUtils.getCallingUserId() : webinarRegistrationReq.getUserId();

        if (callingUserId == null) {
            throw new NotFoundException(ErrorCode.NOT_LOGGED_IN, "No calling userId found");
        }

        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(callingUserId, true);
        if (userInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "No user found with id:" + callingUserId);
        }
//        if(StringUtils.isEmpty(userInfo.getEmail())){
//            throw new BadRequestException(ErrorCode.EMAIL_NOT_FOUND, "Email not found for registering to  webinar");
//        }

        WebinarUserRegistrationInfo user = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(userInfo.getUserId().toString(), webinarRegistrationReq.getWebinarId());
        if (user != null) {
            user.updateUserBasicInfo(userInfo);
            webinarUserRegistrationInfoDAO.create(user);
            response = mapper.map(user, WebinarRegisterUserRes.class);
            if (!WebinarUserRegistrationStatus.OTP_VERIFICATION_PENDING.equals(response.getStatus())) {
                response.setStatus(WebinarUserRegistrationStatus.ALREADY_REGISTERED);
                response.setEmailId(userInfo.getEmail());
                return response;
            } else {
                user.setStatus(WebinarUserRegistrationStatus.CONFIRMED);
            }
        } else {
            user = mapper.map(webinarRegistrationReq, WebinarUserRegistrationInfo.class);
            user.updateUserBasicInfo(userInfo);
            user.setStatus(WebinarUserRegistrationStatus.CONFIRMED);
        }
        if (WebinarToolType.GTW.equals(webinarListing.getToolType())) {
            user.setTrainingId(webinarListing.getGtwWebinarId());
        }
        webinarUserRegistrationInfoDAO.create(user);

        postWebinarRegistrationConfirmed(user, webinarListing);

//        if(!"10".equalsIgnoreCase(user.getGrade())){
//            sendWebinarRegistrationNotification(user);
//        }

        //reviseindia
        reviseManager.addToRemindWebinar(user.getUserId(), user.getWebinarId());
        response = mapper.map(user, WebinarRegisterUserRes.class);
        response.setEmailId(userInfo.getEmail());
        logger.info("webinarRegister-response: " + response);
        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.WEBINAR_REGISTERED, gson.toJson(user), user.getUserId() + "_WEBINAR_REGISTERED");
        return response;
    }

    //    private void addDataFromNewRequest(WebinarUserRegistrationInfo webinarUserRegistrationInfo, WebinarUserRegistrationInfo req) {
//        if(StringUtils.isNotEmpty(req.getGrade())) {
//            webinarUserRegistrationInfo.setGrade(req.getGrade());
//        }
//        if(StringUtils.isNotEmpty(req.getEmailId())) {
//            webinarUserRegistrationInfo.setEmailId(req.getEmailId());
//        }
//        if(StringUtils.isNotEmpty(req.getFirstName())) {
//            webinarUserRegistrationInfo.setFirstName(req.getFirstName());
//        }
//    }
//
//    private String getMaskedPhoneNumber(String inputPhoneNum) {
//        return inputPhoneNum.replaceAll(".(?=.{4})", "*");
//    }
//
//    private String getMaskedEmail(String email) {
//        return email.replaceAll("(^[^@]{3}|(?!^)\\G)[^@]", "$1*");
//    }
    public void postWebinarRegistrationConfirmed(WebinarUserRegistrationInfo webinarUserRegistrationInfo, Webinar webinarInfo) throws VException {
        if (webinarInfo == null) {
            webinarInfo = webinarDAO.getById(webinarUserRegistrationInfo.getWebinarId());
            if (webinarInfo == null) {
                throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with code:" + webinarUserRegistrationInfo.getWebinarId());
            }
        }
        Long currentTime = System.currentTimeMillis();

        if (WebinarToolType.GTW.equals(webinarInfo.getToolType()) && webinarInfo.getEndTime() > currentTime) {
            registerWebinarGTW(webinarUserRegistrationInfo);
        } else if ((WebinarToolType.YOUTUBE.equals(webinarInfo.getToolType()) || WebinarToolType.VEDANTU_WAVE.equals(webinarInfo.getToolType()) || WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinarInfo.getToolType()) || WebinarToolType.VEDANTU_WAVE_OTO.equals(webinarInfo.getToolType())) && webinarInfo.getEndTime() > currentTime) {
            registerWebinarYoutube(webinarUserRegistrationInfo, webinarInfo);
        } else {
            awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "WEBINAR_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
        }
    }

    public PlatformBasicResponse sendWebinarRegistrationVerificationOTP(WebinarUserRegistrationInfo webinarUserRegistrationInfo) throws VException {
        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(webinarUserRegistrationInfo.getUserId(), true);
        String phone = userInfo.getContactNumber();
        PlatformBasicResponse response = new PlatformBasicResponse();
        response.setSuccess(false);
        if (StringUtils.isEmpty(phone)) {
            return response;
        }

        WebinarUserRegistrationToken webinarUserRegistrationToken = webinarUserRegistrationTokenDAO.getTokenByPhone(phone, webinarUserRegistrationInfo.getWebinarId());
        if (webinarUserRegistrationToken == null) {
            String code = StringUtils
                    .randomNumericString(WebinarUserRegistrationToken.VERIFICATION_CODE_LENGTH);
            webinarUserRegistrationToken = new WebinarUserRegistrationToken(code, phone, webinarUserRegistrationInfo.getWebinarId());
            webinarUserRegistrationTokenDAO.upsert(webinarUserRegistrationToken);
        }
        Webinar webinarListing = webinarDAO.getById(webinarUserRegistrationInfo.getWebinarId());
        String smsText = "To complete your registration for the webinar  "
                + " on " + webinarListing.getTitle()
                + ". Please use this verification code: " + webinarUserRegistrationToken.getVerificationCode();
        sendSMS(smsText, phone);
        response.setSuccess(true);
        return response;
    }

    public WebinarRegisterUserRes addQuestionToRegistration(AddQuestionToRegistrationReq req) throws VException {
        req.verify();

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfoFromEmail(req.getEmail(),false);
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(userBasicInfo.getUserId().toString(), req.getWebinarId());
        if (webinarUserRegistrationInfo == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No webinar registration found for  with id:" + req.getWebinarId() + ", getEmail:" + req.getEmail());
        }
        webinarUserRegistrationInfo.setSource(req.getSource());
        WebinarRegistrationQuestion question = new WebinarRegistrationQuestion(req.getQuestion());
        webinarUserRegistrationInfo.addQuestion(question);
        webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
        WebinarRegisterUserRes response = mapper.map(webinarUserRegistrationInfo, WebinarRegisterUserRes.class);
        response.setSource(webinarUserRegistrationInfo.getSource());
        response.setEmailId(userBasicInfo.getEmail());
        return response;
    }

    public WebinarUserRegistrationInfo confirmWebinarRegistrationVerificationOTP(ConfirmWebinarRegisterOTPReq req) throws VException {
        req.verify();

        UserBasicInfo userInfo = fosUtils.getUserBasicInfoFromContactNumber(req.getPhone());

        WebinarUserRegistrationInfo webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfo(userInfo.getUserId().toString(), req.getWebinarId());
        if (webinarUserRegistrationInfo == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No webinar registration found for  with id:" + req.getWebinarId() + ", phone:" + req.getPhone());
        }

        String phone = req.getPhone();
        WebinarUserRegistrationToken webinarUserRegistrationToken = webinarUserRegistrationTokenDAO.getTokenByPhone(phone, req.getWebinarId());
        if (webinarUserRegistrationToken == null || !webinarUserRegistrationToken.getVerificationCode().equalsIgnoreCase(req.getOtp())) {
            throw new BadRequestException(ErrorCode.INVALID_TOKEN, "this token is not valid" + req.getWebinarId() + ", phone:" + req.getPhone());
        }
        if (WebinarUserRegistrationStatus.CONFIRMED.equals(webinarUserRegistrationInfo.getStatus())) {
            return trimWebinarUserRegistrationInfo(webinarUserRegistrationInfo);
        }
        Webinar webinarListing = webinarDAO.getById(webinarUserRegistrationInfo.getWebinarId());
        webinarUserRegistrationInfo.setStatus(WebinarUserRegistrationStatus.CONFIRMED);
        webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
        postWebinarRegistrationConfirmed(webinarUserRegistrationInfo, webinarListing);
        return trimWebinarUserRegistrationInfo(webinarUserRegistrationInfo);
    }

    public PlatformBasicResponse updateWebinarAttended(GetWebinarRegistrationInfoReq req) throws VException {
        req.verify();
        PlatformBasicResponse response = new PlatformBasicResponse();
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(req.getUserId(), req.getWebinarId());
        Webinar webinarListing = webinarDAO.getById(req.getWebinarId());
        if (webinarUserRegistrationInfo == null) {
            if (WebinarType.QUIZ.equals(webinarListing.getType()) || WebinarType.LIVECLASS.equals(webinarListing.getType())) {
                WebinarRegistrationReq webinarRegistrationReq = new WebinarRegistrationReq();
                webinarRegistrationReq.setWebinarId(req.getWebinarId());
                if (httpSessionUtils.getCurrentSessionData() != null && httpSessionUtils.getCurrentSessionData().getGrade() != null) {
                    webinarRegistrationReq.setGrade(httpSessionUtils.getCurrentSessionData().getGrade());
                }

                webinarRegister(webinarRegistrationReq);

                // sending event to clevertap
                webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(req.getUserId(), req.getWebinarId());
                logger.info("GOT_WebinarUserRegistrationInfo: " + webinarUserRegistrationInfo.getUserId());
                if (WebinarType.QUIZ.equals(webinarListing.getType())) {
                    awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "VBRAINER_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
                } else {
                    awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "LIVECLASS_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
                }
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No webinar registration found for  with id:" + req.getWebinarId() + ", phone:" + req.getUserId());
            }
        }

//        Webinar webinarListing = webinarListingDAO.getById(webinarUserRegistrationInfo.getWebinarId());
        if (webinarListing == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No webinar found for  with id:" + req.getWebinarId() + ", phone:" + req.getUserId());
        }

        if (!WebinarToolType.VEDANTU_WAVE.equals(webinarListing.getToolType())
                && !WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinarListing.getToolType())
                && !WebinarToolType.YOUTUBE.equals(webinarListing.getToolType())) {
            response.setSuccess(false);
            return response;
        }
        Long currentTime = System.currentTimeMillis();
        if ((currentTime + 30 * DateTimeUtils.MILLIS_PER_MINUTE) < webinarListing.getStartTime()) {
            return response;
        }

        if (webinarListing.getEndTime() != null && currentTime <= webinarListing.getEndTime() + 15 * DateTimeUtils.MILLIS_PER_MINUTE) {
            webinarUserRegistrationInfo.setWebinarAttended(true);
        } else {
            webinarUserRegistrationInfo.setReplayWatched(true);
        }
        webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);

        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.WEBINAR_ATTENDED, gson.toJson(webinarUserRegistrationInfo), webinarUserRegistrationInfo.getUserId() + "_WEBINAR_REGISTERED");

        return response;
    }

    public WebinarUserRegistrationInfo getWebinarUserRegistrationInfo(GetWebinarRegistrationInfoReq req) throws VException {
        req.verify();

        String userId = req.getUserId();

        if(userId == null)
            userId = req.getCallingUserId().toString();

        String key = "WEBINARUSERREGITRATIONINFO_" + req.getUserId() + "_" + req.getWebinarId();
        if(null != req.getUserId() && null != req.getWebinarId()){
            try {
                String webianrStr = redisDAO.get(key);
                if (webianrStr != null) {
                    return gson.fromJson(webianrStr, WebinarUserRegistrationInfo.class);
                }
            } catch (InternalServerErrorException | BadRequestException e) {
                logger.error(e.getErrorMessage(), e);
            }
        }

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(userBasicInfo.getUserId().toString(), req.getWebinarId());
        logger.info("webinarUserRegistrationInfo : " + webinarUserRegistrationInfo);
        logger.info("trimWebinarUserRegistrationInfo(webinarUserRegistrationInfo) : " + trimWebinarUserRegistrationInfo(webinarUserRegistrationInfo));
        if(null != webinarUserRegistrationInfo){
            redisDAO.set(key, gson.toJson(webinarUserRegistrationInfo));
        }
        return trimWebinarUserRegistrationInfo(webinarUserRegistrationInfo);
    }

    public List<WebinarRegisterUserRes> getWebinarUserRegistrationInfos(String email, List<String> webinarIds) throws VException {

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfoFromEmail(email, false);

        String userId = "";
        if(userBasicInfo != null && userBasicInfo.getUserId() != null){
            userId = userBasicInfo.getUserId().toString();
        }else {
        	if(null != httpSessionUtils.getCallingUserId()) {
        		userId = httpSessionUtils.getCallingUserId().toString();
        	}
        }
        List<WebinarUserRegistrationInfo> infos = new ArrayList<WebinarUserRegistrationInfo>();
        if(StringUtils.isNotEmpty(userId)) {
        	infos = webinarUserRegistrationInfoDAO.getWebinarUserInfo(userId, webinarIds);
        }
        List<WebinarRegisterUserRes> response = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(infos)) {
            for (WebinarUserRegistrationInfo info : infos) {
                WebinarRegisterUserRes resp = mapper.map(info, WebinarRegisterUserRes.class);
                resp.setEmailId(email);
                response.add(resp);
            }
        }
        return response;
    }

    private WebinarUserRegistrationInfo trimWebinarUserRegistrationInfo(WebinarUserRegistrationInfo info) {
        if (info != null) {
            info.setFirstName(null);
            info.setLastName(null);
            info.setAttendanceIntervals(null);
            info.setOtpVerificationRequired(null);
            info.setRegisterRegistrantKey(null);
        }
        return info;
    }

    public WebinarUserRegistrationInfo registerForGTWWebinar(String id) throws VException {
        if (StringUtils.isEmpty(id)) {
            return null;
        }
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getById(id);
        if (webinarUserRegistrationInfo == null) {
            return null;
        }
        registerWebinarGTW(webinarUserRegistrationInfo);
        return webinarUserRegistrationInfo;
    }

    private void registerWebinarYoutube(WebinarUserRegistrationInfo webinarUserRegistrationInfo, Webinar webinarInfo) throws VException {
        if (webinarInfo == null) {
            webinarInfo = webinarDAO.getById(webinarUserRegistrationInfo.getWebinarId());
            if (webinarInfo == null) {
                throw new NotFoundException(ErrorCode.WEBINAR_NOT_FOUND, "No webinar found with code:" + webinarUserRegistrationInfo.getWebinarId());
            }
        }

        String registerUrl;
        if (WebinarType.QUIZ.equals(webinarInfo.getType())) {
            registerUrl = String.format("%s/campaigns/vbrainer?utm_source=vbrainer_registration&utm_medium=sms&utm_campaign=quizID_%s", FOS_ENDPOINT, webinarInfo.getId());
        } else if (WebinarType.LIVECLASS.equals(webinarInfo.getType())) {
            registerUrl = String.format("%s/campaigns/liveclass?utm_source=vbrainer_registration&utm_medium=sms&utm_campaign=quizID_%s", FOS_ENDPOINT, webinarInfo.getId());
        } else {
            registerUrl = String.format("%s/masterclass/%s?utm_source=webinar_registration&utm_medium=sms&utm_campaign=webinarID_%s&showSeatConfirmedAlways=true", FOS_ENDPOINT, webinarInfo.getWebinarCode(), webinarInfo.getId());
        }
        webinarUserRegistrationInfo.setRegisterJoinUrl(registerUrl);
        logger.info("sending sms to user registered");
        String joinUrl = webinarUserRegistrationInfo.getRegisterJoinUrl();
        if (!StringUtils.isEmpty(joinUrl)) {
            String shortenedUrl = WebUtils.INSTANCE.shortenUrl(joinUrl);
            webinarUserRegistrationInfo.setRegisterJoinUrlShort(shortenedUrl);
            sdfWebinarReg.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
            String timestr = sdfWebinarReg.format(new Date(webinarInfo.getStartTime()));
            String smsText;
            if (WebinarType.QUIZ.equals(webinarInfo.getType())) {
                smsText = "You have been successfully registered for the "
                        + "LIVE online quiz on " + webinarInfo.getTitle()
                        + ". Please click on the link " + shortenedUrl
                        + " to JOIN the session 10 mins before the scheduled time - "
                        + timestr;
                awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "VBRAINER_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
            } else if (WebinarType.LIVECLASS.equals(webinarInfo.getType())) {
                smsText = "You have been successfully registered for the "
                        + "LIVE online class on " + webinarInfo.getTitle()
                        + ". Please click on the link " + shortenedUrl
                        + " to JOIN the session 10 mins before the scheduled time - "
                        + timestr;
                awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "LIVECLASS_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
            } else {
                smsText = "You have been successfully registered for the "
                        + "LIVE online class on " + webinarInfo.getTitle()
                        + ". Please click on the link " + shortenedUrl
                        + " to JOIN the session 10 mins before the scheduled time - "
                        + timestr;

                if (webinarInfo.getId().equals("5cadfdbce4b091a8779c2134")) {
                    smsText = "Congrats! You have been Successfully Registered for the MastertTalk with AIIMS Toppers AIR 1 & AIR 4 on Sunday, 21st April at 7 PM.\n"
                            + "\n"
                            + "Use this link to Join the Session: https://vdnt.in/AIIMS-Talk";
                }

                if (!webinarInfo.getId().equals("5cadfdbce4b091a8779c2134")) {
                    awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "WEBINAR_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
                }
            }

            UserBasicInfo userInfo = fosUtils.getUserBasicInfo(webinarUserRegistrationInfo.getUserId(), true);
            sendSMS(smsText, userInfo.getContactNumber());
        }
        webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
    }

    private void registerWebinarGTW(WebinarUserRegistrationInfo webinarUserRegistrationInfo)
            throws VException {
        String trainingId = webinarUserRegistrationInfo.getTrainingId();

        Webinar webinar = webinarDAO.getById(webinarUserRegistrationInfo.getWebinarId());
        if (webinar == null) {
            return;
        }
        if (StringUtils.isEmpty(trainingId)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "There is no proper traningId for this webinar:" + webinarUserRegistrationInfo.getWebinarId());
        }
        String[] trainingIds = trainingId.split(",");
        for (int k = 0; k < trainingIds.length; k++) {
            // Register user in gtw
            String _newTrId = trainingIds[k];
            if (StringUtils.isEmpty(_newTrId)) {
                continue;
            }
            try {
                webinarUserRegistrationInfo.setTrainingId(_newTrId.trim());
                webinarUserRegistrationInfo = webinarGTWManager.registerWebinar(webinarUserRegistrationInfo, webinarUserRegistrationInfo.getTrainingId());
                logger.info("gtw response : " + webinarUserRegistrationInfo.toString());

                WebinarInfo webinarInfo = null;
                try {
                    // Fetch webinar info
                    webinarInfo = webinarGTWManager.getWebinarInfo(webinarUserRegistrationInfo.getTrainingId(), webinar.getGtwSeat());
                } catch (Exception ex) {
                    logger.info("Exception:" + ex.toString() + " message:" + ex.getMessage());
                }

                logger.info("sending sms to user registered");
                String joinUrl = webinarUserRegistrationInfo.getRegisterJoinUrl();
                if (!StringUtils.isEmpty(joinUrl)) {
                    String shortenedUrl = WebUtils.INSTANCE.shortenUrl(joinUrl);
                    webinarUserRegistrationInfo.setRegisterJoinUrlShort(shortenedUrl);
                    sdfWebinarReg.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                    String timestr = sdfWebinarReg.format(new Date(webinarInfo.getStartTime()));
                    String smsText = "You have been successfully registered for the "
                            + "LIVE online class on " + webinarInfo.getSubject()
                            + ". Please click on the link " + shortenedUrl
                            + " to JOIN the session 5 mins before the scheduled time on "
                            + timestr;
                    UserBasicInfo userInfo = fosUtils.getUserBasicInfo(webinarUserRegistrationInfo.getUserId(), true);
                    sendSMS(smsText, userInfo.getContactNumber());
                }
                awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "WEBINAR_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
                webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
            } catch (Exception e) {
                logger.error("Error in registering to webinar " + e.getMessage() + " registration: " + webinarUserRegistrationInfo);
            }
        }

    }

    public PlatformBasicResponse registerviamissedcall(String mobileno,
            String trainingId, String smstext) throws Exception {

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUsersByContactNumber?contactNumber="
                + mobileno, HttpMethod.GET, null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type filterListType = new TypeToken<List<User>>() {
        }.getType();
        List<User> users = new Gson().fromJson(jsonString, filterListType);

        logger.info("registering to the webinar for users " + users);
        if (ArrayUtils.isNotEmpty(users)) {
            for (User user : users) {
                WebinarUserRegistrationInfo req = new WebinarUserRegistrationInfo();
                req.setTrainingId(trainingId);
                req.setFirstName(user.getFirstName());
                req.setLastName(user.getLastName());
                registerWebinarGTW(req);
            }
        } else {
            String dummyEmail = generateRandomChars(7)
                    + trainingId.substring(trainingId.length() - 4)
                    + "@vedantu.com";
            WebinarUserRegistrationInfo newreq = new WebinarUserRegistrationInfo();
            newreq.setTrainingId(trainingId);
            newreq.setFirstName("");
            registerWebinarGTW(newreq);
        }

//        else if (com.vedantu.util.StringUtils.isNotEmpty(smstext)) {
//            TextSMSRequest textSMSRequest = new TextSMSRequest();
//            textSMSRequest.setBody(smstext);
//            textSMSRequest.setTo(mobileno);
//            ClientResponse respsms = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT
//                    + "/SMS/sendSMS", HttpMethod.POST, gson.toJson(textSMSRequest), true);
//            VExceptionFactory.INSTANCE.parseAndThrowException(respsms);
//            String respsmsJson = respsms.getEntity(String.class);
//            logger.info("respone of sendsms " + respsmsJson);
//        }
        return new PlatformBasicResponse();
    }

    private void sendSMS(String smsText, String phone) throws VException {
        TextSMSRequest textSMSRequest = new TextSMSRequest();
        textSMSRequest.setBody(smsText);
        logger.info("sendSMSPhoneWebinar: " + phone);
        textSMSRequest.setTo(phone);
        textSMSRequest.setPriorityType(SMSPriorityType.HIGH);
        textSMSRequest.setType(CommunicationType.WEBINAR_REGISTRATION);
        smsManager.sendSMSViaRest(textSMSRequest);
    }

    public void syncGtwAttendenceAsync() {
        logger.info("syncing Gtw attendence async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CMS_SYNC_WEBINAR_ATTENDEE_INFO, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendWebinarRemindersAsync() {
        logger.info("sendWebinarRemindersAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CMS_SEND_WEBINAR_REMINDERS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendWebinarRegistrationQuestionsAsync() {
        logger.info("sendWebinarRegistrationQuestionsAsync async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CMS_SEND_WEBINAR_REGISTRATION_QUESTIONS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void sendWebinarRegistrationQuestions() {
        logger.info("sendWebinarRegistrationQuestions");
        Long currentTime = System.currentTimeMillis();
        logger.info("sendWebinarRegistrationQuestions starting at time : " + currentTime);
        currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long afterStartTime = currentTime + 4 * sendWebinarRemindersCronRunTime + 1;
        Long beforeStartTime = afterStartTime + sendWebinarRemindersCronRunTime;
        logger.info("sendWebinarRegistrationQuestions afterStartTime " + afterStartTime + " beforeStartTime " + beforeStartTime);
        FindWebinarsReq req = new FindWebinarsReq();
        req.setAfterStartTime(afterStartTime);
        req.setBeforeStartTime(beforeStartTime);
        req.setWebinarStatus(WebinarStatus.ACTIVE);
        List<Webinar> webinars = webinarDAO.findWebinars(req);
        if (ArrayUtils.isNotEmpty(webinars)) {
            for (Webinar webinar : webinars) {
                try {
                    sendWebinarQuestionsEmail(webinar);
                } catch (Exception ex) {
                    logger.error("error in sending questions email for webinar:" + webinar);
                }
            }
        }
    }

    public void syncGtwAttendence() {
        try {
            logger.info("syncing Gtw attendence");
            Long currentTime = System.currentTimeMillis();
            logger.info("syncGtwAttendence starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long beforeEndTime = currentTime - 2 * syncGtwAttendenceCronRunTime;
            Long afterEndTime = beforeEndTime - syncGtwAttendenceCronRunTime;
            logger.info("syncGtwAttendence beforeEndTime " + beforeEndTime + " afterEndTime " + afterEndTime);

            List<WebinarInfo> webinarInfos = webinarGTWManager.getWebinarInfos(beforeEndTime, afterEndTime);
            if (CollectionUtils.isEmpty(webinarInfos)) {
                return;
            }
            webinarGTWManager.processWebinarAttendance(webinarInfos);

        } catch (Exception ex) {
            logger.error("error in syncGtwAttendence " + ex.getMessage());
        }

    }

    public void sendWebinarsReminders() {
        try {
            logger.info("sending webinars reminders");
            Long currentTime = System.currentTimeMillis();
            logger.info("sendWebianrReminders starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterStartTime = currentTime + sendWebinarRemindersCronRunTime + 1;
            Long beforeStartTime = afterStartTime + sendWebinarRemindersCronRunTime;
            logger.info("sendWebianrReminders afterStartTime " + afterStartTime + " beforeStartTime " + beforeStartTime);
            FindWebinarsReq req = new FindWebinarsReq();
            req.setAfterStartTime(afterStartTime);
            req.setBeforeStartTime(beforeStartTime);
            req.setWebinarStatus(WebinarStatus.ACTIVE);
            List<Webinar> webinars = webinarDAO.findWebinars(req);
            if (ArrayUtils.isNotEmpty(webinars)) {
                for (Webinar webinar : webinars) {
                    sendWebinarReminder(webinar, null, 0);
                }
            }
        } catch (Exception ex) {
            logger.error("error in sendWebianrReminders " + ex.getMessage());
        }
    }

    public void sendWebinarReminder(Webinar webinar, WebinarInfo webinarInfo, Integer start) {
        try {
            logger.info("sending webinar reminder");
            if (webinarInfo == null && WebinarToolType.GTW.equals(webinar.getToolType())) {
                webinarInfo = webinarGTWManager.getWebinarInfo(webinar.getGtwWebinarId(), webinar.getGtwSeat());
            }
            if (webinarInfo != null || WebinarType.QUIZ.equals(webinar.getType()) || WebinarType.LIVECLASS.equals(webinar.getType())) {
                Integer FETCH_SIZE = 1000;
                List<WebinarUserRegistrationInfo> registrations = webinarUserRegistrationInfoDAO.getWebinarUserRegistrationInfos(webinar.getId(), start, FETCH_SIZE);
                for (WebinarUserRegistrationInfo registration : registrations) {
                    sendWebinarReminderSms(registration, webinarInfo, webinar);
                }
                if (registrations.size() == FETCH_SIZE) {
                    sendWebinarReminder(webinar, webinarInfo, start + FETCH_SIZE);
                }
            }

        } catch (Exception ex) {
            logger.error("error in sendWebinarReminder webinar:" + webinar + " error:" + ex.getMessage());
        }
    }

    public void sendWebinarReminderSms(WebinarUserRegistrationInfo registration, WebinarInfo webinarInfo, Webinar webinar) throws VException {
        logger.info("sending sms to user registered");
        String joinUrl = registration.getRegisterJoinUrl();
        if (!StringUtils.isEmpty(joinUrl)) {
            String shortenedUrl = registration.getRegisterJoinUrlShort();
            if (StringUtils.isEmpty(shortenedUrl)) {
                shortenedUrl = WebUtils.INSTANCE.shortenUrl(joinUrl);
            }
            String subject = (WebinarToolType.GTW.equals(webinar.getToolType())) ? webinarInfo.getSubject() : webinar.getTitle();
            Long startTime = webinar.getStartTime() - System.currentTimeMillis();
            startTime = startTime / DateTimeUtils.MILLIS_PER_MINUTE;

            String smsText;
            if (WebinarType.QUIZ.equals(webinar.getType())) {
                smsText = "Reminder: Your V-Brainer quiz on "
                        + subject
                        + " will start in " + startTime + "Mins. Join the session through this link: "
                        + shortenedUrl;
            } else if (WebinarType.LIVECLASS.equals(webinar.getType())) {
                smsText = "Reminder: Your LIVE class on "
                        + subject
                        + " will start in " + startTime + "Mins. Join the session through this link: "
                        + shortenedUrl;
            } else {
                smsText = "Reminder: Your MasterClass on "
                        + subject
                        + " will start in " + startTime + " Mins. Join the session through this link: "
                        + shortenedUrl;
            }
            UserBasicInfo userInfo = fosUtils.getUserBasicInfo(registration.getUserId(), true);
            sendSMS(smsText, userInfo.getContactNumber());
        }
    }

    public void sendWebinarQuestionsEmail(Webinar webinar) throws AddressException, VException {
        logger.info("sending sendWebinarQuestionsEmail");
        if (webinar == null) {
            return;
        }
        List<WebinarUserRegistrationInfo> infos = webinarUserRegistrationInfoDAO.getWebinarUserRegistrationInfosWithQuestion(webinar.getId(), 0, 1500);
        if (ArrayUtils.isEmpty(infos)) {
            return;
        }
        logger.info("got WebinarUserRegistrationInfo info for webinarId:" + webinar.getId() + " size:" + infos.size());
        String data = "Question,Name,Grade\n";
        for (WebinarUserRegistrationInfo info : infos) {
            List<WebinarRegistrationQuestion> questions = info.getQuestions();
            if (ArrayUtils.isEmpty(questions)) {
                logger.error("got empty questions result from query" + info);
            }
            for (WebinarRegistrationQuestion question : questions) {
                StringBuilder entry = new StringBuilder();
                entry.append(getCSVEscapedString(question.getQuestion()));
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString(info.getFirstName()));
                entry.append(CSV_DELIMITER);
                entry.append(getCSVEscapedString(info.getGrade()));
                entry.append("\n");
                data += (entry.toString());
            }
        }
        communicationManager.sendWebinarQuestionsEmail(webinar, data, webinarCCList);
    }

    private String getCSVEscapedString(String value) {
        String output = value != null ? value.replace(",", "#") : value;
        output = output != null ? output.replace("\n", "#") : output;
        return output;
    }

    public FindQuizzesRes findQuizzes(WebinarType type) throws BadRequestException, NotFoundException {
        FindWebinarsReq req = new FindWebinarsReq();
        req.setWebinarStatus(WebinarStatus.ACTIVE);
        req.setStart(0);

        if (WebinarType.QUIZ.equals(type)) {
            req.setType(type);
            req.setSize(2);
        }
        if (WebinarType.LIVECLASS.equals(type)) {
            req.setType(type);
            req.setSize(5);
        }

        req.setSortOrder(SortOrder.DESC);
        req.setBeforeEndTime(System.currentTimeMillis());
        List<Webinar> pastWebinars = webinarDAO.findWebinars(req);

        req.setSize(5);
        req.setBeforeEndTime(null);
        req.setAfterEndTime(System.currentTimeMillis());
        req.setSortOrder(SortOrder.ASC);
        List<Webinar> futureWebinars = webinarDAO.findWebinars(req);

        FindQuizzesRes res = new FindQuizzesRes();
        res.setPastWebinars(pastWebinars);
        res.setLiveOrUpcomingWebinars(futureWebinars);
        return res;
    }

    public List<CourseMobileRes> getWebinars(String board, String grade, Integer start, Integer size) {
        List<Webinar> webinars = webinarDAO.findWebinarsByGradeAndBoard(board, grade, start, size);
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();
        if (ArrayUtils.isEmpty(webinars)) {
            return courseMobileReses;
        }

        for (Webinar webinar : webinars) {

            courseMobileReses.add(toCourseMobileRes(webinar));
        }

        return courseMobileReses;

    }

    public CourseMobileRes toCourseMobileRes(Webinar webinar) {
        logger.info("converting webinar " + webinar.getId());
        CourseMobileRes courseMobileRes = new CourseMobileRes();
        courseMobileRes.setDuration((int) (webinar.getDuration() * (long) DateTimeUtils.MILLIS_PER_MINUTE));
        courseMobileRes.setTitle(webinar.getTitle());
        courseMobileRes.setStartsFrom(webinar.getStartTime());
        courseMobileRes.setSubjects(webinar.getSubjects());
        courseMobileRes.setType(EntityType.WEBINAR);
        courseMobileRes.setId(webinar.getId());
        courseMobileRes.setWebLink(FOS_ENDPOINT + String.format(WEBINAR_URL, webinar.getWebinarCode()));
        if (webinar.getTeacherInfo() != null && webinar.getTeacherInfo().get("name") != null) {
            String takenBy = (String) webinar.getTeacherInfo().get("name");
            logger.info("taken by " + takenBy);
            courseMobileRes.setTakenBy(takenBy);
        }
        return courseMobileRes;
    }

    public List<CourseMobileRes> getWebinarsForPast(String board, String grade, Integer start, Integer size) {
        List<Webinar> webinars = webinarDAO.findWebinarsByGradeAndBoardPast(board, grade, start, size);
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();
        if (ArrayUtils.isEmpty(webinars)) {
            return courseMobileReses;
        }

        for (Webinar webinar : webinars) {

            courseMobileReses.add(toCourseMobileRes(webinar));
        }

        return courseMobileReses;

    }

    public List<CourseMobileRes> getWebinarsForUser(String userId, Integer start, Integer size) {

        List<WebinarUserRegistrationInfo> webinarUserRegistrationInfos = webinarUserRegistrationInfoDAO.getWebinarRegistrationsForMobileUser(userId, start, size);

        logger.info(webinarUserRegistrationInfos);
        Set<String> webinarIds = new HashSet<>();

        if (ArrayUtils.isEmpty(webinarUserRegistrationInfos)) {
            return new ArrayList<>();
        }

        Map<String, WebinarUserRegistrationInfo> regMap = new HashMap<>();
        for (WebinarUserRegistrationInfo webinarUserRegistrationInfo : webinarUserRegistrationInfos) {
            webinarIds.add(webinarUserRegistrationInfo.getWebinarId());
            regMap.put(webinarUserRegistrationInfo.getWebinarId(), webinarUserRegistrationInfo);
        }

        List<Webinar> webinars = webinarDAO.getLiveOrUpcomingWebinarsForIds(webinarIds);
        logger.info(webinars);
        logger.info(regMap);
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();

        if (ArrayUtils.isEmpty(webinars)) {
            return new ArrayList<>();
        }

        for (Webinar webinar : webinars) {
            CourseMobileRes courseMobileRes = toCourseMobileRes(webinar);

            if (webinar.getWebinarInfo() != null && webinar.getWebinarInfo().containsKey("replayUrl")) {
                courseMobileRes.setWebinarUrl(webinar.getWebinarInfo().get("replayUrl").toString());
            }
            if (regMap.get(webinar.getId()) != null) {
                courseMobileRes.setRegisteredOn(regMap.get(webinar.getId()).getCreationTime());
            }
            logger.info("adding " + courseMobileRes);
            courseMobileReses.add(courseMobileRes);
        }

        return courseMobileReses;
    }

    public WebinarCallbackData webinarRequestCallback(WebinarCallbackReq req, Long userId) throws BadRequestException {
        req.verify();
        WebinarCallbackData webinarCallbackData = webinarDAO.getWebinarCallbackWithUser(userId);
        if (webinarCallbackData == null) {
            webinarCallbackData = new WebinarCallbackData();
            webinarCallbackData.setTime(req.getTime());
            webinarCallbackData.setUserId(userId);
            webinarCallbackData.setWebinarId(req.getWebinarId());
        } else {
            webinarCallbackData.setTime(req.getTime());
            webinarCallbackData.setWebinarId(req.getWebinarId());
        }
        webinarDAO.upsert(webinarCallbackData, userId);
        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.WEBINAR_MOBILE_CALLBACK, gson.toJson(webinarCallbackData), userId.toString() + "_WEBINAR_MOBILE_CALLBACK");
        return webinarCallbackData;
    }

    public WebinarCallbackData getWebinarCallbackInfo(Long userId) throws NotFoundException {
        WebinarCallbackData webinarCallbackData = webinarDAO.getWebinarCallbackWithUser(userId);

        logger.info("WEBINAR-CALLBACK-DATA: " + webinarCallbackData);

        if (webinarCallbackData == null) {
//            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No webinarcallbackdata found with user id:" + userId.toString());
            webinarCallbackData = new WebinarCallbackData();
            webinarCallbackData.setInboundNumber("08047097519");
        } else {
            Long currentMillis = System.currentTimeMillis();
            if (webinarCallbackData.getTime() != null && currentMillis > webinarCallbackData.getTime()) {
                webinarCallbackData.setTime(null);
            }
            webinarCallbackData.setInboundNumber("08047097519");
        }
        return webinarCallbackData;
    }

    public String getHlsUrlVimeo(String vimeoId) throws VException {

        if (StringUtils.isNotEmpty(vimeoId)) {
            ClientResponse vimeoMetadata = WebUtils.INSTANCE.doCall("https://api.vimeo.com/videos/" + vimeoId, HttpMethod.GET, null, "bearer 3124b1decd94ef96232a6488c1bd5526");
            VExceptionFactory.INSTANCE.parseAndThrowException(vimeoMetadata);
            String jsonString = vimeoMetadata.getEntity(String.class);

            JsonObject vimeoVideoMetadata = new Gson().fromJson(jsonString, JsonObject.class);

            String secureLink = "";
            try {
                JsonArray fileLinks = vimeoVideoMetadata.get("files").getAsJsonArray();
                for (int i = 0; i < fileLinks.size(); i++) {
                    JsonObject jo = fileLinks.get(i).getAsJsonObject();
                    if (StringUtils.isNotEmpty(jo.get("quality").getAsString()) && jo.get("quality").getAsString().equals("hls")) {
                        secureLink = jo.get("link").getAsString();
                        if (StringUtils.isEmpty(secureLink)) {
                            secureLink = jo.get("link_secure").getAsString();
                        }
                        return secureLink;
                    }
                }
            } catch (Exception ex) {
                //
            }
            return secureLink;
        } else {
            return "";
        }
    }

    public void prepareAlertForWebinarRegisteredUsers() throws VException {
        Instant now = Instant.now();
        Instant threeHourFiveMin = now.plus(187, ChronoUnit.MINUTES);
        logger.info("WEBINAR MARKETING MAIL CRON");
        logger.info("WEBINAR QUERY START TIME " + now.atZone(ZoneId.systemDefault()));
        logger.info("WEBINAR QUERY END TIME " + threeHourFiveMin.atZone(ZoneId.systemDefault()));

        List<Webinar> webinars = webinarDAO.getWebinarsByStartTime(now.toEpochMilli(), threeHourFiveMin.toEpochMilli());
        for (Webinar webinar : webinars) {
            long timeDiff = webinar.getStartTime() - now.toEpochMilli();

            WebinarNotifcation toSendNotifcationType = null;
            /*if (timeDiff >= (DateTimeUtils.MILLIS_PER_MINUTE * 180) && timeDiff <= (DateTimeUtils.MILLIS_PER_MINUTE * 187)) {
                toSendNotifcationType = WebinarNotifcation.WEBINAR_3_HRS;
            } else */if (timeDiff >= (DateTimeUtils.MILLIS_PER_HOUR) && timeDiff <= (DateTimeUtils.MILLIS_PER_MINUTE * 67)) {
                toSendNotifcationType = WebinarNotifcation.WEBINAR_1_HR;
            }/* else if (timeDiff >= (DateTimeUtils.MILLIS_PER_MINUTE * 15) && timeDiff < (DateTimeUtils.MILLIS_PER_MINUTE * 22)) {
                toSendNotifcationType = WebinarNotifcation.WEBINAR_15_MIN;
            } */else if (timeDiff >= (DateTimeUtils.MILLIS_PER_MINUTE * 5) && timeDiff < (DateTimeUtils.MILLIS_PER_MINUTE * 10)) {
                toSendNotifcationType = WebinarNotifcation.WEBINAR_LIVE;
            }

            if (toSendNotifcationType != null) {
                int start = 0, size = 100;
                List<WebinarUserRegistrationInfo> registrationInfos = webinarUserRegistrationInfoDAO.getWebinarUserRegistrationInfos(webinar.getId(), start, size);

                while (registrationInfos != null && !registrationInfos.isEmpty()) {

                    List<Long> userIds = registrationInfos.parallelStream()
                            .filter(info -> StringUtils.isNotEmpty(info.getUserId()))
                            .map(info -> Long.parseLong(info.getUserId()))
                            .collect(Collectors.toList());

                    Map<Long,User> usersMap = fosUtils.getUserInfosMap(userIds, false);

                    for (WebinarUserRegistrationInfo registrationInfo : registrationInfos) {

                        if(StringUtils.isNotEmpty(registrationInfo.getUserId())) {
                            Long userId = Long.parseLong(registrationInfo.getUserId());
                            User user = usersMap.get(userId);
                            /*if (canIgnoreNotificationForUser(registrationInfo, user))
                                continue;*/
                        }

                        Set<WebinarNotifcation> notifcations = registrationInfo.getNotifcations();

                        if (!notifcations.contains(WebinarNotifcation.WEBINAR_1_HR) && WebinarNotifcation.WEBINAR_1_HR.equals(toSendNotifcationType)) {
                            registrationInfo.addNotifcations(WebinarNotifcation.WEBINAR_1_HR);
                            sendWebinarNotification(WebinarNotifcation.WEBINAR_1_HR, registrationInfo, webinar);
                        } else if (!notifcations.contains(WebinarNotifcation.WEBINAR_LIVE) && WebinarNotifcation.WEBINAR_LIVE.equals(toSendNotifcationType)) {
                            registrationInfo.addNotifcations(WebinarNotifcation.WEBINAR_LIVE);
                            sendWebinarNotification(WebinarNotifcation.WEBINAR_LIVE, registrationInfo, webinar);
                        }

                        webinarUserRegistrationInfoDAO.save(registrationInfo);
                    }
                    start += size;
                    registrationInfos = webinarUserRegistrationInfoDAO.getWebinarUserRegistrationInfos(webinar.getId(), start, size);
                }
            }
        }

    }

    private boolean canIgnoreNotificationForUser(WebinarUserRegistrationInfo registrationInfo, User user) {

        if(user!=null && FeatureSource.DOUBT_APP.equals(user.getSignUpFeature()))
            return true;

        if(StringUtils.isEmpty(registrationInfo.getGrade()))
            return false;

        switch(registrationInfo.getGrade()){
            case "10":
            case "12":
            case "13": return true;
            default: return false;
        }

    }

    private void sendWebinarNotification(WebinarNotifcation notifcation, WebinarUserRegistrationInfo registrationInfo, Webinar webinar) throws VException {

        Map<String, Object> bodyScopes = new HashMap<>();
        String webinarStartUntill = WebinarNotifcation.WEBINAR_3_HRS.equals(notifcation) ? "3 Hours"
                : WebinarNotifcation.WEBINAR_1_HR.equals(notifcation) ? "60 Minutes"
                : WebinarNotifcation.WEBINAR_15_MIN.equals(notifcation) ? "15 Minutes"
                : WebinarNotifcation.WEBINAR_LIVE.equals(notifcation) ? "" : "";

        bodyScopes.put("webinarStartUntill", webinarStartUntill);
        bodyScopes.put("MasterClassTitle", webinar.getTitle());
        bodyScopes.put("TeacherName", webinar.getTeacherInfo().get("name"));
        bodyScopes.put("ClassStartTime", Instant.ofEpochMilli(webinar.getStartTime()).atZone(ZoneId.of("Asia/Kolkata"))
                .format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy, hh:mm a")) + " IST");
        bodyScopes.put("Link", registrationInfo.getRegisterJoinUrlShort());

        TextSMSRequest textSMSRequest;
        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(registrationInfo.getUserId(), true);
        if (StringUtils.isNotEmpty(userInfo.getContactNumber())) {
            if (WebinarNotifcation.WEBINAR_LIVE.equals(notifcation)) {
                textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(), userInfo.getPhoneCode(),
                        bodyScopes, CommunicationType.WEBINAR_LIVE_NOTIFICATION, Role.STUDENT);

            } else {
                textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(), userInfo.getPhoneCode(),
                        bodyScopes, CommunicationType.WEBINAR_NOTIFICATION, Role.STUDENT);
            }
            smsManager.sendSMS(textSMSRequest);
        }
        if (WebinarNotifcation.WEBINAR_1_HR.equals(notifcation)) {
            if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                sendWebinarAlertBeforeOneHour(registrationInfo);
            }
        }
    }

    private void sendWebinarAlertBeforeOneHour(WebinarUserRegistrationInfo user) throws VException {
        try {
            CommunicationType emailType = CommunicationType.WEBINAR_EMAIL_ALERT_BEFORE_1_HOUR;
            HashMap<String, Object> bodyScopes = new HashMap<>();

            Webinar webinar = webinarDAO.getById(user.getWebinarId());
            bodyScopes.put("Name", StringUtils.defaultIfEmpty(user.getFirstName()) + " " + StringUtils.defaultIfEmpty(user.getLastName()));
            bodyScopes.put("Title", webinar.getTitle());
            bodyScopes.put("Time", Instant.ofEpochMilli(webinar.getStartTime()).atZone(ZoneId.of("Asia/Kolkata"))
                    .format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy, hh:mm a")) + " (IST)");
            bodyScopes.put("Link", user.getRegisterJoinUrlShort());
            if (webinar.getTeacherInfo() != null) {
                bodyScopes.put("TeacherName", StringUtils.defaultIfEmpty(webinar.getTeacherInfo().get("name").toString()));
            }
            bodyScopes.put("webinarID", webinar.getId());

            HashMap<String, Object> subjectScopes = new HashMap<>();

            ArrayList<InternetAddress> toAddress = new ArrayList<>();
            UserBasicInfo userInfo = fosUtils.getUserBasicInfo(user.getUserId(), true);
            if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                toAddress.add(new InternetAddress(userInfo.getEmail()));
                EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.STUDENT);
                request.setIncludeHeaderFooter(Boolean.FALSE);
                communicationManager.sendEmail(request);
            }
        } catch (AddressException e) {
            logger.error(e);
            throw new VException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    private void sendWebinarRegistrationNotification(WebinarUserRegistrationInfo user) throws VException {
        try {
            CommunicationType emailType = CommunicationType.WEBINAR_REGISTRATION;
            HashMap<String, Object> bodyScopes = new HashMap<>();

            Webinar webinar = webinarDAO.getById(user.getWebinarId());
            bodyScopes.put("Name", StringUtils.defaultIfEmpty(user.getFirstName()) + " " + StringUtils.defaultIfEmpty(user.getLastName()));
            bodyScopes.put("Title", webinar.getTitle());
            bodyScopes.put("Time", Instant.ofEpochMilli(webinar.getStartTime()).atZone(ZoneId.of("Asia/Kolkata"))
                    .format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy, hh:mm a")) + " (IST)");
            bodyScopes.put("Link", user.getRegisterJoinUrlShort());

            HashMap<String, Object> subjectScopes = new HashMap<>();

            ArrayList<InternetAddress> toAddress = new ArrayList<>();

            UserBasicInfo userInfo = fosUtils.getUserBasicInfo(user.getUserId(), true);
            if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                toAddress.add(new InternetAddress(userInfo.getEmail()));
                EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.STUDENT);
                request.setIncludeHeaderFooter(Boolean.FALSE);
                communicationManager.sendEmail(request);
            }
        } catch (AddressException e) {
            logger.error(e);
            throw new VException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    public void prepareEmailForWebinarCompleted() throws VException {
        Instant now = Instant.now();
        Instant twohourback = now.minus(106, ChronoUnit.MINUTES);
        Instant halfanhour = now.minus(90, ChronoUnit.MINUTES);
        List<Webinar> Endedwebinars = webinarDAO.getWebinarsByEndTime(twohourback.toEpochMilli(), halfanhour.toEpochMilli());
        for (Webinar webinar : Endedwebinars) {
            logger.info("WEBINAR_COMPLETED_ID " + webinar.getId());
            Set<String> processed = new HashSet<>();
            int start = 0, size = 100;
            List<WebinarUserRegistrationInfo> registrationInfos = webinarUserRegistrationInfoDAO.getWebinarUserRegistrationInfos(webinar.getId(), start, size);
            while (registrationInfos != null && !registrationInfos.isEmpty()) {
                logger.info("START_SIZE: " + start + " " + size);

                List<Long> userIds = registrationInfos.parallelStream()
                        .filter(info -> StringUtils.isNotEmpty(info.getUserId()))
                        .map(info -> Long.parseLong(info.getUserId()))
                        .collect(Collectors.toList());

                Map<Long,User> usersMap = fosUtils.getUserInfosMap(userIds, true);

                for (WebinarUserRegistrationInfo registrationInfo : registrationInfos) {

                    if(StringUtils.isNotEmpty(registrationInfo.getUserId())) {
                        Long userId = Long.parseLong(registrationInfo.getUserId());
                        User user = usersMap.get(userId);
                        /*if (canIgnoreNotificationForUser(registrationInfo, user))
                            continue;*/

                        try {
                            if (!registrationInfo.getNotifcations().contains(WebinarNotifcation.WEBINAR_COMPLETED)
                                    && !processed.contains(registrationInfo.getUserId())) {
                                logger.info("WEBINAR_COMPLETED_USER_ID " + registrationInfo.getId() + " " + user.getUserId());
                                processed.add(registrationInfo.getUserId());
                                sendEmailandSMSAfterWebinarCompleted(webinar, registrationInfo);
                                registrationInfo.addNotifcations(WebinarNotifcation.WEBINAR_COMPLETED);
                                webinarUserRegistrationInfoDAO.save(registrationInfo);
                            }
                        }catch (Exception e){
                            logger.error("Error sending webinar completed email to user : {}", registrationInfo.getUserId(), e);
                        }
                    }

                }
                start += size;
                registrationInfos = webinarUserRegistrationInfoDAO.getWebinarUserRegistrationInfos(webinar.getId(), start, size);
            }
        }
    }

    private void sendEmailandSMSAfterWebinarCompleted(Webinar webinar, WebinarUserRegistrationInfo user) throws VException {
        try {
            //sending email
            CommunicationType emailType = CommunicationType.SEND_WEBINAR_EMAIL_AFTER_MASTER_CLASS_COMPLETED;
            HashMap<String, Object> bodyScopes = new HashMap<>();
            bodyScopes.put("Name", StringUtils.defaultIfEmpty(user.getFirstName()) + " " + StringUtils.defaultIfEmpty(user.getLastName()));
            if (webinar.getTeacherInfo() != null) {
                bodyScopes.put("TeacherName", StringUtils.defaultIfEmpty(webinar.getTeacherInfo().get("name").toString()));
            }
            if (webinar.getCourseInfo() != null) {
                Map<String, Object> courseInfo = webinar.getCourseInfo();
                Object subTitle = courseInfo.get("subTitle");
                bodyScopes.put("CourseName", subTitle == null ? "" : subTitle);
                Object lightningDeal = courseInfo.get("lightningDeal");
                if (lightningDeal instanceof Map) {
                    Map lightningDealMap = (Map) lightningDeal;
                    bodyScopes.put("CutPrice", lightningDealMap.get("cutPrice"));
                    bodyScopes.put("LightningPrice", lightningDealMap.get("offerPrice"));
                }
            }
            bodyScopes.put("Link", user.getRegisterJoinUrlShort());
            bodyScopes.put("registerJoinUrl", user.getRegisterJoinUrl());
            HashMap<String, Object> subjectScopes = new HashMap<>();
            ArrayList<InternetAddress> toAddress = new ArrayList<>();

            UserBasicInfo userInfo = fosUtils.getUserBasicInfo(user.getUserId(), true);
            if (StringUtils.isNotEmpty(userInfo.getEmail())) {
                toAddress.add(new InternetAddress(userInfo.getEmail()));
                EmailRequest request = new EmailRequest(toAddress, subjectScopes, bodyScopes, emailType, Role.STUDENT);
                request.setIncludeHeaderFooter(Boolean.FALSE);
                //sending sms
                communicationManager.sendEmail(request);
            }

            if (StringUtils.isNotEmpty(userInfo.getContactNumber())) {
                TextSMSRequest textSMSRequest;
                textSMSRequest = new TextSMSRequest(userInfo.getContactNumber(), userInfo.getPhoneCode(),
                        bodyScopes, CommunicationType.SEND_WEBINAR_EMAIL_AFTER_MASTER_CLASS_COMPLETED, Role.STUDENT);
                smsManager.sendSMS(textSMSRequest);
            }
        } catch (AddressException e) {
            logger.error(e);
            throw new VException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    public List<WebinarResponse> findWebinarsForHomeFeed(FindWebinarsReq findWebinarsReq) throws VException {
        logger.info("FindWebinars Request : {}", findWebinarsReq);
        List<Webinar> webinars = webinarDAO.findWebinarsForHomeFeed(findWebinarsReq);
        List<WebinarResponse> webinarResponses = new ArrayList<>();

        Long currentMillis = System.currentTimeMillis();

//        Set<String> parentWebinarsList = new HashSet<>();
//        for (Webinar webinar : webinars) {
//            logger.info("Get Title : " + webinar.getTitle());
//            logger.info("Get Parent Id" + webinar.getParentWebinarId());
//            if (StringUtils.isNotEmpty(webinar.getParentWebinarId())) {
////                parentWebinarsList.add(webinar.getId());
//                parentWebinarsList.add("5df7689f2be0866cf5946502");
//                parentWebinarsList.add("5df769a84615012e76a27f89");
//                parentWebinarsList.add(webinar.getParentWebinarId());
//            }
//        }

//        logger.info("ParentWebinarList : " + parentWebinarsList);
//        Set<String> ignoreWebinars = webinarUserRegistrationInfoDAO.getWebinarUserInfoByWebinars(findWebinarsReq.getCallingUserId().toString(), parentWebinarsList);
//        logger.info("IgnoreWebinar count: " + ignoreWebinars.size());

        List<Webinar> filteredWebinars = filterSimLives(webinars, findWebinarsReq.getCallingUserId().toString());

        if (filteredWebinars != null && !filteredWebinars.isEmpty())
            webinars = filteredWebinars;

        //Adding logic to fetch whether reminder has been set for user -> GA-2346
        List<String> webinarIds = new ArrayList<String>();
        webinarIds = webinars.stream().map(Webinar :: getId).collect(Collectors.toList());
        List<WebinarRegisterUserRes> webinarRegRes = new ArrayList<WebinarRegisterUserRes>();
        webinarRegRes = getWebinarUserRegistrationInfos(findWebinarsReq.getStudentEmail(), webinarIds);
        List<String> registeredWebinarIds = new ArrayList<String>();
        if(ArrayUtils.isNotEmpty(webinarRegRes)) {
        registeredWebinarIds = webinarRegRes.stream().map(WebinarRegisterUserRes::getWebinarId).collect(Collectors.toList());
        }

        for (Webinar webinar : webinars) {
            WebinarResponse webinarResponse = new WebinarResponse();
            webinarResponse.setId(webinar.getId());
            webinarResponse.setTitle(webinar.getTitle());
            webinarResponse.setStartTime(webinar.getStartTime());
            webinarResponse.setEndTime(webinar.getEndTime());
            webinarResponse.setTeacherInfo(webinar.getTeacherInfo());
            webinarResponse.setCourseInfo(webinar.getCourseInfo());
            webinarResponse.setWebinarCode(webinar.getWebinarCode());
            webinarResponse.setCreationTime(webinar.getCreationTime());
            webinarResponse.setGrades(webinar.getGrades());
            webinarResponse.setWebinarInfo(webinar.getWebinarInfo());
            webinarResponse.setSessionId(webinar.getSessionId());

          //Adding logic to fetch whether reminder has been set for user -> GA-2346
            if(ArrayUtils.isNotEmpty(registeredWebinarIds) && registeredWebinarIds.contains(webinar.getId())) {
            	webinarResponse.setIsReminderSet(true);
            }

            if (webinar.getSubjects() != null && webinar.getSubjects().size() != 0) {
                webinarResponse.setSubject(Collections.min(webinar.getSubjects()));
                webinarResponse.setSubjects(webinar.getSubjects());
            }
            String popupDelay = redisDAO.get("POP_UP_DELAY");
            webinarResponse.setPopUpDeplay(Long.parseLong(popupDelay));

            String fosEnvironment = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
            if (StringUtils.isEmpty(fosEnvironment)) {
                fosEnvironment = "https://www.vedantu.com";
            }
            String masterClassUrl = fosEnvironment + "/masterclass/" + webinar.getWebinarCode() + "?utm_source=seo&utm_medium=seo_hp&utm_campaign=seo_hp-masterclass&utm_content=homepage_tiles&utm_term=seo_hp_list_masterclass";
            webinarResponse.setMasterClassUrl(masterClassUrl);

            if (StringUtils.isNotEmpty(webinar.getSessionId()) && webinar.getStartTime() != null && webinar.getStartTime() < currentMillis) {
                String sessionId = webinar.getSessionId();
                try {
                    if (httpSessionUtils.getCallingUserId() != null) {
                        WaveWebinarResponse waveWebinarResponse = getWebinarPerSessionFromWave(sessionId, httpSessionUtils.getCallingUserId().toString());
                        if (waveWebinarResponse != null) {
                            logger.info("Response from wave={}", waveWebinarResponse);
                            addDataFromWave(waveWebinarResponse, webinarResponse);
                        }
                    }
                } catch (VException vex) {
                    logger.info("Response from WaveWebinarInfo error={}", sessionId);
                }
                String[] initialHideWebinars = redisDAO.get("HIDE_WEBINARS").split(",");
                List<String> hideWebinars = Arrays.asList(initialHideWebinars);
                if(!hideWebinars.contains(webinar.getId())) {
                    webinarResponses.add(webinarResponse);
                }
//                if (!webinar.getId().equals("5e4bf8c21571f202107d4168") && !webinar.getId().equals("5e4bfa2a1571f202107d429a") && !webinar.getId().equals("5e4bfa9b1c8b7b6e84a00d3f")
//                        && !webinar.getId().equals("5e4bfb051c8b7b6e84a00d98") && !webinar.getId().equals("5e4bfb521c8b7b6e84a00dd0") && !webinar.getId().equals("5e4bfbce42713d714f73ea6f")
//                        && !webinar.getId().equals("5e4bfc5242713d714f73ead1") && !webinar.getId().equals("5e4bfcfc0ed65806a979cf7b") && !webinar.getId().equals("5e4bfdadd3f2430fd18b8fa6")
//                        && !webinar.getId().equals("5e4bfe1d1571f202107d4566") && !webinar.getId().equals("5e4bfe7e1c8b7b6e84a00fd1") && !webinar.getId().equals("5e4bff1942713d714f73ecc2")
//                        && !webinar.getId().equals("5e4bff921571f202107d4695") && !webinar.getId().equals("5e4c0015d3f2430fd18b9810")) {
//                    webinarResponses.add(webinarResponse);
//                }
            } else {
                String[] initialHideWebinars = redisDAO.get("HIDE_WEBINARS").split(",");
                List<String> hideWebinars = Arrays.asList(initialHideWebinars);
                if(!hideWebinars.contains(webinar.getId())) {
                    webinarResponses.add(webinarResponse);
                }
//                if (!webinar.getId().equals("5e4bf8c21571f202107d4168") && !webinar.getId().equals("5e4bfa2a1571f202107d429a") && !webinar.getId().equals("5e4bfa9b1c8b7b6e84a00d3f")
//                        && !webinar.getId().equals("5e4bfb051c8b7b6e84a00d98") && !webinar.getId().equals("5e4bfb521c8b7b6e84a00dd0") && !webinar.getId().equals("5e4bfbce42713d714f73ea6f")
//                        && !webinar.getId().equals("5e4bfc5242713d714f73ead1") && !webinar.getId().equals("5e4bfcfc0ed65806a979cf7b") && !webinar.getId().equals("5e4bfdadd3f2430fd18b8fa6")
//                        && !webinar.getId().equals("5e4bfe1d1571f202107d4566") && !webinar.getId().equals("5e4bfe7e1c8b7b6e84a00fd1") && !webinar.getId().equals("5e4bff1942713d714f73ecc2")
//                        && !webinar.getId().equals("5e4bff921571f202107d4695") && !webinar.getId().equals("5e4c0015d3f2430fd18b9810")) {
//                    webinarResponses.add(webinarResponse);
//                }
            }
        }
//            webinarResponses.add(webinarResponse);
        logger.info("Webinar Responses : {}", webinarResponses);

     // Commenting as per discussion with Nitin, this A/B Testing was never evolved later -> GA-2346
//        if (findWebinarsReq.getAppVersionCode() != null && findWebinarsReq.getAppVersionCode().compareTo("1.6.2") >= 0
//                && hasNeverAttendedWebinar(findWebinarsReq.getCallingUserId().toString()) && webinarResponses.size() > 1)
//            return webinarResponses.subList(0,1);


        return webinarResponses;
    }

    public void salesDemoSendToLeadSquaredThroughSqs(String payload) throws VException {
        if (StringUtils.isNotEmpty(payload)) {
            logger.info("calling sqs with sns payload : " + payload);

            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> data = gson.fromJson(payload, type);
            awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.SALES_DEMO_LEADSQUARED, new Gson().toJson(data), SQSMessageType.SALES_DEMO_LEADSQUARED.name());
        }

    }

    public void webinarHomeDemoDetailsSendToLeadSquaredThroughSqs(String payload) throws VException {
        if (StringUtils.isNotEmpty(payload)) {
            logger.info("calling sqs with sns payload : " + payload);

            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> data = gson.fromJson(payload, type);

            if(StringUtils.isNotEmpty(data.get("userId"))){
                logger.info("getting user Details by using userid : " + data.get("userId"));
                UserBasicInfo user = fosUtils.getUserBasicInfo(data.get("userId"),true);
                if (user == null) {
                    throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found");
                }
                logger.info("user Details : " + user.toString());
                if (StringUtils.isNotEmpty(user.getEmail())) {
                    data.put("emailAddress", user.getEmail());
                }
                if (StringUtils.isNotEmpty(user.getContactNumber())) {
                    data.put("phone", user.getContactNumber());
                }
                awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.WEBINAR_HOMEDEMO_LEADSQUARED, new Gson().toJson(data), SQSMessageType.WEBINAR_HOMEDEMO_LEADSQUARED.name());
            }
        }

    }

    public PlatformBasicResponse updateOtmWebinarAttended(GetWebinarRegistrationInfoReq req) throws VException {
        req.verify();
        PlatformBasicResponse response = new PlatformBasicResponse();
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(req.getUserId(), req.getWebinarId());
        Webinar webinarListing = webinarDAO.getById(req.getWebinarId());
        if (webinarUserRegistrationInfo == null) {
            WebinarRegistrationReq webinarRegistrationReq = new WebinarRegistrationReq();
            webinarRegistrationReq.setWebinarId(req.getWebinarId());
            if (httpSessionUtils.getCurrentSessionData() != null && httpSessionUtils.getCurrentSessionData().getGrade() != null) {
                webinarRegistrationReq.setGrade(httpSessionUtils.getCurrentSessionData().getGrade());
            }

            if (!StringUtils.isEmpty(req.getUserId())) {
                webinarRegistrationReq.setUserId(Long.parseLong(req.getUserId()));
            }
            webinarRegister(webinarRegistrationReq);

            // sending event to clevertap
            webinarUserRegistrationInfo = webinarUserRegistrationInfoDAO.getWebinarUserInfoByUserId(req.getUserId(), req.getWebinarId());
            if (webinarUserRegistrationInfo == null) {
                logger.info("user not registered: " + req);
                response.setSuccess(true);
                response.setResponse("user not registered: " + req.getUserId() + ", webinarId: " + req.getWebinarId());
                return response;
            }

            logger.info("GOT_WebinarUserRegistrationInfo: " + webinarUserRegistrationInfo.getUserId());
            if (WebinarType.QUIZ.equals(webinarListing.getType())) {
                awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "VBRAINER_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
            } else {
                awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "LIVECLASS_REGISTRATION_CONFIRMED", new Gson().toJson(webinarUserRegistrationInfo));
            }
        }
//        Webinar webinarListing = webinarListingDAO.getById(webinarUserRegistrationInfo.getWebinarId());
        if (webinarListing == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No webinar found for  with id:" + req.getWebinarId() + ", phone:" + req.getUserId());
        }

        if (!WebinarToolType.VEDANTU_WAVE.equals(webinarListing.getToolType())
                && !WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(webinarListing.getToolType())
                && !WebinarToolType.YOUTUBE.equals(webinarListing.getToolType())) {
            response.setSuccess(false);
            return response;
        }
        Long currentTime = System.currentTimeMillis();
        if ((currentTime + 30 * DateTimeUtils.MILLIS_PER_MINUTE) < webinarListing.getStartTime()) {
            return response;
        }

        if (webinarListing.getEndTime() != null && currentTime <= webinarListing.getEndTime() + 15 * DateTimeUtils.MILLIS_PER_MINUTE) {
            webinarUserRegistrationInfo.setWebinarAttended(true);
        } else {
            webinarUserRegistrationInfo.setReplayWatched(true);
        }
        webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);

        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.WEBINAR_ATTENDED, gson.toJson(webinarUserRegistrationInfo), webinarUserRegistrationInfo.getUserId() + "_WEBINAR_REGISTERED");

        return response;
    }

    private boolean hasNeverAttendedWebinar(String userId) {

        logger.info("Getting Webinars attended for user : {}", userId);
        if (!userId.endsWith("1") && !userId.endsWith("3"))
            return false;

        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId))
                .addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINAR_ATTENDED).is(true));
        Long count = webinarUserRegistrationInfoDAO.queryCount(query, WebinarUserRegistrationInfo.class);
        return (count == 0);
    }

    private List<Webinar> filterSimLives(List<Webinar> webinars, String userId) {

        if (StringUtils.isEmpty(userId) || webinars.size() == 0)
            return null;

        Set<String> parentWebIds = new HashSet<>();
        for (Webinar w : webinars) {
            if (w.getIsSimLive() != null && w.getIsSimLive())
                parentWebIds.add(w.getParentWebinarId());
        }

        logger.info("parentWeb ids : {}", parentWebIds);
        if (!parentWebIds.isEmpty()) {

            List<Webinar> simLiveWebinars = webinarDAO.getSimLiveParentIds(parentWebIds);
            logger.info("Sim Lives : {}", simLiveWebinars);

            for (Webinar w : simLiveWebinars)
                parentWebIds.add(w.getId());

            List<String> attendedIds = webinarUserRegistrationInfoDAO.getAttendedIds(userId, parentWebIds);
            logger.info("Attended webinars : {}", attendedIds);

            if (attendedIds.isEmpty())
                return null;

            for (String attendedId : attendedIds) {
                for (Webinar w : simLiveWebinars) {
                    if (w.getId().equals(attendedId))
                        parentWebIds.remove(w.getParentWebinarId());
                }
                parentWebIds.remove(attendedId);
            }

            List<Webinar> filteredWebinars = new ArrayList<>();

            logger.info("parentWeb ids after removal: {}", parentWebIds);

            for (Webinar w : webinars) {
                if (w.getIsSimLive() != null && w.getIsSimLive())
                {
                    if (parentWebIds.contains(w.getId()) && parentWebIds.contains(w.getParentWebinarId()))
                        filteredWebinars.add(w);
                }
                else
                    filteredWebinars.add(w);
            }

            logger.info("Filtered webinars : {}", filteredWebinars);
            return filteredWebinars;
        }
        return null;
    }

    public void startUpcomingWebinarsAsync() {

        awsSQSManager.sendToSQS(SQSQueue.START_UPCOMING_WEBINAR, SQSMessageType.START_UPCOMING_WEBINAR, "{}", SQSMessageType.START_UPCOMING_WEBINAR.name());

    }

    public void startUpcomingWebinars() throws VException {

        logger.info("Launching upcoming webinars");

        Long currentTimeMillis = System.currentTimeMillis();

        Long beforeStartTime =  currentTimeMillis + (DateTimeUtils.MILLIS_PER_MINUTE * 15);

        Set<Webinar> webinars = new HashSet<>();

        int start = 0;
        int size = 50;

        //Getting webinars for the next 30 minutes in batches of 50
        List<Webinar> webinarsBatch;
        do{
            FindWebinarsReq req = new FindWebinarsReq();
            req.setAfterStartTime(currentTimeMillis);
            req.setBeforeStartTime(beforeStartTime);
            req.setSimLive(Boolean.TRUE);
            req.setShowSimLiveWebinars(Boolean.TRUE);
            req.setWebinarStatus(WebinarStatus.ACTIVE);
            req.setStart(start);
            req.setSize(size);
            webinarsBatch = webinarDAO.findWebinars(req);

            if(CollectionUtils.isNotEmpty(webinarsBatch))
                webinars.addAll(webinarsBatch);

            start += size;

        }while(webinars != null && webinars.size() == size);

        if(CollectionUtils.isNotEmpty(webinars)) {

            webinars = filterWebinarsNotUnderProcess(webinars);
            webinars = webinars.parallelStream().filter(v -> StringUtils.isNotEmpty(v.getSessionId())).collect(Collectors.toSet());

            for (Webinar webinar : webinars) {
                //Submit the request for webinar launch
                markRequestForWebinarSPotInstance(webinar);
            }
        }

    }

    private Set<Webinar> filterWebinarsNotUnderProcess(Set<Webinar> webinars) {
        Set<String> webinarIds = webinars.parallelStream().map(v -> v.getId()).collect(Collectors.toSet());

        //Checking if the webinar is already in queue
        WebinarSpotInstanceFilterReq filterReq = new WebinarSpotInstanceFilterReq();
        filterReq.setWebinarIds(webinarIds);
        List<WebinarSpotInstanceMetadata> webinarSpotInstanceMetadataList = webinarSpotInstanceMetadataDao.getSpotInstanceMetadata(filterReq);
        Set<String> alreadyLaunchedWebinarIds = webinarSpotInstanceMetadataList.parallelStream().map(v -> v.getWebinarId()).collect(Collectors.toSet());

        //Filter pending webinars
        webinars = webinars.parallelStream().filter(v -> !alreadyLaunchedWebinarIds.contains(v.getId())).collect(Collectors.toSet());
        return webinars;
    }

    private void markRequestForWebinarSPotInstance(Webinar webinar) throws VException {

        try{

            WebinarSpotInstanceMetadata webinarSpotInstanceMetadata = new WebinarSpotInstanceMetadata();
            webinarSpotInstanceMetadata.setWebinarId(webinar.getId());
            webinarSpotInstanceMetadata.setSessionId(webinar.getSessionId());
            webinarSpotInstanceMetadata.setWebinarStartTime(webinar.getStartTime());
            webinarSpotInstanceMetadata.setWebinarEndTime(webinar.getEndTime());

            setPresenterEmailId(webinar.getSessionId(), webinarSpotInstanceMetadata);

            webinarSpotInstanceMetadata.setInstanceStatus(WebinarSpotInstanceStatus.REQUESTED);

            webinarSpotInstanceMetadataDao.upsert(webinarSpotInstanceMetadata);

            String request = gson.toJson(webinarSpotInstanceMetadata);
            awsSQSManager.sendToSQS(SQSQueue.START_UPCOMING_WEBINAR, SQSMessageType.LAUNCH_WEBINAR_INSTANCE, request, SQSMessageType.LAUNCH_WEBINAR_INSTANCE.name());

        }catch (Exception e){
            logger.error("Error launching webinar instance : {}",e.getMessage(), e);
        }

    }

    public void setPresenterEmailId(String sessionId, WebinarSpotInstanceMetadata webinarSpotInstanceMetadata) throws VException {
        String createSessionUrl = SCHEDULING_ENDPOINT + "/onetofew/session/" + sessionId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("-----------" + jsonString);
        OTFSessionInfo otfSessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);

        String userId = otfSessionInfo.getPresenter();

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);

        webinarSpotInstanceMetadata.setTeacherUserId(userId);
        webinarSpotInstanceMetadata.setTeacherEmail(userBasicInfo.getEmail());
    }

    public void launchWebinarInstance(WebinarSpotInstanceMetadata instanceMetadata){

        if(WebinarSpotInstanceStatus.INSTANCE_LAUNCH_FAILED.equals(instanceMetadata.getInstanceStatus())){
            if(instanceMetadata.getAttemptCount() >= WEBINAR_SPOT_INSTANCE_MAX_RETRY) {
                logger.error("Error launching webinar instance even after 5 retries : {}", instanceMetadata.getWebinarId());
                return;
            }
        }

        if(WebinarSpotInstanceStatus.REQUESTED.equals(instanceMetadata.getInstanceStatus())
        || WebinarSpotInstanceStatus.INSTANCE_LAUNCH_FAILED.equals(instanceMetadata.getInstanceStatus())){

            boolean isLaunched = awsEC2Manager.createWebinarLauncherEc2(instanceMetadata);

            //Retry on fail
            if(!isLaunched) {

                try{
                    Thread.sleep(2000);
                }catch (Exception e){

                }

                String request = gson.toJson(instanceMetadata);
                logger.info("Retrying : {}", request);
                awsSQSManager.sendToSQS(SQSQueue.START_UPCOMING_WEBINAR, SQSMessageType.LAUNCH_WEBINAR_INSTANCE, request, SQSMessageType.LAUNCH_WEBINAR_INSTANCE.name());
            }
        }

    }

    public void launchWebinarsById(String webinarId) throws VException {

        if(StringUtils.isEmpty(webinarId))
            throw new BadRequestException(ErrorCode.INCORRECT_ID, "Please enter the webinar id");

        logger.info("launchWebinarsById : {}", webinarId);

        Webinar webinar = webinarDAO.getById(webinarId);

        if(webinar == null)
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Webinar not found with the given id");

        if(webinar.getWebinarStatus() != WebinarStatus.ACTIVE)
            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Webinar is not active");

        if(webinar.getIsSimLive() == null || !webinar.getIsSimLive())
            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Webinar is not simlive");

        if(StringUtils.isEmpty(webinar.getSessionId()))
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No sesion found for this webinar");

        Long currentTimeMillis = System.currentTimeMillis();

        Long beforeStartTime =  currentTimeMillis + (DateTimeUtils.MILLIS_PER_HOUR * 1);

        if(webinar.getStartTime() > beforeStartTime)
            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Webinar can be started only before 30 minutes of the start time");

        if(webinar.getEndTime() < currentTimeMillis)
            throw new BadRequestException(ErrorCode.ALREADY_ENDED, "Webinar is already ended");

        Set<Webinar> webinars = new HashSet<>();
        webinars.add(webinar);

        webinars = filterWebinarsNotUnderProcess(webinars);

        if(CollectionUtils.isEmpty(webinars))
            throw new BadRequestException(ErrorCode.ALREADY_LAUNCHED, "Webinar is already launched");

        markRequestForWebinarSPotInstance(webinar);

    }

    public void updateWebinarStatus(String webinarId, WebinarSpotInstanceStatus status, String message){

        if(WebinarSpotInstanceStatus.WEBINAR_TERMINATED.equals(status))
            logger.error("Error during webinar stream webinarId :, Error: {}", webinarId, message);

        logger.info("Webinar status update: webinarId : {} status : ", webinarId, status);

        WebinarSpotInstanceMetadata instanceMetadata = webinarSpotInstanceMetadataDao.getSpotInstanceMetadataByWebinarId(webinarId);

        if(instanceMetadata!=null) {
            instanceMetadata.setInstanceStatus(status);

            if(StringUtils.isNotEmpty(message))
                instanceMetadata.setTetminateMessage(message);

            webinarSpotInstanceMetadataDao.upsert(instanceMetadata);
        }

    }

    public List<LaunchedWebinar> getTodaysWebinars() {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 0, 0, 0);

        Long dayStartTime = calendar.getTime().getTime();

        WebinarSpotInstanceFilterReq filterReq = new WebinarSpotInstanceFilterReq();
        filterReq.setWebinarFromStartTime(dayStartTime);
        filterReq.setWebinarToStartTime(System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_HOUR);
        List<WebinarSpotInstanceMetadata> webinarSpotInstanceMetadataList = webinarSpotInstanceMetadataDao.getSpotInstanceMetadata(filterReq);

        Set<String> webinarId =  webinarSpotInstanceMetadataList.parallelStream().map(v -> v.getWebinarId()).collect(Collectors.toSet());

        List<Webinar> webinars = webinarDAO.getWebinarsForIds(webinarId);

        Type _type = new TypeToken<List<LaunchedWebinar>>() {
        }.getType();
        List<LaunchedWebinar> webinarMetadataList = gson.fromJson(gson.toJson(webinars), _type);

        Map<String, List<WebinarSpotInstanceMetadata>> webinarSpotInstanceMetadataListMap = webinarSpotInstanceMetadataList.stream().collect(Collectors.groupingBy(WebinarSpotInstanceMetadata::getWebinarId));

        for(LaunchedWebinar launchedWebinar : webinarMetadataList){

            WebinarSpotInstanceMetadata spotInstanceMetadata = webinarSpotInstanceMetadataListMap.get(launchedWebinar.getId()).get(0);
            launchedWebinar.setInstanceStatus(spotInstanceMetadata.getInstanceStatus());
        }

        return webinarMetadataList;

    }

    public WebinarLaunchStatusRes getWebinarStatus(String webinarId) throws NotFoundException {

        WebinarSpotInstanceMetadata instanceMetadata = webinarSpotInstanceMetadataDao.getSpotInstanceMetadataByWebinarId(webinarId);

        if(instanceMetadata == null)
            throw  new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Webinar launch status not found");

        String key = "WEBINAR_LAUNCH/screenshots/" + ENV.toLowerCase() + "/" + webinarId + "/";

        logger.info("Bucket : {}", WEBINAR_SPOT_INSTANCE_BUCKET);
        logger.info("key : {}", key);

        List<String> screenshorts = awsS3Manager.getBucketContents(WEBINAR_SPOT_INSTANCE_BUCKET, key);

        logger.info("screenshorts : {}", screenshorts);

        String imageUrl = null;
        if(CollectionUtils.isNotEmpty(screenshorts)) {
            Collections.sort(screenshorts, new Comparator<String>() {
                public int compare(String o1, String o2) {

                    String fileName1 = o1.substring(o1.lastIndexOf("/") + 1,o1.lastIndexOf("."));
                    String fileName2 = o2.substring(o2.lastIndexOf("/") + 1,o2.lastIndexOf("."));


                    return Long.compare(Long.parseLong(fileName2), Long.parseLong(fileName1));
                }
            });
            imageUrl = screenshorts.get(0);
        }

        WebinarLaunchStatusRes launchStatusRes = new WebinarLaunchStatusRes();
        launchStatusRes.setInstanceMetadata(instanceMetadata);
        launchStatusRes.setLatestScreenshotUrl(imageUrl);

        return launchStatusRes;

    }

    public WebinarMonitorActionButtonResponse getActionButton(String webinarId) throws VException {

        if(StringUtils.isEmpty(webinarId))
            throw new BadRequestException(ErrorCode.INCORRECT_ID,"Webinar id cannot be empty");

        WebinarSpotInstanceMetadata instanceMetadata = webinarSpotInstanceMetadataDao.getSpotInstanceMetadataByWebinarId(webinarId);

        if(instanceMetadata == null)
            throw  new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Webinar launch status not found");

        String sessionId = instanceMetadata.getSessionId();

        String createSessionUrl = SCHEDULING_ENDPOINT + "/onetofew/session/" + sessionId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("-----------" + jsonString);
        OTFSessionInfo otfSessionInfo = gson.fromJson(jsonString, OTFSessionInfo.class);

        if(otfSessionInfo==null)
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,"Session not found with the given id: " + sessionId);

        WebinarMonitorActionButtonResponse webinarMonitorActionButtonResponse = new WebinarMonitorActionButtonResponse();

        if(otfSessionInfo.getEndedAt() == null || OTMSessionInProgressState.ACTIVE.equals(otfSessionInfo.getProgressState())) {
            if(WebinarSpotInstanceStatus.WEBINAR_LAUNCHED.equals(instanceMetadata.getInstanceStatus())) {
                webinarMonitorActionButtonResponse.setCanRefresh(Boolean.TRUE);
                webinarMonitorActionButtonResponse.setCanEnd(Boolean.TRUE);
            }
        }

        return webinarMonitorActionButtonResponse;
    }
}
