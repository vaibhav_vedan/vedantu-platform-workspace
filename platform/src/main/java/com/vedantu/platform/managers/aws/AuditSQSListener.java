package com.vedantu.platform.managers.aws;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.platform.dao.AuditHistoryDAO;

import com.vedantu.platform.pojo.audit.AuditHistoryII;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

import com.vedantu.util.enums.SQSMessageType;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author Darshit
 */
@Component
public class AuditSQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AuditHistoryDAO auditHistoryDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;
    @Autowired
    private DozerBeanMapper mapper;

    private Logger logger = logFactory.getLogger(AuditSQSListener.class);

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(sqsMessageType, textMessage.getText());
            message.acknowledge();

        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }


    private void handleMessage(SQSMessageType sqsMessageType, String text) throws Exception {
        switch (sqsMessageType) {
            case DO_AUDIT:
                AuditHistoryII auditHistoryII = new Gson().fromJson(text, AuditHistoryII.class);
                LinkedTreeMap  treeMap = (LinkedTreeMap) auditHistoryII.getEntityObject();
             //   AbstractMongoStringIdEntity entity =mapper.map(auditHistory.getEntityObject() , AbstractMongoStringIdEntity.class);
                // (AbstractMongoStringIdEntity) auditHistory.getEntityObject();
                auditHistoryII.setAuditEntityId( treeMap.get("id").toString());
                auditHistoryDAO.create(auditHistoryII);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);

        }
        logger.info("Message handled");
        }

}
