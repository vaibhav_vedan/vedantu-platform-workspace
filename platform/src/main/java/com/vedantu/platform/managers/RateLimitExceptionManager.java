package com.vedantu.platform.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.*;
import com.vedantu.util.redis.RateLimitingRedisDao;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class RateLimitExceptionManager {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RateLimitExceptionManager.class);

    @Autowired
    private RateLimitingRedisDao abstractRedisDAO;

    public PlatformBasicResponse deleteUserRecord(List<String> keys) throws BadRequestException, InternalServerErrorException {
        logger.info("redis keys for delete : " + keys);
        if(ArrayUtils.isEmpty(keys)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Enter at least one key");
        }
        for (String key : keys){
            abstractRedisDAO.del(key);
        }
        return new PlatformBasicResponse();
    }

    public List<String> getRateLimitRecords(String keyPattern) throws BadRequestException, InternalServerErrorException {
        List<String> rlKeysList = new ArrayList<>();

        logger.info("keyPattern : " + keyPattern);
        if(keyPattern == null || StringUtils.isEmpty(keyPattern)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"ip or userId not found");
        }
        String newConfig = ConfigUtils.INSTANCE.getStringValue("rl.config");
        java.lang.reflect.Type type = new TypeToken<HashMap<String, HashMap<String, String>>>() {}.getType();
        HashMap<String, HashMap<String, String>> rlConfig = new Gson().fromJson(newConfig, type);

        keyPattern = keyPattern.trim();

        String rlKey = "RL:" + keyPattern;
        if(abstractRedisDAO.get(rlKey) != null){
            rlKeysList.add(rlKey);
        }

        String rlBlKey = "BL:RL:" + keyPattern;
        if(abstractRedisDAO.get(rlBlKey) != null){
            rlKeysList.add(rlBlKey);
        }

        for(String endPoint : rlConfig.keySet()){
            String rlKeyWithEndPoint = rlKey + ":" + endPoint;
            if(abstractRedisDAO.get(rlKeyWithEndPoint) != null){
                rlKeysList.add(rlKeyWithEndPoint);
            }
            String rlBlKeyWithEndPoint = rlBlKey + ":" + endPoint;
            if(abstractRedisDAO.get(rlBlKeyWithEndPoint) != null){
                rlKeysList.add(rlBlKeyWithEndPoint);
            }
        }

        return rlKeysList;
    }
}
