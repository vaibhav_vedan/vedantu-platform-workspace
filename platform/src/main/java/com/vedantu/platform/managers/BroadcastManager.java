package com.vedantu.platform.managers;

import com.vedantu.User.UserBasicInfo;
import inti.ws.spring.exception.client.BadRequestException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.exception.BroadcastAcceptedException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.askadoubt.DoubtDao;
import com.vedantu.platform.managers.askadoubt.AskADoubtManager;
import com.vedantu.platform.pojo.askadoubt.Doubt;
import com.vedantu.platform.pojo.askadoubt.DoubtInfo;
import com.vedantu.platform.dao.broadcast.BroadcastDao;
import com.vedantu.platform.dao.broadcast.BroadcastLevelDao;
import com.vedantu.platform.enums.broadcast.BroadcastStatus;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.pojo.broadcast.Broadcast;
import com.vedantu.platform.pojo.broadcast.BroadcastInfo;
import com.vedantu.platform.pojo.broadcast.BroadcastLevel;
import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import java.util.HashSet;
import java.util.Set;
import org.dozer.DozerBeanMapper;

@Service
public class BroadcastManager {

	@Autowired
	LogFactory logFactory;

	Logger logger = logFactory.getLogger(RequestCallbackDao.class);

	@Autowired
	BroadcastDao broadcastDao;

	@Autowired
	AskADoubtManager askADoubtManager;

	@Autowired
	RequestCallbackManager requestCallbackManager;

	@Autowired
	BroadcastLevelDao broadcastLevelDao;
	
	@Autowired
	DoubtDao doubtDao;

        @Autowired
        FosUtils fosUtils;
        
        @Autowired
        private DozerBeanMapper mapper;

	public List<BroadcastInfo> getBroadcast(String id, Long userId,
			BroadcastType broadcastType, BroadcastStatus broadcastStatus,
			Integer level, String referenceId, Long fromTime, Long toTime,
			Integer start, Integer size) {
		Map<String, BroadcastInfo> broadcastInfoMap = new HashMap<>();
		List<Doubt> doubts = new ArrayList<>();
		Set<Long> userIds = new HashSet<>();
		try {
			List<Broadcast> broadcasts = broadcastDao.get(id, userId,
					broadcastType, level, fromTime, toTime, broadcastStatus,
					referenceId, start, size);

			for (Broadcast broadcast : broadcasts) {

				BroadcastInfo broadcastInfo = mapper.map(broadcast,
						BroadcastInfo.class);

				switch (broadcast.getBroadcastType()) {
				case ASKADOUBT:
					doubts.add(doubtDao.getById(broadcast.getReferenceId()));
					broadcastInfoMap.put(broadcast.getId(), broadcastInfo);
					userIds.add(broadcast.getToUserId());
					break;
				default:
					break;
				}
			}

                        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);

			Map<String ,DoubtInfo> doubtInfoMap = askADoubtManager.getDoubtInfoMap(askADoubtManager.getDoubtInfo(doubts));
			
			List<BroadcastInfo> broadcastInfos = new ArrayList<>();
			for (Broadcast broadcast : broadcasts) {
				UserBasicInfo user = userMap.get(broadcast.getToUserId());
				BroadcastInfo broadcastInfo = broadcastInfoMap.get(broadcast
						.getId());
				broadcastInfo.setUser(user);
				broadcastInfos.add(broadcastInfo);
				
				switch (broadcast.getBroadcastType()) {
				case ASKADOUBT:
					DoubtInfo doubtInfo = doubtInfoMap.get(broadcast.getReferenceId());
					broadcastInfo.setEntity(doubtInfo);
					//broadcastInfoMap.put(broadcast.getId(), broadcastInfo);
					//userIds.add(broadcast.getToUserId());
					break;
				default:
					break;
				}
			}
			return broadcastInfos;
		} catch (Exception e) {
			logger.error("Error occured while fetching braodcasts : "
					+ e.getMessage());
			throw e;
		}
	}

	public Boolean acceptBroadcast(String id) throws Exception {
		try {
			Broadcast broadcast = getBroadcast(id);

			checkIfAccepted(broadcast);

			broadcast.setStatus(BroadcastStatus.ACCEPTED);
			broadcast.setLastUpdated(System.currentTimeMillis());
			broadcastDao.upsert(broadcast);

			switch (broadcast.getBroadcastType()) {
			case ASKADOUBT:
				askADoubtManager.acceptDoubt(broadcast);
//				TODO : removed because of circular dependecy. broadcastAsync.markDoubtAccepted(broadcast);
//				broadcastAsync.markDoubtAccepted(broadcast);
				break;
			default:
				throw new BadRequestException("Broadcast type not supported : "
						+ broadcast.getBroadcastType());
			}

		//	BroadcastInfo broadcastInfo = this.getBroadcast(id, null, null,
			//		null, null, null, null, null, null, null).get(0);
			return true;
		} catch (Exception e) {
			logger.error("Error occured while rejecting broadcast. Id : " + id
					+ "Error : " + e.getMessage());
			throw e;
		}
	}

	public BroadcastInfo rejectBroadcast(String id) throws Exception {
		try {
			Broadcast broadcast = getBroadcast(id);
			broadcast.setStatus(BroadcastStatus.REJECTED);
			broadcastDao.upsert(broadcast);
			BroadcastInfo broadcastInfo = this.getBroadcast(id, null, null,
					null, null, null, null, null, null, null).get(0);
			return broadcastInfo;
		} catch (Exception e) {
			logger.error("Error occured while rejecting broadcast. Id : " + id
					+ "Error : " + e.getMessage());
			throw e;
		}
	}

	public BroadcastInfo expireBroadcast(String id) throws Exception {
		try {
			Broadcast broadcast = getBroadcast(id);
			broadcast.setStatus(BroadcastStatus.REJECTED);
			broadcastDao.upsert(broadcast);
			BroadcastInfo broadcastInfo = this.getBroadcast(id, null, null,
					null, null, null, null, null, null, null).get(0);
			return broadcastInfo;
		} catch (Exception e) {
			logger.error("Error occured while rejecting broadcast. Id : " + id
					+ "Error : " + e.getMessage());
			throw e;
		}
	}

	private Broadcast getBroadcast(String id) throws BadRequestException {
		Broadcast broadcast = broadcastDao.getById(id);
		if (broadcast == null) {
			throw new BadRequestException("Broadcast doesn't exist for id: "
					+ id);
		}
		return broadcast;
	}

	private void checkIfAccepted(Broadcast broadcast) throws VException {
		List<Broadcast> acceptedBroadcastList = broadcastDao.get(null, null,
				broadcast.getBroadcastType(), null, null, null,
				BroadcastStatus.ACCEPTED, broadcast.getReferenceId(), null,
				null);

		if (!acceptedBroadcastList.isEmpty()) {
			throw new BroadcastAcceptedException(ErrorCode.BROADCAST_ALREADY_ACCEPTED,
					"Request Already Accepted");
		}

	}

	public BroadcastLevel addUpdateBroadcastLevel(String id,
			BroadcastType broadcastType, Integer level, Long expiryTime,
			List<Long> userIds) throws VException {
		try {
			BroadcastLevel broadcastLevel = new BroadcastLevel();
			
			if (id != null) {
				broadcastLevel = broadcastLevelDao.getById(id);
			}else{
				broadcastLevel.setId(id);
			}

			broadcastLevel.setBroadcastType(broadcastType);
			broadcastLevel.setLevel(level);
			broadcastLevel.setExpiryTime(expiryTime);
			broadcastLevel.setUserIds(userIds);

			broadcastLevelDao.upsert(broadcastLevel);

			return broadcastLevel;
		} catch (Exception e) {
			logger.error("Error occured while adding Broadcast level : "
					+ broadcastType + " ," + level);
			throw new VException(ErrorCode.SERVICE_ERROR,
					"Error occured while adding Broadcast level : "
							+ broadcastType + " ," + level);
		}
	}

	public List<BroadcastLevel> getBroadcastLevel(String id,
			BroadcastType broadcastType, Integer level, Integer start,
			Integer size) throws Exception {
		try {
			List<BroadcastLevel> broadcastLevels = new ArrayList<BroadcastLevel>();
			if (id != null) {
				BroadcastLevel broadcastLevel = broadcastLevelDao.getById(id);
				if (broadcastLevel == null) {
					throw new VException(ErrorCode.BAD_REQUEST_ERROR,
							"No level exists for this id");
				}
				broadcastLevels.add(broadcastLevel);
				return broadcastLevels;
			}
			broadcastLevels = broadcastLevelDao.get(id, broadcastType, level,
					start, size);
			return broadcastLevels;

		} catch (Exception e) {
			logger.error("unable to get broadcast levels :Broadcst type : "
					+ broadcastType + ", level: " + level + ",Id:  " + id);
			throw e;
		}
	}
}
