package com.vedantu.platform.managers;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.*;
import com.dropbox.core.v2.sharing.RequestedVisibility;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.dropbox.core.v2.sharing.SharedLinkSettings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.managers.aws.AwsS3Manager;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.pojo.CubeAutoCallRecorder;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.joda.time.Hours;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class DropboxManager {


    private static final DbxRequestConfig DBX_REQUEST_CONFIG = new DbxRequestConfig(ConfigUtils.INSTANCE.getStringValue("dropbox.vedantu.acr.app.client.identifier"));

    private static final String APP_SECRET = ConfigUtils.INSTANCE.getStringValue("dropbox.vedantu.acr.app.secret");

    private static final String ACCESS_TOKEN = ConfigUtils.INSTANCE.getStringValue("dropbox.vedantu.acr.app.access.token");

    private static final String DROPBOX_CALL_RECORD_PATH = ConfigUtils.INSTANCE.getStringValue("dropbox.vedantu.acr.app.record.path");

    private static final String DROPBOX_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.lead.squared.bucket"); // TODO

    private static final String VEDANTUDATA_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("VEDANTUDATA_ENDPOINT");

    private static final Gson GSON = new Gson();

    @Autowired
    private LeadSquaredManager leadsquaredManager;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DropboxManager.class);

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    private Boolean validateRequest(String signature, String requestBody) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(APP_SECRET.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secretKey);
            byte[] messageBytes = requestBody.getBytes();
            byte[] encodedBytes = sha256_HMAC.doFinal(messageBytes);
            logger.info("Encoded our bytes are : " + Hex.encodeHexString(encodedBytes));
            return signature.equals(Hex.encodeHexString(encodedBytes));
        } catch (NoSuchAlgorithmException e) {
            logger.error("Cannot validate request. Invalid algorithm : {0}", e);
            return false;
        } catch (InvalidKeyException e) {
            logger.error("Cannot validate request. Invalid key : {0}", e);
            return false;
        } catch (NullPointerException ignored) {
            return false;
        } catch (Exception e) {
            logger.error("Error in validating: ", e);
            return false;
        }
    }

    public void executeAsyncTask(String requestBody, String dropboxSignature) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("requestBody", requestBody);
        payload.put("dropboxSignature", dropboxSignature);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.DROPBOX_LS_ASYNC_TASK, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void performWebHookTask(String requestBody, String dropboxSignature) throws Exception {
//        !validateRequest(dropboxSignature, requestBody);
        Map<String, CubeAutoCallRecorder> recorderMap = new HashMap<>();
        DbxClientV2 client = new DbxClientV2(DBX_REQUEST_CONFIG, ACCESS_TOKEN);
        logger.info("Dbx Path: " + DROPBOX_CALL_RECORD_PATH);

        ListFolderResult result = client.files().listFolder(DROPBOX_CALL_RECORD_PATH);
        while (true) {
            for (Metadata metadata : result.getEntries()) {

                String pathLower = metadata.getPathLower();
                String key = DigestUtils.sha256Hex(pathLower.substring(0, pathLower.lastIndexOf(".")));
                String ext = pathLower.substring(pathLower.lastIndexOf("."));

                String tDir = System.getProperty("java.io.tmpdir");
                String pathname = tDir + File.separator + key + ext;

                logger.info("Dbx Path: " + pathLower);
                if (pathLower.contains(".json")) {
                    try {
                        DbxDownloader<FileMetadata> download = client.files().download(pathLower);
                        File file = new File(pathname);
                        try (OutputStream downloadFile = new FileOutputStream(pathname)) {
                            download.download(downloadFile);
                            if (file.exists()) {
                                uploadAndGetAwsUrl(file);
                                String json = new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);
                                CubeAutoCallRecorder value = GSON.fromJson(json, CubeAutoCallRecorder.class);
                                value.setJsonFileName(pathLower);
                                recorderMap.merge(key, value, CubeAutoCallRecorder::merge);
                                file.delete();
                            }
                        }
                    } catch (DbxException | FileNotFoundException e) {
                        // Already deleted file
                        logger.warn(e.getMessage());
                        recorderMap.remove(key);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        recorderMap.remove(key);
                    }
                } else {
                    try {
                        // To remove duplicate entries created in LeadSquared
//                        client.sharing().createSharedLinkWithSettings(metadata.getPathDisplay());

                        logger.info("Dbx Audio Download Path: " + pathname);
                        DbxDownloader<FileMetadata> download = client.files().download(pathLower);
                        File file = new File(pathname);
                        try (OutputStream downloadFile = new FileOutputStream(pathname)) {
                            download.download(downloadFile);
                            if (file.exists()) {
                                String url = uploadAndGetAwsUrl(file);
                                if (StringUtils.isNotEmpty(url)) {
                                    Date date = download.getResult().getServerModified();
                                    recorderMap.merge(key, new CubeAutoCallRecorder(url, pathLower, date), CubeAutoCallRecorder::merge);
                                } else {
                                    recorderMap.remove(key);
                                }
                                file.delete();
                            }
                        }
                    } catch (DbxException | FileNotFoundException e) {
                        // Already deleted file or already created shared link
                        logger.warn(e.getMessage());
                        recorderMap.remove(key);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        recorderMap.remove(key);
                    }
                }
            }

            if (!result.getHasMore()) {
                break;
            }

            result = client.files().listFolderContinue(result.getCursor());
        }

        logger.info("Dbx Cube ACR Json = " + GSON.toJson(recorderMap));
        // Push to lead squared
        pushToLeadSquared(recorderMap);
        List<DeleteArg> deleteList = getDeleteList(recorderMap);
        deleteInDropBox(deleteList, client);
    }

    private List<DeleteArg> getDeleteList(Map<String, CubeAutoCallRecorder> recorderMap) {
        List<DeleteArg> deleteArgs = new ArrayList<>();
        for (String key : recorderMap.keySet()) {
            CubeAutoCallRecorder recorder = recorderMap.get(key);
            if (StringUtils.isNotEmpty(recorder.getJsonFileName()) && StringUtils.isNotEmpty(recorder.getAudioFileName())
                    && (recorder.isPushedToLeadSquared() ||
                    (recorder.getServerModifiedDate() != null && recorder.getServerModifiedDate().toInstant()
                            .atZone(ZoneId.systemDefault()).toLocalDateTime()
                            .until(LocalDateTime.now(ZoneId.systemDefault()), ChronoUnit.HOURS) >= 12))
            ) {
                deleteArgs.add(new DeleteArg(recorder.getJsonFileName()));
                deleteArgs.add(new DeleteArg(recorder.getAudioFileName()));
            }
        }
        return deleteArgs;
    }

    private void deleteInDropBox(List<DeleteArg> deleteList, DbxClientV2 client) {
        try {
            if (ArrayUtils.isEmpty(deleteList)) return;
            client.files().deleteBatch(deleteList);
        } catch (DbxException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    private String uploadAndGetAwsUrl(File file) {
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        String key = "dropbox/" + date + "/" + file.getName();
        awsS3Manager.uploadFile(DROPBOX_BUCKET, key, file);
        return awsS3Manager.getPublicBucketDownloadUrl(DROPBOX_BUCKET, key);
    }

    private Map<String, CubeAutoCallRecorder> pushToLeadSquared(Map<String, CubeAutoCallRecorder> recorderMap) {
        for (String key : recorderMap.keySet()) {
            CubeAutoCallRecorder recorder = recorderMap.get(key);
            String phoneNo = recorder.getLeadPhoneNo();
            if (StringUtils.isNotEmpty(phoneNo) && StringUtils.isNotEmpty(recorder.getSharedLink())
                    && StringUtils.isNotEmpty(recorder.getJsonFileName()) && StringUtils.isNotEmpty(recorder.getAudioFileName())) {
                phoneNo = phoneNo.replaceAll("\\s", "");
                phoneNo = phoneNo.length() <= 10 ? phoneNo : phoneNo.substring(phoneNo.length() - 10);
            } else continue;

            if (phoneNo.length() < 10) continue; // for india no only we need to process these things

            List<Map<String,String>> leadEmails = getEmails(phoneNo);
            logger.info("Dbx Email: " + GSON.toJson(leadEmails));
            if (ArrayUtils.isNotEmpty(leadEmails)) {
                recorder.setPushedToLeadSquared(true);
            }
            List<LeadSquaredRequest> requests=new ArrayList<>();
            for (Map<String,String> leadEmail : leadEmails) {

                if (StringUtils.isEmpty(leadEmail.get("emailAddress")) && StringUtils.isEmpty(leadEmail.get("prospectId"))) continue;

                Map<String, String> params = new HashMap<>();
                params.put("userEmail", leadEmail.get("emailAddress"));
                params.put("prospectId", leadEmail.get("prospectId"));
                params.put("mobileNumber", phoneNo);
                params.put("loc", recorder.getLocation());
                params.put("addr", recorder.getAddress());
                params.put("direction", recorder.getDirection());
                params.put("callee", recorder.getLeadPhoneNo());
                params.put("duration", recorder.getDuration());
                params.put("sharedLink", recorder.getSharedLink());
                params.put("created", recorder.getCreated());

                logger.info("LEAD SQUARED REQUEST DATA: " + GSON.toJson(params));
                LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_ON_CALL, null, params, null);
                // leadsquaredManager.executeAsyncTask(request);
                requests.add(request);
            }
            if(ArrayUtils.isNotEmpty(requests)){
                leadsquaredManager.executeBulkAsyncTask(requests);
            }
        }
        return recorderMap;
    }

    private List<Map<String,String>> getEmails(String phoneNo) {
        List<Map<String,String>> leadEmails = callVedantuDataSearchWithKey(phoneNo);
        if (ArrayUtils.isEmpty(leadEmails)) {
            leadEmails = callVedantuDataSearchWithKey("+91-" + phoneNo);
        }
        return leadEmails;
    }

    private List<Map<String,String>> callVedantuDataSearchWithKey(String phoneNo) {
        List<Map<String,String>> leadEmails;
        String serverUrl = VEDANTUDATA_ENDPOINT + "/leadsquared/lead/getLeadEmailsByPhone?phoneNo=" + URLEncoder.encode(phoneNo);
        logger.info("ved-data server url: " + serverUrl);
        ClientResponse response = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null);
        String entity = null;
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            entity = response.getEntity(String.class);
        } catch (VException e) {
            logger.error(e.getErrorMessage(), e);
        }

        if (StringUtils.isNotEmpty(entity)) {
            Type listType=new TypeToken<List<Map<String,String>>>(){}.getType();
            leadEmails = GSON.fromJson(entity, listType);
        } else return new ArrayList<>();
        return leadEmails;
    }
}
