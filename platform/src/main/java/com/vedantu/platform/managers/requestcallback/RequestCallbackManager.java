package com.vedantu.platform.managers.requestcallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.board.pojo.BoardInfoRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.platform.enums.requestcallback.TeacherLeadStatus;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.requestcallback.request.AddRequestCallbackDetailsReq;
import com.vedantu.platform.pojo.requestcallback.request.ESPopulatorRequest;
import com.vedantu.platform.pojo.requestcallback.request.GetRequestCallBacksDetailsReq;
import com.vedantu.platform.pojo.requestcallback.request.IncrementType;
import com.vedantu.platform.pojo.requestcallback.request.UpdateTeacherStatusRequestCallbackReq;
import com.vedantu.platform.pojo.requestcallback.response.RequestCallbackDetailsRes;
import com.vedantu.platform.pojo.requestcallback.response.SessionInfoResponse;
import com.vedantu.platform.pojo.requestcallback.response.TeacherLeadStatusColourCoding;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.pojo.subscription.GetSubscriptionsResponse;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.subscription.request.GetSubscriptionsReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SessionModel;
import java.util.HashSet;
import java.util.Set;
import org.dozer.DozerBeanMapper;

@Service
public class RequestCallbackManager {

    @Autowired
    LogFactory logFactory;

    @Autowired
    RequestCallbackDao requestCallbackDao;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SubscriptionManager subscriptionManager;

    Logger logger = logFactory.getLogger(RequestCallbackManager.class);
    
    @Autowired
    private DozerBeanMapper mapper;     

    public RequestCallbackManager() {
    }

    public static Map<TeacherLeadStatus, TeacherLeadStatusColourCoding> colourCodingMap = new HashMap<TeacherLeadStatus, TeacherLeadStatusColourCoding>() {
        {
            put(TeacherLeadStatus.INTERESTED, new TeacherLeadStatusColourCoding("#416731", "#eaf1c9"));
            put(TeacherLeadStatus.FOLLOW_UP, new TeacherLeadStatusColourCoding("#726525", "#ffeea9"));
            put(TeacherLeadStatus.NOT_INTERESTED, new TeacherLeadStatusColourCoding("#8e3820", "#fedabd"));
            put(TeacherLeadStatus.NOT_UPDATED, new TeacherLeadStatusColourCoding("#646464", "#cccccc"));
        }
    };

    public RequestCallBackDetails addRequestCallback(AddRequestCallbackDetailsReq addRequestCallbackDetailsReq)
            throws VException {
        logger.info("Request Received for adding request callback : " + addRequestCallbackDetailsReq.toString());
        RequestCallBackDetails requestCallBackDetails = new RequestCallBackDetails(addRequestCallbackDetailsReq, mapper);

        try {
            List<RequestCallBackDetails> callBackDetailsPresent = requestCallbackDao.getByTandSCombination(addRequestCallbackDetailsReq.getStudentId(), addRequestCallbackDetailsReq.getTeacherId());
            if (callBackDetailsPresent.isEmpty()) {
                requestCallbackDao.upsert(requestCallBackDetails, addRequestCallbackDetailsReq.getCallingUserId() != null ? addRequestCallbackDetailsReq.getCallingUserId().toString() : null);
                try {
                    ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbCreatedCount",
                            requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                    logger.info(" es populate for rcb " + new Gson().toJson(esPopulatorRequest));
                    populateESParam(esPopulatorRequest);
                    //logger.info(" fosEndpointForESpopulator: " + fosEndpointForESpopulator);
                    //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                    //		new Gson().toJson(esPopulatorRequest));
                } catch (Exception e) {
                    logger.error("could not index into es for create rcb :" + requestCallBackDetails.toString());
                }
            } else {
                requestCallBackDetails = callBackDetailsPresent.get(0);
            }
            //sending leadsquare request
            sendLeadActivity(requestCallBackDetails);

            //sending email
            Map<String, Object> payload = new HashMap<>();
            payload.put("requestCallBackDetails", requestCallBackDetails);
            payload.put("communicationType", CommunicationType.REQUEST_CALLBACK);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REQUEST_CALLBACK_COMMUNICATION, payload);
            asyncTaskFactory.executeTask(params);
            logger.info("Request Callback added : " + requestCallBackDetails.toString());
        } catch (Throwable e) {
            logger.error("request callback adding failed : " + e.getMessage());
            throw new VException(ErrorCode.SERVICE_ERROR, "request callback addition  failed");
        }
        return requestCallBackDetails;
    }

    public RequestCallbackDetailsRes updateTeacherStatus(
            UpdateTeacherStatusRequestCallbackReq updateTeacherStatusRequestCallbackReq) throws VException {
        logger.info("Request Received for updating request callback teacher status : "
                + updateTeacherStatusRequestCallbackReq.toString());

        RequestCallBackDetails requestCallBackDetails = requestCallbackDao
                .getById(updateTeacherStatusRequestCallbackReq.getId());
        if (updateTeacherStatusRequestCallbackReq.getTeacherLeadStatus() != null) {
            requestCallBackDetails.setTeacherActionTaken(true);
            requestCallBackDetails.setTeacherLeadStatus(updateTeacherStatusRequestCallbackReq.getTeacherLeadStatus());
            //requestCallBackDetails
            //        .setLastUpdatedBy(updateTeacherStatusRequestCallbackReq.getCallingUserId().toString());
            requestCallBackDetails.setLastUpdated(System.currentTimeMillis());
            try {
                requestCallbackDao.upsert(requestCallBackDetails, updateTeacherStatusRequestCallbackReq.getCallingUserId() != null ? updateTeacherStatusRequestCallbackReq.getCallingUserId().toString() : null);

                try {
                    ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbStatusUpdatedCount",
                            requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                    //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                    //		new Gson().toJson(esPopulatorRequest));
                    populateESParam(esPopulatorRequest);
                } catch (Exception e) {
                    logger.error("could not index into es for update status rcb :" + requestCallBackDetails.toString());
                }
                logger.info("request callback updated : " + requestCallBackDetails.toString());

                SessionInfoResponse sessionInfoResponse = getUser(requestCallBackDetails.getStudentId(),
                        requestCallBackDetails.getTeacherId(), requestCallBackDetails.getBoardId().toString(),
                        requestCallBackDetails.getSubscriptionId());

                return new RequestCallbackDetailsRes(requestCallBackDetails, sessionInfoResponse);
            } catch (Throwable e) {
                logger.error("request callback updating failed : " + e.getMessage());
                throw new VException(ErrorCode.SERVICE_ERROR, "Teacher Lead Status update failed");
            }
        } else {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Teacher Lead Status Not Present");
        }
    }

    public List<RequestCallbackDetailsRes> getRequestCallBacksDetails(
            GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq) throws VException {
        logger.info(
                "Request received for getting Request call back details : " + getRequestCallBacksDetailsReq.toString());
        try {
            List<RequestCallBackDetails> requestCallBackDetailsList = requestCallbackDao
                    .get(getRequestCallBacksDetailsReq);
            logger.debug("Get request call back details received from DAO : " + requestCallBackDetailsList.toString());
            List<RequestCallbackDetailsRes> requestCallbackDetailsRes = new ArrayList<>();

            Set<Long> userIds = new HashSet<>();
            List<Long> boardIds = new ArrayList<>();
            Set<Long> subscriptionIds = new HashSet<>();
            for (RequestCallBackDetails requestCallBackDetails : requestCallBackDetailsList) {
                if (requestCallBackDetails.getStudentId() != null) {
                    userIds.add(requestCallBackDetails.getStudentId());
                }
                if (requestCallBackDetails.getTeacherId() != null) {
                    userIds.add(requestCallBackDetails.getTeacherId());
                }
                if (requestCallBackDetails.getBoardId() != null) {
                    boardIds.add(requestCallBackDetails.getBoardId());
                }
                if (requestCallBackDetails.getSubscriptionId() != null) {
                    subscriptionIds.add(requestCallBackDetails.getSubscriptionId());
                }
            }

            Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            Map<Long, List<SessionInfo>> subscriptionInfosMap = fosUtils.getSubscriptionSessionInfosMap(subscriptionIds);

            for (RequestCallBackDetails requestCallBackDetails : requestCallBackDetailsList) {
                SessionInfoResponse sessionInfoResponse = new SessionInfoResponse();
                Board board = boardMap.get(requestCallBackDetails.getBoardId());
                UserBasicInfo student = userMap.get(requestCallBackDetails.getStudentId());
                UserBasicInfo teacher = userMap.get(requestCallBackDetails.getTeacherId());
                List<SessionInfo> sessions = subscriptionInfosMap.get(requestCallBackDetails.getSubscriptionId());
                if (board != null) {
                    sessionInfoResponse.setBoard(new BoardInfoRes(board));
                }
                if (student != null) {
                    sessionInfoResponse.setStudent(student);
                }

                if (teacher != null) {
                    sessionInfoResponse.setTeacher(teacher);
                }
                if (sessions != null && !sessions.isEmpty()) {
                    sessionInfoResponse.setSessionList(sessions);
                }
                requestCallbackDetailsRes
                        .add(new RequestCallbackDetailsRes(requestCallBackDetails, sessionInfoResponse));
            }
            logger.debug("Get request call back details return : " + requestCallbackDetailsRes.toString());
            return requestCallbackDetailsRes;
        } catch (Throwable e) {
            logger.error("Get request call back details failed : " + e.getMessage());
            throw new VException(ErrorCode.SERVICE_ERROR, "Get request call back details failed");
        }
    }

    private SessionInfoResponse getUser(Long studentId, Long teacherId, String subject, Long subscriptionId) {
        Long boardId = null;
        if (subject != null) {
            boardId = Long.parseLong(subject);
        }

        if (studentId == null && teacherId == null && boardId == null) {
            logger.info("getUser: Called with all params null");
            // TODO throw custom exception
            throw new IllegalArgumentException("getUser: getting Users failed");
        }

        Set<Long> userIds = new HashSet<>();
        List<Long> boardIds = new ArrayList<>();
        Set<Long> subscriptionIds = new HashSet<>();
        userIds.add(teacherId);
        userIds.add(studentId);
        boardIds.add(boardId);
        subscriptionIds.add(subscriptionId);

        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
        Map<Long, List<SessionInfo>> subscriptionInfosMap = fosUtils.getSubscriptionSessionInfosMap(subscriptionIds);

        SessionInfoResponse sessionInfoResponse = new SessionInfoResponse();
        Board board = boardMap.get(boardId);
        UserBasicInfo student = userMap.get(studentId);
        UserBasicInfo teacher = userMap.get(teacherId);
        List<SessionInfo> sessions = subscriptionInfosMap.get(subscriptionId);
        if (board != null) {
            sessionInfoResponse.setBoard(new BoardInfoRes(board));
        }
        if (student != null) {
            sessionInfoResponse.setStudent(student);
        }

        if (teacher != null) {
            sessionInfoResponse.setTeacher(teacher);
        }
        if (sessions != null && !sessions.isEmpty()) {
            sessionInfoResponse.setSessionList(sessions);
        }

        return sessionInfoResponse;
    }

    public Map<String, TeacherLeadStatus> getAllStatusMessages() {

        Map<String, TeacherLeadStatus> statusStrings = new LinkedHashMap<>();
        for (TeacherLeadStatus status : TeacherLeadStatus.values()) {
            statusStrings.put(TeacherLeadStatus.getStatusString(status), status);
        }
        return statusStrings;
    }

    public RequestCallbackDetailsRes updateCallingStatus(String id) throws VException {

        try {
            RequestCallBackDetails requestCallBackDetails = requestCallbackDao.getById(id);
            if (requestCallBackDetails != null) {
                if (requestCallBackDetails.getCallingTime() != null) {
                    requestCallBackDetails.setNumberOfCalls(requestCallBackDetails.getNumberOfCalls() + 1);
                } else {
                    requestCallBackDetails.setCallingTime(System.currentTimeMillis());
                    requestCallBackDetails.setNumberOfCalls(1);

                    try {
                        ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbCalledCount",
                                requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                        //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                        //		new Gson().toJson(esPopulatorRequest));
                        populateESParam(esPopulatorRequest);
                    } catch (Exception e) {
                        logger.error("could not index into es for call count update status rcb :"
                                + requestCallBackDetails.toString());
                    }
                    try {
                        ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbAvgResponseTime",
                                requestCallBackDetails.getTeacherId(),
                                requestCallBackDetails.getCallingTime() - requestCallBackDetails.getCreationTime(),
                                IncrementType.AVERAGE);
                        //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                        //		new Gson().toJson(esPopulatorRequest));
                        populateESParam(esPopulatorRequest);
                    } catch (Exception e) {
                        logger.error("could not index into es for average rcb response time :"
                                + requestCallBackDetails.toString());
                    }
                }
                requestCallbackDao.upsert(requestCallBackDetails);
                return new RequestCallbackDetailsRes(requestCallBackDetails,
                        getUser(requestCallBackDetails.getStudentId(), requestCallBackDetails.getTeacherId(),
                                requestCallBackDetails.getBoardId().toString(),
                                requestCallBackDetails.getSubscriptionId()));
            } else {
                throw new VException(ErrorCode.BAD_REQUEST_ERROR, "No request callback present for this id");
            }
        } catch (Exception e) {
            throw new VException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    public void migrateRCBDetailsToES(int start, int size, long time) {

        Query query = new Query();
        query.skip(start);
        query.limit(size);
        query.addCriteria(Criteria.where("creationTime").lte(time));
        List<RequestCallBackDetails> requestCallBackDetailsList = requestCallbackDao.runQuery(query,
                RequestCallBackDetails.class);
        for (RequestCallBackDetails requestCallBackDetails : requestCallBackDetailsList) {

            try {
                ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbCreatedCount",
                        requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                //		new Gson().toJson(esPopulatorRequest));
                populateESParam(esPopulatorRequest);
            } catch (Exception e) {
                logger.error("could not index into es for create rcb :" + requestCallBackDetails.toString());
            }

            try {
                if (requestCallBackDetails.getTeacherLeadStatus() != null
                        && !requestCallBackDetails.getTeacherLeadStatus().equals(TeacherLeadStatus.NOT_UPDATED)) {
                    ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbStatusUpdatedCount",
                            requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                    //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                    //		new Gson().toJson(esPopulatorRequest));
                    populateESParam(esPopulatorRequest);
                }
            } catch (Exception e) {
                logger.error("could not index into es for update status rcb :" + requestCallBackDetails.toString());
            }

            try {
                if (requestCallBackDetails.getCallingTime() != null) {
                    ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbCalledCount",
                            requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                    //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                    //		new Gson().toJson(esPopulatorRequest));
                    populateESParam(esPopulatorRequest);
                }
            } catch (Exception e) {
                logger.error("could not index into es for call count update status rcb :"
                        + requestCallBackDetails.toString());
            }

            try {
                if (requestCallBackDetails.getCallingTime() != null) {
                    ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbAvgResponseTime",
                            requestCallBackDetails.getTeacherId(),
                            requestCallBackDetails.getCallingTime() - requestCallBackDetails.getCreationTime(),
                            IncrementType.AVERAGE);
                    //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                    //		new Gson().toJson(esPopulatorRequest));
                    populateESParam(esPopulatorRequest);
                }
            } catch (Exception e) {
                logger.error(
                        "could not index into es for average rcb response time :" + requestCallBackDetails.toString());
            }

            try {
                if (requestCallBackDetails.getSessionId() != null
                        || requestCallBackDetails.getSubscriptionId() != null) {
                    ESPopulatorRequest esPopulatorRequest = new ESPopulatorRequest("rcbConversionCount",
                            requestCallBackDetails.getTeacherId(), 1L, IncrementType.INCREMENTAL);
                    //ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpointForESpopulator, HttpMethod.POST,
                    //		new Gson().toJson(esPopulatorRequest));
                    populateESParam(esPopulatorRequest);
                }
            } catch (Exception e) {
                logger.error("could not index into es for call count update status rcb :"
                        + requestCallBackDetails.toString());
            }
        }
    }

    public void populateESParam(ESPopulatorRequest esPopulatorRequest) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("esPopulatorRequest", esPopulatorRequest);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_POPULATE, payload);
        asyncTaskFactory.executeTask(params);
    }

    private void sendLeadActivity(RequestCallBackDetails requestCallBackDetails)
            throws BadRequestException {
        Map<String, String> params = new HashMap<>();
        Long studentId = requestCallBackDetails.getStudentId();
        com.vedantu.platform.mongodbentities.board.Board board = boardManager.getBoardById(requestCallBackDetails.getBoardId());
        if (board != null) {
            params.put("subject", board.getName());
        }
        if (requestCallBackDetails.getGrade() != null) {
            params.put("grade", requestCallBackDetails.getGrade().toString());
        }
        params.put("target", requestCallBackDetails.getTarget());
        params.put("message", requestCallBackDetails.getMessage());
        if (requestCallBackDetails.getStudentId() != null) {
            params.put("studentId", requestCallBackDetails.getStudentId().toString());
        }
        if (requestCallBackDetails.getTeacherId() != null) {
            params.put("teacherId", requestCallBackDetails.getTeacherId().toString());
        }
        params.put("eventTime", String.valueOf(System.currentTimeMillis()));
        if (requestCallBackDetails.getTeacherLeadStatus() != null) {
            params.put("teacherLeadStatus", requestCallBackDetails.getTeacherLeadStatus().name());
        }

        List<Long> userIds = new ArrayList<>();
        userIds.add(studentId);
        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, true);

        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_REQUEST_CALLBACK,
                userInfos.get(studentId), params, studentId);
        leadSquaredManager.executeAsyncTask(request);
    }

    public List<RequestCallBackDetails> toBeChecked() {

        Integer daysToUpdateAssignee = ConfigUtils.INSTANCE.getIntValue("vedantu.platform.requestcallback.daysToUpdateAssignee");

        Long currentTime = System.currentTimeMillis();
        GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq = new GetRequestCallBacksDetailsReq(null, null, null, null, null, null, null, null, null, null, new Long((currentTime - daysToUpdateAssignee * (DateTimeUtils.MILLIS_PER_DAY))), currentTime, null, null);

        List<RequestCallBackDetails> requestCallBackDetailsList = requestCallbackDao.get(getRequestCallBacksDetailsReq);

        List<RequestCallBackDetails> toBeChecked = new ArrayList<>();

        for (RequestCallBackDetails requestCallBackDetails : requestCallBackDetailsList) {
            if (requestCallBackDetails.getSessionId() == null || requestCallBackDetails.getSubscriptionId() == null) {
                toBeChecked.add(requestCallBackDetails);
            }
        }

        return toBeChecked;
    }

    public void updateCallbackListWithSessionData(List<RequestCallBackDetails> requestCallBackDetailsList) {
        logger.debug("Request Received for updating request callbacks :" + requestCallBackDetailsList.toString());
        requestCallbackDao.updateAll(requestCallBackDetailsList);
    }

    public void updateLeadsSessionSubscription() throws VException {

        List<RequestCallBackDetails> toBeUpdated = new ArrayList<>();

        List<RequestCallBackDetails> response = toBeChecked();
        if (response == null) {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Not able to call Platform");
        }

        for (RequestCallBackDetails requestCallBackDetails : response) {
            Long studentId = requestCallBackDetails.getStudentId();
            Long teacherId = requestCallBackDetails.getTeacherId();

            Long oldSubscriptionId = requestCallBackDetails.getSubscriptionId();

            requestCallBackDetails = sessionManager.checkForSessions(requestCallBackDetails, studentId, teacherId);

            if (requestCallBackDetails.getSessionId() != null) {
                // add rcb for es
                try {
                    GetSubscriptionsReq req = new GetSubscriptionsReq();
                    req.setStudentId(studentId);
                    req.setTeacherId(teacherId);
                    req.setModel(SessionModel.OTO);
                    req.setActive(true);
                    req.setStart(0);
                    req.setSize(1);
                    GetSubscriptionsResponse res = subscriptionManager.getSubscriptions(req);
                    //Ideally requestCallBackDetails should be assigned the new OTO subscriptionid but since
                    //it was not happening earlier not doing it now
                    logger.info("Size " + res.getSubscriptionsList() != null ? res.getSubscriptionsList().size() : null);
                    if (ArrayUtils.isNotEmpty(res.getSubscriptionsList()) && oldSubscriptionId == null) {
                        toBeUpdated.add(requestCallBackDetails);
                        ESPopulatorRequest reqPojo = new ESPopulatorRequest("rcbConversionCount", teacherId, 1L,
                                IncrementType.INCREMENTAL);
                        populateESParam(reqPojo);
                    }
                } catch (Exception e) {
                    logger.info(e.getMessage());
                    logger.info("could not update conversion status for rcb: studentid " + studentId + " teacherId: "
                            + teacherId);
                }
            }
        }

        if (toBeUpdated.isEmpty()) {
            return;
        }
        updateCallbackListWithSessionData(toBeUpdated);

    }
}
