/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.board;

import com.vedantu.User.Role;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.EditProfileReq;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.board.pojo.BoardInfoRes;
import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.board.pojo.GetBoardsRes;
import com.vedantu.board.requests.CreateBoardReq;
import com.vedantu.board.requests.GetBoardsReq;
import com.vedantu.board.requests.UpdateBoardReq;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.board.BoardDAO;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.utils.PojoUtils;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.IBoardAware;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author somil
 */
@Service
public class BoardManager {

    @Autowired
    private LogFactory logFactory;
    
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BoardManager.class);
    
    @Autowired
    private BoardDAO boardDAO;
    
    @Autowired
    private PojoUtils pojoUtils;
    
    public BoardManager() {
    }
    
    public BoardInfoRes createBoard(CreateBoardReq req)
			throws VException {
		logger.info("createBoard "+req);
                req.verify();
		//Board board = req.toBoard();
                Board board = new Board(req.getName().trim(), req.getSlug().trim(), req.getParentId(), req.getState(),
				req.getCategories(), req.getGrades(), req.getExams(), req.getCallingUserId());
		boardDAO.upsert(board, req.getCallingUserId());

		BoardInfoRes res = new BoardInfoRes(pojoUtils.convertToBoardPojo(board));
		logger.info("createBoard "+res);
		return res;
	}

	public BoardInfoRes updateBoard(UpdateBoardReq req)
			throws VException {
		logger.info("updateBoard "+req);
                req.verify();

		Board board = new Board(req.getName().trim(), req.getSlug().trim(), req.getParentId(), req.getState(),
				req.getCategories(), req.getGrades(), req.getExams(), req.getCallingUserId());
                board.setId(req.getId());
		boardDAO.upsert(board, req.getCallingUserId());
		BoardInfoRes res = new BoardInfoRes(pojoUtils.convertToBoardPojo(board));
		logger.info("createBoard "+res);
		return res;
	}

	public GetBoardsRes getBoards(GetBoardsReq req) throws VException {
		logger.info("updateBoard "+req);
                req.verify();

		GetBoardsRes res = new GetBoardsRes();
                
                if(req.getAddChilds() != null && req.getAddChilds()){
                    req.setParentId(null);
                }
                //if(req.getState() == null){
                //    req.setState(VisibilityState.VISIBLE);
                //}
		List<Board> boards = boardDAO.getBoards(req.getCategory(),
				req.getGrade(), req.getExam(), req.getParentId(),
				req.getState());
                List<BoardTreeInfo> boardInfoList = new ArrayList<BoardTreeInfo>();
		for (Board board : boards) {
                    BoardTreeInfo boardInfo = new BoardTreeInfo(pojoUtils.convertToBoardPojo(board));
                    Long parentId = board.getParentId();
                    if(parentId == null || parentId <=0 || parentId.equals(req.getParentId())){
                        boardInfoList.add(boardInfo);
                    }else{
                        searchAndUpdateBoard(boardInfoList,boardInfo);
                    }
                    //res.addItem(new BoardInfoRes(board));
		}
                res.setList(boardInfoList);
		logger.info("getBoards "+res);
		return res;
	}
        private void searchAndUpdateBoard(List<BoardTreeInfo> boardInfoList,BoardTreeInfo boardInfo){
            Long parentId = boardInfo.getParentId();
            for(BoardTreeInfo _boardInfo : boardInfoList){
                List<BoardTreeInfo> children = _boardInfo.getChildren();
                if(_boardInfo.getId().equals(parentId)){
                    if(children == null){
                        children = new ArrayList<BoardTreeInfo>();
                    }
                    children.add(boardInfo);
                    _boardInfo.setChildren(children);
                }else if(children!=null && children.size()>0){
                    searchAndUpdateBoard(children, boardInfo);
                }
            }
        }
	public List<BoardTreeInfo> toBoardTree(IBoardAware mapping,
			Map<Long, Board> boardInfoMap) {
		List<BoardTreeInfo> boardTree = new ArrayList<BoardTreeInfo>();
		if (mapping._getBoardIds() == null || boardInfoMap == null) {
			return boardTree;
		}

		Map<Long, List<BoardTreeInfo>> parentChildMap = new HashMap<Long, List<BoardTreeInfo>>();
		for (Long boardId : mapping._getBoardIds()) {
			Board board = boardInfoMap.get(boardId);
                    if (board != null) {
                        if (board.getParentId() == null
                                || board.getParentId() == Board.DEFAULT_PARENT_ID) {
                            boardTree.add(new BoardTreeInfo(pojoUtils.convertToBoardPojo(board)));
                            continue;
                        }

                        if (parentChildMap.get(board.getParentId()) == null) {
                            parentChildMap.put(board.getParentId(),
                                    new ArrayList<BoardTreeInfo>());
                        }
                        parentChildMap.get(board.getParentId()).add(
                                new BoardTreeInfo(pojoUtils.convertToBoardPojo(boardInfoMap.get(boardId))));
                    }
		}

		for (BoardTreeInfo bTree : boardTree) {
			List<BoardTreeInfo> boardInfoTree = parentChildMap.get(bTree
					.getId());
			if (boardInfoTree != null) {
				Collections.sort(boardInfoTree, new SlugValueComparator());
			}
			bTree.setChildren(boardInfoTree);
		}
		Collections.sort(boardTree, new SlugValueComparator());
		return boardTree;
	}

    public Board getBoardById(Long boardId){
        return boardDAO.getById(boardId);
    }    

    public Board getBoardByField(String fieldName, String fieldValue) throws VException {
        return boardDAO.getBoardByField(fieldName, fieldValue);
    }


    public Map<Long, Board> getBoardsMap(List<Long> ids) throws VException {
        return boardDAO.getBoardsMap(ids);
    }

    public List<Board> getBoards(Collection<Long> ids) throws VException {
        return boardDAO.getBoards(ids);
    }

    public void fillBoardInfo(UserInfo userInfo) {
        //TODO: Scope for optimization, should make in one call
        if (Role.TEACHER.equals(userInfo.getRole())) {
            TeacherInfo teacherInfo = (TeacherInfo) userInfo.getInfo();
            if (teacherInfo.getPrimarySubject() != null) {
                Board board = boardDAO.getById(teacherInfo.getPrimarySubject());
                if (board != null) {
                    userInfo.setPrimaryBoard(pojoUtils.convertToBoardPojo(board));
                }
            }
            if (teacherInfo.getSecondarySubject() != null) {
                Board board = boardDAO.getById(teacherInfo.getSecondarySubject());
                if (board != null) {
                    userInfo.setSecondaryBoard(pojoUtils.convertToBoardPojo(board));
                }
            }
        }
    }

    public List<BoardTreeInfo> getBoardTreeInfoForContent(Set<Long> ids) throws VException {
        Map<Long, Board> boardInfoMap = getBoardsMap(new ArrayList<>(ids));
        return toBoardTree(new IBoardAware() {
            @Override
            public Set<String> _getBoardIdsInString() {
                Set<String> boardIdsLong = new HashSet<>();
		for (Long boardId : ids) {
			boardIdsLong.add(boardId.toString());
		}
		return boardIdsLong;
            }

            @Override
            public Set<Long> _getBoardIds() {
                return ids;
            }
        }, boardInfoMap);
    }

    private static class SlugValueComparator implements Comparator<BoardTreeInfo>{

        @Override
        public int compare(BoardTreeInfo o1, BoardTreeInfo o2) {
            return o1 != null && o2 != null ? o1.getSlug().compareTo(
				o2.getSlug()) : 0;
        }

        
    }
    
    
    public List<Board> getBoards(String category, String grade, String exam,
            Long parentId, VisibilityState state) {
        logger.info("getBoards ", category, grade, exam, parentId);
        return boardDAO.getBoards(category, grade, exam, parentId, state);
    }
    
    
    
    private HashMap<String, HashMap<Object, Object>>
            _createDiffMap(HashMap<String, HashMap<Object, Object>> existingMap,
                    String _key, Object _old, Object _new) {
        if (existingMap == null) {
            existingMap = new HashMap<String, HashMap<Object, Object>>();
        }
        HashMap<Object, Object> value = new HashMap<Object, Object>();
        value.put(_old, _new);
        existingMap.put(_key, value);
        return existingMap;
    }

    private boolean _hasArrayListChanged(List<String> oldList, List<String> newList) {
        List<String> _oldList = null;
        List<String> _oldListCopy = null;
        if (oldList != null) {
            _oldList = new ArrayList<String>(oldList);
            _oldListCopy = new ArrayList<String>(oldList);
        } else {
            _oldList = new ArrayList<String>();
            _oldListCopy = new ArrayList<String>();
        }
        List<String> _newList = null;
        if (newList != null) {
            _newList = new ArrayList<String>(newList);
        } else {
            _newList = new ArrayList<String>();
        }
        _oldList.removeAll(_newList);
        _newList.removeAll(_oldListCopy);
        if (_oldList.size() > 0 || _newList.size() > 0) {
            return true;
        }
        return false;
    }

    public HashMap<String, HashMap<Object, Object>> diffEditTeacher(TeacherInfo tInfoUser, EditProfileReq req) {
        HashMap<String, HashMap<Object, Object>> retMap = null;

        //TeacherInfo tInfoUser = user.getTeacherInfo();
        TeacherInfo tInfoReq = req.getTeacherInfo();
        if (tInfoUser.getActive() == null || !tInfoUser.getActive().equals(tInfoReq.getActive())) {
            retMap = _createDiffMap(retMap, "Active Status", tInfoUser.getActive(), tInfoReq.getActive());
        }
        if (tInfoUser.getCurrentStatus() == null || !tInfoUser.getCurrentStatus().equals(tInfoReq.getCurrentStatus())) {
            retMap = _createDiffMap(retMap, "Current Status", tInfoUser.getCurrentStatus(), tInfoReq.getCurrentStatus());
        }

        if (_hasArrayListChanged(tInfoUser.getGrades(), tInfoReq.getGrades())) {
            String gradesOld = "";
            if (tInfoUser.getGrades() != null) {
                gradesOld = tInfoUser.getGrades().toString();
            }
            String gradesNew = "";
            if (tInfoReq.getGrades() != null) {
                gradesNew = tInfoReq.getGrades().toString();
            }
            retMap = _createDiffMap(retMap, "Grades", gradesOld, gradesNew);
        }
        if (_hasArrayListChanged(tInfoUser.getCategories(), tInfoReq.getCategories())) {
            String categoriesOld = "";
            if (tInfoUser.getCategories() != null) {
                categoriesOld = tInfoUser.getCategories().toString();
            }
            String categoriesNew = "";
            if (tInfoReq.getCategories() != null) {
                categoriesNew = tInfoReq.getCategories().toString();
            }
            retMap = _createDiffMap(retMap, "Target Categories", categoriesOld, categoriesNew);
        }

        if (tInfoUser.getPrimarySubject() == null || !tInfoUser.getPrimarySubject().equals(tInfoReq.getPrimarySubject())) {
            String primaryBoardOld = "";
            if (tInfoUser.getPrimarySubject() != null) {
                Board primaryBoardOldObj = boardDAO.getById(tInfoUser.getPrimarySubject());
                if (primaryBoardOldObj != null) {
                    primaryBoardOld = primaryBoardOldObj.getName();
                }
            }
            String primaryBoardNew = "";
            if (tInfoReq.getPrimarySubject() != null) {
                Board primaryBoardNewObj = boardDAO.getById(tInfoReq.getPrimarySubject());
                if (primaryBoardNewObj != null) {
                    primaryBoardNew = primaryBoardNewObj.getName();
                }
            }
            retMap = _createDiffMap(retMap, "Primary Subject", primaryBoardOld, primaryBoardNew);
        }
        if (tInfoUser.getSecondarySubject() == null || !tInfoUser.getSecondarySubject().equals(tInfoReq.getSecondarySubject())) {
            String secondaryBoardOld = "";
            if (tInfoUser.getSecondarySubject() != null) {
                Board secondaryBoardOldObj = boardDAO.getById(tInfoUser.getSecondarySubject());
                if (secondaryBoardOldObj != null) {
                    secondaryBoardOld = secondaryBoardOldObj.getName();
                }
            }
            String secondaryBoardNew = "";
            if (tInfoReq.getSecondarySubject() != null) {
                Board secondaryBoardNewObj = boardDAO.getById(tInfoReq.getSecondarySubject());
                if (secondaryBoardNewObj != null) {
                    secondaryBoardNew = secondaryBoardNewObj.getName();
                }
            }
            retMap = _createDiffMap(retMap, "Secondary Subject", secondaryBoardOld, secondaryBoardNew);
        }
        return retMap;
    }
    
    
    public Board getBoardBySlug(String slug) throws VException {
        return getBoardByField(Board.Constants.SLUG, slug);
    }
}
