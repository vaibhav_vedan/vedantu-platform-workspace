package com.vedantu.platform.managers.subscription;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.response.dinero.GetTeacherPlansResponse;
import com.vedantu.platform.pojo.subscription.CancelSubscriptionRequestResponse;
import com.vedantu.platform.pojo.subscription.GetCompactSubscriptionRequestsResponse;
import com.vedantu.platform.pojo.subscription.GetPlanByIdResponse;
import com.vedantu.platform.pojo.subscription.GetSubscriptionRequestsResponse;
import com.vedantu.platform.pojo.subscription.SubscriptionReqVO;
import com.vedantu.platform.pojo.subscription.SubscriptionRequest;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestCompactInfo;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestInfo;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestPaymentDetails;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestPaymentReq;
import com.vedantu.platform.pojo.subscription.SubscriptionVO;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.dinero.pojo.LockBalanceRes;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.pojo.TeacherSlabPlan;
import com.vedantu.dinero.util.InstalmentUtils;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.managers.board.BoardManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.dinero.request.RechargeReq;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.pojo.subscription.SubscriptionRequestPaymentRes;
import com.vedantu.session.pojo.CumilativeRating;
import com.vedantu.subscription.request.GetSubscriptionRequestReq;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.subscription.ResolveSlotConflictReq;
import com.vedantu.util.subscription.UpdateSubscriptionRequest;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.json.JSONException;
import org.json.JSONObject;

@Service
public class SubscriptionRequestManager {

    @Autowired
    private SubscriptionRequestTaskManager subscriptionRequestTaskManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SubscriptionRequestManager.class);

    @Autowired
    private InstalmentUtils instalmentUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private CumilativeRatingDao cumilativeRatingDao;

    public String subscriptionEndpoint;
    public String dineroEndpoint;
    public String userendPoint;
    public Long expireCronTime;

    public static final String NO_SCHEDULE_STRING_STUDENT = "We recommend students to discuss the convenient time slots with their teacher. You can schedule your sessions as soon as your teacher accepts the subscription.";
    public static final String NO_SCHEDULE_STRING_TEACHER = "We recommend you to call student and discuss the convenient time slots. This will help them to start learning immediately with you.";

    public SubscriptionRequestManager() {

    }

    @PostConstruct
    public void init() {
        subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        userendPoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
        expireCronTime = ConfigUtils.INSTANCE.getLongValue("subscriptionRequest.expiryCronTime");
    }

    public SubscriptionRequestInfo createSubscriptionRequest(SubscriptionVO subscriptionVO) throws VException {
        logger.info("ENTRY " + subscriptionVO);
        SubscriptionReqVO subscriptionReqVO = getSubscriptionReqVO(subscriptionVO);
        String requestBody = new Gson().toJson(subscriptionReqVO);
        logger.info("Request to Subscription: " + requestBody);

        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/create",
                HttpMethod.POST, requestBody);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);
        subscriptionRequestInfo
                .setPlan(getTeacherPlan(subscriptionRequestInfo.getPlanId(), subscriptionRequestInfo.getTeacherId()));
        subscriptionRequestInfo = fillUsers(subscriptionRequestInfo);
        //TODO make it async
        ClientResponse incResp = WebUtils.INSTANCE.doCall(userendPoint + "/incSubscriptionRequestCount/"
                + subscriptionRequestInfo.getStudentId(), HttpMethod.POST, null,true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String incRespStr = incResp.getEntity(String.class);
        logger.info("Response from incSubscriptionRequestCount: " + incRespStr);
        logger.info("EXIT " + subscriptionRequestInfo);
        return subscriptionRequestInfo;
    }

    public SubscriptionRequestInfo getSubscriptionRequest(Long id, Long callingUserId) throws VException {
        return getSubscriptionRequest(id, callingUserId, null);
    }

    public SubscriptionRequestInfo getSubscriptionRequest(Long id, Long callingUserId, RequestSource requestSource) throws VException {
        String getUrl = subscriptionEndpoint + "subscriptionRequest/get/" + id;
        if (callingUserId != null) {
            getUrl += "?callingUserId=" + callingUserId;
        }
        logger.info(getUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);
        SessionSchedule schedule = new Gson().fromJson(subscriptionRequestInfo.getSlots(), SessionSchedule.class);
        List<SessionSlot> slots = schedule.getSessionSlots();
        subscriptionRequestInfo.setScheduleSlotInfos(slots);

        InstalmentUtils.ConflictDetails conflictDetails = instalmentUtils.fillResolvableConflictDetails(schedule);
        if (conflictDetails != null && conflictDetails.conflictSessions != null) {
            subscriptionRequestInfo.setTotalConflictCount(conflictDetails.conflictSessions.size());
        } else {
            subscriptionRequestInfo.setTotalConflictCount(schedule.getConflictCount());
        }

//		subscriptionRequestInfo.setSlots(null);
        subscriptionRequestInfo.setScheduleStartTime(schedule.getStartTime());

        subscriptionRequestInfo = fillSubscriptionRequestInfo(subscriptionRequestInfo, requestSource, callingUserId);
        return subscriptionRequestInfo;
    }

    public BasicResponse cancelSubscriptionRequest(UpdateSubscriptionRequest req) throws VException {
        // State changed to CANCELLED
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/cancel",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);

        // unlock the Balance
        unlockBalance(subscriptionRequestInfo.getLockBalanceInfo());

        subscriptionRequestInfo
                .setPlan(getTeacherPlan(subscriptionRequestInfo.getPlanId(), subscriptionRequestInfo.getTeacherId()));

        subscriptionRequestTaskManager.sendCommunication(subscriptionRequestInfo, CommunicationType.SUBSCRIPTION_REQUEST_CANCELLED);

        if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.MOBILE)) {
            CancelSubscriptionRequestResponse cancelSubscriptionRequestResponse = new CancelSubscriptionRequestResponse(
                    subscriptionRequestInfo.getAmount());
            logger.info("Response:" + cancelSubscriptionRequestResponse.toString());
            return cancelSubscriptionRequestResponse;
        }

        if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.WEB)) {
            subscriptionRequestInfo = fillUsers(subscriptionRequestInfo);
        }
        return subscriptionRequestInfo;
    }

    public BasicResponse rejectSubscriptionRequest(UpdateSubscriptionRequest req) throws VException {
        // State changed to REJECTED
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/reject",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);

        // unlock the Balance
        unlockBalance(subscriptionRequestInfo.getLockBalanceInfo());

        subscriptionRequestInfo
                .setPlan(getTeacherPlan(subscriptionRequestInfo.getPlanId(), subscriptionRequestInfo.getTeacherId()));

        subscriptionRequestTaskManager.sendCommunication(subscriptionRequestInfo, CommunicationType.SUBSCRIPTION_REQUEST_REJECTED);

        if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.MOBILE)) {
            CancelSubscriptionRequestResponse cancelSubscriptionRequestResponse = new CancelSubscriptionRequestResponse(
                    subscriptionRequestInfo.getAmount());
            logger.info("Response:" + cancelSubscriptionRequestResponse.toString());
            return cancelSubscriptionRequestResponse;
        }
        if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.WEB)) {
            subscriptionRequestInfo = fillUsers(subscriptionRequestInfo);
        }
        return subscriptionRequestInfo;
    }

    public void expireSubscriptionRequests(Long callingUserId) throws VException {
        Gson gson = new Gson();
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/expire?callingUserId="+callingUserId,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String subResp = resp.getEntity(String.class);
        GetSubscriptionRequestsResponse resp1 = new Gson().fromJson(subResp, GetSubscriptionRequestsResponse.class);
        for (SubscriptionRequest s : resp1.getSubscriptionRequestsList()) {
            String json = gson.toJson(s);
            SubscriptionRequestInfo subscriptionRequestInfo = gson.fromJson(json, SubscriptionRequestInfo.class);
            try {
                unlockBalance(subscriptionRequestInfo.getLockBalanceInfo());
            } catch (Exception e) {
                logger.error("Unlock Amount Failed for subscriptionId: " + subscriptionRequestInfo.getId(),e);
                continue;
            }
            subscriptionRequestTaskManager.sendCommunication(subscriptionRequestInfo, CommunicationType.SUBSCRIPTION_REQUEST_EXPIRED);
        }
    }

    public void sendExpiryReminderSubscriptionRequests() throws VException {
        Gson gson = new Gson();
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/expiryReminder",
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String subResp = resp.getEntity(String.class);
        GetSubscriptionRequestsResponse resp1 = new Gson().fromJson(subResp, GetSubscriptionRequestsResponse.class);
        logger.info("Response from Subscription: " + resp1.toString());
        for (SubscriptionRequest s : resp1.getSubscriptionRequestsList()) {
            String json = gson.toJson(s);
            SubscriptionRequestInfo s1 = gson.fromJson(json, SubscriptionRequestInfo.class);
            subscriptionRequestTaskManager.sendCommunication(s1, CommunicationType.SUBSCRIPTION_REQUEST_EXPIRY_REMINDER);
        }
    }

    public BasicResponse acceptSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws VException {
        // state changed to ACCEPTED
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/accept",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);

        // Async call for remaining updates.
        subscriptionRequestTaskManager.processSubscriptionRequestSubStateAsync(subscriptionRequestInfo);

        subscriptionRequestInfo
                .setPlan(getTeacherPlan(subscriptionRequestInfo.getPlanId(), subscriptionRequestInfo.getTeacherId()));
        if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.MOBILE)) {
            BasicResponse basicResponse = new BasicResponse();
            return basicResponse;
        }
        if (req.getRequestSource() != null && req.getRequestSource().equals(RequestSource.WEB)) {
            subscriptionRequestInfo = fillUsers(subscriptionRequestInfo);
        }
        return subscriptionRequestInfo;
    }

    public SubscriptionRequestInfo activateSubscriptionRequest(UpdateSubscriptionRequest req) throws VException {
        logger.info("Request: " + req.toString());
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndpoint + "subscriptionRequest/activate",
                HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);
        return subscriptionRequestInfo;
    }

    public SubscriptionRequestPaymentRes handlePayments(SubscriptionRequestPaymentReq req) throws VException, JSONException {

        // Validate SubscriptionRequestPaymentReq
        List<String> errors = req.collectErrors();
        if (!errors.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Arrays.toString(errors.toArray()));
        }

        Gson gson = new Gson();
        // Fetch subscription request
        SubscriptionRequestInfo subscriptionRequestInfo = getSubscriptionRequest(req.getSubscriptionRequestId(),
                req.getUserId());

        // Update payment details
        SubscriptionRequestPaymentDetails paymentDetails = gson.fromJson(req.getPaymentDetails(),
                SubscriptionRequestPaymentDetails.class);

        if (paymentDetails == null) {
            paymentDetails = new SubscriptionRequestPaymentDetails();
        }
        paymentDetails.updateDetails(subscriptionRequestInfo);
        logger.info("handlePaymentsPaymentDetails - " + paymentDetails.toString());

        // Lock amount in student wallet
        String lockBalanceResponse = lockBalance(paymentDetails);
        LockBalanceRes lockBalanceRes = new Gson().fromJson(lockBalanceResponse, LockBalanceRes.class);

        // Update Teacher HourlyRate based on discount provided
        Long teacherHourlyRate = subscriptionRequestInfo.getHourlyRate();
        Long totalHours = subscriptionRequestInfo.getTotalHours();
        Long teacherDiscountAmount = lockBalanceRes.getTeacherDiscountAmount();
        if (teacherDiscountAmount != null && teacherDiscountAmount > 0l) {
            if (totalHours != null && totalHours > 0l) {
                teacherHourlyRate -= (teacherDiscountAmount / (totalHours / DateTimeUtils.MILLIS_PER_HOUR));
            }
        }

        // Activate subscription request
        if (lockBalanceRes.getErrorCode() == null) {
            try {
                UpdateSubscriptionRequest updateSubscriptionRequest = new UpdateSubscriptionRequest();
                updateSubscriptionRequest.setId(req.getSubscriptionRequestId());
                updateSubscriptionRequest.setCallingUserId(req.getUserId());
                updateSubscriptionRequest.setReason("SYSTEM_ACTIVATE");
                updateSubscriptionRequest.setLockBalanceInfo(lockBalanceResponse);
                updateSubscriptionRequest.setAmount(
                        Long.valueOf(lockBalanceRes.getPromotionalCost() + lockBalanceRes.getNonPromotionalCost()));
                updateSubscriptionRequest.setPaymentDetails(gson.toJson(paymentDetails));
                updateSubscriptionRequest.setHourlyRate(teacherHourlyRate);
                updateSubscriptionRequest.setCallingUserId(req.getCallingUserId());
                subscriptionRequestInfo = activateSubscriptionRequest(updateSubscriptionRequest);
            } catch (Exception ex) {
                try {
                    logger.info("Activate failed, unlocking the balance: " + ex.getMessage());
                    unlockBalance(lockBalanceResponse);
                } catch (Exception e) {
                    logger.error("handlePayments Unlock also Failed after Activate failure: " + e.getMessage()
                            + " req - " + req.toString() + " lockedBalanceInfo - " + lockBalanceResponse);
                    throw e;
                }
                throw ex;
            }
        }else{            
              String updateSubscriptionRequestUrl = subscriptionEndpoint + "/subscriptionRequest/updatePaymentDetails";
              JSONObject updateSubscriptionRequest = new JSONObject();
              updateSubscriptionRequest.put("subscriptionRequestId", subscriptionRequestInfo.getId().toString());
              updateSubscriptionRequest.put("paymentDetails", gson.toJson(paymentDetails));

              WebUtils.INSTANCE.doCall(updateSubscriptionRequestUrl, HttpMethod.POST,
                      updateSubscriptionRequest.toString());            
        }

        SubscriptionRequestPaymentRes subscriptionRequestPaymentRes = new SubscriptionRequestPaymentRes();

        if (lockBalanceRes.getErrorCode() != null
                && ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE.equals(lockBalanceRes.getErrorCode())) {
            //String redirectUrl = request.getRequestURL().toString() + "?" + request.getQueryString()
            //					+ "&redirectRequest=true";
            RechargeReq rechargeReq = new RechargeReq(req.getCallingUserId(),
                    lockBalanceRes.getAmountToBePaid(),
                    req.getRedirectUrl());
            rechargeReq.setRefType(TransactionRefType.SUBSCRIPTION_REQUEST);
            rechargeReq.setRefId(req.getSubscriptionRequestId().toString());
            RechargeUrlInfo rechargeUrlInfo = paymentManager.rechargeAccount(rechargeReq);
            subscriptionRequestPaymentRes.updateRechargeUrlParams(rechargeUrlInfo);
            subscriptionRequestPaymentRes.setAmount((long) lockBalanceRes.getAmountToBePaid());
        } else {
            subscriptionRequestPaymentRes.setRedirect(true);
            subscriptionRequestTaskManager.sendCommunication(subscriptionRequestInfo, CommunicationType.SUBSCRIPTION_REQUEST_CREATED);
        }

        return subscriptionRequestPaymentRes;
    }

    public String lockBalance(SubscriptionRequestPaymentDetails paymentDetails) throws VException {
        logger.info("Dinero Request: " + paymentDetails);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/lockBalance", HttpMethod.POST,
                new Gson().toJson(paymentDetails));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String response = resp.getEntity(String.class);
        logger.info("Dinero Response: " + response);
        return response;
    }

    public String unlockBalance(String lockBalanceInfo) throws VException {
        logger.info("Dinero Request: " + lockBalanceInfo);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/unLockBalance", HttpMethod.POST,
                lockBalanceInfo);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String response = resp.getEntity(String.class);
        logger.info("Dinero Response: " + response);
        return response;
    }

    public TeacherSlabPlan getTeacherPlan(String planId, Long teacherId) {
        if (planId != null) {
            try {
                //TODO when not fetch plan using planId directly
                logger.info("Dinero Request: " + teacherId);
                ClientResponse resp = WebUtils.INSTANCE.doCall(
                        dineroEndpoint + "/pricing/getTeacherPlans?teacherId=" + teacherId, HttpMethod.GET, null);
                String planResponse = resp.getEntity(String.class);
                GetTeacherPlansResponse plan = new Gson().fromJson(planResponse, GetTeacherPlansResponse.class);
                List<TeacherSlabPlan> plans = plan.getPlans();
                if (plans != null && !plans.isEmpty()) {
                    for (TeacherSlabPlan t : plans) {
                        if (t.getId().equals(planId)) {
                            return t;
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Unable to get Teacher Plan: " + e.getMessage());
            }
        } else {
            logger.info("No plan Id");
        }
        return null;
    }

    public SubscriptionReqVO getSubscriptionReqVO(SubscriptionVO subscriptionVO) {
        try {

            SubscriptionReqVO subscriptionReqVO = null;
            if (subscriptionVO.getEntityType() == null) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "PlanId: " + subscriptionVO.getEntityId());
            }

            //adding subject slug from boardid
            if (subscriptionVO.getBoardId() != null) {
                Board board = boardManager.getBoardById(subscriptionVO.getBoardId());
                if (board != null) {
                    subscriptionVO.setSubject(board.getSlug());
                }
            }

            if (subscriptionVO.getEntityType().equals(EntityType.PLAN)) {

                Long teacherHourlyRate = getHourlyRate(subscriptionVO.getEntityId());
                if (teacherHourlyRate == null) {
                    logger.error("Null hourly rate from dinero for PlanId: " + subscriptionVO.getEntityId());
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                            "Null hourly rate from dinero for PlanId: " + subscriptionVO.getEntityId());
                }

                String paymentDetails = createPaymentDetails(subscriptionVO);
                subscriptionReqVO = new SubscriptionReqVO(subscriptionVO.getTeacherId(), subscriptionVO.getStudentId(),
                        subscriptionVO.getTitle(), subscriptionVO.getEntityId(), subscriptionVO.getModel(),
                        subscriptionVO.getOfferingId(), subscriptionVO.getHours(), subscriptionVO.getSubject(),
                        subscriptionVO.getTarget(), subscriptionVO.getGrade(), paymentDetails,
                        subscriptionVO.getBoardId(), subscriptionVO.getSessionSchedule(), null,
                        subscriptionVO.getNote(), teacherHourlyRate);

            } else if (subscriptionVO.getEntityType().equals(EntityType.SUBSCRIPTION_REQUEST)) {

                SessionSchedule sessionSchedule = subscriptionVO.getSessionSchedule();
                Long totalHours = sessionSchedule.calculateTotalHours();
                Long subscriptionRequestId = Long.parseLong(subscriptionVO.getEntityId());
                SubscriptionRequestInfo subscriptionRequestInfo = getSubscriptionRequest(subscriptionRequestId, null);
                Double hours = (double) totalHours / DateTimeUtils.MILLIS_PER_HOUR;
                GetPlanByIdResponse plan = getPlanByHours(subscriptionRequestInfo.getTeacherId(), hours);
                String planId = plan.getId();
                Long hourlyRate = plan.getPrice();

                subscriptionReqVO = new SubscriptionReqVO();
                subscriptionReqVO.setSchedule(sessionSchedule);
                subscriptionReqVO.setTotalHours(totalHours);
                subscriptionReqVO.setSubscriptionRequestId(subscriptionRequestId);
                subscriptionReqVO.setPlanId(planId);
                subscriptionReqVO.setHourlyRate(hourlyRate);
            }
            subscriptionReqVO.setCallingUserId(subscriptionVO.getCallingUserId());
            return subscriptionReqVO;
        } catch (Exception e) {
            logger.error("Error in getSubscriptionReqVO for"
                    + " creating subscription request Req: " + subscriptionVO + ", Error: " + e.getMessage(), e);
        }
        return null;
    }

    public String getUrlString(GetSubscriptionRequestReq req) {
        String url = subscriptionEndpoint + "subscriptionRequest/gets?";
        for (Field field : req.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(req) != null) {
                    url += field.getName() + "=" + field.get(req) + "&";
                }
            } catch (IllegalArgumentException e) {
                logger.debug(e.toString());
            } catch (IllegalAccessException e) {
                logger.debug(e.toString());
            }
        }
        for (Field field : req.getClass().getSuperclass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(req) != null) {
                    url += field.getName() + "=" + field.get(req) + "&";
                }
            } catch (IllegalArgumentException e) {
                logger.debug(e.toString());
            } catch (IllegalAccessException e) {
                logger.debug(e.toString());
            }
        }
        logger.info("url: " + url);
        return url;
    }

    public GetCompactSubscriptionRequestsResponse getSubscriptionRequests(GetSubscriptionRequestReq req) throws VException {
        String getString = getUrlString(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String subResp = resp.getEntity(String.class);
        GetSubscriptionRequestsResponse resp1 = new Gson().fromJson(subResp, GetSubscriptionRequestsResponse.class);
        GetCompactSubscriptionRequestsResponse response = getCompactSubscriptionRequestsResponse(resp1, req.getRequestSource());
        return response;
    }

    public GetCompactSubscriptionRequestsResponse getCompactSubscriptionRequestsResponse(
            GetSubscriptionRequestsResponse resp, RequestSource requestSource) {

        GetCompactSubscriptionRequestsResponse response = new GetCompactSubscriptionRequestsResponse(resp);

        List<Long> studentIds = new ArrayList<>();
        List<Long> teacherIds = new ArrayList<>();
        for (SubscriptionRequest s : resp.getSubscriptionRequestsList()) {
            studentIds.add(s.getStudentId());
            teacherIds.add(s.getTeacherId());
        }
        Map<Long, UserBasicInfo> studentInfosMap = fosUtils.getUserBasicInfosMapFromLongIds(studentIds, true);
        Integer i = 0;
        for (SubscriptionRequestCompactInfo s : response.getCompactSubscriptionRequestsList()) {
            Long studentId = studentIds.get(i);
            if (studentInfosMap.containsKey(studentId)) {
                s.setStudentName(studentInfosMap.get(studentId).getFullName());
            }
            i++;
        }

        if (requestSource != null) {
            switch (requestSource) {

                case MOBILE:
                    break;

                case WEB:
                    Map<Long, UserBasicInfo> teacherInfosMap = fosUtils.getUserBasicInfosMapFromLongIds(teacherIds, true);
                    i = 0;
                    for (SubscriptionRequestCompactInfo s
                            : response.getCompactSubscriptionRequestsList()) {
                        Long studentId = studentIds.get(i);
                        if (studentInfosMap.containsKey(studentId)) {
                            s.setStudent(studentInfosMap.get(studentId));
                        }
                        Long teacherId = teacherIds.get(i);
                        if (teacherInfosMap.containsKey(teacherId)) {
                            s.setTeacher(teacherInfosMap.get(teacherId));
                        }
                        s.setStudentName(null);
                        i++;
                    }
                    break;

                default:
                    break;
            }
        }

        return response;
    }

    public SubscriptionRequestInfo fillSubscriptionRequestInfo(SubscriptionRequestInfo info,
            RequestSource requestSource, Long callingUserId) throws VException {

        List<Long> ids = new ArrayList<>();
        if (requestSource == null) {
            requestSource = RequestSource.WEB;
        }
        switch (requestSource) {
            case MOBILE:
                ids.add(info.getTeacherId());
                ids.add(info.getStudentId());
                Map<Long, User> users = fosUtils.getUserInfosMap(ids, true);
                if (users.containsKey(info.getTeacherId())) {
                    info.setTeacher(new UserInfo(users.get(info.getTeacherId()), null, true));
                }
                if (users.containsKey(info.getStudentId())) {
                    info.setStudent(new UserInfo(users.get(info.getStudentId()), null, true));
                }
                setNoScheduleString(info, callingUserId);
                break;

            default:
                info = fillUsers(info);
                break;
        }
        return info;
    }

    @Deprecated
    public String getSubscriptionRequestConflicts(Long id) throws VException {
        logger.info("Subscription Request: " + id);
        String getUrl = subscriptionEndpoint + "subscriptionRequest/getSlotConflicts?id=" + id;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        return jsonString;
    }

    @Deprecated
    public String resolveSlotConflicts(ResolveSlotConflictReq req) throws VException {
        logger.info("Subscription Request: " + req.toString());
        String getUrl = subscriptionEndpoint + "subscriptionRequest/resolveSlotConflicts";
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUrl, HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        return jsonString;
    }

    public String markScheduledSubState(Long subscriptionRequestId) throws VException {
        logger.info("Subscription Request: " + subscriptionRequestId);
        String postUrl = subscriptionEndpoint + "subscriptionRequest/markScheduled?subscriptionRequestId="
                + subscriptionRequestId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(postUrl, HttpMethod.POST, postUrl);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        // Invoke task
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(resp.getEntity(String.class),
                SubscriptionRequestInfo.class);
        subscriptionRequestTaskManager.processSubscriptionRequestSubStateAsync(subscriptionRequestInfo);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        return jsonString;
    }

    public void retrySubscriptionRequestProcessing(Long subscriptionRequestId) throws VException {
        logger.info("retrySubscriptionRequestProcessing - subscriptionRequestId : " + subscriptionRequestId);
        String getUrl = subscriptionEndpoint + "subscriptionRequest/get/" + subscriptionRequestId;
        logger.info(getUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        SubscriptionRequestInfo subscriptionRequestInfo = new Gson().fromJson(jsonString,
                SubscriptionRequestInfo.class);
        subscriptionRequestTaskManager.processSubscriptionRequestSubStateAsync(subscriptionRequestInfo);
    }

    protected Long getHourlyRate(String planId) throws VException {
        logger.info("Dinero Request: " + planId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/pricing/getPlanById?id=" + planId,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String planResponse = resp.getEntity(String.class);
        GetPlanByIdResponse planRes = new Gson().fromJson(planResponse, GetPlanByIdResponse.class);
        logger.info("Dinero Response: " + planRes.toString());
        return planRes.getPrice();
    }

    protected String createPaymentDetails(SubscriptionVO subscriptionVO) {
        JsonObject json = new JsonObject();
        json.addProperty("teacherDiscountCouponId", subscriptionVO.getTeacherDiscountCouponId());
        json.addProperty("teacherDiscountAmount", subscriptionVO.getTeacherDiscountAmount());
        json.addProperty("vedantuDiscountCouponId", subscriptionVO.getVedantuDiscountCouponId());
        json.addProperty("vedantuDiscountAmount", subscriptionVO.getVedantuDiscountAmount());
        if (subscriptionVO.getPaymentType() != null) {
            json.addProperty("paymentType", subscriptionVO.getPaymentType().name());
        }
        String paymentDetails = new Gson().toJson(json);
        return paymentDetails;
    }

    public GetPlanByIdResponse getPlanByHours(Long teacherId, Double hours) throws VException {
        logger.info("Dinero Request: teacherId: " + teacherId + ", hours: " + hours);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/pricing/getPlanByHours?teacherId=" + teacherId + "&hours=" + hours, HttpMethod.GET,
                null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        GetPlanByIdResponse response = new Gson().fromJson(jsonString, GetPlanByIdResponse.class);
        logger.info("Dinero Response: " + response.toString());
        return response;
    }

    public String getPendingSubscriptionRequests(Long startTime, Long endTime) throws VException {
        logger.info("Subscription Request: startTime: " + startTime + ",endTime: " + endTime);
        String getUrl = subscriptionEndpoint + "subscriptionRequest/getPendingRequests?startTime=" + startTime
                + "&endTime=" + endTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        logger.info("Response from Subscription: " + jsonString);
        return jsonString;
    }

    public SubscriptionRequestInfo fillUsers(SubscriptionRequestInfo subscriptionReqResponse) throws VException {

        // filling teacher and student info
        List<Long> userIds = new ArrayList<>();
        Long teacherId = subscriptionReqResponse.getTeacherId();
        Long studentId = subscriptionReqResponse.getStudentId();
        userIds.add(teacherId);
        userIds.add(studentId);
        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, true);

        List<String> userIdsString = new ArrayList<>();
        userIdsString.add(teacherId.toString());
        userIdsString.add(studentId.toString());
        Map<String, CumilativeRating> ratingsMap = cumilativeRatingDao.getCumulativeRatingsMap(userIdsString, com.vedantu.util.EntityType.USER);
        UserInfo teacherUserInfo = new UserInfo(userInfos.get(teacherId), ratingsMap.get(teacherId.toString()), true);
        UserInfo studentUserInfo = new UserInfo(userInfos.get(studentId), ratingsMap.get(studentId.toString()), true);

        subscriptionReqResponse.setTeacher(teacherUserInfo);
        subscriptionReqResponse.setStudent(studentUserInfo);

        //filling boardinfo
        Board board = boardManager.getBoardById(subscriptionReqResponse.getBoardId());
        subscriptionReqResponse.setSubject(board.getSlug());
        return subscriptionReqResponse;
    }

    public void retrySubscriptionRequestProcessingCron() {
        Long currentTime = System.currentTimeMillis();
        currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long cronTimeWindow = 60 * 1000
                * ConfigUtils.INSTANCE.getLongValue("subscription.request.retry.time.window.mins");
        Long timeDelay = 60 * 1000
                * ConfigUtils.INSTANCE.getLongValue("subscription.request.retry.execution.delay.mins");
        Long endTime = currentTime - timeDelay;
        Long startTime = endTime - cronTimeWindow;

        logger.info("SubscriptionRequestProcessingTask - startTime : " + startTime + " endTime : " + endTime);
        try {
            String response = getPendingSubscriptionRequests(startTime, endTime);
            GetSubscriptionRequestsResponse getSubscriptionRequestsResponse
                    = new Gson().fromJson(response, GetSubscriptionRequestsResponse.class);
            List<SubscriptionRequest> pendingSubscriptions = getSubscriptionRequestsResponse.getSubscriptionRequestsList();
            if (pendingSubscriptions == null || pendingSubscriptions.isEmpty()) {
                logger.info("pendingSubscriptionsEmpty - No pending subscription requests found ");
                return;
            }

            for (SubscriptionRequest subscriptionRequest : pendingSubscriptions) {
                try {
                    logger.info("pendingSubscriptionRequest : " + subscriptionRequest.toString());
                    retrySubscriptionRequestProcessing(subscriptionRequest.getId());
                } catch (Exception ex) {
                    logger.error("SubscriptionRequestProcessingTaskError - "
                            + System.currentTimeMillis() + " ex: " + ex.getMessage() + " ex: " + ex.toString()
                            + " for id : " + subscriptionRequest.getId());
                }
            }
        } catch (Exception ex) {
            logger.error("SubscriptionRequestProcessingTaskError - "
                    + System.currentTimeMillis() + " ex: " + ex.getMessage() + " ex: " + ex.toString());
        }
        updateLeadActivites();
    }

    //TODO move to Subscription once leadsquared manager is created
    public void updateLeadActivites() {
        logger.info("updateLeadActivites  ");
        Long currentTime = System.currentTimeMillis();
        currentTime -= currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
        Long lastUpdatedTime = currentTime - 15 * DateTimeUtils.MILLIS_PER_MINUTE;

        GetSubscriptionRequestReq getSubscriptionRequestReq = new GetSubscriptionRequestReq();
        getSubscriptionRequestReq.setLastUpdatedTime(lastUpdatedTime);
        getSubscriptionRequestReq.setRequestSource(RequestSource.WEB);

        try {
            GetCompactSubscriptionRequestsResponse response = getSubscriptionRequests(getSubscriptionRequestReq);
            if (response.getCount() == 0) {
                logger.info("No Subscription requests found");
                return;
            }

            List<SubscriptionRequestCompactInfo> subscriptionRequestInfos = response
                    .getCompactSubscriptionRequestsList();
            if (subscriptionRequestInfos != null) {
                List<LeadSquaredRequest> requests=new ArrayList<>();
                for (SubscriptionRequestCompactInfo entry : subscriptionRequestInfos) {
                    try {
                        logger.info("SubscriptionRequestCompactInfo entry - " + entry.toString());
                        Map<String, String> bodyScopes = new HashMap<>();
                        bodyScopes.put("subscriptionRequestState", String.valueOf(entry.getState()));
                        bodyScopes.put("totalHours",
                                String.valueOf(entry.getTotalHours() / (DateTimeUtils.MILLIS_PER_HOUR)));
                        bodyScopes.put("noOfWeeks", String.valueOf(entry.getNoOfWeeks()));
                        bodyScopes.put("subscriptionRequestId", String.valueOf(entry.getId()));
                        bodyScopes.put("studentId", String.valueOf(entry.getStudent().getUserId()));
                        bodyScopes.put("teacherId", String.valueOf(entry.getTeacher().getUserId()));
                        User user = fosUtils.getUserInfo(entry.getStudent().getUserId(), true);
                        bodyScopes.put("userPojo", new Gson().toJson(entry.getStudent()));
                        bodyScopes.put("userEmail", entry.getStudent().getEmail());
                        bodyScopes.put("callingUserId", "0");
                        bodyScopes.put("action", LeadSquaredAction.POST_LEAD_DATA.name());
                        bodyScopes.put("dataType", LeadSquaredDataType.LEAD_SUBSCRIPTION_REQUEST.name());
                        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_SUBSCRIPTION_REQUEST,
                                user, bodyScopes, user.getId());
                        // leadSquaredManager.executeAsyncTask(request);
                        requests.add(request);
                    } catch (Exception ex) {
                        if(ArrayUtils.isNotEmpty(requests)) {
                            leadSquaredManager.executeBulkAsyncTask(requests);
                        }
                        logger.info("Exeception occured ",ex);
                    }
                }
                if(ArrayUtils.isNotEmpty(requests)){
                    leadSquaredManager.executeBulkAsyncTask(requests);
                }
            }
        } catch (Exception ex) {
            logger.error("updateLeadActivitesError occured - ", ex);
        }
        logger.info("updateLeadActivites");
    }

    public static void setNoScheduleString(SubscriptionRequestInfo info, Long callingUserId) {
        if (CollectionUtils.isEmpty(info.getScheduleSlotInfos())) {
            if ((callingUserId != null) && (callingUserId.equals(info.getStudentId()))) {
                info.setNoScheduleText(NO_SCHEDULE_STRING_STUDENT);
            } else {
                info.setNoScheduleText(NO_SCHEDULE_STRING_TEACHER);
            }
        }
    }
}
