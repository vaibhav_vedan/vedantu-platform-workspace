/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;

import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.request.GetBundlesReq;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.response.InstalmentDues;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.OrderItemInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.enums.dinero.InstalmentDueEmailType;
import com.vedantu.platform.managers.onetofew.OTFAsyncTaskManager;
import com.vedantu.platform.managers.onetofew.OTFBundleManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.request.dinero.GetOrderInfoReq;
import com.vedantu.platform.request.dinero.SendInstalmentEmailReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.CancelSubscriptionRequest;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

/**
 *
 * @author ajith
 */
@Service
public class PaymentAsyncManager {

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private BundleManager bundleManager;



    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    
    @Autowired
    private OTFAsyncTaskManager otfAsyncTaskManager;
    
    @Autowired
    private OTFBundleManager otfBundleManager;

    private String ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT")+"/";

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentAsyncManager.class);

    public PaymentAsyncManager() {
    }

    @Async
    public void checkInstalmentDues(int start, int size) throws VException {
        logger.info("ENTRY " + start + ", " + size);
        List<InstalmentDues> results = getDueInstalments(start, size);
        if (ArrayUtils.isNotEmpty(results)) {
            // Assuming current time is 5:35 AM IST by adding 5 minutes because
            // due dates will be 5:30 AM IST in case of OTF
            Long currentTime = System.currentTimeMillis() + 5 * DateTimeUtils.MILLIS_PER_MINUTE;
            Long days7Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 7);
            Long days8Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 8);
            Long day2Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 2);
            Long dayMillis = new Long(DateTimeUtils.MILLIS_PER_DAY);
            Long days6Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 6);
            Long days3Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 3);
            for (InstalmentDues instalmentDue : results) {
                Long dueTime = instalmentDue.getDueTime();
                Long diff = dueTime - currentTime;
                try {
                    OrderInfo orderInfo = null;
                    InstalmentInfo instalmentInfo = null;
                    SendInstalmentEmailReq sendInstalmentEmailReq = null;
                    EntityType entityType = null;
                    String entityId = null;
                    if ((diff <= days8Millis && diff > days7Millis) || (diff <= day2Millis && diff > dayMillis)
                            || (diff < 0 && diff > -dayMillis) || (diff <= -dayMillis && diff > -day2Millis)
                            || (diff <= -days7Millis) || (diff <= -days6Millis && diff > -days7Millis) 
                            || (diff <= -day2Millis && diff > -days3Millis)) {
                        orderInfo = getOrderInfo(instalmentDue.getOrderId());
                        entityType = orderInfo.getItems().get(0).getEntityType();
                        entityId = orderInfo.getItems().get(0).getEntityId();
                        instalmentInfo = getInstalmentFromDueTime(instalmentDue.getOrderId(), dueTime);
                        sendInstalmentEmailReq = new SendInstalmentEmailReq();
                        sendInstalmentEmailReq.setAmount(instalmentInfo.getTotalAmount());
                        sendInstalmentEmailReq.setDueTime(dueTime);
                        sendInstalmentEmailReq.setEntityType(entityType);
                        if (null != entityType) {
                            switch (entityType) {
                                case OTF:
                                    ClientResponse resp = WebUtils.INSTANCE.doCall(ontofewEndpoint + "batch/" + entityId,
                                            HttpMethod.GET, null);
                                    VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                                    String jsonString = resp.getEntity(String.class);
                                    BatchInfo batchInfo = new Gson().fromJson(jsonString,
                                            BatchInfo.class);
                                    logger.info("batch info " + jsonString);
                                    String subscriptionLink = "/payment-details/OTF_BATCH/"+batchInfo.getBatchId()+"/"+batchInfo.getCourseInfo().getId();
                                    sendInstalmentEmailReq.setSubscriptionTitle(batchInfo.getCourseInfo().getTitle());
                                    sendInstalmentEmailReq.setSubscriptionLink(subscriptionLink);
                                    sendInstalmentEmailReq.setUserId(orderInfo.getUserId());
                                    break;
                                case COURSE_PLAN:
                                    String coursePlanId = orderInfo.getItems().get(0).getEntityId();
                                    CoursePlanBasicInfo coursePlanBasicInfo=coursePlanManager
                                            .getCoursePlanBasicInfo(coursePlanId, false, true);
                                    sendInstalmentEmailReq.setStudentId(coursePlanBasicInfo.getStudentId());
                                    sendInstalmentEmailReq.setTeacherId(coursePlanBasicInfo.getTeacherId());
                                    sendInstalmentEmailReq.setSubscriptionId(coursePlanId);
                                    sendInstalmentEmailReq.setSubscriptionTitle(coursePlanBasicInfo.getTitle());
                                    break;
                                case OTM_BUNDLE:
                                    OTFBundleInfo bundleInfo = otfBundleManager.getOTFBundle(entityId);
                                    String subscriptionLink1 = "/mysubscription";
                                    sendInstalmentEmailReq.setSubscriptionTitle(bundleInfo.getTitle());
                                    sendInstalmentEmailReq.setSubscriptionLink(subscriptionLink1);
                                    sendInstalmentEmailReq.setUserId(orderInfo.getUserId());
                                    break;
                                case BUNDLE:
                                    GetBundlesReq req = new GetBundlesReq();
                                    List<String> ids = new ArrayList<>();
                                    ids.add(entityId);
                                    req.setBundleIds(ids);
                                    BundleInfo bundle = bundleManager.getBundleName(req);
                                    String subscriptionLink2 = "/payment-details/BUNDLE/"+bundle.getId()+"/";
                                    sendInstalmentEmailReq.setSubscriptionTitle(bundle.getTitle());
                                    sendInstalmentEmailReq.setSubscriptionLink(subscriptionLink2);
                                    sendInstalmentEmailReq.setUserId(orderInfo.getUserId());
                                    break;
                                default:
                                    Long subscriptionId = orderInfo.getContextId();
                                    SubscriptionResponse subscriptionBasicInfo = subscriptionManager.getSubscriptionBasicInfo(subscriptionId);
                                    sendInstalmentEmailReq.setStudentId(subscriptionBasicInfo.getStudentId());
                                    sendInstalmentEmailReq.setTeacherId(subscriptionBasicInfo.getTeacherId());
                                    sendInstalmentEmailReq.setSubscriptionId(subscriptionId.toString());
                                    sendInstalmentEmailReq.setSubscriptionTitle(subscriptionBasicInfo.getTitle());
                                    break;
                            }
                        }

                        logger.info("OrderId is " + instalmentDue.getOrderId());
                        logger.info("Due time is " + new Date(instalmentDue.getDueTime()));
                        logger.info("diff is " + diff);
                    }
                    if (diff <= days8Millis && diff > days7Millis) {
                        if (null != entityType) // before 7 days
                        {
                            switch (entityType) {
                                case OTM_BUNDLE:
                                case OTF:
                                case BUNDLE:
                                    sendInstalmentEmailReq
                                            .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_7DAYS_LEFT_OTF);
                                    break;
                                default:
                                    sendInstalmentEmailReq
                                            .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_7DAYS_LEFT);
                                    break;
                            }
                        }
                        sendEmail(sendInstalmentEmailReq);
                    } else if (diff <= day2Millis && diff > dayMillis) {
                        if (null != entityType) // before 1 day
                        {
                            switch (entityType) {
                                case OTM_BUNDLE:
                                case OTF:
                                case BUNDLE:
                                    sendInstalmentEmailReq
                                            .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_1DAY_LEFT_OTF);
                                    break;
                                default:
                                    sendInstalmentEmailReq
                                            .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_1DAY_LEFT);
                                    break;
                            }
                        }
                        sendEmail(sendInstalmentEmailReq);

                    } else if (diff < 0 && diff > -dayMillis) {// block
                        // sendInstalmentEmailReq.setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_OTF_BLOCKED);
                        // endBatchEnrolment(orderInfo);
                        // sendEmail(sendInstalmentEmailReq);
                    } else if (diff <= -dayMillis && diff > -day2Millis) {// after
                        // due time
                        if (EntityType.PLAN.equals(entityType)||EntityType.COURSE_PLAN.equals(entityType)) {
                            sendInstalmentEmailReq
                                    .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_1DAY_PASSED);
                            sendEmail(sendInstalmentEmailReq);
                        }else if (EntityType.OTF.equals(entityType)) {
                            //TODO
                        }
                    } else if (diff <= -day2Millis && diff > -days3Millis){
                        if (EntityType.OTF.equals(entityType) || EntityType.OTM_BUNDLE.equals(entityType)|| EntityType.BUNDLE.equals(entityType)) {
                            sendInstalmentEmailReq
                                    .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_OTF_DUE_2DAYS_PASSED);
                            sendEmail(sendInstalmentEmailReq);
                        }
                    }else if (diff <= -days6Millis && diff > -days7Millis){
                        if (EntityType.OTF.equals(entityType) || EntityType.OTM_BUNDLE.equals(entityType) || EntityType.BUNDLE.equals(entityType)) {
                            sendInstalmentEmailReq
                                    .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_OTF_FINAL_REMINDER);
                            sendEmail(sendInstalmentEmailReq);
                        }
                    }else if (diff <= -days7Millis) {
                        // end subscription
                        if (EntityType.PLAN.equals(entityType)) {
                            sendInstalmentEmailReq.setCommunicationType(
                                    InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_SUBSCRIPTION_ENDED);
                            CancelSubscriptionResponse cancelSubscriptionResponse = endSubscription(orderInfo);
                            if (cancelSubscriptionResponse != null) {
                                sendInstalmentEmailReq.setRefundAmount(cancelSubscriptionResponse.getRefundAmount());
                            }
                            sendEmail(sendInstalmentEmailReq);
                        }else if(EntityType.COURSE_PLAN.equals(entityType)){
//                            logger.error("Instalment not paid for courseplan "
//                                    +entityId+" but end logic is not defined, consult product asap");
                        }else if (EntityType.OTF.equals(entityType)) {
                            if(!ArrayUtils.isEmpty(orderInfo.getPurchasingEntities()) && orderInfo.getPurchasingEntities().get(0).getDeliverableId() != null) {
                                otfAsyncTaskManager.markEnrollmentStatus(orderInfo.getUserId().toString(), entityId,
                                        "INACTIVE", Role.STUDENT, orderInfo.getId(),orderInfo.getPurchasingEntities().get(0).getDeliverableId(),true);
                            }
                            sendInstalmentEmailReq
                                    .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_OTF_BLOCKED);
                            sendEmail(sendInstalmentEmailReq);
                        }else if (EntityType.OTM_BUNDLE.equals(entityType) || EntityType.OTM_BUNDLE_REGISTRATION.equals(entityType)){
                            //
                            // Add Logic to mark all enrollments inactives
                            otfBundleManager.markEnrollmentStatus(orderInfo.getUserId(), entityId, EntityStatus.INACTIVE,orderInfo.getId());
                            sendInstalmentEmailReq
                                    .setCommunicationType(InstalmentDueEmailType.INSTALMENT_PAYMENT_DUE_OTF_BLOCKED);
                            sendEmail(sendInstalmentEmailReq);
                        }else if (EntityType.BUNDLE.equals(entityType)) {
                            if(!ArrayUtils.isEmpty(orderInfo.getPurchasingEntities()) && orderInfo.getPurchasingEntities().get(0).getDeliverableId() != null) {
                                bundleManager.processStatusUpdate(entityId, orderInfo.getUserId().toString(), orderInfo.getId(),orderInfo.getPurchasingEntities().get(0).getDeliverableId());
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("error in processing instalment dues for  " + instalmentDue.getOrderId() + ", error ",
                            e);
                }
            }
            if (results.size() == size) {
                logger.info("fetching more instalment dues ");
                checkInstalmentDues((size + start), size);
            }
        }
    }

    private List<InstalmentDues> getDueInstalments(int start, int size) throws VException {
        logger.info("ENTRY " + start + ", " + size);
        Gson gson = new Gson();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/getDueInstalments?start=" + start + "&size=" + size, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<InstalmentDues>>() {
        }.getType();
        List<InstalmentDues> results = gson.fromJson(jsonString, listType);
        logger.info("EXIT size: " + results.size());
        return results;
    }

    private OrderInfo getOrderInfo(String orderId) throws VException {
        logger.info("ENTRY " + orderId);
        Gson gson = new Gson();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        GetOrderInfoReq getOrderInfoReq = new GetOrderInfoReq();
        getOrderInfoReq.setOrderId(orderId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrderInfo", HttpMethod.POST,
                gson.toJson(getOrderInfoReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        OrderInfo orderInfo = gson.fromJson(jsonString, OrderInfo.class);
        logger.info("EXIT " + orderInfo);
        return orderInfo;
    }

    private InstalmentInfo getInstalmentFromDueTime(String orderId, Long dueTime) throws VException {
        logger.info("ENTRY " + orderId + " time " + dueTime);
        Gson gson = new Gson();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = dineroEndpoint + "/payment/getInstalmentFromDueTime?orderId=" + orderId + "&dueTime=" + dueTime;
        logger.info(url);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        InstalmentInfo instalmentInfo = gson.fromJson(jsonString, InstalmentInfo.class);
        logger.info("EXIT " + instalmentInfo);
        return instalmentInfo;
    }

    private void sendEmail(SendInstalmentEmailReq sendInstalmentEmailReq) {
        logger.info("ENTRY " + sendInstalmentEmailReq);
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("request", sendInstalmentEmailReq);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.INSTALMENT_DUE_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.error("Error in sendEmail for instalment due SubscriptionId " + sendInstalmentEmailReq + " error "
                    + e.getMessage());
        }
    }

    private CancelSubscriptionResponse endSubscription(OrderInfo orderInfo)
            throws VException, UnsupportedEncodingException, AddressException {
        String orderId = orderInfo.getId();
        logger.info("ENTRY " + orderId);
        OrderItemInfo orderItem = orderInfo.getItems().get(0);
        CancelSubscriptionResponse cancelResp = null;
        // TODO we should not end the whole subscription, instead end only the
        // subscription details associated
        Long subscriptionId = orderInfo.getContextId();
        if (subscriptionId == null) {
            throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND, "subscriptionId is null");
        }
        if (EntityType.PLAN.equals(orderItem.getEntityType())) {
            CancelSubscriptionRequest cancelSubscriptionRequest = new CancelSubscriptionRequest();
            cancelSubscriptionRequest.setRefundReason(com.vedantu.util.Constants.NON_PAYMENT_OF_INSTALMENT_DUES);
            cancelSubscriptionRequest.setRefundInfoCall(Boolean.FALSE);
            cancelSubscriptionRequest.setSubscriptionId(subscriptionId);
            cancelSubscriptionRequest.setCallingUserId(CancelSubscriptionRequest.Constants.CALLING_USER_ID_SYSTEM);
            logger.info("cancelling subscription " + cancelSubscriptionRequest);
            cancelResp = subscriptionManager.cancelSubscription(cancelSubscriptionRequest);
        }
        logger.info("EXIT " + cancelResp);
        return cancelResp;
    }

}
