package com.vedantu.platform.managers.scheduling;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;

import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.scheduling.request.session.*;
import com.vedantu.scheduling.response.session.*;
import com.vedantu.util.enums.*;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.FeatureSource;
import com.vedantu.User.IPhoneNumber;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.LoginTokenContext;
import com.vedantu.User.request.ConnectSessionCallReq;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.User.response.ConnectSessionCallRes;
import com.vedantu.User.response.CreateUserAuthenticationTokenRes;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.onetofew.pojo.GetOTFUpcomingSessionReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.KeyValuePairDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.managers.aws.AwsEC2Manager;
import com.vedantu.platform.managers.aws.AwsSNSManager;
import com.vedantu.platform.managers.click2call.ExotelPhoneCallManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.managers.wave.BillingManager;
import com.vedantu.platform.mongodbentities.click2call.PhoneCallMetadata;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.scheduling.SendTrialReminderReq;
import com.vedantu.platform.pojo.scheduling.SessionParticipantsId;
import com.vedantu.platform.pojo.subscription.UnlockHoursRequest;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.scheduling.pojo.TotalSessionDuration;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.scheduling.pojo.session.GTMGTTSessionDetails;
import com.vedantu.scheduling.pojo.session.GetSessionRescheduleRes;
import com.vedantu.scheduling.pojo.session.RescheduleData;
import com.vedantu.scheduling.pojo.session.Session;
import com.vedantu.scheduling.pojo.session.SessionAttendee;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.pojo.session.SessionRescheduleInfo;
import com.vedantu.session.pojo.CumilativeRating;
import com.vedantu.session.pojo.EntityRatingInfo;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSortType;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.EntityType;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.subscription.TrialSessionInfos;
import java.net.URISyntaxException;
import java.util.Arrays;
import org.dozer.DozerBeanMapper;

@Service
public class SessionManager {

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private ReferralManager referralManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private UserManager userManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private ExotelPhoneCallManager exotelPhoneCallManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private AwsEC2Manager awsEC2Manager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    BillingManager billingManager;

    @Autowired
    private CumilativeRatingDao cumilativeRatingDao;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionManager.class);

    private String SCHEDULING_ENDPOINT;

    @Autowired
    SubscriptionManager subscriptionManager;

    @Autowired
    CalendarManager calendarManager;

    @Autowired
    RequestCallbackManager requestCallbackManager;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    KeyValuePairDAO keyValuePairDAO;

    @Autowired
    CoursePlanManager coursePlanManager;

    private static Gson gson = new Gson();

    @Autowired
    private DozerBeanMapper mapper;

    public final static Long launchOTORecorderCronRunTime = 5 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    private final static String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private static final Type sessionInfoListType = new TypeToken<List<SessionInfo>>() {
    }.getType();

    private static final Type sessionAttendeeListType = new TypeToken<List<SessionAttendee>>() {
    }.getType();

    private static int LATEST_SESSION_CACHE_EXPIRY_MINUTE = 30;

    private ArrayList<Long> recorderStudentIds = new ArrayList<>(Arrays.asList(
            4102377012048975l,
            4102383022330929l,
            4102379058467737l,
            6274448322199552l,
            4102376895639827l,
            4664250688077824l,
            4102380010387977l,
            5939791415214080l,
            4102377401280221l,
            6014541861224448l));

    @PostConstruct
    public void init() {
        SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    }

    public SessionInfo createSession(SessionScheduleReq sessionScheduleReq) throws VException {
        String createSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.create");
        ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
                gson.toJson(sessionScheduleReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, false);
        return sessionInfo;
    }

    public CreateDemoSessionRes createDemoSession(CreateDemoSessionReq createDemoSessionReq) throws VException, IOException, URISyntaxException {
        SessionScheduleReq sessionScheduleReq = (SessionScheduleReq) createDemoSessionReq;
        String createSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.create");
        ClientResponse resp = WebUtils.INSTANCE.doCall(createSessionUrl, HttpMethod.POST,
                gson.toJson(sessionScheduleReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, false);
        CreateDemoSessionRes res = new CreateDemoSessionRes(sessionInfo);
        if (createDemoSessionReq.isCreateStudentJoinLink()) {
            CreateUserAuthenticationTokenRes createUserAuthenticationTokenRes = userManager.createUserAuthenticationToken(sessionInfo.getStudentIds().get(0), LoginTokenContext.DEMO_SESSION, String.valueOf(sessionInfo.getId()), createDemoSessionReq.getCallingUserId());
            String joinLink = "/joinDemoSession/" + String.valueOf(sessionInfo.getId()) + "/" + createUserAuthenticationTokenRes.getLoginToken();
            res.setStudentJoinLink(joinLink);
        }
        return res;
    }

    public List<SessionInfo> createMultipleSessions(MultipleSessionScheduleReq multipleSessionScheduleReq)
            throws VException {
        String createMultipleSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.createMultiple");
        ClientResponse resp = WebUtils.INSTANCE.doCall(createMultipleSessionsUrl, HttpMethod.POST,
                gson.toJson(multipleSessionScheduleReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        populateUserBasicInfos(sessionInfos, false);
        return sessionInfos;
    }

    public PlatformBasicResponse rescheduleSession(RescheduleSessionReq rescheduleSessionReq) throws VException {
        String rescheduleSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.reschedule");
        ClientResponse resp = WebUtils.INSTANCE.doCall(rescheduleSessionUrl, HttpMethod.POST,
                gson.toJson(rescheduleSessionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, true);

        // email
        List<UserSessionInfo> attendees = sessionInfo.getAttendees();
        for (UserSessionInfo attendee : attendees) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("sessionInfo", sessionInfo);
            payload.put("communicationType", CommunicationType.SESSION_RESCHEDULE);
            payload.put("emailRecipientUser", attendee);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SESSION_RELATED_EMAIL, payload);
            asyncTaskFactory.executeTask(params);
        }

        // leadsquared task
        Map<String, String> params = new HashMap<>();
        Long studentId = sessionInfo.getStudentIds().get(0);
        params.put("teacherId", String.valueOf(sessionInfo.getTeacherId()));
        params.put("sessionState", SessionState.SCHEDULED.name());
        params.put("studentId", studentId.toString());
        params.put("sessionStartTime", String.valueOf(sessionInfo.getStartTime()));
        params.put("sessionId", String.valueOf(sessionInfo.getId()));
        params.put("sessionModel", String.valueOf(sessionInfo.getModel()));
        User student = fosUtils.getUserInfo(studentId, true);
        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                LeadSquaredDataType.LEAD_ACTIVITY_SESSION, student, params, studentId);
        leadSquaredManager.executeAsyncTask(request);

        return new PlatformBasicResponse();
    }

    public SessionInfo joinSession(JoinSessionReq joinSessionReq) throws VException {
        String joinSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.join");
        ClientResponse resp = WebUtils.INSTANCE.doCall(joinSessionUrl, HttpMethod.POST, gson.toJson(joinSessionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, false);
        return sessionInfo;
    }

    public SessionInfo joinSessionFromNode(JoinSessionReq joinSessionReq) throws VException {
        String joinSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.join.from.node");
        ClientResponse resp = WebUtils.INSTANCE.doCall(joinSessionUrl, HttpMethod.POST, gson.toJson(joinSessionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, false);
        return sessionInfo;
    }

    public PlatformBasicResponse endSession(EndSessionReq endSessionReq, boolean makeAuthCheck) throws VException, JSONException {
        String joinSessionUrl = SCHEDULING_ENDPOINT + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.end");
        if (!makeAuthCheck) {
            joinSessionUrl = SCHEDULING_ENDPOINT
                    + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.end.without.auth.check");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(joinSessionUrl, HttpMethod.POST, gson.toJson(endSessionReq));
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        } catch (BadRequestException ex) {
            logger.warn("End session - bad request exception:" + ex.toString());
            return new PlatformBasicResponse();
        }

        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, true);
        String studentEmail = null;
        for (UserSessionInfo userInfo : sessionInfo.getAttendees()) {
            if (Role.STUDENT.equals(userInfo.getRole())) {
                studentEmail = userInfo.getEmail();
            }
        }

        if (SessionState.ENDED.equals(sessionInfo.getState()) && sessionInfo.getStartedAt() != null
                && sessionInfo.getStartedAt() > 0 && sessionInfo.getEndedAt() != null && sessionInfo.getEndedAt() > 0) {
            long duration = sessionInfo.getEndedAt() - sessionInfo.getStartedAt();
            if (duration > 0) {
                try {
                    redisDAO.increment(SessionManager.getCacheKey(), duration);
                } catch (Exception ex) {
                    // Not throwing error because it will updated by the cron
                    logger.info("Error incrementing session count redis cache - Ex:" + ex.getMessage() + " ex:"
                            + ex.toString());
                }
            }
        }

        Map<String, String> params = new HashMap<>();
        Long studentId = sessionInfo.getStudentIds().get(0);
        params.put("teacherId", String.valueOf(sessionInfo.getTeacherId()));
        params.put("studentId", String.valueOf(studentId));
        params.put("sessionState", SessionState.ENDED.name());
        params.put("sessionStartTime", String.valueOf(sessionInfo.getStartTime()));
        params.put("sessionId", String.valueOf(sessionInfo.getId()));
        params.put("userEmail", studentEmail);
        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                LeadSquaredDataType.LEAD_TOS_DONE, null, params, null);
        leadSquaredManager.executeAsyncTask(request);

        Map<String, Object> esPayload = new HashMap<>();
        esPayload.put("session", sessionInfo);
        AsyncTaskParams esParams = new AsyncTaskParams(AsyncTaskName.ES_UPDATE_SESSION_DATA, esPayload);
        asyncTaskFactory.executeTask(esParams);

        JSONObject sessionDetails = new JSONObject();
        sessionDetails.put("sessionId", String.valueOf(sessionInfo.getId()));
        awsSNSManager.triggerAsyncSNS(SNSTopic.SESSION_DELAYED_EVENTS, "SESSION_ENDED", sessionDetails.toString(),
                400000L);

        SessionBillingDetails session = new SessionBillingDetails(sessionInfo);

        /*
		Map<String, Object> payload = new HashMap<>();
		payload.put("session", session);
		Random random = new Random();
		Integer payoutDelay = ConfigUtils.INSTANCE.getIntValue("session.payout.delayMillis");
		AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.CALCULATE_SESSION_PAYOUT, payload,
				new Long(random.nextInt(payoutDelay)));
		asyncTaskFactory.executeTask(asyncTaskParams);
         */
        awsSQSManager.sendToSQS(SQSQueue.OTO_POSTSESSION_OPS, SQSMessageType.CALCULATE_SESSION_PAYOUT_POST_OTO_SESSION, new Gson().toJson(session));

        // referral
        referralManager.processReferralBonusAsync(ReferralStep.FIRST_SESSION, session.getStudentId());

        // post session emails
//		List<UserSessionInfo> attendees = sessionInfo.getAttendees();
//		for (UserSessionInfo attendee : attendees) {
//			Map<String, Object> payload2 = new HashMap<>();
//			payload2.put("sessionInfo", sessionInfo);
//			payload2.put("communicationType", CommunicationType.SESSION_POST);
//			payload2.put("emailRecipientUser", attendee);
//			AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.SESSION_RELATED_EMAIL, payload2);
//			asyncTaskFactory.executeTask(params2);
//		}
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse triggerPayoutQueueEvent(Long sessionId) throws VException {
        Session session = getSessionById(sessionId);
        if (session == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "session not found with id " + sessionId);
        }
        SessionInfo info = mapper.map(session, SessionInfo.class);
        SessionBillingDetails billingDetails = new SessionBillingDetails(info);
        awsSQSManager.sendToSQS(SQSQueue.OTO_POSTSESSION_OPS, SQSMessageType.CALCULATE_SESSION_PAYOUT_POST_OTO_SESSION, new Gson().toJson(billingDetails));
        return new PlatformBasicResponse();
    }

    public void expireSession(SessionInfo session) throws VException, JSONException {

        // Checking if the session should be marked Active 
//                if(SessionState.STARTED.equals(session.getState())){
//                    Boolean active = true;
//                    List<UserSessionInfo> userSessionInfos = session.getAttendees();
//
//                    for(UserSessionInfo userSessionInfo: userSessionInfos) {
//                        if(!SessionUserState.JOINED.equals(userSessionInfo.getUserState())) {
//                            active = false;
//                        }
//                    }
//
//                    if(active) {
//                       SessionBillingDetails sessionBillingDetails = new SessionBillingDetails(session);
//                       Long billingDuration = billingManager.getBillingDuration(sessionBillingDetails);
//                       if(billingDuration!=null && billingDuration!= 0l){
//                           MarkSessionActiveReq markSessionActiveReq = new MarkSessionActiveReq();
//                           markSessionActiveReq.setSessionId(session.getId());
//                           markSessionActive(markSessionActiveReq);
//                           return;
//                       }
//                    }
//                }
        // Unlock amount
        try {
            long duration = (session.getEndTime() - session.getStartTime());
            List<UnlockHoursRequest> reqs = new ArrayList<>();
            UnlockHoursRequest req = new UnlockHoursRequest(session.getSubscriptionId(), duration, session.getId());
            reqs.add(req);
            if (session.getSubscriptionId() != null && session.getSubscriptionId() > 0) {
                subscriptionManager.unlockHours(reqs);
            } else if (com.vedantu.session.pojo.EntityType.COURSE_PLAN.equals(session.getContextType())
                    && !SessionModel.TRIAL.equals(session.getModel())) {
                req.setContextType(com.vedantu.session.pojo.EntityType.COURSE_PLAN);
                req.setContextId(session.getContextId());
                coursePlanManager.unlockHours(reqs);
            }
        } catch (Exception e) {
            logger.error(e);
        }

        // Unblock calendar
        try {
            calendarManager.unmarkCalendarEntries(new AddCalendarEntryReq(session.getTeacherId().toString(),
                    session.getStartTime(), session.getEndTime(), CalendarEntrySlotState.SESSION,
                    CalendarReferenceType.OTO_SESSION, String.valueOf(session.getId())));
            calendarManager.unmarkCalendarEntries(new AddCalendarEntryReq(session.getStudentIds().get(0).toString(),
                    session.getStartTime(), session.getEndTime(), CalendarEntrySlotState.SESSION,
                    CalendarReferenceType.OTO_SESSION, String.valueOf(session.getId())));
        } catch (Exception e) {
            logger.error(e);
        }

        // Mark the session expired
        session.setState(SessionState.EXPIRED);
        markSessionExpired(session.getId());
        JSONObject sessionDetails = new JSONObject();
        sessionDetails.put("sessionId", String.valueOf(session.getId()));
        awsSNSManager.triggerAsyncSNS(SNSTopic.SESSION_DELAYED_EVENTS, "SESSION_ENDED", sessionDetails.toString(),
                400000L);

        // Send FMAT emails
        try {
            if (FeatureSource.FMAT.equals(session.getSessionSource())) {
                // Send out an email that FMAT session is ended.
                communicationManager.sendFMATEndSessionMail(session, "SESSION_EXPIRE");
            }
        } catch (Exception ex) {
            logger.error("Error sending inactive session mail:" + ex.getMessage());
        }

        // Expired session
        try {
            communicationManager.sendInactiveSessionEmail(session);
        } catch (Exception ex) {
            logger.error("Error sending inactive session mail:" + ex.getMessage());
        }
    }

    public SessionInfo cancelSession(EndSessionReq endSessionReq) throws VException {
        String joinSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.cancel");
        ClientResponse resp = WebUtils.INSTANCE.doCall(joinSessionUrl, HttpMethod.POST, gson.toJson(endSessionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, false);
        return sessionInfo;
    }

    public SessionInfo markSessionActive(MarkSessionActiveReq markSessionActiveReq) throws VException {
        String markSessionActiveUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.markActive");
        ClientResponse resp = WebUtils.INSTANCE.doCall(markSessionActiveUrl, HttpMethod.POST,
                gson.toJson(markSessionActiveReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);

        // Populate user basic infos
        populateUserBasicInfos(sessionInfo, false);
        return sessionInfo;
    }

    public SessionInfo setVimeoVideoId(Long sessionId, SetVimeoVideoIdRequest setVimeoVideoIdRequest) throws VException {
        setVimeoVideoIdRequest.verify();

        String setVimeoVideoIdUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.setVimeoVideoId") + "/" + String.valueOf(sessionId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(setVimeoVideoIdUrl, HttpMethod.POST,
                gson.toJson(setVimeoVideoIdRequest));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);
        return sessionInfo;
    }

    public SessionInfo getSessionById(Long id) throws VException {
        return getSessionById(id, true, false);
    }

    public GetSessionRescheduleRes getSessionRescheduleDetails(Long id) throws VException {
        SessionInfo sessionInfo = getSessionById(id, true, false);
        GetSessionRescheduleRes getSessionRescheduleRes = new GetSessionRescheduleRes(sessionInfo);
        if (!CollectionUtils.isEmpty(sessionInfo.getRescheduleData())) {
            Set<Long> userIds = sessionInfo.getRescheduleData().stream().map(RescheduleData::getRescheduledBy)
                    .filter(Objects::nonNull).collect(Collectors.toSet());
            Map<Long, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            for (RescheduleData r : sessionInfo.getRescheduleData()) {
                SessionRescheduleInfo sessionRescheduleInfo = mapper.map(r, SessionRescheduleInfo.class);
                if (sessionRescheduleInfo.getRescheduledBy() != null) {
                    sessionRescheduleInfo.setRescheduledByInfo(userInfoMap.get(sessionRescheduleInfo.getRescheduledBy()));
                }
                getSessionRescheduleRes.addRescheduleData(sessionRescheduleInfo);
            }
        }

        return getSessionRescheduleRes;
    }

    public SessionInfo getSessionById(Long id, boolean includeAttendeeRatings) throws VException {
        return getSessionById(id, true, includeAttendeeRatings);
    }

    public SessionInfo getSessionById(Long id, boolean populateUserInfos, boolean includeAttendeeRatings)
            throws VException {
        String getSessionByIdUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getSessionById") + "/" + id;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionByIdUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfos = gson.fromJson(jsonString, SessionInfo.class);
        if (populateUserInfos) {
            populateUserBasicInfos(sessionInfos, includeAttendeeRatings, false);
        }
        return sessionInfos;
    }

    public List<SessionInfo> getSessions(GetSessionsReq getSessionsReq, boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getSessions") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getSessionsReq);
        logger.info("Url : " + getSessionsUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        populateUserBasicInfos(sessionInfos, exposeEmail);
        return sessionInfos;
    }

    public List<SessionInfo> getSessionsInfo(GetSessionsReq getSessionsReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getSessions") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getSessionsReq);
        logger.info("Url : " + getSessionsUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        // populateUserBasicInfos(sessionInfos);
        return sessionInfos;
    }

    @Deprecated
    public List<SessionInfo> getUserUpcomingSessions(GetUserUpcomingSessionReq getUserUpcomingSessionReq,
            boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getUpcomingSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getUserUpcomingSessions") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getUserUpcomingSessionReq);
        logger.info("Url : " + getUpcomingSessionsUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        populateUserBasicInfos(sessionInfos, exposeEmail);
        return sessionInfos;
    }

    @Deprecated
    private List<SessionInfo> getUserLatestActiveSession(GetUserLatestActiveSessionReq getUserLatestActiveSessionReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getUpcomingSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getUserLatestActiveSession") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getUserLatestActiveSessionReq);
        logger.info("Url : " + getUpcomingSessionsUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = new ArrayList<SessionInfo>();
        if (!StringUtils.isEmpty(jsonString)) {
            sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        }
        populateUserBasicInfos(sessionInfos, false);
        return sessionInfos;
    }

    @Deprecated
    public GetLatestUpComingOrOnGoingSessionRes getUserLatestSessions(GetUserUpcomingSessionReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        GetLatestUpComingOrOnGoingSessionRes res = new GetLatestUpComingOrOnGoingSessionRes();
        // Fetch OTO session
        try {
            GetUserLatestActiveSessionReq getUserLatestActiveSessionReq = new GetUserLatestActiveSessionReq();
            getUserLatestActiveSessionReq.setCallingUserId(req.getCallingUserId());
            getUserLatestActiveSessionReq.setCallingUserRole(req.getCallingUserRole());
            getUserLatestActiveSessionReq.setStart(0);
            getUserLatestActiveSessionReq.setSize(10);
            List<SessionInfo> sessions = getUserLatestActiveSession(getUserLatestActiveSessionReq);
            SessionInfo sessionInfo = null;
            if (!CollectionUtils.isEmpty(sessions)) {
                sessionInfo = sessions.get(0);
            }
            res.setSessionRes(sessionInfo);
        } catch (Exception ex) {
            // Error log will provide huge no of sentry errors. Making it info for now
            logger.info("Fetching latest oto session failed:" + ex.getMessage() + " req:" + req.toString());
        }

        // Fetch OTF session
        try {
            GetOTFUpcomingSessionReq getOTFUpcomingSessionReq = new GetOTFUpcomingSessionReq(System.currentTimeMillis(),
                    null, null, 0, 1);
            switch (req.getCallingUserRole()) {
                case TEACHER:
                    getOTFUpcomingSessionReq.setTeacherId(String.valueOf(req.getCallingUserId()));
                    break;
                case STUDENT:
                    getOTFUpcomingSessionReq.setStudentId(String.valueOf(req.getCallingUserId()));
                    break;
                default:
                    logger.info("Role invalid for the above request : " + req.toString());
                    throw new VException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user");
            }

            GetLatestUpComingOrOnGoingSessionRes otfResponse = otfManager
                    .getUpcomingSessionsForTicker(getOTFUpcomingSessionReq);
            if (otfResponse.getOtfSession() != null) {
                // Populate users
                populateUserBasicInfos(otfResponse.getOtfSession(), false, false);
            }
            if (otfResponse.getWebinarSession() != null) {
                // Populate users
                populateUserBasicInfos(otfResponse.getWebinarSession(), false, false);
            }

            res.includeResponse(otfResponse);
        } catch (Exception ex) {
            // Error log will provide huge no of sentry errors. Making it info for now
            logger.info("Fetching latest otf session failed:" + ex.getMessage() + " req:" + req.toString());
        }

        // Reset current time
        res.setCurrentSystemTime(System.currentTimeMillis());
        return res;
    }

    public List<SessionInfo> getUserCalendarSessions(GetUserCalendarSessionsReq getUserCalendarSessionsReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getUpcomingSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getUserCalendarSessions") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getUserCalendarSessionsReq);
        logger.info("Url : " + getUpcomingSessionsUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = new ArrayList<SessionInfo>();
        if (!StringUtils.isEmpty(jsonString)) {
            sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        }
        populateUserBasicInfos(sessionInfos, false);
        return sessionInfos;
    }

    @Deprecated
    public List<SessionInfo> getUserPastSessions(GetUserPastSessionReq getUserPastSessionReq,
            boolean exposeEmail)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getPastSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getUserPastSessions") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getUserPastSessionReq);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getPastSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        populateUserBasicInfos(sessionInfos, exposeEmail);
        return sessionInfos;
    }

    public GetSessionBillDetailsRes getSessionBillDetails(GetSessionBillDetailsReq getSessionBillDetailsReq)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getSessionBillDetailsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getSessionBillDetails") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(getSessionBillDetailsReq);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionBillDetailsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        GetSessionBillDetailsRes response = gson.fromJson(jsonString, GetSessionBillDetailsRes.class);
        return response;
    }

    public long getOTOTotalSessionHourDuration(long afterEndTime, long beforeEndTime) throws VException {
        logger.info("Request - afterEndTime: " + afterEndTime + " beforeEndTime:" + beforeEndTime);
        String getTotalSessionDurationUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getTotalSessionDuration")
                + "?afterEndTime=" + afterEndTime + "&beforeEndTime=" + beforeEndTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getTotalSessionDurationUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("calculatedDuration: " + jsonString);
        return Long.parseLong(jsonString);
    }

    // For ES
    public TotalSessionDuration getTotalSessionDurationForTeacher(Long userId) throws VException {
        String getTotalSessionDurationForUserUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getTotalSessionDurationForTeacher")
                + "?userId=" + userId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(getTotalSessionDurationForUserUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return new Gson().fromJson(jsonString, TotalSessionDuration.class);
    }

    public List<SessionAttendee> processSessionPayout(ProcessSessionPayoutReq req) throws VException {
        String processSessionPayoutUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.processSessionPayout");
        logger.info("Url : " + processSessionPayoutUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(processSessionPayoutUrl, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionAttendee> sessionAttendees = gson.fromJson(jsonString, sessionAttendeeListType);
        return sessionAttendees;
    }

    public void triggerPayoutSNS(Long sessionId) throws VException {
        SessionInfo sessionInfo = getSessionById(sessionId, false);
        awsSNSManager.triggerSNS(SNSTopic.SESSION_EVENTS, SessionEvents.SESSION_PAYOUT_CALCULATED.name(),
                gson.toJson(sessionInfo));
    }

    public void populateUserBasicInfos(SessionInfo sessionInfo, boolean exposeEmail) {
        populateUserBasicInfos(sessionInfo, false, exposeEmail);
    }

    private void populateUserBasicInfos(SessionInfo sessionInfo,
            boolean includeAttendeeRatings, boolean exposeEmail) {
        if (!CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
            List<UserSessionInfo> attendees = sessionInfo.getAttendees();
            Map<Long, UserSessionInfo> userMap = new HashMap<>();
            List<String> userIdsString = new ArrayList<>();
            for (UserSessionInfo attendee : attendees) {
                userMap.put(attendee.getUserId(), attendee);
                userIdsString.add(attendee.getUserId().toString());
            }

            Map<Long, UserBasicInfo> userBasicInfosMap = fosUtils.getUserBasicInfosMapFromLongIds(userMap.keySet(), exposeEmail);
            Map<String, CumilativeRating> ratingsMap = new HashMap<>();
            if (includeAttendeeRatings) {
                ratingsMap = cumilativeRatingDao.getCumulativeRatingsMap(userIdsString, EntityType.USER);
            }

            for (UserSessionInfo attendee : attendees) {
                if (userBasicInfosMap.containsKey(attendee.getUserId())) {
                    Long userId = attendee.getUserId();
                    attendee.updateUserDetails(userBasicInfosMap.get(userId));
                    if (ratingsMap.containsKey(userId.toString())) {
                        EntityRatingInfo entityRatingInfo = new EntityRatingInfo(ratingsMap.get(userId.toString()));
                        attendee.setRatingInfo(entityRatingInfo);
                    }
                    userMap.put(userId, attendee);
                } else {
                    logger.error("user info not found for " + attendee.getUserId());
                }
            }
            List<UserSessionInfo> users = new ArrayList<>();
            users.addAll(userMap.values());
            sessionInfo.setAttendees(users);
        }
    }

    private void populateUserBasicInfos(List<SessionInfo> sessionInfos, boolean exposeEmail) {
        if (!CollectionUtils.isEmpty(sessionInfos)) {
            // Collect all the userIds from attendee list
            Set<String> userIds = new HashSet<String>();
            for (SessionInfo sessionInfo : sessionInfos) {
                if (!CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        userIds.add(String.valueOf(userSessionInfo.getUserId()));
                    }
                }
            }

            // Fetch user info and update the session info
            Map<String, UserBasicInfo> userInfos = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (SessionInfo sessionInfo : sessionInfos) {
                if (!CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                    List<UserSessionInfo> users = new ArrayList<UserSessionInfo>();
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        UserBasicInfo userBasicInfo = userInfos.get(String.valueOf(userSessionInfo.getUserId()));
                        userSessionInfo.updateUserDetails(userBasicInfo);
                        logger.info("platform populateUserBasicInfos userBasicInfo : {}, userSessionInfo {} ",
                                gson.toJson(userBasicInfo), gson.toJson(userSessionInfo));
                        users.add(userSessionInfo);
                    }
                    sessionInfo.setAttendees(users);
                }
            }
        }
    }

    public void populateUserBasicInfos(OTFSessionPojoUtils sessionInfo, boolean includeAttendees, boolean exposeEmail) {
        Set<String> userIds = new HashSet<>();
        String presenter = sessionInfo.getPresenter();
        if (!StringUtils.isEmpty(presenter)) {
            userIds.add(presenter);
        }

        if (includeAttendees && !CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
            userIds.addAll(sessionInfo.getAttendees());
        }

        Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
        if (userMap != null && !userMap.isEmpty()) {
            // Populate presenter
            if (userMap.containsKey(presenter)) {
                sessionInfo.setPresenterInfo(userMap.get(presenter));
            }

            // Populate attendees
            if (includeAttendees && !CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
                List<UserBasicInfo> attendees = new ArrayList<>();
                for (String attendee : sessionInfo.getAttendees()) {
                    if (userMap.containsKey(attendee)) {
                        attendees.add(userMap.get(attendee));
                    }
                }
            }
        }
    }

    public ConnectSessionCallRes connectSessionCall(ConnectSessionCallReq req, HttpSessionData httpSessionData)
            throws VException {

        SessionInfo session = getSessionById(req.getSessionId(), false);
        if (session == null) {
            throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "session not found with id:" + req.getSessionId());
        }

        Long studentId = session.getStudentIds().get(0);
        Long teacherId = session.getTeacherId();

        if (httpSessionData == null
                || !(studentId.equals(httpSessionData.getUserId()) || teacherId.equals(httpSessionData.getUserId()))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not allowed");
        }

        // TODO: Optimize this
        User teacher = userManager.getUserById(teacherId);
        User student = userManager.getUserById(studentId);

        IPhoneNumber teacherPhoneNumberForCall = teacher == null ? null : teacher._getActivePhoneNumberForCall();

        IPhoneNumber studentPhoneNumberForCall = student == null ? null : student._getActivePhoneNumberForCall();

        if (teacherPhoneNumberForCall == null
                || (teacherPhoneNumberForCall instanceof User && StringUtils.isEmpty(teacherPhoneNumberForCall._getNumber()))) {
            throw new ConflictException(ErrorCode.TEACHER_ACTIVE_NUMBER_NOT_FOUND,
                    "teacher number [" + teacherPhoneNumberForCall + "] not found/active");
        }

        if (studentPhoneNumberForCall == null
                || (studentPhoneNumberForCall instanceof User && StringUtils.isEmpty(studentPhoneNumberForCall._getNumber()))) {
            throw new ConflictException(ErrorCode.STUDENT_ACTIVE_NUMBER_NOT_FOUND,
                    "student number[" + studentPhoneNumberForCall + "] not found/active");
        }

        if (StringUtils.isNotEmpty(teacherPhoneNumberForCall._getPhoneCode())
                && !"91".equals(teacherPhoneNumberForCall._getPhoneCode())) {
            throw new ForbiddenException(ErrorCode.TEACHER_CALL_SERVICE_NOT_ALLOWED,
                    "teacher phoneCode [" + teacherPhoneNumberForCall._getPhoneCode() + "] not allowed");
        }

        if (StringUtils.isNotEmpty(studentPhoneNumberForCall._getPhoneCode())
                && !"91".equals(studentPhoneNumberForCall._getPhoneCode())) {
            throw new ForbiddenException(ErrorCode.STUDENT_CALL_SERVICE_NOT_ALLOWED,
                    "student phoneCode [" + studentPhoneNumberForCall._getPhoneCode() + "] not allowed");
        }

        PhoneCallMetadata phoneCallMetadata = exotelPhoneCallManager.connectCall(teacherPhoneNumberForCall._getNumber(),
                studentPhoneNumberForCall._getNumber(), String.valueOf(session.getId()),
                ExotelPhoneCallManager.CALL_CONTEXT_SESSION, httpSessionData.getUserId());

        ConnectSessionCallRes res = new ConnectSessionCallRes(phoneCallMetadata.getCallSid());
        return res;
    }

    public PlatformBasicResponse sendTrialReminder(SendTrialReminderReq reqPojo) throws VException {
        List<SessionParticipantsId> userIds = reqPojo.getUserIds();
        for (SessionParticipantsId participants : userIds) {
            SessionInfo session = getLatestTrial(participants);
            if (session == null) {
                return new PlatformBasicResponse(false, "No session found", null);
            }

            try {
                communicationManager.sendTrialReminder(session);
            } catch (Exception e) {
                logger.error("sendTrialReminder", e);
            }
        }
        return new PlatformBasicResponse();
    }

    public SessionInfo getLatestTrial(SessionParticipantsId ids) throws VException {
        String getSessionByIdUrl = SCHEDULING_ENDPOINT + "/session/getLatestTrial?studentId=" + ids.getStudentId()
                + "&teacherId=" + ids.getTeacherId();
        logger.info("Url : " + getSessionByIdUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionByIdUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfos = gson.fromJson(jsonString, SessionInfo.class);
        populateUserBasicInfos(sessionInfos, false);
        return sessionInfos;
    }

    public GetSessionPartnersRes getSessionPartners(GetSessionPartnersReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String url = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getSessionPartners") + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        GetSessionPartnersRes responseScheduling = gson.fromJson(jsonString, GetSessionPartnersRes.class);
        GetSessionPartnersRes responseOTF = otfManager.getOTFSessionPartners(req);

        List<UserBasicInfo> partners = new ArrayList<>();
        if (responseScheduling != null && responseScheduling.getPartners() != null) {
            partners.addAll(responseScheduling.getPartners());
        }

        if (responseOTF != null && responseOTF.getPartners() != null) {
            partners.addAll(responseOTF.getPartners());
        }
        List<UserBasicInfo> finalPartners = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(partners)) {
            for (UserBasicInfo user : partners) {
                if (!userIds.contains(user.getUserId())) {
                    userIds.add(user.getUserId());
                    finalPartners.add(user);
                }
            }
        }
        GetSessionPartnersRes response = new GetSessionPartnersRes(finalPartners);

        return response;

    }

    public GetScheduledAndAllSessionCountRes getSessionCounts(Long userId) throws VException {
        httpSessionUtils.checkIfAllowed(userId, null, true);
        User user = fosUtils.getUserInfo(userId, false);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user " + userId + " not found");
        }
        String url = SCHEDULING_ENDPOINT + "/session/getSessionCounts?userId=" + userId + "&userRole="
                + user.getRole().name();
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        GetScheduledAndAllSessionCountRes response = gson.fromJson(jsonString, GetScheduledAndAllSessionCountRes.class);
        response.setOtfSubscriptionCount(
                (user.getOtfSubscriptionCount() == null) ? 0l : user.getOtfSubscriptionCount());
        response.setSubscriptionRequestCount(
                (user.getSubscriptionRequestCount() == null) ? 0l : user.getSubscriptionRequestCount());
        logger.info("Exit " + response);
        return response;
    }

    public boolean checkTOSSession(Long studentId) throws IllegalArgumentException, IllegalAccessException, VException {
        // We consider the no of ended sessions to conclude first session
        GetSessionsReq getSessionsReq = new GetSessionsReq();
        getSessionsReq.setStudentId(studentId);

        List<SessionState> states = new ArrayList<>();
        states.add(SessionState.ENDED);
        states.add(SessionState.ACTIVE);
        getSessionsReq.setSessionStates(states);
        getSessionsReq.setSize(5);
        List<SessionInfo> sessionInfos = getSessionsInfo(getSessionsReq);
        if (!CollectionUtils.isEmpty(sessionInfos) && sessionInfos.size() == 1) {
            return true;
        }
        return false;
    }

    public void sessionEventsOperation(SessionEvents eventType, String message) throws VException {
        SessionInfo sessionInfo = null;
        switch (eventType) {
            case SESSION_SCHEDULED:
                sessionInfo = gson.fromJson(message, SessionInfo.class);
            // break not here purposely
            case SESSION_RESCHEDULED:
            case SESSION_CANCELED:
                if (sessionInfo == null) {
                    sessionInfo = gson.fromJson(message, SessionInfo.class);
                }

                // Populate attendees
                populateUserBasicInfos(sessionInfo, true);
                Map<String, String> params = new HashMap<>();
                Long studentId = sessionInfo.getStudentIds().get(0);
                params.put("studentId", String.valueOf(studentId));
                params.put("teacherId", String.valueOf(sessionInfo.getTeacherId()));
                params.put("sessionState", sessionInfo.getState().toString());
                params.put("sessionStartTime", String.valueOf(sessionInfo.getStartTime()));
                params.put("sessionId", String.valueOf(sessionInfo.getId()));
                params.put("sessionModel", String.valueOf(sessionInfo.getModel()));
                LeadSquaredRequest req = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_ACTIVITY_SESSION, null, params, studentId);
                leadSquaredManager.executeAsyncTask(req);
                break;
            default:
                // swallow
                break;
        }
    }

    public Boolean checkFirstBookedSession(Long studentId) throws VException {
        String url = SCHEDULING_ENDPOINT + "/session/checkFirstBookedSession?studentId=" + studentId;
        logger.info("Url : " + url);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return gson.fromJson(jsonString, Boolean.class);
    }

    public void handleSessionEndingAsync() {
        logger.info("Invoking SESSION ENDING async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTO_SESSION_END_HANDLER, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void launchOTOSessionRecordersAsync() {
        logger.info("Invoking SESSION Recorders async");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.LAUNCH_OTO_SESSION_RECORDERS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void launchOTOSessionRecorders() {
        try {
            logger.info("Launching SESSION Recorders");
            Long currentTime = System.currentTimeMillis();
            logger.info("launchOTOSessionRecorders starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterStartTime = currentTime + launchOTORecorderCronRunTime;
            Long beforeStartTime = afterStartTime + launchOTORecorderCronRunTime - 1;
            logger.info("launchOTOSessionRecorders beforeStartTime " + beforeStartTime + " afterStartTime " + afterStartTime);
            List<SessionState> states = new ArrayList<>();

            states.add(SessionState.STARTED);
            states.add(SessionState.SCHEDULED);

            GetSessionsReq newreq = new GetSessionsReq();
            newreq.setAfterStartTime(afterStartTime);
            newreq.setBeforeStartTime(beforeStartTime);
            newreq.setSessionStates(states);
            List<SessionInfo> sessions = getSessions(newreq, false);
            logger.info("launchOTOSessionRecorders got sessions: " + sessions);

            for (SessionInfo session : sessions) {
                if (recorderStudentIds.contains(session.getStudentIds().get(0))) {
                    launchOTOSessionRecorder(session);
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException | VException ex) {
            logger.error("error in launchOTOSessionRecorders ", ex);
        }

    }

    public void launchOTOSessionRecorder(SessionInfo session) {
        Long timeout = session.getEndTime() - session.getStartTime();
        Long currentTime = System.currentTimeMillis();
        timeout += session.getStartTime() - currentTime;
        timeout /= 1000;
        logger.info("Launching SESSION Recorder for timeOut:" + timeout + " session:" + session);
        awsEC2Manager.createSessionRecorderEc2(session.getId().toString(), timeout);
    }

    public void handleSessionEnding()
            throws VException, JSONException, IllegalArgumentException, IllegalAccessException {
        List<SessionInfo> sessionsTobeEnded = getSessionsForEnding();
        if (!CollectionUtils.isEmpty(sessionsTobeEnded)) {
            for (SessionInfo sessionEntry : sessionsTobeEnded) {
                logger.info("Session pre update:" + sessionEntry.toString());
                releaseSessionLockedAmount(sessionEntry);
            }
        }
    }

    public GetSessionBillingDurationRes getSessionBillingDuration(Long sessionId)
            throws VException, IllegalArgumentException, IllegalAccessException {
        SessionBillingDetails sessionBillingDetails = new SessionBillingDetails(getSessionById(sessionId));
        Long duration = billingManager.getBillingDuration(sessionBillingDetails);
        GetSessionBillingDurationRes getSessionBillDetailsRes = new GetSessionBillingDurationRes(sessionId, duration);
        return getSessionBillDetailsRes;
    }

    private List<SessionInfo> getSessionsForEnding()
            throws VException, IllegalArgumentException, IllegalAccessException {
        String getSessionsUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.getSessionsForEnding");
        logger.info("Url : " + getSessionsUrl);
        ClientResponse resp = WebUtils.INSTANCE.doCall(getSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        populateUserBasicInfos(sessionInfos, false);
        return sessionInfos;
    }

    public void releaseSessionLockedAmount(SessionInfo session) throws VException, JSONException {
        logger.info("Session state:" + session.getState() + " display state:" + session.getDisplayState());

        switch (session.getDisplayState()) {
            case ACTIVE:
            case STARTED:
                if (!LiveSessionPlatformType.WIZIQ.equals(session.getLiveSessionPlatformType())) {
                    EndSessionReq req = new EndSessionReq(session.getId(), null, null, null);
                    endSession(req, false);
                } else {
                    if ((System.currentTimeMillis() - session.getEndTime()) > 3 * DateTimeUtils.MILLIS_PER_HOUR) {
                        logger.warn("More then 2hrs passed but the session is not ended from wiziq " + session.getId());
                        EndSessionReq req = new EndSessionReq(session.getId(), null, null, null);
                        endSession(req, false);
                    } else {
                        logger.info("not ending wiziq session, waiting for the webhook from wiziq server");
                    }
                }
                break;
            case EXPIRED:
                if (!LiveSessionPlatformType.WIZIQ.equals(session.getLiveSessionPlatformType())) {
                    expireSession(session);
                } else {
                    if ((System.currentTimeMillis() - session.getEndTime()) > 3 * DateTimeUtils.MILLIS_PER_HOUR) {
                        logger.info("More then 3hrs passed, marking it active now " + session.getId());
                        String url = SCHEDULING_ENDPOINT + "/session/markSessionActiveForExpiredWiziq?sessionId=" + session.getId();
                        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, null);
                        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                        String jsonString = resp.getEntity(String.class);
                        logger.info(jsonString);
                    }
                }
                break;
            default:
                String errorMessage = "Invalid state for releaseSessionLockedAmount method id:" + session.getId()
                        + " display state:" + session.getDisplayState() + " session:" + session.toString();
                logger.error(errorMessage);
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorMessage);
        }
    }

    public void markSessionExpired(Long id) throws VException {
        MarkSessionExpiredReq req = new MarkSessionExpiredReq(id);
        req.setCallingUserId(0L);
        req.setCallingUserRole(Role.ADMIN);
        String url = SCHEDULING_ENDPOINT + "/session/markSessionExpired";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
    }

    public static String getCacheKey() {
        return "Session_total_session_duration";
    }

    public RequestCallBackDetails checkForSessions(RequestCallBackDetails requestCallBackDetails, Long studentId,
            Long teacherId) {

        Long createdTime = requestCallBackDetails.getCreationTime();
        try {
            GetSessionsByCreationTime req = new GetSessionsByCreationTime(studentId, teacherId,
                    createdTime - 10 * DateTimeUtils.MILLIS_PER_MINUTE, null, SessionModel.TRIAL);
            List<Session> sessions = getSessionsByCreationTime(req);
            if (sessions.isEmpty()) {

            } else {
                Session session = sessions.get(0);
                requestCallBackDetails.setSessionId(session.getId());
                if (session.getSubscriptionId() != null) {
                    requestCallBackDetails.setSubscriptionId(session.getSubscriptionId());
                }
            }
        } catch (Exception e) {
            logger.error("UpdateLeadsSessionSubscriptionCronProcessor - checkForSessions", e);
        }

        return requestCallBackDetails;
    }

    public List<Session> getSessionsByCreationTime(GetSessionsByCreationTime req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        String url = SCHEDULING_ENDPOINT + "/session/getSessionsByCreationTime" + "?"
                + WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);

        Type sessionListType = new TypeToken<List<Session>>() {
        }.getType();
        List<Session> response = gson.fromJson(jsonString, sessionListType);
        return response;
    }

    public String updateLiveSessionPlatformDetails(UpdateLiveSessionPlatformDetailsReq req) throws VException {
        req.verify();
        String url = SCHEDULING_ENDPOINT + "/session/updateLiveSessionPlatformDetails";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        return jsonString;
    }

    public JoinOTONoNDefaultSessionRes joinOTONoNDefaultSession(JoinSessionReq joinSessionReq)
            throws VException, IOException {

        JoinOTONoNDefaultSessionRes res = new JoinOTONoNDefaultSessionRes();
        joinSessionReq.verify();
        String joinSessionUrl = SCHEDULING_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.join");
        ClientResponse resp = WebUtils.INSTANCE.doCall(joinSessionUrl, HttpMethod.POST, gson.toJson(joinSessionReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        SessionInfo sessionInfo = gson.fromJson(jsonString, SessionInfo.class);
        List<UserSessionInfo> attendees = sessionInfo.getAttendees();
        if (ArrayUtils.isNotEmpty(attendees)) {
            int count = 0;
            for (UserSessionInfo info : attendees) {
                if (com.vedantu.session.pojo.SessionAttendee.SessionUserState.JOINED.equals(info.getUserState())) {
                    count++;
                }
            }
            if (count == attendees.size()) {
                // marking it active
                MarkSessionActiveReq markSessionActiveReq = new MarkSessionActiveReq();
                markSessionActiveReq.setSessionId(sessionInfo.getId());
                String markSessionActiveUrl = SCHEDULING_ENDPOINT
                        + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.markActive");
                ClientResponse resp1 = WebUtils.INSTANCE.doCall(markSessionActiveUrl, HttpMethod.POST,
                        gson.toJson(markSessionActiveReq));
                VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
                String jsonString1 = resp1.getEntity(String.class);
                logger.info(jsonString1);

            }
        }
        // now redirecting
        String link = null;
        if (LiveSessionPlatformType.GTM.equals(sessionInfo.getLiveSessionPlatformType())
                || LiveSessionPlatformType.GTT.equals(sessionInfo.getLiveSessionPlatformType())) {
            GTMGTTSessionDetails details = (GTMGTTSessionDetails) sessionInfo.getLiveSessionPlatformDetails();
            if (Role.STUDENT.equals(joinSessionReq.getCallingUserRole())) {
                link = details.getStudentLink();
            } else if (Role.TEACHER.equals(joinSessionReq.getCallingUserRole())) {
                link = details.getTeacherLink();
            }
        }
        logger.info("redirecting to : " + link);
        res.setLink(link);
        return res;
    }

    public CheckSessionTOSRes isTOS(CheckSessionTOSReq req)
            throws VException, IOException, IllegalArgumentException, IllegalAccessException {
        req.verify();
        CheckSessionTOSRes res = new CheckSessionTOSRes();
        Session session = getSessionById(req.getSessionId(), false, false);
        List<Long> studentIds = session.getStudentIds();
        Long studentId = studentIds.get(0);// let it throw error
        GetSessionsReq newreq = new GetSessionsReq();
        List<SessionState> states = new ArrayList<>();
        states.add(SessionState.ENDED);
        newreq.setSessionStates(states);
        newreq.setStudentId(studentId);
        newreq.setStart(0);
        newreq.setSize(3);
        newreq.setIncludeAttendees(false);
        newreq.setSortType(SessionSortType.START_TIME_ASC);
        List<SessionInfo> sessions = getSessions(newreq, false);
        logger.info("sessions " + sessions);
        // logic: get ended sessions and this session should be the first ended
        // session among them
        if (sessions != null && !sessions.isEmpty()) {
            Session firstSession = sessions.get(0);
            if (req.getSessionId().equals(firstSession.getId())) {
                res.setIsTOS(true);
            }
        }
        return res;
    }

    public List<SessionInfo> getAttendeesNextSessions(String sessionId, Long userId) throws VException {
        logger.info("SessionId:" + sessionId);
        String url = SCHEDULING_ENDPOINT + "/session/getAttendeesNextSessions?sessionId="
                + sessionId + "&userId=" + userId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        List<SessionInfo> sessionInfos = gson.fromJson(jsonString, sessionInfoListType);
        return sessionInfos;
    }

    public Map<String, TrialSessionInfos> getTrialSessionInfos(Set<String> coursePlanIdsSet) throws VException {
        List<String> coursePlanIds = new ArrayList<>(coursePlanIdsSet);

        int LIMIT = 20;
        Map<String, TrialSessionInfos> trialSessionInfos = new HashMap<>();
        int listSize = coursePlanIds.size();
        for (int i = 0; i < listSize; i += LIMIT) {
            List<String> subList;
            if (listSize > i + LIMIT) {
                subList = coursePlanIds.subList(i, (i + LIMIT));
            } else {
                subList = coursePlanIds.subList(i, listSize);
            }

            String url = SCHEDULING_ENDPOINT + "/session/getCoursePlanTrialSessionInfo";
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(subList));
            String output = response.getEntity(String.class);
            if (!org.springframework.util.StringUtils.isEmpty(output)) {
                try {
                    if (response.getStatus() != 200) {
                        throw new Exception("getUserBasicInfos: getting Users' BasicInfo failed");
                    }

                    Type listType = new TypeToken<Map<String, TrialSessionInfos>>() {
                    }.getType();
                    Map<String, TrialSessionInfos> resultList = gson.fromJson(output, listType);
                    if (resultList != null && !resultList.isEmpty()) {
                        trialSessionInfos.putAll(resultList);
                    }
                } catch (Exception ex) {
                    logger.info("Exception while parsing userBasicInfos", ex);
                }
            }
        }

        return trialSessionInfos;
    }

    public PlatformBasicResponse launchOTOSessionRecorder(String sessionId, Long timeout) {
        awsEC2Manager.createSessionRecorderEc2(sessionId, timeout);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse adminSessionUpdate(AdminSessionUpdateReq adminSessionUpdateReq)
            throws VException, AddressException, UnsupportedEncodingException {
        logger.info("Request : " + adminSessionUpdateReq.toString());
        adminSessionUpdateReq.validate();
        SessionInfo sessionInfo = getSessionById(adminSessionUpdateReq.getSessionId());
        Long callingUserId = httpSessionUtils.getCallingUserId();

        if (!((sessionInfo.getStudentIds() != null && sessionInfo.getStudentIds().contains(callingUserId))
                || sessionInfo.getTeacherId().equals(callingUserId))) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for this user");
        }

        User callingUser = fosUtils.getUserInfo(callingUserId, true);
        communicationManager.sendAdminSessionUpdateEmail(adminSessionUpdateReq, callingUser, sessionInfo);
        return new PlatformBasicResponse();
    }
 

    //getUserLatestSessionCacheKey
    //LATEST_SESSION_CACHE_EXPIRY_MINUTE
    public int getExpiryTimeForLastestSessions(GetLatestUpComingOrOnGoingSessionRes res) {
        Long currentTime = System.currentTimeMillis();
        Long expiryTime = new Long(DateTimeUtils.MILLIS_PER_MINUTE * LATEST_SESSION_CACHE_EXPIRY_MINUTE);
        expiryTime += currentTime;
        if (res != null) {
            if (res.getSessionRes() != null) {
                if (res.getSessionRes().getEndTime() != null && res.getSessionRes().getEndTime() < expiryTime) {
                    expiryTime = res.getSessionRes().getEndTime();
                }
                if (res.getSessionRes().getSessionExpireTime() != null && res.getSessionRes().getSessionExpireTime() < expiryTime
                        && res.getSessionRes().getSessionExpireTime() > currentTime) {
                    expiryTime = res.getSessionRes().getSessionExpireTime();
                }
            }
            if (res.getOtfSession() != null && res.getOtfSession().getEndTime() != null) {
                if (res.getOtfSession().getEndTime() < expiryTime) {
                    expiryTime = res.getOtfSession().getEndTime();
                }
            }
            if (res.getWebinarSession() != null && res.getWebinarSession().getEndTime() != null) {
                if (res.getWebinarSession().getEndTime() < expiryTime) {
                    expiryTime = res.getWebinarSession().getEndTime();
                }
            }
        }
        int expiry = (int) ((expiryTime - currentTime) / 1000);
        if (expiry > 0) {
            return expiry;
        } else {
            return 0;
        }
    }

    @Deprecated
    public GetLatestUpComingOrOnGoingSessionRes getUserLatestSessionsRedis(GetUserUpcomingSessionReq req)
            throws IllegalArgumentException, IllegalAccessException, VException {
        GetLatestUpComingOrOnGoingSessionRes res = new GetLatestUpComingOrOnGoingSessionRes();
        req.verify();
        String cacheKey = redisDAO.getUserLatestSessionCacheKey(req.getCallingUserId());
        if (StringUtils.isNotEmpty(cacheKey)) {

            String response = null;
            try {
                response = redisDAO.get(cacheKey);
            } catch (Exception e) {
                logger.info("redis get throws exception." + cacheKey + " " + e);
            }
            if (StringUtils.isEmpty(response)) {
                res = getUserLatestSessions(req);
                int expiry = getExpiryTimeForLastestSessions(res);
                if (expiry > DateTimeUtils.SECONDS_PER_MINUTE) {
//                    if (res.getOtfSession() == null && res.getSessionRes() == null && res.getWebinarSession() == null) {
//                        expiry = DateTimeUtils.SECONDS_PER_MINUTE * 5;
//                    }                 
                    String redisValue = gson.toJson(res);
                    try {
                        redisDAO.setex(cacheKey, redisValue, expiry);
                    } catch (Exception e) {
                        logger.info("redis setex throws exception." + cacheKey + " " + e);
                    }
                }
            } else {
                res = gson.fromJson(response, GetLatestUpComingOrOnGoingSessionRes.class);
            }
        }
        res.setCurrentSystemTime(System.currentTimeMillis());
        return res;
    }

    // todo exp/segRedis deprecate this after separating redis cluster for scheduling
    public void handleSessionEventsForLatestSession(SessionEvents eventType, String message) throws InternalServerErrorException {
        SessionInfo sessionInfo = null;
        sessionInfo = gson.fromJson(message, SessionInfo.class);
        if (sessionInfo != null) {
            Long currentTime = System.currentTimeMillis();
            Long expiryTime = new Long(DateTimeUtils.MILLIS_PER_MINUTE * (LATEST_SESSION_CACHE_EXPIRY_MINUTE + 15));
            expiryTime += currentTime;
            /*if(sessionInfo.getStartTime() != null && sessionInfo.getStartTime() > expiryTime 
					&& !SessionEvents.SESSION_RESCHEDULED.equals(eventType)){
				return;
			}*/
            List<String> cacheKeys = new ArrayList<>();
            String teacher = null;
            String student = null;
            List<String> cachedKeyForSessions = new ArrayList<>();
            if (sessionInfo.getTeacherId() != null) {
                teacher = redisDAO.getUserLatestSessionCacheKey(sessionInfo.getTeacherId());
                if (StringUtils.isNotEmpty(teacher)) {
                    cacheKeys.add(teacher);
                }
                String teacherKey = redisDAO.getCachedKeyForUserLatestSessions(sessionInfo.getTeacherId().toString(), env);
                if (StringUtils.isNotEmpty(teacherKey)) {
                    cachedKeyForSessions.add(teacherKey);
                }
            }
            if (ArrayUtils.isNotEmpty(sessionInfo.getStudentIds()) && sessionInfo.getStudentIds().get(0) != null) {
                student = redisDAO.getUserLatestSessionCacheKey(sessionInfo.getStudentIds().get(0));
                if (StringUtils.isNotEmpty(student)) {
                    cacheKeys.add(student);
                }
                String studentKey = redisDAO.getCachedKeyForUserLatestSessions(sessionInfo.getStudentIds().get(0).toString(), env);
                if (StringUtils.isNotEmpty(studentKey)) {
                    cachedKeyForSessions.add(studentKey);
                }
            }
            switch (eventType) {
                case SESSION_SCHEDULED:
                case SESSION_CANCELED:
                case SESSION_FORFEITED:
                case SESSION_EXPIRED:
                case SESSION_ENDED:
                case SESSION_RESCHEDULED:
                    String[] toDelete = new String[cacheKeys.size()];
                    try {
                        redisDAO.deleteKeys(cacheKeys.toArray(toDelete));
                    } catch (Exception e) {
                        logger.info("Error in deleting Keys." + e);
                    }

                    String[] toDeleteSession = new String[cachedKeyForSessions.size()];
                    try {
                        redisDAO.deleteKeys(cachedKeyForSessions.toArray(toDeleteSession));
                    } catch (Exception ex) {
                        logger.info("Error in deleting keys: " + ex.getMessage());
                    }
                    return;
            }

            Map<String, String> redisMap = null;
            try {
                redisMap = redisDAO.getValuesForKeys(cacheKeys);
            } catch (Exception e) {
                logger.info("redis get throws exception." + e);
            }
            if (redisMap == null || redisMap.isEmpty()) {
                return;
            }
            GetLatestUpComingOrOnGoingSessionRes resStudent = null;
            GetLatestUpComingOrOnGoingSessionRes resTeacher = null;
            if (student != null && redisMap.containsKey(student)) {
                resStudent = gson.fromJson(redisMap.get(student), GetLatestUpComingOrOnGoingSessionRes.class);
            }
            if (teacher != null && redisMap.containsKey(teacher)) {
                resTeacher = gson.fromJson(redisMap.get(teacher), GetLatestUpComingOrOnGoingSessionRes.class);
            }
            Boolean saveStudent = Boolean.FALSE;
            Boolean saveTeacher = Boolean.FALSE;
            switch (eventType) {
//			case SESSION_SCHEDULED:
//				populateUserBasicInfos(sessionInfo);
//				if(resStudent != null){
//					SessionInfo session = resStudent.getSessionRes();
//					if(session == null){						
//						resStudent.setSessionRes(sessionInfo);
//						saveStudent = Boolean.TRUE;
//					}else if(session.getStartTime() > sessionInfo.getStartTime()){						
//						resStudent.setSessionRes(sessionInfo);
//						saveStudent = Boolean.TRUE;
//					}
//				}
//				if(resTeacher != null){
//					SessionInfo session = resTeacher.getSessionRes();
//					if(session == null){
//						resTeacher.setSessionRes(sessionInfo);
//						saveTeacher = Boolean.TRUE;
//					}else if(session.getStartTime() > sessionInfo.getStartTime()){
//						resTeacher.setSessionRes(sessionInfo);
//						saveTeacher = Boolean.TRUE;
//					}
//				}
//				break;
//			case SESSION_FORFEITED:
//			case SESSION_EXPIRED:
//			case SESSION_ENDED:
//			case SESSION_RESCHEDULED:
//				if(resStudent != null && resStudent.getSessionRes() != null){
//					if(resStudent.getSessionRes().getId().equals(sessionInfo.getId())){
//						saveStudent = Boolean.TRUE;
//								
//					}
//				}
//				if(resTeacher != null && resTeacher.getSessionRes() != null){
//					if(resTeacher.getSessionRes().getId().equals(sessionInfo.getId())){
//						saveTeacher =Boolean.TRUE;
//					}
//				}
//				if(!saveStudent && !saveTeacher){
//					break;
//				}
//			case SESSION_CANCELED:
//				if(resStudent != null && resStudent.getSessionRes() != null) {
//					try {
//						GetUserLatestActiveSessionReq getUserLatestActiveSessionReq = new GetUserLatestActiveSessionReq();
//						getUserLatestActiveSessionReq.setCallingUserId(resStudent.getSessionRes().getStudentIds().get(0));
//						getUserLatestActiveSessionReq.setCallingUserRole(Role.STUDENT);
//						getUserLatestActiveSessionReq.setStart(0);
//						getUserLatestActiveSessionReq.setSize(10);
//						List<SessionInfo> sessions = getUserLatestActiveSession(getUserLatestActiveSessionReq);
//						if (!CollectionUtils.isEmpty(sessions)) {
//							sessionInfo = sessions.get(0);
//						}
//						resStudent.setSessionRes(sessionInfo);
//						saveStudent = Boolean.TRUE;
//					}
//				 catch (Exception ex) {
//					// Error log will provide huge no of sentry errors. Making it info for now
//					logger.info("Fetching latest oto session failed:" + ex.getMessage());
//				}
//			}
//				if(resTeacher != null && resTeacher.getSessionRes() != null) {
//					try {
//						GetUserLatestActiveSessionReq getUserLatestActiveSessionReq = new GetUserLatestActiveSessionReq();
//						getUserLatestActiveSessionReq.setCallingUserId(resTeacher.getSessionRes().getStudentIds().get(0));
//						getUserLatestActiveSessionReq.setCallingUserRole(Role.TEACHER);
//						getUserLatestActiveSessionReq.setStart(0);
//						getUserLatestActiveSessionReq.setSize(10);
//						List<SessionInfo> sessions = getUserLatestActiveSession(getUserLatestActiveSessionReq);
//						if (!CollectionUtils.isEmpty(sessions)) {
//							sessionInfo = sessions.get(0);
//						}
//						resTeacher.setSessionRes(sessionInfo);
//						saveTeacher = Boolean.TRUE;
//					}
//					 catch (Exception ex) {
//						// Error log will provide huge no of sentry errors. Making it info for now
//						logger.info("Fetching latest oto session failed:" + ex.getMessage());
//					}
//				}
//				break;
                case SESSION_ACTIVE:
                    if (resStudent != null && resStudent.getSessionRes() != null) {
                        if (resStudent.getSessionRes().getId().equals(sessionInfo.getId())) {
                            resStudent.getSessionRes().setState(SessionState.ACTIVE);
                            saveStudent = Boolean.TRUE;
                        }
                    }
                    if (resTeacher != null && resTeacher.getSessionRes() != null) {
                        if (resTeacher.getSessionRes().getId().equals(sessionInfo.getId())) {
                            resTeacher.getSessionRes().setState(SessionState.ACTIVE);
                            saveTeacher = Boolean.TRUE;
                        }
                    }
                    break;
                case SESSION_STARTED:
                    if (resStudent != null && resStudent.getSessionRes() != null) {
                        if (resStudent.getSessionRes().getId().equals(sessionInfo.getId())
                                && sessionInfo.getStartedAt() != null) {
                            resStudent.getSessionRes().setStartedAt(sessionInfo.getStartedAt());
                            resStudent.getSessionRes().setState(SessionState.STARTED);
                            saveStudent = Boolean.TRUE;

                            if (sessionInfo.getStartedBy() != null) {
                                resStudent.getSessionRes().setStartedBy(sessionInfo.getStartedBy());
                                saveStudent = Boolean.TRUE;
                            }
                        }
                    }
                    if (resTeacher != null && resTeacher.getSessionRes() != null) {
                        if (resTeacher.getSessionRes().getId().equals(sessionInfo.getId())
                                && sessionInfo.getStartedAt() != null) {
                            resTeacher.getSessionRes().setStartedAt(sessionInfo.getStartedAt());
                            resTeacher.getSessionRes().setState(SessionState.STARTED);
                            saveTeacher = Boolean.TRUE;

                            if (sessionInfo.getStartedBy() != null) {
                                resTeacher.getSessionRes().setStartedBy(sessionInfo.getStartedBy());
                                saveTeacher = Boolean.TRUE;
                            }
                        }
                    }
                    break;
                default:
                    String[] toDelete = new String[cacheKeys.size()];
                    try {
                        redisDAO.deleteKeys(cacheKeys.toArray(toDelete));
                    } catch (Exception e) {
                        logger.info("Error in deleting Keys." + e);
                    }

                    String[] toDeleteSession = new String[cachedKeyForSessions.size()];
                    try {
                        redisDAO.deleteKeys(cachedKeyForSessions.toArray(toDeleteSession));
                    } catch (Exception ex) {
                        logger.info("Error in deleting keys: " + ex.getMessage());
                    }

                    return;
            }
            if (saveStudent) {
                String redisValue = gson.toJson(resStudent);
                int expiry = getExpiryTimeForLastestSessions(resStudent);
                if (expiry > DateTimeUtils.SECONDS_PER_MINUTE) {
                    try {
                        redisDAO.setex(student, redisValue, expiry);
                    } catch (Exception e) {
                        logger.info("redis student setex throws exception." + student + " " + e);
                    }
                }
            }
            if (saveTeacher) {
                int expiry = getExpiryTimeForLastestSessions(resTeacher);
                String redisValue = gson.toJson(resTeacher);
                if (expiry > DateTimeUtils.SECONDS_PER_MINUTE) {
                    try {
                        redisDAO.setex(teacher, redisValue, expiry);
                    } catch (Exception e) {
                        logger.info("redis teacher setex throws exception." + teacher + " " + e);
                    }
                }
            }
        }
    }


     public PlatformBasicResponse createTestLinkWithLoginToken(CreateTestLinkWithLoginTokenReq createTestLinkWithLoginTokenReq) throws VException, IOException, URISyntaxException {
         CreateUserAuthenticationTokenRes createUserAuthenticationTokenRes = userManager.createUserAuthenticationTokenForTest(createTestLinkWithLoginTokenReq.getStudentId(), LoginTokenContext.JRP_TEST_LINK, createTestLinkWithLoginTokenReq.getTestLink(),
                 createTestLinkWithLoginTokenReq.getCallingUserId());
        String url = createTestLinkWithLoginTokenReq.getOrigin()+createUserAuthenticationTokenRes.getLoginToken();
        String finalUrl = WebUtils.INSTANCE.shortenUrl(url);
        return new PlatformBasicResponse(true, finalUrl, null);
     }

    public String getSessionShortUrl(String sessionUrl) {
        return WebUtils.INSTANCE.shortenUrl(sessionUrl);
    }
}
