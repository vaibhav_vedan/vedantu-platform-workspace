/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.subscription;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.User.Role;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.pojo.courseplan.BlockCoursePlanSlotsPojo;
import com.vedantu.platform.pojo.courseplan.BookCoursePlanSessionPojo;
import com.vedantu.platform.pojo.courseplan.ResetCoursePlanSlotsPojo;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.request.MarkOrderEndedRes;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.ProcessPaymentReq;
import com.vedantu.dinero.request.RefundReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.CMDSShareContentinfo;
import com.vedantu.lms.cmds.request.CMDSShareOTFContentsReq;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.review.CumilativeRatingDao;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.dinero.PaymentManager;
import com.vedantu.platform.managers.lms.CMDSTestManager;
import com.vedantu.platform.managers.notification.CommunicationManager;
import com.vedantu.platform.managers.scheduling.SessionManager;
import com.vedantu.platform.pojo.dinero.SessionBillingDetails;
import com.vedantu.platform.pojo.leadsquared.LeadDetailsSerialized;
import com.vedantu.platform.pojo.subscription.CompleteSessionRequest;
import com.vedantu.platform.pojo.subscription.UnlockHoursRequest;
import com.vedantu.platform.pojo.subscription.UpdateConflictDurationRequest;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.request.session.EndSessionReq;
import com.vedantu.scheduling.request.session.GetSessionDurationByContextIdReq;
import com.vedantu.scheduling.request.session.GetSessionsReq;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.scheduling.request.session.SubscriptioncoursePlanCancelSessionsReq;
import com.vedantu.scheduling.response.session.ContextSessionDurationResponse;
import com.vedantu.session.pojo.CumilativeRating;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionEvents;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.subscription.enums.ReminderTimeType;
import com.vedantu.subscription.pojo.ContentInfo;
import com.vedantu.subscription.pojo.OTOCurriculumPojo;
import com.vedantu.subscription.request.AddCoursePlanHoursReq;
import com.vedantu.subscription.request.AddEditCoursePlanReq;
import com.vedantu.subscription.request.AddEditStructuredCourseReq;
import com.vedantu.subscription.request.BlockSlotScheduleReq;
import com.vedantu.subscription.request.CoursePlanAdminAlertReq;
import com.vedantu.subscription.request.CoursePlanReqFormReq;
import com.vedantu.subscription.request.CoursePlanSessionRequest;
import com.vedantu.subscription.request.EditInstalmentAmtAndSchedule;
import com.vedantu.subscription.request.EndCoursePlanReq;
import com.vedantu.subscription.request.EnrollCoursePlanReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.request.GetCoursePlanReminderListReq;
import com.vedantu.subscription.request.GetCoursePlansReq;
import com.vedantu.subscription.request.GetStructuredCoursesReq;
import com.vedantu.subscription.request.MarkCoursePlanStateReq;
import com.vedantu.subscription.request.MarkRegisteredForParentCourseReq;
import com.vedantu.subscription.request.MarkTrialPaymentDoneReq;
import com.vedantu.subscription.request.PayTrialAmountReq;
import com.vedantu.subscription.request.TransferCancelSessionHoursReq;
import com.vedantu.subscription.response.CheckIfRegFeePaidRes;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.CoursePlanHours;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.EnrollCoursePlanRes;
import com.vedantu.subscription.response.GetCoursePlanBalanceRes;
import com.vedantu.subscription.response.GetCoursePlanReminderListRes;
import com.vedantu.subscription.response.PayTrialAmountRes;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.scheduling.SchedulingUtils;
import com.vedantu.util.subscription.GetSlotConflictReq;
import com.vedantu.util.subscription.GetSlotConflictRes;
import org.dozer.DozerBeanMapper;

/**
 *
 * @author ajith
 */
@Service
public class CoursePlanManager {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private CMDSTestManager testManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private CumilativeRatingDao cumilativeRatingDao;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(CoursePlanManager.class);

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    public String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    private static final Type COURSE_PLAN_HOURS_TYPE = new TypeToken<List<CoursePlanHours>>() {
    }.getType();

    private static final Type CONTEXT_SESSION_DURATION_TYPE = new TypeToken<List<ContextSessionDurationResponse>>() {
    }.getType();

    private static final int VERIFY_COURSE_PLAN_HOURS_FETCH_SIZE = 50;

    private final Gson gson = new Gson();

    @Autowired
    private DozerBeanMapper mapper;

    public CoursePlanInfo addEditCoursePlan(AddEditCoursePlanReq addEditCoursePlanReq)
            throws VException, CloneNotSupportedException {

        CoursePlanEnums.CoursePlanState currentState = CoursePlanEnums.CoursePlanState.DRAFT;
        boolean makeConflicCheck = true;
        if (addEditCoursePlanReq.getId() != null) {
            CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanBasicInfo(addEditCoursePlanReq.getId(), false, true);
            if (coursePlanBasicInfo != null) {
                currentState = coursePlanBasicInfo.getState();
                if (CoursePlanEnums.CoursePlanState.ENROLLED.equals(currentState)) {
                    makeConflicCheck = false;// disabling it because this edit is only for compact schedule and
                    // curriculum
                }
            }
        }

        logger.info("preparing all slots to block");
        List<SessionSlot> allSlots = new ArrayList<>();

        if (currentState.order < CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE.order) {
            if (addEditCoursePlanReq.getTrialSessionSchedule() != null
                    && ArrayUtils.isNotEmpty(addEditCoursePlanReq.getTrialSessionSchedule().getSessionSlots())) {
                allSlots.addAll(addEditCoursePlanReq.getTrialSessionSchedule().getSessionSlots());
            }
        }

        if (currentState.order < CoursePlanEnums.CoursePlanState.ENROLLED.order) {
            if (addEditCoursePlanReq.getRegularSessionSchedule() != null
                    && ArrayUtils.isNotEmpty(addEditCoursePlanReq.getRegularSessionSchedule().getSessionSlots())) {
                allSlots.addAll(addEditCoursePlanReq.getRegularSessionSchedule().getSessionSlots());
            }
        }

        logger.info("checking if all the slots are available");

        SessionSchedule allSlotsSchedule = new SessionSchedule();
        allSlotsSchedule.setSessionSlots(allSlots);
        allSlotsSchedule.setStartTime(CommonCalendarUtils.getDayStartTime(allSlotsSchedule.getStartDate()));
        if (makeConflicCheck) {
            checkConflicts(allSlotsSchedule, addEditCoursePlanReq.getStudentId(), addEditCoursePlanReq.getTeacherId(),
                    addEditCoursePlanReq.getId());
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/addEditCoursePlan",
                HttpMethod.POST, gson.toJson(addEditCoursePlanReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String json = resp.getEntity(String.class);
        CoursePlanInfo coursePlanInfo = gson.fromJson(json, CoursePlanInfo.class);

        if (ArrayUtils.isNotEmpty(allSlots)) {
            logger.info("blocking the calendar");
            SQSMessageType messageType = SQSMessageType.COURSE_PLAN_BLOCK_SLOTS;
            BlockCoursePlanSlotsPojo pojo = new BlockCoursePlanSlotsPojo(allSlotsSchedule, coursePlanInfo.getStudentId(),
                    coursePlanInfo.getTeacherId(), coursePlanInfo.getId());
            awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo), coursePlanInfo.getId());
            /*Map<String, Object> payload = new HashMap<>();
            payload.put("sessionSchedule", allSlotsSchedule);
            payload.put("studentId", coursePlanInfo.getStudentId());
            payload.put("teacherId", coursePlanInfo.getTeacherId());
            payload.put("coursePlanId", coursePlanInfo.getId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BLOCK_COURSE_PLAN_SLOTS, payload);
            asyncTaskFactory.executeTask(params);*/
        }

        if (CoursePlanEnums.CoursePlanState.PUBLISHED.equals(coursePlanInfo.getState())) {
            logger.info("sending email");
            Map<String, Object> payload = new HashMap<>();
            payload.put("coursePlanInfo", coursePlanInfo);
            payload.put("type", CommunicationType.COURSE_PLAN_PUBLISHED);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.COURSE_PLAN_EMAILS, payload);
            asyncTaskFactory.executeTask(params);
        }

        // Push activity to lead squared
        if (coursePlanInfo.getStudentId() != null) {
            try {
                User student = fosUtils.getUserInfo(coursePlanInfo.getStudentId(), true);
                Map<String, String> lsParams = new HashMap<>();
                lsParams.put("userEmail", student.getEmail());
                lsParams.put("CoursePlanObject", gson.toJson(coursePlanInfo));
                LeadSquaredRequest leadReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_ACTIVITY_COURSEPLAN_UPDATE, student, lsParams, student.getId());
                leadSquaredManager.executeTask(leadReq);
            } catch (Exception ex) {
                logger.error("Error occured creating leadsquared activity for coursePlanUpdate");
            }
        }
        return coursePlanInfo;
    }

    public CoursePlanInfo getCoursePlan(String id, boolean fillDetails, boolean exposeEmail) throws VException {
        // TODO add access control
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/" + id, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CoursePlanInfo coursePlanInfo = gson.fromJson(jsonString, CoursePlanInfo.class);

        boolean hideSensitiveInfo = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || (!Role.ADMIN.equals(sessionData.getRole()))) {
            hideSensitiveInfo = true;
        }

        if (fillDetails) {
            List<Long> userIds = new ArrayList<>();
            Long teacherId = coursePlanInfo.getTeacherId();
            Long studentId = coursePlanInfo.getStudentId();
            userIds.add(teacherId);
            userIds.add(studentId);
            Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, exposeEmail);

            List<String> userIdsString = new ArrayList<>();
            userIdsString.add(teacherId.toString());
            userIdsString.add(studentId.toString());
            Map<String, CumilativeRating> ratingsMap = cumilativeRatingDao.getCumulativeRatingsMap(userIdsString,
                    com.vedantu.util.EntityType.USER);
            UserInfo teacherUserInfo = new UserInfo(userInfos.get(teacherId), ratingsMap.get(teacherId.toString()), exposeEmail);
            UserInfo studentUserInfo = new UserInfo(userInfos.get(studentId), ratingsMap.get(studentId.toString()), exposeEmail);

            if(hideSensitiveInfo){
                teacherUserInfo.setInfo(null);
                teacherUserInfo.setSocialInfo(null);

                studentUserInfo.setInfo(null);
                studentUserInfo.setSocialInfo(null);
            }

            coursePlanInfo.setStudent(studentUserInfo);
            coursePlanInfo.setTeacher(teacherUserInfo);
            // fetching session infos
            resp = WebUtils.INSTANCE.doCall(
                    SCHEDULING_ENDPOINT + "/session/getSessions?contextId=" + coursePlanInfo.getId()
                    + "&includeAttendees=true&sortType=START_TIME_DESC&size=25&contextType=COURSE_PLAN",
                    HttpMethod.GET, null);
            String sessionListResp = resp.getEntity(String.class);
            Type SessionListType = new TypeToken<ArrayList<SessionInfo>>() {
            }.getType();
            List<SessionInfo> slist = gson.fromJson(sessionListResp, SessionListType);

            // adding the teacher and student infos
            if (ArrayUtils.isNotEmpty(slist)) {
                for (SessionInfo sessionInfo : slist) {
                    if (sessionInfo != null && ArrayUtils.isNotEmpty(sessionInfo.getAttendees())) {
                        for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                            userSessionInfo.updateUserDetails(
                                    new UserBasicInfo(userInfos.get(userSessionInfo.getUserId()), exposeEmail));
                        }
                    }
                }
            }
            coursePlanInfo.setSessionList(slist);
        }
        return coursePlanInfo;
    }

    public CoursePlanBasicInfo getCoursePlanTitle(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/getTitle/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CoursePlanBasicInfo coursePlanBasicInfo = gson.fromJson(jsonString, CoursePlanBasicInfo.class);
        return coursePlanBasicInfo;
    }

    public CoursePlanBasicInfo getCoursePlanBasicInfo(String id, boolean fillDetails, boolean exposeEmail) throws VException {
        // TODO add access control
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/basicInfo/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CoursePlanBasicInfo coursePlanBasicInfo = gson.fromJson(jsonString, CoursePlanBasicInfo.class);
        if (fillDetails) {
            List<Long> userIds = new ArrayList<>();
            userIds.add(coursePlanBasicInfo.getStudentId());
            userIds.add(coursePlanBasicInfo.getTeacherId());
            Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, exposeEmail);
            UserInfo teacherUserInfo = new UserInfo(userInfos.get(coursePlanBasicInfo.getTeacherId()), null, exposeEmail);
            UserInfo studentUserInfo = new UserInfo(userInfos.get(coursePlanBasicInfo.getStudentId()), null, exposeEmail);
            coursePlanBasicInfo.setStudent(studentUserInfo);
            coursePlanBasicInfo.setTeacher(teacherUserInfo);
        }
        return coursePlanBasicInfo;
    }

    public List<CoursePlanInfo> getCoursePlanInfos(GetCoursePlansReq req, boolean fillDetails,
            boolean exposeEmail) throws VException {
        req.verify();
        // TODO add access control
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/get?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<CoursePlanInfo>>() {
        }.getType();
        List<CoursePlanInfo> coursePlanInfos = gson.fromJson(jsonString, listType1);

        if (fillDetails) {
            if (ArrayUtils.isNotEmpty(coursePlanInfos)) {
                Set<Long> userIds = new HashSet<>();
                for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
                    userIds.add(coursePlanInfo.getStudentId());
                    userIds.add(coursePlanInfo.getTeacherId());
                }
                Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, exposeEmail);
                for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
                    if (usermap.containsKey(coursePlanInfo.getStudentId())) {
                        coursePlanInfo.setStudent(usermap.get(coursePlanInfo.getStudentId()));
                    }
                    if (usermap.containsKey(coursePlanInfo.getTeacherId())) {
                        coursePlanInfo.setTeacher(usermap.get(coursePlanInfo.getTeacherId()));
                    }
                }
            }
        }

        return coursePlanInfos;
    }

    public String addEditStructuredCourse(AddEditStructuredCourseReq addEditStructuredCourseReq) throws VException {
        addEditStructuredCourseReq.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/addEditStructuredCourse",
                HttpMethod.POST, new Gson().toJson(addEditStructuredCourseReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        return resp.getEntity(String.class);
    }

    @Deprecated
    public StructuredCourseInfo getStructuredCourse(String id) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/structuredCourse/" + id,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        StructuredCourseInfo structuredCourseInfo = gson.fromJson(jsonString, StructuredCourseInfo.class);
        return structuredCourseInfo;
    }

    public List<StructuredCourseInfo> getStructuredCourses(GetStructuredCoursesReq req)
            throws VException, IllegalArgumentException, IllegalAccessException {
        req.verify();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/structuredCourses?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<StructuredCourseInfo>>() {
        }.getType();
        List<StructuredCourseInfo> otoCourses = gson.fromJson(jsonString, listType1);
        List<Long> boardIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(otoCourses)) {
            for (StructuredCourseInfo courseInfo : otoCourses) {
                boardIds.add(courseInfo.getBoardId());
            }
            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            for (StructuredCourseInfo courseInfo : otoCourses) {
                if (boardMap.containsKey(courseInfo.getBoardId())) {
                    courseInfo.setSubject(boardMap.get(courseInfo.getBoardId()).getSlug());
                }
            }
        }
        return otoCourses;
    }

    public PayTrialAmountRes payTrialAmount(PayTrialAmountReq req) throws VException {
        req.verify();
        String courseId = req.getCoursePlanId();

        logger.info("finding the amount to pay");
        CoursePlanInfo coursePlanInfo = getCoursePlan(courseId, false, false);
        if (!coursePlanInfo.getStudentId().equals(req.getUserId())) {
            throw new ForbiddenException(ErrorCode.NOT_PART_OF_COURSE_PLAN,
                    req.getUserId() + " cannot pay for courseplan of student Id " + coursePlanInfo.getStudentId());
        }

        // if (coursePlanInfo.getTrialRegistrationFee() == null
        // || coursePlanInfo.getTrialRegistrationFee() <= 0) {
        // throw new ConflictException(ErrorCode.NO_REGISTRATION_FEE_REQUIRED,
        // "no trialRegistrationFee to pay for course " + courseId);
        // }
        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        logger.info("asking dinero if the user has paid or does he have balance to make payment of "
                + coursePlanInfo.getTrialRegistrationFee());
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        buyItemsReqNew.setAmountToPay(coursePlanInfo.getTrialRegistrationFee());
        buyItemsReqNew.setEntityId(courseId);
        buyItemsReqNew.setEntityType(EntityType.COURSE_PLAN_REGISTRATION);
        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        if (!orderInfo.getNeedRecharge()) {
            processCoursePlanAfterTrialAmountPayment(orderInfo.getId(), orderInfo.getUserId(), coursePlanInfo, true);
        }
        return mapper.map(orderInfo, PayTrialAmountRes.class);
    }

    public CheckIfRegFeePaidRes checkIfRegFeePaid(MarkTrialPaymentDoneReq req) throws VException {
        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanBasicInfo(req.getCoursePlanId(), false, false);
        if (!coursePlanBasicInfo.getStudentId().equals(req.getUserId())) {
            throw new ForbiddenException(ErrorCode.NOT_PART_OF_COURSE_PLAN,
                    "Not part of the coursepan");
        }
        if (coursePlanBasicInfo.getParentCourseId() == null
                || !coursePlanBasicInfo.getParentCourseId().equals(req.getParentCourseId())) {
            throw new ForbiddenException(ErrorCode.COURSE_PLAN_PARENT_ID_MISMATCH,
                    "Either course plan parentId does not exist or does not match with "
                    + req.getParentCourseId());
        }
        CheckIfRegFeePaidRes res = new CheckIfRegFeePaidRes();
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(DINERO_ENDPOINT + "/payment/getNonRefundedOrdersWithoutDeliverableEntityId?entityId="
                        + req.getParentCourseId() + "&userId=" + req.getUserId(), HttpMethod.GET, null);
        String listResp = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(listResp, listType);
        if (ArrayUtils.isEmpty(orders)) {
            throw new ForbiddenException(ErrorCode.NOT_REGISTERED_FOR_COURSE_OR_REG_FEE_REFUNDED,
                    "Registration fee not paid or refunded");
        } else {
            res.setOrderId(orders.get(0).getId());
        }
        return res;
    }

    public PlatformBasicResponse markTrialPaymentDone(MarkTrialPaymentDoneReq req) throws VException {
        req.verify();
        CheckIfRegFeePaidRes checkIfPaidTrialRegFeeAvailableRes = checkIfRegFeePaid(req);
        MarkRegisteredForParentCourseReq markReq = new MarkRegisteredForParentCourseReq();
        markReq.setCourseId(req.getCoursePlanId());
        markReq.setUserId(req.getUserId());
        markReq.setCallingUserId(req.getCallingUserId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                SUBSCRIPTION_ENDPOINT + "/courseplan/markRegisteredForParentCourse", HttpMethod.POST,
                gson.toJson(markReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CoursePlanInfo coursePlanInfo = gson.fromJson(jsonString, CoursePlanInfo.class);
        paymentManager.updateDeliverableEntityIdInOrder(checkIfPaidTrialRegFeeAvailableRes.getOrderId(),
                coursePlanInfo.getId(), DeliverableEntityType.COURSE_PLAN);

        processCoursePlanAfterTrialAmountPayment(checkIfPaidTrialRegFeeAvailableRes.getOrderId(), req.getUserId(),
                coursePlanInfo, false);
        return new PlatformBasicResponse();
    }

    public void processCoursePlanAfterTrialAmountPayment(String orderId, Long userId, CoursePlanInfo coursePlanInfo,
            boolean processOrderAfterPayment) throws VException {
        if (processOrderAfterPayment) {
            logger.info("making call for dinero operations");
            ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
            processPaymentReq.setUserId(userId);
            processPaymentReq.setOrderId(orderId);
            processPaymentReq.setPurchaseEntityType(InstalmentPurchaseEntity.COURSE_PLAN);
            processPaymentReq.setPurchaseEntityId(coursePlanInfo.getId());
            processPaymentReq.setDeliverableEntityId(coursePlanInfo.getId());
            OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
            logger.info(res);
        }

        logger.info("scheduling trial sessions ");
        SessionSchedule trialSessionSchedule = coursePlanInfo.getTrialSessionSchedule();
        if (trialSessionSchedule != null && ArrayUtils.isNotEmpty(trialSessionSchedule.getSessionSlots())) {
            try {
                MultipleSessionScheduleReq scheduleReq = createScheduleRequest(coursePlanInfo,
                        trialSessionSchedule.getSessionSlots(), SessionModel.TRIAL, userId);
                sessionManager.createMultipleSessions(scheduleReq);
            } catch (Exception e) {
                if (e instanceof BadRequestException) {
                    BadRequestException eb = (BadRequestException) e;
                    if (ErrorCode.SLOTS_NOT_AVAILABLE.equals(eb.getErrorCode())
                            || ErrorCode.INVALID_SESSION_FIELDS.equals(eb.getErrorCode())
                            || ErrorCode.BAD_REQUEST_ERROR.equals(eb.getErrorCode())) {
                        logger.warn("no session scheduled " + e.getMessage());
                    } else {
                        // TODO revert money
                        logger.error("err in scheduling trial sessions ", e.getMessage());
                        throw e;
                    }
                } else {
                    // TODO revert money
                    logger.error("err in scheduling trial sessions ", e.getMessage());
                    throw e;
                }
            }
        } else {
            logger.info("no trial slots to schedule");
        }

        markCoursePlanState(userId, coursePlanInfo.getId(), CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE, null,
                null);

        if (ArrayUtils.isNotEmpty(coursePlanInfo.getCurriculum())) {
            logger.info("sharing the contents");
            List<ContentInfo> contents = getContentInfosFromCurriculum(coursePlanInfo.getCurriculum());
            if (ArrayUtils.isNotEmpty(contents)) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("contentInfos", contents);
                payload.put("studentId", coursePlanInfo.getStudentId());
                payload.put("title", coursePlanInfo.getTitle());
                payload.put("coursePlanId", coursePlanInfo.getId());
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SHARE_CONTENT_INFOS, payload);
                asyncTaskFactory.executeTask(params);
            }
        } else {
            logger.info("no contents to share");
        }

        logger.info("resetting the calendar block entry");
        if (trialSessionSchedule != null && ArrayUtils.isNotEmpty(trialSessionSchedule.getSessionSlots())) {
            // the above condition will make sure we have a calendar block entry
            logger.info("blocking the calendar");
            ResetCoursePlanSlotsPojo pojo = new ResetCoursePlanSlotsPojo(trialSessionSchedule.getEndDate(), coursePlanInfo.getId());
            SQSMessageType messageType = SQSMessageType.COURSE_PLAN_RESET_SLOTS;
            awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo), coursePlanInfo.getId());
            /*Map<String, Object> payload = new HashMap<>();
            payload.put("endTime", trialSessionSchedule.getEndDate());
            payload.put("coursePlanId", coursePlanInfo.getId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.RESET_COURSE_PLAN_SLOTS, payload);
            asyncTaskFactory.executeTask(params);*/
        }

    }

    public EnrollCoursePlanRes enroll(EnrollCoursePlanReq req) throws VException, CloneNotSupportedException {
        req.verify();
        String courseId = req.getCoursePlanId();

        logger.info("finding the amount to pay");
        CoursePlanInfo coursePlanInfo = getCoursePlan(courseId, false, false);
        if (!coursePlanInfo.getStudentId().equals(req.getUserId())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    req.getUserId() + " cannot pay for courseplan of student Id " + coursePlanInfo.getStudentId());
        }

        // should not be in between PUBLISHED ---- TRIAL_SESSIONS_DONE states
        if (coursePlanInfo.getState().order < CoursePlanEnums.CoursePlanState.PUBLISHED.order
                || coursePlanInfo.getState().order > CoursePlanEnums.CoursePlanState.TRIAL_SESSIONS_DONE.order) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    "courseplan is in " + coursePlanInfo.getState() + " state " + courseId);
        }

        // reg fee needs to be paid but the state is in between DRAFT ---- PUBLISHED
        if (coursePlanInfo.getTrialRegistrationFee() != null && coursePlanInfo.getTrialRegistrationFee() > 0
                && coursePlanInfo.getState().order < CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE.order) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    "courseplan reg fee exist but the state is in " + coursePlanInfo.getState() + ", " + courseId);
        }

        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        int amountToPay = coursePlanInfo.getPrice();
        if (coursePlanInfo.getTrialRegistrationFee() != null) {
            amountToPay -= coursePlanInfo.getTrialRegistrationFee();
        }
        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(courseId);
        buyItemsReqNew.setEntityType(EntityType.COURSE_PLAN);
        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setBaseInstalmentInfos(coursePlanInfo.getInstalmentsInfo());
        buyItemsReqNew.setPaymentType(req.getPaymentType());
        buyItemsReqNew.setIpAddress(req.getIpAddress());

        if (PaymentType.INSTALMENT.equals(req.getPaymentType())
                && ArrayUtils.isEmpty(coursePlanInfo.getInstalmentsInfo())) {
            throw new BadRequestException(ErrorCode.NO_BASE_INSTALMENTS_FOUND, "No base instalments found");
        }
        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        if (!orderInfo.getNeedRecharge()) {
            processCoursePlanAfterEnrollPayment(orderInfo.getId(), orderInfo.getUserId(), coursePlanInfo);
        }
        return mapper.map(orderInfo, EnrollCoursePlanRes.class);
    }

    public void processCoursePlanAfterEnrollPayment(String orderId, Long userId, CoursePlanInfo coursePlanInfo)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        processPaymentReq.setPurchaseEntityType(InstalmentPurchaseEntity.COURSE_PLAN);
        processPaymentReq.setPurchaseEntityId(coursePlanInfo.getId());
        processPaymentReq.setDeliverableEntityId(coursePlanInfo.getId());
        processPaymentReq.setBaseInstalmentInfos(coursePlanInfo.getInstalmentsInfo());
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        markCoursePlanState(userId, coursePlanInfo.getId(), CoursePlanEnums.CoursePlanState.ENROLLED, null, null);

        logger.info("unblocking the calendar for courseplan");
        // try {
        // String markprocessedUrl = SCHEDULING_ENDPOINT
        // + "/calendarBlockEntry/markProcessed" + "?referenceId="
        // + coursePlanInfo.getId();
        // ClientResponse markprocessedResp = WebUtils.INSTANCE.doCall(markprocessedUrl,
        // HttpMethod.POST,
        // null);
        // VExceptionFactory.INSTANCE.parseAndThrowException(markprocessedResp);
        // String jsonString = markprocessedResp.getEntity(String.class);
        // logger.info("Response for block slot markprocessed : " + jsonString);
        // } catch (Exception ex) {
        // logger.error("Error in marking calendar processed for course plan "
        // + coursePlanInfo.getId() + ", ex " + ex.getMessage());
        // }

        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_BOOK_SESSIONS_TASK;
        BookCoursePlanSessionPojo pojo = new BookCoursePlanSessionPojo(coursePlanInfo, userId, res.getPaymentType());
        String groupId = coursePlanInfo.getId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo), groupId);

        /*Map<String, Object> payload = new HashMap<>();
        payload.put("coursePlanInfo", new Gson().toJson(coursePlanInfo));
        payload.put("userId", userId);
        payload.put("paymentType", res.getPaymentType());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BOOK_COURSE_PLAN_REGULAR_SESSIONS, payload);
        asyncTaskFactory.executeTask(params);*/

        if (PaymentType.INSTALMENT.equals(res.getPaymentType())) {
            blockInstalmentSchedule(coursePlanInfo);
        } else {
            markBlockedSlotsProcessed(coursePlanInfo.getId());
        }
    }

    public void processCoursePlanFromUpdateCoursePlanState(Long userId, String coursePlanId,
            PaymentType paymentType)
            throws VException {

        CoursePlanInfo coursePlanInfo = getCoursePlan(coursePlanId, false, false);
        markCoursePlanState(userId, coursePlanId, CoursePlanEnums.CoursePlanState.ENROLLED, null, null);

        logger.info("booking regular sessions");
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_BOOK_SESSIONS_TASK;
        BookCoursePlanSessionPojo pojo = new BookCoursePlanSessionPojo(coursePlanInfo, userId, paymentType);
        String groupId = coursePlanInfo.getId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo), groupId);
        /*Map<String, Object> payload = new HashMap<>();
        payload.put("coursePlanInfo", new Gson().toJson(coursePlanInfo));
        payload.put("userId", userId);
        payload.put("paymentType", paymentType);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BOOK_COURSE_PLAN_REGULAR_SESSIONS, payload);
        asyncTaskFactory.executeTask(params);*/

        if (PaymentType.INSTALMENT.equals(paymentType)) {
            blockInstalmentSchedule(coursePlanInfo);
        } else {
            markBlockedSlotsProcessed(coursePlanInfo.getId());
        }
    }

    public PlatformBasicResponse processCoursePlanHrs(String coursePlanId, Long callingUserId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(dineroEndpoint + "/payment/getInstalments?coursePlanId=" + coursePlanId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        InstalmentInfo info = null;
        List<InstalmentInfo> instalmentInfos = new ArrayList(Arrays.asList(new Gson().fromJson(jsonString, InstalmentInfo[].class)));
        if (ArrayUtils.isNotEmpty(instalmentInfos)) {
            Collections.sort(instalmentInfos, new Comparator<InstalmentInfo>() {
                @Override
                public int compare(InstalmentInfo o1, InstalmentInfo o2) {
                    return o2.getDueTime().compareTo(o1.getDueTime());
                }
            });
            for (InstalmentInfo instalmentInfo : instalmentInfos) {
                if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                    info = instalmentInfo;
                    logger.info("Latest Paid Installment", info);
                    break;
                }
            }
        }

        if (info == null) {
            throw new VException(ErrorCode.SERVICE_ERROR, "Instalment not started");
        }

        processInstalmentPayment(info, callingUserId, Boolean.FALSE);
        return new PlatformBasicResponse();
    }

    public void processInstalmentPayment(InstalmentInfo info, Long callingUserId, Boolean processInstalment) throws VException {
        logger.info("making call for dinero operations");
        if (processInstalment) {
            List<InstalmentInfo> res = paymentManager.processInstalmentAfterPayment(info.getId());
            logger.info(res);
        }

        CoursePlanInfo coursePlanInfo = getCoursePlan(info.getContextId(), false, false);
        Long dueTime = info.getDueTime();
        List<SessionSlot> slots = getInstalmentSlots(coursePlanInfo.getRegularSessionSchedule(),
                coursePlanInfo.getInstalmentsInfo(), dueTime);
        if (ArrayUtils.isEmpty(slots)) {
            logger.error("Empty slots found for instalments " + info);
            return;
        }
        int courseHoursToAdd = 0;
        Long endTime = slots.get(0).getEndTime();
        for (SessionSlot slot : slots) {
            courseHoursToAdd += slot.getEndTime().intValue() - slot.getStartTime().intValue();
            if (slot.getEndTime() > endTime) {
                endTime = slot.getEndTime();
            }
        }

        logger.info("adding hrs to course plan hrs: " + courseHoursToAdd);
        AddCoursePlanHoursReq addCoursePlanHoursReq = new AddCoursePlanHoursReq();
        addCoursePlanHoursReq.setCoursePlanId(coursePlanInfo.getId());
        addCoursePlanHoursReq.setTransactionRefNo(info.getId());
        addCoursePlanHoursReq.setHoursToAdd(courseHoursToAdd);
        addCoursePlanHoursReq.setInstalmentPayment(true);
        addCoursePlanHours(addCoursePlanHoursReq);

        logger.info("scheduling regular sessions");
        if (ArrayUtils.isNotEmpty(slots)) {
            MultipleSessionScheduleReq scheduleReq = createScheduleRequest(coursePlanInfo, slots, SessionModel.OTO,
                    callingUserId);
            List<SessionSlot> bookedSlots = new ArrayList<>();
            Long sessionId = null;
            try {
                List<SessionInfo> scheduledSessionInfos = sessionManager.createMultipleSessions(scheduleReq);
                sessionId = scheduledSessionInfos.get(0).getId();
                SessionSlot tempSlot = slots.get(0);
                for (SessionInfo sessionInfo : scheduledSessionInfos) {
                    SessionSlot _slot = (SessionSlot) tempSlot.clone();
                    _slot.setStartTime(sessionInfo.getStartTime());
                    _slot.setEndTime(sessionInfo.getEndTime());
                    bookedSlots.add(_slot);
                }
            } catch (BadRequestException e) {
                logger.warn("no session scheduled " + e.getMessage());
            } catch (Exception e) {
                logger.error("error in creating clone of session slot " + e.getMessage());
            }
            if (ArrayUtils.isNotEmpty(bookedSlots)) {
                CoursePlanSessionRequest coursePlanSessionRequest = new CoursePlanSessionRequest();
                coursePlanSessionRequest.setSlots(bookedSlots);
                coursePlanSessionRequest.setEntityId(coursePlanInfo.getId());
                coursePlanSessionRequest.setEntityType(EntityType.COURSE_PLAN);
                coursePlanSessionRequest.setSessionId(sessionId);
                transferBookSessionHours(coursePlanSessionRequest);
            }
        } else {
            logger.info("no regular slots to schedule");
        }

        logger.info("updating blocked instalment slots");
        updateBlockedInstalmentSlots(coursePlanInfo.getId(), endTime);

        Collections.sort(coursePlanInfo.getInstalmentsInfo(), new Comparator<BaseInstalmentInfo>() {
            @Override
            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                return o2.getDueTime().compareTo(o1.getDueTime());
            }
        });
        Long lastDueTime = coursePlanInfo.getInstalmentsInfo().get(0).getDueTime();
        if (info.getDueTime().equals(lastDueTime)) {
            markBlockedSlotsProcessed(coursePlanInfo.getId());
        }
    }

    public void updateBlockedInstalmentSlots(String coursePlanId, Long endTime) {
        String updateBlockCalendarUrl = SCHEDULING_ENDPOINT + "/calendarBlockEntry/resetBlockEntry?referenceId="
                + coursePlanId + "&endTime=" + endTime;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(updateBlockCalendarUrl, HttpMethod.POST, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info("Response from updating the calendar block entry: " + jsonString);
        } catch (Exception ex) {
            logger.error("Error in updating blocking instalment slots for coursePlanId " + coursePlanId + ", ex "
                    + ex.getMessage());
        }
        logger.info("EXIT ");
    }

    public void bookRegularSessions(CoursePlanInfo coursePlanInfo, Long userId, PaymentType paymentType)
            throws VException, CloneNotSupportedException {
        logger.info("preparing the schedule for which payment is made");
        Integer courseHoursToAdd = 0;
        List<SessionSlot> slots = null;
        if (coursePlanInfo.getRegularSessionSchedule() != null
                && ArrayUtils.isNotEmpty(coursePlanInfo.getRegularSessionSchedule().getSessionSlots())) {
            slots = coursePlanInfo.getRegularSessionSchedule().getSessionSlots();
            if (PaymentType.INSTALMENT.equals(paymentType)) {
                slots = getInstalmentSlots(coursePlanInfo.getRegularSessionSchedule(),
                        coursePlanInfo.getInstalmentsInfo(), coursePlanInfo.getInstalmentsInfo().get(0).getDueTime());
            }
            for (SessionSlot slot : slots) {
                courseHoursToAdd += slot.getEndTime().intValue() - slot.getStartTime().intValue();
            }
        } else {
            courseHoursToAdd = (coursePlanInfo.getTotalCourseHours() != null) ? coursePlanInfo.getTotalCourseHours()
                    : 0;
        }

        logger.info("adding hrs to course plan hrs: " + courseHoursToAdd);
        AddCoursePlanHoursReq addCoursePlanHoursReq = new AddCoursePlanHoursReq();
        addCoursePlanHoursReq.setCoursePlanId(coursePlanInfo.getId());
        addCoursePlanHoursReq.setTransactionRefNo(coursePlanInfo.getId());
        addCoursePlanHoursReq.setHoursToAdd(courseHoursToAdd);
        addCoursePlanHours(addCoursePlanHoursReq);

        logger.info("scheduling regular sessions ");
        if (ArrayUtils.isNotEmpty(slots)) {
            MultipleSessionScheduleReq scheduleReq = createScheduleRequest(coursePlanInfo, slots, SessionModel.OTO,
                    userId);
            Long sessionId = null;
            List<SessionSlot> bookedSlots = new ArrayList<>();
            try {
                List<SessionInfo> scheduledSessionInfos = sessionManager.createMultipleSessions(scheduleReq);
                sessionId = scheduledSessionInfos.get(0).getId();
                SessionSlot tempSlot = slots.get(0);
                for (SessionInfo sessionInfo : scheduledSessionInfos) {
                    SessionSlot _slot = (SessionSlot) tempSlot.clone();
                    _slot.setStartTime(sessionInfo.getStartTime());
                    _slot.setEndTime(sessionInfo.getEndTime());
                    bookedSlots.add(_slot);
                }
            } catch (BadRequestException e) {
                logger.warn("no session scheduled " + e.getMessage());
            } catch (Exception e) {
                logger.error("error in creating clone of session slot " + e.getMessage());
            }
            if (ArrayUtils.isNotEmpty(bookedSlots)) {
                CoursePlanSessionRequest coursePlanSessionRequest = new CoursePlanSessionRequest();
                coursePlanSessionRequest.setSlots(bookedSlots);
                coursePlanSessionRequest.setEntityId(coursePlanInfo.getId());
                coursePlanSessionRequest.setEntityType(EntityType.COURSE_PLAN);
                coursePlanSessionRequest.setSessionId(sessionId);
                transferBookSessionHours(coursePlanSessionRequest);
            }
        } else {
            logger.info("no regular slots to schedule");
        }
    }

    public void blockInstalmentSchedule(CoursePlanInfo coursePlanInfo) {
        logger.info("blocking remaining schedule for instalments");
        Map<Long, List<SessionSlot>> allSlots = getInstalmentSlots(coursePlanInfo.getRegularSessionSchedule(),
                coursePlanInfo.getInstalmentsInfo());
        Collections.sort(coursePlanInfo.getInstalmentsInfo(), new Comparator<BaseInstalmentInfo>() {
            @Override
            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });
        Long firstDueTime = coursePlanInfo.getInstalmentsInfo().get(0).getDueTime();
        List<SessionSlot> firstInstSlots = allSlots.get(firstDueTime);
        if (ArrayUtils.isNotEmpty(firstInstSlots)) {
            logger.info("blocking the calendar");
            SessionSchedule sessionSchedule = new SessionSchedule();
            sessionSchedule.setSessionSlots(firstInstSlots);
            ResetCoursePlanSlotsPojo pojo = new ResetCoursePlanSlotsPojo(sessionSchedule.getEndDate(), coursePlanInfo.getId());
            SQSMessageType messageType = SQSMessageType.COURSE_PLAN_RESET_SLOTS;
            awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo), coursePlanInfo.getId());

            /*Map<String, Object> payload = new HashMap<>();
            payload.put("endTime", sessionSchedule.getEndDate());
            payload.put("coursePlanId", coursePlanInfo.getId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.RESET_COURSE_PLAN_SLOTS, payload);
            asyncTaskFactory.executeTask(params);*/
        }
    }

    private List<SessionSlot> getInstalmentSlots(SessionSchedule sessionSchedule,
            List<BaseInstalmentInfo> baseInstalmentInfos, Long dueTime) {
        return getInstalmentSlots(sessionSchedule, baseInstalmentInfos).get(dueTime);
    }

    private Map<Long, List<SessionSlot>> getInstalmentSlots(SessionSchedule sessionSchedule,
            List<BaseInstalmentInfo> baseInstalmentInfos) {
        Collections.sort(baseInstalmentInfos, new Comparator<BaseInstalmentInfo>() {
            @Override
            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });
        List<SessionSlot> sessionSlots = null;
        if (sessionSchedule != null && ArrayUtils.isNotEmpty(sessionSchedule.getSessionSlots())) {
            sessionSlots = sessionSchedule.getSessionSlots();
        } else {
            sessionSlots = new ArrayList<>();
        }

        // making sure slots are in order
        Collections.sort(sessionSlots,
                (SessionSlot o1, SessionSlot o2) -> o1.getStartTime().compareTo(o2.getStartTime()));
        // sessionSlots = instalmentUtils.addSessionsForAllWeeks(sessionSlots,
        // noOfWeeks);

        Map<Long, List<SessionSlot>> sessionSlotsMap = new HashMap<>();

        int count = 0, instalmentsCount = baseInstalmentInfos.size();
        int k = 0, slotsSize = sessionSlots.size(), upTill = 0;
        for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
            Long nextInstDueTime = null;
            if (count <= instalmentsCount - 2) {
                nextInstDueTime = baseInstalmentInfos.get(count + 1).getDueTime();
            }
            for (int i = k; i < slotsSize; i++) {
                SessionSlot slot = sessionSlots.get(i);
                if (nextInstDueTime != null && slot.getStartTime() >= nextInstDueTime) {
                    break;
                }
                upTill++;
            }
            List<SessionSlot> instSlots = sessionSlots.subList(k, upTill);
            k = upTill;
            sessionSlotsMap.put(baseInstalmentInfo.getDueTime(), instSlots);
            count++;
        }
        return sessionSlotsMap;
    }

    public void shareCoursePlanContents(List<ContentInfo> contents, Long studentId, String title, String coursePlanId) {
        if (ArrayUtils.isNotEmpty(contents)) {
            List<Long> userIds = new ArrayList<>();
            userIds.add(studentId);
            for (ContentInfo content : contents) {
                userIds.add(content.getTeacherId());
            }
            Map<Long, UserBasicInfo> map = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
            CMDSShareOTFContentsReq req = new CMDSShareOTFContentsReq();
            req.setContextType(EntityType.COURSE_PLAN);
            req.setContextId(coursePlanId);
            req.setEngagementType(EngagementType.OTO);
            if (map.containsKey(studentId)) {
                req.getStudents().add(map.get(studentId));
            }
            req.setCourseName(title);
            for (ContentInfo content : contents) {
                if (map.containsKey(content.getTeacherId())) {
                    req.getShareInfo().add(new CMDSShareContentinfo(map.get(content.getTeacherId()), content.getUrl(), content.getTestId()));
                }
            }
            testManager.createCMDSShareEvents(req);
        }
    }

    public void blockCoursePlanSlots(SessionSchedule sessionSchedule, Long studentId, Long teacherId,
            String coursePlanId) throws VException {
        String blockCalendarUrl = SCHEDULING_ENDPOINT + "/calendarEntry/blockSlotSchedule";
        sessionSchedule.setStartTime(CommonCalendarUtils.getDayStartTime(sessionSchedule.getStartDate()));
        sessionSchedule.setEndTime(sessionSchedule.getEndDate());
        sessionSchedule.setNoOfWeeks(1l);
        BlockSlotScheduleReq req = new BlockSlotScheduleReq();
        if (studentId != null) {
            req.setStudentId(studentId.toString());
        }
        if (teacherId != null) {
            req.setTeacherId(teacherId.toString());
        }
        req.setReferenceId(coursePlanId);
        req.setSchedule(sessionSchedule);
        logger.info("BlockSlotScheduleReq " + req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(blockCalendarUrl, HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from blocking instalment slots: " + jsonString);
    }

    public List<SessionInfo> bookSession(CoursePlanSessionRequest sessionRequest) throws VException {
        sessionRequest.verify();
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                SUBSCRIPTION_ENDPOINT + "/courseplan/basicInfoWithHours/" + sessionRequest.getEntityId(),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CoursePlanBasicInfo coursePlanBasicInfo = gson.fromJson(jsonString, CoursePlanBasicInfo.class);

        if (coursePlanBasicInfo == null) {
            logger.info("coursePlan with id: " + sessionRequest.getEntityId() + " doesn't exist");
            throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
                    "Course Plan with id: " + sessionRequest.getEntityId() + " doesn't exist");
        } else if (CoursePlanEnums.CoursePlanState.ENDED.equals(coursePlanBasicInfo.getState())) {
            throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
                    "Course Plan with id: " + sessionRequest.getEntityId() + " already ended");
        }

        sessionRequest.setTeacherId(coursePlanBasicInfo.getTeacherId());
        sessionRequest.setStudentId(coursePlanBasicInfo.getStudentId());
        if (sessionRequest.getModel() == null) {
            sessionRequest.setModel(SessionModel.OTO);
        }
        sessionRequest.setBoardId(coursePlanBasicInfo.getBoardId());

        Long scheduledHours = sessionRequest.getHours();

        if (!SessionModel.TRIAL.equals(sessionRequest.getModel())
                && scheduledHours > coursePlanBasicInfo.getRemainingHours()) {
            logger.info("RequestedHours: " + scheduledHours + " are more than the hours: "
                    + coursePlanBasicInfo.getRemainingHours() + " Subscription: " + coursePlanBasicInfo.getId());
            throw new BadRequestException(ErrorCode.SUBSCRIPTION_REQUESTED_HOURS_INCORRECT,
                    "RequestedHours: " + scheduledHours + " are more than the hours: "
                    + coursePlanBasicInfo.getRemainingHours() + " Subscription: "
                    + coursePlanBasicInfo.getId());
        }

        MultipleSessionScheduleReq multipleSessionScheduleReq = createScheduleRequest(coursePlanBasicInfo,
                sessionRequest.getSlots(), sessionRequest.getModel(), sessionRequest.getCallingUserId());
        List<SessionInfo> scheduledSessionInfos = null;
        try {
            scheduledSessionInfos = sessionManager.createMultipleSessions(multipleSessionScheduleReq);
        } catch (BadRequestException e) {
            logger.warn("no session scheduled " + e.getMessage());
        }
        if (ArrayUtils.isEmpty(scheduledSessionInfos)) {
            logger.info("Session scheduling failed in bookSession: " + scheduledSessionInfos);
            throw new ConflictException(ErrorCode.SESSION_NOT_SCHEDULED,
                    "Session scheduling failed in bookSession: " + scheduledSessionInfos);
        }
        Long sessionId = scheduledSessionInfos.get(0).getId();
        sessionRequest.setSessionId(sessionId);

        if (!SessionModel.TRIAL.equals(sessionRequest.getModel())) {
            transferBookSessionHours(sessionRequest);
        }
        return scheduledSessionInfos;
    }

    public RefundRes endCoursePlan(EndCoursePlanReq endCoursePlanReq) throws Exception {
        endCoursePlanReq.verify();
        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanBasicInfo(endCoursePlanReq.getCoursePlanId(), false, false);
        // TODO handle draft state end

        RefundRes res = null;
        switch (coursePlanBasicInfo.getState()) {
            case DRAFT:
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "courseplan " + coursePlanBasicInfo.getId()
                        + " is in " + coursePlanBasicInfo.getState() + " state, for now action not supported yet");
            case PUBLISHED:
                res = endCoursePlanPublished(coursePlanBasicInfo, endCoursePlanReq);
                break;
            case TRIAL_PAYMENT_DONE:
            case TRIAL_SESSIONS_DONE:
                res = endCoursePlanTrial(coursePlanBasicInfo, endCoursePlanReq);
                break;
            case ENROLLED:
                res = endCoursePlanEnrolled(coursePlanBasicInfo, endCoursePlanReq);
                break;
            case ENDED:
                throw new ForbiddenException(ErrorCode.ALREADY_ENDED,
                        "courseplan " + coursePlanBasicInfo.getId() + " is already ended");
            default:
                break;
        }
        return res;
    }

    private RefundRes endCoursePlanPublished(CoursePlanBasicInfo coursePlanBasicInfo, EndCoursePlanReq endCoursePlanReq)
            throws VException {

        // for now not refunding, as per product requirement
        if (Boolean.TRUE.equals(endCoursePlanReq.getGetRefundInfoOnly())) {
            return new RefundRes();
        }

        logger.info("marking the courseplan state");
        markCoursePlanState(endCoursePlanReq.getCallingUserId(), endCoursePlanReq.getCoursePlanId(),
                CoursePlanEnums.CoursePlanState.ENDED, CoursePlanEnums.CoursePlanEndType.TRIAL_REFUNDED,
                endCoursePlanReq.getReason());

        // TODO unshare content
        logger.info("unblocking the calendar for courseplan");
        markBlockedSlotsProcessed(coursePlanBasicInfo.getId());
        return null;
    }

    private RefundRes endCoursePlanTrial(CoursePlanBasicInfo coursePlanBasicInfo, EndCoursePlanReq endCoursePlanReq)
            throws Exception {

        if (!Boolean.TRUE.equals(endCoursePlanReq.getGetRefundInfoOnly())) {
            cancelAllCoursePlanSessions(coursePlanBasicInfo, endCoursePlanReq);
        }
        RefundReq refundReq = new RefundReq();
        refundReq.setUserId(coursePlanBasicInfo.getStudentId());
        refundReq.setCallingUserId(endCoursePlanReq.getCallingUserId());
        refundReq.setRefundPolicy(RefundPolicy.FULL_REFUND);
        refundReq.setGetRefundInfoOnly(endCoursePlanReq.getGetRefundInfoOnly());
        if (Boolean.TRUE.equals(coursePlanBasicInfo.getRegisteredForParentCourse())) {
            refundReq.setEntityType(EntityType.OTO_COURSE_REGISTRATION);
            refundReq.setEntityId(coursePlanBasicInfo.getParentCourseId());
        } else {
            refundReq.setEntityType(EntityType.COURSE_PLAN_REGISTRATION);
            refundReq.setEntityId(coursePlanBasicInfo.getId());
        }

        if (Boolean.TRUE.equals(endCoursePlanReq.getGetRefundInfoOnly())) {
            String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
            ClientResponse resp = WebUtils.INSTANCE.doCall(
                    dineroEndpoint + "/refund", HttpMethod.POST, new Gson().toJson(refundReq));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info("EXIT: " + jsonString);
            RefundRes res = new Gson().fromJson(jsonString, RefundRes.class);
            return res;
        }

        try {
            String markOrderForfeited = DINERO_ENDPOINT + "/payment/markTrialOrderEnded";
            JSONObject markOrderForfeitedReq = new JSONObject();
            if (Boolean.TRUE.equals(coursePlanBasicInfo.getRegisteredForParentCourse())) {
                markOrderForfeitedReq.put("entityType", EntityType.OTO_COURSE_REGISTRATION);
                markOrderForfeitedReq.put("entityId", coursePlanBasicInfo.getParentCourseId());
            } else {
                markOrderForfeitedReq.put("entityType", EntityType.COURSE_PLAN_REGISTRATION);
                markOrderForfeitedReq.put("entityId", coursePlanBasicInfo.getId());
            }
            markOrderForfeitedReq.put("userId", coursePlanBasicInfo.getStudentId());

            ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                    markOrderForfeitedReq.toString());
            VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
            String jsonString = markOrderForfeitedRes.getEntity(String.class);
            logger.info("Response for markOrderForfeited  : " + jsonString);
            MarkOrderEndedRes markOrderEndedRes = gson.fromJson(jsonString, MarkOrderEndedRes.class);
            refundReq.setOrderId(markOrderEndedRes.getOrderId());
        } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
            logger.error("Error in markOrderForfeited for courseplan  " + coursePlanBasicInfo.getId(), ex);
            throw new VException(ErrorCode.SERVICE_ERROR, ex.getMessage());
        }
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/refund", HttpMethod.POST, new Gson().toJson(refundReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        RefundRes res = new Gson().fromJson(jsonString, RefundRes.class);

        logger.info("marking the courseplan state");
        markCoursePlanState(endCoursePlanReq.getCallingUserId(), endCoursePlanReq.getCoursePlanId(),
                CoursePlanEnums.CoursePlanState.ENDED, CoursePlanEnums.CoursePlanEndType.TRIAL_REFUNDED,
                endCoursePlanReq.getReason());

        // TODO unshare content
        logger.info("unblocking the calendar for courseplan");
        markBlockedSlotsProcessed(coursePlanBasicInfo.getId());
        return res;
    }

    private RefundRes endCoursePlanEnrolled(CoursePlanBasicInfo coursePlanBasicInfo, EndCoursePlanReq endCoursePlanReq)
            throws Exception {

        if (!Boolean.TRUE.equals(endCoursePlanReq.getGetRefundInfoOnly())) {
            cancelAllCoursePlanSessions(coursePlanBasicInfo, endCoursePlanReq);
        }

        ClientResponse resp1 = WebUtils.INSTANCE.doCall(
                SUBSCRIPTION_ENDPOINT + "/courseplan/getCoursePlanBalanceRes?coursePlanId=" + endCoursePlanReq.getCoursePlanId(),
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
        String jsonString1 = resp1.getEntity(String.class);
        GetCoursePlanBalanceRes getCoursePlanBalanceRes = gson.fromJson(jsonString1, GetCoursePlanBalanceRes.class);
        RefundReq refundReq = new RefundReq();
        refundReq.setUserId(coursePlanBasicInfo.getStudentId());
        refundReq.setCallingUserId(endCoursePlanReq.getCallingUserId());
        refundReq.setRefundPolicy(RefundPolicy.PARTIAL);
        refundReq.setGetRefundInfoOnly(endCoursePlanReq.getGetRefundInfoOnly());
        refundReq.setEntityType(EntityType.COURSE_PLAN);
        refundReq.setEntityId(coursePlanBasicInfo.getId());
        if (getCoursePlanBalanceRes.getNpLeft() > 0) {
            refundReq.setNonPromotionalAmount(getCoursePlanBalanceRes.getNpLeft());
        } else {
            refundReq.setNonPromotionalAmount(0);
        }

        if (getCoursePlanBalanceRes.getpLeft() > 0) {
            refundReq.setPromotionalAmount(getCoursePlanBalanceRes.getpLeft());
        } else {
            refundReq.setPromotionalAmount(0);
        }

        if (getCoursePlanBalanceRes.getDiscountLeft() > 0) {
            refundReq.setDiscountLeft(getCoursePlanBalanceRes.getDiscountLeft());
        } else {
            refundReq.setDiscountLeft(0);
        }

        if (Boolean.FALSE.equals(endCoursePlanReq.getGetRefundInfoOnly())) {

            logger.info("marking the courseplan state");
            markCoursePlanState(endCoursePlanReq.getCallingUserId(), endCoursePlanReq.getCoursePlanId(),
                    CoursePlanEnums.CoursePlanState.ENDED, endCoursePlanReq.getEndType(), endCoursePlanReq.getReason());

            // TODO unshare content
            logger.info("unblocking the calendar for courseplan");
            markBlockedSlotsProcessed(coursePlanBasicInfo.getId());

            logger.info("marking the order forfeited if the payment is done by instalment and not fully paid");
            try {
                String markOrderForfeited = DINERO_ENDPOINT + "/payment/markOrderEnded";
                JSONObject markOrderForfeitedReq = new JSONObject();
                markOrderForfeitedReq.put("entityType", EntityType.COURSE_PLAN);
                markOrderForfeitedReq.put("entityId", coursePlanBasicInfo.getId());
                markOrderForfeitedReq.put("userId", coursePlanBasicInfo.getStudentId());

                ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                        markOrderForfeitedReq.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
                String jsonString = markOrderForfeitedRes.getEntity(String.class);
                logger.info("Response for markOrderForfeited  : " + jsonString);
                MarkOrderEndedRes markOrderEndedRes = gson.fromJson(jsonString, MarkOrderEndedRes.class);
                refundReq.setOrderId(markOrderEndedRes.getOrderId());
            } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                logger.error("Error in markOrderForfeited for courseplan  " + coursePlanBasicInfo.getId(), ex);
            }
        }

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/refund", HttpMethod.POST, new Gson().toJson(refundReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String refjson = resp.getEntity(String.class);
        logger.info("EXIT: " + refjson);
        RefundRes res = new Gson().fromJson(refjson, RefundRes.class);

        return res;
    }

    private void cancelAllCoursePlanSessions(CoursePlanBasicInfo coursePlanBasicInfo, EndCoursePlanReq endCoursePlanReq)
            throws Exception {
        logger.info("cancelling all sessions of this courseplan");
        try {
            SubscriptioncoursePlanCancelSessionsReq req = new SubscriptioncoursePlanCancelSessionsReq();
            req.setCallingUserId(endCoursePlanReq.getCallingUserId());
            req.setRemark(endCoursePlanReq.getReason());
            req.setContextType(EntityType.COURSE_PLAN);
            req.setContextId(coursePlanBasicInfo.getId());
            ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/session/cancelSubscriptionSessions",
                    HttpMethod.POST, gson.toJson(req));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
        } catch (ConflictException e) {
            throw e;
        } catch (Exception e) {
            logger.error(
                    "Error in cancelling sessions courseplanId " + coursePlanBasicInfo.getId() + ", " + e.getMessage());
            throw e;
        }
    }

    private void markBlockedSlotsProcessed(String coursePlanId) {
        try {
            String markprocessedUrl = SCHEDULING_ENDPOINT + "/calendarBlockEntry/markProcessed" + "?referenceId="
                    + coursePlanId;
            ClientResponse markprocessedResp = WebUtils.INSTANCE.doCall(markprocessedUrl, HttpMethod.POST, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(markprocessedResp);
            String jsonString = markprocessedResp.getEntity(String.class);
            logger.info("Response for block slot markprocessed : " + jsonString);
        } catch (Exception ex) {
            logger.error(
                    "Error in marking calendar processed for course plan " + coursePlanId + ", ex " + ex.getMessage());
        }
    }

    private void addCoursePlanHours(AddCoursePlanHoursReq req) throws VException {
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_PRIOR_SESSION_BOOK;
        String groupId = req.getCoursePlanId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(req), groupId);
//        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/addCoursePlanHours",
//                HttpMethod.POST, gson.toJson(req));
//        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        String jsonString = resp.getEntity(String.class);
//        logger.info(jsonString);
    }

    private void transferBookSessionHours(CoursePlanSessionRequest request) throws VException {

        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_BOOK;
        String groupId = request.getEntityId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(request), groupId);
//        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/transferBookSessionHours",
//                HttpMethod.POST, gson.toJson(sessionRequest));
//        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        String jsonString = resp.getEntity(String.class);
        logger.info("Sent to SQS {} {}", messageType, groupId);
    }

    public SessionInfo cancelSession(EndSessionReq endSessionReq) throws VException {
        SessionInfo sessionInfo = sessionManager.cancelSession(endSessionReq);
        if (!EntityType.COURSE_PLAN.equals(sessionInfo.getContextType())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Session type not courseplan");
        }

        TransferCancelSessionHoursReq req = new TransferCancelSessionHoursReq(sessionInfo.getContextId(),
                sessionInfo.getId(), (int) (sessionInfo.getEndTime() - sessionInfo.getStartTime()));

        if (!SessionModel.TRIAL.equals(sessionInfo.getModel())) {
            transferCancelSessionHours(req);
        }

        // Fetch the course plan
        CoursePlanInfo coursePlanInfo = getCoursePlan(sessionInfo.getContextId(), false, false);

        // Send email
        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("sessionInfo", sessionInfo);
        payload2.put("sessionEvent", SessionState.CANCELED);
        payload2.put("cancelSessionUserId", endSessionReq.getCallingUserId());
        payload2.put("cancelSessionUserRole", endSessionReq.getCallingUserRole());
        payload2.put("coursePlanInfo", coursePlanInfo);
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.COURSE_PLAN_SESSION_EMAILS, payload2);
        asyncTaskFactory.executeTask(params2);

        return sessionInfo;
    }

    private void transferCancelSessionHours(TransferCancelSessionHoursReq req) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/transferCancelSessionHours",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
    }

    public void completeCoursePlanSession(SessionBillingDetails sessionBillingDetails) throws VException {
        if (SessionModel.TRIAL.equals(sessionBillingDetails.getModel())) {
            return;
        }
        logger.info("completeSession Request:" + sessionBillingDetails.toString());
        CompleteSessionRequest completeSessionRequest = new CompleteSessionRequest(null,
                sessionBillingDetails.getSessionScheduledDuration(), sessionBillingDetails.getBillingDuration(),
                Long.parseLong(sessionBillingDetails.getSessionId()));
        completeSessionRequest.setContextType(EntityType.COURSE_PLAN);
        completeSessionRequest.setContextId(sessionBillingDetails.getContextId());
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/completeCoursePlanSession",
                HttpMethod.POST, new Gson().toJson(completeSessionRequest));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String res = resp.getEntity(String.class);
        logger.info("completeSession Response:" + res);
    }

    public void unlockHours(List<UnlockHoursRequest> reqs) throws VException {
        logger.info("unlockHours Request:" + reqs.toString());
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/unlockHours",
                HttpMethod.POST, new Gson().toJson(reqs));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String res = resp.getEntity(String.class);
        logger.info("unlockHours Response:" + res);
    }

    private MultipleSessionScheduleReq createScheduleRequest(CoursePlanBasicInfo coursePlanInfo,
            List<SessionSlot> sessionSlots, SessionModel sessionModel, Long callingUserId) throws VException {
        MultipleSessionScheduleReq req = new MultipleSessionScheduleReq();
        List<Long> studentIds = new ArrayList<>();
        studentIds.add(coursePlanInfo.getStudentId());
        req.setStudentIds(studentIds);
        req.setTeacherId(coursePlanInfo.getTeacherId());
        req.setContextType(EntityType.COURSE_PLAN);
        req.setContextId(coursePlanInfo.getId());
        req.setSubject(coursePlanInfo.getSubject());
        req.setCallingUserId(callingUserId);
        req.setTitle(coursePlanInfo.getTitle());

        List<SessionSlot> futureSlots = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sessionSlots)) {
            for (SessionSlot slot : sessionSlots) {
                if (slot.getStartTime() != null && slot.getStartTime() > System.currentTimeMillis()) {
                    futureSlots.add(slot);
                }
            }
        }
        req.setSlots(futureSlots);
        req.setSessionModel(sessionModel);
        return req;
    }

    private List<ContentInfo> getContentInfosFromCurriculum(List<OTOCurriculumPojo> oTOCurriculumPojos) {
        List<ContentInfo> infos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(oTOCurriculumPojos)) {
            for (OTOCurriculumPojo oTOCurriculumPojo : oTOCurriculumPojos) {
                if (ArrayUtils.isNotEmpty(oTOCurriculumPojo.getContents())) {
                    infos.addAll(oTOCurriculumPojo.getContents());
                }
                if (ArrayUtils.isNotEmpty(oTOCurriculumPojo.getChildren())) {
                    infos.addAll(getContentInfosFromCurriculum(oTOCurriculumPojo.getChildren()));
                }
            }
        }
        return infos;
    }

    private PlatformBasicResponse markCoursePlanState(Long callingUserId, String coursePlanId,
            CoursePlanEnums.CoursePlanState state, CoursePlanEnums.CoursePlanEndType endType, String reason)
            throws VException {
        logger.info("marking the state of courseplan");
        MarkCoursePlanStateReq markCoursePlanStateReq = new MarkCoursePlanStateReq();
        markCoursePlanStateReq.setCallingUserId(callingUserId);
        markCoursePlanStateReq.setCoursePlanId(coursePlanId);
        markCoursePlanStateReq.setState(state);
        markCoursePlanStateReq.setEndType(endType);
        markCoursePlanStateReq.setReason(reason);
        ClientResponse markCoursePlanStateResp = WebUtils.INSTANCE.doCall(
                SUBSCRIPTION_ENDPOINT + "/courseplan/markCoursePlanState", HttpMethod.POST,
                gson.toJson(markCoursePlanStateReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(markCoursePlanStateResp);
        String markCoursePlanStateRes = markCoursePlanStateResp.getEntity(String.class);
        PlatformBasicResponse res = gson.fromJson(markCoursePlanStateRes, PlatformBasicResponse.class);

        // Push activity to leadsquared
        if (!StringUtils.isEmpty(res.getResponse())) {
            try {
                String response = res.getResponse();
                CoursePlanInfo coursePlanInfo = gson.fromJson(response, CoursePlanInfo.class);
                User student = fosUtils.getUserInfo(coursePlanInfo.getStudentId(), true);
                Map<String, String> lsParams = new HashMap<>();
                lsParams.put("userEmail", student.getEmail());
                lsParams.put("CoursePlanObject", response);
                LeadSquaredRequest leadReq = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA,
                        LeadSquaredDataType.LEAD_ACTIVITY_COURSEPLAN_UPDATE, student, lsParams, student.getId());
                leadSquaredManager.executeTask(leadReq);
            } catch (Exception ex) {
                logger.error("Error occured creating leadsquared activity for coursePlanUpdate");
            }
        }

        return res;
    }

    @Async
    public void checkTrialCoursePlan(int start, int size) throws VException {
        logger.info("ENTRY " + start + ", " + size);
        GetCoursePlansReq req = new GetCoursePlansReq();
        req.setOnlyBasicDetails(true);
        req.setStates(Arrays.asList(CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE));
        req.setStart(start);
        req.setSize(size);
        List<CoursePlanInfo> results = getCoursePlanInfos(req, true, true);
        if (ArrayUtils.isNotEmpty(results)) {
            Long currentTime = System.currentTimeMillis();
            // Long days7Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 7);
            // Long days8Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 8);
            Long day2Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 2);
            Long dayMillis = new Long(DateTimeUtils.MILLIS_PER_DAY);
            for (CoursePlanInfo coursePlanInfo : results) {
                Long dueTime = getFirstPaymentDate(coursePlanInfo);
                if (dueTime != null) {
                    Long diff = dueTime - currentTime;
                    try {
                        if (diff <= day2Millis && diff > dayMillis) { // before 1 day
                            communicationManager.sendTrialCoursePlanDueEmail(
                                    CommunicationType.COURSE_PLAN_TRIAL_DUE_1DAY_LEFT, coursePlanInfo, dueTime);
                        }
                    } catch (Exception e) {
                        logger.error("error in processing trial dues for  " + coursePlanInfo.getId() + ", error ", e);
                    }
                }
            }
            if (results.size() == size) {
                logger.info("fetching more trial dues ");
                checkTrialCoursePlan((size + start), size);
            }
        }
    }

    public Long getFirstPaymentDate(CoursePlanInfo coursePlanInfo) {
        Long dueTime = null;
        List<BaseInstalmentInfo> instalmentInfoList = coursePlanInfo.getInstalmentsInfo();
        if (instalmentInfoList != null && !instalmentInfoList.isEmpty()) {
            Collections.sort(instalmentInfoList, new Comparator<BaseInstalmentInfo>() {
                @Override
                public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                    return o1.getDueTime().compareTo(o2.getDueTime());
                }
            });

            dueTime = instalmentInfoList.get(0).getDueTime();

        }
        if (dueTime == null || dueTime == 0L) {
            dueTime = coursePlanInfo.getRegularSessionStartDate();
        }
        return dueTime;

    }

    public List<InstalmentInfo> prepareDiscountedInstalments(EnrollCoursePlanReq req)
            throws VException, CloneNotSupportedException {
        req.verify();
        String courseId = req.getCoursePlanId();

        logger.info("finding the amount to pay");
        CoursePlanInfo coursePlanInfo = getCoursePlan(courseId, false, false);
        if (ArrayUtils.isEmpty(coursePlanInfo.getInstalmentsInfo())) {
            throw new BadRequestException(ErrorCode.NO_BASE_INSTALMENTS_FOUND, "No base instalments found");
        }
        if (!coursePlanInfo.getStudentId().equals(req.getUserId())) {
            throw new ForbiddenException(ErrorCode.NOT_PART_OF_COURSE_PLAN,
                    req.getUserId() + " cannot pay for courseplan of student Id " + coursePlanInfo.getStudentId());
        }
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        int amountToPay = coursePlanInfo.getPrice();
        if (coursePlanInfo.getTrialRegistrationFee() != null) {
            amountToPay -= coursePlanInfo.getTrialRegistrationFee();
        }
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(courseId);
        buyItemsReqNew.setEntityType(EntityType.COURSE_PLAN);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setBaseInstalmentInfos(coursePlanInfo.getInstalmentsInfo());

        List<InstalmentInfo> instalmentInfos = paymentManager.prepareDiscountedInstalmentsCoursePlan(buyItemsReqNew);
        return instalmentInfos;
    }

    public PlatformBasicResponse addCoursePlanReqForm(CoursePlanReqFormReq req)
            throws VException, NotFoundException, Exception {
        req.verify();
        logger.info("sending email");
        Map<String, Object> payload = new HashMap<>();
        payload.put("coursePlanReqFormReq", req);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ADD_COURSE_PLAN_REQ_EMAIL, payload);
        asyncTaskFactory.executeTask(params);
        return new PlatformBasicResponse();
    }

    public void alertAboutFirstRegularSessionDate(int start, int size) throws VException, JSONException {
        Long currentTime = System.currentTimeMillis();
        GetCoursePlansReq req = new GetCoursePlansReq();
        req.setOnlyBasicDetails(true);
        int hoursBeforeToSendAlert = 6;
        req.setFromRegularSessionStartDate(currentTime + (hoursBeforeToSendAlert * DateTimeUtils.MILLIS_PER_HOUR));
        req.setTillRegularSessionStartDate(
                currentTime + ((hoursBeforeToSendAlert + 1) * DateTimeUtils.MILLIS_PER_HOUR));
        req.setStates(Arrays.asList(CoursePlanEnums.CoursePlanState.PUBLISHED,
                CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE,
                CoursePlanEnums.CoursePlanState.TRIAL_SESSIONS_DONE));
        req.setStart(start);
        req.setSize(size);
        List<CoursePlanInfo> plansToAlert = getCoursePlanInfos(req, false, true);

        if (ArrayUtils.isNotEmpty(plansToAlert)) {
            logger.info("sending email");
            Map<String, Object> _payload = new HashMap<>();
            CoursePlanAdminAlertReq coursePlanAdminAlertReq = new CoursePlanAdminAlertReq();
            coursePlanAdminAlertReq.setFirstPaymentApproachingPlans(plansToAlert);
            _payload.put("coursePlanAdminAlertReq", coursePlanAdminAlertReq);
            _payload.put("type", CommunicationType.COURSE_PLAN_FIRST_PAYMENT_DUE_APPROACHING_ALERT);
            AsyncTaskParams _params = new AsyncTaskParams(AsyncTaskName.COURSE_PLAN_ADMIN_ALERTS, _payload);
            asyncTaskFactory.executeTask(_params);
            if (plansToAlert.size() == size) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("start", (start + size));
                payload.put("size", size);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ALERT_ABOUT_FIRST_REGULAR_SESSION_DATE,
                        payload);
                asyncTaskFactory.executeTask(params);
            }
        }
    }

    public void alertNonTrialPayment(int start, int size) throws VException, JSONException {
        Long currentTime = System.currentTimeMillis();
        GetCoursePlansReq req = new GetCoursePlansReq();
        req.setOnlyBasicDetails(true);
        int hoursBeforeToSendAlert = 6;
        req.setFromStartDate(currentTime + (hoursBeforeToSendAlert * DateTimeUtils.MILLIS_PER_HOUR));
        req.setTillStartDate(currentTime + ((hoursBeforeToSendAlert + 1) * DateTimeUtils.MILLIS_PER_HOUR));
        req.setStates(Arrays.asList(CoursePlanEnums.CoursePlanState.PUBLISHED));
        req.setStart(start);
        req.setSize(size);
        List<CoursePlanInfo> plansToAlert = getCoursePlanInfos(req, false, true);

        if (ArrayUtils.isNotEmpty(plansToAlert)) {
            List<CoursePlanInfo> finalPlanAlerts = new ArrayList<>();
            for (CoursePlanInfo coursePlanInfo : plansToAlert) {
                if (coursePlanInfo.getStartDate() != null && coursePlanInfo.getRegularSessionStartDate() != null
                        && coursePlanInfo.getStartDate() < coursePlanInfo.getRegularSessionStartDate()) {
                    // a check to ensure there are trial sessions and regular sessions
                    finalPlanAlerts.add(coursePlanInfo);
                } else {
                    finalPlanAlerts.add(coursePlanInfo);
                }
            }
            if (!finalPlanAlerts.isEmpty()) {
                logger.info("sending email");
                Map<String, Object> _payload = new HashMap<>();
                CoursePlanAdminAlertReq coursePlanAdminAlertReq = new CoursePlanAdminAlertReq();
                coursePlanAdminAlertReq.setFirstPaymentApproachingPlans(plansToAlert);
                _payload.put("coursePlanAdminAlertReq", coursePlanAdminAlertReq);
                _payload.put("type", CommunicationType.COURSE_PLAN_TRIAL_PAYMENT_NOT_DONE_ALERT);
                AsyncTaskParams _params = new AsyncTaskParams(AsyncTaskName.COURSE_PLAN_ADMIN_ALERTS, _payload);
                asyncTaskFactory.executeTask(_params);
            }
            if (plansToAlert.size() == size) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("start", (start + size));
                payload.put("size", size);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ALERT_ABOUT_NON_TRIAL_PAYMENT, payload);
                asyncTaskFactory.executeTask(params);
            }
        }
    }

    public List<CoursePlanInfo> getCoursePlanInfosByCreationTime(ExportCoursePlansReq req, boolean exposeEmail) throws VException {
        req.verify();
        // TODO add access control
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/getByCreationTime?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<CoursePlanInfo>>() {
        }.getType();
        List<CoursePlanInfo> coursePlanInfos = gson.fromJson(jsonString, listType1);

        if (ArrayUtils.isNotEmpty(coursePlanInfos)) {
            Set<Long> userIds = new HashSet<>();
            for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
                userIds.add(coursePlanInfo.getStudentId());
                userIds.add(coursePlanInfo.getTeacherId());
            }
            Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, exposeEmail);
            for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
                if (usermap.containsKey(coursePlanInfo.getStudentId())) {
                    coursePlanInfo.setStudent(usermap.get(coursePlanInfo.getStudentId()));
                }
                if (usermap.containsKey(coursePlanInfo.getTeacherId())) {
                    coursePlanInfo.setTeacher(usermap.get(coursePlanInfo.getTeacherId()));
                }
            }
        }

        return coursePlanInfos;
    }

    public void handleSessionEvents(SessionEvents event, String sessionInfoMessage) {
        try {
            if (SessionEvents.SESSION_CANCELED.equals(event) || SessionEvents.SESSION_FORFEITED.equals(event)
                    || SessionEvents.SESSION_EXPIRED.equals(event)
                    || SessionEvents.SESSION_PAYOUT_CALCULATED.equals(event)) {
                SessionInfo sessionInfo = gson.fromJson(sessionInfoMessage, SessionInfo.class);
                if (sessionInfo != null && sessionInfo.getEndCoursePlan() != null
                        && Boolean.TRUE.equals(sessionInfo.getEndCoursePlan())) {
                    logger.info("Ending CoursePlan. Therefore not changing coursePlan State here. Id:" + sessionInfo.getContextId());
                    return;
                }
                if (EntityType.COURSE_PLAN.equals(sessionInfo.getContextType())
                        && SessionModel.TRIAL.equals(sessionInfo.getModel())) {
                    String coursePlanId = sessionInfo.getContextId();
                    CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanBasicInfo(coursePlanId, false, true);
                    if (CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE.equals(coursePlanBasicInfo.getState())) {
                        logger.info(
                                "event " + event + " for course plan " + coursePlanId + ", fetching trial sessions ");
                        GetSessionsReq getSessionsReq = new GetSessionsReq();
                        getSessionsReq.setModel(SessionModel.TRIAL);
                        getSessionsReq.setContextId(coursePlanId);
                        getSessionsReq.setIncludeAttendees(true);
                        List<SessionInfo> sessionInfos = sessionManager.getSessionsInfo(getSessionsReq);
                        if (ArrayUtils.isNotEmpty(sessionInfos)) {
                            boolean allSessionsDone = true;
                            for (SessionInfo _sessionInfo : sessionInfos) {
                                if (SessionState.SCHEDULED.equals(_sessionInfo.getState())
                                        || SessionState.STARTED.equals(_sessionInfo.getState())
                                        || SessionState.ACTIVE.equals(_sessionInfo.getState())) {
                                    allSessionsDone = false;
                                    break;
                                }
                            }
                            if (allSessionsDone) {
                                logger.info("sending an email to academics alerting");
                                markCoursePlanState(null, coursePlanId,
                                        CoursePlanEnums.CoursePlanState.TRIAL_SESSIONS_DONE, null, null);
                                Map<String, Object> _payload = new HashMap<>();
                                CoursePlanAdminAlertReq coursePlanAdminAlertReq = new CoursePlanAdminAlertReq();
                                coursePlanAdminAlertReq.setTrailDoneSessions(sessionInfos);
                                coursePlanAdminAlertReq.setTrialDoneCoursePlan(coursePlanBasicInfo);
                                _payload.put("coursePlanAdminAlertReq", coursePlanAdminAlertReq);
                                _payload.put("type", CommunicationType.COURSE_PLAN_TRAIL_SESSIONS_DONE);
                                AsyncTaskParams _params = new AsyncTaskParams(AsyncTaskName.COURSE_PLAN_ADMIN_ALERTS,
                                        _payload);
                                asyncTaskFactory.executeTask(_params);
                            } else {
                                logger.info("all sessions not done");
                            }
                        }
                    }
                }
            }
        } catch (JsonSyntaxException | VException | IllegalArgumentException | IllegalAccessException e) {
            logger.error("Error in handling  sessionevent  ", e);
        }
    }

    public Long getTrialDueDate(CoursePlanInfo coursePlanInfo) {
        Long dueTime = null;
        List<BaseInstalmentInfo> instalmentInfoList = coursePlanInfo.getInstalmentsInfo();
        if (instalmentInfoList != null && !instalmentInfoList.isEmpty()) {
            Collections.sort(instalmentInfoList, new Comparator<BaseInstalmentInfo>() {
                @Override
                public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                    return o1.getDueTime().compareTo(o2.getDueTime());
                }
            });

            dueTime = instalmentInfoList.get(0).getDueTime();

        }
        if (dueTime == null || dueTime == 0L) {
            dueTime = coursePlanInfo.getRegularSessionStartDate();
        }
        return dueTime;

    }

    public PlatformBasicResponse editInstalmentSchedule(EditInstalmentAmtAndSchedule req)
            throws VException, NotFoundException, Exception, CloneNotSupportedException {
        req.verify();
        if (req.getFromNextInstalmentSchedule() != null
                && ArrayUtils.isNotEmpty(req.getFromNextInstalmentSchedule().getSessionSlots())) {
            CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanBasicInfo(req.getCoursePlanId(), false, true);

            SessionSchedule allSlotsSchedule = new SessionSchedule();
            allSlotsSchedule.setSessionSlots(req.getFromNextInstalmentSchedule().getSessionSlots());
            allSlotsSchedule.setStartTime(CommonCalendarUtils.getDayStartTime(allSlotsSchedule.getStartDate()));
            checkConflicts(allSlotsSchedule, coursePlanBasicInfo.getStudentId(), coursePlanBasicInfo.getTeacherId(),
                    coursePlanBasicInfo.getId());
            checkConflicts(allSlotsSchedule, coursePlanBasicInfo.getStudentId(), coursePlanBasicInfo.getTeacherId(),
                    req.getCoursePlanId());

            ClientResponse resp1 = WebUtils.INSTANCE.doCall(
                    DINERO_ENDPOINT + "/payment/getInstalments?coursePlanId=" + req.getCoursePlanId(), HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
            String jsonString1 = resp1.getEntity(String.class);
            Type InstalType = new TypeToken<ArrayList<InstalmentInfo>>() {
            }.getType();
            List<InstalmentInfo> instalmentInfos = gson.fromJson(jsonString1, InstalType);
            Long nextDueTime = null;
            if (ArrayUtils.isNotEmpty(instalmentInfos)) {
                Collections.sort(instalmentInfos, new Comparator<InstalmentInfo>() {
                    @Override
                    public int compare(InstalmentInfo o1, InstalmentInfo o2) {
                        return o1.getDueTime().compareTo(o2.getDueTime());
                    }
                });
                for (InstalmentInfo instalmentInfo : instalmentInfos) {
                    if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())) {
                        nextDueTime = instalmentInfo.getDueTime();
                        break;
                    }
                }
            }
            req.setNextDueTime(nextDueTime);

            ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/editInstalmentSchedule",
                    HttpMethod.POST, gson.toJson(req));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            CoursePlanInfo coursePlanInfo = gson.fromJson(jsonString, CoursePlanInfo.class);

            SessionSchedule scheduleToBlock = coursePlanInfo.getRegularSessionSchedule();
            if (scheduleToBlock != null && ArrayUtils.isNotEmpty(scheduleToBlock.getSessionSlots())) {
                logger.info("blocking the calendar");
                SQSMessageType messageType = SQSMessageType.COURSE_PLAN_BLOCK_SLOTS;
                BlockCoursePlanSlotsPojo pojo = new BlockCoursePlanSlotsPojo(scheduleToBlock, coursePlanInfo.getStudentId(),
                        coursePlanInfo.getTeacherId(), coursePlanInfo.getId());
                awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo), coursePlanInfo.getId());

               /* Map<String, Object> payload = new HashMap<>();
                payload.put("sessionSchedule", scheduleToBlock);
                payload.put("studentId", coursePlanInfo.getStudentId());
                payload.put("teacherId", coursePlanInfo.getTeacherId());
                payload.put("coursePlanId", coursePlanInfo.getId());
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BLOCK_COURSE_PLAN_SLOTS, payload);
                asyncTaskFactory.executeTask(params);*/
            }

        }
        return new PlatformBasicResponse();
    }

    private void checkConflicts(SessionSchedule allSlotsSchedule, Long studentId, Long teacherId, String coursePlanId)
            throws ConflictException, VException, CloneNotSupportedException {
        logger.info("checking if all the slots are available");

        String getSlotConflictsUrl = SCHEDULING_ENDPOINT + "/calendarEntry/getSlotConflicts";
        GetSlotConflictReq getSlotConflictReq = new GetSlotConflictReq();
        getSlotConflictReq.setStudentId(studentId);
        getSlotConflictReq.setTeacherId(teacherId);
        getSlotConflictReq.setReferenceId(coursePlanId);
        long[] slotBitSetToCheckConflicts = SchedulingUtils.createSlotBitSetLongValues(allSlotsSchedule);
        allSlotsSchedule.setSlotBitSet(slotBitSetToCheckConflicts);
        allSlotsSchedule.setConflictSlotBitSet(slotBitSetToCheckConflicts);
        // cloning instalment schedule to send to getslotconflicts request
        SessionSchedule _sessionSchedule = (SessionSchedule) allSlotsSchedule.clone();
        Long noOfWeeks = ((allSlotsSchedule.getEndDate() - allSlotsSchedule.getStartDate())
                / CommonCalendarUtils.MILLIS_PER_WEEK);
        noOfWeeks++;// buffer
        _sessionSchedule.setNoOfWeeks(noOfWeeks);
        getSlotConflictReq.setSchedule(_sessionSchedule);

        logger.info("Getting slot conflicts for " + getSlotConflictReq);
        ClientResponse slotsConflictsResp = WebUtils.INSTANCE.doCall(getSlotConflictsUrl, HttpMethod.POST,
                new Gson().toJson(getSlotConflictReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(slotsConflictsResp);
        GetSlotConflictRes getSlotConflictRes = new Gson().fromJson(slotsConflictsResp.getEntity(String.class),
                GetSlotConflictRes.class);
        logger.info("Response from getSlotConflictsUrl: " + getSlotConflictRes);
        if (getSlotConflictRes != null && ArrayUtils.isNotEmpty(getSlotConflictRes.getConflictSessions())) {
            throw new ConflictException(ErrorCode.SLOT_CONFLICTS_FOUND,
                    gson.toJson(getSlotConflictRes.getConflictSessions()));
        }
    }

    public void updateDurationConflict(UpdateConflictDurationRequest updateConflictDurationRequest) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/updateDurationConflict",
                HttpMethod.POST, new Gson().toJson(updateConflictDurationRequest));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String res = resp.getEntity(String.class);
        logger.info("updateDurationConflict Response:" + res);
    }

    public List<CoursePlanHours> getActiveCoursePlanHours(long start, long size) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                SUBSCRIPTION_ENDPOINT + "courseplan/getActiveCoursePlanHours?start=" + start + "&size=" + size,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return gson.fromJson(jsonString, COURSE_PLAN_HOURS_TYPE);
    }

    public void verifyCoursePlanHoursConsistencyAsync() throws VException {
        logger.info("Invoking daily session task");
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_INCONSISTENCY_REPORT;
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, "{}");
        /*Map<String, Object> payload = new HashMap<>();
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.VERIFY_COURSE_PLAN_HOURS, payload);
        asyncTaskFactory.executeTask(params);*/
    }

    public void verifyCoursePlanHoursConsistency() throws VException, UnsupportedEncodingException {
        logger.info("Debug start for verifyCoursePlanHoursConsistency");
        int start = 0;
        int size = VERIFY_COURSE_PLAN_HOURS_FETCH_SIZE;

        long calculationStartTime = System.currentTimeMillis();
        List<CoursePlanHourInconsistentDetails> coursePlanHourInconsistentDetalsList = new ArrayList<>();
        for (;; start += size) {
            logger.info("-->start : " + start + " ~ size " + size + " <--");
            List<CoursePlanHours> coursePlanHours = getActiveCoursePlanHours(start, size);
            logger.info("coursePlanHours --> " + coursePlanHours);
            if (CollectionUtils.isEmpty(coursePlanHours)) {
                break;
            }

            GetSessionDurationByContextIdReq req = new GetSessionDurationByContextIdReq(EntityType.COURSE_PLAN);
            Map<String, CoursePlanHours> coursePlanHoursMap = new HashMap<>();

            coursePlanHours.forEach(c -> {
                req.addContextId(c.getCoursePlanId());
                coursePlanHoursMap.put(c.getCoursePlanId(), c);
            });
            logger.info("coursePlanHoursMap --> " + coursePlanHoursMap);
            logger.info("req --> " + req);

            try {
                ClientResponse cRes = WebUtils.INSTANCE.doCall(
                        SCHEDULING_ENDPOINT + "/session/getSessionDurationByContextId", HttpMethod.POST,
                        gson.toJson(req));
                VExceptionFactory.INSTANCE.parseAndThrowException(cRes);
                String jsonString = cRes.getEntity(String.class);
                List<ContextSessionDurationResponse> contextSessionDurationResponse = gson.fromJson(jsonString,
                        CONTEXT_SESSION_DURATION_TYPE);
                logger.info("contextSessionDurationResponse --> " + contextSessionDurationResponse);
                for (ContextSessionDurationResponse responseEntry : contextSessionDurationResponse) {
                    CoursePlanHours coursePlanEntry = coursePlanHoursMap.get(responseEntry.getContextId());
                    logger.info("responseEntry --> " + responseEntry);
                    logger.info("coursePlanEntry --> " + coursePlanEntry);
                    if (coursePlanEntry.getConsumedHours() != responseEntry.getEndedSessionDuration()
                            || coursePlanEntry.getLockedHours() != (responseEntry.getUpcomingDuration()
                            + responseEntry.getActiveSessionDuration())) {
                        coursePlanHourInconsistentDetalsList
                                .add(new CoursePlanHourInconsistentDetails(coursePlanEntry, responseEntry));
                    }
                }
                if (coursePlanHourInconsistentDetalsList.size() > VERIFY_COURSE_PLAN_HOURS_FETCH_SIZE) {
                    break;
                }
            } catch (Exception ex) {
                logger.error("Error occured verifying course plan hours - verifyCoursePlanHourError : " + req.toString()
                        + " ex:" + ex.getMessage());
            }

        }
        logger.info("coursePlanHourInconsistentDetalsList --> " + coursePlanHourInconsistentDetalsList);
        // Trigger email
        /*if (ArrayUtils.isNotEmpty(coursePlanHourInconsistentDetalsList)) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("errorCoursePlanEntries", coursePlanHourInconsistentDetalsList);
            payload.put("calculatedStartTime", calculationStartTime);
            payload.put("calculatedEndTime", System.currentTimeMillis());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.COURSE_PLAN_HOURS_INCONSISTENCY_REPORT, payload);
            asyncTaskFactory.executeTask(params);
        }*/
        communicationManager.sendCoursePlanInconsistentHours(coursePlanHourInconsistentDetalsList, calculationStartTime, System.currentTimeMillis());
        logger.info("Debug end for verifyCoursePlanHoursConsistency");
    }

    public List<GetCoursePlanReminderListRes> getCoursePlanTrialSessionList(GetCoursePlanReminderListReq req) throws IllegalArgumentException, IllegalAccessException, VException {
        if (ArrayUtils.isNotEmpty(req.getState()) && ReminderTimeType.TRIAL.equals(req.getTimeType())) {
            if (req.getState().contains(CoursePlanState.TRIAL_PAYMENT_DONE)
                    || req.getState().contains(CoursePlanState.TRIAL_SESSIONS_DONE)
                    || req.getState().contains(CoursePlanState.ENDED)
                    || req.getState().contains(CoursePlanState.ENROLLED)) {
                GetSessionsReq getSessionsReq = new GetSessionsReq();
                getSessionsReq.setModel(SessionModel.TRIAL);
                getSessionsReq.setBeforeStartTime(req.getEndTime());
                getSessionsReq.setAfterStartTime(req.getStartTime());
                if (ArrayUtils.isNotEmpty(req.getSessionStates())) {
                    getSessionsReq.setSessionStates(req.getSessionStates());
                }
                List<SessionInfo> sessionInfos = sessionManager.getSessionsInfo(getSessionsReq);
                List<String> coursePlanIds = new ArrayList<>();
                if (ArrayUtils.isNotEmpty(sessionInfos)) {
                    for (SessionInfo sessionInfo : sessionInfos) {
                        if (StringUtils.isNotEmpty(sessionInfo.getContextId())) {
                            coursePlanIds.add(sessionInfo.getContextId());
                        }
                    }
                    if (ArrayUtils.isEmpty(coursePlanIds)) {
                        return new ArrayList<>();
                    }
                    GetCoursePlanReminderListReq request = new GetCoursePlanReminderListReq();
                    request.setCoursePlanIds(coursePlanIds);
                    request.setState(req.getState());
                    List<GetCoursePlanReminderListRes> slist = getCoursePlanReminderListResForIds(request);

                    try {
                        getAgentIdFromLeadSquare(slist);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    return slist;

                } else {
                    return new ArrayList<>();
                }
            }
        }
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/getCoursePlanTrialSessionList?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String res = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<GetCoursePlanReminderListRes>>() {
        }.getType();
        List<GetCoursePlanReminderListRes> slist = gson.fromJson(res, listType);
        try {
            getAgentIdFromLeadSquare(slist);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return slist;
    }

    public List<GetCoursePlanReminderListRes> getCoursePlanReminderListResForIds(List<String> coursePlanIds) throws VException {
        if (ArrayUtils.isEmpty(coursePlanIds)) {
            return new ArrayList<>();
        }
        GetCoursePlanReminderListReq request = new GetCoursePlanReminderListReq();
        request.setCoursePlanIds(coursePlanIds);
        return getCoursePlanReminderListResForIds(request);
    }

    public List<GetCoursePlanReminderListRes> getCoursePlanReminderListResForIds(GetCoursePlanReminderListReq request) throws VException {

        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(request);
        ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/courseplan/getCoursePlansWithTrialSessions?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String res = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<GetCoursePlanReminderListRes>>() {
        }.getType();
        List<GetCoursePlanReminderListRes> slist = gson.fromJson(res, listType);

        return slist;
    }

    public void getAgentIdFromLeadSquare(List<GetCoursePlanReminderListRes> resps) throws BadRequestException, InterruptedException, ConflictException, VException {
        //TODO: Fetch Data from LeadSquared and student info
        if (ArrayUtils.isEmpty(resps)) {
            return;
        }
        List<Long> userIds = new ArrayList<>();
        List<Long> studentIds = new ArrayList<>();
        for (GetCoursePlanReminderListRes res : resps) {
            if (res.getStudentId() != null) {
                userIds.add(res.getStudentId());
                studentIds.add(res.getStudentId());
            }
            if (res.getTeacherId() != null) {
                userIds.add(res.getTeacherId());
            }
        }
        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        List<LeadDetailsSerialized> leadDetails = leadSquaredManager.getLeadDetails("mx_User_Id", studentIds);
        Map<Long, DashboardUserInstalmentHistoryRes> historyMap = paymentManager.getUserInstalmentTransactionHistory(studentIds);
        Map<Long, LeadDetailsSerialized> leadMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(leadDetails)) {
            for (LeadDetailsSerialized lead : leadDetails) {
                if (StringUtils.isNotEmpty(lead.getMx_User_Id()) && !"null".equalsIgnoreCase(lead.getMx_User_Id())) {
                    leadMap.put(Long.parseLong(lead.getMx_User_Id()), lead);
                }
            }
        }
        for (GetCoursePlanReminderListRes res : resps) {
            if (res.getStudentId() != null) {
                if (leadMap.containsKey(res.getStudentId())) {
                    res.setAgentName(leadMap.get(res.getStudentId()).getOwnerIdName());
                }
                if (userMap.containsKey(res.getStudentId())) {
                    res.setStudent(userMap.get(res.getStudentId()));
                }
                DashboardUserInstalmentHistoryRes history = historyMap.get(res.getStudentId());
                if (history != null) {
                    res.setExtraInfos(history);
                }
            }
            if (res.getTeacherId() != null) {
                if (userMap.containsKey(res.getTeacherId())) {
                    res.setTeacher(userMap.get(res.getTeacherId()));
                }
            }
        }
    }

    public static final class CoursePlanHourInconsistentDetails {

        private String coursePlanId;

        private Integer existingConsumedHours;// millis
        private Integer existingLockedHours;// millis

        private Integer actualEndedSessionDuration;
        private Integer actualLockedHours;
        private Integer actualActiveSessionDuration;

        public CoursePlanHourInconsistentDetails(CoursePlanHours coursePlanHours,
                ContextSessionDurationResponse contextSessionDurationResponse) {
            super();
            this.coursePlanId = coursePlanHours.getCoursePlanId();
            this.existingConsumedHours = coursePlanHours.getConsumedHours();
            this.existingLockedHours = coursePlanHours.getLockedHours();

            this.actualLockedHours = contextSessionDurationResponse.getUpcomingDuration();
            this.actualEndedSessionDuration = contextSessionDurationResponse.getEndedSessionDuration();
            this.actualActiveSessionDuration = contextSessionDurationResponse.getActiveSessionDuration();
        }

        public String getCoursePlanId() {
            return coursePlanId;
        }

        public void setCoursePlanId(String coursePlanId) {
            this.coursePlanId = coursePlanId;
        }

        public Integer getExistingConsumedHours() {
            return existingConsumedHours;
        }

        public void setExistingConsumedHours(Integer existingConsumedHours) {
            this.existingConsumedHours = existingConsumedHours;
        }

        public Integer getExistingLockedHours() {
            return existingLockedHours;
        }

        public void setExistingLockedHours(Integer existingLockedHours) {
            this.existingLockedHours = existingLockedHours;
        }

        public Integer getActualLockedHours() {
            return actualLockedHours;
        }

        public void setActualLockedHours(Integer actualLockedHours) {
            this.actualLockedHours = actualLockedHours;
        }

        public Integer getActualEndedSessionDuration() {
            return actualEndedSessionDuration;
        }

        public void setActualEndedSessionDuration(Integer actualEndedSessionDuration) {
            this.actualEndedSessionDuration = actualEndedSessionDuration;
        }

        public Integer getActualActiveSessionDuration() {
            return actualActiveSessionDuration;
        }

        public void setActualActiveSessionDuration(Integer actualActiveSessionDuration) {
            this.actualActiveSessionDuration = actualActiveSessionDuration;
        }

        @Override
        public String toString() {
            return "CoursePlanHourInconsistentDetails [coursePlanId=" + coursePlanId + ", existingConsumedHours="
                    + existingConsumedHours + ", existingLockedHours=" + existingLockedHours
                    + ", actualEndedSessionDuration=" + actualEndedSessionDuration + ", actualLockedHours="
                    + actualLockedHours + ", actualActiveSessionDuration=" + actualActiveSessionDuration + "]";
        }
    }
}
