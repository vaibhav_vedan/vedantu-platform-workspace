/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.response.UserLeadInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.notification.requests.SMSPriorityType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.dao.DoubtsAppVersionDAO;
import com.vedantu.platform.dao.MiscDAO;
import com.vedantu.platform.dao.cms.WebinarUserRegistrationInfoDAO;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.platform.dao.social.ViewDAO;
import com.vedantu.platform.enums.DoubtAppType;
import com.vedantu.platform.managers.notification.SMSManager;
import com.vedantu.platform.mongodbentities.DoubtsAppVersion;
import com.vedantu.platform.mongodbentities.LeadData;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.platform.request.misc.RegisterWebinarUserReq;
import com.vedantu.platform.request.misc.WebinarUserRegistrationInfoReq;
import com.vedantu.platform.response.ActiveTabsRes;
import com.vedantu.platform.response.AppRatingConfigParamsRes;
import com.vedantu.platform.response.DexConfigRes;
import com.vedantu.platform.response.DoubtsAppVersionRes;
import com.vedantu.util.*;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.fos.request.AbstractFrontEndUserReq;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;

/**
 *
 * @author ajith
 */
@Service
public class MiscManager {

    @Autowired
    LogFactory logFactory;

    Logger logger = LogFactory.getLogger(MiscManager.class);

    @Autowired
    private MiscDAO miscDAO;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    
    @Autowired
    private DoubtsAppVersionDAO doubtsAppVersionDAO;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private WebinarUserRegistrationInfoDAO webinarUserRegistrationInfoDAO;

    @Autowired
    private ViewDAO viewDAO;

    private final Gson gson = new Gson();
    @Autowired
    private DozerBeanMapper mapper;
    private static final String URL_SHORTNER_API_KEY = ConfigUtils.INSTANCE.getStringValue("google.url.shortner.api.key");
    private static Map<String, String> MASTER_CLASS_MSGS_MAP = new HashMap<>();
    private static Map<String, Integer> MASTER_CLASS_MSGS_COUNT_MAP = new HashMap<>();
    private static final String[] MASTER_CLASS_MSGS = {
        //        "You are successfully registered for the Online Session on Sun, 14th Jan at 7pm. Complete other steps and find more details of the session here ",
        //        "You have been successfully registered for the Online Session on Sun, 14th Jan at 7pm. You can ask questions for Sarvesh here ",
        //        "You have been successfully registered for the Online Session on Sun, 14th Jan at 7pm. Find more details of the event here ",
        "Thank you for showing interest for FREE Online Webinar on Sun, 21st Jan at 7pm. To confirm and book your FREE seat, click here "
    };

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final String DINERO_ENDPOINT_PLAT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT_PLAT");

    private static final String messageString = "Your registration is Confirmed for Online Webinar with IIT AIR 1 Sarvesh Mehtani on Sun, 21st Jan at 7PM. You can attend it from the following link on Sunday : ";

    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    public MiscManager() {

        for (int k = 1; k <= MASTER_CLASS_MSGS.length; k++) {
            MASTER_CLASS_MSGS_MAP.put("msg" + k, MASTER_CLASS_MSGS[k - 1]);
            MASTER_CLASS_MSGS_COUNT_MAP.put("msg" + k, 0);
        }
    }

    public PlatformBasicResponse addmobileno(String mobileno, String campaign) {
        LeadData leadData = new LeadData();
        leadData.setMobileno(mobileno);
        leadData.setCampaign(campaign);
        leadData.setSource("EDUCHAT");
        miscDAO.create(leadData);
        return new PlatformBasicResponse();
    }

    public void registerWebinar(WebinarUserRegistrationInfoReq req) throws JSONException, BadRequestException {
        req.verify();
        WebinarUserRegistrationInfo webinarUserRegistrationInfo = mapper.map(req, WebinarUserRegistrationInfo.class);
        logger.info("Request:" + webinarUserRegistrationInfo.toString());

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(webinarUserRegistrationInfo.getUserId(), true);
        RegisterWebinarUserReq registerWebinarUserReq = new RegisterWebinarUserReq(webinarUserRegistrationInfo);
        registerWebinarUserReq.setEmail(userBasicInfo.getEmail());
        registerWebinarUserReq.setPhone(userBasicInfo.getContactNumber());
        ClientResponse respOTFBatch = WebUtils.INSTANCE.doCall(getRegisterWebinarUrl(webinarUserRegistrationInfo.getTrainingId()), HttpMethod.POST,
                gson.toJson(registerWebinarUserReq), false);
        String jsonRespOTFBatch = respOTFBatch.getEntity(String.class);
        logger.info(jsonRespOTFBatch);
        JSONObject jsonObject = new JSONObject(jsonRespOTFBatch);

        webinarUserRegistrationInfo.updateRegisterResponse(jsonObject);
        miscDAO.createWebinarRegInfo(webinarUserRegistrationInfo);
        logger.info(" saved entity:" + webinarUserRegistrationInfo.toString());
    }

    public PlatformBasicResponse missedcalltrigger(String mobileno,
            String target, String smstext) throws Exception {
        if (StringUtils.isNotEmpty(target)) {
            target = target.toUpperCase();
            switch (target) {
                case "MASTERCLASS_WEBINAR":
                    String baseUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
                    String prefixMsgKey = "msg1";
                    String utmVal = "1";
                    for (int k = 1; k <= MASTER_CLASS_MSGS.length; k++) {
                        if (MASTER_CLASS_MSGS_MAP.get("msg" + k) != null
                                && MASTER_CLASS_MSGS_COUNT_MAP.get("msg" + k)
                                < MASTER_CLASS_MSGS_COUNT_MAP.get(prefixMsgKey)) {
                            prefixMsgKey = "msg" + k;
                            utmVal = k + "";
                        }
                    }
                    MASTER_CLASS_MSGS_COUNT_MAP.put(prefixMsgKey,
                            MASTER_CLASS_MSGS_COUNT_MAP.get(prefixMsgKey) + 1);
                    String link = baseUrl + "/iit-jee/jee-main-crash-courses?leadform=true&phonecode=91&contactnumber="
                            + mobileno + "&utm_source=s" + utmVal;
                    String shortenedUrl = shortenUrl(link);
                    String prefixMsg = MASTER_CLASS_MSGS_MAP.get(prefixMsgKey);
                    smstext = prefixMsg + shortenedUrl;
                    break;
            }
        }

        if (StringUtils.isNotEmpty(smstext)) {
            TextSMSRequest textSMSRequest = new TextSMSRequest();
            textSMSRequest.setBody(smstext);
            textSMSRequest.setTo(mobileno);
            smsManager.sendSMS(textSMSRequest);
        } else {
            logger.error("empty smstext for target " + target + ", mobile " + mobileno);
        }

        return new PlatformBasicResponse();
    }

    private static String getRegisterWebinarUrl(String traningId) {
        // https://globalattspa.gotowebinar.com/api/webinars/150586194716221441/registrants?requireLicense=true&client=spa
        return "https://globalattspa.gotowebinar.com/api/webinars/" + traningId
                + "/registrants?requireLicense=true&client=spa";
    }

    private String shortenUrl(String url) {
        String finalUrl = url;
        if (StringUtils.isNotEmpty(url)) {
            try {
                String urlShortner = "https://www.googleapis.com/urlshortener/v1/url?key="
                        + URL_SHORTNER_API_KEY;
                JSONObject json = new JSONObject();
                json.put("longUrl", url);
                ClientResponse urlshortresp = WebUtils.INSTANCE.doCall(urlShortner, HttpMethod.POST,
                        json.toString(), false);
                VExceptionFactory.INSTANCE.parseAndThrowException(urlshortresp);
                String urlshortrespStr = urlshortresp.getEntity(String.class);
                logger.info("respone of urlshortner " + urlshortrespStr);
                JSONObject responseObj = new JSONObject(urlshortrespStr);
                if (!StringUtils.isEmpty(responseObj.getString("id"))) {
                    finalUrl = responseObj.getString("id");
                }
            } catch (ClientHandlerException | UniformInterfaceException | VException | JSONException e) {
                //swallow
                logger.info("error in creating shortened url " + e.getMessage());
            }
        }
        return finalUrl;
    }

    public void sendMessageToLeads(String checkedNumber) throws VException {
        if (StringUtils.isNotEmpty(checkedNumber)) {
            String smstext = getMessageText(checkedNumber);
            sendMessage(checkedNumber, smstext);
        } else {
            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_BULK_MESSAGE_TO_LEADS, payload);
            asyncTaskFactory.executeTask(params);
        }
    }

    public void sendMessageToRegisteredLeads() throws VException {

        String url = USER_ENDPOINT + "/userLeads/getUserLeadsNumber?key=jsfvjnfvn#$omds";
        ClientResponse incResp = WebUtils.INSTANCE.doCall(url,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);
        String jsonString = incResp.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<String>>() {
        }.getType();
        List<String> mobiles = gson.fromJson(jsonString, listType1);

        if (ArrayUtils.isNotEmpty(mobiles)) {
            for (String mobile : mobiles) {
                sendLeadRegistrationMessage(mobile);
            }
        }
    }

    public void sendLeadRegistrationMessage(String mobile) throws VException {
        String smstext = getMessageText(mobile);
        sendMessage(mobile, smstext);
    }

    public String getMessageText(String mobile) {
//    	String baseUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
//    	String link = baseUrl + "/iit-jee/jee-main-crash-courses?leadform=false&phonecode=91&contactnumber="
//                + mobile;
//        String shortenedUrl = shortenUrl(link);
//        String smstext = messageString + shortenedUrl + "  Team Vedantu";
        String smstext = "Your registration is confirmed for 'Tips & Tricks to Solve Kinematics Problems 3x Faster' Master Class, on Thu, 28th June at 9PM. You can attend the class on Thursday by clicking on this link: https://vdnt.in/2VP7m";
        return smstext;
    }

    public void sendUserLeadMessage(UserLeadInfo info) throws VException {
        if (info != null) {
            String smsText = null;
            if ("CAMPAIGN_WEBINAR_MOLECONCEPT".equalsIgnoreCase(info.getSource())) {
                smsText = "Your registration is confirmed for 'Tricks to Solve Questions on Mole Concept for JEE & NEET' Master Class, on Sun, 8th July at 9 PM. You can attend the class on Sunday by clicking on this link: https://vdnt.in/2VTr4";
            } else if ("CAMPAIGN_WEBINAR_FUNCTIONS".equalsIgnoreCase(info.getSource())) {
                smsText = "Your registration is confirmed for 'How to Visualise Functions to Solve Questions 3X Faster for IIT-JEE' Master Class, on Tue, 10th July at 9 PM. You can attend the class on Tuesday by clicking on this link: https://vdnt.in/2VTr5";
            } else if ("CAMPAIGN_WEBINAR_KINEMATICS_2".equalsIgnoreCase(info.getSource())) {
                smsText = "Your registration is confirmed for 'Amazing Shortcuts to Solve 2D Kinematics Problems for JEE & NEET' Master Class, on Sun, 15th July at 7 PM. You can attend the class on Sunday by clicking on this link: https://vdnt.in/2VTrB";
            }

            if (StringUtils.isNotEmpty(smsText) && StringUtils.isNotEmpty(info.getContactNumber())) {
                sendMessage(info.getContactNumber(), smsText);
            }
        }
    }

    public void sendMessage(String mobileno, String messageText) throws VException {

        if (StringUtils.isNotEmpty(mobileno) && StringUtils.isNotEmpty(messageText)) {
            TextSMSRequest textSMSRequest = new TextSMSRequest();
            textSMSRequest.setBody(messageText);
            textSMSRequest.setTo(mobileno);
            textSMSRequest.setPriorityType(SMSPriorityType.HIGH);
            smsManager.sendSMS(textSMSRequest);
        } else {
            logger.info("empty mobile " + mobileno);
        }
    }

    public Map<String, Object> getipinformation(AbstractFrontEndUserReq req) throws VException {
        String ipAddress = req.getIpAddress();
        if (StringUtils.isEmpty(ipAddress)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "ip address not found in req:" + req);
        }
        try{
            Type type = new TypeToken<Map<String, Object>>() {
            }.getType();
            String jsonString = ipUtil.fetchFromApiGurus(ipAddress, true);
            Map<String, Object> response = gson.fromJson(jsonString, type);
            if (response != null && response.containsKey("geolocation_data")) {
                Map<String, Object> test = (Map) response.get("geolocation_data");
                if (test.containsKey("country_code_iso3166alpha2")) {
                    String country = (String) test.get("country_code_iso3166alpha2");
                    if (StringUtils.isNotEmpty(country)) {
                        String code = ConfigUtils.INSTANCE.getStringValue("countrycode.iso3166." + country);
    //                    if(StringUtils.isEmpty(code)){
    //                        logger.error("calling code not found for:"+test);
    //                    }
                        test.put("callingCode", code);
                    }
                }
            }
            return response;
        }catch(Exception e){
            logger.warn("exception in fetching location info "+e.getMessage());
            //swallow
        }
        Map<String, Object> res = new HashMap<>();
        res.put("geolocation_data", new HashMap<>());
        return res;
    }
    
    public DoubtsAppVersionRes getLatestDoubtAppVersion(String currentVersion, DoubtAppType doubtAppType) throws NotFoundException{
        DoubtsAppVersion doubtsAppVersion = doubtsAppVersionDAO.getLatest(currentVersion, doubtAppType);
        
        DoubtsAppVersionRes doubtsAppVersionRes = new DoubtsAppVersionRes();
        doubtsAppVersionRes.setVersionName(doubtsAppVersion.getVersionName());
        doubtsAppVersionRes.setForceUpdate(doubtsAppVersion.isToBeForceUpdated());
//        if(doubtsAppVersion.getToBeForceUpdated().contains(currentVersion)){
//            doubtsAppVersionRes.setForceUpdate(true);
//        }
        return doubtsAppVersionRes;
    }
    
    public void addDoubtsAppVersion(DoubtsAppVersion doubtsAppVersion){

        doubtsAppVersionDAO.create(doubtsAppVersion);
    }

    public AppRatingConfigParamsRes getAppRatingConfigParams(String userId, String appVersionCode, RequestSource requestSource) throws VException {
//        UserBasicInfo ubi = fosUtils.getUserBasicInfo(userId, false);

        User ubi = fosUtils.getUserInfo(Long.valueOf(userId), false);

        logger.info("APP-VERSION-CODE: " + appVersionCode);


        String doubtYesFeedbackKey = "MOBILE_APP_DOUBT_YES_FEEDBACK";
        String videoLikeKey = "MOBILE_APP_VIDEO_LIKE";
        String pdfSaveKey = "MOBILE_APP_PDF_SAVE";

        AppRatingConfigParamsRes res = new AppRatingConfigParamsRes();

        //keep +1 of the actual value the product team says
        try {
            String doubtYesFeedbackValue = redisDAO.get(doubtYesFeedbackKey);
            String videoLikeValue = redisDAO.get(videoLikeKey);
            String pdfSaveValue = redisDAO.get(pdfSaveKey);

            res.setDoubtYesFeedback(Integer.parseInt(doubtYesFeedbackValue));
            res.setVideoLike(Integer.parseInt(videoLikeValue));
            res.setPdfSave(Integer.parseInt(pdfSaveValue));

        } catch (Exception e) {
            res.setDoubtYesFeedback(3);
            res.setVideoLike(3);
            res.setPdfSave(3);
            logger.error("Error in redis get for MOBILE APP RATING CONFIG " + e.getMessage());
        }

        String activeTabsKey;
        if(ubi !=null && ubi.getStudentInfo() != null && StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
            activeTabsKey = getActiveTabsForGrade(ubi.getStudentInfo().getGrade());
        } else {
            activeTabsKey = "MOBILE_APP_ACTIVE_TABS_GRADE_ALL";
        }

        try {
            String value = redisDAO.get(activeTabsKey);
            logger.info("MOBILE_APP_ACTIVE_TABS_GRADE value: " + value);
            List<String> tabs = Arrays.asList(value.split(","));
            res.setActiveTabs(tabs);
        } catch (Exception e) {
            String value = "DOUBTS,CLASSROOM,STUDY,TEST";
            List<String> tabs = Arrays.asList(value.split(","));
            res.setActiveTabs(tabs);
//            logger.error("Error in redis get for MOBILE_APP_ACTIVE_TABS_GRADE " + e.getMessage());
        }

//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
//        calendar.set(Calendar.YEAR, 2019);
//        calendar.set(Calendar.MONTH, Calendar.APRIL);
//        calendar.set(Calendar.DAY_OF_MONTH, 19);
//        calendar.set(Calendar.HOUR_OF_DAY, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        Long dateTime = calendar.getTime().getTime();
//
//
//        Calendar calendar2 = Calendar.getInstance();
//        calendar2.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
//        calendar2.set(Calendar.YEAR, 2019);
//        calendar2.set(Calendar.MONTH, Calendar.MAY);
//        calendar2.set(Calendar.DAY_OF_MONTH, 10);
//        calendar2.set(Calendar.HOUR_OF_DAY, 0);
//        calendar2.set(Calendar.MINUTE, 0);
//        calendar2.set(Calendar.SECOND, 0);
//        calendar2.set(Calendar.MILLISECOND, 0);
//        Long dateTime2 = calendar2.getTime().getTime();


//        logger.info("DATE-TIME-CREATION-CHECK");
//        logger.info(dateTime);
//        logger.info(ubi.getCreationTime());
//        if(dateTime > ubi.getCreationTime()) {
//            String value = "DOUBTS,CLASSROOM,STUDY,COURSES";
//            List<String> tabs = Arrays.asList(value.split(","));
//            res.setActiveTabs(tabs);
//        }
//        if(ubi.getCreationTime() > dateTime) {
//            if(StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
//                String grade = ubi.getStudentInfo().getGrade();
//                if(grade.equals("11") || grade.equals("12") || grade.equals("13")) {
//                    try {
//                        String url = DINERO_ENDPOINT_PLAT + "/payment/isDoubtsPaidUser?";
//
//                        url = url + "userId=" + userId;
//
//                        logger.info("PAID-USER-DINERO-URL: " + url);
//
//                        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
//                        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//                        String respString = resp.getEntity(String.class);
//
//                        Type filterListType = new TypeToken<Map<String, Boolean>>() {
//                        }.getType();
//
//                        Map<String, Boolean> userInDoubt = new Gson().fromJson(respString, filterListType);
//                        logger.info("PAID-USER-DINERO-VERIFIED");
//                        logger.info(userInDoubt);
//
//                        if (userInDoubt.get("isPaid") != null && userInDoubt.get("isPaid") == true) {
//                            String value = "DOUBTS,CLASSROOM,STUDY,TEST,COURSES";
//                            List<String> tabs = Arrays.asList(value.split(","));
//                            res.setActiveTabs(tabs);
//                        } else {
//                            String value = "DOUBTS,CLASSROOM,STUDY,TEST,COURSES";
//                            List<String> tabs = Arrays.asList(value.split(","));
//                            res.setActiveTabs(tabs);
//                        }
//                    } catch (VException vex) {
//                        String value = "DOUBTS,CLASSROOM,STUDY,TEST,COURSES";
//                        List<String> tabs = Arrays.asList(value.split(","));
//                        res.setActiveTabs(tabs);
//                    }
//                }
//            }
//        }

//        if(ubi.getCreationTime() > dateTime2) {
//            if(StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
//                String grade = ubi.getStudentInfo().getGrade();
////                if(grade.equals("6") || grade.equals("7") || grade.equals("8") || grade.equals("9") || grade.equals("10")) {
//                    try {
//                        String url = DINERO_ENDPOINT_PLAT + "/payment/isDoubtsPaidUser?";
//
//                        url = url + "userId=" + userId;
//
//                        logger.info("PAID-USER-DINERO-URL: " + url);
//
//                        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
//                        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//                        String respString = resp.getEntity(String.class);
//
//                        Type filterListType = new TypeToken<Map<String, Boolean>>() {
//                        }.getType();
//
//                        Map<String, Boolean> userInDoubt = new Gson().fromJson(respString, filterListType);
//                        logger.info("PAID-USER-DINERO-VERIFIED");
//                        logger.info(userInDoubt);
//
//                        if (userInDoubt.get("isPaid") != null && userInDoubt.get("isPaid") == true) {
//                            String value = "DOUBTS,CLASSROOM,STUDY,TEST,COURSES";
//                            List<String> tabs = Arrays.asList(value.split(","));
//                            res.setActiveTabs(tabs);
//                        } else {
//                            String value = "CLASSROOM,STUDY,TEST,COURSES";
//                            List<String> tabs = Arrays.asList(value.split(","));
//                            res.setActiveTabs(tabs);
//                        }
//                    } catch (VException vex) {
//                        String value = "CLASSROOM,STUDY,TEST,COURSES";
//                        List<String> tabs = Arrays.asList(value.split(","));
//                        res.setActiveTabs(tabs);
//                    }
////                }
//            }
//        }

        int total = 0;
        if(StringUtils.isNotEmpty(appVersionCode)) {
            String[] a = appVersionCode.split("\\.");
            if(a.length == 3) {
                total = Integer.parseInt(a[0]) * 100 + Integer.parseInt(a[1]) * 10 + Integer.parseInt(a[2]);
            }
        }

        logger.info("TOTAL-APP-VERSION-CODE: " + total);
        if(total >= 131) {
            if(total >= 170){
                String nValue = "DOUBTS,STUDY,PLAY,MORE";
                if (total >= 173)
                    nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
                List<String> ntabs = Arrays.asList(nValue.split(","));
                res.setActiveTabs(ntabs);

            }else{
                String nValue = "DOUBTS,CLASSROOM,COURSES,STUDY,TEST";
                List<String> ntabs = Arrays.asList(nValue.split(","));
                res.setActiveTabs(ntabs);
            }
        } else {
            String nValue = "CLASSROOM,COURSES,STUDY,TEST";
            List<String> ntabs = Arrays.asList(nValue.split(","));
            res.setActiveTabs(ntabs);
        }

        String paidUsersString = redisDAO.get("PAID_USERS");
        List<String> paidUsersList = new ArrayList<>();
        if(StringUtils.isNotEmpty(paidUsersString)) {
            paidUsersList = Arrays.asList(paidUsersString.split(","));
            if(ArrayUtils.isNotEmpty(paidUsersList) && paidUsersList.contains(userId)) {
                if(total >= 170){
                    String nValue = "DOUBTS,STUDY,PLAY,MORE";
                    if (total >= 173)
                        nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
                    List<String> ntabs = Arrays.asList(nValue.split(","));
                    res.setActiveTabs(ntabs);
                }else{
                    String value = "DOUBTS,CLASSROOM,COURSES,STUDY,TEST";
                    List<String> tabs = Arrays.asList(value.split(","));
                    res.setActiveTabs(tabs);
                }

            }
        }

        if(RequestSource.IOS.equals(requestSource) && (total < 141)) {
            String nValue = "STUDY";
            List<String> ntabs = Arrays.asList(nValue.split(","));
            res.setActiveTabs(ntabs);
        }

        // For Trials
        if(StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
            String grade = ubi.getStudentInfo().getGrade();
            
        //For 1.7.0 app version onwards the the bottom tabs are getting renamed-> now Study tab is actually the courses tab of previous versions    
            if(grade.equals("1") || grade.equals("2") || grade.equals("3") || grade.equals("4") || grade.equals("5")) {
            	if(total >= 170){
            		 String value = "STUDY,MORE";
                     List<String> tabs = Arrays.asList(value.split(","));
                     res.setActiveTabs(tabs);
                }else{
                	//Hence UI needs courses in response for backward compatibility of versions
                    String value = "COURSES";
                    List<String> tabs = Arrays.asList(value.split(","));
                    res.setActiveTabs(tabs);
                }
               
            }
        }

        return res;
    }

    public ActiveTabsRes getActiveTabs(String userId, String appVersionCode, RequestSource requestSource) throws VException {
//        UserBasicInfo ubi = fosUtils.getUserBasicInfo(userId, false);

        User ubi = fosUtils.getUserInfo(Long.valueOf(userId), false);

        logger.info("APP-VERSION-CODE: " + appVersionCode);

        ActiveTabsRes res = new ActiveTabsRes();

        String activeTabsKey;
        String moreMenuKey;
        if(ubi !=null && ubi.getStudentInfo() != null && StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
            activeTabsKey = getActiveTabsForGrade(ubi.getStudentInfo().getGrade());
            moreMenuKey = getMoreMenuForGrade(ubi.getStudentInfo().getGrade());
        } else {
            activeTabsKey = "MOBILE_APP_ACTIVE_TABS_GRADE_ALL";
            moreMenuKey = "MOBILE_APP_MORE_MENU_GRADE_ALL";
        }

//        try {
//            String value = redisDAO.get(activeTabsKey);
//            logger.info("MOBILE_APP_ACTIVE_TABS_GRADE value: " + value);
//            List<String> tabs = Arrays.asList(value.split(","));
//            res.setActiveTabs(tabs);
//        } catch (Exception e) {
//            String value = "DOUBTS,CLASSROOM,STUDY,TEST";
//            List<String> tabs = Arrays.asList(value.split(","));
//            res.setActiveTabs(tabs);
////            logger.error("Error in redis get for MOBILE_APP_ACTIVE_TABS_GRADE " + e.getMessage());
//        }

        int total = 0;
        if(StringUtils.isNotEmpty(appVersionCode)) {
            String[] a = appVersionCode.split("\\.");
            if(a.length == 3) {
                total = Integer.parseInt(a[0]) * 100 + Integer.parseInt(a[1]) * 10 + Integer.parseInt(a[2]);
            }
        }

        logger.info("TOTAL-APP-VERSION-CODE: " + total);
        if(total >= 131) {
            if(total >= 170){


//                String nValue = redisDAO.get(activeTabsKey);
//                String moreMenu = redisDAO.get(moreMenuKey);
//                logger.info("MOBILE_APP_ACTIVE_TABS_GRADE value: " + nValue);
//                logger.info("MOBILE_APP_MORE_MENU_GRADE value: " + moreMenu);
//
//                if(nValue == null){
//                    nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
//                }
//                if(moreMenu == null){
//                    moreMenu = "CLASSROOM,TEST,PDF,MY_CONTENT";
//                }

//                List<String> ntabs = Arrays.asList(nValue.split(","));
//                List<String> moreMenuList = Arrays.asList(moreMenu.split(","));
//                res.setActiveTabs(ntabs);
//                res.setMoreMenu(moreMenuList);

                String nValue = "DOUBTS,STUDY,PLAY,MORE";
                String moreMenu=""; // Default values are hardcoded in app before 1.7.4
                if (total >= 173)
                    nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
                if (total >= 174){
                    // From 1.7.4 onwards more menu is configurable
                    nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
                    moreMenu = "CLASSROOM,TEST,PDF,MY_CONTENT";
                }
                List<String> ntabs = Arrays.asList(nValue.split(","));
                List<String> moreMenuList = Arrays.asList(moreMenu.split(","));
                res.setActiveTabs(ntabs);
                res.setMoreMenu(moreMenuList);

            }else{
                String nValue = "DOUBTS,CLASSROOM,COURSES,STUDY,TEST";
                List<String> ntabs = Arrays.asList(nValue.split(","));
                res.setActiveTabs(ntabs);
            }
        } else {
            String nValue = "CLASSROOM,COURSES,STUDY,TEST";
            List<String> ntabs = Arrays.asList(nValue.split(","));
            res.setActiveTabs(ntabs);
        }

        String paidUsersString = redisDAO.get("PAID_USERS");
        List<String> paidUsersList = new ArrayList<>();
        if(StringUtils.isNotEmpty(paidUsersString)) {
            paidUsersList = Arrays.asList(paidUsersString.split(","));
            if(ArrayUtils.isNotEmpty(paidUsersList) && paidUsersList.contains(userId)) {
                if(total >= 170){
                    String moreMenu="";
                    String nValue = "DOUBTS,STUDY,PLAY,MORE";
                    if (total >= 173)
                        nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
                    if (total >= 174) {
                        nValue = "DOUBTS,STUDY,PLAY,GROUPS,MORE";
                        moreMenu = "CLASSROOM,TEST,PDF,MY_CONTENT";
                    }
                    List<String> ntabs = Arrays.asList(nValue.split(","));
                    List<String> moreMenuList = Arrays.asList(moreMenu.split(","));
                    res.setActiveTabs(ntabs);
                    res.setMoreMenu(moreMenuList);
                }else{
                    String value = "DOUBTS,CLASSROOM,COURSES,STUDY,TEST";
                    List<String> tabs = Arrays.asList(value.split(","));
                    res.setActiveTabs(tabs);
                }

            }
        }

        if(RequestSource.IOS.equals(requestSource) && (total < 141)) {
            String nValue = "STUDY";
            List<String> ntabs = Arrays.asList(nValue.split(","));
            res.setActiveTabs(ntabs);
        }

        // For Trials
        if(StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
            String grade = ubi.getStudentInfo().getGrade();

            //For 1.7.0 app version onwards the the bottom tabs are getting renamed-> now Study tab is actually the courses tab of previous versions
            if(grade.equals("1") || grade.equals("2") || grade.equals("3") || grade.equals("4") || grade.equals("5")) {
                if(total >= 170){
                    String value = "DOUBTS,STUDY,MORE";
                    String moreMenu = "";
                    List<String> tabs = Arrays.asList(value.split(","));
                    List<String> moreMenuList = Arrays.asList(moreMenu.split(","));
                    res.setActiveTabs(tabs);
                    res.setMoreMenu(moreMenuList);
                }else{
                    //Hence UI needs courses in response for backward compatibility of versions
                    String value = "COURSES";
                    List<String> tabs = Arrays.asList(value.split(","));
                    res.setActiveTabs(tabs);
                }

            }
        }

        return res;
    }

    public DexConfigRes getDexConfigs() {
        DexConfigRes res = new DexConfigRes();
        // minutes
        try {
            String v1 = redisDAO.get("MOBILE_APP_CHAT_TIMEOUT");
            res.setChatTimeout(Integer.parseInt(v1));
        } catch (Exception e) {
            res.setChatTimeout(5);
            logger.error("Error in redis get for CHAT TIMEOUT " + e.getMessage());
        }

        try {
            String v1 = redisDAO.get("MOBILE_APP_POOL_TIMEOUT");
            res.setPoolTimeout(Long.parseLong(v1));
        } catch (Exception e) {
            res.setPoolTimeout(Long.parseLong("900"));
            logger.error("Error in redis get for POOL TIMEOUT " + e.getMessage());
        }

        return res;
    }

    public String getMoreMenuForGrade(String grade) {
        if(grade.equals("1")) {
            return "MOBILE_APP_MORE_MENU_GRADE_1";
        }
        if(grade.equals("2")) {
            return "MOBILE_APP_MORE_MENU_GRADE_2";
        }
        if(grade.equals("3")) {
            return "MOBILE_APP_MORE_MENU_GRADE_3";
        }
        if(grade.equals("4")) {
            return "MOBILE_APP_MORE_MENU_GRADE_4";
        }
        if(grade.equals("5")) {
            return "MOBILE_APP_MORE_MENU_GRADE_5";
        }
        if(grade.equals("6")) {
            return "MOBILE_APP_MORE_MENU_GRADE_6";
        }
        if(grade.equals("7")) {
            return "MOBILE_APP_MORE_MENU_GRADE_7";
        }
        if(grade.equals("8")) {
            return "MOBILE_APP_MORE_MENU_GRADE_8";
        }
        if(grade.equals("9")) {
            return "MOBILE_APP_MORE_MENU_GRADE_9";
        }
        if(grade.equals("10")) {
            return "MOBILE_APP_MORE_MENU_GRADE_10";
        }
        if(grade.equals("11")) {
            return "MOBILE_APP_MORE_MENU_GRADE_11";
        }
        if(grade.equals("12")) {
            return "MOBILE_APP_MORE_MENU_GRADE_12";
        }
        if(grade.equals("13")) {
            return "MOBILE_APP_MORE_MENU_GRADE_13";
        }
        else {
            return null;
        }
    }

    public String getActiveTabsForGrade(String grade) {
        if(grade.equals("1")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_1";
        }
        if(grade.equals("2")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_2";
        }
        if(grade.equals("3")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_3";
        }
        if(grade.equals("4")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_4";
        }
        if(grade.equals("5")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_5";
        }
        if(grade.equals("6")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_6";
        }
        if(grade.equals("7")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_7";
        }
        if(grade.equals("8")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_8";
        }
        if(grade.equals("9")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_9";
        }
        if(grade.equals("10")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_10";
        }
        if(grade.equals("11")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_11";
        }
        if(grade.equals("12")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_12";
        }
        if(grade.equals("13")) {
            return "MOBILE_APP_ACTIVE_TABS_GRADE_13";
        }
        else {
            return null;
        }
    }

    public Boolean showPopup(String userId, Long releaseTime, String appVersionCode)
    {
        if (appVersionCode.compareTo("1.6.2") >= 0) {
            boolean satisfiesWebinarCriteria = satisfiesWebinarCriteria(userId, releaseTime);
            boolean satisfiesTestCriteria = satisfiesTestCriteria(userId, releaseTime);
            boolean satisfiesPdfCriteria = satisfiesPdfCriteria(userId, releaseTime);
            return (satisfiesWebinarCriteria || satisfiesTestCriteria || satisfiesPdfCriteria);
        }
        else
            return false;
    }

    private boolean satisfiesWebinarCriteria(String userId, Long releaseDate)
    {
        Long webinarsAttended = webinarUserRegistrationInfoDAO.getWebinarUserRegInfoByUserId(userId, releaseDate);
        logger.info("Webinars Attended : {}", webinarsAttended);
        return  (webinarsAttended > 0 && webinarsAttended % 3 == 0);
    }

    private boolean satisfiesTestCriteria(String userId, Long releaseDate) {
        try {
            String url = LMS_ENDPOINT + "/cmds/contentInfo/getRecentlyAttemptedTests?userId=" + userId + "&releaseDate=" + releaseDate;
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, false);
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            String resString = response.getEntity(String.class);
            Long testCount = new Gson().fromJson(resString, Long.class);
            logger.info("Tests attempted : {}", testCount);
            return (testCount > 0 && testCount % 3 == 0);
        }
        catch (Exception e) {
            //logger.error("Could not get test attempt info for userId : {} after date : {}", userId, releaseDate);
            return false;
        }
    }

    private boolean satisfiesPdfCriteria(String userId, Long releaseDate)
    {
        Long pdfsViewed = viewDAO.getPdfViewsByUserId(userId, releaseDate);
        logger.info("Pdfs viewed : {}", pdfsViewed);
        return (pdfsViewed > 0 && pdfsViewed % 3 == 0);
    }
}
