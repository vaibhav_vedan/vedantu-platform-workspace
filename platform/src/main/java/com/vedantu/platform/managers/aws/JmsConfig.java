package com.vedantu.platform.managers.aws;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.platform.listeners.ClevarTapSQSListener;
import com.vedantu.platform.listeners.SQSListener;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/getting-started.html
 * https://dzone.com/articles/aws-sqs-and-spring-jmsintegration
 *
 */
@Configuration
@EnableJms
public class JmsConfig {

    @Autowired
    private ClevarTapSQSListener clevarTapSQSListener;

    @Autowired
    private SQSListener sqsListener;

    @Autowired
    private AuditSQSListener auditSQSListener;

    private SQSConnection connection;

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;

    private Set<SQSQueue> queueListToCreateAlarm = new HashSet<>();

    private static final Logger logger = LogManager.getRootLogger();

    @Autowired
    private LeadsquaredConsumerCreationManager jmsConfigLeadSquaredDependencyDestructor;

    final SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
            .withRegion(Region.getRegion(Regions.EU_WEST_1))
            .withNumberOfMessagesToPrefetch(10).build();
    final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    final boolean usesqs = (!(StringUtils.isEmpty(env) || env.equals("LOCAL")));

    private List<DefaultMessageListenerContainer> dmlcs = new ArrayList<>();

    @Bean
    public SQSConnection sqsConnection() throws JMSException {

        if (usesqs) {
            connection = sqsConnectionFactory.createConnection();
            Session session = connection.createSession(false, SQSSession.CLIENT_ACKNOWLEDGE);

            //sometimes if the actual LS queue gets clogged, we pull from that queue and push to this non fifo queue for removing clogging
            if(StringUtils.isNotEmpty(env) && env.equalsIgnoreCase("PROD")) {
                MessageConsumer dmlcLeadSquare2 = session.createConsumer(session.createQueue("leadsquared_prod_test"));
                dmlcLeadSquare2.setMessageListener(sqsListener);
            }

            //Add queues to create alarm
            queueListToCreateAlarm.add(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE);
            queueListToCreateAlarm.add(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE_DL);
            queueListToCreateAlarm.add(SQSQueue.SEND_GPS_LOCALS_TO_LS);
            queueListToCreateAlarm.add(SQSQueue.SEND_GPS_LOCALS_TO_LS_DL);
            queueListToCreateAlarm.add(SQSQueue.OTO_POSTSESSION_OPS);
            queueListToCreateAlarm.add(SQSQueue.OTO_POSTSESSION_OPS_DL);
            queueListToCreateAlarm.add(SQSQueue.LEADSQUARED_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.LEADSQUARED_QUEUE_DL);
            queueListToCreateAlarm.add(SQSQueue.CLEVERTAP_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.AUDIT_QUEUE);

            connection.start();
            awsCloudWatchManager.createAlarms(queueListToCreateAlarm);
            logger.info("JmsConfig Bean created");

            return connection;
        } else {
            return null;
        }
    }

    @Bean
    public DefaultMessageListenerContainer seoPageQueueListenerContainer2() {

        return generateDMLC(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer gpsListenerContainer2() {
        return generateDMLC(SQSQueue.SEND_GPS_LOCALS_TO_LS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer otopostsessionopsListenerContainer2() {
        return generateDMLC(SQSQueue.OTO_POSTSESSION_OPS, sqsListener);
    }

   /* @Bean
    public DefaultMessageListenerContainer lsListenerContainer2() {

        return generateDMLC(SQSQueue.LEADSQUARED_QUEUE, sqsListener);
    }*/

/*    @Bean
    public DefaultMessageListenerContainer clevertapListenerContainer2() {
        return generateDMLC(SQSQueue.CLEVERTAP_QUEUE, clevarTapSQSListener);
    }*/

    @Bean
    public DefaultMessageListenerContainer auditSQSListenerContainer2() {
        return generateDMLC(SQSQueue.AUDIT_QUEUE, auditSQSListener);
    }

    public DefaultMessageListenerContainer generateDMLC(SQSQueue queue, MessageListener _sqsListener) {
        if (usesqs) {
            logger.info("creating dmlc for " + queue);

            DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
            dmlc.setConnectionFactory(sqsConnectionFactory);
            dmlc.setDestinationName(queue.getQueueName(env));
            dmlc.setMessageListener(_sqsListener);
            dmlc.setConcurrency(queue.getMaxConcurrency());//dmlc.setConcurrency("3-10");
            dmlc.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
            dmlcs.add(dmlc);

            return dmlc;
        } else {
            return null;
        }
    }

    @PreDestroy
    public void cleanUp() {

        try {
            if (connection != null) {
                connection.close();
            }
            if (ArrayUtils.isNotEmpty(dmlcs)) {
                dmlcs.stream().filter((dmlc) -> (dmlc != null)).forEachOrdered((dmlc) -> {
                    logger.info("destroying dmlc for " + dmlc.getDestinationName());
                    dmlc.destroy();
                });
            }
            com.amazonaws.http.IdleConnectionReaper.shutdown();
            AwsSdkMetrics.unregisterMetricAdminMBean();


        } catch (JMSException e) {
            logger.error("Error in closing sqs connection ", e);
        }

    }

}
