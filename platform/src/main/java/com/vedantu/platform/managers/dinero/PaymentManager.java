/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.dinero;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import com.vedantu.User.User;
import com.vedantu.User.UserInfo;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.pojo.*;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.response.*;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.onetofew.request.GetOTFSessionsListReq;
import com.vedantu.platform.managers.aws.AwsSQSManager;
import com.vedantu.platform.pojo.courseplan.ProcessInstallmentForCoursePlanPojo;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.scheduling.pojo.session.ReferenceType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.util.enums.*;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.request.MarkRegistrationStatusReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.managers.subscription.SubscriptionManager;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.managers.LeadSquaredManager;
import com.vedantu.platform.managers.ReferralManager;
import com.vedantu.platform.managers.onetofew.EnrollmentManager;
import com.vedantu.platform.managers.onetofew.OTFBundleManager;
import com.vedantu.platform.managers.onetofew.OTFManager;
import com.vedantu.platform.managers.subscription.BundleManager;
import com.vedantu.platform.managers.subscription.BundlePackageManager;
import com.vedantu.platform.managers.subscription.CoursePlanManager;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.platform.mongodbentities.LeadSquaredAgent;
import com.vedantu.platform.request.dinero.OnPaymentReceiveReq;
import com.vedantu.dinero.request.RechargeReq;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.request.GetRegisteredCoursesReq;
import com.vedantu.subscription.request.GetStructuredCoursesReq;
import com.vedantu.subscription.request.ResetInstallmentStateReq;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.GetCoursePlanReminderListRes;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;

import org.json.JSONException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author ajith
 */
@Service
public class PaymentManager {

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private ReferralManager referralManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentManager.class);

    @Autowired
    private PaymentAsyncManager paymentAsyncManager;

    @Autowired
    private OTFManager otfManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private UserManager userManager;

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private BundlePackageManager bundlePackageManager;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private LeadSquaredManager leadSquaredManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private final Gson gson = new Gson();

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    public PaymentManager() {
        super();
    }

    private final String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    public void performDineroTasks(int start, int size) throws VException {
        paymentAsyncManager.checkInstalmentDues(start, size);
        coursePlanManager.checkTrialCoursePlan(start, size);
        Map<String, Object> payload = new HashMap<>();
        payload.put("start", start);
        payload.put("size", size);
        AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.PASS_EXPIRY_CRON_TASK, payload);
        couponManager.checkPassExpiration(start, size);
    }

    public Map<Long, List<Orders>> getOrdersBySubscriptionIds(List<Long> ids) throws VException {
        logger.info("Entering " + ids.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getOrdersBySubscriptionIds?ids=";
        if (ids.size() >= 1) {
            url += ids.get(0);
            for (int i = 1; i < ids.size(); i++) {
                url += "&ids=" + ids.get(i);
            }
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);

        Type mapType = new TypeToken<Map<Long, List<Orders>>>() {
        }.getType();

        Map<Long, List<Orders>> response = gson.fromJson(jsonString, mapType);
        logger.info("EXIT " + response);
        return response;
    }

    public Orders getOrderBySubscriptionId(Long id) throws VException {
        List<Long> ids = new ArrayList<>();
        ids.add(id);
        Orders order = null;
        Map<Long, List<Orders>> response = getOrdersBySubscriptionIds(ids);
        if (response != null && response.get(id) != null) {
            List<Orders> orders = response.get(id);
            if (ArrayUtils.isNotEmpty(orders)) {
                return orders.get(0);
            }
        }
        return order;
    }

    public List<InstalmentInfo> getInstalments(String orderId) throws VException {
        logger.info("Entering " + orderId);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getInstalments?orderId=" + orderId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);

        Type mapType = new TypeToken<List<InstalmentInfo>>() {
        }.getType();

        List<InstalmentInfo> response = gson.fromJson(jsonString, mapType);
        logger.info("EXIT " + response);
        return response;
    }

    public OrderInfo buyItems(BuyItemsReq buyItemsReq) throws VException, IOException, AddressException {
        buyItemsReq.verify();

        if (buyItemsReq.getCallingUserRole().equals(Role.ADMIN) || buyItemsReq.getCallingUserRole().equals(Role.TEACHER)
                || buyItemsReq.getCallingUserRole().equals(Role.STUDENT_CARE)) {
            if (buyItemsReq.getUserId() != null) {
                buyItemsReq.setCallingUserId(buyItemsReq.getUserId());
                buyItemsReq.setCallingUserRole(Role.STUDENT);
            }
        }
        // If a student calls the api, the user id must for the student calling the api
        if(null != httpSessionUtils.getCurrentSessionData() && httpSessionUtils.isStudent(httpSessionUtils.getCurrentSessionData())){
            if(!buyItemsReq.getUserId().equals(httpSessionUtils.getCallingUserId())){
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Operation not allowed");
            }
        }

        OrderItem item_temp = buyItemsReq.getItems().get(0);
        if (item_temp.getEntityType() != null && ((EntityType.OTF.equals(item_temp.getEntityType()))
                || (EntityType.OTF_BATCH_REGISTRATION.equals(item_temp.getEntityType())))
                || (EntityType.OTF_COURSE_REGISTRATION.equals(item_temp.getEntityType()))) {
            //throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Forbidden call to buy items");
        }

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/buyItems", HttpMethod.POST,
                gson.toJson(buyItemsReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        OrderInfo response = gson.fromJson(jsonString, OrderInfo.class);
        logger.info("OrderInfo response is {}",response);

        logger.info("Performing operations after buy items");
        if (!response.getNeedRecharge()) {
            logger.info("In case no reacharge is necessary.");
            if(StringUtils.isNotEmpty(buyItemsReq.getCampaign())){
                response.setCampaign(buyItemsReq.getCampaign());
            }

            performBuyItemsEmailEnrollTask(response);
            try {
                // User user = userManager.getUserById(buyItemsReq.getUserId());
                // logger.info(user.toString());
                // if (user.getReferrerCode() != null) {
                if (buyItemsReq.getItems().get(0).getModel() != null) {
                    if (buyItemsReq.getItems().get(0).getModel().equals(SessionModel.OTF)) {
                        referralManager.processReferralBonusAsync(ReferralStep.FIRST_OTF_PURCHASE,
                                buyItemsReq.getCallingUserId());
                    } else {
                        // referralManager.processReferralBonusAsync(ReferralStep.FIRST_PURCHASE,
                        // buyItemsReq.getCallingUserId());
                    }
                }
                // }
            } catch (Exception e) {
                logger.error("processReferralBonus", e);
            }
        }

        try {
            if (buyItemsReq.getItems().get(0).getModel() != null
                    && buyItemsReq.getItems().get(0).getModel().equals(SessionModel.OTF)) {
                userManager.incOtfSubscriptionCount(buyItemsReq.getCallingUserId());
            }
        } catch (Exception e) {
            logger.error("otf subscription count update", e);
        }

        if (!response.getNeedRecharge()) {
            try {
                List<OrderItemInfo> items = response.getItems();
                for (OrderItemInfo item : items) {
                    if (item.getModel() != null && (item.getModel().equals(SessionModel.IL)
                            || item.getModel().equals(SessionModel.TRIAL) || (item.getModel().equals(SessionModel.OTO)
                            && item.getSubModel() != null && item.getSubModel().equals(SubModel.LOOSE)))) {
                        Map<String, Object> payload = new HashMap<>();
                        payload.put("sessionInfo", null);
                        payload.put("subscriptionId", response.getContextId());
                        payload.put("role", httpSessionUtils.getCurrentSessionData().getRole());
                        payload.put("model", item.getModel());
                        payload.put("subModel", item.getSubModel());
                        payload.put("scheduled", true);
                        payload.put("grade", item.getGrade());
                        payload.put("target", item.getTarget());
                        payload.put("note", item.getNote());
                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_SESSION_EMAIL, payload);
                        asyncTaskFactory.executeTask(params);
                    }
                }
            } catch (Exception e) {
                logger.error("Exception : ", e);
            }
        }

        logger.info("Exiting: " + response.toString());

        return response;

    }

    private void performBuyItemsEmailEnrollTask(OrderInfo orderInfo) throws VException, AddressException {

        logger.info("performBuyItemsEmailEnrollTask {}", orderInfo);

        User userBasicInfo = fosUtils.getUserInfo(orderInfo.getUserId(), true);
        if (userBasicInfo == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user in the order mentioned not found");
        }

        for (OrderItemInfo item : orderInfo.getItems()) {
            if(null == item){
                return;
            }
            switch (item.getEntityType()) {
                case OTF_BATCH_REGISTRATION:
                case OTF:
                    EnrollmentState state = EnrollmentState.REGULAR;
                    if (EntityType.OTF_BATCH_REGISTRATION.equals(item.getEntityType())) {
                        state = EnrollmentState.TRIAL;
                    }
                    OTFEnrollmentReq oTFEnrollmentReq = new OTFEnrollmentReq(userBasicInfo.getUserId().toString(),
                            item.getEntityId(), null, null, userBasicInfo.getRole(), "ACTIVE", null, state);
                    EnrollmentPojo oTFEnrollmentInfo = null;
                    try {
                        oTFEnrollmentInfo = enrollmentManager.enrollment(oTFEnrollmentReq);
                    } catch (VException e) {
                        reverseOrderTransaction(orderInfo.getId());
                        throw e;
                    }
                    if (oTFEnrollmentInfo != null) {
                        logger.info("Enrollment done for User:" + orderInfo.getUserId() + ", Batch Id:" + item.getEntityId());
                        String courseId = oTFEnrollmentInfo.getCourse().getId();
                        updateOrderReferenceTag(orderInfo.getId(), courseId);

                        logger.info("sending email");
                        Map<String, Object> payload = new HashMap<>();
                        payload.put("enrollmentInfo", oTFEnrollmentInfo);
                        payload.put("orderInfo", orderInfo);
                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_ENROLLMENT_EMAIL, payload);
                        asyncTaskFactory.executeTask(params);

                        logger.info("updating the deliverable entityId");
                        Map<String, Object> payload2 = new HashMap<>();
                        payload2.put("orderId", orderInfo.getId());
                        payload2.put("enrollmentId", oTFEnrollmentInfo.getEnrollmentId());
                        if (oTFEnrollmentInfo.getBatch() != null) {
                            payload2.put("batchId", oTFEnrollmentInfo.getBatch().getBatchId());
                        }
                        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.POST_OTF_UPDATE_DELIVERABLE_ID,
                                payload2);
                        asyncTaskFactory.executeTask(params2);

                        if (EntityType.OTF_BATCH_REGISTRATION.equals(item.getEntityType())) {
                            MarkRegistrationStatusReq markRegistrationStatusReq = new MarkRegistrationStatusReq();
                            markRegistrationStatusReq.setEntityId(oTFEnrollmentInfo.getBatchId());
                            markRegistrationStatusReq.setEntityType(EntityType.OTF_BATCH_REGISTRATION);
                            markRegistrationStatusReq.setUserId(userBasicInfo.getId());
                            markRegistrationStatusReq.setNewstatus("ENROLLED");
                            markRegistrationStatusReq.setDeliverableEntityIds(Arrays.asList(oTFEnrollmentInfo.getId()));
                            markRegistrationStatusReq.setOldStatuses(Arrays.asList("REGISTERED"));
                            ClientResponse resp1 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/registration/markRegistrationStatus",
                                    HttpMethod.POST, new Gson().toJson(markRegistrationStatusReq));
                            VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
                        }
                    }
                    break;
                case PLAN:
                case INSTALEARN:
                    Long subscriptionId = orderInfo.getContextId();
                    // Keep in mind this is Subscription pojo
                    SubscriptionResponse subscriptionResponse = subscriptionManager
                            .getSubscriptionBasicInfo(subscriptionId);
                    /*
				 * GetSubscriptionRequest getSubscriptionRequest = new
				 * GetSubscriptionRequest();
				 * getSubscriptionRequest.setSubscriptionId(subscriptionId);
				 * SubscriptionResponseVO subscriptionResponse =
				 * NewSubscriptionManager
				 * .getSubscription(getSubscriptionRequest).getSubscriptionsList
				 * ().get(0);
				 *
                     */
                    logger.info("Recevied subscription" + subscriptionResponse.toString());
                    if (subscriptionResponse.getModel().equals(SessionModel.OTO)) {
                        if (subscriptionResponse.getSubModel() != null
                                && subscriptionResponse.getSubModel().equals(SubModel.LOOSE)) {
                        } else if (subscriptionResponse.getSubModel().equals(SubModel.SESSIONS3CHALLENGE)) {
                            // not used as of now
                            // emailManager.sendSchedule3SessionChallengeMail(subscriptionResponse);
                        } else {
                            Map<String, Object> payload = new HashMap<>();
                            payload.put("sessionInfo", null);
                            payload.put("subscriptionId", subscriptionResponse.getId());
                            payload.put("role", httpSessionUtils.getCurrentSessionData().getRole());
                            payload.put("model", item.getModel());
                            payload.put("subModel", item.getSubModel());
                            payload.put("scheduled", true);
                            payload.put("grade", item.getGrade());
                            payload.put("target", item.getTarget());
                            payload.put("note", item.getNote());
                            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_SESSION_EMAIL, payload);
                            asyncTaskFactory.executeTask(params);
                        }
                    }
                    break;
                case OTF_COURSE_REGISTRATION: {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("userId", orderInfo.getUserId());
                    payload.put("courseId", item.getEntityId());
                    payload.put("amount", orderInfo.getAmount());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_REGISTRATION_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                    break;
                }
                case OTO_COURSE_REGISTRATION: {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("userId", orderInfo.getUserId());
                    payload.put("courseId", item.getEntityId());
                    payload.put("amount", orderInfo.getAmount());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTO_REGISTRATION_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                    break;
                }
                case BUNDLE:
                case BUNDLE_TRIAL:
                    logger.info("auto enrolling in bundle courses");
                    if(orderInfo.getPurchasingEntities().get(0).getDeliverableId() != null) {
                        Map<String, Object> bp = new HashMap<>();
                        bp.put("userId", orderInfo.getUserId());
                        bp.put("entityId", item.getEntityId());
                        bp.put("enrollmentId", orderInfo.getPurchasingEntities().get(0).getDeliverableId());
                        sqsManager.sendToSQS(SQSQueue.BUNDLE_NEW_ENROLL_OPS, SQSMessageType.AUTO_ENROLL_NEW_ENROLLMENT, gson.toJson(bp), "ENROLL" + orderInfo.getPurchasingEntities().get(0).getDeliverableId());
                        //AsyncTaskParams bptask = new AsyncTaskParams(AsyncTaskName.BUNDLE_AUTO_ENROLL, bp);
                        //asyncTaskFactory.executeTask(bptask);
                    }
                    logger.info("Sending email to internal team if necessary");
                    sendEmailToInternalTeamIfNecessary(orderInfo,userBasicInfo);

                case BUNDLE_PACKAGE:
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("userId", orderInfo.getUserId());
                    payload.put("entityId", item.getEntityId());
                    payload.put("entityType", item.getEntityType());
                    if(StringUtils.isNotEmpty(orderInfo.getCampaign())) {
                        payload.put("campaign", orderInfo.getCampaign());
                    }
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BUNDLE_ENROLLMENT_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                    break;
                default:
                    break;
            }
        }
    }

    private void sendEmailToInternalTeamIfNecessary(OrderInfo orderInfo, User userBasicInfo) throws AddressException, VException {

        logger.info("sendEmailToInternalTeamIfNecessary");

        if(Objects.isNull(orderInfo.getPurchaseContextId())){
            BundleInfo bundleInfo=bundleManager.getBundleBasicInfo(orderInfo.getItems().get(0).getEntityId());
            logger.info("sendEmailToInternalTeamIfNecessary - bundleInfo - {}",bundleInfo);
            if(Objects.nonNull(bundleInfo) && bundleInfo.isEarlyLearning()){
                GetOrdersReq getOrdersReq=new GetOrdersReq();
                getOrdersReq.setUserId(orderInfo.getUserId());
                getOrdersReq.setEntityId(orderInfo.getPurchasingEntities().get(0).getEntityId());
                List<Orders> orders = getOrders(getOrdersReq);
                if(ArrayUtils.isNotEmpty(orders)){
                    logger.info("Finding the order to be taken...");
                    Orders order = orders.stream()
                                        .filter(currentOrder -> PaymentStatus.PAID.equals(currentOrder.getPaymentStatus()))
                                        .peek(logger::info)
                                        .findFirst()
                                        .orElse(null);
                    if(Objects.nonNull(order)){
                        StringBuffer stringBuffer=new StringBuffer();
                        stringBuffer.append("<strong>Customer's registered Email ID -</strong> "+userBasicInfo.getEmail()+"<br />");
                        stringBuffer.append("<strong>Customer's registered Number - </strong>"+userBasicInfo.getContactNumber()+"<br />");
                        if(Objects.nonNull(userBasicInfo.getStudentInfo()) &&
                                ArrayUtils.isNotEmpty(userBasicInfo.getStudentInfo().getParentInfos())){
                            stringBuffer.append("<strong>Parent's Name - </strong>"+userBasicInfo.getStudentInfo().getParentInfos().get(0).getFirstName()+"<br />");
                        }
                        stringBuffer.append("<strong>Student's Name - </strong>"+userBasicInfo.getFirstName()+"<br />");
                        stringBuffer.append("<strong>AIO/BundleId - </strong>"+bundleInfo.getId()+"<br />");
                        stringBuffer.append("<strong>AIO/Bundle - </strong>"+bundleInfo.getTitle()+"<br />");
                        stringBuffer.append("<strong>CourseId - </strong>"+bundleInfo.getBundleEntityInfos().get(0).getCourseId()+"<br />");
                        stringBuffer.append("<strong>Course Title - </strong>"+bundleInfo.getBundleEntityInfos().get(0).getCourseTitle()+"<br />");
                        stringBuffer.append("<strong>BatchId - </strong>"+bundleInfo.getBundleEntityInfos().get(0).getEntityId()+"<br />");

                        OrderedItem orderedItem = order.getItems().get(0);
                        stringBuffer.append("<strong>Amount Paid - </strong>"+ orderedItem.getCost()/100 +"<br />");
                        if(ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())){
                            stringBuffer.append("<strong>Coupon code used - </strong>"+ orderInfo.getOrderedItems().get(0).getVedantuDiscountCouponId() +"<br />");
                        }

                        EmailRequest emailRequest = new EmailRequest();
                        emailRequest.setSubject("Order confirmation mail for early learning");
                        emailRequest.setBody(stringBuffer.toString());
                        emailRequest.setTo(Arrays.asList(
                                new InternetAddress("super.kids@vedantu.com")
                        ));
                        emailRequest.setType(CommunicationType.EARLY_LEARNING_INTERNAL_PURPOSE);
                        logger.info("Email request inside sendEmailToInternalTeamIfNecessary is {}",emailRequest);
                        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, gson.toJson(emailRequest));

                        try {
                            Map<String, String> userLead = new HashMap<>();
                            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, String.valueOf(orderInfo.getUserId()));
                            userLead.put(LeadSquaredRequest.Constants.COURSE_ID, bundleInfo.getBundleEntityInfos().get(0).getCourseId());
                            userLead.put(LeadSquaredRequest.Constants.AIO_ID, bundleInfo.getId());
                            userLead.put(LeadSquaredRequest.Constants.BATCH_ID, bundleInfo.getBundleEntityInfos().get(0).getEntityId());
                            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Payment Completed");
                            Map<String, Object> payload = new HashMap<>();
                            payload.put("userLeads", userLead);
                            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.GET_DEMO_SESSION_SUPERKIDS, payload);
                            asyncTaskFactory.executeTask(params);
                        } catch (Exception e) {
                            logger.error("LeadSquared Event fail In course payment"+e.getMessage());
                        }
                    }
                }
            }
        }

    }

    public void makeLeadSquaredELBundlePayment(Map<String , String> userLeads) throws Exception{

        String studentId = userLeads.get(LeadSquaredRequest.Constants.STUDENT_ID);
        SessionState state =SessionState.SCHEDULED;
        String serverUrl = schedulingEndpoint + "/earlyLearning/getEarlyLearningDemoSessionTime?studentId=" + studentId + "&states=" + state;
        logger.info("LeadSquared serverUrl :"+serverUrl);
        ClientResponse response = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,true);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String output = response.getEntity(String.class);
        if(output != null) {
            Long demoStartTime = Long.valueOf(output);
            if (demoStartTime != null || demoStartTime != 0L) {
                Date date = new Date(demoStartTime);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                String formattedDate = format.format(date);
                userLeads.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, formattedDate);
            }
        }
        logger.info("makeLeadSquaredELBundlePayment userLeads : "+userLeads);
        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_COURSE_PAYMENT, null, userLeads, null);
        //String messageGorupId = userLeads.get(LeadSquaredRequest.Constants.STUDENTID) + "_UPSERT";
        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.COURSE_PAYMENT_COMPLETION_SUPERKIDS, gson.toJson(request));
    }

    private void updateOrderReferenceTag(String orderId, String courseId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/updateOrderReferenceTag?orderId=" + orderId + "&courseId=" + courseId,
                HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
    }

    public void reverseOrderTransaction(String orderId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/reverseOrderTransaction?orderId=" + orderId,
                HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
    }
//add logic for otm_bundle

    public PaymentReceiveResponse onPaymentReceived(OnPaymentReceiveReq onPaymentReceiveReq)
            throws VException, IOException, AddressException {
        logger.info("Entering " + onPaymentReceiveReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/onPaymentReceived", HttpMethod.POST,
                gson.toJson(onPaymentReceiveReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("onpayment response for {} {} ", onPaymentReceiveReq.getCallingUserId(), jsonString);

        PaymentReceiveResponse response = gson.fromJson(jsonString, PaymentReceiveResponse.class);

        if (TransactionStatus.SUCCESS.equals(response.getTransaction().getStatus())) {
            if (response.getOrderInfo() != null) {
                OrderInfo orderInfo = response.getOrderInfo();
                OrderItemInfo orderItemInfo = response.getOrderInfo().getItems().get(0);
                String entityId = orderItemInfo.getEntityId();
                switch (orderItemInfo.getEntityType()) {
                    case COURSE_PLAN_REGISTRATION:
                        CoursePlanInfo coursePlanInfo2 = coursePlanManager.getCoursePlan(entityId, false, true);
                        coursePlanManager.processCoursePlanAfterTrialAmountPayment(orderInfo.getId(), orderInfo.getUserId(),
                                coursePlanInfo2, true);
                        break;
                    case COURSE_PLAN:
                        CoursePlanInfo coursePlanInfo = coursePlanManager.getCoursePlan(entityId, false, true);
                        coursePlanManager.processCoursePlanAfterEnrollPayment(orderInfo.getId(), orderInfo.getUserId(),
                                coursePlanInfo);
                        break;
                    case BUNDLE_PACKAGE:
                        BundlePackageInfo bundlePackageInfo = bundlePackageManager.getBundlePackage(entityId);
                        bundlePackageManager.processBundlePurchaseAfterPayment(orderInfo.getId(), orderInfo.getUserId(),
                                bundlePackageInfo, orderInfo.getPurchasingEntities());
                        break;
                    case OTM_BUNDLE_REGISTRATION:
//                        OTFBundleInfo otfBundleInfo = otfBundleManager.getOTFBundle(entityId);
                        otfBundleManager.processOTFBundlePurchaseAfterRegFeePayment(orderInfo.getId(), orderInfo.getUserId(),
                                entityId);
                        break;
                    case OTM_BUNDLE_ADVANCE_PAYMENT:
//                        OTFBundleInfo otfBundleInfo2 = otfBundleManager.getOTFBundle(entityId);
                        otfBundleManager.processOtfBundlePurchaseAfterAdvancePayment(orderInfo.getId(), orderInfo.getUserId(),
                                entityId);
                        break;
                    case OTM_BUNDLE:
//                        OTFBundleInfo otfBundleInfo2 = otfBundleManager.getOTFBundle(entityId);
                        otfBundleManager.processOtfBundlePurchaseAfterPayment(orderInfo.getId(), orderInfo.getUserId(),
                                entityId);
                        break;
                    case OTF:
                        otfManager.processBatchAfterPayment(orderInfo.getId(), orderInfo.getUserId(), orderInfo.getPurchaseContextId(),
                                entityId);
                        break;
                    case OTF_COURSE_REGISTRATION:
                        otfManager.processCourseRegAfterPayment(orderInfo.getId(), orderInfo.getUserId(),
                                entityId);
                        Map<String, Object> payload2 = new HashMap<>();
                        payload2.put("userId", orderInfo.getUserId());
                        payload2.put("courseId", entityId);
                        payload2.put("amount", orderInfo.getAmount());
                        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.OTF_REGISTRATION_EMAIL, payload2);
                        asyncTaskFactory.executeTask(params2);
                        break;
                    case OTF_BATCH_REGISTRATION:
                        otfManager.processBatchRegAfterPayment(orderInfo.getId(), orderInfo.getUserId(),
                                entityId);
                        break;
                    default:
                        performBuyItemsEmailEnrollTask(orderInfo);
                        try {
                            List<OrderItemInfo> items = orderInfo.getItems();
                            Long subscriptionId = orderInfo.getContextId();
                            Role userRole = Role.STUDENT;
                            for (OrderItemInfo item : items) {
                                if (item.getModel() != null && (item.getModel().equals(SessionModel.IL)
                                        || item.getModel().equals(SessionModel.TRIAL))) {
                                    Map<String, Object> payload = new HashMap<>();
                                    payload.put("sessionInfo", null);
                                    payload.put("subscriptionId", subscriptionId);
                                    payload.put("role", userRole);
                                    payload.put("model", item.getModel());
                                    payload.put("subModel", item.getSubModel());
                                    payload.put("grade", item.getGrade());
                                    payload.put("target", item.getTarget());
                                    payload.put("scheduled", true);
                                    payload.put("note", item.getNote());
                                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SUBSCRIPTION_SESSION_EMAIL,
                                            payload);
                                    asyncTaskFactory.executeTask(params);
                                }
                            }
                        } catch (Exception e) {
                            logger.error("Exception : ", e);
                        }
                        break;
                }
            } else if (response.getInstalmentInfo() != null) {
                InstalmentInfo instalmentInfo = response.getInstalmentInfo();
                if (null != instalmentInfo.getContextType()) {
                    switch (instalmentInfo.getContextType()) {
                        case COURSE_PLAN:
                            coursePlanManager.processInstalmentPayment(instalmentInfo,
                                    Long.parseLong(instalmentInfo.getTriggeredBy()), Boolean.TRUE);
                            break;
                        case OTF_BUNDLE:
                            otfBundleManager.processInstalmentPayment(instalmentInfo,
                                    Long.parseLong(instalmentInfo.getTriggeredBy()));
                            break;
                        case BATCH:
                            otfManager.processInstalmentForBatch(instalmentInfo);
                            break;
                        case BUNDLE:
                            bundleManager.processInstalmentPayment(instalmentInfo,
                                    Long.parseLong(instalmentInfo.getTriggeredBy()));
                            break;
                        default:
                            break;
                    }
                }
            }

            Long userId = response.getTransaction().getUserId();
            try {
                // Send post payment SMS
//                smsManager.sendPostPaymentCommunication(userId);
            } catch (Exception ex) {
                logger.warn("Error in sendPostPaymentCommunication", ex);
            }
        }

        logger.info("Exiting: " + response.toString());
        return response;
    }

    public RechargeUrlInfo rechargeAccount(RechargeReq rechargeReq) throws VException {

        logger.info("Entering " + rechargeReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/rechargeAccount", HttpMethod.POST,
                gson.toJson(rechargeReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        RechargeUrlInfo response = gson.fromJson(jsonString, RechargeUrlInfo.class);
        logger.info("Exiting: " + response.toString());
        return response;

    }

    public List<Orders> getOrders(GetOrdersReq ordersReq) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrders", HttpMethod.POST,
                gson.toJson(ordersReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("\n\n\nResponse from dinero is "+jsonString);
        Type listType = new TypeToken<List<Orders>>() {
        }.getType();
        List<Orders> response = gson.fromJson(jsonString, listType);
        logger.info("\n\n\nExiting: " + response.toString());
        return response;
    }

    public void updateDeliverableEntityIdInOrder(String orderId, String deliverableEntityId, DeliverableEntityType type)
            throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/updateDeliverableEntityIdInOrder",
                HttpMethod.POST,
                gson.toJson(new UpdateDeliverableEntityIdInOrderReq(orderId, deliverableEntityId, type)));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Exiting: " + jsonString);
    }

    public void updateBaseInstalment(BaseInstalment baseInstalment) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/updateBaseInstalment";
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST,
                gson.toJson(baseInstalment));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of updateBaseInstalment " + jsonString);
    }

    public BaseInstalment getBaseInstalment(InstalmentPurchaseEntity instalmentPurchaseEntity, String purchaseEntityId)
            throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseInstalment?instalmentPurchaseEntity=" + instalmentPurchaseEntity
                + "&purchaseEntityId=" + purchaseEntityId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);

        BaseInstalment response = gson.fromJson(jsonString, BaseInstalment.class);
        logger.info("EXIT " + response);
        return response;
    }

    public List<BaseInstalment> getBaseInstalments(InstalmentPurchaseEntity instalmentPurchaseEntity,
            List<String> purchaseEntityIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseInstalments?instalmentPurchaseEntity=" + instalmentPurchaseEntity
                + "&purchaseEntityIds="
                + org.springframework.util.StringUtils.collectionToDelimitedString(purchaseEntityIds, ",");
        ;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BaseInstalment>>() {
        }.getType();
        List<BaseInstalment> response = gson.fromJson(jsonString, listType);
        logger.info("EXIT " + response);
        return response;
    }

    public void validateBaseInstalmentInfos(String courseId, List<BaseInstalmentInfo> baseInstalmentInfos)
            throws VException {
        if (baseInstalmentInfos != null && !baseInstalmentInfos.isEmpty()) {
            int count = 0;
            Set<Long> installmentDueTimes = new HashSet<>();
            for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
                if (count != 0 && (baseInstalmentInfo.getDueTime() == null || baseInstalmentInfo.getDueTime() <= 0L)) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Due date must be specified");
                }
                installmentDueTimes.add(baseInstalmentInfo.getDueTime());
                count++;
            }
            if (installmentDueTimes.size() != baseInstalmentInfos.size()) {
                throw new BadRequestException(ErrorCode.SAME_DUE_TIME_FOR_INSTALMENTS_NOT_ALLOWED, "Due time can't be same for two installments");
            }
        } else {
            BaseInstalment baseInstalment = getBaseInstalment(InstalmentPurchaseEntity.OTF_COURSE, courseId);
            if (baseInstalment != null && ArrayUtils.isNotEmpty(baseInstalment.getInfo())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Instalments must be specified");
            }
        }
    }

    public OrderDetails getOrderDetails(GetOrderDetailsReq req) throws VException {
        req.verify();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrderDetails?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        OrderDetails res = gson.fromJson(jsonString, OrderDetails.class);
        return res;
    }

    public OrderInfo checkAndGetAccountInfoForPayment(BuyItemsReqNew req) throws VException {
        req.verify();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/checkAndGetAccountInfoForPayment",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        OrderInfo res = gson.fromJson(jsonString, OrderInfo.class);
        return res;
    }

    public OrderInfo processOrderAfterPayment(String id, ProcessPaymentReq req) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/processOrderAfterPayment/" + id,
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        OrderInfo res = gson.fromJson(jsonString, OrderInfo.class);
        return res;
    }

    public List<InstalmentInfo> processInstalmentAfterPayment(String id) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/processInstalmentAfterPayment/" + id,
                HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type listType = new TypeToken<ArrayList<InstalmentInfo>>() {
        }.getType();
        List<InstalmentInfo> instalments = gson.fromJson(jsonString, listType);
        return instalments;
    }

    public List<RegisteredCourse> getRegisteredCourses(GetRegisteredCoursesReq req)
            throws VException, IllegalArgumentException, IllegalAccessException, JSONException {
        List<RegisteredCourse> res = new ArrayList<>();
        req.verify();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getRegisteredCourses?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(jsonString, listType);
        if (ArrayUtils.isNotEmpty(orders)) {
            List<String> structuredCourseIds = new ArrayList<>();
            List<String> otfCourseIds = new ArrayList<>();
            Map<String, Orders> entityVsAmountPaid = new HashMap<>();

            Map<String, List<Orders>> referenceIdOrdersMap = new HashMap<>();
            for (Orders order : orders) {
                String referenceId = getReferenceId(order);
                if (referenceId == null) {
                    continue;
                }
                List<Orders> referenceOrdersList;
                if (referenceIdOrdersMap.get(referenceId) == null) {
                    referenceOrdersList = new ArrayList<>();
                    referenceIdOrdersMap.put(referenceId, referenceOrdersList);
                } else {
                    referenceOrdersList = referenceIdOrdersMap.get(referenceId);
                }
                referenceOrdersList.add(order);
            }

            for (List<Orders> ordersList : referenceIdOrdersMap.values()) {
                if (ordersList.size() == 1) {
                    OrderedItem orderItem = ordersList.get(0).getItems().get(0);
                    if (EntityType.OTO_COURSE_REGISTRATION.equals(orderItem.getEntityType())) {
                        structuredCourseIds.add(orderItem.getEntityId());
                    } else if (EntityType.OTF_COURSE_REGISTRATION.equals(orderItem.getEntityType())) {
                        otfCourseIds.add(orderItem.getEntityId());
                    }
                    entityVsAmountPaid.put(orderItem.getEntityId(), ordersList.get(0));
                }
            }
            if (ArrayUtils.isNotEmpty(structuredCourseIds)) {
                GetStructuredCoursesReq getStructuredCoursesReq = new GetStructuredCoursesReq();
                getStructuredCoursesReq.setCourseIds(structuredCourseIds);
                List<StructuredCourseInfo> otoCourses = coursePlanManager.getStructuredCourses(getStructuredCoursesReq);
                if (ArrayUtils.isNotEmpty(otoCourses)) {
                    for (StructuredCourseInfo structuredCourseInfo : otoCourses) {
                        RegisteredCourse _course = new RegisteredCourse(structuredCourseInfo);
                        if (entityVsAmountPaid.containsKey(structuredCourseInfo.getId())) {
                            Orders order = entityVsAmountPaid.get(structuredCourseInfo.getId());
                            _course.setRegistrationFee(order.getAmount());
                            _course.setRegistrationTime(order.getCreationTime());
                        }
                        res.add(_course);
                    }
                }
            }
            if (ArrayUtils.isNotEmpty(otfCourseIds)) {
                List<CourseInfo> otfCourses = otfManager.getCoursesBasicInfos(otfCourseIds);
                if (ArrayUtils.isNotEmpty(otfCourses)) {
                    for (CourseInfo courseInfo : otfCourses) {
                        RegisteredCourse _course = new RegisteredCourse(courseInfo);
                        if (entityVsAmountPaid.containsKey(courseInfo.getId())) {
                            Orders order = entityVsAmountPaid.get(courseInfo.getId());
                            _course.setRegistrationFee(order.getAmount());
                            _course.setRegistrationTime(order.getCreationTime());
                        }
                        res.add(_course);
                    }
                }
            }
        }
        return res;
    }

    public PayInstalmentRes payInstalment(PayInstalmentReq req) throws VException, IOException {
        req.verify();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/payInstalment", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PayInstalmentRes response = gson.fromJson(jsonString, PayInstalmentRes.class);
        logger.info("PayInstalmentRes :" + jsonString);
        if (Boolean.TRUE.equals(req.getNewFlow()) && !response.getNeedRecharge()
                && response.getInstalmentInfo() != null) {
            if (InstalmentPurchaseEntity.COURSE_PLAN.equals(response.getInstalmentInfo().getContextType())) {
                SQSMessageType messageType = SQSMessageType.COURSE_PLAN_PROCESS_INSTALLMENT_PAYMENT;
                ProcessInstallmentForCoursePlanPojo pojo = new ProcessInstallmentForCoursePlanPojo(response.getInstalmentInfo(), req.getCallingUserId());
                awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(pojo));
                /*Map<String, Object> payload = new HashMap<>();
                payload.put("instalmentInfo", response.getInstalmentInfo());
                payload.put("callingUserId", req.getCallingUserId());
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.PROCESS_COURSE_PLAN_INSTALMENT_PAYMENT,
                        payload);
                asyncTaskFactory.executeTask(params);*/
            }
        }
        return response;
    }

    private String getReferenceId(Orders order) {
        if (order.getReferenceTags() != null && !order.getReferenceTags().isEmpty()) {
            for (ReferenceTag referenceTag : order.getReferenceTags()) {
                if (ReferenceType.OTO_STRUCTURED_COURSE_ID.equals(referenceTag.getReferenceType())
                        || ReferenceType.OTF_COURSE_ID.equals(referenceTag.getReferenceType())) {
                    return referenceTag.getReferenceId();
                }
            }
        }
        return null;
    }

    public GetInstalmentDetailsOTFRes getInstalmentDetailsOTF(String courseId, String batchId, String userId)
            throws VException {
        String queryString = "courseId=" + courseId + "&userId=" + userId + "&batchId=" + batchId;
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(dineroEndpoint + "/payment/getInstalmentDetailsOTF?" + queryString, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GetInstalmentDetailsOTFRes res = new Gson().fromJson(jsonString, GetInstalmentDetailsOTFRes.class);
        return res;
    }

    public List<InstalmentInfo> prepareDiscountedInstalmentsCoursePlan(BuyItemsReqNew buyItemsReqNew)
            throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/prepareDiscountedInstalmentsCoursePlan", HttpMethod.POST,
                gson.toJson(buyItemsReqNew));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type mapType = new TypeToken<List<InstalmentInfo>>() {
        }.getType();

        List<InstalmentInfo> response = gson.fromJson(jsonString, mapType);
        return response;
    }

    public List<Orders> getOrdersByDeliverableOrEntity(List<String> deliverableorEntityIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        GetOrdersByDeliverableEntityReq req = new GetOrdersByDeliverableEntityReq();
        req.setDeliverableorEntityIds(deliverableorEntityIds);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrdersByDeliverableOrEntity",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type mapType = new TypeToken<List<Orders>>() {
        }.getType();

        List<Orders> response = gson.fromJson(jsonString, mapType);
        return response;
    }

    public List<FirstExtTransaction> getFirstExtTransactions(List<Long> studentIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        GetExtTransactionsForUsers req = new GetExtTransactionsForUsers();
        req.setUserIds(studentIds);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getFirstExtTransactions",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type mapType = new TypeToken<List<FirstExtTransaction>>() {
        }.getType();

        List<FirstExtTransaction> response = gson.fromJson(jsonString, mapType);
        return response;
    }

    public PlatformBasicResponse resetInstallmentState(ResetInstallmentStateReq req) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/resetInstallmentState",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse(jsonString);
        return platformBasicResponse;
    }

    public PlatformBasicResponse changeInstalmentDueDate(ChangeInstalmentDueDateReq req) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/changeInstalmentDueDate",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = gson.fromJson(jsonString, PlatformBasicResponse.class);
        return platformBasicResponse;
    }

    public Map<String, InstalmentPaymentInfoRes> getOrderIdInstalmentInfoMap(List<String> orderIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getOrderIdInstalmentInfoMap?ids=";
        if (orderIds.size() >= 1) {
            url += orderIds.get(0);
            for (int i = 1; i < orderIds.size(); i++) {
                url += "&ids=" + orderIds.get(i);
            }
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type mapType = new TypeToken<Map<String, InstalmentPaymentInfoRes>>() {
        }.getType();
        Map<String, InstalmentPaymentInfoRes> response = gson.fromJson(jsonString, mapType);
        logger.info("EXIT " + response);
        return response;
    }

    public List<GetOrderUserInfoRes> getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded(GetOrdersWithoutDeliverableEntityReq req) throws VException {

        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        Type listType1 = new TypeToken<ArrayList<GetOrderUserInfoRes>>() {
        }.getType();
        List<GetOrderUserInfoRes> orders = new Gson().fromJson(jsonString, listType1);

        if (ArrayUtils.isNotEmpty(orders)) {
            Set<Long> userIds = new HashSet<>();
            for (GetOrderUserInfoRes order : orders) {
                userIds.add(order.getUserId());
            }
            Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            for (GetOrderUserInfoRes order : orders) {
                if (usermap.containsKey(order.getUserId())) {
                    order.setUser(usermap.get(order.getUserId()));
                }
            }
        }
        return orders;
    }

    public List<GetDueInstalmentsForTimeRes> getDueInstalmentsForContextTypeAndTime(GetDueInstalmentsForTimeReq req) throws VException {
        req.verify();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getDueInstalmentsForContextTypeAndTime?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        Type listType1 = new TypeToken<ArrayList<InstalmentInfo>>() {
        }.getType();
        List<InstalmentInfo> instalments = new Gson().fromJson(jsonString, listType1);

        List<Long> userIds = new ArrayList<>();
        List<GetDueInstalmentsForTimeRes> response = new ArrayList<>();
        if (ArrayUtils.isEmpty(instalments)) {
            logger.info("Due Instalments is Empty for startTime " + req.getStartTime());
            return response;
        }
        if (req.getContextType().equals(InstalmentPurchaseEntity.COURSE_PLAN)) {
            List<String> coursePlanIds = new ArrayList<>();
            Map<String, InstalmentInfo> map = new HashMap<>();
            for (InstalmentInfo instalment : instalments) {
                if (StringUtils.isNotEmpty(instalment.getContextId())) {
                    coursePlanIds.add(instalment.getContextId());
                    map.put(instalment.getContextId(), instalment);
                }
            }

            List<GetCoursePlanReminderListRes> slist = coursePlanManager.getCoursePlanReminderListResForIds(coursePlanIds);

            if (ArrayUtils.isNotEmpty(slist)) {
                for (GetCoursePlanReminderListRes res : slist) {
                    if (map.containsKey(res.getCoursePlanId())) {
                        InstalmentInfo instal = map.get(res.getCoursePlanId());
                        if (instal != null) {
                            GetDueInstalmentsForTimeRes instalRes = new GetDueInstalmentsForTimeRes();
                            instalRes.setContextId(instal.getContextId());
                            instalRes.setContextType(instal.getContextType());
                            instalRes.setDueTime(instal.getDueTime());
                            instalRes.setTotalAmount(instal.getTotalAmount());
                            if (res.getState() != null) {
                                instalRes.setState(res.getState().name());
                            }
                            instalRes.setStudentId(res.getStudentId());
                            userIds.add(res.getStudentId());
                            response.add(instalRes);
                        }
                    }
                }
            }
        } else if (req.getContextType().equals(InstalmentPurchaseEntity.BATCH)) {
            List<String> enrollmentIds = new ArrayList<>();
            Map<String, InstalmentInfo> map = new HashMap<>();
            for (InstalmentInfo instalment : instalments) {
                if (StringUtils.isNotEmpty(instalment.getDeliverableEntityId())) {
                    enrollmentIds.add(instalment.getDeliverableEntityId());
                    map.put(instalment.getDeliverableEntityId(), instalment);
                }
            }
            GetEnrollmentsReq enrollmentReq = new GetEnrollmentsReq();
            enrollmentReq.setEnrollmentIds(enrollmentIds);
            List<EnrollmentPojo> enrollments = enrollmentManager.getEnrollmentsByEnrollmentIds(enrollmentReq);

            if (ArrayUtils.isNotEmpty(enrollments)) {
                for (EnrollmentPojo res : enrollments) {
                    if (map.containsKey(res.getEnrollmentId())) {
                        InstalmentInfo instal = map.get(res.getEnrollmentId());
                        if (instal != null) {
                            GetDueInstalmentsForTimeRes instalRes = new GetDueInstalmentsForTimeRes();
                            instalRes.setContextId(instal.getContextId());
                            instalRes.setContextType(instal.getContextType());
                            instalRes.setDueTime(instal.getDueTime());
                            instalRes.setTotalAmount(instal.getTotalAmount());
                            if (res.getState() != null) {
                                instalRes.setState(res.getState().name());
                            }
                            if (res.getStatus() != null) {
                                instalRes.setStatus(res.getStatus().name());
                            }
                            if (StringUtils.isNotEmpty(res.getUserId())) {
                                Long studentId = Long.parseLong(res.getUserId());
                                instalRes.setStudentId(studentId);
                                userIds.add(studentId);
                            }
                            response.add(instalRes);
                        }
                    }
                }
            }
        }
        Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        if (ArrayUtils.isNotEmpty(response)) {
            for (GetDueInstalmentsForTimeRes res : response) {
                if (usermap.containsKey(res.getStudentId())) {
                    res.setStudent(usermap.get(res.getStudentId()));
                }
            }
        }
        return response;
    }

    public List<AgentClosedSalesRes> getAgentClosedSalesForTime(GetDueInstalmentsForTimeReq req) throws VException, InterruptedException {
        List<AgentClosedSalesRes> agentList = new ArrayList<>();
        String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getNewStudentsForTimePeriod?" + queryString,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        Type listType1 = new TypeToken<ArrayList<Long>>() {
        }.getType();
        List<Long> userIds = new Gson().fromJson(jsonString, listType1);
        if (ArrayUtils.isEmpty(userIds)) {
            return agentList;
        }
        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        List<LeadSquaredAgent> agents = leadSquaredManager.getLeadAgents(userIds);
        Map<Long, String> userAgentMap = new HashMap<>();
        Map<String, AgentClosedSalesRes> agentMap = new HashMap<>();
        AgentClosedSalesRes agentClosedSalesRes = new AgentClosedSalesRes();
        agentClosedSalesRes.setAgentName("Default");
        if (ArrayUtils.isNotEmpty(agents)) {
            for (LeadSquaredAgent agent : agents) {
                if (agent.getUserId() != null) {
                    userAgentMap.put(agent.getUserId(), agent.getAgentName());
                }
            }
        }
        for (Long id : userIds) {
            if (userAgentMap.containsKey(id)) {
                if (agentMap.containsKey(userAgentMap.get(id))) {
                    AgentClosedSalesRes res = agentMap.get(userAgentMap.get(id));

                    if (res.getStudentId() == null) {
                        res.setStudentId(new ArrayList<>());
                    }
                    if (res.getStudent() == null) {
                        res.setStudent(new ArrayList<>());
                    }
                    List<Long> studentIds = res.getStudentId();
                    List<UserBasicInfo> students = res.getStudent();
                    studentIds.add(id);
                    students.add(userMap.get(id));
                    res.setCount(res.getCount() + 1);
                } else {
                    AgentClosedSalesRes res = new AgentClosedSalesRes();
                    res.setStudentId(new ArrayList<>());
                    res.setStudent(new ArrayList<>());
                    List<Long> studentIds = res.getStudentId();
                    List<UserBasicInfo> students = res.getStudent();
                    studentIds.add(id);
                    students.add(userMap.get(id));
                    res.setCount(res.getCount() + 1);
                    agentMap.put(userAgentMap.get(id), res);
                }
            } else {
                if (agentClosedSalesRes.getStudentId() == null) {
                    agentClosedSalesRes.setStudentId(new ArrayList<>());
                }
                if (agentClosedSalesRes.getStudent() == null) {
                    agentClosedSalesRes.setStudent(new ArrayList<>());
                }
                List<Long> studentIds = agentClosedSalesRes.getStudentId();
                List<UserBasicInfo> students = agentClosedSalesRes.getStudent();
                studentIds.add(id);
                students.add(userMap.get(id));
                agentClosedSalesRes.setCount(agentClosedSalesRes.getCount() + 1);
            }

        }
        // agentMap.put(agentClosedSalesRes.getAgentName(), agentClosedSalesRes);

        for (Entry<String, AgentClosedSalesRes> entry_temp : agentMap.entrySet()) {
            agentList.add(entry_temp.getValue());
        }
        if (agentClosedSalesRes.getCount() > 0) {
            agentList.add(agentClosedSalesRes);
        }
        return agentList;
    }

    public List<BaseInstalment> getFirstBaseInstalmentsForDueTime(Long startTime, Long endTime) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getFirstBaseInstalmentsForDueTime?startTime=" + startTime
                + "&endTime=" + endTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BaseInstalment>>() {
        }.getType();
        List<BaseInstalment> response = gson.fromJson(jsonString, listType);
        logger.info("EXIT " + response);
        return response;
    }

    public void updateDeliverableData(UpdateDeliverableDataReq updateDeliverableDataReq) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/updateDeliverableData",
                HttpMethod.POST, gson.toJson(updateDeliverableDataReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
    }

    public List<Orders> getOrdersByDeliverableIds(List<String> deliverableIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        GetOrdersByDeliverableEntityReq req = new GetOrdersByDeliverableEntityReq();
        req.setDeliverableorEntityIds(deliverableIds);
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getOrdersByDeliverableIds",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type mapType = new TypeToken<List<Orders>>() {
        }.getType();

        List<Orders> response = gson.fromJson(jsonString, mapType);
        return response;
    }

    public List<ExtTransactionPojo> getExtTransactionsFromOrderIds(List<String> vedantuOrderIds) throws VException {
        if (ArrayUtils.isEmpty(vedantuOrderIds)) {
            return new ArrayList<>();
        }
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getExtTransactionsFromOrderIds?vedantuOrderIds="
                + org.springframework.util.StringUtils.collectionToDelimitedString(vedantuOrderIds, ","), HttpMethod.GET,
                null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<ExtTransactionPojo>>() {
        }.getType();
        List<ExtTransactionPojo> response = gson.fromJson(jsonString, listType);
        logger.info("Exiting: " + response.toString());
        return response;
    }

    // No checks for now
    public PayInstalmentRes payInstalmentForBundle(PayInstalmentReq req) throws VException, IOException {
        req.verify();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/payInstalment", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PayInstalmentRes response = gson.fromJson(jsonString, PayInstalmentRes.class);
        logger.info("PayInstalmentRes :" + jsonString);
        if (!response.getNeedRecharge()
                && response.getInstalmentInfo() != null) {
            if (InstalmentPurchaseEntity.OTF_BUNDLE.equals(response.getInstalmentInfo().getContextType())) {
                otfBundleManager.processInstalmentPayment(response.getInstalmentInfo(), req.getCallingUserId());
            }
        }
        return response;
    }

    public Map<Long, DashboardUserInstalmentHistoryRes> getUserInstalmentTransactionHistory(List<Long> userIds) throws VException {
        String url = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT") + "/payment/getUserInstalmentTransactionHistory?userIds="
                + org.springframework.util.StringUtils.collectionToDelimitedString(userIds, ",");
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<Map<Long, DashboardUserInstalmentHistoryRes>>() {
        }.getType();
        Map<Long, DashboardUserInstalmentHistoryRes> response = gson.fromJson(jsonString, listType);
        logger.info("getUserInstalmentTransactionHistory :" + jsonString);
        return response;
    }
}
