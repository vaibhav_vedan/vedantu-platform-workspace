/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.board;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.BoardTreeInfo;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import com.vedantu.platform.dao.board.TeacherBoardMappingDAO;
import com.vedantu.platform.dao.board.BoardDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.request.CreateTeacherBoardMappingReq;
import com.vedantu.platform.request.GetTeacherBoardMappingsReq;
import com.vedantu.platform.response.board.GetTeacherBoardMappingsRes;
import com.vedantu.platform.request.RemoveTeacherMappingReq;
import com.vedantu.platform.response.board.RemoveTeacherMappingRes;
import com.vedantu.platform.response.board.TeacherBoardMappingRes;
import com.vedantu.platform.managers.moodle.MoodleManager;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.platform.managers.user.UserManager;
import com.vedantu.teacherBoardMappings.pojo.GetTeacherBoardMappingHierarchialRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class TeacherBoardMappingManager {

    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TeacherBoardMappingManager.class);

    @Autowired
    private TeacherBoardMappingDAO teacherMappingDAO;

    @Autowired
    private BoardManager boardManager;

    @Autowired
    private BoardDAO boardDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;
    
    @Autowired
    private HttpSessionUtils httpSessionUtils;
    

    public TeacherBoardMappingRes createTeacherBoardMapping(
            CreateTeacherBoardMappingReq req) throws BadRequestException {
        logger.info("createTeacherBoardMapping", req);
        req.verify();

        TeacherBoardMapping teacherBoardMapping = teacherMappingDAO
                .getTeacherBoardMapping(req.getUserId(), req.getCategory(),
                        req.getGrade());
        if (teacherBoardMapping == null) {
            teacherBoardMapping = req.toTeacherMapping();
        }
        teacherBoardMapping
                .setBoardIds(req.getBoardIds() == null ? new HashSet<Long>()
                        : new HashSet<Long>(req.getBoardIds()));
        teacherMappingDAO.upsert(teacherBoardMapping, req.getCallingUserId());

        Map<Long, Board> boardInfoMap = boardDAO.getBoardsMap(teacherBoardMapping.getBoardIds());
        String subjectName = "";
        try {

            for (Long boardId : teacherBoardMapping.getBoardIds()) {
                Board board = boardInfoMap.get(boardId);
		subjectName += (board.getName() + " (" + boardId + ") ");
            }

        } catch (Exception ex) {
            // do nothing
        }
        TeacherBoardMappingRes res = new TeacherBoardMappingRes(
                teacherBoardMapping, boardManager.toBoardTree(teacherBoardMapping, boardInfoMap));
        try {
            Map<String, Object> payload = new HashMap<>();
                    payload.put("userId", req.getUserId());
                    payload.put("subjectName", subjectName);
                    payload.put("grade", req.getGrade());
                    payload.put("category", req.getCategory());
                    payload.put("changeType", "ADDED");
                    payload.put("callerUser", httpSessionUtils.getCurrentSessionData());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.MAPPING_UPDATE_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
            
            
            //emailManager.sendTeacherBoardMappingUpdateToAdmin(req.getUserId(), subjectName, req.getGrade(),
            //        req.getCategory(), "ADDED");
        } catch (Exception ex) {
            logger.error(ex);
        }
        logger.info("createTeacherBoardMapping", res);
        return res;
    }

    public RemoveTeacherMappingRes removeTeacherMapping(
            RemoveTeacherMappingReq req) throws VException {
        logger.info("removeTeacherMapping", req);
        req.verify();
        TeacherBoardMapping teacherBoardMapping = teacherMappingDAO
                .getById(req.getId());
        if (teacherBoardMapping == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "no teachar board mapping found for id: " + req.getId());
        }

        String subjectName = "";
        for (Long boardId : teacherBoardMapping.getBoardIds()) {
            Board board = boardDAO.getById(boardId);
            if(board!=null)
                subjectName += (board.getName() + " (" + boardId + ") ");
        }
        String grade = teacherBoardMapping.getGrade();
        String category = teacherBoardMapping.getCategory();
        teacherMappingDAO.deleteById(teacherBoardMapping.getId());
        
        try {
            String changeType = "REMOVED";
            Map<String, Object> payload = new HashMap<>();
                    payload.put("userId", teacherBoardMapping.getUserId());
                    payload.put("subjectName", subjectName);
                    payload.put("grade", grade);
                    payload.put("category", category);
                    payload.put("changeType", changeType);
                    payload.put("callerUser", httpSessionUtils.getCurrentSessionData());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.MAPPING_UPDATE_EMAIL, payload);
                    asyncTaskFactory.executeTask(params);
                    //emailManager.sendTeacherBoardMappingUpdateToAdmin(teacherBoardMapping.getUserId(), subjectName, grade, category,
                    //changeType);
        } catch (Exception ex) {
            logger.error(ex);
        }
        


        return new RemoveTeacherMappingRes(true);
    }

    public GetTeacherBoardMappingsRes getTeacherBoardMappingTree(
            Long userId) throws VException {
        List<TeacherBoardMapping> teacherBoardMappings = teacherMappingDAO
                .getTeacherBoardMappings(userId);

        GetTeacherBoardMappingsRes res = new GetTeacherBoardMappingsRes();

        Set<Long> boardIds = new HashSet<>();

        for (TeacherBoardMapping teacherBoardMapping : teacherBoardMappings) {
            if (teacherBoardMapping.getBoardIds() != null) {
                boardIds.addAll(teacherBoardMapping.getBoardIds());
            }
        }

        Map<Long, Board> boardInfoMap = boardDAO.getBoardsMap(boardIds);

        for (TeacherBoardMapping teacherBoardMapping : teacherBoardMappings) {
            res.addItem(new TeacherBoardMappingRes(teacherBoardMapping,
                    boardManager.toBoardTree(teacherBoardMapping, boardInfoMap)));
        }
        Collections.sort(res.getList(), new CategoryGradeComparator());

        return res;
    }

    public GetTeacherBoardMappingHierarchialRes getHierarchialTeacherMappings(Long userId) throws VException {
        GetTeacherBoardMappingsReq reqPojo = new GetTeacherBoardMappingsReq(userId);
        reqPojo.verify();
        GetTeacherBoardMappingsRes resPojo = getTeacherBoardMappingTree(userId);

        Set<String> grades = new HashSet<String>();
        Set<String> categories = new HashSet<String>();
        Set<BoardTreeInfo> boards = new HashSet<BoardTreeInfo>();
        for (TeacherBoardMappingRes teacherBoardMappingRes : resPojo.getList()) {
            grades.add(teacherBoardMappingRes.getGrade());
            categories.add(teacherBoardMappingRes.getCategory());
            for (BoardTreeInfo boardTreeInfo : teacherBoardMappingRes.getBoards()) {
                if (!boards.isEmpty() && boards.contains(boardTreeInfo)) {
                    boards.add(boardTreeInfo);
                } else {
                    boards.add(boardTreeInfo);
                }
            }
        }

        List<Object> gradesListObject = Arrays.asList(grades.toArray());

        List<String> gradesListString = new ArrayList<String>();
        for (Object object : gradesListObject) {
            gradesListString.add(object != null ? object.toString() : null);
        }

        Collections.sort(gradesListString, new ComparatorOfNumericString());

        GetTeacherBoardMappingHierarchialRes getTeacherBoardMappingHierarchialRes = new GetTeacherBoardMappingHierarchialRes(new LinkedHashSet<String>(gradesListString), categories, boards);

        return getTeacherBoardMappingHierarchialRes;
    }

    private static class CategoryGradeComparator implements Comparator<TeacherBoardMappingRes> {

        @Override
        public int compare(TeacherBoardMappingRes o1, TeacherBoardMappingRes o2) {
            return o1 != null && o2 != null ? (o1.getCategory() + o1.getGrade()).compareTo(
                    (o2.getCategory() + o2.getGrade())) : 0;
        }

    }
    
    
    
    private static class ComparatorOfNumericString implements Comparator<String>{

            @Override
	    public int compare(String string1, String string2) {
	        // TODO Auto-generated method stub
	        return Integer.parseInt(string1)-Integer.parseInt(string2);
	    }
    }
    
    
    public List<TeacherBoardMapping> getTeacherBoardMappings(Long userId) {
        logger.info("getTeacherBoardMappings", userId);
        return teacherMappingDAO.getTeacherBoardMappings(userId);
    }

}
