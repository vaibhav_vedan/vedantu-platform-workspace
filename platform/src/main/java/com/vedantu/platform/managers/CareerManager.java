package com.vedantu.platform.managers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.dao.CareerDAO;
import com.vedantu.platform.entity.Career;
import com.vedantu.platform.request.CareerReq;
import com.vedantu.platform.response.CareersWithFilterRes;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author MNPK
 */

@Service
public class CareerManager {
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(CareerManager.class);

    @Autowired
    private CareerDAO careerDAO;

    @Autowired
    private FosUtils fosUtils;

    public List<Career> getAllCareerJobs() throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Career.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        List<Career> careers = careerDAO.runQuery(query, Career.class);
        if (ArrayUtils.isNotEmpty(careers)) {
            return careers;
        } else {
            return new ArrayList<>();
        }
    }

    public PlatformBasicResponse setCareers(List<CareerReq> reqList) {
        List<Career> careerList = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(reqList)) {
            int result = 0;
            try {
                Query q = new Query();
                q.addCriteria(Criteria.where(Career.Constants.DATA_REMOVE_TAG).is(true));
                result = careerDAO.deleteEntities(q, Career.class);
            } catch (Exception ex) {
                logger.error("delete All career documents where createdBy id SYSTEM : " + ex.getMessage());
            }
            logger.info("No of career documets deleted : " + result);
            for (CareerReq req : reqList) {
                Career career = new Career();
                career.setDataRemoveTag(true);
                if (StringUtils.isNotEmpty(req.getSkills())) {
                    career.setSkills(req.getSkills());
                }
                if (StringUtils.isNotEmpty(req.getTechnology())) {
                    career.setTechnology(req.getTechnology());
                }
                if (StringUtils.isNotEmpty(req.getTitle())) {
                    career.setTitle(req.getTitle());
                }
                if (StringUtils.isNotEmpty(req.getDescription())) {
                    career.setDescription(req.getDescription());
                }
                careerList.add(career);
            }
        }
        if (ArrayUtils.isNotEmpty(careerList)) {
            careerDAO.insertAllEntities(careerList, "Career");
        }
        return new PlatformBasicResponse(true, "", "");
    }

    public PlatformBasicResponse createJd(CareerReq req) throws VException {
        req.verify();
        if (careerDAO.getCareerByUsingTechnologyAndTitle(req.getTechnology(), req.getTitle()) != null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Title and Technology combination already existed");
        }
        Career career = new Career(req.getTechnology(), req.getTitle(), req.getDescription());
        logger.info("career JD : " + career);
        careerDAO.create(career);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse updateJd(CareerReq req) throws VException {
        req.verify();
        if (StringUtils.isEmpty(req.getId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "career Id required");
        }
        Career career = careerDAO.getEntityById(req.getId(), Career.class);
        if (career == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "career not found with career Id : " + req.getId());
        }
        career.setTechnology(req.getTechnology());
        career.setTitle(req.getTitle());
        career.setDescription(req.getDescription());
        logger.info("career JD : " + career);
        careerDAO.create(career);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse updateEntityStateById(String id, EntityState state) throws VException {
        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "career Id required");
        }
        Career career = careerDAO.getEntityById(id, Career.class);
        if (career == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "career not found with career Id : " + id);
        }
        if (!state.equals(career.getEntityState())) {
            career.setEntityState(state);
            careerDAO.create(career);
        }
        return new PlatformBasicResponse();
    }

    public List<CareersWithFilterRes> getCareersWithFilter(Integer start, Integer limit, EntityState state) throws VException {
        List<CareersWithFilterRes> careersWithFilterResList = new ArrayList<>();
        List<Career> careers = careerDAO.getCareersByUsingFilters(start, limit, state);
        Set<String> uniqueUserIdsSet = new HashSet<>();
        careers.forEach(career -> {
            if (career != null) {
                if (StringUtils.isNotEmpty(career.getCreatedBy())) {
                    uniqueUserIdsSet.add(career.getCreatedBy());
                }
                if (StringUtils.isNotEmpty(career.getLastUpdatedBy())) {
                    uniqueUserIdsSet.add(career.getLastUpdatedBy());
                }
            }
        });
        List<String> uniqueUserIds = uniqueUserIdsSet.stream().collect(Collectors.toList());
        logger.info("uniquieUserIdsSet : " + uniqueUserIds);
        Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(uniqueUserIds, Boolean.TRUE);
        for (Career career : careers) {
            String createdByEmail = null;
            if (StringUtils.isNotEmpty(career.getCreatedBy())) {
                if (userBasicInfoMap.get(career.getCreatedBy()) != null && StringUtils.isNotEmpty(userBasicInfoMap.get(career.getCreatedBy()).getEmail())) {
                    createdByEmail = userBasicInfoMap.get(career.getCreatedBy()).getEmail();
                }
            }
            String lastUpdatedByEmail = null;
            if (StringUtils.isNotEmpty(career.getLastUpdatedBy())) {
                if (userBasicInfoMap.get(career.getLastUpdatedBy()) != null && StringUtils.isNotEmpty(userBasicInfoMap.get(career.getLastUpdatedBy()).getEmail())) {
                    lastUpdatedByEmail = userBasicInfoMap.get(career.getLastUpdatedBy()).getEmail();
                }
            }
            careersWithFilterResList.add(new CareersWithFilterRes(career, createdByEmail, lastUpdatedByEmail));
        }
        logger.info("careersWithFilterResList : " + careersWithFilterResList);
        return careersWithFilterResList;
    }

}
