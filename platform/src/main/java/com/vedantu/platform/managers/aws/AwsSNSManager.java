/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.managers.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author somil
 */
@Service
public class AwsSNSManager extends AbstractAwsSNSManager{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);

    

    private String env;
    private String arn;
    private AmazonSNSAsync snsClient;
    private String snsArnBuilder;
    
    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public AwsSNSManager() {
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        snsArnBuilder = ConfigUtils.INSTANCE.getStringValue("aws.sns.arn.builder");
        snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
        logger.info("initializing AwsSNSManager");
        try {
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                //creating SNS topic CMS_WEBINAR_EVENTS
                CreateTopicRequest createTopicRequest = new CreateTopicRequest(SNSTopic.CMS_WEBINAR_EVENTS.getTopicName(env));
                snsClient.createTopic(createTopicRequest);
            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSNSManager " + e.getMessage());
        }
        
    }

    @Override
    public void triggerSNS(SNSTopic topic, String subject, String message) {
        String topicArn = getTopicArn(topic); 
        PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
        logger.info("publishRequest "+publishRequest);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }               
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }
    
    public void triggerAsyncSNS(SNSTopic topic, String subject, String message, Long delayMillis) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("topic", topic);
        payload.put("subject", subject);
        payload.put("message", message);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }           
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_SNS, payload, delayMillis);
        asyncTaskFactory.executeTask(params);
    }
    
    @Override
    public String getTopicArn(SNSTopic topic) {
        return String.format(snsArnBuilder,Regions.AP_SOUTHEAST_1.getName(), arn, topic.getTopicName(env));
    }
    
    public SubscribeResult createSubscription(SNSTopic topic, String type, String typeArn) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(getTopicArn(topic), type, typeArn);
        SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
        return subscribeResult;
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSNSManager connection ", e);
        }
    }              

    @Override
    public void createTopics() {
        
        //logger.info("No topics to create");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        createSNSTopic(CronTopic.ADMIN_ALERTS);
        createSNSTopic(SNSTopic.CMS_WEBINAR_EVENTS);
        createSNSTopic(CronTopic.CRON_CHIME_15_Minutes);
        createSNSTopic(CronTopic.CRON_CHIME_2HOURLY);
        createSNSTopic(CronTopic.CRON_CHIME_30_Minutes);
        createSNSTopic(CronTopic.CRON_CHIME_3HOURLY);
        createSNSTopic(CronTopic.CRON_CHIME_3_Minutes);
        createSNSTopic(CronTopic.CRON_CHIME_4_Minutes);
        createSNSTopic(CronTopic.CRON_CHIME_5_Minutes);
        createSNSTopic(CronTopic.CRON_CHIME_DAILY);
        createSNSTopic(CronTopic.CRON_CHIME_DAILY_11PM);
        createSNSTopic(CronTopic.CRON_CHIME_DAILY_1AM_IST);
        createSNSTopic(CronTopic.CRON_CHIME_DAILY_2PM_IST);
        createSNSTopic(CronTopic.CRON_CHIME_DAILY_3PM_IST);
        createSNSTopic(CronTopic.CRON_CHIME_FORTNIGHTLY);
        createSNSTopic(CronTopic.CRON_CHIME_HOURLY);
        createSNSTopic(CronTopic.CRON_CHIME_LISTING_DAILY_COUNT);
        createSNSTopic(SNSTopic.DATA_TO_S3_DONE);
        createSNSTopic(SNSTopic.DATA_TO_S3_DONE_OTF);
        createSNSTopic(SNSTopic.DELETE_GTT_RECORDING);
        createSNSTopic(CronTopic.DELETE_GTT_RECORDING);
        createSNSTopic(CronTopic.Devops);
        createSNSTopic(SNSTopic.ENROLL_BUNDLE_REFERRAL);
        createSNSTopic(SNSTopic.EQ_DONE);
        createSNSTopic(CronTopic.ISL_csv_uploaded);
        createSNSTopic(CronTopic.MyNotify);
        createSNSTopic(CronTopic.NotifyMe);
        createSNSTopic(SNSTopicOTF.ORDER_UPDATED);
        createSNSTopic(CronTopic.OTF_SESSION_FEEDBACK_GENERATED);
        createSNSTopic(SNSTopicOTF.OTM_SNAPSHOT_SESSION_ENDED);
        createSNSTopic(SNSTopic.PROCESS_OTF_POLLS_DATA);
        createSNSTopic(SNSTopic.REPLAY_SESSION_PREPARED);
        createSNSTopic(SNSTopic.SESSION_DELAYED_EVENTS);
        createSNSTopic(SNSTopic.SESSION_EVENTS);
        createSNSTopic(SNSTopic.SESSION_EVENTS_TEMP);
        createSNSTopic(CronTopic.TEST_CRON);
        createSNSTopic(CronTopic.Test);
        createSNSTopic(SNSTopic.USER_EVENTS);
        createSNSTopic(SNSTopic.WEBINAR_HOMEDEMO_LEADSQUARED);
        createSNSTopic(SNSTopic.SALES_DEMO_LEADSQUARED);
        //ec2-instances
        //lambda-sns-test
        createSNSTopic(SNSTopic.UPDATE_LEAD_GPS_TO_LS);
        createSNSTopic(SNSTopic.PAGE_SPEED_LIGHT_HOUSE);
        createSNSTopic(SNSTopic.REVISE_JEE_POSTTEST);
        createSNSTopic(SNSTopic.VSAT_EVENT_UPDATED);
    }

    @Override
    public void createSubscriptions() {

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        // todo  -- remove this once redis clusters have stabilised with correct data - needs to be manually deleted from aws sns console
        // createCronSubscription(SNSTopicOTF.BATCH_EVENTS_OTF, "onetofew/session/handleBatchEventsForOTF");
        createCronSubscription(SNSTopicOTF.BATCH_EVENTS_OTF, "notification-centre/chat/handleOTFBatchEvents");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "cms/webinar/sendReminders");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "cms/webinar/sendWebinarRegistrationQuestions");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "dropbox/file-change/sns");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "calendar/retryCalendarEvents");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "cms/webinar/syncGtwAttendence");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "/cms/webinar/send-email-after-webinar-completed");
        createCronSubscription(CronTopic.CRON_CHIME_2HOURLY, "subscriptionRequest/expiryReminder");
        createCronSubscription(CronTopic.CRON_CHIME_30_Minutes, "onetofew/feedback/scheduler");
        createCronSubscription(CronTopic.CRON_CHIME_30_Minutes, "onetofew/feedback/triggerSessionEnd");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "lms/expireContents");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "session/markSessionExpired");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "notification-centre/email/communicationReminder");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "subscriptionRequest/expire");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "onetofew/session/otfScheduler");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "/cms/webinar/send-webinar-attend-alert");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "dinero/payment/checkInstalmentDues");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "listing/updateFaceCount");        
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "getVedantuDailyMigration");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "session/updateTotalSessionDuration");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "lms/moodleReminders");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "dinero/payment/subscriptionMetrices");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "onetofew/enroll/checkTrialEnrollments");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "getEngagementDailyReport");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_11PM, "feedbackForm/createFormInstance");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_11PM, "feedbackForm/expireForms");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_1AM_IST, "onetofew/session/otfDailySessions");        
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_1AM_IST, "calendarEntry/syncNextMonthCalendar");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_1AM_IST, "courseplan/verifyCoursePlanHoursConsistency");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_1AM_IST, "onetofew/session/deleteRecordingsCron");
        createCronSubscription(CronTopic.CRON_CHIME_FORTNIGHTLY, "getTeacherPercentile");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "onetofew/session/otfRecordingScheduler");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "onetofew/feedback/otfFeedbackscheduler");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "courseplan/alertNonTrialPayment");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "requestcallback/updateLeadsSessionSubscription");
        createCronSubscription(CronTopic.CRON_CHIME_HOURLY, "courseplan/alertAboutFirstRegularSessionDate");
        createCronSubscription(CronTopic.CRON_CHIME_LISTING_DAILY_COUNT, "listing/oto/updateDailyCount");
        createCronSubscription(SNSTopic.ENROLL_BUNDLE_REFERRAL, "postforwarder?forwardTo=/platform/referral/handleBundleReferral");
        createCronSubscription(SNSTopic.SESSION_EVENTS, "session/sessionEventsOperation");
        createCronSubscription(SNSTopic.SESSION_EVENTS, "notification-centre/chat/handleOTOSessionEvents");
        createCronSubscription(SNSTopic.SESSION_EVENTS, "courseplan/handleSessionEvents");

        // migrated to scheduling
        // createCronSubscription(SNSTopic.SESSION_EVENTS, "session/handleSessionEventsForLatestSession");
        // createCronSubscription(SNSTopicOTF.SESSION_EVENTS_OTF, "onetofew/session/handleSessionEventsForOTF");

        createCronSubscription(SNSTopic.SESSION_EVENTS, "dinero/payout/calculateSessionPayout");
        createCronSubscription(CronTopic.TEST_CRON, "notification-centre/email/communicationReminder");
        createCronSubscription(CronTopic.Test, "session/markSessionExpired");
        createCronSubscription(SNSTopic.USER_EVENTS, "user/userEventsOperation");
        createCronSubscription(CronTopic.OTF_SESSION_FEEDBACK_GENERATED, "feedbackForm/sendFeedbackToSlack");
        createCronSubscription(SNSTopic.WEBINAR_HOMEDEMO_LEADSQUARED, "cms/webinar/webinarHomeDemoDetailsSendToLeadSquaredThroughSqs");
        createCronSubscription(SNSTopic.SALES_DEMO_LEADSQUARED, "cms/webinar/salesDemoSendToLeadSquaredThroughSqs");
        createCronSubscription(SNSTopic.UPDATE_LEAD_GPS_TO_LS, "leadsquared/updateGPSLocalesToLSUsingSNS");
        createCronSubscription(SNSTopic.PAGE_SPEED_LIGHT_HOUSE, "pageSpeed/triggerPageUrlsToSQS");
        createCronSubscription(SNSTopic.REVISE_JEE_POSTTEST, "revise/updateAfterReviseJeeTest");
        createCronSubscription(SNSTopic.VSAT_EVENT_UPDATED, "/vsat/changeRedisVsatEventDetails");

    }
}
