package com.vedantu.platform.managers.review;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.platform.dao.review.RemarkDAO;
import com.vedantu.platform.mongodbentities.review.Remark;
import com.vedantu.review.pojo.RemarkContext;
import com.vedantu.review.requests.AddRemarkReq;
import com.vedantu.review.response.GetLastSessionRemarkResp;
import com.vedantu.review.response.RemarkResp;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.dozer.DozerBeanMapper;

@Service
public class RemarkManager {

	@Autowired
	private LogFactory logFactory;

	private Logger logger = logFactory.getLogger(RemarkManager.class);

	@Autowired
	private RemarkDAO remarkDAO;

        @Autowired
        private DozerBeanMapper mapper; 

	private static Gson gson = new Gson();

	public RemarkResp addRemark(AddRemarkReq req) throws VException {
		req.verify();
		Remark remark = null;
		if(req.getContextType().equals(RemarkContext.SESSION)){
			Query query = new Query();
			query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_ID).is(req.getContextId()));
			query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_TYPE).is(req.getContextType()));
			
			List<Remark> remarks = remarkDAO.runQuery(query, Remark.class);
			if(ArrayUtils.isNotEmpty(remarks)){
				remark = remarks.get(0);
			}else{
				remark = new Remark();
			}
		}else{
			remark = new Remark();
		}
		
		remark.setContextId(req.getContextId());
		remark.setContextType(req.getContextType());
		remark.setRemark(req.getRemark());
		remark.setReviewer(req.getCallingUserId());
		if(req.getBoardId() != null){
			remark.setBoardId(req.getBoardId());
		}
		if (StringUtils.isNotEmpty(req.getNextSessionRemark())) {
			remark.setNextSessionRemark(req.getNextSessionRemark());
		}
		if (req.getContextType().equals(RemarkContext.OTF_BATCH)
				|| req.getContextType().equals(RemarkContext.COURSE_PLAN)) {
			if (req.getToUser() != null) {
				remark.setToUser(req.getToUser());
			} else {
				throw new VException(ErrorCode.BAD_REQUEST_ERROR, "student userId is null");
			}
		}
		remarkDAO.create(remark);
		RemarkResp resp = mapper.map(remark, RemarkResp.class);
		return resp;
	}

	public RemarkResp editRemark(AddRemarkReq req) throws BadRequestException {
		req.verify();
		Query query = new Query();
		query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_ID).is(req.getContextId()));
		query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_TYPE).is(req.getContextType()));
		if (req.getContextType().equals(RemarkContext.OTF_BATCH)
				|| req.getContextType().equals(RemarkContext.COURSE_PLAN)) {
			if (req.getToUser() != null) {
				query.addCriteria(Criteria.where(Remark.Constants.TO_USER).is(req.getToUser()));
			} else {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "student userId is null");
			}
		}

		List<Remark> remarks = remarkDAO.runQuery(query, Remark.class);
		if (ArrayUtils.isEmpty(remarks) || remarks.size() > 1) {
			// throw error
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "student userId is null");
		}
		Remark remark = remarks.get(0);
		remark.setContextId(req.getRemark());
		if (StringUtils.isNotEmpty(req.getNextSessionRemark())) {
			remark.setNextSessionRemark(req.getNextSessionRemark());
		}
		if (req.getToUser() != null) {
			remark.setToUser(req.getToUser());
		}
		remarkDAO.create(remark);
		RemarkResp resp = mapper.map(remark, RemarkResp.class);
		return resp;

	}

	public GetLastSessionRemarkResp getLastSessionRemark(String batchId, Long boardId,String currentSessionId) throws VException {
		
		GetLastSessionRemarkResp getLastSessionRemarkResp = new GetLastSessionRemarkResp();
		String ontofewEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT")+"/onetofew/";
		String url = ontofewEndpoint + "session/getTeacherForBatchIdAndBoardId";
		String boardIdUrl = null;
		String completeUrl = url + "?batchId=" + batchId;
		if (boardId != null) {
			completeUrl += "&boardId=" + boardId;
		}

		ClientResponse resp = WebUtils.INSTANCE.doCall(completeUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		Type listType = new TypeToken<ArrayList<OTFSessionInfo>>() {
        }.getType();
		List<OTFSessionInfo> sessionInfos = gson.fromJson(jsonString, listType);

		OTFSessionInfo sessionInfo = null;
		Query query = new Query();
		List<String> sessionIds = new ArrayList<>();
		if(StringUtils.isNotEmpty(currentSessionId) ){
			sessionIds.add(currentSessionId);
			for(OTFSessionInfo info : sessionInfos){
				if(!info.getId().equals(currentSessionId)){
					sessionInfo = info;
				}
			}
		}else{
			if(ArrayUtils.isNotEmpty(sessionInfos)){
				sessionInfo = sessionInfos.get(0);
			}			
		}
		if (sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getPresenter())){
			query.addCriteria(Criteria.where(Remark.Constants.REVIEWER).is(Long.parseLong(sessionInfo.getPresenter())));
		}
		if(sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getId())){
			sessionIds.add(sessionInfo.getId());
		}

		if(ArrayUtils.isNotEmpty(sessionIds))
		{			
			query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_ID).in(sessionIds));

			query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_TYPE).is(RemarkContext.SESSION));
			query.with(Sort.by(Sort.Direction.DESC, Remark.Constants.LAST_UPDATED));

			List<Remark> remarks = remarkDAO.runQuery(query, Remark.class);
			if (ArrayUtils.isNotEmpty(remarks)) {
				for(Remark remark : remarks){
					if(StringUtils.isNotEmpty(currentSessionId) && getLastSessionRemarkResp.getCurrentSessionRemark() == null){
						if(remark.getContextId().equals(currentSessionId) ){
							getLastSessionRemarkResp.setCurrentSessionRemark(mapper.map(remark, RemarkResp.class));
						}
						}
					if(getLastSessionRemarkResp.getLastSessionRemark()== null && sessionInfo != null && StringUtils.isNotEmpty(sessionInfo.getId())){
						if(remark.getContextId().equals(sessionInfo.getId())){
							getLastSessionRemarkResp.setLastSessionRemark(mapper.map(remark, RemarkResp.class));
						}
					}					
				}
				return getLastSessionRemarkResp;
			} else {
				throw new NotFoundException(ErrorCode.REMARK_NOT_FOUND, "Remark for Session not Found");
			}
		}else{
	
		throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "Previous Session or currentSessionId not Found");
	}
	}

	public Map<Long, RemarkResp> getBatchLevelRemarksByTeacher(TeacherBatchProgressReq req) throws VException {
		req.verify();
		// TODO : remark batch Level
		Query query = new Query();
		Criteria reviewer = Criteria.where(Remark.Constants.REVIEWER).is(req.getTeacherId());
		Criteria boardId = Criteria.where(Remark.Constants.BOARD_ID).is(req.getBoardId());
		Criteria criteria = new Criteria();
		criteria.orOperator(reviewer,boardId);
		query.addCriteria(criteria);
		query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_ID).is(req.getContextId()));

		query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_TYPE).is(RemarkContext.OTF_BATCH));
		query.with(Sort.by(Sort.Direction.DESC, Remark.Constants.LAST_UPDATED));
		Map<Long, RemarkResp> mapRemark = new HashMap<>();
		Set<String> ids = new HashSet<>();
		if (ArrayUtils.isNotEmpty(req.getStudentIds())) {
			ids.addAll(req.getStudentIds());
		}

		List<Remark> remarks = remarkDAO.runQuery(query, Remark.class);
		if (ArrayUtils.isNotEmpty(remarks)) {
			for (Remark remark : remarks) {
				if (ids.contains(remark.getToUser().toString())) {
					if (!mapRemark.containsKey(remark.getToUser())) {
						mapRemark.put(remark.getToUser(), mapper.map(remark, RemarkResp.class));						
						ids.remove(remark.getToUser().toString());						
					}
				}
				if (ArrayUtils.isEmpty(ids)) {
					break;
				}
			}
		}
		return mapRemark;
	}
        
        public RemarkResp getRemarkForSession(String sessionId){
            Remark remark =  remarkDAO.getByContextId(sessionId, RemarkContext.SESSION);
            if(remark == null){
                return null;
            }
            RemarkResp response = mapper.map(remark, RemarkResp.class);
            return response;
        }

	public List<RemarkResp> getBatchRemarkForStudent(StudentBatchProgressReq req) throws VException {
		req.verify();

		List<RemarkResp> remarkResps = new ArrayList<>();
		if (req.getBoardId() != null) {
			Query query = new Query();
			query.addCriteria(Criteria.where(Remark.Constants.BOARD_ID).is(req.getBoardId()));
			query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_ID).is(req.getContextId()));
			query.addCriteria(Criteria.where(Remark.Constants.TO_USER).is(req.getUserId()));

			query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_TYPE)
					.in(Arrays.asList(RemarkContext.OTF_BATCH, RemarkContext.COURSE_PLAN)));
			query.with(Sort.by(Sort.Direction.DESC, Remark.Constants.LAST_UPDATED));
			remarkDAO.setFetchParameters(query, req);

			List<Remark> remarks = remarkDAO.runQuery(query, Remark.class);
			
			if (ArrayUtils.isNotEmpty(remarks)) {
				for(Remark remark : remarks){
					RemarkResp response = mapper.map(remark, RemarkResp.class);
					remarkResps.add(response);
				}				
				return remarkResps;
			} else {
				return remarkResps;
				//throw new NotFoundException(ErrorCode.REMARK_NOT_FOUND, "Previous Remarks not Found");
			}
		} else {
			return remarkResps;
		//	throw new NotFoundException(ErrorCode.SESSION_NOT_FOUND, "Previous Session not Found");
		}
	}
}
