package com.vedantu.platform.dao.fosAgent;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.entity.FOSAgent;
import com.vedantu.platform.pojo.FOSAgentPOJO;
import com.vedantu.platform.request.FOSAgentReq;
import com.vedantu.platform.request.FosAgentRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FOSAgentDAO extends AbstractMongoDAO {
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(FOSAgentDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public FOSAgent create(FOSAgent newAgent) throws BadRequestException {
        if (newAgent != null) {
            logger.info("Creating agents");
            //logger.info("The agent to be inserted have agent code "+newAgent.getAgentCode()+"\n with details: "+newAgent);
            FOSAgent existingAgent = getFOSAgentById(newAgent.getAgentCode());
            //logger.info("Existing agent is "+existingAgent);
            if (existingAgent == null) {
                logger.info("Pusing new agent");
                existingAgent = newAgent;
            } else {
                logger.info("Pushing existing agent");
                populateAgent(newAgent, existingAgent);
            }
            logger.info("existingAgent" + existingAgent.toString());
            saveEntity(existingAgent);
            return newAgent;
        }
        return null;
    }

    public void populateAgent(FOSAgent newAgent, FOSAgent existingAgent) {
        if (!existingAgent.getAgentCode().equals(newAgent.getAgentCode())) {
            existingAgent.setAgentCode(newAgent.getAgentCode());
        }
        if (existingAgent.getAgentName() != null && !existingAgent.getAgentName().equals(newAgent.getAgentName())) {
            existingAgent.setAgentName(newAgent.getAgentName());
        }
        if (!existingAgent.getAgentEmailId().equals(newAgent.getAgentEmailId())) {
            existingAgent.setAgentEmailId(newAgent.getAgentEmailId());
        }
        if (newAgent.getDOJ_EDOJ() != 0 && existingAgent.getDOJ_EDOJ() != newAgent.getDOJ_EDOJ()) {
            existingAgent.setDOJ_EDOJ(newAgent.getDOJ_EDOJ());
        }
        if (newAgent.getLocation() != null && (existingAgent.getLocation() == null || !existingAgent.getLocation().equals(newAgent.getLocation()))) {
            existingAgent.setLocation(newAgent.getLocation());
        }
        if (newAgent.getDateOfResignation() != 0 && existingAgent.getDateOfResignation() != newAgent.getDateOfResignation()) {
            existingAgent.setDateOfResignation(newAgent.getDateOfResignation());
        }
        if (newAgent.getReportingTeamLeadId() != null && (existingAgent.getReportingTeamLeadId() == null || !existingAgent.getReportingTeamLeadId().equals(newAgent.getReportingTeamLeadId()))) {
            existingAgent.setReportingTeamLeadId(newAgent.getReportingTeamLeadId());
        }
        if (newAgent.getReportingManagerId() != null && (existingAgent.getReportingManagerId() == null || !existingAgent.getReportingManagerId().equals(newAgent.getReportingManagerId()))) {
            existingAgent.setReportingManagerId(newAgent.getReportingManagerId());
        }
        if (newAgent.getStatus() != null && (existingAgent.getStatus() == null || !newAgent.getStatus().equals(existingAgent.getStatus()))) {
            existingAgent.setStatus(newAgent.getStatus());
        }
        if (newAgent.getDesignation() != null && (existingAgent.getDesignation() == null || !newAgent.getDesignation().equals(existingAgent.getDesignation()))) {
            existingAgent.setDesignation(newAgent.getDesignation());
        }
        if (newAgent.getFunction() != null && (existingAgent.getFunction() == null || !newAgent.getFunction().equals(existingAgent.getFunction()))) {
            existingAgent.setFunction(newAgent.getFunction());
        }
        if (newAgent.getSubFunction() != null && (existingAgent.getSubFunction() == null || !newAgent.getSubFunction().equals(existingAgent.getSubFunction()))) {
            existingAgent.setSubFunction(newAgent.getSubFunction());
        }

        existingAgent.setLastUpdated(newAgent.getLastUpdated());
        existingAgent.setLastUpdatedBy(newAgent.getLastUpdatedBy());
    }

    public FOSAgent getById(Long id) {
        FOSAgent fosAgent = null;
        if (!StringUtils.isEmpty(id)) {
            fosAgent = getEntityById(id, FOSAgent.class);
        }
        return fosAgent;
    }

    public List<FOSAgent> getByEmail(String email) {
        List<FOSAgent> employees = null;
        if (!StringUtils.isEmpty(email)) {
            Query query = new Query();
            String regex = ("^.*".concat(email.trim().toLowerCase())).concat(".*$");
            query.addCriteria(Criteria.where(FOSAgent.Constants.AGENT_EMAIL).regex(regex));
            employees = runQuery(query, FOSAgent.class);
        }
        return employees != null ? employees : null;
    }

    public List<FOSAgent> getEmployeesMatchingEmailStringOrAgentCode(String pattern) {
        List<FOSAgent> employees = null;
        if (!StringUtils.isEmpty(pattern)) {
            String regexForEmail = ("^.*".concat(pattern.trim().toLowerCase())).concat(".*$");
            String regexForAgentCode = ("^.*".concat(pattern.trim())).concat(".*$");
            Criteria criteria = new Criteria();
            criteria.orOperator(
                    Criteria.where(FOSAgent.Constants.AGENT_CODE).regex(regexForAgentCode),
                    Criteria.where(FOSAgent.Constants.AGENT_EMAIL).regex(regexForEmail)
            );
            Query query = new Query(criteria);
            query.fields().include(FOSAgent.Constants.AGENT_EMAIL);
            query.fields().include(FOSAgent.Constants.AGENT_CODE);
            query.fields().exclude(FOSAgent.Constants._ID);
            employees = runQuery(query, FOSAgent.class);
        }
        return employees != null ? employees : null;
    }

    public FOSAgent getByEmailId(String emailId) {
        if (!StringUtils.isEmpty(emailId)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(FOSAgent.Constants.AGENT_EMAIL).is(emailId.trim().toLowerCase()));
            List<FOSAgent> agents = runQuery(query, FOSAgent.class);
            if (agents.size() == 1) {
                return agents.get(0);
            } else if (agents.size() > 1) {
                logger.error("Multiple entries found for same email address");
                return agents.get(0);
            }
        }
        return null;
    }

    public List<FOSAgent> getFOSAgentByIds(List<String> employeeCodes, List<String> includeFields) {
        List<FOSAgent> employees = null;
        if (employeeCodes != null && !employeeCodes.isEmpty()) {
            Query query = new Query();
            query.addCriteria(Criteria.where(FOSAgent.Constants.AGENT_CODE).in(employeeCodes));
            employees = runQuery(query, FOSAgent.class, includeFields);
        }
        return employees != null ? employees : null;
    }

    public FOSAgent getFOSAgentById(String agentCode) {
        FOSAgent employee = null;
        //logger.info("Getting fos agent for agent code "+agentCode);
        if (!StringUtils.isEmpty(agentCode)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(FOSAgent.Constants.AGENT_CODE).is(agentCode));
            List<FOSAgent> employees = runQuery(query, FOSAgent.class);
            //logger.info("Query for agent code is: "+query+"\n and the agents retrieved are "+employees);
            if (employees == null || employees.size() == 0 || employees.isEmpty()) {
                return null;
            }
            employee = employees.get(0);
        }
        return employee != null ? employee : null;
    }

    public List<FOSAgent> getFOSEmployeeByEmailOrEmployeeCode(FosAgentRequest req) {

        Query query = new Query();
        if (ArrayUtils.isNotEmpty(req.getAgentEmailIds())) {
            query.addCriteria(Criteria.where(FOSAgent.Constants.AGENT_EMAIL).in(req.getAgentEmailIds()));
        }
        query.fields().include(FOSAgent.Constants._ID)
                .include(FOSAgent.Constants.AGENT_EMAIL);
        List<FOSAgent> employees = runQuery(query, FOSAgent.class);

        return employees != null ? employees : null;
    }
}
