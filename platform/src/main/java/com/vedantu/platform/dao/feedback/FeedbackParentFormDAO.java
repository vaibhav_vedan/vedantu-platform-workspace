/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.feedback;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.feedback.FeedbackParentForm;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author parashar
 */
@Service
public class FeedbackParentFormDAO extends AbstractMongoDAO{
    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
    
    @Autowired
    private LogFactory logfactory;
    
    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(FeedbackParentFormDAO.class);    
    
    public FeedbackParentFormDAO() {
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void create(FeedbackParentForm p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        boolean newCP = StringUtils.isEmpty(p.getId());
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
    }
    
    public void save(FeedbackParentForm p) {
        if (p != null) {
            boolean newCP = StringUtils.isEmpty(p.getId());
            saveEntity(p);
        }
    }
    
    public FeedbackParentForm getParenFormById(String id){
        FeedbackParentForm feedbackParentForm = null;
        if(!StringUtils.isEmpty(id)){
            feedbackParentForm = getEntityById(id, FeedbackParentForm.class);
        }
        return feedbackParentForm;        
    }
    
    public List<FeedbackParentForm> getParentFormsToEvaluate(Long currentTime){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackParentForm.Constants.VALID_TILL).gte(currentTime));
        return runQuery(query, FeedbackParentForm.class);
        
    }
    
    public List<FeedbackParentForm> getParentForms(int start, int size){
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, FeedbackParentForm.Constants.CREATION_TIME));        
        setFetchParameters(query, start, size);
        
        return runQuery(query, FeedbackParentForm.class);
    }
        
}
