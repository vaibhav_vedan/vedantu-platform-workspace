/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.platform.enums.DoubtAppType;
import com.vedantu.platform.mongodbentities.DoubtsAppVersion;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class DoubtsAppVersionDAO extends AbstractMongoDAO{

    @Autowired
    private MongoClientFactoryMobile mongoClientFactoryMobile;

    public DoubtsAppVersionDAO(){
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactoryMobile.getMongoOperations();
    }  
    
    public void create(DoubtsAppVersion p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }
    
    public DoubtsAppVersion getLatest(String currentVersion, DoubtAppType doubtAppType) throws NotFoundException{
        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtsAppVersion.Constants.VERSION_NAME).is(currentVersion));
//        query.with(new Sort(Sort.Direction.DESC, DoubtsAppVersion.Constants.CREATION_TIME));
        setFetchParameters(query, 0, 1);
        List<DoubtsAppVersion> doubtsAppVersions = runQuery(query, DoubtsAppVersion.class);
        if(ArrayUtils.isEmpty(doubtsAppVersions)){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "latest app version not found");
        }
        return doubtsAppVersions.get(0);
    }
    
}
