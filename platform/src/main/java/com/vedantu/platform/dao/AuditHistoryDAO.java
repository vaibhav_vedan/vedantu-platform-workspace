package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.pojo.audit.AuditHistoryII;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class AuditHistoryDAO extends AbstractMongoDAO {

    public AuditHistoryDAO() {
        super();
    }

    @Autowired
    private MongoClientFactory mongoClientFactory;


    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public void create(AuditHistoryII p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }
}
