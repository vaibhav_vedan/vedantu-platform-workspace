package com.vedantu.platform.dao.loam;


import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class LOAMAmbassadorPlatformDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    private final Logger logger = logFactory.getLogger(LOAMAmbassadorPlatformDao.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public List<Board> loam_getBoardData(Long fromTime, Long thruTime, Set<String> requiredFields, boolean b, Integer start, Integer size) {
        Query query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(Board.Constants.LAST_UPDATED).gte(fromTime),
                Criteria.where(Board.Constants.LAST_UPDATED).lte(thruTime));
        query.addCriteria(andCriteria);
        /*loam_manageFieldsInclude(query, keySet, isInclude);*/
        query.with(Sort.by(Sort.Direction.DESC, Board.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);

        return runQuery(query, Board.class);
    }

    private void loam_manageFieldsInclude(Query q, Set<String> includeKeySet, boolean isInclude){
        if(includeKeySet !=null && !includeKeySet.isEmpty())
        {
            if(isInclude)
                includeKeySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
            else
                includeKeySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
        }
    }
}
