/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.filemgmt;

import com.vedantu.platform.mongodbentities.filemgmt.FileMetadata;
import com.vedantu.exception.NotFoundException;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class FileMetadataDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(FileMetadataDAO.class);

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public FileMetadataDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(FileMetadata p, Long callingUserId) {
            String callingUserIdString = null;
            if(callingUserId!=null) {
                callingUserIdString = callingUserId.toString();
            }
            if (p != null) {
                saveEntity(p, callingUserIdString);
            }
    }

    public FileMetadata getById(String id) throws NotFoundException {
        FileMetadata FileMetadata = null;
            if (!StringUtils.isEmpty(id)) {
                FileMetadata = getEntityById(id, FileMetadata.class);
            }
        return FileMetadata;
    }


    public FileMetadata getFileMetadataByLocalObjectId(String localObjectId) {
        FileMetadata fileMetadata = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(FileMetadata.Constants.LOCAL_OBJECT_ID).is(localObjectId));
        List<FileMetadata> results = runQuery(query, FileMetadata.class);
        if (null != results && !results.isEmpty()) {
            fileMetadata = results.get(0);
        }
        return fileMetadata;
    }

    public FileMetadata getFileMetadataByUserIdAndFileId(Long userId, Long fileId) {
        FileMetadata fileMetadata = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(FileMetadata.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(FileMetadata.Constants.FILE_ID).is(fileId));
        List<FileMetadata> results = runQuery(query, FileMetadata.class);
        if (null != results && !results.isEmpty()) {
            fileMetadata = results.get(0);
        }
        return fileMetadata;
    }

    public List<FileMetadata> getData(Long sessionId, Long userId,
            Long fromTime, Long tillTime, String fileType, Long start,
            Long limit, Boolean orderDesc) {
        List<FileMetadata> results = new ArrayList<>();

        Query query = new Query();
        List<Sort.Order> orderList = new ArrayList<>();
        if (orderDesc) {
            orderList.add(new Sort.Order(Sort.Direction.DESC, FileMetadata.Constants.CREATION_TIME));
        } else {
            orderList.add(new Sort.Order(Sort.Direction.ASC, FileMetadata.Constants.CREATION_TIME));
        }
        query.with(Sort.by(orderList));

        if (sessionId != null) {
            query.addCriteria(Criteria.where(FileMetadata.Constants.SESSION_ID).is(sessionId));
        }
        if (userId != null) {
            query.addCriteria(Criteria.where(FileMetadata.Constants.USER_ID).is(userId));
        }
        if (StringUtils.isNotEmpty(fileType)) {
            query.addCriteria(Criteria.where(FileMetadata.Constants.FILE_TYPE).is(fileType.toUpperCase()));
        }

        if (fromTime != null && fromTime >= 0) {
            query.addCriteria(Criteria.where(FileMetadata.Constants.CREATION_TIME).gte(fromTime));
        }

        if (tillTime != null && tillTime >= 0) {
            query.addCriteria(Criteria.where(FileMetadata.Constants.CREATION_TIME).lt(tillTime));
        }

        start = start == null ? 0 : start;
        query.skip(start.intValue());

        if (limit != null && limit != 0 && (limit >= start)) {
            query.limit((int) (limit - start));
        }

        //long queryStartTime = System.currentTimeMillis();
        results = runQuery(query, FileMetadata.class);
        //long querySuccessTime = System.currentTimeMillis();
        //logger.info("total turnaround time  "
        //        + (querySuccessTime - queryStartTime));
        if (null == results || results.isEmpty()) {
            return null;
        }

        return results;
    }

}
