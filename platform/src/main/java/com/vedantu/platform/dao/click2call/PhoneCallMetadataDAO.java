/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.click2call;

import com.vedantu.platform.mongodbentities.click2call.PhoneCallMetadata;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.StringUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class PhoneCallMetadataDAO extends AbstractMongoDAO {

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public PhoneCallMetadataDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(PhoneCallMetadata p, Long callingUserId) {
            String callingUserIdString = null;
            if(callingUserId!=null) {
                callingUserIdString = callingUserId.toString();
            }
            if (p != null) {
                saveEntity(p, callingUserIdString);
            }
    }

    public PhoneCallMetadata getById(String id) {
        PhoneCallMetadata PhoneCallMetadata = null;
            if (!StringUtils.isEmpty(id)) {
                PhoneCallMetadata = getEntityById(id, PhoneCallMetadata.class);
            }
        return PhoneCallMetadata;
    }


    public PhoneCallMetadata getPhoneCallMetadataCallSid(String callSid) {

        PhoneCallMetadata phoneCallMetadata = null;
        Query query = new Query();

        query.addCriteria(Criteria.where(PhoneCallMetadata.Constants.CALL_SID).is(callSid));
        List<PhoneCallMetadata> datas = runQuery(query, PhoneCallMetadata.class);
        if (datas != null && !datas.isEmpty()) {
            phoneCallMetadata = datas.get(0);
        }

        return phoneCallMetadata;
    }

    public List<PhoneCallMetadata> getPhonecallMetadata(String contextId, String contextType) {
        Query query = new Query();

        query.addCriteria(Criteria.where(PhoneCallMetadata.Constants.CONTEXT_ID).is(contextId));
        query.addCriteria(Criteria.where(PhoneCallMetadata.Constants.CONTEXT_TYPE).is(contextType));
        List<PhoneCallMetadata> datas = runQuery(query, PhoneCallMetadata.class);
        return datas;
    }


}
