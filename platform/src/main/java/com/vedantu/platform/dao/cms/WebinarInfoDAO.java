package com.vedantu.platform.dao.cms;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.pojo.cms.WebinarInfo;
import com.vedantu.platform.pojo.cms.WebinarInfoRes;
import com.vedantu.util.LogFactory;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;

import org.springframework.data.domain.Sort;

@Service
public class WebinarInfoDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;
    
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(WebinarInfoDAO.class);

    public WebinarInfoDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(WebinarInfo p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public void updateWebinarInfos(List<WebinarInfoRes> webinarInfoRes, String accountEmailId) {
        for (WebinarInfoRes entry : webinarInfoRes) {
            Query query = new Query();
            query.addCriteria(Criteria.where(WebinarInfo.Constants.WEBINAR_ID).is(entry.getWebinarKey()));
            List<WebinarInfo> results = runQuery(query, WebinarInfo.class);
            if (CollectionUtils.isEmpty(results)) {
                try {
                    WebinarInfo webinarInfo = new WebinarInfo(entry, accountEmailId);
                    save(webinarInfo);
                } catch (Exception ex) {
                    logger.error("error in updateWebinarInfos"+ex.getMessage());
                }
            }
        }
    }

    public WebinarInfo getById(String id) {
        WebinarInfo p = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                p = (WebinarInfo) getEntityById(id, WebinarInfo.class);
            }
        } catch (Exception ex) {
            logger.error("error in getById id:"+id+" ex:"+ex.getMessage());
            p = null;
        }
        return p;
    }

    public void save(WebinarInfo p) throws Exception {
        if (p != null) {
            saveEntity(p);
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, WebinarInfo.class);
        } catch (Exception ex) {
            logger.error("error in deleteById id:"+id+" ex:"+ex.getMessage());
        }

        return result;
    }

    public List<WebinarInfo> getWebinarInfos(Long fromTime, Long tillTime, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(WebinarInfo.Constants.START_TIME).gte(fromTime)
                .lt(tillTime));
        query.with(Sort.by(Sort.Direction.DESC, WebinarInfo.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, WebinarInfo.class);
    }

}
