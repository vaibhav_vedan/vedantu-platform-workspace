package com.vedantu.platform.dao.referral;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.pojo.referral.model.Referral;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class ReferralDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReferralDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public ReferralDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Referral p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(Referral p) {
        upsert(p, null);
    }

    public void upsert(Referral p, String callingUserId) {
        try {
            if (p != null) {
                saveEntity(p, callingUserId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public Referral getById(String id) {
        Referral GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (Referral) getEntityById(id, Referral.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id,
                    Referral.class);
        } catch (Exception ex) {
        }

        return result;
    }

//		public List<ReferralStepBonus> get(
//				GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq) {
//			try {
//				Query query = new Query();
//				if (getRequestCallBacksDetailsReq.getId() != null) {
//					query.addCriteria(Criteria.where(
//							"id").is(
//							getRequestCallBacksDetailsReq.getId()));
//				}
//				if (getRequestCallBacksDetailsReq.getStudentId() != null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.STUDENT_ID).is(
//							getRequestCallBacksDetailsReq.getStudentId()));
//				}
//				if (getRequestCallBacksDetailsReq.getTeacherId() != null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.TEACHER_ID).is(
//							getRequestCallBacksDetailsReq.getTeacherId()));
//				}
//				if (getRequestCallBacksDetailsReq.getAssignedTo() != null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.ASSIGNED_TO).is(
//							getRequestCallBacksDetailsReq.getAssignedTo()));
//				}
//				if (getRequestCallBacksDetailsReq.getBoardId() != null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.BOARD_ID).is(
//							getRequestCallBacksDetailsReq.getBoardId()));
//				}
//				if (getRequestCallBacksDetailsReq.getGrade() != null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.GRADE).is(
//							getRequestCallBacksDetailsReq.getGrade()));
//				}
//				if (getRequestCallBacksDetailsReq.getTarget() != null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.TARGET).is(
//							getRequestCallBacksDetailsReq.getStudentId()));
//				}
//				if (getRequestCallBacksDetailsReq.getTeacherLeadStatuses() != null) {
//					query.addCriteria(Criteria.where(RequestCallBackDetails.Constants.TEACHER_LEAD_STATUS).in(getRequestCallBacksDetailsReq.getTeacherLeadStatuses()));
//					}
//				
//				if (getRequestCallBacksDetailsReq.getParent()!=null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.PARENT).is(
//							getRequestCallBacksDetailsReq.getParent()));
//				}
//				if (getRequestCallBacksDetailsReq.getTeacherActionTaken()!=null) {
//					query.addCriteria(Criteria.where(
//							RequestCallBackDetails.Constants.TEACHER_ACTION_TAKEN)
//							.is(getRequestCallBacksDetailsReq
//									.getTeacherActionTaken()));
//				}
//				
//				if (getRequestCallBacksDetailsReq.getFromTime()!=null) {
//					Long toTime = getRequestCallBacksDetailsReq.getToTime();
//					if(toTime==null){
//						toTime = System.currentTimeMillis();
//					}
//					query.addCriteria(Criteria.where(
//							AbstractEntity.Constants.CREATION_TIME)
//							.lte(toTime).gte(getRequestCallBacksDetailsReq.getFromTime()));
//				}
//				
//				if (getRequestCallBacksDetailsReq.getLimit() != null) {
//					query.limit(getRequestCallBacksDetailsReq.getLimit());
//				} 
//				
//				if (getRequestCallBacksDetailsReq.getStart() != null) {
//					query.skip(getRequestCallBacksDetailsReq.getStart());
//				} else {
//					query.skip(0);
//				}
//				query.with(Sort.by(Sort.Direction.DESC,
//						AbstractEntity.Constants.CREATION_TIME));
    //
//				List<RequestCallBackDetails> result = runQuery(query,
//						RequestCallBackDetails.class);
    //
//				return result;
//			} catch (Throwable e) {
//				throw e;
//			}
    //
//		}
    public Boolean updateAll(List<Referral> referralList) {
        try {
            insertAllEntities(referralList, Referral.class.getSimpleName());
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public Referral getByUserIds(Long userId, Long referrerUserId) {
        try {
            Query query = new Query();
            if (userId != null) {
                query.addCriteria(Criteria.where("userId").is(userId));
            }
            if (referrerUserId != null) {
                query.addCriteria(Criteria.where("referrerUserId").is(referrerUserId));
            }
            List<Referral> result = runQuery(query,Referral.class);

            if (result != null && !result.isEmpty()) {
                return result.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.info("getting referral failed:userId=" + userId + "& referrer Id=" + referrerUserId + e.getMessage());
            throw e;
        }
    }
    
    public List<Referral> getByReferrerId(Long referrerUserId, ReferralStep referralStep, String entityId) {
        try {
            Query query = new Query();
            if (referrerUserId != null) {
                query.addCriteria(Criteria.where("referrerUserId").is(referrerUserId));
            }
            if(referralStep != null){
            	query.addCriteria(Criteria.where("stepsDone").in(referralStep));
            }
            if(StringUtils.isNotEmpty(entityId)){
            	query.addCriteria(Criteria.where("entityId").is(entityId));
            }
            List<Referral> result = runQuery(query,Referral.class);

            if (result != null && !result.isEmpty()) {
                return result;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.info("getting referral failed: referrer Id=" + referrerUserId + e.getMessage());
            throw e;
        }
    }
}
