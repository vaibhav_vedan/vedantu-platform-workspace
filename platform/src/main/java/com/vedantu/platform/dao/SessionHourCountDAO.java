package com.vedantu.platform.dao;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.SessionHourCount;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class SessionHourCountDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionHourCountDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(SessionHourCount p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
        }
    }

    public SessionHourCount getById(String id) {
    	SessionHourCount hourCountEntry = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                hourCountEntry = getEntityById(id, SessionHourCount.class);
            }
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
            hourCountEntry = null;
        }
        return hourCountEntry;
    }
    
    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, SessionHourCount.class);
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
        }

        return result;
    }
}
