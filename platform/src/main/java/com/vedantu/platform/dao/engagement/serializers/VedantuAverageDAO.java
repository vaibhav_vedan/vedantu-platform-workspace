package com.vedantu.platform.dao.engagement.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.mongodbentities.engagement.VedantuAverage;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;

@Service
public class VedantuAverageDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public VedantuAverageDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(VedantuAverage p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public VedantuAverage getById(String id) {
        VedantuAverage ChannelUserInfo = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                ChannelUserInfo = (VedantuAverage) getEntityById(id, VedantuAverage.class);
            }
        } catch (Exception ex) {
            ChannelUserInfo = null;
        }
        return ChannelUserInfo;
    }


    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, VedantuAverage.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<VedantuAverage> getByStartTimeAndSubject(Long startTime, Long endTime, String subject) {
        Query query = new Query();
        query.addCriteria(Criteria.where("startTime").gte(startTime).lte(endTime).and("subject").is(subject));
        query.with(Sort.by(Sort.Direction.ASC, "startTime"));
        return getMongoOperations().find(query, VedantuAverage.class);
    }
}
