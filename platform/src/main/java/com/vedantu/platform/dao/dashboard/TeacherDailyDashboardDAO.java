package com.vedantu.platform.dao.dashboard;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.dashboard.TeacherDailyDashboard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
@Service
public class TeacherDailyDashboardDAO extends AbstractMongoDAO{

	@Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TeacherDailyDashboardDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public TeacherDailyDashboardDAO() {
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void create(TeacherDailyDashboard p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    
	public void updateAll(List<TeacherDailyDashboard> p) {
		if (p != null && !p.isEmpty()) {
			insertAllEntities(p, TeacherDailyDashboard.class.getSimpleName());
		}
	}
	
	public List<TeacherDailyDashboard> getTeacherDailyDashboard(int start,int size) {
		Query query = new Query();
		query.skip(start);
        query.limit(size);
        query.with(Sort.by(Sort.Direction.DESC, TeacherDailyDashboard.Constants.CREATION_TIME));
        return runQuery(query,TeacherDailyDashboard.class);
	}
}
