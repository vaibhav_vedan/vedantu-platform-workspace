/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.social;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.mongodbentities.social.Bookmark;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class BookmarkDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BookmarkDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public BookmarkDAO() {
        super();
    }

    public void save(Bookmark entity) {
        saveEntity(entity);
    }

    public List<Bookmark> getBookmarks(Long userId, List<String> inputContextIds, SocialContextType socialEntityType,
            String entityId, int start, int size) {
        Query query = prepareQuery(userId, inputContextIds, socialEntityType, entityId);
        setFetchParameters(query, start, size);
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        logger.info("query "+query);
        return runQuery(query, Bookmark.class);
    }

    public Query prepareQuery(Long userId, List<String> inputContextIds,
            SocialContextType socialEntityType, String entityId) {

        List<String> queryContextIds = new ArrayList<>();

        Query query = new Query();
        query.addCriteria(Criteria.where(Bookmark.Constants.BOOKMARK_VALUE).is(true));
        if (userId != null) {
            query.addCriteria(Criteria.where(Bookmark.Constants.USER_ID).is(userId));
        }
        if (StringUtils.isNotEmpty(entityId)) {
            queryContextIds.add(entityId);
            if(ArrayUtils.isNotEmpty(inputContextIds)) {
                queryContextIds.addAll(inputContextIds);
            }
        } else if(ArrayUtils.isNotEmpty(inputContextIds)) {
            queryContextIds.addAll(inputContextIds);
        }
        if(ArrayUtils.isNotEmpty(queryContextIds)) {
            query.addCriteria(Criteria.where(Bookmark.Constants.CONTEXT_ID).in(inputContextIds));
        }
        if (socialEntityType != null) {
            query.addCriteria(Criteria.where(Bookmark.Constants.SOCIAL_CONTEXT_TYPE).is(socialEntityType));
        }
        return query;
    }

    public Map<String, Boolean> getBookmarkedMap(Long userId,
            List<String> entityIds) {

        Map<String, Boolean> map = new HashMap<>();
        if (userId == null || ArrayUtils.isEmpty(entityIds)) {
            return map;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Bookmark.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Bookmark.Constants.CONTEXT_ID).in(entityIds));

        List<Bookmark> bookmarks = runQuery(query, Bookmark.class);
        if (ArrayUtils.isNotEmpty(bookmarks)) {
            for (Bookmark bookmark : bookmarks) {
                map.put(bookmark.getContextId(), bookmark.isBookmark());
            }
        }
        return map;
    }

}
