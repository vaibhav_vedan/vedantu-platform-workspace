package com.vedantu.platform.dao.broadcast;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.pojo.broadcast.BroadcastLevel;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.seo.entity.Category;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class BroadcastLevelDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(RequestCallbackDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public BroadcastLevelDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(BroadcastLevel p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(BroadcastLevel p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public BroadcastLevel getById(String id) {
        BroadcastLevel GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (BroadcastLevel) getEntityById(id, BroadcastLevel.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id,
                    BroadcastLevel.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<BroadcastLevel> get(String id, BroadcastType broadcastType,
            Integer level, Integer start, Integer size) {
        try {
            Query query = new Query();

            if (id != null) {
                query.addCriteria(Criteria.where(
                        "id").is(
                                id));
            } else {

                if (broadcastType != null) {
                    query.addCriteria(Criteria.where(
                            BroadcastLevel.Constants.BROADCAST_TYPE).is(
                                    broadcastType));
                }

                if (level != null) {
                    query.addCriteria(Criteria.where(
                            BroadcastLevel.Constants.LEVEL).is(
                                    level));
                }

                if (size != null) {
                    query.limit(size);
                }

                if (start != null) {
                    query.skip(start);
                } else {
                    query.skip(0);
                }
            }
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));

            List<BroadcastLevel> result = runQuery(query,
                    BroadcastLevel.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }

}
