package com.vedantu.platform.dao.board;

import com.google.gson.Gson;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.mongodbentities.board.Board;

import java.util.*;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.session.pojo.VisibilityState;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.dozer.DozerBeanMapper;

@Service
public class BoardDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(BoardDAO.class);

    @Autowired
    CounterService counterService;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @Autowired
    RedisDAO redisDAO;

    @Autowired
    private DozerBeanMapper mapper;
    private final Gson gson = new Gson();
    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    public static Integer redisExpiry = ConfigUtils.INSTANCE.getIntValue("redis.expiry.sessionvsserverkey");

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public BoardDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    /*
	 * public void create(Board p) { try { if (p != null) {
	 * AbstractDAO.saveEntity(p); } } catch (Exception ex) {
	 * logger.error(ex.getMessage()); } }
     */
    public void upsert(Board p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            if (p.getId() == null || p.getId() == 0L) {
                p.setId(counterService.getNextSequence(p.getClass().getSimpleName()));
            }
            super.saveEntity(p, callingUserIdString);

            try {
                // CreateElasticSearchTask.INSTANCE.createTask(
                // ElasticSearchTask.ADD_BOARD, (new Gson().toJson(board)));
                Map<String, Object> payload = new HashMap<String, Object>();
                payload.put("board", p);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_ADD_BOARD, payload);
                asyncTaskFactory.executeTask(params);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            try {
                redisDAO.del(getBoardRedisKey(p.getId()));
            } catch (Exception e) {
                logger.error("Error in redis " + e.getMessage());
            }
        }
    }

    public Board getById(Long id) {
        Board boardRes = null;
        if (id != null) {
            String key = getBoardRedisKey(id);
            try {
                String value = redisDAO.get(key);
                if (!StringUtils.isEmpty(value)) {
                    boardRes = gson.fromJson(value, Board.class);
                    if (boardRes != null) {
                        return boardRes;
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.error("Error in redis " + e.getMessage());
            }
            boardRes = super.getEntityById(id, Board.class);
            if (boardRes != null) {
                try {
                    redisDAO.setex(key, gson.toJson(boardRes),redisExpiry);
                } catch (Exception e) {
                    logger.error("Error in redis set " + e.getMessage());
                }
            }
        }
        return boardRes;
    }

    public List<Board> getBoards(String category, String grade, String exam,
            Long parentId, VisibilityState state) {
        logger.info("getBoards " + category + " " + grade + " " + exam + " " + parentId);
        Query query = new Query();

        if (!StringUtils.isEmpty(category)) {
            query.addCriteria(Criteria.where(Board.Constants.CATEGORIES).is(
                    category.trim()));
        }

        if (!StringUtils.isEmpty(grade)) {
            query.addCriteria(Criteria.where(Board.Constants.GRADES).is(
                    grade.trim()));
        }

        if (state != null) {
            query.addCriteria(Criteria.where(Board.Constants.STATE).is(state));
        }

        if (!StringUtils.isEmpty(exam)) {
            query.addCriteria(Criteria.where(Board.Constants.EXAMS).is(
                    exam.trim()));
        }

        if (parentId != null) {
            query.addCriteria(Criteria.where(Board.Constants.PARENT_ID).is(
                    parentId));
        }

        query.with(Sort.by(Sort.Direction.DESC, Board.Constants.CREATION_TIME));
        logger.info("query " + query);
        List<Board> boards = runQuery(query, Board.class);
        logger.info("getBoards" + boards);
        return boards;
    }



    public Map<Long, Board> getBoardsMap(Collection<Long> ids) {
        Map<Long, Board> boardsMap = new HashMap<>();
        if (CollectionUtils.isEmpty(ids)) {
            logger.info("ids is null.");
            return boardsMap;
        }
        boardsMap = getBoardsMapFromRedis(ids);
        List<Long> fetchList = new ArrayList<>();

        if (!boardsMap.isEmpty()) {
            for (Long id : ids) {
                if (!boardsMap.keySet().contains(id)) {
                    logger.info("board id" + id);
                    fetchList.add(id);
                }
            }
        } else {
            fetchList.addAll(ids);
        }
        if (ArrayUtils.isNotEmpty(fetchList)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Board.Constants.ID).in(fetchList));
            List<Board> boards = runQuery(query, Board.class);
            if (boards != null) {
                Map<String, String> redisMap = new HashMap<>();
                for (Board board : boards) {
                    boardsMap.put(board.getId(), board);
                    redisMap.put(getBoardRedisKey(board.getId()), gson.toJson(board));
                }
                try {
                    redisDAO.setexKeyPairs(redisMap, redisExpiry);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    logger.error("error in redis " + e.getMessage());
                }
                logger.info("getBoardsMap size " + boards.size());
            }
        }

        return boardsMap;
    }

    public Board getBoardByField(String fieldName, String fieldValue) {
        Board board = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(fieldName).is(fieldValue));
        List<Board> boards = runQuery(query, Board.class);
        if (boards != null && !boards.isEmpty()) {
            board = boards.get(0);
        }
        logger.info("getBoardByField"+ board);
        return board;
    }

    public List<Board> getBoards(Collection<Long> ids) {
        logger.info("getBoards"+ ids);
        List<Board> boards = null;
        if (CollectionUtils.isEmpty(ids)) {
            return boards;
        }
        boards = new ArrayList<>();
        Map<Long, Board> boardsMap = getBoardsMapFromRedis(ids);

        List<Long> fetchList = new ArrayList<>();

        if (!boardsMap.isEmpty()) {
            for (Long id : ids) {
                if (!boardsMap.keySet().contains(id)) {
                    fetchList.add(id);                    
                }else {
                	boards.add(boardsMap.get(id));
                }
            }
        } else {
            fetchList.addAll(ids);
        }
        if (ArrayUtils.isNotEmpty(fetchList)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Board.Constants.ID).in(ids));
            List<Board> boardsNew = runQuery(query, Board.class);
            if (ArrayUtils.isNotEmpty(boardsNew)) {
                Map<String, String> redisMap = new HashMap<>();
                for (Board board : boardsNew) {
                    boards.add(board);
                    redisMap.put(getBoardRedisKey(board.getId()), gson.toJson(board));
                }
                try {
                    redisDAO.setexKeyPairs(redisMap, redisExpiry);
                } catch (Exception e) {
                    logger.error("error in redis " + e.getMessage());
                }
                logger.info("getBoardsMap size " + boardsNew.size());
            }
        }

        return boards;
    }

    public Map<Long, com.vedantu.board.pojo.Board> getBoardsMapInBoardPojo(
            Collection<Long> ids) {
        logger.info("getBoardsMap" + ids);
        Map<Long, com.vedantu.board.pojo.Board> boardsMap = new HashMap<>();
        if (CollectionUtils.isEmpty(ids)) {
            return boardsMap;
        }
        List<Board> boards = getBoards(ids);
        if (boards != null) {
            for (Board board : boards) {
                com.vedantu.board.pojo.Board boardPojo = mapper.map(board,
                        com.vedantu.board.pojo.Board.class);
                boardsMap.put(board.getId(), boardPojo);
            }
            logger.info("getBoardsMap size " + boards.size());
        }
        return boardsMap;
    }

    public Map<Long, Board> getBoardsMapFromRedis(Collection<Long> ids) {

        Map<Long, Board> boardMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            List<String> keys = new ArrayList<>();
            for (Long id : ids) {
                if (id != null) {
                    keys.add(getBoardRedisKey(id));
                }
            }
            Map<String, String> valueMap = new HashMap<>();
            try {
                valueMap = redisDAO.getValuesForKeys(keys);
            } catch (Exception e) {
                logger.error("Error in redis " + e.getMessage());
            }
            if (valueMap != null && !valueMap.isEmpty()) {
                for (Long id : ids) {
                    if (id != null) {
                        String value = valueMap.get(getBoardRedisKey(id));
                        if (!StringUtils.isEmpty(value)) {
                            Board board = gson.fromJson(value, Board.class);
                            boardMap.put(id, board);
                        }
                    }
                }
            }
        }
        return boardMap;
    }

    public String getBoardRedisKey(Long id) {
        return env + "_BOARD_" + id;
    }
}
