package com.vedantu.platform.dao.broadcast;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.enums.broadcast.BroadcastStatus;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.pojo.broadcast.Broadcast;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.seo.entity.Category;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.dao.requestcallback.RequestCallbackDao;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class BroadcastDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(RequestCallbackDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public BroadcastDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Broadcast p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(Broadcast p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public Broadcast getById(String id) {
        Broadcast GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (Broadcast) getEntityById(id, Broadcast.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id,
                    Broadcast.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<Broadcast> get(String id, Long userId, BroadcastType broadcastType, Integer level, Long fromTime, Long toTime, BroadcastStatus status, String referenceId, Integer start, Integer size) {
        try {
            Query query = new Query();

            if (id != null) {
                query.addCriteria(Criteria.where(
                        "id").is(
                                id));
            } else {

                if (userId != null) {
                    query.addCriteria(Criteria.where(
                            Broadcast.Constants.TO_USER_ID).is(
                                    userId));
                }

                if (broadcastType != null) {
                    query.addCriteria(Criteria.where(
                            Broadcast.Constants.BROADCAST_TYPE).is(
                                    broadcastType));
                }

                if (level != null) {
                    query.addCriteria(Criteria.where(
                            Broadcast.Constants.LEVEL).is(
                                    level));
                }

                if (status != null) {
                    query.addCriteria(Criteria.where(
                            Broadcast.Constants.STATUS).is(
                                    status));
                }

                if (referenceId != null) {
                    query.addCriteria(Criteria.where(
                            Broadcast.Constants.REFERENCE_ID).is(
                                    referenceId));
                }

                if (fromTime != null) {
                    if (toTime == null) {
                        toTime = System.currentTimeMillis();
                    }
                    query.addCriteria(Criteria.where(AbstractMongoStringIdEntity.Constants.CREATION_TIME)
                            .lte(toTime).gte(fromTime));
                }

                if (size != null) {
                    query.limit(size);
                }

                if (start != null) {
                    query.skip(start);
                } else {
                    query.skip(0);
                }
            }
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));

            List<Broadcast> result = runQuery(query,
                    Broadcast.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }

    public Boolean updateAll(List<Broadcast> BroadcastList) {
        try {
            insertAllEntities(BroadcastList, Broadcast.class.getSimpleName());
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
