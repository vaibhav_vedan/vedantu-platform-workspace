package com.vedantu.platform.dao.board;

import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.platform.mongodbentities.board.TeacherBoardMapping;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class TeacherBoardMappingDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TeacherBoardMappingDAO.class);
    
    @Autowired
    CounterService counterService;


    
    @Autowired
    AsyncTaskFactory asyncTaskFactory;
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public TeacherBoardMappingDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    /*
    public void create(TeacherBoardMapping p) {
        try {
            if (p != null) {
                AbstractDAO.saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    */

    public void upsert(TeacherBoardMapping p, Long callingUserId) {
            if (p != null) {
                String callingUserIdString = null;
                if(callingUserId!=null) {
                    callingUserIdString = callingUserId.toString();
                }
                if (p.getId() == null || p.getId() == 0L) {
                    p.setId(counterService.getNextSequence(p.getClass().getSimpleName()));
                }
                super.saveEntity(p, callingUserIdString);
                try {
			//CreateElasticSearchTask.INSTANCE.createTask(
			//		ElasticSearchTask.UPDATE_TEACHER_BOARD_MAPPING,
			//		(new Gson().toJson(teacherBoardMapping)));
                        //eSManager.updateTeacherBoardMapping(p);
                    Map<String, Object> payload = new HashMap<String, Object>();
                    payload.put("teacherBoardMapping", p);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_UPDATE_TEACHER_BOARD_MAPPING, payload);
                    asyncTaskFactory.executeTask(params);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
            }
    }

    public TeacherBoardMapping getById(Long id) {
        TeacherBoardMapping GetRequestsResponse = null;
            if (id!=null) {
                GetRequestsResponse = super
                        .getEntityById(id, TeacherBoardMapping.class);
            }
        return GetRequestsResponse;
    }

    public int deleteById(Long id) {
        int result = 0;
            result = super.deleteEntityById(id,
                    TeacherBoardMapping.class);
            
            try {
                Map<String, Object> payload = new HashMap<>();
                    payload.put("id", id);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ES_DELETE_TEACHER_BOARD_MAPPING, payload);
                    asyncTaskFactory.executeTask(params);
                //eSManager.deleteTeacherBoardMapping(id);
            } catch (Exception ex) {
                logger.error("Exception : ", ex);
            }

        return result;
    }


    public TeacherBoardMapping getTeacherBoardMapping(Long userId,
            String category, String grade) {
        logger.info("getTeacherBoardMapping"+ category+ grade);
        TeacherBoardMapping teacherBoardMapping = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(TeacherBoardMapping.Constants.CATEGORY).is(category));
        query.addCriteria(Criteria.where(TeacherBoardMapping.Constants.GRADE).is(grade));

        List<TeacherBoardMapping> results = runQuery(query, TeacherBoardMapping.class);
        if (null != results && !results.isEmpty()) {
            teacherBoardMapping = results.get(0);
        }
        logger.info("getTeacherBoardMapping "+ teacherBoardMapping);
        return teacherBoardMapping;
    }

    public List<TeacherBoardMapping> getTeacherBoardMappings(Long userId) {
        logger.info("getTeacherBoardMappings "+ userId);

        Query query = new Query();
        query.addCriteria(Criteria.where(User.Constants.USER_ID).is(userId));
        List<TeacherBoardMapping> results = runQuery(query, TeacherBoardMapping.class);
        logger.info("getTeacherBoardMappings "+  results);
        return results;
    }

}
