/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.SchoolData;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class SchoolDataDAO extends AbstractMongoDAO{
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(SchoolDataDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;   

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public List<SchoolData> getSchools(String pinCode, String state, String country, String board,String nameQuery,String addressQuery,Integer start,Integer size){
        logger.info("getSchools "+",start:"+start+",size:"+size);
        List<SchoolData> schools = new ArrayList<>();
        Query query = new Query();
        if (StringUtils.isNotEmpty(pinCode)) {
            query.addCriteria(Criteria.where(SchoolData.Constants.PIN_CODE).is(pinCode));
        }
        if (StringUtils.isNotEmpty(state)) {
            query.addCriteria(Criteria.where(SchoolData.Constants.STATE).is(state));
        }
        if (StringUtils.isNotEmpty(country)) {
            query.addCriteria(Criteria.where(SchoolData.Constants.COUNTRY).is(country));
        }
        if (StringUtils.isNotEmpty(board)) {
            query.addCriteria(Criteria.where(SchoolData.Constants.BOARD).is(board));
        }
        if (StringUtils.isNotEmpty(nameQuery)) {
            nameQuery = nameQuery.toLowerCase();
            query.addCriteria(Criteria.where(SchoolData.Constants.SEARCH_STRING).regex(Pattern.quote(nameQuery), "i"));
        }
        if (StringUtils.isNotEmpty(addressQuery)) {
            addressQuery = addressQuery.toLowerCase();
            query.addCriteria(Criteria.where(SchoolData.Constants.ADDRESS).regex(Pattern.quote(addressQuery), "i"));
        }
        if (size != null) {
            query.limit(size);
        }else{
            query.limit(100);
        }
        if (start != null) {
            query.skip(start);
        } else {
            query.skip(0);
        }
        logger.info("query " + query);
        schools = runQuery(query, SchoolData.class);
        logger.info("getSchools" + schools);
        return schools;
    }
    
}
