package com.vedantu.platform.dao;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.TosEntry;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class TosAllocatorDAO extends AbstractMongoDAO {

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public TosAllocatorDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    

	public void create(TosEntry p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}
	
	public TosEntry getById(String id) {
		TosEntry tosEntry = null;
		try {
			Assert.hasText(id);
			tosEntry = (TosEntry) getEntityById(id, TosEntry.class);
		} catch (Exception ex) {
			throw new RuntimeException("CalendarEntryUpdateError : Error fetch the calendarEntry with id " + id, ex);
		}
		return tosEntry;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			Assert.hasText(id);
			result = deleteEntityById(id, TosEntry.class);
		} catch (Exception ex) {
		}

		return result;
	}
}
