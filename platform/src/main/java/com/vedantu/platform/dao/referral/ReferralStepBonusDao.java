package com.vedantu.platform.dao.referral;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.platform.pojo.referral.model.ReferralStepBonus;
import com.vedantu.util.LogFactory;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class ReferralStepBonusDao extends AbstractMongoDAO {

	@Autowired
	LogFactory logFactory;

	Logger logger = logFactory.getLogger(ReferralStepBonusDao.class);

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public ReferralStepBonusDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

	public void create(ReferralStepBonus p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}

	public void upsert(ReferralStepBonus p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}

	public ReferralStepBonus getById(String id) {
		ReferralStepBonus GetRequestsResponse = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				GetRequestsResponse = (ReferralStepBonus) getEntityById(id, ReferralStepBonus.class);
			}
		} catch (Exception ex) {
			GetRequestsResponse = null;
		}
		return GetRequestsResponse;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, ReferralStepBonus.class);
		} catch (Exception ex) {
		}

		return result;
	}

	// public List<ReferralStepBonus> get(
	// GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq) {
	// try {
	// Query query = new Query();
	// if (getRequestCallBacksDetailsReq.getId() != null) {
	// query.addCriteria(Criteria.where(
	// "id").is(
	// getRequestCallBacksDetailsReq.getId()));
	// }
	// if (getRequestCallBacksDetailsReq.getStudentId() != null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.STUDENT_ID).is(
	// getRequestCallBacksDetailsReq.getStudentId()));
	// }
	// if (getRequestCallBacksDetailsReq.getTeacherId() != null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.TEACHER_ID).is(
	// getRequestCallBacksDetailsReq.getTeacherId()));
	// }
	// if (getRequestCallBacksDetailsReq.getAssignedTo() != null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.ASSIGNED_TO).is(
	// getRequestCallBacksDetailsReq.getAssignedTo()));
	// }
	// if (getRequestCallBacksDetailsReq.getBoardId() != null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.BOARD_ID).is(
	// getRequestCallBacksDetailsReq.getBoardId()));
	// }
	// if (getRequestCallBacksDetailsReq.getGrade() != null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.GRADE).is(
	// getRequestCallBacksDetailsReq.getGrade()));
	// }
	// if (getRequestCallBacksDetailsReq.getTarget() != null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.TARGET).is(
	// getRequestCallBacksDetailsReq.getStudentId()));
	// }
	// if (getRequestCallBacksDetailsReq.getTeacherLeadStatuses() != null) {
	// query.addCriteria(Criteria.where(RequestCallBackDetails.Constants.TEACHER_LEAD_STATUS).in(getRequestCallBacksDetailsReq.getTeacherLeadStatuses()));
	// }
	//
	// if (getRequestCallBacksDetailsReq.getParent()!=null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.PARENT).is(
	// getRequestCallBacksDetailsReq.getParent()));
	// }
	// if (getRequestCallBacksDetailsReq.getTeacherActionTaken()!=null) {
	// query.addCriteria(Criteria.where(
	// RequestCallBackDetails.Constants.TEACHER_ACTION_TAKEN)
	// .is(getRequestCallBacksDetailsReq
	// .getTeacherActionTaken()));
	// }
	//
	// if (getRequestCallBacksDetailsReq.getFromTime()!=null) {
	// Long toTime = getRequestCallBacksDetailsReq.getToTime();
	// if(toTime==null){
	// toTime = System.currentTimeMillis();
	// }
	// query.addCriteria(Criteria.where(
	// AbstractEntity.Constants.CREATION_TIME)
	// .lte(toTime).gte(getRequestCallBacksDetailsReq.getFromTime()));
	// }
	//
	// if (getRequestCallBacksDetailsReq.getLimit() != null) {
	// query.limit(getRequestCallBacksDetailsReq.getLimit());
	// }
	//
	// if (getRequestCallBacksDetailsReq.getStart() != null) {
	// query.skip(getRequestCallBacksDetailsReq.getStart());
	// } else {
	// query.skip(0);
	// }
	// query.with(Sort.by(Sort.Direction.DESC,
	// AbstractEntity.Constants.CREATION_TIME));
	//
	// List<RequestCallBackDetails> result = runQuery(query,
	// RequestCallBackDetails.class);
	//
	// return result;
	// } catch (Throwable e) {
	// throw e;
	// }
	//
	// }
	public Boolean updateAll(List<ReferralStepBonus> referralStepBonusList, String callingUserId) {
		try {
			insertAllEntities(referralStepBonusList,
					ReferralStepBonus.class.getSimpleName(), callingUserId);
			return true;
		} catch (Exception e) {
			throw e;
		}
	}

	public Boolean updateAll(List<ReferralStepBonus> referralStepBonusList) {
		return updateAll(referralStepBonusList, null);
	}

	public Long getLatestPolicyId() {
		Query query = new Query();

		query.with(Sort.by(Sort.Direction.DESC,
				ReferralStepBonus.Constants.REFERRAL_POLICY_ID));

		query.limit(1);

		List<ReferralStepBonus> result = runQuery(query,
				ReferralStepBonus.class);

		if (result != null && !result.isEmpty()) {
			return result.get(0).getReferralPolicyId();
		} else {
			return 0l;
		}
	}

//	public ReferralStepBonus getPolicyStep(Long referralPolicyId,
//			ReferralStep referralStep){
//		return getPolicyStep(referralPolicyId,referralStep,null);
//	}
	
	public ReferralStepBonus getPolicyStep(Long referralPolicyId,
			ReferralStep referralStep, String entityId) {
		
		Query query = new Query();

		query.addCriteria(Criteria.where(
				ReferralStepBonus.Constants.REFERRAL_POLICY_ID).is(
				referralPolicyId));

		query.addCriteria(Criteria.where(
				ReferralStepBonus.Constants.REFERRAL_STEP).is(referralStep));

		if(!StringUtils.isEmpty(entityId)){
			query.addCriteria(Criteria.where(
					ReferralStepBonus.Constants.ENTITY_ID).is(entityId));
		}
		
		List<ReferralStepBonus> result = runQuery(query,
				ReferralStepBonus.class);

		if (result != null && !result.isEmpty()) {
			return result.get(0);
		} else {
			return null;
		}
	}

	public List<ReferralStepBonus> getAllByPolicyId(Long referralPolicyId) {
		
		Query query = new Query();

		query.addCriteria(Criteria.where(
				ReferralStepBonus.Constants.REFERRAL_POLICY_ID).is(
				referralPolicyId));

		List<ReferralStepBonus> result = runQuery(query,
				ReferralStepBonus.class);

			return result;
	}

	public List<ReferralStepBonus> getStepByPolicyId(Long referralPolicyId,
			ReferralStep referralStep, String entityId) {
		
		Query query = new Query();

		query.addCriteria(Criteria.where(
				ReferralStepBonus.Constants.REFERRAL_POLICY_ID).is(
				referralPolicyId));
		
		query.addCriteria(Criteria.where(
				ReferralStepBonus.Constants.REFERRAL_STEP).is(
				referralStep));

		if(!StringUtils.isEmpty(entityId)){
			query.addCriteria(Criteria.where(
					ReferralStepBonus.Constants.ENTITY_ID).is(entityId));
		}
		
		List<ReferralStepBonus> result = runQuery(query,
				ReferralStepBonus.class);

			return result;
	}
}
