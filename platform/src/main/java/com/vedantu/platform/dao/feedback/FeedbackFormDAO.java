/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.feedback;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.enums.feedback.FeedbackCommunication;
import com.vedantu.platform.mongodbentities.feedback.FeedbackForm;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author parashar
 */
@Service
public class FeedbackFormDAO extends AbstractMongoDAO{

    @Autowired
    private MongoClientFactory mongoClientFactory;    
    
    @Autowired
    private LogFactory logfactory;
    
    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(FeedbackFormDAO.class);    
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public FeedbackFormDAO(){
        super();
    }
    
    public void create(FeedbackForm p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        boolean newCP = StringUtils.isEmpty(p.getId());
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
    }
    
    public void save(FeedbackForm p) {
        if (p != null) {
            boolean newCP = StringUtils.isEmpty(p.getId());
            saveEntity(p);
        }
    }

    public FeedbackForm getFormForUserAndId(String formId, Long userId, EntityStatus entityStatus){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants._ID).is(formId));        
        query.addCriteria(Criteria.where(FeedbackForm.Constants.USER_IDS).is(userId));
        // query.addCriteria(Criteria.where(FeedbackForm.Constants.RESPONDED_BY).ne(userId));
        
        if(entityStatus != null){
            query.addCriteria(Criteria.where(FeedbackForm.Constants.STATUS).is(entityStatus));
        }
        
        return findOne(query, FeedbackForm.class);
    }
    
    public List<FeedbackForm> getFeedbackFormNotRespondedByUser(Long userId, EntityStatus entityStatus, List<FeedbackCommunication> feedbackCommunications){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.USER_IDS).is(userId));
        query.addCriteria(Criteria.where(FeedbackForm.Constants.RESPONDED_BY).ne(userId));
        if(entityStatus != null){
            query.addCriteria(Criteria.where(FeedbackForm.Constants.STATUS).is(entityStatus));
        }
        
        if(ArrayUtils.isNotEmpty(feedbackCommunications)){
            query.addCriteria(Criteria.where(FeedbackForm.Constants.FEEDBACK_COMMUNICATION).in(feedbackCommunications));
        }
        
        return runQuery(query, FeedbackForm.class);
    }
    
    public List<FeedbackForm> getFeedbackFormNotRespondedByUser(Long userId, EntityStatus entityStatus, List<FeedbackCommunication> feedbackCommunications, List<String> contextIds){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.USER_IDS).is(userId));
        query.addCriteria(Criteria.where(FeedbackForm.Constants.RESPONDED_BY).ne(userId));
        query.addCriteria(Criteria.where(FeedbackForm.Constants.CONTEXT_ID).in(contextIds));
        if(entityStatus != null){
            query.addCriteria(Criteria.where(FeedbackForm.Constants.STATUS).is(entityStatus));
        }
        
        if(ArrayUtils.isNotEmpty(feedbackCommunications)){
            query.addCriteria(Criteria.where(FeedbackForm.Constants.FEEDBACK_COMMUNICATION).in(feedbackCommunications));
        }
        
        return runQuery(query, FeedbackForm.class);
    }    
    
    public List<FeedbackForm> getFeedbackFormsForParentIdAndContextId(String parentFormId, String contextId){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.CONTEXT_ID).is(contextId));
        query.addCriteria(Criteria.where(FeedbackForm.Constants.PARENT_FORM_ID).is(parentFormId));
        return runQuery(query, FeedbackForm.class);
    }
    
    public Long getCountOfFormsSharedInContextForParentForm(String parentFormId, String contextId){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.PARENT_FORM_ID).is(parentFormId));
        query.addCriteria(Criteria.where(FeedbackForm.Constants.CONTEXT_ID).is(contextId));
        
        return queryCount(query, FeedbackForm.class);
        
    }
    
    public List<FeedbackForm> getFeedbackFormsForContextId(String contextId, int start, int size){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.CONTEXT_ID).is(contextId));
        query.with(Sort.by(Sort.Direction.DESC,FeedbackForm.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, FeedbackForm.class);
    }    
    
    public List<FeedbackForm> getFormForUser(Long userId, int start, int size){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.USER_IDS).is(userId));
        query.with(Sort.by(Sort.Direction.DESC,FeedbackForm.Constants.CREATION_TIME));        
        setFetchParameters(query, start, size);
        return runQuery(query, FeedbackForm.class);
    }    
    
    public List<FeedbackForm> getActiveForms(){
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackForm.Constants.STATUS).is(EntityStatus.ACTIVE));
        
        return runQuery(query, FeedbackForm.class);
    }
    
}
