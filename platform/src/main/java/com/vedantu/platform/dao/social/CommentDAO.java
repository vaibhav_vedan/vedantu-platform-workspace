/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.social;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.mongodbentities.social.Comment;
import com.vedantu.platform.pojo.social.CommentsRepliesCount;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SortOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

/**
 *
 * @author ajith
 */
@Service
public class CommentDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CommentDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public CommentDAO() {
        super();
    }

    public void save(Comment entity) {
        saveEntity(entity);
    }

    public List<Comment> getEntities(SocialContextType socialEntityType,
            String entityId, String parentId,
            Integer start, Integer size,String orderBy,String sortOrder) {
        Query query = prepareQuery(socialEntityType, entityId, parentId);
        setFetchParameters(query, start, size);

        if (StringUtils.isNotEmpty(orderBy) && StringUtils.isNotEmpty(sortOrder)) {
            SortOrder sortOrder1 = SortOrder.valueQuitentOf(sortOrder);
            if (Sort.Direction.ASC.name().equals(sortOrder1.name())) {
                query.with(Sort.by(Sort.Direction.ASC, orderBy));
            } else {
                query.with(Sort.by(Sort.Direction.DESC, orderBy));
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        }
        return runQuery(query, Comment.class);
    }

    public long getEntitiesCount(
            SocialContextType socialEntityType, String entityId, String parentId) {
        Query query = prepareQuery(socialEntityType, entityId, parentId);
        return queryCount(query, Comment.class);
    }

    public Query prepareQuery(
            SocialContextType socialEntityType, String entityId, String parentId) {
        Query query = new Query();
        if (StringUtils.isNotEmpty(parentId)) {
            query.addCriteria(Criteria.where(Comment.Constants.PARENT_ID).is(parentId));
        } else {
            query.addCriteria(Criteria.where(Comment.Constants.PARENT_ID).exists(false));
            if (socialEntityType != null) {
                query.addCriteria(Criteria.where(Comment.Constants.SOCIAL_CONTEXT_TYPE).is(socialEntityType));
            }
            if (StringUtils.isNotEmpty(entityId)) {
                query.addCriteria(Criteria.where(Comment.Constants.CONTEXT_ID).is(entityId));
            }
        }
        return query;
    }

    public Map<String, CommentsRepliesCount> getRepliesCount(Set<String> commentIds) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(match(Criteria.where(Comment.Constants.PARENT_ID).in(commentIds)),
                group(Comment.Constants.PARENT_ID).count().as(Comment.Constants.REPLIES),
                project(Comment.Constants.REPLIES).and(Comment.Constants.PARENT_ID).previousOperation()).withOptions(aggregationOptions);
        AggregationResults<CommentsRepliesCount> groupResults
                = getMongoOperations().aggregate(agg, Comment.class.getSimpleName(), CommentsRepliesCount.class);

        List<CommentsRepliesCount> result = groupResults.getMappedResults();
        logger.info("agg " + agg);
        Map<String, CommentsRepliesCount> map = new HashMap<>();
        if (ArrayUtils.isNotEmpty(result)) {
            for (CommentsRepliesCount obj : result) {
                map.put(obj.getParentId(), obj);
            }
        }
        return map;
    }

}
