package com.vedantu.platform.dao.engagement.serializers;

import java.util.List;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.mongodbentities.engagement.TeacherPercentile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class TeacherPercentileDAO extends AbstractMongoDAO{

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public TeacherPercentileDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

	public void create(TeacherPercentile p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public TeacherPercentile getById(String id) {
		TeacherPercentile ChannelUserInfo = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				ChannelUserInfo = (TeacherPercentile) getEntityById(id, TeacherPercentile.class);
			}
		} catch (Exception ex) {
			ChannelUserInfo = null;
		}
		return ChannelUserInfo;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, TeacherPercentile.class);
		} catch (Exception ex) {
		}

		return result;
	}

	public TeacherPercentile getTeacherPercentile(Long startTimeoFMonth, Long teacherId, int fortnight)
	{
		Query query = new Query();
		query.addCriteria(Criteria.where("startTimeoFMonth").is(startTimeoFMonth).and("teacherId").is(teacherId).and("fortnight").is(fortnight));
		return getMongoOperations().findOne(query, TeacherPercentile.class);
	}
}
