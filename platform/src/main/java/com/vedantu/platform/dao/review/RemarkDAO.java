package com.vedantu.platform.dao.review;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.review.Remark;
import com.vedantu.review.pojo.RemarkContext;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
@Service
public class RemarkDAO extends AbstractMongoDAO{
	
	@Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(RemarkDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public RemarkDAO() {
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void create(Remark p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    
	public void updateAll(List<Remark> p) {
		if (p != null && !p.isEmpty()) {
			insertAllEntities(p, Remark.class.getSimpleName());
		}
	} 
	
	public Remark getById(String id) {
		Remark session = null;
		if (!StringUtils.isEmpty(id)) {
			session = getEntityById(id, Remark.class);
		}
		return session;
	}
        
        public Remark getByContextId(String contextId, RemarkContext context) {
                if(StringUtils.isEmpty(contextId)){
                    return null;
                }
		Query query = new Query();
                query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_ID).is(contextId));
                if(context != null){
                    query.addCriteria(Criteria.where(Remark.Constants.CONTEXT_TYPE).is(context));
                }
                Remark remark = findOne(query, Remark.class);
		return remark;
	}
}
