package com.vedantu.platform.dao.wave;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SessionIdVsServer")
public class SessionIdVsServer extends AbstractMongoStringIdEntity {
    @Indexed(background=true)
    private String sessionId;
    private String serverIp;
    private String serverPort;

    public SessionIdVsServer() {
        super();
    }

    public SessionIdVsServer(String sessionId, String serverIp, String serverPort) {
        this.sessionId = sessionId;
        this.serverIp = serverIp;
        this.serverPort = serverPort;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    @Override
    public String toString() {
        return "SessionIdVsServer{" + "sessionId=" + sessionId + ", serverIp=" + serverIp + ", serverPort=" + serverPort + '}';
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String SESSIONID = "sessionId";
    }

}
