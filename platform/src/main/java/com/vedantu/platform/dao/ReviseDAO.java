package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.ReviseIndiaStats;
import com.vedantu.platform.mongodbentities.ReviseJeeStats;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;


@Service
public class ReviseDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReviseDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
        logger.info("ENTRY: " + p);
        if (p != null) {
            saveEntity(p, callingUserId);
        }
        logger.info("EXIT");
    }

    public ReviseIndiaStats getStats(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseIndiaStats.Constants.USER_ID).is(userId));
        List<ReviseIndiaStats> reviseIndiaStats = runQuery(query, ReviseIndiaStats.class);
        if(reviseIndiaStats.size() > 0){
            return reviseIndiaStats.get(0);
        }
        return null;
    }

    public ReviseJeeStats getJeetats(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseIndiaStats.Constants.USER_ID).is(userId));
        List<ReviseJeeStats> reviseJeeStats = runQuery(query, ReviseJeeStats.class);
        if(reviseJeeStats.size() > 0){
            return reviseJeeStats.get(0);
        }
        return null;
    }

    public void updateAfterClass(Update update, String callingUserId) {
        if (update == null || StringUtils.isEmpty(callingUserId)) {
            return;
        }
        if (!StringUtils.isEmpty(callingUserId)) {
            update.set(ReviseIndiaStats.Constants.LAST_UPDATED_BY, callingUserId);
        }
        update.set(ReviseIndiaStats.Constants.LAST_UPDATED, System.currentTimeMillis());
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseIndiaStats.Constants.USER_ID).is(Long.parseLong(callingUserId)));
        upsertEntity(query, update, ReviseIndiaStats.class);
    }


    public void updateAfterTest(Update update, String callingUserId) {
        if (update == null || StringUtils.isEmpty(callingUserId)) {
            return;
        }
        if (!StringUtils.isEmpty(callingUserId)) {
            update.set(ReviseIndiaStats.Constants.LAST_UPDATED_BY, callingUserId);
        }
        update.set(ReviseIndiaStats.Constants.LAST_UPDATED, System.currentTimeMillis());
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseIndiaStats.Constants.USER_ID).is(Long.parseLong(callingUserId)));
        upsertEntity(query, update, ReviseIndiaStats.class);
    }

    public void updateReviseJeeAfterTest(Update update, String callingUserId) {
        if (update == null || StringUtils.isEmpty(callingUserId)) {
            return;
        }
        if (!StringUtils.isEmpty(callingUserId)) {
            update.set(ReviseJeeStats.Constants.LAST_UPDATED_BY, callingUserId);
        }
        update.set(ReviseJeeStats.Constants.LAST_UPDATED, System.currentTimeMillis());
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseJeeStats.Constants.USER_ID).is(Long.parseLong(callingUserId)));
        upsertEntity(query, update, ReviseJeeStats.class);
    }


}
