package com.vedantu.platform.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.entity.Career;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author MNPK
 */
@Service
public class CareerDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(CareerDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public CareerDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Career p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("Create : Error Creating Career JD : " + ex.getMessage());
        }
    }

    public Career getCareerByUsingTechnologyAndTitle(String technology, String title) throws BadRequestException {
        if (StringUtils.isEmpty(technology) || StringUtils.isEmpty(title)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "title or technology not found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Career.Constants.TECHNOLOGY).is(technology));
        query.addCriteria(Criteria.where(Career.Constants.TITLE).is(title));
        return findOne(query, Career.class);
    }

    public List<Career> getCareersByUsingFilters(Integer start, Integer limit, EntityState state) {
        Query query = new Query();
        if (state == null) {
            query.addCriteria(Criteria.where(Career.Constants.ENTITY_STATE).in(EntityState.ACTIVE, EntityState.INACTIVE));
        } else {
            query.addCriteria(Criteria.where(Career.Constants.ENTITY_STATE).is(state));
        }
        setFetchParameters(query, start, limit);
        query.with(Sort.by(Sort.Direction.DESC, Career.Constants.LAST_UPDATED));
        logger.info("query : " + query);
        return runQuery(query, Career.class);
    }

}
