package com.vedantu.platform.dao.wave;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.Collection;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class SessionBillingDataDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SessionBillingDataDAO.class);

    @Autowired
    private MongoClientFactoryWave mongoClientFactoryWave;

    public SessionBillingDataDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactoryWave.getMongoOperations();
    }

    public void insertAllEntities(List<SessionBillingData> billingdatas) {
        insertAllEntities(billingdatas, "sessionbillingdatas");
    }

    public List<SessionBillingData> getBySessionId(Long sessionId) {
        logger.info("ENTRY: " + sessionId);
        Query query = new Query();
        query.addCriteria(Criteria.where("sessionId").is(sessionId));
        List<SessionBillingData> result = runQuery(query, SessionBillingData.class, "sessionbillingdatas");
        logger.info("EXIT: " + result);
        return result;
    }

}
