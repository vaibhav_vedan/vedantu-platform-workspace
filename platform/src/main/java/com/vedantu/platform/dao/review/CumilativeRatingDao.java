package com.vedantu.platform.dao.review;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.review.CumilativeRating;
import com.vedantu.platform.utils.PojoUtils;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.EntityType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.HashMap;
import java.util.Map;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class CumilativeRatingDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(CumilativeRatingDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;
    
    @Autowired
    PojoUtils pojoUtils;

    public CumilativeRatingDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(CumilativeRating p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(CumilativeRating p) {
        upsert(p, null);
    }

    public void upsert(CumilativeRating p, String callingUserId) {
        try {
            if (p != null) {
                saveEntity(p, callingUserId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public CumilativeRating getById(String id) {
        CumilativeRating GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (CumilativeRating) getEntityById(id,
                        CumilativeRating.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, CumilativeRating.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<CumilativeRating> get(String id, String entityId, EntityType entityType, Long totalRatingCount, Long totalRating,
            Float avgRating, Boolean baseRatingAssigned, Long fromTime, Long toTime, Integer start, Integer limit) {
        try {
            Query query = new Query();

            if (id != null) {
                query.addCriteria(Criteria.where("id").is(id));
            } else {

                if (entityId != null) {
                    query.addCriteria(Criteria
                            .where(CumilativeRating.Constants.ENTITY_ID).is(entityId));
                }

                if (entityType != null) {
                    query.addCriteria(Criteria.where(
                            CumilativeRating.Constants.ENTITY_TYPE).is(entityType));
                }

                if (totalRatingCount != null) {
                    query.addCriteria(Criteria.where(CumilativeRating.Constants.TOTAL_RATING_COUNT)
                            .is(totalRatingCount));
                }

                if (totalRating != null) {
                    query.addCriteria(Criteria.where(CumilativeRating.Constants.TOTAL_RATING)
                            .is(totalRating));
                }

                if (avgRating != null) {
                    query.addCriteria(Criteria.where(
                            CumilativeRating.Constants.AVG_RATING).is(avgRating));
                }
                if (baseRatingAssigned != null) {
                    query.addCriteria(Criteria.where(CumilativeRating.Constants.BASE_RATING_ASSIGNED)
                            .is(baseRatingAssigned));
                }

                if (fromTime != null) {
                    if (toTime == null) {
                        toTime = System.currentTimeMillis();
                    }
                    query.addCriteria(Criteria
                            .where(AbstractMongoStringIdEntity.Constants.CREATION_TIME)
                            .lte(toTime).gte(fromTime));
                }

                if (limit != null) {
                    query.limit(limit);
                }

                if (start != null) {
                    query.skip(start);
                } else {
                    query.skip(0);
                }
            }
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));

            List<CumilativeRating> result = runQuery(query, CumilativeRating.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }

    public CumilativeRating getCumulativeRating(String entityId, EntityType entityType) {
        logger.info("entityId: " + entityId + ", entityType: " + entityType);
        CumilativeRating rating = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(CumilativeRating.Constants.ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(CumilativeRating.Constants.ENTITY_TYPE).is(entityType));

        List<CumilativeRating> results = runQuery(query, CumilativeRating.class);
        if (null != results && !results.isEmpty()) {
            rating = results.get(0);
        }
        logger.info("Exit " + rating);
        return rating;

    }

    public Map<String, com.vedantu.session.pojo.CumilativeRating> getCumulativeRatingsMap(List<String> entityIds, EntityType entityType) {
        logger.info("entityIds: " + entityIds + ", entityType: " + entityType);
        List<CumilativeRating> results = getCumulativeRatings(entityIds, entityType);
        Map<String, com.vedantu.session.pojo.CumilativeRating> resultsMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(results)) {
            for (CumilativeRating cumilativeRating : results) {
                if (cumilativeRating != null) {
                    resultsMap.put(cumilativeRating.getEntityId(), pojoUtils.convertToRatingPojo(cumilativeRating));
                }
            }
        }
        return resultsMap;
    }

    public List<CumilativeRating> getCumulativeRatings(List<String> entityIds, EntityType entityType) {
        logger.info("entityIds: " + entityIds + ", entityType: " + entityType);
        Query query = new Query();
        query.addCriteria(Criteria.where(CumilativeRating.Constants.ENTITY_ID).in(entityIds));
        query.addCriteria(Criteria.where(CumilativeRating.Constants.ENTITY_TYPE).is(entityType));

        List<CumilativeRating> results = runQuery(query, CumilativeRating.class);
        logger.info("Exit " + results);
        return results;
    }

}
