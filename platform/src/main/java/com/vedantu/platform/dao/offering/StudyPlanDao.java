package com.vedantu.platform.dao.offering;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.mongodbentities.offering.StudyPlan;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class StudyPlanDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(StudyPlanDao.class);

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public StudyPlanDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(StudyPlan p) {
        try {
            if (p != null) {
                p.setLastUpdated(System.currentTimeMillis());
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(StudyPlan p) {
        try {
            if (p != null) {
                p.setLastUpdated(System.currentTimeMillis());
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public StudyPlan getById(String id) {
        StudyPlan GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (StudyPlan) getEntityById(id,
                        StudyPlan.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, StudyPlan.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<StudyPlan> get(List<String> ids, String userId,
            String offeringId, String targetUserId, String note,
            Long targetDate, String name, Long noOfSessions, String status,
            Long fromTime, Long toTime, Integer start, Integer limit) {
        try {
            Query query = new Query();

            if (ids != null && !ids.isEmpty()) {
                query.addCriteria(Criteria.where("id").in(ids));
            } else {

                if (userId != null && !userId.isEmpty()) {
                    query.addCriteria(Criteria.where(
                            StudyPlan.Constants.USER_ID).is(userId));
                }

                if (offeringId != null && !offeringId.isEmpty()) {
                    query.addCriteria(Criteria.where(
                            StudyPlan.Constants.OFFERING_ID).is(offeringId));
                }

                if (targetUserId != null && !targetUserId.isEmpty()) {
                    query.addCriteria(Criteria.where(
                            StudyPlan.Constants.TARGET_USER_ID)
                            .is(targetUserId));
                }

                if (targetDate != null) {
                    query.addCriteria(Criteria.where(
                            StudyPlan.Constants.TARGET_DATE).is(targetDate));
                }

                if (name != null && !name.isEmpty()) {
                    query.addCriteria(Criteria.where(StudyPlan.Constants.NAME)
                            .is(name));
                }

                if (status != null && !status.isEmpty()) {
                    query.addCriteria(Criteria
                            .where(StudyPlan.Constants.STATUS).is(status));
                }

                if (fromTime != null) {
                    if (toTime == null) {
                        toTime = System.currentTimeMillis();
                    }
                    query.addCriteria(Criteria
                            .where(AbstractMongoStringIdEntity.Constants.CREATION_TIME)
                            .lte(toTime).gte(fromTime));
                }

                if (limit != null) {
                    query.limit(limit);
                }

                if (start != null) {
                    query.skip(start);
                } else {
                    query.skip(0);
                }

                query.with(Sort.by(Sort.Direction.DESC,
                        AbstractMongoStringIdEntity.Constants.CREATION_TIME));
            }
            List<StudyPlan> result = runQuery(query,
                    StudyPlan.class);
            return result;
        } catch (Throwable e) {
            throw e;
        }

    }

}
