package com.vedantu.platform.dao.review;

import java.util.List;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.mongodbentities.review.Review;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ContextType;
import com.vedantu.util.EntityType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.Arrays;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class ReviewDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReviewDao.class);

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    


    public ReviewDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }        


    public void create(Review p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(Review p) {
        upsert(p, null);
    }

    public void upsert(Review p, String callingUserId) {
        try {
            if (p != null) {
                saveEntity(p, callingUserId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public Review getById(String id) {
        Review getRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                getRequestsResponse = (Review) getEntityById(id,
                        Review.class);
            }
        } catch (Exception ex) {
            getRequestsResponse = null;
        }
        return getRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, Review.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<Review> get(List<String> ids, String userId, String entityId,
            EntityType entityType, Integer rating, String review,
            Boolean isReviewed, VisibilityState state, ContextType contextType,
            String contextId, Integer priority, Long fromTime, Long toTime,
            Integer start, Integer limit, Long excludeUserId,
            Boolean skipReviewCheck, Boolean sortByPriority) {
        try {
            Query query = new Query();

            if (ids != null && !ids.isEmpty()) {
                query.addCriteria(Criteria.where("id").in(ids));
            } else {

                if (userId != null && !userId.isEmpty()) {
                    query.addCriteria(Criteria.where(Review.Constants.USER_ID)
                            .is(userId));
                }

                if (entityId != null) {
                    query.addCriteria(Criteria
                            .where(Review.Constants.ENTITY_ID).is(entityId));
                }

                if (entityType != null) {
                    query.addCriteria(Criteria.where(
                            Review.Constants.ENTITY_TYPE).is(entityType));
                }

                if (rating != null) {
                    query.addCriteria(Criteria.where(Review.Constants.RATING)
                            .is(rating));
                }

                if (review != null) {
                    query.addCriteria(Criteria.where(Review.Constants.REVIEW)
                            .is(review));
                }

                if (isReviewed != null) {
                    query.addCriteria(Criteria.where(
                            Review.Constants.IS_REVIEWED).is(isReviewed));
                }
                if (state != null) {
                    query.addCriteria(Criteria.where(Review.Constants.state)
                            .is(state));
                }
                if (contextType != null) {
                    query.addCriteria(Criteria.where(
                            Review.Constants.CONTEXT_TYPE).is(contextType));
                }
                if (contextId != null) {
                    query.addCriteria(Criteria.where(
                            Review.Constants.CONTEXT_ID).is(contextId));
                }

                if (priority != null) {
                    query.addCriteria(Criteria.where(Review.Constants.PRIORITY)
                            .is(priority));
                }

                if (fromTime != null) {
                    if (toTime == null) {
                        toTime = System.currentTimeMillis();
                    }
                    query.addCriteria(Criteria
                            .where(AbstractMongoStringIdEntity.Constants.CREATION_TIME)
                            .lte(toTime).gte(fromTime));
                }

                if (limit != null) {
                    query.limit(limit);
                }

                if (start != null) {
                    query.skip(start);
                } else {
                    query.skip(0);
                }

                if (excludeUserId != null) {
                    query.addCriteria(Criteria.where(Review.Constants.USER_ID)
                            .ne(excludeUserId.toString()));
                }
                if (skipReviewCheck == null || !skipReviewCheck) {
                    query.addCriteria(Criteria.where(
                            Review.Constants.IS_REVIEWED).is(true));
                }

            }

            if (sortByPriority != null && sortByPriority) {
                query.with(Sort.by(Sort.Direction.DESC,
                        Review.Constants.PRIORITY, AbstractMongoStringIdEntity.Constants.CREATION_TIME));
            } else {
                query.with(Sort.by(Sort.Direction.DESC,
                        AbstractMongoStringIdEntity.Constants.CREATION_TIME));
            }

            List<Review> result = runQuery(query, Review.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }
    
    public Boolean updateAll(List<Review> reviews) {
        try {
            insertAllEntities(reviews,
                    Review.class.getSimpleName());
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
