/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.wave;

/**
 *
 * @author ajith
 */
public class WiziqBillingDataReq {

    private Long sessionId;
    private Long teacherId;
    private Long studentId;
    private Long teacher_entry_time;
    private Long teacher_exit_time;
    private Long student_entry_time;
    private Long student_exit_time;

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacher_entry_time() {
        return teacher_entry_time;
    }

    public void setTeacher_entry_time(Long teacher_entry_time) {
        this.teacher_entry_time = teacher_entry_time;
    }

    public Long getTeacher_exit_time() {
        return teacher_exit_time;
    }

    public void setTeacher_exit_time(Long teacher_exit_time) {
        this.teacher_exit_time = teacher_exit_time;
    }

    public Long getStudent_entry_time() {
        return student_entry_time;
    }

    public void setStudent_entry_time(Long student_entry_time) {
        this.student_entry_time = student_entry_time;
    }

    public Long getStudent_exit_time() {
        return student_exit_time;
    }

    public void setStudent_exit_time(Long student_exit_time) {
        this.student_exit_time = student_exit_time;
    }

    @Override
    public String toString() {
        return "WiziqBillingDataReq{" + "sessionId=" + sessionId + ", teacherId=" + teacherId + ", studentId=" + studentId + ", teacher_entry_time=" + teacher_entry_time + ", teacher_exit_time=" + teacher_exit_time + ", student_entry_time=" + student_entry_time + ", student_exit_time=" + student_exit_time + '}';
    }

}
