package com.vedantu.platform.dao;

import com.vedantu.platform.mongodbentities.mobile.MobileConfig;
import com.vedantu.platform.mongodbentities.mobile.MobileGlobalConfiguration;
import com.vedantu.platform.enums.mobile.MobilePlatform;
import com.vedantu.platform.enums.mobile.MobileRole;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class MobileRequestDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactoryMobile mongoClientFactoryMobile;

    public MobileRequestDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactoryMobile.getMongoOperations();
    }

    public void create(MobileConfig p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public MobileConfig getById(String id) {
        MobileConfig mobileConfig = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                mobileConfig = (MobileConfig) getEntityById(id, MobileConfig.class);
            }
        } catch (Exception ex) {
            mobileConfig = null;
        }
        return mobileConfig;
    }

    public List<MobileConfig> getMobileConfigs(Long userId, String deviceId, String key) {

        List<MobileConfig> mobileConfigs = null;
        ///////// left

        Query query = new Query();
        query.addCriteria(Criteria.where(MobileConfig.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(MobileConfig.Constants.DEVICE_ID).is(deviceId));

        List<MobileConfig> results = runQuery(query, MobileConfig.class);
        if (results.size() > 0) {
            for (MobileConfig result : results) {
                mobileConfigs.add(result);
            }
        }
        return mobileConfigs;
    }

    public void update(MobileConfig p, Update update) {
        try {
            if (p != null) {
                Query query = new Query();
                if (p.getUserId() != null) {
                    query.addCriteria(Criteria.where(MobileConfig.Constants.USER_ID).is(p.getUserId()));
                }
                if (p.getDeviceId() != null) {
                    query.addCriteria(Criteria.where(MobileConfig.Constants.DEVICE_ID).is(p.getDeviceId()));
                }
                if (p.getPlatform() != null) {
                    query.addCriteria(Criteria.where(MobileConfig.Constants.MOBILE_PLATFORM).is(p.getPlatform()));
                }
                if (p.getPlatformVersion() != null) {
                    query.addCriteria(Criteria.where(MobileConfig.Constants.PLATFORM_VERSION).is(p.getPlatformVersion()));
                }
                updateFirst(query, update, MobileConfig.class);
            }
        } catch (Exception ex) {
        }
    }

    public void updateGlobalConfig(MobileGlobalConfiguration p, Update update) {
        try {

            //logger.info("update global : "+update.toString());
            if (p != null) {
                Query query = new Query();
                query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.USER_ID).is(p.getUserId()));
                query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.DEVICE_ID).is(p.getDeviceId()));
                query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.ROLE).is(p.getRole()));
                query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.PLATFORM).is(p.getPlatform()));
                updateFirst(query, update, MobileGlobalConfiguration.class);
            }
        } catch (Exception ex) {
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, MobileConfig.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<MobileConfig> get(String userId, String deviceId, MobilePlatform platform, String version, String versionCode) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MobileConfig.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(MobileConfig.Constants.DEVICE_ID).is(deviceId));
        query.addCriteria(Criteria.where(MobileConfig.Constants.MOBILE_PLATFORM).is(platform));
        query.addCriteria(Criteria.where(MobileConfig.Constants.PLATFORM_VERSION).is(version));
        query.addCriteria(Criteria.where(MobileConfig.Constants.VERSION_CODE).is(versionCode));
        List<MobileConfig> mobileConfig = runQuery(query, MobileConfig.class);
        return mobileConfig;
    }

    public List<MobileGlobalConfiguration> getGlobalMobileParams(String userId, String deviceId, MobilePlatform platform, MobileRole role) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.DEVICE_ID).is(deviceId));
        query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.PLATFORM).is(platform));
        query.addCriteria(Criteria.where(MobileGlobalConfiguration.Constants.ROLE).is(role));
        List<MobileGlobalConfiguration> mobileConfig = runQuery(query, MobileGlobalConfiguration.class);
        return mobileConfig;
    }
}
