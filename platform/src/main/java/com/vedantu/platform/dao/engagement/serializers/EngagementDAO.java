package com.vedantu.platform.dao.engagement.serializers;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.vedantu.platform.response.engagement.EngagementAnalysis;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class EngagementDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public EngagementDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(EngagementAnalysis p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public EngagementAnalysis getById(String id) {
        EngagementAnalysis GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (EngagementAnalysis) getEntityById(id, EngagementAnalysis.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, EngagementAnalysis.class);
        } catch (Exception ex) {
        }

        return result;
    }


    public List<EngagementAnalysis> getExport(Long startTime, Long endTime) {
        List<EngagementAnalysis> result = runExport(EngagementAnalysis.class, startTime, endTime);
        return result;
    }

    public List<EngagementAnalysis> getExportByUserIdSubject(Long startTime, Long endTime, String userId, String subject) {
        List<EngagementAnalysis> result = runExportByUserIdSubject(EngagementAnalysis.class, startTime, endTime, userId, subject);
        return result;
    }

    public List<EngagementAnalysis> getAllByTeacherId(Long startTime, Long endTime) {
        List<EngagementAnalysis> result = getAllByTeacherId(EngagementAnalysis.class, startTime, endTime);
        return result;
    }

    public List<DBObject> getAverageBySubjectWhiteBoard(Long startTime, Long endTime) {
        List<DBObject> result = getAverageBySubjectWhiteBoard(EngagementAnalysis.class, startTime, endTime);
        return result;
    }

    public List<DBObject> getAverageBySubjectAudio(Long startTime, Long endTime) {
        List<DBObject> result = getAverageBySubjectAudio(EngagementAnalysis.class, startTime, endTime);
        return result;
    }

    public List<DBObject> getTeacherWiseAverageScore(Long startTime, Long endTime) {
        List<DBObject> result = getTeacherWiseAverageScore(EngagementAnalysis.class, startTime, endTime);
        return result;
    }

    public List<EngagementAnalysis> getByTime(Long startTime, Long endTime) {
        List<EngagementAnalysis> result = getByTime(EngagementAnalysis.class, startTime, endTime);
        return result;
    }

    public <T extends EngagementAnalysis> List<T> getByTime(Class<T> calzz, Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where("serverTime").gte(startTime).lte(endTime));
        query.with(Sort.by(Sort.Direction.ASC, "serverTime"));
        return getMongoOperations().find(query, calzz, calzz.getSimpleName());
    }

    public <T extends EngagementAnalysis> List<T> runExport(Class<T> calzz, Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where("serverTime").gte(startTime).lte(endTime).and("analysable").exists(true).ne(false).and("teacherId").exists(true).ne("null").and("audioPresent").exists(true));
        query.with(Sort.by(Sort.Direction.ASC, "serverTime"));
        return getMongoOperations().find(query, calzz, calzz.getSimpleName());
    }

    public <T extends EngagementAnalysis> List<T> runExportByUserIdSubject(Class<T> calzz, Long startTime, Long endTime, String userId, String subject) {
        Query query = new Query();
        query.addCriteria(Criteria.where("analysable").is(true).and("teacherId").exists(true).ne("null").and("teacherName").exists(true).ne(null).and("audioPresent").exists(true));
        query.addCriteria(Criteria.where("serverTime").gte(startTime).lte(endTime).and("teacherId").is(userId).and("subject").is(subject));

        query.with(Sort.by(Sort.Direction.ASC, "serverTime"));
        return getMongoOperations().find(query, calzz, calzz.getSimpleName());
    }

    public <T extends EngagementAnalysis> List<T> getAllByTeacherId(Class<T> calzz, Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where("serverTime").gte(startTime).lte(endTime).and("analysable").exists(true).ne(false).and("teacherId").exists(true).ne("null").and("teacherName").exists(true).ne(null).and("audioPresent").exists(true));
        //			query.with(Sort.by(Sort.Direction.DESC, "subject","teacherId","serverTime"));
        //			query.with(Sort.by(Sort.Direction.DESC, "teacherId","serverTime"));
        return getMongoOperations().find(query, calzz, calzz.getSimpleName());
    }

    public <T> List<DBObject> getAverageBySubjectWhiteBoard(Class<T> calzz, Long startTime, Long endTime) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("analysable").is(true).and("acadRating").is("5")),
                //				Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("analysable").is(true)),
                //				Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("audioPresent").is("Audio Present").and("analysable").exists(true).is(true)),
                //				Aggregation.project(Criteria.where("serverTime").gte(o).lte("e"))
                Aggregation.group("subject")
                .avg("scoreTotal").as("scoreTotal")
                .avg("scoreContentUsage").as("scoreContentUsage")
                .avg("scoreWhiteBoardUsage").as("scoreWhiteBoardUsage")
        ).withOptions(aggregationOptions);
        
        AggregationResults<DBObject> result = getMongoOperations().aggregate(agg, "EngagementAnalysis", DBObject.class);
        return result.getMappedResults();

    }

    public <T> List<DBObject> getAverageBySubjectAudio(Class<T> calzz, Long startTime, Long endTime) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("audioPresent").is("Audio Present").and("analysable").is(true).and("acadRating").is("5")),
                //				Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("audioPresent").is("Audio Present").and("analysable").is(true)),
                //				Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("audioPresent").is("Audio Present").and("analysable").exists(true).is(true)),
                //				Aggregation.project(Criteria.where("serverTime").gte(o).lte("e"))
                Aggregation.group("subject")
                .avg("scoreInteraction").as("scoreInteraction")
        ).withOptions(aggregationOptions);
        AggregationResults<DBObject> result = getMongoOperations().aggregate(agg, "EngagementAnalysis", DBObject.class);
        return result.getMappedResults();

    }

    public <T> List<DBObject> getTeacherWiseAverageScore(Class<T> calzz, Long startTime, Long endTime) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("analysable").is(true).and("subject").in("mathematics", "physics", "chemistry", "biology")),
                //				Aggregation.match(Criteria.where("serverTime").gte(startTime).lte(endTime).and("analysable").exists(true).is(true)),
                Aggregation.group("teacherId")
                .avg("scoreTotal").as("scoreTotal"),
                Aggregation.sort(Direction.DESC, "scoreTotal")
        ).withOptions(aggregationOptions);
        AggregationResults<DBObject> result = getMongoOperations().aggregate(agg, "EngagementAnalysis", DBObject.class);
        return result.getMappedResults();

    }
}
