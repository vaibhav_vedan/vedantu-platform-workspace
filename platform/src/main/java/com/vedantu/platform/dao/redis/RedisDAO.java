/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.redis;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
public class RedisDAO extends AbstractRedisDAO{

    private static final Gson gson = new Gson();
    private Logger logger = LogFactory.getLogger(RedisDAO.class);

    public RedisDAO() {
    }
}
