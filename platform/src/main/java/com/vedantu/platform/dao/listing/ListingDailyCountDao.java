package com.vedantu.platform.dao.listing;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.mongodbentities.listing.ListingDailyCount;
import com.vedantu.util.LogFactory;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class ListingDailyCountDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ListingDailyCountDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public ListingDailyCountDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(ListingDailyCount p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(ListingDailyCount p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public ListingDailyCount getById(String id) {
        ListingDailyCount GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (ListingDailyCount) getEntityById(id, ListingDailyCount.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id,
                    ListingDailyCount.class);
        } catch (Exception ex) {
        }

        return result;
    }
}
