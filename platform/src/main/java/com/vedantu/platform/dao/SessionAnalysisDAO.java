/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao;

import com.vedantu.platform.dao.wave.MongoClientFactoryWave;
import com.vedantu.platform.pojo.sessionmetrics.SessionAnalysisData;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class SessionAnalysisDAO extends AbstractMongoDAO{
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(SessionAnalysisDAO.class);
    
    @Autowired
    private MongoClientFactoryWave mongoClientFactoryWave; 
    
    public final static String COLLECTION_NAME="sessionanalyses";
    
    public SessionAnalysisDAO() {
        super();
    }

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactoryWave.getMongoOperations();
    }
    
    public List<SessionAnalysisData> getBySessionId(String sessionId) {
        logger.info("ENTRY: " + sessionId);
        Query query = new Query();
        query.addCriteria(Criteria.where("sessionId").is(sessionId));
        List<SessionAnalysisData> result = runQuery(query,SessionAnalysisData.class,COLLECTION_NAME);
        logger.info("EXIT: " + result);
        return result;
    }
    
    public List<SessionAnalysisData> getByUserId(String userId,Long fromTime,Long tillTime,String order) {
        logger.info("ENTRY: " + userId);
        Query query = new Query();
        if(fromTime==null){
            fromTime=0l;
        }
        if(tillTime==null){
            tillTime=System.currentTimeMillis();
        }
        
        Criteria criteria=Criteria.where("userId").is(userId).and("startTime").gte(fromTime).lte(tillTime);
        query.addCriteria(criteria);
        if(StringUtils.isNotEmpty(order) && order.equalsIgnoreCase("ASC")){
            query.with(Sort.by( Sort.Direction.ASC,"startTime"));
        }else{
            query.with(Sort.by(Sort.Direction.DESC, "startTime"));
        }
        query.limit(1000);
        List<SessionAnalysisData> result = runQuery(query,SessionAnalysisData.class,COLLECTION_NAME);
        logger.info("EXIT: " + result);
        return result;
    }
    
     public List<SessionAnalysisData> getByUserEmail(String email,Long fromTime,Long tillTime,String order) {
        logger.info("ENTRY: " + email);
        Query query = new Query();
        if(fromTime==null){
            fromTime=0l;
        }
        if(tillTime==null){
            tillTime=System.currentTimeMillis();
        }
        
        Criteria criteria=Criteria.where("email").is(email).and("startTime").gte(fromTime).lte(tillTime);
        query.addCriteria(criteria);
        if(StringUtils.isNotEmpty(order) && order.equalsIgnoreCase("ASC")){
            query.with(Sort.by( Sort.Direction.ASC,"startTime"));
        }else{
            query.with(Sort.by(Sort.Direction.DESC, "startTime"));
        }
        query.limit(1000);
        List<SessionAnalysisData> result = runQuery(query,SessionAnalysisData.class,COLLECTION_NAME);
        logger.info("EXIT: " + result);
        return result;
    }
    
    
}
