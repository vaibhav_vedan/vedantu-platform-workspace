package com.vedantu.platform.dao.wave;

import com.vedantu.platform.pojo.wave.WhiteboardPDF;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class WaveDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(WaveDAO.class);
    
    @Autowired
    private MongoClientFactoryWave mongoClientFactoryWave;    
        

    public WaveDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactoryWave.getMongoOperations();
    }

    public void create(SessionIdVsServer p) {
        logger.info("Entering create: "+ p.toString());
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("Error in saving sessionIdVsServer "+ex.getMessage());
        }
    }
    public List<SessionIdVsServer> getServerOfSessionId(String sessionId) {

        List<SessionIdVsServer> response = new ArrayList<>();

        Query query = new Query();
        query.addCriteria(Criteria.where(SessionIdVsServer.Constants.SESSIONID).is(sessionId));

        logger.info("fetching servers for "+sessionId+", query: "+query.toString());
        
        List<SessionIdVsServer> results = runQuery(query, SessionIdVsServer.class);
        if (results.size() > 0) {
            results.stream().forEach((result) -> {
                response.add(result);
            });
        }
        logger.info("exiting getServerOfSessionId "+response);
        return response;
    }
    
    public void upsertWhiteboardPdf(WhiteboardPDF p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            super.saveEntity(p, callingUserIdString);
        }
    }
    
    public WhiteboardPDF getWhiteboardPdfById(String id) {
        WhiteboardPDF GetRequestsResponse = null;
        if (id != null) {
            GetRequestsResponse = super.getEntityById(id,
                    WhiteboardPDF.class);
        }
        return GetRequestsResponse;
    }
}
