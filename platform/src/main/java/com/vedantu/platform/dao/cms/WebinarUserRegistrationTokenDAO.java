/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.cms;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.pojo.cms.WebinarUserRegistrationToken;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class WebinarUserRegistrationTokenDAO extends AbstractMongoDAO {
    
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(WebinarUserRegistrationTokenDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public WebinarUserRegistrationTokenDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void upsert(WebinarUserRegistrationToken p) {
        if (p != null) {
            super.saveEntity(p);
        }
    }
    
    public WebinarUserRegistrationToken getTokenByPhone(String phone, String id) {
        logger.info("getTokenByPhone ", phone);
        WebinarUserRegistrationToken token = null;
        Query query = new Query();

        if (phone != null  && id != null) {
            query.addCriteria(Criteria.where(WebinarUserRegistrationToken.Constants.PHONE).is(
                    phone));
            query.addCriteria(Criteria.where(WebinarUserRegistrationToken.Constants.WEBINARID).is(
                    id));
            logger.info("query " + query);
            token = findOne(query, WebinarUserRegistrationToken.class);
            logger.info("getByWebinarCode" + token);
        }
        return token;
    }
}
