/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.feedback;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.feedback.FeedbackFormResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


/**
 *
 * @author parashar
 */
@Service
public class FeedbackFormResponseDAO extends AbstractMongoDAO{

    @Autowired
    private MongoClientFactory mongoClientFactory;    
    
    @Autowired
    private LogFactory logfactory;
    
    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(FeedbackFormResponseDAO.class);      
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(FeedbackFormResponse p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        boolean newCP = StringUtils.isEmpty(p.getId());
        if (p != null) {
            saveEntity(p, callingUserIdString);
        }
    }
    
    public void save(FeedbackFormResponse p) {
        if (p != null) {
            boolean newCP = StringUtils.isEmpty(p.getId());
            saveEntity(p);
        }
    }
    
    public List<FeedbackFormResponse> getResponsesForUser(Long userId){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackFormResponse.Constants.USER_ID).is(userId));
        return runQuery(query, FeedbackFormResponse.class);        
    }
    
    public List<FeedbackFormResponse> getResponsesForForm(String formId, Long userId){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(FeedbackFormResponse.Constants.FORM_ID).is(formId));
        if(userId != null){
            query.addCriteria(Criteria.where(FeedbackFormResponse.Constants.USER_ID).is(userId));
        }
        return runQuery(query, FeedbackFormResponse.class);        
    }    
    
}
