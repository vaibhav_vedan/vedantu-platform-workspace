package com.vedantu.platform.dao;

import com.vedantu.platform.dao.askadoubt.DoubtDao;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.TeachersDay2020;
import com.vedantu.platform.pojo.askadoubt.Doubt;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class TeachersDay2020DAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TeachersDay2020DAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public TeachersDay2020DAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public void create(TeachersDay2020 p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void getTeacherWithQueryString(String nameQuery) {
        Query query = new Query();

        if (!StringUtils.isEmpty(nameQuery)) {
            nameQuery = nameQuery.toLowerCase();
        }

        TextCriteria textcriteria = TextCriteria.forDefaultLanguage();
        String[] mustContain = nameQuery.split(" ");
        if (Objects.nonNull(mustContain) && mustContain.length > 0) {
            List<String> mustContainList = Arrays.asList(mustContain);
            if (mustContainList.size() > 4) {
                mustContainList = mustContainList.subList(0, 4);
            }
            for (String ph : mustContainList) {
                textcriteria.matchingPhrase(ph);
            }
        }

        query = TextQuery.queryText(textcriteria)
                .sortByScore();
    }

}
