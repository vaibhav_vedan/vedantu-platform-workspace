package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.entity.HomeDemoRequest;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeDemoRequestDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;


    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public String persist(HomeDemoRequest homeDemoRequest) {

        saveEntity(homeDemoRequest);

        return homeDemoRequest.getId();
    }

    public List<HomeDemoRequest> getRecentRequestByContactNumber(String contactNumber) {

        Query query = new Query();
        query.addCriteria(Criteria.where(HomeDemoRequest.Constants.IS_ACTIVITY_PUSHED).is(Boolean.FALSE));
        query.addCriteria(Criteria.where(HomeDemoRequest.Constants.CONTACT_NUMBER).is(contactNumber));
        List<HomeDemoRequest> results = runQuery(query, HomeDemoRequest.class);

        if(CollectionUtils.isNotEmpty(results))
            return results;

        return null;
    }

    public List<HomeDemoRequest> getHomeDemoRequestByTimeRange(Long startTime, Long endTime) {

        Query query = new Query();
        query.addCriteria(Criteria.where(HomeDemoRequest.Constants.IS_ACTIVITY_PUSHED).is(Boolean.FALSE));
        query.addCriteria(Criteria.where(HomeDemoRequest.Constants.CREATION_TIME).gte(startTime).lte(endTime));

        List<HomeDemoRequest> results = runQuery(query, HomeDemoRequest.class);

        return results;
    }

    public List<HomeDemoRequest> getHomeDemoRequestNotPushed() {

        Query query = new Query();
        query.addCriteria(Criteria.where(HomeDemoRequest.Constants.IS_ACTIVITY_PUSHED).is(Boolean.FALSE));

        List<HomeDemoRequest> results = runQuery(query, HomeDemoRequest.class);

        return results;
    }

    public HomeDemoRequest getLastLSPushedHomeDemoDetails() {

        Query query = new Query();
        query.addCriteria(Criteria.where(HomeDemoRequest.Constants.IS_ACTIVITY_PUSHED).is(Boolean.TRUE));
        query.with(Sort.by(Sort.Direction.DESC, HomeDemoRequest.Constants.CREATION_TIME));
        query.limit(1);
        List<HomeDemoRequest> results = runQuery(query, HomeDemoRequest.class);

        if(CollectionUtils.isNotEmpty(results))
            return results.get(0);

        return null;
    }
}
