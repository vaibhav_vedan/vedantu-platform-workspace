package com.vedantu.platform.dao.dashboard;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.dashboard.OTFDashboard;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.mongodb.core.query.Criteria;

@Service
public class OTFDashboardDAO extends AbstractMongoDAO{

	@Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(OTFDashboardDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public OTFDashboardDAO() {
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void create(OTFDashboard p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    
	public void updateAll(List<OTFDashboard> p) {
		if (p != null && !p.isEmpty()) {
			insertAllEntities(p, OTFDashboard.class.getSimpleName());
		}
	}
        
        public void remove(List<String> ids) {
            if(ArrayUtils.isNotEmpty(ids)){
                Query query = new Query();
                logger.info("enrollment Id " +ids.get(0));
                query.addCriteria(Criteria.where(OTFDashboard.Constants.ENROLLMENT_ID).in(ids));
                getMongoOperations().remove(query, OTFDashboard.class,OTFDashboard.class.getSimpleName());                
            }else {
            	logger.info("EnrollmentIds is null");
            }
        }  
	
	public OTFDashboard getById(String id) {
		OTFDashboard session = null;
		if (!StringUtils.isEmpty(id)) {
			session = getEntityById(id, OTFDashboard.class);
		}
		return session;
	}
	
	public List<OTFDashboard> getOTFDashboard(int start,int size) {
		Query query = new Query();
		query.skip(start);
        query.limit(size);
        return runQuery(query,OTFDashboard.class);
	}
}
