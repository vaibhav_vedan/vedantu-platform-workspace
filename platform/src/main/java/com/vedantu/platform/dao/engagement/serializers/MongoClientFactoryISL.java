package com.vedantu.platform.dao.engagement.serializers;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoClientFactory;
import org.springframework.stereotype.Service;

@Service
public class MongoClientFactoryISL extends AbstractMongoClientFactory {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(MongoClientFactoryISL.class);

    private final String hosts = ConfigUtils.INSTANCE.getStringValue("MONGO_HOST_ISL");
    private final String port = ConfigUtils.INSTANCE.getStringValue("MONGO_PORT");
    private final boolean useAuthentication = ConfigUtils.INSTANCE.getBooleanValue("useAuthentication");
    private final String mongoUsername = ConfigUtils.INSTANCE.getStringValue("MONGO_USER_NAME_ISL");
    private final String mongoPassword = ConfigUtils.INSTANCE.getStringValue("MONGO_PASSWORD_ISL");
    private final String mongoDBName = "vedantuisl";
    private final int connectionsPerHost = 50;
    private final String connectionStringType = ConfigUtils.INSTANCE.getStringValue("mongo.connection.string.type", ConnectionStringType.STANDARD_HOST_BASED.name());
    ConnectionStringType _connectionStringType = ConnectionStringType.valueOf(connectionStringType);

    public MongoClientFactoryISL() {
        super();
        initMongoOperations(hosts, port, useAuthentication, mongoUsername,
                mongoPassword, mongoDBName, connectionsPerHost,_connectionStringType);
        logger.info("Mongo connection created successfully, connections created: " + connectionsPerHost);
    }
}
