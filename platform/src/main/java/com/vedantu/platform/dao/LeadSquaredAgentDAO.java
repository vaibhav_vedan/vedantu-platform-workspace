package com.vedantu.platform.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.LeadSquaredAgent;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class LeadSquaredAgentDAO extends AbstractMongoDAO{

	@Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(LeadSquaredAgentDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public LeadSquaredAgentDAO() {
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void create(LeadSquaredAgent p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    
	public LeadSquaredAgent getByUserId(Long id) {
		LeadSquaredAgent session = null;
		if (id!= null) {
			Query query = new Query();
			query.addCriteria(Criteria.where(LeadSquaredAgent.Constants.USER_ID).is(id));
			
			List<LeadSquaredAgent> agents = runQuery(query,LeadSquaredAgent.class);
			if(ArrayUtils.isNotEmpty(agents)){
				session = agents.get(0);
			}
		}
		return session;
	}
	
	public List<LeadSquaredAgent> getByUserIds(List<Long> ids){
		if(ArrayUtils.isNotEmpty(ids)){
			Set<Long> userIds = new HashSet<>(ids);
			return getByUserIds(userIds);
		} else {
			logger.info("ids is empty");
			return new ArrayList<>();
		}
	}
	public List<LeadSquaredAgent> getByUserIds(Set<Long> ids) {
		List<LeadSquaredAgent> agents = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(ids)) {
			Query query = new Query();
			query.addCriteria(Criteria.where(LeadSquaredAgent.Constants.USER_ID).in(ids));
			
			agents = runQuery(query,LeadSquaredAgent.class);
			
		}
		return agents;
	}
	
	public void updateAll(List<LeadSquaredAgent> p) {
		if (p != null && !p.isEmpty()) {
			insertAllEntities(p, LeadSquaredAgent.class.getSimpleName());
		}
	}
}
