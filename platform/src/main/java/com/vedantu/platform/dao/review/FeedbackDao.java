package com.vedantu.platform.dao.review;

import java.util.List;

import org.apache.logging.log4j.Logger;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.StringUtils;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.mongodbentities.review.Feedback;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class FeedbackDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(FeedbackDao.class);

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public FeedbackDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Feedback p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(Feedback p) {
        upsert(p, null);
    }

    public void upsert(Feedback p, String callingUserId) {
        try {
            if (p != null) {
                saveEntity(p, callingUserId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public Feedback getById(String id) {
        Feedback GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (Feedback) getEntityById(id,
                        Feedback.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }


    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, Feedback.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<Feedback> get(List<String> ids, String receiverId,
            String senderId, String sessionId, Integer rating, Long fromTime,
            Long toTime, Integer start, Integer limit, Boolean orderDesc) {
        try {
            Query query = new Query();

            if (ids != null && !ids.isEmpty()) {
                query.addCriteria(Criteria.where("id").in(ids));
            } else {

                if (receiverId != null && !receiverId.isEmpty()) {
                    query.addCriteria(Criteria.where(
                            Feedback.Constants.RECEIVER_ID).is(receiverId));
                }

                if (senderId != null && !senderId.isEmpty()) {
                    query.addCriteria(Criteria.where(
                            Feedback.Constants.SENDER_ID).is(senderId));
                }

                if (sessionId != null && !sessionId.isEmpty()) {
                    query.addCriteria(Criteria.where(
                            Feedback.Constants.SESSION_ID).is(sessionId));
                }

                if (rating != null && rating > 0) {
                    query.addCriteria(Criteria.where(Feedback.Constants.RATING)
                            .is(rating));
                }

                if (fromTime != null) {
                    if (toTime == null) {
                        toTime = System.currentTimeMillis();
                    }
                    query.addCriteria(Criteria
                            .where(AbstractMongoStringIdEntity.Constants.CREATION_TIME)
                            .lte(toTime).gte(fromTime));
                }

                setFetchParameters(query, start, limit);

            }

            if (orderDesc != null && orderDesc) {
                query.with(Sort.by(Sort.Direction.DESC,
                        AbstractMongoStringIdEntity.Constants.CREATION_TIME));
            } else {
                query.with(Sort.by(Sort.Direction.ASC,
                        AbstractMongoStringIdEntity.Constants.CREATION_TIME));
            }

            List<Feedback> result = runQuery(query, Feedback.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }


}
