package com.vedantu.platform.dao.cms;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.entity.WebinarSpotInstanceMetadata;
import com.vedantu.platform.pojo.WebinarSpotInstanceFilterReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebinarSpotInstanceMetadataDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = LogFactory.getLogger(WebinarDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public WebinarSpotInstanceMetadataDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void upsert(WebinarSpotInstanceMetadata webinarSpotInstanceMetadata) {
        if (webinarSpotInstanceMetadata != null) {
            super.saveEntity(webinarSpotInstanceMetadata, null);
        }
    }

    public List<WebinarSpotInstanceMetadata> getSpotInstanceMetadata(WebinarSpotInstanceFilterReq filterReq) {
        logger.info("getSpotInstanceMetadata {}", filterReq);
        List<WebinarSpotInstanceMetadata> webinarSpotInstanceMetadataList = null;

        if(filterReq.getStart() == null)
            filterReq.setSize(0);

        if(filterReq.getSize() == null || filterReq.getSize() <= 0)
            filterReq.setSize(100);

        Query query = new Query();

        if (CollectionUtils.isNotEmpty(filterReq.getWebinarIds())) {
            query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_ID).in(filterReq.getWebinarIds()));
        }else if(StringUtils.isNotEmpty(filterReq.getWebinarId())){
            query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_ID).is(filterReq.getWebinarId()));
        }

        if (CollectionUtils.isNotEmpty(filterReq.getInstanceStatusList())) {
            query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.INSTANCE_STATUS).in(filterReq.getInstanceStatusList()));
        }else if (filterReq.getInstanceStatus() != null) {
            query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.INSTANCE_STATUS).is(filterReq.getInstanceStatus()));
        }

        if(filterReq.getWebinarFromStartTime() != null && filterReq.getWebinarToStartTime() != null){
            Criteria criteria1 = Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_START_TIME).gte(filterReq.getWebinarFromStartTime());
            Criteria criteria2 = Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_START_TIME).lte(filterReq.getWebinarToStartTime());
            Criteria andCriteria = new Criteria().andOperator(criteria1, criteria2);
            query.addCriteria(andCriteria);
        }else {

            if (filterReq.getWebinarFromStartTime() != null) {
                query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_START_TIME).gte(filterReq.getWebinarFromStartTime()));
            }else if (filterReq.getWebinarToStartTime() != null) {
                query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_START_TIME).lte(filterReq.getWebinarToStartTime()));
            }
        }

        query.with(Sort.by(Sort.Direction.DESC,WebinarSpotInstanceMetadata.Constants.LAST_UPDATED_TIME));

        logger.info("query " + query);
        webinarSpotInstanceMetadataList = runQuery(query, WebinarSpotInstanceMetadata.class);
        logger.info("getSpotInstanceMetadata" + webinarSpotInstanceMetadataList);
        return webinarSpotInstanceMetadataList;
    }

    public WebinarSpotInstanceMetadata getSpotInstanceMetadataByWebinarId(String webinarId) {
        logger.info("getSpotInstanceMetadataByWebinarId {}", webinarId);

        Query query = new Query();

        query.addCriteria(Criteria.where(WebinarSpotInstanceMetadata.Constants.WEBINAR_ID).is(webinarId));
        query.with(Sort.by(Sort.Direction.DESC,WebinarSpotInstanceMetadata.Constants.LAST_UPDATED_TIME));

        logger.info("query " + query);
        return findOne(query, WebinarSpotInstanceMetadata.class);
    }

}
