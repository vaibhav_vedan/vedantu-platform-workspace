/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.filemgmt;

/**
 *
 * @author somil
 */
import com.vedantu.platform.mongodbentities.filemgmt.File;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.platform.dao.board.CounterService;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import org.springframework.data.mongodb.core.MongoOperations;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class FileDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    @Autowired
    CounterService counterService;

    Logger logger = logFactory.getLogger(FileDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public FileDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    /*
    public void create(Board p) {
        try {
            if (p != null) {
                AbstractDAO.saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
     */
    public void upsert(File p, Long callingUserId) {
            if (p != null) {
                String callingUserIdString = null;
                if(callingUserId!=null) {
                    callingUserIdString = callingUserId.toString();
                }
                if (p.getId() == null || p.getId() == 0L) {
                    p.setId(counterService.getNextSequence(p.getClass().getSimpleName()));
                }
                super.saveEntity(p, callingUserIdString);
            }
    }

    public File getById(Long id) throws NotFoundException {
        File GetRequestsResponse = null;
            if (id != null) {
                GetRequestsResponse = super
                        .getEntityById(id, File.class);
            }
        return GetRequestsResponse;
    }


}
