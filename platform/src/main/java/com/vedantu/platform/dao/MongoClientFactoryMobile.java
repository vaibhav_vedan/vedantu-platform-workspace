package com.vedantu.platform.dao;


import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoClientFactory;
import org.springframework.stereotype.Service;

@Service
public class MongoClientFactoryMobile extends AbstractMongoClientFactory {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(MongoClientFactoryMobile.class);

    private final String hosts = ConfigUtils.INSTANCE.getStringValue("MONGO_HOST_MOBILE");
    private final String port = ConfigUtils.INSTANCE.getStringValue("MONGO_PORT");
    private final boolean useAuthentication = ConfigUtils.INSTANCE.getBooleanValue("useAuthentication");
    private final String mongoUsername = "mongoadmin";
    private final String mongoPassword = "3Kyec7jUcAAePDCC";
    private final String mongoDBName = "vedantumobile";
    private final String connectionStringType = ConfigUtils.INSTANCE.getStringValue("mongo.connection.string.type", ConnectionStringType.STANDARD_HOST_BASED.name());
    ConnectionStringType _connectionStringType=ConnectionStringType.valueOf(connectionStringType);
    private final int connectionsPerHost = 50;

    public MongoClientFactoryMobile() {
        super();
        initMongoOperations(hosts, port, useAuthentication, mongoUsername,
                mongoPassword, mongoDBName, connectionsPerHost,_connectionStringType);
        logger.info("Mongo connection created successfully, connections created: " + connectionsPerHost);
    }
}
