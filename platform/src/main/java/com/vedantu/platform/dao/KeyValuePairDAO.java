/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.KeyValuePair;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class KeyValuePairDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(KeyValuePairDAO.class);

    
    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(KeyValuePair p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
        }
    }

    public KeyValuePair getById(String id) {
        KeyValuePair KeyValuePair = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                KeyValuePair = getEntityById(id, KeyValuePair.class);
            }
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
            KeyValuePair = null;
        }
        return KeyValuePair;
    }
    
    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, KeyValuePair.class);
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
        }

        return result;
    }


    public String getByKey(String key) {
        KeyValuePair keyValuePair = null;
        String value = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(KeyValuePair.Constants.KEY).is(key));
        List<KeyValuePair> results = runQuery(query, KeyValuePair.class);
        if (null != results && !results.isEmpty()) {
            keyValuePair = results.get(0);
        }
        if(keyValuePair!=null) {
            value = keyValuePair.getValue();
        }
        return value;
    }

}

