package com.vedantu.platform.dao.requestcallback;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.pojo.requestcallback.request.GetRequestCallBacksDetailsReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class RequestCallbackDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(RequestCallbackDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public RequestCallbackDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(RequestCallBackDetails p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(RequestCallBackDetails p) {
        upsert(p, null);
    }

    public void upsert(RequestCallBackDetails p, String callingUserId) {
        try {
            if (p != null) {
                saveEntity(p, callingUserId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public RequestCallBackDetails getById(String id) {
        RequestCallBackDetails GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (RequestCallBackDetails) getEntityById(id, RequestCallBackDetails.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id,
                    RequestCallBackDetails.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<RequestCallBackDetails> get(
            GetRequestCallBacksDetailsReq getRequestCallBacksDetailsReq) {
        try {
            Query query = new Query();
            if (getRequestCallBacksDetailsReq.getId() != null) {
                query.addCriteria(Criteria.where(
                        "id").is(
                                getRequestCallBacksDetailsReq.getId()));
            }
            if (getRequestCallBacksDetailsReq.getStudentId() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.STUDENT_ID).is(
                                getRequestCallBacksDetailsReq.getStudentId()));
            }
            if (getRequestCallBacksDetailsReq.getTeacherId() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.TEACHER_ID).is(
                                getRequestCallBacksDetailsReq.getTeacherId()));
            }
            if (getRequestCallBacksDetailsReq.getAssignedTo() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.ASSIGNED_TO).is(
                                getRequestCallBacksDetailsReq.getAssignedTo()));
            }
            if (getRequestCallBacksDetailsReq.getBoardId() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.BOARD_ID).is(
                                getRequestCallBacksDetailsReq.getBoardId()));
            }
            if (getRequestCallBacksDetailsReq.getGrade() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.GRADE).is(
                                getRequestCallBacksDetailsReq.getGrade()));
            }
            if (getRequestCallBacksDetailsReq.getTarget() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.TARGET).is(
                                getRequestCallBacksDetailsReq.getStudentId()));
            }
            if (getRequestCallBacksDetailsReq.getTeacherLeadStatuses() != null) {
                query.addCriteria(Criteria.where(RequestCallBackDetails.Constants.TEACHER_LEAD_STATUS).in(getRequestCallBacksDetailsReq.getTeacherLeadStatuses()));
            }

            if (getRequestCallBacksDetailsReq.getParent() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.PARENT).is(
                                getRequestCallBacksDetailsReq.getParent()));
            }
            if (getRequestCallBacksDetailsReq.getTeacherActionTaken() != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.TEACHER_ACTION_TAKEN)
                        .is(getRequestCallBacksDetailsReq
                                .getTeacherActionTaken()));
            }

            if (getRequestCallBacksDetailsReq.getFromTime() != null) {
                Long toTime = getRequestCallBacksDetailsReq.getToTime();
                if (toTime == null) {
                    toTime = System.currentTimeMillis();
                }
                query.addCriteria(Criteria.where(AbstractMongoStringIdEntity.Constants.CREATION_TIME)
                        .lte(toTime).gte(getRequestCallBacksDetailsReq.getFromTime()));
            }

            if (getRequestCallBacksDetailsReq.getSize() != null) {
                query.limit(getRequestCallBacksDetailsReq.getSize());
            }

            if (getRequestCallBacksDetailsReq.getStart() != null) {
                query.skip(getRequestCallBacksDetailsReq.getStart());
            } else {
                query.skip(0);
            }
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));

            List<RequestCallBackDetails> result = runQuery(query,
                    RequestCallBackDetails.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }

    public Boolean updateAll(List<RequestCallBackDetails> requestCallBackDetailsList) {
        try {
            insertAllEntities(requestCallBackDetailsList, RequestCallBackDetails.class.getSimpleName());
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    // public List<RequestCallBackDetails> getExport(RequestCallBackDetails p,
    // Long startTime, Long endTime) {
    // List<RequestCallBackDetails> result =
    // runExport(RequestCallBackDetails.class,startTime,endTime);
    // return result;
    // }

    public List<RequestCallBackDetails> getByTandSCombination(Long studentId, Long teacherId) {
        Query query = new Query();

        try {
            if (studentId != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.STUDENT_ID)
                        .is(studentId));
            }

            if (teacherId != null) {
                query.addCriteria(Criteria.where(
                        RequestCallBackDetails.Constants.TEACHER_ID)
                        .is(teacherId));
            }

            query.skip(0);
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));

            List<RequestCallBackDetails> result = runQuery(query,
                    RequestCallBackDetails.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }
    }
}
