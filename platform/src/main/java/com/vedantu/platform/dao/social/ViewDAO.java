/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.social;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.mongodbentities.social.View;
import com.vedantu.platform.response.social.LastViewTime;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import static com.vedantu.platform.enums.SocialContextType.STUDY_ENTRY_ITEM;

/**
 *
 * @author parashar
 */
@Service
public class ViewDAO extends AbstractMongoDAO{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ViewDAO.class);
 
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public ViewDAO() {
        super();
    }

    public void save(View entity) {
        saveEntity(entity);
    }
    
    public View getByContextIdAndUserId(String contextId, Long userId){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(View.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(View.Constants.CONTEXT_ID).is(contextId));
        
        return findOne(query, View.class);
        
    }
    
    public LastViewTime getLastView(Long userId, String socialConextType) {
    	Query query = new Query();
        query.addCriteria(Criteria.where(View.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(View.Constants.SOCIAL_CONTEXT_TYPE).is(socialConextType));
        query.with(Sort.by(Sort.Direction.DESC, View.Constants.LAST_UPDATED));
        logger.info("Query :: " + query);
        View view = findOne(query, View.class);

        LastViewTime viewDet = new LastViewTime();

        if(view != null) {
            logger.info("last view updated time :: " + view.getCreationTime() + " of video :: " + view.getContextId());
            viewDet.setVideoId(view.getContextId());
            viewDet.setViewTime(view.getLastUpdated());
        }
        return viewDet;
    }

    public List<View> getRecentActivities(String userId, Set<SocialContextType> contextTypes, int limit) {
        logger.info("Getting {} Recent Activities from View Collection for userId={}", limit, userId);
        Query query = new Query();
        query.addCriteria(Criteria.where(View.Constants.USER_ID).is(Long.valueOf(userId)));
        query.addCriteria(Criteria.where(View.Constants.SOCIAL_CONTEXT_TYPE).in(contextTypes));
        query.with(Sort.by(Sort.Direction.DESC, View.Constants.LAST_UPDATED))
                .limit(limit);
        return runQuery(query, View.class);
    }

    public Long getPdfViewsByUserId(String userId, Long releaseDate)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(View.Constants.LAST_UPDATED).gte(releaseDate))
                .addCriteria(Criteria.where(View.Constants.USER_ID).is(Long.parseLong(userId)))
                .addCriteria(Criteria.where(View.Constants.SOCIAL_CONTEXT_TYPE).is(STUDY_ENTRY_ITEM));
        return queryCount(query, View.class);
    }
}