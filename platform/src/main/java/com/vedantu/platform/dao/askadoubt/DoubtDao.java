package com.vedantu.platform.dao.askadoubt;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.enums.askadoubt.DoubtStatus;
import com.vedantu.platform.pojo.askadoubt.Doubt;
import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class DoubtDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(DoubtDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public DoubtDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Doubt p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void upsert(Doubt p) {
        upsert(p, null);
    }

    public void upsert(Doubt p, String callingUserId) {
        try {
            if (p != null) {
                saveEntity(p, callingUserId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public Doubt getById(String id) {
        Doubt GetRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GetRequestsResponse = (Doubt) getEntityById(id, Doubt.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id,
                    Doubt.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<Doubt> get(String id, Long userId, DoubtStatus status, Long assignedTo,
            Integer start, Integer size) {
        try {
            Query query = new Query();

            if (id != null) {
                query.addCriteria(Criteria.where(
                        "id").is(
                                id));
            } else {

                if (userId != null) {
                    query.addCriteria(Criteria.where(
                            Doubt.Constants.USER_ID).is(
                                    userId));
                }
                if (status != null) {
                    query.addCriteria(Criteria.where(
                            Doubt.Constants.STATUS).is(
                                    status));
                }

                if (assignedTo != null) {
                    query.addCriteria(Criteria.where(
                            Doubt.Constants.ASSIGNED_TO).is(
                                    assignedTo));
                }

//			if (getRequestCallBacksDetailsReq.getFromTime()!=null) {
//				Long toTime = getRequestCallBacksDetailsReq.getToTime();
//				if(toTime==null){
//					toTime = System.currentTimeMillis();
//				}
//				query.addCriteria(Criteria.where(
//						AbstractMongoStringIdEntity.Constants.CREATION_TIME)
//						.lte(toTime).gte(getRequestCallBacksDetailsReq.getFromTime()));
//			}
                if (size != null) {
                    query.limit(size);
                }

                if (start != null) {
                    query.skip(start);
                } else {
                    query.skip(0);
                }
            }
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));

            List<Doubt> result = runQuery(query,
                    Doubt.class);

            return result;
        } catch (Throwable e) {
            throw e;
        }

    }

}
