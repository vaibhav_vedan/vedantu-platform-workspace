/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.AdminAccess;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class AdminAccessDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AdminAccessDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(AdminAccess p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
        }
    }


    public AdminAccess getByAdminId(Long adminId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(AdminAccess.Constants.ADMIN_ID).is(adminId));
        return findOne(query, AdminAccess.class);
    }

}
