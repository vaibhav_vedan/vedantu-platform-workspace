/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao;

/**
 *
 * @author somil
 */
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Service;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ESConfig {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ESConfig.class);

    private TransportClient client = null;

    private static String host;
    private static int port;
    private static String index;
    private static String type;
    private static String cluster;
    private static String node;

    @PostConstruct
    public void init() {
        host = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseurl");
        port = ConfigUtils.INSTANCE.getIntValue("elasticsearch.port");
        index = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseIndex");
        type = ConfigUtils.INSTANCE.getStringValue("elasticsearch.teachers.typeName");
        cluster = ConfigUtils.INSTANCE.getStringValue("elasticsearch.clusterName");
        node = ConfigUtils.INSTANCE.getStringValue("elasticsearch.nodeName");

        ImmutableSettings.Builder builder = ImmutableSettings.settingsBuilder();
        if (cluster != null) {
            builder.put("cluster.name", cluster);
        }
        if (node != null) {
            builder.put("node.name", node);
        }

        Settings settings = builder.build();
        client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress(host, port));
        logger.info("Transport Client: " + client.transportAddresses());
    }

    public ESConfig() {
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getIndex() {
        return index;
    }

    public String getType() {
        return type;
    }

    public static String getCluster() {
        return cluster;
    }

    public static String getNode() {
        return node;
    }

    public Client getTransportClient() {
        return this.client;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing es connection ", e);
        }
    }
}
