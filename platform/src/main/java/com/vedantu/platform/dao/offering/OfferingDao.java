package com.vedantu.platform.dao.offering;

import com.vedantu.platform.dao.board.CounterService;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.platform.mongodbentities.offering.Offering;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.enums.OfferingType;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class OfferingDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(OfferingDao.class);

    @Autowired
    private CounterService counterService;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public OfferingDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(Offering offering, String callingUserId) {
        try {
            // Create session
            if (offering.getId() == null || offering.getId() <= 0l) {
                offering.setId(counterService.getNextSequence(offering.getClass().getSimpleName()));
            }
            saveEntity(offering, callingUserId);
        } catch (Exception ex) {
            throw new RuntimeException("update : Error inserting the offering " + offering, ex);
        }
    }

    public void save(Offering offering) {
        save(offering, null);
    }

    public Offering getById(Long id) {
        Offering getRequestsResponse = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                getRequestsResponse = (Offering) getEntityById(id,
                        Offering.class);
            }
        } catch (Exception ex) {
            getRequestsResponse = null;
        }
        return getRequestsResponse;
    }


    public List<Offering> get(List<String> ids, OfferingType offeringType,
            List<String> tags, String exam, String grade, String boardId,
            String boardSlug, String category, String userId,
            VisibilityState offeringState, String query,
            Boolean onlyRequiredFields, List<String> offeringIds,
            List<String> subtypes, List<String> includeScopes, boolean includeFeaturedOnly, Long fromTime, Long toTime, Integer start,
            Integer limit) {
        try {
            Query mongoQuery = new Query();

            if (ids != null && !ids.isEmpty()) {
                mongoQuery.addCriteria(Criteria.where("id").in(ids));
            } else {

                if (offeringType != null) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.TYPE).is(offeringType));
                }

                if (!StringUtils.isEmpty(grade)) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.GRADES).in(grade));
                }

                if (boardId != null && !boardId.isEmpty()) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.BOARD_IDS).in(boardId));
                }

                if (!StringUtils.isEmpty(category)) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.CATEGORIES).in(category));
                }

                if (offeringState != null) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.STATE).is(offeringState));
                }

                if (tags != null && !tags.isEmpty()) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.TAGS).in(tags));
                }

                if (exam != null && !exam.isEmpty()) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.EXAM_TAGS).in(exam));
                }

                if (includeScopes != null && !includeScopes.isEmpty()) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.SCOPE).in(includeScopes));
                }
                if (includeFeaturedOnly) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.IS_FEATURED).in(includeFeaturedOnly));
                }
                if (subtypes != null && subtypes.isEmpty()) {
                    mongoQuery.addCriteria(Criteria
                            .where(Offering.Constants.OFFERING_SUB_TYPE).in(subtypes));
                }

                if (fromTime != null) {
                    if (toTime == null) {
                        toTime = System.currentTimeMillis();
                    }
                    mongoQuery.addCriteria(Criteria
                            .where(AbstractMongoLongIdEntity.Constants.CREATION_TIME)
                            .lte(toTime).gte(fromTime));
                }

                if (limit != null) {
                    mongoQuery.limit(limit);
                }

                if (start != null) {
                    mongoQuery.skip(start);
                } else {
                    mongoQuery.skip(0);
                }

                mongoQuery.with(Sort.by(Sort.Direction.DESC,
                        Offering.Constants.PRIORITY));
                logger.info("query " + mongoQuery);
            }
            List<Offering> result = runQuery(mongoQuery, Offering.class);
            return result;

//				if (StringUtils.isNotEmpty(titleQuery)) {
//
//					Set<String> queryTokens = SearchJanitorUtils.getTokensForIndexingOrQuery(titleQuery, false);
//
//					Query q2 = getPersistenceManager().newQuery("SELECT id FROM " + OfferingIndex.class.getName());
//					q2.setFilter("keywords == param0");
//					q2.declareParameters("String param0");
//					List<Key> keys;
//					List<Key> parents = new ArrayList<Key>();
//					keys = (List<Key>) q2.execute(queryTokens);
//					for (Key k : keys) {
//						parents.add(k.getParent());
//					}
//
//					LOG.info("parent ids : " + parents);
//					filters.add("id == keyParam");
//					declareParams.add("com.google.appengine.api.datastore.Key keyParam");
//					inputParams.add(parents);
//				}
        } catch (Throwable e) {
            throw e;
        }

    }


    public List<Offering> getTeacherOfferings(Long userId) {
        Query mongoQuery = new Query();
        mongoQuery.addCriteria(Criteria
                .where(Offering.Constants.TEACHER_IDS).in(userId.toString()));
        return runQuery(mongoQuery, Offering.class);
    }

}
