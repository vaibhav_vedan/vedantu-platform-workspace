package com.vedantu.platform.dao.wave;

import com.vedantu.platform.enums.wave.SessionBillingVersion;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.bson.types.ObjectId;

public class SessionBillingData extends AbstractMongoStringIdEntity {

    private String context; // utility
    private String type; // type of message which was schedule
    private Long sessionId; // session id which was scheduled
    private Long userId; // Logged-in userId which was directly or indirectly
    private Long sentTime = new Long(0); // This is client time at the time of
    // sending message
    private Long serverTime; // This is server time when server receives this
    // message
    private String socketId;
    private SessionBillingVersion billingVersion = SessionBillingVersion.VERSION_1;

    public String getContext() {
        return context;
    }

    public SessionBillingData(String context, String type, Long sessionId, Long userId, Long serverTime, String socketId, String data) {
        super();
        this.context = context;
        this.type = type;
        this.sessionId = sessionId;
        this.userId = userId;
        this.serverTime = serverTime;
        this.socketId = socketId;
        this.data = data;
    }

    public SessionBillingData(Long sessionId, Long userId, String type, Long serverTime, String socketId) {
        super();
        this.type = type;
        this.sessionId = sessionId;
        this.userId = userId;
        this.serverTime = serverTime;
        this.socketId = socketId;
        this.billingVersion = SessionBillingVersion.VERSION_2;
        this.setId(new ObjectId().toString());//bad but no time, no choice
        this.setCreationTime(serverTime);
    }

    public SessionBillingData() {
        super();
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public void setSentTime(Long sentTime) {
        this.sentTime = sentTime;
    }

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public String getSocketId() {
        return socketId;
    }

    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }

    public SessionBillingVersion getBillingVersion() {
        if (billingVersion == null) {
            billingVersion = SessionBillingVersion.VERSION_1;
        }
        return billingVersion;
    }

    public void setBillingVersion(SessionBillingVersion billingVersion) {
        this.billingVersion = billingVersion;
    }

    @Override
    public String toString() {
        return "SessionBillingData{" + "context=" + context + ", type=" + type + ", sessionId=" + sessionId + ", userId=" + userId + ", sentTime=" + sentTime + ", serverTime=" + serverTime + ", socketId=" + socketId + ", billingVersion=" + billingVersion + ", data=" + data + '}';
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    private String data; // This contains raw data ( Preferably JSON )
}
