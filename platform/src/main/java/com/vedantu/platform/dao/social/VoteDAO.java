/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.social;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.mongodbentities.social.Vote;
import com.vedantu.platform.pojo.social.VoteCount;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

/**
 *
 * @author ajith
 */
@Service
public class VoteDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VoteDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public VoteDAO() {
        super();
    }

    public void save(Vote entity) {
        saveEntity(entity);
    }

    public List<Vote> getVotes(Long userId, SocialContextType socialEntityType,
            String entityId, int start, int size) {
        Query query = prepareQuery(userId, socialEntityType, entityId);
        setFetchParameters(query, start, size);
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        return runQuery(query, Vote.class);
    }

    public Vote getVotesForUser(Long userId, SocialContextType socialEntityType,String entityId, int start, int size) {
        Query query = prepareQuery(userId, socialEntityType, entityId);
        setFetchParameters(query, start, size);
        return findOne(query, Vote.class);
    }    
    public long getVotesCount(Long userId,
            SocialContextType socialEntityType, String entityId) {
        Query query = prepareQuery(userId, socialEntityType, entityId);
        return queryCount(query, Vote.class);
    }

    public Query prepareQuery(Long userId,
            SocialContextType socialEntityType, String entityId) {
        Query query = new Query();
        if (userId != null) {
            query.addCriteria(Criteria.where(Vote.Constants.USER_ID).is(userId));
        }
        if (socialEntityType != null) {
            query.addCriteria(Criteria.where(Vote.Constants.SOCIAL_CONTEXT_TYPE).is(socialEntityType));
        }
        if (StringUtils.isNotEmpty(entityId)) {
            query.addCriteria(Criteria.where(Vote.Constants.CONTEXT_ID).is(entityId));
        }
        return query;
    }

    public Map<String, VoteCount> votesCount(String entityId) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(match(Criteria.where(Vote.Constants.CONTEXT_ID).is(entityId).andOperator(Criteria.where(Vote.Constants.VOTE_VALUE).ne(0))),
                group(Vote.Constants.VOTE_VALUE).sum(Vote.Constants.VOTE_VALUE).as(Vote.Constants.UP_VOTES),
                project(Vote.Constants.UP_VOTES).and(Vote.Constants.VOTE_VALUE).previousOperation()).withOptions(aggregationOptions);
        AggregationResults<VoteCount> groupResults
                = getMongoOperations().aggregate(agg, Vote.class.getSimpleName(), VoteCount.class);
        logger.info("aff " + agg);
        List<VoteCount> result = groupResults.getMappedResults();
        Map<String, VoteCount> map = new HashMap<>();
        if (ArrayUtils.isNotEmpty(result)) {
            for (VoteCount obj : result) {
                obj.setDownVotes(Math.abs(obj.getDownVotes()));
                map.put(Integer.toString(obj.getVoteValue()), obj);
            }
        }
        return map;
    }    
    
    public Map<String, VoteCount> votesCount(Set<String> commentIds) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(match(Criteria.where(Vote.Constants.CONTEXT_ID).in(commentIds)),
                group(Vote.Constants.CONTEXT_ID).sum(Vote.Constants.VOTE_VALUE).as(Vote.Constants.UP_VOTES)
                .sum(Vote.Constants.VOTE_VALUE).as(Vote.Constants.DOWN_VOTES),
                project(Vote.Constants.UP_VOTES, Vote.Constants.DOWN_VOTES).and(Vote.Constants.CONTEXT_ID).previousOperation()).withOptions(aggregationOptions);
        AggregationResults<VoteCount> groupResults
                = getMongoOperations().aggregate(agg, Vote.class.getSimpleName(), VoteCount.class);
        logger.info("aff " + agg);
        List<VoteCount> result = groupResults.getMappedResults();
        Map<String, VoteCount> map = new HashMap<>();
        if (ArrayUtils.isNotEmpty(result)) {
            for (VoteCount obj : result) {
                obj.setDownVotes(Math.abs(obj.getDownVotes()));
                map.put(obj.getContextId(), obj);
            }
        }
        return map;
    }

    public Map<String, Boolean> getUpvotedMap(Long userId,
            List<String> entityIds) {
        Map<String, Boolean> map = new HashMap<>();
        if (userId == null) {
            return map;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Vote.Constants.USER_ID).is(userId));
        if (ArrayUtils.isNotEmpty(entityIds)) {
            query.addCriteria(Criteria.where(Vote.Constants.CONTEXT_ID).in(entityIds));
        }
        List<Vote> votes = runQuery(query, Vote.class);
        if (ArrayUtils.isNotEmpty(votes)) {
            for (Vote vote : votes) {
                if (vote.getVoteValue() >= 1) {
                    map.put(vote.getContextId(), true);
                }
            }
        }
        return map;
    }

}
