/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.dao.wave;

import com.vedantu.User.Role;
import com.vedantu.platform.enums.wave.WavebookScope;
import com.vedantu.platform.pojo.wave.Wavebook;
import com.vedantu.platform.pojo.wave.WavebookVersion;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class WavebookDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(WavebookDAO.class);

    @Autowired
    private MongoClientFactoryWave mongoClientFactoryWave;

    public WavebookDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactoryWave.getMongoOperations();
    }

    public void upsert(Wavebook p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            super.saveEntity(p, callingUserIdString);
        }
    }

    public Wavebook getById(String id) {
        Wavebook GetRequestsResponse = null;
        if (id != null) {
            GetRequestsResponse = super.getEntityById(id,
                    Wavebook.class);
        }
        return GetRequestsResponse;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, Wavebook.class);
        } catch (Exception ex) {
            logger.error("EXCEPTION: " + ex.getMessage());
        }

        return result;
    }

    public List<Wavebook> getWavebooksByUserId(Long userId, Integer start, Integer size) {
        logger.info("getWavebooksByUserId ", userId);
        List<Wavebook> wavebooks = null;
        Query query = new Query();

        if (userId != null) {
            query.addCriteria(Criteria.where(Wavebook.Constants.USERID).is(
                    userId));
            query.addCriteria(Criteria.where(Wavebook.Constants.DELETED).ne(true));
            query.with(Sort.by(Sort.Direction.DESC, Wavebook.Constants.CREATION_TIME));
            logger.info("query " + query);
            if (size != null) {
                query.limit(size);
            }
            if (start != null) {
                query.skip(start);
            } else {
                query.skip(0);
            }
            wavebooks = runQuery(query, Wavebook.class);
            logger.info("getWavebooksByUserId" + wavebooks);
        }
        return wavebooks;
    }

    public List<Wavebook> getWavebooksByUserId(Long userId, WavebookScope scope, Integer start, Integer size,
                                               WavebookVersion wavebookVersion) {
        logger.info("getWavebooksByUserId userId:" + userId + ",start:" + start + ",size:" + size);
        List<Wavebook> wavebooks = null;
        Query query = new Query();

        if (userId != null) {
            query.addCriteria(Criteria.where(Wavebook.Constants.USERID).is(
                    userId));
            query.addCriteria(Criteria.where(Wavebook.Constants.DELETED).ne(true));
            if (scope != null) {
                query.addCriteria(Criteria.where(Wavebook.Constants.WAVEBOOKSCOPE).is(scope));
            }
            if (wavebookVersion != null) {
                query.addCriteria(Criteria.where(Wavebook.Constants.WAVEBOOK_VERSION).is(wavebookVersion));
            }
            query.with(Sort.by(Sort.Direction.DESC, Wavebook.Constants.CREATION_TIME));
            logger.info("query " + query);
            if (size != null) {
                query.limit(size);
            }
            if (start != null) {
                query.skip(start);
            } else {
                query.skip(0);
            }
            wavebooks = runQuery(query, Wavebook.class);
            logger.info("getWavebooksByUserId" + wavebooks);
        }
        return wavebooks;
    }

    public List<Wavebook> getPublicWavebooks(Integer start, Integer size, Role userRole) {
        logger.info("getPublicWavebooks " + ",start:" + start + ",size:" + size);
        List<Wavebook> wavebooks = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(Wavebook.Constants.WAVEBOOKSCOPE).is("PUBLIC"));
        if (userRole != null) {
            query.addCriteria(Criteria.where(Wavebook.Constants.USERROLE).is(userRole));
        }
        query.addCriteria(Criteria.where(Wavebook.Constants.DELETED).ne(true));
        query.with(Sort.by(Sort.Direction.DESC, Wavebook.Constants.CREATION_TIME));
        if (size != null) {
            query.limit(size);
        }
        if (start != null) {
            query.skip(start);
        } else {
            query.skip(0);
        }
        logger.info("query " + query);
        wavebooks = runQuery(query, Wavebook.class);
        logger.info("getPublicWavebooks" + wavebooks);
        return wavebooks;
    }

    public List<Wavebook> getAllWavebooks(WavebookScope scope, Integer start, Integer size, WavebookVersion wavebookVersion) {
        logger.info("getAllWavebooks start:" + start + ",size:" + size);
        List<Wavebook> wavebooks = null;
        Query query = new Query();

        query.addCriteria(Criteria.where(Wavebook.Constants.DELETED).ne(true));
        if (scope != null) {
            query.addCriteria(Criteria.where(Wavebook.Constants.WAVEBOOKSCOPE).is(scope));
        }
        if (wavebookVersion != null) {
            query.addCriteria(Criteria.where(Wavebook.Constants.WAVEBOOK_VERSION).is(wavebookVersion));
        }
        query.with(Sort.by(Sort.Direction.DESC, Wavebook.Constants.CREATION_TIME));
        logger.info("query " + query);
        if (size != null) {
            query.limit(size);
        }
        if (start != null) {
            query.skip(start);
        } else {
            query.skip(0);
        }
        wavebooks = runQuery(query, Wavebook.class);
        logger.info("getWavebooksByUserId" + wavebooks);

        return wavebooks;
    }
}
