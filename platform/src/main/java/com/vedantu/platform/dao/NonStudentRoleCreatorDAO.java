package com.vedantu.platform.dao;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import com.vedantu.platform.mongodbentities.NonStudentRoleCreator;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class NonStudentRoleCreatorDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(NonStudentRoleCreatorDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(NonStudentRoleCreator creator){
        try {
            if(creator != null){
                saveEntity(creator);
            }
        }catch (Exception e){
            logger.error("EXCEPTION: "+e.getMessage());
        }
    }

    public NonStudentRoleCreator getByCreatorId(Long creatorId){
        NonStudentRoleCreator creator = null;
        if(creatorId != null){
            Query query = new Query(Criteria.where(NonStudentRoleCreator.CONSTANTS.CREATOR_ID).is(creatorId));
            creator = findOne(query, NonStudentRoleCreator.class);
        }
        return creator;
    }

}
