package com.vedantu.platform.converters;

import com.vedantu.platform.pojo.cms.Webinar;
import com.vedantu.platform.response.WebinarResponse;

public class WebinarToWebinarResponseConverter {

    public static WebinarResponse convert(Webinar webinar) {
        return WebinarResponse.builder()
                .boards(webinar.getBoards())
                .courseInfo(webinar.getCourseInfo())
                .duration(webinar.getDuration())
                .emailImageUrl(webinar.getEmailImageUrl())
                .endTime(webinar.getEndTime())
                .grades(webinar.getGrades())
                .gtwAttendenceSynced(webinar.getGtwAttendenceSynced())
                .gtwSeat(webinar.getGtwSeat())
                .gtwWebinarId(webinar.getGtwWebinarId())
                .leaderBoard(webinar.getLeaderBoard())
                .registerSectionInfo(webinar.getRegisterSectionInfo())
                .replayUrl(webinar.getReplayUrl())
                .rescheduleData(webinar.getRescheduleData())
                .sessionId(webinar.getSessionId())
                .sessionInfo(webinar.getSessionInfo())
                .showInPastWebinars(webinar.getShowInPastWebinars())
                .startTime(webinar.getStartTime())
                .subjects(webinar.getSubjects())
                .tags(webinar.getTags())
                .taIds(webinar.getTaIds())
                .targets(webinar.getTargets())
                .teacherEmail(webinar.getTeacherEmail())
                .teacherInfo(webinar.getTeacherInfo())
                .testimonial(webinar.getTestimonial())
                .title(webinar.getTitle())
                .toolType(webinar.getToolType())
                .type(webinar.getType())
                .webinarCode(webinar.getWebinarCode())
                .webinarInfo(webinar.getWebinarInfo())
                .webinarStatus(webinar.getWebinarStatus())
                .id(webinar.getId())
                .createdBy(webinar.getCreatedBy())
                .creationTime(webinar.getCreationTime())
                .lastUpdatedBy(webinar.getLastUpdatedBy())
                .lastUpdated(webinar.getLastUpdated())
                .entityState(webinar.getEntityState())
                .contextTags(webinar.getContextTags())
                .build();
    }
}
