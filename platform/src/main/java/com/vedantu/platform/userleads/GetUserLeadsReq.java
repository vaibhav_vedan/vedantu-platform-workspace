/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author ajith
 */
public class GetUserLeadsReq extends AbstractFrontEndListReq {

    private String school;
    private Integer grade;
    private String target;
    private String country;
    private UserLeadParentType parentType;
    private String city;

    public GetUserLeadsReq() {
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public UserLeadParentType getParentType() {
        return parentType;
    }

    public void setParentType(UserLeadParentType parentType) {
        this.parentType = parentType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
