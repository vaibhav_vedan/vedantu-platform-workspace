/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AddMultipleUserLeadsReq extends AbstractFrontEndReq {

    private List<AddUserLeadReq> items;

    public AddMultipleUserLeadsReq() {
    }

    public List<AddUserLeadReq> getItems() {
        return items;
    }

    public void setItems(List<AddUserLeadReq> items) {
        this.items = items;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(items)) {
            errors.add("items");
        } else {
            for (int i = 0; i < items.size(); i++) {
                AddUserLeadReq addUserLeadReq = items.get(i);
                List<String> _errors = addUserLeadReq.collectVerificationErrors();
                _errors.remove(Constants.CALLING_USER_ID);
                if (ArrayUtils.isNotEmpty(_errors)) {
                    errors.add("at item no " + i);
                    errors.addAll(errors);
                    break;
                }
            }
        }
        return errors;
    }
}
