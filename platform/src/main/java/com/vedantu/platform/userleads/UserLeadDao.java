/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.platform.dao.engagement.serializers.MongoClientFactory;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class UserLeadDao extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(UserLeadDao.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public UserLeadDao() {
        super();
    }

    public void save(UserLead p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public UserLead getById(String id) {
        UserLead response = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                response = getEntityById(id, UserLead.class);
            }
        } catch (Exception ex) {
            logger.error(ex);
            response = null;
        }
        return response;
    }

    public void update(UserLead p, Query q, Update u) {
        try {
            if (p != null) {
                updateFirst(q, u, UserLead.class);
            }
        } catch (Exception ex) {
        }
    }

    public boolean parentChildComboExists(String studentName, String studentEmail, String parentEmail) {
        logger.info("studentName " + studentName + ", studentEmail " + studentEmail
                + ", parentEmail " + parentEmail);
        Criteria studentEmailCriteria = null;
        if (StringUtils.isNotEmpty(studentEmail)) {
            studentEmailCriteria = Criteria.where(UserLead.Constants.STUDENT_EMAIL).is(studentEmail);
        }

        Criteria stuNameParentEmailCriteria = null;
        if (StringUtils.isNotEmpty(studentName) && StringUtils.isNotEmpty(parentEmail)) {
            stuNameParentEmailCriteria = new Criteria().andOperator(Criteria.where(UserLead.Constants.STUDENT_NAME)
                    .is(studentName),
                    Criteria.where(UserLead.Constants.PARENT_EMAIL).is(parentEmail));
        }

        Criteria finalCriteria = new Criteria();
        if (studentEmailCriteria != null && stuNameParentEmailCriteria != null) {
            finalCriteria.orOperator(studentEmailCriteria, stuNameParentEmailCriteria);
        } else if (studentEmailCriteria != null && stuNameParentEmailCriteria == null) {
            finalCriteria.orOperator(studentEmailCriteria);
        } else if (studentEmailCriteria == null && stuNameParentEmailCriteria != null) {
            finalCriteria.orOperator(stuNameParentEmailCriteria);
        }

        Query query = new Query(finalCriteria);
        query.limit(1);
        logger.info(query);
        List<UserLead> result = runQuery(query,
                UserLead.class);
        boolean val = ArrayUtils.isNotEmpty(result);
        return val;
    }

    public List<UserLead> get(
            GetUserLeadsReq getUserLeadsReq) {
        try {
            Query query = getQuery(getUserLeadsReq);
            setFetchParameters(query, getUserLeadsReq);
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.CREATION_TIME));
            List<UserLead> result = runQuery(query,
                    UserLead.class);
            return result;
        } catch (Throwable e) {
            throw e;
        }
    }

    public int getTotalCount(
            GetUserLeadsReq getUserLeadsReq) {
        try {
            Query query = getQuery(getUserLeadsReq);
            int result = (int)queryCount(query, UserLead.class);
            return result;
        } catch (Throwable e) {
            throw e;
        }
    }

    private Query getQuery(GetUserLeadsReq getUserLeadsReq) {
        Query query = new Query();
        if (getUserLeadsReq.getCity() != null) {
            query.addCriteria(Criteria.where(
                    UserLead.Constants.CITY).is(
                            getUserLeadsReq.getCity()));
        }
        if (getUserLeadsReq.getCountry() != null) {
            query.addCriteria(Criteria.where(
                    UserLead.Constants.COUNTRY).is(
                            getUserLeadsReq.getCountry()));
        }
        if (getUserLeadsReq.getGrade() != null) {
            query.addCriteria(Criteria.where(
                    UserLead.Constants.GRADE).is(
                            getUserLeadsReq.getGrade()));
        }
        if (getUserLeadsReq.getParentType() != null) {
            query.addCriteria(Criteria.where(
                    UserLead.Constants.PARENT_TYPE).is(
                            getUserLeadsReq.getParentType()));
        }
        if (getUserLeadsReq.getSchool() != null) {
            query.addCriteria(Criteria.where(
                    UserLead.Constants.SCHOOL).is(
                            getUserLeadsReq.getSchool()));
        }
        if (getUserLeadsReq.getTarget() != null) {
            query.addCriteria(Criteria.where(
                    UserLead.Constants.TARGET).is(
                            getUserLeadsReq.getTarget()));
        }        
        return query;
    }
}
