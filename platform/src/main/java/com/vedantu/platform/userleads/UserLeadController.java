/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.util.BasicResponse;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/userleads")
public class UserLeadController {

    @Autowired
    private UserLeadManager userLeadManager;

    @CrossOrigin
    @RequestMapping(value = "/addLeads", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public BasicResponse addUserLeads(
            @RequestBody AddMultipleUserLeadsReq addMultipleUserLeadsReq) throws BadRequestException {
        return userLeadManager.addUserLeads(addMultipleUserLeadsReq);
    }

    @CrossOrigin
    @RequestMapping(value = "/addLead", method = RequestMethod.POST)
    @ResponseBody
    public UserLead addUserLead(AddUserLeadReq addUserLeadReq) throws BadRequestException, IOException, ConflictException {
        UserLead userLead = userLeadManager.addUserLead(addUserLeadReq);
        return userLead;
    }

    @RequestMapping(value = "/addAndRedirect", method = RequestMethod.POST)
    @ResponseBody
    public void addUserLeadAndRedirect(
            AddUserLeadReq addUserLeadReq,
            @RequestParam(value = "redirectUrl", required = true) String redirectUrl,
            HttpServletResponse response) throws BadRequestException, IOException, ConflictException {
        userLeadManager.addUserLead(addUserLeadReq);
        response.sendRedirect(redirectUrl);
    }

    @CrossOrigin
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public GetUserLeadRes getUserLeads(GetUserLeadsReq getUserLeadsReq) {
        return userLeadManager.getUserLeads(getUserLeadsReq);
    }

}
