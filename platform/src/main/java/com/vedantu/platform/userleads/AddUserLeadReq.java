/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.User.UTMParams;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AddUserLeadReq extends AbstractFrontEndReq{

    private String studentName;
    private String studentEmail;
    private String studentPhoneNo;
    private String studentPhoneCode;
    private String school;
    private Integer grade;
    private String target;
    private String country;
    private String parentName;
    private String parentEmail;
    private String parentPhoneNo;
    private String parentPhoneCode;
    private UserLeadParentType parentType;
    private String address;
    private String city;
    private UTMParams utmParams;

    public AddUserLeadReq() {
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentPhoneNo() {
        return parentPhoneNo;
    }

    public void setParentPhoneNo(String parentPhoneNo) {
        this.parentPhoneNo = parentPhoneNo;
    }

    public String getParentPhoneCode() {
        return parentPhoneCode;
    }

    public void setParentPhoneCode(String parentPhoneCode) {
        this.parentPhoneCode = parentPhoneCode;
    }

    public UserLeadParentType getParentType() {
        return parentType;
    }

    public void setParentType(UserLeadParentType parentType) {
        this.parentType = parentType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentPhoneNo() {
        return studentPhoneNo;
    }

    public void setStudentPhoneNo(String studentPhoneNo) {
        this.studentPhoneNo = studentPhoneNo;
    }

    public String getStudentPhoneCode() {
        return studentPhoneCode;
    }

    public void setStudentPhoneCode(String studentPhoneCode) {
        this.studentPhoneCode = studentPhoneCode;
    }

    public UTMParams getUtmParams() {
        return utmParams;
    }

    public void setUtmParams(UTMParams utmParams) {
        this.utmParams = utmParams;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        errors.remove(Constants.CALLING_USER_ID);
        if (StringUtils.isEmpty(studentName)) {
            errors.add("studentName");
        }
        if (grade == null) {
            errors.add("grade");
        }
        if (StringUtils.isEmpty(city)) {
            errors.add("city");
        }
        if (StringUtils.isEmpty(studentEmail) && StringUtils.isEmpty(parentEmail)) {
            errors.add("studentEmail or parentEmail");
        }
        if (StringUtils.isEmpty(studentPhoneNo) && StringUtils.isEmpty(parentPhoneNo)) {
            errors.add("studentPhoneNo or parentPhoneNo");
        }

        return errors;
    }

}
