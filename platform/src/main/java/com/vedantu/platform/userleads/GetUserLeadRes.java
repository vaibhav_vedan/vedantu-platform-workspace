/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.util.fos.response.AbstractListRes;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetUserLeadRes extends AbstractListRes<UserLead> {

    public GetUserLeadRes() {
        super();
    }

    public GetUserLeadRes(List<UserLead> list, int count) {
        super(list, count);
    }

    @Override
    public String toString() {
        return "GetUserLeadRes{" + "}" + super.toString();
    }

}
