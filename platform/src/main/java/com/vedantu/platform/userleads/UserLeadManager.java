/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.LogFactory;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class UserLeadManager {

	@Autowired
	private UserLeadDao userLeadDao;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserLeadManager.class);

        @Autowired
        private DozerBeanMapper mapper; 

	@Autowired
	private AsyncUserLeadManager asyncUserLeadManager;

	public UserLeadManager() {
	}

	public BasicResponse addUserLeads(AddMultipleUserLeadsReq addMultipleUserLeadsReq) throws BadRequestException {
		logger.info("ENTRY " + addMultipleUserLeadsReq);
		addMultipleUserLeadsReq.verify();
		asyncUserLeadManager.addLeads(addMultipleUserLeadsReq);
		logger.info("EXIT ");
		return new BasicResponse();
	}

	public UserLead addUserLead(AddUserLeadReq addUserLeadReq) throws BadRequestException, ConflictException {
		logger.info("ENTRY " + addUserLeadReq);
		addUserLeadReq.verify();
		UserLead userLead = mapper.map(addUserLeadReq, UserLead.class);
		if (userLeadDao.parentChildComboExists(userLead.getStudentName(), userLead.getStudentEmail(),
				userLead.getParentEmail())) {
			throw new ConflictException(ErrorCode.USER_LEAD_ALREADY_ADDED,
					"student email or (student name + parent email) already added");
		}

		userLeadDao.save(userLead);
		// TODO add unique index instead of doing this

		asyncUserLeadManager.sendEmail(userLead);
		logger.info("EXIT " + userLead);
		return userLead;
	}

	public GetUserLeadRes getUserLeads(GetUserLeadsReq getUserLeadsReq) {
		logger.info("ENTRY " + getUserLeadsReq);
		List<UserLead> userLeads = userLeadDao.get(getUserLeadsReq);
		int count = userLeadDao.getTotalCount(getUserLeadsReq);
		GetUserLeadRes res = new GetUserLeadRes(userLeads, count);
		logger.info("EXIT " + res);
		return res;
	}
}
