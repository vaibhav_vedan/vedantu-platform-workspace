/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

/**
 *
 * @author ajith
 */
public class SendISLEmailReq {

    private String studentName;
    private String studentEmail;
    private String studentPhoneNo;
    private String studentPhoneCode;
    private String parentName;
    private String parentEmail;
    private String parentPhoneNo;
    private String parentPhoneCode;

    public SendISLEmailReq() {
    }

    public SendISLEmailReq(UserLead userLead) {
        this.studentName = userLead.getStudentName();
        this.studentEmail = userLead.getStudentEmail();
        this.studentPhoneNo = userLead.getStudentPhoneNo();
        this.studentPhoneCode = userLead.getStudentPhoneCode();
        this.parentName = userLead.getParentName();
        this.parentEmail = userLead.getParentEmail();
        this.parentPhoneNo = userLead.getParentPhoneNo();
        this.parentPhoneCode = userLead.getParentPhoneCode();
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentPhoneNo() {
        return studentPhoneNo;
    }

    public void setStudentPhoneNo(String studentPhoneNo) {
        this.studentPhoneNo = studentPhoneNo;
    }

    public String getStudentPhoneCode() {
        return studentPhoneCode;
    }

    public void setStudentPhoneCode(String studentPhoneCode) {
        this.studentPhoneCode = studentPhoneCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentPhoneNo() {
        return parentPhoneNo;
    }

    public void setParentPhoneNo(String parentPhoneNo) {
        this.parentPhoneNo = parentPhoneNo;
    }

    public String getParentPhoneCode() {
        return parentPhoneCode;
    }

    public void setParentPhoneCode(String parentPhoneCode) {
        this.parentPhoneCode = parentPhoneCode;
    }

    @Override
    public String toString() {
        return "SendISLEmailReq{" + "studentName=" + studentName + ", studentEmail=" + studentEmail + ", studentPhoneNo=" + studentPhoneNo + ", studentPhoneCode=" + studentPhoneCode + ", parentName=" + parentName + ", parentEmail=" + parentEmail + ", parentPhoneNo=" + parentPhoneNo + ", parentPhoneCode=" + parentPhoneCode + '}';
    }

}
