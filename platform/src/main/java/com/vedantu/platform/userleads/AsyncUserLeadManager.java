/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.userleads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.platform.async.AsyncTaskName;
import com.vedantu.util.LogFactory;
import org.dozer.DozerBeanMapper;

/**
 *
 * @author ajith
 */
@Service
public class AsyncUserLeadManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(AsyncUserLeadManager.class);

	@Autowired
	private UserLeadDao userLeadDao;

        @Autowired
        private DozerBeanMapper mapper; 

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	public AsyncUserLeadManager() {
	}

	@Async
	public void addLeads(AddMultipleUserLeadsReq addMultipleUserLeadsReq) {
		logger.info("ENTRY " + addMultipleUserLeadsReq);
		List<UserLead> userLeads = new ArrayList<>();
		for (AddUserLeadReq addUserLeadReq : addMultipleUserLeadsReq.getItems()) {
			UserLead userLead = mapper.map(addUserLeadReq, UserLead.class);
			if (addMultipleUserLeadsReq.getCallingUserId() != null) {
				userLead.setCreatedBy(addMultipleUserLeadsReq.getCallingUserId().toString());
			}
			boolean isNotAllowed = userLeadDao.parentChildComboExists(userLead.getStudentName(),
					userLead.getStudentEmail(), userLead.getParentEmail());
			if (!isNotAllowed) {
				userLeads.add(userLead);
			} else {
				logger.warn("user lead already added " + userLead);
			}
		}
		logger.info("adding userleads " + userLeads);
		userLeadDao.insertAllEntities(userLeads, UserLead.class.getSimpleName());
		sendEmails(userLeads);
	}

	private void sendEmails(List<UserLead> userLeads) {
		logger.info("ENTRY " + userLeads);
		for (UserLead userLead : userLeads) {
			sendEmail(userLead);
		}
	}

	public void sendEmail(UserLead userLead) {
		SendISLEmailReq req = new SendISLEmailReq(userLead);
		Map<String, Object> payload = new HashMap<>();
		payload.put("sendISLEmailReq", req);
		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.USER_LEAD_REGISTRATION_EMAIL, payload);
		asyncTaskFactory.executeTask(params);
	}
}
