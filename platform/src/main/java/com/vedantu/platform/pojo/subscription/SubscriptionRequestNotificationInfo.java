package com.vedantu.platform.pojo.subscription;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.notification.enums.CommunicationType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.subscription.SubscriptionRequestState;
import java.util.List;

public class SubscriptionRequestNotificationInfo {
	private Long studentId;
	private Long teacherId;
	private Long subscriptionRequestId;
	private Long creationTime;
	private String subscriptionName;//
	private String totalHours;//
	private Long totalHoursLong;
	private Long hourlyRate;
	private Integer weeks;
	private Integer hoursPerWeek;//
	private String refundReason;//
	private String expiryDiff;//
	private Double refundAmount;//
	private Double amount;//
	private String dateTime;//
	private String studentName;//
	private String teacherName;//
	private String teacherExtNo;//
	private String studentPhoneNo;//
	private String tutorListLink;//
	private String mySubscriptionsLink;//
	private CommunicationType communicationType;
	private Long subscriptionId;
	private SubscriptionRequestState subscriptionRequestState;
	private PaymentType paymentType;
	private List<InstalmentInfo> instalments;

	@SuppressWarnings("unused")
	private SubscriptionRequestNotificationInfo() {
		super();
	}

	public SubscriptionRequestNotificationInfo(SubscriptionRequestInfo subscriptionRequestInfo) {
		super();
		this.studentId = subscriptionRequestInfo.getStudentId();
		this.teacherId = subscriptionRequestInfo.getTeacherId();
		this.subscriptionRequestId = subscriptionRequestInfo.getId();
		this.creationTime = subscriptionRequestInfo.getCreationTime();
		this.subscriptionName = subscriptionRequestInfo.getTitle();
		this.totalHours = getHourString(subscriptionRequestInfo.getTotalHours());
		this.totalHoursLong = subscriptionRequestInfo.getTotalHours();
		this.hourlyRate = subscriptionRequestInfo.getHourlyRate();
                if(subscriptionRequestInfo.getNoOfWeeks()!=null){
                    this.weeks = subscriptionRequestInfo.getNoOfWeeks().intValue();
                }
		this.refundReason = subscriptionRequestInfo.getReason();
		Long expDiff = subscriptionRequestInfo.getExpiryTime() - subscriptionRequestInfo.getActivatedAt();
		this.expiryDiff = getHourString(expDiff);
		this.refundAmount = (double) subscriptionRequestInfo.getAmount() / 100;
		this.amount = (double) subscriptionRequestInfo.getAmount() / 100;
		this.dateTime = getTimeString(subscriptionRequestInfo.getCreationTime());
                if(this.weeks!=null){
                    this.hoursPerWeek = Long.valueOf((subscriptionRequestInfo.getTotalHours() / DateTimeUtils.MILLIS_PER_HOUR))
                                    .intValue() / this.weeks;                    
                }
		this.subscriptionId = subscriptionRequestInfo.getSubscriptionId();
		this.subscriptionRequestState = subscriptionRequestInfo.getState();
	}

	public void updateDetails(String studentName, String teacherName, String teacherExtNo, String studentPhoneNo,
			String tutorListLink, String mySubscriptionsLink) {
		this.studentName = studentName;
		this.teacherName = teacherName;
		this.teacherExtNo = teacherExtNo;
		this.studentPhoneNo = studentPhoneNo;
		this.tutorListLink = tutorListLink;
		this.mySubscriptionsLink = mySubscriptionsLink;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getSubscriptionRequestId() {
		return subscriptionRequestId;
	}

	public void setSubscriptionRequestId(Long subscriptionRequestId) {
		this.subscriptionRequestId = subscriptionRequestId;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public String getSubscriptionName() {
		return subscriptionName;
	}

	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Integer getWeeks() {
		return weeks;
	}

	public void setWeeks(Integer weeks) {
		this.weeks = weeks;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTeacherExtNo() {
		return teacherExtNo;
	}

	public void setTeacherExtNo(String teacherExtNo) {
		this.teacherExtNo = teacherExtNo;
	}

	public String getStudentPhoneNo() {
		return studentPhoneNo;
	}

	public void setStudentPhoneNo(String studentPhoneNo) {
		this.studentPhoneNo = studentPhoneNo;
	}

	public String getTutorListLink() {
		return tutorListLink;
	}

	public void setTutorListLink(String tutorListLink) {
		this.tutorListLink = tutorListLink;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getMySubscriptionsLink() {
		return mySubscriptionsLink;
	}

	public void setMySubscriptionsLink(String mySubscriptionsLink) {
		this.mySubscriptionsLink = mySubscriptionsLink;
	}

	public CommunicationType getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(CommunicationType communicationType) {
		this.communicationType = communicationType;
	}

	public String getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(String totalHours) {
		this.totalHours = totalHours;
	}

	public String getExpiryDiff() {
		return expiryDiff;
	}

	public void setExpiryDiff(String expiryDiff) {
		this.expiryDiff = expiryDiff;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getHoursPerWeek() {
		return hoursPerWeek;
	}

	public void setHoursPerWeek(Integer hoursPerWeek) {
		this.hoursPerWeek = hoursPerWeek;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public SubscriptionRequestState getSubscriptionRequestState() {
		return subscriptionRequestState;
	}

	public void setSubscriptionRequestState(SubscriptionRequestState subscriptionRequestState) {
		this.subscriptionRequestState = subscriptionRequestState;
	}

	public String getHourString(Long milis) {
		long t = milis;
		long h = t / DateTimeUtils.MILLIS_PER_HOUR;
		long m = ((t % DateTimeUtils.MILLIS_PER_HOUR) / DateTimeUtils.MILLIS_PER_MINUTE);
		String hours = "";
		hours += h == 0 ? "" : h + " hours";
		hours += m == 0 ? "" : ":" + m + " mins";
		return hours;
	}

	public String getTimeString(Long t) {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		String time = sdf.format(new Date(t));
		return time;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public List<InstalmentInfo> getInstalments() {
		return instalments;
	}

	public void setInstalments(List<InstalmentInfo> instalments) {
		this.instalments = instalments;
	}

	public Long getTotalHoursLong() {
		return totalHoursLong;
	}

	public void setTotalHoursLong(Long totalHoursLong) {
		this.totalHoursLong = totalHoursLong;
	}

	@Override
	public String toString() {
		return "SubscriptionRequestNotificationInfo [studentId=" + studentId + ", teacherId=" + teacherId
				+ ", subscriptionRequestId=" + subscriptionRequestId + ", creationTime=" + creationTime
				+ ", subscriptionName=" + subscriptionName + ", totalHours=" + totalHours + ", totalHoursLong="
				+ totalHoursLong + ", hourlyRate=" + hourlyRate + ", weeks=" + weeks + ", hoursPerWeek=" + hoursPerWeek
				+ ", refundReason=" + refundReason + ", expiryDiff=" + expiryDiff + ", refundAmount=" + refundAmount
				+ ", amount=" + amount + ", dateTime=" + dateTime + ", studentName=" + studentName + ", teacherName="
				+ teacherName + ", teacherExtNo=" + teacherExtNo + ", studentPhoneNo=" + studentPhoneNo
				+ ", tutorListLink=" + tutorListLink + ", mySubscriptionsLink=" + mySubscriptionsLink
				+ ", communicationType=" + communicationType + ", subscriptionId=" + subscriptionId
				+ ", subscriptionRequestState=" + subscriptionRequestState + ", paymentType=" + paymentType
				+ ", instalments=" + instalments + "]";
	}
}
