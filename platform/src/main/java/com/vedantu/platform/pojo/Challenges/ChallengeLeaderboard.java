package com.vedantu.platform.pojo.Challenges;

import com.vedantu.platform.enums.ChallengerGradeType.ChallengerGradeType;

public class ChallengeLeaderboard {
	
	private String name;
	private String email;
	private Long startTime;
	private ChallengerGradeType challengerGradeType;
	private String grade;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public ChallengerGradeType getChallengerGradeType() {
		return challengerGradeType;
	}

	public void setChallengerGradeType(ChallengerGradeType challengerGradeType) {
		this.challengerGradeType = challengerGradeType;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

}
