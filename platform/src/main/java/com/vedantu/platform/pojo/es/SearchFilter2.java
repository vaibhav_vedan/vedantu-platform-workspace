package com.vedantu.platform.pojo.es;


public class SearchFilter2 {

    private String filterName;
    private String filterType;
    private String filterValueType;
    private String[] filterParams;

    public SearchFilter2() {
    }

    public SearchFilter2(String filterName, String filterType,String filterValueType, String[] filterParams) {
        this.filterName = filterName;
        this.filterType = filterType;
        this.filterValueType = filterValueType;
        this.filterParams = filterParams;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String getFilterValueType() {
		return filterValueType;
	}

	public void setFilterValueType(String filterValueType) {
		this.filterValueType = filterValueType;
	}

	public String[] getfilterParams() {
        return filterParams;
    }

    public void setfilterParams(String[] filterParams) {
        this.filterParams = filterParams;
    }

}
