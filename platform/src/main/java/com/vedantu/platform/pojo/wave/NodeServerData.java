/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.wave;

import java.io.Serializable;

/**
 *
 * @author ajith
 */
public class NodeServerData implements Serializable {

    private String serverIp;
    private String serverPort;
    private Float currentCpuUsage;
    private Float currentMemUsage;
    private Integer connectedSockets;
    private Long timestampInSecs;

    public NodeServerData(String serverIp, String serverPort,
            Float currentCpuUsage, Float currentMemUsage, Integer connectedSockets,
            Long timestampInSecs) {
        this.serverIp = serverIp;
        this.serverPort = serverPort;
        this.currentCpuUsage = currentCpuUsage;
        this.currentMemUsage = currentMemUsage;
        this.connectedSockets = connectedSockets;
        this.timestampInSecs = timestampInSecs;
    }

    public NodeServerData() {

    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public Float getCurrentCpuUsage() {
        return currentCpuUsage;
    }

    public void setCurrentCpuUsage(Float currentCpuUsage) {
        this.currentCpuUsage = currentCpuUsage;
    }

    public Float getCurrentMemUsage() {
        return currentMemUsage;
    }

    public void setCurrentMemUsage(Float currentMemUsage) {
        this.currentMemUsage = currentMemUsage;
    }

    public Integer getConnectedSockets() {
        return connectedSockets;
    }

    public void setConnectedSockets(Integer connectedSockets) {
        this.connectedSockets = connectedSockets;
    }

    public Long getTimestampInSecs() {
        return timestampInSecs;
    }

    public void setTimestampInSecs(Long timestampInSecs) {
        this.timestampInSecs = timestampInSecs;
    }

    @Override
    public String toString() {
        return "NodeServerData{" + "serverIp=" + serverIp + ", serverPort=" + serverPort + ", currentCpuUsage=" + currentCpuUsage + ", currentMemUsage=" + currentMemUsage + ", connectedSockets=" + connectedSockets + ", timestampInSecs=" + timestampInSecs + '}';
    }

}
