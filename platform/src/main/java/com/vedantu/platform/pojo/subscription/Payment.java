package com.vedantu.platform.pojo.subscription;

public class Payment extends AbstractSqlEntity {
	private Long subscriptionDetailsId;
	private String couponId;
	private Long discountAmount;
	private String transactionId;
	private Long amountPaid;

	public Payment() {
		super();
	}

	public Payment(Long subscriptionDetailsId, String couponId, Long discountAmount, String transactionId,
			Long amountPaid) {
		super();
		this.subscriptionDetailsId = subscriptionDetailsId;
		this.couponId = couponId;
		this.discountAmount = discountAmount;
		this.transactionId = transactionId;
		this.amountPaid = amountPaid;
	}

	public Long getSubscriptionDetailsId() {
		return subscriptionDetailsId;
	}

	public void setSubscriptionDetailsId(Long subscriptionDetailsId) {
		this.subscriptionDetailsId = subscriptionDetailsId;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public Long getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Long discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Long getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Long amountPaid) {
		this.amountPaid = amountPaid;
	}

	@Override
	public String toString() {
		return "SessionPayout [subscriptionDetailsId=" + subscriptionDetailsId + ", couponId=" + couponId + ", discountAmount="
				+ discountAmount + ",transactionId =" + transactionId + ", amountPaid=" + amountPaid
				+  "]";
	}

	public static class Constants extends AbstractSqlEntity.Constants {
		public static final String SUBSCRIPTIONDETAILSID = "subscriptionDetailsId";
		public static final String COUPONSID = "couponId";
		public static final String DISCOUNTAMOUNT = "discountAmount";
		public static final String TRANSACTIONID = "transactionId";
		public static final String AMOUNTPAID = "amountPaid";
		public static final String DATE = "date";
	}
}
