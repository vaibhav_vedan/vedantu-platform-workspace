
package com.vedantu.platform.pojo.es;


public class SearchSortReq {
	private String sortField;
	private String order;
	
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	
	@Override
	public String toString() {
		return "SearchSortReq [sortField=" + sortField + ", order=" + order
				+ "]";
	}
}
