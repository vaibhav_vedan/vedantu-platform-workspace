/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.user;

import com.vedantu.User.UTMParams;
import com.vedantu.User.request.SocialSource;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 *
 * @author somil
 */
public class AppSocialLoginReq extends AbstractFrontEndReq {

    private String code;
    private SocialSource source;
    @Valid
    private UTMParams utm = new UTMParams();
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String signUpURL;
    private String referrer;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SocialSource getSource() {
        return source;
    }

    public void setSource(SocialSource source) {
        this.source = source;
    }

    public UTMParams getUtm() {
        return utm;
    }

    public void setUtm(UTMParams utm) {
        this.utm = utm;
    }

    public String getSignUpURL() {
        return signUpURL;
    }

    public void setSignUpURL(String signUpURL) {
        this.signUpURL = signUpURL;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

}
