package com.vedantu.platform.pojo.scheduling;

public class ElasticSearchQueryFormatter {

	public static ElasticSearchQueryFormatter INSTANCE = new ElasticSearchQueryFormatter();

	private ElasticSearchQueryFormatter() {

	}

	public String getTermFilter(String filterName, String filterValue) {
		String termFilter = "{\"term\":{\"%s\":\"%s\"}}";
		if (filterValue == null || filterValue.equals("")) {
			return null;
		}
		return String.format(termFilter, filterName, filterValue);
	}

	public String getRangeFilter(String filterName, String lteFilterValue, String gteFilterValue) {

		String rangeFilter = "{\"range\":{\"%s\":{%s}}}";
		String filterValue = "";
		String lteFilter = "\"lte\":" + lteFilterValue;
		String gteFilter = "\"gte\":" + gteFilterValue;
		if (lteFilter != null && lteFilter != "") {
			filterValue = lteFilter;
		}
		if (gteFilter != null && gteFilter != "") {
			if (filterValue != "") {
				filterValue = lteFilter + "," + gteFilter;
			} else {
				filterValue = gteFilter;
			}

		}
		return String.format(rangeFilter, filterName, filterValue);
	}

	public String getSortFilter(String filterName, String order) {
		String sortFilter = "{\"%s\":{\"order\":\"%s\"}}";
		if (order == null || order.equals("")) {
			return null;
		}
		return String.format(sortFilter, filterName, order);
	}

	public String getHasChildFilter() {
		return null;
	}
}