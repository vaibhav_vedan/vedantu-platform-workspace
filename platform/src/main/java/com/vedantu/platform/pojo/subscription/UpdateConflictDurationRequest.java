package com.vedantu.platform.pojo.subscription;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UpdateConflictDurationRequest extends AbstractFrontEndReq{

	private Long subscriptionId;
	private Long previousDuration;
	private Long newDuration;
	private Long sessionId;
	private EntityType contextType;
	private String contextId;

	public UpdateConflictDurationRequest() {
		super();
	}

	public UpdateConflictDurationRequest(Long subscriptionId, Long previousDuration, Long newDuration, Long sessionId) {
		super();
		this.subscriptionId = subscriptionId;
		this.previousDuration = previousDuration;
		this.newDuration = newDuration;
		this.sessionId = sessionId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getPreviousDuration() {
		return previousDuration;
	}

	public void setPreviousDuration(Long previousDuration) {
		this.previousDuration = previousDuration;
	}

	public Long getNewDuration() {
		return newDuration;
	}

	public void setNewDuration(Long newDuration) {
		this.newDuration = newDuration;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
}
