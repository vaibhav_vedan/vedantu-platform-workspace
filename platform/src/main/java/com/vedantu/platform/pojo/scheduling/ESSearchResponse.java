package com.vedantu.platform.pojo.scheduling;
import java.util.List;


public class ESSearchResponse {

	private Integer totalHits;
	private List<EsTeacherInfo> teacherData;
	public Integer getTotalHits() {
		return totalHits;
	}
	public void setTotalHits(Integer totalHits) {
		this.totalHits = totalHits;
	}
	public List<EsTeacherInfo> getTeacherData() {
		return teacherData;
	}
	public void setTeacherData(List<EsTeacherInfo> teacherData) {
		this.teacherData = teacherData;
	}
	
}
