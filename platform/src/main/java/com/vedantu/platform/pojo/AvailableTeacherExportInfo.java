package com.vedantu.platform.pojo;

import com.vedantu.platform.mongodbentities.board.Board;
import com.vedantu.platform.pojo.scheduling.TeacherAvailabilityData;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by somil on 25/01/17.
 */
public class AvailableTeacherExportInfo {
    public static final String AVAILABLE_STRING = "AVAILABLE";
    public static final String AVAILABLE_BOOKED = "AVAILABLE_BOOKED";
    public static final String UNAVAILABLE_STRING = "UNAVAILABLE";

    private String teacherId;
    private String emailId;
    private String startPrice;
    private Long sessionHours;
    private Long sessions;
    private String experience;
    private String latestEducation;
    private Float rating;

    private String primarySubject;
    private String secondarySubject;
    private String grades;
    private List<String> categories;

    private String fullName;
    private List<String> availability = new ArrayList<String>();

    public AvailableTeacherExportInfo() {
        super();
    }



    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<String> getAvailability() {
        return availability;
    }

    public void setAvailability(List<String> availability) {
        this.availability = availability;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPrimarySubject() {
        return primarySubject;
    }

    public void setPrimarySubject(String primarySubject) {
        this.primarySubject = primarySubject;
    }

    public String getSecondarySubject() {
        return secondarySubject;
    }

    public void setSecondarySubject(String secondarySubject) {
        this.secondarySubject = secondarySubject;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(String startPrice) {
        this.startPrice = startPrice;
    }

    public String getGrades() {
        return grades;
    }

    public void setGrades(String grades) {
        this.grades = grades;
    }

    public Long getSessionHours() {
        return sessionHours;
    }

    public void setSessionHours(Long sessionHours) {
        this.sessionHours = sessionHours;
    }

    public Long getSessions() {
        return sessions;
    }

    public void setSessions(Long sessions) {
        this.sessions = sessions;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getLatestEducation() {
        return latestEducation;
    }

    public void setLatestEducation(String latestEducation) {
        this.latestEducation = latestEducation;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public void updateAvailability(int index) {
        this.availability.set(index, AVAILABLE_STRING);
    }

    public void updateUnavailability(int index) {
        this.availability.set(index, UNAVAILABLE_STRING);
    }

    public void initAvailabilityList(int size) {
        this.availability = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            this.availability.add(UNAVAILABLE_STRING);
        }
    }

    public boolean isAvailable(int index) {
        if (AVAILABLE_STRING.equals(this.availability.get(index))) {
            return true;
        } else {
            return false;
        }
    }

    public String fetchAvailableString(int index) {
        if (!CollectionUtils.isEmpty(this.availability) && this.availability.size() > index) {
            return this.availability.get(index);
        } else {
            return AvailableTeacherExportInfo.UNAVAILABLE_STRING;
        }
    }

    public static String removeCommas(String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        } else {
            return value.replace(",", ":");
        }
    }

    @Override
    public String toString() {
        return "AvailableTeacherExportInfo [teacherId=" + teacherId + ", emailId=" + emailId + ", startPrice="
                + startPrice + ", sessionHours=" + sessionHours + ", sessions=" + sessions + ", experience="
                + experience + ", latestEducation=" + latestEducation + ", rating=" + rating + ", primarySubject="
                + primarySubject + ", secondarySubject=" + secondarySubject + ", grades=" + grades + ", categories="
                + categories + ", fullName=" + fullName + ", availability=" + availability + "]";
    }

}
