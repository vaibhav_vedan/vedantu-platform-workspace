package com.vedantu.platform.pojo.requestcallback.request;

public class ESPopulatorRequest {

	private String param;
	private Long userId;
	private Long increment;
	private IncrementType incrementType;
	public ESPopulatorRequest(String param, Long userId,Long increment,IncrementType incrementType) {
		super();
		this.param = param;
		this.userId = userId;
		this.increment=increment;
		this.incrementType=incrementType;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getIncrement() {
		return increment;
	}
	public void setIncrement(Long increment) {
		this.increment = increment;
	}
	public IncrementType getIncrementType() {
		return incrementType;
	}
	public void setIncrementType(IncrementType incrementType) {
		this.incrementType = incrementType;
	}
	
	
}
