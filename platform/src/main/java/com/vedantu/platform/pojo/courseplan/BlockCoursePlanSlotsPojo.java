package com.vedantu.platform.pojo.courseplan;

import com.vedantu.session.pojo.SessionSchedule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mano
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlockCoursePlanSlotsPojo {
    private SessionSchedule sessionSchedule;
    private Long studentId;
    private Long teacherId;
    private String coursePlanId;
}
