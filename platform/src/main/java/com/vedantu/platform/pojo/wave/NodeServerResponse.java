/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.wave;

/**
 *
 * @author ajith
 */
public class NodeServerResponse {

    private String socketServerIp;
    private String socketServerPort;
    private String dataSourceServerIp;
    private String dataSourceServerPort;

    public NodeServerResponse(String socketServerIp, String socketServerPort, String dataSourceServerIp, String dataSourceServerPort) {
        this.socketServerIp = socketServerIp;
        this.socketServerPort = socketServerPort;
        this.dataSourceServerIp = dataSourceServerIp;
        this.dataSourceServerPort = dataSourceServerPort;
    }

    public NodeServerResponse() {
        super();
    }


    public String getSocketServerIp() {
        return socketServerIp;
    }

    public void setSocketServerIp(String socketServerIp) {
        this.socketServerIp = socketServerIp;
    }

    public String getSocketServerPort() {
        return socketServerPort;
    }

    public void setSocketServerPort(String socketServerPort) {
        this.socketServerPort = socketServerPort;
    }

    public String getDataSourceServerIp() {
        return dataSourceServerIp;
    }

    public void setDataSourceServerIp(String dataSourceServerIp) {
        this.dataSourceServerIp = dataSourceServerIp;
    }

    public String getDataSourceServerPort() {
        return dataSourceServerPort;
    }

    public void setDataSourceServerPort(String dataSourceServerPort) {
        this.dataSourceServerPort = dataSourceServerPort;
    }

    @Override
    public String toString() {
        return "NodeServerResponse{" + "socketServerIp=" + socketServerIp + ", socketServerPort=" + socketServerPort + ", dataSourceServerIp=" + dataSourceServerIp + ", dataSourceServerPort=" + dataSourceServerPort + '}';
    }

}
