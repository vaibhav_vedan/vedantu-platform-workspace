/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import com.vedantu.platform.enums.feedback.RuleSetType;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

/**
 *
 * @author parashar
 */
public class ParentFormContextSharedInfo {
    
    private String contextId;
    private Integer numOfShare = 0;
    private EntityState state;
    private RuleSetType ruleSetType;
    private Integer ruleSetValue;
    private Long validTill;
    private Long expiresIn;

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the numOfShare
     */
    public Integer getNumOfShare() {
        return numOfShare;
    }

    /**
     * @param numOfShare the numOfShare to set
     */
    public void setNumOfShare(Integer numOfShare) {
        this.numOfShare = numOfShare;
    }

    /**
     * @return the state
     */
    public EntityState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(EntityState state) {
        this.state = state;
    }

    /**
     * @return the ruleSetType
     */
    public RuleSetType getRuleSetType() {
        return ruleSetType;
    }

    /**
     * @param ruleSetType the ruleSetType to set
     */
    public void setRuleSetType(RuleSetType ruleSetType) {
        this.ruleSetType = ruleSetType;
    }

    /**
     * @return the ruleSetValue
     */
    public Integer getRuleSetValue() {
        return ruleSetValue;
    }

    /**
     * @param ruleSetValue the ruleSetValue to set
     */
    public void setRuleSetValue(Integer ruleSetValue) {
        this.ruleSetValue = ruleSetValue;
    }

    /**
     * @return the validTill
     */
    public Long getValidTill() {
        return validTill;
    }

    /**
     * @param validTill the validTill to set
     */
    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    /**
     * @return the expiresIn
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * @param expiresIn the expiresIn to set
     */
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }
    
}
