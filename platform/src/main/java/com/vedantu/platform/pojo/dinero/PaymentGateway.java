package com.vedantu.platform.pojo.dinero;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class PaymentGateway extends AbstractMongoStringIdEntityBean {

	private String name;
	// amount in paisa
	private Boolean enabled;

	// amount in paisa
	private Integer percentageTraffic;

	public PaymentGateway(PaymentGatewayName name, Boolean enabled,
			Integer percentageTraffic) {
		this.name = name.toString();
		this.enabled = enabled;
		this.percentageTraffic = percentageTraffic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getPercentageTraffic() {
		return percentageTraffic;
	}

	public void setPercentageTraffic(Integer percentageTraffic) {
		this.percentageTraffic = percentageTraffic;
	}

	@Override
	public String toString() {
		return "Wallet [name=" + name + ", enabled=" + enabled
				+ ", percentageTraffic=" + percentageTraffic + "]";
	}

	public static class Constants extends AbstractMongoEntityBean.Constants {

		public static final String NAME = "name";
		public static final String ENABLED = "enabled";
		public static final String PERCENTAGE_TRAFFIC = "percentageTraffic";
	}
}
