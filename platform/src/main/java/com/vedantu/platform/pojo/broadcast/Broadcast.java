package com.vedantu.platform.pojo.broadcast;

import com.vedantu.platform.enums.broadcast.BroadcastStatus;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Broadcast extends AbstractMongoStringIdEntity{

	private BroadcastType broadcastType;
	private Integer level;
	private Long lastBroadcastTime;
	private BroadcastStatus status;
	private Long toUserId;
	private String referenceId;
	
	public Broadcast() {
		super();
	}
	
	
	public Broadcast(Long creationTime, String createdBy, Long lastUpdated,
			String lastUpdatedBy) {
		super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
	}
	
	public Broadcast(BroadcastType broadcastType, Integer level,
			Long lastBroadcastTime, BroadcastStatus status, Long toUserId,
			String referenceId) {
		super();
		this.broadcastType = broadcastType;
		this.level = level;
		this.lastBroadcastTime = lastBroadcastTime;
		this.status = status;
		this.toUserId = toUserId;
		this.referenceId = referenceId;
	}

	public BroadcastType getBroadcastType() {
		return broadcastType;
	}

	public void setBroadcastType(BroadcastType broadcastType) {
		this.broadcastType = broadcastType;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getLastBroadcastTime() {
		return lastBroadcastTime;
	}

	public void setLastBroadcastTime(Long lastBroadcastTime) {
		this.lastBroadcastTime = lastBroadcastTime;
	}

	public BroadcastStatus getStatus() {
		return status;
	}

	public void setStatus(BroadcastStatus status) {
		this.status = status;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "Broadcast [broadcastType=" + broadcastType + ", level=" + level
				+ ", lastBroadcastTime=" + lastBroadcastTime + ", status="
				+ status + ", toUserId=" + toUserId + ", referenceId="
				+ referenceId + "]";
	}
	
	public static class Constants {
		public static final String BROADCAST_TYPE = "broadcastType";
		public static final String LEVEL = "level";
		public static final String LAST_BROADCAST_TIME = "lastBroadcastTime";
		public static final String STATUS = "status";
		public static final String TO_USER_ID = "toUserId";
		public static final String REFERENCE_ID = "referenceId";
	}
	
}
