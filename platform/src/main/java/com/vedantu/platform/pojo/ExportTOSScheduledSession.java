package com.vedantu.platform.pojo;

/**
 * Created by somil on 24/01/17.
 */
public class ExportTOSScheduledSession {
    public String teacherEmail;
    public String studentEmail;
    public String studentId;
    public String teacherId;
    public String sessionId;
    public String startTime;
    public Boolean isTOS;
    public String firstSessionState;
    public String firstSessionBillingPeriod;
    public String firstSessionId;

    public ExportTOSScheduledSession() {
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Boolean getIsTOS() {
        return isTOS;
    }

    public void setIsTOS(Boolean isTOS) {
        this.isTOS = isTOS;
    }

    public String getFirstSessionState() {
        return firstSessionState;
    }

    public void setFirstSessionState(String firstSessionState) {
        this.firstSessionState = firstSessionState;
    }

    public String getFirstSessionBillingPeriod() {
        return firstSessionBillingPeriod;
    }

    public void setFirstSessionBillingPeriod(String firstSessionBillingPeriod) {
        this.firstSessionBillingPeriod = firstSessionBillingPeriod;
    }

    public String getFirstSessionId() {
        return firstSessionId;
    }

    public void setFirstSessionId(String firstSessionId) {
        this.firstSessionId = firstSessionId;
    }

    @Override
    public String toString() {
        return "ExportTOSScheduledSession{" + "teacherEmail=" + teacherEmail + ", studentEmail=" + studentEmail + ", studentId=" + studentId + ", teacherId=" + teacherId + ", sessionId=" + sessionId + ", startTime=" + startTime + ", isTOS=" + isTOS + ", firstSessionState=" + firstSessionState + ", firstSessionBillingPeriod=" + firstSessionBillingPeriod + ", firstSessionId=" + firstSessionId + '}';
    }
}

