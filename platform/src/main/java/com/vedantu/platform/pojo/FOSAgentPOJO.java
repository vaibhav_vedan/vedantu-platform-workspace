package com.vedantu.platform.pojo;

import com.vedantu.dinero.enums.Centre;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FOSAgentPOJO {
    private String agentCode;
    private String agentName;
    private String agentEmailId;
    private long DOJ_EDOJ;
    private Centre location;
    private long dateOfResignation;
    private String reportingTeamLeadId;
    private String reportingManagerId;
    private String status;
    private String designation;
    private String function;
    private String subFunction;
    private String teamLeader;
    private String centreHead;
    private String centreHeadCode;
    private String teacherAccount;
    private String regionalManager;
    private String regionalManagerCode;

}
