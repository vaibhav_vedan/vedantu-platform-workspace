/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import com.vedantu.platform.enums.feedback.FeedbackFormType;

/**
 *
 * @author parashar
 */
public class FormSharedInContextTypeInfo {
    
    private FeedbackFormType feedbackFormType;
    private String formName;
    private String formId; // not parent form id
    private Long sharedOn;
    private Integer numOfResponses;

    /**
     * @return the feedbackFormType
     */
    public FeedbackFormType getFeedbackFormType() {
        return feedbackFormType;
    }

    /**
     * @param feedbackFormType the feedbackFormType to set
     */
    public void setFeedbackFormType(FeedbackFormType feedbackFormType) {
        this.feedbackFormType = feedbackFormType;
    }

    /**
     * @return the formName
     */
    public String getFormName() {
        return formName;
    }

    /**
     * @param formName the formName to set
     */
    public void setFormName(String formName) {
        this.formName = formName;
    }

    /**
     * @return the formId
     */
    public String getFormId() {
        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * @return the sharedOn
     */
    public Long getSharedOn() {
        return sharedOn;
    }

    /**
     * @param sharedOn the sharedOn to set
     */
    public void setSharedOn(Long sharedOn) {
        this.sharedOn = sharedOn;
    }

    /**
     * @return the numOfResponses
     */
    public Integer getNumOfResponses() {
        return numOfResponses;
    }

    /**
     * @param numOfResponses the numOfResponses to set
     */
    public void setNumOfResponses(Integer numOfResponses) {
        this.numOfResponses = numOfResponses;
    }
    
    
    
}
