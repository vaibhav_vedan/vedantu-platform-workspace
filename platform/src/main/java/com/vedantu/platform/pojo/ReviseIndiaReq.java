package com.vedantu.platform.pojo;

import com.vedantu.platform.pojo.leadsquared.SchemaValuePair;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class ReviseIndiaReq {

    private String emailAddress;
    private String phone;
    private String userId;
    private List<SchemaValuePair> fields = new ArrayList<>();
}
