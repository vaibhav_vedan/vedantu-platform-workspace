/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.leadsquared;

import com.google.gson.annotations.SerializedName;
import com.vedantu.platform.utils.LeadSquaredUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class LeadActivityReq {

    @SerializedName("RelatedProspectId")
    private String relatedProspectId;

    @SerializedName("EmailAddress")
    private String emailAddress;

    @SerializedName("ActivityEvent")
    private int activityEvent;

    @SerializedName("ActivityNote")
    private String activityNote;

    @SerializedName("ActivityTime")
    private String activityTime;

    @SerializedName("SearchByValue")
    private String searchByValue;

    @SerializedName("SearchBy")
    private String searchBy;
    
    private String userId;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("Fields")
    private List<SchemaValuePair> fields = new ArrayList<>();

    public LeadActivityReq() {
        super();
    }

    public LeadActivityReq(String emailAddress, int activityEvent, String activityNote) {
        super();
        this.emailAddress = emailAddress;
        this.activityEvent = activityEvent;
        this.activityNote = activityNote;
    }

    public String getRelatedProspectId() {
        return relatedProspectId;
    }

    public void setRelatedProspectId(String relatedProspectId) {
        this.relatedProspectId = relatedProspectId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getActivityEvent() {
        return activityEvent;
    }

    public void setActivityEvent(int activityEvent) {
        this.activityEvent = activityEvent;
    }

    public String getActivityNote() {
        return activityNote;
    }

    public void setActivityNote(String activityNote) {
        this.activityNote = activityNote;
    }

    public List<SchemaValuePair> getFields() {
        return fields;
    }

    public void setFields(List<SchemaValuePair> fields) {
        this.fields = fields;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getSearchByValue() {
        return searchByValue;
    }

    public void setSearchByValue(String searchByValue) {
        this.searchByValue = searchByValue;
    }

    public void addFieldPair(String key, String value) {
        this.fields.add(new SchemaValuePair(key, value));
    }

    public void addActivityTime(Long activityTime) {
        this.activityTime = LeadSquaredUtils.getLeadSquaredDate(activityTime);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    @Override
    public String toString() {
        return "LeadActivityReq{" + "emailAddress=" + emailAddress + ", activityEvent=" + activityEvent + ", activityNote=" + activityNote + ", activityTime=" + activityTime + ", searchByValue=" + searchByValue + ", fields=" + fields + '}';
    }

    

}
