package com.vedantu.platform.pojo.subscription;

import com.vedantu.util.subscription.SubscriptionRequestState;

public class SubscriptionRequestBasicInfo {
	
	private Long id;
	private String studentName;
	private String title;
	private Long startDate;
	private Long expiryTime;
	private Long amount;
	private SubscriptionRequestState state;

	public SubscriptionRequestBasicInfo() {
		super();
	}
	
	public SubscriptionRequestBasicInfo(SubscriptionRequest req) {
		super();
		this.id = req.getId();
		this.title = req.getTitle();
		this.startDate = req.getStartDate();
		this.expiryTime = req.getExpiryTime();
		this.amount = req.getAmount();
		this.state = req.getState();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public SubscriptionRequestState getState() {
		return state;
	}

	public void setState(SubscriptionRequestState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "SubscriptionRequestBasicInfo [id=" + id + ", studentName=" + studentName + ", title=" + title
				+ ", startDate=" + startDate + ", expiryTime=" + expiryTime + ", amount=" + amount + ", state=" + state
				+ "]";
	}
}
