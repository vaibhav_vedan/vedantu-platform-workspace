/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.cms;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "WebinarUserRegistrationToken")
@CompoundIndexes({
    @CompoundIndex(name = "phone_webinarid", def = "{'phone' : 1, 'webinarId': 1}", background = true)
})
public class WebinarUserRegistrationToken extends AbstractMongoStringIdEntity {
    public static final int VERIFICATION_CODE_LENGTH = 4;
    private String verificationCode;
    @Indexed(background = true)
    private String phone;
    private String webinarId;

    public WebinarUserRegistrationToken(String verificationCode, String phone, String id) {
        this.verificationCode = verificationCode;
        this.phone = phone;
        this.webinarId = id;
    }

    public WebinarUserRegistrationToken() {
    }
    
    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }
    
    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String PHONE = "phone";
        public static final String WEBINARID = "webinarId";
    }
}
