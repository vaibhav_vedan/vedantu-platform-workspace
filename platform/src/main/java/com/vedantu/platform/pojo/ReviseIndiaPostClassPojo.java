package com.vedantu.platform.pojo;

import lombok.Data;

@Data
public class ReviseIndiaPostClassPojo {
    private Integer doubts;
    private String userId;
}
