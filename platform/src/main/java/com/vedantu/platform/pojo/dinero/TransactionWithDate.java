package com.vedantu.platform.pojo.dinero;

import com.vedantu.dinero.enums.TransactionRefType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;

public class TransactionWithDate {

	private String id;
	private float amount;// amount in paisa
	private String currencyCode;
	private String debitFromAccount;
	private String creditToAccount;
	private String reasonType;
	private String reasonRefNo;

	private TransactionRefType reasonRefType;

	private String reasonNote;

	private String triggredBy;

	private String creationTime;

	public TransactionWithDate() {

		super();
	}

	public TransactionWithDate(String id, float amount, String currencyCode, String debitFromAccount,
			String creditToAccount, String reasonType, String reasonRefNo, TransactionRefType reasonRefType,
			String reasonNote, String triggredBy, String creationTime) {
		super();
		this.id = id;
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.debitFromAccount = debitFromAccount;
		this.creditToAccount = creditToAccount;
		this.reasonType = reasonType;
		this.reasonRefNo = reasonRefNo;
		this.reasonRefType = reasonRefType;
		this.reasonNote = reasonNote;
		this.triggredBy = triggredBy;
		this.creationTime = creationTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public TransactionWithDate(Transaction transaction){
		this.id = transaction.getId();
		this.amount = (float) ((float)transaction.getAmount()/ (float)100);
		this.currencyCode = transaction.getCurrencyCode();
		this.debitFromAccount = transaction.getDebitFromAccount();
		this.creditToAccount = transaction.getCreditToAccount();
		this.reasonType = transaction.getReasonType();
		this.reasonRefNo = transaction.getReasonRefNo();
		this.reasonRefType = transaction.getReasonRefType();
		this.reasonNote = transaction.getReasonNote();
		this.triggredBy = transaction.getTriggredBy();
		Date date = new Date(transaction.getCreationTime());
		date = DateUtils.addHours(date, 5);
		date = DateUtils.addMinutes(date, 30);
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		this.creationTime = formatter.format(date);
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public static String _getTriggredByUser(Long userId) {

		return "user/" + userId;
	}

	public static String _getTriggredBySystem() {

		return "system";
	}

	public float getAmount() {

		return amount;
	}

	public void setAmount(float amount) {

		this.amount = amount;
	}

	public String getCurrencyCode() {

		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {

		this.currencyCode = currencyCode;
	}

	public String getDebitFromAccount() {

		return debitFromAccount;
	}

	public void setDebitFromAccount(String debitFromAccount) {

		this.debitFromAccount = debitFromAccount;
	}

	public String getCreditToAccount() {

		return creditToAccount;
	}

	public void setCreditToAccount(String creditToAccount) {

		this.creditToAccount = creditToAccount;
	}

	public String getReasonType() {

		return reasonType;
	}

	public void setReasonType(String reasonType) {

		this.reasonType = reasonType;
	}

	public String getReasonRefNo() {

		return reasonRefNo;
	}

	public void setReasonRefNo(String reasonRefNo) {

		this.reasonRefNo = reasonRefNo;
	}

	public TransactionRefType getReasonRefType() {

		return reasonRefType;
	}

	public void setReasonRefType(TransactionRefType reasonRefType) {

		this.reasonRefType = reasonRefType;
	}

	public String getReasonNote() {
		return reasonNote;
	}

	public void setReasonNote(String reasonNote) {
		this.reasonNote = reasonNote;
	}

	public String getTriggredBy() {
		return triggredBy;
	}

	public void setTriggredBy(String triggredBy) {
		this.triggredBy = triggredBy;
	}

	@Override
	public String toString() {
		return "TransactionWithDate [id=" + id + ", amount=" + amount + ", currencyCode=" + currencyCode
				+ ", debitFromAccount=" + debitFromAccount + ", creditToAccount=" + creditToAccount + ", reasonType="
				+ reasonType + ", reasonRefNo=" + reasonRefNo + ", reasonRefType=" + reasonRefType + ", reasonNote="
				+ reasonNote + ", triggredBy=" + triggredBy + ", creationTime=" + creationTime + "]";
	}

	public static class Constants extends AbstractMongoEntityBean.Constants {
		public static final String CREDIT_TO_ACCOUNT = "creditToAccount";
		public static final String REASON_REF_TYPE = "reasonRefType";
		public static final String REASON_REF_NO = "reasonRefNo";
		public static final String DEFAULT_CURRENCY_CODE = "INR";
	}

}
