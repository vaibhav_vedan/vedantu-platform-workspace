package com.vedantu.platform.pojo.subscription;

import java.util.List;

import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetCancelSubscriptionReq extends AbstractFrontEndReq{
	private Long subscriptionId;
	private List<Long> subscriptionDetailsIds;
	private List<SessionInfo> sessionList;

	public GetCancelSubscriptionReq() {
		super();
	}

	public GetCancelSubscriptionReq(Long subscriptionId, List<Long> subscriptionDetailsIds,
			List<SessionInfo> sessionList) {
		super();
		this.subscriptionId = subscriptionId;
		this.subscriptionDetailsIds = subscriptionDetailsIds;
		this.sessionList=sessionList;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public List<Long> getSubscriptionDetailsIds() {
		return subscriptionDetailsIds;
	}

	public void setSubscriptionDetailsIds(List<Long> subscriptionDetailsIds) {
		this.subscriptionDetailsIds = subscriptionDetailsIds;
	}

	public List<SessionInfo> getSessionList() {
		return sessionList;
	}

	public void setSessionList(List<SessionInfo> sessionList) {
		this.sessionList = sessionList;
	}
}
