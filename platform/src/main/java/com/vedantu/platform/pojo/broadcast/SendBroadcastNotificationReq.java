package com.vedantu.platform.pojo.broadcast;

import com.vedantu.notification.enums.NotificationType;
import com.vedantu.platform.interfaces.broadcast.INotificationInfo;


public class SendBroadcastNotificationReq {

	private Long userId;
	private NotificationType notificationType;
	private SendAskADoubtNotificationInfo notificationInfo;
	
	public SendBroadcastNotificationReq() {
		super();
	}

	public SendBroadcastNotificationReq(Long userId, NotificationType notificationType,
			SendAskADoubtNotificationInfo notificationInfo) {
		super();
		this.userId = userId;
		this.notificationType = notificationType;
		this.notificationInfo = notificationInfo;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public SendAskADoubtNotificationInfo getNotificationInfo() {
		return notificationInfo;
	}

	public void setNotificationInfo(SendAskADoubtNotificationInfo notificationInfo) {
		this.notificationInfo = notificationInfo;
	}

	@Override
	public String toString() {
		return "SendBroadcastNotificationReq [userId=" + userId
				+ ", notificationType=" + notificationType
				+ ", notificationInfo=" + notificationInfo + "]";
	}

}
