package com.vedantu.platform.pojo;

/**
 * Created by somil on 24/01/17.
 */
public class ExportTOSFailedAndNotScheduled {
    public String studentId;
    public String studentEmail;
    public String sessionIds;
    public String states;
    public String billingPeriods;

    public ExportTOSFailedAndNotScheduled() {
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getSessionIds() {
        return sessionIds;
    }

    public void setSessionIds(String sessionIds) {
        this.sessionIds = sessionIds;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getBillingPeriods() {
        return billingPeriods;
    }

    public void setBillingPeriods(String billingPeriods) {
        this.billingPeriods = billingPeriods;
    }

    @Override
    public String toString() {
        return "ExportTOSFailedAndNotScheduled{" + "studentId=" + studentId + ", studentEmail=" + studentEmail + ", sessionIds=" + sessionIds + ", states=" + states + ", billingPeriods=" + billingPeriods + '}';
    }
}
