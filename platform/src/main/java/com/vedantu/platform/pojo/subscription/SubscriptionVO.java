package com.vedantu.platform.pojo.subscription;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SubscriptionVO extends AbstractFrontEndReq {

    private String entityId;
    private EntityType entityType;
    private Integer count;
    private Long hours;
    private SessionModel model;
    private String target;
    private Integer grade;
    private Long boardId;
    private SessionSchedule sessionSchedule;
    private Long teacherId;
    private String transactionId;
    private Long totalCost;
    private Long offeringId;
    private Long studentId;
    private Long subscriptionId;
    private String note;
    private String title;
    private String teacherDiscountCouponId;
    private Long teacherDiscountAmount;
    private String vedantuDiscountCouponId;
    private Long vedantuDiscountAmount;
    private SubModel subModel;
    private String subject;
    private PaymentType paymentType;
    private SessionSource sessionSource;

    public SubscriptionVO() {
        super();
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public SessionSchedule getSessionSchedule() {
        return sessionSchedule;
    }

    public void setSessionSchedule(SessionSchedule sessionSchedule) {
        this.sessionSchedule = sessionSchedule;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Long totalCost) {
        this.totalCost = totalCost;
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTeacherDiscountCouponId() {
        return teacherDiscountCouponId;
    }

    public void setTeacherDiscountCouponId(String teacherDiscountCouponId) {
        this.teacherDiscountCouponId = teacherDiscountCouponId;
    }

    public Long getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public String getVedantuDiscountCouponId() {
        return vedantuDiscountCouponId;
    }

    public void setVedantuDiscountCouponId(String vedantuDiscountCouponId) {
        this.vedantuDiscountCouponId = vedantuDiscountCouponId;
    }

    public Long getVedantuDiscountAmount() {
        return vedantuDiscountAmount;
    }

    public void setVedantuDiscountAmount(Long vedantuDiscountAmount) {
        this.vedantuDiscountAmount = vedantuDiscountAmount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SubModel getSubModel() {
        return subModel;
    }

    public void setSubModel(SubModel subModel) {
        this.subModel = subModel;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public SessionSource getSessionSource() {
        return sessionSource;
    }

    public void setSessionSource(SessionSource sessionSource) {
        this.sessionSource = sessionSource;
    }

}
