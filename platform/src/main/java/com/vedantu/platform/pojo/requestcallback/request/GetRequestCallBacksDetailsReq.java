package com.vedantu.platform.pojo.requestcallback.request;

import java.util.List;

import com.vedantu.platform.enums.requestcallback.LeadAssignee;
import com.vedantu.platform.enums.requestcallback.TeacherLeadStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetRequestCallBacksDetailsReq extends AbstractFrontEndListReq{

	public String id;
	public Long studentId;
	public Long teacherId;
	public Long boardId;
	public Long grade;
	public String target;
	public Boolean parent;
	public LeadAssignee assignedTo;
	public Boolean teacherActionTaken;
	public List<TeacherLeadStatus> teacherLeadStatuses;
	public Long fromTime;
	public Long toTime;
	
	public GetRequestCallBacksDetailsReq(String id,Long studentId, Long teacherId,
			Long boardId, Long grade, String target, Boolean parent,
			LeadAssignee assignedTo, Boolean teacherActionTaken,
			List<TeacherLeadStatus> teacherLeadStatuses,Long fromTime, Long toTime, Integer start, Integer limit) {
		super(start,limit);
		this.id = id;
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.boardId = boardId;
		this.grade = grade;
		this.target = target;
		this.parent = parent;
		this.assignedTo = assignedTo;
		this.teacherActionTaken = teacherActionTaken;
		this.teacherLeadStatuses = teacherLeadStatuses;
		this.fromTime = fromTime;
		this.toTime = toTime;
	}

        public GetRequestCallBacksDetailsReq() {
        }
        
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getGrade() {
		return grade;
	}

	public void setGrade(Long grade) {
		this.grade = grade;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Boolean getParent() {
		return parent;
	}

	public void setParent(Boolean parent) {
		this.parent = parent;
	}

	public LeadAssignee getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(LeadAssignee assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Boolean getTeacherActionTaken() {
		return teacherActionTaken;
	}

	public void setTeacherActionTaken(Boolean teacherActionTaken) {
		this.teacherActionTaken = teacherActionTaken;
	}

	public List<TeacherLeadStatus> getTeacherLeadStatuses() {
		return teacherLeadStatuses;
	}

	public void setTeacherLeadStatuses(List<TeacherLeadStatus> teacherLeadStatuses) {
		this.teacherLeadStatuses = teacherLeadStatuses;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public Long getFromTime() {
		return fromTime;
	}

	public void setFromTime(Long fromTime) {
		this.fromTime = fromTime;
	}

	public Long getToTime() {
		return toTime;
	}

	public void setToTime(Long toTime) {
		this.toTime = toTime;
	}
}
