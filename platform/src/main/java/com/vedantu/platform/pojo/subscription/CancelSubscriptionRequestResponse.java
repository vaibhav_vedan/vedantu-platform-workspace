package com.vedantu.platform.pojo.subscription;

import com.vedantu.util.BasicResponse;

public class CancelSubscriptionRequestResponse extends BasicResponse{
	private Long refundAmount;
	
	public CancelSubscriptionRequestResponse() {
		super();
	}

	public CancelSubscriptionRequestResponse(Long refundAmount) {
		super();
		this.refundAmount = refundAmount;
	}

	public Long getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Long refundAmount) {
		this.refundAmount = refundAmount;
	}

	@Override
	public String toString() {
		return "CancelSubscriptionRequestResponse [refundAmount=" + refundAmount + "]";
	}
}
