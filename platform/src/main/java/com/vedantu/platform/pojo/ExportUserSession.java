package com.vedantu.platform.pojo;

/**
 * Created by somil on 24/01/17.
 */

public class ExportUserSession {
    public String teacherName;
    public String studentName;
    public String sessionId;
    public String sessionTitle;
    public String subject;
    public String topic;
    public String packageName;
    public String grade;

    public float sessionTime;
    public String sessionDate;
    public String startTime;
    public String endTime;
    public String startedAt;
    public String endedAt;
    public String displayState;
    public String type;
    public int payRate;
    public int chargeRate;
    public String replayLink;

    public String bookedDate;
    public String bookedTime;

    public long packageTime;
    public long packagePaidAmount;
    public String packageRequestDate;
    public String packagePaidDate;
    public String packageStartDate;
    public String packageEndDate;
    public String studentState;
    public String studentFeedback;
    public String studentComments;
    public String teacherState;
    public String teacherFeedback;
    public String teacherComments;
    public String remarks;
    public String lastUpdatedName;
    public String lastUpdatedRole;
    public String lastUpdatedEmail;
    public String lastUpdatedDate;
    public String lastUpdatedTime;

    public float sessionRatingStudent;
    public float sessionRatingTeacher;
    public float teacherRating;

    public String studentReason;
    public float studentReviewRating;
    public String studentReviewReason;
    public String studentReview;

    public String teacherReason;
    public String teacherSessionCoverage;
    public String teacherStudentPerformance;
    public String teacherHomeWork;
    public String teacherNextSessionPlan;
    public String teacherRemark;

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionTitle() {
        return sessionTitle;
    }

    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public float getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(float sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(String endedAt) {
        this.endedAt = endedAt;
    }

    public String getDisplayState() {
        return displayState;
    }

    public void setDisplayState(String displayState) {
        this.displayState = displayState;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPayRate() {
        return payRate;
    }

    public void setPayRate(int payRate) {
        this.payRate = payRate;
    }

    public int getChargeRate() {
        return chargeRate;
    }

    public void setChargeRate(int chargeRate) {
        this.chargeRate = chargeRate;
    }

    public String getReplayLink() {
        return replayLink;
    }

    public void setReplayLink(String replayLink) {
        this.replayLink = replayLink;
    }

    public long getPackageTime() {
        return packageTime;
    }

    public void setPackageTime(long packageTime) {
        this.packageTime = packageTime;
    }

    public long getPackagePaidAmount() {
        return packagePaidAmount;
    }

    public void setPackagePaidAmount(long packagePaidAmount) {
        this.packagePaidAmount = packagePaidAmount;
    }

    public String getPackageRequestDate() {
        return packageRequestDate;
    }

    public void setPackageRequestDate(String packageRequestDate) {
        this.packageRequestDate = packageRequestDate;
    }

    public String getPackagePaidDate() {
        return packagePaidDate;
    }

    public void setPackagePaidDate(String packagePaidDate) {
        this.packagePaidDate = packagePaidDate;
    }

    public String getPackageStartDate() {
        return packageStartDate;
    }

    public void setPackageStartDate(String packageStartDate) {
        this.packageStartDate = packageStartDate;
    }

    public String getPackageEndDate() {
        return packageEndDate;
    }

    public void setPackageEndDate(String packageEndDate) {
        this.packageEndDate = packageEndDate;
    }

    public String getStudentFeedback() {
        return studentFeedback;
    }

    public void setStudentFeedback(String studentFeedback) {
        this.studentFeedback = studentFeedback;
    }

    public String getStudentComments() {
        return studentComments;
    }

    public void setStudentComments(String studentComments) {
        this.studentComments = studentComments;
    }

    public String getTeacherFeedback() {
        return teacherFeedback;
    }

    public void setTeacherFeedback(String teacherFeedback) {
        this.teacherFeedback = teacherFeedback;
    }

    public String getTeacherComments() {
        return teacherComments;
    }

    public void setTeacherComments(String teacherComments) {
        this.teacherComments = teacherComments;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStudentState() {
        return studentState;
    }

    public void setStudentState(String studentState) {
        this.studentState = studentState;
    }

    public String getTeacherState() {
        return teacherState;
    }

    public void setTeacherState(String teacherState) {
        this.teacherState = teacherState;
    }

    public String getLastUpdatedName() {
        return lastUpdatedName;
    }

    public void setLastUpdatedName(String lastUpdatedName) {
        this.lastUpdatedName = lastUpdatedName;
    }

    public String getLastUpdatedRole() {
        return lastUpdatedRole;
    }

    public void setLastUpdatedRole(String lastUpdatedRole) {
        this.lastUpdatedRole = lastUpdatedRole;
    }

    public String getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(String bookedDate) {
        this.bookedDate = bookedDate;
    }

    public String getBookedTime() {
        return bookedTime;
    }

    public void setBookedTime(String bookedTime) {
        this.bookedTime = bookedTime;
    }

    public String getLastUpdatedEmail() {
        return lastUpdatedEmail;
    }

    public void setLastUpdatedEmail(String lastUpdatedEmail) {
        this.lastUpdatedEmail = lastUpdatedEmail;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(String lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public float getSessionRatingStudent() {
        return sessionRatingStudent;
    }

    public void setSessionRatingStudent(float sessionRatingStudent) {
        this.sessionRatingStudent = sessionRatingStudent;
    }

    public float getSessionRatingTeacher() {
        return sessionRatingStudent;
    }

    public void setSessionRatingTeacher(float sessionRatingTeacher) {
        this.sessionRatingTeacher = sessionRatingTeacher;
    }

    public float getTeacherRating() {
        return teacherRating;
    }

    public void setTeacherRating(float teacherRating) {
        this.teacherRating = teacherRating;
    }

    public String getStudentReason() {
        return studentReason;
    }

    public void setStudentReason(String studentReason) {
        this.studentReason = studentReason;
    }

    public float getStudentReviewRating() {
        return studentReviewRating;
    }

    public void setStudentReviewRating(float studentReviewRating) {
        this.studentReviewRating = studentReviewRating;
    }

    public String getStudentReviewReason() {
        return studentReviewReason;
    }

    public void setStudentReviewReason(String studentReviewReason) {
        this.studentReviewReason = studentReviewReason;
    }

    public String getStudentReview() {
        return studentReview;
    }

    public void setStudentReview(String studentReview) {
        this.studentReview = studentReview;
    }

    public String getTeacherReason() {
        return teacherReason;
    }

    public void setTeacherReason(String teacherReason) {
        this.teacherReason = teacherReason;
    }

    public String getTeacherSessionCoverage() {
        return teacherSessionCoverage;
    }

    public void setTeacherSessionCoverage(String teacherSessionCoverage) {
        this.teacherSessionCoverage = teacherSessionCoverage;
    }

    public String getTeacherStudentPerformance() {
        return teacherStudentPerformance;
    }

    public void setTeacherStudentPerformance(String teacherStudentPerformance) {
        this.teacherStudentPerformance = teacherStudentPerformance;
    }

    public String getTeacherHomeWork() {
        return teacherHomeWork;
    }

    public void setTeacherHomeWork(String teacherHomeWork) {
        this.teacherHomeWork = teacherHomeWork;
    }

    public String getTeacherNextSessionPlan() {
        return teacherNextSessionPlan;
    }

    public void setTeacherNextSessionPlan(String teacherNextSessionPlan) {
        this.teacherNextSessionPlan = teacherNextSessionPlan;
    }

    public String getTeacherRemark() {
        return teacherRemark;
    }

    public void setTeacherRemark(String teacherRemark) {
        this.teacherRemark = teacherRemark;
    }

    // @Override
    // public String toString() {
    // return "ExportUser {userId:" + userId + ", firstName:" + firstName
    // + ", lastName:" + lastName + ", grade:" + grade + ", board:"
    // + board + ", school:" + school + ", parentContactNo:"
    // + parentContactNo + ", contactNo:" + contactNumber + ", emailId:"
    // + email + "}";
    // }


    @Override
    public String toString() {
        return "ExportUserSession{" +
                "teacherName='" + teacherName + '\'' +
                ", studentName='" + studentName + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", sessionTitle='" + sessionTitle + '\'' +
                ", subject='" + subject + '\'' +
                ", topic='" + topic + '\'' +
                ", packageName='" + packageName + '\'' +
                ", grade='" + grade + '\'' +
                ", sessionTime=" + sessionTime +
                ", sessionDate='" + sessionDate + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", startedAt='" + startedAt + '\'' +
                ", endedAt='" + endedAt + '\'' +
                ", displayState='" + displayState + '\'' +
                ", type='" + type + '\'' +
                ", payRate=" + payRate +
                ", chargeRate=" + chargeRate +
                ", replayLink='" + replayLink + '\'' +
                ", bookedDate='" + bookedDate + '\'' +
                ", bookedTime='" + bookedTime + '\'' +
                ", packageTime=" + packageTime +
                ", packagePaidAmount=" + packagePaidAmount +
                ", packageRequestDate='" + packageRequestDate + '\'' +
                ", packagePaidDate='" + packagePaidDate + '\'' +
                ", packageStartDate='" + packageStartDate + '\'' +
                ", packageEndDate='" + packageEndDate + '\'' +
                ", studentState='" + studentState + '\'' +
                ", studentFeedback='" + studentFeedback + '\'' +
                ", studentComments='" + studentComments + '\'' +
                ", teacherState='" + teacherState + '\'' +
                ", teacherFeedback='" + teacherFeedback + '\'' +
                ", teacherComments='" + teacherComments + '\'' +
                ", remarks='" + remarks + '\'' +
                ", lastUpdatedName='" + lastUpdatedName + '\'' +
                ", lastUpdatedRole='" + lastUpdatedRole + '\'' +
                ", lastUpdatedEmail='" + lastUpdatedEmail + '\'' +
                ", lastUpdatedDate='" + lastUpdatedDate + '\'' +
                ", lastUpdatedTime='" + lastUpdatedTime + '\'' +
                ", sessionRatingStudent=" + sessionRatingStudent +
                ", sessionRatingTeacher=" + sessionRatingTeacher +
                ", teacherRating=" + teacherRating +
                ", studentReason='" + studentReason + '\'' +
                ", studentReviewRating=" + studentReviewRating +
                ", studentReviewReason='" + studentReviewReason + '\'' +
                ", studentReview='" + studentReview + '\'' +
                ", teacherReason='" + teacherReason + '\'' +
                ", teacherSessionCoverage='" + teacherSessionCoverage + '\'' +
                ", teacherStudentPerformance='" + teacherStudentPerformance + '\'' +
                ", teacherHomeWork='" + teacherHomeWork + '\'' +
                ", teacherNextSessionPlan='" + teacherNextSessionPlan + '\'' +
                ", teacherRemark='" + teacherRemark + '\'' +
                '}';
    }
}

