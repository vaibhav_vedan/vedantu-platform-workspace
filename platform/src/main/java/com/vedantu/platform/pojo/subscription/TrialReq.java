package com.vedantu.platform.pojo.subscription;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class TrialReq extends AbstractFrontEndReq {

	String userId;


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
