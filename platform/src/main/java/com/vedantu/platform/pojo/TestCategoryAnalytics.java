package com.vedantu.platform.pojo;

import lombok.Data;

@Data
public class TestCategoryAnalytics {
    private String name;
    private AnalyticsInfo info;
}
