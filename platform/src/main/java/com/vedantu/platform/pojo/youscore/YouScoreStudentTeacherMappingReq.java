/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.youscore;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author somil
 */
public class YouScoreStudentTeacherMappingReq extends YouScoreAbstractBasicReq {
	@SerializedName("MappingId")
	public List<YouScoreMappingEntry> MappingId = new ArrayList<YouScoreMappingEntry>();

	public YouScoreStudentTeacherMappingReq() {
		super();
	}

	public YouScoreStudentTeacherMappingReq(List<YouScoreMappingEntry> mappingId) {
		super();
		MappingId = mappingId;
	}

	public List<YouScoreMappingEntry> getMappingId() {
		return MappingId;
	}

	public void setMappingId(List<YouScoreMappingEntry> mappingId) {
		MappingId = mappingId;
	}

	public void addMapping(YouScoreMappingEntry youScoreMappingEntry) {
		MappingId.add(youScoreMappingEntry);
	}

	@Override
	public String toString() {
		return "YouScoreStudentTeacherMappingReq [MappingId=" + MappingId + "]";
	}
}
