/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

/**
 *
 * @author parashar
 */
public class ContextSharedFormInfo {
    private String contextId;
    private Long sharedOn;
    private Integer numOfResponses;
    private String formId;
    private Integer numOfStudentsSharedWith;

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the sharedOn
     */
    public Long getSharedOn() {
        return sharedOn;
    }

    /**
     * @param sharedOn the sharedOn to set
     */
    public void setSharedOn(Long sharedOn) {
        this.sharedOn = sharedOn;
    }

    /**
     * @return the numOfResponses
     */
    public Integer getNumOfResponses() {
        return numOfResponses;
    }

    /**
     * @param numOfResponses the numOfResponses to set
     */
    public void setNumOfResponses(Integer numOfResponses) {
        this.numOfResponses = numOfResponses;
    }

    /**
     * @return the formId
     */
    public String getFormId() {
        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * @return the numOfStudentsSharedWith
     */
    public Integer getNumOfStudentsSharedWith() {
        return numOfStudentsSharedWith;
    }

    /**
     * @param numOfStudentsSharedWith the numOfStudentsSharedWith to set
     */
    public void setNumOfStudentsSharedWith(Integer numOfStudentsSharedWith) {
        this.numOfStudentsSharedWith = numOfStudentsSharedWith;
    }
}
