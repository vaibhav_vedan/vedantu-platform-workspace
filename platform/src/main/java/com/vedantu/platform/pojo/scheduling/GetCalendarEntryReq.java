package com.vedantu.platform.pojo.scheduling;

import org.springframework.http.HttpRequest;

public class GetCalendarEntryReq {
	private Long startTime;
	private Long endTime;

	public GetCalendarEntryReq() {
		super();
	}

//	public GetCalendarEntryReq(HttpRequest req) {
//		super(req);
//	}

	public GetCalendarEntryReq(Long startTime, Long endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
}

