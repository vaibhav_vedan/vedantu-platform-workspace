/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class FeedbackFormDynamicSection {
    
    private String dynamicEntityName;
    private List<FeedbackQuestion> questions;
    private Set<Long> userIds;

    /**
     * @return the dynamicEntityName
     */
    public String getDynamicEntityName() {
        return dynamicEntityName;
    }

    /**
     * @param dynamicEntityName the dynamicEntityName to set
     */
    public void setDynamicEntityName(String dynamicEntityName) {
        this.dynamicEntityName = dynamicEntityName;
    }

    /**
     * @return the questions
     */
    public List<FeedbackQuestion> getQuestions() {
        return questions;
    }

    /**
     * @param questions the questions to set
     */
    public void setQuestions(List<FeedbackQuestion> questions) {
        this.questions = questions;
    }

    /**
     * @return the userIds
     */
    public Set<Long> getUserIds() {
        return userIds;
    }

    /**
     * @param userIds the userIds to set
     */
    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }

    @Override
    public String toString() {
        return "FeedbackFormDynamicSection{" + "dynamicEntityName=" + dynamicEntityName + ", questions=" + questions + ", userIds=" + userIds + '}';
    }
    
}
