package com.vedantu.platform.pojo.Challenges;

import java.util.List;

import com.vedantu.platform.enums.ChallengerGradeType.ChallengerGradeType;

public class Challenges {

	private List<ChallengePOJO> dayChallenges;
	private List<ChallengeLeaderboard> leaderboards;

	public List<ChallengeLeaderboard> getLeaderboards() {
		return leaderboards;
	}

	public void setLeaderboards(List<ChallengeLeaderboard> leaderboards) {
		this.leaderboards = leaderboards;
	}

	public List<ChallengePOJO> getDayChallenges() {
		return dayChallenges;
	}

	public void setDayChallenges(List<ChallengePOJO> dayChallenges) {
		this.dayChallenges = dayChallenges;
	}
	
}
