/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import com.vedantu.platform.enums.feedback.FeedbackFormType;
import com.vedantu.session.pojo.EntityType;

/**
 *
 * @author parashar
 */
public class StudentSharedFormInfo {
    
    private EntityType contextType;
    private String contextId;
    private String formName;
    private FeedbackFormType feedbackFormType;
    private boolean responded = false;
    private Long sharedOn;
    private Long respondedOn;
    private String formId;
    private String contextName;

    /**
     * @return the contextType
     */
    public EntityType getContextType() {
        return contextType;
    }

    /**
     * @param contextType the contextType to set
     */
    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the formName
     */
    public String getFormName() {
        return formName;
    }

    /**
     * @param formName the formName to set
     */
    public void setFormName(String formName) {
        this.formName = formName;
    }

    /**
     * @return the feedbackFormType
     */
    public FeedbackFormType getFeedbackFormType() {
        return feedbackFormType;
    }

    /**
     * @param feedbackFormType the feedbackFormType to set
     */
    public void setFeedbackFormType(FeedbackFormType feedbackFormType) {
        this.feedbackFormType = feedbackFormType;
    }

    /**
     * @return the responded
     */
    public boolean isResponded() {
        return responded;
    }

    /**
     * @param responded the responded to set
     */
    public void setResponded(boolean responded) {
        this.responded = responded;
    }

    /**
     * @return the sharedOn
     */
    public Long getSharedOn() {
        return sharedOn;
    }

    /**
     * @param sharedOn the sharedOn to set
     */
    public void setSharedOn(Long sharedOn) {
        this.sharedOn = sharedOn;
    }

    /**
     * @return the respondedOn
     */
    public Long getRespondedOn() {
        return respondedOn;
    }

    /**
     * @param respondedOn the respondedOn to set
     */
    public void setRespondedOn(Long respondedOn) {
        this.respondedOn = respondedOn;
    }

    /**
     * @return the formId
     */
    public String getFormId() {
        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * @return the contextName
     */
    public String getContextName() {
        return contextName;
    }

    /**
     * @param contextName the contextName to set
     */
    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    @Override
    public String toString() {
        return "StudentSharedFormInfo{" + "contextType=" + contextType + ", contextId=" + contextId + ", formName=" + formName + ", feedbackFormType=" + feedbackFormType + ", responded=" + responded + ", sharedOn=" + sharedOn + ", respondedOn=" + respondedOn + ", formId=" + formId + ", contextName=" + contextName + '}';
    }
    
}
