package com.vedantu.platform.pojo.subscription;

import java.util.List;
import com.vedantu.subscription.response.SubscriptionResponse;

public class GetSubscriptionsResponse {
	
	private List<SubscriptionResponse> subscriptionsList;

	public GetSubscriptionsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetSubscriptionsResponse(List<SubscriptionResponse> subscriptionsList) {
		super();
		this.subscriptionsList = subscriptionsList;
	}

	public List<SubscriptionResponse> getSubscriptionsList() {
		return subscriptionsList;
	}

	public void setSubscriptionsList(List<SubscriptionResponse> subscriptionsList) {
		this.subscriptionsList = subscriptionsList;
	}
}
