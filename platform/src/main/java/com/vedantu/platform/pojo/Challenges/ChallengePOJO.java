package com.vedantu.platform.pojo.Challenges;

import com.vedantu.platform.enums.ChallengerGradeType.ChallengerGradeType;

public class ChallengePOJO {

	private Long startTime;
	private Long duration;
	private Integer numberOfQuestions = 0;
	private String testName = null;
	private String testLink;
	private String rewardTitle;
	private String rewardDescription;
	private ChallengerGradeType challengerGradeType;
	
	
	public Long getStartTime() {
		return startTime;
	}
	
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	
	public Long getDuration() {
		return duration;
	}
	
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	
	public Integer getNumberOfQuestions() {
		return numberOfQuestions;
	}
	
	public void setNumberOfQuestions(Integer numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}
	
	public String getTestLink() {
		return testLink;
	}
	
	public void setTestLink(String testLink) {
		this.testLink = testLink;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getRewardTitle() {
		return rewardTitle;
	}

	public void setRewardTitle(String rewardTitle) {
		this.rewardTitle = rewardTitle;
	}

	public String getRewardDescription() {
		return rewardDescription;
	}

	public void setRewardDescription(String rewardDescription) {
		this.rewardDescription = rewardDescription;
	}

	public ChallengerGradeType getChallengerGradeType() {
		return challengerGradeType;
	}

	public void setChallengerGradeType(ChallengerGradeType challengerGradeType) {
		this.challengerGradeType = challengerGradeType;
	}
	
}
