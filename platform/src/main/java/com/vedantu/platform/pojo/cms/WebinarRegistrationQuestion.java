/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.cms;

/**
 *
 * @author jeet
 */
public class WebinarRegistrationQuestion {
    private String question;

    public WebinarRegistrationQuestion() {
    }
    
    public WebinarRegistrationQuestion(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
    
}
