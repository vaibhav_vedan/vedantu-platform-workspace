/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo;

/**
 *
 * @author jeet
 */
public class ISLWhatsappMessage {
    private String business_id ="pzg8PjD5c2cL4oekx";
    private String mobile_nos;
    private String text;

    public String getMobile_nos() {
        return mobile_nos;
    }

    public void setMobile_nos(String mobile_nos) {
        this.mobile_nos = mobile_nos;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ISLWhatsappMessage(String mobile_nos, String text) {
        this.mobile_nos = mobile_nos;
        this.text = text;
    }

    @Override
    public String toString() {
        return "ISLWhatsappMessage{" + "business_id=" + business_id + ", mobile_nos=" + mobile_nos + ", text=" + text + '}';
    }
    
}
