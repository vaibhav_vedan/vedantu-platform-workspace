package com.vedantu.platform.pojo.referral.response;

import com.vedantu.platform.enums.referral.ReferralStep;

public class ReferralStepResponse {

	private ReferralStep name;
	private String value;
	
	public ReferralStepResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReferralStep getName() {
		return name;
	}

	public void setName(ReferralStep name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ReferralStepResponse [name=" + name + ", value=" + value + "]";
	}
	
}
