package com.vedantu.platform.pojo.broadcast;

import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SendBroadcastRequest extends AbstractFrontEndReq {

	public List<Long> userIds;
	public BroadcastType broadcastType;
	public Integer level;
	public String referenceId;
	public Integer grade;
	public String boardId;

	public SendBroadcastRequest() {
		super();
	}

	public SendBroadcastRequest(List<Long> userIds,
			BroadcastType broadcastType, Integer level, String referenceId,
			Integer grade, String boardId) {
		super();
		this.userIds = userIds;
		this.broadcastType = broadcastType;
		this.level = level;
		this.referenceId = referenceId;
		this.grade = grade;
		this.boardId = boardId;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	public BroadcastType getBroadcastType() {
		return broadcastType;
	}

	public void setBroadcastType(BroadcastType broadcastType) {
		this.broadcastType = broadcastType;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

        @Override
	public void verify() throws BadRequestException {

		List<String> verificationErrors = super.collectVerificationErrors();

//		if (userIds == null || userIds.isEmpty()) {
//			verificationErrors.add("USER_IDS");
//		}
		
		if(broadcastType == null){
			verificationErrors.add("BROADCAST_TYPE");
		}

		if (CollectionUtils.isNotEmpty(verificationErrors)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
					verificationErrors.toString());
		}
	}
}
