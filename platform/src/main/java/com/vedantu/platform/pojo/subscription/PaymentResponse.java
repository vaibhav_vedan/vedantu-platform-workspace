package com.vedantu.platform.pojo.subscription;

import com.vedantu.exception.ErrorCode;

public class PaymentResponse {

	private Payment payment;

	public PaymentResponse() {
		super();
	}

	public PaymentResponse(Payment payment, ErrorCode errorCode, String errorMessage) {
		this.payment = payment;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	@Override
	public String toString() {
		return "PaymentResponse [payment=" + payment + "]";
	}
}
