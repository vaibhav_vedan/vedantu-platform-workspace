package com.vedantu.platform.pojo.subscription;

import java.util.List;

import com.vedantu.User.UserInfo;
import com.vedantu.dinero.pojo.TeacherSlabPlan;
import com.vedantu.session.pojo.SessionSlot;

public class SubscriptionRequestInfo extends SubscriptionRequest {
	private TeacherSlabPlan plan;
	private List<SessionSlot> scheduleSlotInfos;
	private List<SessionSlot> allSlotInfos;
	private UserInfo teacher;
	private UserInfo student;
	private Integer totalConflictCount = 0;
	private Long scheduleStartTime;
	private String noScheduleText;

	public SubscriptionRequestInfo() {
		super();
	}

	public TeacherSlabPlan getPlan() {
		return plan;
	}

	public void setPlan(TeacherSlabPlan plan) {
		this.plan = plan;
	}

	public List<SessionSlot> getScheduleSlotInfos() {
		return scheduleSlotInfos;
	}

	public void setScheduleSlotInfos(List<SessionSlot> scheduleSlotInfos) {
		this.scheduleSlotInfos = scheduleSlotInfos;
	}

	public List<SessionSlot> getAllSlotInfos() {
		return allSlotInfos;
	}

	public void setAllSlotInfos(List<SessionSlot> allSlotInfos) {
		this.allSlotInfos = allSlotInfos;
	}

	public UserInfo getTeacher() {
		return teacher;
	}

	public void setTeacher(UserInfo teacher) {
		this.teacher = teacher;
	}

	public UserInfo getStudent() {
		return student;
	}

	public void setStudent(UserInfo student) {
		this.student = student;
	}

	public Integer getTotalConflictCount() {
		return totalConflictCount;
	}

	public void setTotalConflictCount(Integer totalConflictCount) {
		this.totalConflictCount = totalConflictCount;
	}

	public Long getScheduleStartTime() {
		return scheduleStartTime;
	}

	public void setScheduleStartTime(Long scheduleStartTime) {
		this.scheduleStartTime = scheduleStartTime;
	}

	public String getNoScheduleText() {
		return noScheduleText;
	}

	public void setNoScheduleText(String noScheduleText) {
		this.noScheduleText = noScheduleText;
	}

        @Override
        public String toString() {
            return "SubscriptionRequestInfo{" + "plan=" + plan + ", scheduleSlotInfos=" + scheduleSlotInfos + ", allSlotInfos=" + allSlotInfos + ", teacher=" + teacher + ", student=" + student + ", totalConflictCount=" + totalConflictCount + ", scheduleStartTime=" + scheduleStartTime + ", noScheduleText=" + noScheduleText + '}';
        }


}
