package com.vedantu.platform.pojo.dinero;

public class TeacherSessionInfo {

	Long teacherId;
	Long billingDuration;
	int sessionsTaught;

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getBillingDuration() {
		return billingDuration;
	}

	public void setBillingDuration(Long billingDuration) {
		this.billingDuration = billingDuration;
	}

	public int getSessionsTaught() {
		return sessionsTaught;
	}

	public void setSessionsTaught(int sessionsTaught) {
		this.sessionsTaught = sessionsTaught;
	}

	@Override
	public String toString() {
		return "TeacherSessionInfo [teacherId=" + teacherId + ", billingDuration=" + billingDuration
				+ ", sessionsTaught=" + sessionsTaught + "]";
	}

	public TeacherSessionInfo(Long teacherId, Long billingDuration, int sessionsTaught) {
		super();
		this.teacherId = teacherId;
		this.billingDuration = billingDuration;
		this.sessionsTaught = sessionsTaught;
	}

	public TeacherSessionInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

}
