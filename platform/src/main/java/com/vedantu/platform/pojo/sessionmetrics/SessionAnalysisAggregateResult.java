/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.sessionmetrics;

import com.vedantu.platform.enums.sessionmetrics.SessionAnalysisAggregatorAction;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author jeet
 */
public class SessionAnalysisAggregateResult {
    private SessionAnalysisAggregatorAction action;
    private Map avgResp;
    private List<Map> perUserAvgResp;

    public SessionAnalysisAggregateResult(SessionAnalysisAggregatorAction action, Map avgResp, List<Map> perUserAvgResp) {
        this.action = action;
        this.avgResp = avgResp;
        this.perUserAvgResp = perUserAvgResp;
    }

    public SessionAnalysisAggregateResult(SessionAnalysisAggregatorAction action) {
        this.action = action;
    }

    public SessionAnalysisAggregatorAction getAction() {
        return action;
    }

    public void setAction(SessionAnalysisAggregatorAction action) {
        this.action = action;
    }

    public Map getAvgResp() {
        return avgResp;
    }

    public void setAvgResp(Map avgResp) {
        this.avgResp = avgResp;
    }

    public List<Map> getPerUserAvgResp() {
        return perUserAvgResp;
    }

    public void setPerUserAvgResp(List<Map> perUserAvgResp) {
        this.perUserAvgResp = perUserAvgResp;
    }

    @Override
    public String toString() {
        return "SessionAnalysisAggregateResult{" + "action=" + action + ", avgResp=" + avgResp + ", perUserAvgResp=" + perUserAvgResp + '}';
    }
    
    
}
