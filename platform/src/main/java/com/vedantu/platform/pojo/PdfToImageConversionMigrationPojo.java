package com.vedantu.platform.pojo;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.platform.seo.enums.SeoDomain;
import com.vedantu.util.StringUtils;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PdfToImageConversionMigrationPojo {
    private String categoryPageUrl;
    private SeoDomain domain;

    public void verify() throws VException {

        if(StringUtils.isEmpty(categoryPageUrl) || domain == null)
            throw new VException(ErrorCode.SERVICE_ERROR, "Invalid Request");
    }
}
