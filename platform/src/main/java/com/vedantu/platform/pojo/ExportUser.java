package com.vedantu.platform.pojo;

import com.vedantu.User.Role;

import java.util.List;

/**
 * Created by somil on 23/01/17.
 */
public class ExportUser {
    public String userId;
    public String firstName;
    public String lastName;
    public String grade;
    public String board;
    public String school;
    public String gender;
    public List<String> parentContactNumbers;
    public String parentEmailId;
    public String contactNumber;
    public String email;
    public String registrationDate;
    public String registrationTime;
    public Boolean isEmailVerified;
    public Boolean isContactNumberVerified;
    public String packageRequested;
    public String requestedDate;
    public String requestedTime;
    public Role role;
    public String campaign;
    public String campaignMedium;
    public String campaignSource;
    public Boolean parentRegistration;

    public String country;
    public String state;
    public String city;
    public String street;
    public Integer pincode;

    public Long numberOfSessions;
    public int numberOfSessionsOutsidePackage;
    public int numberOfSessionsOutsidePackageFree;
    public int numberOfSessionsOutsidePackagePaid;
    public int numberOfSessionsInsidePackage;
    public int numberOfSessionsInsidePackageFree;
    public int numberOfSessionsInsidePackagePaid;

    public float activeDuration;
    public float activeDurationOutsidePackage;
    public float activeDurationOutsidePackageFree;
    public float activeDurationOutsidePackagePaid;
    public float activeDurationInsidePackage;
    public float activeDurationInsidePackageFree;
    public float activeDurationInsidePackagePaid;

    // Teacher Data
    public String experience;
    public String latestEducation;
    public String latestJob;
    public String primarySubject;
    public String secondarySubject;
    public String biggestStrength;
    public String professionalCategory;
    public List<String> grades;
    public List<String> categories;
    public String primaryCallingNumber;
    public String extensionNumber;
    public String responseTime;
    public Boolean active;
    public Float rating;
    public Integer numberOfRatings;
    public Float numberOfHoursTaught;
    public Boolean profilePicUploaded;
    public List<String> courses;
    public List<String> languagePrefs;
    public Float retainabilityIndex;
    public Float punctualityIndex;
    public Float regularityIndex;
    public Float contentSharingIndex;
    public Long responseTimeIndex;
    public Long usersChattedWith;

    public String signUpURL;

    public String referralCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public List<String> getParentContactNumbers() {
        return this.parentContactNumbers;
    }

    public void setParentContactNumbers(List<String> parentContactNumbers) {
        this.parentContactNumbers = parentContactNumbers;
    }

    public String getParentEmailId() {
        return parentEmailId;
    }

    public void setParentEmailId(String parentEmailId) {
        this.parentEmailId = parentEmailId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime) {
        this.registrationTime = registrationTime;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public Boolean getIsContactNumberVerified() {
        return isContactNumberVerified;
    }

    public void setIsContactNumberVerified(Boolean isContactNumberVerified) {
        this.isContactNumberVerified = isContactNumberVerified;
    }

    public String getPackageRequested() {
        return packageRequested;
    }

    public void setPackageRequested(String packageRequested) {
        this.packageRequested = packageRequested;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getRequestedTime() {
        return requestedTime;
    }

    public void setRequestedTime(String requestedTime) {
        this.requestedTime = requestedTime;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getCampaignMedium() {
        return campaignMedium;
    }

    public void setCampaignMedium(String campaignMedium) {
        this.campaignMedium = campaignMedium;
    }

    public String getCampaignSource() {
        return campaignSource;
    }

    public void setCampaignSource(String campaignSource) {
        this.campaignSource = campaignSource;
    }

    public Boolean getParentRegistration() {
        return parentRegistration;
    }

    public void setParentRegistration(Boolean parentRegistration) {
        this.parentRegistration = parentRegistration;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public Long getNumberOfSessions() {
        return numberOfSessions;
    }

    public void setNumberOfSessions(Long numberOfSessions) {
        this.numberOfSessions = numberOfSessions;
    }

    public int getNumberOfSessionsOutsidePackage() {
        return numberOfSessionsOutsidePackage;
    }

    public void setNumberOfSessionsOutsidePackage(
            int numberOfSessionsOutsidePackage) {
        this.numberOfSessionsOutsidePackage = numberOfSessionsOutsidePackage;
    }

    public int getNumberOfSessionsOutsidePackageFree() {
        return numberOfSessionsOutsidePackageFree;
    }

    public void setNumberOfSessionsOutsidePackageFree(
            int numberOfSessionsOutsidePackageFree) {
        this.numberOfSessionsOutsidePackageFree = numberOfSessionsOutsidePackageFree;
    }

    public int getNumberOfSessionsOutsidePackagePaid() {
        return numberOfSessionsOutsidePackagePaid;
    }

    public void setNumberOfSessionsOutsidePackagePaid(
            int numberOfSessionsOutsidePackagePaid) {
        this.numberOfSessionsOutsidePackagePaid = numberOfSessionsOutsidePackagePaid;
    }

    public int getNumberOfSessionsInsidePackage() {
        return numberOfSessionsInsidePackage;
    }

    public void setNumberOfSessionsInsidePackage(
            int numberOfSessionsInsidePackage) {
        this.numberOfSessionsInsidePackage = numberOfSessionsInsidePackage;
    }

    public int getNumberOfSessionsInsidePackageFree() {
        return numberOfSessionsInsidePackageFree;
    }

    public void setNumberOfSessionsInsidePackageFree(
            int numberOfSessionsInsidePackageFree) {
        this.numberOfSessionsInsidePackageFree = numberOfSessionsInsidePackageFree;
    }

    public int getNumberOfSessionsInsidePackagePaid() {
        return numberOfSessionsInsidePackagePaid;
    }

    public void setNumberOfSessionsInsidePackagePaid(
            int numberOfSessionsInsidePackagePaid) {
        this.numberOfSessionsInsidePackagePaid = numberOfSessionsInsidePackagePaid;
    }

    public float getActiveDuration() {
        return activeDuration;
    }

    public void setActiveDuration(float activeDuration) {
        this.activeDuration = activeDuration;
    }

    public float getActiveDurationOutsidePackage() {
        return activeDurationOutsidePackage;
    }

    public void setActiveDurationOutsidePackage(
            float activeDurationOutsidePackage) {
        this.activeDurationOutsidePackage = activeDurationOutsidePackage;
    }

    public float getActiveDurationOutsidePackageFree() {
        return activeDurationOutsidePackageFree;
    }

    public void setActiveDurationOutsidePackageFree(
            float activeDurationOutsidePackageFree) {
        this.activeDurationOutsidePackageFree = activeDurationOutsidePackageFree;
    }

    public float getActiveDurationOutsidePackagePaid() {
        return activeDurationOutsidePackagePaid;
    }

    public void setActiveDurationOutsidePackagePaid(
            float activeDurationOutsidePackagePaid) {
        this.activeDurationOutsidePackagePaid = activeDurationOutsidePackagePaid;
    }

    public float getActiveDurationInsidePackage() {
        return activeDurationInsidePackage;
    }

    public void setActiveDurationInsidePackage(float activeDurationInsidePackage) {
        this.activeDurationInsidePackage = activeDurationInsidePackage;
    }

    public float getActiveDurationInsidePackageFree() {
        return activeDurationInsidePackageFree;
    }

    public void setActiveDurationInsidePackageFree(
            float activeDurationInsidePackageFree) {
        this.activeDurationInsidePackageFree = activeDurationInsidePackageFree;
    }

    public float getActiveDurationInsidePackagePaid() {
        return activeDurationInsidePackagePaid;
    }

    public void setActiveDurationInsidePackagePaid(
            float activeDurationInsidePackagePaid) {
        this.activeDurationInsidePackagePaid = activeDurationInsidePackagePaid;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getLatestEducation() {
        return latestEducation;
    }

    public void setLatestEducation(String latestEducation) {
        this.latestEducation = latestEducation;
    }

    public String getLatestJob() {
        return latestJob;
    }

    public void setLatestJob(String latestJob) {
        this.latestJob = latestJob;
    }

    public String getPrimarySubject() {
        return primarySubject;
    }

    public void setPrimarySubject(String primarySubject) {
        this.primarySubject = primarySubject;
    }

    public String getSecondarySubject() {
        return secondarySubject;
    }

    public void setSecondarySubject(String secondarySubject) {
        this.secondarySubject = secondarySubject;
    }

    public String getBiggestStrength() {
        return biggestStrength;
    }

    public void setBiggestStrength(String biggestStrength) {
        this.biggestStrength = biggestStrength;
    }

    public String getProfessionalCategory() {
        return professionalCategory;
    }

    public void setProfessionalCategory(String professionalCategory) {
        this.professionalCategory = professionalCategory;
    }

    public String getPrimaryCallingNumber() {
        return primaryCallingNumber;
    }

    public void setPrimaryCallingNumber(String primaryCallingNumber) {
        this.primaryCallingNumber = primaryCallingNumber;
    }

    public String getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(String extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getNoOfRatings() {
        return numberOfRatings;
    }

    public void setNoOfRatings(Integer numberOfRatings) {
        this.numberOfRatings = numberOfRatings;
    }

    public Float getNumberOfHoursTaught() {
        return numberOfHoursTaught;
    }

    public void setNumberOfHoursTaught(Float numberOfHoursTaught) {
        this.numberOfHoursTaught = numberOfHoursTaught;
    }

    public Boolean getProfilePicUploaded() {
        return profilePicUploaded;
    }

    public void setProfilePicUploaded(Boolean profilePicUploaded) {
        this.profilePicUploaded = profilePicUploaded;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getLanguagePrefs() {
        return languagePrefs;
    }

    public void setLanguagePrefs(List<String> languagePrefs) {
        this.languagePrefs = languagePrefs;
    }

    public Float getRetainabilityIndex() {
        return retainabilityIndex;
    }

    public void setRetainabilityIndex(Float retainabilityIndex) {
        this.retainabilityIndex = retainabilityIndex;
    }

    public Float getPunctualityIndex() {
        return punctualityIndex;
    }

    public void setPunctualityIndex(Float punctualityIndex) {
        this.punctualityIndex = punctualityIndex;
    }

    public Float getRegularityIndex() {
        return regularityIndex;
    }

    public void setRegularityIndex(Float regularityIndex) {
        this.regularityIndex = regularityIndex;
    }

    public Float getContentSharingIndex() {
        return contentSharingIndex;
    }

    public void setContentSharingIndex(Float contentSharingIndex) {
        this.contentSharingIndex = contentSharingIndex;
    }

    public Long getResponseTimeIndex() {
        return responseTimeIndex;
    }

    public void setResponseTimeIndex(Long responseTimeIndex) {
        this.responseTimeIndex = responseTimeIndex;
    }

    public Long getUsersChattedWith() {
        return usersChattedWith;
    }

    public void setUsersChattedWith(Long usersChattedWith) {
        this.usersChattedWith = usersChattedWith;
    }

    public String getSignUpURL() {
        return signUpURL;
    }

    public void setSignUpURL(String signUpURL) {
        this.signUpURL = signUpURL;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    @Override
    public String toString() {
        return "ExportUser [userId=" + userId + ", firstName=" + firstName
                + ", lastName=" + lastName + ", grade=" + grade + ", board="
                + board + ", school=" + school + ", gender=" + gender
                + ", parentContactNumbers=" + parentContactNumbers
                + ", parentEmailId=" + parentEmailId + ", contactNumber="
                + contactNumber + ", email=" + email + ", registrationDate="
                + registrationDate + ", registrationTime=" + registrationTime
                + ", isEmailVerified=" + isEmailVerified
                + ", isContactNumberVerified=" + isContactNumberVerified
                + ", packageRequested=" + packageRequested + ", requestedDate="
                + requestedDate + ", requestedTime=" + requestedTime
                + ", role=" + role + ", campaign=" + campaign
                + ", campaignMedium=" + campaignMedium + ", campaignSource="
                + campaignSource + ", parentRegistration=" + parentRegistration
                + ", country=" + country + ", state=" + state + ", city="
                + city + ", street=" + street + ", pincode=" + pincode
                + ", numberOfSessions=" + numberOfSessions
                + ", numberOfSessionsOutsidePackage="
                + numberOfSessionsOutsidePackage
                + ", numberOfSessionsOutsidePackageFree="
                + numberOfSessionsOutsidePackageFree
                + ", numberOfSessionsOutsidePackagePaid="
                + numberOfSessionsOutsidePackagePaid
                + ", numberOfSessionsInsidePackage="
                + numberOfSessionsInsidePackage
                + ", numberOfSessionsInsidePackageFree="
                + numberOfSessionsInsidePackageFree
                + ", numberOfSessionsInsidePackagePaid="
                + numberOfSessionsInsidePackagePaid + ", activeDuration="
                + activeDuration + ", activeDurationOutsidePackage="
                + activeDurationOutsidePackage
                + ", activeDurationOutsidePackageFree="
                + activeDurationOutsidePackageFree
                + ", activeDurationOutsidePackagePaid="
                + activeDurationOutsidePackagePaid
                + ", activeDurationInsidePackage="
                + activeDurationInsidePackage
                + ", activeDurationInsidePackageFree="
                + activeDurationInsidePackageFree
                + ", activeDurationInsidePackagePaid="
                + activeDurationInsidePackagePaid + ", experience="
                + experience + ", latestEducation=" + latestEducation
                + ", latestJob=" + latestJob + ", primarySubject="
                + primarySubject + ", secondarySubject=" + secondarySubject
                + ", biggestStrength=" + biggestStrength
                + ", professionalCategory=" + professionalCategory
                + ", grades=" + grades + ", categories=" + categories
                + ", primaryCallingNumber=" + primaryCallingNumber
                + ", extensionNumber=" + extensionNumber + ", responseTime="
                + responseTime + ", active=" + active + ", rating=" + rating
                + ", numberOfRatings=" + numberOfRatings
                + ", numberOfHoursTaught=" + numberOfHoursTaught
                + ", profilePicUploaded=" + profilePicUploaded + ", courses="
                + courses + ", languagePrefs=" + languagePrefs
                + ", retainabilityIndex=" + retainabilityIndex
                + ", punctualityIndex=" + punctualityIndex
                + ", regularityIndex=" + regularityIndex
                + ", contentSharingIndex=" + contentSharingIndex
                + ", responseTimeIndex=" + responseTimeIndex
                + ", usersChattedWith=" + usersChattedWith + ", signUpURL="
                + signUpURL + ", referralCode=" + referralCode + "]";
    }

}

