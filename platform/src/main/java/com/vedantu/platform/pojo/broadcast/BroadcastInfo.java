package com.vedantu.platform.pojo.broadcast;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.platform.enums.broadcast.BroadcastStatus;
import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.platform.interfaces.broadcast.Broadcastable;

public class BroadcastInfo {

	private String id;
	private BroadcastType broadcastType;
	private Integer level;
	private Long lastBroadcastTime;
	private BroadcastStatus status;
	private UserBasicInfo user;
	private Broadcastable entity;
	private Long creationTime;

	public BroadcastInfo() {
		super();
	}

	public BroadcastInfo(String id, BroadcastType broadcastType, Integer level,
			Long lastBroadcastTime, BroadcastStatus status, UserBasicInfo user,
			Broadcastable entity, Long creationTime) {
		super();
		this.id = id;
		this.broadcastType = broadcastType;
		this.level = level;
		this.lastBroadcastTime = lastBroadcastTime;
		this.status = status;
		this.user = user;
		this.entity = entity;
		this.creationTime = creationTime;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BroadcastType getBroadcastType() {
		return broadcastType;
	}

	public void setBroadcastType(BroadcastType broadcastType) {
		this.broadcastType = broadcastType;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getLastBroadcastTime() {
		return lastBroadcastTime;
	}

	public void setLastBroadcastTime(Long lastBroadcastTime) {
		this.lastBroadcastTime = lastBroadcastTime;
	}

	public BroadcastStatus getStatus() {
		return status;
	}

	public void setStatus(BroadcastStatus status) {
		this.status = status;
	}

	public UserBasicInfo getUser() {
		return user;
	}

	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	public Broadcastable getEntity() {
		return entity;
	}

	public void setEntity(Broadcastable entity) {
		this.entity = entity;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "BroadcastInfo [id=" + id + ", broadcastType=" + broadcastType
				+ ", level=" + level + ", lastBroadcastTime="
				+ lastBroadcastTime + ", status=" + status + ", user=" + user
				+ ", entity=" + entity + ", creationTime=" + creationTime + "]";
	}

}
