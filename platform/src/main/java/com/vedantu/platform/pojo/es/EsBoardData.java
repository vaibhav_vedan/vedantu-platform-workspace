/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.es;

import com.vedantu.session.pojo.VisibilityState;
import java.util.Set;

/**
 *
 * @author somil
 */
public class EsBoardData {

	public String name;
	public String slug;
	public Long parentId;
	public VisibilityState state;

	public Set<String> categories;
	public Set<String> grades;
	public Set<String> exams;
	public String parentInfo;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public VisibilityState getState() {
		return state;
	}
	public void setState(VisibilityState state) {
		this.state = state;
	}
	public Set<String> getCategories() {
		return categories;
	}
	public void setCategories(Set<String> categories) {
		this.categories = categories;
	}
	public Set<String> getGrades() {
		return grades;
	}
	public void setGrades(Set<String> grades) {
		this.grades = grades;
	}
	public Set<String> getExams() {
		return exams;
	}
	public void setExams(Set<String> exams) {
		this.exams = exams;
	}
	public String getParentInfo() {
		return parentInfo;
	}
	public void setParentInfo(String parentInfo) {
		this.parentInfo = parentInfo;
	}
	
}
