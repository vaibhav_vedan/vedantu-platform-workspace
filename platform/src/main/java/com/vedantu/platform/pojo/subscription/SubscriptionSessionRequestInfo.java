/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.subscription;

import com.vedantu.platform.interfaces.broadcast.INotificationInfo;

/**
 *
 * @author ajith
 */
public class SubscriptionSessionRequestInfo implements INotificationInfo{
	public Long studentId;
	public Long teacherId;
	public Long startTime;
	public Long endTime;
	public String note;
	public Long subscriptionId;
	public String subscriptionName;

	public SubscriptionSessionRequestInfo() {
		super();
	}

	public SubscriptionSessionRequestInfo(Long studentId, Long teacherId, Long startTime, Long endTime, String note,
			Long subscriptionId, String subscriptionName) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.note = note;
		this.subscriptionId = subscriptionId;
		this.subscriptionName = subscriptionName;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getSubscriptionName() {
		return subscriptionName;
	}

	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}

	@Override
	public String toString() {
		return "SubscriptionSessionRequestInfo [studentId=" + studentId + ", teacherId=" + teacherId + ", startTime="
				+ startTime + ", endTime=" + endTime + ", note=" + note + ", subscriptionId=" + subscriptionId
				+ ", subscriptionName=" + subscriptionName + "]";
	}
}
