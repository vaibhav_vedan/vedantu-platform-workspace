package com.vedantu.platform.pojo.scheduling;
public class SearchFilter {

    private String filterName;
    private String filterType;
    private String[] filterParams;

    public SearchFilter() {
    }

    public SearchFilter(String filterName, String filterType, String[] filterParams) {
        this.filterName = filterName;
        this.filterType = filterType;
        this.filterParams = filterParams;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String[] getfilterParams() {
        return filterParams;
    }

    public void setfilterParams(String[] filterParams) {
        this.filterParams = filterParams;
    }

}

