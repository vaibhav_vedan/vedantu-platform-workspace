package com.vedantu.platform.pojo;

/**
 * Created by somil on 27/03/17.
 */
public class ISLReportDetail {
    private String voucherCode;
    private String offer;
    private String linkIcse;
    private String linkCbse;
    private String brochureLink;


    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getLinkIcse() {
        return linkIcse;
    }

    public void setLinkIcse(String linkIcse) {
        this.linkIcse = linkIcse;
    }

    public String getLinkCbse() {
        return linkCbse;
    }

    public void setLinkCbse(String linkCbse) {
        this.linkCbse = linkCbse;
    }

    public String getBrochureLink() {
        return brochureLink;
    }

    public void setBrochureLink(String brochureLink) {
        this.brochureLink = brochureLink;
    }
}
