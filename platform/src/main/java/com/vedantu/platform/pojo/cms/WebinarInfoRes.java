/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.cms;

import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author jeet
 */
public class WebinarInfoRes extends AbstractRes{

    /*
	 * { "webinarKey": "5578289065203599105", "webinarId": "5578289065203599105",
	 * "organizerKey": "7470416618302296069", "omid": "5578289065203599105",
	 * "accountKey": "94810155808804869", "name": "TurboMath - Be a Maths Genius!",
	 * "subject": "TurboMath - Be a Maths Genius!", "description": "", "startTime":
	 * "2017-08-12T06:30:00Z", "endTime": "2017-08-12T07:30:00Z", "times": [ {
	 * "startTime": "2017-08-12T06:30:00Z", "endTime": "2017-08-12T07:30:00Z" } ],
	 * "webinarTimes": [ { "startTime": "2017-08-12T06:30:00Z", "endTime":
	 * "2017-08-12T07:30:00Z" } ], "timeZone": "Asia/Calcutta", "locale": "en_US",
	 * "status": "NEW", "approvalType": "AUTOMATIC", "registrationUrl":
	 * "https://attendee.gotowebinar.com/register/5578289065203599105", "impromptu":
	 * false, "isPasswordProtected": false, "type": "single_session",
	 * "registrationSettingsKey": "10273025" }
     */
    private String webinarKey;
    private String webinarId;
    private String organizerKey;
    private String accountKey;

    private String name;
    private String subject;
    private String description;

    private String startTime;
    private String endTime;
    private String timeZone;
    private String registrationUrl;
    private List<GTWInterval> times;

    public WebinarInfoRes() {
        super();
    }

    public String getWebinarKey() {
        return webinarKey;
    }

    public void setWebinarKey(String webinarKey) {
        this.webinarKey = webinarKey;
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public String getOrganizerKey() {
        return organizerKey;
    }

    public void setOrganizerKey(String organizerKey) {
        this.organizerKey = organizerKey;
    }

    public String getAccountKey() {
        return accountKey;
    }

    public void setAccountKey(String accountKey) {
        this.accountKey = accountKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getRegistrationUrl() {
        return registrationUrl;
    }

    public void setRegistrationUrl(String registrationUrl) {
        this.registrationUrl = registrationUrl;
    }

    public List<GTWInterval> getTimes() {
        return times;
    }

    public void setTimes(List<GTWInterval> times) {
        this.times = times;
    }

}
