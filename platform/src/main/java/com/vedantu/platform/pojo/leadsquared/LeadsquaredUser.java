package com.vedantu.platform.pojo.leadsquared;

import com.google.gson.annotations.SerializedName;
import com.vedantu.platform.enums.LeadsquaredAgentBucket;

import java.util.List;

public class LeadsquaredUser {
    @SerializedName("ID")
    private String leadsquaredId;

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("LastName")
    private String lastName;

    @SerializedName("EmailAddress")
    private String email;

    @SerializedName("Role")
    private String role;

    @SerializedName("StatusCode")
    private String active;

    @SerializedName("UserId")
    private String userId;

    @SerializedName("AssociatedPhoneNumbers")
    private String associatedPhoneNumbers;

    private String convoxAgentId;

    private List<LeadsquaredAgentBucket> bucketName;

    public LeadsquaredUser() {
        super();
    }

    public String getLeadsquaredId() {
        return leadsquaredId;
    }

    public void setLeadsquaredId(String leadsquaredId) {
        this.leadsquaredId = leadsquaredId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<LeadsquaredAgentBucket> getBucketName() {
        return bucketName;
    }

    public void setBucketName(List<LeadsquaredAgentBucket> bucketName) {
        this.bucketName = bucketName;
    }

    public String getConvoxAgentId() {
        return convoxAgentId;
    }

    public void setConvoxAgentId(String convoxAgentId) {
        this.convoxAgentId = convoxAgentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAssociatedPhoneNumbers() {
        return associatedPhoneNumbers;
    }

    public void setAssociatedPhoneNumbers(String associatedPhoneNumbers) {
        this.associatedPhoneNumbers = associatedPhoneNumbers;
    }

}
