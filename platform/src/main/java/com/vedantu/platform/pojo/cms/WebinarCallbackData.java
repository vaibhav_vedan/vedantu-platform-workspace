package com.vedantu.platform.pojo.cms;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.RequestSource;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "WebinarCallbackData")
public class WebinarCallbackData extends AbstractMongoStringIdEntity {

    private String webinarId;
    private Long time;
    private Long userId;
    private RequestSource source = RequestSource.MOBILE;
    private String inboundNumber;

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public RequestSource getSource() {
        return source;
    }

    public void setSource(RequestSource source) {
        this.source = source;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getInboundNumber() {
        return inboundNumber;
    }

    public void setInboundNumber(String inboundNumber) {
        this.inboundNumber = inboundNumber;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String WEBINARID = "webinarId";
        public static final String TIME = "time";
        public static final String USERID = "userId";
        public static final String SOURCE = "source";

    }

}
