package com.vedantu.platform.pojo;


import lombok.Data;

import java.util.List;

@Data
public class TestAnalytics {
    private String testId;
    private String testAttemptId;
    private Long studentId;
    private Double percentile;
    private Integer rank;
    private AnalyticsInfo info;
    private List<TestCategoryAnalytics> categories;
    private String name;

    private Long testStartTime;
    private Boolean groupAnalyticsComputed;

    private String eventCategory;
}
