package com.vedantu.platform.pojo.leadsquared;

import java.util.*;

import com.vedantu.User.FeatureSource;
import com.vedantu.User.Gender;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.PhoneNumber;
import com.vedantu.User.SocialInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.User;
import com.vedantu.platform.utils.LeadSquaredUtils;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;

public class LeadDetails {
	// As the fields are directly mapped to the ones in Lead squared fields, it
	// does not follow the camel case format
	public String ProspectID;
	public String FirstName;
	public String LastName;
	public String EmailAddress;
	public String Mobile;
	public String Phone;
	public String SourceCampaign;
	public String Source;
	public String SourceMedium;
	public String SourceContent;
	public String Grade;
	public String mx_Source_Channel;
	public String mx_Source_term;
	public String mx_SourceMedium;

	public String mx_Board;
	public String mx_School;
	public String mx_User_Target;
	public String mx_PhoneNumberList;
	public String mx_CreationTime;
	public String mx_ModifiedTime;
        /*
        The Following are removed Post deletion in LeadSquared
        */
        /*
	public String mx_Gender;
        public String mx_Street1;
        public String mx_featureHide;
        */
	public String mx_LanguageList;
	public String mx_Country;
	public String mx_State;
	public String mx_City;
	public String mx_Zip;


	public String mx_TempNumber;
	public String mx_Tempphcode;

	public String FacebookId;
	public String TwitterId;
	public String LinkedInId;
	public boolean mx_IsEmailVerifed;
	public boolean mx_IsContactNumberVerified;
	public boolean mx_IsContactNumberWhiteListed;
	public boolean mx_IsContactNumberDND;
	public boolean mx_IsParentRegistration;
	public String mx_Parent_Contact_Number;
	public String mx_User_Id;
	public String mx_Query_Type;
	public String mx_Sign_up_feature;
	public String mx_Sign_up_URL;
	

	// Lead squared fields
	public String Revenue;
	public boolean mx_SusbcriptionRequestLead;
	public String mx_ghfh;
        public String mx_Sibling_Name;
    public String mx_GPS_City;
    public String mx_GPS_Long;
    public String mx_GPS_lat;
    public String OwnerId;


	public LeadDetails() {
		super();
	}

	public LeadDetails(User user) {
            
		super();
                ArrayList<String> referrers = new ArrayList<>(Arrays.asList("vedantu","google","yahoo","bing","rediff","ask"));
                ArrayList<String> signUpURLParams = new ArrayList<>(Arrays.asList("courses.vedantu.com","fmat","find-me-a-teacher","talk_to_counsellor","vedantu.com/courses"));
                
		this.FirstName = user.getFirstName();
		this.LastName = user.getLastName();
		this.EmailAddress = user.getEmail();
		this.Mobile = user.getContactNumber();
		this.Phone = user.getContactNumber();
		this.Source = user.getUtm_source();
		this.SourceMedium = user.getUtm_medium();
		this.mx_SourceMedium = user.getUtm_medium();
		this.SourceCampaign = user.getUtm_campaign();
		this.mx_Source_term = user.getUtm_term();
		this.SourceContent = user.getUtm_content();
		this.mx_Source_Channel = user.getChannel();
		this.mx_TempNumber = user.getTempContactNumber();
		this.mx_Tempphcode = user.getTempPhoneCode();

		StudentInfo studentInfo = user.getStudentInfo();
		try {
			if (Integer.parseInt(studentInfo.getGrade()) >= 0) {
				this.Grade = studentInfo.getGrade();
			}
		} catch (Exception ex) {
			// Ignore the exception
		}

		this.mx_Board = studentInfo.getBoard();
		this.mx_School = studentInfo.getSchool();

		String target = "";
		try {
			if (StringUtils.isNotEmpty(studentInfo.getTarget()) && ArrayUtils.isEmpty(studentInfo.getExamTargets())) {
				target = studentInfo.getTarget();
			}else if (StringUtils.isEmpty(studentInfo.getTarget()) && ArrayUtils.isNotEmpty(studentInfo.getExamTargets())){
				target = String.join(",", studentInfo.getExamTargets());
			}else if (StringUtils.isNotEmpty(studentInfo.getTarget()) && ArrayUtils.isNotEmpty(studentInfo.getExamTargets())){
				List<String> targets = studentInfo.getExamTargets();
				if (!targets.contains(studentInfo.getTarget())) {
					targets.add(studentInfo.getTarget());
				}
				target = String.join(",", targets);
			}

		}catch (Exception e) {

		}
		this.mx_User_Target = target;

		List<PhoneNumber> phones = user.getPhones();
		if (phones != null && !phones.isEmpty()) {
			List<String> stringList = new ArrayList<String>();
			for (PhoneNumber phone : phones) {
				stringList.add(phone.getNumber());
			}
			this.mx_PhoneNumberList = LeadSquaredUtils.join(stringList, ",");
		}

		this.mx_CreationTime = LeadSquaredUtils.getLeadSquaredDate(user.getCreationTime());
		this.mx_ModifiedTime = LeadSquaredUtils.getLeadSquaredDate(user.getLastUpdated());
		Gender gender = user.getGender();
		if (gender != null) {
//			this.mx_Gender = gender.name();
		}

		List<String> stringList = user.getLanguagePrefs();
		if (stringList != null && !stringList.isEmpty()) {
			this.mx_LanguageList = LeadSquaredUtils.join(stringList, ",");
		}

		LocationInfo location = user.getLocationInfo();
		if(location!=null) {
			this.mx_Country = location.getCountry();
			this.mx_State = location.getState();
			this.mx_City = location.getCity();
			this.mx_Zip = location.getPincode();
//		this.mx_Street1 = location.getStreetAddress();
		}

		SocialInfo socialInfo = user.getSocialInfo();
		if(socialInfo!=null) {
			this.FacebookId = socialInfo.getFacebook();
			this.LinkedInId = socialInfo.getLinkedIn();
		}

		this.mx_IsContactNumberDND = user.getIsContactNumberDND() != null ? user.getIsContactNumberDND() : Boolean.FALSE;
		this.mx_IsEmailVerifed = user.getIsEmailVerified();
		this.mx_IsContactNumberVerified = user.getIsContactNumberVerified() != null ? user.getIsContactNumberVerified() : Boolean.FALSE;
		this.mx_IsContactNumberWhiteListed = user.getIsContactNumberWhitelisted() != null ? user.getIsContactNumberWhitelisted() : Boolean.FALSE;
		this.mx_IsParentRegistration = user.getParentRegistration() != null ? user.getParentRegistration() : Boolean.FALSE;

		if(user.getId() != null)
			this.mx_User_Id = user.getId().toString();

		this.mx_Sign_up_URL = user.getSignUpURL();
		FeatureSource feature = user.getSignUpFeature();
		if (feature != null) {
			this.mx_Sign_up_feature = feature.toString();
		}
		this.mx_ghfh = user.getPhoneCode();
               
                String channel = "";
                
                try {
                    if(user.getUtm_campaign()!= null && "SEO_PDF_DOWNLOAD".equalsIgnoreCase(user.getUtm_campaign())){
                        channel = "PDF";
                    }else if(user.getUtm_source() != null && "in".equalsIgnoreCase(user.getUtm_source())){
                        channel = "SEM";
                    }/*else if(user.getUtm_term() != null && user.getUtm_term().contains("SEO")){
                        channel = "SEO";
                    }*/
                    else if(StringUtils.isNotEmpty(user.getSignUpURL())){
                        String signupURL = user.getSignUpURL().toLowerCase();
                        for(String referrer : signUpURLParams){
                            if(signupURL.contains(referrer)){
                                channel = "Direct";
                                break;
                            }
                        }
                        if(user.getSignUpURL().endsWith("www.vedantu.com")){
                            channel = "Direct";
                        }
                        if(StringUtils.isEmpty(user.getReferrer()) && signupURL.contains("vedantu")){
                            channel = "Direct";
                        }
                    }
                    if(StringUtils.isEmpty(channel) && StringUtils.isNotEmpty(user.getReferrer())){
                        for(String referrer : referrers){
                            if(user.getReferrer().toLowerCase().contains(referrer)){
                                channel = "SEO";
                                break;
                            }
                        }
                    }
                }catch(Exception e){
                    
                }
                if(StringUtils.isEmpty(channel)){
                    channel = "Others";
                } 
                
                this.mx_Sibling_Name = channel;
	}

	public String getProspectID() {
		return ProspectID;
	}

	public void setProspectID(String prospectID) {
		ProspectID = prospectID;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getSourceCampaign() {
		return SourceCampaign;
	}

	public void setSourceCampaign(String sourceCampaign) {
		SourceCampaign = sourceCampaign;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getSourceMedium() {
		return SourceMedium;
	}

	public void setSourceMedium(String sourceMedium) {
		SourceMedium = sourceMedium;
	}

	public String getMx_Source_Channel() {
		return mx_Source_Channel;
	}

	public void setMx_Source_Channel(String mx_Source_Channel) {
		this.mx_Source_Channel = mx_Source_Channel;
	}

	public String getMx_Board() {
		return mx_Board;
	}

	public void setMx_Board(String mx_Board) {
		this.mx_Board = mx_Board;
	}

	public String getMx_School() {
		return mx_School;
	}

	public void setMx_School(String mx_School) {
		this.mx_School = mx_School;
	}

	public String getMx_PhoneNumberList() {
		return mx_PhoneNumberList;
	}

	public void setMx_PhoneNumberList(String mx_PhoneNumberList) {
		this.mx_PhoneNumberList = mx_PhoneNumberList;
	}

	public String getMx_CreationTime() {
		return mx_CreationTime;
	}

	public void setMx_CreationTime(String mx_CreationTime) {
		this.mx_CreationTime = mx_CreationTime;
	}

	public String getMx_ModifiedTime() {
		return mx_ModifiedTime;
	}

	public void setMx_ModifiedTime(String mx_ModifiedTime) {
		this.mx_ModifiedTime = mx_ModifiedTime;
	}

//	public String getMx_Gender() {
//		return mx_Gender;
//	}
//
//	public void setMx_Gender(String mx_Gender) {
//		this.mx_Gender = mx_Gender;
//	}

	public String getMx_LanguageList() {
		return mx_LanguageList;
	}

	public void setMx_LanguageList(String mx_LanguageList) {
		this.mx_LanguageList = mx_LanguageList;
	}

	public String getMx_Country() {
		return mx_Country;
	}

	public void setMx_Country(String mx_Country) {
		this.mx_Country = mx_Country;
	}

	public String getMx_State() {
		return mx_State;
	}

	public void setMx_State(String mx_State) {
		this.mx_State = mx_State;
	}

	public String getMx_City() {
		return mx_City;
	}

	public void setMx_City(String mx_City) {
		this.mx_City = mx_City;
	}

	public String getMx_Zip() {
		return mx_Zip;
	}

	public void setMx_Zip(String mx_Zip) {
		this.mx_Zip = mx_Zip;
	}

	public boolean isMx_IsEmailVerifed() {
		return mx_IsEmailVerifed;
	}

	public void setMx_IsEmailVerifed(boolean mx_IsEmailVerifed) {
		this.mx_IsEmailVerifed = mx_IsEmailVerifed;
	}

	public boolean isMx_IsContactNumberVerified() {
		return mx_IsContactNumberVerified;
	}

	public void setMx_IsContactNumberVerified(boolean mx_IsContactNumberVerified) {
		this.mx_IsContactNumberVerified = mx_IsContactNumberVerified;
	}

	public boolean isMx_IsContactNumberWhiteListed() {
		return mx_IsContactNumberWhiteListed;
	}

	public void setMx_IsContactNumberWhiteListed(boolean mx_IsContactNumberWhiteListed) {
		this.mx_IsContactNumberWhiteListed = mx_IsContactNumberWhiteListed;
	}

	public boolean isMx_IsParentRegistration() {
		return mx_IsParentRegistration;
	}

	public void setMx_IsParentRegistration(boolean mx_IsParentRegistration) {
		this.mx_IsParentRegistration = mx_IsParentRegistration;
	}

	public String getRevenue() {
		return Revenue;
	}

	public void setRevenue(String revenue) {
		Revenue = revenue;
	}

	public boolean isMx_IsContactNumberDND() {
		return mx_IsContactNumberDND;
	}

	public void setMx_IsContactNumberDND(boolean mx_IsContactNumberDND) {
		this.mx_IsContactNumberDND = mx_IsContactNumberDND;
	}

	public String getMx_User_Id() {
		return mx_User_Id;
	}

	public void setMx_User_Id(String mx_User_Id) {
		this.mx_User_Id = mx_User_Id;
	}

	public String getSourceContent() {
		return SourceContent;
	}

	public void setSourceContent(String sourceContent) {
		SourceContent = sourceContent;
	}

	public String getMx_Source_term() {
		return mx_Source_term;
	}

	public void setMx_Source_term(String mx_Source_term) {
		this.mx_Source_term = mx_Source_term;
	}

	public String getMx_Query_Type() {
		return mx_Query_Type;
	}

	public void setMx_Query_Type(String mx_Query_Type) {
		this.mx_Query_Type = mx_Query_Type;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getGrade() {
		return Grade;
	}

	public void setGrade(String grade) {
		Grade = grade;
	}

	public String getMx_Sign_up_feature() {
		return mx_Sign_up_feature;
	}

	public void setMx_Sign_up_feature(String mx_Sign_up_feature) {
		this.mx_Sign_up_feature = mx_Sign_up_feature;
	}

	public String getMx_Sign_up_URL() {
		return mx_Sign_up_URL;
	}

	public void setMx_Sign_up_URL(String mx_Sign_up_URL) {
		this.mx_Sign_up_URL = mx_Sign_up_URL;
	}

	public boolean isMx_SusbcriptionRequestLead() {
		return mx_SusbcriptionRequestLead;
	}

	public void setMx_SusbcriptionRequestLead(boolean mx_SusbcriptionRequestLead) {
		this.mx_SusbcriptionRequestLead = mx_SusbcriptionRequestLead;
	}

	public String getMx_SourceMedium() {
		return mx_SourceMedium;
	}

	public void setMx_SourceMedium(String mx_SourceMedium) {
		this.mx_SourceMedium = mx_SourceMedium;
	}

	public String getMx_Sibling_Name() {
            return mx_Sibling_Name;
        }

	public void setMx_Sibling_Name(String mx_Sibling_Name) {
            this.mx_Sibling_Name = mx_Sibling_Name;
        }

	public String getMx_TempNumber() {
		return mx_TempNumber;
	}

	public void setMx_TempNumber(String mx_TempNumber) {
		this.mx_TempNumber = mx_TempNumber;
	}

	public String getMx_Tempphonecode() {
		return mx_Tempphcode;
	}

	public void setMx_Tempphcode(String mx_Tempphcode) {
		this.mx_Tempphcode = mx_Tempphcode;
	}

	public String getMx_GPS_City() {
		return mx_GPS_City;
	}

	public void setMx_GPS_City(String mx_GPS_City) {
		this.mx_GPS_City = mx_GPS_City;
	}

	public String getMx_GPS_Long() {
		return mx_GPS_Long;
	}

	public void setMx_GPS_Long(String mx_GPS_Long) {
		this.mx_GPS_Long = mx_GPS_Long;
	}

	public String getMx_GPS_lat() {
		return mx_GPS_lat;
	}

	public void setMx_GPS_lat(String mx_GPS_lat) {
		this.mx_GPS_lat = mx_GPS_lat;
	}

	public String getOwnerId() { return OwnerId; }

	public void setOwnerId(String ownerId) { OwnerId = ownerId;}

	public String getMx_User_Target() {
		return mx_User_Target;
	}

	public void setMx_User_Target(String mx_User_Target) {
		this.mx_User_Target = mx_User_Target;
	}

	public static class Constants {
		public static final String EMAIL_ADDRESS = "EmailAddress";
		public static final String LEAD_STAGE = "ProspectStage";
		public static final String USER_ID = "mx_User_Id";
		public static final String MOBILE = "Mobile";
		public static final String PHONE = "Phone";
		public static final String IS_EMAIL_VERIFIED = "mx_IsEmailVerifed";
		public static final String IS_CONTACT_NUMBER_VERIFIED = "mx_IsContactNumberVerified";
		public static final String SUBSCRIPTION_REQUEST_LEAD = "mx_SusbcriptionRequestLead";
	}
}
